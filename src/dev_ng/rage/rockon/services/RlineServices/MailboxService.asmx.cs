using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Rline
{
    /// <summary>
    /// This service provides methods used by rlMailbox.  Since this is 
    /// a client-callable service, it should only have methods we are
    /// ok with clients seeing.  Methods that should only be called by
    /// admins should go in AdminMailboxService.
    /// </summary>
    [WebService(Namespace = "http://rline.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class MailboxService : System.Web.Services.WebService
    {
        //PURPOSE
        //  Broadcasts a binary message, using the account ID as sender.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "BroadcastMessageResult")]
        public TResult<Int64> BroadcastMessage(int titleId,
                                               Int64 accountId,
                                               string mailboxName,
                                               string senderDisplayName,
                                               byte[] userData,
                                               byte[] msgData,
                                               uint expirationMinutes)
        {
            TResult<Int64> result = new TResult<Int64>();
            Error err = null;

            try
            {
                Int64 recordId = 0;
                if (null == (err = Mailbox.CreateRecord(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         senderDisplayName,
                                                         Mailbox.BROADCAST_ID,
                                                         userData,
                                                         msgData,
                                                         expirationMinutes,
                                                         out recordId)))
                {
                    return result.SetSucceeded(recordId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Sends a binary message to specified receiver, using the account ID as sender.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "SendMessageResult")]
        public TResult<Int64> SendMessage(int titleId,
                                          Int64 accountId,
                                          string mailboxName,
                                          string senderDisplayName,
                                          Int64 receiverId,
                                          byte[] userData,
                                          byte[] msgData,
                                          uint expirationMinutes)
        {
            TResult<Int64> result = new TResult<Int64>();
            Error err = null;

            try
            {
                Int64 recordId = 0;
                if (null == (err = Mailbox.CreateRecord(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         senderDisplayName,
                                                         receiverId,
                                                         userData,
                                                         msgData,
                                                         expirationMinutes,
                                                         out recordId)))
                {
                    return result.SetSucceeded(recordId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns the number of records sent to the specified account that 
        //  can be read.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "GetNumRecordsResult")]
        public TResult<int> GetNumRecords(int titleId,
                                          Int64 accountId,
                                          string mailboxName,
                                          Int64 startRecordId,
                                          Int64 endRecordId,
                                          bool includeExpired)
        {
            TResult<int> result = new TResult<int>();
            Error err = null;

            try
            {
                int numRecords = 0;
                if (null == (err = Mailbox.QueryNumRecords(titleId,
                                                           accountId,
                                                           mailboxName,                                                            
                                                           Mailbox.ANY_ID,  //senderId
                                                           accountId,
                                                           startRecordId,
                                                           endRecordId,
                                                           includeExpired,
                                                           out numRecords)))
                {
                    return result.SetSucceeded(numRecords);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Result class returned by all ReadXXXX() web methods.
        public class ReadRecordsResult
        {
            public List<MailboxRecord> Records = null;
        }

        //PURPOSE
        //  Returns records sent to the specified account.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "ReadRecordsResult")]
        public TResult<ReadRecordsResult> ReadRecords(int titleId,
                                                      Int64 accountId,
                                                      string mailboxName,
                                                      Int64 startRecordId,
                                                      Int64 endRecordId,
                                                      uint maxRecords,
                                                      bool readUserData,
                                                      bool readMsgData,
                                                      bool oldestFirst)
        {
            TResult<ReadRecordsResult> result = new TResult<ReadRecordsResult>();
            Error err = null;

            try
            {
                ReadRecordsResult r = new ReadRecordsResult();

                if (null == (err = Mailbox.QueryRecords(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         Mailbox.ANY_ID,
                                                         accountId,
                                                         startRecordId,
                                                         endRecordId,
                                                         maxRecords,
                                                         readUserData,
                                                         readMsgData,
                                                         oldestFirst,
                                                         false,
                                                         out r.Records)))
                {
                    return result.SetSucceeded(r);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns msg data for specified record.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "ReadMsgDataResult")]
        public TResult<byte[]> ReadMsgData(int titleId,
                                           Int64 accountId,
                                           string mailboxName,
                                           Int64 recordId)
        {
            TResult<byte[]> result = new TResult<byte[]>();
            Error err = null;

            try
            {
                byte[] data;

                if (null == (err = Mailbox.QueryMsgData(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         Mailbox.ANY_ID,
                                                         accountId,
                                                         recordId,
                                                         out data)))
                {
                    return result.SetSucceeded(data);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Deletes a range of records.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteRecordsResult")]
        public TResult<uint> DeleteRecords(int titleId,
                                           Int64 accountId,
                                           string mailboxName,
                                           Int64 startRecordId,
                                           Int64 endRecordId)
        {
            TResult<uint> result = new TResult<uint>();
            Error err = null;

            try
            {
                uint numDeleted;

                if (null == (err = Mailbox.DeleteRecords(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         Mailbox.ANY_ID,
                                                         accountId,
                                                         startRecordId,
                                                         endRecordId,
                                                         false,
                                                         out numDeleted)))
                {
                    return result.SetSucceeded(numDeleted);
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
