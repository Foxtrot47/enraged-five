using System;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using System.Transactions;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;

using Rockstar.Rockon;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.SocialClub;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.SocialClub
{
    /// <summary>
    /// Summary description for AccountService
    /// </summary>
    [WebService(Namespace = "http://socialclub.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AccountService : System.Web.Services.WebService
    {
        //PURPOSE
        //  Response for all QueryCreate{Service}() calls.
        [XmlRoot("Response")]
        public class QueryCreateResponse : Response
        {
            [XmlIgnore]
            public bool AccountIdSpecified = false;
            public Int64 AccountId = 0;

            public void SetSucceeded(Int64 accountId)
            {
                base.SetSucceeded();

                AccountId = accountId;
                AccountIdSpecified = true;
            }

            public override void SetFailed(Error err)
            {
                base.SetFailed(err);
                AccountIdSpecified = false;
            }
        }

        //PURPOSE
        //  Returns rockon account ID for specified NP account
        [WebMethod]
        public void QueryCreateNp(string npOnlineId)
        {
            QueryCreateResponse r = new QueryCreateResponse();
            Error err = null;

            try
            {
                Int64 acctId;
                if (null == (err = AccountNpLink.QueryCreate(npOnlineId, out acctId)))
                {
                    r.SetSucceeded(acctId);
                }
                else
                {
                    r.SetFailed(err);
                }
            }
            catch (Exception ex)
            {
                r.SetFailed(new Error(ex));
            }

            r.SerializeToHttp(Context.Response);
        }

        //PURPOSE
        //  Returns rockon account ID for specified XBL account
        [WebMethod]
        public void QueryCreateXbl(UInt64 xuid, 
                                   string gamertag)
        {
            QueryCreateResponse r = new QueryCreateResponse();
            Error err = null;

            try
            {
                Int64 acctId;
                if (null == (err = AccountXblLink.QueryCreate((Int64)xuid, gamertag, out acctId)))
                {
                    r.SetSucceeded(acctId);
                }
                else
                {
                    r.SetFailed(err);
                }
            }
            catch (Exception ex)
            {
                r.SetFailed(new Error(ex));
            }

            r.SerializeToHttp(Context.Response);
        }
    }
}
