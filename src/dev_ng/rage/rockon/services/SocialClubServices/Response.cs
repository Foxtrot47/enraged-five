﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using Rockstar.Rockon.Net;

namespace Rockstar.Rockon.SocialClub
{
    //PURPOSE
    //  The serialization format of Social Club errors differs from Diag.Error,
    //  so we create a separate class for it.
    [XmlRoot("Error")]
    public class ResponseError
    {
        [XmlAttribute("Code")]
        public string m_Code;

        [XmlText]
        public string m_Msg;

        public ResponseError()
        {
        }

        public ResponseError(string code, string msg)
        {
            m_Code = code;
            m_Msg = msg;
        }
    }

    //PURPOSE
    //  Base class for all responses sent to the Social Club.  The base is only
    //  good for simple success/fail responses; to return data on success, you
    //  must create a derived class, taking care that the data members only
    //  serialize when m_Status is 1, otherwise you will get extra garbage in
    //  the result when there is an error.
    //
    //  Conditionally returning data is simple for reference types, since null
    //  members don't serialize.  For value members, there's a poorly-documented
    //  behavior you must use:
    //  - For each value member, you must have a bool member named
    //    {ValueMemberName}Specified.
    //  - Create a SetSucceeded() method that sets these bools to true.
    //
    //  When the XML serializer serializes value members, it looks for a corresponding
    //  {ValueMemberName}Specified member, and decides whether to serialize the member
    //  based on its value.
    //
    //  NOTE: Put an [XmlIgnore] before the {ValueMemberName}Specified bool, or it
    //        will be serialized as well.
    //
    //  Here is an example of a derived response class that does this:
    //
    //  [XmlRoot("Response")]   //Anything derived from Response must appear as <Response>
    //  class QueryAccountResponse : Response
    //  {
    //      public int AccountId;
    //
    //      [XmlIgnore]
    //      public bool AccountIdSpecififed = false;
    //
    //      public void SetSucceeded(int acctId)
    //      {
    //          AccountId = acctId;
    //          AccountIdSpecififed = true;
    //
    //          base.SetSucceeded();
    //      }
    //  }

    [XmlRoot("Response")]
    public class Response
    {
        [XmlElement("Status")]
        public int m_Status { get; set; }

        [XmlElement("Error")]
        public ResponseError m_Error { get; set; }

        public Response()
        {
            SetSucceeded();
        }

        public virtual void SetSucceeded()
        {
            m_Status = 1;
            m_Error = null;
        }

        public virtual void SetFailed(Diag.Error err)
        {
            m_Status = 0;

            if (err != null)
            {
                m_Error = new ResponseError(err.Code, err.Msg);
            }
            else
            {
                m_Error = null;
            }
        }

        public void SerializeToHttp(HttpResponse httpResponse)
        {
            httpResponse.Clear();
            httpResponse.StatusCode = 200;
            httpResponse.ContentType = "text/xml";

            XmlHelper.Serialize(httpResponse.Output, this, true, false);
        }
    }
}
