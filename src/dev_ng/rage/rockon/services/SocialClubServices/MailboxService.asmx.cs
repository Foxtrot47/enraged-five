using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.SocialClub;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.SocialClub
{
    /// <summary>
    /// Summary description for MailboxService
    /// </summary>
    [WebService(Namespace = "http://socialclub.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class MailboxService : System.Web.Services.WebService
    {
        [XmlRoot("Response")]
        public class BroadcastMessageResponse : Response
        {
            [XmlIgnore]
            public bool RecordIdSpecified = false;
            public Int64 RecordId = 0;

            public void SetSucceeded(Int64 recordId)
            {
                base.SetSucceeded();
                RecordIdSpecified = true;
                RecordId = recordId;
            }


            public override void SetFailed(Error err)
            {
                base.SetFailed(err);
                RecordIdSpecified = false;
            }
        }

        [WebMethod]
        public void BroadcastMessage(int titleId,
                                     string mailboxName,
                                     Int64 senderId,
                                     string senderDisplayName,
                                     string userDataStr,
                                     string msgDataStr,
                                     uint expirationMinutes)
        {
            BroadcastMessageResponse r = new BroadcastMessageResponse();
            Error err = null;

            try
            {
                Int64 recordId;
                byte[] userData = Encoding.UTF8.GetBytes(userDataStr);
                byte[] msgData = Encoding.UTF8.GetBytes(msgDataStr);

                if (null == (err = Mailbox.CreateRecord(titleId,
                                                        senderId,
                                                        mailboxName,
                                                        senderDisplayName,
                                                        Mailbox.BROADCAST_ID,
                                                        userData,
                                                        msgData,
                                                        expirationMinutes,
                                                        out recordId)))
                {
                    r.SetSucceeded(recordId);
                }
                else
                {
                    r.SetFailed(err);
                }
            }
            catch (Exception ex)
            {
                r.SetFailed(new Error(ex));
            }

            r.SerializeToHttp(Context.Response);
        }

        [XmlRoot("Response")]
        public class SendMessageResponse : Response
        {
            [XmlIgnore]
            public bool RecordIdSpecified = false;
            public Int64 RecordId = 0;

            public void SetSucceeded(Int64 recordId)
            {
                base.SetSucceeded();
                RecordIdSpecified = true;
                RecordId = recordId;
            }


            public override void SetFailed(Error err)
            {
                base.SetFailed(err);
                RecordIdSpecified = false;
            }
        }

        [WebMethod]
        public void SendMessage(int titleId,
                                string mailboxName,
                                Int64 senderId,
                                string senderDisplayName,
                                Int64 receiverId,
                                string userDataStr,
                                string msgDataStr,
                                uint expirationMinutes)
        {
            SendMessageResponse r = new SendMessageResponse();
            Error err = null;

            try
            {
                Int64 recordId;
                byte[] userData = Encoding.UTF8.GetBytes(userDataStr);
                byte[] msgData = Encoding.UTF8.GetBytes(msgDataStr);

                if (null == (err = Mailbox.CreateRecord(titleId,
                                                        senderId,
                                                        mailboxName,
                                                        senderDisplayName,
                                                        receiverId,
                                                        userData,
                                                        msgData,
                                                        expirationMinutes,
                                                        out recordId)))
                {
                    r.SetSucceeded(recordId);
                }
                else
                {
                    r.SetFailed(err);
                }
            }
            catch (Exception ex)
            {
                r.SetFailed(new Error(ex));
            }

            r.SerializeToHttp(Context.Response);
        }

        [XmlRoot("Response")]
        public class QueryNumRecordsResponse : Response
        {
            [XmlIgnore]
            public bool NumRecordsSpecified = false;
            public int NumRecords = 0;

            public void SetSucceeded(int num)
            {
                base.SetSucceeded();

                NumRecords = num;
                NumRecordsSpecified = true;
            }

            public override void SetFailed(Error err)
            {
                base.SetFailed(err);
                NumRecordsSpecified = false;
            }
        }

        [WebMethod]
        public void QueryNumRecords(int titleId,
                                    Int64 accountId,
                                    string mailboxName,
                                    Int64 startRecordId,
                                    Int64 endRecordId)
        {
            QueryNumRecordsResponse r = new QueryNumRecordsResponse();
            Error err = null;

            try
            {
                int numRecords = 0;

                if (null == (err = Mailbox.QueryNumRecords(titleId,
                                                           accountId,
                                                           mailboxName,
                                                           Mailbox.ANY_ID,                                                           
                                                           accountId,
                                                           startRecordId,
                                                           endRecordId,
                                                           false,
                                                           out numRecords)))
                {
                    r.SetSucceeded(numRecords);
                }
                else
                {
                    r.SetFailed(err);
                }
            }
            catch (Exception ex)
            {
                r.SetFailed(new Error(ex));
            }

            r.SerializeToHttp(Context.Response);
        }

        [XmlRoot("Response")]
        public class QueryRecordsResponse : Response
        {
            [XmlArray("Records")]
            [XmlArrayItem("Record")]
            public MailboxRecord[] m_Records;

            public void SetSucceeded(List<MailboxRecord> records)
            {
                base.SetSucceeded();
                m_Records = records.ToArray();
            }

            public override void SetFailed(Error err)
            {
                base.SetFailed(err);
                m_Records = null;
            }
        }

        [WebMethod]
        public void QueryRecords(int titleId,
                                 Int64 accountId,
                                 string mailboxName,
                                 Int64 startRecordId,
                                 Int64 endRecordId,
                                 uint maxRecords,
                                 bool readUserData,
                                 bool readMsgData,
                                 bool oldestFirst)
        {
            QueryRecordsResponse r = new QueryRecordsResponse();
            Error err = null;

            try
            {
                List<MailboxRecord> records;

                if (null == (err = Mailbox.QueryRecords(titleId,
                                                        accountId,
                                                        mailboxName,
                                                        Mailbox.ANY_ID,
                                                        accountId,
                                                        startRecordId,
                                                        endRecordId,
                                                        maxRecords,
                                                        readUserData,
                                                        readMsgData,
                                                        oldestFirst,
                                                        false,
                                                        out records)))
                {
                    r.SetSucceeded(records);
                }
                else
                {
                    r.SetFailed(err);
                }
            }
            catch (Exception ex)
            {
                r.SetFailed(new Error(ex));
            }

            r.SerializeToHttp(Context.Response);
        }

        [WebMethod]
        public void DeleteRecords(int titleId,
                                  Int64 accountId,
                                  string mailboxName,
                                  Int64 startRecordId,
                                  Int64 endRecordId)
        {
            Response r = new Response();
            Error err = null;

            try
            {
                uint numDeleted;

                if (null == (err = Mailbox.DeleteRecords(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         Mailbox.ANY_ID,
                                                         accountId,
                                                         startRecordId,
                                                         endRecordId,
                                                         false,
                                                         out numDeleted)))
                {
                    r.SetSucceeded();
                }
                else
                {
                    r.SetFailed(err);
                }
            }
            catch (Exception ex)
            {
                r.SetFailed(new Error(ex));
            }

            r.SerializeToHttp(Context.Response);
        }

        [XmlRoot("Response")]
        public class QueryMailboxInfoResponse : Response
        {
            [XmlArray("Records")]
            [XmlArrayItem("Record")]
            public MailboxInfo[] m_Infos;

            public void SetSucceeded(List<MailboxInfo> infos)
            {
                base.SetSucceeded();
                m_Infos = infos.ToArray();
            }


            public override void SetFailed(Error err)
            {
                base.SetFailed(err);
                m_Infos = null;
            }
        }

        [WebMethod]
        public void QueryMailboxInfo(int titleId,
                                     string mailboxName)
        {
            QueryMailboxInfoResponse r = new QueryMailboxInfoResponse();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using(SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    List<MailboxInfo> infos;

                    if (null == (err = MailboxMetadata.QueryMailboxInfo(con, mailboxName, out infos)))
                    {
                        r.SetSucceeded(infos);
                    }
                    else
                    {
                        r.SetFailed(err);
                    }
                }
            }
            catch (Exception ex)
            {
                r.SetFailed(new Error(ex));
            }

            r.SerializeToHttp(Context.Response);
        }
    }
}
