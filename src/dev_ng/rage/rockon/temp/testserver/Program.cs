﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace testserver
{
    class Program
    {
        static void Main(string[] args)
        {
            Trace.Listeners.Add(new ConsoleTraceListener());

            Context ctx = new Context();

            try
            {
                ctx.Debug1("Cmdline args:");
                for (int i = 0; i < args.Length; i++)
                {
                    ctx.Debug1("  [" + i + "] " + args[i]);
                }

                string addr = args[0];
                string port = args[1];
                uint listenBacklog = uint.Parse(args[2]);
                uint timeoutMs = uint.Parse(args[3]);
                uint statReportMs = uint.Parse(args[4]);

                Server s = new Server();                
                Error err = s.Init(new IPEndPoint(IPAddress.Parse(addr), int.Parse(port)), 
                                   listenBacklog,
                                   timeoutMs,
                                   statReportMs);

                if (null != err)
                {
                    ctx.Error(err);
                    return;
                }

                while (!s.IsDone)
                {
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
                throw;
            }
        }
    }
}
