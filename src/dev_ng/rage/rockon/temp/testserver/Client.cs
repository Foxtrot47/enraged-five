﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace testserver
{
    public class RawMsgReader : IMsgReader
    {
        public Error Read(byte[] src,
                          int offset,
                          int size,
                          out int numBytesRead,
                          out object msg)
        {
            msg = src;
            numBytesRead = size;
            return null;
        }
    };

    public class RawMsgWriter : IMsgWriter
    {
        public override void Begin(object msg)
        {
            byte[] bytes = (byte[])msg;
            Begin(bytes, 0, bytes.Length);
        }
    }

    public class Client
    {
        public uint Id { get; private set; }
        public bool IsDead { get; private set; }

        public IPEndPoint RemoteEndPoint { get; private set; }
        
        private object m_Lock = new object();
        private SocketHelper m_SktHelper;
        private Server.RemoveClientDelegate m_RemoveDlgt;

        public Client(uint id)
        {
            Id = id;
            IsDead = false;
        }

        public Error Init(Socket skt,
                          uint timeoutMs,
                          Server.RemoveClientDelegate removeDlgt)
        {
            lock (m_Lock)
            {
                Context ctx = new Context("id={0}", this.Id);

                RemoteEndPoint = skt.RemoteEndPoint as IPEndPoint;
                m_RemoveDlgt = removeDlgt;

                m_SktHelper = new SocketHelper();

                Error err;
                if (null != (err = m_SktHelper.Init(skt,
                                      new WaitCallback(ClientMsgReceived),
                                      new WaitCallback(ClientConnectionClosed),
                                      new RawMsgReader(),
                                      new RawMsgWriter(),
                                      timeoutMs)))
                {
                    return ctx.Error(err);
                }
               
                return null;
                //return ctx.Success3();
            }
        }

        public void Die()
        {
            Context ctx = new Context("id={0}", this.Id);

            lock (m_Lock)
            {
                if (IsDead)
                {
                    ctx.Warning("Already dead");
                    return;
                }

                IsDead = true;

                m_RemoveDlgt.Invoke(this);

                //ctx.Success2();
            }
        }

        private void ClientConnectionClosed(object reason)
        {
            //Context ctx = new Context("id={0}", this.Id);
            //ctx.Debug2("{0}", (SocketHelper.Reason)reason);
            Die();
        }

        private void ClientMsgReceived(object msg)
        {
            lock (m_Lock)
            {
                Context ctx = new Context("id={0}", this.Id);

                byte[] bytes = msg as byte[];

                if (null == bytes)
                {
                    ctx.Error(ErrorCode.InvalidData, "Null msg");
                    return;
                }

                //Echo it right back to the client
                m_SktHelper.Send(bytes);

                ctx.Success3("Received {0} bytes", bytes.Length);
            }
        }
    }
}
