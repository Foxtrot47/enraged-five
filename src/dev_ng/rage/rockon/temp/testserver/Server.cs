﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace testserver
{
    public class Server
    {
        public bool IsDone { get; private set; }
        public delegate void RemoveClientDelegate(Client c);

        private static object m_Lock = new object();

        private ListenSocketHelper m_Lsh;

        private uint m_NextClientId = 0;
        private List<Client> m_Clients = new List<Client>();

        private uint m_TimeoutMs = 30000;
        private Timer m_StatsTimer;
        private string m_LastStatsStr = "";

        public Server()
        {
            IsDone = false;
        }

        public Error Init(IPEndPoint listenEndPoint,
                          uint listenBacklog,
                          uint timeoutMs,
                          uint statReportMs)
        {
            Context ctx = new Context();

            try
            {
                m_Lsh = new ListenSocketHelper();

                Error err = m_Lsh.Init("Client", listenEndPoint, listenBacklog, AcceptClientCallback);
                
                if (null != err)
                {
                    return ctx.Error(err);
                }

                m_TimeoutMs = timeoutMs;

                m_StatsTimer = new Timer(new TimerCallback(this.OnStatsTimeout),
                                         this,
                                         statReportMs,
                                         statReportMs);

                return ctx.Success1("Listening for clients on " + listenEndPoint.ToString());
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        private void RemoveClient(Client c)
        {
            lock (m_Lock)
            {
                m_Clients.Remove(c);
            }
        }

        public bool AcceptClientCallback(Socket skt)
        {
            lock (m_Lock)
            {
                Context ctx = new Context();

                try
                {
                    Client c = new Client(++m_NextClientId);

                    Error err = c.Init(skt, m_TimeoutMs, RemoveClient);
                    if(null != err)
                    {
                        ctx.Error(err);
                        return false;
                    }

                    m_Clients.Add(c);

                    return true;
                }
                catch (Exception ex)
                {
                    ctx.Error(ex);
                    return false;
                }
            }
        }

        public void OnStatsTimeout(object state)
        {
            string s = m_Lsh.Stats + "  " + SocketHelperStats.ToString();

            if (s != m_LastStatsStr)
            {
                Trace.WriteLine(m_Lsh.Stats + "  " + SocketHelperStats.ToString());
                m_LastStatsStr = s;
            }
        }
    }
}
