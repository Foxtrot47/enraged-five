﻿using System;
using Dimebrain.TweetSharp.Fluent;
using Dimebrain.TweetSharp.Extensions;
using Twitterizer.Framework;

namespace twittertest
{
    class Program
    {
        static void TwitterizerUpdateStatus(string text)
        {
            Twitter t = new Twitter(MyGlobals.username, MyGlobals.password);

            t.Status.Update(text);
        }

        static void TweetSharpReadMine()
        {
            var request = FluentTwitter.CreateRequest()
                .AuthenticateAs(MyGlobals.username, MyGlobals.password)
                .Statuses().OnUserTimeline()
                .AsJson();

            var response = request.Request();

            if (request.HasError) throw new Exception("Request has error");

            if (response != null)
            {
                var statuses = response.AsStatuses();

                foreach (var status in statuses)
                {
                    Console.WriteLine(status.User.Name + ": " + status.Text);
                }
            }
        }

        static void TweetSharpUpdateStatus(string text)
        {
            var request = FluentTwitter.CreateRequest()
                .AuthenticateAs(MyGlobals.username, MyGlobals.password)
                .Statuses().Update(text)
                .AsXml();

            request.Request();
        }

        static void Main(string[] args)
        {
            //TweetSharpUpdateStatus("Hello, world3");
            //TweetSharpReadMine();

            Random r = new Random();
            TwitterizerUpdateStatus("Hello, world" + r.Next(10000));

            Console.ReadLine();
        }
    }
}
