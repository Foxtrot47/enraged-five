﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace StatSplitter
{
    class Program
    {
        const uint MAJOR_VERSION = 0;
        const uint MINOR_VERSION = 2;

        static void Usage()
        {
            Trace.Listeners.Add(new ConsoleTraceListener());
            Trace.WriteLine(string.Format("StatSplitter v{0}.{1}", MAJOR_VERSION, MINOR_VERSION));
            Trace.WriteLine("Usage: StatSplitter [in filename] [out filename]");
        }

        static void Main(string[] args)
        {
            Context ctx = new Context();

            try
            {
                if (args.Length > 2)
                {
                    Usage();
                    return;
                }

                StreamReader strmIn = null;
                if (args.Length >= 1)
                {
                    string filenameIn = args[0];
                    FileInfo fiIn = new FileInfo(filenameIn);
                    strmIn = fiIn.OpenText();
                    if (strmIn == null)
                    {
                        ctx.Error(ErrorCode.InvalidArguments, "Could not open input file " + filenameIn);
                        return;
                    }
                }
                //read from std input if no args

                FileStream strmOut = null;
                if (args.Length == 2)
                {
                    string filenameOut = args[1];

                    FileInfo fiOut = new FileInfo(filenameOut);
                    if (fiOut.Exists)
                    {
                        fiOut.Delete();
                    }
                    strmOut = fiOut.OpenWrite();
                }
                //write to std output if no file present


                string rson = strmIn != null ? strmIn.ReadLine() : Console.ReadLine();
                int numSubmissions = 0;
                int numMetrics = 0;

                while (rson != null && rson.Length > 0)
                {
                    RsonReader rr = new RsonReader();
                    if (!rr.Init(rson))
                    {
                        ctx.Error(ErrorCode.UnknownError, "Failed to init RsonReader");
                        return;
                    }

                    //Break it down into stime and sub components.
                    string stime;
                    string sub;

                    if (!(rr.GetFirstMember(ref rr) && rr.AsString(out stime))
                        || !(rr.GetNextSibling(ref rr) && rr.GetValue(out sub)))
                    {
                        ctx.Error(ErrorCode.UnknownError, "Failed to get stime, and sub");
                        return;
                    }

                    //Parse the submission into its header and submissions
                    RsonReader rrSub = new RsonReader();
                    if (!rrSub.Init(sub))
                    {
                        ctx.Error(ErrorCode.UnknownError, "Failed to init sub reader");
                        return;
                    }

                    if (!rrSub.GetFirstMember(ref rrSub))
                    {
                        ctx.Error(ErrorCode.UnknownError, "Failed to get sub header");
                        return;
                    }
                    string header = rrSub.ToString();
                    List<string> metrics = new List<string>();

                    while (rrSub.GetNextSibling(ref rrSub))
                    {
                        metrics.Add(rrSub.ToString());
                    }

                    //Display debug info
                    //ctx.Debug2("Submission {0}:", numSubmissions);
                    //ctx.Debug2("    ip:    " + ip);
                    //ctx.Debug2("    stime: " + stime);
                    //ctx.Debug2("    sub:   " + sub);
                    //ctx.Debug2("        header:   " + header);
                    //foreach (string s in metrics)
                    //{
                    //    ctx.Debug2("        metric:   " + s);
                    //}

                    //Write to file
                    foreach (string s in metrics)
                    {
                        if (strmOut != null)
                        {
                            byte[] bytes = Encoding.ASCII.GetBytes("stime=" + stime + "," + header + "," + s + "\n");
                            strmOut.Write(bytes, 0, bytes.Length);
                        }
                        else
                        {
                            Console.Write("stime=" + stime + "," + header + "," + s + "\n");
                        }
                    }

                    ++numSubmissions;
                    numMetrics += metrics.Count;

                    rson = strmIn != null ? strmIn.ReadLine() : Console.ReadLine();
                }

                if (strmOut != null)
				{
	                Trace.Listeners.Add(new ConsoleTraceListener());
                    Trace.WriteLine(string.Format("Processed {0} submissions and {1} metrics", numSubmissions, numMetrics));
				}
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
        }
    }
}
