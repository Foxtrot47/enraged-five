﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Rline;

namespace cstester
{
    class Program
    {
        static void Main(string[] args)
        {
            Trace.Listeners.Add(new ConsoleTraceListener());

            Log.WriteLine("Press key to start tests.");
            Console.ReadKey(true);

            ConsoleKeyInfo cki = new ConsoleKeyInfo();
            do
            {
                LanguageTests.TestContextStruct(100000000);
                //LanguageTests.TestTimestampBuilding(1000000);
                //LanguageTests.TestStringBuilding(10000, 10);

                //LanguageTests.RunAll();
                //ThreadingTests.RunAll();

                Log.WriteLine("Tests done. Press ESC to exit, or any key to run again.");
                cki = Console.ReadKey(true);
            } while (cki.Key != ConsoleKey.Escape);
        }
    }
}
