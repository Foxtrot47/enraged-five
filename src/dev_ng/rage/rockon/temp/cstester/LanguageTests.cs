﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace cstester
{
    class LanguageTests
    {
        enum TestEnum
        {
            TestValue
        };

        public struct ContextStruct
        {
            private Channel m_Channel;
            private string m_Desc;
            private string m_Prefix;
        }

        //PURPOSE
        //  See if instancing struct Contexts is faster than class ones.
        public static void TestContextStruct(uint numIterations)
        {
            Context ctx = new Context("{0}", numIterations);

            ctx.Debug2("Starting tests...");

            System.DateTime dt;
            double deltaMs;

            dt = DateTime.Now;
            for (int i = 0; i < numIterations; i++)
            {
                Context temp = new Context();
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Instancing Context class took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            for (int i = 0; i < numIterations; i++)
            {
                ContextStruct tempStruct = new ContextStruct();
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Instancing Context struct took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            ctx.Debug2("Tests done.");
        }

        //PURPOSE
        //  Test if try-catch blocks that don't generate exceptions can still
        //  affect performance.
        public static void TestNonfiringTryCatch(uint numIterations)
        {
            Context ctx = new Context("{0}", numIterations);
            
            ctx.Debug2("Starting tests...");

            List<int> list = new List<int>();

            System.DateTime dt;
            double deltaMs;
            int j = 0;

            dt = DateTime.Now;
            for (int i = 0; i < numIterations; i++)
            {
                j++;
            }

            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Without try-catch took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            for (int i = 0; i < numIterations; i++)
            {
                try
                {
                    j++;
                }
                catch
                {
                }
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("With try-catch took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            ctx.Debug2("Tests done.");
        }

        //PURPOSE
        //  Test the speed of list iteration, to see if it's an issue.
        public static void TestListIteration(uint numIterations)
        {
            Context ctx = new Context("{0}", numIterations);
            
            ctx.Debug2("Starting tests...");

            List<int> list = new List<int>();

            for (int i = 0; i < numIterations; i++)
            {
                list.Add(i);
            }

            System.DateTime dt;
            double deltaMs;

            dt = DateTime.Now;
            for (int i = 0; i < numIterations; i++)
            {
                int temp = list[i];
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Indexed took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            foreach (int i in list)
            {
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Foreach took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            ctx.Debug2("Tests done.");           
        }

        //PURPOSE
        //  Test the speed of ToString() on various types of objects.
        public static void TestToString(uint numIterations)
        {
            Context ctx = new Context("{0}", numIterations);
            
            ctx.Debug2("Starting tests...");

            string s = "This is a test";
            int testInt = 1215343;
            TestEnum e = TestEnum.TestValue;

            System.DateTime dt;
            double deltaMs;

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                s.ToString();
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("string.ToString() took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                testInt.ToString();
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("int.ToString() took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                e.ToString();
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("enum.ToString() took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            ctx.Debug2("Tests done.");
        }

        //PURPOSE
        //  Test performance of returning Errors vs exceptions handling.
        public static void TestErrorHandling(uint numIterations)
        {
            Context ctx = new Context("{0}", numIterations);
            
            ctx.Debug2("Starting tests...");

            System.DateTime dt;
            double deltaMs;

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                Error err = new Error(ErrorCode.InvalidArguments);
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Errors took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                try
                {
                    throw new Exception();
                }
                catch
                {
                }
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Exceptions took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                try
                {
                    try
                    {
                        try
                        {
                            throw new Exception();
                        }
                        catch (Exception ex)
                        {
                            throw (ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
                }
                catch
                {
                }
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Nested exceptions (depth=3) took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            ctx.Debug2("Tests done.");
        }

        //PURPOSE
        //  Just a test to verify how exceptions are passed up.
        public static void TestExceptionPercolation()
        {
            Context ctx = new Context();
            
            int numRecursions = 4;

            try
            {
                DoSomething(numRecursions);
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
        }

        public static void DoSomething(int numRecursions)
        {
            if (numRecursions == 0)
            {
                throw new Exception("BLECH!");
            }
            else
            {
                DoSomething(--numRecursions);
            }
        }

        //PURPOSE
        //  See how long it takes to create a Context.
        public static void TestContextCreation(int numIterations)
        {
            Context ctx = new Context("{0}", numIterations);
            
            ctx.Debug2("Starting tests...");

            System.DateTime dt;
            double deltaMs;

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                Context tempCtx = new Context();
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Simple creation (no text) took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                Context tempCtx = new Context("This is a test");
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Simple creation (with text) took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            Context tempCtx1 = new Context("This is a test");
            {
                Context tempCtx2 = new Context("This is a test");
                {
                    dt = DateTime.Now;
                    for (uint i = 0; i < numIterations; i++)
                    {
                        Context tempCtx3 = new Context("This is a test");
                    };
                };
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("Nested contexts took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            ctx.Debug2("Tests done.");
        }

        //PURPOSE
        //  See how long a lot of Trace.Write() calls takes.
        public static void TestTrace(int numIterations)
        {
            Context ctx = new Context("{0}", numIterations);

            ctx.Debug2("Starting tests...");

            System.DateTime dt;
            double deltaMs;

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                Trace.Write(" ");
            }

            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("{0} Trace.Write() took {1}ms to complete)", numIterations, deltaMs);

            ctx.Debug2("Tests done.");
        }

        //PURPOSE
        //  Test the speed of various ways of constructing a string.
        public static void TestStringBuilding(int numStrings, int stringLength)
        {
            Context ctx = new Context("{0}", numStrings);

            ctx.Debug2("Starting tests...");

            string s = "";
            for (uint j = 0; j < stringLength; j++)
            {
                s += "A";
            }

            List<string> strings = new List<string>();
            for (uint i = 0; i < numStrings; i++)
            {
                strings.Add(s);
            }

            System.DateTime dt;
            double deltaMs;

            dt = DateTime.Now;
            s = null;
            foreach(string cur in strings)
            {
                s = string.Format("{0} {1}", s, cur);
            }

            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("String.Format (num={0}, len={1}) took {2}ms ({3}ms each)", numStrings, stringLength, deltaMs, deltaMs / numStrings);

            dt = DateTime.Now;           
            s = null;
            foreach (string cur in strings)
            {
                s += cur;
            }

            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("String += (num={0}, len={1}) took {2}ms ({3}ms each)", numStrings, stringLength, deltaMs, deltaMs / numStrings);

            dt = DateTime.Now;
            s = null;
            StringBuilder sb = new StringBuilder();
            foreach (string cur in strings)
            {
                sb.Append(cur);
            }
            s = sb.ToString();

            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("StringBuilder (num={0}, len={1}) took {2}ms ({3}ms each)", numStrings, stringLength, deltaMs, deltaMs / numStrings);

            ctx.Debug2("Tests done.");
        }

        public static void TestTimestampBuilding(int numIterations)
        {
            Context ctx = new Context("{0}", numIterations);

            ctx.Debug2("Starting tests...");

            System.DateTime dt;
            double deltaMs;

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                StringBuilder sb = new StringBuilder();

                DateTime _dt = DateTime.UtcNow;

                sb.Append(_dt.Month.ToString("D2"));
                sb.Append("/");
                sb.Append(_dt.Day.ToString("D2"));
                sb.Append("/");
                sb.Append((_dt.Year % 1000).ToString("D2"));
                sb.Append(" ");
                sb.Append(_dt.Hour.ToString("D2"));
                sb.Append(":");
                sb.Append(_dt.Minute.ToString("D2"));
                sb.Append(":");
                sb.Append(_dt.Second.ToString("D2"));
                sb.Append(".");
                sb.Append(_dt.Millisecond.ToString("D3"));
                sb.Append(" ");
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("StringBuilder took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);

            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                StringBuilder sb = new StringBuilder();

                DateTime _dt = DateTime.UtcNow;

                sb.Append(string.Format("{0}/{1}/{2} {3}:{4}:{5}.{6} ",
                                      _dt.Month.ToString("D2"),
                                      _dt.Day.ToString("D2"),
                                      (_dt.Year % 1000).ToString("D2"),
                                      _dt.Hour.ToString("D2"),
                                      _dt.Minute.ToString("D2"),
                                      _dt.Second.ToString("D2"),
                                      _dt.Millisecond.ToString("D3")));
            }
            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("String.Format took {0}ms ({1}ms each)", deltaMs, deltaMs / numIterations);
        }

        public static void RunAll()
        {
            TestTimestampBuilding(10000);
            TestStringBuilding(1000, 100);
            TestTrace(1000);
            TestContextCreation(100000);
            TestExceptionPercolation();
            TestErrorHandling(1000000);
            TestToString(10000000);
            TestListIteration(10000000);
            TestNonfiringTryCatch(10000000);
        }
    }
}
