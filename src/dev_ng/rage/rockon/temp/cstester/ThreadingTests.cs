﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace cstester
{
    class ThreadingTests
    {
        static readonly object m_Lock = new object();
        static int m_NumThreadsCompleted = 0;

        static void ThreadMain()
        {
            //Hog CPU for 10ms, to simulate a fairly intensive function run synchronously
            //in a background thread.
            DateTime dt = DateTime.Now;
            while (DateTime.Now.Subtract(dt).TotalMilliseconds < 10)
            {
            }

            lock (m_Lock)
            {
                ++m_NumThreadsCompleted;
            }
        }

        static void TestThreadThrashing(int numIterations)
        {
            Context ctx = new Context("{0}", numIterations);

            ctx.Debug2("Starting tests...");

            System.DateTime dt;
            double deltaMs;

            m_NumThreadsCompleted = 0;
            dt = DateTime.Now;
            for (uint i = 0; i < numIterations; i++)
            {
                Thread t = new Thread(ThreadMain);
                t.Start();
            }

            while (m_NumThreadsCompleted < numIterations)
            {
            }

            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("{0} threads blocking for 10ms each took {1}ms to complete)", numIterations, deltaMs);

            ctx.Debug2("Tests done.");
        }

        static void CallWebMethod()
        {
            DateTime dt = DateTime.Now;

            //string uri = "http://dev.services.sg.rockon.rockstargames.com/RlineServices/MailboxService.asmx";
            //string ns = "http://rline.services.rockon.rockstar.com";
            //string m = "NoOp";

            //SoapEnvelopeBuilder eb = new SoapEnvelopeBuilder(SoapVersion.Soap1_2, m, ns);
            //SoapEnvelopeEx env = eb.GenerateEnvelopeEx();

            //string responseContent;
            //HttpWebRequestHelper.ExecuteSoapRequest(uri,
            //                                        env,
            //                                        out responseContent);

            Trace.WriteLine(string.Format("CallWebMethod took {0}ms", DateTime.Now.Subtract(dt).TotalMilliseconds));

            //lock (m_Lock)
            //{
            //    ++m_NumThreadsCompleted;
            //}
        }

        static void TestMailboxReadRecords(int numIterations)
        {
            Context ctx = new Context("{0}", numIterations);

            ctx.Debug2("Starting tests...");

            System.DateTime dt;
            double deltaMs;

            //Tests todo:
            //  - Synchronous
            //  - Sync per thread
            //  - Completely async (Begin-End methods)
            //  - Completely manual (make TCP connection, compose the HTML and send it, read response, disconnect)

            /*
                        string uri  = "http://dev.services.sg.rockon.rockstargames.com/RlineServices/MailboxService.asmx";
                        string ns   = "http://rline.services.rockon.rockstar.com";
                        string m    = "NoOp";
                        //string m = "ReadRecords";

                        Dictionary<string, string> args = new Dictionary<string, string>();
                        //args["titleId"]         = "10201";
                        //args["accountId"]       = "0";
                        //args["mailboxName"]     = "TestMailbox";
                        //args["recordId"]        = "0";
                        //args["maxRecords"]      = "0";
                        //args["readUserData"]    = "true";
                        //args["readMsgData"]     = "true";
                        //args["oldestFirst"]     = "true";

                        SoapEnvelopeBuilder eb = new SoapEnvelopeBuilder(SoapVersion.Soap1_2, m, ns);
                        foreach(KeyValuePair<string,string> kvp in args)
                        {
                            eb.AddParam(kvp.Key, kvp.Value);
                        }

                        SoapEnvelopeEx env = eb.GenerateEnvelopeEx();

                        m_NumThreadsCompleted = 0;
                        dt = DateTime.Now;
                        for (uint i = 0; i < numIterations; i++)
                        {
                            string responseContent;
                            HttpWebRequestHelper.ExecuteSoapRequest(uri,
                                                                    env,
                                                                    out responseContent);

                            //uint requestId;
                            //HttpWebRequestHelper.ExecuteSoapRequest(uri,
                            //                                        env,
                            //                                        delegate { lock (m_Lock) { ++m_NumThreadsCompleted; } },
                            //                                        delegate { lock (m_Lock) { ++m_NumThreadsCompleted; } },
                            //                                        10000,
                            //                                        null,
                            //                                        out requestId);
                        }
            */
            dt = DateTime.Now;
            bool async = false;

            if (async)
            {
                m_NumThreadsCompleted = 0;
                for (int i = 0; i < numIterations; i++)
                {
                    Thread t = new Thread(CallWebMethod);
                    t.Start();
                }

                while (m_NumThreadsCompleted < numIterations)
                {
                }
            }
            else
            {
                for (int i = 0; i < numIterations; i++)
                {
                    CallWebMethod();
                }
            }

            deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
            ctx.Debug2("{0} requests took {1}ms to complete)", numIterations, deltaMs);

            ctx.Debug2("Tests done.");
        }

        public static void RunAll()
        {
            TestThreadThrashing(1000);
        }
    }
}
