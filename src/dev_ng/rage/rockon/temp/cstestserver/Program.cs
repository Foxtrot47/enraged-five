﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace cstestserver
{
    //NOTE: Couldn't templatize because C# generics fucking blow.
    public class Stat
    {
        public Stat()
        {
            Min = float.MaxValue;
            Max = float.MinValue;
        }

        public float Min { get; private set; }
        public float Max { get; private set; }
        public float Total { get; private set; }
        public uint NumSamples { get; private set; }
        public float Avg
        {
            get
            {
                if (NumSamples == 0) return 0;
                return Total / NumSamples;
            }
        }

        public void AddSample(float val)
        {
            Total += val;
            ++NumSamples;

            if (val < Min) Min = val;
            if (val > Max) Max = val;
        }
    }
    
    public struct AcceptStats
    {
        public int NumCallbacks;
        public int NumAccepts;
        public int NumNullSockets;
        public int NumSocketExceptions;
        public int NumOtherExceptions;
    }

    public struct ReceiveStats
    {
        public int NumCallbacks;
        public int NumReceives;
        public int NumNullSockets;
        public int NumSocketExceptions;
        public int NumOtherExceptions;
        public int NumNormalCloses;
    }

    class Program
    {
        static object m_Lock = new object();

        static AcceptStats acceptStats = new AcceptStats();
        static ReceiveStats receiveStats = new ReceiveStats();

        static AsyncCallback acceptCb = new AsyncCallback(AcceptCallback);
        static AsyncCallback receiveCb = new AsyncCallback(ReceiveCallback);

        static int RCVBUFSIZE = 128;
        static byte[] rcvBuf = new byte[128];
        
        static void Run(string addr, int port, bool async)
        {
            Socket skt = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(addr), port);
            skt.Bind(ep);
            skt.Listen(200);

            Trace.WriteLine("Listening on " + ep + "(" + (async ? "async" : "sync") + ")");

            if (async)
            {
                try
                {
                    skt.BeginAccept(acceptCb, skt);
                }
                catch (SocketException ex)
                {
                    ++acceptStats.NumSocketExceptions;
                    Trace.WriteLine(string.Format("BeginAccept SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                }
                catch (Exception ex)
                {
                    ++acceptStats.NumOtherExceptions;
                    Trace.WriteLine(ex);
                }

                Thread.Sleep(Timeout.Infinite);
            }
            else
            {
                while (true)
                {
                    try
                    {
                        Socket s = skt.Accept();

                        if (s != null && s.Connected)
                        {
                            ++acceptStats.NumAccepts;
                        }
                        else
                        {
                            Trace.WriteLine("Null or unconnected socket");
                            ++acceptStats.NumNullSockets;
                        }
                    }
                    catch (SocketException ex)
                    {
                        ++acceptStats.NumSocketExceptions;
                        Trace.WriteLine(string.Format("Accept SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                    }
                    catch (Exception ex)
                    {
                        ++acceptStats.NumOtherExceptions;
                        Trace.WriteLine(ex);
                    }

                    PrintStats();
                }
            }
        }

        static void PrintStats()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Acc[");
            if (acceptStats.NumCallbacks > 0) sb.Append("cb=" + acceptStats.NumCallbacks + " ");
            if (acceptStats.NumAccepts > 0) sb.Append("a=" + acceptStats.NumAccepts + " ");
            if (acceptStats.NumNullSockets > 0) sb.Append("ns=" + acceptStats.NumNullSockets + " ");
            if (acceptStats.NumOtherExceptions > 0) sb.Append("oe=" + acceptStats.NumOtherExceptions + " ");
            if (acceptStats.NumSocketExceptions > 0) sb.Append("se=" + acceptStats.NumSocketExceptions + " ");
            sb = new StringBuilder(sb.ToString().TrimEnd(' '));
            sb.Append("]  ");

            sb.Append("Rec[");
            if (receiveStats.NumCallbacks > 0) sb.Append("cb=" + receiveStats.NumCallbacks + " ");
            if (receiveStats.NumReceives > 0) sb.Append("r=" + receiveStats.NumReceives + " ");
            if (receiveStats.NumNormalCloses > 0) sb.Append("cl=" + receiveStats.NumNormalCloses + " ");
            if (receiveStats.NumNullSockets > 0) sb.Append("ns=" + receiveStats.NumNullSockets + " ");
            if (receiveStats.NumOtherExceptions > 0) sb.Append("oe=" + receiveStats.NumOtherExceptions + " ");
            if (receiveStats.NumSocketExceptions > 0) sb.Append("se=" + receiveStats.NumSocketExceptions + " ");
            sb = new StringBuilder(sb.ToString().TrimEnd(' '));
            sb.Append("]");

            Trace.WriteLine(sb.ToString());
        }

        static void AcceptCallback(IAsyncResult ar)
        {
            lock (m_Lock)
            {
                ++acceptStats.NumCallbacks;

                try
                {
                    Socket skt = ar.AsyncState as Socket;

                    //Get the accepted socket
                    try
                    {
                        Socket s = skt.EndAccept(ar);

                        if (s != null && s.Connected)
                        {
                            ++acceptStats.NumAccepts;

                            bool beginReceiveSucceeded = false;

                            try
                            {
                                s.BeginReceive(new byte[RCVBUFSIZE], 0, RCVBUFSIZE, 0, receiveCb, s);
                                beginReceiveSucceeded = true;
                            }
                            catch (SocketException ex)
                            {
                                Trace.WriteLine(string.Format("BeginReceive SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                            }
                            catch (Exception ex)
                            {
                                Trace.WriteLine(ex);
                            }

                            if (!beginReceiveSucceeded)
                            {
                                s.Close();
                            }
                        }
                        else
                        {
                            ++acceptStats.NumNullSockets;
                            Trace.WriteLine("Null or unconnected socket");
                        }
                    }
                    catch (SocketException ex)
                    {
                        ++acceptStats.NumSocketExceptions;
                        Trace.WriteLine(string.Format("EndAccept SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                    }
                    catch (Exception ex)
                    {
                        ++acceptStats.NumOtherExceptions;
                        Trace.WriteLine(ex);
                    }

                    //Start next accept
                    try
                    {
                        skt.BeginAccept(acceptCb, skt);
                    }
                    catch (SocketException ex)
                    {
                        ++acceptStats.NumSocketExceptions;
                        Trace.WriteLine(string.Format("BeginAccept SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                    }
                    catch (Exception ex)
                    {
                        ++acceptStats.NumOtherExceptions;
                        Trace.WriteLine(ex);
                    }
                }
                catch (Exception ex)
                {
                    ++acceptStats.NumOtherExceptions;
                    Trace.WriteLine(ex);
                }

                PrintStats();
            }
        }

        static void ReceiveCallback(IAsyncResult ar)
        {
            lock (m_Lock)
            {
                ++receiveStats.NumCallbacks;

                try
                {
                    Socket skt = ar.AsyncState as Socket;

                    if (skt == null)
                    {
                        Trace.WriteLine("Null socket");
                        return;
                    }

                    ++receiveStats.NumReceives;

                    int numBytes = 0;

                    try
                    {
                        numBytes = skt.EndReceive(ar);
                    }
                    catch (SocketException ex)
                    {
                        if (ex.SocketErrorCode == SocketError.ConnectionReset)
                        {
                            ++receiveStats.NumNormalCloses;
                        }
                        else
                        {
                            ++receiveStats.NumSocketExceptions;
                            Trace.WriteLine(string.Format("EndReceive SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                        }
                    }
                    catch (Exception ex)
                    {
                        ++receiveStats.NumOtherExceptions;
                        Trace.WriteLine(ex);
                    }

                    if (numBytes > 0)
                    {
                        //Start the next recieve
                        bool beginReceiveSucceeded = false;

                        try
                        {
                            skt.BeginReceive(new byte[RCVBUFSIZE],
                                             0,
                                             RCVBUFSIZE,
                                             0,
                                             receiveCb,
                                             skt);

                            beginReceiveSucceeded = true;
                        }
                        catch (SocketException ex)
                        {
                            ++receiveStats.NumSocketExceptions;
                            Trace.WriteLine(string.Format("BeginReceive SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                        }
                        catch (Exception ex)
                        {
                            ++receiveStats.NumSocketExceptions;
                            Trace.WriteLine(ex);
                        }

                        if (!beginReceiveSucceeded)
                        {
                            if (skt.Connected)
                            {
                                skt.Close();
                            }
                        }
                    }
                    else if (numBytes == 0)
                    {
                        //Normal disconnect.  Can release socket if not already closed.
                        if (skt.Connected)
                        {
                            ++receiveStats.NumNormalCloses;
                            skt.Close();
                        }
                    }
                    else
                    {
                        //Error case.  Should have been handled by exception handling?
                        throw new Exception("Negative numBytes: " + numBytes);
                    }
                }
                catch (Exception ex)
                {
                    ++receiveStats.NumOtherExceptions;
                    Trace.WriteLine(ex);
                }

                PrintStats();
            }
        }

        static void Main(string[] args)
        {
            Trace.Listeners.Add(new ConsoleTraceListener());

            Run(args[0],                        //Address
                int.Parse(args[1]),             //Port
                args[2].ToLower() == "async");  //Sync/async?
        }
    }
}
