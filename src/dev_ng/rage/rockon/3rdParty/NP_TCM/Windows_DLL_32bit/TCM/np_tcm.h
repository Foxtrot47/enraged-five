//*****************************************************************************
// tab=4, code:US-ASCII
//
/** @file	np_tcm.h
*	@brief	Header file for NP TCM
*
*	@par
*		This is a header file for NP TCM.
*		This file includes TCM specific types, structures, and functions.
*
*
*	@attention		SCE CONFIDENTIAL
*	@note
*	Copyright (C) 2007	Sony Computer Entertainment Inc.
*											All Rights Reserved.
*/
//*****************************************************************************
#ifndef _SCE_NET_NP_TCM_H
#define _SCE_NET_NP_TCM_H

// for Visual C++
#ifdef _MSC_VER
#ifdef TCM_EXPORTS
#define TCM_API __declspec(dllexport)
#else
#define TCM_API __declspec(dllimport)
#endif
#else
// clear windows-related macros.
#define TCM_API
#define WINAPI
#endif

/****************************************************************************/
/*		Includes.															*/
/****************************************************************************/
#ifndef _MSC_VER
#include <stdint.h>
#endif
#include "np_common.h"


#ifdef	__cplusplus
extern "C" {
#endif

/****************************************************************************/
/*		Constants.															*/
/****************************************************************************/

/// return value of status "ok"
#define		SCE_NET_NP_TCM_RET_OK					(0)

/// return value of status "ok"
#define		SCE_NET_NP_TCM_RET_OK_PARTIAL			(1)

/// return value of status "unknown error"
#define		SCE_NET_NP_TCM_RET_UNKNOWN				(-1)

/// return value of status "initialization failed"
#define		SCE_NET_NP_TCM_RET_BROKENTICKET			(-2)

/// return value of status "not initialized"
#define		SCE_NET_NP_TCM_RET_INVALIDCIPHER		(-3)

/// return value of status "specified date is invalid"
#define		SCE_NET_NP_TCM_RET_BADSIGNATURE			(-4)

/// return value of status "buffer is too small"
#define		SCE_NET_NP_TCM_RET_BUFTOOSMALL			(-5)

/// return value of status "pass phrase is invalid"
#define		SCE_NET_NP_TCM_RET_BADPASSPHRASE		(-6)

/// return value of status "suitable cipher information not found"
#define		SCE_NET_NP_TCM_RET_NOSUITABLECIPHER		(-7)

/// max length of online id
#define		SCE_NET_NP_ONLINEID_MAX_LENGTH			(16)

/// max length of np id resource
#define		SCE_NET_NP_TCM_NPID_RESOURCE_MAX_LENGTH	(4)


/****************************************************************************/
/*		Types.																*/
/****************************************************************************/

/// type of online id
typedef struct SceNpOnlineId {
	char data[SCE_NET_NP_ONLINEID_MAX_LENGTH];
	char term;
	char dummy[3];
} SceNpOnlineId;
typedef struct SceNpOnlineId SceNpPsHandle;

/// type of np id
typedef struct SceNpId {
	SceNpOnlineId handle;
	uint8_t opt[8];
	uint8_t reserved[8];
} SceNpId;

/// type of np id resource
typedef struct SceNetNpTcmNpIdResource {
	char data[SCE_NET_NP_TCM_NPID_RESOURCE_MAX_LENGTH];
} SceNetNpTcmNpIdResource;

/// NpId resource: PS3
#define		SCE_NET_NP_TCM_NPID_RESOURCE_PS3	\
					((SceNetNpTcmNpIdResource *) "ps3")

/// NpId resource: PSP
#define		SCE_NET_NP_TCM_NPID_RESOURCE_PSP	\
					((SceNetNpTcmNpIdResource *) "psp")


/****************************************************************************/
/*		Structures.															*/
/****************************************************************************/


/****************************************************************************/
/*		Functions.															*/
/****************************************************************************/

//*****************************************************************************
/**
*	@brief	prepare Cipher Information
*
*	@par
*		This function is used for preparation of Cipher Information.
*
*	@param	pass_phrase					(IN)	Static pass phrase
*	@param	cipher_information			(IN)	Original Cipher Information
*	@param	prepared_cipher_information (OUT)	Prepared Cipher Information
*
*	@retval SCE_NET_NP_TCM_RET_OK				The process is success.
*	@retval SCE_NET_NP_TCM_RET_UNKNOWN			Unknown error.
*	@retval SCE_NET_NP_TCM_RET_INVALIDCIPHER	The cipher information is
*												invalid.
*	@retval SCE_NET_NP_TCM_RET_BADPASSPHRASE	The pass phrase is invalid.
*
*	@note
*/
//*****************************************************************************
extern TCM_API int32_t WINAPI SceNetNpTcmPrepareCipherInformation(
		const unsigned char					*pass_phrase,
		const SceNetNpCipherInformation		*cipher_information,
		SceNetNpPreparedCipherInformation	*prepared_cipher_information
);
#define sceNetNpTcmPrepareCipherInformation SceNetNpTcmPrepareCipherInformation

//*****************************************************************************
/**
*	@brief	Get information from NP Ticket without verifying the signature
*
*	@par
*		This function is used for getting information from NP Ticket
*		without verifying the signature.
*
*	@param	ticket_data				(IN)	NP Ticket data
*	@param	ticket_size				(IN)	NP Ticket size
*	@param	major_version			(OUT)	Ticket's major version
*	@param	minor_version			(OUT)	Ticket's minor version
*	@param	serial_id				(OUT)	Serial id of the ticket
*	@param	issued_date				(OUT)	Issued date of the ticket
*	@param	not_on_or_after_date	(OUT)	Expiration date of the ticket
*	@param	service_id				(OUT)	Service id of the ticket
*	@param	entitlement_count		(OUT)	Entitlement count of the ticket
*	@param	role_count				(OUT)	Role count of the ticket
*
*	@retval SCE_NET_NP_TCM_RET_OK				The process is success.
*	@retval SCE_NET_NP_TCM_RET_OK_PARTIAL		The process is success, but
*												this ticket	has partial data.
*	@retval SCE_NET_NP_TCM_RET_UNKNOWN			Unknown error.
*	@retval SCE_NET_NP_TCM_RET_BROKENTICKET		The ticket is broken.
*
*	@note
*/
//*****************************************************************************
extern TCM_API int32_t WINAPI SceNetNpTcmGetInformationWithoutVerify(
	const unsigned char		*ticket_data,
	size_t					ticket_size,
	SceNetNpMajorVersion	*major_version,
	SceNetNpMinorVersion	*minor_version,
	SceNetNpTicketSerialId	*serial_id,
	SceNetNpTime			*issued_date,
	SceNetNpTime			*not_on_or_after_date,
	SceNetNpServiceId		*service_id,
	uint32_t				*entitlement_count,
	uint32_t				*role_count
);
#define sceNetNpTcmGetInformationWithoutVerify SceNetNpTcmGetInformationWithoutVerify

//*****************************************************************************
/**
*	@brief	Verify the signature of NP Ticket
*
*	@par
*		This function is used for verifying the signature of NP Ticket.
*
*	@param	prepared_cipher_information			(IN)	Prepared Cipher
*														Information
*	@param	prepared_cipher_information_count	(IN)	Number of Prepared
*														Cipher Information
*	@param	ticket_data							(IN)	NP Ticket data
*	@param	ticket_size							(IN)	NP Ticket size
*
*	@retval SCE_NET_NP_TCM_RET_OK				The signature is ok.
*	@retval SCE_NET_NP_TCM_RET_UNKNOWN			Unknown error.
*	@retval SCE_NET_NP_TCM_RET_BROKENTICKET	The ticket is broken.
*	@retval SCE_NET_NP_TCM_RET_INVALIDCIPHER	Cipher Information is invalid.
*	@retval SCE_NET_NP_TCM_RET_BADSIGNATURE	The signature is wrong.
*	@retval SCE_NET_NP_TCM_RET_NOSUITABLECIPHER	Suitable Cipher Information is
*												not found.
*
*	@note
*/
//*****************************************************************************
extern TCM_API int32_t WINAPI SceNetNpTcmVerifySignature(
		const SceNetNpPreparedCipherInformation	*prepared_cipher_information,
		uint32_t							prepared_cipher_information_count,
		const unsigned char					*ticket_data,
		size_t								ticket_size
);
#define sceNetNpTcmVerifySignature SceNetNpTcmVerifySignature

//*****************************************************************************
/**
*	@brief	Get information from NP Ticket with verifying the signature
*
*	@par
*		This function is used for getting all information from NP Ticket
*		with verifying the signature.
*
*	@param	prepared_cipher_information			(IN)	Prepared Cipher
*														Information
*	@param	prepared_cipher_information_count	(IN)	Number of Prepared
*														Cipher Information
*	@param	ticket_data							(IN)	NP Ticket data
*	@param	ticket_size							(IN)	NP Ticket size
*	@param	major_version						(OUT)	Ticket's major version
*	@param	minor_version						(OUT)	Ticket's minor version
*	@param	issuer_id							(OUT)	Issuer id of the ticket
*	@param	serial_id							(OUT)	Serial id of the ticket
*	@param	issued_date							(OUT)	Issued date of the
*														ticket
*	@param	not_on_or_after_date				(OUT)	Expiration date of the
*														ticket
*	@param	service_id							(OUT)	Service id of the
*														ticket
*	@param	subject								(OUT)	Subject of the ticket
*	@param	entitlements						(OUT)	Entitlements of the
*														ticket
*	@param	entitlement_count					(OUT)	Entitlement count of
*														the ticket
*	@param	roles								(OUT)	Roles of the ticket
*	@param	role_count							(OUT)	Role count of the ticket
*	@param	cookie								(OUT)	Cookie data of the
*														ticket
*
*	@retval SCE_NET_NP_TCM_RET_OK				The process is success.
*	@retval SCE_NET_NP_TCM_RET_OK_PARTIAL		The process is success, but
*												this ticket has partial data.
*	@retval SCE_NET_NP_TCM_RET_UNKNOWN			Unknown error.
*	@retval SCE_NET_NP_TCM_RET_BROKENTICKET		The ticket is broken.
*	@retval SCE_NET_NP_TCM_RET_INVALIDCIPHER	Cipher Information is invalid.
*	@retval SCE_NET_NP_TCM_RET_BADSIGNATURE		The signature is wrong.
*	@retval SCE_NET_NP_TCM_RET_BUFTOOSMALL		Entitlement buffer is too small.
*	@retval SCE_NET_NP_TCM_RET_NOSUITABLECIPHER	Suitable Cipher Information is
*												not found.
*
*	@note
*/
//*****************************************************************************
extern TCM_API int32_t WINAPI SceNetNpTcmGetInformation(
	const SceNetNpPreparedCipherInformation	*prepared_cipher_information,
	uint32_t								prepared_cipher_information_count,
	const unsigned char						*ticket_data,
	size_t									ticket_size,
	SceNetNpMajorVersion					*major_version,
	SceNetNpMinorVersion					*minor_version,
	SceNetNpIssuerId						*issuer_id,
	SceNetNpTicketSerialId					*serial_id,
	SceNetNpTime							*issued_date,
	SceNetNpTime							*not_on_or_after_date,
	SceNetNpServiceId						*service_id,
	SceNetNpSubject							*subject,
	SceNetNpEntitlement						*entitlements,
	uint32_t								*entitlement_count,
	SceNetNpRole							*roles,
	uint32_t								*role_count,
	SceNetNpCookie							*cookie
);
#define sceNetNpTcmGetInformation SceNetNpTcmGetInformation

//*****************************************************************************
/**
*	@brief	Get information from NP Ticket with verifying the signature (ver.2)
*
*	@par
*		This function is used for getting all information from NP Ticket
*		with verifying the signature.
*
*	@param	prepared_cipher_information			(IN)	Prepared Cipher
*														Information
*	@param	prepared_cipher_information_count	(IN)	Number of Prepared
*														Cipher Information
*	@param	ticket_data							(IN)	NP Ticket data
*	@param	ticket_size							(IN)	NP Ticket size
*	@param	major_version						(OUT)	Ticket's major version
*	@param	minor_version						(OUT)	Ticket's minor version
*	@param	issuer_id							(OUT)	Issuer id of the ticket
*	@param	serial_id							(OUT)	Serial id of the ticket
*	@param	issued_date							(OUT)	Issued date of the
*														ticket
*	@param	not_on_or_after_date				(OUT)	Expiration date of the
*														ticket
*	@param	service_id							(OUT)	Service id of the
*														ticket
*	@param	subject								(OUT)	Subject of the ticket
*	@param	entitlements						(OUT)	Entitlements of the
*														ticket
*	@param	entitlement_count					(OUT)	Entitlement count of
*														the ticket
*	@param	roles								(OUT)	Roles of the ticket
*	@param	role_count							(OUT)	Role count of the ticket
*	@param	cookie								(OUT)	Cookie data of the
*														ticket
*
*	@retval SCE_NET_NP_TCM_RET_OK				The process is success.
*	@retval SCE_NET_NP_TCM_RET_OK_PARTIAL		The process is success, but
*												this ticket has partial data.
*	@retval SCE_NET_NP_TCM_RET_UNKNOWN			Unknown error.
*	@retval SCE_NET_NP_TCM_RET_BROKENTICKET		The ticket is broken.
*	@retval SCE_NET_NP_TCM_RET_INVALIDCIPHER	Cipher Information is invalid.
*	@retval SCE_NET_NP_TCM_RET_BADSIGNATURE		The signature is wrong.
*	@retval SCE_NET_NP_TCM_RET_BUFTOOSMALL		Entitlement buffer is too small.
*	@retval SCE_NET_NP_TCM_RET_NOSUITABLECIPHER	Suitable Cipher Information is
*												not found.
*
*	@note
*/
//*****************************************************************************
extern TCM_API int32_t WINAPI SceNetNpTcmGetInformation2(
	const SceNetNpPreparedCipherInformation	*prepared_cipher_information,
	uint32_t								prepared_cipher_information_count,
	const unsigned char						*ticket_data,
	size_t									ticket_size,
	SceNetNpMajorVersion					*major_version,
	SceNetNpMinorVersion					*minor_version,
	SceNetNpIssuerId						*issuer_id,
	SceNetNpTicketSerialId					*serial_id,
	SceNetNpTime							*issued_date,
	SceNetNpTime							*not_on_or_after_date,
	SceNetNpServiceId						*service_id,
	SceNetNpSubjectWithDoB					*subject,
	SceNetNpEntitlement						*entitlements,
	uint32_t								*entitlement_count,
	SceNetNpRole							*roles,
	uint32_t								*role_count,
	SceNetNpCookie							*cookie
);
#define sceNetNpTcmGetInformation2 SceNetNpTcmGetInformation2

//*****************************************************************************
/**
*	@brief	Get information from NP Ticket with verifying the signature
*
*	@par
*		This function is used for getting all information from NP Ticket
*		including Entitlement Annotation with verifying the signature.
*
*	@param	prepared_cipher_information			(IN)	Prepared Cipher
*														Information
*	@param	prepared_cipher_information_count	(IN)	Number of Prepared
*														Cipher Information
*	@param	ticket_data							(IN)	NP Ticket data
*	@param	ticket_size							(IN)	NP Ticket size
*	@param	major_version						(OUT)	Ticket's major version
*	@param	minor_version						(OUT)	Ticket's minor version
*	@param	issuer_id							(OUT)	Issuer id of the ticket
*	@param	serial_id							(OUT)	Serial id of the ticket
*	@param	issued_date							(OUT)	Issued date of the
*														ticket
*	@param	not_on_or_after_date				(OUT)	Expiration date of the
*														ticket
*	@param	service_id							(OUT)	Service id of the
*														ticket
*	@param	subject								(OUT)	Subject of the ticket
*	@param	entitlements						(OUT)	Entitlements of the
*														ticket
*	@param	entitlement_count					(OUT)	Entitlement count of
*														the ticket
*	@param	roles								(OUT)	Roles of the ticket
*	@param	role_count							(OUT)	Role count of the ticket
*	@param	cookie								(OUT)	Cookie data of the
*														ticket
*
*	@retval SCE_NET_NP_TCM_RET_OK				The process is success.
*	@retval SCE_NET_NP_TCM_RET_OK_PARTIAL		The process is success, but
*												this ticket has partial data.
*	@retval SCE_NET_NP_TCM_RET_UNKNOWN			Unknown error.
*	@retval SCE_NET_NP_TCM_RET_BROKENTICKET		The ticket is broken.
*	@retval SCE_NET_NP_TCM_RET_INVALIDCIPHER	Cipher Information is invalid.
*	@retval SCE_NET_NP_TCM_RET_BADSIGNATURE		The signature is wrong.
*	@retval SCE_NET_NP_TCM_RET_BUFTOOSMALL		Entitlement buffer is too small.
*	@retval SCE_NET_NP_TCM_RET_NOSUITABLECIPHER	Suitable Cipher Information is
*												not found.
*
*	@note
*/
//*****************************************************************************
extern TCM_API int32_t WINAPI SceNetNpTcmGetInformationIncludingEntitlementAnnotation(
	const SceNetNpPreparedCipherInformation	*prepared_cipher_information,
	uint32_t								prepared_cipher_information_count,
	const unsigned char						*ticket_data,
	size_t									ticket_size,
	SceNetNpMajorVersion					*major_version,
	SceNetNpMinorVersion					*minor_version,
	SceNetNpIssuerId						*issuer_id,
	SceNetNpTicketSerialId					*serial_id,
	SceNetNpTime							*issued_date,
	SceNetNpTime							*not_on_or_after_date,
	SceNetNpServiceId						*service_id,
	SceNetNpSubject							*subject,
	SceNetNpEntitlementWithAnnotation		*entitlements,
	uint32_t								*entitlement_count,
	SceNetNpRole							*roles,
	uint32_t								*role_count,
	SceNetNpCookie							*cookie
);
#define sceNetNpTcmGetInformationIncludingEntitlementAnnotation\
		SceNetNpTcmGetInformationIncludingEntitlementAnnotation

//*****************************************************************************
/**
*	@brief	Get NP ID from NP Ticket
*
*	@par
*		This function is used for getting NP ID from NP Ticket
*
*	@param	ticket_data				(IN)	NP Ticket data
*	@param	ticket_size				(IN)	NP Ticket size
*	@param	resource				(IN)	NP ID's resource
*	@param	npid					(OUT)	NP ID
*
*	@retval SCE_NET_NP_TCM_RET_OK				The process is success.
*	@retval SCE_NET_NP_TCM_RET_UNKNOWN			Unknown error.
*	@retval SCE_NET_NP_TCM_RET_BROKENTICKET		The ticket is broken.
*
*	@note
*/
//*****************************************************************************
extern TCM_API int32_t WINAPI SceNetNpTcmGetNpId(
	const unsigned char				*ticket_data,
	size_t							ticket_size,
	const SceNetNpTcmNpIdResource	*resource,
	SceNpId							*npid
);
#define sceNetNpTcmGetNpId SceNetNpTcmGetNpId

#ifdef	__cplusplus
}
#endif
#endif	/* _SCE_NET_NP_TCM_H */
//*****************************************************************************
//	Copyright (C) 2007	Sony Computer Entertainment Inc.
//											All Rights Reserved.
//*****************************************************************************
