//*****************************************************************************
// tab=4, code:US-ASCII
//
/** @file	np_common.h
*	@brief	Common header file for for NP project
*
*	@par
*		This is a common header file for NP project.
*		This file includes common types, common structures, and so on.
*
*	@attention		SCE CONFIDENTIAL
*	@note
*	Copyright (C) 2007	Sony Computer Entertainment Inc.
*											All Rights Reserved.
*/
//*****************************************************************************
#ifndef _SCE_NET_NP_COMMON_H
#define _SCE_NET_NP_COMMON_H


/****************************************************************************/
/*		Includes.															*/
/****************************************************************************/
#ifndef _MSC_VER
#include <stdint.h>
#else
typedef __int8			int8_t;
typedef __int16			int16_t;
typedef __int32			int32_t;
typedef __int64			int64_t;
typedef unsigned __int8		uint8_t;
typedef unsigned __int16	uint16_t;
typedef unsigned __int32	uint32_t;
typedef unsigned __int64	uint64_t;
#endif

#ifdef  __cplusplus
extern "C" {
#endif

/****************************************************************************/
/*		Constants.															*/
/****************************************************************************/

/// size of Cipher Information
#define		SCE_NET_NP_CIPHER_INFORMATION_SIZE				(1024)

/// size of Prepared Cipher Information
#define		SCE_NET_NP_PREPARED_CIPHER_INFORMATION_SIZE		(1024)

/// size of NP Ticket Serial ID
#define		SCE_NET_NP_TICKET_SERIAL_ID_SIZE				(20)

/// size of Subject Online ID
#define		SCE_NET_NP_SUBJECT_ONLINE_ID_SIZE				(32)

/// size of Subject Region
#define		SCE_NET_NP_SUBJECT_REGION_SIZE					(4)

/// size of Subject Domain
#define		SCE_NET_NP_SUBJECT_DOMAIN_SIZE					(4)

/// size of Service ID
#define		SCE_NET_NP_SERVICE_ID_SIZE						(24)

/// size of Entitlement ID
#define		SCE_NET_NP_ENTITLEMENT_ID_SIZE					(32)

/// maximum size of Entitlement Annotation
#define		SCE_NET_NP_ENTITLEMENT_ANNOTATION_MAX_SIZE		(512)

/// maximum size of Cookie Information
#define		SCE_NET_NP_COOKIE_MAX_SIZE						(1024)

/// maximum size of NP Ticket
#define		SCE_NET_NP_TICKET_MAX_SIZE						(65536)

/// size of Role Domain
#define		SCE_NET_NP_ROLE_DOMAIN_SIZE						(24)


/****************************************************************************/
/*		Macros.																*/
/****************************************************************************/

/// macro of getting the subject's age
#define		SCE_NP_SUBJECT_STATUS_GET_AGE(u32)				(((u32)>>24)&0x7f)

/// macro of checking whether the subject is suspended
#define		SCE_NP_SUBJECT_STATUS_IS_SUSPENDED(u32)			((u32)&0x80)

/// macro of checking whether chat function is restricted
#define		SCE_NP_SUBJECT_STATUS_IS_CHAT_DISABLED(u32)		((u32)&0x100)

/// macro of checking whether content rating is enabled
#define		SCE_NP_SUBJECT_STATUS_CONTENT_RATING(u32)		((u32)&0x200) 


/****************************************************************************/
/*		Types.																*/
/****************************************************************************/

/// type of Time in millisecond
typedef		int64_t			SceNetNpTime;

/// type of NP Issuer ID
typedef		uint32_t		SceNetNpIssuerId;

/// type of Major version
typedef		uint8_t			SceNetNpMajorVersion;

/// type of Minor version
typedef		uint8_t			SceNetNpMinorVersion;

/// type of NP Subject Account ID
typedef		uint64_t		SceNetNpSubjectAccountId;

/// type of NP Subject Status
typedef		uint32_t		SceNetNpSubjectStatus;


/****************************************************************************/
/*		Structures.															*/
/****************************************************************************/
/****************************************************************************/
/**
*	@brief Cipher Information Structure
*
*	@par
*		This is the structure for Cipher Information.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpCipherInformation {

	/// actual Cipher Information data
	unsigned char		data[SCE_NET_NP_CIPHER_INFORMATION_SIZE];

} SceNetNpCipherInformation;

/****************************************************************************/
/**
*	@brief Prepared Cipher Information Structure
*
*	@par
*		This is the structure for Prepared Cipher Information.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpPreparedCipherInformation {

	/// actual Prepared Cipher Information data
	unsigned char		data[SCE_NET_NP_PREPARED_CIPHER_INFORMATION_SIZE];

} SceNetNpPreparedCipherInformation;

/****************************************************************************/
/**
*	@brief NP Ticket Serial ID Structure
*
*	@par
*		This is the structure for NP Ticket Serial ID.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpTicketSerialId {

	/// actual NP Ticket serial id data
	unsigned char		data[SCE_NET_NP_TICKET_SERIAL_ID_SIZE];

} SceNetNpTicketSerialId;

/****************************************************************************/
/**
*	@brief Subject Online ID Structure
*
*	@par
*		This is the structure for Subject Online ID.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpSubjectOnlineId {

	/// actual Subject Online ID data
	unsigned char		data[SCE_NET_NP_SUBJECT_ONLINE_ID_SIZE];

} SceNetNpSubjectOnlineId;

/****************************************************************************/
/**
*	@brief Subject Region Structure
*
*	@par
*		This is the structure for Subject Region.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpSubjectRegion {

	/// actual Subject Region data
	unsigned char		data[SCE_NET_NP_SUBJECT_REGION_SIZE];

} SceNetNpSubjectRegion;

/****************************************************************************/
/**
*	@brief Subject Domain Structure
*
*	@par
*		This is the structure for Subject Domain.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpSubjectDomain {

	/// actual Subject Domain data
	unsigned char		data[SCE_NET_NP_SUBJECT_DOMAIN_SIZE];

} SceNetNpSubjectDomain;

/****************************************************************************/
/**
*	@brief Structure for NP Date
*
*	@par
*		This is the structure for NP Date.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNpDate {

	/// year
	uint16_t year;
	/// month
	uint8_t month;
	/// day
	uint8_t day;

} SceNpDate;

/****************************************************************************/
/**
*	@brief Service ID Structure
*
*	@par
*		This is the structure for Service ID.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpServiceId {

	/// actual Service ID data
	unsigned char		data[SCE_NET_NP_SERVICE_ID_SIZE];

} SceNetNpServiceId;

/****************************************************************************/
/**
*	@brief Entitlement ID Structure
*
*	@par
*		This is the structure for Entitlement ID.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpEntitlementId {

	/// actual Entitlement ID data
	unsigned char		data[SCE_NET_NP_ENTITLEMENT_ID_SIZE];

} SceNetNpEntitlementId;

/****************************************************************************/
/**
*	@brief Entitlement Annotation Structure
*
*	@par
*		This is the structure for Entitlement Annotation.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpEntitlementAnnotation {

	/// actual Entitlement Annotation data
	unsigned char		data[SCE_NET_NP_ENTITLEMENT_ANNOTATION_MAX_SIZE];

	/// Entitlement Annotation data size
	size_t				size;

} SceNetNpEntitlementAnnotation;

/****************************************************************************/
/**
*	@brief Role Domain Structure
*
*	@par
*		This is the structure for Role Domain.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpRoleDomain {

	/// actual Role Domain data
	unsigned char		data[SCE_NET_NP_ROLE_DOMAIN_SIZE];

} SceNetNpRoleDomain;

/****************************************************************************/
/**
*	@brief Cookie Data Structure
*
*	@par
*		This is the structure for Cookie Data.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpCookie {

	/// actual Cookie data
	unsigned char		data[SCE_NET_NP_COOKIE_MAX_SIZE];

	/// Cookie data size
	size_t				size;

} SceNetNpCookie;

/****************************************************************************/
/**
*	@brief Structure for NP Subject
*
*	@par
*		This is the structure for NP Subject.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpSubject {

	/// Subject Account ID
	SceNetNpSubjectAccountId	id;
	/// Subject Online ID
	SceNetNpSubjectOnlineId		handle;
	/// Subject Region
	SceNetNpSubjectRegion		region;
	/// Subject Domain
	SceNetNpSubjectDomain		domain;
	/// Subject Status
	SceNetNpSubjectStatus		status;
	/// Status Duration
	SceNetNpTime				duration;

} SceNetNpSubject;

/****************************************************************************/
/**
*	@brief Structure for NP Subject with DoB
*
*	@par
*		This is the structure for NP Subject which has DoB.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpSubjectWithDoB {

	/// Subject Account ID
	SceNetNpSubjectAccountId	id;
	/// Subject Online ID
	SceNetNpSubjectOnlineId		handle;
	/// Subject Region
	SceNetNpSubjectRegion		region;
	/// Subject Domain
	SceNetNpSubjectDomain		domain;
	/// Subject Date of Birth
	SceNpDate					dob;
	/// Subject Status
	SceNetNpSubjectStatus		status;
	/// Status Duration
	SceNetNpTime				duration;

} SceNetNpSubjectWithDoB;

/****************************************************************************/
/**
*	@brief Structure for NP Entitlement
*
*	@par
*		This is the structure for NP Entitlement.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpEntitlement {

	/// Entitlement ID
	SceNetNpEntitlementId		id;
	/// Created date of this entitlement
	SceNetNpTime				created_date;
	/// Expired date of this entitlement
	SceNetNpTime				expired_date;
	/// Entitlement type
	uint32_t					type;
	/// Remaining count of this entitlement
	int32_t						remaining_count;
	/// Consumed count of this entitlement
	uint32_t					consumed_count;

} SceNetNpEntitlement;

/****************************************************************************/
/**
*	@brief Structure for NP Entitlement with Annotation
*
*	@par
*		This is the structure for NP Entitlement with Annotation.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpEntitlementWithAnnotation {

	/// Entitlement ID
	SceNetNpEntitlementId			id;
	/// Created date of this entitlement
	SceNetNpTime					created_date;
	/// Expired date of this entitlement
	SceNetNpTime					expired_date;
	/// Entitlement type
	uint32_t						type;
	/// Remaining count of this entitlement
	int32_t							remaining_count;
	/// Consumed count of this entitlement
	uint32_t						consumed_count;
	/// Annotation for this entitlement
	SceNetNpEntitlementAnnotation	annotation;

} SceNetNpEntitlementWithAnnotation;

/****************************************************************************/
/**
*	@brief Structure for NP Role
*
*	@par
*		This is the structure for NP Role.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpRole {

	/// Role ID
	int32_t						id;
	/// Role Domain
	SceNetNpRoleDomain			domain;

} SceNetNpRole;

/****************************************************************************/
/**
*	@brief Structure for whole NP Ticket
*
*	@par
*		This is the structure for whole NP Ticket. This is not used in
*		TGM/TCM API.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpTicket {

	/// Ticket Serial ID
	SceNetNpTicketSerialId		*id;
	/// Issuer ID
	SceNetNpIssuerId			issuer;
	/// Issued date
	SceNetNpTime				issued_date;
	/// Not on or after date
	SceNetNpTime				not_on_or_after_date;
	/// Subject
	SceNetNpSubject				*subject;
	/// Entitlements
	SceNetNpEntitlement			*entitlements;
	/// Entitlement count
	uint32_t					entitlement_count;
	/// Roles
	SceNetNpRole				*roles;
	/// Role count
	uint32_t					role_count;
	/// Cookie
	SceNetNpCookie				*cookie;

} SceNetNpTicket;

/****************************************************************************/
/**
*	@brief Structure for whole NP Ticket
*
*	@par
*		This is the structure for whole NP Ticket. This is not used in
*		TGM/TCM API.
*
*	@note
*/
/****************************************************************************/
typedef struct SceNetNpTicket2 {

	/// Ticket Serial ID
	SceNetNpTicketSerialId		*id;
	/// Issuer ID
	SceNetNpIssuerId			issuer;
	/// Issued date
	SceNetNpTime				issued_date;
	/// Not on or after date
	SceNetNpTime				not_on_or_after_date;
	/// Subject
	SceNetNpSubjectWithDoB		*subject;
	/// Entitlements
	SceNetNpEntitlement			*entitlements;
	/// Entitlement count
	uint32_t					entitlement_count;
	/// Roles
	SceNetNpRole				*roles;
	/// Role count
	uint32_t					role_count;
	/// Cookie
	SceNetNpCookie				*cookie;

} SceNetNpTicket2;

#ifdef  __cplusplus
}
#endif
#endif	/* _SCE_NET_NP_COMMON_H */
//*****************************************************************************
//	Copyright (C) 2007	Sony Computer Entertainment Inc.
//											All Rights Reserved.
//*****************************************************************************
