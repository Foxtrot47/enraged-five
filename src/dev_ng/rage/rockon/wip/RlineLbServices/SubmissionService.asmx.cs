using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;        

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Rline
{
    /// <summary>
    /// Summary description for ArbitrationService
    /// </summary>
    [WebService(Namespace = "http://rline.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class SubmissionService : System.Web.Services.WebService
    {
        public class QuerySubmissionResult
        {
            public List<SubmissionInfo> m_SubmissionInfos;

            public QuerySubmissionResult() { }
        }

        public class QueryMatchesReadyForArbitrationResult
        {
            public List<MatchInfo> m_MatchInfos;

            public QueryMatchesReadyForArbitrationResult() { }
        }


        //PURPOSE
        //  Performs arbitration on all matches ready to be arbitrated for the title.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "ArbitrateMatchesResult")]
        public Result ArbitrateMatches(int titleId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        if (null == (err = Arbitration.ArbitrateMatches(con)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Commits to leaderboards all submissions for specified title.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CommitSubmissionsResult")]
        public Result CommitSubmissions(int titleId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        if (null == (err = CommitStats.CommitTitleSubmissions(con, titleId)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Quick tester for making submissions.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateRawIntSubmissionResult")]
        public Result CreateRawIntSubmission(int titleId,
                                                     uint lbId,
                                                     int matchId,
                                                     bool isArbitrated,
                                                     Int64 submitterAccountId,
                                                     Int64 targetAccountId,
                                                     string targetDisplayName,
                                                     uint propId,
                                                     int value)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        List<XblProperty> props = new List<XblProperty>();
                        props.Add(new XblProperty(propId, value));

                        if (null == (err = Submission.CreateRawSubmission(con, isArbitrated, matchId, submitterAccountId, targetAccountId, targetDisplayName, lbId, props)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns matches ready to for arbitration.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryMatchesReadyforArbitrationResult")]
        public TResult<QueryMatchesReadyForArbitrationResult> QueryMatchesReadyForArbitration(int titleId)
        {
            TResult<QueryMatchesReadyForArbitrationResult> result = new TResult<QueryMatchesReadyForArbitrationResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        QueryMatchesReadyForArbitrationResult r = new QueryMatchesReadyForArbitrationResult();

                        if (null == (err = Match.GetMatchesReadyForArbitration(con, 0, out r.m_MatchInfos)))
                        {
                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Commits to leaderboards all submissions for specified title.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QuerySubmissionsResult")]
        public TResult<QuerySubmissionResult> QuerySubmissions(int titleId,
                                                              bool arbitrated,
                                                              uint maxResults,
                                                              int matchId)
        {
            TResult<QuerySubmissionResult> result = new TResult<QuerySubmissionResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        QuerySubmissionResult r = new QuerySubmissionResult();

                        if (null == (err = Submission.QuerySubmission(con, arbitrated, matchId, false, maxResults, out r.m_SubmissionInfos)))
                        {
                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
