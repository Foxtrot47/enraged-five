<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Rockstar.Rockon.login" Title="Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UnprotectedContent" runat="server">
    <div id="login" style="text-align: center;">
        <table style="margin-right: auto; margin-left: auto; text-align: left;">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Domain"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="DomainTextBox" runat="server" TabIndex="100">rockstar</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Username"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="UsernameTextBox" runat="server" TabIndex="101"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="PasswordTextBox" runat="server" TabIndex="102" 
                        TextMode="Password"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <asp:Button ID="LogInButton" runat="server" OnClick="LogInButton_Click" Text="Log In" TabIndex="102" /><br />
        <br />
        <asp:Label ID="InvalidLoginLabel" runat="server" ForeColor="Red"></asp:Label></div>
        <!--<%= Environment.UserName %>-->
        <!--<%=User.Identity.Name %>-->
</asp:Content>
