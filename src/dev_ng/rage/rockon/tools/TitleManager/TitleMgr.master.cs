﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Rockstar.Rockon {
    public partial class TitleMgr:System.Web.UI.MasterPage {
        protected void Page_Load(object sender,EventArgs e) {

        }

        public void SetBodyClass(string cls)
        {
            Master.SetBodyClass(cls);
        }

        protected void LogOutButton_Click(object sender,EventArgs e)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
        }
    }
}
