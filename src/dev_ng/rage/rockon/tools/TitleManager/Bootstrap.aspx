<%@ Page Language="C#" MasterPageFile="~/TitleMgr.Master" AutoEventWireup="true" CodeBehind="Bootstrap.aspx.cs" Inherits="Rockstar.Rockon.Bootstrap" Title="Bootstrap"%>
<%@ MasterType VirtualPath="~/TitleMgr.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label2" runat="server" Text="Enter the title id in hex format with no leading 0x.  E.g. 545473A2, not 0x545473A2."></asp:Label><br />
    <br />
    <asp:Label ID="Label1" runat="server" Text="Title Id"></asp:Label><br />
    <asp:Button ID="SubmitButton" runat="server" Text="Submit" OnClick="SubmitButton_Click" />
    <asp:TextBox ID="TitleIdTextBox" runat="server"></asp:TextBox><br />
    <br />
    <asp:Label ID="Result" runat="server"></asp:Label>
</asp:Content>
