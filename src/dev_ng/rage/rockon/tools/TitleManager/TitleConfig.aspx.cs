using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using Rockstar.Rockon;
using Rockstar.Rockon.Rline;
using System.Drawing;

namespace Rockstar.Rockon
{
    public partial class SubmitForm:System.Web.UI.Page
    {
        protected void Page_Load(object sender,EventArgs e)
        {
            Master.SetBodyClass("titleconfig");
        }

        protected void SubmitButton_Click(object sender,EventArgs e)
        {
            if(FileUpload1.FileContent.Length > 0)
            {
                try
                {
                    XlastInfo xlastInfo;

                    if (!Xlast.ParseStream(FileUpload1.FileContent, out xlastInfo))
                    {
                        throw (new Exception("Failed to parse XLAST file"));
                    }

                    if (!RlineDb.CreateTablesFromXlast(xlastInfo))
                    {
                        throw (new Exception("Failed to create rline tables"));
                    }

                    //uint titleId = 
                    //TableGen.GenerateTables(FileUpload1.FileContent, "localhost", "rockstar", null);
                    //TitleTableGen.GenerateTables(FileUpload1.FileContent); //, @"rsgsansql1\rockon", "svcrockon", "$Fun4you");

                    Result.ForeColor = Color.Black;
                    Result.Text = string.Format("Submitted title config for {0:X}", xlastInfo.m_TitleId);
                }
                catch(Exception ex)
                {
                    Result.ForeColor = Color.Red;
                    Result.Text = ex.Message;
                }
            }
            else
            {
                Result.ForeColor = Color.Red;
                Result.Text = "Invalid file path";
            }
        }
    }
}
