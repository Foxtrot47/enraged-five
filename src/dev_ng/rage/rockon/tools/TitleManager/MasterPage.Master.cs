using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Rockstar.Rockon {
    public partial class MasterPage:System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender,EventArgs e)
        {
            bool authorized = true;

            if("Bootstrap" == this.Page.Title)
            {
                authorized = this.IsAuthorized("administrator");
            }
            else if("Xlast Submission" == this.Page.Title)
            {
                authorized = this.IsAuthorized("titleadmin,administrator");
            }

            ProtectedContent.Visible = authorized;
            YouAreNotWorthyLabel.Visible = !authorized;
        }

        public void SetBodyClass(string cls)
        {
            BodyTag.Attributes.Add("class", cls);
        }

        public bool IsAuthorized(string rolesStr)
        {
            bool isAuthorized = false;
            string[] roles = rolesStr.Split(new char[]{','});
            string myRoles = Session["roles"] as string;

            if(null != myRoles)
            {
                foreach(string role in roles)
                {
                    if(myRoles.Contains(role))
                    {
                        isAuthorized = true;
                    }
                }
            }

            return isAuthorized;
        }
    }
}
