using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Drawing;

using Rockstar.Rockon.Core;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon {
    public partial class Bootstrap:System.Web.UI.Page
    {
        protected void Page_Load(object sender,EventArgs e)
        {
            Master.SetBodyClass("titlebootstrap");
        }

        protected void SubmitButton_Click(object sender,EventArgs e)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            uint titleId;
            if(UInt32.TryParse(TitleIdTextBox.Text, NumberStyles.HexNumber, nfi, out titleId))
            {
                try
                {
                    TitleDb.CreateTitleDatabase((int)titleId);

                    Result.ForeColor = Color.Black;
                    Result.Text = String.Format("Created title database {0:X}", titleId);
                }
                catch(Exception ex)
                {
                    Result.ForeColor = Color.Red;
                    Result.Text = ex.Message;
                }
            }
            else
            {
                Result.ForeColor = Color.Red;
                Result.Text = "Invalid title id";
            }
        }
    }
}
