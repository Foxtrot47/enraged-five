<%@ Page Language="C#" MasterPageFile="~/TitleMgr.Master" AutoEventWireup="true" CodeBehind="TitleConfig.aspx.cs" Inherits="Rockstar.Rockon.SubmitForm" Title="Xlast Submission" %>
<%@ MasterType VirtualPath="~/TitleMgr.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Submit Xlast file"></asp:Label><br />
    <asp:Button ID="SubmitButton" runat="server" OnClick="SubmitButton_Click" Text="Submit" />
    <asp:FileUpload ID="FileUpload1" runat="server" /><br />
    <br />
    <asp:Label ID="Result" runat="server"></asp:Label>
</asp:Content>
