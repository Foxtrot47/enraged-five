using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.DirectoryServices;

using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon {
    public partial class login:System.Web.UI.Page
    {
        bool m_UseActiveDirectory = false;

        protected void Page_Load(object sender,EventArgs e)
        {
            FormsAuthentication.SignOut();
            UsernameTextBox.Focus();
        }

        protected void LogInButton_Click(object sender,EventArgs e)
        {
            string domain = DomainTextBox.Text;
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Text;
            string domainUser = domain + @"\" + username;

            ConnectionStringSettingsCollection cxnStrings =
                System.Web.Configuration.WebConfigurationManager.ConnectionStrings;
            string ldapCxnString = "";

            foreach(ConnectionStringSettings cs in cxnStrings)
            {
                if("LDAPConnectionString" == cs.Name)
                {
                    ldapCxnString = cs.ConnectionString;
                    break;
                }
            }

            bool success = false;
            string diag = "Invalid login";

            if(m_UseActiveDirectory)
            {
                try
                {
                    DirectoryEntry entry =
                        new DirectoryEntry(ldapCxnString, domainUser, password);

                    // Bind to the native AdsObject to force authentication.
                    Object obj = entry.NativeObject;
                    DirectorySearcher search = new DirectorySearcher(entry);
                    search.Filter = "(SAMAccountName=" + username + ")";
                    SearchResult result = search.FindOne();
                    if(null != result) 
                    {
                        string email = result.Properties["mail"][0] as string;
                        if(null != email)
                        {
                            Session.Add("email", email);
                        }

                        success = true;
                    }
                }
                catch(Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex.ToString());
                    diag = ex.Message;
                }
            }
            else
            {
                //FIXME (KB) - we need a better solution here.
                success = true;
            }

            if(success)
            {
                string cmd = "SELECT Roles FROM dbo.Users WHERE UserName='" + username + "' AND Domain='" + domain + "';";
                string connStr = SqlHelper.MakeConnectionString(@"rsgsansql1\rockon", "svcrockon", "$Fun4you");
                connStr += "database=Title_Global;";
                string roles = SqlHelper.ExecuteScalar(connStr, cmd) as string;
                if(null != roles)
                {
                    Session.Add("roles", roles);
                }
                else
                {
                    Session.Remove("roles");
                    success = false;
                }
            }

            if(success)
            {
                FormsAuthentication.RedirectFromLoginPage(username, false);
            }
            else
            {
                InvalidLoginLabel.Text = diag;
            }
        }
    }
}
