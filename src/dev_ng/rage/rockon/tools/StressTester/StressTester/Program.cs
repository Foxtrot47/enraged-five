using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace Rockstar.Rockon.StressTester
{
    static class Program 
    {
        static void Usage()
        {
            Trace.WriteLine("Usage: stresstester -class {-asm} {-numclients} {-numiter} {-beginargs {args}}");
            Trace.WriteLine("");
            Trace.WriteLine("Where:");
            Trace.WriteLine("   -class          Name of class containing test method (case insensitive)");
            Trace.WriteLine("   -method         Name of method to run (case insensitive)");
            Trace.WriteLine("   -asm            Additional assembly to load (can be specified multiple times");
            Trace.WriteLine("   -numclients     Number of virtual clients to run (default is 1)");
            Trace.WriteLine("   -numiter        Number of iterations to run each client (default is 1)");
            Trace.WriteLine("   -maxrecent      Max number of most recent stats to average separate from whole run (default is 10)");
            Trace.WriteLine("   -beginargs      All arguments after this are passed to the clients running on this machine");
        }

        static void Main(string[] args)
        {
            List<VirtualClient> clients = new List<VirtualClient>();


            //TEMP place for these global settings now.  They shouldn't be buried in the
            //     request helper.
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.DefaultConnectionLimit = 100;

            try
            {
                string className = null;
                string methodName = "TestMain";
                int numClients = 1;
                int numIterations = 1;
                int maxRecent = 10;
                List<string> clientArgs = new List<string>();

                //For convenience, we always load in the TestSuite assembly, whether specified or not.
                //FIXME: Find out why this only works if we reference TestSuite in StressTester project.
                //       Currently, it's necessary to both do this load, AND reference TestSuite.
                Assembly a = AppDomain.CurrentDomain.Load("TestSuite");

                //Process cmdline args
                for (int i = 0; i < args.Length; i++)
                {
                    string s = args[i];

                    if (s == "-class")
                    {
                        className = args[++i];
                    }
                    else if (s == "-method")
                    {
                        methodName = args[++i];
                    }
                    else if (s == "-asm")
                    {
                        string assemblyName = args[++i];

                        if (null == AppDomain.CurrentDomain.Load(assemblyName))
                        {
                            throw new NullReferenceException("Could not load assembly " + assemblyName);
                        }
                    }
                    else if (s == "-numclients")
                    {
                        numClients = int.Parse(args[++i]);
                    }
                    else if (s == "-numiter")
                    {
                        numIterations = int.Parse(args[++i]);
                    }
                    else if (s == "-maxrecent")
                    {
                        maxRecent = int.Parse(args[++i]);
                    }
                    else if (s == "-beginargs")
                    {
                        while(++i < args.Length)
                        {
                            clientArgs.Add(args[i]);
                        }
                        break;
                    }
                    else
                    {
                        Usage();
                        return;
                    }
                }

                if (className == null 
                    || methodName == null
                    || numClients < 1 
                    || numIterations < 1
                    || maxRecent < 1)
                {
                    Usage();
                    return;
                }

                //Create the clients (this is done separate from running, in case
                //creation time could throw off timings.
                for (int i = 0; i < numClients; i++)
                {
                    clients.Add(new VirtualClient(className, methodName, clientArgs, numIterations, maxRecent));
                }

                //Start each client.
                foreach (VirtualClient vc in clients)
                {
                    vc.Start();                        
                }

                //Wait until all clients are done
                DateTime dt = DateTime.Now;
                bool done = false;
                while(!done)
                {
                    done = true;

                    foreach (VirtualClient vc in clients)
                    {
                        if (vc.IsRunning())
                        {
                            done = false;
                            break;
                        }
                    }

                    if (done)
                    {
                        Trace.WriteLine(string.Format("{0} clients finished in {1}ms", 
                                        clients.Count, 
                                        DateTime.Now.Subtract(dt).TotalMilliseconds));
                    }
                    else
                    {
                        Thread.Sleep(1);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                Trace.WriteLine("");

                foreach (VirtualClient vc in clients)
                {
                    vc.Stop();
                }
            }
        }
    }
}