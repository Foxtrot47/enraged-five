using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;

using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.StressTester
{
    public class Stat
    {
        public int Min { get; private set; }
        public int Max { get; private set; }
        public int Total { get; private set; }
        public uint NumSamples { get; private set; }
        public float Avg
        {
            get
            {
                if (NumSamples > 0)
                {
                    return (float)Total / NumSamples;
                }
                return 0;
            }
        }

        public Stat()
        {
            Min = int.MaxValue;
            Max = int.MinValue;
            Total = 0;
            NumSamples = 0;
        }

        public void AddSample(int sample)
        {
            if (sample < Min) Min = sample;
            if (sample > Max) Max = sample;
            Total += sample;
            ++NumSamples;
        }
    }

    public class MostRecentStat
    {
        public int Min { get { Refresh(); return m_Min; } }
        public int Max { get { Refresh(); return m_Max; } }
        public int Total { get { Refresh(); return m_Total; } }
        public int NumSamples { get { return m_Samples.Count; } }
        public float Avg
        {
            get
            {
                if (NumSamples > 0)
                {
                    return (float)Total / NumSamples;
                }
                return 0;
            }
        }

        private bool m_Dirty = true;
        private int m_MaxSamples;
        private Queue<int> m_Samples;
        private int m_Min;
        private int m_Max;
        private int m_Total;

        public MostRecentStat(int num)
        {
            m_MaxSamples = num;
            m_Samples = new Queue<int>(m_MaxSamples);
        }

        public void AddSample(int sample)
        {
            m_Samples.Enqueue(sample);
            if (m_Samples.Count > m_MaxSamples) m_Samples.Dequeue();
            m_Dirty = true;

        }

        private void Refresh()
        {
            if (m_Dirty)
            {
                m_Min = int.MaxValue;
                m_Max = int.MinValue;
                m_Total = 0;

                foreach (int sample in m_Samples)
                {
                    if (sample < m_Min) m_Min = sample;
                    if (sample > m_Max) m_Max = sample;
                    m_Total += sample;
                }
                m_Dirty = false;
            }
        }
    }

    public class VirtualClient
    {
        private static int m_NextId = 0;
        private int m_Id = 0;

        private string m_ClassName = null;
        private string m_MethodName = null;
        private List<string> m_Args = null;
        private int m_NumIterations = 0;
        private int m_MaxRecent = 0;
        
        private Thread m_Thread = null;

        public VirtualClient(string className, 
                             string methodName,
                             List<string> args,
                             int numIterations,
                             int maxRecent)
        {
            m_Id = m_NextId++;
            m_ClassName = className;
            m_MethodName = methodName;
            m_Args = args;
            m_NumIterations = numIterations;
            m_MaxRecent = maxRecent;
        }

        public void Start()
        {
            m_Thread = new Thread(ThreadMain);
            m_Thread.Start();
        }

        public void Stop()
        {
            if (m_Thread != null && m_Thread.IsAlive)
            {
                m_Thread.Abort();
            }
        }

        public bool IsRunning()
        {
            return (m_Thread != null && m_Thread.IsAlive);
        }

        public void ThreadMain()
        {
            Context ctx = new Context("{0}", m_Id);

            try
            {
                //Prefix our class name with default namespace, if none specified.
                if (!m_ClassName.Contains("."))
                {
                    m_ClassName = "Rockstar.Rockon.StressTester." + m_ClassName;
                }

                //Find the class in our loaded assemblies.
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                Assembly testAssembly = null;
                Type testType = null;
                foreach (Assembly a in assemblies)
                {
                    testType = a.GetType(m_ClassName, false, true);

                    if(testType != null)
                    {
                        testAssembly = a;
                        break;
                    }
                }

                if (testType == null)
                {
                    throw new NullReferenceException("Could not find type " + m_ClassName + " in any loaded assembly.");
                }

                Stat statAllMs = new Stat();
                MostRecentStat statRecentMs = new MostRecentStat(m_MaxRecent);
                DateTime start;

                for (int i = 0; i < m_NumIterations; i++)
                {
                    //Create a fresh instance of the object
                    object obj = testAssembly.CreateInstance(testType.FullName);
                    object[] args = { m_Id, i, m_Args };

                    //Call the test Prepare method (if any).  This allows the test to do
                    //any necessary prep without counting against the run time.
                    if (null != testType.GetMethod("Prepare" + m_MethodName))
                    {
                        start = DateTime.Now;
                        testType.InvokeMember("Prepare" + m_MethodName,
                                              BindingFlags.InvokeMethod,
                                              null,
                                              obj,
                                              args);
                        ctx.Debug2("Prepare took {0}ms", DateTime.Now.Subtract(start).TotalMilliseconds);
                    }

                    //Call the actual test method.  Its execution is counted in our stats.
                    start = DateTime.Now;
                    testType.InvokeMember(m_MethodName,
                                          BindingFlags.InvokeMethod,
                                          null,
                                          obj,
                                          args);


                    int deltaMs = (int)DateTime.Now.Subtract(start).TotalMilliseconds;

                    statAllMs.AddSample(deltaMs);
                    statRecentMs.AddSample(deltaMs);
                }

                ctx.Success2("{0} iter took {1}ms (min={2}ms  max={3}ms  avg={4}ms)",
                             statAllMs.NumSamples,
                             statAllMs.Total,
                             statAllMs.Min,
                             statAllMs.Max,
                             statAllMs.Avg);

                ctx.Success2("Most recent {0} iter took {1}ms (min={2}ms  max={3}ms  avg={4}ms)",
                             statRecentMs.NumSamples,
                             statRecentMs.Total,
                             statRecentMs.Min,
                             statRecentMs.Max,
                             statRecentMs.Avg);
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
        }
    }
}