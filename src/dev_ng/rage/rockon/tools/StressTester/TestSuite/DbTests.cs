﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.StressTester
{
    public class DbTests
    {
        //PURPOSE
        //  See how fast the DB can fetch and randomly read X simple rows.
        public void AcctTest1(int clientId, int iteration, List<string> args)
        {
            EnvDbChannel.Channel.SetSeverity(Channel.Severity.Error);

            Context ctx = new Context("{0}:{1}", clientId, iteration);
            Error err;

            try
            {
                int index = 0;
                uint numAccounts = uint.Parse(args[index++]);

                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    //Read up to X accounts
                    List<AccountInfo> accounts;
                    if (null != (err = Account.QueryAccount(con, 0, numAccounts, out accounts)))
                    {
                        ctx.Error(err);
                        return;
                    }

                    if (accounts == null)
                    {
                        accounts = new List<AccountInfo>();
                    }

                    //Create accounts up to X, if necessary
                    while (accounts.Count < numAccounts)
                    {
                        AccountInfo a = new AccountInfo();
                        if (null != (err = Account.CreateAccount(con, out a.Id)))
                        {
                            ctx.Error(err);
                            return;
                        }

                        a.CreateDateTime = DateTime.UtcNow;

                        accounts.Add(a);
                    }

                    //Randomly read the accounts
                    Random rng = new Random();
                    for (int i = 0; i < accounts.Count; i++)
                    {
                        AccountInfo info;
                        if (null != (err = Account.QueryAccount(con, accounts[rng.Next() % accounts.Count].Id, out info)))
                        {
                            ctx.Error(err);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Same as Test1, except we create an SqlConnectionHelper for each
        //  individual query.  This is to test connection reuse.
        //NOTE
        //  Wrapping the entire thing in a using of the db results in
        //  no gain (tested that).
        public void AcctTest2(int clientId, int iteration, List<string> args)
        {
            EnvDbChannel.Channel.SetSeverity(Channel.Severity.Error);

            Context ctx = new Context("{0}:{1}", clientId, iteration);
            Error err;

            try
            {
                int index = 0;
                uint numAccounts = uint.Parse(args[index++]);

                List<AccountInfo> accounts;

                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    //Read up to X accounts
                    if (null != (err = Account.QueryAccount(con, 0, numAccounts, out accounts)))
                    {
                        ctx.Error(err);
                        return;
                    }

                    if (accounts == null)
                    {
                        accounts = new List<AccountInfo>();
                    }
                }

                //Create accounts up to X, if necessary
                while (accounts.Count < numAccounts)
                {
                    using (SqlConnectionHelper con = EnvDbHelper.Open())
                    {
                        AccountInfo a = new AccountInfo();
                        if (null != (err = Account.CreateAccount(con, out a.Id)))
                        {
                            ctx.Error(err);
                            return;
                        }

                        a.CreateDateTime = DateTime.UtcNow;

                        accounts.Add(a);
                    }
                }

                //Randomly read the accounts
                Random rng = new Random();
                for (int i = 0; i < accounts.Count; i++)
                {
                    using (SqlConnectionHelper con = EnvDbHelper.Open())
                    {
                        AccountInfo info;
                        if (null != (err = Account.QueryAccount(con, accounts[rng.Next() % accounts.Count].Id, out info)))
                        {
                            ctx.Error(err);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
        }

        //PURPOSE
        //  See how fast we can open connections to a title's DB.
        public void TitleTest1(int clientId, int iteration, List<string> args)
        {
            EnvDbChannel.Channel.SetSeverity(Channel.Severity.Error);

            Context ctx = new Context("{0}:{1}", clientId, iteration);

            try
            {
                int titleId = int.Parse(args[0]);
                int numConnects = int.Parse(args[1]);

                for (int i = 0; i < numConnects; i++)
                {
                    using (SqlConnectionHelper con = Title.Open(titleId))
                    {
                    }                
                }
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
        }
    }
}
