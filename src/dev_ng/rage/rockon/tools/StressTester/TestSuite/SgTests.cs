﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.StressTester
{
    public class SbTests
    {
        public class ClientStats
        {
            public static int NumConnectionsClosed;
            public static int NumConnectionsOpened;
            public static int NumInvalidSockets;
            public static int NumOtherExceptions;
            public static int NumRequestsSent;
            public static int NumSocketExceptions;

            public static string ToString()
            {
                StringBuilder sb = new StringBuilder();
                int num = 0;

                sb.Append("Cs[");
                if (NumConnectionsOpened > 0)
                {
                    sb.Append("co=" + NumConnectionsOpened);
                    ++num;
                }
                if (NumRequestsSent > 0)
                {
                    if (num > 0) sb.Append(" ");
                    sb.Append("r=" + NumRequestsSent);
                    ++num;
                }
                if (NumInvalidSockets > 0)
                {
                    if (num > 0) sb.Append(" ");
                    sb.Append("inv=" + NumInvalidSockets);
                    ++num;
                }
                if (NumOtherExceptions > 0)
                {
                    if (num > 0) sb.Append(" ");
                    sb.Append("oex=" + NumOtherExceptions);
                    ++num;
                }
                if (NumSocketExceptions > 0)
                {
                    if (num > 0) sb.Append(" ");
                    sb.Append("sex=" + NumSocketExceptions);
                    ++num;
                }
                if (NumConnectionsClosed > 0)
                {
                    if (num > 0) sb.Append(" ");
                    sb.Append("cl=" + NumConnectionsClosed);
                    ++num;
                }
                sb.Append("]");

                return sb.ToString();
            }
        }


        //PURPOSE
        //  Client that async connects to an SG, logs in, and then sends requests.
        public class Client
        {
            static public Random rng = new Random();

            private readonly object m_Lock = new object();

            private SocketHelper m_SktHelper = new SocketHelper();
            private int m_TitleId;
            private UInt64 m_Xuid;
            private int m_ReceiveTimeoutMs;
            private int m_MinSendDelayMs;
            private int m_MaxSendDelayMs;
            private Timer m_SendTimer;

            private enum State
            {
                Idle,
                Connecting,
                LoggingIn,
                SendRequest,
                WaitingForReply,
                Dead
            }
            private State m_State = State.Idle;

            public Client()
            {
            }

            public Error Init(int titleId,
                              UInt64 xuid,
                              IPEndPoint serverEp,
                              int receiveTimeoutMs,
                              int minSendDelayMs,
                              int maxSendDelayMs)
            {
                lock (m_Lock)
                {
                    Context ctx = new Context();

                    try
                    {
                        m_TitleId = titleId;
                        m_Xuid = xuid;
                        m_ReceiveTimeoutMs = receiveTimeoutMs;
                        m_MinSendDelayMs = minSendDelayMs;
                        m_MaxSendDelayMs = maxSendDelayMs;

                        Socket s = new Socket(AddressFamily.InterNetwork,
                                              SocketType.Stream,
                                              ProtocolType.Tcp);
                        try
                        {
                            s.BeginConnect(serverEp, new AsyncCallback(ConnectCallback), s);
                        }
                        catch (SocketException ex)
                        {
                            Interlocked.Increment(ref ClientStats.NumSocketExceptions);
                            ctx.Error(ex.SocketErrorCode, "BeginConnect SocketError: " + ex.SocketErrorCode);
                        }
                        catch (Exception ex)
                        {
                            Interlocked.Increment(ref ClientStats.NumOtherExceptions);
                            ctx.Error(ex);
                        }

                        m_State = State.Connecting;

                        return null;
                    }
                    catch (Exception ex)
                    {
                        Interlocked.Increment(ref ClientStats.NumOtherExceptions);
                        return ctx.Error(ex);
                    }
                }
            }

            public void Shutdown()
            {
                lock (m_Lock)
                {
                    if (m_SktHelper != null)
                    {
                        m_SktHelper.Shutdown();
                    }

                    if (m_SendTimer != null)
                    {
                        m_SendTimer.Dispose();
                        m_SendTimer = null;
                    }
                }
            }

            public void Die()
            {
                lock (m_Lock)
                {
                    if (m_State != State.Dead)
                    {
                        m_State = State.Dead;
                        //Trace.WriteLine("Client " + m_Xuid + " died");

                        Shutdown();
                    }
                }
            }

            public bool IsDead()
            {
                return m_State == State.Dead;
            }

            public void ConnectCallback(IAsyncResult ar)
            {
                if (IsDead()) return;

                lock (m_Lock)
                {
                    Context ctx = new Context();

                    try
                    {
                        Socket s = ar.AsyncState as Socket;

                        try
                        {
                            s.EndConnect(ar);
                        }
                        catch (SocketException ex)
                        {
                            Interlocked.Increment(ref ClientStats.NumSocketExceptions);
                            Trace.WriteLine(string.Format("EndConnect SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                            Die();
                            return;
                        }
                        catch (Exception ex)
                        {
                            Interlocked.Increment(ref ClientStats.NumOtherExceptions);
                            Trace.WriteLine(ex);
                            Die();
                            return;
                        }

                        Interlocked.Increment(ref ClientStats.NumConnectionsOpened);

                        m_SktHelper = new SocketHelper();

                        Error err;
                        if (null != (err = m_SktHelper.Init(s,
                                         new WaitCallback(ReceivedMsgCallback),
                                         new WaitCallback(ConnectionClosedCallback),
                                         new RttpMsgReader(),
                                         new RttpMsgWriter(),
                                         (uint)m_ReceiveTimeoutMs)))
                        {
                            Trace.WriteLine(err);
                            Die();
                            return;
                        }

                        //Send login message
                        SendRttp("{titleId=" + m_TitleId + ",xuid=" + m_Xuid + ",gamertag=rtrickey" + m_Xuid + "}");
                        m_State = State.LoggingIn;
                    }
                    catch (Exception ex)
                    {
                        Interlocked.Increment(ref ClientStats.NumOtherExceptions);
                        Trace.WriteLine(ex);
                        Die();
                        return;
                    }
                }
            }

            private void ConnectionClosedCallback(object reason)
            {
                Trace.WriteLine("Client " + m_Xuid + " closed (" + (SocketHelper.Reason)reason + ")");
                Die();
            }

            private void ReceivedMsgCallback(object msg)
            {
                lock (m_Lock)
                {
                    RttpMessage m = msg as RttpMessage;
                    if (m == null) throw new ArgumentNullException("msg");

                    if (m_State == State.LoggingIn)
                    {
                        //If we got a response, we succeeded (server doesn't reply on failure).
                        //Start randomly sending requests.
                        RestartSendTimer();
                        m_State = State.SendRequest;
                    }
                    else if (m_State == State.WaitingForReply)
                    {
                        //We don't care about the response.  Just start sending another request.
                        RestartSendTimer();
                        m_State = State.SendRequest;
                    }
                    else
                    {
                        Trace.WriteLine("Received msg in invalid state: " + m_State);
                        Die();
                    }
                }
            }

            private void SendRttp(string msg)
            {
                lock (m_Lock)
                {
                    RttpMessage rttpMsg = new RttpMessage();
                    rttpMsg.ResetForPush(Encoding.UTF8.GetBytes(msg));
                    m_SktHelper.Send(rttpMsg);
                }
            }

            private void RestartSendTimer()
            {
                lock (m_Lock)
                {
                    int delayMs = rng.Next(m_MinSendDelayMs, m_MaxSendDelayMs);

                    if (m_SendTimer == null)
                    {
                        m_SendTimer = new Timer(new TimerCallback(SendCallback), null, delayMs, Timeout.Infinite);
                    }
                    else
                    {
                        m_SendTimer.Change(delayMs, Timeout.Infinite);
                    }
                }
            }

            private void SendCallback(object state)
            {
                lock (m_Lock)
                {
                    if (m_State == State.SendRequest)
                    {
                        List<string> requestStrs = new List<string>();
                        requestStrs.Add("{m=MailboxService.GetNumRecords,args={mailboxName=TestMailbox,startRecordId=0,endRecordId=0,includeExpired=false}}");
                        requestStrs.Add("{m=MailboxService.ReadRecords,args={mailboxName=TestMailbox,startRecordId=0,endRecordId=0,maxRecords=10,readUserData=false,readMsgData=false,oldestFirst=true}}");
                        requestStrs.Add("{m=MailboxService.ReadRecords,args={mailboxName=TestMailbox,startRecordId=0,endRecordId=0,maxRecords=10,readUserData=true,readMsgData=false,oldestFirst=true}}");
                        requestStrs.Add("{m=MailboxService.ReadRecords,args={mailboxName=TestMailbox,startRecordId=0,endRecordId=0,maxRecords=10,readUserData=true,readMsgData=true,oldestFirst=true}}");

                        //Randomly select a request to send from our test sequence.
                        Random rng = new Random();
                        SendRttp(requestStrs[rng.Next(0, requestStrs.Count)]);
                        m_State = State.WaitingForReply;

                        Interlocked.Increment(ref ClientStats.NumRequestsSent);
                    }
                    else
                    {
                        Trace.WriteLine("SendCallback in invalid state: " + m_State);
                        Die();
                    }
                }
            }
        }

        private static Timer m_StatsTimer;

        private static void PrintClientStats(object state)
        {
            Trace.WriteLine(ClientStats.ToString());
        }

        //PURPOSE
        public void Test1(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);

            int index = 0;
            IPEndPoint serverEp = new IPEndPoint(IPAddress.Parse(args[index++]), int.Parse(args[index++]));
            int titleId = int.Parse(args[index++]);
            int baseXuid = int.Parse(args[index++]);
            int numClients = int.Parse(args[index++]);
            int betweenConnectMs = int.Parse(args[index++]);
            int receiveTimeoutMs = int.Parse(args[index++]);
            int minSendDelayMs = int.Parse(args[index++]);
            int maxSendDelayMs = int.Parse(args[index++]);

            //NOTE: Wierd bug: if this timer is a local variable, it stops working shortly after
            //      starting in Release builds.  Works fine in debug, though.
            m_StatsTimer = new Timer(new TimerCallback(PrintClientStats), null, 1000, 1000);

            List<Client> clients = new List<Client>();

            for (int i = 0; i < numClients; i++)
            {
                Client c = new Client();

                Error err;
                if (null != (err = c.Init(titleId,
                                         (UInt64)(baseXuid + i),
                                         serverEp,
                                         receiveTimeoutMs,
                                         minSendDelayMs,
                                         maxSendDelayMs)))
                {
                    ctx.Error(err);
                }
                else
                {
                    clients.Add(c);

                    if (betweenConnectMs > 0)
                    {
                        Thread.Sleep(betweenConnectMs);
                    }

                    //ctx.Debug1("Past between sleep...");
                    //PrintClientStats(null);
                }
            }

            bool done = false;
            while (!done)
            {
                foreach (Client c in clients)
                {
                    if (!c.IsDead())
                    {
                        Thread.Sleep(5000);
                        break;
                    }
                    done = true;
                }
            }
        }
    }
}
