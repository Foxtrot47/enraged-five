﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.StressTester
{
    public class AccountNpLinkTests
    {
        //----------------------------------------------------------------------
        //Account creation/query/delete speed
        //----------------------------------------------------------------------
        public void TestAll(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);
            Error err = null;

            Channel.Severity severity = Channel.GlobalMaxSeverity;
            Channel.GlobalMaxSeverity = Channel.Severity.None;

            using (SqlConnectionHelper con = EnvDbHelper.Open())
            {
                DateTime dt;

                //Read all links
                ctx.Debug2("Reading all links");
                dt = DateTime.Now;
                List<AccountNpLinkInfo> infos;
                if (null != (err = AccountNpLink.QueryLinks(con, null, 0, out infos)))
                {
                    throw new Exception(err.ToString());
                }
                ctx.Debug2("{0} in {1}ms",
                           infos.Count,
                           (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                //Randomly read links
                ctx.Debug2("Randomly reading links");
                dt = DateTime.Now;
                Random rng = new Random();
                for (int i = 0; i < infos.Count; i++)
                {
                    int index = rng.Next(infos.Count);
                    AccountNpLinkInfo dummyInfo;
                    if (null != (err = AccountNpLink.QueryLink(con, infos[index].OnlineId, out dummyInfo)))
                    {
                        throw new Exception(err.ToString());
                    }
                }
                ctx.Debug2("{0} in {1}ms",
                           infos.Count,
                           (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                //Delete all links
                ctx.Debug2("Deleting existing links");
                dt = DateTime.Now;
                foreach (AccountNpLinkInfo info in infos)
                {
                    AccountNpLink.DeleteLink(con, info.OnlineId);
                }
                ctx.Debug2("{0} in {1}ms",
                           infos.Count,
                           (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                //Read all accounts
                ctx.Debug2("Reading all existing accounts");
                dt = DateTime.Now;
                List<AccountInfo> accounts;
                if (null != (err = Account.QueryAccount(con, 0, 0, out accounts)))
                {
                    throw new Exception(err.ToString());
                }
                ctx.Debug2("{0} accts in {1}ms",
                           accounts.Count,
                           (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                //Create a bunch of links
                ctx.Debug2("Creating new links");
                Channel.GlobalMaxSeverity = Channel.Severity.Error;
                dt = DateTime.Now;
                for (int i = 0; i < accounts.Count; i++)
                {
                    AccountNpLink.CreateLink(con, accounts[i].Id, "MyOnlineId" + i);
                }
                ctx.Debug2("{0} in {1}ms",
                           accounts.Count,
                           (int)DateTime.Now.Subtract(dt).TotalMilliseconds);
            }

            Channel.GlobalMaxSeverity = severity;
        }

        //----------------------------------------------------------------------
        //Tests only random record access
        //----------------------------------------------------------------------
        public void TestRandomAccess(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);
            Error err = null;

            Channel.Severity severity = Channel.GlobalMaxSeverity;
            Channel.GlobalMaxSeverity = Channel.Severity.None;

            using (SqlConnectionHelper con = EnvDbHelper.Open())
            {
                DateTime dt;

                //Read all links
                ctx.Debug2("Reading all links");
                dt = DateTime.Now;
                List<AccountNpLinkInfo> infos;
                if (null != (err = AccountNpLink.QueryLinks(con, null, 0, out infos)))
                {
                    throw new Exception(err.ToString());
                }
                ctx.Debug2("{0} in {1}ms",
                           infos.Count,
                           (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                //Randomly read links
                ctx.Debug2("Randomly reading links");
                dt = DateTime.Now;
                Random rng = new Random();
                for (int i = 0; i < infos.Count; i++)
                {
                    int index = rng.Next(infos.Count);
                    AccountNpLinkInfo dummyInfo;
                    if (null != (err = AccountNpLink.QueryLink(con, infos[index].OnlineId, out dummyInfo)))
                    {
                        throw new Exception(err.ToString());
                    }
                }
                ctx.Debug2("{0} in {1}ms",
                           infos.Count,
                           (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

            }

            Channel.GlobalMaxSeverity = severity;
        }
    }
}
