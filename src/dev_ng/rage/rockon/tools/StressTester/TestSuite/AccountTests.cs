﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.StressTester
{
    public class AccountTests
    {
        //----------------------------------------------------------------------
        //Account creation/query/delete speed
        //----------------------------------------------------------------------
        public void TestAll(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);
            Error err = null;

            Channel.Severity severity = Channel.GlobalMaxSeverity;
            Channel.GlobalMaxSeverity = Channel.Severity.Error;

            int numAccounts = int.Parse(args[0]);

            try
            {
                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    DateTime dt;

                    //Read all accounts
                    ctx.Debug2("Reading all existing accounts");
                    dt = DateTime.Now;
                    List<AccountInfo> accounts;
                    if (null != (err = Account.QueryAccount(con, 0, 0, out accounts)))
                    {
                        throw new Exception(err.ToString());
                    }
                    ctx.Debug2("{0} accts in {1}ms",
                               accounts.Count,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                    //Randomly read accounts
                    ctx.Debug2("Randomly reading accounts");
                    dt = DateTime.Now;
                    Random rng = new Random();
                    for (int i = 0; i < accounts.Count; i++)
                    {
                        int index = rng.Next(accounts.Count);
                        AccountInfo dummyInfo;
                        if (null != (err = Account.QueryAccount(con, accounts[index].Id, out dummyInfo)))
                        {
                            throw new Exception(err.ToString());
                        }
                    }
                    ctx.Debug2("{0} accts in {1}ms",
                               accounts.Count,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                    //Delete all accounts
                    ctx.Debug2("Deleting existing accounts");
                    dt = DateTime.Now;
                    foreach (AccountInfo info in accounts)
                    {
                        Account.DeleteAccount(con, info.Id);
                    }
                    ctx.Debug2("{0} accts in {1}ms",
                               accounts.Count,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                    //Create a bunch of accounts
                    ctx.Debug2("Creating new accounts");
                    dt = DateTime.Now;
                    for (int i = 0; i < numAccounts; i++)
                    {
                        Int64 accountId;
                        Account.CreateAccount(con, out accountId);
                    }
                    ctx.Debug2("{0} accts in {1}ms",
                               numAccounts,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);
                }

                Channel.GlobalMaxSeverity = severity;
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
        }
    }
}
