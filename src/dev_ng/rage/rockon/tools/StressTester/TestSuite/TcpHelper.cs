﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace Rockstar.Rockon.StressTester
{
    public class TcpHelper
    {
        private Socket m_Skt = null;
        private SocketHelper m_SktHelper = new SocketHelper();
        private List<string> m_ReceivedMsgs = new List<string>();

        public bool Connected
        {
            get { return m_Skt != null; }
        }

        public bool Init(Socket s, uint timeoutMs)
        {
            Context ctx = new Context();

            bool success = false;

            try
            {
                m_Skt = s;

                Error err;
                if (null == (err = m_SktHelper.Init(m_Skt,
                                     new WaitCallback(MsgReceived),
                                     new WaitCallback(ConnectionClosed),
                                     new RttpMsgReader(),
                                     new RttpMsgWriter(),
                                     timeoutMs)))
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
            finally
            {
                if (!success)
                {
                    Disconnect();
                }
            }

            return success;
        }

        public bool Connect(string host, int port)
        {
            Context ctx = new Context("host={0}, port={1}", host, port);

            bool success = false;

            try
            {
                m_Skt = new Socket(AddressFamily.InterNetwork,
                                   SocketType.Stream,
                                   ProtocolType.Tcp);

                m_Skt.Connect(host, port);

                Error err;
                if (null == (err = m_SktHelper.Init(m_Skt,
                                     new WaitCallback(MsgReceived),
                                     new WaitCallback(ConnectionClosed),
                                     new RttpMsgReader(),
                                     new RttpMsgWriter(),
                                     0)))
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
            finally
            {
                if (!success)
                {
                    Disconnect();
                }
            }

            return success;
        }

        public void Disconnect()
        {
            if (m_SktHelper != null)
            {
                m_SktHelper.Shutdown();
            }

            if (m_Skt != null)
            {
                if (m_Skt.Connected)
                {
                    m_Skt.Close();
                }
                m_Skt = null;
            }

            if (m_ReceivedMsgs != null)
            {
                m_ReceivedMsgs.Clear();
            }
        }

        public bool SendRttp(string msg)
        {
            if (Connected)
            {
                RttpMessage rttpMsg = new RttpMessage();
                rttpMsg.ResetForPush(Encoding.UTF8.GetBytes(msg));
                m_SktHelper.Send(rttpMsg);
                return true;
            }
            
            return false;
        }

        public string Receive(int timeoutMs)
        {
            string s = null;

            DateTime dt = DateTime.Now;
            while((0 == m_ReceivedMsgs.Count)
                  && (DateTime.Now.Subtract(dt).TotalMilliseconds < timeoutMs))
            {
                Thread.Sleep(1);
            }

            if (0 != m_ReceivedMsgs.Count)
            {
                s = m_ReceivedMsgs[0];
                m_ReceivedMsgs.RemoveAt(0);
            }

            return s;
        }

        private void ConnectionClosed(object reason)
        {
            Disconnect();
        }

        private void MsgReceived(object msg)
        {
            RttpMessage rttpMsg = msg as RttpMessage;

            if (null == rttpMsg)
            {
                throw new Exception("Msg is not an RsonReader");
            }

            m_ReceivedMsgs.Add(rttpMsg.GetRsonString());
        }
    }
}
