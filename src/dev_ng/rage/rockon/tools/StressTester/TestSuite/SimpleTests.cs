﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace Rockstar.Rockon.StressTester
{
    public class SimpleTests
    {
        private Semaphore m_WebCallSemaphore = new Semaphore(0, 1);

        private class CallUserData
        {
            public object UserData = null;
        }

        public void WebCall(string uri,                        //URI of web service
                            string ns,                         //Namespace of method
                            string m,                          //Method to call
                            Dictionary<string, string> args,   //Args to pass
                            int timeoutMs,
                            object userdata,
                            bool async,
                            out uint requestId,
                            HttpWebRequestStats stats)
        {
            Error err;
            requestId = 0;

            SoapEnvelopeBuilder sb = new SoapEnvelopeBuilder(SoapVersion.Soap1_2, m, ns);

            foreach (KeyValuePair<string, string> kvp in args)
            {
                sb.AddParam(kvp.Key, kvp.Value);
            }

            SoapEnvelopeEx env = sb.GenerateEnvelopeEx();

            CallUserData cud = new CallUserData();
            cud.UserData = userdata;

            HttpWebRequestHelper.SuccessDelegate successDlgt =
                delegate(string content, object ud)
                {
                    CallUserData _cud = ud as CallUserData;
                    m_WebCallSemaphore.Release();
                };

            HttpWebRequestHelper.ErrorDelegate errorDlgt =
                delegate(Error e, object ud)
                {
                    CallUserData _cud = ud as CallUserData;
                    m_WebCallSemaphore.Release();
                };

            if (async)
            {
                err = HttpWebRequestHelper.ExecuteSoapRequest(uri,
                                                              env,
                                                              successDlgt,
                                                              errorDlgt,
                                                              timeoutMs,
                                                              cud,
                                                              out requestId,
                                                              stats);

                if (err != null)
                {
                    errorDlgt(err, userdata);
                    return;
                }

                m_WebCallSemaphore.WaitOne();
            }
            else
            {
                string responseContent;
                err = HttpWebRequestHelper.ExecuteSoapRequest(uri, env, out responseContent, stats);

                if (err != null)
                {
                    errorDlgt(err, userdata);
                    return;
                }
            }
        }

        public void TestThreadSwitchSpeed(int clientId, int iteration, List<string> args)
        {
            for (int i = 0; i < 1000; i++)
            {
                Thread.Sleep(1);
            }
        }

        public void TestContextSpeed(int clientId, int iteration, List<string> args)
        {
            for (int i = 0; i < 100000; i++)
            {
                Context ctx = new Context("{0}:{1}", clientId, iteration);
            }
        }

        public void TestRsonWriting(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);

            RsonWriter rw = new RsonWriter(100000);

            for (int i = 0; i < 1000; i++)
            {
                rw.WriteString("Test" + i, "My string " + i);
            }
        }

        public void TestWebCallSpeed(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);

            bool async = false;

            Dictionary<string, string> soapArgs = new Dictionary<string,string>();
            soapArgs.Add("async", "true");
            soapArgs.Add("delayMs", "100");
           
            for(int i = 0; i < args.Count; i++)
            {
                if(args[i] == "async")
                {
                    async = bool.Parse(args[++i]);
                    //soapArgs["async"] = args[++i];
                }
                else if(args[i] == "delayMs")
                {
                    soapArgs["delayMs"] = args[++i];
                }
            }

            DateTime dt = DateTime.Now;

            uint requestId;

            HttpWebRequestStats stats = new HttpWebRequestStats();

            WebCall("http://dev.services.sg.rockon.rockstargames.com/RlineServices/MailboxService.asmx",
                    "http://rline.services.rockon.rockstar.com",
                    "Dummy",
                    soapArgs,
                    30000,
                    null,
                    async,
                    out requestId,
                    stats);

            ctx.Debug2(stats.ToString());
        }
    }
}
