﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;
namespace Rockstar.Rockon.StressTester
{
    public class MailboxTests
    {
        //----------------------------------------------------------------------
        //Account creation/query/delete speed
        //----------------------------------------------------------------------
        public void TestAll(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);
            Error err = null;

            Random rng = new Random(1000);

            Channel.Severity severity = Channel.GlobalMaxSeverity;
            Channel.GlobalMaxSeverity = Channel.Severity.Debug1;

            int titleId = int.Parse(args[0]);
            int numRecords = int.Parse(args[1]);
            
            byte[] userData = new byte[int.Parse(args[2])];
            for(int i = 0; i < userData.Length; i++)
            {
                userData[i] = (byte)(i % 10);
            }

            byte[] msgData = new byte[int.Parse(args[3])];
            for (int i = 0; i < msgData.Length; i++)
            {
                msgData[i] = (byte)(i % 10);
            }

            try
            {
                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    DateTime dt;

                    //Read all accounts; we'll use them as senders and receivers
                    ctx.Debug2("Reading all existing accounts");
                    dt = DateTime.Now;
                    List<AccountInfo> accounts;
                    if (null != (err = Account.QueryAccount(con, 0, (uint)numRecords, out accounts)))
                    {
                        throw new Exception(err.ToString());
                    }
                    ctx.Debug2("{0} accts in {1}ms",
                               accounts.Count,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                    //Read all records
                    ctx.Debug2("Reading all existing");
                    dt = DateTime.Now;
                    List<MailboxRecord> records;
                    err = Mailbox.QueryRecords(titleId,
                                               Mailbox.SYSTEM_ID,
                                               "TestMailbox",
                                               Mailbox.ANY_ID,
                                               Mailbox.ANY_ID,
                                               0,
                                               0,
                                               0,
                                               true,
                                               true,
                                               true,
                                               true,
                                               out records);
                    if (null != err)
                    {
                        throw new Exception(err.ToString());
                    }
                    ctx.Debug2("{0} recs in {1}ms",
                               records.Count,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                    //Verify the user and msg data is correct
                    if (records.Count > 0)
                    {
                        ctx.Debug2("Verifying userdata ({0} bytes)", records[0].UserDataLength);
                        for (int i = 0; i < records[0].UserDataLength; i++)
                        {
                            if (records[0].UserData[i] != (byte)(i % 10))
                            {
                                ctx.Error(ErrorCode.InvalidData, "Byte {0} has unexpected value", i);
                            }
                        }
                        ctx.Debug2("Verified");

                        ctx.Debug2("Verifying msgdata ({0} bytes)", records[0].MsgDataLength);
                        for (int i = 0; i < records[0].MsgDataLength; i++)
                        {
                            if (records[0].MsgData[i] != (byte)(i % 10))
                            {
                                ctx.Error(ErrorCode.InvalidData, "Byte {0} has unexpected value", i);
                            }
                        }
                        ctx.Debug2("Verified");
                    }

                    //Randomly read records, using each account as a receiver
                    ctx.Debug2("Randomly reading records");
                    dt = DateTime.Now;
                    for (int i = 0; i < records.Count; i++)
                    {
                        AccountInfo actInfo = accounts[rng.Next() % accounts.Count];

                        int index = rng.Next(records.Count);
                        List<MailboxRecord> dummyRecords;
                        err = Mailbox.QueryRecords(titleId,
                                                   actInfo.Id,
                                                   "TestMailbox",
                                                   Mailbox.ANY_ID,
                                                   actInfo.Id,
                                                   0,
                                                   0,
                                                   0,
                                                   true,
                                                   true,
                                                   true,
                                                   true,
                                                   out dummyRecords);
                        if (null != err)
                        {
                            throw new Exception(err.ToString());
                        }
                    }
                    ctx.Debug2("{0} recs in {1}ms",
                               records.Count,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                    //Delete all records
                    ctx.Debug2("Deleting existing records");
                    dt = DateTime.Now;
                    foreach (MailboxRecord r in records)
                    {
                        uint numDeleted;
                        Mailbox.DeleteRecords(titleId, 
                                              r.ReceiverId, 
                                              "TestMailbox", 
                                              Mailbox.ANY_ID,
                                              r.ReceiverId,
                                              r.RecordId,
                                              r.RecordId,
                                              false,
                                              out numDeleted);
                    }
                    ctx.Debug2("{0} recs in {1}ms",
                               records.Count,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);

                    //Create a bunch of records, randomly selecting senders and receivers
                    ctx.Debug2("Creating new records");
                    dt = DateTime.Now;
                    for (int i = 0; i < numRecords; i++)
                    {
                        Int64 senderId = accounts[rng.Next() % accounts.Count].Id;
                        Int64 receiverId = accounts[rng.Next() % accounts.Count].Id;
                        while (receiverId == senderId)
                        {
                            receiverId = accounts[rng.Next() % accounts.Count].Id;
                        }


                        Int64 recordId;
                        Mailbox.CreateRecord(titleId, senderId, "TestMailbox", "System", receiverId, userData, msgData, 100000, out recordId);
                    }
                    ctx.Debug2("{0} recs in {1}ms",
                               numRecords,
                               (int)DateTime.Now.Subtract(dt).TotalMilliseconds);
                }

                Channel.GlobalMaxSeverity = severity;
            }
            catch (Exception ex)
            {
                ctx.Error(ex);
            }
        }
    }
}
