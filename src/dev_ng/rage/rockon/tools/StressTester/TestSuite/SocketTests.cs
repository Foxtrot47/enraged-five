﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace Rockstar.Rockon.StressTester
{
    public class SocketTests
    {
        //Opens as many connections to a server as possible
        private static void OpenConnections(IPEndPoint ep,
                                            int maxConnections,
                                            int maxConsecutiveFailures,
                                            int betweenAttemptsSleepMs,
                                            out List<Socket> skts)
        {
            Context ctx = new Context();

            ctx.Debug1("Creating connections to {0}", ep);

            bool done = false;
            skts = new List<Socket>();
            int consecutiveFailures = 0;

            while (!done)
            {
                try
                {
                    Socket s = new Socket(AddressFamily.InterNetwork,
                                          SocketType.Stream,
                                          ProtocolType.Tcp);

                    bool success = false;

                    try
                    {
                        s.Connect(ep);
                        success = true;
                    }
                    catch (SocketException ex)
                    {
                        ctx.Error(ex.SocketErrorCode, "SocketError: " + ex.SocketErrorCode);
                    }
                    catch (Exception ex)
                    {
                        ctx.Error(ex);
                    }

                    if (success)
                    {
                        skts.Add(s);

                        if (maxConnections > 0 && (skts.Count == maxConnections))
                        {
                            done = true;
                        }
                        else
                        {
                            consecutiveFailures = 0;

                            if (betweenAttemptsSleepMs > 0)
                            {
                                Thread.Sleep(betweenAttemptsSleepMs);
                            }
                        }
                    }
                    else if (++consecutiveFailures >= maxConsecutiveFailures)
                    {
                        done = true;
                    }
                }
                catch (Exception ex)
                {
                    ctx.Error(ex);

                    if (++consecutiveFailures >= maxConsecutiveFailures)
                    {
                        done = true;
                    }
                }
            }

            ctx.Debug1("{0} connections before error", skts.Count);
        }

        public List<Socket> CommonConnect(List<string> args)
        {
            IPAddress addr = IPAddress.Parse(args[0]);
            int port = int.Parse(args[1]);
            int numClients = int.Parse(args[2]);
            int betweenAttemptsSleepMs = (args.Count > 3) ? int.Parse(args[3]) : 0;
            int postSleepMs = (args.Count > 4) ? int.Parse(args[4]) : 0;

            List<Socket> skts = new List<Socket>();
            OpenConnections(new IPEndPoint(addr, port), numClients, 1, betweenAttemptsSleepMs, out skts);

            if (postSleepMs > 0)
            {
                Thread.Sleep(postSleepMs);
            }

            return skts;
        }
                              
        //Test 1: Connect to a server, and close it using Socket.Close
        public void Test1(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);

            ctx.Debug1("Closing sockets with Close");
            foreach (Socket s in CommonConnect(args))
            {
                s.Close();
            }
        }

        //Test 2: Connect to a server, and close it using Socket.Close(10)
        public void Test2(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);

            ctx.Debug1("Closing sockets with Close(10)");
            foreach (Socket s in CommonConnect(args))
            {
                s.Close(10);
            }
        }

        //Test 3: Connect to a server, and close it using Socket.Disconnect
        public void Test3(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);

            ctx.Debug1("Closing sockets with Disconnect");
            foreach (Socket s in CommonConnect(args))
            {
                s.Disconnect(false);
            }
        }

        //Test 4: Connect to a server, and close it using Shutdown then Disconnect
        public void Test4(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);

            ctx.Debug1("Closing sockets with Shutdown & Disconnect");
            foreach (Socket s in CommonConnect(args))
            {
                s.Shutdown(SocketShutdown.Both);
                s.Disconnect(false);
            }
        }

        //Test 5: Connect to X servers, then randomly send data to them.
        public class Client
        {
            static public Random rng = new Random();

            private SocketHelper m_SktHelper = new SocketHelper();
            private int m_MinSendDelayMs;
            private int m_MaxSendDelayMs;
            private Timer m_SendTimer;

            public Client(Socket s, 
                          int receiveTimeoutMs,
                          int minSendDelayMs, 
                          int maxSendDelayMs)
            {
                m_SktHelper = new SocketHelper();
                m_SktHelper.Init(s,
                                 new WaitCallback(ReceivedMsgCallback),
                                 new WaitCallback(ConnectionClosedCallback),
                                 new RttpMsgReader(),
                                 new RttpMsgWriter(),
                                 (uint)receiveTimeoutMs);

                m_MinSendDelayMs = minSendDelayMs;
                m_MaxSendDelayMs = maxSendDelayMs;

                RestartSendTimer();
            }

            public void Shutdown()
            {
                if (m_SktHelper != null)
                {
                    m_SktHelper.Shutdown();
                }

                if (m_SendTimer != null)
                {
                    m_SendTimer.Dispose();
                    m_SendTimer = null;
                }
            }

            public bool SendRttp(string msg)
            {
                RttpMessage rttpMsg = new RttpMessage();
                rttpMsg.ResetForPush(Encoding.UTF8.GetBytes(msg));
                m_SktHelper.Send(rttpMsg);
                return true;
            }

            private void RestartSendTimer()
            {                
                int delayMs = rng.Next(m_MinSendDelayMs, m_MaxSendDelayMs);

                if (m_SendTimer == null)
                {
                    m_SendTimer = new Timer(new TimerCallback(SendCallback), null, delayMs, Timeout.Infinite);
                }
                else
                {
                    m_SendTimer.Change(delayMs, Timeout.Infinite);
                }
            }

            private void SendCallback(object state)
            {
                SendRttp("{text=\"This is my test text\"}");
                RestartSendTimer();
            }

            private void ReceivedMsgCallback(object msg)
            {
                //Trace.WriteLine("Received msg: " + ((RttpMessage)msg).GetRsonString());
            }

            private void ConnectionClosedCallback(object reason)
            {
                Trace.WriteLine("Client closed");
                Shutdown();
            }
        }
        
        public void Test5(int clientId, int iteration, List<string> args)
        {
            Context ctx = new Context("{0}:{1}", clientId, iteration);

            IPAddress addr = IPAddress.Parse(args[0]);
            int port = int.Parse(args[1]);
            int numClients = int.Parse(args[2]);
            int betweenAttemptsSleepMs = (args.Count > 3) ? int.Parse(args[3]) : 0;
            int postSleepMs = (args.Count > 4) ? int.Parse(args[4]) : 0;
            int recvTimeoutMs = (args.Count > 5) ? int.Parse(args[5]) : 0;
            int minSendDelayMs = (args.Count > 6) ? int.Parse(args[6]) : 0;
            int maxSendDelayMs = (args.Count > 7) ? int.Parse(args[7]) : 0;

            IPEndPoint ep = new IPEndPoint(addr, port);

            List<Client> clients = new List<Client>();
            for (int i = 0; i < numClients; i++ )
            {
                try
                {
                    Socket s = new Socket(AddressFamily.InterNetwork,
                                          SocketType.Stream,
                                          ProtocolType.Tcp);

                    bool success = false;

                    try
                    {
                        s.Connect(ep);
                        success = true;
                    }
                    catch (SocketException ex)
                    {
                        ctx.Error(ex.SocketErrorCode, "SocketError: " + ex.SocketErrorCode);
                    }
                    catch (Exception ex)
                    {
                        ctx.Error(ex);
                    }

                    if (success)
                    {
                        Client c = new Client(s, recvTimeoutMs, minSendDelayMs, maxSendDelayMs);
                        clients.Add(c);

                        if (betweenAttemptsSleepMs > 0)
                        {
                            Thread.Sleep(betweenAttemptsSleepMs);
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    ctx.Error(ex);
                    break;
                }
            }

            ctx.Debug1("{0} clients connected", clients.Count);

            Thread.Sleep(recvTimeoutMs);

            foreach (Client c in clients)
            {
                c.Shutdown();
            }
        }
    }
}
