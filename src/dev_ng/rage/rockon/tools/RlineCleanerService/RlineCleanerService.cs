﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Rline;

namespace RlineCleanerService
{
    public partial class RlineCleanerService : ServiceBase
    {
        private bool IsDone = false;
        private EventLogHelper m_Log = null;

        static void Main(string[] args)
        {
            RlineCleanerService service = new RlineCleanerService();

            //This bit of trickery allows the app to run as either a windows
            //service or a console application.  To run as a console app,
            //go to Properties and set Output Type to "Console Application".
            //You may then run it from Visual Studio, using Ctrl+C to exit
            //like any other console app.
            if (Environment.UserInteractive)
            {
                service.OnStart(args);

                Console.CancelKeyPress += new ConsoleCancelEventHandler(
                delegate(object sender, ConsoleCancelEventArgs e)
                {
                    service.OnStop();
                });
            }
            else
            {
                ServiceBase.Run(service);
            }
        }

        public RlineCleanerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            IsDone = false;

            Thread t = new Thread(ThreadMain);
            t.Start();
        }

        protected override void OnStop()
        {
            IsDone = true;
        }

        private void ThreadMain(object param)
        {
            m_Log = new EventLogHelper(this.ServiceName);

            //Start the main loop
            while (!IsDone)
            {
                const int ERROR_RECOVERY_DELAY_MS = 5 * 60000;

                int delayMs = ERROR_RECOVERY_DELAY_MS;
                Error err = null;

                try
                {
                    err = Cleaner.CleanEnvironment(out delayMs);
                }
                catch (Exception ex)
                {
                    err = new Error(ex);
                }

                if (err != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Clean failed:");
                    sb.AppendLine(err.ToString());
                    sb.AppendLine();
                    sb.AppendLine(string.Format("Sleeping for {0}ms to allow error recovery...", 
                                                ERROR_RECOVERY_DELAY_MS));

                    m_Log.WriteError(sb.ToString());
                    
                    Thread.Sleep(ERROR_RECOVERY_DELAY_MS);
                }
                else if (delayMs > 0)
                {
                    m_Log.Write(string.Format("Clean succeeded. Sleeping for {0}ms...", delayMs));
                    Thread.Sleep(delayMs);
                }
            }

            m_Log.Write("ThreadMain exited");
        }
    }
}
