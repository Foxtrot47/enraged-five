@echo off

rem Use this file to make a snapshot of a build, suitable for distribution
rem and debugging.

if "%1"=="" goto needname

set SNAPSHOTDIR=snapshot_%1
set SNAPSHOTDEBUGDIR=%SNAPSHOTDIR%\Debug
set SNAPSHOTRELEASEDIR=%SNAPSHOTDIR%\Release

echo Snapshotting to %SNAPSHOTDIR%...


rem --------------------------------------------------------
rem Make complete rar containing all code and binaries
rem --------------------------------------------------------

if exist %SNAPSHOTDIR% goto alreadyexists

mkdir %SNAPSHOTDIR%
if not exist %SNAPSHOTDIR% goto errormakingdir

if exist bin (
mkdir %SNAPSHOTDIR%\bin
)

if exist bin\Debug (
mkdir %SNAPSHOTDIR%\bin\Debug
copy bin\Debug\*.* %SNAPSHOTDIR%\bin\Debug
)

if exist bin\Release (
mkdir %SNAPSHOTDIR%\bin\Release
copy bin\Release\*.* %SNAPSHOTDIR%\bin\Release
)

copy *.config  %SNAPSHOTDIR%
copy *.cs  %SNAPSHOTDIR%
copy *.dat  %SNAPSHOTDIR%
copy *.dll  %SNAPSHOTDIR%
copy *.sln  %SNAPSHOTDIR%
copy *.csproj  %SNAPSHOTDIR%
copy *.xml  %SNAPSHOTDIR%

if exist Properties (
mkdir %SNAPSHOTDIR%\Properties
copy bin\Properties\*.* %SNAPSHOTDIR%\Properties
)

"C:\Program Files\WinRAR\rar.exe" a %SNAPSHOTDIR% %SNAPSHOTDIR%

rmdir /S /Q %SNAPSHOTDIR%


rem --------------------------------------------------------
rem Make .exe rar suitable for deployment
rem --------------------------------------------------------

if exist %SNAPSHOTDIR% goto alreadyexists

mkdir %SNAPSHOTDIR%
if not exist %SNAPSHOTDIR% goto errormakingdir

if exist bin\Debug (
mkdir %SNAPSHOTDEBUGDIR%
copy bin\Debug\*.exe %SNAPSHOTDEBUGDIR%
copy bin\Debug\*.dll %SNAPSHOTDEBUGDIR%
copy bin\Debug\*.config %SNAPSHOTDEBUGDIR%
rem copy *.config %SNAPSHOTDEBUGDIR%
rem copy *.dat %SNAPSHOTDEBUGDIR%
copy *.dll %SNAPSHOTDEBUGDIR%
rem copy *.xml %SNAPSHOTDEBUGDIR%
)

if exist bin\Release (
mkdir %SNAPSHOTRELEASEDIR%
copy bin\Release\*.exe %SNAPSHOTRELEASEDIR%
copy bin\Release\*.dll %SNAPSHOTRELEASEDIR%
copy bin\Release\*.config %SNAPSHOTRELEASEDIR%
rem copy *.config %SNAPSHOTRELEASEDIR%
rem copy *.dat %SNAPSHOTRELEASEDIR%
copy *.dll %SNAPSHOTRELEASEDIR%
rem copy *.xml %SNAPSHOTRELEASEDIR%
)

"C:\Program Files\WinRAR\rar.exe" a %SNAPSHOTDIR%_exe %SNAPSHOTDIR%

rmdir /S /Q %SNAPSHOTDIR%

goto done

:needname
echo Error: Missing snapshot name
goto done

:alreadyexists
echo Error: Directory %SNAPSHOTDIR% already exists
goto done

:errormakingdir
echo Error: Creating directory %SNAPSHOTDIR%
goto done

echo Snapshot succeeded.

:done
