﻿namespace RlineCleanerService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RlineCleanerServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.RlineCleanerServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // RlineCleanerServiceProcessInstaller
            // 
            this.RlineCleanerServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.RlineCleanerServiceProcessInstaller.Password = null;
            this.RlineCleanerServiceProcessInstaller.Username = null;
            // 
            // RlineCleanerServiceInstaller
            // 
            this.RlineCleanerServiceInstaller.Description = "Does periodic cleaning of DB tables used by rline.";
            this.RlineCleanerServiceInstaller.ServiceName = "RockOn RlineCleaner";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.RlineCleanerServiceProcessInstaller,
            this.RlineCleanerServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller RlineCleanerServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller RlineCleanerServiceInstaller;
    }
}