﻿namespace TestSg {
    partial class NkForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NkForm));
            this.ipTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rcvdTextBox = new System.Windows.Forms.TextBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.sndButton = new System.Windows.Forms.Button();
            this.sndComboBox = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadRequestsFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.sndTextBox = new System.Windows.Forms.TextBox();
            this.sentTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ipTextBox
            // 
            this.ipTextBox.Location = new System.Drawing.Point(56,32);
            this.ipTextBox.Name = "ipTextBox";
            this.ipTextBox.Size = new System.Drawing.Size(100,20);
            this.ipTextBox.TabIndex = 0;
            this.ipTextBox.TextChanged += new System.EventHandler(this.ipTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32,40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17,13);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP";
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(56,64);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(100,20);
            this.portTextBox.TabIndex = 2;
            this.portTextBox.TextChanged += new System.EventHandler(this.portTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24,72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26,13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port";
            // 
            // rcvdTextBox
            // 
            this.rcvdTextBox.Location = new System.Drawing.Point(56,336);
            this.rcvdTextBox.Multiline = true;
            this.rcvdTextBox.Name = "rcvdTextBox";
            this.rcvdTextBox.ReadOnly = true;
            this.rcvdTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.rcvdTextBox.Size = new System.Drawing.Size(392,64);
            this.rcvdTextBox.TabIndex = 5;
            this.rcvdTextBox.WordWrap = false;
            // 
            // connectButton
            // 
            this.connectButton.Enabled = false;
            this.connectButton.Location = new System.Drawing.Point(56,96);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75,23);
            this.connectButton.TabIndex = 6;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Enabled = false;
            this.closeButton.Location = new System.Drawing.Point(144,96);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75,23);
            this.closeButton.TabIndex = 7;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // sndButton
            // 
            this.sndButton.Enabled = false;
            this.sndButton.Location = new System.Drawing.Point(456,160);
            this.sndButton.Name = "sndButton";
            this.sndButton.Size = new System.Drawing.Size(75,23);
            this.sndButton.TabIndex = 8;
            this.sndButton.Text = "Send";
            this.sndButton.UseVisualStyleBackColor = true;
            this.sndButton.Click += new System.EventHandler(this.sndButton_Click);
            // 
            // sndComboBox
            // 
            this.sndComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sndComboBox.Enabled = false;
            this.sndComboBox.FormattingEnabled = true;
            this.sndComboBox.Location = new System.Drawing.Point(56,128);
            this.sndComboBox.Name = "sndComboBox";
            this.sndComboBox.Size = new System.Drawing.Size(392,21);
            this.sndComboBox.TabIndex = 10;
            this.sndComboBox.TextChanged += new System.EventHandler(this.sndComboBox_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0,0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(541,24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reloadRequestsFileToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40,20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // reloadRequestsFileToolStripMenuItem
            // 
            this.reloadRequestsFileToolStripMenuItem.Enabled = false;
            this.reloadRequestsFileToolStripMenuItem.Name = "reloadRequestsFileToolStripMenuItem";
            this.reloadRequestsFileToolStripMenuItem.Size = new System.Drawing.Size(208,22);
            this.reloadRequestsFileToolStripMenuItem.Text = "Reload Requests File";
            this.reloadRequestsFileToolStripMenuItem.Click += new System.EventHandler(this.reloadRequestsFileToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56,320);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53,13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Received";
            // 
            // sndTextBox
            // 
            this.sndTextBox.Location = new System.Drawing.Point(56,160);
            this.sndTextBox.Multiline = true;
            this.sndTextBox.Name = "sndTextBox";
            this.sndTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.sndTextBox.Size = new System.Drawing.Size(392,64);
            this.sndTextBox.TabIndex = 13;
            this.sndTextBox.TextChanged += new System.EventHandler(this.sndTextBox_TextChanged);
            // 
            // sentTextBox
            // 
            this.sentTextBox.Location = new System.Drawing.Point(56,248);
            this.sentTextBox.Multiline = true;
            this.sentTextBox.Name = "sentTextBox";
            this.sentTextBox.ReadOnly = true;
            this.sentTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.sentTextBox.Size = new System.Drawing.Size(392,64);
            this.sentTextBox.TabIndex = 15;
            this.sentTextBox.WordWrap = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56,232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29,13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Sent";
            // 
            // NkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F,13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541,421);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.sentTextBox);
            this.Controls.Add(this.sndTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.sndComboBox);
            this.Controls.Add(this.sndButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.rcvdTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.portTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ipTextBox);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NkForm";
            this.Text = "NetKitty";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ipTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox rcvdTextBox;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button sndButton;
        private System.Windows.Forms.ComboBox sndComboBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadRequestsFileToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox sndTextBox;
        private System.Windows.Forms.TextBox sentTextBox;
        private System.Windows.Forms.Label label4;
    }
}

