﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;

namespace TestSg
{
    public partial class NkForm:Form
    {
        Socket m_Skt;
        Socket sk = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
        string m_FileName;
        byte[] m_RcvBuf = new byte[8*1024];
        delegate void MyDelegate();
        readonly object m_SktLock = new object();

        int m_NumSnds = 1;
        int m_NumRcvs = 1;

        public NkForm()
        {
            InitializeComponent();

            string[] args = Environment.GetCommandLineArgs();

            for(int i = 1; i < args.Length - 1; ++i)
            {
                if("-ip" == args[i])
                {
                    ipTextBox.Text = args[i+1];
                    ++i;
                }
                else if("-port" == args[i])
                {
                    portTextBox.Text = args[i+1];
                    ++i;
                }
                else if("-file" == args[i])
                {
                    m_FileName = args[i+1];
                    this.PopulateRsonList(m_FileName);
                    ++i;
                }
            }

            reloadRequestsFileToolStripMenuItem.Enabled = (null != m_FileName);
        }

        private void PopulateRsonList(string filename)
        {
            StreamReader sr = null;

            try
            {
                sr = File.OpenText(filename);

                sndComboBox.Items.Clear();

                for(string s = sr.ReadLine(); null != s; s = sr.ReadLine())
                {
                    sndComboBox.Items.Add(s);
                }
            }
            catch (System.Exception e)
            {
                Trace.WriteLine(e);
            }
            finally
            {
                if(null != sr)
                {
                    sr.Close();
                }
            }
        }

        private void CloseSocket()
        {
            lock(m_SktLock)
            {
                if(null != m_Skt)
                {
                    m_Skt.Close();
                    m_Skt = null;
                    connectButton.Enabled = true;
                    closeButton.Enabled = false;
                    sndButton.Enabled = false;
                    sndComboBox.Enabled = false;
                }
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            lock(m_SktLock)
            {
                string s = "";
                bool closeTheSocket = false;

                try
                {
                    if(null != m_Skt)
                    {
                        int numBytes = m_Skt.EndReceive(ar);

                        if(numBytes > 0)
                        {
                            s = System.Text.Encoding.Default.GetString(m_RcvBuf, 0, numBytes);

                            m_Skt.BeginReceive(m_RcvBuf,
                                               0,
                                               m_RcvBuf.Length,
                                               0,
                                               new AsyncCallback(ReceiveCallback),
                                               null);
                        }
                        else
                        {
                            closeTheSocket = true;
                            s = "Disconnected";
                        }
                    }
                    else
                    {
                        s = "Closed";
                    }
                }
                catch (SocketException /*e*/)
                {
                    closeTheSocket = true;
                    s = "Disconnected";
                }

                MyDelegate d;

                if(closeTheSocket)
                {
                    d = delegate()
                    {
                        this.CloseSocket();
                    };

                    this.BeginInvoke(d);
                }

                d = delegate()
                {
                    this.AddRcvdText(s);
                };

                rcvdTextBox.BeginInvoke(d);
            }
        }

        private void AddSentText(string s)
        {
            string str = string.Format("{0}. {1}", m_NumSnds++, s);

            sentTextBox.Text = str + Environment.NewLine + sentTextBox.Text;
        }

        private void AddRcvdText(string s)
        {
            string str = string.Format("{0}. {1}", m_NumRcvs++, s);

            rcvdTextBox.Text = str + Environment.NewLine + rcvdTextBox.Text;
        }

        private void connectButton_Click(object sender,EventArgs e)
        {
            this.CloseSocket();

            rcvdTextBox.Text = "";
            sentTextBox.Text = "";
            m_NumRcvs = m_NumSnds = 1;

            m_Skt = new Socket(AddressFamily.InterNetwork,
                                SocketType.Stream,
                                ProtocolType.IP);

            bool success = false;

            try
            {
                int port;
                IPAddress ipAddr;
                if(int.TryParse(portTextBox.Text, out port)
                    && IPAddress.TryParse(ipTextBox.Text, out ipAddr))
                {
                    m_Skt.Connect(ipAddr, port);
                    closeButton.Enabled = true;
                    sndButton.Enabled = true;
                    sndComboBox.Enabled = true;

                    m_Skt.BeginReceive(m_RcvBuf,
                                       0,
                                       m_RcvBuf.Length,
                                       0,
                                       new AsyncCallback(ReceiveCallback),
                                       null);

                    connectButton.Enabled = false;

                    success = true;
                }
            }
            catch (SocketException /*e*/)
            {
            }

            if(!success)
            {
                m_Skt = null;
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.CloseSocket();
        }

        private void sndButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddSentText(sndTextBox.Text);

                m_Skt.Send(System.Text.Encoding.ASCII.GetBytes(sndTextBox.Text));

                /*if(!sndComboBox.Items.Contains(sndComboBox.Text))
                {
                    sndComboBox.Items.Add(sndComboBox.Text);
                }*/
            }
            catch (SocketException /*e*/)
            {
                this.CloseSocket();
            }
        }

        private void ipTextBox_TextChanged(object sender,EventArgs e)
        {
            IPAddress ipAddr;
            int port;
            connectButton.Enabled =
                IPAddress.TryParse(ipTextBox.Text, out ipAddr)
                && int.TryParse(portTextBox.Text, out port);
        }

        private void portTextBox_TextChanged(object sender,EventArgs e)
        {
            IPAddress ipAddr;
            int port;
            connectButton.Enabled =
                IPAddress.TryParse(ipTextBox.Text, out ipAddr)
                && int.TryParse(portTextBox.Text, out port);
        }

        private void reloadRequestsFileToolStripMenuItem_Click(object sender,EventArgs e)
        {
            this.PopulateRsonList(m_FileName);
        }

        private void sndTextBox_TextChanged(object sender,EventArgs e)
        {
            sndButton.Enabled = (null != m_Skt) && (sndTextBox.Text.Length > 0);
        }

        private void sndComboBox_TextChanged(object sender,EventArgs e)
        {
            sndTextBox.Text = sndComboBox.Text;
        }
    }
}
