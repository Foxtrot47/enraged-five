NetKitty - the replacement for netcat.

Usage:

NetKitty [-ip <ipaddr> -port <port>] [-file <filename>]

-ip <ipaddr>        IP address on which SG listens.
-port <port>        Port on which SG listens.
-file <filename>    File containing strings used to populate the "Send" list.
                    Each strin must be in RSON format and must be on a separate line.
                    Example:
                    {titleId=99901,xuid=0x1234567812345678,gamertag="Your Anus"}
                    {uri=http://rockon.rockstar.com/testsvc,m=foo,ns=bar}