﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.GlobalDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Sg
{
    //PURPOSE
    //  Core SG server logic.  Handles accepting, authenticating, and managing client 
    //  connections.  Message handling is handled in SgClient.
    public class SgServer
    {
        //Used to async remove clients from the server when they die.
        public delegate void RemoveClientDelegate(SgClient c);

        //Used to async remove a relay from the server when it dies.
        public delegate void RemoveRelayDelegate(SgRelay relay);

        public SgConfig Config { get; private set; }

        public enum EventId
        {
            USER_LOGIN,
            USER_LOGOUT
        }

        //Client (console) connections.
        private List<ListenSocketHelper> m_ClientListeners = new List<ListenSocketHelper>();
        private Pool<uint, SgClient> m_ClientPool = new Pool<uint, SgClient>();

        //Relays are connections from internal servers, used to push msgs to clients.
        private List<ListenSocketHelper> m_RelayListeners = new List<ListenSocketHelper>();
        private Pool<uint, SgRelay> m_RelayPool = new Pool<uint, SgRelay>();

        //Telnet clients, used for administration.
        private List<ListenSocketHelper> m_TelnetListeners = new List<ListenSocketHelper>();
        private Pool<uint, TelnetClient> m_TelnetPool = new Pool<uint, TelnetClient>();

        //Helper that processes authentication requests asynchronously.
        private SgAuth m_AuthHelper;

        //Map of logged in account numbers to client IDs.
        private Dictionary<Int64, uint> m_LoggedInAccounts = new Dictionary<long, uint>();

        private readonly object m_Lock = new object();

        //PURPOSE
        //  Initializes the SG from an XML config doc, and starts it listening for clients.
        public Error Init(XmlDocument doc)
        {
            Context ctx = SgContext.Create();

            try
            {
                Config = new SgConfig();
                m_AuthHelper = new SgAuth(this);

                //Process our config file.
                Error err = Config.Init(doc);
                if (null != err)
                {
                    this.Shutdown();
                    return ctx.Error(err);
                }

                //Console client listener
                if (Config.ClientConnections.MaxConcurrent > 0)
                {
                    //NOTE: Valid object IDs start at 1, not 0.
                    for (uint i = 1; i <= Config.ClientConnections.MaxConcurrent; ++i)
                    {
                        m_ClientPool.Grow(i, new SgClient(i));
                    }

                    foreach (SgConfigListenSocketInfo info in Config.ClientConnections.ListenSockets)
                    {
                        ListenSocketHelper lsh = new ListenSocketHelper();

                        if (null != (err = lsh.Init("Client" + m_ClientListeners.Count,
                            info.EndPoint,
                            uint.MaxValue,
                            AcceptClientCallback)))
                        {
                            this.Shutdown();
                            return ctx.Error(err);
                        }

                        m_ClientListeners.Add(lsh);
                    }
                }

                //Relay listener
                if (Config.RelayConnections.MaxConcurrent > 0)
                {
                    for (uint i = 1; i <= Config.RelayConnections.MaxConcurrent; ++i)
                    {
                        m_RelayPool.Grow(i, new SgRelay(i));
                    }

                    foreach (SgConfigListenSocketInfo info in Config.RelayConnections.ListenSockets)
                    {
                        ListenSocketHelper lsh = new ListenSocketHelper();

                        if (null != (err = lsh.Init("Relay" + m_RelayListeners.Count,
                            info.EndPoint,
                            uint.MaxValue,
                            AcceptRelayCallback)))
                        {
                            this.Shutdown();
                            return ctx.Error(err);
                        }

                        m_RelayListeners.Add(lsh);
                    }
                }

                //Telnet listener
                if (Config.TelnetConnections.MaxConcurrent > 0)
                {
                    for (uint i = 1; i <= Config.TelnetConnections.MaxConcurrent; ++i)
                    {
                        m_TelnetPool.Grow(i, new TelnetClient(i));
                    }

                    foreach (SgConfigListenSocketInfo info in Config.TelnetConnections.ListenSockets)
                    {
                        ListenSocketHelper lsh = new ListenSocketHelper();

                        if (null != (err = lsh.Init("Telnet" + m_TelnetListeners.Count,
                            info.EndPoint,
                            5,
                            AcceptTelnetCallback)))
                        {
                            this.Shutdown();
                            return ctx.Error(err);
                        }

                        m_TelnetListeners.Add(lsh);
                    }
                }

                return ctx.Success1();
            }
            catch (Exception ex)
            {
                this.Shutdown();
                return ctx.Error(ex);
            }
        }

        public void Shutdown()
        {
            lock(m_Lock)
            {
                foreach (ListenSocketHelper lsh in m_ClientListeners)
                {
                    lsh.Shutdown();
                }
                foreach (ListenSocketHelper lsh in m_RelayListeners)
                {
                    lsh.Shutdown();
                }
                foreach (ListenSocketHelper lsh in m_TelnetListeners)
                {
                    lsh.Shutdown();
                }

                m_ClientPool.FreeAll(delegate(uint id, SgClient c) { c.Shutdown(); });
                m_RelayPool.FreeAll(delegate(uint id, SgRelay c) { c.Shutdown(); });
                m_TelnetPool.FreeAll(delegate(uint id, TelnetClient c) { c.Shutdown(); });
            }
        }

        //PURPOSE
        //  Returns true if title is served by this SG.  The list of served titles
        //  is loaded from the config XML file.
        public bool IsTitleServed(int titleId)
        {
            return Config.Titles.ContainsKey(titleId);
        }

        //PURPOSE
        //  Returns false if communication with clients for the title must be encrypted.
        public bool IsTitlePlatformSecure(int titleId)
        {
            GlobalDb.TitleInfo titleInfo = GlobalDb.Title.GetCachedTitleInfo(titleId);
            GlobalDb.PlatformInfo platformInfo = GlobalDb.Platform.GetCachedPlatformInfo(titleInfo.PlatformId);

            return platformInfo.IsSecure;
        }

        //PURPOSE
        //  Pushes a message to a client.  A "pushed" message is an unsolicited
        //  message.  It is not triggered by a request.
        //PARAMS
        //  clientId    - Client id.
        //  accountId   - Associated account id.
        //  msg         - The message to send.
        //NOTES
        //  The client with the given id must be associated with the given
        //  account id.
        public Error PushMessageToClient(uint clientId, Int64 accountId, string msg)
        {
            Context ctx = SgContext.Create();

            lock(m_Lock)
            {
                Error err = null;
                SgClient c = m_ClientPool.GetActive(clientId);

                if (c == null)
                {
                    return ctx.Error(ErrorCode.InvalidArguments,
                                        "Invalid client id: {0}", clientId);
                }

                if(c.AccountId == accountId)
                {
                    if(null != (err = c.SendPushMessage(msg)))
                    {
                        return ctx.Error(err);
                    }
                }
                else
                {
                    return ctx.Error(ErrorCode.InvalidArguments,
                                        "Client:{0} does not have account id:{1}",
                                        clientId,
                                        accountId);
                }

                return ctx.Success2();
            }
        }

        //PURPOSE
        //  Returns true if account is currently logged into this SG.
        public bool IsLoggedInLocally(Int64 accountId, out uint clientId)
        {
            clientId = 0;
            return m_LoggedInAccounts.TryGetValue(accountId, out clientId);
        }

        //PURPOSE
        //  Dispatch the USER_LOGIN event.
        //PARAMS
        //  titleId     - Id of title for which user was logged in.
        //  accountId   - User's account id.
        public void DispatchUserLogin(int titleId,
                                      Int64 accountId,
                                      uint clientId)
        {
            Context ctx = SgContext.Create();

            //Note the account is logged in locally.  It is possible for them
            //to already be logged in (most likely because they died and reconnected
            //before the SG noticed they were gone).
            uint tempClientId;
            if (IsLoggedInLocally(accountId, out tempClientId))
            {
                ctx.Error(ErrorCode.InvalidState, "Account {0} already logged in (client {1}", accountId, tempClientId);
                return;
            }

            m_LoggedInAccounts.Add(accountId, clientId);

            //Notify listeners
            Dictionary<string, string> args = new Dictionary<string,string>();

            args.Add("titleId", titleId.ToString());
            args.Add("accountId", accountId.ToString());

            this.DispatchEvent(titleId, EventId.USER_LOGIN, args);

            ctx.Success2();
        }

        //PURPOSE
        //  Dispatch the USER_LOGOUT event.
        //PARAMS
        //  titleId     - Id of title for which user was logged in.
        //  accountId   - User's account id.
        public void DispatchUserLogout(int titleId,
                                       Int64 accountId)
        {
            Context ctx = SgContext.Create();

            //Note the account is no longer logged in locally
            uint clientId;
            if (IsLoggedInLocally(accountId, out clientId))
            {
                m_LoggedInAccounts.Remove(accountId);
            }

            //Notify listeners
            Dictionary<string, string> args = new Dictionary<string,string>();
            args.Add("titleId", titleId.ToString());
            args.Add("accountId", accountId.ToString());

            this.DispatchEvent(titleId, EventId.USER_LOGOUT, args);

            ctx.Success2();
        }

        //PURPOSE
        //  Returns the IP address the relay socket is bound to. 
        public IPEndPoint RelayEndpoint  
        { 
            get 
            {
                if (m_RelayListeners.Count > 0)
                {
                    return m_RelayListeners[0].EndPoint;
                }
                else
                {
                    return null;
                }
            } 
        }

        private bool AcceptClientCallback(Socket skt)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create();

                SgClient c;
                uint id;
                if (!m_ClientPool.Alloc(out id, out c))
                {
                    ctx.Warning("Denying connection from {0}; already have max clients ({1})",
                                skt.RemoteEndPoint, m_ClientPool.Capacity);
                    return false;
                }


                Error err = c.Init(this, skt, m_AuthHelper.AuthenticateClient, this.RemoveClient);
                if (err != null)
                {
                    ctx.Error(err);
                    return false;
                }

                ctx.Debug2("Added client {0} ({1} clients now)", c.Id, m_ClientPool.NumActive);
                return true;
            }
        }

        private bool AcceptRelayCallback(Socket skt)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create();

                SgRelay r;
                uint id;
                if (!m_RelayPool.Alloc(out id, out r))
                {
                    ctx.Warning("Denying connection from {0}; already have max relays ({1})",
                                skt.RemoteEndPoint, m_RelayPool.Capacity);
                    return false;
                }


                Error err = r.Init(this, skt, Config.RelayConnections.InactivityTimeoutMs, this.RemoveRelay);
                if (err != null)
                {
                    ctx.Error(err);
                    return false;
                }

                ctx.Debug2("Added relay {0} ({1} now)", r.Id, m_RelayPool.NumActive);
                return true;
            }
        }

        private bool AcceptTelnetCallback(Socket skt)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create();

                TelnetClient c;
                uint id;
                if (!m_TelnetPool.Alloc(out id, out c))
                {
                    ctx.Warning("Denying connection from {0}; already have max telent clients ({1})",
                                skt.RemoteEndPoint, m_TelnetPool.Capacity);
                    return false;
                }


                Error err = c.Init(skt,
                                   Config.TelnetConnections.InactivityTimeoutMs,
                                   TelnetMsgReceived,
                                   RemoveTelnetClient,
                                   this);
                if (err != null)
                {
                    ctx.Error(err);
                    return false;
                }

                ctx.Debug2("Added telnet client {0} ({1} now)", c.Id, m_TelnetPool.NumActive);
                return true;
            }
        }

        public Error KickClient(uint clientId)
        {
            Context ctx = SgContext.Create(clientId.ToString());

            try
            {
                SgClient c = m_ClientPool.GetActive(clientId);

                if (c == null)
                {
                    return ctx.Error(ErrorCode.DoesNotExist);
                }

                c.Die();

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        private void RemoveClient(SgClient c)
        {
            Context ctx = SgContext.Create(c.Id.ToString());

            try
            {
                lock (m_Lock)
                {
                    int titleId = c.TitleId;
                    Int64 accountId = c.AccountId;
                    Int64 sessionDurationMs = (Int64)DateTime.UtcNow.Subtract(c.LoginDateTime).TotalMilliseconds;

                    m_ClientPool.Free(c.Id);
                    c.Shutdown();
                    ctx.Debug2("{0} clients now", m_ClientPool.NumActive);

                    if(0 != titleId && 0 != accountId)
                    {
                        using(SqlConnectionHelper con = EnvDbHelper.Open())
                        {
                            if(null != Account.LogoutUser(con, 
                                                          titleId, 
                                                          accountId, 
                                                          RelayEndpoint,
                                                          sessionDurationMs))
                            {
                                ctx.Error("Failed to remove logged in user");
                            }

                            this.DispatchUserLogout(titleId, accountId);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
            	ctx.Error(ex);
            }
        }

        private void RemoveRelay(SgRelay relay)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create(relay.Id.ToString());

                m_ClientPool.Free(relay.Id);
                relay.Shutdown();
                ctx.Debug2("{0} relays now", m_RelayPool.NumActive);
            }
        }

        private void RemoveTelnetClient(TelnetClient c, SocketHelper.Reason reason)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create(c.Id.ToString());

                m_ClientPool.Free(c.Id);
                c.Shutdown();
                ctx.Debug2("{0} telnet clients now", m_TelnetPool.NumActive);
            }
        }

        //PURPOSE
        //  Called when we receive a telnet message.  The string will have the 
        //  terminating /r/n alreaddy stripped.
        private void TelnetMsgReceived(TelnetClient c, string s)
        {
            Context ctx = SgContext.Create("id=" + c.Id);
            ctx.Debug2("Received cmd: " + s);
        }

        //PURPOSE
        //  Dispatch an event.
        //PARAMS
        //  titleId     - Title id.
        //  eventId     - Event id.
        //  args        - Event args.
        private void DispatchEvent(int titleId,
                                   EventId eventId,
                                   Dictionary<string, string> args)
        {
            Context ctx = SgContext.Create("title={0}, event={1}", titleId, eventId);
            Error err;

            SgConfigTitleInfo titleInfo;
            
            if(Config.Titles.TryGetValue(titleId, out titleInfo))
            {
                foreach (KeyValuePair<string, SgConfigEventInfo> kvpEi in titleInfo.Events)
                {
                    if(kvpEi.Value.EventId == eventId)
                    {
                        string serviceUri;
                        string methodNamespace;
                        string methodName;
                        if(null != (err = Config.ResolveWebMethod(WebServiceVisibility.FROM_SERVER,
                                                                    titleId,
                                                                    kvpEi.Value.Handler,
                                                                    out serviceUri,
                                                                    out methodNamespace,
                                                                    out methodName)))
                        {
                            ctx.Error(err);
                        }

                        SoapEnvelopeBuilder sb = new SoapEnvelopeBuilder(SoapVersion.Soap1_2,
                                                                         methodName,
                                                                         methodNamespace);

                        foreach (KeyValuePair<string, string> kvp in args)
                        {
                            sb.AddParam(kvp.Key, kvp.Value);
                        }

                        SoapEnvelopeEx env = sb.GenerateEnvelopeEx();

                        const int TIMEOUT_MS = 30 * 1000;
                        uint requestId;
                        err = HttpWebRequestHelper.ExecuteSoapRequest(serviceUri,
                                                                      env,
                                                                      null,
                                                                      null,
                                                                      TIMEOUT_MS,
                                                                      this,
                                                                      out requestId,
                                                                      null);
                        if (err != null)
                        {
                            ctx.Error(err);
                        }
                    }
                }
            }

            ctx.Success2();
        }
    }
}
