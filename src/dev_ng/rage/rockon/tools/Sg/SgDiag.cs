using System;
using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Sg
{
    public class SgChannel
    {
        private static Channel m_Channel = new Channel("Sg");

        public static Channel Channel
        {
            get { return m_Channel; }
        }
    }

    public class SgContext
    {
        public static Context Create()
        {
            return new Context(SgChannel.Channel);
        }

        public static Context Create(string desc)
        {
            return new Context(SgChannel.Channel, desc);
        }

        public static Context Create(string format, params object[] args)
        {
            return new Context(SgChannel.Channel, format, args);
        }
    }
}
