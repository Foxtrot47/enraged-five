﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.GlobalDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Np;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Sg
{   
    //PURPOSE
    //  A pending authentication request from a client.
    public class AuthRequest
    {
        //Set by the client requesting authentication.
        public uint ClientId { get; private set; }
        public RttpMessage LoginMsg { get; private set; }
        public RsonReader LoginRson { get; private set; }

        //Set by the auth helper upon successful auth.
        public int TitleId = 0;
        public Int64 AccountId = 0;
        public byte[] SharedSecret;

        public AuthRequest(uint clientId, RttpMessage rttpMsg)
        {
            ClientId = clientId;
            LoginMsg = rttpMsg;
            LoginRson = new RsonReader();
            LoginRson.Init(rttpMsg.GetRsonString());
        }
    };

    //PURPOSE
    //  Processes client login messages. This unpacks the platform service
    //  credentials, authenticates them, and uses them to query-create
    //  a Rockon account ID that the client can use.
    public class SgAuth
    {
        //Used to async authenticate.
        public delegate Error AuthenticateClientDelegate(AuthRequest ar);

        private SgServer m_Server;
        
        //PURPOSE
        //  Intializes object.
        public SgAuth(SgServer server)
        {
            m_Server = server;
        }

        //PURPOSE
        //  Called by clients when their login message is received.
        //  This unpacks the login message, authenticates the credentials it 
        //  contains, and writes the title ID and account ID back to the
        //  auth request for processing by the client.
        public Error AuthenticateClient(AuthRequest ar)
        {
            Context ctx = SgContext.Create();
            Error err = null;

            try
            {
                DateTime dt = DateTime.Now;

                //Parse out the message version.  This is to allow the same SG to support
                //newer protocols without breaking login for older games.
                int version = 0;

                if (!ar.LoginRson.ReadInt("v", out version))
                {
                    ctx.Warning("Could not find version in login msg; assuming version 0");
                    version = (int)SgProtocol.Versions.Version_0;
                }

                //Authenticate their login.
                err = AuthenticateLoginMsg((SgProtocol.Versions)version,
                                           ar.LoginRson,
                                           out ar.TitleId,
                                           out ar.AccountId,
                                           out ar.SharedSecret);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Update the user's login status in the database.
                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    //Handle case of user already logged in (locally, or on another SG).
                    if (null != (err = HandleDuplicateLogin(con, ar.AccountId)))
                    {
                        //Note that we don't allow failure handling the dupe login to
                        //prevent an account from logging in.  This is intentional, so
                        //some backend problem that prevents handling of the dupe will
                        //not prevent a player from playing.
                        ctx.Error(err);
                    }

                    //Update the user's login info
                    if(null != (err = Account.LoginUser(con,
                                                        ar.TitleId,
                                                        ar.AccountId,
                                                        //ar.ClientId,
                                                        m_Server.RelayEndpoint)))
                    {
                        return ctx.Error(err);
                    }

                    m_Server.DispatchUserLogin(ar.TitleId, ar.AccountId, ar.ClientId);
                }                

                return ctx.Success2("Login took {0}ms", DateTime.Now.Subtract(dt).TotalMilliseconds);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Authenticates the RSON payload of a login msg.  This payload contains
        //  the title and account credentials of the user logging in.
        //  If successful, this returns the title ID, account ID, and shared secret
        //  to use when encrypting the client's crypto keys.
        public Error AuthenticateLoginMsg(SgProtocol.Versions version,
                                          RsonReader loginRson,
                                          out int titleId,
                                          out Int64 accountId,
                                          out byte[] sharedSecret)
        {
            titleId = 0;
            accountId = 0;
            sharedSecret = null;

            try
            {
                //Get the title ID (present in all msg versions) and verify the SG serves it.
                if (!loginRson.ReadInt("titleId", out titleId))
                {
                    return SgContext.Create().Error(ErrorCode.InvalidData, "Missing titleId, or is not a number");
                }

                if (!m_Server.IsTitleServed(titleId))
                {
                    return SgContext.Create().Error(ErrorCode.NotAllowed,
                                     "Title {0} is not served by this SG; login rejected",
                                     titleId);
                }

                //Authenticate the account credentials for this platform.
                GlobalDb.TitleInfo titleInfo = GlobalDb.Title.GetCachedTitleInfo(titleId);
                GlobalDb.PlatformId platformId = (GlobalDb.PlatformId)titleInfo.PlatformId;

                switch (platformId)
                {
                    case PlatformId.Xbox360:
                    case PlatformId.PC_GFWL:
                        return AuthenticateLoginMsgXbl(loginRson, out accountId);

                    case PlatformId.PSP:
                    case PlatformId.PS3:
                        return AuthenticateLoginMsgNp(loginRson,
                                                      titleId,
                                                      platformId,
                                                      out accountId,
                                                      out sharedSecret);

                    default:
                        return SgContext.Create().Error(ErrorCode.NotAllowed,
                                         "Title {0} is on platform {1}, but authentication not supported on that platform (why is the title served by the SG?)",
                                         titleId,
                                         platformId);
                }
            }
            catch (Exception ex)
            {
                return SgContext.Create().Error(ex);
            }
        }

        //PURPOSE
        //  Unpacks and authenticates XBL login credentials.
        private Error AuthenticateLoginMsgXbl(RsonReader rr,
                                              out Int64 accountId)
        {
            Context ctx = SgContext.Create();
            accountId = 0;

            try
            {
                UInt64 xuid;
                string gamertag;

                //Get the <xuid> element
                if (!rr.ReadUns64("xuid", out xuid))
                {
                    return ctx.Error(ErrorCode.InvalidData,
                                     "Login msg missing <xuid>");
                }

                if (xuid == 0)
                {
                    return ctx.Error(ErrorCode.InvalidData,
                                     "<xuid> value \"{0}\" somehow parsed to zero",
                                     xuid);
                }

                //Get the <gamertag> element
                if (!rr.ReadString("gamertag", out gamertag))
                {
                    return ctx.Error(ErrorCode.InvalidData,
                                     "Login msg missing <gamertag>");
                }

                //Get or create the RockOn account ID linked to our platform ID.
                Error err = AccountXblLink.QueryCreate((Int64)xuid, gamertag, out accountId);
                if (null != err)
                {
                    return ctx.Error(err);
                }

                return ctx.Success3("xuid={0}, gamertag={1}, acctId={2}",
                                    xuid, gamertag, accountId);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Unpacks and authenticates NP login credentials.
        private Error AuthenticateLoginMsgNp(RsonReader rr,
                                             int titleId,
                                             GlobalDb.PlatformId platformId,
                                             out Int64 accountId,
                                             out byte[] sharedSecret)
        {
            Context ctx = SgContext.Create();
            accountId = 0;
            sharedSecret = null;

            try
            {
                //Get the <ticket> element
                int ticketLen = rr.GetBinaryValueLength("ticket");
                if (ticketLen <= 0)
                {
                    return ctx.Error(ErrorCode.InvalidData, "Login msg has missing or invalid <ticket>");
                }

                byte[] ticket = new byte[ticketLen];
                if (!rr.ReadBinary("ticket", ticket, out ticketLen))
                {
                    return ctx.Error(ErrorCode.InvalidData, "Login msg has missing or invalid <ticket>");
                }

                int result;
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

                DateTime dt = DateTime.Now;

                //Do quick (non-verifying) ticket check
                byte majorVersion = 0;
                byte minorVersion = 0;
                Tcm.SceNetNpTicketSerialId serialId = new Tcm.SceNetNpTicketSerialId();
                Int64 issuedDate = 0;
                Int64 notOnOrAfterDate = 0;
                Tcm.SceNetNpServiceId serviceId = new Tcm.SceNetNpServiceId();
                uint entitlementCount = 0;
                uint roleCount = 0;

                if (Tcm.SCE_NET_NP_TCM_RET_OK !=
                    (result = Tcm.SceNetNpTcmGetInformationWithoutVerify(
                    ticket,
                    (uint)ticket.Length,
                    ref majorVersion,
                    ref minorVersion,
                    ref serialId,
                    ref issuedDate,
                    ref notOnOrAfterDate,
                    ref serviceId,
                    ref entitlementCount,
                    ref roleCount)))
                {
                    return ctx.Error(ErrorCode.InvalidData, "Ticket failed quick verify ({0})", result);
                }

                //Make sure the ticket isn't expired. NP times are expressed as milliseconds
                //since 1970/01/01 00:00:00 GMT.
                DateTime expirationDt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                expirationDt = expirationDt.AddMilliseconds(notOnOrAfterDate);
                if (expirationDt <= DateTime.UtcNow)
                {
                    return ctx.Error(ErrorCode.InvalidData, "Ticket expired on " + expirationDt);
                }

                //Verify the service ID is one this title uses.
                string serviceIdStr = Encoding.UTF8.GetString(serviceId.data).TrimEnd('\0');
                if (!m_Server.Config.IsServiceIdValid(titleId, serviceIdStr))
                {
                    return ctx.Error(ErrorCode.NotAllowed, "Service Id {0} is not in list for title {1}", serviceIdStr, titleId);
                }

                //Get the online ID from the TCM.
                Tcm.SceNetNpTcmNpIdResource resource = new Tcm.SceNetNpTcmNpIdResource();

                if (platformId == GlobalDb.PlatformId.PS3)
                {
                    resource.data = new byte[] { (byte)'p', (byte)'s', (byte)'3', (byte)'\0' };
                }
                else if (platformId == GlobalDb.PlatformId.PSP)
                {
                    resource.data = new byte[] { (byte)'p', (byte)'s', (byte)'p', (byte)'\0' };
                }
                else
                {
                    return ctx.Error(ErrorCode.NotAllowed, "Unsupported platform ({0})", platformId);
                }

                Tcm.SceNpId npId = new Tcm.SceNpId();

                if (Tcm.SCE_NET_NP_TCM_RET_OK != (result = Tcm.SceNetNpTcmGetNpId(
                    ticket,
                    (uint)ticket.Length,
                    ref resource,
                    ref npId)))
                {
                    return ctx.Error(ErrorCode.InvalidData, "Failed to read online ID from ticket ({0})", result);
                }

                //Verify the ticket signature with our prepared cipher. This also 
                //gets the cookie used as the shared secret.
                if (!m_Server.Config.VerifyTicketSignature(ticket, out sharedSecret))
                {
                    return ctx.Error(ErrorCode.InvalidData, "Ticket failed signature check");
                }

                double deltaMs = DateTime.Now.Subtract(dt).TotalMilliseconds;
                if (deltaMs > 0)
                {
                    ctx.Debug3("NP authentication took {0}ms", deltaMs);
                }

                //Everything passed, so get or create the RockOn account ID linked to our platform ID.
                string npOnlineId = Encoding.UTF8.GetString(npId.handle.data).TrimEnd('\0');
                Error err = AccountNpLink.QueryCreate(npOnlineId, out accountId);
                if (null != err)
                {
                    return ctx.Error(err);
                }

                return ctx.Success3("npOnlineId={0}, acctId={1}", npOnlineId, accountId);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Checks to see if account already logged in locally or remotely, and
        //  takes action as appropriate.
        private Error HandleDuplicateLogin(SqlConnectionHelper con, Int64 accountId)
        {
            Context ctx = SgContext.Create(accountId.ToString());
            Error err = null;

            try
            {
                //If logged in locally, most likely this is a client who died and
                //logged back in before the SG noticed.  We disconnect the old client.
                uint clientId;
                if (m_Server.IsLoggedInLocally(accountId, out clientId))
                {
                    ctx.Debug2("{0} was already logged in (possibly unnoticed linkdead client logging back in)", accountId);
                    m_Server.KickClient(clientId);
                    return ctx.Success3();
                }

                //Find out if the account is logged in, for any title.
                List<LoginStatusInfo> infos;
                if (null != (err = Account.QueryLoginStatus(con, 0, accountId, true, out infos)))
                {
                    return ctx.Error(err);
                }

                if (infos.Count == 0)
                {
                    return ctx.Success3();
                }

                LoginStatusInfo info = infos[0];

                //if ((info.SgEndPoint.Port == 0))
                //{
                //    return ctx.Error(ErrorCode.InvalidData, "Account {0} has invalid login status", accountId);
                //}

                //TODO: Connect to the SG (if not already connected)

                //Send the SG a message telling them to log the account out.
                RsonWriter msg = new RsonWriter(64);
                if (!(msg.Begin("LogoutAccount")
                     && msg.WriteInt64("AccountId", accountId)
                     && msg.End()))
                {
                    return ctx.Error(ErrorCode.UnknownError, "Failed to write LogoutAccount msg");
                }

                //if (null != (err = m_Server.SendToPeer(info.m_SgEndPoint, msg.ToString())))
                //{
                //    return ctx.Error(err);
                //}

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
