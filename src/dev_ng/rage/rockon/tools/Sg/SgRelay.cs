﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace Rockstar.Rockon.Sg
{
    //PURPOSE
    //  Used to relay messages from internal servers, through the SG,
    //  to a client.
    public class SgRelay
    {
        public uint Id {get; private set;}

        private readonly object m_Lock = new object();
        private SgServer m_Server;
        private SgServer.RemoveRelayDelegate m_RemoveDlgt;
        private SocketHelper m_Skt = new SocketHelper();

        public SgRelay(uint id)
        {
            Id = id;
        }

        //PURPOSE
        //  Initialize the relay
        //PARAMS
        //  server      - The SgServer that owns the relay.
        //  skt         - The recently accepted socket connection from a server.
        //  inactivityTimeoutMs - Number of milliseconds to remain open with no
        //                        activity.
        //  removeDlgt  - Callback used to remove the relay from the server's
        //                active collection of relays.
        public Error Init(SgServer server,
                            Socket skt,
                            uint inactivityTimeoutMs,
                            SgServer.RemoveRelayDelegate removeDlgt)
        {
            Context ctx = SgContext.Create();

            Error err;
            if (null != (err = m_Skt.Init(skt,
                              new WaitCallback(MsgReceived),
                              new WaitCallback(ConnectionClosed),
                              new RttpMsgReader(),
                              new RttpMsgWriter(),
                              inactivityTimeoutMs)))
            {
                return ctx.Error(err);
            }

            m_Server = server;
            m_RemoveDlgt = removeDlgt;

            return ctx.Success2();
        }

        public void Shutdown()
        {
            lock(m_Lock)
            {
                m_Skt.Shutdown();

                m_RemoveDlgt = null;
                m_Server = null;
            }
        }

        //PURPOSE
        //  Called back from SocketHelper when a fully formed RSON message
        //  is received.
        private void MsgReceived(object msg)
        {
            lock(m_Lock)
            {
                Context ctx = SgContext.Create();

                RsonReader rr = msg as RsonReader;

                if(null == rr)
                {
                    ctx.Error(ErrorCode.InvalidData, "Msg is not an RsonReader");
                    return;
                }

                //Parse out the client id, account id, and message content.
                //The client/account id will be used to identify the
                //client to which the message will be sent.
                uint clientId;
                UInt64 accountId;
                string clientMsg;

                if(!rr.ReadUns("clientId", out clientId))
                {
                    ctx.Error(ErrorCode.InvalidData, "Missing clientId, or is not a number");
                }
                else if(!rr.ReadUns64("accountId", out accountId))
                {
                    ctx.Error(ErrorCode.InvalidData, "Missing accountId, or is not a number");
                }
                else if(!rr.ReadString("msg", out clientMsg))
                {
                    ctx.Error(ErrorCode.InvalidData, "Missing message");
                }
                else
                {
                    ctx.Debug2("Received message for client {0}", clientId);
                    m_Server.PushMessageToClient(clientId,
                                                (Int64) accountId,
                                                clientMsg);
                }
            }
        }

        //PURPOSE
        //  Called back from SocketHelper when the socket is closed.
        private void ConnectionClosed(object reason)
        {
            lock(m_Lock)
            {
                //Async remove ourselves from the SG.
                m_RemoveDlgt.BeginInvoke(this, null, null);
            }
        }
    }

}
