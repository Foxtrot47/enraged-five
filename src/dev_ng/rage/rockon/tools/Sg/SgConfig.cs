﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Xml;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Np;

namespace Rockstar.Rockon.Sg
{
    class MethodFriendlyNameDelimiter
    {
        public static char[] Value = new char[] {'.'};
    }

    //Used to determine visibility of web services/methods.
    public class WebServiceVisibility
    {
        public const uint FROM_CLIENT   = 0x01;     //Visible from client only
        public const uint FROM_SERVER   = 0x02;     //Visible from server only
        public const uint FROM_ANY      = FROM_CLIENT | FROM_SERVER;

        public static uint Parse(string str)
        {
            uint val = 0;

            if("FROM_CLIENT" == str || str.ToLower() == "client")
            {
                val = FROM_CLIENT;
            }
            else if("FROM_SERVER" == str || str.ToLower() == "server")
            {
                val = FROM_SERVER;
            }
            else if("FROM_ANY" == str || str.ToLower() == "any")
            {
                val = FROM_ANY;
            }
            else
            {
                throw new Exception("Invalid web service visibility: " + str);
            }

            return val;
        }
    }

    public class SgConfigMethodInfo
    {
        public string Name { get; private set; }
        public string Uri { get; private set; }
        public string Namespace { get; private set; }
        public string Method { get; private set; }
        public uint Visibility { get; private set;}

        SgConfigMethodInfo(string name,
                            string uri,
                            string ns,
                            string method,
                            string visibility)
        {
            Name = name;
            Uri = uri;
            Namespace = ns;
            Method = method;
            if(null == visibility)
            {
                Visibility = WebServiceVisibility.FROM_ANY;
            }
            else
            {
                Visibility = WebServiceVisibility.Parse(visibility);
            }
        }

        public static Error ParseXml(XmlNode n, out SgConfigMethodInfo info)
        {
            Context ctx = SgContext.Create();
            info = null;

            try
            {
                XmlNode node;

                string name = n.Attributes.GetNamedItem("name").Value;

                node = n.Attributes.GetNamedItem("uri");
                string uri = (null != node) ? node.Value : null;
                node = n.Attributes.GetNamedItem("ns");
                string ns = (null != node) ? node.Value : null;
                node = n.Attributes.GetNamedItem("method");
                string method = (null != node) ? node.Value : null;
                node = n.Attributes.GetNamedItem("visibility");
                string visibility = (null != node) ? node.Value : null;

                info = new SgConfigMethodInfo(name, uri, ns, method, visibility);

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }

    public class SgConfigServiceInfo
    {
        public string Name {get; private set; }
        public string Uri {get; private set; }
        public string Namespace { get; private set; }
        public Dictionary<string, SgConfigMethodInfo> Methods { get; private set; }
        public HashSet<string> DisabledMethods { get; private set; }

        SgConfigServiceInfo(string name,
                            string uri,
                            string ns,
                            Dictionary<string, SgConfigMethodInfo> methods,
                            HashSet<string> disabledMethods)
        {
            Name = name;
            Uri = uri;
            Namespace = ns;
            Methods = methods;
            DisabledMethods = disabledMethods;
        }

        public static Error ParseXml(XmlNode n, out SgConfigServiceInfo info)
        {
            Context ctx = SgContext.Create();
            Error err;
            info = null;

            try
            {
                XmlNode node;

                string name = n.Attributes.GetNamedItem("name").Value;

                node = n.Attributes.GetNamedItem("uri");
                string uri = (null != node) ? node.Value : null;
                node = n.Attributes.GetNamedItem("ns");
                string ns = (null != node) ? node.Value : null;

                Dictionary<string, SgConfigMethodInfo> methods =
                    new Dictionary<string, SgConfigMethodInfo>();
                HashSet<string> disabledMethods = new HashSet<string>();

                foreach (XmlNode child in n.ChildNodes)
                {
                    switch (child.Name)
                    {
                        case "Method":
                            SgConfigMethodInfo wmInfo;
                            
                            if (null != (err = SgConfigMethodInfo.ParseXml(child, out wmInfo)))
                            {
                                return ctx.Error(err);
                            }
                            
                            if (methods.ContainsKey(wmInfo.Name))
                            {
                                methods.Remove(wmInfo.Name);
                            }

                            methods.Add(wmInfo.Name, wmInfo);
                            break;

                        case "DisableMethod":
                            disabledMethods.Add(child.Attributes.GetNamedItem("name").Value);
                            break;

                        default:
                            if (!(child is XmlComment))
                            {
                                return ctx.Error(ErrorCode.InvalidData, "Unhandled node type: {0}", child.Name);
                            }
                            break;
                    }
                }

                info = new SgConfigServiceInfo(name,
                                                uri,
                                                ns,
                                                methods,
                                                disabledMethods);

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }

    public class SgConfigEventInfo
    {
        public SgServer.EventId EventId {get; private set;}
        public string Handler {get; private set;}
        public string UniqueId {get; private set;}

        SgConfigEventInfo(string eventName, string handler)
        {
            EventId = (SgServer.EventId) Enum.Parse(typeof(SgServer.EventId), eventName);
            Handler = handler;
            UniqueId = eventName + "_" + handler;
        }

        public static Error ParseXml(XmlNode n, out SgConfigEventInfo info)
        {
            Context ctx = SgContext.Create();
            info = null;

            try
            {
                string eventName = n.Attributes.GetNamedItem("name").Value;
                string handler = n.Attributes.GetNamedItem("handler").Value;

                info = new SgConfigEventInfo(eventName, handler);

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }

    public class SgConfigTitleInfo
    {
        public int TitleId { get; private set; }
        public Dictionary<string, SgConfigServiceInfo> Services { get; private set; }
        public Dictionary<string, SgConfigEventInfo> Events { get; private set; }
        public HashSet<string> DisabledServices { get; private set; }
        public List<string> NpServiceIds { get; private set; }

        SgConfigTitleInfo(int titleId,
                            Dictionary<string, SgConfigServiceInfo> services,
                            Dictionary<string, SgConfigEventInfo> events,
                            HashSet<string> disabledServices,
                            List<string> npServiceIds)
        {
            TitleId = titleId;
            Services = services;
            Events = events;
            DisabledServices = disabledServices;
            NpServiceIds = npServiceIds;
        }

        public static Error ParseXml(XmlNode n, out SgConfigTitleInfo info)
        {
            Context ctx = SgContext.Create();
            Error err;
            info = null;

            try
            {
                int titleId = int.Parse(n.Attributes.GetNamedItem("id").Value);
                Dictionary<string, SgConfigServiceInfo> services =
                    new Dictionary<string,SgConfigServiceInfo>();
                Dictionary<string, SgConfigEventInfo> events =
                    new Dictionary<string,SgConfigEventInfo>();
                HashSet<string> disabledServices = new HashSet<string>();
                List<string> npServiceIds = new List<string>();

                foreach (XmlNode child in n.ChildNodes)
                {
                    switch (child.Name)
                    {
                        case "Service":
                            SgConfigServiceInfo svcInfo;

                            if (null != (err = SgConfigServiceInfo.ParseXml(child, out svcInfo)))
                            {
                                return ctx.Error(err);
                            }

                            if (services.ContainsKey(svcInfo.Name))
                            {
                                services.Remove(svcInfo.Name);
                            }

                            services.Add(svcInfo.Name, svcInfo);
                            break;

                        case "Event":
                            SgConfigEventInfo eventInfo;

                            if (null != (err = SgConfigEventInfo.ParseXml(child, out eventInfo)))
                            {
                                return ctx.Error(err);
                            }

                            if (events.ContainsKey(eventInfo.UniqueId))
                            {
                                return ctx.Error(string.Format("Duplicate event: {0}:{1}",
                                            eventInfo.EventId,
                                            eventInfo.Handler));
                            }

                            events.Add(eventInfo.UniqueId, eventInfo);
                            break;

                        case "DisableService":
                            disabledServices.Add(child.Attributes.GetNamedItem("name").Value);
                            break;

                        case "NpServiceId":
                            npServiceIds.Add(child.InnerText.ToUpper());
                            break;

                        default:
                            if (!(child is XmlComment))
                            {
                                return ctx.Error(ErrorCode.InvalidData, "Unhandled node type: {0}", child.Name);
                            }
                            break;
                    }
                }

                info = new SgConfigTitleInfo(titleId,
                                            services,
                                            events,
                                            disabledServices,
                                            npServiceIds);

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }

    public class SgConfigNpCipherInfo
    {
        public Tcm.SceNetNpCipherInformation RawCipher { get; private set; }
        public Tcm.SceNetNpPreparedCipherInformation PreparedCipher { get; private set; }

        SgConfigNpCipherInfo(Tcm.SceNetNpCipherInformation rawCipher,
                             Tcm.SceNetNpPreparedCipherInformation preparedCipher)
        {
            RawCipher = rawCipher;
            PreparedCipher = preparedCipher;
        }

        public static Error ParseXml(XmlNode n, out SgConfigNpCipherInfo info)
        {
            Context ctx = SgContext.Create();
            info = null;

            try
            {
                Tcm.SceNetNpCipherInformation rawCipher = new Tcm.SceNetNpCipherInformation();
                rawCipher.data = new byte[Tcm.SCE_NET_NP_CIPHER_INFORMATION_SIZE];

                Tcm.SceNetNpPreparedCipherInformation preparedCipher = new Tcm.SceNetNpPreparedCipherInformation();
                preparedCipher.data = new byte[Tcm.SCE_NET_NP_PREPARED_CIPHER_INFORMATION_SIZE];

                string cipherName = n.Attributes["name"].Value.ToString();

                List<CipherInfo> infos;
                Error err = Np.Cipher.QueryCipherInfo(cipherName, 1, out infos);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                if (infos.Count < 1)
                {
                    return ctx.Error(ErrorCode.DoesNotExist);
                }

                rawCipher.data = infos[0].CipherData;

                //Prepare the cipher and store it
                int result = Tcm.SceNetNpTcmPrepareCipherInformation(
                    Tcm.PassPhrase,
                    ref rawCipher,
                    ref preparedCipher);
                
                if (Tcm.SCE_NET_NP_TCM_RET_OK != result)
                {
                    return ctx.Error(ErrorCode.InvalidData, "Failed to prepare cipher {0}", cipherName);
                }

                info = new SgConfigNpCipherInfo(rawCipher, preparedCipher);

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }

    public class SgConfigListenSocketInfo
    {
        public IPEndPoint EndPoint { get; private set; }

        public SgConfigListenSocketInfo()
        {
        }

        public SgConfigListenSocketInfo(IPEndPoint ep)
        {
            EndPoint = ep;
        }

        public static Error ParseXml(XmlNode n, out SgConfigListenSocketInfo info)
        {
            Context ctx = SgContext.Create();
            info = null;

            try
            {
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(n.Attributes.GetNamedItem("addr").Value),
                                               int.Parse(n.Attributes.GetNamedItem("port").Value));

                info = new SgConfigListenSocketInfo(ep);

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }

    public class SgConfigConnectionsInfo
    {
        public uint MaxConcurrent { get; private set; }
        public uint InactivityTimeoutMs { get; private set; }
        public uint LoginTimeoutMs { get; private set; }
        public List<SgConfigListenSocketInfo> ListenSockets { get; private set; }

        public SgConfigConnectionsInfo()
        {
            ListenSockets = new List<SgConfigListenSocketInfo>();
        }

        public static Error ParseXml(XmlNode n, out SgConfigConnectionsInfo info)
        {
            Context ctx = SgContext.Create();
            Error err = null;
            info = null;

            try
            {
                info = new SgConfigConnectionsInfo();

                info.MaxConcurrent = uint.Parse(n.Attributes.GetNamedItem("maxConcurrent").Value);

                info.InactivityTimeoutMs = uint.Parse(n.Attributes.GetNamedItem("inactivityTimeoutSecs").Value) * 1000;
                
                XmlNode attr = n.Attributes.GetNamedItem("loginTimeoutSecs");
                info.LoginTimeoutMs = (attr == null) ? 0 : uint.Parse(attr.Value) * 1000;

                foreach (XmlNode child in n.ChildNodes)
                {
                    switch (child.Name)
                    {
                        case "ListenSocket":
                            SgConfigListenSocketInfo lsInfo;

                            if (null != (err = SgConfigListenSocketInfo.ParseXml(child, out lsInfo)))
                            {
                                return ctx.Error(err);
                            }

                            info.ListenSockets.Add(lsInfo);
                            break;

                        default:
                            if (!(child is XmlComment))
                            {
                                return ctx.Error(ErrorCode.InvalidData, "Unhandled node type: {0}", child.Name);
                            }
                            break;
                    }
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }

    public class SgConfigSoapCallInfo
    {
        public string Uri {get; private set;}
        public string Namespace {get; private set;}
        public string Method {get; private set;}

        public SgConfigSoapCallInfo(string uri,
                                    string methodNamespace,
                                    string methodName)
        {
            Uri = uri;
            Namespace = methodNamespace;
            Method = methodName;
        }
    }

    public class SgConfig
    {
        private const uint DEFAULT_LISTEN_BACKLOG                = 100;

        public SgConfigConnectionsInfo ClientConnections = new SgConfigConnectionsInfo();
        public SgConfigConnectionsInfo RelayConnections = new SgConfigConnectionsInfo();
        public SgConfigConnectionsInfo TelnetConnections = new SgConfigConnectionsInfo();

        public bool SendFullErrorInfo { get; private set; }

        public Dictionary<int, SgConfigTitleInfo> Titles {get; private set;}
        private Dictionary<string, SgConfigServiceInfo> m_Services = new Dictionary<string, SgConfigServiceInfo>();
        private HashSet<string> m_DisabledServices = new HashSet<string>();
        private Dictionary<int, Dictionary<string, SgConfigSoapCallInfo>> m_SoapCallCache = new Dictionary<int, Dictionary<string, SgConfigSoapCallInfo>>();
        private Tcm.SceNetNpPreparedCipherInformation[] m_PreparedCiphersArray;

        public SgConfig()
        {
            SendFullErrorInfo = false;
            Titles = new Dictionary<int, SgConfigTitleInfo>();
        }

        public Error Init(XmlDocument doc)
        {
            Context ctx = SgContext.Create();
            Error err;

            try
            {
                XmlElement el = doc.DocumentElement;
                List<Tcm.SceNetNpPreparedCipherInformation> preparedCiphers = new List<Tcm.SceNetNpPreparedCipherInformation>();

                foreach (XmlNode n in el.ChildNodes)
                {
                    switch (n.Name)
                    {
                        case "SendFullErrorInfo":
                            SendFullErrorInfo = bool.Parse(n.InnerText);
                            break;

                        case "ClientConnections":
                            if (null != (err = SgConfigConnectionsInfo.ParseXml(n, out ClientConnections)))
                            {
                                return ctx.Error(err);
                            }
                            break;

                        case "RelayConnections":
                            if (null != (err = SgConfigConnectionsInfo.ParseXml(n, out RelayConnections)))
                            {
                                return ctx.Error(err);
                            }
                            break;

                        case "TelnetConnections":
                            if (null != (err = SgConfigConnectionsInfo.ParseXml(n, out TelnetConnections)))
                            {
                                return ctx.Error(err);
                            }
                            break;

                        case "Service":
                            SgConfigServiceInfo svcInfo;
                            if (null != (err = SgConfigServiceInfo.ParseXml(n, out svcInfo)))
                            {
                                return ctx.Error(err);
                            }
                            if (m_Services.ContainsKey(svcInfo.Name))
                            {
                                m_Services.Remove(svcInfo.Name);
                            }
                            m_Services.Add(svcInfo.Name, svcInfo);
                            break;

                        case "DisableService":
                            string s = n.Attributes.GetNamedItem("name").Value;
                            if (!m_DisabledServices.Contains(s))
                            {
                                m_DisabledServices.Add(s);
                            }
                            break;

                        case "Title":
                            SgConfigTitleInfo titleInfo;
                            if (null != (err = SgConfigTitleInfo.ParseXml(n, out titleInfo)))
                            {
                                return ctx.Error(err);
                            }
                            if (Titles.ContainsKey(titleInfo.TitleId))
                            {
                                Titles.Remove(titleInfo.TitleId);
                            }
                            Titles.Add(titleInfo.TitleId, titleInfo);
                            break;

                        case "NpCipher":
                            SgConfigNpCipherInfo cipherInfo;
                            if (null != (err = SgConfigNpCipherInfo.ParseXml(n, out cipherInfo)))
                            {
                                return ctx.Error(err);
                            }
                            preparedCiphers.Add(cipherInfo.PreparedCipher);
                            break;

                        default:
                            if (!(n is XmlComment))
                            {
                                //Log error, but continue.  It may just be an obsolete param.
                                ctx.Error(ErrorCode.InvalidData, "Unhandled node type: {0}", n.Name);
                            }
                            break;
                    }
                }

                m_PreparedCiphersArray = preparedCiphers.ToArray();

                foreach (KeyValuePair<int,SgConfigTitleInfo> kvpTi in Titles)
                {
                    SgConfigTitleInfo titleInfo = kvpTi.Value;

                    foreach (KeyValuePair<string,SgConfigEventInfo> kvpEi in titleInfo.Events)
                    {
                        string serviceUri;
                        string methodNamespace;
                        string methodName;
                        if(null != (err = this.ResolveWebMethod(WebServiceVisibility.FROM_ANY,
                                                                kvpTi.Key,
                                                                kvpEi.Value.Handler,
                                                                out serviceUri,
                                                                out methodNamespace,
                                                                out methodName)))
                        {
                            return ctx.Error(err);
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public bool IsServiceIdValid(int titleId,
                                     string serviceId)
        {
            SgConfigTitleInfo info;
            if (Titles.TryGetValue(titleId, out info))
            {
                foreach (string s in info.NpServiceIds)
                {
                    if (0 == s.CompareTo(serviceId))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool VerifyTicketSignature(byte[] ticket, out byte[] sharedSecret)
        {
            sharedSecret = null;

            if (m_PreparedCiphersArray != null && m_PreparedCiphersArray.Length > 0)
            {
                byte majorVersion = 0;
                byte minorVersion = 0;
                uint issuerId = 0;
                Tcm.SceNetNpTicketSerialId serialId = new Tcm.SceNetNpTicketSerialId();
                Int64 issuedDate = 0;
                Int64 notOnOrAfterDate = 0;
                Tcm.SceNetNpServiceId serviceId = new Tcm.SceNetNpServiceId();
                Tcm.SceNetNpSubject subject = new Tcm.SceNetNpSubject();
                Tcm.SceNetNpEntitlement[] entitlements = new Tcm.SceNetNpEntitlement[256];
                uint entitlementCount = (uint)entitlements.Length;
                Tcm.SceNetNpRole[] roles = null;
                uint roleCount = 0;
                Tcm.SceNetNpCookie cookie = new Tcm.SceNetNpCookie();

                if (Tcm.SCE_NET_NP_TCM_RET_OK == Tcm.SceNetNpTcmGetInformation(
                    m_PreparedCiphersArray,
                    (uint)m_PreparedCiphersArray.Length,
                    ticket,
                    (uint)ticket.Length,
                    ref majorVersion,
                    ref minorVersion,
                    ref issuerId,
                    ref serialId,
                    ref issuedDate,
                    ref notOnOrAfterDate,
                    ref serviceId,
                    ref subject,
                    entitlements,
                    ref entitlementCount,
                    roles,
                    ref roleCount,
                    ref cookie
                    ))
                {
                    if (cookie.size > 0)
                    {
                        sharedSecret = new byte[cookie.size];
                        Buffer.BlockCopy(cookie.data, 0, sharedSecret, 0, (int)cookie.size);
                        return true;
                    }
                }
            }

            return false;
        }

        public Error ResolveWebMethod(uint visibility,
                                        int titleId,
                                        string fullFriendlyName,
                                        out string serviceUri,
                                        out string methodNamespace,
                                        out string methodName)
        {
            Context ctx = SgContext.Create();
            serviceUri = null;
            methodNamespace = null;
            methodName = null;

            try
            {
                //Check our cache of resolved calls first.
                Dictionary<string, SgConfigSoapCallInfo> callInfoDict;
                SgConfigSoapCallInfo callInfo;
                
                if (m_SoapCallCache.TryGetValue(titleId, out callInfoDict)
                   && callInfoDict.TryGetValue(fullFriendlyName, out callInfo))
                {
                    serviceUri = callInfo.Uri;
                    methodNamespace = callInfo.Namespace;
                    methodName = callInfo.Method;
                    
                    return ctx.Success3();
                }

                //Not in cache, so resolve the call components.
                const int SERVICE_INDEX = 0;
                const int METHOD_INDEX = 1;

                string[] callParts = fullFriendlyName.Split(MethodFriendlyNameDelimiter.Value);

                if(callParts == null || callParts.Length != 2)
                {
                    return ctx.Error(ErrorCode.InvalidArguments, 
                                     "Invalid method name: {0}", 
                                     fullFriendlyName);
                }

                string methodFriendlyName = callParts[METHOD_INDEX];
                methodName = methodFriendlyName;

                //Get the service info at global and title level, and check if disabled at either level.
                if (m_DisabledServices.Contains(callParts[SERVICE_INDEX]))
                {
                    return ctx.Error(ErrorCode.NotAllowed, 
                                     "Service {0} is disabled globally", 
                                     callParts[SERVICE_INDEX]);
                }
                
                SgConfigServiceInfo globalSvcInfo = null;
                m_Services.TryGetValue(callParts[SERVICE_INDEX], out globalSvcInfo);

                SgConfigTitleInfo titleInfo;
                SgConfigServiceInfo titleSvcInfo = null;
                if (Titles.TryGetValue(titleId, out titleInfo))
                {
                    if (titleInfo.DisabledServices.Contains(callParts[SERVICE_INDEX]))
                    {
                        return ctx.Error(ErrorCode.NotAllowed, 
                                         "Service {0} is disabled for title {1}", 
                                         callParts[SERVICE_INDEX], titleId);
                    }

                    titleInfo.Services.TryGetValue(callParts[SERVICE_INDEX], out titleSvcInfo);
                }

                //Process the service infos, allowing the title to override the global settings.
                List<SgConfigServiceInfo> svcInfos = new List<SgConfigServiceInfo>();
                svcInfos.Add(globalSvcInfo);
                svcInfos.Add(titleSvcInfo);

                foreach (SgConfigServiceInfo svcInfo in svcInfos)
                {
                    if (svcInfo == null)
                    {
                        continue;
                    }

                    if (svcInfo.DisabledMethods.Contains(methodFriendlyName))
                    {
                        return ctx.Error(ErrorCode.NotAllowed, "{0} is disabled for title {1}", methodFriendlyName, titleId);
                    }

                    if (svcInfo.Uri != null)
                    {
                        serviceUri = svcInfo.Uri;
                    }

                    if (svcInfo.Namespace != null)
                    {
                        methodNamespace = svcInfo.Namespace;
                    }

                    SgConfigMethodInfo mtdInfo;
                    if (svcInfo.Methods.TryGetValue(methodFriendlyName, out mtdInfo)
                        && 0 != (mtdInfo.Visibility & visibility))
                    {
                        if (mtdInfo.Method != null)
                        {
                            methodName = mtdInfo.Method;
                        }

                        if (mtdInfo.Uri != null)
                        {
                            serviceUri = mtdInfo.Uri;
                        }

                        if (mtdInfo.Namespace != null)
                        {
                            methodNamespace = mtdInfo.Namespace;
                        }
                    }
                }

                if (serviceUri == null
                    || methodNamespace == null
                    || methodName == null)
                {
                    return ctx.Error(ErrorCode.InvalidData,
                                     "Mapping for {0} is not fully defined for title {1}: uri={2}, ns={3}, method={4}",
                                     fullFriendlyName,
                                     titleId,
                                     (null == serviceUri) ? "null" : serviceUri,
                                     (null == methodNamespace) ? "null" : methodNamespace,
                                     (null == methodName) ? "null" : methodName);
                }

                //Cache the resolved call params
                callInfo = new SgConfigSoapCallInfo(serviceUri, methodNamespace, methodName);
                
                if (callInfoDict == null)
                {
                    callInfoDict = new Dictionary<string, SgConfigSoapCallInfo>();
                    m_SoapCallCache.Add(titleId, callInfoDict);
                }

                callInfoDict.Add(fullFriendlyName, callInfo);

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
