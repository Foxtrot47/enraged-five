﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Runtime.InteropServices;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace Rockstar.Rockon.Sg
{
    public partial class SgService : ServiceBase
    {
        protected Thread m_Thread;
        protected ManualResetEvent m_ShutdownEvent;
        
        private EventLogHelper m_Log;
        private static string[] m_RequiredDlls =
        {
            "libeay32.dll",
            "np_tcm.dll",
            "ssleay32.dll"
        };

        static void Main(string[] args)
        {
            Context ctx = SgContext.Create();

            SgService service = new SgService();

            //This bit of trickery allows the app to run as either a windows
            //service or a console application. 
            //NOTE: For this to work, the Output Type under Properties must
            //      be set to "Console Application".
            if (Environment.UserInteractive)
            {
                ctx.Debug2("SG is running as an interactive console app");
                ctx.Debug2("Working directory is " + Path.GetDirectoryName(Application.ExecutablePath));

                CheckRequiredDlls();

                //Run as a console app, exiting upon Ctrl+C.
                service.OnStart(args);

                Console.CancelKeyPress += new ConsoleCancelEventHandler(
                delegate(object sender, ConsoleCancelEventArgs e)
                {
                    service.OnStop();
                });            
            }
            else
            {
                ctx.Debug2("SG is running as a Windows service");

                //Run as a windows service.  In this case, we need to set our working directory.
                string dir = ConfigurationManager.AppSettings["SgServiceWorkingDirectory"];

                if (dir != null && dir.Length > 0 && !dir.Contains(':'))
                {
                    dir = Path.GetDirectoryName(Application.ExecutablePath) 
                          + "\\"
                          + dir;
                }
                
                if(dir == null || dir.Length == 0)
                {
                    dir = Path.GetDirectoryName(Application.ExecutablePath);
                }

                Directory.SetCurrentDirectory(dir);

                ctx.Debug2("Working directory is " + dir);

                CheckRequiredDlls();

                //Run the service.
                ServiceBase.Run(service);
            }
        }

        public SgService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);

            m_ShutdownEvent = new ManualResetEvent(false);

			m_Thread = new Thread(ThreadMain);
			m_Thread.Start();
        }

        protected override void OnStop()
        {
            m_ShutdownEvent.Set();

            //Wait for the thread to stop giving it 10 seconds
            m_Thread.Join(10000);

            base.OnStop();
        }

        private static void CheckRequiredDlls()
        {
            Context ctx = SgContext.Create();

            ctx.Debug2("Checking for required DLLs...");

            foreach (string s in m_RequiredDlls)
            {
                FileInfo fi = new FileInfo(s);
                if (fi.Exists)
                {
                    ctx.Debug2("Found " + s);
                }
                else
                {
                    ctx.Warning("Could not find " + s);
                }
            }
        }

        private Error LoadConfig(string filename,
                                 out XmlDocument doc)
        {
            Context ctx = SgContext.Create("filename={0}", filename);
            doc = null;

            try
            {
                FileInfo inf_info = new FileInfo(filename);

                if (!inf_info.Exists)
                {
                    return ctx.Error(ErrorCode.DoesNotExist);
                }

                doc = new XmlDocument();
                string uri = "file://" + inf_info.FullName;
                doc.Load(uri);

                return ctx.Success1();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        private void ServiceWrite(string format, params object[] args)
        {
            if (!Environment.UserInteractive)
            {
                if (m_Log == null)
                {
                    m_Log = new EventLogHelper(this.ServiceName);
                }
                m_Log.Write(string.Format(format, args));
            }
        }

        private void ServiceWriteError(string format, params object[] args)
        {
            if (!Environment.UserInteractive)
            {
                if (m_Log == null)
                {
                    m_Log = new EventLogHelper(this.ServiceName);
                }
                m_Log.Write(string.Format(format, args));
            }
        }

        virtual protected void ThreadMain()
        {
            Context ctx = SgContext.Create();
            Error err = null;

            try
            {
                if (!Environment.UserInteractive)
                {
                    //NOTE: We delay a bit, because it is possible we may crash 
                    //      before something in the underlying service has 
                    //      occurred, leaving us in a limbo state where the 
                    //      thread has exited but the service keeps running.
                    Thread.Sleep(5000);
                }

                const string KEY_SGCONFIGFILENAME = "SgConfigFilename";

                string filename = ConfigurationManager.AppSettings[KEY_SGCONFIGFILENAME];
                if ((filename == null) || (filename.Length == 0))
                {
                    err = ctx.Error(ErrorCode.DoesNotExist,
                                    string.Format("Could not find config key [{0}]", KEY_SGCONFIGFILENAME));
                }
                else
                {
                    XmlDocument doc;
                    if (null == (err = LoadConfig(filename, out doc)))
                    {
                        ServiceWrite("Successfully loaded config file {0}", filename);
                    }
                    else
                    {
                        err = ctx.Error(err);
                    }

                    if (err == null)
                    {
                        SgServer sg = new SgServer();
                        if (null == (err = sg.Init(doc)))
                        {
                            ServiceWrite("SG initialized. Running until service is stopped.");
                            m_ShutdownEvent.WaitOne();
                        }
                        else
                        {
                            err = ctx.Error(err);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err = ctx.Error(ex);
            }

            if (!Environment.UserInteractive)
            {
                if (err != null)
                {
                    ServiceWriteError(err.ToString());
                    throw (new Exception(err.ToString()));
                }
            }
        }
    }
}
