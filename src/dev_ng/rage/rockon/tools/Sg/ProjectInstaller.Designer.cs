﻿namespace Rockstar.Rockon.Sg
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SgServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.SgServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // SgServiceProcessInstaller
            // 
            this.SgServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.SgServiceProcessInstaller.Password = null;
            this.SgServiceProcessInstaller.Username = null;
            // 
            // SgServiceInstaller
            // 
            this.SgServiceInstaller.Description = "The Security Gateway sits between client consoles and the RockOn backend.";
            this.SgServiceInstaller.ServiceName = "RockOn SG 200907281531";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.SgServiceProcessInstaller,
            this.SgServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller SgServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller SgServiceInstaller;
    }
}