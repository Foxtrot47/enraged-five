using System;
using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Sg
{
    public class SgProtocol
    {
        public enum Versions
        {
            Version_0 = 0,
            Current = Version_0
        };

        public enum MsgTypes
        {
            Rpc = 0,
            Telemetry
        };
    }
}
