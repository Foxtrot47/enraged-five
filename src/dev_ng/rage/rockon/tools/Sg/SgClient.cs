﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Xml;

//using Ionic.Zlib;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;

namespace Rockstar.Rockon.Sg
{
    //PURPOSE
    //  Manages an individual client connection to the SG.
    public class SgClient
    {
        public uint Id { get; private set; }
        public DateTime LoginDateTime { get; private set; }

        private SgServer m_Server = null;

        private readonly object m_Lock = new object();
        private SocketHelper m_Skt = new SocketHelper();

        private int m_TitleId = 0;      //Rockon title ID, given to us by SG when authenticated
        private Int64 m_AccountId = 0;  //Rockon account ID, given to us by SG when authenticated

        //Used to async remove ourselves from the server when we die.
        private SgServer.RemoveClientDelegate m_RemoveDlgt;

        //Used to async authenticate.
        private SgAuth.AuthenticateClientDelegate m_AuthDlgt;

        private AuthRequest m_AuthRequest;    //Pending auth request.

        private enum State
        {
            Dead,               //Unusable until Init()
            WaitingForLogin,    //Expects a single login message
            WaitingForAuth,     //Waiting for server to call our Authenticated() method
            Active              //Can send and receive freely
        };
        private State m_State = State.Dead;

        private Queue<RttpMessage> m_RpcQueue = new Queue<RttpMessage>();

        private Rc4Context m_Encryptor;
        private Rc4Context m_Decryptor;

        public SgClient(uint id)
        {
            this.Id = id;
        }

        //PURPOSE
        //  Creates an SgContext that 

        //PURPOSE
        //  Initializes the client with a socket connected to the
        //  remote host.
        public Error Init(SgServer server,
                        Socket s,
                        SgAuth.AuthenticateClientDelegate authDlgt,
                        SgServer.RemoveClientDelegate removeDlgt)
        {
            Debug.Assert(null == m_AuthRequest);

            lock (m_Lock)
            {
                Context ctx = SgContext.Create();

                m_Server = server;

                m_AuthDlgt = authDlgt;
                m_RemoveDlgt = removeDlgt;

                Error err;
                if (null != (err = m_Skt.Init(s,
                                new WaitCallback(ClientMsgReceived),
                                new WaitCallback(ClientConnectionClosed),
                                new RttpMsgReader(),
                                new RttpMsgWriter(),
                                server.Config.ClientConnections.LoginTimeoutMs)))
                {
                    return ctx.Error(err);
                }

                m_State = State.WaitingForLogin;

                return ctx.Success2();
            }
        }

        //PURPOSE
        //  Shuts down the client.
        public void Shutdown()
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create("id={0}", this.Id);

                if(!this.IsDead())
                {
                    this.Die();
                }

                m_Skt.Shutdown();

                m_AuthRequest = null;
                m_AuthDlgt = null;
                m_RemoveDlgt = null;

                m_Server = null;
                m_TitleId = 0;
                m_AccountId = 0;

                ctx.Success2();
            }
        }

        static void CopyStream(System.IO.Stream src, System.IO.Stream dest)
        {
            byte[] buffer = new byte[1024];
            int len = src.Read(buffer, 0, buffer.Length);
            while (len > 0)
            {
                dest.Write(buffer, 0, len);
                len = src.Read(buffer, 0, buffer.Length);
            }
            dest.Flush();
        }
        
        private void PrepareMessageContent(byte[] content)
        {
            lock (m_Lock)
            {
                //See if the content can be compressed
                //System.IO.MemoryStream ms = new System.IO.MemoryStream(content.Length/2);                
                //ZlibStream zOut = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.Default);
                //zOut.Write(content, 0, content.Length);
                //zOut.Flush();

                //if (ms.Length < content.Length)
                //{
                //    content = ms.ToArray();
                //    isCompressed = true;
                //}

                //zOut.Close();

                //Encrypt the compressed content.
                if (m_Encryptor != null && m_Encryptor.IsValid)
                {
                    m_Encryptor.Encrypt(content);
                }
            }
        }

        public Error SendPushMessage(string msg)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create();

                try
                {
                    if (this.IsDead())
                    {
                        return ctx.Error(ErrorCode.InvalidState, "Attempt to send to a dead client");
                    }

                    byte[] content = Encoding.UTF8.GetBytes(msg);
                    PrepareMessageContent(content);

                    RttpMessage rttpMsg = new RttpMessage();
                    rttpMsg.ResetForPush(content);
                    m_Skt.Send(rttpMsg);

                    return ctx.Success3();
                }
                catch (Exception ex)
                {
                    return ctx.Error(ex);
                }
            }
        }

        public Error SendTransactionMessage(string msg, UInt16 txId)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create();

                try
                {
                    if (this.IsDead())
                    {
                        return ctx.Error(ErrorCode.InvalidState, "Attempt to send to a dead client");
                    }

                    byte[] content = Encoding.UTF8.GetBytes(msg);                   
                    PrepareMessageContent(content);

                    RttpMessage rttpMsg = new RttpMessage();
                    rttpMsg.ResetForTransaction(content, txId);
                    m_Skt.Send(rttpMsg);

                    return ctx.Success3();
                }
                catch(Exception ex)
                {
                    return ctx.Error(ex);
                }
            }
        }

        public int TitleId
        {
            get{return m_TitleId;}
        }

        public Int64 AccountId
        {
            get{return m_AccountId;}
        }

        //PURPOSE
        //  Return true if the client is dead.
        private bool IsDead()
        {
            return (m_State == State.Dead);
        }

        //PURPOSE
        //  Kills a client, rendering is useless.
        public void Die()
        {
            Context ctx = SgContext.Create("id={0}", this.Id);

            lock (m_Lock)
            {
                if (IsDead())
                {
                    ctx.Warning("Already dead");
                    return;
                }

                m_State = State.Dead;

                //Async remove ourselves from the server.
                m_RemoveDlgt.BeginInvoke(this, null, null);

                ctx.Success2();
            }
        }

        //PURPOSE
        //  Callback for when the client socket is closed,
        private void ClientConnectionClosed(object reason)
        {
            Context ctx = SgContext.Create("id={0}", this.Id);
            ctx.Debug2("Reason={0}", (SocketHelper.Reason)reason);
            Die();
        }

        //PURPOSE
        //  Called when a message is received on the client's socket.  
        //  How it is processed depends on the client's current state.
        private void ClientMsgReceived(object msg)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create("id={0}", this.Id);

                RttpMessage rttpMsg = msg as RttpMessage;

                if(null == rttpMsg)
                {
                    ctx.Error(ErrorCode.InvalidData, "Msg is not an RsonReader");
                    return;
                }

                if (m_Decryptor != null && m_Decryptor.IsValid)
                {
                    m_Decryptor.Encrypt(rttpMsg.Content);
                }

                //if (rttpMsg.Header.IsCompressed)
                //{
                //    System.IO.MemoryStream ms = new System.IO.MemoryStream(rttpMsg.Content.Length);
                //    ZlibStream zOut = new ZlibStream(ms, CompressionMode.Decompress);
                //    zOut.Write(rttpMsg.Content, 0, rttpMsg.Content.Length);
                //    zOut.Flush();
                //    zOut.Close();

                //    if (rttpMsg.IsTransaction)
                //    {
                //        rttpMsg.ResetForTransaction(ms.ToArray(), rttpMsg.Header.TxId, rttpMsg.Header.IsEncrypted, false);
                //    }
                //    else if (rttpMsg.IsPush)
                //    {
                //        rttpMsg.ResetForPush(ms.ToArray(), rttpMsg.Header.IsEncrypted, false);
                //    }
                //    else
                //    {
                //        throw new Exception("Compressed msg that's not tx or push; should not be possible");
                //    }
                //}

                switch (m_State)
                {
                    case State.Dead:
                        ctx.Error(ErrorCode.InvalidState, "Client is dead");
                        break;

                    case State.WaitingForLogin:
                        ctx.Debug2("Received what should be a login msg");
                        if (null != HandleLoginMsg(rttpMsg))
                        {
                            ctx.Error(ErrorCode.UnknownError, "Failed to handle login msg, killing client");
                            Die();
                        }
                        break;

                    case State.WaitingForAuth:
                        //The client should never send us anything while waiting for auth.
                        ctx.Error(ErrorCode.InvalidState, "Unexpected msg while WaitingForAuth, killing client");
                        Die();
                        break;

                    case State.Active:
                        RsonReader rr = new RsonReader();
                        if (!rr.Init(rttpMsg.GetRsonString()))
                        {
                            ctx.Error("Error initializing RSON reader");
                            return;
                        }

                        int msgType;
                        if (!rr.ReadInt("t", out msgType))
                        {
                            ctx.Warning("Failed to read msg type; assuming RPC");
                            msgType = (int)SgProtocol.MsgTypes.Rpc;
                        }

                        switch ((SgProtocol.MsgTypes)msgType)
                        {
                            case SgProtocol.MsgTypes.Rpc:
                                if (null != HandleRpcMsg(rttpMsg))
                                {
                                    ctx.Error(ErrorCode.UnknownError, "Failed to handle RPC request msg, killing client");
                                    Die();
                                }
                                break;

                            default:
                                ctx.Error(ErrorCode.InvalidData, "Unhandled msg type {0}", msgType);
                                break;
                        }
                        break;

                    default:
                        ctx.Error(ErrorCode.InvalidState, "Unhandled state ({0})", m_State);
                        break;
                }
            }
        }

        //PURPOSE
        //  Handles message received during login state.  Unpacks the message
        //  and queues us for authentication with the SG.
        private Error HandleLoginMsg(RttpMessage rttpMsg)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create("{0}", this.Id);

                if (m_State != State.WaitingForLogin)
                {
                    return ctx.Error(ErrorCode.InvalidState, "Invalid state ({0})", m_State);
                }

                Debug.Assert(null == m_AuthRequest);

                //We got a login request, so we can go to the normal timeout now; if it was 
                //a fake login they'll get disconnected when auth fails.
                m_Skt.SetReceiveTimeoutMs(m_Server.Config.ClientConnections.InactivityTimeoutMs);
                
                m_State = State.WaitingForAuth;

                m_AuthRequest = new AuthRequest(this.Id, rttpMsg);

                m_AuthDlgt.BeginInvoke(m_AuthRequest, new AsyncCallback(this.AuthCallback), m_AuthRequest);

                return ctx.Success2();
            }
        }

        private void AuthCallback(IAsyncResult iar)
        {
            //TODO: Return to this, see if it can be cleaned up
            lock (m_Lock)
            {
                Context ctx = SgContext.Create("id={0}", this.Id);

                if (m_State != State.WaitingForAuth)
                {
                    ctx.Error(ErrorCode.InvalidState, "Invalid state ({0})", m_State);
                    return;
                }

                AuthRequest ar = (AuthRequest) iar.AsyncState;

                //Make sure the auth request we got is the one we're waiting for.
                //The following situation could arise:
                //1.  Client A connects, requests auth.
                //2.  Client A disconnects, SgClient instance is recycled.
                //3.  Client B connects and recycled SgClient instance is reused.
                //4.  Client B requests auth.
                //5.  Client A's auth request comes back but is given to B.
                if(null == m_AuthRequest
                    || ar.GetHashCode() != m_AuthRequest.GetHashCode())
                {
                    ctx.Error(ErrorCode.InvalidState, "The pending auth request does not match that returned - could be due to timeout");
                    return;
                }

                m_AuthRequest = null;

                Error err = m_AuthDlgt.EndInvoke(iar);
                m_AuthDlgt = null;

                if (null != err)
                {
                    ctx.Error(ErrorCode.UnknownError, "Auth failed, killing client");
                    Die();
                }
                else
                {
                    //Record the title and account IDs determined during login.
                    m_TitleId = ar.TitleId;
                    m_AccountId = ar.AccountId;

                    ctx.Success2("Client is active now (titleId={0}, accountId={1})", 
                                 m_TitleId, m_AccountId);

                    m_State = State.Active;
                    LoginDateTime = DateTime.UtcNow;

                    //Notify client of success, so they can start sending requests.
                    RsonWriter rw = new RsonWriter(512);

                    if (m_Server.IsTitlePlatformSecure(m_TitleId))
                    {
                        if (!rw.Begin()
                            || !rw.WriteBool("Success", true)
                            || !rw.End())
                        {
                            ctx.Error(ErrorCode.InvalidData, "Failed to write RSON login success");
                            return;
                        }

                        err = SendTransactionMessage(rw.ToString(), ar.LoginMsg.TxId);
                    }
                    else
                    {
                        //Generate random crypto keys for the client
                        Rc4Context encryptor = Rc4Context.Generate(32);
                        Rc4Context decryptor = Rc4Context.Generate(32);

                        byte[] decryptKey = encryptor.Key;
                        byte[] encryptKey = decryptor.Key;

                        //Use the client's shared secret to encrypt the crypto keys.
                        if (ar.SharedSecret != null)
                        {
                            Rc4Context rc = new Rc4Context();
                            
                            rc.Reset(ar.SharedSecret);
                            rc.Encrypt(decryptKey);

                            rc.Reset(ar.SharedSecret);
                            rc.Encrypt(encryptKey);
                        }
                        
                        //Write the crypto keys in the login message
                        if (!rw.Begin()
                            || !rw.WriteBool("Success", true)
                            || !rw.Begin("Result")
                            || !rw.WriteBinary("DecryptKey", decryptKey)
                            || !rw.WriteBinary("EncryptKey", encryptKey)
                            || !rw.End()
                            || !rw.End())
                        {
                            ctx.Error(ErrorCode.InvalidData, "Failed to write RSON login success");
                            return;
                        }

                        err = SendTransactionMessage(rw.ToString(), ar.LoginMsg.TxId);

                        //IMPORTANT: We do not set the client's encryptor until after we've
                        //           called SendTransactionMessage --the reply must be in the clear.
                        m_Encryptor = encryptor;
                        m_Decryptor = decryptor;
                    }

                    if(null == err)
                    {
                        ctx.Debug2("Sent login success reply to client");
                    }
                    else
                    {
                        ctx.Error(err);
                    }
                }
            }
        }

        //PURPOSE
        //  Handles RPC request message received during active state,
        //  queuing it for execution.
        private Error HandleRpcMsg(RttpMessage rttpRequest)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create("id={0}, txId={1}", this.Id, rttpRequest.TxId);

                if (m_State != State.Active)
                {
                    return ctx.Error(ErrorCode.UnknownError, "Invalid state ({0})", m_State);
                }

                //Queue the RPC call for execution after the current call,
                //or execute immediately if nothing is currently executing.
                if (m_RpcQueue.Count == 0)
                {
                    ExecuteRpc(rttpRequest);
                }
                else
                {
                    m_RpcQueue.Enqueue(rttpRequest);
                }
                
                return ctx.Success2();
            }
        }

        //PURPOSE
        //  Begins execution of an RPC.  This composes an HTTP request and begins sending it 
        //  to its destination.
        private void ExecuteRpc(RttpMessage rttpRequest)
        {
            Context ctx = SgContext.Create("id={0}", this.Id);
            Error err = null;

            lock (m_Lock)
            {
                try
                {
                    //Get the soap params and create the envelope.
                    string serviceUri;
                    string methodName;
                    string methodNamespace;

                    RsonReader rr = new RsonReader();
                    if (!rr.Init(rttpRequest.GetRsonString()))
                    {
                        ctx.Error("Error initializing RSON reader");
                        return;
                    }

                    SoapEnvelopeEx env;
                    if (null != (err = ParseSoapInfo(rr,
                                                     out serviceUri,
                                                     out methodName,
                                                     out methodNamespace,
                                                     out env)))
                    {
                        ctx.Error(err);
                        return;
                    }

                    //Start the request.
                    const int TIMEOUT_MS = 30 * 1000;
                    uint requestId;
                    err = HttpWebRequestHelper.ExecuteSoapRequest(serviceUri,
                                                                  env,
                                                                  ExecuteRpcSuccessCallback,
                                                                  ExecuteRpcErrorCallback,
                                                                  TIMEOUT_MS,
                                                                  rttpRequest.TxId,
                                                                  out requestId,
                                                                  null);
                    if (err != null)
                    {
                        ctx.Error(err);
                    }
                }
                catch (Exception ex)
                {
                    err = ctx.Error(ex);
                    SendErrorReply(err, rttpRequest.TxId);
                    return;
                }
            }
        }

        //PURPOSE
        //  Parse SOAP call details from an RSON message, and adds additional
        //  parameters (titleId and accountId) that are sent with every RPC call.
        private Error ParseSoapInfo(RsonReader rr,
                                    out string serviceUri,
                                    out string methodName,
                                    out string methodNamespace,
                                    out SoapEnvelopeEx env)
        {
            Context ctx = SgContext.Create("id={0}", this.Id);
            Error err = null;

            serviceUri = null;
            methodName = null;
            methodNamespace = null;
            env = null;

            try
            {
                string methodFriendlyName;
                if (!rr.ReadString("m", out methodFriendlyName))
                {
                    return ctx.Error(ErrorCode.InvalidData, "Missing or malformed {{m}} (method friendly name)");
                }

                if (null != (err = m_Server.Config.ResolveWebMethod(WebServiceVisibility.FROM_CLIENT,
                                                                      m_TitleId,
                                                                      methodFriendlyName,
                                                                      out serviceUri,
                                                                      out methodNamespace,
                                                                      out methodName)))
                {
                    return ctx.Error(err);
                }

                if (serviceUri == null || methodName == null || methodNamespace == null)
                {
                    return ctx.Error(ErrorCode.InvalidArguments, "RSON is missing required parameter");
                }

                //Construct a SOAP envelope.
                //NOTE: Soap1_1 is broken for some reason right now, so don't use it
                SoapEnvelopeBuilder envBuilder = new SoapEnvelopeBuilder(SoapVersion.Soap1_2,
                                                                         methodName,
                                                                         methodNamespace);

                //The first two arguments of all RPCs made by clients must be the 
                //titleId and accountId, both for routing and for tracking. Add them first.
                envBuilder.AddParam("titleId", m_TitleId.ToString());
                envBuilder.AddParam("accountId", m_AccountId.ToString());

                //Look for the (optional) args object in the RSON.  Each child of this 
                //object is an additional argument to the call.
                RsonReader rrArgs = null;
                if (rr.GetMember("args", ref rrArgs))
                {
                    RsonReader rrArg = null;
                    string name;
                    string value;

                    for (bool ok = rrArgs.GetFirstMember(ref rrArg); ok; ok = rrArg.GetNextSibling(ref rrArg))
                    {
                        if (!rrArg.GetName(out name) || !rrArg.AsString(out value))
                        {
                            return ctx.Error(ErrorCode.InvalidData, "Could not read argument");
                        }

                        envBuilder.AddParam(name, value);
                    }
                }

                env = envBuilder.GenerateEnvelopeEx();

                return ctx.Success3("uri=\"{0}\", m=\"{1}\", ns=\"{2}\", numArgs={3}",
                                    serviceUri,
                                    methodName,
                                    methodNamespace,
                                    envBuilder.NumParams);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        private void ExecuteRpcErrorCallback(Error err, object userdata)
        {
            ushort transactionId = (ushort)userdata;
            SendErrorReply(err, transactionId);
        }

        private void ExecuteRpcSuccessCallback(string content, object userdata)
        {
            lock (m_Lock)
            {
                ushort transactionId = (ushort)userdata;

                Context ctx = SgContext.Create("id={0}, txId={1}", this.Id, transactionId);

                try
                {
                    SendXmlReply(content, transactionId);

                    //Start the next RPC call we have queued (if any).
                    if (!IsDead() && (m_RpcQueue.Count > 0))
                    {
                        RttpMessage rttpRequest = m_RpcQueue.Dequeue();
                        ExecuteRpc(rttpRequest);
                    }
                }
                catch (Exception ex)
                {
                    Error err = ctx.Error(ex);
                    SendErrorReply(err, transactionId);
                }
            }
        }

        private void SendXmlReply(string xmlContent, UInt16 txId)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create("id={0}, txId={1}", this.Id, txId);
                Error err = null;

                try
                {
                    //All our replies have the same general format.  There is a containing
                    //element with these children:
                    //
                    //  <Success>true|false</Success>
                    //      This is the only required element.  It dictates which of the
                    //      other two elements we look for.
                    //
                    //  <Error>                     An Error *may* be present if Success is false.
                    //      <Code>string</Code>     If Error is present, this is required.
                    //      <Msg>msg</Msg>          The descriptive msg is optional.
                    //  </Error>
                    //
                    //  <Result>                    A result *may* be present if Success is true.
                    //      {Result-specific data}
                    //  </Result>

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xmlContent);

                    //Look for the <Success> element.
                    XmlNode successNode = XmlHelper.FindXmlNode("Success", doc.DocumentElement);
                    if (successNode == null)
                    {
                        err = ctx.Error(ErrorCode.InvalidData, "Could not find <Success> tag in response");
                        SendErrorReply(err, txId);
                        return;
                    }

                    bool success = false;
                    
                    if(!bool.TryParse(successNode.InnerText, out success))
                    {
                        success = false;
                    }

                    RsonWriter rw = new RsonWriter(xmlContent.Length);
                    rw.SetFlags(RsonWriter.FLAG_DEFAULT_FLAGS & ~RsonWriter.FLAG_QUOTED_STRINGS);

                    if (!rw.Begin() || !rw.WriteBool("Success", success))
                    {
                        err = ctx.Error(ErrorCode.InvalidData, "Failed to write RSON reply header");
                        SendErrorReply(err, txId);
                        return;
                    }

                    XmlNode parentNode = successNode.ParentNode;

                    if (!success)
                    {
                        XmlNode errorNode = XmlHelper.FindXmlNode("Error", parentNode);

                        if (errorNode != null && !XmlHelper.WriteXmlToRson(errorNode, rw))
                        {
                            err = ctx.Error(ErrorCode.InvalidData, "Failed to write <Error> element");
                            SendErrorReply(err, txId);
                            return;
                        }
                    }
                    else
                    {
                        XmlNode resultNode = XmlHelper.FindXmlNode("Result", parentNode);

                        for(; null != resultNode; resultNode = resultNode.NextSibling)
                        {
                            if("Result" != resultNode.Name)
                            {
                                continue;
                            }

                            if(!XmlHelper.WriteXmlToRson(resultNode, rw))
                            {
                                err = ctx.Error(ErrorCode.InvalidData, "Failed to write <Result> element");
                                SendErrorReply(err, txId);
                                return;
                            }
                        }
                    }

                    if (!rw.End())
                    {
                        err = ctx.Error(ErrorCode.UnknownError, "Failed to end RSON writer");
                        SendErrorReply(err, txId);
                        return;
                    }

                    if(null != (err = SendTransactionMessage(rw.ToString(), txId)))
                    {
                        ctx.Error(err);
                    }
                }
                catch (Exception ex)
                {
                    err = ctx.Error(ex);
                    SendErrorReply(err, txId);
                }
            }
        }

        //PURPOSE
        //  Sends an error reply to the client.  This is used to generate replies for 
        //  times when we can't get a reply from the web server to forward.
        private Error SendErrorReply(Error err, UInt16 txId)
        {
            lock (m_Lock)
            {
                Context ctx = SgContext.Create("id={0}, txId={1}", this.Id, txId);

                try
                {
                    //FIXME: Is there an easy way to get an accurate size needed for the message?
                    RsonWriter rw = new RsonWriter(100 + err.ToString().Length);
                    rw.SetFlags(RsonWriter.FLAG_DEFAULT_FLAGS & ~RsonWriter.FLAG_QUOTED_STRINGS);

                    if (!rw.Begin())
                    {
                        Die();
                        return ctx.Error(ErrorCode.UnknownError, "Failed to Begin() RSON writer");
                    }

                    if (!rw.WriteBool("Success", false))
                    {
                        Die();
                        return ctx.Error(ErrorCode.UnknownError, "Failed to write <Success> to RSON");
                    }

                    if (m_Server.Config.SendFullErrorInfo)
                    {
                        if (!rw.Begin("Error") ||
                            !rw.WriteString("Code", err.Code) ||
                            !((err.Msg == null) || rw.WriteString("Msg", err.Msg)) ||
                            !rw.End())
                        {
                            Die();
                            return ctx.Error(ErrorCode.UnknownError, "Failed to write <Error> to RSON");
                        }
                    }

                    if (!rw.End())
                    {
                        Die();
                        return ctx.Error(ErrorCode.UnknownError, "Failed to End() RSON writer");
                    }

                    if (null != (err = SendTransactionMessage(rw.ToString(), txId)))
                    {
                        return ctx.Error(err);
                    }

                    return ctx.Success3();
                }
                catch (Exception ex)
                {
                    Die();
                    return ctx.Error(ex);
                }
            }
        }
    }
}
