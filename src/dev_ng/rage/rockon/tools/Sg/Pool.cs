﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.GlobalDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Sg
{
    //PURPOSE
    //  Pool of items.  Users must initialize the pool by Grow()-ing it to the
    //  desired size.  These instances can then be fetched with Alloc(), and 
    //  returned to the pool with Free().
    public class Pool<T_KEY, T_VALUE> where T_VALUE : class
    {
        public delegate void FreeAllDelegate(T_KEY key,  T_VALUE value);

        public int Capacity { get { return m_Active.Count + m_Free.Count; } }
        public int NumActive { get { return m_Active.Count; } }
        public int NumFree { get { return m_Free.Count; } }

        private Dictionary<T_KEY, T_VALUE> m_All = new Dictionary<T_KEY, T_VALUE>();
        private List<T_KEY> m_Active = new List<T_KEY>();
        private List<T_KEY> m_Free = new List<T_KEY>();

        private readonly object m_Lock = new object();

        public void Grow(T_KEY key, T_VALUE value)
        {
            lock (m_Lock)
            {
                Debug.Assert(!m_All.ContainsKey(key) && !m_All.ContainsValue(value));

                m_All.Add(key, value);
                m_Free.Add(key);
            }
        }

        public bool Alloc(out T_KEY key, out T_VALUE value)
        {
            key = default(T_KEY);
            value = default(T_VALUE);

            lock (m_Lock)
            {
                if (m_Free.Count > 0)
                {
                    key = m_Free[0];
                    m_Free.RemoveAt(0);
                    m_Active.Add(key);
                    value = m_All[key];
                    return true;
                }
            }
            return false;
        }

        public void Free(T_KEY key)
        {
            lock (m_Lock)
            {
                Debug.Assert(m_All.ContainsKey(key) && m_Active.Contains(key));

                m_Active.Remove(key);
                m_Free.Add(key);
            }
        }

        public void FreeAll(FreeAllDelegate del)
        {
            lock (m_Lock)
            {
                while (m_Active.Count > 0)
                {
                    T_KEY key = m_Active[0];
                    m_Active.RemoveAt(0);
                    m_Free.Add(key);

                    T_VALUE value = m_All[key];

                    if (del != null)
                    {
                        del(key, value);
                    }
                }
            }
        }

        public T_VALUE GetActive(T_KEY key)
        {
            T_VALUE value = default(T_VALUE);
            m_All.TryGetValue(key, out value);
            return value;
        }
    }
}
