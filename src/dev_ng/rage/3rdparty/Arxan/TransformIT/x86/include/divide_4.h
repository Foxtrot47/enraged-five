/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/









#ifndef __DIVIDE_4_H__
#define __DIVIDE_4_H__

#include "wbrsa_config.h"

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size top_level_divide_4(const mword *a, _size aSize, const mword *b, _size bSize, mword *quotientOut, mword *remainderOut, _size *rSize);


#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size top_level_remainder_4(const mword *a, _size aSize, const mword *b, _size bSize, mword *remainderOut);


#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size top_level_quotient_4(const mword *a, _size aSize, const mword *b, _size bSize, mword *quotientOut);


#ifdef __INLINE_OPS_
#include "divide_4.c"
#endif


#endif


