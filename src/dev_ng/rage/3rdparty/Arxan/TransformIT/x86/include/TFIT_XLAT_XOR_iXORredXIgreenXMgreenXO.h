/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: XLAT/XOR Instance=iXORredXIgreenXMgreenXO
 */

#ifndef __TFIT_XLAT_XOR_iXORredXIgreenXMgreenXO_H__
#define __TFIT_XLAT_XOR_iXORredXIgreenXMgreenXO_H__

#include "TFIT.h"

#include "TFIT_apixor_iXORredXIgreenXMgreenXO.h"
#include "TFIT_xlatdef_iXORredXIgreenXMgreenXO.h"
#include "TFIT_xlat_iXORredXIgreenXMgreenXO.h"

#define TFIT_init_xlat_xor_iXORredXIgreenXMgreenXO(ctx) \
    wbxlat_init_xor(ctx, WBXLAT_ENCODE, WBXLAT_OBFUSCATED, WBXLAT_OBFUSCATED, WBXLAT_OBFUSCATED, &TFIT_apixor_iXORredXIgreenXMgreenXO, &TFIT_xlat_iXORredXIgreenXMgreenXO)
#define TFIT_update_xlat_xor_iXORredXIgreenXMgreenXO wbxlat_update
#define TFIT_final_xlat_xor_iXORredXIgreenXMgreenXO wbxlat_final

#define TFIT_xlat_xor_iXORredXIgreenXMgreenXO(src, len, mask, dest) \
    TFIT_opxor_iXORredXIgreenXMgreenXO((const unsigned char*)&TFIT_xlat_iXORredXIgreenXMgreenXO, src, len, mask, dest)

#endif /* __TFIT_XLAT_XOR_iXORredXIgreenXMgreenXO_H__ */
