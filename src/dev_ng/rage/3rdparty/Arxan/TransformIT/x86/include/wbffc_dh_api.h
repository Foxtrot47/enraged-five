/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */



#ifndef __WBFFCDH_H
   #define __WBFFCDH_H

   #include <arxstdint.h>
   #include "wbbbi_api.h"
   #include "std_bigint.h"
   #define MAX_FFCDH_EPHEM_PUB_KEYSIZE 256
   #define MAX_FFCDH_RETRIES 128

   #ifdef __cplusplus
      extern "C"
      {
   #endif

   static const unsigned char VERSION[4] = {0, 2, 0, 0};

   typedef enum wbffc_status_t
   {
      WBFFC_OK = 0,
      WBFFC_INVALID_CONTEXT_OR_PARAM = 1,
      WBFFC_OUTPUT_BUFFER_TOO_SMALL = 2,
      WBFFC_KEY_NOT_READY = 3,
      WBFFC_COMPUTATION_FAILURE = 4,
      WBFFC_PRNG_FAILURE = 5,
      WBFFC_NOMEM = 6,
      WBFFC_INVALID_INSTANCE = 7,
   } wbffc_status;

   typedef enum ffc_dh_mode_t
   {
      WBFFC_DH_HYBRID1,
      WBFFC_DH_EPHEM,
      WBFFC_DH_HYBRIDONEFLOW_INITIATOR,
      WBFFC_DH_HYBRIDONEFLOW_RESPONDER,
      WBFFC_DH_ONEFLOW_INITIATOR,
      WBFFC_DH_ONEFLOW_RESPONDER,
      WBFFC_DH_STATIC
   } ffc_dh_mode;

   typedef unsigned int (*wbffc_dh_get_rand_word)(void);
   typedef struct wbffc_dh_ctx_t
   {
      const uint8_t *cfg;
      wbffc_dh_get_rand_word get_rand_word;
      uint8_t flags;
      uint32_t n;
      uint32_t m;
      uint32_t s;
      boxy_context_t bc;
      std_bigint_t p;
      modulus_t pm;
      uint8_t ephemeral[MAX_FFCDH_EPHEM_PUB_KEYSIZE * 2];
   } wbffc_dh_ctx;


   


   int wbffc_dh_init(wbffc_dh_ctx * const ctx,
                     wbffc_dh_get_rand_word get_rand_word,
                     const uint8_t * const wbffc_cfg);

   


   int wbffc_dh_cleanup(wbffc_dh_ctx * const ctx);


   

   int wbffc_dh_get_static_public_key(const wbffc_dh_ctx * const ctx,
                                      uint8_t * const output,
                                      unsigned int output_len,
                                      unsigned int * const bytes_written);


   

   int wbffc_dh_get_ephemeral_public_key(const wbffc_dh_ctx * const ctx,
                                         uint8_t * const output,
                                         unsigned int output_len,
                                         unsigned int * const bytes_written);


   

   int wbffc_dh_generate_key(wbffc_dh_ctx * const ctx);


   

   int wbffc_dh_compute_secret(const wbffc_dh_ctx * const ctx,
                               ffc_dh_mode mode,
                               const uint8_t * const static_public_key,
                               const uint8_t * const ephemeral_public_key,
                               unsigned int public_key_len,
                               uint8_t * const output,
                               unsigned int output_len,
                               unsigned int * const bytes_written);

   

   const uint8_t * wbffc_dh_get_oxdid(const wbffc_dh_ctx * const ctx);

   #ifdef __cplusplus
      }
   #endif

#endif

