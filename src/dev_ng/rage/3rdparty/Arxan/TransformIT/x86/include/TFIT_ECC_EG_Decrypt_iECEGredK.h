/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: ECC/EG/Decrypt Instance=iECEGredK
 */

#ifndef __TFIT_ECC_EG_Decrypt_iECEGredK_H__
#define __TFIT_ECC_EG_Decrypt_iECEGredK_H__

#include "TFIT.h"

#include "wbecc_eg_api.h"
#include "TFIT_generated_ecc_encodings_iECEGredK.h"

#define TFIT_prepare_dynamic_key_iECEGredK_red(tcurve, input, input_len, key_pair) \
    wbecc_prepare_key(tcurve, &TFIT_ecc_cfg_iECEGredK, input, input_len, key_pair)

#define TFIT_wbecc_eg_get_public_key_iECEGredK(tcurve, key_pair, output, output_len, bytes_written) \
    wbecc_get_public_key(tcurve, &TFIT_ecc_cfg_iECEGredK, key_pair, output, output_len, bytes_written)

#define TFIT_init_wbecc_eg_iECEGredK(ctx, pkey, tcurve)                \
    wbecc_eg_init(ctx, WBECC_EG_DECRYPT, WBECC_CLASSICAL, WBECC_CLASSICAL, pkey, tcurve, NULL, &TFIT_ecc_cfg_iECEGredK)

#define TFIT_final_wbecc_eg_iECEGredK  wbecc_eg_final

#define TFIT_update_wbecc_eg_iECEGredK wbecc_eg_update

#endif /* __TFIT_ECC_EG_Decrypt_iECEGredK_H__ */
