/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES-CTR/Encrypt Instance=iAES15
 */

#ifndef __TFIT_AES_CTR_ENCRYPT_iAES15_H__
#define __TFIT_AES_CTR_ENCRYPT_iAES15_H__

#include "TFIT.h"

#include "TFIT_defs_iAES15.h"
#include "wrapper_modes.h"
#define TFIT_init_wbaes_ctr_iAES15(ctx, wbaes_key, ctr) \
    wbaes_init_ctr(ctx, WBAES_ENCRYPT, WBAES_CLASSICAL, WBAES_CLASSICAL, &TFIT_aes_ecb_iAES15, wbaes_key, &wbw_apictr_classical_enc, NULL, ctr)

#ifdef __cplusplus
extern "C" {
#endif
int TFIT_wbaes_ctr_encrypt_iAES15(const void *wbaes_key, const unsigned char *input, size_t input_len, block_t ctr, unsigned char *output);

int TFIT_wbaes_ctr_encrypt_custom_iAES15(const void *wbaes_key, aes_ctr_inc_fn_t *const inc_func, const unsigned char *input, size_t input_len, block_t ctr, unsigned char *output);

#ifdef __cplusplus
}
#endif

#define TFIT_update_wbaes_iAES15 wbaes_update

#define TFIT_update_slice_wbaes_iAES15 wbaes_update_slice

#define TFIT_final_wbaes_iAES15 wbaes_final

#define TFIT_validate_wb_key_iAES15 TFIT_validate_key_id_iAES15

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iAES15_<colour> */
#define TFIT_prepare_dynamic_key_iAES15(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)



#endif /* __TFIT_AES_CTR_ENCRYPT_iAES15_H__ */
