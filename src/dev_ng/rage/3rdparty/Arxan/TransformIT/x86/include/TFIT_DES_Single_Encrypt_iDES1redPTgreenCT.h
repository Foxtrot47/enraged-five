/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

/*
 * TransformIT: DES/Single/Encrypt Instance=iDES1redPTgreenCT
 */

#ifndef __TFIT_DES_iDES1redPTgreenCT_H__
#define __TFIT_DES_iDES1redPTgreenCT_H__

#include "TFIT.h"

#include "wbdes_api.h"
#include "TFIT_ddefs_iDES1redPTgreenCT.h"

#define TFIT_init_wbdes_iDES1redPTgreenCT(ctx, ecb_key, ecb_key_len)          \
    wbdes_init_ecb(ctx, WBDES_ENCRYPT, WBDES_OBFUSCATED, WBDES_OBFUSCATED, &TFIT_des_ecb_iDES1redPTgreenCT, ecb_key, ecb_key_len)
#define TFIT_update_wbdes_iDES1redPTgreenCT wbdes_update
#define TFIT_update_slice_wbdes_iDES1redPTgreenCT wbdes_update_slice
#define TFIT_final_wbdes_iDES1redPTgreenCT wbdes_final

#ifdef __cplusplus
extern "C"
#endif
int TFIT_wbdes_encrypt_iDES1redPTgreenCT(const void *wbdes_key,
                                      const unsigned char *input, 
                                      const size_t input_len, 
                                      unsigned char *output);

#define TFIT_validate_wb_key_iDES1redPTgreenCT TFIT_validate_dkey_id_iDES1redPTgreenCT

#endif /* __TFIT_DES_iDES1redPTgreenCT_H__ */
