/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: XLAT/Data/Decode Instance=iXDECred
 */

#ifndef __TFIT_XLAT_Data_Decode_iXDECred_H__
#define __TFIT_XLAT_Data_Decode_iXDECred_H__

#include "TFIT.h"

#include "TFIT_apixlat_iXDECred.h"
#include "TFIT_xlatdef_iXDECred.h"
#include "TFIT_xlat_iXDECred.h"

#define TFIT_init_xlat_data_decode_iXDECred(ctx) \
    wbxlat_init_xlat(ctx, WBXLAT_ENCODE, WBXLAT_OBFUSCATED, WBXLAT_CLASSICAL, &TFIT_apixlat_iXDECred, &TFIT_xlat_iXDECred)
#define TFIT_update_xlat_data_decode_iXDECred(ctx, input, input_len, output, output_len, bytes_written) \
    wbxlat_update(ctx, input, input_len, NULL, output, output_len, bytes_written)
#define TFIT_final_xlat_data_decode_iXDECred wbxlat_final

#define TFIT_xlat_data_decode_iXDECred(src, len, dest) \
    TFIT_opxlat_iXDECred((const unsigned char*)&TFIT_xlat_iXDECred, src, len, dest)

#endif /* __TFIT_XLAT_Data_Decode_iXDECred_H__ */
