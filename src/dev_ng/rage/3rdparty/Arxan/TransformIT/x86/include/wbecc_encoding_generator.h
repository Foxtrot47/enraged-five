/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_ENCODING_GENERATOR_H__
#define __WBECC_ENCODING_GENERATOR_H__

extern "C"
{
    #include "randprovider.h"
    #include "obfuscated_xchange_def.h"
    #include "wbecc_encoding_types.h"
}

#include <vector>
#include <algorithm>
#include <string>

using namespace std;



class generator_context {
public:
    
    
    
    
    static rand_provider_ctx rand_ctx;

    
    uint32_t num_encodings;

    
    
    uint32_t encoding_bit_width;

    
    uint32_t num_poisoned_encodings;

    
    std::vector<wbecc_enc_t> poison_list;

    
    
    bool plaintext_output;
    bool plaintext_input;
    bool include_plaintext_output;
    bool plaintext_key;

    
    uint8_t **oxdin_enc;
    uint8_t **oxdin_dec;
    uint8_t **oxdout_enc;
    uint8_t **oxdout_dec;
    uint8_t **oxdkey_enc;
    uint8_t **oxdkey_dec;

    
    obfuscated_xchange_def_t *oxdin;
    obfuscated_xchange_def_t *oxdout;
    obfuscated_xchange_def_t *oxdkey;
    long oxdin_len;

    
    std::string suffix;
    std::string prefix;
    std::string outdir;

    
    uint8_t mask;

    
    wbecc_enc_t *nibble_encode;
    wbecc_enc_t *nibble_decode;
    wbecc_enc_t *encode_table;
    wbecc_enc_t *decode_table;
    
    
    wbecc_enc_t encode_nibble(uint8_t val, uint8_t idx);
    uint8_t decode_nibble(wbecc_enc_t val);

    wbecc_enc_t encode(uint32_t val, uint32_t idx);
    uint8_t decode(wbecc_enc_t val);
    
    
    uint32_t decode_idx(wbecc_enc_t val);

    generator_context();
    ~generator_context();

    
    int populate_generator_context(  rand_provider_ctx rand_ctx,
                        uint32_t num_encodings,
                        uint32_t num_poisoned_encodings,
                        std::string prefix,
                        std::string suffix,
                        std::string outdir);
    
    
    static uint32_t bounded_rand(uint32_t high);
    
    
    int populate_oxd_tables();
    
    int generate_encodings();
    
    int build_alu_tables();
    
    int generate_eps();

    

    
    wbecc_enc_t *negate_lookup;
    wbecc_enc_t *add_unary_lookup;
    wbecc_enc_t *addc_unary_lookup;
    wbecc_enc_t *mult_lookup;
    wbecc_enc_t *multc_lookup;
    wbecc_enc_t *fmap_lookup;

    
    wbecc_enc_t *wbecc_greater_than_lookup;
    wbecc_enc_t *add_lookup;
    wbecc_enc_t *addc_lookup;
    wbecc_enc_t *concat_31_lookup;
    wbecc_enc_t *concat_22_lookup;
    wbecc_enc_t *concat_13_lookup;
    
    wbecc_enc_t *byte_to_nib;
    wbecc_enc_t *keybyte_to_nib;
    
    uint8_t *nib_to_byte;

    
    wbecc_enc_t *big_table;

    wbecc_enc_cfg cfg;
    
    int prepare_big_table();
};


#endif
