/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES-CBC/Decrypt Instance=iAES14orangeIVredPT
 */

#ifndef __TFIT_AES_CBC_DECRYPT_iAES14orangeIVredPT_H__
#define __TFIT_AES_CBC_DECRYPT_iAES14orangeIVredPT_H__

#include "TFIT.h"

#include "TFIT_defs_iAES14orangeIVredPT.h"
#include "wrapper_modes.h"
#include "TFIT_apicbc_iAES14orangeIVredPT.h"

#define TFIT_init_wbaes_cbc_iAES14orangeIVredPT(ctx, wbaes_key, iv)                 \
    wbaes_init_cbc(ctx, WBAES_DECRYPT, WBAES_CLASSICAL, WBAES_OBFUSCATED, &TFIT_aes_ecb_iAES14orangeIVredPT, wbaes_key, &TFIT_apicbc_iAES14orangeIVredPT, &TFIT_wrap_iAES14orangeIVredPT, iv)

#ifdef __cplusplus
extern "C" {
#endif

int TFIT_wbaes_cbc_decrypt_iAES14orangeIVredPT(const void *wbaes_key, const unsigned char *input, size_t input_len, block_t iv, unsigned char *output);

#ifdef __cplusplus
}
#endif



#define TFIT_update_wbaes_iAES14orangeIVredPT wbaes_update

#define TFIT_update_slice_wbaes_iAES14orangeIVredPT wbaes_update_slice

#define TFIT_final_wbaes_iAES14orangeIVredPT wbaes_final

#define TFIT_validate_wb_key_iAES14orangeIVredPT TFIT_validate_key_id_iAES14orangeIVredPT

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iAES14orangeIVredPT_<colour> */
#define TFIT_prepare_dynamic_key_iAES14orangeIVredPT(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)



#endif /* __TFIT_AES_CBC_DECRYPT_iAES14orangeIVredPT_H__ */
