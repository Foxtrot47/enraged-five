/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __GRAPH_TABLE_GENERATOR_H__
#define __GRAPH_TABLE_GENERATOR_H__

#include "wbrsa_config.h"
#include "graph_table.h"

void graph_table_generator(_size maxSize, const mword *key, _size kSize, const mword *p, _size pSize, const mword *q, _size qSize, int rows, int cols, graph_table *graph);

#endif

