/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_ENCODING_TYPES_H__
    #define __WBECC_ENCODING_TYPES_H__

    #include <arxstdint.h>
    #include <stdlib.h>
    #include "nac_max_sizes.h"

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    

    typedef uint8_t wbecc_enc_t;
    #define CARRY_POS 7
    #define CARRY_MASK (~(1<<CARRY_POS))

    typedef enum {
        WBECC_CLASSICAL = 0, 
        WBECC_OBFUSCATED = 1
    } wbecc_obf_t;
    
    

    
    
    

    typedef struct _wbecc_enc_cfg {
        uint32_t pt_lfsr_seed;
        uint32_t pt_lfsr_poly;
        uint32_t pt_table_seed_ptr;
        uint32_t pt_table_mask_ptr;
        uint32_t table_seed;
        uint32_t table_poly;
        uint32_t table_size;
        uint32_t table_lfsr_size;
        uint32_t num_mangled_indices;
        uint32_t num_nibble_indices;
        uint32_t num_input_encodings;
        uint32_t num_output_encodings;
        uint32_t num_key_encodings;
        uint32_t input_tile_size;
        uint32_t output_tile_size;
        uint32_t key_tile_size;
        uint32_t num_interleaved_tables;
        uint8_t toggled;
        uint8_t oxdin_uuid[16];
        uint8_t oxdout_uuid[16];
        uint8_t inst_uuid[16];
        uint8_t oxdkey_uuid[16];
        wbecc_enc_t eps[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t *wbecc_tables;
        wbecc_enc_t *wbecc_concat_31_lookup;
        wbecc_enc_t *wbecc_concat_22_lookup;
        wbecc_enc_t *wbecc_concat_13_lookup;
        wbecc_enc_t *wbecc_add_lookup;
        wbecc_enc_t *wbecc_addc_lookup;
        wbecc_enc_t *wbecc_mult_lookup;
        wbecc_enc_t *wbecc_multc_lookup;
        wbecc_enc_t *wbecc_addc_unary_lookup;
        wbecc_enc_t *wbecc_fmap_lookup;
        wbecc_enc_t *wbecc_negate_lookup;
        wbecc_enc_t *wbecc_greater_than_lookup;
        wbecc_enc_t *byte_to_nib;
        wbecc_enc_t *keybyte_to_nib;
        wbecc_enc_t *nib_to_byte;
    } wbecc_enc_cfg;

    
    
    
    static __inline wbecc_enc_t wbecc_concat_3_1(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg){
        return cfg->wbecc_concat_31_lookup[a*(cfg->num_mangled_indices<<3) + b];
    }
    static __inline wbecc_enc_t wbecc_concat_2_2(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg){
        return cfg->wbecc_concat_22_lookup[a*(cfg->num_mangled_indices<<3) + b];
    }
    static __inline wbecc_enc_t wbecc_concat_1_3(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg){
        return cfg->wbecc_concat_13_lookup[a*(cfg->num_mangled_indices<<3) + b];
    }
    static __inline wbecc_enc_t wbecc_concat_4_0(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg){
        (void)cfg;   

        (void)b;     

        return a;
    }
    static __inline wbecc_enc_t wbecc_concat_0_4(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg){
        (void)cfg;   

        (void)a;     

        return b;
    }
    static __inline wbecc_enc_t wbecc_add2(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg) {
        return cfg->wbecc_add_lookup[a*(cfg->num_mangled_indices<<3) + b];
    }
    static __inline wbecc_enc_t wbecc_add2c(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg) {
        
        return cfg->wbecc_addc_lookup[a*(cfg->num_mangled_indices<<3) + b];
    }
    static __inline wbecc_enc_t wbecc_mult2(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg) {
        return cfg->wbecc_mult_lookup[a*(cfg->num_mangled_indices<<3) + b];
    }
    static __inline wbecc_enc_t wbecc_mult2c(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg) {
        return cfg->wbecc_multc_lookup[a*(cfg->num_mangled_indices<<3) + b];
    }
    static __inline wbecc_enc_t wbecc_greater_than(wbecc_enc_t a, wbecc_enc_t b, const wbecc_enc_cfg *cfg) {
        return cfg->wbecc_greater_than_lookup[a*(cfg->num_mangled_indices<<3) + b];
    }

    static __inline wbecc_enc_t wbecc_add2c_unary(wbecc_enc_t a, const wbecc_enc_cfg *cfg) {
        return cfg->wbecc_addc_unary_lookup[a];
    }
    static __inline wbecc_enc_t wbecc_negate(wbecc_enc_t a, const wbecc_enc_cfg *cfg) {
        return cfg->wbecc_negate_lookup[a];
    }

    

    static __inline wbecc_enc_t pt_to_enc(uint32_t val, const wbecc_enc_cfg *cfg) {
        uint32_t lfsr = cfg->pt_lfsr_seed;
        uint32_t i;
        for (i = 0; i < val ; i++){
            lfsr = (lfsr >> 1) ^ ((uint32_t)(-((int32_t)(lfsr & 1u))) & cfg->pt_lfsr_poly); 
        }
        return (wbecc_enc_t) lfsr;
    }

    static __inline void translate_pt_input_data(uint8_t data, wbecc_enc_t *ptr, const wbecc_enc_cfg *cfg) {
        ptr[0] = pt_to_enc((data>>4)&0xf, cfg);
        ptr[1] = pt_to_enc((data>>0)&0xf, cfg);
    }

    static __inline void translate_input_data(uint8_t data, wbecc_enc_t *ptr, uint32_t idx, const wbecc_enc_cfg *cfg) {
        ptr[0] = cfg->byte_to_nib[idx*256*2 + data*2U + 0];
        ptr[1] = cfg->byte_to_nib[idx*256*2 + data*2U + 1];
    }
    
    static __inline void translate_key_input_data(uint8_t data, wbecc_enc_t *ptr, uint32_t idx, const wbecc_enc_cfg *cfg) {
        ptr[0] = cfg->keybyte_to_nib[idx*256*2 + data*2U + 0];
        ptr[1] = cfg->keybyte_to_nib[idx*256*2 + data*2U + 1];
    }
    
    

    static __inline int translate_random_input_data(
            const wbecc_enc_cfg * const cfg,
            const uint8_t * const input, 
            unsigned int input_len,
            wbecc_enc_t * const output,
            unsigned int output_size,
            unsigned int * const bytes_written
        ) {
        unsigned int i = 0;
        *bytes_written = 0;

        if (input_len*2 > output_size) {return 1;}
        if (cfg == NULL) {return 2;}
        if (input == NULL) {return 3;}
        if (output == NULL) {return 4;}

        for (i = 0; i < input_len; i++) {
            output[i*2+0] = cfg->byte_to_nib[(i*2+3 + input[i]*2U + 0) % 256*2];
            output[i*2+1] = cfg->byte_to_nib[(i*2+7 + (uint32_t)(~input[i])*2U + 1) % 256*2];
        }
        *bytes_written = input_len*2;

        return 0;
    }
 
    

    static __inline void swap(wbecc_enc_t *a, wbecc_enc_t *b) {
        
        
        
        
        *b = (wbecc_enc_t)(*b + *a);
        *a = (wbecc_enc_t)(*b - *a);
        *b = (wbecc_enc_t)(*b - *a);
    }

    static __inline void toggle_table_break(wbecc_enc_t *table, const wbecc_enc_cfg *cfg){
        uint32_t lfsr = cfg->table_seed;
        uint32_t period = 0;
        uint32_t i;
        do {
            for (i = 0; i < cfg->table_size/cfg->table_lfsr_size; i+=2){
                swap(&table[i*cfg->table_lfsr_size + period], &table[(i+1)*cfg->table_lfsr_size + lfsr - 1]);
            }
            period++;
            lfsr = (lfsr >> 1) ^ ((uint32_t)(-((int32_t)(lfsr & 1u))) & cfg->table_poly); 
        } while (lfsr != cfg->table_seed);
    }

    
    static __inline void setup_configuration(wbecc_enc_cfg *cfg){
        unsigned int pos = 0;
        
        
        if (cfg->toggled) {return;}

        toggle_table_break(cfg->wbecc_tables, cfg);
        cfg->toggled = 1;
        cfg->wbecc_add_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
        cfg->wbecc_addc_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
        cfg->wbecc_mult_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
        cfg->wbecc_multc_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
        cfg->wbecc_concat_31_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
        cfg->wbecc_concat_22_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
        cfg->wbecc_concat_13_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
        cfg->wbecc_greater_than_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
        pos = cfg->num_mangled_indices*cfg->num_mangled_indices*pos;
        cfg->wbecc_negate_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[pos];
        pos += cfg->num_mangled_indices;
        cfg->wbecc_addc_unary_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[pos];
        pos += cfg->num_mangled_indices;
        cfg->wbecc_fmap_lookup = (wbecc_enc_t *) &cfg->wbecc_tables[pos];
        pos += (1<<5)*2;
        cfg->byte_to_nib = (wbecc_enc_t *) &cfg->wbecc_tables[pos];
        pos += cfg->num_input_encodings*256*2;
        cfg->keybyte_to_nib = (wbecc_enc_t *) &cfg->wbecc_tables[pos];
        pos += cfg->num_key_encodings*256*2;
        cfg->nib_to_byte = (wbecc_enc_t *) &cfg->wbecc_tables[pos];
        pos += cfg->num_output_encodings*cfg->num_nibble_indices*cfg->num_nibble_indices;
    }

    #ifdef __cplusplus
    }
    #endif

#endif

