/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __WBECC_COMMON_API_FST_H__
    #define __WBECC_COMMON_API_FST_H__

    #define WBECC_FAST_PREPARE_KEY_SUCCESS         0
    #define WBECC_FAST_PREPARE_KEY_INPUT_TOO_SHORT 1
    #define WBECC_FAST_PREPARE_KEY_INPUT_TOO_LONG  2
    #define WBECC_FAST_PREPARE_KEY_ILLEGAL_ARG     3
    #define WBECC_FAST_PREPARE_KEY_REDUCE_FAILED   4

#endif
