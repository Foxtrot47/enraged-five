/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: ECC/DSA Instance=iECDSASredK
 */

#ifndef __TFIT_ECC_DSA_SIGN_iECDSASredK_H__
#define __TFIT_ECC_DSA_SIGN_iECDSASredK_H__

#include "TFIT.h"

#include "wbecc_dsa_api.h"
#include "TFIT_generated_ecc_encodings_iECDSASredK.h"

#define TFIT_prepare_dynamic_key_iECDSASredK_red(tcurve, input, input_len, key_pair) \
    wbecc_prepare_key(tcurve, &TFIT_ecc_cfg_iECDSASredK, input, input_len, key_pair)

#define TFIT_wbecc_dsa_get_public_key_iECDSASredK(tcurve, key_pair, output, output_len, bytes_written) \
    wbecc_get_public_key(tcurve, &TFIT_ecc_cfg_iECDSASredK, key_pair, output, output_len, bytes_written)

#define TFIT_init_wbecc_dsa_iECDSASredK(ctx, pkey, tcurve, get_nonce, digest_mode) \
    wbecc_dsa_init(ctx, pkey, tcurve, get_nonce, &TFIT_ecc_cfg_iECDSASredK, digest_mode)
#define TFIT_final_sign_wbecc_dsa_iECDSASredK  wbecc_dsa_final_sign

#define TFIT_update_wbecc_dsa_iECDSASredK wbecc_dsa_update

#endif /* __TFIT_ECC_DSA_SIGN_iECDSASredK_H__ */
