/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WRAPPER_MODES_H__
#define __WRAPPER_MODES_H__

#ifdef __cplusplus
extern "C" {
#endif

#include<stddef.h>
#include "wbaes_api.h"

#define BLOCKLEN_OCTETS 16
typedef unsigned char block_t[BLOCKLEN_OCTETS];

#define HALFBLOCKLEN_OCTETS (BLOCKLEN_OCTETS/2)
typedef unsigned char halfblock_t[HALFBLOCKLEN_OCTETS];



typedef int (fn_wbany_op_t)();



typedef int (fn_wbaes_op_t)(const void * const key, const block_t in, block_t out);



typedef int (fn_wbgcm_op_t)(fn_wbaes_op_t *const wbaes_op, const void *const key[2], const void *const data_in, size_t octet_len, block_t IV, void *const H, void *const tag, void *const data_out );
typedef int (fn_wbgcm_op_tag_t)(fn_wbaes_op_t *const wbaes_op, const void *const key[2], block_t AC_len, size_t tag_len, block_t tag, block_t J, void *const data_out );
typedef int (fn_wbcbc_op_t)(fn_wbaes_op_t *const wbaes_op, const void *const key[2], const void *const data_in, size_t octet_len, block_t IV, void *const data_out );
typedef int (fn_wbnwrap_op_t)(fn_wbaes_op_t *const wbaes_op, const void * const key[2], const void * const data_in, size_t octet_len, void * const data_out);
typedef int (fn_wbcwrap_op_t)(const void * const key, const void * const data_in, size_t octet_len, void * const data_out);







extern const wbaes_wrapper_t wbw_apicbc_classical_enc;
extern const wbaes_wrapper_t wbw_apicbc_classical_dec;
extern const wbaes_wrapper_t wbw_apictr_classical_enc;
extern const wbaes_wrapper_t wbw_apictr_classical_dec;
extern const wbaes_wrapper_t wbw_apigcm_classical_enc;
extern const wbaes_wrapper_t wbw_apigcm_classical_dec;
extern const wbaes_wrapper_t wbw_apiccm_classical_enc;
extern const wbaes_wrapper_t wbw_apiccm_classical_dec;
extern const wbaes_wrapper_t wbw_apicmac_classical;
extern const wbaes_wrapper_t wbw_apinwrap_classical_enc;
extern const wbaes_wrapper_t wbw_apinwrap_classical_dec;
extern const wbaes_wrapper_t wbw_apicwrap_classical_enc;
extern const wbaes_wrapper_t wbw_apicwrap_classical_dec;







void block_xor(const block_t a, const block_t b, block_t dest);











int aes_cbc_encrypt(fn_wbaes_op_t *const wbaes_op, const void * const key, const void * const data_in, size_t octet_len, block_t IV, void *const data_out);



int aes_cbc_decrypt(fn_wbaes_op_t *const wbaes_op, const void *const key, const void *const data_in, size_t octet_len, block_t IV, void *const data_out);



int aes_wbcbc_encrypt_piecewise(fn_wbany_op_t *const wbaes_op[2], 
  const void *const key[2], const void *const data_in, size_t octet_len, 
  block_t iv, unsigned char input_opt, void *const data_out); 
int aes_wbcbc_decrypt_piecewise(fn_wbany_op_t *const wbaes_op[2], 
  const void *const key[2], const void *const data_in, size_t octet_len, 
  block_t iv, unsigned char input_opt, void *const data_out);
int aes_wbcbc_encrypt(fn_wbany_op_t *const wbaes_op[2], 
  const void *const key[2], const void *const data_in, size_t octet_len, 
  block_t iv, void *const data_out); 
int aes_wbcbc_decrypt(fn_wbany_op_t *const wbaes_op[2], 
  const void *const key[2], const void *const data_in, size_t octet_len, 
  block_t iv, void *const data_out);
int aes_wbcbc_first_encrypt(fn_wbany_op_t *const wbaes_op[2], 
  const void *const key[2], const void *const data_in, size_t octet_len, 
  block_t iv, void *const data_out); 
int aes_wbcbc_first_decrypt(fn_wbany_op_t *const wbaes_op[2], 
  const void *const key[2], const void *const data_in, size_t octet_len, 
  block_t iv, void *const data_out);











int ecb_cts8(fn_wbaes_op_t *const wbaes_op, const void *const key, const void *const data_in, size_t octet_len, void *const data_out);











int aes_ctr8_std_inc_m128(fn_wbaes_op_t *const wbaes_op, const void *const key, const void *const data_in, size_t octet_len, block_t ctr, void *const data_out);
int aes_wbctr8_std_inc_m128( fn_wbany_op_t *const wbaes_op[2], const void *const key[2], const void *const data_in, size_t octet_len, block_t ctr, void *const data_out );





#ifndef __AES_CTR_INC_FN_T__
#define __AES_CTR_INC_FN_T__
typedef int aes_ctr_inc_fn_t(const block_t ctr_src, block_t ctr_dest);


#endif 


int aes_ctr8(fn_wbaes_op_t *const wbaes_op, const void *const key, aes_ctr_inc_fn_t *const inc_fn, const void *const data_in, size_t octet_len, block_t ctr, void *const data_out);
int aes_wbctr8( fn_wbany_op_t *const wbaes_op[2], const void *const key[2], aes_ctr_inc_fn_t *const inc_fn, const void *const data_in, size_t octet_len, block_t ctr, void *const data_out );



int aes_ctr_standard_increment_m128(const block_t ctr_src, block_t ctr_dest);













int aes_gcm_encrypt(fn_wbaes_op_t *const wbaes_op, const void *const key, const void *const data_in, size_t octet_len, block_t ctr, block_t H, block_t tag, void *const data_out);
int aes_gcm_encrypt_tag(fn_wbaes_op_t *const wbaes_op, const void *const key, const void *const data_in, size_t data_len, const void *const aad, size_t aad_len, const uint8_t iv[12], void *const Tag, size_t tag_len, void *const data_out);
int aes_gcm_decrypt(fn_wbaes_op_t *const wbaes_op, const void *const key, const void *const data_in, size_t octet_len, block_t ctr, block_t H, block_t tag, void *const data_out);
int aes_gcm_decrypt_verify(fn_wbaes_op_t *const wbaes_op, const void *const key, const void *const data_in, size_t data_len, const void *const aad, size_t aad_len, const uint8_t iv[12], const void *const TAG, size_t tag_len, void *const data_out);
int aes_gcm_get_tag(fn_wbaes_op_t *const wbaes_op, const void *const key, block_t AC_len, size_t tag_len, block_t tag, block_t J, void *const data_out);



int aes_wbgcm_encrypt(fn_wbany_op_t *const wbaes_op[2], const void *const key[2], const void *const data_in, size_t octet_len, block_t ctr, block_t H, block_t tag, void *const data_out );
int aes_wbgcm_decrypt(fn_wbany_op_t *const wbaes_op[2], const void *const key[2], const void *const data_in, size_t octet_len, block_t ctr, block_t H, block_t tag, void *const data_out );
int aes_wbgcm_get_tag(fn_wbany_op_t *const wbaes_op[3], const void *const key[2], block_t AC_len, size_t tag_len, block_t tag, block_t J, void *const data_out);
int aes_wbgcm_encrypt_tag(fn_wbany_op_t *const wbaes_op[2], const void *const key[2], const void *const data_in, size_t data_len, const void *const aad, size_t aad_len, const uint8_t iv[12], void *const Tag, size_t tag_len, void *const data_out);
int aes_wbgcm_decrypt_verify(fn_wbany_op_t *const wbaes_op[2], const void *const key[2], const void *const data_in, size_t data_len, const void *const aad, size_t aad_len, const uint8_t iv[12], const void *const TAG, size_t tag_len, void *const data_out);










int aes_ccm_encrypt_piecewise(fn_wbaes_op_t* const wbaes_op, const void * const key, const void * const data_in, size_t octet_len, wbaes_ccm_ctx *ctx, unsigned char input_opt, size_t *bytes_written, size_t out_octet_len, void * const data_out);
int aes_ccm_decrypt_piecewise(fn_wbaes_op_t* const wbaes_op, const void * const key, const void * const data_in, size_t octet_len, wbaes_ccm_ctx *ctx, unsigned char input_opt, size_t *bytes_written, size_t out_octet_len, void * const data_out);




int aes_wbccm_encrypt_piecewise(fn_wbany_op_t *const wbaes_op[4], const void *const key[4], const void *const data_in, size_t octet_len, wbaes_ccm_ctx *ctx, unsigned char input_opt, size_t *bytes_written, size_t out_octet_len, void * const data_out);
int aes_wbccm_decrypt_piecewise(fn_wbany_op_t *const wbaes_op[4], const void *const key[4], const void *const data_in, size_t octet_len, wbaes_ccm_ctx *ctx, unsigned char input_opt, size_t *bytes_written, size_t out_octet_len, void * const data_out);


int aes_ccm_encrypt(fn_wbaes_op_t* const wbaes_op, const void * const key, const unsigned char *assoc_data, uint64_t assoc_data_len, const unsigned char *data_in, uint64_t data_in_len, size_t digest_len, const unsigned char *nonce, size_t nonce_len, size_t *bytes_written, size_t output_len, void * const data_out);
int aes_ccm_decrypt(fn_wbaes_op_t* const wbaes_op, const void * const key, const unsigned char *assoc_data, uint64_t assoc_data_len, const unsigned char *data_in, uint64_t data_in_len, size_t digest_len, const unsigned char *nonce, size_t nonce_len, size_t *bytes_written, size_t output_len, void * const data_out);
int aes_wbccm_encrypt(fn_wbany_op_t *const wbaes_op[4], const void *const key[4], const unsigned char *assoc_data, uint64_t assoc_data_len, const unsigned char *data_in, uint64_t data_in_len, size_t digest_len, const unsigned char *nonce, size_t nonce_len, size_t *bytes_written, size_t output_len, void * const data_out);
int aes_wbccm_decrypt(fn_wbany_op_t *const wbaes_op[4], const void *const key[4], const unsigned char *assoc_data, uint64_t assoc_data_len, const unsigned char *data_in, uint64_t data_in_len, size_t digest_len, const unsigned char *nonce, size_t nonce_len, size_t *bytes_written, size_t output_len, void * const data_out);










int aes_cmac(fn_wbaes_op_t* const wbaes_op, const void * const key, const void * const data_in, size_t octet_len, void * const data_out, size_t out_octet_len);
int aes_cmac_piecewise(fn_wbaes_op_t* const wbaes_op, const void * const key, const void * const data_in, size_t octet_len, void * const data_iv, const char input_complete, void * const data_out, size_t out_octet_len);



int aes_wbcmac(fn_wbany_op_t *const wbaes_op[2],
  const void *const key[2], const void *const data_in, size_t octet_len,
  void *const data_out, size_t digest_len);
int aes_wbcmac_piecewise(fn_wbany_op_t *const wbaes_op[2],
  const void *const key[2], const void *const data_in, size_t octet_len,
  void * const data_iv, const char input_opt,
  void *const data_out, size_t digest_len);








int aes_wrap(fn_wbaes_op_t* const wbaes_op, const void * const key, const void * const data_in, size_t octet_len, void * const data_out, int cmla_perm);

int aes_unwrap(fn_wbaes_op_t* const wbaes_op, const void * const key, const void * const data_in, size_t octet_len, void * const data_out, int cmla_perm);

int aes_wbunwrap(fn_wbany_op_t* const wbany_op[3], const void * const key[3], const void * const data_in, size_t octet_len, void * const data_out);


#ifdef __cplusplus
}
#endif

#endif

