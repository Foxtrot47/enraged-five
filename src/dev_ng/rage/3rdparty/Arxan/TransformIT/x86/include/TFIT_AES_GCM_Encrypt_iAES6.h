/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES-GCM/Encrypt Instance=iAES6
 */

#ifndef __TFIT_AES_GCM_ENCRYPT_iAES6_H__
#define __TFIT_AES_GCM_ENCRYPT_iAES6_H__

#include "TFIT.h"

#include "TFIT_defs_iAES6.h"
#include "wrapper_modes.h"
#define TFIT_update_wbaes_iAES6 wbaes_update

#define TFIT_update_slice_wbaes_iAES6 wbaes_update_slice

#define TFIT_final_wbaes_iAES6 wbaes_final

#define TFIT_validate_wb_key_iAES6 TFIT_validate_key_id_iAES6

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iAES6_<colour> */
#define TFIT_prepare_dynamic_key_iAES6(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)


#define TFIT_init_wbaes_gcm_iAES6(ctx, wbaes_key, ctr)                 \
    wbaes_init_gcm(ctx, WBAES_ENCRYPT, WBAES_CLASSICAL, WBAES_CLASSICAL, &TFIT_aes_ecb_iAES6, wbaes_key, &wbw_apigcm_classical_enc, NULL, ctr)
#define TFIT_wbaes_gcm_encrypt_tag_iAES6(wbaes_key, input, input_len, aad, aad_len, ctr, tag_out, tag_len, output) \
        aes_gcm_encrypt_tag((fn_wbaes_op_t *)&TFIT_op_iAES6, wbaes_key, input, input_len, aad, aad_len, ctr, tag_out, tag_len, output)


#define TFIT_wbaes_gcm_get_tag_iAES6 wbaes_gcm_get_tag
#define TFIT_wbaes_gcm_verify_tag_iAES6 wbaes_gcm_verify_tag

#endif /* __TFIT_AES_GCM_ENCRYPT_iAES6_H__ */
