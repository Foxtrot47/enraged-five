/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: RSA Instance=iRSA2048
 */

#ifndef __TFIT_RSA_iRSA2048_H__
#define __TFIT_RSA_iRSA2048_H__

#include "TFIT.h"
#include "TFIT_whiteboxrsa_iRSA2048.h"

#define TFIT_rsa_seed_internal_prng_iRSA2048 rsa_seed_internal_prng
#define TFIT_validate_wb_key_iRSA2048 TFIT_rsa_validate_key_id_iRSA2048

#endif /* __TFIT_RSA_iRSA2048_H__ */
