/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES-ECB/Encrypt Instance=iAES1redPT
 */

#ifndef __TFIT_AES_ECB_ENCRYPT_iAES1redPT_H__
#define __TFIT_AES_ECB_ENCRYPT_iAES1redPT_H__

#include "TFIT.h"

#include "TFIT_defs_iAES1redPT.h"
#include "wrapper_modes.h"
#define TFIT_init_wbaes_ecb_iAES1redPT(ctx, wbaes_key) \
    wbaes_init_ecb(ctx, WBAES_ENCRYPT, WBAES_OBFUSCATED, WBAES_CLASSICAL, &TFIT_aes_ecb_iAES1redPT, wbaes_key)

#define TFIT_update_wbaes_iAES1redPT wbaes_update

#define TFIT_update_slice_wbaes_iAES1redPT wbaes_update_slice

#define TFIT_final_wbaes_iAES1redPT wbaes_final

#define TFIT_validate_wb_key_iAES1redPT TFIT_validate_key_id_iAES1redPT

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iAES1redPT_<colour> */
#define TFIT_prepare_dynamic_key_iAES1redPT(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)



#ifdef __cplusplus
extern "C"
#endif

int TFIT_wbaes_ecb_encrypt_iAES1redPT(const TFIT_key_iAES1redPT_t *wbaes_key, 
                                                const unsigned char *input, 
                                                const size_t input_len, 
                                                unsigned char *output);

#endif /* __TFIT_AES_ECB_ENCRYPT_iAES1redPT_H__ */
