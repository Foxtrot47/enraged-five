/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_NBV_IECEGREDPTFASTP256_H__
#define __TFIT_NBV_IECEGREDPTFASTP256_H__
#define TFIT_nbv_iECEGredPTfastP256_8_0 ((uint8_t)0x53)
#define TFIT_nbv_iECEGredPTfastP256_8_1 ((uint8_t)0x7e)
#define TFIT_nbv_iECEGredPTfastP256_8_2 ((uint8_t)0x72)
#define TFIT_nbv_iECEGredPTfastP256_8_3 ((uint8_t)0xc0)
#define TFIT_nbv_iECEGredPTfastP256_8_4 ((uint8_t)0x07)
#define TFIT_nbv_iECEGredPTfastP256_8_5 ((uint8_t)0x2a)
#define TFIT_nbv_iECEGredPTfastP256_8_6 ((uint8_t)0xb1)
#define TFIT_nbv_iECEGredPTfastP256_8_7 ((uint8_t)0x03)
#define TFIT_nbv_iECEGredPTfastP256_8_8 ((uint8_t)0x52)
#define TFIT_nbv_iECEGredPTfastP256_8_9 ((uint8_t)0xe5)
#define TFIT_nbv_iECEGredPTfastP256_8_10 ((uint8_t)0x16)
#define TFIT_nbv_iECEGredPTfastP256_8_11 ((uint8_t)0x17)
#define TFIT_nbv_iECEGredPTfastP256_8_12 ((uint8_t)0xa3)
#define TFIT_nbv_iECEGredPTfastP256_8_13 ((uint8_t)0x8b)
#define TFIT_nbv_iECEGredPTfastP256_8_14 ((uint8_t)0x2d)
#define TFIT_nbv_iECEGredPTfastP256_8_15 ((uint8_t)0xe2)
#define TFIT_nbv_iECEGredPTfastP256_8_16 ((uint8_t)0x31)
#define TFIT_nbv_iECEGredPTfastP256_8_17 ((uint8_t)0x79)
#define TFIT_nbv_iECEGredPTfastP256_8_18 ((uint8_t)0xcb)
#define TFIT_nbv_iECEGredPTfastP256_8_19 ((uint8_t)0x07)
#define TFIT_nbv_iECEGredPTfastP256_8_20 ((uint8_t)0x14)
#define TFIT_nbv_iECEGredPTfastP256_8_21 ((uint8_t)0xc0)
#define TFIT_nbv_iECEGredPTfastP256_8_22 ((uint8_t)0x85)
#define TFIT_nbv_iECEGredPTfastP256_8_23 ((uint8_t)0x7a)
#define TFIT_nbv_iECEGredPTfastP256_8_24 ((uint8_t)0x26)
#define TFIT_nbv_iECEGredPTfastP256_8_25 ((uint8_t)0x60)
#define TFIT_nbv_iECEGredPTfastP256_8_26 ((uint8_t)0x28)
#define TFIT_nbv_iECEGredPTfastP256_8_27 ((uint8_t)0x38)
#define TFIT_nbv_iECEGredPTfastP256_8_28 ((uint8_t)0xbd)
#define TFIT_nbv_iECEGredPTfastP256_8_29 ((uint8_t)0x5e)
#define TFIT_nbv_iECEGredPTfastP256_8_30 ((uint8_t)0x67)
#define TFIT_nbv_iECEGredPTfastP256_8_31 ((uint8_t)0x01)
#define TFIT_nbv_iECEGredPTfastP256_16_0 ((uint16_t)0x537e)
#define TFIT_nbv_iECEGredPTfastP256_16_1 ((uint16_t)0x72c0)
#define TFIT_nbv_iECEGredPTfastP256_16_2 ((uint16_t)0x072a)
#define TFIT_nbv_iECEGredPTfastP256_16_3 ((uint16_t)0xb103)
#define TFIT_nbv_iECEGredPTfastP256_16_4 ((uint16_t)0x52e5)
#define TFIT_nbv_iECEGredPTfastP256_16_5 ((uint16_t)0x1617)
#define TFIT_nbv_iECEGredPTfastP256_16_6 ((uint16_t)0xa38b)
#define TFIT_nbv_iECEGredPTfastP256_16_7 ((uint16_t)0x2de2)
#define TFIT_nbv_iECEGredPTfastP256_16_8 ((uint16_t)0x3179)
#define TFIT_nbv_iECEGredPTfastP256_16_9 ((uint16_t)0xcb07)
#define TFIT_nbv_iECEGredPTfastP256_16_10 ((uint16_t)0x14c0)
#define TFIT_nbv_iECEGredPTfastP256_16_11 ((uint16_t)0x857a)
#define TFIT_nbv_iECEGredPTfastP256_16_12 ((uint16_t)0x2660)
#define TFIT_nbv_iECEGredPTfastP256_16_13 ((uint16_t)0x2838)
#define TFIT_nbv_iECEGredPTfastP256_16_14 ((uint16_t)0xbd5e)
#define TFIT_nbv_iECEGredPTfastP256_16_15 ((uint16_t)0x6701)
#define TFIT_nbv_iECEGredPTfastP256_32_0 ((uint32_t)0x537e72c0)
#define TFIT_nbv_iECEGredPTfastP256_32_1 ((uint32_t)0x072ab103)
#define TFIT_nbv_iECEGredPTfastP256_32_2 ((uint32_t)0x52e51617)
#define TFIT_nbv_iECEGredPTfastP256_32_3 ((uint32_t)0xa38b2de2)
#define TFIT_nbv_iECEGredPTfastP256_32_4 ((uint32_t)0x3179cb07)
#define TFIT_nbv_iECEGredPTfastP256_32_5 ((uint32_t)0x14c0857a)
#define TFIT_nbv_iECEGredPTfastP256_32_6 ((uint32_t)0x26602838)
#define TFIT_nbv_iECEGredPTfastP256_32_7 ((uint32_t)0xbd5e6701)
#define TFIT_nbv_iECEGredPTfastP256_64_0 ((uint64_t)0x537e72c0072ab103ULL)
#define TFIT_nbv_iECEGredPTfastP256_64_1 ((uint64_t)0x52e51617a38b2de2ULL)
#define TFIT_nbv_iECEGredPTfastP256_64_2 ((uint64_t)0x3179cb0714c0857aULL)
#define TFIT_nbv_iECEGredPTfastP256_64_3 ((uint64_t)0x26602838bd5e6701ULL)
#endif
