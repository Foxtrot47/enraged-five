/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: SHA-Digest Instance=iSHAgreenM
 */

#ifndef __TFIT_SHA_Digest_iSHAgreenM_H__
#define __TFIT_SHA_Digest_iSHAgreenM_H__

#include "TFIT.h"

#include "wbsha.h"
#include "TFIT_generated_encodings_iSHAgreenM.h"

#define TFIT_init_wbsha_digest_iSHAgreenM(ctx, sha_mode) \
    wbsha_digest_init(ctx, WBSHA_CLASSICAL, &TFIT_sha_cfg_iSHAgreenM, sha_mode)

#define TFIT_update_wbsha_digest_iSHAgreenM wbsha_digest_update
#define TFIT_update_slice_wbsha_digest_iSHAgreenM wbsha_digest_update_slice
#ifndef WBSHA_DIGEST_UPDATE_NO_REMOVE_LEADING_ZEROS
#define TFIT_update_remove_zeros_wbsha_digest_iSHAgreenM wbsha_digest_update_remove_zeros
#define TFIT_update_slice_remove_zeros_wbsha_digest_iSHAgreenM wbsha_digest_update_slice_remove_zeros
#endif
#define TFIT_final_wbsha_digest_iSHAgreenM wbsha_digest_final

#endif /* __TFIT_SHA_Digest_iSHAgreenM_H__ */
