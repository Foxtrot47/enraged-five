/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES/NWRAP Decrypt Instance=iNWRAP12AES
 */

#ifndef __TFIT_AES_NWRAP_iNWRAP12AES_H__
#define __TFIT_AES_NWRAP_iNWRAP12AES_H__

#include "TFIT.h"

#include "TFIT_defs_iNWRAP12AES.h"
#define TFIT_update_wbaes_iNWRAP12AES wbaes_update

#define TFIT_update_slice_wbaes_iNWRAP12AES wbaes_update_slice

#define TFIT_final_wbaes_iNWRAP12AES wbaes_final

#define TFIT_validate_wb_key_iNWRAP12AES TFIT_validate_key_id_iNWRAP12AES

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iNWRAP12AES_<colour> */
#define TFIT_prepare_dynamic_key_iNWRAP12AES(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)


#include "wrapper_modes.h"
#include "wrapper_modes.h"

#define TFIT_init_wbaes_nwrap_iNWRAP12AES(ctx, wbaes_key, digest_len) \
    wbaes_init_nwrap(ctx, WBAES_DECRYPT, WBAES_CLASSICAL, WBAES_CLASSICAL, &TFIT_aes_ecb_iNWRAP12AES, wbaes_key, &wbw_apinwrap_classical_dec, NULL, digest_len)
#define TFIT_wbaes_nwrap_iNWRAP12AES(wbaes_key, input, input_len, output) \
    aes_unwrap((fn_wbaes_op_t *)&TFIT_op_iNWRAP12AES, wbaes_key, input, input_len, output, 0)


#endif /* __TFIT_AES_NWRAP_Decrypt_iNWRAP12AES_H__ */
