/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

/*
 * TransformIT: Dynamic-Key for Instance=i3DES2greenCT_red
 */

#ifndef __TFIT_DK_i3DES2greenCT_red_H__
#define __TFIT_DK_i3DES2greenCT_red_H__

#include "TFIT_ddyninit_i3DES2greenCT_red.h"
#include "wbdes_dyninit.h"

#define TFIT_prepare_dynamic_key_i3DES2greenCT_red(key, key_len, wbkey, wbkey_len, out_len) \
    wbdes_dyninit_prepare_key(key, key_len, (const uint8_t *)&TFIT_ddyninit_i3DES2greenCT_red, wbkey, wbkey_len, out_len)

#endif /* __TFIT_DK_i3DES2greenCT_red_H__ */
