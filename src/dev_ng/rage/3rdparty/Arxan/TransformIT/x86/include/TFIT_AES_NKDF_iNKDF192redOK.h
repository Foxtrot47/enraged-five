/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES/NK108 Instance=iNKDF192redOK
 */

#ifndef __TFIT_AES_NK108_iNKDF192redOK_H__
#define __TFIT_AES_NK108_iNKDF192redOK_H__

#include "TFIT.h"

#include "TFIT_defs_iNKDF192redOK.h"
#include "TFIT_apicmac_iNKDF192redOK.h"
#include "wrapper_modes.h"
#include "kdf_800_108.h"

#define TFIT_wbaes_nist_kdf108_iNKDF192redOK(fixed_input_data, input_len, length, wbaes_key, output, output_len, bytes_written) \
    wbnist_kdf108(fixed_input_data, input_len, length, &TFIT_aes_ecb_iNKDF192redOK, wbaes_key, &TFIT_apicmac_iNKDF192redOK, &TFIT_wrap_iNKDF192redOK, output, output_len, bytes_written)

#define TFIT_validate_wb_key_iNKDF192redOK TFIT_validate_key_id_iNKDF192redOK

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iNKDF192redOK_<colour> */
#define TFIT_prepare_dynamic_key_iNKDF192redOK(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, &TFIT_op_iNKDF192redOK, out_len)


#endif /* __TFIT_AES_NK108_iNKDF192redOK_H__ */
