/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: OMA/KDF Instance=iOKDFgreenSredOK
 */

#ifndef __TFIT_OKDF_iOKDFgreenSredOK_H__
#define __TFIT_OKDF_iOKDFgreenSredOK_H__

#include "TFIT.h"
#include "oma_kdf.h"
#include "TFIT_dmap_iOKDFgreenSredOK.h"
#include "TFIT_slice_table_in_to_in_iOKDFgreenSredOK.h"
#include "TFIT_generated_encodings_iOKDFgreenSredOK.h"

#define TFIT_omakdf_iOKDFgreenSredOK(z,z_len,klen,other,other_len,output,output_len,bytes_written) \
    wbomakdf_kdf(z,z_len,klen,other,other_len,&TFIT_sha_cfg_iOKDFgreenSredOK,&TFIT_omakdf_slice_data_in_to_in_iOKDFgreenSredOK,&TFIT_omakdf_data_iOKDFgreenSredOK,output,output_len,bytes_written)

#endif /* __TFIT_OKDF_iOKDFgreenSredOK_H__ */
