/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __CMLA_KDF_H__
#define __CMLA_KDF_H__ 1

#include <arxstdint.h>

#include "wbsha_encoding_types.h"
#include "slice.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum _wbomakdf_status_t {
    WBOMAKDF_OK = 0,
    WBOMAKDF_NULL_ARG = 1,
    WBOMAKDF_INVALID_INPUT_LEN = 2,
    WBOMAKDF_INVALID_OUTPUT_LEN = 3,
    WBOMAKDF_SLICE_UNKNOWN_ERROR = 4,
    WBOMAKDF_SLICE_NULL_ARG = 5,
    WBOMAKDF_SLICE_FULL_INPUT_LEN_NOT_WORD_SIZE_MULTIPLE = 6,
    WBOMAKDF_SLICE_INTERNAL_ERROR = 7,
    WBOMAKDF_SLICE_UNSUPPORTED_WORD_SIZE = 8,
    WBOMAKDF_SLICE_INVALID_BYTE_ORDER_IN_WORD = 9,
    WBOMAKDF_SHA_UNKNOWN_ERROR = 10,
    WBOMAKDF_SHA_OUTPUT_ERROR = 11,
    WBOMAKDF_FILE_OPEN_ERROR = 12,
    WBOMAKDF_FILE_SEEK_ERROR = 13,
    WBOMAKDF_FILE_READ_ERROR = 14,
    WBOMAKDF_FILE_CLOSE_ERROR = 15,
    WBOMAKDF_OUTPUTSIZE_TOO_SMALL = 16,
} wbomakdf_status_t;

typedef struct _wbomakdf_table_t {
    uint8_t *dmap;
    unsigned int dlen;
} wbomakdf_table_t;

#ifdef ALLOW_RUNTIME_FILEIO
int wbomakdf_read_table(
            const char * const filename,
            wbomakdf_table_t ** const to_create
        );
#endif 
int wbomakdf_dealloc_table(
            wbomakdf_table_t ** const to_dealloc
        );

int wbomakdf_kdf(
            const unsigned char* const z, 
            unsigned int z_len, 
            const unsigned int klen,
            const unsigned char* const other,
            const unsigned int other_len,
            wbsha_enc_cfg_t* const wbsha_cfg,
            const wbslice_table_t* const slice_table_in_to_in,
            const wbomakdf_table_t *const omakdf_table,
            unsigned char* const output,
            unsigned int output_len,
            unsigned int * const bytes_written
        );

typedef enum _wbomakdf_oxdsel_t {
    WBOMAKDF_INPUT_OXD_ID,
    WBOMAKDF_OUTPUT_OXD_ID
} wbomakdf_oxdsel_t;



const uint8_t * wbomakdf_get_oxdid(
            wbsha_enc_cfg_t* const wbsha_cfg,
             wbomakdf_oxdsel_t which
         );

#ifdef __cplusplus
}
#endif

#endif
