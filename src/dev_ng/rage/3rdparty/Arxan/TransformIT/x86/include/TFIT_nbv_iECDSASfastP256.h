/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_NBV_IECDSASFASTP256_H__
#define __TFIT_NBV_IECDSASFASTP256_H__
#define TFIT_nbv_iECDSASfastP256_8_0 ((uint8_t)0xb7)
#define TFIT_nbv_iECDSASfastP256_8_1 ((uint8_t)0x87)
#define TFIT_nbv_iECDSASfastP256_8_2 ((uint8_t)0x5a)
#define TFIT_nbv_iECDSASfastP256_8_3 ((uint8_t)0x3b)
#define TFIT_nbv_iECDSASfastP256_8_4 ((uint8_t)0x30)
#define TFIT_nbv_iECDSASfastP256_8_5 ((uint8_t)0x3f)
#define TFIT_nbv_iECDSASfastP256_8_6 ((uint8_t)0xac)
#define TFIT_nbv_iECDSASfastP256_8_7 ((uint8_t)0xc9)
#define TFIT_nbv_iECDSASfastP256_8_8 ((uint8_t)0x0e)
#define TFIT_nbv_iECDSASfastP256_8_9 ((uint8_t)0x94)
#define TFIT_nbv_iECDSASfastP256_8_10 ((uint8_t)0x76)
#define TFIT_nbv_iECDSASfastP256_8_11 ((uint8_t)0x3a)
#define TFIT_nbv_iECDSASfastP256_8_12 ((uint8_t)0x05)
#define TFIT_nbv_iECDSASfastP256_8_13 ((uint8_t)0x0c)
#define TFIT_nbv_iECDSASfastP256_8_14 ((uint8_t)0x75)
#define TFIT_nbv_iECDSASfastP256_8_15 ((uint8_t)0x3d)
#define TFIT_nbv_iECDSASfastP256_8_16 ((uint8_t)0xf7)
#define TFIT_nbv_iECDSASfastP256_8_17 ((uint8_t)0xaa)
#define TFIT_nbv_iECDSASfastP256_8_18 ((uint8_t)0x5f)
#define TFIT_nbv_iECDSASfastP256_8_19 ((uint8_t)0x1a)
#define TFIT_nbv_iECDSASfastP256_8_20 ((uint8_t)0xce)
#define TFIT_nbv_iECDSASfastP256_8_21 ((uint8_t)0x13)
#define TFIT_nbv_iECDSASfastP256_8_22 ((uint8_t)0xe2)
#define TFIT_nbv_iECDSASfastP256_8_23 ((uint8_t)0x58)
#define TFIT_nbv_iECDSASfastP256_8_24 ((uint8_t)0x3c)
#define TFIT_nbv_iECDSASfastP256_8_25 ((uint8_t)0x2c)
#define TFIT_nbv_iECDSASfastP256_8_26 ((uint8_t)0xef)
#define TFIT_nbv_iECDSASfastP256_8_27 ((uint8_t)0x39)
#define TFIT_nbv_iECDSASfastP256_8_28 ((uint8_t)0x4f)
#define TFIT_nbv_iECDSASfastP256_8_29 ((uint8_t)0x00)
#define TFIT_nbv_iECDSASfastP256_8_30 ((uint8_t)0x14)
#define TFIT_nbv_iECDSASfastP256_8_31 ((uint8_t)0xc5)
#define TFIT_nbv_iECDSASfastP256_16_0 ((uint16_t)0xb787)
#define TFIT_nbv_iECDSASfastP256_16_1 ((uint16_t)0x5a3b)
#define TFIT_nbv_iECDSASfastP256_16_2 ((uint16_t)0x303f)
#define TFIT_nbv_iECDSASfastP256_16_3 ((uint16_t)0xacc9)
#define TFIT_nbv_iECDSASfastP256_16_4 ((uint16_t)0x0e94)
#define TFIT_nbv_iECDSASfastP256_16_5 ((uint16_t)0x763a)
#define TFIT_nbv_iECDSASfastP256_16_6 ((uint16_t)0x050c)
#define TFIT_nbv_iECDSASfastP256_16_7 ((uint16_t)0x753d)
#define TFIT_nbv_iECDSASfastP256_16_8 ((uint16_t)0xf7aa)
#define TFIT_nbv_iECDSASfastP256_16_9 ((uint16_t)0x5f1a)
#define TFIT_nbv_iECDSASfastP256_16_10 ((uint16_t)0xce13)
#define TFIT_nbv_iECDSASfastP256_16_11 ((uint16_t)0xe258)
#define TFIT_nbv_iECDSASfastP256_16_12 ((uint16_t)0x3c2c)
#define TFIT_nbv_iECDSASfastP256_16_13 ((uint16_t)0xef39)
#define TFIT_nbv_iECDSASfastP256_16_14 ((uint16_t)0x4f00)
#define TFIT_nbv_iECDSASfastP256_16_15 ((uint16_t)0x14c5)
#define TFIT_nbv_iECDSASfastP256_32_0 ((uint32_t)0xb7875a3b)
#define TFIT_nbv_iECDSASfastP256_32_1 ((uint32_t)0x303facc9)
#define TFIT_nbv_iECDSASfastP256_32_2 ((uint32_t)0x0e94763a)
#define TFIT_nbv_iECDSASfastP256_32_3 ((uint32_t)0x050c753d)
#define TFIT_nbv_iECDSASfastP256_32_4 ((uint32_t)0xf7aa5f1a)
#define TFIT_nbv_iECDSASfastP256_32_5 ((uint32_t)0xce13e258)
#define TFIT_nbv_iECDSASfastP256_32_6 ((uint32_t)0x3c2cef39)
#define TFIT_nbv_iECDSASfastP256_32_7 ((uint32_t)0x4f0014c5)
#define TFIT_nbv_iECDSASfastP256_64_0 ((uint64_t)0xb7875a3b303facc9ULL)
#define TFIT_nbv_iECDSASfastP256_64_1 ((uint64_t)0x0e94763a050c753dULL)
#define TFIT_nbv_iECDSASfastP256_64_2 ((uint64_t)0xf7aa5f1ace13e258ULL)
#define TFIT_nbv_iECDSASfastP256_64_3 ((uint64_t)0x3c2cef394f0014c5ULL)
#endif
