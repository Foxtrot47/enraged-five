/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __WBBBI_API_H__
#define __WBBBI_API_H__ 1


#ifdef __cplusplus
extern "C" {
#endif

#include <arxstdint.h>
#include "std_bigint.h"
#include "libmontbigint.h"
typedef MONTBI_SUFFIX(montbi_int) mont_bigint_t;
typedef MONTBI_SUFFIX(montbi_ctx) mont_bigint_ctx_t;


#define MAX_ROW_COUNT MAX_BIGINT_PRODUCT
#define MAX_COLUMN_COUNT 2
#define MAX_TRIES 1000
#define WB_BBI_VERSION_1_00   0x00010000
#define WB_BBI_VERSION_2_00   0x00020000

extern const char WB_BBI_MAGIC[4];


typedef struct boxy_bigint_type {
  uint32_t skew_index;
  uint32_t box[MAX_ROW_COUNT][MAX_COLUMN_COUNT][2];
  const struct boxy_bigint_context *ctx;
} boxy_bigint_t;


typedef struct boxy_bigint_context {
  uint8_t magic[4];
  uint32_t version;
  uint32_t skew_table_length;
  uint32_t column_count;
  uint32_t row_count;
  uint32_t num_ksps;
  uint32_t modlen;
  std_bigint_t modbi;
  modulus_t mod;
  std_bigint_t inv_exp;
  mont_bigint_t* k_tbl;
  mont_bigint_t* ksps_tbl;
  std_bigint_t* ssps_tbl;
  uint32_t* sws_tbl;
  uint64_t* rs_tbl;
  mont_bigint_t ki;
  boxy_bigint_t o;
  boxy_bigint_t pow;
  mont_bigint_ctx_t bama;
  uint8_t oxdid[16];
  uint32_t xtbl_sz;
  uint8_t* xtbl;
  uint32_t signature_len;
  uint8_t signature[16];
} boxy_context_t;




typedef enum _wbbbi_status_t
{
   WBBBI_OK = 0,
   WBBBI_NULL_ARG = 1,
   WBBBI_INTERNAL_ERROR = 2,
   WBBBI_FILE_OPEN_ERROR = 3,
   WBBBI_FILE_WRITE_ERROR = 4,
   WBBBI_FILE_CLOSE_ERROR = 5,
   WBBBI_FILE_SEEK_ERROR = 6,
   WBBBI_MEMORY_ALLOCATION_ERROR = 7,
   WBBBI_UNEXPECTED_BINARY_FILE_SIZE = 8,
   WBBBI_BAD_MAGIC = 9,
   WBBBI_UNSUPPORTED_VERSION = 10,
   WBBBI_DESERIALIZE_ERROR = 11,
   WBBBI_NO_MEM = 12,
   WBBBI_PRNG_FAIL = 13,
   WBBBI_COMPUTATION_ERROR = 14,
   WBBBI_MAX_RETRIES = 15
} wbbbi_status_t;




int wbbbi_validate_boxy_context( boxy_context_t* ctx );
int wbbbi_dealloc_boxy_context( boxy_context_t** to_dealloc );
#ifdef ALLOW_RUNTIME_FILEIO
int wbbbi_read_serialized_boxy_context_from_file( const char* filename,
                                                  boxy_context_t** to_create );
#endif 
const uint8_t* wbbbi_get_oxdid( const boxy_context_t* ctx );
const uint8_t* get_oxdid_from_serialized_boxy_context( const uint8_t* buf,
                                                       uint32_t buf_length );
int wbbbi_deserialize_boxy_context( boxy_context_t* ctx,
                                    const uint8_t* buf,
                                    uint32_t buf_length );
boxy_context_t* deserialize_boxy_context( boxy_context_t* ctx,
                                          const uint8_t* buf,
                                          uint32_t buf_length,
                                          uint32_t* bytes_read );
void free_boxy_context( boxy_context_t* arg );




unsigned int montbi_to_sbint( std_bigint_t* dest, const mont_bigint_t* src,
                              const mont_bigint_ctx_t* mctx,
                              uint32_t bigint_size );
unsigned int bbint_zeroize( boxy_bigint_t* dest,
                            const boxy_context_t* ctx );
unsigned int bbint_copy_constructor( boxy_bigint_t* dest,
                                     const boxy_bigint_t* src );
unsigned int bbint_serlen( const boxy_bigint_t* src );
unsigned int bbint_ser( uint8_t* dest,
                        uint32_t max_len,
                        const boxy_bigint_t* src );
unsigned int bbint_deser( boxy_bigint_t* dest,
                          const uint8_t* buf,
                          uint32_t buf_len,
                          const boxy_context_t* ctx );
unsigned int bbint_add( boxy_bigint_t* result,
                        const boxy_bigint_t* op_left,
                        const boxy_bigint_t* op_right );
mont_bigint_t* bbint_flatten( mont_bigint_t* in_ptr,
                              const boxy_bigint_t* arg );
unsigned int bbint_box( boxy_bigint_t* result,
                        uint32_t id,
                        const mont_bigint_t* fx );
unsigned int bbint_multiply( boxy_bigint_t* result,
                             const boxy_bigint_t* op_left,
                             const boxy_bigint_t* op_right );
unsigned int bbint_reduce( boxy_bigint_t* result );




int bbint_f( const boxy_context_t* ctx,
             const uint8_t* xx,
             boxy_bigint_t* R );
int bbint_f2( const boxy_context_t* ctx,
              const uint8_t* yy,
              const uint8_t* xx,
              boxy_bigint_t* R );
int bbint_fi( const boxy_bigint_t* p, uint8_t* R );


#ifdef __cplusplus
}
#endif

#endif

