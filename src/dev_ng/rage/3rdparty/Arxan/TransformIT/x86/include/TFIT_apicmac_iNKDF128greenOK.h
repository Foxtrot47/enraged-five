/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

      

      

      


#ifndef __TFIT_APICMAC_INKDF128GREENOK_H__
#define __TFIT_APICMAC_INKDF128GREENOK_H__

#include "wbaes_api.h"

#ifdef __cplusplus
extern "C" {
#endif

int TFIT_opcmac_iNKDF128greenOK( wbaes_inner_func *wbop, const unsigned char *keys[2], const unsigned char src[16], size_t octet_len, unsigned char iv[16], unsigned char input_opt, size_t output_octet_len, unsigned char dest[16]);
extern const unsigned char TFIT_wrap_iNKDF128greenOK[];
extern const wbaes_wrapper_t TFIT_apicmac_iNKDF128greenOK;

#ifdef __cplusplus
}
#endif

#endif 


