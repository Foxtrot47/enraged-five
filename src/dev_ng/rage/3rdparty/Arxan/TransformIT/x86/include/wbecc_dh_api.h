/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_DH_API_H__
    #define __WBECC_DH_API_H__

    #include <arxstdint.h>
    #include "wbecc.h"
    #include "wbecc_ecc_alu.h"
    #include "wbecc_encoding_types.h"
    #include "wbecc_common_api.h"
    #include "wbecc_dh_api_tbl.h"
    #include "wbecc_dh_api_fst.h"

    #ifdef __cplusplus
        extern "C"
        {
    #endif

    
    typedef enum _wbecc_dh_status_t {
        WBECC_DH_CDH_ERROR = -2,
        WBECC_DH_INTERNAL_ARITHMETIC_ERROR = -1, 
        WBECC_DH_OK = 0,
        WBECC_DH_NULL_PARAM = 1,
        WBECC_DH_OBFUSCATED_OXDOUT_INVALID = 2,
        WBECC_DH_CLASSICAL_OXDOUT_INVALID = 3,
        WBECC_DH_FST_PREPARE_CONSTANTS_ERROR = 4, 
        WBECC_DH_FST_TABLE_KEY_PROVIDED = 5,
        WBECC_DH_DO_NOT_HAVE_STATIC_KEY = 6,
        WBECC_DH_DO_NOT_HAVE_EPHEMERAL_KEY = 7,
        WBECC_DH_OUTPUT_BUFFER_TOO_SMALL = 8,
        WBECC_DH_GET_EPHEMERAL_FAILURE = 9,
        WBECC_DH_PARSE_AFFINE_POINT_FAILURE = 10,
        WBECC_DH_FULL_UNIFIED_NULL_PUBLIC_KEY = 11,
        WBECC_DH_FULL_UNIFIED_MISSING_EPHEMERAL = 12,
        WBECC_DH_FULL_UNIFIED_MISSING_STATIC = 13,
        WBECC_DH_UNIFIED_NULL_PUBLIC_KEY = 15,
        WBECC_DH_UNIFIED_MISSING_EPHEMERAL = 16,
        WBECC_DH_ONE_PASS_UNIFIED_NULL_PUBLIC_KEY = 17,
        WBECC_DH_ONE_PASS_UNIFIED_MISSING_EPHEMERAL = 18,
        WBECC_DH_ONE_PASS_UNIFIED_MISSING_STATIC = 19,
        WBECC_DH_ONE_PASS_NULL_PUBLIC_KEY = 20,
        WBECC_DH_ONE_PASS_MISSING_EPHEMERAL = 21,
        WBECC_DH_STATIC_UNIFIED_NULL_PUBLIC_KEY = 22,
        WBECC_DH_STATIC_UNIFIED_MISSING_STATIC = 23,
        WBECC_DH_INTERNAL_ERROR_UNKNOWN_MODE = 24,
        WBECC_DH_POINT_NOT_ON_CURVE = 25,
        WBECC_DH_OBFUSCATED_OUT_CONVERSION_FAILURE = 26,
        WBECC_DH_DOMAIN_PARAMS_INCOMPATIBLE_WITH_INSTANCE = 27,
        WBECC_DH_INVALID_NUM_ENCODINGS = 28,
        WBECC_DH_KEY_INCOMPATIBLE_WITH_INSTANCE = 29,
        WBECC_DH_FST_TABLE_NOT_KEY_PROVIDED = 30,
        WBECC_DH_UNSUPPORTED_MODE = 31,
        WBECC_DH_EPHEMERAL_UNIFIED_NULL_PUBLIC_KEY = 32,
        WBECC_DH_EPHEMERAL_UNIFIED_MISSING_EPHEMERAL = 33,
        WBECC_DH_ONE_PASS_UNIFIED_UNSUPPORTED_MODE = 34,
        WBECC_DH_ONE_PASS_UNSUPPORTED_MODE = 35,
        WBECC_DH_OXD_IN_PLAINTEXT = 36,
        WBECC_DH_FST_MISSING_TABLE_CONFIG = 37,
        WBECC_DH_KEYS_ARE_NEWER_FORMAT = 38,
        WBECC_DH_PMULT_FAILURE = 39,
    } wbecc_dh_status_t;
    
    

    typedef int (*wbecc_dh_get_ephemeral_data) (const unsigned int, wbecc_enc_t * const, const wbecc_enc_cfg * const);

    typedef struct _wbecc_dh_ctx {
        uint8_t table;
        union {
            wbecc_dh_ctx_tbl_t tbl;
            wbecc_dh_ctx_fst_t fst;
        } u;
    } wbecc_dh_ctx_t;

    

    int wbecc_dh_init(
        wbecc_dh_ctx_t * const ctx, 
        wbecc_obf_t obf_out,
        const wbecc_key_pair_t * const wbecc_static_key, 
        const wbecc_table_curve_t * const tcurve,
        wbecc_dh_get_ephemeral_data get_ephemeral,
        wbecc_enc_cfg * const wbecc_cfg
    );

    int wbecc_dh_init_fst(
        wbecc_dh_ctx_t * const ctx, 
        wbecc_obf_t obf_out,
        const wbecc_key_pair_t * const wbecc_static_key, 
        wbecc_dh_get_ephemeral_data get_ephemeral,
        const wbecc_fst_funcs_t * const funcs,
        const wbecc_domain_host_t * const domain
    );
    int wbecc_dh_fst_cleanup(
        wbecc_dh_ctx_t * const ctx
    );

    

    int wbecc_dh_get_static_public_key(
        const wbecc_dh_ctx_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    

    int wbecc_dh_montgomery_get_static_public_key(
        const wbecc_dh_ctx_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written,
        const uint8_t * A, unsigned int Alen, const uint8_t * B, unsigned int Blen
    );
    
    

    int wbecc_dh_get_static_public_key_curve25519(
        const wbecc_dh_ctx_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    

    int wbecc_dh_get_ephemeral_public_key(
        const wbecc_dh_ctx_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    

    int wbecc_dh_montgomery_get_ephemeral_public_key(
        const wbecc_dh_ctx_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written,
        const uint8_t * A, unsigned int Alen, const uint8_t * B, unsigned int Blen
    );

    

    int wbecc_dh_get_ephemeral_public_key_curve25519(
        const wbecc_dh_ctx_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    

    int wbecc_dh_generate_key(
        wbecc_dh_ctx_t * const ctx
    );
    
   typedef enum _ecc_dh_mode_t {
      WBECC_DH_FULL_UNIFIED_MODEL,
      WBECC_DH_EPHEMERAL_UNIFIED_MODEL,
      WBECC_DH_ONE_PASS_UNIFIED_MODEL_INITIATOR,
      WBECC_DH_ONE_PASS_UNIFIED_MODEL_RESPONDER,
      WBECC_DH_ONE_PASS_DIFFIE_HELLMAN_INITIATOR,
      WBECC_DH_ONE_PASS_DIFFIE_HELLMAN_RESPONDER,
      WBECC_DH_STATIC_UNIFIED_MODEL
   } ecc_dh_mode_t;

   
    

    int wbecc_dh_compute_secret(
        wbecc_dh_ctx_t*  const ctx,
        ecc_dh_mode_t mode,
        const uint8_t* const static_public_key,
        const uint8_t* const ephemeral_public_key,
        unsigned int public_key_len,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );

    const uint8_t * wbecc_dh_get_oxdid(
        const wbecc_dh_ctx_t * const ctx,
        wbecc_oxdsel_t which
    );

    #ifdef __cplusplus
        }
    #endif
#endif
