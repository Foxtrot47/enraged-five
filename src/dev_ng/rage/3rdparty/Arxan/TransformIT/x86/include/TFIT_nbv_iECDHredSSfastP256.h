/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_NBV_IECDHREDSSFASTP256_H__
#define __TFIT_NBV_IECDHREDSSFASTP256_H__
#define TFIT_nbv_iECDHredSSfastP256_8_0 ((uint8_t)0x41)
#define TFIT_nbv_iECDHredSSfastP256_8_1 ((uint8_t)0x5e)
#define TFIT_nbv_iECDHredSSfastP256_8_2 ((uint8_t)0x02)
#define TFIT_nbv_iECDHredSSfastP256_8_3 ((uint8_t)0xee)
#define TFIT_nbv_iECDHredSSfastP256_8_4 ((uint8_t)0x1e)
#define TFIT_nbv_iECDHredSSfastP256_8_5 ((uint8_t)0x3f)
#define TFIT_nbv_iECDHredSSfastP256_8_6 ((uint8_t)0xcd)
#define TFIT_nbv_iECDHredSSfastP256_8_7 ((uint8_t)0x6d)
#define TFIT_nbv_iECDHredSSfastP256_8_8 ((uint8_t)0xb3)
#define TFIT_nbv_iECDHredSSfastP256_8_9 ((uint8_t)0x13)
#define TFIT_nbv_iECDHredSSfastP256_8_10 ((uint8_t)0xa9)
#define TFIT_nbv_iECDHredSSfastP256_8_11 ((uint8_t)0x5f)
#define TFIT_nbv_iECDHredSSfastP256_8_12 ((uint8_t)0xf1)
#define TFIT_nbv_iECDHredSSfastP256_8_13 ((uint8_t)0x64)
#define TFIT_nbv_iECDHredSSfastP256_8_14 ((uint8_t)0xd2)
#define TFIT_nbv_iECDHredSSfastP256_8_15 ((uint8_t)0xe4)
#define TFIT_nbv_iECDHredSSfastP256_8_16 ((uint8_t)0x95)
#define TFIT_nbv_iECDHredSSfastP256_8_17 ((uint8_t)0x94)
#define TFIT_nbv_iECDHredSSfastP256_8_18 ((uint8_t)0xd9)
#define TFIT_nbv_iECDHredSSfastP256_8_19 ((uint8_t)0x74)
#define TFIT_nbv_iECDHredSSfastP256_8_20 ((uint8_t)0x9e)
#define TFIT_nbv_iECDHredSSfastP256_8_21 ((uint8_t)0x40)
#define TFIT_nbv_iECDHredSSfastP256_8_22 ((uint8_t)0xfb)
#define TFIT_nbv_iECDHredSSfastP256_8_23 ((uint8_t)0x1e)
#define TFIT_nbv_iECDHredSSfastP256_8_24 ((uint8_t)0xaf)
#define TFIT_nbv_iECDHredSSfastP256_8_25 ((uint8_t)0x6c)
#define TFIT_nbv_iECDHredSSfastP256_8_26 ((uint8_t)0xf7)
#define TFIT_nbv_iECDHredSSfastP256_8_27 ((uint8_t)0xdb)
#define TFIT_nbv_iECDHredSSfastP256_8_28 ((uint8_t)0xe9)
#define TFIT_nbv_iECDHredSSfastP256_8_29 ((uint8_t)0x3f)
#define TFIT_nbv_iECDHredSSfastP256_8_30 ((uint8_t)0xa9)
#define TFIT_nbv_iECDHredSSfastP256_8_31 ((uint8_t)0xbc)
#define TFIT_nbv_iECDHredSSfastP256_16_0 ((uint16_t)0x415e)
#define TFIT_nbv_iECDHredSSfastP256_16_1 ((uint16_t)0x02ee)
#define TFIT_nbv_iECDHredSSfastP256_16_2 ((uint16_t)0x1e3f)
#define TFIT_nbv_iECDHredSSfastP256_16_3 ((uint16_t)0xcd6d)
#define TFIT_nbv_iECDHredSSfastP256_16_4 ((uint16_t)0xb313)
#define TFIT_nbv_iECDHredSSfastP256_16_5 ((uint16_t)0xa95f)
#define TFIT_nbv_iECDHredSSfastP256_16_6 ((uint16_t)0xf164)
#define TFIT_nbv_iECDHredSSfastP256_16_7 ((uint16_t)0xd2e4)
#define TFIT_nbv_iECDHredSSfastP256_16_8 ((uint16_t)0x9594)
#define TFIT_nbv_iECDHredSSfastP256_16_9 ((uint16_t)0xd974)
#define TFIT_nbv_iECDHredSSfastP256_16_10 ((uint16_t)0x9e40)
#define TFIT_nbv_iECDHredSSfastP256_16_11 ((uint16_t)0xfb1e)
#define TFIT_nbv_iECDHredSSfastP256_16_12 ((uint16_t)0xaf6c)
#define TFIT_nbv_iECDHredSSfastP256_16_13 ((uint16_t)0xf7db)
#define TFIT_nbv_iECDHredSSfastP256_16_14 ((uint16_t)0xe93f)
#define TFIT_nbv_iECDHredSSfastP256_16_15 ((uint16_t)0xa9bc)
#define TFIT_nbv_iECDHredSSfastP256_32_0 ((uint32_t)0x415e02ee)
#define TFIT_nbv_iECDHredSSfastP256_32_1 ((uint32_t)0x1e3fcd6d)
#define TFIT_nbv_iECDHredSSfastP256_32_2 ((uint32_t)0xb313a95f)
#define TFIT_nbv_iECDHredSSfastP256_32_3 ((uint32_t)0xf164d2e4)
#define TFIT_nbv_iECDHredSSfastP256_32_4 ((uint32_t)0x9594d974)
#define TFIT_nbv_iECDHredSSfastP256_32_5 ((uint32_t)0x9e40fb1e)
#define TFIT_nbv_iECDHredSSfastP256_32_6 ((uint32_t)0xaf6cf7db)
#define TFIT_nbv_iECDHredSSfastP256_32_7 ((uint32_t)0xe93fa9bc)
#define TFIT_nbv_iECDHredSSfastP256_64_0 ((uint64_t)0x415e02ee1e3fcd6dULL)
#define TFIT_nbv_iECDHredSSfastP256_64_1 ((uint64_t)0xb313a95ff164d2e4ULL)
#define TFIT_nbv_iECDHredSSfastP256_64_2 ((uint64_t)0x9594d9749e40fb1eULL)
#define TFIT_nbv_iECDHredSSfastP256_64_3 ((uint64_t)0xaf6cf7dbe93fa9bcULL)
#endif
