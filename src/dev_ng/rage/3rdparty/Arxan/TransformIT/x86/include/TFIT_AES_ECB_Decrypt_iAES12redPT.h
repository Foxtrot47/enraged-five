/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES-ECB/Decrypt Instance=iAES12redPT
 */

#ifndef __TFIT_AES_ECB_DECRYPT_iAES12redPT_H__
#define __TFIT_AES_ECB_DECRYPT_iAES12redPT_H__

#include "TFIT.h"

#include "TFIT_defs_iAES12redPT.h"
#include "wrapper_modes.h"
#define TFIT_init_wbaes_ecb_iAES12redPT(ctx, wbaes_key) \
    wbaes_init_ecb(ctx, WBAES_DECRYPT, WBAES_CLASSICAL, WBAES_OBFUSCATED, &TFIT_aes_ecb_iAES12redPT, wbaes_key)

#define TFIT_update_wbaes_iAES12redPT wbaes_update

#define TFIT_update_slice_wbaes_iAES12redPT wbaes_update_slice

#define TFIT_final_wbaes_iAES12redPT wbaes_final

#define TFIT_validate_wb_key_iAES12redPT TFIT_validate_key_id_iAES12redPT

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iAES12redPT_<colour> */
#define TFIT_prepare_dynamic_key_iAES12redPT(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)



#ifdef __cplusplus
extern "C"
#endif

int TFIT_wbaes_ecb_decrypt_iAES12redPT(const TFIT_key_iAES12redPT_t *wbaes_key, 
                                                const unsigned char *input, 
                                                const size_t input_len, 
                                                unsigned char *output);

#endif /* __TFIT_AES_ECB_DECRYPT_iAES12redPT_H__ */
