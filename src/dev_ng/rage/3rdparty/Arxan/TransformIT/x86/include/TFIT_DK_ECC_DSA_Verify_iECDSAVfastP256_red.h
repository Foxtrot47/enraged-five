/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: Dynamic-Key for Instance=iECDSAVfastP256_red
 */

#ifndef __TFIT_DK_iECDSAVfastP256_red_H__
#define __TFIT_DK_iECDSAVfastP256_red_H__

#include "TFIT.h"
#include "wbecc_common_api.h"

extern
#ifdef __cplusplus
"C"
#endif
int TFIT_prepare_dynamic_key_iECDSAVfastP256_red(const uint8_t * const input,
                                        unsigned int input_len,
                                        wbecc_key_pair_t * const key_pair);

#endif /* __TFIT_DK_iECDSAVfastP256_red_H__ */
