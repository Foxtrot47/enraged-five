/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: ECC/DH Instance=iECDHredKgreenSS
 */

#ifndef __TFIT_ECC_DH_iECDHredKgreenSS_H__
#define __TFIT_ECC_DH_iECDHredKgreenSS_H__

#include "TFIT.h"

#include "wbecc_dh_api.h"
#include "TFIT_generated_ecc_encodings_iECDHredKgreenSS.h"

#define TFIT_prepare_dynamic_key_iECDHredKgreenSS_red(tcurve, input, input_len, key_pair) \
    wbecc_prepare_key(tcurve, &TFIT_ecc_cfg_iECDHredKgreenSS, input, input_len, key_pair)

#define TFIT_wbecc_dh_get_public_key_iECDHredKgreenSS(tcurve, key_pair, output, output_len, bytes_written) \
    wbecc_get_public_key(tcurve, &TFIT_ecc_cfg_iECDHredKgreenSS, key_pair, output, output_len, bytes_written)

#define TFIT_init_wbecc_dh_iECDHredKgreenSS(ctx, wbecc_static_key, curve_param, get_ephemeral) \
    wbecc_dh_init(ctx, WBECC_OBFUSCATED, wbecc_static_key, curve_param, get_ephemeral, &TFIT_ecc_cfg_iECDHredKgreenSS)

#define TFIT_wbecc_dh_get_static_public_key_iECDHredKgreenSS wbecc_dh_get_static_public_key
#define TFIT_wbecc_dh_get_static_public_key_curve25519_iECDHredKgreenSS wbecc_dh_get_static_public_key_curve25519
#define TFIT_wbecc_dh_get_ephemeral_public_key_iECDHredKgreenSS wbecc_dh_get_ephemeral_public_key
#define TFIT_wbecc_dh_get_ephemeral_public_key_curve25519_iECDHredKgreenSS wbecc_dh_get_ephemeral_public_key_curve25519
#define TFIT_wbecc_dh_generate_key_iECDHredKgreenSS wbecc_dh_generate_key
#define TFIT_wbecc_dh_compute_secret_iECDHredKgreenSS  wbecc_dh_compute_secret

#endif /* __TFIT_ECC_DH_iECDHredKgreenSS_H__ */
