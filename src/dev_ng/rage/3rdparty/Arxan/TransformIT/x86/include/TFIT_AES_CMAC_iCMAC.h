/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES/CMAC Instance=iCMAC
 */

#ifndef __TFIT_AES_CMAC_iCMAC_H__
#define __TFIT_AES_CMAC_iCMAC_H__

#include "TFIT.h"

#include "TFIT_defs_iCMAC.h"
#define TFIT_update_wbaes_iCMAC wbaes_update

#define TFIT_update_slice_wbaes_iCMAC wbaes_update_slice

#define TFIT_final_wbaes_iCMAC wbaes_final

#define TFIT_validate_wb_key_iCMAC TFIT_validate_key_id_iCMAC

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iCMAC_<colour> */
#define TFIT_prepare_dynamic_key_iCMAC(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, &TFIT_op_iCMAC, out_len)


#include "wrapper_modes.h"
#include "wrapper_modes.h"

#define TFIT_init_wbaes_cmac_iCMAC(ctx, wbaes_key, digest_size)                 \
    wbaes_init_cmac(ctx, WBAES_CLASSICAL, WBAES_CLASSICAL, &TFIT_aes_ecb_iCMAC, wbaes_key, &wbw_apicmac_classical, NULL, digest_size)
#define TFIT_wbaes_cmac_iCMAC(wbaes_key, input, input_len, output, output_len)  \
    aes_cmac((fn_wbaes_op_t *)&TFIT_op_iCMAC, wbaes_key, input, input_len, output, output_len)

#endif /* __TFIT_AES_CMAC_iCMAC_H__ */
