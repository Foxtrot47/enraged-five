/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: XLAT/XOR Instance=iXORredXIgreenXM
 */

#ifndef __TFIT_XLAT_XOR_iXORredXIgreenXM_H__
#define __TFIT_XLAT_XOR_iXORredXIgreenXM_H__

#include "TFIT.h"

#include "TFIT_apixor_iXORredXIgreenXM.h"
#include "TFIT_xlatdef_iXORredXIgreenXM.h"
#include "TFIT_xlat_iXORredXIgreenXM.h"

#define TFIT_init_xlat_xor_iXORredXIgreenXM(ctx) \
    wbxlat_init_xor(ctx, WBXLAT_ENCODE, WBXLAT_OBFUSCATED, WBXLAT_CLASSICAL, WBXLAT_OBFUSCATED, &TFIT_apixor_iXORredXIgreenXM, &TFIT_xlat_iXORredXIgreenXM)
#define TFIT_update_xlat_xor_iXORredXIgreenXM wbxlat_update
#define TFIT_final_xlat_xor_iXORredXIgreenXM wbxlat_final

#define TFIT_xlat_xor_iXORredXIgreenXM(src, len, mask, dest) \
    TFIT_opxor_iXORredXIgreenXM((const unsigned char*)&TFIT_xlat_iXORredXIgreenXM, src, len, mask, dest)

#endif /* __TFIT_XLAT_XOR_iXORredXIgreenXM_H__ */
