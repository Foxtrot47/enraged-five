/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_ECC_FAST_PREPARE_KEY_IECDSASFASTP256_GREEN_KSL8__
#define __TFIT_ECC_FAST_PREPARE_KEY_IECDSASFASTP256_GREEN_KSL8__

#if defined(__cplusplus)
extern "C" {
#endif

#include <arxstdint.h>
#include <string.h>
#include "wbecc_common_api.h"
#include "wb_ecc_static.h"
#include "wb_ecc_montbigint.h"

#define WBECC_FAST_PREPARE_KEY_SUCCESS         0
#define WBECC_FAST_PREPARE_KEY_INPUT_TOO_SHORT 1
#define WBECC_FAST_PREPARE_KEY_INPUT_TOO_LONG  2
#define WBECC_FAST_PREPARE_KEY_ILLEGAL_ARG     3
#define WBECC_FAST_PREPARE_KEY_REDUCE_FAILED   4
#define WBECC_FAST_PREPARE_KEY_PMULT_FAILED    5

/* prepares a whitebox-ecc key pair from obfuscated
 * private key.  This key is prepared specifically
 * for the WhiteboxECC instance to which this routine
 * belongs */
int TFIT_ecc_fast_prepare_key_iECDSASfastP256_green_KSL8(const uint8_t *input, unsigned int input_len,
  const wbecc_fst_funcs_t *cfg, const wbecc_domain_host_t* domain, 
  const void * constants, //const wb_ecc_constants* constants, 
  struct _wbecc_key_pair *out);

#if defined(__cplusplus)
}
#endif

#endif /*__TFIT_ECC_FAST_PREPARE_KEY_IECDSASFASTP256_GREEN_KSL8__*/

