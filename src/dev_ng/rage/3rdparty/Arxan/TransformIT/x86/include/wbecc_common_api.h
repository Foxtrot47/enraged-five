/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_COMMON_API_H__
    #define __WBECC_COMMON_API_H__

    #include <arxstdint.h>
    #include "wbecc_ecc_alu.h"
    #include "wbecc_encoding_types.h"
    #include "wb_ecc_static.h"

    #ifdef __cplusplus
        extern "C"
        {
    #endif

    
    typedef enum _wbecc_common_status_t {
        WBECC_COMMON_INTERNAL_ARITHMETIC_ERROR = -1,
        WBECC_COMMON_OK = 0,
        WBECC_COMMON_NULL_PARAM = 1,
        WBECC_COMMON_OUTPUT_BUFFER_TOO_SMALL = 2,
        WBECC_COMMON_FAST_KEY_PROVIDED = 3,
        WBECC_COMMON_TABLE_KEY_PROVIDED = 4,
        WBECC_COMMON_INPUT_LEN_INCORRECT = 5,
		  WBECC_COMMON_OXD_IN_PLAINTEXT = 6
    } wbecc_common_status_t;
    
    

    #define WBECC_KEY_PAIR_TYPE_TABLE_MASK    0x01
    #define WBECC_KEY_PAIR_TYPE_TABLE_SHFT       0
    #define WBECC_KEY_PAIR_TYPE_EXTENDED_MASK 0x02
    #define WBECC_KEY_PAIR_TYPE_EXTENDED_SHFT    1

    #define WBECC_KEY_PAIR_TYPE_IS_TABLE(type) \
      ((type) & WBECC_KEY_PAIR_TYPE_TABLE_MASK)
    #define WBECC_KEY_PAIR_TYPE_IS_EXTENDED(type) \
      ((type) & WBECC_KEY_PAIR_TYPE_EXTENDED_MASK)

    #define WBECC_KEY_PAIR_TYPE_SET_TABLE(type, boolVal) \
      (((type) & ~WBECC_KEY_PAIR_TYPE_TABLE_MASK) | \
        (boolVal ? WBECC_KEY_PAIR_TYPE_TABLE_MASK : 0))

    #define WBECC_KEY_PAIR_TYPE_SET_EXTENDED(type, boolVal) \
      (((type) & ~WBECC_KEY_PAIR_TYPE_EXTENDED_MASK) | \
        (boolVal ? WBECC_KEY_PAIR_TYPE_EXTENDED_MASK : 0))

    #define WBECC_KEY_PAIR_TYPE_FST_S_MASK    0x7c
    #define WBECC_KEY_PAIR_TYPE_FST_S_SHFT       2
    #define WBECC_KEY_PAIR_TYPE_FST_FREE_MASK 0x80
    #define WBECC_KEY_PAIR_TYPE_FST_FREE_SHFT    5

    #define WBECC_KEY_PAIR_TYPE_FST_GET_S(params) \
      (((params) & WBECC_KEY_PAIR_TYPE_FST_S_MASK) \
        >> WBECC_KEY_PAIR_TYPE_FST_S_SHFT)

    #define WBECC_KEY_PAIR_TYPE_FST_SET_S(params, S) \
      (((params) & ~WBECC_KEY_PAIR_TYPE_FST_S_MASK) \
        | (((S) << WBECC_KEY_PAIR_TYPE_FST_S_SHFT) \
          & WBECC_KEY_PAIR_TYPE_FST_S_MASK))

    #define WBECC_KEY_PAIR_TYPE_TBL_FREE_MASK 0xfc
    #define WBECC_KEY_PAIR_TYPE_TBL_FREE_SHFT    2

    typedef struct _wbecc_key_pair {
        wbecc_enc_t d[MAX_WBECC_ENC_T_SIZE];
        wbecc_affine_point_t q;
        uint8_t dlen;
        uint8_t inst_uuid[16];
        

        uint8_t type; 
    } wbecc_key_pair_t;

    int wbecc_get_public_key(
        const wbecc_table_curve_t * const tcurve,
        wbecc_enc_cfg * const wbecc_cfg,
        const wbecc_key_pair_t * const key_pair,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    int wbecc_montgomery_get_public_key(
        const wbecc_table_curve_t * const tcurve,
        wbecc_enc_cfg * const wbecc_cfg,
        const wbecc_key_pair_t * const key_pair,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written,
        const uint8_t * A,
        unsigned int Alen,
        const uint8_t * B,
        unsigned int Blen
    );

    int wbecc_get_public_key_fst(
        const wbecc_key_pair_t * const key_pair,
        const wbecc_fst_funcs_t * const funcs,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    int wbecc_montgomery_get_public_key_fst(
        const wbecc_key_pair_t * const key_pair,
        const wbecc_fst_funcs_t * const funcs,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written,
        const uint8_t * A,
        unsigned int Alen,
        const uint8_t * B,
        unsigned int Blen
    );
    
    int wbecc_prepare_key(
        const wbecc_table_curve_t * const tcurve,
        wbecc_enc_cfg * const wbecc_cfg,
        const uint8_t * const input,
        unsigned int input_len,
        wbecc_key_pair_t * const key_pair
    );

    int wbecc_wr_to_mg_convert_x(
        const wbecc_enc_t * const p,
        unsigned int plen,
        wbecc_enc_cfg * const wbecc_cfg,
        wbecc_enc_t * const output,
        const wbecc_enc_t * const input,
        const uint8_t * const A,
        unsigned int Alen,
        const uint8_t * const B,
        unsigned int Blen);
            
    #ifdef __cplusplus
        }
    #endif
#endif
