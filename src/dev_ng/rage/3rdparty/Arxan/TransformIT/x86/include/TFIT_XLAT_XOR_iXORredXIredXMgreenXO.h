/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: XLAT/XOR Instance=iXORredXIredXMgreenXO
 */

#ifndef __TFIT_XLAT_XOR_iXORredXIredXMgreenXO_H__
#define __TFIT_XLAT_XOR_iXORredXIredXMgreenXO_H__

#include "TFIT.h"

#include "TFIT_apixor_iXORredXIredXMgreenXO.h"
#include "TFIT_xlatdef_iXORredXIredXMgreenXO.h"
#include "TFIT_xlat_iXORredXIredXMgreenXO.h"

#define TFIT_init_xlat_xor_iXORredXIredXMgreenXO(ctx) \
    wbxlat_init_xor(ctx, WBXLAT_ENCODE, WBXLAT_OBFUSCATED, WBXLAT_OBFUSCATED, WBXLAT_OBFUSCATED, &TFIT_apixor_iXORredXIredXMgreenXO, &TFIT_xlat_iXORredXIredXMgreenXO)
#define TFIT_update_xlat_xor_iXORredXIredXMgreenXO wbxlat_update
#define TFIT_final_xlat_xor_iXORredXIredXMgreenXO wbxlat_final

#define TFIT_xlat_xor_iXORredXIredXMgreenXO(src, len, mask, dest) \
    TFIT_opxor_iXORredXIredXMgreenXO((const unsigned char*)&TFIT_xlat_iXORredXIredXMgreenXO, src, len, mask, dest)

#endif /* __TFIT_XLAT_XOR_iXORredXIredXMgreenXO_H__ */
