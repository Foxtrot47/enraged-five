/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __STD_ECC_H__
    #define __STD_ECC_H__

    #include "std_bigint.h"
    
    #ifdef __cplusplus
        extern "C"
        {
    #endif

    #include "nac_max_sizes.h"
    
    
    #define MAX_BIGINT_ECC_PRODUCT_BUFFER \
      (2 * ((MAX_FIELD_ORDER_BYTES + sizeof(uint32_t) - 1) \
        / sizeof(uint32_t)))

    #define MAX_BIGINT_ECC_BUFFER \
      ((MAX_BIGINT_ECC_PRODUCT_BUFFER > MAX_CURVE_ORDER_BYTES) \
      ? MAX_BIGINT_ECC_PRODUCT_BUFFER  \
      : MAX_CURVE_ORDER_BYTES)

    
    typedef enum _stdecc_status_t {
        STDECC_OK = 0,
        STDECC_SBINT_INITIALIZE_ERROR,
        STDECC_AFFINIFY_INFINITY,
        STDECC_AFFINIFY_SBINT_INVERSE_ERROR,
        STDECC_SBINT_MULTIPLY_AND_REDUCE_ERROR,
        STDECC_SBINT_ADD_AND_REDUCE_ERROR,
        STDECC_SBINT_SUBTRACT_AND_REDUCE_ERROR,
        STDECC_DOUBLE_ERROR,
        STDECC_FULL_ADD_ERROR,
        STDECC_LAST_CODE
    } stdecc_status_t;

    
    static const char * const stdecc_status_strings[STDECC_LAST_CODE] = {
        "stdecc success",
        "stdecc error: initializing a sbint value",
        "stdecc error: can't affinify the point at infinity",
        "stdecc error: failed to find inverse with sbint",
        "stdecc error: failed to multiply and reduce with sbint",
        "stdecc error: failed to add and reduce with sbint",
        "stdecc error: failed to subtract and reduce with sbint",
        "stdecc error: failed to double within stdecc_multiply",
        "stdecc error: failed to full_add within stdecc_multiply",
    };
    const char * stdecc_get_status_string( int status );
    
    typedef struct _STDECC_AFFINE_POINT {
        std_bigint_t x;
        std_bigint_t y;
    } stdecc_affine_point_t;

    typedef struct _STDECC_PROJECTIVE_POINT {
        std_bigint_t x;
        std_bigint_t y;
        std_bigint_t z;
    } stdecc_projective_point_t;
    
    typedef struct _STDECC_CURVE {
        modulus_t p;
        std_bigint_t a;
        std_bigint_t b;
        std_bigint_t mod;
        unsigned int len;
        stdecc_affine_point_t g;
    } stdecc_curve_t;

    int stdecc_prepare_curve(
                            const uint8_t * const p,
                            const uint8_t * const a,
                            const uint8_t * const b,
                            const uint8_t * const gx,
                            const uint8_t * const gy,
                            unsigned int len,
                            stdecc_curve_t * const c
                        );
    
    
    int stdecc_projectify(
                            const stdecc_affine_point_t * const s,
                            stdecc_projective_point_t * const r,
                            const stdecc_curve_t * const c
                        );

    
    int stdecc_affinify(
                            const stdecc_projective_point_t * const s,
                            stdecc_affine_point_t * const r,
                            const stdecc_curve_t * const c
                        );

    
    int stdecc_double(
                        const stdecc_projective_point_t * const s,
                        stdecc_projective_point_t * const r,
                        const stdecc_curve_t * const c
                    );

    
    int stdecc_add(
                        const stdecc_projective_point_t * const s,
                        const stdecc_projective_point_t * const t,
                        stdecc_projective_point_t * const r,
                        const stdecc_curve_t * const c
                    );

    
    int stdecc_full_add(
                        const stdecc_projective_point_t * const s,
                        const stdecc_projective_point_t * const t,
                        stdecc_projective_point_t * const r,
                        const stdecc_curve_t * const c
                    );

    
    int stdecc_full_sub(
                        const stdecc_projective_point_t * const s,
                        const stdecc_projective_point_t * const t,
                        stdecc_projective_point_t * const r,
                        const stdecc_curve_t * const c
                    );

    
    int stdecc_mult(
                        const std_bigint_t * const d,
                        const stdecc_projective_point_t * const s,
                        stdecc_projective_point_t * const r,
                        const stdecc_curve_t * const c
                    );

    #ifdef __cplusplus
        }
    #endif

#endif

