/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_NBV_IECDHGREENSSFASTP256_H__
#define __TFIT_NBV_IECDHGREENSSFASTP256_H__
#define TFIT_nbv_iECDHgreenSSfastP256_8_0 ((uint8_t)0x2e)
#define TFIT_nbv_iECDHgreenSSfastP256_8_1 ((uint8_t)0x4a)
#define TFIT_nbv_iECDHgreenSSfastP256_8_2 ((uint8_t)0xb3)
#define TFIT_nbv_iECDHgreenSSfastP256_8_3 ((uint8_t)0xd5)
#define TFIT_nbv_iECDHgreenSSfastP256_8_4 ((uint8_t)0xee)
#define TFIT_nbv_iECDHgreenSSfastP256_8_5 ((uint8_t)0x3e)
#define TFIT_nbv_iECDHgreenSSfastP256_8_6 ((uint8_t)0x06)
#define TFIT_nbv_iECDHgreenSSfastP256_8_7 ((uint8_t)0xfd)
#define TFIT_nbv_iECDHgreenSSfastP256_8_8 ((uint8_t)0xfb)
#define TFIT_nbv_iECDHgreenSSfastP256_8_9 ((uint8_t)0x68)
#define TFIT_nbv_iECDHgreenSSfastP256_8_10 ((uint8_t)0x61)
#define TFIT_nbv_iECDHgreenSSfastP256_8_11 ((uint8_t)0xd2)
#define TFIT_nbv_iECDHgreenSSfastP256_8_12 ((uint8_t)0xec)
#define TFIT_nbv_iECDHgreenSSfastP256_8_13 ((uint8_t)0x16)
#define TFIT_nbv_iECDHgreenSSfastP256_8_14 ((uint8_t)0x10)
#define TFIT_nbv_iECDHgreenSSfastP256_8_15 ((uint8_t)0x0d)
#define TFIT_nbv_iECDHgreenSSfastP256_8_16 ((uint8_t)0x10)
#define TFIT_nbv_iECDHgreenSSfastP256_8_17 ((uint8_t)0xda)
#define TFIT_nbv_iECDHgreenSSfastP256_8_18 ((uint8_t)0xbb)
#define TFIT_nbv_iECDHgreenSSfastP256_8_19 ((uint8_t)0x86)
#define TFIT_nbv_iECDHgreenSSfastP256_8_20 ((uint8_t)0x2d)
#define TFIT_nbv_iECDHgreenSSfastP256_8_21 ((uint8_t)0xf3)
#define TFIT_nbv_iECDHgreenSSfastP256_8_22 ((uint8_t)0x62)
#define TFIT_nbv_iECDHgreenSSfastP256_8_23 ((uint8_t)0xd0)
#define TFIT_nbv_iECDHgreenSSfastP256_8_24 ((uint8_t)0x92)
#define TFIT_nbv_iECDHgreenSSfastP256_8_25 ((uint8_t)0x49)
#define TFIT_nbv_iECDHgreenSSfastP256_8_26 ((uint8_t)0xc0)
#define TFIT_nbv_iECDHgreenSSfastP256_8_27 ((uint8_t)0x49)
#define TFIT_nbv_iECDHgreenSSfastP256_8_28 ((uint8_t)0x83)
#define TFIT_nbv_iECDHgreenSSfastP256_8_29 ((uint8_t)0xe6)
#define TFIT_nbv_iECDHgreenSSfastP256_8_30 ((uint8_t)0x18)
#define TFIT_nbv_iECDHgreenSSfastP256_8_31 ((uint8_t)0x24)
#define TFIT_nbv_iECDHgreenSSfastP256_16_0 ((uint16_t)0x2e4a)
#define TFIT_nbv_iECDHgreenSSfastP256_16_1 ((uint16_t)0xb3d5)
#define TFIT_nbv_iECDHgreenSSfastP256_16_2 ((uint16_t)0xee3e)
#define TFIT_nbv_iECDHgreenSSfastP256_16_3 ((uint16_t)0x06fd)
#define TFIT_nbv_iECDHgreenSSfastP256_16_4 ((uint16_t)0xfb68)
#define TFIT_nbv_iECDHgreenSSfastP256_16_5 ((uint16_t)0x61d2)
#define TFIT_nbv_iECDHgreenSSfastP256_16_6 ((uint16_t)0xec16)
#define TFIT_nbv_iECDHgreenSSfastP256_16_7 ((uint16_t)0x100d)
#define TFIT_nbv_iECDHgreenSSfastP256_16_8 ((uint16_t)0x10da)
#define TFIT_nbv_iECDHgreenSSfastP256_16_9 ((uint16_t)0xbb86)
#define TFIT_nbv_iECDHgreenSSfastP256_16_10 ((uint16_t)0x2df3)
#define TFIT_nbv_iECDHgreenSSfastP256_16_11 ((uint16_t)0x62d0)
#define TFIT_nbv_iECDHgreenSSfastP256_16_12 ((uint16_t)0x9249)
#define TFIT_nbv_iECDHgreenSSfastP256_16_13 ((uint16_t)0xc049)
#define TFIT_nbv_iECDHgreenSSfastP256_16_14 ((uint16_t)0x83e6)
#define TFIT_nbv_iECDHgreenSSfastP256_16_15 ((uint16_t)0x1824)
#define TFIT_nbv_iECDHgreenSSfastP256_32_0 ((uint32_t)0x2e4ab3d5)
#define TFIT_nbv_iECDHgreenSSfastP256_32_1 ((uint32_t)0xee3e06fd)
#define TFIT_nbv_iECDHgreenSSfastP256_32_2 ((uint32_t)0xfb6861d2)
#define TFIT_nbv_iECDHgreenSSfastP256_32_3 ((uint32_t)0xec16100d)
#define TFIT_nbv_iECDHgreenSSfastP256_32_4 ((uint32_t)0x10dabb86)
#define TFIT_nbv_iECDHgreenSSfastP256_32_5 ((uint32_t)0x2df362d0)
#define TFIT_nbv_iECDHgreenSSfastP256_32_6 ((uint32_t)0x9249c049)
#define TFIT_nbv_iECDHgreenSSfastP256_32_7 ((uint32_t)0x83e61824)
#define TFIT_nbv_iECDHgreenSSfastP256_64_0 ((uint64_t)0x2e4ab3d5ee3e06fdULL)
#define TFIT_nbv_iECDHgreenSSfastP256_64_1 ((uint64_t)0xfb6861d2ec16100dULL)
#define TFIT_nbv_iECDHgreenSSfastP256_64_2 ((uint64_t)0x10dabb862df362d0ULL)
#define TFIT_nbv_iECDHgreenSSfastP256_64_3 ((uint64_t)0x9249c04983e61824ULL)
#endif
