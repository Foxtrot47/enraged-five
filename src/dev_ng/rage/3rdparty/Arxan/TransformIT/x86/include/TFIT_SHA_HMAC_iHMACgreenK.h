/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: SHA-HMAC Instance=iHMACgreenK
 */

#ifndef __TFIT_SHA_HMAC_iHMACgreenK_H__
#define __TFIT_SHA_HMAC_iHMACgreenK_H__

#include "TFIT.h"

#include "wbhmac.h"
#include "TFIT_generated_encodings_iHMACgreenK.h"

#define TFIT_init_wbsha_hmac_iHMACgreenK(ctx, hmac_key, sha_mode)                    \
    wbsha_hmac_init(ctx, &TFIT_sha_cfg_iHMACgreenK, hmac_key, sha_mode)

#define TFIT_update_wbsha_hmac_iHMACgreenK wbsha_hmac_update
#define TFIT_update_slice_wbsha_hmac_iHMACgreenK wbsha_digest_update_slice
#define TFIT_final_wbsha_hmac_iHMACgreenK wbsha_hmac_final

#define TFIT_wbsha_hmac_sign_iHMACgreenK(hmac_key, sha_mode, input, input_len, tlen, output, output_len, bytes_written) \
    wbsha_hmac_sign(&TFIT_sha_cfg_iHMACgreenK, hmac_key, sha_mode, input, input_len, tlen, output, output_len, bytes_written)

#define TFIT_wbsha_hmac_verify_iHMACgreenK(hmac_key, sha_mode, input, input_len, sig, sig_len, output, output_len, bytes_written) \
    wbsha_hmac_verify(&TFIT_sha_cfg_iHMACgreenK, hmac_key, sha_mode, input, input_len, sig, sig_len, output, output_len, bytes_written)

#define TFIT_key_iHMACgreenK_t wbsha_hmac_key_t

/* Deprecated API: Please use TFIT_prepare_dynamic_key_iHMACgreenK_green */
#define TFIT_prepare_dynamic_key_iHMACgreenK(sha_mode, inputData, length, hmackey) \
    wbsha_hmac_prepare_key(&TFIT_sha_cfg_iHMACgreenK, sha_mode, inputData, length, hmackey)

#define TFIT_prepare_dynamic_key_iHMACgreenK_green(sha_mode, inputData, length, hmackey) \
    wbsha_hmac_prepare_key(&TFIT_sha_cfg_iHMACgreenK, sha_mode, inputData, length, hmackey)

#endif /* __TFIT_SHA_HMAC_iHMACgreenK_H__ */
