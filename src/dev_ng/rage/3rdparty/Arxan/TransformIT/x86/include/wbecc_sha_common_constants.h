/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_SHA_COMMON_CONSTANTS_H__
#define __WBECC_SHA_COMMON_CONSTANTS_H__

#include "wbecc_sha.h"

#ifdef __cplusplus
extern "C"
{
#endif

extern uint32_t K_SHA1[];
extern uint32_t H_SHA1[];

extern uint32_t K_SHA2_224_256[64];
extern uint32_t *K_SHA2_224;
extern uint32_t *K_SHA2_256;
extern uint32_t H_SHA2_224[8];
extern uint32_t H_SHA2_256[8];

#ifdef __cplusplus
}
#endif

#endif
