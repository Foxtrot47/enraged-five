/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

      

      

      


#ifndef __TFIT_APIXOR_IXORREDXIREDXMGREENXO_H__
#define __TFIT_APIXOR_IXORREDXIREDXMGREENXO_H__

#include "wbxlat_api.h"

#ifdef __cplusplus
extern "C" {
#endif

int TFIT_opxor_iXORredXIredXMgreenXO( const unsigned char *keyblob, const unsigned char *src, size_t octet_len, const unsigned char *xor_mask, unsigned char *dest);
extern const unsigned char TFIT_xlat_iXORredXIredXMgreenXO[];
extern const wbxlat_mode_info_t TFIT_apixor_iXORredXIredXMgreenXO;

#ifdef __cplusplus
}
#endif

#endif 


