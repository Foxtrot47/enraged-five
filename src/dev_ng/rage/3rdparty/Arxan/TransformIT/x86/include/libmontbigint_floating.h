/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __MONT_BIGINT_FLOATING_H__
#define __MONT_BIGINT_FLOATING_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "libmontbigint_2048_32.h"
#define MONTBI_WORD_TYPE MONTBI_WORD_TYPE_2048_32
#define MONTBI_MAX_WORDS MONTBI_MAX_WORDS_2048_32
#define MONTBI_WORD_BITS MONTBI_WORD_BITS_2048_32

#define MONTBI_PASTE2( x, y) x ## y
#define MONTBI_PASTE(x, y) MONTBI_PASTE2(x, y)
#define MONTBI_TYPESUFFIX(name) name ##_generic_32
#define MONTBI_SUFFIX(name) MONTBI_PASTE(MONTBI_PASTE(name,_2048),_32)

#ifdef __cplusplus
}
#endif

#endif
