/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: XLAT/Data/Decode Instance=iXDECorange
 */

#ifndef __TFIT_XLAT_Data_Decode_iXDECorange_H__
#define __TFIT_XLAT_Data_Decode_iXDECorange_H__

#include "TFIT.h"

#include "TFIT_apixlat_iXDECorange.h"
#include "TFIT_xlatdef_iXDECorange.h"
#include "TFIT_xlat_iXDECorange.h"

#define TFIT_init_xlat_data_decode_iXDECorange(ctx) \
    wbxlat_init_xlat(ctx, WBXLAT_ENCODE, WBXLAT_OBFUSCATED, WBXLAT_CLASSICAL, &TFIT_apixlat_iXDECorange, &TFIT_xlat_iXDECorange)
#define TFIT_update_xlat_data_decode_iXDECorange(ctx, input, input_len, output, output_len, bytes_written) \
    wbxlat_update(ctx, input, input_len, NULL, output, output_len, bytes_written)
#define TFIT_final_xlat_data_decode_iXDECorange wbxlat_final

#define TFIT_xlat_data_decode_iXDECorange(src, len, dest) \
    TFIT_opxlat_iXDECorange((const unsigned char*)&TFIT_xlat_iXDECorange, src, len, dest)

#endif /* __TFIT_XLAT_Data_Decode_iXDECorange_H__ */
