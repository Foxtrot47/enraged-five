/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

#ifndef __PLAT_x86_GCC_H__
#define __PLAT_x86_GCC_H__

#ifdef __cplusplus
extern "C" {
#endif



#define extending_madd2_x86_gcc_32(a,b,c,d,res,cout) \
{ \
asm(   "mul %3;" \
       "add %4, %%eax;" \
       "adc $0, %%edx;" \
       "add %5, %%eax;" \
       "adc $0, %%edx;" \
   : "=a" (res), "=d" (cout) \
   : "0" (a), "1" (b), "g" (c), "g" (d) \
   : \
  ); \
}



#define extending_madc_x86_gcc_32(a,b,c,res,cout) \
{ \
asm(   "mul %3;" \
       "add %4, %%eax;" \
       "adc $0, %%edx;" \
   : "=a" (res), "=d" (cout) \
   : "0" (a), "1" (b), "g" (c) \
   : \
  ); \
}

#define extending_add_x86_gcc_32(a,b,res,cout) \
{ \
asm( "add %3, %0;" \
       "mov $0, %1;" \
       "adc $0, %1;" \
   : "=g" (res), "=r" (cout) \
   : "0" (a), "1" (b) \
   : \
  ); \
}

#define extending_adc_x86_gcc_32(a,b,c,res,cout) \
{ \
asm( "add %3, %0;" \
       "mov $0, %1;" \
       "adc $0, %1;" \
       "add %4, %0;" \
       "adc $0, %1;" \
   : "=g" (res), "=r" (cout) \
   : "0" (a), "1" (b), "g"(c) \
   : \
  ); \
}

#define borrowing_sub_x86_gcc_32(a,b,c,res,bout) \
{ \
asm( "sub %3, %0;" \
       "mov $0, %1;" \
       "adc $0, %1;" \
       "sub %4, %0;" \
       "adc $0, %1;" \
   : "=g" (res), "=g" (bout) \
   : "0" (a), "1" (b), "g" (c) \
   : \
  ); \
}



#ifdef __cplusplus
}
#endif

#endif

