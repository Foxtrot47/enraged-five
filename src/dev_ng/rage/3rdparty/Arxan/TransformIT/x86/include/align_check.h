/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#if !defined(__ALIGN_CHECK_H__)
#define __ALIGN_CHECK_H__

#if defined(__cplusplus)
extern "C" {
#endif

#include <stddef.h>
#include <arxstdint.h>
#include <stdlib.h> 

#ifdef _MSC_VER
#define ALIGNOF(type) __alignof(type)
#else


#define ALIGNOF(type) offsetof (struct { uint8_t c; type member; }, member)
#endif



#define ALIGNED(ptr, granularity) !(((intptr_t)(ptr)) & ((intptr_t)ALIGNOF(granularity)-1))



#if defined(FORCE_ALIGNMENT_CHECKS) && defined(OMIT_ALIGNMENT_CHECKS)
  #error Both FORCE_ALIGNMENT_CHECKS and OMIT_ALIGNMENT_CHECKS are defined.
#endif



#if (defined(FORCE_ALIGNMENT_CHECKS) || defined(__sparc__) || defined(__arm__) || defined(__arm64__) || defined(_M_ARM_FP))
  #define VERIFY_ALIGNMENT(ptr, granularity, err_code) \
    if(!ALIGNED((ptr), granularity)) return (err_code);
#else
  #define VERIFY_ALIGNMENT(ptr, granularity, err_code) 
#endif

#if defined(__cplusplus)
}
#endif

#endif 

