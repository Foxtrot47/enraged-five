/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef _SLICE_H_
#define _SLICE_H_

#include <arxstdint.h>

#ifdef __cplusplus
extern "C" {
#endif

extern const char WB_SLICE_MAGIC[4];

#define WB_SLICE_VERSION_1_00   0x00010000

#ifndef __GNUC__
#ifndef __attribute__
#define __attribute__( A )
#endif 

#endif 


#ifdef _MSC_VER
#pragma pack (push, 1)
#endif




typedef enum _wb_slice_status_t
{
   WB_SLICE_OK = 0,
   WB_SLICE_NULL_ARG = 1,
   WB_SLICE_INVALID_BYTE_ORDER = 2,
   WB_SLICE_INTERNAL_ERROR = 3,
   WB_SLICE_FILE_OPEN_ERROR = 4,
   WB_SLICE_FILE_WRITE_ERROR = 5,
   WB_SLICE_FILE_CLOSE_ERROR = 6,
   WB_SLICE_FILE_SEEK_ERROR = 7,
   WB_SLICE_MEMORY_ALLOCATION_ERROR = 8,
   WB_SLICE_UNEXPECTED_BINARY_FILE_SIZE = 9,
   WB_SLICE_UNSUPPORTED_WORD_SIZE = 10,
   WB_SLICE_FULL_INPUT_LEN_NOT_WORD_SIZE_MULTIPLE = 11, 
   WB_SLICE_BAD_MAGIC = 12,
   WB_SLICE_UNSUPPORTED_VERSION = 13,
   WB_SLICE_INVALID_BYTE_ORDER_IN_WORD = 14,
   WB_SLICE_BYTES_OUT_OF_RANGE = 15,
   WB_SLICE_UNALIGNED_BUFFER = 16
} wb_slice_status_t;

typedef enum _wb_word_order_t
{
   WBSLICE_FIRST_WORD_AT_HIGHEST_ADDRESS,
   WBSLICE_FIRST_WORD_AT_LOWEST_ADDRESS,
} wbslice_word_order_t;

typedef enum _wb_pad_side_t
{
   WBSLICE_ZERO_PAD_AT_HIGHEST_ADDRESS,
   WBSLICE_ZERO_PAD_AT_LOWEST_ADDRESS,
} wbslice_pad_side_t;

#ifndef WB_SLICE_MAX_WORD_SIZE
#define WB_SLICE_MAX_WORD_SIZE 16
#endif

typedef struct _wb_slice_byte_order_t
{
   unsigned int inverse_byte_order[WB_SLICE_MAX_WORD_SIZE];
   int word_size;
   wbslice_word_order_t word_order;
} wbslice_byte_order_t;

extern const uint8_t wb_slice_s_table[16][256];

struct _wb_slice_table_params_t
{
   uint8_t s;
   uint8_t t;
   uint8_t u;
   uint8_t r;
   uint8_t s_idx;
   uint8_t z_val;
} __attribute__((__packed__));
typedef struct _wb_slice_table_params_t wb_slice_table_params_t;

struct _wbslice_table_t
{
   uint8_t magic[4];
   uint32_t version;
   uint8_t oxdid_in[16];
   uint8_t oxdid_out[16];
   uint32_t input_tile_size;
   uint32_t output_tile_size;
   uint32_t num_input_encodings;
   uint32_t num_output_encodings;
   wb_slice_table_params_t params[1];  
} __attribute__((__packed__));
typedef struct _wbslice_table_t wbslice_table_t;

struct _wb_opaque_leuint32 
{
   uint8_t bytes[4];
} __attribute__((__packed__));

typedef struct _wb_opaque_leuint32 wb_slice_leuint32;

uint32_t wb_slice_leuint32tohui(const wb_slice_leuint32 * const le);
void wb_huitoslice_leuint32(uint32_t h, wb_slice_leuint32 * const le_out);



struct _wb_slice_table_file_t
{
   uint8_t magic[4];
   wb_slice_leuint32 version;
   uint8_t oxdid_in[16];
   uint8_t oxdid_out[16];
   wb_slice_leuint32 input_tile_size;
   wb_slice_leuint32 output_tile_size;
   wb_slice_leuint32 num_input_encodings;
   wb_slice_leuint32 num_output_encodings;
   wb_slice_table_params_t params[1];  
} __attribute__((__packed__));
typedef struct _wb_slice_table_file_t wb_slice_table_file_t;

#ifdef _MSC_VER
#pragma pack(pop)
#endif

int wbslice_create_order(const unsigned int* byte_numbering_within_word,
                    int word_size, wbslice_word_order_t word_order,
                    wbslice_byte_order_t* to_fill);

int wb_slice(const unsigned char* in, unsigned int full_input_len, 
             unsigned int input_len,
             unsigned int byte_number, unsigned int length,
             wbslice_pad_side_t pad_side,
             const wbslice_table_t* slice_table,
             const wbslice_byte_order_t* input_byte_order,
             unsigned char* out, unsigned int out_offset);

#ifdef ALLOW_RUNTIME_FILEIO
int wbslice_read_slice_table(const char* filename, wbslice_table_t** to_create);
#endif 

int wbslice_dealloc_slice_table(wbslice_table_t** to_dealloc);

typedef enum {
   WBSLICE_OXDIN = 0, 
   WBSLICE_OXDOUT = 1
} wbslice_oxdsel_t;
    
const uint8_t * wbslice_get_oxdid(
   const wbslice_table_t * const table,
   wbslice_oxdsel_t which
);

#ifdef __cplusplus
}
#endif

#endif 
