/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */




#ifndef __WBECC_ALU_H__
    #define __WBECC_ALU_H__

    #include <string.h>
    #include <arxstdint.h>
    #include "wbecc_encoding_types.h"

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    
    
    
    #define MAX_BUFFER_SIZE 2*MAX_FIELD_ORDER_BYTES*2

    int wbecc_div2(wbecc_enc_t * const a, const uint32_t alen, 
                    const wbecc_enc_t *p, const uint32_t plen, const wbecc_enc_cfg *cfg);
    
    int wbecc_is_set(wbecc_enc_t * const a, const uint32_t alen, const wbecc_enc_cfg *cfg);
    int wbecc_is_set_top(wbecc_enc_t * const a, const uint32_t alen, const wbecc_enc_cfg *cfg);
    
    int wbecc_enc_shl(wbecc_enc_t * const a, const uint32_t num, const uint32_t max_num_enc_values, const wbecc_enc_cfg *cfg);

    int wbecc_enc_shr(wbecc_enc_t * const a, const uint32_t num, const uint32_t max_num_enc_values, const wbecc_enc_cfg *cfg);

    int wbecc_enc_mult_karatsuba(const wbecc_enc_t *const a, const uint32_t alen,
                                    const wbecc_enc_t * const b, const uint32_t blen,
                                    wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
    
    int wbecc_enc_mult(const wbecc_enc_t *const a, const uint32_t alen,
                                    const wbecc_enc_t * const b, const uint32_t blen,
                                    wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);

    int wbecc_enc_greater_than_or_equal(const wbecc_enc_t *const a, const uint32_t alen,
                            const wbecc_enc_t * const b, const uint32_t blen, const wbecc_enc_cfg *cfg);

    int wbecc_enc_greater_than(const wbecc_enc_t *const a, const uint32_t alen,
                                const wbecc_enc_t * const b, const uint32_t blen, const wbecc_enc_cfg *cfg);
    

    int wbecc_mod_p(const wbecc_enc_t * const a, const uint32_t alen, 
                const wbecc_enc_t * const p, const uint32_t plen,
                const uint8_t type,
                wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
                
    int wbecc_mod_p160(const wbecc_enc_t * const a, const uint32_t alen, 
            const wbecc_enc_t * const p, const uint32_t plen,
             wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
             
    int wbecc_mod_p192(const wbecc_enc_t * const a, const uint32_t alen, 
                const wbecc_enc_t * const p, const uint32_t plen,
                wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
    
    int wbecc_mod_p224(const wbecc_enc_t * const a, const uint32_t alen, 
                const wbecc_enc_t * const p, const uint32_t plen,
                wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
                
    int wbecc_mod_p256(const wbecc_enc_t * const a, const uint32_t alen, 
                const wbecc_enc_t * const p, const uint32_t plen,
                wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
                
    int wbecc_mod_p384(const wbecc_enc_t * const a, const uint32_t alen, 
                const wbecc_enc_t * const p, const uint32_t plen,
                wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
                
    int wbecc_mod_p521(const wbecc_enc_t * const a, const uint32_t alen, 
                const wbecc_enc_t * const p, const uint32_t plen,
                wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);

    int wbecc_exp_mod_p(const wbecc_enc_t * const a, const uint32_t alen,
                        const wbecc_enc_t * const e, const uint32_t elen,
                        const wbecc_enc_t * const p, const uint32_t plen,
                        const uint8_t type,
                        wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
                        
    int wbecc_inv_mod_p(const wbecc_enc_t * const a, const uint32_t alen,
                        const wbecc_enc_t * const p, const uint32_t plen,
                        const uint8_t type,
                        wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
                        
    int wbecc_mult_mod_p(const wbecc_enc_t *const a, const uint32_t alen,
                        const wbecc_enc_t * const b, const uint32_t blen,
                        const wbecc_enc_t * const p, const uint32_t plen,
                        const uint8_t type,
                        wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
    
    int wbecc_add_mod_p(const wbecc_enc_t *const a, const uint32_t alen,
                        const wbecc_enc_t * const b, const uint32_t blen,
                        const wbecc_enc_t * const p, const uint32_t plen,
                        const uint8_t type,
                        wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);

    int wbecc_sub_mod_p(const wbecc_enc_t *const a, const uint32_t alen,
        const wbecc_enc_t * const b, const uint32_t blen,
        const wbecc_enc_t * const p, const uint32_t plen,
        const uint8_t type,
        wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
        
    int wbecc_sqrt_mod_p(const wbecc_enc_t * const a, const uint32_t alen,
        const wbecc_enc_t * const p, const uint32_t plen,
        const uint8_t type,
        wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
        
    int wbecc_is_square_mod_p(const wbecc_enc_t * const a, const uint32_t alen,
        const wbecc_enc_t * const p, const uint32_t plen,
        const uint8_t type,
        const wbecc_enc_cfg *cfg);
        
    int wbecc_count_leading_zeros(const wbecc_enc_t * const a, const uint32_t alen, 
        const wbecc_enc_cfg *cfg);

    int bi_shl(uint8_t *a, uint32_t num, uint32_t len);
    
    
    static __inline uint8_t concat_nm(uint8_t a, uint8_t b, uint8_t n){
        uint8_t left_mask = (1<<n)-1;
        uint8_t right_mask = ~left_mask;
        return ((a&left_mask) << (8-n)) | ((b&right_mask) >> n);
    }

    int wbecc_enc_sub(const wbecc_enc_t *const a, const uint32_t alen,
                            const wbecc_enc_t * const b, const uint32_t blen,
                            wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);
    int wbecc_enc_add(const wbecc_enc_t *const a, const uint32_t alen,
                            const wbecc_enc_t * const b, const uint32_t blen,
                            wbecc_enc_t * const r, const wbecc_enc_cfg *cfg);



    static __inline int bi_add(const uint8_t *const a, const uint32_t alen, const uint8_t *const b, const uint32_t blen, uint8_t * r){
        uint32_t i;

        uint32_t result;
        uint8_t *s = NULL;
        uint8_t *re = NULL;
    
        if (blen > alen) return 1;
        s = (uint8_t *) malloc (sizeof(uint8_t)*alen);
        re = (uint8_t *) malloc (sizeof(uint8_t)*(alen+1));

        result = a[alen-1] + b[blen-1];
        s[alen-1] = result & 0xff;
        re[alen] = s[alen-1];
        
        for (i = 1; i < blen; i++){
            if (result >> 8) {
                result = a[alen-i-1] + b[blen-i-1] + 1;
            } else {
                result = a[alen-i-1] + b[blen-i-1];
            }
            s[alen-i-1] = result & 0xff;
            re[alen-i] = s[alen-i-1];
        }
        
        for (; i < alen; i++){
            if (result >> 8) {
                result = a[alen-i-1] + 1;
            } else {
                result = a[alen-i-1];
            }
            s[alen-i-1] = result & 0xff;
            re[alen-i] = s[alen-i-1];
        }
        re[0] = (uint8_t)(result >> 8);
        memcpy(r,re,sizeof(uint8_t)*(alen+1));
        free(s);
        free(re);
        return 0;
    }
    static __inline int bi_sub(const uint8_t *const a, const uint32_t alen, const uint8_t *const b, const uint32_t blen, uint8_t * r){
        uint32_t i;

        uint8_t tmp = 0x1;
        uint8_t *s = NULL;
        uint8_t *result = NULL;
    
        if (blen > alen) return 1;
        s = (uint8_t *) malloc (sizeof(uint8_t)*alen);
        result = (uint8_t *) malloc (sizeof(uint8_t)*(alen+1));
        
        for (i = 0; i < alen; i++){
            if (i < (alen-blen)){
                s[i] = 0xff;
            } else {
                s[i] = ~(b[i-(alen-blen)]);
            }
        }
        
        bi_add(s, alen, &tmp, 1, result);
        for (i = 0; i < alen; i++){
            s[i] = result[i+1];
        }
        bi_add(a, alen, s, alen, result);
        for (i = 0; i < alen; i++){
            r[i] = result[i+1];
        }

        free(s);
        free(result);

        return 0;
    }

    #ifdef DEBUG_WITH_PT_OUTPUT
    static __inline void decode(const wbecc_enc_t * const a, const uint32_t alen, const wbecc_enc_cfg *cfg) {
        unsigned i = 0;
        if (alen % 2 != 0) i = 1;
        else i = 0;
        if (i == 1) {
            printf("%02X", cfg->nib_to_byte[(cfg->num_output_encodings-1)*cfg->num_nibble_indices*cfg->num_nibble_indices + pt_to_enc(0x0,cfg)*cfg->num_nibble_indices + a[0]]);
            if (alen == 0) {
                printf("\n");
                return;
            }
        }
        for (i = i; i < alen; i+=2){
            printf("%02X", cfg->nib_to_byte[(cfg->num_output_encodings-1)*cfg->num_nibble_indices*cfg->num_nibble_indices + a[i]*cfg->num_nibble_indices + a[i+1]]);
        }
        printf("\n");
    }
    #endif

    #ifdef __cplusplus
    }
    #endif
#endif
