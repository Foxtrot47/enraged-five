/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */



#ifndef _cdefs_h
#define _cdefs_h









#ifndef __ARXSTDINT_H__
#define __ARXSTDINT_H__ 1

#ifdef __cplusplus
extern "C" {
#endif

  

  #ifdef _MSC_VER
    

    #if _MSC_VER >= 1600
      #include <stddef.h>
      #define __STDC_FORMAT_MACROS
    #endif
    typedef unsigned __int8       uint8;
    typedef unsigned __int16      uint16;
    typedef unsigned __int32      uint32;
    typedef unsigned __int64      uint64;
    typedef __int8                int8;
    typedef __int16               int16;
    typedef __int32               int32;
    typedef __int64               int64;
    #define PRIi8  "i"
    #define PRIi16 "hi"
    #define PRIi32 "I32i"
    #define PRIi64 "I64i"
    #define PRId8  "d"
    #define PRId16 "hd"
    #define PRId32 "I32d"
    #define PRId64 "I64d"
    #define PRIu8  "u"
    #define PRIu16 "hu"
    #define PRIu32 "I32u"
    #define PRIu64 "I64u"
    #define PRIx8  "x"
    #define PRIx16 "hx"
    #define PRIx32 "I32x"
    #define PRIx64 "I64x"
    #define PRIX8  "X"
    #define PRIX16 "hX"
    #define PRIX32 "I32X"
    #define PRIX64 "I64X"
    #define PRIo8  "o"
    #define PRIo16 "ho"
    #define PRIo32 "I32o"
    #define PRIo64 "I64o"
  #else
    #ifdef __cplusplus
    extern "C" {
    #endif
    #include <stddef.h>
    #define __STDC_FORMAT_MACROS
    #include <inttypes.h>
    #ifdef __cplusplus
    }
    #endif

    

    #if defined(__sun) && defined(__svr4__) && defined(_ILP32) && defined(__STDC__)
      #define PRIx64                  "llx"
      #define PRIX64                  "llX"
      #define PRIu64                  "llu"
      #define PRId64                  "lld"
      #define PRIi64                  "lli"
      #define PRIo64                  "llo"
    #endif

    typedef uint8_t               uint8;
    typedef uint16_t              uint16;
    typedef uint32_t              uint32;
    typedef uint64_t              uint64;
    typedef int8_t                int8;
    typedef int16_t               int16;
    typedef int32_t               int32;
    typedef int64_t               int64;
  #endif 


#ifdef __cplusplus
}
#endif

#endif 




#ifdef __cplusplus
extern "C" {
#endif
   typedef void (*VOIDPRINTVARFUNCPTR)(const char *, ...);

   

   #define countof(staticArray)   ((sizeof(staticArray))/(sizeof(staticArray[0])))

   

   

   #define byteOffsetOf(classname,membername) \
      ((int)((reinterpret_cast<ptrdiff_t>(&((reinterpret_cast<classname*>(8))->membername)))-8))


#ifndef COMPATIBILITY_INCLUDES
#define COMPATIBILITY_INCLUDES

   #ifndef WIN32
      #define O_BINARY   0x0
   #endif
   

   #ifdef _MSC_VER   

      #define getpid _getpid
      #define snprintf _snprintf
      #define strtoull _strtoui64
      #define strtoll _strtoi64
      #define strcasecmp stricmp
      #define strncasecmp strnicmp
      
      
      #if _MSC_VER != 1500
      #define vsnprintf _vsnprintf
      #endif 

      #define S_IRUSR _S_IREAD
      #define S_IWUSR _S_IWRITE
      
      typedef int ssize_t;
      typedef long off_t;

   #endif
#endif

   #ifdef GCC
      #ifdef WIN32
         char *strdup(const char *);
      #endif
   #endif

   #define INDENT_SPACES "    "

    

    #ifndef SLASH
    #ifdef WIN32
    #define SLASH '\\'
    #else
    #define SLASH '/'
    #endif
    #endif

#ifdef __cplusplus
}
#endif


#endif
