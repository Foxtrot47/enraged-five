/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

#ifndef _TFIT_DDEFS_I3DES2REDCTGREENPT_H_
#define _TFIT_DDEFS_I3DES2REDCTGREENPT_H_

#include <stdio.h>
#include "arxstdint.h"
#include "wbdes_api.h"
#ifdef __cplusplus
extern "C" {
#endif
  struct _TFIT_dkey_i3DES2redCTgreenPT {
   uint64_t data[51]; 
  };
#ifndef __TFIT_DKEY_I3DES2REDCTGREENPT_T__
#define __TFIT_DKEY_I3DES2REDCTGREENPT_T__
  typedef struct _TFIT_dkey_i3DES2redCTgreenPT TFIT_dkey_i3DES2redCTgreenPT_t;
#endif
  struct _TFIT_ddyninit_i3DES2redCTgreenPT {
   uint64_t data[9270]; 
  };
#ifndef __TFIT_DDYNINIT_I3DES2REDCTGREENPT_T__
#define __TFIT_DDYNINIT_I3DES2REDCTGREENPT_T__
  typedef struct _TFIT_ddyninit_i3DES2redCTgreenPT TFIT_ddyninit_i3DES2redCTgreenPT_t;
#endif
  void TFIT_dop_i3DES2redCTgreenPT(const void* void_ks, const unsigned char in[8], unsigned char out[8]);

  int TFIT_validate_dkey_id_i3DES2redCTgreenPT(const void * void_ks, size_t len);

  const uint8_t* TFIT_get_doxd_id_in_i3DES2redCTgreenPT();
  const uint8_t* TFIT_get_doxd_id_out_i3DES2redCTgreenPT();

  extern const wbdes_ecb_cipher_t TFIT_des_ecb_i3DES2redCTgreenPT;
#ifdef __cplusplus
}
#endif
#endif 

