/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

      

      

      


#ifndef __TFIT_APIGCM_IAES16GREENPT_H__
#define __TFIT_APIGCM_IAES16GREENPT_H__

#include "wbaes_api.h"

#ifdef __cplusplus
extern "C" {
#endif

int TFIT_opgcm_iAES16greenPT( wbaes_inner_func *wbop, const unsigned char *keys[2], const unsigned char src[16], size_t octet_len, unsigned char iv[16], unsigned char HH[16], unsigned char tag[16], unsigned char dest[16]);
int TFIT_tag_opgcm_iAES16greenPT( wbaes_inner_func *wbop, const unsigned char *keys[2], const unsigned char ac_len[16], size_t tag_len, unsigned char tag[16], const unsigned char J[16], unsigned char *data_out);
extern const unsigned char TFIT_wrap_iAES16greenPT[];
extern const wbaes_wrapper_t TFIT_apigcm_iAES16greenPT;

#ifdef __cplusplus
}
#endif

#endif 


