/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __WB_ECC_STATIC_H__
#define __WB_ECC_STATIC_H__

#ifdef __cplusplus
extern "C" {
#endif 

#include <arxstdint.h>
#include <stddef.h>

#include "wb_ecc_montbigint.h"


#define MAX_CURVE_ORDER_BYTES 66

#ifndef __WBECC_STATIC_ERROR_CODES__
#define __WBECC_STATIC_ERROR_CODES__
#define WBECC_STATIC_OK                              0
#define WBECC_STATIC_NOT_SDP                         1
#define WBECC_STATIC_UNSUPPORTED_SDP                 2
#define WBECC_STATIC_BAD_SDP_LEN                     3
#define WBECC_STATIC_INCOMPATIBLE_SDP_MOD_SIZE       4
#define WBECC_STATIC_INTERNAL_ERROR                  5
#define WBECC_STATIC_UNKNOWN_MONTBI_CTX_VERSION      6
#define WBECC_STATIC_BAD_MONTBI_CTX_SERLEN           7
#define WBECC_STATIC_FAILED_DESERIALIZING_MONTBI_CTX 8
#define WBECC_STATIC_MONTBI_CTX_DESERIALIZE_BAD_SIZE 9
#endif

#ifndef __SERIALIZED_DOMAIN_PARAMETER_FILE_HEADER__
#define __SERIALIZED_DOMAIN_PARAMETER_FILE_HEADER__

typedef struct _wbecc_sdp_header_common
{
  uint8_t magic[4];
  uint8_t version[4];
} wbecc_sdp_header_common_t;

typedef struct _wbecc_sdp_header_v1
{
  wbecc_sdp_header_common_t common;
  uint8_t total_size[4];
  uint8_t mod_size[4];
  uint8_t montbi_ctx_ver[4];
  uint8_t montbi_ctx_size[4];
  uint8_t reserved[8];
} wbecc_sdp_header_v1_t;

#endif



typedef struct _wbecc_affine_point_host
{
  wbecc_montbi_int x;
  wbecc_montbi_int y;
} wbecc_affine_point_host_t;

typedef struct _wbecc_point_host
{
  wbecc_montbi_int xn;
  wbecc_montbi_int xd;
  wbecc_montbi_int yn;
  wbecc_montbi_int yd;
} wbecc_point_host_t;

typedef struct _wbecc_domain_host
{
  

  wbecc_montbi_ctx ctx;
  

  wbecc_montbi_ctx order_ctx;

  

  wbecc_montbi_int d;
  wbecc_montbi_int tm2;

  

  wbecc_montbi_int kSq;
  wbecc_montbi_int kCu;
  wbecc_montbi_int kSqInv;
  wbecc_montbi_int kCuInv;
  wbecc_montbi_int kSqInvTwelfthb2;

  

  wbecc_point_host_t G;
  

  wbecc_point_host_t O;

  

  wbecc_point_host_t twoRObfMRInG;

  

  wbecc_point_host_t twoRClMRInG;

  

  wbecc_point_host_t rObfG;

  

  wbecc_point_host_t rClG;

  

  wbecc_point_host_t rInG;

  

  wbecc_point_host_t rndTG[32];

  

  wbecc_point_host_t preG[32];

  

  wbecc_point_host_t cGCl[32];

  

  wbecc_point_host_t gCorCl[32];

  

  wbecc_point_host_t cTCl[32];

  

  wbecc_point_host_t gCorObf[32];

  

  wbecc_point_host_t cTObf[32];

  

  wbecc_point_host_t unused[32];

  

  wbecc_montbi_int starInvM;
  wbecc_montbi_int starInvSqM;
  
} wbecc_domain_host_t;




int wbecc_parse_affine_point(wbecc_affine_point_host_t*dest, const unsigned char xCoord[], size_t xLen, const unsigned char yCoord[], size_t yLen, int xyFormat, const wbecc_montbi_ctx*ctx);



int wbecc_parse_domain_params(wbecc_domain_host_t* target, const void * serialized_data, unsigned int serialized_data_len);


typedef struct _wbecc_pm_table
{
  wbecc_point_host_t entries[32];
} wbecc_pm_table_t;


typedef struct wbecc_fst_funcs {
    
    void (*wbecc_convert_from_classical_affine_weierstrass)(
            wbecc_point_host_t *, 
            const wbecc_affine_point_host_t *,
            const void *,   
            const wbecc_domain_host_t *
        );
    
    void (*wbecc_convert_to_classical_affine_weierstrass)(
            wbecc_affine_point_host_t *,
            const wbecc_point_host_t *, 
            unsigned int,
            const void *,   
            const wbecc_domain_host_t *
        );
    int (*wbecc_convert_m_to_classical_u_operand)(
            const unsigned char in[],
            unsigned int len,
            unsigned char out[]
        );
    
    void (*wbecc_convert_to_obfuscated_affine_weierstrass)(
            wbecc_affine_point_host_t *,
            const wbecc_point_host_t *, 
            unsigned int,
            const void *,   
            const wbecc_domain_host_t *
        );
    
    int (*wbecc_convert_to_obfuscated_be_weierstrass)(
            uint8_t *,
            unsigned int,
            uint8_t *,
            unsigned int,
            const wbecc_affine_point_host_t *,
            const void *,   
            const wbecc_domain_host_t * 
        );
    
    
    void (*wbecc_build_pm_table)(
            const wbecc_pm_table_t *,
            const wbecc_point_host_t* , 
            const void *, 
            const wbecc_domain_host_t *, 
            const void * 
        );
    
    void (*wbecc_add3)(
            wbecc_point_host_t *, 
            const wbecc_point_host_t*, 
            const wbecc_point_host_t*, 
            const wbecc_point_host_t*, 
            const void *, 
            const wbecc_domain_host_t *
        );
    
    void (*wbecc_shift5)(
            wbecc_point_host_t *, 
            const wbecc_point_host_t *, 
            const void *, 
            const wbecc_domain_host_t *
        );
    
    void (*wbecc_correcting_sub)(
            wbecc_point_host_t *, 
            const wbecc_point_host_t *, 
            const wbecc_point_host_t *, 
            const void *, 
            const wbecc_domain_host_t *
        );
    
    int (*wbecc_prepare_constants)(
            void *, 
            const wbecc_montbi_ctx *
        );

    
    int (*wbecc_point_validate)(
            const wbecc_point_host_t *,
            const void *, 
            const wbecc_domain_host_t *
        );

    size_t pmscratch_size;
    size_t constants_size;
    unsigned int bytes_for_coordinate;
    unsigned int order_bits;
    unsigned int pentets_for_key;
    unsigned int octets_for_key;

    
    int (*wbecdsa_parameter_helper)(
            wbecc_montbi_int* k_inv_star_m_inv, 
            wbecc_montbi_int* d_star, 
            const uint8_t k_pentets[], 
            const uint8_t d_pentets[], 
            uint8_t S,
            unsigned int pentet_len, 
            const wbecc_domain_host_t* domain
        );

    
    int (*wbecc_fast_prepare_key)(
            const uint8_t *input, unsigned int input_len,
            const void* cfg, const wbecc_domain_host_t* domain,
            const void* constants, 
            void* out
        );

    uint8_t eps[MAX_CURVE_ORDER_BYTES];
    struct _wbecc_enc_cfg *wbecc_table_config;
    struct _WBECC_TABLE_CURVE *wbecc_table_curve;
} wbecc_fst_funcs_t;


int wbecc_point_multiply(wbecc_point_host_t *dest, const uint8_t d[], size_t dlen, uint8_t S, const wbecc_point_host_t pmtab[], const wbecc_point_host_t ctab[], const wbecc_fst_funcs_t *funcs, const wbecc_domain_host_t *domain, const void *constants);


#ifdef __cplusplus
}
#endif 

#endif 

