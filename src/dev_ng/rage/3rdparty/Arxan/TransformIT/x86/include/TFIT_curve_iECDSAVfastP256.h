/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_CURVE_IECDSAVFASTP256_H__
#define __TFIT_CURVE_IECDSAVFASTP256_H__

#include <arxstdint.h>
#include "wbecc_ecc_alu.h"

#ifdef __cplusplus
extern "C" {
#endif



extern wbecc_table_curve_t TFIT_curve_iECDSAVfastP256;
extern unsigned int TFIT_curve_len_iECDSAVfastP256;

#ifdef __cplusplus
}
#endif
#endif 

