/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_NBV_IECDSAVFASTP256_H__
#define __TFIT_NBV_IECDSAVFASTP256_H__
#define TFIT_nbv_iECDSAVfastP256_8_0 ((uint8_t)0xa0)
#define TFIT_nbv_iECDSAVfastP256_8_1 ((uint8_t)0x20)
#define TFIT_nbv_iECDSAVfastP256_8_2 ((uint8_t)0xff)
#define TFIT_nbv_iECDSAVfastP256_8_3 ((uint8_t)0x78)
#define TFIT_nbv_iECDSAVfastP256_8_4 ((uint8_t)0x9c)
#define TFIT_nbv_iECDSAVfastP256_8_5 ((uint8_t)0x8c)
#define TFIT_nbv_iECDSAVfastP256_8_6 ((uint8_t)0x3d)
#define TFIT_nbv_iECDSAVfastP256_8_7 ((uint8_t)0x57)
#define TFIT_nbv_iECDSAVfastP256_8_8 ((uint8_t)0x61)
#define TFIT_nbv_iECDSAVfastP256_8_9 ((uint8_t)0xdd)
#define TFIT_nbv_iECDSAVfastP256_8_10 ((uint8_t)0x2b)
#define TFIT_nbv_iECDSAVfastP256_8_11 ((uint8_t)0x2d)
#define TFIT_nbv_iECDSAVfastP256_8_12 ((uint8_t)0x10)
#define TFIT_nbv_iECDSAVfastP256_8_13 ((uint8_t)0x2f)
#define TFIT_nbv_iECDSAVfastP256_8_14 ((uint8_t)0xab)
#define TFIT_nbv_iECDSAVfastP256_8_15 ((uint8_t)0x27)
#define TFIT_nbv_iECDSAVfastP256_8_16 ((uint8_t)0x12)
#define TFIT_nbv_iECDSAVfastP256_8_17 ((uint8_t)0x55)
#define TFIT_nbv_iECDSAVfastP256_8_18 ((uint8_t)0xc2)
#define TFIT_nbv_iECDSAVfastP256_8_19 ((uint8_t)0x83)
#define TFIT_nbv_iECDSAVfastP256_8_20 ((uint8_t)0x97)
#define TFIT_nbv_iECDSAVfastP256_8_21 ((uint8_t)0x57)
#define TFIT_nbv_iECDSAVfastP256_8_22 ((uint8_t)0x7b)
#define TFIT_nbv_iECDSAVfastP256_8_23 ((uint8_t)0xc0)
#define TFIT_nbv_iECDSAVfastP256_8_24 ((uint8_t)0xa3)
#define TFIT_nbv_iECDSAVfastP256_8_25 ((uint8_t)0x47)
#define TFIT_nbv_iECDSAVfastP256_8_26 ((uint8_t)0x43)
#define TFIT_nbv_iECDSAVfastP256_8_27 ((uint8_t)0x61)
#define TFIT_nbv_iECDSAVfastP256_8_28 ((uint8_t)0xb4)
#define TFIT_nbv_iECDSAVfastP256_8_29 ((uint8_t)0x2a)
#define TFIT_nbv_iECDSAVfastP256_8_30 ((uint8_t)0x64)
#define TFIT_nbv_iECDSAVfastP256_8_31 ((uint8_t)0xf7)
#define TFIT_nbv_iECDSAVfastP256_16_0 ((uint16_t)0xa020)
#define TFIT_nbv_iECDSAVfastP256_16_1 ((uint16_t)0xff78)
#define TFIT_nbv_iECDSAVfastP256_16_2 ((uint16_t)0x9c8c)
#define TFIT_nbv_iECDSAVfastP256_16_3 ((uint16_t)0x3d57)
#define TFIT_nbv_iECDSAVfastP256_16_4 ((uint16_t)0x61dd)
#define TFIT_nbv_iECDSAVfastP256_16_5 ((uint16_t)0x2b2d)
#define TFIT_nbv_iECDSAVfastP256_16_6 ((uint16_t)0x102f)
#define TFIT_nbv_iECDSAVfastP256_16_7 ((uint16_t)0xab27)
#define TFIT_nbv_iECDSAVfastP256_16_8 ((uint16_t)0x1255)
#define TFIT_nbv_iECDSAVfastP256_16_9 ((uint16_t)0xc283)
#define TFIT_nbv_iECDSAVfastP256_16_10 ((uint16_t)0x9757)
#define TFIT_nbv_iECDSAVfastP256_16_11 ((uint16_t)0x7bc0)
#define TFIT_nbv_iECDSAVfastP256_16_12 ((uint16_t)0xa347)
#define TFIT_nbv_iECDSAVfastP256_16_13 ((uint16_t)0x4361)
#define TFIT_nbv_iECDSAVfastP256_16_14 ((uint16_t)0xb42a)
#define TFIT_nbv_iECDSAVfastP256_16_15 ((uint16_t)0x64f7)
#define TFIT_nbv_iECDSAVfastP256_32_0 ((uint32_t)0xa020ff78)
#define TFIT_nbv_iECDSAVfastP256_32_1 ((uint32_t)0x9c8c3d57)
#define TFIT_nbv_iECDSAVfastP256_32_2 ((uint32_t)0x61dd2b2d)
#define TFIT_nbv_iECDSAVfastP256_32_3 ((uint32_t)0x102fab27)
#define TFIT_nbv_iECDSAVfastP256_32_4 ((uint32_t)0x1255c283)
#define TFIT_nbv_iECDSAVfastP256_32_5 ((uint32_t)0x97577bc0)
#define TFIT_nbv_iECDSAVfastP256_32_6 ((uint32_t)0xa3474361)
#define TFIT_nbv_iECDSAVfastP256_32_7 ((uint32_t)0xb42a64f7)
#define TFIT_nbv_iECDSAVfastP256_64_0 ((uint64_t)0xa020ff789c8c3d57ULL)
#define TFIT_nbv_iECDSAVfastP256_64_1 ((uint64_t)0x61dd2b2d102fab27ULL)
#define TFIT_nbv_iECDSAVfastP256_64_2 ((uint64_t)0x1255c28397577bc0ULL)
#define TFIT_nbv_iECDSAVfastP256_64_3 ((uint64_t)0xa3474361b42a64f7ULL)
#endif
