/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

      

      

      

#ifndef __TFIT_WRAP_IAES14ORANGEIVGREENPT_H__
#define __TFIT_WRAP_IAES14ORANGEIVGREENPT_H__

#ifdef __cplusplus
extern "C" {
#endif

extern const unsigned char TFIT_wrap_iAES14orangeIVgreenPT[];
extern const unsigned int TFIT_wrap_len_iAES14orangeIVgreenPT;

#ifdef __cplusplus
}
#endif

#endif 
 
