/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */



#ifndef __GEN_HKEY_H__
   #define __GEN_HKEY_H__

   #include <arxstdint.h>

   #ifdef __cplusplus
      extern "C" {
   #endif

   

   int wbaes_dyninit_prepare_key(
      const uint8_t *const obfuscated_key,
      unsigned int obfuscated_key_len,
      const uint8_t *const dyninit_table,
      uint8_t * const output,
      unsigned int output_len,
      int (*encrypt_func)(const void *, const unsigned char *, unsigned char *),
      unsigned int * const bytes_written
   );


   

   const uint8_t * wbaes_dyninit_get_oxd_in_id(
      const uint8_t * const dyninit_table
   );

   #ifdef __cplusplus
   }
   #endif

#endif

