/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __SIZE_UTILS_H__
#define __SIZE_UTILS_H__

#include "wbrsa_config.h"

#define MAX_SUM_SIZE3(a,b,c) (((a > b) ? ((a > c) ? a : c) : b) + 1)
#define MAX_SUM_SIZE(a,b) (((a > b) ? a : b) + 1)
#define MAX_PROD_SIZE(a,b) (a+b)

#define WORDS_TO_BYTES(a) ((a) * sizeof(mword))

#define ZERO(a, aLen) \
  { \
  _size ___temp; \
    for(___temp=0;___temp < (aLen);___temp++){ \
      (a)[___temp] = 0; \
    } \
  }

#define INTERPRET_NEXT_FOUR_BYTES_AS_UINT32(ptr) \
   ( (*((uint8*)(ptr)) << 24) + (*(((uint8*)((ptr)))+1) << 16) + \
     (*(((uint8*)((ptr)))+2) << 8) + *(((uint8*)((ptr)))+3) )

#ifndef __ALIGNED_MEMACCESS
#define COPY_DST_SRC(a,b,bLen) \
  { \
    unsigned int temp; \
    for(temp = 0;temp < (bLen);temp++){ \
      (a)[temp] = (b)[temp]; \
    } \
  }

#define MEMCPY_DST_SRC(d, s, slen) \
  { \
	_size ___temp; \
    _size dwords = ((slen) >> 2); \
    for(___temp=0; ___temp < dwords; ___temp++){ \
      ((uint32 *)(d))[___temp] = ((uint32 *)(s))[___temp]; \
    } \
    for(___temp=(dwords << 2); ___temp < (slen);___temp++){ \
      ((uint8 *)(d))[___temp] = ((uint8 *)(s))[___temp]; \
    } \
  }

#define MEMCPY_DST_SRC_REVERSED(d, s, slen) \
  { \
    _size ___temp; \
    _size dwords = (slen) >> 2; \
    _size rem = (slen) & 0x03; \
    for(___temp=0; ___temp < dwords; ___temp++){ \
      ((uint32 *)(((uint8 *)(d))+rem))[dwords-___temp-1] = UINT32_REVERSE_BYTE_ORDER(((uint32 *)(s))[___temp]); \
    } \
    for(___temp=(dwords << 2); ___temp < (slen);___temp++){ \
      ((uint8 *)(d))[(slen)-___temp-1] = ((uint8 *)(s))[___temp]; \
    } \
  }

#define MEMCMP(a,b,len, result) \
  { \
    _size dwords; \
	 _size ___temp; \
    (result) = 0; \
    dwords = (len) >> 2; \
    for(___temp=0; ___temp < dwords; ___temp++){ \
      if(((uint32 *)(a))[___temp] != ((uint32 *)(b))[___temp]){(result)=1;break;} \
    } \
    if(!(result)) \
    for(___temp=(dwords << 2); ___temp < (len);___temp++){ \
      if(((uint8 *)(a))[___temp] != ((uint8 *)(b))[___temp]){(result)=1;break;} \
    } \
  }
#else 

#define COPY_DST_SRC(a,b,bLen) MEMCPY_DST_SRC((a), (b), (bLen) * sizeof((b)[0]))
#define MEMCPY_DST_SRC(d, s, slen) \
  { \
    _size ___temp; \
    for(___temp=0; ___temp < (slen); ___temp++){ \
      ((uint8*)(d))[___temp] = ((uint8*)(s))[___temp]; \
    } \
  }

#define MEMCPY_DST_SRC_REVERSED(d, s, slen) \
  { \
    _size ___temp; \
    for(___temp=0; ___temp < (slen); ___temp++){ \
      ((uint8*)(d))[___temp] = ((uint8*)(s))[(slen)-___temp-1]; \
    } \
  }

#define MEMCMP(a,b,len, result) \
  { \
    _size ___temp; \
    (result) = 0; \
    for(___temp=0; ___temp < (len); ___temp++){ \
      if(((uint8*)(a))[___temp] != ((uint8*)(b))[___temp]){(result)=1;break;} \
    } \
  }

#endif 


#endif 


