/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_DSA_API_FST_H__
    #define __WBECC_DSA_API_FST_H__

    #include <arxstdint.h>
    #include "wbecc.h"
    #include "wbecc_ecc_alu.h"
    #include "wbecc_encoding_types.h"
    #include "wbecc_common_api.h"
    #include "wbecc_sha.h"
    
    #include "wb_ecc_static.h"

    #ifdef __cplusplus
        extern "C"
        {
    #endif

    typedef int (*wbecc_dsa_nonce_func_fst) (const unsigned int, uint8_t * const, const void * const);

    typedef struct _wbecc_dsa_ctx_fst {
        sha_ctx_t hash_ctx;
        const wbecc_key_pair_t * key_pair;
        wbecc_dsa_nonce_func_fst get_nonce;
        const wbecc_fst_funcs_t *funcs;
        const wbecc_domain_host_t *domain;
        uint8_t *constants;
        uint8_t *pmscratch;
        wbecc_pm_table_t pmtable;
    } wbecc_dsa_ctx_fst_t;

    int wbecc_dsa_final_sign_fst(
        wbecc_dsa_ctx_fst_t *ctx, 
        uint8_t * const r,
        unsigned int r_size,
        unsigned int * const r_bytes_written,
        uint8_t * const s,
        unsigned int s_size,
        unsigned int * const s_bytes_written
    );

    int wbecc_dsa_final_verify_fst(
        wbecc_dsa_ctx_fst_t *ctx,
        uint8_t * const r,
        unsigned int r_size,
        uint8_t * const s,
        unsigned int s_size,
        uint8_t * const Qx,
        unsigned int Qx_size,
        uint8_t * const Qy,
        unsigned int Qy_size,
        uint8_t * const result,
        unsigned int result_size
    );


    const uint8_t * wbecc_dsa_get_oxdid_fst(
        const wbecc_dsa_ctx_fst_t * const ctx,
        wbecc_oxdsel_t which
    );

    #ifdef __cplusplus
        }
    #endif
#endif
