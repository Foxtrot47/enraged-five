/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

#ifndef _TFIT_DDYNINIT_IDES1GREENPT_RED_H_
#define _TFIT_DDYNINIT_IDES1GREENPT_RED_H_

#include "arxstdint.h"
#ifdef __cplusplus
extern "C" { 
#endif
#ifndef __TFIT_DDYNINIT_IDES1GREENPT_T__
#define __TFIT_DDYNINIT_IDES1GREENPT_T__
  typedef struct _TFIT_ddyninit_iDES1greenPT TFIT_ddyninit_iDES1greenPT_t;
#endif
extern const TFIT_ddyninit_iDES1greenPT_t TFIT_ddyninit_iDES1greenPT_red;
extern const unsigned int TFIT_ddyninit_len_iDES1greenPT_red;
#ifdef __cplusplus
}
#endif
#endif //_TFIT_DDYNINIT_IDES1GREENPT_RED_H_

