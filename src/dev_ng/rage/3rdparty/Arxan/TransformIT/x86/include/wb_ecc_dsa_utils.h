/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __WB_ECC_DSA_UTILS_H__
#define __WB_ECC_DSA_UTILS_H__

#if defined(__cplusplus)
extern "C" {
#endif

#include "wb_ecc_montbigint.h"
#include <arxstdint.h>



int wbecc_five_bit_montbi_mult(wbecc_montbi_int* target, const uint8_t a[], const uint8_t b[], unsigned int abLen, const wbecc_montbi_ctx* ctx, const uint8_t mul[
][32][32][2], unsigned int pos_mod);


#if defined(__cplusplus)
}
#endif

#endif 

