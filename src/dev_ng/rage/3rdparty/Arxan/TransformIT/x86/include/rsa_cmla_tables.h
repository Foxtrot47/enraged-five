/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */



#include <arxstdint.h>

#ifndef __RSA_CMLA_TABLES_H__
#define __RSA_CMLA_TABLES_H__ 1



#define PE_HEX_0X100 0
#define PE_SECRET_CORRECTION 1
#define PE_HEX_0XFF01 2
#define PE_PLUSONE_HIGH_ZERO 3
#define PE_ONE 4
#define PE_PLUSONE_HIGH_FF 5
#define PE_PLUSONE_LOW_FF 6
#define PE_PLUSONE_LOW_ZERO 7
#define PE_MAX 8











typedef struct table_pointers_type { 
  uint16_t** mult_high_high;
  uint16_t** mult_high_low;
  uint16_t** mult_low_low;
  uint16_t* inverse_table;
  uint16_t* pre_encoded_value;
  uint8_t* correct_and_encode_table_low;
  uint8_t* correct_and_encode_table_high;
  uint8_t* correct_and_encode_carry_needed_low;
  uint8_t* correct_and_encode_carry_needed_high;
  uint8_t* plusone_low;
  uint8_t* plusone_high;
} table_pointers_t;

#endif
