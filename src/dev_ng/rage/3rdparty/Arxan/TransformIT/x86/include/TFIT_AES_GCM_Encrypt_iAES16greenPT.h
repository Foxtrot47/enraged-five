/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES-GCM/Encrypt Instance=iAES16greenPT
 */

#ifndef __TFIT_AES_GCM_ENCRYPT_iAES16greenPT_H__
#define __TFIT_AES_GCM_ENCRYPT_iAES16greenPT_H__

#include "TFIT.h"

#include "TFIT_defs_iAES16greenPT.h"
#include "wrapper_modes.h"
#define TFIT_update_wbaes_iAES16greenPT wbaes_update

#define TFIT_update_slice_wbaes_iAES16greenPT wbaes_update_slice

#define TFIT_final_wbaes_iAES16greenPT wbaes_final

#define TFIT_validate_wb_key_iAES16greenPT TFIT_validate_key_id_iAES16greenPT

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iAES16greenPT_<colour> */
#define TFIT_prepare_dynamic_key_iAES16greenPT(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)


#include "TFIT_apigcm_iAES16greenPT.h"

#define TFIT_init_wbaes_gcm_iAES16greenPT(ctx, wbaes_key, ctr)    \
    wbaes_init_gcm(ctx, WBAES_ENCRYPT, WBAES_OBFUSCATED, WBAES_CLASSICAL, &TFIT_aes_ecb_iAES16greenPT, wbaes_key, &TFIT_apigcm_iAES16greenPT, &TFIT_wrap_iAES16greenPT, ctr)
#ifdef __cplusplus
extern "C"
#endif
int TFIT_wbaes_gcm_encrypt_tag_iAES16greenPT(const void *wbaes_key, 
                                          const unsigned char *input, 
                                          size_t input_len,
                                          const unsigned char *aad,
                                          size_t aad_len,
                                          const unsigned char ctr[12],
                                          unsigned char *const tag_out,
                                          size_t tag_len,
                                          unsigned char *const output);


#define TFIT_wbaes_gcm_get_tag_iAES16greenPT wbaes_gcm_get_tag
#define TFIT_wbaes_gcm_verify_tag_iAES16greenPT wbaes_gcm_verify_tag

#endif /* __TFIT_AES_GCM_ENCRYPT_iAES16greenPT_H__ */
