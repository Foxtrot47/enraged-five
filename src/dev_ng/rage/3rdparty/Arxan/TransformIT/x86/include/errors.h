/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/






#ifndef __ERRORS_H__
#define __ERRORS_H__




#define ERR_PKCS1_PADDING_TOO_SMALL -1
#define ERR_PKCS1_PADDING_TOO_BIG -2
#define ERR_PKCS1_PADDING_INVALID_PARAMETER -3
#define ERR_PKCS1_DECODE_ERROR -4;

#endif


