/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_ALU_ECC_H__
    #define __WBECC_ALU_ECC_H__

    #include <string.h>
    #include "wbecc_encoding_types.h"
    #include "wbecc.h"
    #include "wb_ecc_static.h"

    #ifdef __cplusplus
    extern "C"
    {
    #endif

    typedef struct _WBECC_AFFINE_POINT {
        wbecc_enc_t x[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t y[MAX_WBECC_ENC_T_SIZE];
    } wbecc_affine_point_t;
    
    typedef struct _WBECC_PROJECTIVE_POINT {
        wbecc_enc_t x[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t y[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t z[MAX_WBECC_ENC_T_SIZE];
    } wbecc_projective_point_t;

    
    
    typedef struct _WBECC_TABLE_CURVE {
        wbecc_enc_t p[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t ne[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t salt[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t a[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t b[MAX_WBECC_ENC_T_SIZE];
        wbecc_affine_point_t G;
        uint8_t inst_uuid[16];
        uint8_t plen;
        uint8_t nelen;
        uint8_t curve_type;
        uint8_t h;
    } wbecc_table_curve_t;
    
    typedef struct _wbecc_fast_table_curve_t {
        wbecc_enc_t kSq[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t kCu[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t d[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t t[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t a1[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t a2[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t a3[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t b2[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t one_twelfth_b2[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t half[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t kSqInv[MAX_WBECC_ENC_T_SIZE];
        wbecc_enc_t kCuInv[MAX_WBECC_ENC_T_SIZE];
    } wbecc_fast_table_curve_t;

    
    
    int wbecc_projectify(const wbecc_affine_point_t * const s, wbecc_projective_point_t * const r,
                        const wbecc_table_curve_t * const c, const wbecc_enc_cfg *cfg);

    
    
    int wbecc_affinify(const wbecc_projective_point_t * const s, wbecc_affine_point_t * const r, 
                        const wbecc_table_curve_t * const c, const wbecc_enc_cfg *cfg);

    int wbecc_is_point_affine(wbecc_affine_point_t * const s, const wbecc_table_curve_t * const c, const wbecc_enc_cfg *cfg);

    
    int wbecc_double(const wbecc_projective_point_t *s, wbecc_projective_point_t *const r, 
                        const wbecc_table_curve_t * const c, const wbecc_enc_cfg *cfg);
    
    int wbecc_add(const wbecc_projective_point_t *s, const wbecc_projective_point_t *t,
                        wbecc_projective_point_t *r, const wbecc_table_curve_t * const c, const wbecc_enc_cfg *cfg);
                        
    int wbecc_full_add(const wbecc_projective_point_t *s, const wbecc_projective_point_t *t,
                        wbecc_projective_point_t *r, const wbecc_table_curve_t * const c, const wbecc_enc_cfg *cfg);
                        
    int wbecc_full_sub(const wbecc_projective_point_t *s, const wbecc_projective_point_t *t,
                        wbecc_projective_point_t *r, const wbecc_table_curve_t * const c, const wbecc_enc_cfg *cfg);
                        
    int wbecc_mult(const wbecc_enc_t *d, const uint32_t dlen, 
                        const wbecc_projective_point_t * const s,
                        wbecc_projective_point_t * const r, const wbecc_table_curve_t * const c, const wbecc_enc_cfg *cfg);

    int wbecc_convert_from_affine_weierstrass(
        const wbecc_fst_funcs_t *funcs,
        const wbecc_domain_host_t *domain,
        const wbecc_fast_table_curve_t *fast_table_curve,
        const wbecc_enc_t * const x,
        const wbecc_enc_t * const y,
        wbecc_point_host_t * const output
    );

    int wbecc_convert_to_affine_weierstrass(
            const wbecc_fst_funcs_t *funcs,
            const wbecc_domain_host_t *domain,
            const wbecc_fast_table_curve_t *fast_table_curve,
            const wbecc_point_host_t * const input,
            uint8_t * const x_out,
            unsigned int x_size,
            uint8_t * const y_out,
            unsigned int y_size,
            unsigned int * const x_bytes_written,
            unsigned int * const y_bytes_written
    );
        
    int convert_domain_from_fast_to_table(
                                        const wbecc_fst_funcs_t *funcs,
                                        const wbecc_domain_host_t *domain,
                                        wbecc_fast_table_curve_t *fast_table_curve
    );

    #ifdef __cplusplus
    }
    #endif
#endif
