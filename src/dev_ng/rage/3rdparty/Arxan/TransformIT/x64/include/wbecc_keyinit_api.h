/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/


#ifndef __WBECC_KEYINIT_API_H__
#define __WBECC_KEYINIT_API_H__

#include <string>
#include <vector>

#include "wbecc_encoding_types.h"
#include "wbecc_dsa_api.h"
#include "wbecc_eg_api.h"
#include "wbecc_dh_api.h"
#include "wbecc_ecc_alu.h"
#include "wbecc_common_api.h"

#include "commonWBACommandLine.h" 

typedef struct _wbecc_meta_data { 
    uint8_t type_width;
    uint8_t number_of_encodings;
    uint8_t number_of_poisoned_encodings;
    wbecc_enc_t *nibble_encode;
    std::vector<wbecc_enc_t> poison_list;
    uint8_t inst_uuid[16];
} wbecc_meta_data;


int load_meta_data(const std::string path, wbecc_meta_data * meta_data);

int write_pkey(std::string outdir, std::string prefix, std::string suffix, std::string name, std::string type, std::string type_hdr, void *pkey, uint32_t len, output_format& outfmt);

int encode_key_pair(uint8_t *pkey, uint32_t len, WBECC_DOMAIN_PARAMS selected_curve, wbecc_meta_data *meta_data, wbecc_key_pair_t *outkey, 
                        rand_provider_ctx &rand_ctx);

int encode_domain_parameters(wbecc_table_curve_t *output_curve, WBECC_DOMAIN_PARAMS selected_curve, wbecc_meta_data *meta_data,
                                   rand_provider_ctx &rand_ctx);

#endif

