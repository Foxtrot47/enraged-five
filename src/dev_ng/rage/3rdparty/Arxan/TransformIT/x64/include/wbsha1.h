/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBSHA1_H__
#define __WBSHA1_H__

#include "wbsha.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef BUILD_SHA1

int wbsha1(wbsha_ctx_t *sha_data);
int wbsha1_init(wbsha_ctx_t *sha_data);

#endif

#ifdef __cplusplus
}
#endif

#endif
