/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __MACHINE_H__
#define __MACHINE_H__

#include <arxstdint.h>
#if defined(__sparc__) || defined(__arm__) || defined(__arm64__) || defined(_M_ARM_FP)
  #define __ALIGNED_MEMACCESS   1
#endif

#if defined(__BYTE_ORDER)
  #if __BYTE_ORDER == __LITTLE_ENDIAN
    

  #elif __BYTE_ORDER == __BIG_ENDIAN
    

  #elif __BYTE_ORDER == __PDP_ENDIAN
    #error PDP/ARM endianness not supported.
  #else
    #error Unknown endianness.
  #endif
#else 

  

  #if defined(__LITTLE_ENDIAN)
    #define __BYTE_ORDER __LITTLE_ENDIAN
  #elif defined(__BIG_ENDIAN)
    #define __BYTE_ORDER __BIG_ENDIAN
  #elif defined(__PDP_ENDIAN)
    #error PDP/ARM endianness not supported.
  #else 

    #define __LITTLE_ENDIAN 1234
    #define __BIG_ENDIAN    4321
    #define __PDP_ENDIAN    3412
    #if defined(__powerpc__) || defined(__PPC__) || defined(__ppc) \
        || defined (_M_PPC) || defined(__sparc__)
      #define __BYTE_ORDER __BIG_ENDIAN
    #elif defined(_M_IX86) || defined(_M_X64) || defined(_M_ARM_FP) \
        || defined(__i386__) || defined(__x86_64__) \
        || defined(__arm__)  || defined(__arm64__) || defined(__aarch64__)
      #define __BYTE_ORDER __LITTLE_ENDIAN
    #else
      #error Unable to determine endianness.
    #endif
  #endif
#endif

#if __BYTE_ORDER == __BIG_ENDIAN
  #include "machine_mword_big_endian_uint32.h"
#elif __BYTE_ORDER == __LITTLE_ENDIAN
  #include "machine_mword_little_endian_uint32.h"
#else
  #error Unable to determine endianness.
#endif

#endif

