/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __TFIT_WHITEBOXRSA_iRSA2048_H__
#define __TFIT_WHITEBOXRSA_iRSA2048_H__

#include <arxstdint.h>

#ifdef __cplusplus
extern "C" {
#endif




#include "wbc_rsa_errors.h"


#define TFIT_RSA_SUCCESS_iRSA2048 WBC_SUCCESS
#define TFIT_RSA_ERR_TOO_SMALL_iRSA2048 ERR_TOO_SMALL
#define TFIT_RSA_ERR_INVALID_PARAMETER_iRSA2048 ERR_INVALID_PARAMETER
#define TFIT_RSA_ERR_DECRYPTION_FAILED_iRSA2048 ERR_DECRYPTION_FAILED
#define TFIT_RSA_ERR_SIG_VERIFY_FAILURE_iRSA2048 ERR_SIG_VERIFY_FAILURE
#define TFIT_RSA_ERR_UNSUP_KEY_SIZE_iRSA2048 ERR_UNSUP_KEY_SIZE
#define TFIT_RSA_ERR_INTERNAL_iRSA2048 ERR_INTERNAL
#define TFIT_RSA_ERR_INTERNAL_TOO_MANY_TRIES_iRSA2048 ERR_INTERNAL_TOO_MANY_TRIES



#define TFIT_RSA_ERR_INVALID_KEY_iRSA2048 ERR_INVALID_KEY
#define TFIT_RSA_ERR_INVALID_MODULUS_iRSA2048 ERR_INVALID_MODULUS
#define TFIT_RSA_ERR_INVALID_PRIME_iRSA2048 ERR_INVALID_PRIME



#define TFIT_RSA_VALIDATE_SUCCESS_iRSA2048 0
#define TFIT_RSA_VALIDATE_FAILURE_iRSA2048 1



#define TFIT_MOD_BITS_iRSA2048 2048
#define WORD_BITS (sizeof(uint32_t)*8)
#define WORD_BYTES (sizeof(uint32_t))
#define TFIT_WORDS_iRSA2048 ((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+((TFIT_MOD_BITS_iRSA2048%WORD_BITS==0)?0:1))
#define SHA1_HASH_LENGTH_OCTETS 20
#define SHA256_HASH_LENGTH_OCTETS 32



#define EME_OAEP_IN_BYTES(m) ((_size)(m) - (SHA1_HASH_LENGTH_OCTETS << 1) - 2)
#define EME_OAEP_OUT_BYTES(m) ((_size)(m))
#define PKCS1_V15_IN_BYTES(m) ((_size)(m) - 11)
#define PKCS1_V15_OUT_BYTES(m) ((_size)(m))



#define WBC_PKCS1_V15_PADDING_PUB 1
#define WBC_PKCS1_V15_PADDING_PRIV 2
#define WBC_PKCS1_OAEP_PADDING 3
#define WBC_USER_PREPADDED_NULL_PADDING 4



#define WBC_SIGTYPE_PKCS1_SHA1 0x1004
#define WBC_SIGTYPE_PKCS1_SHA256 0x1005
#define WBC_SIGTYPE_EMSAPSS_SHA256 0x1006
#define WBC_SIGTYPE_EMSAPSS_SHA1 0x1007



extern unsigned long TFIT_rsa_pool_iRSA2048[128];
unsigned int TFIT_rsa_random_pool_size_iRSA2048();



#ifndef __WBRSA_TYPES_DEFINED__
#define __WBRSA_TYPES_DEFINED__ 1
  typedef uint32_t mword;
  typedef uint64_t mdword;
  typedef unsigned int _size;
#endif
#define TFIT_BYTES_iRSA2048 (TFIT_WORDS_iRSA2048 * sizeof(mword))



typedef struct __TFIT_RSAKeyStruct_iRSA2048{
  uint8_t uuid[16];
_size modulus_size_0x1e;
mword modulus_context_0x17;
mword modulus_0x2c[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x5;
mword modulus_0x5[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x1__col_0x0__edg_0x2;
uint8_t key__row_0x3__col_0x2__edg_0x2c[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword key__row_0x3__col_0x2__edg_0x28[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_0x10[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword key__row_0x1__col_0x0__edg_0xe[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_context_0x24;
_size size__row_0x2__col_0x3__edg_0x11;
mword modulus_0x16[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x7[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword key__row_0x3__col_0x0__edg_0x26[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword key__row_0x1__col_0x0__edg_0x2[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size modulus_size_0x2;
uint8_t key__row_0x3__col_0x0__edg_0x22[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword key__row_0x3__col_0x1__edg_0x21[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_context_0xe;
mword key__row_0x1__col_0x3__edg_0x3[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size size__row_0x1__col_0x1__edg_0xd;
mword modulus_context_0x2d;
_size size__row_0x1__col_0x3__edg_0x3;
mword modulus_context_0x4;
mword modulus_0x19[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x2f[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword key__row_0x2__col_0x3__edg_0x11[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_0xa[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x2c;
mword modulus_context_0x2b;
_size modulus_size_0x1c;
_size modulus_size_0xe;
_size modulus_size_0x24;
mword modulus_context_0x3;
mword modulus_0x21[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x1__col_0x1__edg_0x5;
mword modulus_0x2[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x18[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x11[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x2__col_0x2__edg_0x17;
_size modulus_size_0x2a;
mword key__row_0x2__col_0x0__edg_0x12[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size size__row_0x1__col_0x1__edg_0xa;
_size size__row_0x3__col_0x2__edg_0x28;
_size modulus_size_0x1b;
_size size__row_0x1__col_0x0__edg_0x8;
_size size__row_0x1__col_0x3__edg_0x7;
_size size__row_0x3__col_0x1__edg_0x2a;
mword modulus_0x1e[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x2b[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x13[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x12;
mword modulus_0x22[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x1__col_0x2__edg_0x4;
mword modulus_context_0x11;
mword modulus_context_0x1b;
uint8_t key__row_0x3__col_0x1__edg_0x2d[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword modulus_0x24[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x29[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0xb[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size modulus_size_0x10;
mword modulus_context_0x25;
mword key__row_0x1__col_0x2__edg_0x9[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_0x25[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size modulus_size_0x12;
_size size__row_0x2__col_0x3__edg_0x1c;
mword modulus_context_0xb;
_size modulus_size_0x9;
uint8_t key__row_0x1__col_0x0__edg_0xc[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
_size size__row_0x3__col_0x3__edg_0x24;
_size modulus_size_0x16;
mword key__row_0x1__col_0x0__edg_0x8[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_context_0x23;
_size modulus_size_0x25;
mword modulus_0x8[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0xa;
uint8_t hwidHash[SHA1_HASH_LENGTH_OCTETS];
uint8_t key__row_0x1__col_0x1__edg_0x1[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword modulus_context_0x13;
mword modulus_0xe[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x26;
_size size__row_0x1__col_0x3__edg_0xf;
mword modulus_context_0xf;
uint8_t key__row_0x1__col_0x2__edg_0x6[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
_size size__row_0x1__col_0x0__edg_0xe;
_size modulus_size_0x2c;
_size size__row_0x2__col_0x3__edg_0x15;
mword modulus_0x6[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x29;
mword modulus_0x2a[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size modulus_size_0x1f;
_size modulus_size_0x5;
mword key__row_0x2__col_0x3__edg_0x1d[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size size__row_0x3__col_0x3__edg_0x2f;
mword modulus_context_0x8;
mword modulus_0x1[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x2__col_0x2__edg_0x1b;
mword modulus_0xd[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x1a;
_size modulus_size_0x26;
_size modulus_size_0x8;
_size size__row_0x2__col_0x1__edg_0x16;
mword modulus_0x9[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x1__col_0x3__edg_0xb;
_size size__row_0x3__col_0x0__edg_0x2e;
_size modulus_size_0x6;
_size size__row_0x3__col_0x1__edg_0x21;
mword modulus_context_0x9;
_size size__row_0x3__col_0x2__edg_0x23;
mword key__row_0x2__col_0x2__edg_0x14[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size modulus_size_0x1;
mword modulus_context_0x2;
mword modulus_context_0x6;
mword key__row_0x1__col_0x1__edg_0xd[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_0xf[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x1f[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x3__col_0x3__edg_0x29;
_size modulus_size_0x17;
mword modulus_context_0x20;
_size modulus_size_0xa;
_size modulus_size_0x21;
mword key__row_0x2__col_0x2__edg_0x20[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
uint8_t key__row_0x3__col_0x3__edg_0x25[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
uint8_t key__row_0x3__col_0x0__edg_0x2e[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword modulus_0x1d[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x1__col_0x2__edg_0x10;
_size size__row_0x3__col_0x1__edg_0x27;
mword key__row_0x2__col_0x2__edg_0x1b[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_0x1a[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
uint8_t key__row_0x3__col_0x1__edg_0x2a[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
_size size__row_0x2__col_0x0__edg_0x1f;
mword modulus_context_0x18;
mword key__row_0x2__col_0x0__edg_0x18[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size size__row_0x2__col_0x3__edg_0x1d;
mword modulus_0x23[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x3__col_0x2__edg_0x2c;
_size hwidBufSize;
_size size__row_0x3__col_0x2__edg_0x30;
uint8_t key__row_0x3__col_0x1__edg_0x27[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
_size size__row_0x2__col_0x1__edg_0x13;
_size modulus_size_0x2b;
_size modulus_size_0x28;
_size size__row_0x3__col_0x0__edg_0x22;
mword modulus_0x17[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size modulus_size_0x30;
uint8_t key__row_0x2__col_0x0__edg_0x1f[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
_size modulus_size_0x13;
_size modulus_size_0x14;
mword modulus_context_0x30;
mword modulus_0x27[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x4[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x1f;
_size modulus_size_0x27;
_size size__row_0x2__col_0x0__edg_0x12;
_size modulus_size_0x29;
mword key__row_0x1__col_0x1__edg_0xa[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size modulus_size_0x7;
mword modulus_context_0x1d;
_size size__row_0x2__col_0x1__edg_0x19;
mword key__row_0x2__col_0x0__edg_0x1a[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size hwidBufInitSize;
_size modulus_size_0x19;
mword key__row_0x2__col_0x3__edg_0x15[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size size__row_0x3__col_0x0__edg_0x2b;
_size modulus_size_0x1d;
_size modulus_size_0x23;
mword modulus_0xc[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x1__col_0x2__edg_0x6;
mword modulus_0x12[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_0x28[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x10;
mword modulus_context_0x27;
_size size__row_0x3__col_0x1__edg_0x2d;
uint8_t key__row_0x3__col_0x3__edg_0x24[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword modulus_0x14[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size correctSize;
_size modulus_size_0x4;
mword key__row_0x3__col_0x3__edg_0x2f[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword key__row_0x1__col_0x2__edg_0x4[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size size__row_0x3__col_0x3__edg_0x25;
mword modulus_0x15[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
uint8_t key__row_0x2__col_0x1__edg_0x19[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword modulus_context_0x1c;
mword hwidBuf[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size modulus_size_0x2e;
mword modulus_context_0x2f;
mword modulus_context_0xd;
mword modulus_context_0x14;
mword modulus_context_0xc;
_size modulus_size_0x22;
_size modulus_size_0x15;
mword key__row_0x2__col_0x3__edg_0x1c[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_context_0x2a;
_size modulus_size_0xd;
mword modulus_0x26[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
uint8_t key__row_0x3__col_0x2__edg_0x23[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword modulus_0x2e[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size modulus_size_0x2f;
mword key__row_0x2__col_0x1__edg_0x13[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_context_0x22;
mword key__row_0x2__col_0x1__edg_0x16[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword hwidBufInit[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size modulus_size_0x3;
mword modulus_0x2d[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x2__col_0x1__edg_0x1e;
mword modulus_0x30[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x7;
mword modulus_context_0x1e;
_size size__row_0x1__col_0x0__edg_0xc;
uint8_t key__row_0x1__col_0x3__edg_0xb[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword key__row_0x1__col_0x2__edg_0x10[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size modulus_size_0x11;
_size modulus_size_0xb;
_size modulus_size_0xf;
mword key__row_0x2__col_0x1__edg_0x1e[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size modulus_size_0x18;
mword modulus_0x1b[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword modulus_context_0x19;
_size size__row_0x1__col_0x1__edg_0x1;
mword key__row_0x1__col_0x3__edg_0xf[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
_size size__row_0x2__col_0x0__edg_0x1a;
_size modulus_size_0x20;
uint8_t key__row_0x3__col_0x0__edg_0x2b[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
_size size__row_0x2__col_0x0__edg_0x18;
mword modulus_0x1c[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x2__col_0x2__edg_0x20;
_size modulus_size_0xc;
_size modulus_size_0x2d;
mword modulus_context_0x2e;
mword correct[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword key__row_0x3__col_0x3__edg_0x29[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
uint8_t key__row_0x3__col_0x2__edg_0x30[((TFIT_MOD_BITS_iRSA2048/5)+(((TFIT_MOD_BITS_iRSA2048%5) == 0) ? 0 : 1))];
mword key__row_0x2__col_0x2__edg_0x17[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword modulus_context_0x1;
mword modulus_0x20[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x3__col_0x0__edg_0x26;
_size modulus_size_0x1a;
mword modulus_0x3[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size size__row_0x1__col_0x2__edg_0x9;
mword modulus_context_0x15;
_size size__row_0x2__col_0x2__edg_0x14;
mword modulus_context_0x16;
mword modulus_context_0x28;
mword modulus_context_0x21;
mword key__row_0x1__col_0x1__edg_0x5[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
mword key__row_0x1__col_0x3__edg_0x7[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS) == 0) ? 0 : 1))];
} TFIT_rsa_key_iRSA2048_t;

typedef struct __TFIT_RSAScratchStruct_iRSA2048{
_size buffer_size_0xa;
_size buffer_size_0x6;
mword buffer_0xf[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size buffer_size_0x10;
_size buffer_size_0xc;
_size buffer_size_0xf;
_size buffer_size_0xb;
mword buffer_0xd[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword buffer_0x5[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword buffer_0x9[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size buffer_size_0x9;
_size buffer_size_0x5;
mword buffer_0xa[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword buffer_0x8[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword buffer_0xb[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword buffer_0xe[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword buffer_0xc[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size buffer_size_0xd;
mword buffer_0x10[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
mword buffer_0x7[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
_size buffer_size_0xe;
_size buffer_size_0x8;
_size buffer_size_0x7;
mword buffer_0x6[((TFIT_MOD_BITS_iRSA2048/WORD_BITS)+(((TFIT_MOD_BITS_iRSA2048%WORD_BITS)==0)?0:1))];
} TFIT_rsa_scratch_iRSA2048_t;


typedef struct __TFIT_rsa_context_iRSA2048_t{
  mword multContext;
  mword augmentContext[TFIT_WORDS_iRSA2048];
  _size augmentContextSize;
} TFIT_rsa_context_iRSA2048_t;



void rsa_seed_internal_prng(uint32_t seed);
void TFIT_rsa_prepare_context_iRSA2048(const TFIT_rsa_key_iRSA2048_t *key, TFIT_rsa_context_iRSA2048_t *context);
int TFIT_rsa_key_init_from_strings_iRSA2048(const char* key, const char* n, const char* const p, const char* q, uint8_t *hwid, unsigned int  hwid_length, unsigned int seed, const char* oxd_fname, TFIT_rsa_key_iRSA2048_t *wbcKeyPtr);
int TFIT_rsa_serialize_key_iRSA2048( const TFIT_rsa_key_iRSA2048_t* key, unsigned char *buf,
                       unsigned int buf_len, unsigned int* bytes_written );
int TFIT_rsa_deserialize_key_iRSA2048( const unsigned char* buf, unsigned int buf_len,
                         TFIT_rsa_key_iRSA2048_t* key );
void TFIT_rsa_cleanup_key_iRSA2048( TFIT_rsa_key_iRSA2048_t* key );
void TFIT_rsa_hwid_bind_iRSA2048(const uint8_t *hwid, unsigned int hwid_length);
int TFIT_rsa_validate_key_id_iRSA2048(const void *key);
int TFIT_rsa_encrypt_iRSA2048(int padding_type, uint8_t *plaintext, unsigned int plaintext_length, TFIT_rsa_key_iRSA2048_t *key, TFIT_rsa_context_iRSA2048_t *context, uint8_t *dest_buffer, unsigned int dest_buffer_length, unsigned int* bytes_written);
int TFIT_rsa_decrypt_iRSA2048(int padding_type, uint8_t *ciphertext, unsigned int ciphertext_length, TFIT_rsa_key_iRSA2048_t *key, TFIT_rsa_context_iRSA2048_t *context, uint8_t *dest_buffer, unsigned int dest_buffer_length, unsigned int* bytes_written);
int TFIT_rsa_sign_iRSA2048(int sig_type, uint8_t *message, unsigned int message_length, TFIT_rsa_key_iRSA2048_t *key, TFIT_rsa_context_iRSA2048_t *context, uint8_t *dest_buffer, int* bytes_written);
int TFIT_rsa_verify_iRSA2048(int sig_type, uint8_t *message, unsigned int message_length, uint8_t *signature, unsigned int signature_length, TFIT_rsa_key_iRSA2048_t *key, TFIT_rsa_context_iRSA2048_t *context);

#ifdef __cplusplus
}
#endif

#endif

