/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __MONTGOMERY_AUGMENT_H__
#define __MONTGOMERY_AUGMENT_H__

#include "wbrsa_config.h"

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size montgomery_augment(const mword *a, _size aSize, const mword *m, _size mSize, const mword *augment_context, _size acSize, mword mult_context, mword *result);

#ifdef __INLINE_OPS_
#include "montgomery_augment.c"
#endif

#endif


