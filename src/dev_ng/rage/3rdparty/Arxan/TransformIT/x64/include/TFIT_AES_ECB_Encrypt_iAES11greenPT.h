/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES-ECB/Encrypt Instance=iAES11greenPT
 */

#ifndef __TFIT_AES_ECB_ENCRYPT_iAES11greenPT_H__
#define __TFIT_AES_ECB_ENCRYPT_iAES11greenPT_H__

#include "TFIT.h"

#include "TFIT_defs_iAES11greenPT.h"
#include "wrapper_modes.h"
#define TFIT_init_wbaes_ecb_iAES11greenPT(ctx, wbaes_key) \
    wbaes_init_ecb(ctx, WBAES_ENCRYPT, WBAES_OBFUSCATED, WBAES_CLASSICAL, &TFIT_aes_ecb_iAES11greenPT, wbaes_key)

#define TFIT_update_wbaes_iAES11greenPT wbaes_update

#define TFIT_update_slice_wbaes_iAES11greenPT wbaes_update_slice

#define TFIT_final_wbaes_iAES11greenPT wbaes_final

#define TFIT_validate_wb_key_iAES11greenPT TFIT_validate_key_id_iAES11greenPT

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iAES11greenPT_<colour> */
#define TFIT_prepare_dynamic_key_iAES11greenPT(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)



#ifdef __cplusplus
extern "C"
#endif

int TFIT_wbaes_ecb_encrypt_iAES11greenPT(const TFIT_key_iAES11greenPT_t *wbaes_key, 
                                                const unsigned char *input, 
                                                const size_t input_len, 
                                                unsigned char *output);

#endif /* __TFIT_AES_ECB_ENCRYPT_iAES11greenPT_H__ */
