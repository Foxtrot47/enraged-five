/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: ECC/EG/Decrypt Instance=iECEGgreenPTfastP256
 */

#ifndef __TFIT_ECC_EG_Decrypt_iECEGgreenPTfastP256_H__
#define __TFIT_ECC_EG_Decrypt_iECEGgreenPTfastP256_H__

#include "TFIT.h"

#include "wbecc_eg_api.h"
#include "TFIT_ecc_generated_iECEGgreenPTfastP256.h"

#define TFIT_wbecc_eg_get_public_key_iECEGgreenPTfastP256(key_pair, output, output_len, bytes_written) \
    wbecc_get_public_key_fst(key_pair, &TFIT_ecc_fast_cfg_iECEGgreenPTfastP256, output, output_len, bytes_written)


#ifdef __cplusplus
extern "C" {
#endif

int TFIT_init_wbecc_eg_iECEGgreenPTfastP256(wbecc_eg_ctx_t * const ctx,
                                  const wbecc_key_pair_t * const key_pair);


int TFIT_final_wbecc_eg_iECEGgreenPTfastP256(wbecc_eg_ctx_t * const ctx,
                                   uint8_t * const output,
                                   unsigned int output_size,
                                   unsigned int * const bytes_written);

#ifdef __cplusplus
}
#endif

#define TFIT_update_wbecc_eg_iECEGgreenPTfastP256 wbecc_eg_update

#endif /* __TFIT_ECC_EG_Decrypt_iECEGgreenPTfastP256_H__ */
