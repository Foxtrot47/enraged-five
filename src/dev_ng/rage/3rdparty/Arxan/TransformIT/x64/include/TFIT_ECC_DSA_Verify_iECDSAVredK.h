/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: ECC/DSA Instance=iECDSAVredK
 */

#ifndef __TFIT_ECC_DSA_iECDSAVredK_H__
#define __TFIT_ECC_DSA_iECDSAVredK_H__

#include "TFIT.h"

#include "wbecc_dsa_api.h"
#include "TFIT_generated_ecc_encodings_iECDSAVredK.h"

#define TFIT_prepare_dynamic_key_iECDSAVredK_red(tcurve, input, input_len, key_pair) \
    wbecc_prepare_key(tcurve, &TFIT_ecc_cfg_iECDSAVredK, input, input_len, key_pair)

#define TFIT_wbecc_dsa_get_public_key_iECDSAVredK(tcurve, key_pair, output, output_len, bytes_written) \
    wbecc_get_public_key(tcurve, &TFIT_ecc_cfg_iECDSAVredK, key_pair, output, output_len, bytes_written)


#ifdef __cplusplus
extern "C"
#endif
int TFIT_init_wbecc_dsa_iECDSAVredK(wbecc_dsa_ctx_t * const ctx, 
                                   const wbecc_table_curve_t * const tcurve,
                                   const wbecc_digest_mode_t digest_mode);

#define TFIT_final_verify_wbecc_dsa_iECDSAVredK  wbecc_dsa_final_verify
#define TFIT_final_nb_verify_wbecc_dsa_iECDSAVredK  wbecc_dsa_final_nb_verify

#define TFIT_update_wbecc_dsa_iECDSAVredK wbecc_dsa_update

#endif /* __TFIT_ECC_DSA_iECDSAVredK_H__ */
