/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: FFC/DH Instance=iFFDHgreenSS
 */

#ifndef __TFIT_FFC_DH_iFFDHgreenSS_H__
#define __TFIT_FFC_DH_iFFDHgreenSS_H__

#include "TFIT.h"

#include "wbffc_dh_api.h"

#define TFIT_init_wbffc_dh_iFFDHgreenSS wbffc_dh_init
#define TFIT_wbffc_dh_generate_key_iFFDHgreenSS wbffc_dh_generate_key
#define TFIT_wbffc_dh_get_static_public_key_iFFDHgreenSS wbffc_dh_get_static_public_key
#define TFIT_wbffc_dh_get_ephemeral_public_key_iFFDHgreenSS wbffc_dh_get_ephemeral_public_key
#define TFIT_wbffc_dh_compute_secret_iFFDHgreenSS  wbffc_dh_compute_secret
#define TFIT_final_wbffc_dh_iFFDHgreenSS wbffc_dh_cleanup

#endif /* __TFIT_FFC_DH_iFFDHgreenSS_H__ */
