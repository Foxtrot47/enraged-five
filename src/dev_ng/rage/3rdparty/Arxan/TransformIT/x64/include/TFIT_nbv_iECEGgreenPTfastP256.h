/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_NBV_IECEGGREENPTFASTP256_H__
#define __TFIT_NBV_IECEGGREENPTFASTP256_H__
#define TFIT_nbv_iECEGgreenPTfastP256_8_0 ((uint8_t)0xd5)
#define TFIT_nbv_iECEGgreenPTfastP256_8_1 ((uint8_t)0x14)
#define TFIT_nbv_iECEGgreenPTfastP256_8_2 ((uint8_t)0x76)
#define TFIT_nbv_iECEGgreenPTfastP256_8_3 ((uint8_t)0x0d)
#define TFIT_nbv_iECEGgreenPTfastP256_8_4 ((uint8_t)0x7f)
#define TFIT_nbv_iECEGgreenPTfastP256_8_5 ((uint8_t)0x71)
#define TFIT_nbv_iECEGgreenPTfastP256_8_6 ((uint8_t)0x2a)
#define TFIT_nbv_iECEGgreenPTfastP256_8_7 ((uint8_t)0x00)
#define TFIT_nbv_iECEGgreenPTfastP256_8_8 ((uint8_t)0x8a)
#define TFIT_nbv_iECEGgreenPTfastP256_8_9 ((uint8_t)0x70)
#define TFIT_nbv_iECEGgreenPTfastP256_8_10 ((uint8_t)0x72)
#define TFIT_nbv_iECEGgreenPTfastP256_8_11 ((uint8_t)0x88)
#define TFIT_nbv_iECEGgreenPTfastP256_8_12 ((uint8_t)0x0a)
#define TFIT_nbv_iECEGgreenPTfastP256_8_13 ((uint8_t)0x97)
#define TFIT_nbv_iECEGgreenPTfastP256_8_14 ((uint8_t)0x51)
#define TFIT_nbv_iECEGgreenPTfastP256_8_15 ((uint8_t)0xe9)
#define TFIT_nbv_iECEGgreenPTfastP256_8_16 ((uint8_t)0x73)
#define TFIT_nbv_iECEGgreenPTfastP256_8_17 ((uint8_t)0x8e)
#define TFIT_nbv_iECEGgreenPTfastP256_8_18 ((uint8_t)0x90)
#define TFIT_nbv_iECEGgreenPTfastP256_8_19 ((uint8_t)0x7f)
#define TFIT_nbv_iECEGgreenPTfastP256_8_20 ((uint8_t)0x21)
#define TFIT_nbv_iECEGgreenPTfastP256_8_21 ((uint8_t)0x0d)
#define TFIT_nbv_iECEGgreenPTfastP256_8_22 ((uint8_t)0x4a)
#define TFIT_nbv_iECEGgreenPTfastP256_8_23 ((uint8_t)0xe9)
#define TFIT_nbv_iECEGgreenPTfastP256_8_24 ((uint8_t)0xc6)
#define TFIT_nbv_iECEGgreenPTfastP256_8_25 ((uint8_t)0x33)
#define TFIT_nbv_iECEGgreenPTfastP256_8_26 ((uint8_t)0x20)
#define TFIT_nbv_iECEGgreenPTfastP256_8_27 ((uint8_t)0x9c)
#define TFIT_nbv_iECEGgreenPTfastP256_8_28 ((uint8_t)0x41)
#define TFIT_nbv_iECEGgreenPTfastP256_8_29 ((uint8_t)0x0c)
#define TFIT_nbv_iECEGgreenPTfastP256_8_30 ((uint8_t)0x42)
#define TFIT_nbv_iECEGgreenPTfastP256_8_31 ((uint8_t)0x11)
#define TFIT_nbv_iECEGgreenPTfastP256_16_0 ((uint16_t)0xd514)
#define TFIT_nbv_iECEGgreenPTfastP256_16_1 ((uint16_t)0x760d)
#define TFIT_nbv_iECEGgreenPTfastP256_16_2 ((uint16_t)0x7f71)
#define TFIT_nbv_iECEGgreenPTfastP256_16_3 ((uint16_t)0x2a00)
#define TFIT_nbv_iECEGgreenPTfastP256_16_4 ((uint16_t)0x8a70)
#define TFIT_nbv_iECEGgreenPTfastP256_16_5 ((uint16_t)0x7288)
#define TFIT_nbv_iECEGgreenPTfastP256_16_6 ((uint16_t)0x0a97)
#define TFIT_nbv_iECEGgreenPTfastP256_16_7 ((uint16_t)0x51e9)
#define TFIT_nbv_iECEGgreenPTfastP256_16_8 ((uint16_t)0x738e)
#define TFIT_nbv_iECEGgreenPTfastP256_16_9 ((uint16_t)0x907f)
#define TFIT_nbv_iECEGgreenPTfastP256_16_10 ((uint16_t)0x210d)
#define TFIT_nbv_iECEGgreenPTfastP256_16_11 ((uint16_t)0x4ae9)
#define TFIT_nbv_iECEGgreenPTfastP256_16_12 ((uint16_t)0xc633)
#define TFIT_nbv_iECEGgreenPTfastP256_16_13 ((uint16_t)0x209c)
#define TFIT_nbv_iECEGgreenPTfastP256_16_14 ((uint16_t)0x410c)
#define TFIT_nbv_iECEGgreenPTfastP256_16_15 ((uint16_t)0x4211)
#define TFIT_nbv_iECEGgreenPTfastP256_32_0 ((uint32_t)0xd514760d)
#define TFIT_nbv_iECEGgreenPTfastP256_32_1 ((uint32_t)0x7f712a00)
#define TFIT_nbv_iECEGgreenPTfastP256_32_2 ((uint32_t)0x8a707288)
#define TFIT_nbv_iECEGgreenPTfastP256_32_3 ((uint32_t)0x0a9751e9)
#define TFIT_nbv_iECEGgreenPTfastP256_32_4 ((uint32_t)0x738e907f)
#define TFIT_nbv_iECEGgreenPTfastP256_32_5 ((uint32_t)0x210d4ae9)
#define TFIT_nbv_iECEGgreenPTfastP256_32_6 ((uint32_t)0xc633209c)
#define TFIT_nbv_iECEGgreenPTfastP256_32_7 ((uint32_t)0x410c4211)
#define TFIT_nbv_iECEGgreenPTfastP256_64_0 ((uint64_t)0xd514760d7f712a00ULL)
#define TFIT_nbv_iECEGgreenPTfastP256_64_1 ((uint64_t)0x8a7072880a9751e9ULL)
#define TFIT_nbv_iECEGgreenPTfastP256_64_2 ((uint64_t)0x738e907f210d4ae9ULL)
#define TFIT_nbv_iECEGgreenPTfastP256_64_3 ((uint64_t)0xc633209c410c4211ULL)
#endif
