/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __LIBTBLADDGEN_RUN_H__
#define __LIBTBLADDGEN_RUN_H__

#if defined(__cplusplus)
extern "C" {
#endif



#define ERR_TABLE_CORRUPT 1
#define ERR_ARG_TOO_LARGE 2
#define MAX_ARG_LEN 256
int libtbladdgen_run_add_table(
  const unsigned char arg[], unsigned int argLen,
  int is_le,
  const unsigned char (*table)[7][256], unsigned int tableLen,
  unsigned char out[]);

#if defined(__cplusplus)
}
#endif

#endif
