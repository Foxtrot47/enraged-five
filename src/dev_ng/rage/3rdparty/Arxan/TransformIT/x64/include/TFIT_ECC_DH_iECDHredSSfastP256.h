/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: ECC/DH Instance=iECDHredSSfastP256
 */

#ifndef __TFIT_ECC_DH_iECDHredSSfastP256_H__
#define __TFIT_ECC_DH_iECDHredSSfastP256_H__

#include "TFIT.h"

#include "wbecc_dh_api.h"
#include "TFIT_ecc_generated_iECDHredSSfastP256.h"

#define TFIT_wbecc_dh_get_public_key_iECDHredSSfastP256(key_pair, output, output_len, bytes_written) \
    wbecc_get_public_key_fst(key_pair, &TFIT_ecc_fast_cfg_iECDHredSSfastP256, output, output_len, bytes_written)


#ifdef __cplusplus
extern "C"
#endif
int TFIT_init_wbecc_dh_iECDHredSSfastP256(wbecc_dh_ctx_t * const ctx, 
                                  const wbecc_key_pair_t * const static_key_pair, 
                                  wbecc_dh_get_ephemeral_data get_ephemeral);

#define TFIT_cleanup_wbecc_dh_iECDHredSSfastP256 wbecc_dh_fst_cleanup

#define TFIT_wbecc_dh_get_static_public_key_iECDHredSSfastP256 wbecc_dh_get_static_public_key
#define TFIT_wbecc_dh_get_static_public_key_curve25519_iECDHredSSfastP256 wbecc_dh_get_static_public_key_curve25519
#define TFIT_wbecc_dh_get_ephemeral_public_key_iECDHredSSfastP256 wbecc_dh_get_ephemeral_public_key
#define TFIT_wbecc_dh_get_ephemeral_public_key_curve25519_iECDHredSSfastP256 wbecc_dh_get_ephemeral_public_key_curve25519
#define TFIT_wbecc_dh_generate_key_iECDHredSSfastP256 wbecc_dh_generate_key
#define TFIT_wbecc_dh_compute_secret_iECDHredSSfastP256  wbecc_dh_compute_secret

#endif /* __TFIT_ECC_DH_iECDHredSSfastP256_H__ */
