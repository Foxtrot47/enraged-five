/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBSHA_H__
#define __WBSHA_H__



#ifdef NOBUILD_SHA1
   #ifdef BUILD_SHA1
      #error Cannot define both NOBUILD_SHA1 and BUILD_SHA1
   #endif
#else
   #ifndef BUILD_SHA1
      #define BUILD_SHA1
   #endif
#endif

#ifdef NOBUILD_SHA2_224_256
   #ifdef BUILD_SHA2_224_256
      #error Cannot define both NOBUILD_SHA2_224_256 and BUILD_SHA2_224_256
   #endif
#else
   #ifndef BUILD_SHA2_224_256
      #define BUILD_SHA2_224_256
   #endif
#endif

#ifdef NOBUILD_SHA2_384_512
   #ifdef BUILD_SHA2_384_512
      #error Cannot define both NOBUILD_SHA2_384_512 and BUILD_SHA2_384_512
   #endif
#else
   #ifndef BUILD_SHA2_384_512
      #define BUILD_SHA2_384_512
   #endif
#endif

#include "wbsha_encoding_types.h"
#include "slice.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    WBSHA_CLASSICAL = 0, 
    WBSHA_OBFUSCATED = 1
} wbsha_obf_t;

typedef enum {
    WBSHA_SHA1 = 0,
    WBSHA_SHA2_224 = 1,
    WBSHA_SHA2_256 = 2,
    WBSHA_SHA2_384 = 3,
    WBSHA_SHA2_512 = 4,
    WBSHA_SHA2_512_224 = 5,
    WBSHA_SHA2_512_256 = 6
} wbsha_mode_t;

typedef enum _wbsha_status_t{
    WBSHA_INTERNAL_ERROR = -2, 
    WBSHA_HMAC_INTERNAL_ERROR = -1, 
    WBSHA_OK = 0, 
    WBSHA_DIGEST_INIT_CONTEXT_NULL = 1,
    WBSHA_DIGEST_INIT_CONFIG_NULL = 2, 
    WBSHA_DIGEST_INIT_OBFUSCATED_OXDOUT_INVALID = 3,
    WBSHA_DIGEST_INIT_CLASSICAL_OXDOUT_INVALID = 4, 
    WBSHA_DIGEST_INIT_SHA1_INITIALIZATION_FAILURE = 5, 
    WBSHA_DIGEST_INIT_SHA2_224_256_INITIALIZATION_FAILURE = 6, 
    WBSHA_DIGEST_INIT_SHA2_384_512_INITIALIZATION_FAILURE = 7, 
    WBSHA_DIGEST_INIT_NO_SHA_MODE_DEFINED_FOR_PREPROCESSOR = 8, 
    WBSHA_DIGEST_UPDATE_CONTEXT_NULL = 9, 
    WBSHA_DIGEST_UPDATE_INPUT_NULL = 10, 
    WBSHA_DIGEST_UPDATE_SLICE_SLICE_FAILURE = 11, 
    WBSHA_DIGEST_FINAL_CONTEXT_NULL = 12, 
    WBSHA_DIGEST_FINAL_OUTPUT_NULL = 13, 
    WBSHA_DIGEST_FINAL_BYTES_WRITTEN_NULL = 14, 
    WBSHA_DIGEST_FINAL_OUTPUT_LENGTH_TOO_SMALL = 15, 
    WBSHA_DIGEST_UPDATE_REMOVE_ZEROS_INVALID_INPUT_TYPE = 16, 
    WBSHA_HMAC_INIT_HMAC_AND_SHA_INSTANCE_OXD_MISMATCH = 17,
    WBSHA_HMAC_INIT_NO_OXD_PROVIDED = 18,
    WBSHA_HMAC_INIT_CONTEXT_NULL = 19, 
    WBSHA_HMAC_FINAL_SIGLEN_INCORRECT = 20,
    WBSHA_HMAC_FINAL_OUTPUT_BUFFER_TOO_SMALL = 21,
    WBSHA_HMAC_PREPARE_KEY_NULL_PARAM = 22,
    WBSHA_HMAC_PREPARE_INCORRECT_INPUT_LEN = 23
} wbsha_status_t;

typedef struct _wbsha_ctx_t {
    uint64_t length;                            
    unsigned int idx;                           
    unsigned int oxd_idx;
    unsigned int mode;                          
    enc_t W[80][ENCODED_VALUES_IN_64BITS];      
    enc_t H[8][ENCODED_VALUES_IN_64BITS];       
    enc_t K[80][ENCODED_VALUES_IN_64BITS];      
    enc_t M[128*ENCODED_VALUES_IN_8_BITS];      
    unsigned int hash_length;                   
    unsigned int hash_words;                    
    byte_t H8[128];                             
    unsigned char pt;                           
    wbsha_enc_cfg_t *cfg;
    int (*wbsha) (struct _wbsha_ctx_t*);        
} wbsha_ctx_t;

int wbsha_digest_init(
    wbsha_ctx_t * const ctx,            
    wbsha_obf_t obf_out,                
                                        
    wbsha_enc_cfg_t * const wbsha_cfg,  
    wbsha_mode_t sha_mode               
    );

int wbsha_digest_update(
    wbsha_ctx_t * const ctx, 
    wbsha_obf_t input_type,             
                                        
    const uint8_t * const input,        
    unsigned int input_len              
    );

#ifndef WBSHA_DIGEST_UPDATE_NO_REMOVE_LEADING_ZEROS
int wbsha_digest_update_remove_zeros(
    wbsha_ctx_t * const ctx, 
    wbsha_obf_t input_type,             
                                        
    const uint8_t * const input,        
    unsigned int input_len              
    );
    
int wbsha_digest_update_slice_remove_zeros( 
    wbsha_ctx_t * const ctx,
    const unsigned char * const input, 
    unsigned int full_input_len, 
    unsigned int input_len,      
    unsigned int extract_start,  
    unsigned int extract_len,    
    wbslice_pad_side_t pad_side,      
    const wbslice_table_t * const slice_table, 
    const wbslice_byte_order_t * const input_byte_order
    );
#endif



int wbsha_digest_update_slice( 
    wbsha_ctx_t * const ctx,
    const unsigned char * const input, 
    unsigned int full_input_len, 
    unsigned int input_len,      
    unsigned int extract_start,  
    unsigned int extract_len,    
    wbslice_pad_side_t pad_side,      
    const wbslice_table_t * const slice_table, 
    const wbslice_byte_order_t * const input_byte_order
    );
        
int wbsha_digest_final(
    wbsha_ctx_t * const ctx,
    uint8_t * const output,            
    unsigned int output_len,
    unsigned int * const bytes_written
    );

typedef enum {
    WBSHA_OXDIN = 0, 
    WBSHA_OXDOUT = 1
} wbsha_oxdsel_t;
    
const uint8_t * wbsha_digest_get_oxdid(
    const wbsha_ctx_t * const ctx,
    wbsha_oxdsel_t which
    );

#ifdef __cplusplus
}
#endif

#endif
