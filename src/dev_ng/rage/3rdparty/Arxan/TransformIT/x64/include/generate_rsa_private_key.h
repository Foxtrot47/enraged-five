/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __GENERATE_RSA_PRIVATE_KEY_H__
#define __GENERATE_RSA_PRIVATE_KEY_H__

#define ERR_MAX_TRIES_EXCEEDED 2
#define ERR_BAD_PUB_EXPONENT 1
#define WBC_SUCCESS 0

#include "wbrsa_config.h"

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

int generate_extended_rsa_private_key(const mword *pub, _size pubSize, _size modBits, int maxTries, mword *priv, _size *privSize, mword *mod, _size *mSize, mword *p, _size *pSize, mword *q, _size *qSize);


#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

int generate_rsa_private_key(mword *pub, _size pubSize, _size modBits, int maxTries, mword *priv, _size *privSize, mword *mod, _size *mSize);

#ifdef __INLINE_OPS_
#include "generate_rsa_private_key.c"
#endif

#endif

