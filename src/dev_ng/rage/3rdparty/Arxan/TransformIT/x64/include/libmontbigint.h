/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __MONT_BIGINT_H__
#define __MONT_BIGINT_H__

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(SUPPRESS_MAXBITS_INCLUDE)
   #include "libmontbigint_floating.h"
#endif

#include "libmontbigint_defs.h"



#define MONTBI_MIN_CHARS (MONTBI_MAX_WORDS * sizeof(MONTBI_WORD_TYPE))
#define MONTBI_MAX_BITS  (MONTBI_MAX_WORDS * MONTBI_WORD_BITS)

#ifdef __cplusplus
}
#endif

#endif

