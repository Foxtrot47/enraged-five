/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_EG_API_TBL_H__
    #define __WBECC_EG_API_TBL_H__

    #include <arxstdint.h>
    #include "wbecc.h"
    #include "wbecc_encoding_types.h"
    #include "wbecc_common_api.h"
    
    #ifdef __cplusplus
        extern "C"
        {
    #endif

    

    typedef int (*wbecc_eg_get_ephemeral_data_tbl) (const unsigned int, wbecc_enc_t * const, const wbecc_enc_cfg * const);

    typedef struct _wbecc_eg_ctx_tbl_t {
        uint32_t ws_idx;
        uint8_t ws[MAX_FIELD_ORDER_BYTES*4]; 

        uint8_t enc_dec; 
        wbecc_affine_point_t ct;
        wbecc_affine_point_t pt;
        wbecc_projective_point_t ctp;
        wbecc_projective_point_t ptp;
        wbecc_table_curve_t curve;
        wbecc_key_pair_t key_pair;
        wbecc_enc_cfg *cfg;
        wbecc_eg_get_ephemeral_data_tbl get_ephemeral;
    } wbecc_eg_ctx_tbl_t;

    int wbecc_eg_update_tbl(
        wbecc_eg_ctx_tbl_t * const ctx,
        const uint8_t * input,
        unsigned int input_len, 
        uint8_t * const output,
        unsigned int output_size,
        unsigned int * const bytes_written
    );

    int wbecc_eg_final_tbl(
        wbecc_eg_ctx_tbl_t * const ctx,
        uint8_t * const output,
        unsigned int output_size,
        unsigned int * const bytes_written
    );

    const uint8_t * wbecc_eg_get_oxdid_tbl(
        const wbecc_eg_ctx_tbl_t * const ctx,
        wbecc_oxdsel_t which
    );

    #ifdef __cplusplus
        }
    #endif
#endif
