/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_DSA_API_TBL_H__
    #define __WBECC_DSA_API_TBL_H__

    #include <arxstdint.h>
    #include "wbecc.h"
    #include "wbecc_ecc_alu.h"
    #include "wbecc_encoding_types.h"
    #include "wbecc_common_api.h"
    #include "wbecc_sha.h"

    #ifdef __cplusplus
        extern "C"
        {
    #endif

    typedef int (*wbecc_dsa_nonce_func_tbl) (const unsigned int, wbecc_enc_t * const, const wbecc_enc_cfg * const);

    typedef struct _wbecc_dsa_ctx_tbl {
        sha_ctx_t hash_ctx;
        wbecc_table_curve_t curve;
        wbecc_key_pair_t key_pair;
        wbecc_enc_cfg *cfg;
        
        
        
        
         wbecc_dsa_nonce_func_tbl get_nonce;
    } wbecc_dsa_ctx_tbl_t;

    int wbecc_dsa_final_sign_tbl(
        wbecc_dsa_ctx_tbl_t *ctx, 
        uint8_t * const r,
        unsigned int r_size,
        unsigned int * const r_bytes_written,
        uint8_t * const s,
        unsigned int s_size,
        unsigned int * const s_bytes_written
    );

    int wbecc_dsa_final_verify_tbl(
        wbecc_dsa_ctx_tbl_t *ctx,
        uint8_t * const r,
        unsigned int r_size,
        uint8_t * const s,
        unsigned int s_size,
        uint8_t * const Qx,
        unsigned int Qx_size,
        uint8_t * const Qy,
        unsigned int Qy_size,
        uint8_t * const result,
        unsigned int result_size
    );

    const uint8_t * wbecc_dsa_get_oxdid_tbl(
        const wbecc_dsa_ctx_tbl_t * const ctx,
        wbecc_oxdsel_t which
    );

    #ifdef __cplusplus
        }
    #endif
#endif
