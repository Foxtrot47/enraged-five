/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

#ifndef __CONFIG_H__
#define __CONFIG_H__

#include"arxstdint.h"

#include "montbigint_max_bits.h"

#define MONTBI_WORD_TYPE uint32_t




#define PASTE2( x, y) x ## y
#define PASTE(x, y) PASTE2(x, y) 

#define MONTBI_SUFFIX(name) PASTE(PASTE(PASTE(name,_),MONTBI_MAX_BITS),_32)



#define MONTBI_TYPESUFFIX(name) name ##_generic_32


#include "plat_generic_32.h"


#endif


