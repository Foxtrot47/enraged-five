/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef ENSUREIT_VERSION_MAJOR
#include "EnsureIT.h"
#endif

/* Helper Macros */
#define TFIT_stringize_(m) #m
#define TFIT_stringize(m) TFIT_stringize_(m)

/*
 * Please specify: An invocation location inside your application.
 *
 * Guard details:  Repair Guard which will repair AES data inside
 *                 TransformIT
 */
#ifndef USER_AES_REPAIR_INVOC_LOCATION
#define USER_AES_REPAIR_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

eit::FuncExpr& userAESRepairGuardFunction(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_AES_REPAIR_INVOC_LOCATION));
}

eit::LocExpr& userAESInvocationLocationForRepairGuard(eit::GuardSpec& gs)
{
    return userAESRepairGuardFunction(gs).entry();
}

/*
 * Please specify: An invocation location inside your application.
 *
 * Guard details:  Repair Guard which will repair ECC data inside
 *                 TransformIT
 */
#ifndef USER_ECC_REPAIR_INVOC_LOCATION
#define USER_ECC_REPAIR_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

eit::FuncExpr& userECCRepairGuardFunction(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_ECC_REPAIR_INVOC_LOCATION));
}

eit::LocExpr& userECCInvocationLocationForRepairGuard(eit::GuardSpec& gs)
{
    return userECCRepairGuardFunction(gs).entry();
}

/*
 * Please specify: An invocation location inside your application.
 *
 * Guard details:  Repair Guard which will repair FFC data inside
 *                 TransformIT
 */
#ifndef USER_FFC_REPAIR_INVOC_LOCATION
#define USER_FFC_REPAIR_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

eit::FuncExpr& userFFCRepairGuardFunction(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_FFC_REPAIR_INVOC_LOCATION));
}

eit::LocExpr& userFFCInvocationLocationForRepairGuard(eit::GuardSpec& gs)
{
    return userFFCRepairGuardFunction(gs).entry();
}

/*
 * Please specify: An invocation location inside your application.
 *
 * Guard details:  Repair Guard which will repair SHA data inside
 *                 TransformIT
 */
#ifndef USER_SHA_REPAIR_INVOC_LOCATION
#define USER_SHA_REPAIR_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

eit::FuncExpr& userSHARepairGuardFunction(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_SHA_REPAIR_INVOC_LOCATION));
}

eit::LocExpr& userSHAInvocationLocationForRepairGuard(eit::GuardSpec& gs)
{
    return userSHARepairGuardFunction(gs).entry();
}

/*
 * Please specify: An invocation location inside your application.
 *
 * Guard details: Repair Guard which will repair RSA data inside
 *                  TransformIT
 */
#ifndef USER_RSA_REPAIR_POOL_INVOC_LOCATION
#define USER_RSA_REPAIR_POOL_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

#ifndef USER_RSA_REPAIR_UUID_INVOC_LOCATION
#define USER_RSA_REPAIR_UUID_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

eit::FuncExpr& userRSARepairGuardFunctionPool(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_RSA_REPAIR_POOL_INVOC_LOCATION));
}

eit::FuncExpr& userRSARepairGuardFunctionUuid(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_RSA_REPAIR_UUID_INVOC_LOCATION));
}

eit::LocExpr& userRSAInvocationLocationForRepairGuardUuid(eit::GuardSpec& gs)
{
    return userRSARepairGuardFunctionUuid(gs).entry();
}

eit::LocExpr& userRSAInvocationLocationForRepairGuardPool(eit::GuardSpec& gs)
{
    return userRSARepairGuardFunctionPool(gs).entry();
}


/*
 * Please specify: An invocation location inside your application.
 *
 * Guard details: Repair Guard which will repair CKDF data inside
 *                  TransformIT
 */
#ifndef USER_CKDF_REPAIR_INVOC_LOCATION
#define USER_CKDF_REPAIR_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

eit::FuncExpr& userCKDFRepairGuardFunction(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_CKDF_REPAIR_INVOC_LOCATION));
}

eit::LocExpr& userCKDFInvocationLocationForRepairGuard(eit::GuardSpec& gs)
{
    return userCKDFRepairGuardFunction(gs).entry();
}

/*
 * Please specify: An invocation location inside your application.
 *
 * Guard details: Repair Guard which will repair OKDF data inside
 *                  TransformIT
 */
#ifndef USER_OKDF_REPAIR_INVOC_LOCATION
#define USER_OKDF_REPAIR_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

eit::FuncExpr& userOKDFRepairGuardFunction(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_OKDF_REPAIR_INVOC_LOCATION));
}

eit::LocExpr& userOKDFInvocationLocationForRepairGuard(eit::GuardSpec& gs)
{
    return userOKDFRepairGuardFunction(gs).entry();
}

/*
 * Please specify: An invocation location inside your application.
 *
 * Guard details: Repair Guard which will repair Slice data inside
 *                  TransformIT
 */
#ifndef USER_SLICE_REPAIR_INVOC_LOCATION
#define USER_SLICE_REPAIR_INVOC_LOCATION ENTER_SYMBOL_HERE
#endif

eit::FuncExpr& userSliceRepairGuardFunction(eit::GuardSpec& gs)
{
    return gs.function(TFIT_stringize(USER_SLICE_REPAIR_INVOC_LOCATION));
}

eit::LocExpr& userSliceInvocationLocationForRepairGuard(eit::GuardSpec& gs)
{
    return userSliceRepairGuardFunction(gs).entry();
}
// Comment out if not using AES Ciphers at all:
#define PROTECT_AES
// Comment out if not using OKDF Ciphers at all:
#define PROTECT_OKDF
// Comment out if not using SHA Ciphers at all:
#define PROTECT_SHA
// Comment out if not using FFC Ciphers at all:
#define PROTECT_FFC
// Comment out if not using XLAT Ciphers at all:
#define PROTECT_XLAT
// Comment out if not using ECC Ciphers at all:
#define PROTECT_ECC
// Comment out if not using FastECC Ciphers at all:
#define PROTECT_FastECC
// Comment out if not using CKDF Ciphers at all:
#define PROTECT_CKDF
// Comment out if not using RSA Ciphers at all:
#define PROTECT_RSA

#ifdef PROTECT_AES
   // Comment out to disable AES instance caller binding:
   #define BIND_CALLER_AES
   // Comment out if not using AES/EVP-style APIs:
   #define PROTECT_AES_EVP
   // Comment out if not using AES/Context-free APIS:
   #define PROTECT_AES_CTXFREE
   // Comment out if not using AES/NKDF/EVP-style APIs:
   #define PROTECT_AES_NKDF_EVP
   // Comment out if not using AES/NKDF/Context-free APIs:
   #define PROTECT_AES_NKDF_CTXFREE
   // Comment out if not using instance <iAES1> at all:
   #define PROTECT_iAES1
   // Comment out if not using instance <iAES11> at all:
   #define PROTECT_iAES11
   // Comment out if not using instance <iAES11greenPT> at all:
   #define PROTECT_iAES11greenPT
   // Comment out if not using instance <iAES11redPT> at all:
   #define PROTECT_iAES11redPT
   // Comment out if not using instance <iAES12> at all:
   #define PROTECT_iAES12
   // Comment out if not using instance <iAES12greenPT> at all:
   #define PROTECT_iAES12greenPT
   // Comment out if not using instance <iAES12redPT> at all:
   #define PROTECT_iAES12redPT
   // Comment out if not using instance <iAES13> at all:
   #define PROTECT_iAES13
   // Comment out if not using instance <iAES13greenPT> at all:
   #define PROTECT_iAES13greenPT
   // Comment out if not using instance <iAES13greenPTorangeIV> at all:
   #define PROTECT_iAES13greenPTorangeIV
   // Comment out if not using instance <iAES13redPT> at all:
   #define PROTECT_iAES13redPT
   // Comment out if not using instance <iAES13redPTorangeIV> at all:
   #define PROTECT_iAES13redPTorangeIV
   // Comment out if not using instance <iAES14> at all:
   #define PROTECT_iAES14
   // Comment out if not using instance <iAES14greenPT> at all:
   #define PROTECT_iAES14greenPT
   // Comment out if not using instance <iAES14orangeIVgreenPT> at all:
   #define PROTECT_iAES14orangeIVgreenPT
   // Comment out if not using instance <iAES14orangeIVredPT> at all:
   #define PROTECT_iAES14orangeIVredPT
   // Comment out if not using instance <iAES14redPT> at all:
   #define PROTECT_iAES14redPT
   // Comment out if not using instance <iAES15> at all:
   #define PROTECT_iAES15
   // Comment out if not using instance <iAES16> at all:
   #define PROTECT_iAES16
   // Comment out if not using instance <iAES16greenPT> at all:
   #define PROTECT_iAES16greenPT
   // Comment out if not using instance <iAES16redPT> at all:
   #define PROTECT_iAES16redPT
   // Comment out if not using instance <iAES17> at all:
   #define PROTECT_iAES17
   // Comment out if not using instance <iAES17greenPT> at all:
   #define PROTECT_iAES17greenPT
   // Comment out if not using instance <iAES17redPT> at all:
   #define PROTECT_iAES17redPT
   // Comment out if not using instance <iAES1greenPT> at all:
   #define PROTECT_iAES1greenPT
   // Comment out if not using instance <iAES1redPT> at all:
   #define PROTECT_iAES1redPT
   // Comment out if not using instance <iAES2> at all:
   #define PROTECT_iAES2
   // Comment out if not using instance <iAES21> at all:
   #define PROTECT_iAES21
   // Comment out if not using instance <iAES21greenPT> at all:
   #define PROTECT_iAES21greenPT
   // Comment out if not using instance <iAES21redPT> at all:
   #define PROTECT_iAES21redPT
   // Comment out if not using instance <iAES22> at all:
   #define PROTECT_iAES22
   // Comment out if not using instance <iAES22greenPT> at all:
   #define PROTECT_iAES22greenPT
   // Comment out if not using instance <iAES22redPT> at all:
   #define PROTECT_iAES22redPT
   // Comment out if not using instance <iAES23> at all:
   #define PROTECT_iAES23
   // Comment out if not using instance <iAES23greenPT> at all:
   #define PROTECT_iAES23greenPT
   // Comment out if not using instance <iAES23greenPTorangeIV> at all:
   #define PROTECT_iAES23greenPTorangeIV
   // Comment out if not using instance <iAES23redPT> at all:
   #define PROTECT_iAES23redPT
   // Comment out if not using instance <iAES23redPTorangeIV> at all:
   #define PROTECT_iAES23redPTorangeIV
   // Comment out if not using instance <iAES24> at all:
   #define PROTECT_iAES24
   // Comment out if not using instance <iAES24greenPT> at all:
   #define PROTECT_iAES24greenPT
   // Comment out if not using instance <iAES24orangeIVgreenPT> at all:
   #define PROTECT_iAES24orangeIVgreenPT
   // Comment out if not using instance <iAES24orangeIVredPT> at all:
   #define PROTECT_iAES24orangeIVredPT
   // Comment out if not using instance <iAES24redPT> at all:
   #define PROTECT_iAES24redPT
   // Comment out if not using instance <iAES25> at all:
   #define PROTECT_iAES25
   // Comment out if not using instance <iAES26> at all:
   #define PROTECT_iAES26
   // Comment out if not using instance <iAES26greenPT> at all:
   #define PROTECT_iAES26greenPT
   // Comment out if not using instance <iAES26redPT> at all:
   #define PROTECT_iAES26redPT
   // Comment out if not using instance <iAES27> at all:
   #define PROTECT_iAES27
   // Comment out if not using instance <iAES27greenPT> at all:
   #define PROTECT_iAES27greenPT
   // Comment out if not using instance <iAES27redPT> at all:
   #define PROTECT_iAES27redPT
   // Comment out if not using instance <iAES2greenPT> at all:
   #define PROTECT_iAES2greenPT
   // Comment out if not using instance <iAES2redPT> at all:
   #define PROTECT_iAES2redPT
   // Comment out if not using instance <iAES3> at all:
   #define PROTECT_iAES3
   // Comment out if not using instance <iAES3greenPT> at all:
   #define PROTECT_iAES3greenPT
   // Comment out if not using instance <iAES3greenPTorangeIV> at all:
   #define PROTECT_iAES3greenPTorangeIV
   // Comment out if not using instance <iAES3redPT> at all:
   #define PROTECT_iAES3redPT
   // Comment out if not using instance <iAES3redPTorangeIV> at all:
   #define PROTECT_iAES3redPTorangeIV
   // Comment out if not using instance <iAES4> at all:
   #define PROTECT_iAES4
   // Comment out if not using instance <iAES4greenPT> at all:
   #define PROTECT_iAES4greenPT
   // Comment out if not using instance <iAES4orangeIVgreenPT> at all:
   #define PROTECT_iAES4orangeIVgreenPT
   // Comment out if not using instance <iAES4orangeIVredPT> at all:
   #define PROTECT_iAES4orangeIVredPT
   // Comment out if not using instance <iAES4redPT> at all:
   #define PROTECT_iAES4redPT
   // Comment out if not using instance <iAES5> at all:
   #define PROTECT_iAES5
   // Comment out if not using instance <iAES6> at all:
   #define PROTECT_iAES6
   // Comment out if not using instance <iAES6greenPT> at all:
   #define PROTECT_iAES6greenPT
   // Comment out if not using instance <iAES6redPT> at all:
   #define PROTECT_iAES6redPT
   // Comment out if not using instance <iAES7> at all:
   #define PROTECT_iAES7
   // Comment out if not using instance <iAES7greenPT> at all:
   #define PROTECT_iAES7greenPT
   // Comment out if not using instance <iAES7redPT> at all:
   #define PROTECT_iAES7redPT
   // Comment out if not using instance <iCMAC> at all:
   #define PROTECT_iCMAC
   // Comment out if not using instance <iCWRAP11> at all:
   #define PROTECT_iCWRAP11
   // Comment out if not using instance <iCWRAP12> at all:
   #define PROTECT_iCWRAP12
   // Comment out if not using instance <iCWRAP12greenCTredM> at all:
   #define PROTECT_iCWRAP12greenCTredM
   // Comment out if not using instance <iCWRAP12redCTgreenM> at all:
   #define PROTECT_iCWRAP12redCTgreenM
   // Comment out if not using instance <iNKDF128greenOK> at all:
   #define PROTECT_iNKDF128greenOK
   // Comment out if not using instance <iNKDF128redOK> at all:
   #define PROTECT_iNKDF128redOK
   // Comment out if not using instance <iNKDF192greenOK> at all:
   #define PROTECT_iNKDF192greenOK
   // Comment out if not using instance <iNKDF192redOK> at all:
   #define PROTECT_iNKDF192redOK
   // Comment out if not using instance <iNKDF256greenOK> at all:
   #define PROTECT_iNKDF256greenOK
   // Comment out if not using instance <iNKDF256redOK> at all:
   #define PROTECT_iNKDF256redOK
   // Comment out if not using instance <iNWRAP11AES> at all:
   #define PROTECT_iNWRAP11AES
   // Comment out if not using instance <iNWRAP11ECC> at all:
   #define PROTECT_iNWRAP11ECC
   // Comment out if not using instance <iNWRAP11WBAES> at all:
   #define PROTECT_iNWRAP11WBAES
   // Comment out if not using instance <iNWRAP12AES> at all:
   #define PROTECT_iNWRAP12AES
   // Comment out if not using instance <iNWRAP12AESredM> at all:
   #define PROTECT_iNWRAP12AESredM
   // Comment out if not using instance <iNWRAP12ECC> at all:
   #define PROTECT_iNWRAP12ECC
   // Comment out if not using instance <iNWRAP12ECCredM> at all:
   #define PROTECT_iNWRAP12ECCredM
   // Comment out if not using instance <iNWRAP12WBAES> at all:
   #define PROTECT_iNWRAP12WBAES
   // Comment out if not using instance <iNWRAP1AES> at all:
   #define PROTECT_iNWRAP1AES
   // Comment out if not using instance <iNWRAP1ECC> at all:
   #define PROTECT_iNWRAP1ECC
   // Comment out if not using instance <iNWRAP1WBAES> at all:
   #define PROTECT_iNWRAP1WBAES
   // Comment out if not using instance <iNWRAP21AES> at all:
   #define PROTECT_iNWRAP21AES
   // Comment out if not using instance <iNWRAP21ECC> at all:
   #define PROTECT_iNWRAP21ECC
   // Comment out if not using instance <iNWRAP21WBAES> at all:
   #define PROTECT_iNWRAP21WBAES
   // Comment out if not using instance <iNWRAP22AES> at all:
   #define PROTECT_iNWRAP22AES
   // Comment out if not using instance <iNWRAP22AESredM> at all:
   #define PROTECT_iNWRAP22AESredM
   // Comment out if not using instance <iNWRAP22ECC> at all:
   #define PROTECT_iNWRAP22ECC
   // Comment out if not using instance <iNWRAP22ECCredM> at all:
   #define PROTECT_iNWRAP22ECCredM
   // Comment out if not using instance <iNWRAP22WBAES> at all:
   #define PROTECT_iNWRAP22WBAES
   // Comment out if not using instance <iNWRAP2AES> at all:
   #define PROTECT_iNWRAP2AES
   // Comment out if not using instance <iNWRAP2AESredM> at all:
   #define PROTECT_iNWRAP2AESredM
   // Comment out if not using instance <iNWRAP2ECC> at all:
   #define PROTECT_iNWRAP2ECC
   // Comment out if not using instance <iNWRAP2ECCredM> at all:
   #define PROTECT_iNWRAP2ECCredM
   // Comment out if not using instance <iNWRAP2WBAES> at all:
   #define PROTECT_iNWRAP2WBAES
#endif // PROTECT_AES

#ifdef PROTECT_OKDF
   // Comment out if not using instance <iOKDFgreenSredOK> at all:
   #define PROTECT_iOKDFgreenSredOK
   // Comment out if not using instance <iOKDFredSgreenOK> at all:
   #define PROTECT_iOKDFredSgreenOK
#endif // PROTECT_OKDF

#ifdef PROTECT_SHA
   // Comment out if not using instance <iHMACgreenK> at all:
   #define PROTECT_iHMACgreenK
   // Comment out if not using instance <iHMACredK> at all:
   #define PROTECT_iHMACredK
   // Comment out if not using instance <iSHAgreenM> at all:
   #define PROTECT_iSHAgreenM
   // Comment out if not using instance <iSHAgreenMredD> at all:
   #define PROTECT_iSHAgreenMredD
   // Comment out if not using instance <iSHAredM> at all:
   #define PROTECT_iSHAredM
   // Comment out if not using instance <iSHAredMgreenD> at all:
   #define PROTECT_iSHAredMgreenD
#endif // PROTECT_SHA

#ifdef PROTECT_FFC
   // Comment out if not using instance <iFFDHgreenSS> at all:
   #define PROTECT_iFFDHgreenSS
   // Comment out if not using instance <iFFDHredSS> at all:
   #define PROTECT_iFFDHredSS
#endif // PROTECT_FFC

#ifdef PROTECT_XLAT
   // Comment out if not using instance <iXDECgreen> at all:
   #define PROTECT_iXDECgreen
   // Comment out if not using instance <iXDECorange> at all:
   #define PROTECT_iXDECorange
   // Comment out if not using instance <iXDECred> at all:
   #define PROTECT_iXDECred
   // Comment out if not using instance <iXENCgreen> at all:
   #define PROTECT_iXENCgreen
   // Comment out if not using instance <iXENCorange> at all:
   #define PROTECT_iXENCorange
   // Comment out if not using instance <iXENCred> at all:
   #define PROTECT_iXENCred
   // Comment out if not using instance <iXORgreenXIgreenXMgreenXO> at all:
   #define PROTECT_iXORgreenXIgreenXMgreenXO
   // Comment out if not using instance <iXORgreenXIgreenXMredXO> at all:
   #define PROTECT_iXORgreenXIgreenXMredXO
   // Comment out if not using instance <iXORgreenXIredXMredXO> at all:
   #define PROTECT_iXORgreenXIredXMredXO
   // Comment out if not using instance <iXORredXIgreenXM> at all:
   #define PROTECT_iXORredXIgreenXM
   // Comment out if not using instance <iXORredXIgreenXMgreenXO> at all:
   #define PROTECT_iXORredXIgreenXMgreenXO
   // Comment out if not using instance <iXORredXIredXMgreenXO> at all:
   #define PROTECT_iXORredXIredXMgreenXO
   // Comment out if not using instance <iXORredXIredXMredXO> at all:
   #define PROTECT_iXORredXIredXMredXO
#endif // PROTECT_XLAT

#ifdef PROTECT_ECC
   // Comment out if not using instance <iECDHgreenKredSS> at all:
   #define PROTECT_iECDHgreenKredSS
   // Comment out if not using instance <iECDHredKgreenSS> at all:
   #define PROTECT_iECDHredKgreenSS
   // Comment out if not using instance <iECDSASgreenK> at all:
   #define PROTECT_iECDSASgreenK
   // Comment out if not using instance <iECDSASredK> at all:
   #define PROTECT_iECDSASredK
   // Comment out if not using instance <iECDSAVgreenK> at all:
   #define PROTECT_iECDSAVgreenK
   // Comment out if not using instance <iECDSAVredK> at all:
   #define PROTECT_iECDSAVredK
   // Comment out if not using instance <iECEGgreenK> at all:
   #define PROTECT_iECEGgreenK
   // Comment out if not using instance <iECEGgreenKredPT> at all:
   #define PROTECT_iECEGgreenKredPT
   // Comment out if not using instance <iECEGredK> at all:
   #define PROTECT_iECEGredK
   // Comment out if not using instance <iECEGredKgreenPT> at all:
   #define PROTECT_iECEGredKgreenPT
#endif // PROTECT_ECC

#ifdef PROTECT_ECC
   #ifdef PROTECT_FastECC
      // Comment out if not using instance <iECDHgreenSSfastP256> at all:
      #define PROTECT_iECDHgreenSSfastP256
      // Comment out if not using instance <iECDHredSSfastP256> at all:
      #define PROTECT_iECDHredSSfastP256
      // Comment out if not using instance <iECDSASfastP256> at all:
      #define PROTECT_iECDSASfastP256
      // Comment out if not using instance <iECDSAVfastP256> at all:
      #define PROTECT_iECDSAVfastP256
      // Comment out if not using instance <iECEGfastP256> at all:
      #define PROTECT_iECEGfastP256
      // Comment out if not using instance <iECEGgreenPTfastP256> at all:
      #define PROTECT_iECEGgreenPTfastP256
      // Comment out if not using instance <iECEGredPTfastP256> at all:
      #define PROTECT_iECEGredPTfastP256
   #endif // PROTECT_FastECC
#endif // PROTECT_ECC

#ifdef PROTECT_CKDF
   // Comment out if not using instance <iCKDFgreenSredOK> at all:
   #define PROTECT_iCKDFgreenSredOK
   // Comment out if not using instance <iCKDFredSgreenOK> at all:
   #define PROTECT_iCKDFredSgreenOK
#endif // PROTECT_CKDF

#ifdef PROTECT_RSA
   // Comment out if not using instance <iRSA1024> at all:
   #define PROTECT_iRSA1024
   // Comment out if not using instance <iRSA1024redPT> at all:
   #define PROTECT_iRSA1024redPT
   // Comment out if not using instance <iRSA2048> at all:
   #define PROTECT_iRSA2048
   // Comment out if not using instance <iRSA2048redPT> at all:
   #define PROTECT_iRSA2048redPT
#endif // PROTECT_RSA
