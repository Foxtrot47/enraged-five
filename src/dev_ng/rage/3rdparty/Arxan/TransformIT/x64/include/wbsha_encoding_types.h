/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBSHA_ENCODING_TYPES_H__
#define __WBSHA_ENCODING_TYPES_H__
#ifdef DEBUG_LFSR_BIG_TABLE_MASKING
#include <stdio.h>
#endif
#include <stdlib.h>
#include <arxstdint.h>

#ifdef DEBUG_LFSR_BIG_TABLE_MASKING
#include "cdefs.h"
#endif

#ifdef __cplusplus
extern "C"
{
#endif





#if !defined(CRUMB) && !defined(NIBBLE)
#define CRUMB
#endif

#if !defined(MAX_NUM_ENCODINGS)
#define MAX_NUM_ENCODINGS 16
#endif

#ifdef CRUMB
#define ENCODED_VALUES_BIT_WIDTH 2
#endif
#ifdef NIBBLE
#define ENCODED_VALUES_BIT_WIDTH 4
#endif

#define NUM_ENCODED_VALUES (1<<ENCODED_VALUES_BIT_WIDTH)

#define ENCODED_VALUES_IN_8_BITS    (8/ENCODED_VALUES_BIT_WIDTH)
#define ENCODED_VALUES_IN_32BITS    (32/ENCODED_VALUES_BIT_WIDTH)
#define ENCODED_VALUES_IN_64BITS    (64/ENCODED_VALUES_BIT_WIDTH)



#define MAX_NUM_MANGLED_INDICES MAX_NUM_ENCODINGS*NUM_ENCODED_VALUES



#if MAX_NUM_MANGLED_INDICES <= (1<<7)
    typedef uint8_t enc_t;
    #define CARRY_POS 7
    #define CARRY_MASK (~(1<<CARRY_POS))
#endif
#if MAX_NUM_MANGLED_INDICES > (1<<7) && MAX_NUM_MANGLED_INDICES <= (1<<15)
    typedef uint16_t enc_t;
    #define CARRY_POS 15
    #define CARRY_MASK (~(1<<CARRY_POS))
#endif
#if MAX_NUM_MANGLED_INDICES > (1<<15) && MAX_NUM_MANGLED_INDICES <= (1<<31)
    typedef uint32_t enc_t;
    #define CARRY_POS 31
    #define CARRY_MASK (~(1<<CARRY_POS))
#endif

typedef uint8_t byte_t;





typedef struct _wbsha_enc_cfg {
    unsigned int pt_lfsr_seed;
    unsigned int pt_lfsr_poly;
    unsigned int pt_table_seed_ptr;
    unsigned int pt_table_mask_ptr;
    unsigned int table_seed;
    unsigned int table_poly;
    unsigned int table_size;
    unsigned int table_lfsr_size;
    unsigned int num_mangled_indices;
    unsigned int num_crumb_indices;
    unsigned int num_nibble_indices;
    unsigned int num_input_encodings;
    unsigned int num_output_encodings;
    unsigned int input_tile_size;
    unsigned int output_tile_size;
    unsigned int num_interleaved_tables;
    unsigned int first_allowed_length;
    unsigned int toggled;
    uint8_t oxdin_uuid[16];
    uint8_t oxdout_uuid[16];
    enc_t *wbsha_tables;
    
    enc_t *pt_to_enc_len_tbl;
    enc_t *negate_lookup;
    enc_t *xor_lookup;
    enc_t *and_lookup;
    enc_t *add_lookup;
    enc_t *addc_lookup;
    #ifdef CRUMB
    enc_t *concat_11_lookup;
    enc_t *nib_to_crumb;
    enc_t *crumb_to_nib;
    #endif
    #ifdef NIBBLE
    enc_t *concat_31_lookup;
    enc_t *concat_22_lookup;
    enc_t *concat_13_lookup;
    #endif
    #ifndef WBSHA_DIGEST_UPDATE_NO_REMOVE_LEADING_ZEROS
    enc_t *greater_than_lookup;
    #endif
    enc_t *byte_to_nib;
    enc_t *nib_to_byte;
    enc_t *apply_ipad;
    enc_t *apply_opad;
} wbsha_enc_cfg_t;










static __inline void wbsha_swap(enc_t *a, enc_t *b) {
    
    
    
    
    *b = (enc_t)(*b + *a);
    *a = (enc_t)(*b - *a);
    *b = (enc_t)(*b - *a);
}

static __inline void wbsha_toggle_table_break(enc_t *table, const wbsha_enc_cfg_t *cfg){
    uint32_t lfsr = cfg->table_seed;
    uint32_t period = 0;
    uint32_t i;
    do {
        for (i = 0; i < cfg->table_size/cfg->table_lfsr_size; i+=2){
            wbsha_swap(&table[i*cfg->table_lfsr_size + period], &table[(i+1)*cfg->table_lfsr_size + lfsr - 1]);
        }
        period++;
        lfsr = (lfsr >> 1) ^ ((uint32_t)(-((int32_t)(lfsr & 1u))) & cfg->table_poly); 
    } while (lfsr != cfg->table_seed);
}



static __inline enc_t wbsha_pt_to_enc(uint32_t val, const wbsha_enc_cfg_t *cfg) {
    uint32_t lfsr = cfg->pt_lfsr_seed;
    uint32_t i;
    for (i = 0; i < val ; i++){
        lfsr = (lfsr >> 1) ^ ((uint32_t)(-((int32_t)(lfsr & 1u))) & cfg->pt_lfsr_poly); 
    }
    return (enc_t) lfsr;
}




static __inline void wbsha_prepare_pt_table(uint64_t len, const wbsha_enc_cfg_t *cfg){
    uint64_t seed = *((uint64_t *) &cfg->wbsha_tables[cfg->pt_table_seed_ptr]);
    uint64_t mask = *((uint64_t *) &cfg->wbsha_tables[cfg->pt_table_mask_ptr]);
    uint64_t tmp = 0;
    uint64_t lfsr;
    int32_t i;
    uint32_t j;
    
    uint32_t k = 0;

    while ( (unsigned) (1 << k) < cfg->first_allowed_length){
        k++;
        
        if (k > 64) break;
    }
    for (j = k; j < 64; j++){
        tmp |= (len>>j)&1;
    }
    
    lfsr = (uint64_t)(-(int64_t)tmp) ^ seed;
    
    for (i = 576; i >= 0; i--){
        lfsr = (lfsr >> 1) ^ ((uint64_t)(-(int64_t)(lfsr & 1u)) & mask); 
        if (i < NUM_ENCODED_VALUES) {
            #ifdef DEBUG_LFSR_BIG_TABLE_MASKING
            printf("Masking 0x%02X with 0x%016"PRIX64"\n", cfg->pt_to_enc_len_tbl[i],(enc_t)((cfg->pt_to_enc_len_tbl[i]) ^ ( ((((uint64_t)(-(int64_t)tmp))^lfsr)>>i) % cfg->num_mangled_indices)));
            #endif
            cfg->pt_to_enc_len_tbl[i] = (enc_t)((cfg->pt_to_enc_len_tbl[i]) ^ ( ((((uint64_t)(-(int64_t)tmp))^lfsr)>>i) % cfg->num_mangled_indices));
        }
    }
}



static __inline void wbsha_translate_input_data(byte_t data, enc_t *ptr, unsigned int idx, const wbsha_enc_cfg_t *cfg) {
    #ifdef NIBBLE
    ptr[0] = cfg->byte_to_nib[idx*256*2 + data*2 + 0];
    ptr[1] = cfg->byte_to_nib[idx*256*2 + data*2 + 1];
    #endif
    #ifdef CRUMB
    ptr[0] = cfg->nib_to_crumb[cfg->byte_to_nib[idx*256*2 + data*2 + 0]*2 + 0];
    ptr[1] = cfg->nib_to_crumb[cfg->byte_to_nib[idx*256*2 + data*2 + 0]*2 + 1];
    ptr[2] = cfg->nib_to_crumb[cfg->byte_to_nib[idx*256*2 + data*2 + 1]*2 + 0];
    ptr[3] = cfg->nib_to_crumb[cfg->byte_to_nib[idx*256*2 + data*2 + 1]*2 + 1];
    #endif
}
static __inline void wbsha_translate_pt_input_data(byte_t data, enc_t *ptr, const wbsha_enc_cfg_t *cfg) {
    #ifdef NIBBLE
    ptr[0] = wbsha_pt_to_enc((data>>4)&0xf, cfg);
    ptr[1] = wbsha_pt_to_enc((data>>0)&0xf, cfg);
    #endif
    #ifdef CRUMB
    ptr[0] = wbsha_pt_to_enc((data>>6)&0x3, cfg);
    ptr[1] = wbsha_pt_to_enc((data>>4)&0x3, cfg);
    ptr[2] = wbsha_pt_to_enc((data>>2)&0x3, cfg);
    ptr[3] = wbsha_pt_to_enc((data>>0)&0x3, cfg);
    #endif
}
static __inline enc_t wbsha_pt_to_enc_len(byte_t val, const wbsha_enc_cfg_t *cfg){
    
    if (cfg->pt_to_enc_len_tbl[val] > cfg->num_mangled_indices) {
        return wbsha_pt_to_enc(cfg->pt_to_enc_len_tbl[val] & 0x3, cfg);
    }
    return cfg->pt_to_enc_len_tbl[val];
}

static __inline void wbsha_translate_pt_input_len_data(byte_t data, enc_t *ptr, const wbsha_enc_cfg_t *cfg) {
    #ifdef NIBBLE
    ptr[0] = wbsha_pt_to_enc_len((data>>4)&0xf, cfg);
    ptr[1] = wbsha_pt_to_enc_len((data>>0)&0xf, cfg);
    #endif
    #ifdef CRUMB
    ptr[0] = wbsha_pt_to_enc_len((data>>6)&0x3, cfg);
    ptr[1] = wbsha_pt_to_enc_len((data>>4)&0x3, cfg);
    ptr[2] = wbsha_pt_to_enc_len((data>>2)&0x3, cfg);
    ptr[3] = wbsha_pt_to_enc_len((data>>0)&0x3, cfg);
    #endif
}




static __inline enc_t wbsha_enc_xor(enc_t a, enc_t b, const wbsha_enc_cfg_t *cfg){
    return cfg->xor_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}
static __inline enc_t wbsha_enc_and(enc_t a, enc_t b, const wbsha_enc_cfg_t *cfg){
    return cfg->and_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}
static __inline enc_t wbsha_enc_negate(enc_t a, const wbsha_enc_cfg_t *cfg){
    return cfg->negate_lookup[a];
}

#ifndef WBSHA_DIGEST_UPDATE_NO_REMOVE_LEADING_ZEROS
static __inline enc_t wbsha_greater_than(enc_t a, enc_t b, const wbsha_enc_cfg_t *cfg) {
    return cfg->greater_than_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}
#endif

#ifdef CRUMB
static __inline enc_t wbsha_concat_1_1(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg){
    return cfg->concat_11_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}
static __inline enc_t wbsha_concat_2_0(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg){
    (void)cfg;   

    (void)b;     

    return a;
}
static __inline enc_t wbsha_concat_0_2(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg){
    (void)cfg;   

    (void)a;     

    return b;
}
#endif

#ifdef NIBBLE
static __inline enc_t wbsha_concat_3_1(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg){
    return cfg->concat_31_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}
static __inline enc_t wbsha_concat_2_2(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg){
    return cfg->concat_22_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}
static __inline enc_t wbsha_concat_1_3(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg){
    return cfg->concat_13_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}
static __inline enc_t wbsha_concat_4_0(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg){
    (void)cfg;   

    (void)b;     

    return a;
}
static __inline enc_t wbsha_concat_0_4(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg){
    (void)cfg;   

    (void)a;     

    return b;
}
#endif

static __inline enc_t wbsha_nib_add2(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg) {
    return cfg->add_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}
static __inline enc_t wbsha_nib_add2c(enc_t a, enc_t b, const wbsha_enc_cfg_t * cfg) {
    return cfg->addc_lookup[a*cfg->num_mangled_indices*cfg->num_interleaved_tables + b];
}

static __inline void wbsha_setup_configuration(wbsha_enc_cfg_t *cfg){
    unsigned int pos = 0;

    
    if (cfg->toggled == 0) {
        wbsha_toggle_table_break(cfg->wbsha_tables, cfg);
        cfg->toggled = 1;
    }

    cfg->toggled = 1;
    cfg->xor_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    cfg->and_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    cfg->add_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    cfg->addc_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    #ifdef CRUMB
    cfg->concat_11_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    #endif
    #ifdef NIBBLE
    cfg->concat_31_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    cfg->concat_22_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    cfg->concat_13_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    #endif
    #ifndef WBSHA_DIGEST_UPDATE_NO_REMOVE_LEADING_ZEROS
    cfg->greater_than_lookup = (enc_t *) &cfg->wbsha_tables[cfg->num_mangled_indices*cfg->num_mangled_indices*0+pos++*cfg->num_mangled_indices];
    #endif
    pos = cfg->num_mangled_indices*cfg->num_mangled_indices*pos;
    cfg->negate_lookup = (enc_t *) &cfg->wbsha_tables[pos];
    pos += cfg->num_mangled_indices;
    cfg->pt_to_enc_len_tbl = (enc_t *) &cfg->wbsha_tables[pos];
    #ifdef CRUMB
    pos += (1 << 2);
    #endif
    #ifdef NIBBLE
    pos += (1 << 4);
    #endif
    cfg->byte_to_nib = (enc_t *) &cfg->wbsha_tables[pos];
    pos += cfg->num_input_encodings*256*2;
    cfg->nib_to_byte = (enc_t *) &cfg->wbsha_tables[pos];
    pos += cfg->num_output_encodings*cfg->num_nibble_indices*cfg->num_nibble_indices;
    #ifdef CRUMB
    cfg->nib_to_crumb = (enc_t *) &cfg->wbsha_tables[pos];
    pos += cfg->num_nibble_indices*2;
    cfg->crumb_to_nib = (enc_t *) &cfg->wbsha_tables[pos];
    pos += cfg->num_crumb_indices*cfg->num_crumb_indices;
    #endif
    cfg->apply_ipad = (enc_t *) &cfg->wbsha_tables[pos];
    pos += cfg->num_input_encodings*256;
    cfg->apply_opad = (enc_t *) &cfg->wbsha_tables[pos];
    pos += cfg->num_input_encodings*256;
}

#ifdef __cplusplus
}
#endif

#endif

