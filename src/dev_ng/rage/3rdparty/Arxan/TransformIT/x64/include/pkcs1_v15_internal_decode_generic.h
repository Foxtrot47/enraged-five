/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __PKCS1_V15_INTERNAL_DECODE_GENERIC_H__
#define __PKCS1_V15_INTERNAL_DECODE_GENERIC_H__

#include "wbrsa_config.h"
#include "wbc_rsa_errors.h"

#define PADDER_KEY_TYPE_PUB 0
#define PADDER_KEY_TYPE_PRIV 1

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

int pkcs1_v15_internal_decode_generic(int padderKeyType, mword *data, _size dSize, _size modOctets, uint8 *result, _size bufSize);

#ifdef __INLINE_OPS_
#include "pkcs1_v15_internal_decode_generic.c"
#endif


#endif

