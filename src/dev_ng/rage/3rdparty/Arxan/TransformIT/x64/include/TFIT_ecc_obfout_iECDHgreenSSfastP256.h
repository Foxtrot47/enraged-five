/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_ECC_OBFOUT_IECDHGREENSSFASTP256__
#define __TFIT_ECC_OBFOUT_IECDHGREENSSFASTP256__

#if defined(__cplusplus)
extern "C" {
#endif

#include<arxstdint.h>
#include "wb_ecc_static.h"
#include "TFIT_ecc_generated_iECDHgreenSSfastP256.h"

#define WBECC_OBFOUT_SUCCESS          0
#define WBECC_OBFOUT_REDUCE_FAILED    1
#define WBECC_OBFOUT_OUTBUF_TOO_SMALL 2
#define WBECC_OBFOUT_ADD_FAILED       3

int TFIT_ecc_obfuscated_affine_weierstrass_to_be_array_iECDHgreenSSfastP256
(
  uint8_t* output_x, 
  unsigned int output_x_len, 
  uint8_t* output_y, 
  unsigned int output_y_len, 
  const wbecc_affine_point_host_t* P, 
  const TFIT_ecc_constants_iECDHgreenSSfastP256* constants, 
  const wbecc_domain_host_t* domain
);


#if defined(__cplusplus)
}
#endif

#endif 


