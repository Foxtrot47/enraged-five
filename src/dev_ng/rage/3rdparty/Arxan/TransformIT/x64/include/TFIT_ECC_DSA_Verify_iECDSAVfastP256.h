/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: ECC/DSA Instance=iECDSAVfastP256
 */

#ifndef __TFIT_ECC_DSA_iECDSAVfastP256_H__
#define __TFIT_ECC_DSA_iECDSAVfastP256_H__

#include "TFIT.h"

#include "wbecc_dsa_api.h"
#include "TFIT_ecc_generated_iECDSAVfastP256.h"

#define TFIT_wbecc_dsa_get_public_key_iECDSAVfastP256(key_pair, output, output_len, bytes_written) \
    wbecc_get_public_key_fst(key_pair, &TFIT_ecc_fast_cfg_iECDSAVfastP256, output, output_len, bytes_written)


#ifdef __cplusplus
extern "C" {
#endif


int TFIT_init_wbecc_dsa_iECDSAVfastP256(wbecc_dsa_ctx_t * const ctx,
                                   const wbecc_digest_mode_t digest_mode);

int TFIT_final_verify_wbecc_dsa_iECDSAVfastP256(wbecc_dsa_ctx_t * const ctx,
                                           uint8_t * const r,
                                           unsigned int r_size,
                                           uint8_t * const s,
                                           unsigned int s_size,
                                           uint8_t * const Qx,
                                           unsigned int Qx_size,
                                           uint8_t * const Qy,
                                           unsigned int Qy_size);

int TFIT_final_nb_verify_wbecc_dsa_iECDSAVfastP256(wbecc_dsa_ctx_t * const ctx,
                                              uint8_t * const r,
                                              unsigned int r_size,
                                              uint8_t * const s,
                                              unsigned int s_size,
                                              uint8_t * const Qx,
                                              unsigned int Qx_size,
                                              uint8_t * const Qy,
                                              unsigned int Qy_size,
                                              uint8_t * const result,
                                              unsigned int result_size);

#ifdef __cplusplus
}
#endif

#define TFIT_update_wbecc_dsa_iECDSAVfastP256 wbecc_dsa_update

#endif /* __TFIT_ECC_DSA_iECDSAVfastP256_H__ */
