/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */



#ifndef __WBHMAC_H__
    #define __WBHMAC_H__

    

    #ifdef __cplusplus
        extern "C"
        {
    #endif
    
    #include "wbsha.h"
    #include "wbsha_encoding_types.h"

    

    typedef struct _wbsha_hmac_key{
        unsigned char hmac_key[256];
        unsigned char uuid[16];
    } wbsha_hmac_key_t;
   
    typedef struct _wbsha_hmac_ctx{
        unsigned int sha_mode;
        wbsha_enc_cfg_t *cfg;
        unsigned char *hmac_key;
        unsigned char uuid[16];
        wbsha_ctx_t *sha_ctx;
    } wbsha_hmac_ctx_t;

    

    int wbsha_hmac_init(
        wbsha_hmac_ctx_t * const ctx,
        wbsha_enc_cfg_t * const cfg,
        const wbsha_hmac_key_t * const hmac_key,
        wbsha_mode_t sha_mode
    );

   void wbsha_hmac_ctx_cleanup(
        wbsha_hmac_ctx_t * const ctx
    );
        
   int wbsha_hmac_update(
        wbsha_hmac_ctx_t * const ctx,
        const uint8_t * const input,
        unsigned int input_len
    );

    
    
    
    typedef enum {
        WBSHA_HMAC_SIGN = 0,
        WBSHA_HMAC_VERIFY = 1,
        WBSHA_HMAC_VERIFY_TRUNCATED = 2
    } wbsha_hmac_mode_t;
    
    int wbsha_hmac_final(
        wbsha_hmac_ctx_t* const ctx,
        wbsha_hmac_mode_t hmac_mode,
        const uint8_t * const sig,
        unsigned int sig_len,
        uint8_t* const output,
        unsigned int output_size,
        unsigned int* const bytes_written
    );

    
    int wbsha_hmac_sign(
        wbsha_enc_cfg_t * const cfg,
        const wbsha_hmac_key_t *const hmac_key,
        wbsha_mode_t sha_mode,
        const uint8_t * const input,
        unsigned int input_len,
        unsigned int tlen,
        uint8_t* const output,
        unsigned int output_size,
        unsigned int* const bytes_written
    );

    
    int wbsha_hmac_verify(
        wbsha_enc_cfg_t * const cfg,
        const wbsha_hmac_key_t *const hmac_key,
        wbsha_mode_t sha_mode,
        const uint8_t * const input,
        unsigned int input_len,
        const uint8_t * const sig,
        unsigned int sig_len,
        uint8_t* const output,
        unsigned int output_size,
        unsigned int* const bytes_written
    );
    
    int wbsha_hmac_prepare_key(
            wbsha_enc_cfg_t * const cfg,
            wbsha_mode_t sha_mode,
            const uint8_t * const input,
            unsigned int input_len,
            wbsha_hmac_key_t * const wbsha_hmac_key
        );

    typedef enum {
        WBHMAC_KEYID = 0
    } wbhmac_oxdsel_t;

    const uint8_t * wbsha_hmac_get_oxdid(
        const wbsha_hmac_ctx_t * const ctx,
        wbhmac_oxdsel_t which
    );
        
   

   static const unsigned int CHUNK_LEN_MAP[] =
      {64, 64, 64, 128, 128, 128, 128};
   #define MAX_CHUNK_LEN 128

   

   #ifdef __cplusplus
      }
   #endif

#endif

