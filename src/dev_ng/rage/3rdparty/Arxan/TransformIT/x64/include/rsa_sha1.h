/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __SHA_1__
#define __SHA_1__

#include "wbrsa_config.h"

#define SHA1_HASH_LENGTH_OCTETS 20

extern uint8 SHA1_ZERO_HASH[SHA1_HASH_LENGTH_OCTETS];

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

void rsa_sha1(const uint8 *data, _size dLen, uint8 *result);

#ifdef __INLINE_OPS_
#include "rsa_sha1.c"
#endif

#endif

