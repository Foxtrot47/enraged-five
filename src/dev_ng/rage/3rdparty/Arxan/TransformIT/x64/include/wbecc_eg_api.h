/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_EG_API_H__
    #define __WBECC_EG_API_H__

    #include <arxstdint.h>
    #include "wbecc.h"
    #include "wbecc_ecc_alu.h"
    #include "wbecc_eg_api_tbl.h"
    #include "wbecc_eg_api_fst.h"
    #include "wbecc_encoding_types.h"
    #include "wbecc_common_api.h"
    
    #ifdef __cplusplus
        extern "C"
        {
    #endif

    
    typedef enum _wbecc_eg_status_t {
        WBECC_EG_INTERNAL_ARITHMETIC_ERROR = -1,
        WBECC_EG_OK = 0,
        WBECC_EG_NULL_PARAM,
        WBECC_EG_OBFUSCATED_OXDOUT_INVALID,
        WBECC_EG_CLASSICAL_OXDOUT_INVALID,
        WBECC_EG_FST_PREPARE_CONSTANTS_ERROR,
        WBECC_EG_FST_TABLE_KEY_PROVIDED,
        WBECC_EG_COMMON_TABLE_KEY_PROVIDED,
        WBECC_EG_KEY_INCOMPATIBLE_WITH_TBL,
        WBECC_EG_DOMAIN_PARAMS_INCOMPATIBLE_WITH_INSTANCE,
        WBECC_EG_OUTPUT_BUFFER_TOO_SMALL,
        WBECC_EG_EXTRA_DATA_REMAINING_IN_INTERNAL_BUFFER,
        WBECC_EG_FAILED_TO_OBTAIN_EPHEMERAL_DATA,
        WBECC_EG_OBFUSCATED_OXDIN_INVALID,
        WBECC_EG_CLASSICAL_OXDIN_INVALID,
        WBECC_EG_OBFUSCATED_OXDIN_TILE_SIZE_INVALID,
        WBECC_EG_OBFUSCATED_OXDOUT_TILE_SIZE_INVALID,
        WBECC_EG_FST_MISSING_TABLE_CONFIG,
        WBECC_EG_INVALID_CONTEXT,
        WBECC_EG_KEYS_ARE_NEWER_FORMAT,
        WBECC_EG_PMULT_FAILURE,
    } wbecc_eg_status_t;
    
    typedef struct _wbecc_eg_ctx {
        uint8_t table;  
        union {
            wbecc_eg_ctx_tbl_t tbl;
            wbecc_eg_ctx_fst_t fst;
        } u;
    } wbecc_eg_ctx_t;

    typedef enum _wbecc_eg_direction_t {
        WBECC_EG_ENCRYPT,
        WBECC_EG_DECRYPT
    } wbecc_eg_direction_t;

    

    typedef int (*wbecc_eg_get_ephemeral_data) (const unsigned int, wbecc_enc_t * const, const wbecc_enc_cfg * const);

    int wbecc_eg_init(
        wbecc_eg_ctx_t * const ctx, 
        wbecc_eg_direction_t direction,
        wbecc_obf_t obf_in,
        wbecc_obf_t obf_out,
        const wbecc_key_pair_t * const key_pair, 
        const wbecc_table_curve_t * const tcurve,
        wbecc_eg_get_ephemeral_data get_ephemeral,
        wbecc_enc_cfg * const wbecc_cfg
    );

    int wbecc_eg_init_fst(
        wbecc_eg_ctx_t * const ctx,
        wbecc_eg_direction_t direction,
        wbecc_obf_t obf_in,
        wbecc_obf_t obf_out,
        const wbecc_key_pair_t * const key_pair,
        wbecc_eg_get_ephemeral_data get_ephemeral,
        const wbecc_fst_funcs_t * const funcs,
        const wbecc_domain_host_t * const domain
    );
    
    int wbecc_eg_fst_cleanup(
        wbecc_eg_ctx_t * const ctx
    );

    int wbecc_eg_update(
        wbecc_eg_ctx_t * const ctx,
        const uint8_t * input,
        unsigned int input_len, 
        uint8_t * const output,
        unsigned int output_size,
        unsigned int * const bytes_written
    );

    int wbecc_eg_final(
        wbecc_eg_ctx_t * const ctx,
        uint8_t * const output,
        unsigned int output_size,
        unsigned int * const bytes_written
    );

    const uint8_t * wbecc_eg_get_oxdid(
        const wbecc_eg_ctx_t * const ctx,
        wbecc_oxdsel_t which
    );

    unsigned int wbecc_eg_ciphertext_size(
        unsigned int plaintext_size,
        unsigned int modulus_size,
        unsigned int padding_size
    );

    unsigned int wbecc_eg_plaintext_size(
        unsigned int ciphertext_size,
        unsigned int modulus_size,
        unsigned int padding_size
    );

    #ifdef __cplusplus
        }
    #endif
#endif
