/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __EME_OAEP_DECODE_INTERNAL_H__
#define __EME_OAEP_DECODE_INTERNAL_H__

#include "wbrsa_config.h"
#include "eme_oaep_internal_decode_generic.h"

#define ERR_INTERNAL_DECODING_ERROR ERR_INTERNAL_GENERIC_DECODING_ERROR
#define ERR_INTERNAL_DECODING_TOO_SMALL ERR_INTERNAL_GENERIC_DECODING_TOO_SMALL 

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size eme_oaep_decode_internal(mword *data, _size dSize, _size modOctets, uint8 *result, _size bufSize);

#ifdef __INLINE_OPS_
#include "eme_oaep_decode_internal.c"
#endif


#endif

