/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __NAC_MAX_SIZES_H__
#define __NAC_MAX_SIZES_H__






#define MAX_FIELD_ORDER_BYTES 66
#define MAX_CURVE_ORDER_BYTES 66

#define MAX_WBECC_ENC_T_SIZE \
  ((MAX_FIELD_ORDER_BYTES > MAX_CURVE_ORDER_BYTES) \
  ? (MAX_FIELD_ORDER_BYTES*2) \
  : (MAX_CURVE_ORDER_BYTES*2))

#endif 

