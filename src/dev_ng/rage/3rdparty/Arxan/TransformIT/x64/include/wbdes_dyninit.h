/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/



#ifndef __WBDES_DYNINIT_H__
#define __WBDES_DYNINIT_H__

#include "arxstdint.h"

#ifdef __cplusplus
extern "C" {
#endif



int wbdes_dyninit_prepare_key( const uint8_t *const obfuscated_key, unsigned int obfuscated_key_len, const void *const dyninit_table, void * const output, unsigned int output_len, unsigned int * const bytes_written );




const uint8_t * wbdes_dyninit_get_oxd_in_id( const void * const dyninit_table );

#ifdef __cplusplus
}
#endif

#endif 


