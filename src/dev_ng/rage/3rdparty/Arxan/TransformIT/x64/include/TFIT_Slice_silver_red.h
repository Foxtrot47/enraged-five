/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: Slice Instance=silver_red
 */

#ifndef __TFIT_SLICE_silver_red_H__
#define __TFIT_SLICE_silver_red_H__

#include "TFIT.h"

#include "slice.h"
#include "TFIT_slice_table_silver_red.h"

#define TFIT_Slice_data_silver_red TFIT_data_silver_red

#endif /* __TFIT_ECC_DSA_silver_red_H__ */
