/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef HKEY_SCHEDULE_H
   #define HKEY_SCHEDULE_H

   #include <arxstdint.h>

   

   int hkey_sched(uint32_t keysize, const uint8_t * const key,
                  uint32_t * const sch_out, const uint8_t * const tbl);

#endif 


