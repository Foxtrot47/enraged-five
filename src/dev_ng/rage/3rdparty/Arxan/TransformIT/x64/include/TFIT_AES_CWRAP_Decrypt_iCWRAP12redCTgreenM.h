/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES/CWRAP Decrypt Instance=iCWRAP12redCTgreenM
 */

#ifndef __TFIT_AES_CWRAP_iCWRAP12redCTgreenM_H__
#define __TFIT_AES_CWRAP_iCWRAP12redCTgreenM_H__

#include "TFIT.h"

#include "TFIT_defs_iCWRAP12redCTgreenM.h"
#define TFIT_update_wbaes_iCWRAP12redCTgreenM wbaes_update

#define TFIT_update_slice_wbaes_iCWRAP12redCTgreenM wbaes_update_slice

#define TFIT_final_wbaes_iCWRAP12redCTgreenM wbaes_final

#define TFIT_validate_wb_key_iCWRAP12redCTgreenM TFIT_validate_key_id_iCWRAP12redCTgreenM

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iCWRAP12redCTgreenM_<colour> */
#define TFIT_prepare_dynamic_key_iCWRAP12redCTgreenM(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)


#include "wrapper_modes.h"
#include "TFIT_apinwrap_iCWRAP12redCTgreenM.h"
#include "TFIT_apicwrap_iCWRAP12redCTgreenM.h"

#define TFIT_init_wbaes_cwrap_iCWRAP12redCTgreenM(ctx, wbaes_key)    \
    wbaes_init_cwrap(ctx, WBAES_DECRYPT, WBAES_OBFUSCATED, WBAES_OBFUSCATED, &TFIT_aes_ecb_iCWRAP12redCTgreenM, wbaes_key, &TFIT_apinwrap_iCWRAP12redCTgreenM, &TFIT_wrap_iCWRAP12redCTgreenM, &TFIT_apicwrap_iCWRAP12redCTgreenM, &TFIT_wrap_iCWRAP12redCTgreenM)

#ifdef __cplusplus
extern "C"
#endif
int TFIT_wbaes_cwrap_iCWRAP12redCTgreenM(const void *wbaes_key, 
                                const uint8_t * const input, 
                                unsigned int input_len,
                                uint8_t * const output);


#endif /* __TFIT_AES_CWRAP_Decrypt_iCWRAP12redCTgreenM_H__ */
