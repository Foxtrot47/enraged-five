/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES-CBC/Decrypt Instance=iAES14
 */

#ifndef __TFIT_AES_CBC_DECRYPT_iAES14_H__
#define __TFIT_AES_CBC_DECRYPT_iAES14_H__

#include "TFIT.h"

#include "TFIT_defs_iAES14.h"
#include "wrapper_modes.h"
#define TFIT_init_wbaes_cbc_iAES14(ctx, wbaes_key, iv)                 \
    wbaes_init_cbc(ctx, WBAES_DECRYPT, WBAES_CLASSICAL, WBAES_CLASSICAL, &TFIT_aes_ecb_iAES14, wbaes_key, &wbw_apicbc_classical_dec, NULL, iv)

#define TFIT_wbaes_cbc_decrypt_iAES14(wbaes_key, input, input_len, iv, output) \
        aes_cbc_decrypt((fn_wbaes_op_t *)&TFIT_op_iAES14, wbaes_key, input, input_len, iv, output)

#define TFIT_update_wbaes_iAES14 wbaes_update

#define TFIT_update_slice_wbaes_iAES14 wbaes_update_slice

#define TFIT_final_wbaes_iAES14 wbaes_final

#define TFIT_validate_wb_key_iAES14 TFIT_validate_key_id_iAES14

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iAES14_<colour> */
#define TFIT_prepare_dynamic_key_iAES14(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)



#endif /* __TFIT_AES_CBC_DECRYPT_iAES14_H__ */
