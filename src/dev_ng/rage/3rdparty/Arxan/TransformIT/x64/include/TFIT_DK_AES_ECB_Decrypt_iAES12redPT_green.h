/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: Dynamic-Key for Instance=iAES12redPT_green
 */

#ifndef __TFIT_DK_iAES12redPT_green_H__
#define __TFIT_DK_iAES12redPT_green_H__

#include "TFIT_dyninit_iAES12redPT_green.h"
#include "wbaes_dyninit.h"

#define TFIT_prepare_dynamic_key_iAES12redPT_green(key, key_len, wbkey, wbkey_len, bytes_written) \
    wbaes_dyninit_prepare_key(key, key_len, TFIT_dyninit_iAES12redPT_green, wbkey, wbkey_len, NULL, bytes_written)

#endif /* __TFIT_DK_iAES12redPT_green_H__ */
