/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_SHA_H__
#define __WBECC_SHA_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <arxstdint.h>

    typedef enum {
        WBECC_STD_SHA1 = 0,
        WBECC_STD_SHA2_224 = 1,
        WBECC_STD_SHA2_256 = 2
    } sha_mode_t;


static __inline void UNPACK32(uint32_t x, uint8_t *str) {
    *((str) + 3) = (uint8_t) ((x)      );
    *((str) + 2) = (uint8_t) ((x) >>  8);
    *((str) + 1) = (uint8_t) ((x) >> 16);
    *((str) + 0) = (uint8_t) ((x) >> 24);
}

static __inline void PACK32(uint8_t *str, uint32_t  *x)
{
    *(x) =   ((uint32_t) *((str) + 3)      )    \
           | ((uint32_t) *((str) + 2) <<  8)    \
           | ((uint32_t) *((str) + 1) << 16)    \
           | ((uint32_t) *((str) + 0) << 24);   \
}

static __inline void UNPACK64(uint64_t x, uint8_t *str) {
    *((str) + 7) = (uint8_t) ((x)      );
    *((str) + 6) = (uint8_t) ((x) >>  8);
    *((str) + 5) = (uint8_t) ((x) >> 16);
    *((str) + 4) = (uint8_t) ((x) >> 24);
    *((str) + 3) = (uint8_t) ((x) >> 32);
    *((str) + 2) = (uint8_t) ((x) >> 40);
    *((str) + 1) = (uint8_t) ((x) >> 48);
    *((str) + 0) = (uint8_t) ((x) >> 56);
}


typedef struct _SHA_CTX {
    uint64_t length; 
    uint32_t idx;
    uint32_t mode;
    uint32_t hash_length;                   
    uint32_t hash_words;                    
    uint32_t W[80];
    uint32_t M[16];
    uint8_t Ma[64];
    uint32_t K[64];
    uint32_t H[8];
    uint8_t Ha[8*16];
    int (*sha) (struct _SHA_CTX*);
    int (*sha_ctx_update)(struct _SHA_CTX*, const uint8_t*, uint32_t, uint32_t);
    int (*sha_ctx_final)(struct _SHA_CTX*, uint32_t);
} sha_ctx_t;

int sha_ctx_init(sha_ctx_t *sha_data, sha_mode_t sha_mode);
int sha_ctx_update(sha_ctx_t *sha_data, const uint8_t * data, uint32_t length);
int sha_ctx_final(sha_ctx_t *sha_data);

#ifdef __cplusplus
}
#endif


#endif
