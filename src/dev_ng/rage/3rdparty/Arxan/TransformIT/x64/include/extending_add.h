/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __EXTENDING_ADD_H__
#define __EXTENDING_ADD_H__

#include "wbrsa_config.h"

#define EXTENDING_ADD(a,b,r,c) \
{ \
  r = (a) + (b);  \
  if(((r) < (a)) || ((r) < (b))){ \
    c = 1; \
  } \
  else{ \
    c = 0; \
  } \
}

#define EXTENDING_ADD_WITH_CARRY(a,b,cIn,r,cOut,temp) \
{ \
  temp = (mdword)(a) + (b) + (cIn); \
  r = (mword)temp;  \
  cOut = (mword)(temp >> (sizeof(mword)*8)); \
}

#endif

