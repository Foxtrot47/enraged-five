/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: XLAT/XOR Instance=iXORgreenXIgreenXMredXO
 */

#ifndef __TFIT_XLAT_XOR_iXORgreenXIgreenXMredXO_H__
#define __TFIT_XLAT_XOR_iXORgreenXIgreenXMredXO_H__

#include "TFIT.h"

#include "TFIT_apixor_iXORgreenXIgreenXMredXO.h"
#include "TFIT_xlatdef_iXORgreenXIgreenXMredXO.h"
#include "TFIT_xlat_iXORgreenXIgreenXMredXO.h"

#define TFIT_init_xlat_xor_iXORgreenXIgreenXMredXO(ctx) \
    wbxlat_init_xor(ctx, WBXLAT_ENCODE, WBXLAT_OBFUSCATED, WBXLAT_OBFUSCATED, WBXLAT_OBFUSCATED, &TFIT_apixor_iXORgreenXIgreenXMredXO, &TFIT_xlat_iXORgreenXIgreenXMredXO)
#define TFIT_update_xlat_xor_iXORgreenXIgreenXMredXO wbxlat_update
#define TFIT_final_xlat_xor_iXORgreenXIgreenXMredXO wbxlat_final

#define TFIT_xlat_xor_iXORgreenXIgreenXMredXO(src, len, mask, dest) \
    TFIT_opxor_iXORgreenXIgreenXMredXO((const unsigned char*)&TFIT_xlat_iXORgreenXIgreenXMredXO, src, len, mask, dest)

#endif /* __TFIT_XLAT_XOR_iXORgreenXIgreenXMredXO_H__ */
