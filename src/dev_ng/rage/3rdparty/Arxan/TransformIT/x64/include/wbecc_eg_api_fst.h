/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_EG_API_FST_H__
    #define __WBECC_EG_API_FST_H__

    #include <arxstdint.h>
    #include <stddef.h>
    #include "wbecc.h"
    #include "wbecc_encoding_types.h"
    #include "wbecc_common_api.h"
    #include "wb_ecc_static.h"
    #include "wbecc_ecc_alu.h"

    #ifdef __cplusplus
        extern "C"
        {
    #endif
    
    

    typedef int (*wbecc_eg_get_ephemeral_data_fst) (const unsigned int, uint8_t * const, const void * const);

    typedef struct _wbecc_eg_ctx_fst_t {
        uint32_t ws_idx;
        uint8_t ws[MAX_FIELD_ORDER_BYTES*4]; 

        uint8_t enc_dec; 
        const wbecc_key_pair_t *key_pair;
        const wbecc_fst_funcs_t *funcs;
        const wbecc_domain_host_t *domain;
        wbecc_eg_get_ephemeral_data_fst get_ephemeral;
        
        uint8_t *constants;
        uint8_t *pmscratch;

        wbecc_fast_table_curve_t fast_table_curve;

        wbecc_pm_table_t pmtable;
        wbecc_obf_t obf_out;
        wbecc_obf_t obf_in;
        uint8_t oxdout_uuid[16];
    } wbecc_eg_ctx_fst_t;

    int wbecc_eg_update_fst(
        wbecc_eg_ctx_fst_t * const ctx,
        const uint8_t * input,
        unsigned int input_len, 
        uint8_t * const output,
        unsigned int output_size,
        unsigned int * const bytes_written
    );

    int wbecc_eg_final_fst(
        wbecc_eg_ctx_fst_t * const ctx,
        uint8_t * const output,
        unsigned int output_size,
        unsigned int * const bytes_written
    );

    const uint8_t * wbecc_eg_get_oxdid_fst(
        const wbecc_eg_ctx_fst_t * const ctx,
        wbecc_oxdsel_t which
    );

    #ifdef __cplusplus
        }
    #endif
#endif
