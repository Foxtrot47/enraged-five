/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: CMLA/KDF Instance=iCKDFgreenSredOK
 */

#ifndef __TFIT_CKDF_iCKDFgreenSredOK_H__
#define __TFIT_CKDF_iCKDFgreenSredOK_H__

#include "TFIT.h"
#include "cmla_kdf.h"

#ifdef __cplusplus
extern "C" 
#endif
int TFIT_wbcmla_kdf_iCKDFgreenSredOK(const unsigned char* input, unsigned int input_len, unsigned char* output, unsigned int output_len, unsigned int* bytes_written);

#endif /* __TFIT_CKDF_iCKDFgreenSredOK_H__ */
