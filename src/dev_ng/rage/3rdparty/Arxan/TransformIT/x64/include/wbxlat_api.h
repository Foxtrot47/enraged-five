/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBXLAT_API_H__
#define __WBXLAT_API_H__

#include <stdlib.h>
#include <arxstdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define XLAT_GENERATE_ENUM(ENUM) ENUM,
#define XLAT_GENERATE_STRING(STRING) #STRING,

typedef enum {
    WBXLAT_OK                           = 0,
    WBXLAT_MISMATCH_INPUT_FORM          = 1,
    WBXLAT_MISMATCH_XOR_FORM            = 2,
    WBXLAT_MISMATCH_OUTPUT_FORM         = 3,
    WBXLAT_MISMATCH_DIRECTION           = 4,
    WBXLAT_MISMATCH_BLOB                = 5,
    WBXLAT_UNALIGNED                    = 6,
    WBXLAT_OVERFLOW                     = 7,
    WBXLAT_INTERNAL_ERROR               = 8,
    WBXLAT_FUNC_ERROR                   = 9,
    WBXLAT_NULL_ARG                     = 10,
    WBXLAT_UPDATING_FINALIZED_CTX       = 11,
    WBXLAT_MEMORY_ALLOCATION_FAIL       = 12,
    WBXLAT_INVALID_MSG_SIZE             = 13,
    WBXLAT_OUTPUT_BUFFER_TOO_SMALL      = 14,
    WBXLAT_KEY_BUFFER_TOO_SHORT         = 15,
} wbxlat_status_t;

#define XLAT_FOREACH_DIRECTION(DIRECTION) \
            DIRECTION(WBXLAT_ENCODE) \
            DIRECTION(WBXLAT_DECODE) \

#define XLAT_FOREACH_FORM(FORM) \
            FORM(WBXLAT_CLASSICAL) \
            FORM(WBXLAT_OBFUSCATED) \

#define XLAT_FOREACH_OXDID(OXDID) \
            OXDID(WBXLAT_OXDIN) \
            OXDID(WBXLAT_OXDOUT) \
            OXDID(WBXLAT_OXDIN_XOR) \

#define XLAT_FOREACH_MODE(MODE) \
            MODE(WBXLAT_XLAT) \
            MODE(WBXLAT_XOR) \

typedef enum { XLAT_FOREACH_DIRECTION(XLAT_GENERATE_ENUM) } wbxlat_direction_t;
extern const char* wbxlat_direction_string[];

typedef enum { XLAT_FOREACH_FORM(XLAT_GENERATE_ENUM) } wbxlat_obf_t;
extern const char* wbxlat_obf_string[];

typedef enum { XLAT_FOREACH_OXDID(XLAT_GENERATE_ENUM) } wbxlat_oxdid_t;

typedef enum { XLAT_FOREACH_MODE(XLAT_GENERATE_ENUM) } wbxlat_mode_t;
extern const char* wbxlat_mode_string[];

typedef int (*wbxlat_common_op_func) ();



typedef int (*wbxlat_xlat_op_func) (const unsigned char *blob,
                                   const unsigned char *src,
                                   size_t octet_len,
                                   

                                   unsigned char *dest);
typedef int (*wbxlat_xor_op_func) (const unsigned char *blob,
                                   const unsigned char *src,
                                   size_t octet_len,
                                   const unsigned char *xor_mask,
                                   unsigned char *dest);

typedef struct _wbxlat_mode_info
{
    int (*validate) (const void * mkb);
    const unsigned char* (*get_oxdid) (const void *mkb, wbxlat_oxdid_t which);
    wbxlat_common_op_func op;
    wbxlat_mode_t mode;
    wbxlat_obf_t inputForm;
    wbxlat_obf_t xorForm;
    wbxlat_obf_t outputForm;
    wbxlat_direction_t direction;
    unsigned int tileSize;
    unsigned int maxMsgSize;
} wbxlat_mode_info_t;

typedef struct _wbxlat_ctx
{
    wbxlat_mode_t mode;
    wbxlat_direction_t direction;
    unsigned int tile_size;
    unsigned int msgsize_max;
    unsigned int bytes_written;
    unsigned char* buffer_in;
    unsigned char* buffer_xor;
    unsigned int buffer_index;
    unsigned int finalized : 1;
    unsigned int corrupted : 1; 
                                
    
    wbxlat_common_op_func op_func[1];
    const void* blob;
    
    
    
    const uint8_t *oxd_xor_id; 
    const uint8_t *oxd_in_id;
    const uint8_t *oxd_out_id;
} wbxlat_ctx_t;





int wbxlat_init_xor(wbxlat_ctx_t * const ctx, wbxlat_direction_t direction, 
             wbxlat_obf_t obf_in, wbxlat_obf_t obf_out, wbxlat_obf_t obf_xor,
             const wbxlat_mode_info_t * const mode_info, const void * const mode_blob
);

int wbxlat_init_xlat(wbxlat_ctx_t * const ctx, wbxlat_direction_t direction, 
             wbxlat_obf_t obf_in, wbxlat_obf_t obf_out,
             const wbxlat_mode_info_t * const mode_info, const void * const mode_blob);







int wbxlat_update(wbxlat_ctx_t * const ctx, 
             const uint8_t * const input, unsigned int input_len,
             const uint8_t * const xormask,
             uint8_t * const output, unsigned int output_len, 
             unsigned int * const bytes_written);







int wbxlat_final(wbxlat_ctx_t * const ctx, 
             uint8_t * const output, unsigned int output_len,
             unsigned int * const bytes_written);


typedef enum _wbxlat_oxdsel_t
{
  WBXLAT_XOR_OXD_ID,
  WBXLAT_INPUT_OXD_ID,
  WBXLAT_OUTPUT_OXD_ID
} wbxlat_oxdsel_t;



const uint8_t * wbxlat_get_oxdid(const wbxlat_ctx_t * const ctx, 
             wbxlat_oxdsel_t which);

#ifdef __cplusplus
}
#endif

#endif 

