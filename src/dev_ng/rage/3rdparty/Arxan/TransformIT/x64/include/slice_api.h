/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef _SLICE_API_H_
#define _SLICE_API_H_

#include "obfuscated_xchange_def.h"
#include "commonWBACommandLine.h"
#include "slice.h"

class slice_ctx
{
public:
   typedef enum _tile_dir {
      LEFT=0,
      RIGHT=1
   } tile_dir;

private:
   obfuscated_xchange_def_t* oxdin;
   obfuscated_xchange_def_t* oxdout;
   std::string outdir;
   std::string prefix;
   std::string suffix;
   output_format* outfmt;

public:
   

   slice_ctx(obfuscated_xchange_def_t* oxdin, obfuscated_xchange_def_t* oxdout, 
             output_format* outfmt,
             const std::string& outdir = std::string(""),
             const std::string& prefix = std::string(""), 
             const std::string& suffix = std::string(""));

   int gen_tables(const std::string& id = std::string("data"));
};


int create_slice_table(wbslice_table_t** to_create, 
                       uint32_t num_input_encodings, 
                       uint32_t num_output_encodings);
int get_wb_slice_table_size(wbslice_table_t* table);


int write_slice_table(const char* filename, const char* outdir, const char* prefix, 
                      const char* suffix, wbslice_table_t* to_write, int bin_format, 
                      const char* id);

#endif 
