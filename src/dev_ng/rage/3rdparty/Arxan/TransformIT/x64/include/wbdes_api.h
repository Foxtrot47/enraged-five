/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/


#ifndef __WBDES_API_H__
#define __WBDES_API_H__

#include <stdlib.h>
#include "arxstdint.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "slice.h"


#ifndef GENERATE_ENUM
#define GENERATE_ENUM(ENUM) ENUM,
#endif
#ifndef GENERATE_STRING
#define GENERATE_STRING(STRING) #STRING,
#endif

typedef enum {
    WBDES_OK = 0,
    WBDES_MISMATCH_ECB_INPUT_FORM = 1,
    WBDES_MISMATCH_ECB_OUTPUT_FORM = 2,
    WBDES_MISMATCH_ECB_DIRECTION = 3,
    WBDES_MISMATCH_ECB_KEY = 4,
    WBDES_UNALIGNED = 5,
    WBDES_OVERFLOW = 6,
    WBDES_INTERNAL_ERROR = 7,
    WBDES_FUNC_ERROR = 8,
    WBDES_NULL_ARG = 9,
    WBDES_UPDATING_FINALIZED_CTX = 10,
    WBDES_MEMORY_ALLOCATION_FAIL = 11,
    WBDES_INVALID_MSG_SIZE = 12, 
    WBDES_OUTPUT_BUFFER_TOO_SMALL = 13,
    WBDES_KEY_BUFFER_TOO_SHORT = 14,
    WBDES_KEY_DYNINIT_FAIL = 15,
    WBDES_SLICE_UNKNOWN_ERROR = 16,
    WBDES_SLICE_NULL_ARG = 17,
    WBDES_SLICE_FULL_INPUT_LEN_NOT_WORD_SIZE_MULTIPLE = 18,
    WBDES_SLICE_INTERNAL_ERROR = 19,
    WBDES_KDF_INVALID_OUTPUT_LENGTH = 20,
} wbdes_status_t;

#define WBDES_FOREACH_DIRECTION(DIRECTION) \
            DIRECTION(WBDES_ENCRYPT) \
            DIRECTION(WBDES_DECRYPT) \

#define WBDES_FOREACH_FORM(FORM) \
            FORM(WBDES_CLASSICAL) \
            FORM(WBDES_OBFUSCATED) \

#define WBDES_FOREACH_OXDID(OXDID) \
            OXDID(WBDES_OXDIN) \
            OXDID(WBDES_OXDOUT) \
            OXDID(WBDES_OXDIN_INNER) \
            OXDID(WBDES_OXDOUT_INNER) \
            OXDID(WBDES_OXDIN_IV) \
            OXDID(WBDES_OXDIN_INNER2) \
            OXDID(WBDES_OXDOUT_INNER2) \

#define WBDES_FOREACH_WRAPPER(WRAPPER) \
            WRAPPER(WBDES_ECB) \

typedef enum { WBDES_FOREACH_DIRECTION(GENERATE_ENUM) } wbdes_direction_t;
extern const char* wbdes_direction_string[];

typedef enum { WBDES_FOREACH_FORM(GENERATE_ENUM) } wbdes_obf_t;
extern const char* wbdes_obf_string[];

typedef enum { WBDES_FOREACH_OXDID(GENERATE_ENUM) } wbdes_oxdid_t;

typedef enum { WBDES_FOREACH_WRAPPER(GENERATE_ENUM) } wbdes_wrapper_mode_t;
extern const char* wbdes_wrapper_mode_string[];

typedef void (wbdes_inner_func) ();



typedef void (wbdes_ecb_op_func) (const void* wbk, const unsigned char src[8], 
                                   unsigned char dest[8]);

struct _wbdes_ecb_cipher
{
    int (*validate) (const void * mkb, size_t mkb_len);
    wbdes_ecb_op_func *op;
    const uint8_t* (*get_oxd_in)();
    const uint8_t* (*get_oxd_out)();
    wbdes_obf_t inputForm;
    wbdes_obf_t outputForm;
    wbdes_direction_t direction;
};

struct _wbdes_ctx
{
    wbdes_wrapper_mode_t mode;
    wbdes_direction_t direction;
    unsigned char* buffer;
    unsigned int buffer_index;
    unsigned int finalized : 1;
    unsigned int corrupted : 1; 
                                
    
    wbdes_inner_func* op_func;
    const void* key;
    
    
    unsigned int msg_size;          
                                    
                                    
    
    const uint8_t *oxd_in_id;
    const uint8_t *oxd_out_id;
};

typedef struct _wbdes_ecb_cipher wbdes_ecb_cipher_t;
typedef struct _wbdes_ctx wbdes_ctx_t;





int wbdes_init_ecb(wbdes_ctx_t * const ctx, wbdes_direction_t direction, 
             wbdes_obf_t obf_in, wbdes_obf_t obf_out,
             const wbdes_ecb_cipher_t * const ecb, const void * const ecb_key, const size_t ecb_key_len);







int wbdes_update(wbdes_ctx_t * const ctx, 
             const uint8_t * const input, unsigned int input_len,
             uint8_t * const output, unsigned int output_len, 
             unsigned int * const bytes_written);

int wbdes_update_slice(wbdes_ctx_t * const ctx,
    const uint8_t* const input, 
    unsigned int full_input_len,                  
                                                  
    unsigned int input_len,                       
    unsigned int extract_start,                   
                                                  
    unsigned int extract_len,                     
    wbslice_pad_side_t pad_side,                  
                                                  
                                                  
    const wbslice_table_t* const slice_table,     
                                                  
    const wbslice_byte_order_t* const input_order,
                                                  
                                                  
    uint8_t* const output,                        
    unsigned int output_len,                      
    unsigned int* const bytes_written);           
                                                  







int wbdes_final(wbdes_ctx_t * const ctx, 
             uint8_t * const output, unsigned int output_len,
             unsigned int * const bytes_written);


typedef enum _wbdes_oxdsel_t
{
  WBDES_INPUT_OXD_ID,
  WBDES_OUTPUT_OXD_ID
} wbdes_oxdsel_t;



const uint8_t * wbdes_get_oxdid(const wbdes_ctx_t * const ctx, 
             wbdes_oxdsel_t which);

#ifdef __cplusplus
}
#endif

#endif 


