/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __WB_ECC_MONTBIGINT_H__
#define __WB_ECC_MONTBIGINT_H__



#include "libmontbigint_544_32.h"

typedef montbi_int_544_32 wbecc_montbi_int;
typedef montbi_ctx_544_32 wbecc_montbi_ctx;
#define WBECC_MONTBI_MAX_WORDS MONTBI_MAX_WORDS_544_32
#define WBECC_MONTBI_WORD_TYPE MONTBI_WORD_TYPE_544_32
#define WBECC_MONTBI_WORD_BITS MONTBI_WORD_BITS_544_32

#define wbecc_montbi_is_zero montbi_is_zero_544_32
#define wbecc_montbi_is_even montbi_is_even_544_32
#define wbecc_montbi_is_one montbi_is_one_544_32
#define wbecc_montbi_cmp montbi_cmp_544_32
#define wbecc_montbi_augment montbi_augment_544_32
#define wbecc_montbi_oversize_augment montbi_oversize_augment_544_32
#define wbecc_montbi_reduce montbi_reduce_544_32
#define wbecc_montbi_mult montbi_mult_544_32
#define wbecc_montbi_add montbi_add_544_32
#define wbecc_montbi_sub montbi_sub_544_32
#define wbecc_montbi_inv montbi_inv_544_32
#define wbecc_montbi_exp montbi_exp_544_32
#define wbecc_montbi_big_exp montbi_big_exp_544_32
#define wbecc_montbi_sqrt montbi_sqrt_544_32
#define wbecc_montbi_is_equal montbi_is_equal_544_32  
#define wbecc_montbi_init montbi_init_544_32
#define wbecc_montbi_debug_print montbi_debug_print_544_32
#define wbecc_montbi_ctx_serlen montbi_ctx_serlen_544_32
#define wbecc_montbi_ctx_deserialize montbi_ctx_deserialize_544_32
#define wbecc_montbi_int_deserialize montbi_int_deserialize_544_32
#define wbecc_montbiutil_in_place_remainder montbiutil_in_place_remainder_544_32
#endif 


