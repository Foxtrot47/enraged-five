/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_H__
    #define __WBECC_H__

    #include <arxstdint.h>
    #include "wbecc_encoding_types.h"
    #include "nac_max_sizes.h"

    #define ECC160 160
    #define ECC192 192
    #define ECC224 224
    #define ECC256 256
    #define ECC384 384
    #define ECC521 521
    
    #define CURVE_SEC_P160R1    0
    #define CURVE_NIST_P192R1   1
    #define CURVE_NIST_P224R1   2
    #define CURVE_NIST_P256R1   3
    #define CURVE_NIST_P384R1   4
    #define CURVE_NIST_P521R1   5
    #define CURVE_OTHER         128
    
    #ifdef __cplusplus
        extern "C"
        {
    #endif

    typedef enum {
        WBECC_OXDOUT = 0,
        WBECC_OXDKEY = 1
    } wbecc_oxdsel_t;

    typedef struct _WBECC_DOMAIN_PARAMS {
        uint32_t CURVE_SIZE_BITS; 
        
        uint8_t P[MAX_FIELD_ORDER_BYTES];  
        uint8_t N[MAX_CURVE_ORDER_BYTES];  
        uint8_t Qx[MAX_FIELD_ORDER_BYTES]; 
        uint8_t Qy[MAX_FIELD_ORDER_BYTES]; 
        
        uint8_t A[MAX_FIELD_ORDER_BYTES];  
        uint8_t B[MAX_FIELD_ORDER_BYTES];  
        uint8_t h;      
        uint8_t curve_type;
        
        
        uint8_t EXTRA_N_BYTES;
        uint32_t bytes_for_p;
        uint32_t bytes_for_n;
    } WBECC_DOMAIN_PARAMS;
    
    extern WBECC_DOMAIN_PARAMS ECC_PubKey[6];
 
    #ifdef __cplusplus
        }
    #endif
    
#endif
