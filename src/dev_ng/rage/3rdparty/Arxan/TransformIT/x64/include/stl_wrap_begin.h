/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */




/**  @file  stl_wrap_begin.h

To keep from going postal, I created this file to help ease the occurrence
of Visual Studio warning 4702 that occurs when compiling some STL code in
release mode.  Example use:

#include <stl_wrap_begin.h>
#include    <vector>
#incldue    <set>
#include    <map>
#include <stl_wrap_end.h>

*

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4702) 
#endif
