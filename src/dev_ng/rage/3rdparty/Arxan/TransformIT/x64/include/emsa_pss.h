/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __EMSA_PSS_HEADER__
#define __EMSA_PSS_HEADER__ 1
#include "wbc_rsa_errors.h"

#define HASH_ALGO_SHA256 0x301

#define HASH_ALGO_SHA1 0x302

  int emsa_pss_encode( const uint8 * const msg,
		      int msg_len,
		      int em_bits,
		      int hash_algo,
		      uint8* enc_msg_out,
		      int* enc_msg_out_len );

  int emsa_pss_verify( const uint8 * const msg,
		      int msg_len,
		      const uint8 * const enc_msg,
		      int em_bits,
		      int hash_algo );

#endif

