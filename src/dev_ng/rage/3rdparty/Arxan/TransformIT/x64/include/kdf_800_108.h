/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __KDF_800_108_H__
#define __KDF_800_108_H__

#include <arxstdint.h>

    #ifdef __cplusplus
        extern "C"
        {
    #endif
#include "wbaes_api.h"

int wbnist_kdf108(const uint8_t * const fixed_input_data, 
                  unsigned int input_len, 
                  uint64_t length, 
                  const wbaes_ecb_cipher_t* const ecb, 
                  const void* const ecb_key,
                  const wbaes_wrapper_t* const cmac, 
                  const void* const cmac_key,
                  uint8_t * const output, 
                  unsigned int output_len, 
                  unsigned int * const bytes_written);
    #ifdef __cplusplus
        }
    #endif
#endif 
