/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_SHA_ENCODING_H_iSHAgreenM__
#define __TFIT_SHA_ENCODING_H_iSHAgreenM__

#include "wbsha_encoding_types.h"
#ifdef __cplusplus
extern "C"
{
#endif

extern enc_t TFIT_generated_tables_iSHAgreenM[];
extern unsigned int TFIT_generated_tables_len_iSHAgreenM;

extern wbsha_enc_cfg_t TFIT_sha_cfg_iSHAgreenM;

#ifdef __cplusplus
}
#endif
#endif

