/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/









#ifndef __TEXTBOOK_RIGHTSHIFT_5_H__
#define __TEXTBOOK_RIGHTSHIFT_5_H__

#include "wbrsa_config.h"

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size textbook_rightshift_5(const mword *a, _size aSize, unsigned int shiftAmount, mword *result);

#ifdef __INLINE_OPS_
#include "textbook_rightshift_5.c"
#endif

#endif


