/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#include <arxstdint.h>



#ifndef __P192_POINT_H__
#define __P192_POINT_H__ 1

#ifdef __cplusplus
    extern "C"
    {
#endif





#define MAX_BIGINT_PRODUCT 2*128

typedef struct std_bigint_type {
  int32_t length; 

  uint32_t a[MAX_BIGINT_PRODUCT]; 

} std_bigint_t;

typedef enum { INVALID_GMN=0, P384_GMN=2, P192_GMN=11, P521_GMN=17, P256_GMN=31, P224_GMN=59 } gmn_t; 
								   
								   

typedef struct modulus_type {
  gmn_t fast_modulus;
  const std_bigint_t* slow_modulus; 
} modulus_t;

char* sbint_tostring(const std_bigint_t* arg, int leading_zero_x);
unsigned int sbint_tobuffer(const std_bigint_t* arg, unsigned char** destbuf, int* destlength);
unsigned int sbint_tobuffer_be(const std_bigint_t* arg, unsigned char** destbuf, int* destlength);

std_bigint_t* sbint_allocate_and_zero( int a_length );


unsigned int sbint_buffer_init(std_bigint_t* dest, int max_number_length, const unsigned char * const buf, const unsigned int buf_len);
unsigned int sbint_buffer_init_be(std_bigint_t* dest, int max_number_length, const unsigned char * const buf, const unsigned int buf_len);
unsigned int sbint_tripplet_init( std_bigint_t* dest, int max_number_length, uint64_t arg0, uint64_t arg1, uint64_t arg2 );
unsigned int sbint_string_init( std_bigint_t* dest, int max_number_length, const char *  const hex_string, int leading_zero_x );
unsigned int sbint_word_array_init(std_bigint_t* dest, int max_number_length, const uint32_t * const buf, const unsigned int buf_len);
unsigned int sbint_init_from_gmn(std_bigint_t* dest, gmn_t num); 
unsigned int modulus_get_length(const modulus_t* mod);
unsigned int sbint_init_from_modulus(std_bigint_t* dest, const modulus_t* num); 
unsigned int modulus_init_from_sbint(modulus_t* dest, const std_bigint_t* val);
unsigned int modulus_init_from_sbint2(modulus_t* dest, const std_bigint_t* val);

void sbint_set_word(std_bigint_t* result, uint32_t word, uint32_t index);
void sbint_copy_constructor(std_bigint_t* dest, const std_bigint_t* src);



unsigned int sbint_add(std_bigint_t* result, const std_bigint_t* op_left, const std_bigint_t* op_right);
unsigned int sbint_add_and_reduce(std_bigint_t* result, const std_bigint_t* op_left, const std_bigint_t* op_right, const modulus_t *modulus);
unsigned int sbint_subtract(std_bigint_t* result, const std_bigint_t* op_left, const std_bigint_t* op_right);
unsigned int sbint_subtract_and_reduce(std_bigint_t* result, const std_bigint_t* op_left, const std_bigint_t* op_right, const modulus_t *modulus);
unsigned int sbint_multiply(std_bigint_t* result, const std_bigint_t* op_left, const std_bigint_t* op_right);
unsigned int sbint_multiply_and_reduce(std_bigint_t* result, const std_bigint_t* op_left, const std_bigint_t* op_right, const modulus_t *modulus);

unsigned int sbint_reduce(std_bigint_t* result, const std_bigint_t* dividend, const modulus_t* modulus);
unsigned int p192_reduce_fast(std_bigint_t* result, const std_bigint_t* arg);
unsigned int p224_reduce_fast(std_bigint_t* result, const std_bigint_t* arg);
unsigned int p256_reduce_fast(std_bigint_t* result, const std_bigint_t* arg);
unsigned int p384_reduce_fast(std_bigint_t* result, const std_bigint_t* arg);
unsigned int p521_reduce_fast(std_bigint_t* result, const std_bigint_t* arg);
unsigned int sbint_is_zero(const std_bigint_t* val);
unsigned int sbint_long_divide(std_bigint_t* quotient,
                         std_bigint_t* remainder,
                         const std_bigint_t* dividend,
                         const std_bigint_t* divisor);
unsigned int sbint_reciprocal( std_bigint_t* result,
                               const std_bigint_t* val,
                               const modulus_t* modulus,
                               int prime );
unsigned int sbint_reciprocal2( std_bigint_t* result, const std_bigint_t* val,
                               const modulus_t* modulus );
unsigned int sbint_exponentiate( std_bigint_t* result,
                                 const std_bigint_t* val,
                                 const std_bigint_t* exp,
                                 const modulus_t* modulus );

unsigned int sbint_left_shift(std_bigint_t* dest, const std_bigint_t* op, uint32_t num_bits);
unsigned int sbint_right_shift(std_bigint_t* dest, const std_bigint_t* op, uint32_t num_bits);
int32_t sbint_compare( const std_bigint_t* op_left, const std_bigint_t* op_right);

void sbint_randomize(std_bigint_t* arg, int max_number_length);
 
#ifdef __cplusplus
    }
#endif



#endif
