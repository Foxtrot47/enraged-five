/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __PIECEWISE_MONTGOMERY_EXPONENTIATE_H__
#define __PIECEWISE_MONTGOMERY_EXPONENTIATE_H__

#include "wbrsa_config.h"


#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size piecewise_montgomery_exponentiate(const mword *base, _size bSize, mword exp, const mword *m, _size mSize, mword context, mword *working, _size wSize, mword *result);

#ifdef __INLINE_OPS_
#include "piecewise_montgomery_exponentiate.c"
#endif


#endif


