/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __SHA256_H__
#define __SHA256_H__

#ifdef __cplusplus
extern "C"{
#endif

#ifdef _MSC_VER
  typedef unsigned __int64 uint64_t;
  typedef unsigned __int32 uint32_t;
#else
#include <arxstdint.h>
#endif

#define SHA256_INPUT_BYTES 64
#define SHA256_OUTPUT_BYTES 32
#define SHA256_HASH_LENGTH_OCTETS 32

typedef struct _sha256_ctx
{
  unsigned char buffer[SHA256_INPUT_BYTES];
  unsigned int buf_used;

  uint64_t len_in_state;
  uint32_t state[8];
}sha256_ctx;

  void sha256_init(sha256_ctx *ctx);
  void sha256_digest(sha256_ctx *ctx, const unsigned char *data, unsigned int inLen);
  void sha256_finalize(const sha256_ctx *ctx, unsigned char *out);

#ifdef __cplusplus
}
#endif

#endif
