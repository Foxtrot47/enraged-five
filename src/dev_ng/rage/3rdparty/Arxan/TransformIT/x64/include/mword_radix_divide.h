/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __MWORD_RADIX_DIVIDE_H__
#define __MWORD_RADIX_DIVIDE_H__

#include "wbrsa_config.h"

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size mword_radix_divide_x(const mword *a, _size aSize, const mword *b, _size bSize, mword *quotient, mword *remainder, _size *rSize);


#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size mword_radix_remainder_x(const mword *a, _size aSize, const mword *b, _size bSize, mword *remainder);


#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size mword_radix_quotient_x(const mword *a, _size aSize, const mword *b, _size bSize, mword *quotient);


#ifdef __INLINE_OPS_
#include "mword_radix_divide.c"
#endif


#endif

