/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

/*
 * TransformIT: DES/Triple/Encrypt Instance=i3DES1redPT
 */

#ifndef __TFIT_DES_i3DES1redPT_H__
#define __TFIT_DES_i3DES1redPT_H__

#include "TFIT.h"

#include "wbdes_api.h"
#include "TFIT_ddefs_i3DES1redPT.h"

#define TFIT_init_wbdes_i3DES1redPT(ctx, ecb_key, ecb_key_len)          \
    wbdes_init_ecb(ctx, WBDES_ENCRYPT, WBDES_OBFUSCATED, WBDES_CLASSICAL, &TFIT_des_ecb_i3DES1redPT, ecb_key, ecb_key_len)
#define TFIT_update_wbdes_i3DES1redPT wbdes_update
#define TFIT_update_slice_wbdes_i3DES1redPT wbdes_update_slice
#define TFIT_final_wbdes_i3DES1redPT wbdes_final

#ifdef __cplusplus
extern "C"
#endif
int TFIT_wbdes_encrypt_i3DES1redPT(const void *wbdes_key,
                                      const unsigned char *input, 
                                      const size_t input_len, 
                                      unsigned char *output);

#define TFIT_validate_wb_key_i3DES1redPT TFIT_validate_dkey_id_i3DES1redPT

#endif /* __TFIT_DES_i3DES1redPT_H__ */
