/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __EXTENDING_MULT_H__
#define __EXTENDING_MULT_H__

#include "wbrsa_config.h"

#define EXTENDING_MULT(a, b, r, c, temp)  \
  { \
    temp = ((mdword)a * b);  \
    r = (mword)temp; \
    c = (mword)(temp >> (sizeof(mword)*8)); \
  } 

#define EXTENDING_MULT_WITH_ACCUMULATE(a, b, r, c, temp)  \
  { \
    temp = ((mdword)a * b) + r;  \
    r = (mword)temp; \
    c = (mword)(temp >> (sizeof(mword)*8)); \
  } 

#define EXTENDING_MULT_WITH_CARRY(a, b, cIn, r, cOut, temp)  \
  { \
    temp = ((mdword)a * b) + cIn;  \
    r = (mword)temp; \
    cOut = (mword)(temp >> (sizeof(mword)*8)); \
  } 

#define EXTENDING_MULT_WITH_CARRY_AND_ACCUMULATE(a, b, cIn, r, cOut, temp)  \
  { \
    temp = ((mdword)a * b) + cIn + r;  \
    r = (mword)temp; \
    cOut = (mword)(temp >> (sizeof(mword)*8)); \
  } 

#endif

