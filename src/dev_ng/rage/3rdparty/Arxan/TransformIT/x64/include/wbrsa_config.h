/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __WBRSA_CONFIG_H__
#define __WBRSA_CONFIG_H__

#include "machine.h"
#include "application.h"

#if defined(_MSC_VER) && defined(_WIN32)
#include <malloc.h>
#ifndef alloca
#define alloca(a) _alloca(a)
#endif 

#else
#include <alloca.h>
#endif

#endif

