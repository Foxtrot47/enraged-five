/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

#ifndef __PLAT_GENERIC_32_H__
#define __PLAT_GENERIC_32_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <arxstdint.h>


#define extending_madd2_generic_32(a,b,c,d,res,cout) \
{ \
  uint_fast64_t _____intermediate = (uint_fast64_t)(a) \
    * (uint_fast64_t)(b) \
    + (uint_fast64_t)(c) \
    + (d); \
  res = (uint32_t)(_____intermediate); \
  cout = (uint32_t)(_____intermediate >> 32); \
}


#define extending_madc_generic_32(a,b,c,res,cout) \
{ \
  uint_fast64_t _____intermediate = (uint_fast64_t)(a) \
    * (uint_fast64_t)(b) \
    + (uint_fast64_t)(c); \
  res = (uint32_t)(_____intermediate); \
  cout = (uint32_t)(_____intermediate >> 32); \
}

#define extending_add_generic_32(a,b,res,cout) \
{ \
  uint_fast64_t _____intermediate = (uint_fast64_t)(a) \
    + (uint_fast64_t)(b); \
  res = (uint32_t)(_____intermediate); \
  cout = (uint32_t)(_____intermediate >> 32); \
}

#define extending_adc_generic_32(a,b,c,res,cout) \
{ \
  uint_fast64_t _____intermediate = (uint_fast64_t)(a) \
    + (uint_fast64_t)(b) \
    + (uint_fast64_t)(c); \
  res = (uint32_t)(_____intermediate); \
  cout = (uint32_t)(_____intermediate >> 32); \
}



#define borrowing_sub_generic_32(a,b,c,res,cout) \
{ \
  uint_fast64_t _____intermediate = (uint_fast64_t)(a) \
    - (uint_fast64_t)(b) \
    - (uint_fast64_t)(c); \
  res = (uint32_t)(_____intermediate); \
  cout = (~(uint32_t)(_____intermediate >> 32))+1; \
}


#ifdef __cplusplus
}
#endif

#endif

