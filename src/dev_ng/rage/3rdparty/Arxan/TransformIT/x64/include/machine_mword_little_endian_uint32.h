/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __MACHINE_MWORD_UINT32_H__
#define __MACHINE_MWORD_UINT32_H__

#include <string.h> 

#include "utils.h"

#ifndef __WBC_TYPES_H__
typedef uint32_t mword;
typedef uint64_t mdword;

#ifdef MSVC
typedef uint32_t uint32;
#else
typedef uint32_t uint32;
#endif 


typedef unsigned char uint8;

#endif  

#define MAX_MWORD ((mword)1 << (sizeof(mword)*8-1)) | (((mword)1 << (sizeof(mword)*8-1)) - 1);





#define DEFAULT_LCG_MODULUS 0xFFFFFFFB 
#define DEFAULT_LCG_MULTIPLIER 0x5EB0A82F
#define DEFAULT_LCG_SEED 0x406024

#define BIG_ENDIAN_BITS_TO_UINT32(a) \
( \
  ((a) >> 24) \
  | (((a) & 0x00ff0000) >> 8) \
  | (((a) & 0x0000ff00) << 8) \
  | (((a) & 0x000000ff) << 24)\
)

#define UINT32_TO_BIG_ENDIAN_BITS(a) \
( \
  ((a) >> 24) \
  | (((a) & 0x00ff0000) >> 8) \
  | (((a) & 0x0000ff00) << 8) \
  | (((a) & 0x000000ff) << 24)\
)

#define UINT32_REVERSE_BYTE_ORDER(a) \
( \
  ((a) >> 24) \
  | (((a) & 0x00ff0000) >> 8) \
  | (((a) & 0x0000ff00) << 8) \
  | (((a) & 0x000000ff) << 24)\
)

#define MWORD_REVERSE_BYTE_ORDER(a) \
( \
  ((a) >> 24) \
  | (((a) & 0x00ff0000) >> 8) \
  | (((a) & 0x0000ff00) << 8) \
  | (((a) & 0x000000ff) << 24)\
)

#define LITTLE_ENDIAN_BITS_TO_UINT32(a) (a)

#define UINT32_TO_LITTLE_ENDIAN_BITS(a) (a)

#define ROTATE_LEFT(a, b) ((((uint32)(a)) << (b)) | (((uint32)(a)) >> (sizeof(uint32)*8-(b))))

#define DEREFERENCE_BIG_ENDIAN_BYTES_TO_MWORD(a) MWORD_REVERSE_BYTE_ORDER(*(mword *)(a))

#define DEREFERENCE_LITTLE_ENDIAN_BYTES_TO_MWORD(a) (*(mword *)(a))

#define STORE_MWORD_TO_BIG_ENDIAN_BYTES(m, p) \
  { \
    mword flipped = MWORD_REVERSE_BYTE_ORDER((m)); \
    COPY_DST_SRC((p), &flipped, 1); \
  }

#define STORE_MWORD_TO_LITTLE_ENDIAN_BYTES(m, p) \
  { \
    mword ___m = (m); \
    COPY_DST_SRC((p), &___m, 1); \
  }




#include "extended_divide.h"
#include "extending_add.h"
#include "extending_mult.h"

#endif

