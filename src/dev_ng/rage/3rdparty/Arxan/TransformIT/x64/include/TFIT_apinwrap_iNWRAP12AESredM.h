/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

      

      

      


#ifndef __TFIT_APINWRAP_INWRAP12AESREDM_H__
#define __TFIT_APINWRAP_INWRAP12AESREDM_H__

#include "wbaes_api.h"

#ifdef __cplusplus
extern "C" {
#endif

int TFIT_opnwrap_iNWRAP12AESredM(wbaes_inner_func *wbop, const unsigned char *keys[2], const unsigned char src[40], size_t octet_len, unsigned char dest[32]);
extern const unsigned char TFIT_wrap_iNWRAP12AESredM[];
extern const wbaes_wrapper_t TFIT_apinwrap_iNWRAP12AESredM;

#ifdef __cplusplus
}
#endif

#endif 


