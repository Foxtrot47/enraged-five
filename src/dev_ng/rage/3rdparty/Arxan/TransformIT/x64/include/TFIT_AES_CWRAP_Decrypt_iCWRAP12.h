/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES/CWRAP Decrypt Instance=iCWRAP12
 */

#ifndef __TFIT_AES_CWRAP_iCWRAP12_H__
#define __TFIT_AES_CWRAP_iCWRAP12_H__

#include "TFIT.h"

#include "TFIT_defs_iCWRAP12.h"
#define TFIT_update_wbaes_iCWRAP12 wbaes_update

#define TFIT_update_slice_wbaes_iCWRAP12 wbaes_update_slice

#define TFIT_final_wbaes_iCWRAP12 wbaes_final

#define TFIT_validate_wb_key_iCWRAP12 TFIT_validate_key_id_iCWRAP12

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iCWRAP12_<colour> */
#define TFIT_prepare_dynamic_key_iCWRAP12(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)


#include "wrapper_modes.h"
#include "wrapper_modes.h"

#define TFIT_init_wbaes_cwrap_iCWRAP12(ctx, wbaes_key) \
    wbaes_init_cwrap(ctx, WBAES_DECRYPT, WBAES_CLASSICAL, WBAES_CLASSICAL, &TFIT_aes_ecb_iCWRAP12, wbaes_key, &wbw_apinwrap_classical_dec, NULL, &wbw_apicwrap_classical_dec, NULL)
#define TFIT_wbaes_cwrap_iCWRAP12(wbaes_key, input, input_len, output) \
    aes_unwrap((fn_wbaes_op_t *)&TFIT_op_iCWRAP12, wbaes_key, input, input_len, output, 1)


#endif /* __TFIT_AES_CWRAP_Decrypt_iCWRAP12_H__ */
