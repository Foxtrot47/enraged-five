/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __PKCS1_V15_INTERNAL_ENCODE_GENERIC_H__
#define __PKCS1_V15_INTERNAL_ENCODE_GENERIC_H__

#include "wbrsa_config.h"
#include "wbc_rsa_errors.h"

#define BLOCK_TYPE_ZERO_PADDED 0
#define BLOCK_TYPE_FF_PADDED 1
#define BLOCK_TYPE_RAND_PADDED 2

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

int pkcs1_v15_internal_encode_generic(int blockType, uint8 *data, _size dataOctets, _size modOctets, mword *result);

#ifdef __INLINE_OPS_
#include "pkcs1_v15_internal_encode_generic.c"
#endif


#endif

