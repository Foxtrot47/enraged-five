/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __MILLER_RABIN_PRIMALITY_TEST_H__
#define __MILLER_RABIN_PRIMALITY_TEST_H__



#define COMPOSITE 0
#define PROBABLY_PRIME 1
#define ERR_INTERNAL_INVALID_PARAMETER 2


#include "wbrsa_config.h"

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

int miller_rabin_primality_test(const mword *p, _size pSize, _size k);

#ifdef __INLINE_OPS_
#include "miller_rabin_primality_test.c"
#endif



#endif

