/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

      

      

      


#ifndef __TFIT_WRAPDEF_IAES14REDPT_H__
#define __TFIT_WRAPDEF_IAES14REDPT_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __GNUC__
#ifndef __attribute__
#define __attribute__( A )
#endif 

#endif 


#include "wbaes_api.h"

;
#ifndef DIRECTION_ENUM_DECLARED
#define DIRECTION_ENUM_DECLARED
typedef enum { DIR_ENC=0, DIR_DEC=1 } direction_t;
#endif 



;
#ifdef _MSC_VER
#pragma pack(push,1)
#endif 

struct _TFIT_wrap_iAES14redPT_t
{
  unsigned char data[2774];
} __attribute__((__packed__));
#ifdef _MSC_VER
#pragma pack(pop)
#endif 


typedef struct _TFIT_wrap_iAES14redPT_t TFIT_wrap_iAES14redPT_t;



int TFIT_opcbc_validate_wrap_id_iAES14redPT(const void * key);



const unsigned char* TFIT_opcbc_get_oxdid_iAES14redPT(const void *_key, wbaes_oxdid_t which);

#ifdef __cplusplus
}
#endif

#endif 


