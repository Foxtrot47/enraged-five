/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __FIVE_BIT_UINT8_KEY_PROCESSOR_H__
#define __FIVE_BIT_UINT8_KEY_PROCESSOR_H__

#include "wbrsa_config.h"

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

void five_bit_uint8_array_key_processor(const mword *raw, _size rSize, uint8 * out, _size *oSize);

#ifdef __INLINE_OPS_
#include "five_bit_uint8_array_key_processor.c"
#endif

#endif

