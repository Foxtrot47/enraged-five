/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __TFIT_NBV_IECEGFASTP256_H__
#define __TFIT_NBV_IECEGFASTP256_H__
#define TFIT_nbv_iECEGfastP256_8_0 ((uint8_t)0x35)
#define TFIT_nbv_iECEGfastP256_8_1 ((uint8_t)0x36)
#define TFIT_nbv_iECEGfastP256_8_2 ((uint8_t)0x1f)
#define TFIT_nbv_iECEGfastP256_8_3 ((uint8_t)0x37)
#define TFIT_nbv_iECEGfastP256_8_4 ((uint8_t)0xcb)
#define TFIT_nbv_iECEGfastP256_8_5 ((uint8_t)0xc7)
#define TFIT_nbv_iECEGfastP256_8_6 ((uint8_t)0x86)
#define TFIT_nbv_iECEGfastP256_8_7 ((uint8_t)0xcf)
#define TFIT_nbv_iECEGfastP256_8_8 ((uint8_t)0x00)
#define TFIT_nbv_iECEGfastP256_8_9 ((uint8_t)0x52)
#define TFIT_nbv_iECEGfastP256_8_10 ((uint8_t)0x98)
#define TFIT_nbv_iECEGfastP256_8_11 ((uint8_t)0x0a)
#define TFIT_nbv_iECEGfastP256_8_12 ((uint8_t)0xbf)
#define TFIT_nbv_iECEGfastP256_8_13 ((uint8_t)0xdf)
#define TFIT_nbv_iECEGfastP256_8_14 ((uint8_t)0xea)
#define TFIT_nbv_iECEGfastP256_8_15 ((uint8_t)0x4c)
#define TFIT_nbv_iECEGfastP256_8_16 ((uint8_t)0x4c)
#define TFIT_nbv_iECEGfastP256_8_17 ((uint8_t)0x85)
#define TFIT_nbv_iECEGfastP256_8_18 ((uint8_t)0xad)
#define TFIT_nbv_iECEGfastP256_8_19 ((uint8_t)0x72)
#define TFIT_nbv_iECEGfastP256_8_20 ((uint8_t)0xcd)
#define TFIT_nbv_iECEGfastP256_8_21 ((uint8_t)0xe7)
#define TFIT_nbv_iECEGfastP256_8_22 ((uint8_t)0x6f)
#define TFIT_nbv_iECEGfastP256_8_23 ((uint8_t)0xb8)
#define TFIT_nbv_iECEGfastP256_8_24 ((uint8_t)0x27)
#define TFIT_nbv_iECEGfastP256_8_25 ((uint8_t)0x23)
#define TFIT_nbv_iECEGfastP256_8_26 ((uint8_t)0x60)
#define TFIT_nbv_iECEGfastP256_8_27 ((uint8_t)0xfd)
#define TFIT_nbv_iECEGfastP256_8_28 ((uint8_t)0xfd)
#define TFIT_nbv_iECEGfastP256_8_29 ((uint8_t)0xab)
#define TFIT_nbv_iECEGfastP256_8_30 ((uint8_t)0x5e)
#define TFIT_nbv_iECEGfastP256_8_31 ((uint8_t)0xbc)
#define TFIT_nbv_iECEGfastP256_16_0 ((uint16_t)0x3536)
#define TFIT_nbv_iECEGfastP256_16_1 ((uint16_t)0x1f37)
#define TFIT_nbv_iECEGfastP256_16_2 ((uint16_t)0xcbc7)
#define TFIT_nbv_iECEGfastP256_16_3 ((uint16_t)0x86cf)
#define TFIT_nbv_iECEGfastP256_16_4 ((uint16_t)0x0052)
#define TFIT_nbv_iECEGfastP256_16_5 ((uint16_t)0x980a)
#define TFIT_nbv_iECEGfastP256_16_6 ((uint16_t)0xbfdf)
#define TFIT_nbv_iECEGfastP256_16_7 ((uint16_t)0xea4c)
#define TFIT_nbv_iECEGfastP256_16_8 ((uint16_t)0x4c85)
#define TFIT_nbv_iECEGfastP256_16_9 ((uint16_t)0xad72)
#define TFIT_nbv_iECEGfastP256_16_10 ((uint16_t)0xcde7)
#define TFIT_nbv_iECEGfastP256_16_11 ((uint16_t)0x6fb8)
#define TFIT_nbv_iECEGfastP256_16_12 ((uint16_t)0x2723)
#define TFIT_nbv_iECEGfastP256_16_13 ((uint16_t)0x60fd)
#define TFIT_nbv_iECEGfastP256_16_14 ((uint16_t)0xfdab)
#define TFIT_nbv_iECEGfastP256_16_15 ((uint16_t)0x5ebc)
#define TFIT_nbv_iECEGfastP256_32_0 ((uint32_t)0x35361f37)
#define TFIT_nbv_iECEGfastP256_32_1 ((uint32_t)0xcbc786cf)
#define TFIT_nbv_iECEGfastP256_32_2 ((uint32_t)0x0052980a)
#define TFIT_nbv_iECEGfastP256_32_3 ((uint32_t)0xbfdfea4c)
#define TFIT_nbv_iECEGfastP256_32_4 ((uint32_t)0x4c85ad72)
#define TFIT_nbv_iECEGfastP256_32_5 ((uint32_t)0xcde76fb8)
#define TFIT_nbv_iECEGfastP256_32_6 ((uint32_t)0x272360fd)
#define TFIT_nbv_iECEGfastP256_32_7 ((uint32_t)0xfdab5ebc)
#define TFIT_nbv_iECEGfastP256_64_0 ((uint64_t)0x35361f37cbc786cfULL)
#define TFIT_nbv_iECEGfastP256_64_1 ((uint64_t)0x0052980abfdfea4cULL)
#define TFIT_nbv_iECEGfastP256_64_2 ((uint64_t)0x4c85ad72cde76fb8ULL)
#define TFIT_nbv_iECEGfastP256_64_3 ((uint64_t)0x272360fdfdab5ebcULL)
#endif
