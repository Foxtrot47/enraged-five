/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef _TFIT_DEFS_INKDF128GREENOK_H_
#define _TFIT_DEFS_INKDF128GREENOK_H_

#include <arxstdint.h>
#include "wbaes_api.h"
#ifdef __cplusplus
extern "C" {
#endif
  typedef union {
  struct {
    uint32_t b0:8;
    uint32_t b1:8;
    uint32_t b2:8;
    uint32_t b3:8;
  }bytes;
  uint32_t word:32;
  } TFIT_state_word_b_iNKDF128greenOK;

  typedef union {
  uint32_t word:32;
  struct {
    uint32_t b0:8;
    uint32_t b1:8;
    uint32_t b2:8;
    uint32_t b3:8;
  }bytes;
  } TFIT_state_word_w_iNKDF128greenOK;

  typedef struct _TFIT_key_iNKDF128greenOK {
   TFIT_state_word_b_iNKDF128greenOK data[64]; 
  } TFIT_key_iNKDF128greenOK_t
#if !defined(TFIT_NODECORATE_AES_KEYS) && defined(_MSVC_VER) 
   #define TFIT_NODECORATE_AES_KEYS
#endif
#ifdef TFIT_NODECORATE_AES_KEYS
   ;
#else 
   __attribute__ ((__aligned__(sizeof(uint32_t))));
#endif

  #ifdef DEBUG_WB_KEYSIZES
  #define MUG_DEFS_KEYSIZE (56)
  #endif

  int TFIT_op_iNKDF128greenOK(const void* wbk, const unsigned char src[16], unsigned char dest[16]);
  int TFIT_alt_op_iNKDF128greenOK(const void* wbk, const unsigned char src[16], unsigned char dest[16], int kOpt);

  int TFIT_validate_key_id_iNKDF128greenOK(const void * mkb);

  const uint8_t* TFIT_get_oxd_id_in_iNKDF128greenOK();
  const uint8_t* TFIT_get_oxd_id_out_iNKDF128greenOK();

  extern const wbaes_ecb_cipher_t TFIT_aes_ecb_iNKDF128greenOK;
#ifdef __cplusplus
}
#endif
#endif 
