/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBAES_API_H__
#define __WBAES_API_H__

#include <stdlib.h>
#include <arxstdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "slice.h"
#include "wbaes_error_codes.h"

#define WBAES_BLOCKSIZE 16


#ifndef GENERATE_ENUM
#define GENERATE_ENUM(ENUM) ENUM,
#endif
#ifndef GENERATE_STRING
#define GENERATE_STRING(STRING) #STRING,
#endif

#define WBAES_FOREACH_DIRECTION(DIRECTION) \
            DIRECTION(WBAES_ENCRYPT) \
            DIRECTION(WBAES_DECRYPT) \


#define WBAES_FOREACH_FORM(FORM) \
            FORM(WBAES_CLASSICAL) \
            FORM(WBAES_OBFUSCATED) \

#define WBAES_FOREACH_OXDID(OXDID) \
            OXDID(WBAES_OXDIN) \
            OXDID(WBAES_OXDOUT) \
            OXDID(WBAES_OXDIN_INNER) \
            OXDID(WBAES_OXDOUT_INNER) \
            OXDID(WBAES_OXDIN_IV) \
            OXDID(WBAES_OXDIN_INNER2) \
            OXDID(WBAES_OXDOUT_INNER2) \


#define WBAES_FOREACH_WRAPPER(WRAPPER) \
            WRAPPER(WBAES_ECB) \
            WRAPPER(WBAES_CBC) \
            WRAPPER(WBAES_CTR) \
            WRAPPER(WBAES_GCM) \
            WRAPPER(WBAES_CMAC) \
            WRAPPER(WBAES_NIST) \
            WRAPPER(WBAES_CMLA) \
            WRAPPER(WBAES_CCM) \

typedef enum { WBAES_FOREACH_DIRECTION(GENERATE_ENUM) } wbaes_direction_t;
extern const char* wbaes_direction_string[];

typedef enum { WBAES_FOREACH_FORM(GENERATE_ENUM) } wbaes_obf_t;
extern const char* wbaes_obf_string[];

typedef enum { WBAES_FOREACH_OXDID(GENERATE_ENUM) } wbaes_oxdid_t;

typedef enum { WBAES_FOREACH_WRAPPER(GENERATE_ENUM) } wbaes_wrapper_mode_t;
extern const char* wbaes_wrapper_mode_string[];

                
typedef int (wbaes_inner_func) ();
typedef int (wbaes_inc_func) (const unsigned char src[16], unsigned char dest[16]);





typedef int (wbaes_ecb_op_func) (const void* wbk,
                                 const unsigned char src[16], 
                                 unsigned char dest[16]);
typedef int (wbaes_ecb_altop_func) (const void* wbk,
                                    const unsigned char src[16],
                                    unsigned char dest[16],
                                    int kOpt);


typedef int (wbaes_cbc_op_func)  (wbaes_inner_func *ecb_op,
                                  const unsigned char* keys[2], 
                                  const unsigned char *src,
                                  size_t octet_len,
                                  unsigned char iv[16],
                                  unsigned char input_opt,
                                  unsigned char *dest);


typedef int (wbaes_ctr_op_func) (wbaes_inner_func *ecb_op, 
                                 const unsigned char* keys[2],
                                 wbaes_inc_func *const inc_fn, 
                                 const unsigned char src[16],
                                 size_t octet_len, 
                                 unsigned char iv[16],
                                 unsigned char dest[16]);


typedef int (wbaes_gcm_op_func) (wbaes_inner_func *ecb_op, 
                                 const unsigned char* keys[2], 
                                 const unsigned char src[16],
                                 size_t octet_len, 
                                 unsigned char iv[16],
                                 unsigned char H[16],
                                 unsigned char tag[16],
                                 unsigned char dest[16]);

typedef int (wbaes_gcm_op_tag_func) (wbaes_inner_func *ecb_op,
                                     const unsigned char* keys[2],
                                     const unsigned char AC_len[16], 
                                     size_t tag_len, 
                                     unsigned char tag[16],
                                     const unsigned char J[16],
                                     unsigned char *dest);

typedef struct _wbaes_ccm_state {
	uint64_t  data[32];
} wbaes_ccm_state_t;

typedef struct _wbaes_ccm_ctx {
	
	uint64_t  assoc_data_len;
	uint64_t  msg_len;
	uint8_t   digest_len;
	uint8_t   nonce_len;
	uint8_t   nonce[13];
	
	wbaes_ccm_state_t ccm_state;
} wbaes_ccm_ctx;


typedef int (wbaes_ccm_op_func) (wbaes_inner_func *inner_op[2], 
                                 const unsigned char *keys[3],
                                 const unsigned char *src,
                                 size_t input_octet_len,
                                 wbaes_ccm_ctx *ctx,
                                 unsigned char input_opt, 
                                 size_t *bytes_written,
                                 size_t output_octet_len,
                                 unsigned char *dest);


typedef int (wbaes_cmac_op_func)  (wbaes_inner_func *ecb_op,
                                   const unsigned char* keys[2], 
                                   const unsigned char src[16],
                                   size_t input_octet_len,
                                   unsigned char iv[16],
                                   
                                   unsigned char input_opt,
                                   size_t output_octet_len,
                                   unsigned char dest[16]);


typedef int (wbaes_nwrap_op_func)  (wbaes_inner_func *ecb_op,
                                    const unsigned char* keys[2],
                                    const unsigned char* src,
                                    size_t octet_len,
                                    unsigned char* dest);

typedef int (wbaes_cwrap_op_func)  (const unsigned char* wrap_key,
                                    const unsigned char src[32],
                                    size_t octet_len,
                                    unsigned char dest[32]);

typedef struct _wbaes_ecb_cipher
{
    int (*validate) (const void * mkb);
    wbaes_ecb_op_func *op; 
    const uint8_t* (*get_oxd_in)();
    const uint8_t* (*get_oxd_out)();
    wbaes_obf_t inputForm;
    wbaes_obf_t outputForm;
    wbaes_direction_t direction;
    wbaes_ecb_altop_func *alt_op; 
    unsigned int key_size;
} wbaes_ecb_cipher_t;

typedef struct _wbaes_inner_info
{
    wbaes_obf_t inputForm;
    wbaes_obf_t outputForm;
    wbaes_direction_t direction;
} wbaes_inner_info_t;

typedef struct _wbaes_wrapper
{
    int (*validate) (const void * mkb);
    const unsigned char* (*get_oxdid) (const void *mkb, wbaes_oxdid_t which);
    int (*op) ();
    int (*op_tag)();
    wbaes_wrapper_mode_t mode;
    wbaes_obf_t inputForm;
    wbaes_obf_t outputForm;
    wbaes_direction_t direction;
    int expectedInnerCount; 
    wbaes_inner_info_t expectedInner[1]; 
} wbaes_wrapper_t;

typedef struct _wbaes_wrapper_with_double_inner
{   
    int (*validate) (const void * mkb);
    const unsigned char* (*get_oxdid) (const void *mkb, wbaes_oxdid_t which);
    int (*op) ();
    int (*op_tag)();
    wbaes_wrapper_mode_t mode;
    wbaes_obf_t inputForm;
    wbaes_obf_t outputForm;
    wbaes_direction_t direction;
    int expectedInnerCount; 
    wbaes_inner_info_t expectedInner[2];
} wbaes_wrapper_with_double_inner_t;

typedef struct _wbaes_ctx
{
	wbaes_wrapper_mode_t mode;
	wbaes_direction_t direction;
	unsigned char* buffer;
	unsigned int buffer_index;
	unsigned int finalized : 1;
	unsigned int corrupted : 1; 
	                            

	wbaes_inner_func *op_func[3];
	const void* keys[3];

	
	wbaes_inc_func *increment;      

	unsigned char iv[16];           

	unsigned char H[16];            
	unsigned char J[16];
	unsigned char tag[16];
	uint64_t aad_bit_len;
	unsigned int final_aad : 1;
	uint64_t msg_bit_len;

	wbaes_ccm_ctx *ccm_ctx;         

	unsigned int msg_size;          
	                                
	                                
	                                


	
	const uint8_t *oxd_iv_id; 
	const uint8_t *oxd_in_id;
	const uint8_t *oxd_out_id;
	char cbc_input_opt;
} wbaes_ctx_t;






int wbaes_init_ecb(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb, const void * const ecb_key);
int wbaes_init_cbc(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb, const void *const ecb_key, 
             const wbaes_wrapper_t * const cbc, const void * const cbc_data, 
             const uint8_t iv[16]);
int wbaes_init_cbc_classical_first_iv(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb, const void *const ecb_key, 
             const wbaes_wrapper_t * const cbc, const void * const cbc_data, 
             const uint8_t iv[16]);
int wbaes_init_ctr(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t *const ecb, const void * const ecb_key, 
             const wbaes_wrapper_t * const ctr, const void * const ctr_key, 
             const uint8_t iv[16]);
int wbaes_init_ctr_custom(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb, const void * const ecb_key, 
             const wbaes_wrapper_t * const ctr, const void * ctr_data, 
             const uint8_t iv[16],
             wbaes_inc_func *increment);
int wbaes_init_gcm(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb, const void * const ecb_key, 
             const wbaes_wrapper_t * const gcm, const void * gcm_data, 
             const uint8_t iv[12]);
int wbaes_init_ccm(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb1, const void * const ecb1_key, 
             const wbaes_ecb_cipher_t * const ecb2, const void * const ecb2_key, 
             const wbaes_wrapper_t * const ccm, const void * ccm_data, 
             uint64_t assoc_data_len, uint64_t msg_len,
             unsigned int digest_len,
             unsigned int nonce_len, const uint8_t *nonce);
int wbaes_init_cmac(wbaes_ctx_t * const ctx,
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb, const void * const ecb_key, 
             const wbaes_wrapper_t * const cmac, const void * const cmac_data,
             unsigned int digest_len);
int wbaes_init_nwrap(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb, const void * const ecb_key, 
             const wbaes_wrapper_t * const nwrap, const void * const nwrap_data,
             unsigned int msg_len);
int wbaes_init_cwrap(wbaes_ctx_t * const ctx, wbaes_direction_t direction, 
             wbaes_obf_t obf_in, wbaes_obf_t obf_out,
             const wbaes_ecb_cipher_t * const ecb, const void * const ecb_key,
             const wbaes_wrapper_t * const nwrap, const void * const nwrap_data,
             const wbaes_wrapper_t * const cwrap, const void * const cwrap_data);







int wbaes_update(wbaes_ctx_t * const ctx, 
             const uint8_t * const input, unsigned int input_len,
             uint8_t * const output, unsigned int output_len, 
             unsigned int * const bytes_written);

int wbaes_update_slice(wbaes_ctx_t * const ctx,
    const uint8_t* const input, 
    unsigned int full_input_len,                  
                                                  
    unsigned int input_len,                       
    unsigned int extract_start,                   
                                                  
    unsigned int extract_len,                     
    wbslice_pad_side_t pad_side,                  
                                                  
                                                  
    const wbslice_table_t* const slice_table,     
                                                  
    const wbslice_byte_order_t* const input_order,
                                                  
                                                  
    uint8_t* const output,                        
    unsigned int output_len,                      
    unsigned int* const bytes_written);           
                                                  







int wbaes_final(wbaes_ctx_t * const ctx, 
             uint8_t * const output, unsigned int output_len,
             unsigned int * const bytes_written);




int wbaes_gcm_get_tag(wbaes_ctx_t * const ctx,
      unsigned char * const output, unsigned int tag_len, 
        unsigned int * const bytes_written);


int wbaes_gcm_verify_tag(wbaes_ctx_t * const ctx, const unsigned char * const input, unsigned int tag_len);

typedef enum _wbaes_oxdsel_t
{
  WBAES_IV_OXD_ID,
  WBAES_INPUT_OXD_ID,
  WBAES_OUTPUT_OXD_ID
} wbaes_oxdsel_t;



const uint8_t * wbaes_get_oxdid(const wbaes_ctx_t * const ctx, 
             wbaes_oxdsel_t which);

#ifdef __cplusplus
}
#endif

#endif 

