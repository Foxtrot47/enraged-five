/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_DH_API_FST_H__
    #define __WBECC_DH_API_FST_H__

    #include <arxstdint.h>
    #include "wbecc.h"
    #include "wbecc_ecc_alu.h"
    #include "wbecc_encoding_types.h"
    #include "wbecc_common_api.h"
    #include "wb_ecc_static.h"
    
    #ifdef __cplusplus
        extern "C"
        {
    #endif

    #define WBECC_FST_MAX_BUFFER_SIZE \
      ((MAX_FIELD_ORDER_BYTES > MAX_CURVE_ORDER_BYTES) \
      ? MAX_FIELD_ORDER_BYTES \
      : MAX_CURVE_ORDER_BYTES)

    

    typedef int (*wbecc_dh_get_ephemeral_data_fst) (const unsigned int, uint8_t * const, const void * const);

    typedef struct _wbecc_dh_ctx_fst {
        const wbecc_key_pair_t *static_key;
        unsigned char have_static;
        wbecc_key_pair_t ephemeral_key;
        unsigned char have_ephemeral;
        wbecc_dh_get_ephemeral_data_fst get_ephemeral;

        const wbecc_fst_funcs_t *funcs;
        const wbecc_domain_host_t *domain;
        
        wbecc_fast_table_curve_t fast_table_curve;

        uint8_t *constants;
        uint8_t *pmscratch;

        wbecc_pm_table_t pmtable;

        uint8_t oxdout_uuid[16];
    } wbecc_dh_ctx_fst_t;

    

    int wbecc_dh_get_static_public_key_fst(
        const wbecc_dh_ctx_fst_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    

    int wbecc_dh_montgomery_get_static_public_key_fst(
        const wbecc_dh_ctx_fst_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written,
        const uint8_t * A, unsigned int Alen, const uint8_t * B, unsigned int Blen
    );
    
    

    int wbecc_dh_get_ephemeral_public_key_fst(
        const wbecc_dh_ctx_fst_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    

    int wbecc_dh_montgomery_get_ephemeral_public_key_fst(
        const wbecc_dh_ctx_fst_t * const ctx,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written,
        const uint8_t * A, unsigned int Alen, const uint8_t * B, unsigned int Blen
    );
    
    

    int wbecc_dh_generate_key_fst(
        wbecc_dh_ctx_fst_t * const ctx
    );

    


    int wbecc_dh_compute_secret_fst(
        wbecc_dh_ctx_fst_t*  const ctx,
        unsigned int mode,
        const uint8_t* const static_public_key,
        const uint8_t* const ephemeral_public_key,
        unsigned int public_key_len,
        uint8_t * const output,
        unsigned int output_len,
        unsigned int * const bytes_written
    );
    
    const uint8_t * wbecc_dh_get_oxdid_fst(
        const wbecc_dh_ctx_fst_t * const ctx,
        wbecc_oxdsel_t which
    );

    #ifdef __cplusplus
        }
    #endif
#endif
