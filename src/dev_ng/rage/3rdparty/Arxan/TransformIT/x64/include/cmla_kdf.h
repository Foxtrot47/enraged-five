/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */



#include <arxstdint.h>
#include "wbbbi_api.h"
#include "wbsha_encoding_types.h"
#include "slice.h"
#ifndef __CMLA_KDF_H__
#define __CMLA_KDF_H__ 1

#ifdef __cplusplus
extern "C" {
#endif

typedef enum _wbcmla_status_t
{
    WBCMLA_OK = 0,
    WBCMLA_NULL_ARG = 1,
    WBCMLA_INVALID_INPUT_LEN = 2,
    WBCMLA_INVALID_OUTPUT_LEN = 3,
    WBCMLA_SLICE_UNKNOWN_ERROR = 4,
    WBCMLA_SLICE_NULL_ARG = 5,
    WBCMLA_SLICE_FULL_INPUT_LEN_NOT_WORD_SIZE_MULTIPLE = 6,
    WBCMLA_SLICE_INTERNAL_ERROR = 7,
    WBCMLA_SLICE_UNSUPPORTED_WORD_SIZE = 8,
    WBCMLA_SLICE_INVALID_BYTE_ORDER_IN_WORD = 9,
    WBCMLA_SHA_UNKNOWN_ERROR = 10,
    WBCMLA_SHA_OUTPUT_ERROR = 11,
    WBCMLA_BBI_MULTIPLY_ERROR = 12,
    WBCMLA_BBI_ADD_ERROR = 13,
    WBCMLA_BBI_INIT_ERROR = 14,
    WBCMLA_BBI_F_ERROR = 15,
    WBCMLA_BBI_F2_ERROR = 16,
    WBCMLA_BBI_REDUCE_ERROR = 17,
} wbcmla_status_t;


int wbcmla_kdf(const unsigned char* const input, 
               unsigned int input_len, 
               wbsha_enc_cfg_t* const wbsha_cfg,
               const wbslice_table_t* const slice_table_in_to_out,
               const wbslice_table_t* const slice_table_out_to_out,
               const boxy_context_t* const boxy_ctx,
               unsigned char* const output,
               unsigned int output_len,
               unsigned int * const bytes_written);

typedef enum _wbcmla_oxdsel_t
{
  WBCMLA_INPUT_OXD_ID,
  WBCMLA_OUTPUT_OXD_ID
} wbcmla_oxdsel_t;



const uint8_t * wbcmla_kdf_get_oxdid(wbsha_enc_cfg_t* const wbsha_cfg,
                                     const boxy_context_t* const boxy_ctx,
                                     wbcmla_oxdsel_t which);

#ifdef __cplusplus
}
#endif

#endif
