/*
* Copyright (C) Arxan Technologies Inc. 2013.
* All rights Reserved.
*
*
* Portions of the information disclosed herein are protected by
* U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
* U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
* U.S. Patent 8,510,571 and Patents Pending.
*
*/

/*
 * TransformIT: DES/Single/Encrypt Instance=iDES1
 */

#ifndef __TFIT_DES_iDES1_H__
#define __TFIT_DES_iDES1_H__

#include "TFIT.h"

#include "wbdes_api.h"
#include "TFIT_ddefs_iDES1.h"

#define TFIT_init_wbdes_iDES1(ctx, ecb_key, ecb_key_len)          \
    wbdes_init_ecb(ctx, WBDES_ENCRYPT, WBDES_CLASSICAL, WBDES_CLASSICAL, &TFIT_des_ecb_iDES1, ecb_key, ecb_key_len)
#define TFIT_update_wbdes_iDES1 wbdes_update
#define TFIT_update_slice_wbdes_iDES1 wbdes_update_slice
#define TFIT_final_wbdes_iDES1 wbdes_final

#ifdef __cplusplus
extern "C"
#endif
int TFIT_wbdes_encrypt_iDES1(const void *wbdes_key,
                                      const unsigned char *input, 
                                      const size_t input_len, 
                                      unsigned char *output);

#define TFIT_validate_wb_key_iDES1 TFIT_validate_dkey_id_iDES1

#endif /* __TFIT_DES_iDES1_H__ */
