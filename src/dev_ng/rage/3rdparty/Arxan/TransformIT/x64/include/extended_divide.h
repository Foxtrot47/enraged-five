/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */






#ifndef __EXTENDED_DIVIDE_H__
#define __EXTENDED_DIVIDE_H__

#include "wbrsa_config.h"

#define REDUCING_DIVIDE(aHigh, aLow, b, qLow, r) \
  { \
    mdword temp; \
	mdword quot; \
    if((aHigh) == 0){ \
      (qLow) = (aLow) / (b); \
      (r) = (aLow) % (b); \
    } \
    else{ \
      temp = ((mdword)(aHigh) << (sizeof(mword)*8)) \
                  | (aLow); \
      (r) = temp % (b); \
      quot = temp / (b); \
      (qLow) = (mword)quot; \
    } \
  } 

#define REDUCING_DIVIDE_NO_REMAINDER(aHigh, aLow, b, qLow) \
  { \
    mdword temp; \
	mdword quot; \
    if((aHigh) == 0){ \
      (qLow) = (aLow) / (b); \
    } \
    else{ \
      temp = ((mdword)aHigh << (sizeof(mword)*8)) \
                  | aLow; \
      quot = temp / (b); \
      (qLow) = (mword)quot; \
    } \
  } 

#endif

