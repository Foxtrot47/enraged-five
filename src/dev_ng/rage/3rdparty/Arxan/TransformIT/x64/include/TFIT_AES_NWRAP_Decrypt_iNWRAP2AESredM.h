/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

/*
 * TransformIT: AES/NWRAP Decrypt Instance=iNWRAP2AESredM
 */

#ifndef __TFIT_AES_NWRAP_iNWRAP2AESredM_H__
#define __TFIT_AES_NWRAP_iNWRAP2AESredM_H__

#include "TFIT.h"

#include "TFIT_defs_iNWRAP2AESredM.h"
#define TFIT_update_wbaes_iNWRAP2AESredM wbaes_update

#define TFIT_update_slice_wbaes_iNWRAP2AESredM wbaes_update_slice

#define TFIT_final_wbaes_iNWRAP2AESredM wbaes_final

#define TFIT_validate_wb_key_iNWRAP2AESredM TFIT_validate_key_id_iNWRAP2AESredM

#include "wbaes_dyninit.h"
/* Deprecated API: Please use TFIT_prepare_dynamic_key_iNWRAP2AESredM_<colour> */
#define TFIT_prepare_dynamic_key_iNWRAP2AESredM(key, key_len, dyninit_data, wbkey, wbkey_len, out_len) \
    wbaes_dyninit_prepare_key(key, key_len, dyninit_data, wbkey, wbkey_len, NULL, out_len)


#include "wrapper_modes.h"
#include "TFIT_apinwrap_iNWRAP2AESredM.h"

#define TFIT_init_wbaes_nwrap_iNWRAP2AESredM(ctx, wbaes_key, digest_len)    \
    wbaes_init_nwrap(ctx, WBAES_DECRYPT, WBAES_CLASSICAL, WBAES_OBFUSCATED, &TFIT_aes_ecb_iNWRAP2AESredM, wbaes_key, &TFIT_apinwrap_iNWRAP2AESredM, &TFIT_wrap_iNWRAP2AESredM, digest_len)

#ifdef __cplusplus
extern "C"
#endif
int TFIT_wbaes_nwrap_iNWRAP2AESredM(const void *wbaes_key, 
                                const uint8_t * const input, 
                                unsigned int input_len,
                                uint8_t * const output);


#endif /* __TFIT_AES_NWRAP_Decrypt_iNWRAP2AESredM_H__ */
