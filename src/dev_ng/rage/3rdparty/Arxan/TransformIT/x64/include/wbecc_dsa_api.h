/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */


#ifndef __WBECC_DSA_API_H__
    #define __WBECC_DSA_API_H__

    #include <arxstdint.h>
    #include "wbecc.h"
    #include "wbecc_ecc_alu.h"
    #include "wbecc_dsa_api_tbl.h"
    #include "wbecc_dsa_api_fst.h"
    #include "wbecc_encoding_types.h"
    #include "wbecc_common_api.h"
    #include "wbecc_sha.h"

    #ifdef __cplusplus
        extern "C"
        {
    #endif

    #define MAX_DSA_PKEY 132
    
    
    typedef enum _wbecc_dsa_status_t {
        

        

        WBECC_DSA_OK = 0,
        WBECC_DSA_GET_NONCE_FAILURE = 1,           

        WBECC_DSA_R_OUTPUT_BUFFER_TOO_SMALL = 2,   

        WBECC_DSA_S_OUTPUT_BUFFER_TOO_SMALL = 3,   

        WBECC_DSA_KEY_INSTANCE_ID_MISMATCH = 4,    

        WBECC_DSA_DOMAIN_INSTANCE_ID_MISMATCH = 5, 

        WBECC_DSA_MALLOC_FAILED = 6,               

        WBECC_DSA_SHA_FINAL_ERROR = 7,             

        WBECC_DSA_AFFINIFY_FAILURE = 8,            

        WBECC_DSA_REDUCE_FAILURE = 9,              

        WBECC_DSA_INVERT_FAILURE = 10,              

        WBECC_DSA_MULT_FAILURE = 11,                

        WBECC_DSA_ADD_FAILURE = 12,                 

        

        WBECC_DSA_WRONG_KEY_TYPE = 13,
        WBECC_DSA_NULL_PARAM = 14,
        WBECC_DSA_OBFUSCATED_OXDOUT_INVALID = 15,
        WBECC_DSA_CLASSICAL_OXDOUT_INVALID = 16,
        WBECC_DSA_FST_PREPARE_CONSTANTS_ERROR = 17,
        WBECC_DSA_MONTBI_REDUCE_ERROR = 18,
        WBECC_DSA_FAILED_TO_ENCODE_SHA_OUTPUT = 19,
        WBECC_DSA_FAILED_TO_PREP_FROM_K_AND_D = 20,
        WBECC_DSA_OVERSIZE_AUGMENT_FAILED = 21,
        WBECC_DSA_SUB_FAILURE = 22,
        WBECC_DSA_R_INPUT_WRONG_SIZE = 23,
        WBECC_DSA_S_INPUT_WRONG_SIZE = 24,
        WBECC_DSA_Q_INPUT_WRONG_SIZE = 25,
        WBECC_DSA_RESULT_INPUT_WRONG_SIZE = 26,
        WBECC_DSA_RESULT_INPUT_MISMATCH = 27,
        WBECC_DSA_PROJECTIFY_FAILURE = 28,
        WBECC_DSA_SIG_VERIFY_FAILURE = 29,
        WBECC_DSA_FST_PREPARE_KEY_ERROR = 30,
        WBECC_DSA_FST_NO_DYN_KEYPREP_AVAILABLE = 31,
        WBECC_DSA_KEYS_ARE_NEWER_FORMAT = 32,
        WBECC_DSA_PMULT_FAILURE = 33,
    } wbecc_dsa_status_t;
    
    typedef enum {
        WBECC_SHA1 = WBECC_STD_SHA1,
        WBECC_SHA2_224 = WBECC_STD_SHA2_224,
        WBECC_SHA2_256 = WBECC_STD_SHA2_256
    } wbecc_digest_mode_t;
    
    typedef int (*wbecc_dsa_nonce_func) (const unsigned int, wbecc_enc_t * const, const wbecc_enc_cfg * const);

    typedef struct _wbecc_dsa_ctx {
        uint8_t table;
        union {
            wbecc_dsa_ctx_tbl_t tbl;
            wbecc_dsa_ctx_fst_t fst;
        } u;
    } wbecc_dsa_ctx_t;

    int wbecc_dsa_init(
        wbecc_dsa_ctx_t * const ctx, 
        const wbecc_key_pair_t * const key_pair, 
        const wbecc_table_curve_t * const tcurve,
        wbecc_dsa_nonce_func get_nonce,
        wbecc_enc_cfg * const wbecc_cfg,
        const wbecc_digest_mode_t digest_mode
    );

    int wbecc_dsa_init_fst(
        wbecc_dsa_ctx_t * const ctx, 
        const wbecc_key_pair_t * const key_pair, 
        wbecc_dsa_nonce_func get_nonce,
        const wbecc_digest_mode_t digest_mode,
        const wbecc_fst_funcs_t * const funcs,
        const wbecc_domain_host_t * const domain
    );

    int wbecc_dsa_fst_cleanup(
        wbecc_dsa_ctx_t * const ctx
    );
    
    int wbecc_dsa_update(
        wbecc_dsa_ctx_t * const ctx, 
        const uint8_t * const input, 
        const unsigned int input_len
    );

    int wbecc_dsa_final_sign(
        wbecc_dsa_ctx_t *ctx, 
        uint8_t * const r,
        unsigned int r_size,
        unsigned int * const r_bytes_written,
        uint8_t * const s,
        unsigned int s_size,
        unsigned int * const s_bytes_written
    );

    int wbecc_dsa_final_verify(
        wbecc_dsa_ctx_t *ctx,
        uint8_t * const r,
        unsigned int r_size,
        uint8_t * const s,
        unsigned int s_size,
        uint8_t * const Qx,
        unsigned int Qx_size,
        uint8_t * const Qy,
        unsigned int Qy_size
    );

    int wbecc_dsa_final_nb_verify(
        wbecc_dsa_ctx_t *ctx,
        uint8_t * const r,
        unsigned int r_size,
        uint8_t * const s,
        unsigned int s_size,
        uint8_t * const Qx,
        unsigned int Qx_size,
        uint8_t * const Qy,
        unsigned int Qy_size,
        uint8_t * result,
        unsigned int result_size
   );

    const uint8_t * wbecc_dsa_get_oxdid(
        const wbecc_dsa_ctx_t * const ctx,
        wbecc_oxdsel_t which
    );


    #ifdef __cplusplus
        }
    #endif
#endif
