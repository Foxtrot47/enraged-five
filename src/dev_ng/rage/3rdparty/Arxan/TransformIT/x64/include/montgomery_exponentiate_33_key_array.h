/*
 * TransformIT 6.5 Production-Kit for Rockstar
 *
 * Copyright (C) Arxan Technologies Inc. 2015.
 * All rights Reserved.
 *
 *
 * Portions of the information disclosed herein are protected by
 * U.S. Patent No. 6,941,463, U.S. Patent No. 6,957,341, U.S. Patent 7,287,166,
 * U.S. Patent 7,707,433, U.S. Patent 7,757,097, U.S. Patent 7,853,018,
 * U.S. Patent 8,510,571 and Patents Pending.
 *
 */

#ifndef __MONTGOMERY_EXPONENTIATE_33_KEY_ARRAY_H__
#define __MONTGOMERY_EXPONENTIATE_33_KEY_ARRAY_H__

#include "wbrsa_config.h"

#ifdef __INLINE_OPS_
INLINE_TYPE
#endif

_size montgomery_exponentiate_33_key_array(const mword *base, _size bSize, const uint8 *exp, _size eSize, const mword *m, _size mSize, mword context, mword *result);

#ifdef __INLINE_OPS_
#include "montgomery_exponentiate_33_key_array.c"
#endif

#endif


