// R* CHANGE
// functions that return size of structs. The game uses these functions
// to verify that the static lib has struct sizes that match the 
// sizes expected by the game. These can be out of sync if the game
// uses a different set of #defines than were used when building the lib,
// or different compilers (e.g. game built with clang loading lib built
// with MSVC) 

#ifndef WOLFSSL_ROCKSTAR_CHECK_SIZES_H
#define WOLFSSL_ROCKSTAR_CHECK_SIZES_H

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/wolfcrypt/visibility.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

WOLFSSL_API size_t GetSizeOfRsaKey(void);
WOLFSSL_API size_t GetSizeOfAes(void);
WOLFSSL_API size_t GetSizeOfRng(void);
WOLFSSL_API size_t GetSizeOfMd5(void);
WOLFSSL_API size_t GetSizeOfSha(void);
WOLFSSL_API size_t GetSizeOfSha256(void);
WOLFSSL_API size_t GetSizeOfDhKey(void);
WOLFSSL_API size_t GetSizeOfSslCtx(void);
WOLFSSL_API size_t GetSizeOfSsl(void);
WOLFSSL_API size_t GetSizeOfX509(void);

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* WOLFSSL_ROCKSTAR_CHECK_SIZES_H */
