@echo off

set CONFIGURATION="%1"
set PLATFORM="%2"
set OUT_DIR=%3

IF /I %CONFIGURATION%=="ToolDebug" GOTO Tool
IF /I %CONFIGURATION%=="ToolBeta" GOTO Tool
IF /I %CONFIGURATION%=="ToolRelease" GOTO Tool
IF /I %PLATFORM%=="x64" GOTO x64
IF /I %PLATFORM%=="Orbis" GOTO Orbis
IF /I %PLATFORM%=="Prospero" GOTO Prospero
IF /I %PLATFORM%=="Durango" GOTO Durango
IF /I %PLATFORM%=="Gaming.Xbox.Scarlett.x64" GOTO Gaming.Xbox.Scarlett.x64

GOTO End

:Orbis
set INPUT_FILE=".\wolfcrypt\src\aes_asm.S"
set OUPUT_FILE="%OUT_DIR%\aes_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
orbis-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\aes_gcm_asm.S"
set OUPUT_FILE="%OUT_DIR%\aes_gcm_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
orbis-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sha256_asm.S"
set OUPUT_FILE="%OUT_DIR%\sha256_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
orbis-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sha512_asm.S"
set OUPUT_FILE="%OUT_DIR%\sha512_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
orbis-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\fe_x25519_asm.S"
set OUPUT_FILE="%OUT_DIR%\fe_x25519_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
orbis-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sp_x86_64_asm.S"
set OUPUT_FILE="%OUT_DIR%\sp_x86_64_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
orbis-clang.exe -DWOLFSSL_SP_X86_64_ASM -DWOLFSSL_SP_384 -DWOLFSSL_SP_4096 -c %INPUT_FILE% -o %OUPUT_FILE%

GOTO End

:Prospero
set INPUT_FILE=".\wolfcrypt\src\aes_asm.S"
set OUPUT_FILE="%OUT_DIR%\aes_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
prospero-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\aes_gcm_asm.S"
set OUPUT_FILE="%OUT_DIR%\aes_gcm_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
prospero-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sha256_asm.S"
set OUPUT_FILE="%OUT_DIR%\sha256_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
prospero-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sha512_asm.S"
set OUPUT_FILE="%OUT_DIR%\sha512_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
prospero-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\fe_x25519_asm.S"
set OUPUT_FILE="%OUT_DIR%\fe_x25519_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
prospero-clang.exe -c %INPUT_FILE% -o %OUPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sp_x86_64_asm.S"
set OUPUT_FILE="%OUT_DIR%\sp_x86_64_asm.o" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
prospero-clang.exe -DWOLFSSL_SP_X86_64_ASM -DWOLFSSL_SP_384 -DWOLFSSL_SP_4096 -c %INPUT_FILE% -o %OUPUT_FILE%

GOTO End

:Durango
REM VS 2012's version of ml64.exe doesn't recognize AVX2 asm instructions, so use VS 2015's

set INPUT_FILE=".\wolfcrypt\src\aes_asm.asm"
set OUPUT_FILE="%OUT_DIR%aes_asm.obj" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\x86_amd64\ml64.exe" /c /Zi /Fo%OUPUT_FILE% %INPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sp_x86_64_asm.asm"
set OUPUT_FILE="%OUT_DIR%sp_x86_64_asm.obj" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"

"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\x86_amd64\ml64.exe" /DWOLFSSL_SP_X86_64_ASM /DWOLFSSL_SP_384 /DWOLFSSL_SP_4096 /c /Zi /Fo%OUPUT_FILE% %INPUT_FILE%

GOTO End

:Gaming.Xbox.Scarlett.x64
REM Scarlett uses the __vectorcall calling convention in game. This requires name mangling on the wolfSSL asm functions.
REM For example "AES_256_Key_Expansion" is renamed to "AES_256_Key_Expansion@@16". There are over 150 functions that need remapping.
REM The list of names were obtained from linker errors when linking without name mangling. After grabbing the list, use this regex to create the rempap list:
REM search: ([^@]*)@@([^\ ]*)[^\r\n]*\r\n
REM replace: \n/D\1=\1@@\2
echo "Prebuild: Enabling __vectorcall name mangling"

set INPUT_FILE=".\wolfcrypt\src\aes_asm.asm"
set OUPUT_FILE="%OUT_DIR%aes_asm.obj" 
set REMAP_LIST=/DAES_256_Key_Expansion=AES_256_Key_Expansion@@16 /DAES_192_Key_Expansion=AES_192_Key_Expansion@@16 /DAES_128_Key_Expansion=AES_128_Key_Expansion@@16 /DAES_ECB_decrypt=AES_ECB_decrypt@@40 /DAES_ECB_encrypt=AES_ECB_encrypt@@40 /DAES_CBC_decrypt_by8=AES_CBC_decrypt_by8@@48 /DAES_CBC_encrypt=AES_CBC_encrypt@@48
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
ml64.exe %REMAP_LIST% /c /Zi /Fo%OUPUT_FILE% %INPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sp_x86_64_asm.asm"
set OUPUT_FILE="%OUT_DIR%sp_x86_64_asm.obj" 
set REMAP_LIST=/Dsp_2048_from_bin_bswap=sp_2048_from_bin_bswap@@32 /Dsp_2048_from_bin_movbe=sp_2048_from_bin_movbe@@32 /Dsp_2048_to_bin_bswap_32=sp_2048_to_bin_bswap_32@@16 /Dsp_2048_to_bin_movbe_32=sp_2048_to_bin_movbe_32@@16 /Dsp_2048_mul_16=sp_2048_mul_16@@24 /Dsp_2048_sqr_16=sp_2048_sqr_16@@16 /Dsp_2048_mul_avx2_16=sp_2048_mul_avx2_16@@24 /Dsp_2048_sqr_avx2_16=sp_2048_sqr_avx2_16@@16 /Dsp_2048_add_16=sp_2048_add_16@@24 /Dsp_2048_sub_in_place_32=sp_2048_sub_in_place_32@@16 /Dsp_2048_add_32=sp_2048_add_32@@24 /Dsp_2048_mul_32=sp_2048_mul_32@@24 /Dsp_2048_sqr_32=sp_2048_sqr_32@@16 /Dsp_2048_mul_avx2_32=sp_2048_mul_avx2_32@@24 /Dsp_2048_sqr_avx2_32=sp_2048_sqr_avx2_32@@16 /Dsp_2048_sub_in_place_16=sp_2048_sub_in_place_16@@16 /Dsp_2048_mul_d_32=sp_2048_mul_d_32@@24 /Dsp_2048_cond_sub_16=sp_2048_cond_sub_16@@32 /Dsp_2048_mont_reduce_16=sp_2048_mont_reduce_16@@24 /Dsp_2048_cond_sub_avx2_16=sp_2048_cond_sub_avx2_16@@32 /Dsp_2048_mul_d_16=sp_2048_mul_d_16@@24 /Dsp_2048_mul_d_avx2_16=sp_2048_mul_d_avx2_16@@24 /Dsp_2048_cmp_16=sp_2048_cmp_16@@16 /Dsp_2048_mont_reduce_avx2_16=sp_2048_mont_reduce_avx2_16@@24 /Dsp_2048_cond_sub_32=sp_2048_cond_sub_32@@32 /Dsp_2048_mont_reduce_32=sp_2048_mont_reduce_32@@24 /Dsp_2048_sub_32=sp_2048_sub_32@@24 /Dsp_2048_mul_d_avx2_32=sp_2048_mul_d_avx2_32@@24 /Dsp_2048_cond_sub_avx2_32=sp_2048_cond_sub_avx2_32@@32 /Dsp_2048_cmp_32=sp_2048_cmp_32@@16 /Dsp_2048_mont_reduce_avx2_32=sp_2048_mont_reduce_avx2_32@@24 /Dsp_2048_cond_add_16=sp_2048_cond_add_16@@32 /Dsp_2048_cond_add_avx2_16=sp_2048_cond_add_avx2_16@@32 /Dsp_3072_from_bin_bswap=sp_3072_from_bin_bswap@@32 /Dsp_3072_from_bin_movbe=sp_3072_from_bin_movbe@@32 /Dsp_3072_to_bin_bswap_48=sp_3072_to_bin_bswap_48@@16 /Dsp_3072_to_bin_movbe_48=sp_3072_to_bin_movbe_48@@16 /Dsp_3072_sub_in_place_24=sp_3072_sub_in_place_24@@16 /Dsp_3072_add_24=sp_3072_add_24@@24 /Dsp_3072_mul_24=sp_3072_mul_24@@24 /Dsp_3072_sqr_24=sp_3072_sqr_24@@16 /Dsp_3072_mul_avx2_24=sp_3072_mul_avx2_24@@24 /Dsp_3072_sqr_avx2_24=sp_3072_sqr_avx2_24@@16 /Dsp_3072_sub_in_place_48=sp_3072_sub_in_place_48@@16 /Dsp_3072_add_48=sp_3072_add_48@@24 /Dsp_3072_mul_48=sp_3072_mul_48@@24 /Dsp_3072_sqr_48=sp_3072_sqr_48@@16 /Dsp_3072_mul_avx2_48=sp_3072_mul_avx2_48@@24 /Dsp_3072_sqr_avx2_48=sp_3072_sqr_avx2_48@@16 /Dsp_3072_mul_d_48=sp_3072_mul_d_48@@24 /Dsp_3072_cond_sub_24=sp_3072_cond_sub_24@@32 /Dsp_3072_mont_reduce_24=sp_3072_mont_reduce_24@@24 /Dsp_3072_cond_sub_avx2_24=sp_3072_cond_sub_avx2_24@@32 /Dsp_3072_mul_d_24=sp_3072_mul_d_24@@24 /Dsp_3072_mul_d_avx2_24=sp_3072_mul_d_avx2_24@@24 /Dsp_3072_cmp_24=sp_3072_cmp_24@@16 /Dsp_3072_mont_reduce_avx2_24=sp_3072_mont_reduce_avx2_24@@24 /Dsp_3072_cond_sub_48=sp_3072_cond_sub_48@@32 /Dsp_3072_mont_reduce_48=sp_3072_mont_reduce_48@@24 /Dsp_3072_sub_48=sp_3072_sub_48@@24 /Dsp_3072_mul_d_avx2_48=sp_3072_mul_d_avx2_48@@24 /Dsp_3072_cond_sub_avx2_48=sp_3072_cond_sub_avx2_48@@32 /Dsp_3072_cmp_48=sp_3072_cmp_48@@16 /Dsp_3072_mont_reduce_avx2_48=sp_3072_mont_reduce_avx2_48@@24 /Dsp_3072_cond_add_24=sp_3072_cond_add_24@@32 /Dsp_3072_cond_add_avx2_24=sp_3072_cond_add_avx2_24@@32 /Dsp_4096_from_bin_bswap=sp_4096_from_bin_bswap@@32 /Dsp_4096_from_bin_movbe=sp_4096_from_bin_movbe@@32 /Dsp_4096_to_bin_bswap_64=sp_4096_to_bin_bswap_64@@16 /Dsp_4096_to_bin_movbe_64=sp_4096_to_bin_movbe_64@@16 /Dsp_4096_sub_in_place_64=sp_4096_sub_in_place_64@@16 /Dsp_4096_add_64=sp_4096_add_64@@24 /Dsp_4096_mul_64=sp_4096_mul_64@@24 /Dsp_4096_sqr_64=sp_4096_sqr_64@@16 /Dsp_4096_mul_avx2_64=sp_4096_mul_avx2_64@@24 /Dsp_4096_sqr_avx2_64=sp_4096_sqr_avx2_64@@16 /Dsp_4096_mul_d_64=sp_4096_mul_d_64@@24 /Dsp_4096_cond_sub_64=sp_4096_cond_sub_64@@32 /Dsp_4096_mont_reduce_64=sp_4096_mont_reduce_64@@24 /Dsp_4096_sub_64=sp_4096_sub_64@@24 /Dsp_4096_mul_d_avx2_64=sp_4096_mul_d_avx2_64@@24 /Dsp_4096_cond_sub_avx2_64=sp_4096_cond_sub_avx2_64@@32 /Dsp_4096_cmp_64=sp_4096_cmp_64@@16 /Dsp_4096_mont_reduce_avx2_64=sp_4096_mont_reduce_avx2_64@@24 /Dsp_4096_cond_add_32=sp_4096_cond_add_32@@32 /Dsp_4096_cond_add_avx2_32=sp_4096_cond_add_avx2_32@@32 /Dsp_256_mul_4=sp_256_mul_4@@24 /Dsp_256_sqr_4=sp_256_sqr_4@@16 /Dsp_256_add_4=sp_256_add_4@@24 /Dsp_256_sub_4=sp_256_sub_4@@24 /Dsp_256_cond_copy_4=sp_256_cond_copy_4@@24 /Dsp_256_mont_mul_4=sp_256_mont_mul_4@@40 /Dsp_256_mont_sqr_4=sp_256_mont_sqr_4@@32 /Dsp_256_cmp_4=sp_256_cmp_4@@16 /Dsp_256_cond_sub_4=sp_256_cond_sub_4@@32 /Dsp_256_mont_reduce_4=sp_256_mont_reduce_4@@24 /Dsp_256_mont_add_4=sp_256_mont_add_4@@32 /Dsp_256_mont_dbl_4=sp_256_mont_dbl_4@@24 /Dsp_256_mont_tpl_4=sp_256_mont_tpl_4@@24 /Dsp_256_mont_sub_4=sp_256_mont_sub_4@@32 /Dsp_256_div2_4=sp_256_div2_4@@24 /Dsp_256_get_point_33_4=sp_256_get_point_33_4@@24 /Dsp_256_get_point_33_avx2_4=sp_256_get_point_33_avx2_4@@24 /Dsp_256_mont_mul_avx2_4=sp_256_mont_mul_avx2_4@@40 /Dsp_256_mont_sqr_avx2_4=sp_256_mont_sqr_avx2_4@@32 /Dsp_256_cond_sub_avx2_4=sp_256_cond_sub_avx2_4@@32 /Dsp_256_mont_reduce_avx2_4=sp_256_mont_reduce_avx2_4@@24 /Dsp_256_div2_avx2_4=sp_256_div2_avx2_4@@24 /Dsp_256_get_entry_65_4=sp_256_get_entry_65_4@@24 /Dsp_256_get_entry_65_avx2_4=sp_256_get_entry_65_avx2_4@@24 /Dsp_256_add_one_4=sp_256_add_one_4@@8 /Dsp_256_from_bin_bswap=sp_256_from_bin_bswap@@32 /Dsp_256_from_bin_movbe=sp_256_from_bin_movbe@@32 /Dsp_256_to_bin_bswap_4=sp_256_to_bin_bswap_4@@16 /Dsp_256_to_bin_movbe_4=sp_256_to_bin_movbe_4@@16 /Dsp_256_mul_avx2_4=sp_256_mul_avx2_4@@24 /Dsp_256_sub_in_place_4=sp_256_sub_in_place_4@@16 /Dsp_256_mul_d_4=sp_256_mul_d_4@@24 /Dsp_256_mul_d_avx2_4=sp_256_mul_d_avx2_4@@24 /Dsp_256_mont_mul_order_avx2_4=sp_256_mont_mul_order_avx2_4@@24 /Dsp_256_mont_sqr_order_avx2_4=sp_256_mont_sqr_order_avx2_4@@16 /Dsp_256_mod_inv_4=sp_256_mod_inv_4@@24 /Dsp_256_mod_inv_avx2_4=sp_256_mod_inv_avx2_4@@24 /Dsp_384_mul_6=sp_384_mul_6@@24 /Dsp_384_sqr_6=sp_384_sqr_6@@16 /Dsp_384_add_6=sp_384_add_6@@24 /Dsp_384_sub_6=sp_384_sub_6@@24 /Dsp_384_cond_copy_6=sp_384_cond_copy_6@@24 /Dsp_384_cond_sub_6=sp_384_cond_sub_6@@32 /Dsp_384_mont_reduce_6=sp_384_mont_reduce_6@@24 /Dsp_384_mont_reduce_order_6=sp_384_mont_reduce_order_6@@24 /Dsp_384_cmp_6=sp_384_cmp_6@@16 /Dsp_384_dbl_6=sp_384_dbl_6@@16 /Dsp_384_cond_add_6=sp_384_cond_add_6@@32 /Dsp_384_div2_6=sp_384_div2_6@@24 /Dsp_384_get_point_33_6=sp_384_get_point_33_6@@24 /Dsp_384_get_point_33_avx2_6=sp_384_get_point_33_avx2_6@@24 /Dsp_384_mul_avx2_6=sp_384_mul_avx2_6@@24 /Dsp_384_mont_reduce_order_avx2_6=sp_384_mont_reduce_order_avx2_6@@24 /Dsp_384_sqr_avx2_6=sp_384_sqr_avx2_6@@16 /Dsp_384_cond_sub_avx2_6=sp_384_cond_sub_avx2_6@@32 /Dsp_384_div2_avx2_6=sp_384_div2_avx2_6@@24 /Dsp_384_get_entry_65_6=sp_384_get_entry_65_6@@24 /Dsp_384_get_entry_65_avx2_6=sp_384_get_entry_65_avx2_6@@24 /Dsp_384_add_one_6=sp_384_add_one_6@@8 /Dsp_384_from_bin_bswap=sp_384_from_bin_bswap@@32 /Dsp_384_from_bin_movbe=sp_384_from_bin_movbe@@32 /Dsp_384_to_bin_bswap_6=sp_384_to_bin_bswap_6@@16 /Dsp_384_to_bin_movbe_6=sp_384_to_bin_movbe_6@@16 /Dsp_384_sub_in_place_6=sp_384_sub_in_place_6@@16 /Dsp_384_mul_d_6=sp_384_mul_d_6@@24 /Dsp_384_mul_d_avx2_6=sp_384_mul_d_avx2_6@@24 /Dsp_384_rshift1_6=sp_384_rshift1_6@@16 /Dsp_384_div2_mod_6=sp_384_div2_mod_6@@24 /Dsp_384_num_bits_6=sp_384_num_bits_6@@8
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
ml64.exe /DWOLFSSL_SP_X86_64_ASM /DWOLFSSL_SP_384 /DWOLFSSL_SP_4096 %REMAP_LIST% /c /Zi /Fo%OUPUT_FILE% %INPUT_FILE%
GOTO End

:x64
:Tool

REM VS 2012's version of ml64.exe doesn't recognize AVX2 asm instructions, so use VS 2015's

set INPUT_FILE=".\wolfcrypt\src\aes_asm.asm"
set OUPUT_FILE="%OUT_DIR%aes_asm.obj" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"
"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\x86_amd64\ml64.exe" /c /Zi /Fo%OUPUT_FILE% %INPUT_FILE%

set INPUT_FILE=".\wolfcrypt\src\sp_x86_64_asm.asm"
set OUPUT_FILE="%OUT_DIR%sp_x86_64_asm.obj" 
echo "Prebuild: %INPUT_FILE% -> %OUPUT_FILE%"

"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\x86_amd64\ml64.exe" /DWOLFSSL_SP_X86_64_ASM /DWOLFSSL_SP_384 /DWOLFSSL_SP_4096 /c /Zi /Fo%OUPUT_FILE% %INPUT_FILE%

GOTO End

:End
