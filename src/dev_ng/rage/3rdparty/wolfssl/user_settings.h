// use the force include's RSG_BE define to determine which wolfSSL endian define to enable
#if RSG_BE
	#define BIG_ENDIAN_ORDER
#else
	#define LITTLE_ENDIAN_ORDER
#endif

#define XMALLOC_USER			// enables user-defined memory allocators
#define WOLFSSL_SHA384			// enables SHA-384 support
#define WOLFSSL_SHA512			// enables SHA-512 support
#define HAVE_AESGCM				// enables AES-GCM support
#define HAVE_ECC				// enables Elliptical Curve Cryptography (ECC) support
#define HAVE_ALL_CURVES			// enables all ECC Curve Sizes
#define HAVE_CURVE25519			// enables the use of curve25519 algorithm
#define HAVE_ED25519			// enables the use of ED25519 algorithm
#define HAVE_TLS_EXTENSIONS		// enables TLS extensions - required for ECC cipher suites
#define HAVE_SUPPORTED_CURVES	// required for ECC cipher suites
#define WOLFSSL_GAME_BUILD		// just makes wolfSSL include system/xtl.h
#define WOLFSSL_DTLS			// enables Datagram TLS (UDP)
#define WOLFSSL_DTLS_MTU		// allows setting the MTU (Maximum Transmission Unit) for DTLS
#define WOLFSSL_STATIC_RSA		// enables static RSA cipher suites (used by SC servers)
#define WOLFSSL_AES_DIRECT		// enables AES-ECB mode (considered insecure but we use it for RPFs?)
#define ECC_TIMING_RESISTANT	// enables resistances to timing attacks in the ECC algorithm
#define WC_RSA_BLINDING			// enables RSA Blinding which hardens RSA
#define HAVE_SOCKADDR			// for systems that support sockaddr_storage, etc.
#define HAVE_EXTENDED_MASTER	// hardens tls, enabled by default in wolfSSL build scripts
#define NO_WOLFSSL_SMALL_STACK	// Uses fewer dynamic allocations and speeds up ECC TLS handshakes
#define MEDIUM_STATIC_BUFFERS	// uses static buffers large enough to eliminate per-packet dynamic allocations when using DTLS with AES-GCM
#define USE_FAST_MATH			// enable fast math library - faster and far fewer dynamic allocs
#define TFM_TIMING_RESISTANT	// hardens fast math library against timing attacks
#define ECC_SHAMIR				// speeds up ECC operations, uses more memory

// Enable wolfSSL's SP Math library. This gives a 10x-15x speed up in many RSA, DH, and ECC operations for most common key sizes.
// Note: enable WOLFSSL_SP but not WOLFSSL_SP_MATH. The latter disables fall back when unsupported key sizes are encountered.
// Note: these defines also need to be passed to the asm assembler (see prebuild.bat)
#define WOLFSSL_SP				// main SP Math define
#define WOLFSSL_HAVE_SP_ECC		// ECC 256-bit SECP256R1
#define WOLFSSL_HAVE_SP_DH		// DH-2048 and DH-3072
#define WOLFSSL_HAVE_SP_RSA		// RSA-2048 and RSA-3072
#define WOLFSSL_SP_4096			// RSA/DH 4096-bit
#define WOLFSSL_SP_384			// ECC 384-bit SECP384R1
#define WOLFSSL_SP_ASM			// enable asm for SP Math
#define WOLFSSL_SP_X86_64_ASM	// instruction set to use for SP Math asm

// P2P DTLS
#define WOLFSSL_KEY_GEN			// enables key generation
#define WOLFSSL_CERT_GEN		// enables certificate generation
#define WOLFSSL_CERT_REQ 		// enables certificate signing request (CSR) generation
#define WOLFSSL_ALWAYS_VERIFY_CB// always call the certificate verify callback, even if the certificate verifies successfully
#define KEEP_PEER_CERT			// allows custom inspection of the peer's certificates

// Note: AES-NI requires platform-specific build steps to generate object files from wolfSSL's .asm/.S files.
// Support for new platforms can be enabled as long as these build steps are in place.
#if RSG_PC || RSG_ORBIS || RSG_DURANGO
#define WOLFSSL_AESNI			// use Intel AES New Instructions (hardware accelerated AES)
#endif

// Note: only .S asm files supplied by wolfSSL, no .asm, so not available on platforms that use MSVC.
#if RSG_ORBIS
#define USE_INTEL_SPEEDUP		// enables use of Intel/AMD AVX/AVX2 instructions if supported by the CPU
								// speeds up AES, ChaCha20, Poly1305, SHA256, SHA512, ED25519, Curve25519
#endif

// disable some features that we don't use to reduce code size
#define NO_FILESYSTEM			// disable direct file system use
#define NO_STDIO_FILESYSTEM		// disable direct stdio file system use
#define NO_WOLFSSL_DIR			// disable file system directory use
#define NO_PSK					// disable pre-shared keys
#define NO_DES3					// disable obsolete des3 encryption
#define NO_MD4					// disable obsolete md4 hashing
#define NO_RABBIT				// disable Rabbit stream cipher
#define NO_HC128				// disable HC-128 stream cipher

// dev builds - enable debug output and access to the negotiated
// cipher suite and cert chain, so we can debug SSL connection issues.
#if RSG_DEV
#define DEBUG_WOLFSSL
#define SESSION_CERTS
#define NO_ERROR_QUEUE
#endif
