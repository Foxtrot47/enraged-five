cmake_minimum_required(VERSION 3.4.1)

project(wolfssl LANGUAGES C CXX)

set(wolfssl_src_files
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/aes.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/arc4.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/asm.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/asn.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/async.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/blake2b.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/blake2s.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/camellia.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/chacha.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/chacha20_poly1305.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/cmac.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/coding.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/compress.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/cpuid.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/cryptocb.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/curve25519.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/curve448.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/des3.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/dh.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/dsa.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/ecc.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/ecc_fp.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/eccsi.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/ed25519.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/ed448.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/error.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/falcon.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/fe_448.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/fe_low_mem.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/fe_operations.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/fips.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/fips_test.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/ge_448.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/ge_low_mem.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/ge_operations.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/hash.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/hmac.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/integer.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/kdf.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/logging.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/md2.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/md4.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/md5.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/memory.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/pkcs12.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/pkcs7.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/poly1305.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/pwdbased.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/random.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/rc2.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/ripemd.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/rsa.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sakke.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/selftest.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sha.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sha256.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sha3.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sha512.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/signature.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/siphash.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_arm32.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_arm64.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_armthumb.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_c32.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_c64.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_cortexm.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_dsp32.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_int.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/sp_x86_64.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/srp.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/tfm.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/wc_dsp.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/wc_encrypt.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/wc_pkcs11.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/wc_port.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/wolfcrypt_first.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/wolfcrypt_last.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/wolfevent.c
	${CMAKE_CURRENT_LIST_DIR}/../wolfcrypt/src/wolfmath.c
	${CMAKE_CURRENT_LIST_DIR}/../src/check_sizes.c
	${CMAKE_CURRENT_LIST_DIR}/../src/crl.c
	${CMAKE_CURRENT_LIST_DIR}/../src/internal.c
	${CMAKE_CURRENT_LIST_DIR}/../src/keys.c
	${CMAKE_CURRENT_LIST_DIR}/../src/ocsp.c
	${CMAKE_CURRENT_LIST_DIR}/../src/sniffer.c
	${CMAKE_CURRENT_LIST_DIR}/../src/ssl.c
	${CMAKE_CURRENT_LIST_DIR}/../src/tls.c
	${CMAKE_CURRENT_LIST_DIR}/../src/tls13.c
	${CMAKE_CURRENT_LIST_DIR}/../src/wolfio.c
)

if(PROJECT_BUILD_TYPE STREQUAL "Debug")
	set(wolfssl_forceincludes "-include $ENV{RAGE_DIR}/base/src/forceinclude/android_debug.h")
	set(wolfssl_defines "-DWOLFSSL_USER_SETTINGS -D_LIB -D_DEBUG")
	set(wolfssl_libraries )
endif()
if(PROJECT_BUILD_TYPE STREQUAL "Release")
	set(wolfssl_forceincludes "-include $ENV{RAGE_DIR}/base/src/forceinclude/android_release.h")
	set(wolfssl_defines "-DWOLFSSL_USER_SETTINGS -D_LIB -DNDEBUG")
	set(wolfssl_libraries )
endif()

add_library(wolfssl STATIC ${wolfssl_src_files})

target_include_directories(wolfssl PUBLIC 
	$ENV{RAGE_DIR}/3rdParty/wolfssl/wolfssl/openssl
	$ENV{RAGE_DIR}/3rdParty/wolfssl/wolfssl
	$ENV{RAGE_DIR}/3rdParty/wolfssl
	$ENV{RAGE_DIR}/base/src
	$ENV{RAGE_DIR}/suite/src
	${CMAKE_CURRENT_LIST_DIR}/../..
	$ENV{RAGE_DIR}/script/src
	$ENV{RAGE_DIR}/framework/src
	$ENV{RAGE_DIR}/../tools
	$ENV{RAGE_DIR}/../tools/pscs
	$ENV{RAGE_DIR}/../tools/framework/libs
)

set_target_properties(wolfssl PROPERTIES COMPILE_FLAGS "-frtti ${wolfssl_defines} ${wolfssl_forceincludes} -flax-vector-conversions -fsigned-char -fshort-wchar")
set_property(TARGET wolfssl PROPERTY CXX_STANDARD 11)

