// R* CHANGE
// functions that return size of structs. The game uses these functions
// to verify that the static lib has struct sizes that match the 
// sizes expected by the game. These can be out of sync if the game
// uses a different set of #defines than were used when building the lib.

#ifdef HAVE_CONFIG_H
    #include <config.h>
#endif

#include <wolfssl/wolfcrypt/settings.h>

#include "check_sizes.h"
#include <wolfssl/ssl.h>
#include <wolfssl/internal.h>
#include <wolfssl/wolfcrypt/aes.h>
#include <wolfssl/wolfcrypt/asn.h>
#include <wolfssl/wolfcrypt/dh.h>
#include <wolfssl/wolfcrypt/md5.h>
#include <wolfssl/wolfcrypt/rsa.h>
#include <wolfssl/wolfcrypt/sha.h>

size_t GetSizeOfRsaKey(void)
{
	return sizeof(RsaKey);
}

size_t GetSizeOfAes(void)
{
	return sizeof(Aes);
}

size_t GetSizeOfRng(void)
{
	return sizeof(WC_RNG);
}

size_t GetSizeOfMd5(void)
{
	return sizeof(wc_Md5);
}

size_t GetSizeOfSha(void)
{
	return sizeof(wc_Sha);
}

size_t GetSizeOfSha256(void)
{
	return sizeof(wc_Sha256);
}

size_t GetSizeOfDhKey(void)
{
	return sizeof(DhKey);
}

size_t GetSizeOfSslCtx(void)
{
	return sizeof(WOLFSSL_CTX);
}

size_t GetSizeOfSsl(void)
{
	return sizeof(WOLFSSL);
}

size_t GetSizeOfX509(void)
{
	return sizeof(WOLFSSL_X509);
}
