@echo off

REM Check out the static libs from perforce:

REM debug libs
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfssld_pc.lib
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfssld_orbis.a
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfssld_durango.lib

REM beta libs
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfsslb_pc.lib
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfsslb_orbis.a
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfsslb_durango.lib

REM final libs
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfssl_pc.lib
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfssl_orbis.a
p4 edit %RS_CODEBRANCH%/rage/lib/libwolfssl_durango.lib

REM Rebuild the static libs:

REM debug libs
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Debug|x64"
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Debug|Orbis"
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Debug|Durango"

REM beta libs
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Beta|x64"
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Beta|Orbis"
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Beta|Durango"

REM final libs
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Final|x64"
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Final|Orbis"
call BuildConsole wolfssl_2012.sln /rebuild /cfg="Final|Durango"

REM Copy debug libs to rage\lib
xcopy /V /Y /F .\win64_debug_2012\libwolfssld_pc_2012.lib %RS_CODEBRANCH%\rage\lib\libwolfssld_pc.lib
xcopy /V /Y /F .\orbis_debug_2012\libwolfssld_orbis_2012.a %RS_CODEBRANCH%\rage\lib\libwolfssld_orbis.a
xcopy /V /Y /F .\durango_debug_2012\libwolfssld_durango_2012.lib %RS_CODEBRANCH%\rage\lib\libwolfssld_durango.lib

REM Copy beta libs to rage\lib
xcopy /V /Y /F .\win64_beta_2012\libwolfsslb_pc_2012.lib %RS_CODEBRANCH%\rage\lib\libwolfsslb_pc.lib
xcopy /V /Y /F .\orbis_beta_2012\libwolfsslb_orbis_2012.a %RS_CODEBRANCH%\rage\lib\libwolfsslb_orbis.a
xcopy /V /Y /F .\durango_beta_2012\libwolfsslb_durango_2012.lib %RS_CODEBRANCH%\rage\lib\libwolfsslb_durango.lib

REM Copy final libs to rage\lib
xcopy /V /Y /F .\win64_final_2012\libwolfssl_pc_2012.lib %RS_CODEBRANCH%\rage\lib\libwolfssl_pc.lib
xcopy /V /Y /F .\orbis_final_2012\libwolfssl_orbis_2012.a %RS_CODEBRANCH%\rage\lib\libwolfssl_orbis.a
xcopy /V /Y /F .\durango_final_2012\libwolfssl_durango_2012.lib %RS_CODEBRANCH%\rage\lib\libwolfssl_durango.lib

pause
