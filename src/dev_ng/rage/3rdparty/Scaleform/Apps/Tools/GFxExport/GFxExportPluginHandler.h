/**********************************************************************

Filename    :   GFxExportPluginHandler.h
Content     :   GFxExport plugin handler class 
Created     :   May 6, 2008
Authors     :   

Copyright   :   (c) 2005-2008 Scaleform Corp. All Rights Reserved.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#ifndef INC_GFxExportPluginHandler_H
#define INC_GFxExportPluginHandler_H

#include "GStd.h"
#include "GTypes.h"
#include "GRefCount.h"
#include "GFile.h"
#include "GString.h"
#include "GFxStream.h"

#include "GFxExportPlugin.h"

#define PLUGIN_NAME         "GFxExport_Plugin"
#define PLUGIN_REVISION     1

class GFxExportPluginHandler
{
public:
    bool WantsTag(GFxTagType i_eTag);

    bool LoadPluginLibrary(const char *pLibraryName);
    bool Initialize() { return pLibraryPlugin ? pLibraryPlugin->Initialize() : true; }
    bool BeginFile(const char *i_pSWF) { return pLibraryPlugin ? pLibraryPlugin->BeginFile(i_pSWF) : true; }
    bool EndFile() { return pLibraryPlugin ? pLibraryPlugin->EndFile() : true; }
    bool Shutdown() { return pLibraryPlugin ? pLibraryPlugin->Shutdown() : true; }

    bool HandleTag(const GFxTagInfo& tag, GFile* pin, GFile* pout);

    bool ModifyFileReference(GFxExportPlugin::FileType fileType,  const GString& oldFileName, const GString& exportName, GString& newFileName);

    int ParseCommandlineArg(const char* arg);

    void PrintUsage()
    {
        if (pLibraryPlugin)
            pLibraryPlugin->PrintUsage();
    }
   
    class PluginStringResult : public GFxExportPlugin::StringResult
    {
    public:
        GString Buff;
        virtual void  SetString(const char* buff) {Buff = buff; }
    }; 

protected:
    GPtr<GFxExportPlugin> pLibraryPlugin;


    bool HandleTag_Import(const GFxTagInfo& tag, GFile* pin, GFile* pout);
    bool HandleTag_DefineEditText(const GFxTagInfo& tag, GFile* pin, GFile* pout);

    static void WriteStringToGFile(GFile *pFile, const char *pstr);
};

#endif //#ifndef INC_GFxExportPluginHandler_H
