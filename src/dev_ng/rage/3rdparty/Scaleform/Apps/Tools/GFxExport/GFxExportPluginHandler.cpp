/**********************************************************************

Filename    :   GFxExportPluginHandler.cpp
Content     :   GFxExport plugin handler class implementation
Created     :   May 6, 2008
Authors     :   

Copyright   :   (c) 2005-2008 Scaleform Corp. All Rights Reserved.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/
#include "GFxExportPluginHandler.h"
#include "GFxExport.h"

#ifdef GFC_OS_WIN32
#else
#include <dlfcn.h>
#endif

int GFxExportPluginHandler::ParseCommandlineArg(const char* arg)
{
    if ( pLibraryPlugin )
    {
        if ( pLibraryPlugin->ParseCommandlineArg(arg) )
        {
            return 1;
        }
        else 
        {
            return -1;
        }
    }
    return 0;
}


bool GFxExportPluginHandler::LoadPluginLibrary( const char *pLibraryName )
{
    const char *szFactoryFunction = "CreateGFxExportPlugin";
#ifdef GFC_OS_WIN32
    SetErrorMode(0);
    HMODULE hDllPlugin = LoadLibrary(pLibraryName);
    if ( !hDllPlugin )
    {
        fprintf(stderr, "could not load dll '%s' (%d)!\n", pLibraryName, GetLastError());
        return false;
    }
    // We're going to leak the ref on hDllPlugin.  The OS will clean it up on shutdown.

    FN_CreateGFxExportPlugin fnFactory = (FN_CreateGFxExportPlugin)GetProcAddress(hDllPlugin, szFactoryFunction);

#else
     void *handle = dlopen (pLibraryName, RTLD_LAZY);
     if (!handle)
     {
         fprintf(stderr, "could not load library '%s' !\n", pLibraryName);
         return false;
     }
     FN_CreateGFxExportPlugin fnFactory = (FN_CreateGFxExportPlugin)dlsym(handle, szFactoryFunction);

#endif

     if ( !fnFactory )
     {
         fprintf(stderr, "could not find the function %s in %s!\n", szFactoryFunction, pLibraryName);
         return false;
     }

    GFxExportPlugin *pPlugin = NULL;

     pPlugin = (GFxExportPlugin*)fnFactory(PLUGIN_NAME, PLUGIN_REVISION);

    if (!pPlugin)
    {
        fprintf(stderr, "Could not create the plugin object \n");
        return false;
    }
    pLibraryPlugin.SetPtr(*pPlugin);

    return true;
}


bool GFxExportPluginHandler::HandleTag( const GFxTagInfo& tag, GFile* pin, GFile* pout )
{
    switch (tag.TagType)
    {
    case GFxTag_Import:
    case GFxTag_Import2:
        return HandleTag_Import(tag, pin, pout);

    case GFxTag_DefineEditText:
        return HandleTag_DefineEditText(tag, pin, pout);

    default:
        return false;
    }
}


void GFxExportPluginHandler::WriteStringToGFile( GFile *pFile, const char *pstr )
{
    // The reverse of GFxStream::ReadString()
    const char *p = pstr;
    do 
    {
        pFile->WriteSByte(*p);
    } while (*p++);
}


bool GFxExportPluginHandler::HandleTag_Import( const GFxTagInfo& tag, GFile* pin, GFile* pout )
{
    // Patterned after the code in GFx_ImportLoader(), in GFxTagLoaders.cpp
    GFxStream streamIn(pin, GMemory::GetGlobalHeap(), NULL, NULL);
    GFxStream  *p = &streamIn; // So we can block copy code from GFxTagLoaders
    GASSERT(p->Tell() == tag.TagDataOffset);


    GString strOldSource;
    GString strSource;

    p->ReadString(&strOldSource);
    UInt16  count = p->ReadU16();
    
    if ( !ModifyFileReference(GFxExportPlugin::File_Import, strOldSource, GString(), strSource) )
    {
        strSource = strOldSource;
    }    

    UInt len = tag.TagLength - (p->Tell() - tag.TagDataOffset); // input bytes remaining
    len += (UInt)strSource.GetSize()+1;
    len += 2; // count
    GFxDataExporter::WriteTagHeader(pout, GFxTag_Import2, len);
    UInt oposDataStart = pout->Tell();
    WriteStringToGFile(pout, strSource.ToCStr());
    pout->WriteUInt16(count);

    UInt16 val = 1;
    if (tag.TagType == GFxTag_Import2)
    {
        UInt16 val = p->ReadU16();
        GUNUSED(val);
        GFC_DEBUG_WARNING1(val != 1, "Unexpected attribute in ImportAssets2 - 0x%X instead of 1", val);
    }
    pout->WriteUInt16(val);

    for (int i = 0; i < count; i++)
    {               
        GString symbolName;
        UInt16    id = p->ReadU16();
        p->ReadString(&symbolName);

        pout->WriteUInt16(id);
        WriteStringToGFile(pout, symbolName.ToCStr());
    }

    UInt posCur = pout->Tell();
    GASSERT(posCur-oposDataStart == len);
    pin->Seek(tag.TagLength + tag.TagDataOffset);
    GUNUSED2(posCur,oposDataStart); // To get rid of warning in Release
    return true;
}


bool GFxExportPluginHandler::HandleTag_DefineEditText( const GFxTagInfo& tag, GFile* pin, GFile* pout )
{
    PluginStringResult text;
//    char text[4096];
//    text[0] = 0;
    GString varname;
    GString deftext;

    {
        // Patterned after the code in GFx_DefineEditTextLoader(), in GFxTextField.cpp
        GFxStream streamIn(pin, GMemory::GetGlobalHeap(), NULL, NULL);
        GFxStream  *p = &streamIn; // So we can block copy code from the loader
        GASSERT(p->Tell() == tag.TagDataOffset);

        /*UInt16  characterId = */p->ReadU16();

        // Patterned after the code in GFxEditTextCharacterDef::Read, in GFxTextField.cpp
        GRectF TextRect;
        p->ReadRect(&TextRect);

        p->Align();
        bool    hasText = p->ReadUInt(1) ? true : false;
        bool bWordWrap = p->ReadUInt(1) != 0; GUNUSED(bWordWrap);
        bool bMultiline = p->ReadUInt(1) != 0; GUNUSED(bMultiline);
        bool bPassword = p->ReadUInt(1) != 0;  GUNUSED(bPassword);
        bool bReadOnly = p->ReadUInt(1) != 0; GUNUSED(bReadOnly);

        bool    hasColor = p->ReadUInt(1) ? true : false;
        bool    hasMaxLength = p->ReadUInt(1) ? true : false;
        bool    hasFont = p->ReadUInt(1) ? true : false;

        p->ReadUInt(1);    // reserved
        bool bAutoSize = p->ReadUInt(1) != 0; GUNUSED(bAutoSize);
        bool    hasLayout = p->ReadUInt(1) ? true : false;
        bool bSelectable = p->ReadUInt(1) == 0; GUNUSED(bSelectable);
        bool bBorder  = p->ReadUInt(1) != 0; GUNUSED(bBorder);
        p->ReadUInt(1);    // reserved

        // In SWF 8 text is *ALWAYS* marked as HTML.
        // Correction, AB: no, only if kerning is ON
        // In SWF <= 7, that is not the case.
        bool bHtml = p->ReadUInt(1) != 0; GUNUSED(bHtml);
        bool bUseDeviceFont = p->ReadUInt(1) == 0; GUNUSED(bUseDeviceFont);

        if (hasFont)
        {
            p->ReadU16();
            p->ReadU16();
        }

        if (hasColor)
        { 
            GColor Color;
            p->ReadRgba(&Color);
        }

        if (hasMaxLength)
        {
            p->ReadU16();
        }

        if (hasLayout)
        {
            /*Alignment = (alignment) */p->ReadU8();
            /*LeftMargin = (Float) */p->ReadU16();
            /*RightMargin = (Float) */p->ReadU16();
            /*Indent = (Float) */p->ReadS16();
            /*Leading = (Float) */p->ReadS16();
        }

        p->ReadString(&varname);
        if (hasText)        
            p->ReadString(&deftext);
    }

    bool res = false;
    SInt lenPreString = tag.TagLength;
    if (!deftext.IsEmpty())
    {
        lenPreString -= (UInt)deftext.GetSize() + 1;
        res = pLibraryPlugin->ModifyEditText(varname.ToCStr(), deftext.ToCStr(), &text);
    }

    if (!res)
        return false;

    SInt len = lenPreString + (UInt)text.Buff.GetSize() + 1;

    GFxDataExporter::WriteTagHeader(pout, tag.TagType, len);
    pin->Seek(tag.TagDataOffset);
    if (pout->CopyFromStream(pin, lenPreString) == -1)
    {
        return false;
    }
    WriteStringToGFile(pout, text.Buff.ToCStr());

    pin->Seek(tag.TagDataOffset + tag.TagLength);
    return true;
}

bool GFxExportPluginHandler::ModifyFileReference( GFxExportPlugin::FileType fileType, const GString& oldFileName, const GString& exportName, GString& newFileName )
{
    
    PluginStringResult stringResult;

    if ( pLibraryPlugin )
    {
        const char *szExportName = exportName.GetSize() ? exportName.ToCStr() : NULL;
        if (pLibraryPlugin->ModifyFileReference(fileType, oldFileName.ToCStr(), szExportName, &stringResult))
        {
            newFileName = stringResult.Buff;
            printf("\"%s\" -> \"%s\"\n", oldFileName.ToCStr(), newFileName.ToCStr());
            return true;
        }
        else
            return false;
    }
    return false;
}

bool GFxExportPluginHandler::WantsTag( GFxTagType i_eTag )
{
    if ( !pLibraryPlugin ) return false;
    switch (i_eTag)
    {
    case GFxTag_Import:
    case GFxTag_Import2:
    case GFxTag_DefineEditText:
        return true;
    default:
        break;
    }
    return false;
}
