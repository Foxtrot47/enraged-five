/**********************************************************************

Filename    :   GFxExport.h
Content     :   SWF to GFX resource extraction and conversion tool
Created     :   May, 2007
Authors     :   Artyom Bolgar, Dmitry Polenur, Maxim Didenko
Copyright   :   (c) 2006-2009 Scaleform Corp. All Rights Reserved.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#ifndef INC_GFXEXPORT_H
#define INC_GFXEXPORT_H

// GFx Includes
#include "GStd.h"
#include "GTypes.h"
#include "GAutoPtr.h"
#include "GRefCount.h"

#include "GFile.h"
#include "GZlibFile.h"
#include "GImage.h"
#include "GJPEGUtil.h"
#include "GFxPlayer.h"
#include "GFxLoader.h"
#include "GFxLog.h"
#include "GString.h"
#include "GFxStream.h"
#include "GFxFontResource.h"
#include "GFxStyles.h"
#include "GFxText.h"
#include "FxSettings.h"
#include "GSoundRenderer.h"

#include "GDXTHelper.h"
#include "GFxExportPluginHandler.h"

class  GFxDataExporter;
struct GFxFsCommandOrigin;
struct GFxFsCommand;


struct GFxExportImageDescr
{
    GImageInfo*         pImageInfo;
    GString             ImageName;
    GString             ExportName;
    GString             SwfName;
    UInt32              CharacterId;
    UInt                TargetWidth;
    UInt                TargetHeight;
    UInt16              Format;

    GRect<SInt>         Rect;               // Area of BaseImageID used for this resource
    UInt32              BaseImageId;        // CharacterID of packed texture
    bool                IsPackedImage;
};

struct GFxImageExporterParams
{
    enum ImageExportType
    {
        IET_Image,
        IET_Gradient,
        IET_Font
    };

    UInt32                      CharacterId;
    const GFxDataExporter*      pExporter;
    GPtr<GImage>                pImage;
    GFxExportImageDescr*        pImageDescr;
    GPtr<GFile>                 pFile;
    GDXTHelper::RescaleTypes    Rescale;
    ImageExportType             ExpType;

    inline GFxImageExporterParams() 
    { Set(0, 0, 0, 0, 0, IET_Image); }
    inline GFxImageExporterParams(UInt32 charId, const GFxDataExporter* pexp, GFxExportImageDescr* pimgDescr, GImage* pimage, GFile* pfile = 0, ImageExportType expType = IET_Image) 
    { Set(charId, pexp, pimgDescr, pimage, pfile, expType); }

    void Set(UInt32 charId, const GFxDataExporter* pexp, GFxExportImageDescr* pimgDescr, GImage* pimage, GFile* pfile = 0, ImageExportType expType = IET_Image);
    void Clear() { pImage = 0; }
};

class GFxExporterBase
{
public:
    virtual ~GFxExporterBase() {};

    // Makes and returns full name of the file with extension (w/o path)
    virtual GString   MakeFileName(const GString& nameNoExt)
    {
        return nameNoExt + "." + GetDefaultExtension();
    }
    // Makes and returns full path to the file
    virtual GString   MakePathAndAddExt(const GString& path, const GString& nameNoExt)
    {
        return path + nameNoExt + "." + GetDefaultExtension();
    }
    // Makes and returns full path to the file
    virtual GString   MakePath(const GString& path, const GString& nameWithExt)
    {
        return path + nameWithExt;
    }
    // Returns default extension of the file
    virtual const char* GetDefaultExtension() const = 0;

    virtual void Clear() {}

};
// Interface for plug-in image exporters.
// Each implementation of this interface should also create and store the instance
// of GFxImageExporterParams. This instance should be accessible at least by InitParams() 
// and Write() methods.
class GFxImageExporter : public GFxExporterBase
{
public:
    virtual ~GFxImageExporter() {}

    // Returns short name of format, like "TGA", "DDS", etc
    virtual const char* GetName() const         = 0;
    // Returns description that will be displayed on help screen
    virtual const char* GetDescription() const  = 0;
    // Returns format id. Should be one of the GFxFileConstants::Image_<> constant,
    // or beyond GFxFileConstants::Image_NextAvail
    virtual int         GetFormatId() const     = 0;

    // Initializes and returns GFxImageExporterParams structure
    virtual GFxImageExporterParams& InitParams
        (UInt32 charId, const GFxDataExporter* pexp, GFxExportImageDescr* pimgDescr, GImage* pimage, GFile* pfile = 0, 
         GFxImageExporterParams::ImageExportType expType = GFxImageExporterParams::IET_Image) = 0;

    // Returns true, if image could be rescaled, if -rescale option is used
    virtual bool        MightBeRescaled() const = 0;
    // Returns true, if image need to be converted from the pimage->Format to
    // destFormat.
    virtual bool        NeedToBeConverted(GImage* pimage, int destFormat) const = 0;
    
    // Write image to the disk.
    virtual bool        Write(GDXTHelper* pdxtHepler) = 0;

    // Read file and create GImage. This method used for sharing images. If this method
    // is not implemented (returns NULL) then images of this format cannot be shared
    virtual GImage*     Read(const char* filePath) { GUNUSED(filePath); return NULL; }
};

struct GFxExportSoundAttrs
{
    UInt16           Format;
    UInt32           SampleRate;
    UInt16           Bits;
    UInt16           Channels;
    UInt32           SampleCount;
};

#ifndef GFC_NO_SOUND
struct GFxExportSoundDescr
{
    GSoundInfo*    pSoundInfo;
    GString        SoundName;
    GString        ExportName;
    GString        SwfName;
    GFxExportSoundAttrs Attrs;
    UInt             StartFrame;
    UInt             LastFrame;
};

class GFxSoundExporter : public GFxExporterBase
{
public:
    virtual ~GFxSoundExporter() {}

    virtual bool WriteWaveFile(GSoundData*,            GFile* dest, GFxExportSoundAttrs* ) = 0;
    virtual bool WriteWaveFile(GAppendableSoundData* , GFile* dest, GFxExportSoundAttrs* ) = 0;
};
#endif

class GFxDataExporter
{
    class ExportLoader : public GFxLoader
    {
    public:
        bool DoesNeedTag(int tagType) const
        {
            return (tagType <= 1 || GFxLoader::CheckTagLoader(tagType));
        }
    };

    void                DumpFsCommandsAsTree(FILE* fout, GArray<GFxFsCommandOrigin>& fscommands, 
                                             GArray<GString>& stringHolderArray, int indent);
    void                MakeFsCommandsAsList(GArray<GFxFsCommandOrigin>& fscommands, 
                                             GArray<GString>& stringHolderArray, 
                                             GArray<GString>& cmdList,
                                             GArray<int>& cmdSortedIdx);
    void                LookForFsCommandsInTags(GFxMovieDef* pmovieDef, 
                                                GFile* pin, UInt finalOffset, 
                                                GArray<GFxFsCommandOrigin>& fscommands, 
                                                GArray<GString>& stringHolderArray);

public:
    FxArgs                                  Arguments; 
    GDXTHelper                              DXTHelper;
    GFxExportPluginHandler                  PluginHandler;
    GPtr<GFxMovieDef>                       pMovieDef;
    GFxMovieInfo                            MovieInfo;
    ExportLoader                            Loader;
    GHash<int, GFxImageExporter*>           ImageExporters;
    GHash<UPInt, GArray<GString> >        SharedImagesHash;
    GFxStringHash<int>                      SharedImageNamesSet;

    GPtr<GFxImageSubstProvider>             pImgSubstProvider;

    struct GFxFontTextureDescr
    {
        struct FontDescr
        {
            int             FontId;
            GPtr<GFxFontResource>   pFont;
            struct Pair
            {
                int IndexInFont;
                int IndexInTexture;

                inline Pair() : IndexInFont(0), IndexInTexture(0) {}
                inline Pair(int i1, int i2) : IndexInFont(i1), IndexInTexture(i2) {}
            };
            GArray<Pair> TexGlyphsIndicesMap; // map index of tex glyphs (index_of_tex_glyph_in_font <=> (index in texture))
        };
        GPtr<GImageInfoBase>    pTexture;
        GArray<FontDescr>  Fonts;
        GArray<GPtr<GFxTextureGlyph> > TextureGlyphs;
    };

    struct GFxGradientTextureDescr
    {
        UInt32         MasterId;
        GArray<UInt32> SlaveIds;
    };

    struct GFxFontTextureCounter : public GRefCountBase<GFxFontTextureCounter, GStat_Default_Mem>
    {
        UInt Counter;

        GFxFontTextureCounter(UInt c):Counter(c) {}
    };
    
    typedef GHashIdentity<UInt32, GFxFontTextureDescr>                FontTexturesHashType;
    typedef GHashIdentity<GImageInfoBase*, GFxGradientTextureDescr>   GradientTexturesHashType;
    typedef GHashIdentity<UInt32, GFxExportImageDescr>                ExportImagesHashType;

    typedef GHashIdentity<UInt32, GPtr<GFxFontTextureCounter> >       FontTextureCounterHashType; // TexId -> Counter
    typedef GHashIdentity<UInt16, FontTextureCounterHashType >        FontTextureUseHashType; // FontId -> Texture Ids + UseCnt


    FontTexturesHashType        FontTextures;
    FontTextureUseHashType      FontTextureUse;
    GradientTexturesHashType    GradientTextures;

    GArray<GString>             FileList; // list of all generated files
    bool                        GenFileList;
    GString                     FileListName;

    // Options
    GArray<GString>             SrcFileNames;
    GString                     OutputRootDir;
    GString                     OutputGfxDir;
    GString                     ImgSubstDir;
    GString                     Prefix;
    GString                     ImagesFormat;   // TGA, DDS....
    GString                     SoundsFormat;   // WAV, ...
    bool                        ReplaceImages;
    bool                        ExportSounds;
    bool                        Info;           //Information only, files are not written 
    bool                        DoCompress;
    bool                        DoStrip;
    bool                        DoCreateSubDirs;
    bool                        UncompressedDDS;
    bool                        ToUppercase;
    bool                        ToLowercase;
    bool                        NoExportNames;
    bool                        DXT1Allowed;    // for non-alpha RGB data
    int                         DXTn;           // 3 or 5
    bool                        GenMipMapLevels;
    bool                        NoFontMipMaps;
    bool                        Quiet;
    bool                        QuietProgress;
    GDXTHelper::QualitySetting  Quality;
    GDXTHelper::RescaleTypes    Rescale;
    GDXTHelper::RescaleFilterTypes     RescaleFilter;
    GDXTHelper::MipFilterTypes     MipFilter;
    bool                        JustStripImages;
    bool                        ShareImages;
    // Edit Textfields 
    bool                        ExportDefaultEditText;
    bool                        DefaultDynamicTextOnly;
    // FS Commands
    bool                        ExportFsCommands;
    bool                        FsCommandsAsTree;
    bool                        FsCommandsAsList;
    bool                        FsCommandsParams;
    // font options
    bool                        ExportFonts;
    bool                        ExportFontList;
    bool                        GlyphsStripped;
    int                         TextureGlyphNominalSize;
    int                         TextureGlyphPadPixels;
    int                         FontTextureWidth;
    int                         FontTextureHeight;
    GFxLoader::FileFormatType   FontImagesFormat;   // TGA, DDS
    UByte                       FontImagesBits;     // 8, 24 or 32
    bool                        UseSeparateFontTextures;
    bool                        CompactFonts;
    UInt                        FontNormalizedSize;
    bool                        FontMergeEdges;

    bool                        ExportAS3code;

    // image packer
    bool                        PackImages;
    UInt                        PackImageSize;
    GFxImagePackParamsBase::SizeOptionType    PackImageResize;
    GPtr<GFxImagePackParams>    pPackerParams;

    // gradient options
    bool                        ExportGradients;
    UInt                        GradientSize;
    GFxLoader::FileFormatType   GradientImagesFormat;   // TGA, DDS
    UByte                       GradientImagesBits;     // 32 or 0 (for compressed DDS)
    GArray<GString>             SharedGradDirs;
    GString                     GradientsOutputDir;

    ExportImagesHashType        ImageDescriptors;
    GArray<GFxTagInfo>          TagsToBeCopied, TagsToBeRemoved, TagsWithActions, TagsActions3;
    GArray<GFxTagInfo>          ActionTags;
    struct JpegDesc
    {
        int             TagType;
        UByte*          pData;
        UInt            DataSize;

        inline JpegDesc() : pData(0) {}
    };
    GHash<int,JpegDesc>         JpegDescriptors;
    UInt32                      FirstTagOffset; 
    int                         TotalImageCount;
    int                         ImagesWrittenCount;
    int                         ImagesProcessedCount;
    int                         SharedImagesCount;
    UInt32                      TotalMemoryCount; //used for Info option
    GArray<int>                 GradientShareMap; // used for sharing gradients. Index in this array is
                                                  // gradient id & 0xFFFF, value - id of first identical 
                                                  // gradient; or 0, means this gradient is unique one.
#ifndef GFC_NO_SOUND
    typedef GHashIdentity<UInt32, GFxExportSoundDescr>                ExportSoundsHashType;

    int                         TotalSoundCount;
    int                         SoundsWrittenCount;
    int                         SoundsProcessedCount;
    int                         StreamSoundsWrittenCount;
    int                         StreamSoundsProcessedCount;

    struct StreamSoundData
    {
        bool                    IsHeadTagSet;
        GFxTagInfo              DefSpriteTag;
        GFxTagInfo              HeadTag;
        GArray<GFxTagInfo>      BlockTags;
    };
    GArray<StreamSoundData>     StreamSoundsData;

    GAutoPtr<GFxSoundExporter>  pSoundExporter;
    ExportSoundsHashType        SoundDescriptors;
#endif

    class Log : public GFxLog
    {
    public:
        // We override this function in order to do custom logging.
        virtual void    LogMessageVarg(LogMessageType messageType, const char* pfmt, va_list argList)
        {
            GUNUSED(messageType);
            // Output log to console
            vprintf(pfmt, argList);
        }
    };

    static void         WriteTagHeader(GFile* pout, UInt tag, UInt len);

    static GFile*       FileOpener(const char* url);

    // Removes the leading directory path
    static GString    CutPath(const GString& filename);
    // Removes the trailing filename extension
    static GString    CutExtension(const GString& filename);

    void LoadTagProgressCallback(const GFxProgressHandler::TagInfo& info, bool calledFromDefSprite);
    void ProgressCallback(const GFxProgressHandler::Info& info);

    friend struct ImageExtractorVisitor;
    class ImageExtractorVisitor : public GFxMovieDef::ResourceVisitor
    {
    public:
        GFxDataExporter& Exporter;
        GString        Name;
        GString        DestPath;

        ImageExtractorVisitor(GFxDataExporter& exporter, const GString& name, const GString& destPath) :
        Exporter(exporter), Name(name), DestPath(destPath) { }

        virtual void    Visit(GFxMovieDef* pmovieDef, GFxResource* presource,
                              GFxResourceId rid, const char* pexportName);

        inline ImageExtractorVisitor& operator=(const ImageExtractorVisitor&) { return *this; } // dummy
    };
    void    ExtractImage(GImageInfoBase* pimageInfoBase, GFxResourceId rid, const char* pswfName, 
                         const char* pdestPath, const char* pexportName, GFxImageResource* presource = NULL);

#ifndef GFC_NO_SOUND
    friend struct SoundExtractorVisitor;
    class SoundExtractorVisitor : public GFxMovieDef::ResourceVisitor
    {
    public:
        GFxDataExporter& Exporter;
        GString        Name;
        GString        DestPath;

        SoundExtractorVisitor(GFxDataExporter& exporter, const GString& name, const GString& destPath) :
        Exporter(exporter), Name(name), DestPath(destPath) { }

        virtual void    Visit(GFxMovieDef* pmovieDef, GFxResource* presource,
                              GFxResourceId rid, const char* pexportName);

        inline SoundExtractorVisitor& operator=(const SoundExtractorVisitor&) { return *this; } // dummy
    };
    void    ExtractSound(GSoundInfoBase* psoundData, GFxResourceId rid, const char* pswfName, 
                         const char* pdestPath, const char* pexportName);

    friend struct SpriteSoundExtractorVisitor;
    class SpriteSoundExtractorVisitor : public GFxMovieDef::ResourceVisitor
    {
    public:
        GFxDataExporter& Exporter;
        GString        Name;
        GString        DestPath;

        SpriteSoundExtractorVisitor(GFxDataExporter& exporter, const GString& name, const GString& destPath) :
        Exporter(exporter), Name(name), DestPath(destPath) { }

        virtual void    Visit(GFxMovieDef* pmovieDef, GFxResource* presource,
                              GFxResourceId rid, const char* pexportName);

        inline SpriteSoundExtractorVisitor& operator=(const SpriteSoundExtractorVisitor&) { return *this; } // dummy
    };
    void    ExtractSpriteSound(GFxSoundStreamDef* psoundData, GFxResourceId rid, const char* pswfName, 
                               const char* pdestPath, const char* pexportName);
#endif

    int     Load(const GString& fileName);

    int     GetImageFormatId() const;

    void    WriteStrippedSwf(const char* srcFileName, const char* dstFileName, const GString& name);
    void    WriteExternalImageTag(GFile* pout, UInt32 characterId, const GFxExportImageDescr& imgDescr);
    void    WriteSubImageTag(GFile* pout, UInt32 characterId, UInt32 baseCharId, const GRect<SInt>& rect);
    void    WriteExternalGradientImageTag(GFile* pout, UInt32 characterId, const GFxExportImageDescr& imgDescr);
    void    WriteGlyphTextureInfo(GFile* pout, UInt textureId, const GFxFontTextureDescr& textureDescr);

    void    WriteDefaultEditTextFieldText(GFxMovieDef* pmovieDef, const char* swfFileName, const GString& path, const GString& name);

    void    WriteFontList(GFxMovieDef* pmovieDef, const char* swfFileName, const GString& path, const GString& name);

    void    WriteListOfFiles(const GString& path, const GString& name);

    void    WriteActions3(GFxMovieDef* pmovieDef, const char* swfFileName, const GString& path);

#ifndef  GFC_NO_SOUND

    void    WriteExternalSoundTag(GFile* pout, UInt32 characterId, const GFxExportSoundDescr& sndDescr);
    //void    WriteStreamSoundFile(GSoundData* psound, const char* pdestPath, const char* name, GFxExportSoundAttrs* sndAttrs, GString* soundName);
    void    WriteStreamSoundTag(GFile* pout, const GFxExportSoundDescr& sndDescr);
    UInt    GetStreamSoundTagLen(const GFxExportSoundDescr& sndDescr);
#endif

    enum FsCommandsMasks
    {
        FSTree = 1,
        FSList = 2
    };
    void    WriteFsCommands(GFxMovieDef* pmovieDef, const char* swfFileName, const GString& path, const GString& name, UInt mask);
    bool FindIdenticalImageInDir(const GString& imagePath, const GString& filePath, GString* presultImagePath, GFxExportImageDescr* pdescr, GFxImageExporter* pimageExporter);
    bool FindIdenticalImage(const GString& imagePath, const GString& filePath, GString* presultImagePath, GFxExportImageDescr* pdescr,  GFxImageExporter* pimageExporter);
public:

    GFxDataExporter(UByte verboseFlags = 0);
    ~GFxDataExporter();

    bool    CreateDestDirectory(const char* path);

    int     Process();
    void    ParseFilename(const char* pfilename);
    int     ParseCommandLine(int argc, char* argv[]);

    int     CollectOriginalImageData(const char* srcFileName);
    void    ClearOriginalImageData();

    void                AddImageExporter(int imgFormatId, GFxImageExporter* pimgExp);
    GFxImageExporter*   GetImageExporter(int imgFormatId);

#ifndef  GFC_NO_SOUND
    void                SetSoundExporter(GFxSoundExporter*);
    GFxSoundExporter*   GetSoundExporter();
#endif

    void                ShowHelpScreen();

    // static methods
    static void         ShowVersion();
};

struct GFxImageExporterFactoryBase
{
    virtual ~GFxImageExporterFactoryBase() { }
    virtual GFxImageExporter* Create() = 0;
};

#define GFX_MAX_NUM_OF_IMAGE_EXPORTERS  10
extern GFxImageExporterFactoryBase* GFxImageExporterFactoriesArray[];
extern unsigned                     GFxImageExporterFactoriesArrayCount; 

#define GFX_REGISTER_IMAGE_EXPORTER(exporterClass) \
struct exporterClass##_Factory : public GFxImageExporterFactoryBase \
{   \
    GFxImageExporter* Create() { return new exporterClass(); } \
    exporterClass##_Factory() { GFxImageExporterFactoriesArray[GFxImageExporterFactoriesArrayCount++] = this; } \
} __instance_of_##exporterClass##_factory

#ifndef  GFC_NO_SOUND
GFxSoundExporter* CreateSoundExporter();
#endif

#endif //INC_GFXEXPORT_H
