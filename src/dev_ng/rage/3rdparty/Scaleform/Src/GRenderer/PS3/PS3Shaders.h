enum VertexShaderType2
{
    VS2_None = 0,
    VS2_start_shadows,
    VS2_VVatc = 1,
    VS2_end_shadows = 1,
    VS2_start_blurs,
    VS2_end_blurs = 1,
    VS2_start_cmatrix,
    VS2_end_cmatrix = 1,
    VS2_Count,

};

enum VertexShader2Uniform
{
    VSU_mvp,
    VSU_Count
};

enum FragShaderType2
{
    FS2_None = 0,
    FS2_start_shadows,
    FS2_FBox2InnerShadow = 1,
    FS2_FBox2InnerShadowHighlight,
    FS2_FBox2InnerShadowMul,
    FS2_FBox2InnerShadowMulHighlight,
    FS2_FBox2InnerShadowKnockout,
    FS2_FBox2InnerShadowHighlightKnockout,
    FS2_FBox2InnerShadowMulKnockout,
    FS2_FBox2InnerShadowMulHighlightKnockout,
    FS2_FBox2Shadow,
    FS2_FBox2ShadowHighlight,
    FS2_FBox2ShadowMul,
    FS2_FBox2ShadowMulHighlight,
    FS2_FBox2ShadowKnockout,
    FS2_FBox2ShadowHighlightKnockout,
    FS2_FBox2ShadowMulKnockout,
    FS2_FBox2ShadowMulHighlightKnockout,
    FS2_FBox2Shadowonly,
    FS2_FBox2ShadowonlyHighlight,
    FS2_FBox2ShadowonlyMul,
    FS2_FBox2ShadowonlyMulHighlight,
    FS2_end_shadows = 20,
    FS2_start_blurs,
    FS2_FBox1Blur = 21,
    FS2_FBox2Blur,
    FS2_FBox1BlurMul,
    FS2_FBox2BlurMul,
    FS2_end_blurs = 24,
    FS2_start_cmatrix,
    FS2_FCMatrix = 25,
    FS2_FCMatrixMul,
    FS2_end_cmatrix = 26,
    FS2_Count,

    FS2_shadows_Highlight            = 0x00000001,
    FS2_shadows_Mul                  = 0x00000002,
    FS2_shadows_Knockout             = 0x00000004,
    FS2_blurs_Box2                 = 0x00000001,
    FS2_blurs_Mul                  = 0x00000002,
    FS2_cmatrix_Mul                  = 0x00000001,
};

enum FragShader2Uniform
{
    FSU_cxadd,
    FSU_cxmul,
    FSU_fsize,
    FSU_offset,
    FSU_scolor,
    FSU_scolor2,
    FSU_srctex,
    FSU_srctexscale,
    FSU_tex,
    FSU_texscale,
    FSU_Count
};

static VertexShaderType2 VShaderForFShader[FS2_Count] = {
    VS2_None,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
};

extern int _binary_VVatc_vpo_start;
extern int _binary_FBox2InnerShadow_fpo_start;
extern int _binary_FBox2InnerShadowHighlight_fpo_start;
extern int _binary_FBox2InnerShadowMul_fpo_start;
extern int _binary_FBox2InnerShadowMulHighlight_fpo_start;
extern int _binary_FBox2InnerShadowKnockout_fpo_start;
extern int _binary_FBox2InnerShadowHighlightKnockout_fpo_start;
extern int _binary_FBox2InnerShadowMulKnockout_fpo_start;
extern int _binary_FBox2InnerShadowMulHighlightKnockout_fpo_start;
extern int _binary_FBox2Shadow_fpo_start;
extern int _binary_FBox2ShadowHighlight_fpo_start;
extern int _binary_FBox2ShadowMul_fpo_start;
extern int _binary_FBox2ShadowMulHighlight_fpo_start;
extern int _binary_FBox2ShadowKnockout_fpo_start;
extern int _binary_FBox2ShadowHighlightKnockout_fpo_start;
extern int _binary_FBox2ShadowMulKnockout_fpo_start;
extern int _binary_FBox2ShadowMulHighlightKnockout_fpo_start;
extern int _binary_FBox2Shadowonly_fpo_start;
extern int _binary_FBox2ShadowonlyHighlight_fpo_start;
extern int _binary_FBox2ShadowonlyMul_fpo_start;
extern int _binary_FBox2ShadowonlyMulHighlight_fpo_start;
extern int _binary_FBox1Blur_fpo_start;
extern int _binary_FBox2Blur_fpo_start;
extern int _binary_FBox1BlurMul_fpo_start;
extern int _binary_FBox2BlurMul_fpo_start;
extern int _binary_FCMatrix_fpo_start;
extern int _binary_FCMatrixMul_fpo_start;

static void PS3InitShaders(GRendererPS3Impl* pRenderer);
