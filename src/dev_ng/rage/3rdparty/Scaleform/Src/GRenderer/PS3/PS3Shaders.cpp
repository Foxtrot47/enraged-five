static void PS3InitShaders(GRendererPS3Impl* pRenderer)
{
    pRenderer->InitVertexShader(VS2_VVatc, &_binary_VVatc_vpo_start);
    pRenderer->InitFragShader(FS2_FBox2InnerShadow, &_binary_FBox2InnerShadow_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2InnerShadowHighlight, &_binary_FBox2InnerShadowHighlight_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2InnerShadowMul, &_binary_FBox2InnerShadowMul_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2InnerShadowMulHighlight, &_binary_FBox2InnerShadowMulHighlight_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2InnerShadowKnockout, &_binary_FBox2InnerShadowKnockout_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2InnerShadowHighlightKnockout, &_binary_FBox2InnerShadowHighlightKnockout_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2InnerShadowMulKnockout, &_binary_FBox2InnerShadowMulKnockout_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2InnerShadowMulHighlightKnockout, &_binary_FBox2InnerShadowMulHighlightKnockout_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2Shadow, &_binary_FBox2Shadow_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowHighlight, &_binary_FBox2ShadowHighlight_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowMul, &_binary_FBox2ShadowMul_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowMulHighlight, &_binary_FBox2ShadowMulHighlight_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowKnockout, &_binary_FBox2ShadowKnockout_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowHighlightKnockout, &_binary_FBox2ShadowHighlightKnockout_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowMulKnockout, &_binary_FBox2ShadowMulKnockout_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowMulHighlightKnockout, &_binary_FBox2ShadowMulHighlightKnockout_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2Shadowonly, &_binary_FBox2Shadowonly_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowonlyHighlight, &_binary_FBox2ShadowonlyHighlight_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowonlyMul, &_binary_FBox2ShadowonlyMul_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2ShadowonlyMulHighlight, &_binary_FBox2ShadowonlyMulHighlight_fpo_start);
    pRenderer->InitFragShader(FS2_FBox1Blur, &_binary_FBox1Blur_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2Blur, &_binary_FBox2Blur_fpo_start);
    pRenderer->InitFragShader(FS2_FBox1BlurMul, &_binary_FBox1BlurMul_fpo_start);
    pRenderer->InitFragShader(FS2_FBox2BlurMul, &_binary_FBox2BlurMul_fpo_start);
    pRenderer->InitFragShader(FS2_FCMatrix, &_binary_FCMatrix_fpo_start);
    pRenderer->InitFragShader(FS2_FCMatrixMul, &_binary_FCMatrixMul_fpo_start);
}
