namespace D3D10 { 

const char* pSource_FBox2InnerShadow = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.SampleLevel(srctex_s,  tc * srctexscale, 0.0f);\n"
"        fcolor = lerp(scolor, base, fcolor.a) * base.a;\n"
"	    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2InnerShadowHighlight = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.SampleLevel(srctex_s,  tc * srctexscale, 0.0f);\n"
"      fcolor.ar = saturate((1.0 - fcolor.ar) - (1.0 - fcolor.ra) * 0.5f);\n"
"    fcolor = (scolor * fcolor.a + scolor2 * fcolor.r\n"
"              + base * (1.0 - fcolor.a - fcolor.r)) * base.a;\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2InnerShadowHighlightKnockout = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.SampleLevel(srctex_s,  tc * srctexscale, 0.0f);\n"
"      fcolor.ar = saturate((1.0 - fcolor.ar) - (1.0 - fcolor.ra) * 0.5f);\n"
"    fcolor = (scolor * fcolor.a + scolor2 * fcolor.r\n"
"              + base * (1.0 - fcolor.a - fcolor.r)) * base.a;\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2InnerShadowKnockout = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.SampleLevel(srctex_s,  tc * srctexscale, 0.0f);\n"
"        fcolor = scolor * (1.0 - fcolor.a) * base.a;\n"
"	    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2InnerShadowMul = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.SampleLevel(srctex_s,  tc * srctexscale, 0.0f);\n"
"        fcolor = lerp(scolor, base, fcolor.a) * base.a;\n"
"	    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2InnerShadowMulHighlight = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.SampleLevel(srctex_s,  tc * srctexscale, 0.0f);\n"
"      fcolor.ar = saturate((1.0 - fcolor.ar) - (1.0 - fcolor.ra) * 0.5f);\n"
"    fcolor = (scolor * fcolor.a + scolor2 * fcolor.r\n"
"              + base * (1.0 - fcolor.a - fcolor.r)) * base.a;\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2InnerShadowMulHighlightKnockout = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.SampleLevel(srctex_s,  tc * srctexscale, 0.0f);\n"
"      fcolor.ar = saturate((1.0 - fcolor.ar) - (1.0 - fcolor.ra) * 0.5f);\n"
"    fcolor = (scolor * fcolor.a + scolor2 * fcolor.r\n"
"              + base * (1.0 - fcolor.a - fcolor.r)) * base.a;\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2InnerShadowMulKnockout = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.SampleLevel(srctex_s,  tc * srctexscale, 0.0f);\n"
"        fcolor = scolor * (1.0 - fcolor.a) * base.a;\n"
"	    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2Shadow = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.Sample(srctex_s,  tc * srctexscale);\n"
"\n"
"      fcolor = scolor * fcolor.a * (1.0-base.a) + base;\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2ShadowHighlight = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.Sample(srctex_s,  tc * srctexscale);\n"
"\n"
"      fcolor = (scolor * fcolor.a + scolor2 * fcolor.r) * (1.0-base.a) + base;\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2ShadowHighlightKnockout = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.Sample(srctex_s,  tc * srctexscale);\n"
"\n"
"      fcolor = (scolor * fcolor.a + scolor2 * fcolor.r) * (1.0-base.a) + base;\n"
"      fcolor *= (1.0 - base.a);\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2ShadowKnockout = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.Sample(srctex_s,  tc * srctexscale);\n"
"\n"
"      fcolor = scolor * fcolor.a * (1.0-base.a) + base;\n"
"      fcolor *= (1.0 - base.a);\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2ShadowMul = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.Sample(srctex_s,  tc * srctexscale);\n"
"\n"
"      fcolor = scolor * fcolor.a * (1.0-base.a) + base;\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2ShadowMulHighlight = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.Sample(srctex_s,  tc * srctexscale);\n"
"\n"
"      fcolor = (scolor * fcolor.a + scolor2 * fcolor.r) * (1.0-base.a) + base;\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2ShadowMulHighlightKnockout = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.Sample(srctex_s,  tc * srctexscale);\n"
"\n"
"      fcolor = (scolor * fcolor.a + scolor2 * fcolor.r) * (1.0-base.a) + base;\n"
"      fcolor *= (1.0 - base.a);\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2ShadowMulKnockout = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float4 scolor2;\n"
"    float2 srctexscale;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D srctex : register(t0);\n"
"sampler   srctex_s : register(s0);\n"
"Texture2D tex : register(t1);\n"
"sampler   tex_s : register(s1);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  float4 base = srctex.Sample(srctex_s,  tc * srctexscale);\n"
"\n"
"      fcolor = scolor * fcolor.a * (1.0-base.a) + base;\n"
"      fcolor *= (1.0 - base.a);\n"
"    fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2Shadowonly = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  fcolor = scolor * fcolor.a;\n"
"  fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2ShadowonlyHighlight = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  fcolor = scolor * fcolor.a;\n"
"  fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2ShadowonlyMul = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f);\n"
"      }  fcolor = color * fsize.w;\n"
"  fcolor = scolor * fcolor.a;\n"
"  fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2ShadowonlyMulHighlight = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 offset;\n"
"    float4 scolor;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"  {           color.a += tex.SampleLevel(tex_s,  tc + (offset + i) * texscale, 0.0f).a;\n"
"       color.r += tex.SampleLevel(tex_s,  tc - (offset + i) * texscale, 0.0f).a;\n"
"      }  fcolor = color * fsize.w;\n"
"  fcolor = scolor * fcolor.a;\n"
"  fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_VVatc = 
"cbuffer VSConstants {\n"
"    float4x4 mvp;\n"
"}\n"
"void main(float4 pos : POSITION,\n"
"          float2 atc : TEXCOORD0,\n"
"          out float2 tc : TEXCOORD0,\n"
"          out float4 vpos : SV_Position) {\n"
"  tc = atc;\n"
"  vpos = mul(pos,mvp);\n"
"}";

const char* pSource_FBox1Blur = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float  i = 0.0f;\n"
"  for (i = -fsize.x; i <= fsize.x; i++)\n"
"    color += tex.SampleLevel(tex_s,  tc + i * texscale, 0.0f);\n"
"  fcolor = color * fsize.w;\n"
"  fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox1BlurMul = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float  i = 0.0f;\n"
"  for (i = -fsize.x; i <= fsize.x; i++)\n"
"    color += tex.SampleLevel(tex_s,  tc + i * texscale, 0.0f);\n"
"  fcolor = color * fsize.w;\n"
"  fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FBox2Blur = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"    color += tex.SampleLevel(tex_s,  tc + i * texscale, 0.0f);\n"
"  fcolor = color * fsize.w;\n"
"  fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"}";

const char* pSource_FBox2BlurMul = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4 cxmul;\n"
"    float4 fsize;\n"
"    float2 texscale;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 color = 0.0f;\n"
"  float2 i = 0.0f;\n"
"  for (i.x = -fsize.x; i.x <= fsize.x; i.x++)\n"
"    for (i.y = -fsize.y; i.y <= fsize.y; i.y++)\n"
"    color += tex.SampleLevel(tex_s,  tc + i * texscale, 0.0f);\n"
"  fcolor = color * fsize.w;\n"
"  fcolor = fcolor * cxmul + cxadd * fcolor.a;\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

const char* pSource_FCMatrix = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4x4 cxmul;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 c = tex.Sample(tex_s,  tc);\n"
"  fcolor = mul(c,cxmul) + cxadd * (c.a + cxadd.a);\n"
"}";

const char* pSource_FCMatrixMul = 
"cbuffer PSConstants {\n"
"    float4 cxadd;\n"
"    float4x4 cxmul;\n"
"}\n"
"Texture2D tex : register(t0);\n"
"sampler   tex_s : register(s0);\n"
"void main(float2 tc : TEXCOORD0,\n"
"          out float4 fcolor : SV_Target) {\n"
"  float4 c = tex.Sample(tex_s,  tc);\n"
"  fcolor = mul(c,cxmul) + cxadd * (c.a + cxadd.a);\n"
"  fcolor = lerp(1, fcolor, fcolor.a);\n"
"}";

enum VertexShaderType2
{
    VS2_None = 0,
    VS2_start_shadows,
    VS2_VVatc = 1,
    VS2_end_shadows = 1,
    VS2_start_blurs,
    VS2_end_blurs = 1,
    VS2_start_cmatrix,
    VS2_end_cmatrix = 1,
    VS2_Count,

};

static const char* VShaderSources2[VS2_Count] = {
    NULL,
    pSource_VVatc,
};

enum VertexShader2Uniform
{
    VSU_mvp,
    VSU_Count
};

static const int VUniforms_VVatc[] = {0};

static const int* VShaderUniforms[VS2_Count] = {
    NULL,
    VUniforms_VVatc,
};

enum FragShaderType2
{
    FS2_None = 0,
    FS2_start_shadows,
    FS2_FBox2InnerShadow = 1,
    FS2_FBox2InnerShadowHighlight,
    FS2_FBox2InnerShadowMul,
    FS2_FBox2InnerShadowMulHighlight,
    FS2_FBox2InnerShadowKnockout,
    FS2_FBox2InnerShadowHighlightKnockout,
    FS2_FBox2InnerShadowMulKnockout,
    FS2_FBox2InnerShadowMulHighlightKnockout,
    FS2_FBox2Shadow,
    FS2_FBox2ShadowHighlight,
    FS2_FBox2ShadowMul,
    FS2_FBox2ShadowMulHighlight,
    FS2_FBox2ShadowKnockout,
    FS2_FBox2ShadowHighlightKnockout,
    FS2_FBox2ShadowMulKnockout,
    FS2_FBox2ShadowMulHighlightKnockout,
    FS2_FBox2Shadowonly,
    FS2_FBox2ShadowonlyHighlight,
    FS2_FBox2ShadowonlyMul,
    FS2_FBox2ShadowonlyMulHighlight,
    FS2_end_shadows = 20,
    FS2_start_blurs,
    FS2_FBox1Blur = 21,
    FS2_FBox2Blur,
    FS2_FBox1BlurMul,
    FS2_FBox2BlurMul,
    FS2_end_blurs = 24,
    FS2_start_cmatrix,
    FS2_FCMatrix = 25,
    FS2_FCMatrixMul,
    FS2_end_cmatrix = 26,
    FS2_Count,

    FS2_shadows_Highlight            = 0x00000001,
    FS2_shadows_Mul                  = 0x00000002,
    FS2_shadows_Knockout             = 0x00000004,
    FS2_blurs_Box2                 = 0x00000001,
    FS2_blurs_Mul                  = 0x00000002,
    FS2_cmatrix_Mul                  = 0x00000001,
};

static const char* FShaderSources2[FS2_Count] = {
    NULL,
    pSource_FBox2InnerShadow,
    pSource_FBox2InnerShadowHighlight,
    pSource_FBox2InnerShadowMul,
    pSource_FBox2InnerShadowMulHighlight,
    pSource_FBox2InnerShadowKnockout,
    pSource_FBox2InnerShadowHighlightKnockout,
    pSource_FBox2InnerShadowMulKnockout,
    pSource_FBox2InnerShadowMulHighlightKnockout,
    pSource_FBox2Shadow,
    pSource_FBox2ShadowHighlight,
    pSource_FBox2ShadowMul,
    pSource_FBox2ShadowMulHighlight,
    pSource_FBox2ShadowKnockout,
    pSource_FBox2ShadowHighlightKnockout,
    pSource_FBox2ShadowMulKnockout,
    pSource_FBox2ShadowMulHighlightKnockout,
    pSource_FBox2Shadowonly,
    pSource_FBox2ShadowonlyHighlight,
    pSource_FBox2ShadowonlyMul,
    pSource_FBox2ShadowonlyMulHighlight,
    pSource_FBox1Blur,
    pSource_FBox2Blur,
    pSource_FBox1BlurMul,
    pSource_FBox2BlurMul,
    pSource_FCMatrix,
    pSource_FCMatrixMul,
};

enum FragShader2Uniform
{
    FSU_cxadd,
    FSU_cxmul,
    FSU_fsize,
    FSU_offset,
    FSU_scolor,
    FSU_scolor2,
    FSU_srctex,
    FSU_srctexscale,
    FSU_tex,
    FSU_texscale,
    FSU_Count
};

static const int FUniforms_FBox2InnerShadow[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2InnerShadowHighlight[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2InnerShadowMul[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2InnerShadowMulHighlight[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2InnerShadowKnockout[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2InnerShadowHighlightKnockout[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2InnerShadowMulKnockout[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2InnerShadowMulHighlightKnockout[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2Shadow[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2ShadowHighlight[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2ShadowMul[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2ShadowMulHighlight[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2ShadowKnockout[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2ShadowHighlightKnockout[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2ShadowMulKnockout[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2ShadowMulHighlightKnockout[] = {0, 4, 8, 12, 16, 20, 0, 24, 1, 26};
static const int FUniforms_FBox2Shadowonly[] = {0, 4, 8, 12, 16, -1, -1, -1, 0, 20};
static const int FUniforms_FBox2ShadowonlyHighlight[] = {0, 4, 8, 12, 16, -1, -1, -1, 0, 20};
static const int FUniforms_FBox2ShadowonlyMul[] = {0, 4, 8, 12, 16, -1, -1, -1, 0, 20};
static const int FUniforms_FBox2ShadowonlyMulHighlight[] = {0, 4, 8, 12, 16, -1, -1, -1, 0, 20};
static const int FUniforms_FBox1Blur[] = {0, 4, 8, -1, -1, -1, -1, -1, 0, 12};
static const int FUniforms_FBox2Blur[] = {0, 4, 8, -1, -1, -1, -1, -1, 0, 12};
static const int FUniforms_FBox1BlurMul[] = {0, 4, 8, -1, -1, -1, -1, -1, 0, 12};
static const int FUniforms_FBox2BlurMul[] = {0, 4, 8, -1, -1, -1, -1, -1, 0, 12};
static const int FUniforms_FCMatrix[] = {0, 4, -1, -1, -1, -1, -1, -1, 0, -1};
static const int FUniforms_FCMatrixMul[] = {0, 4, -1, -1, -1, -1, -1, -1, 0, -1};

static const int* FShaderUniforms[FS2_Count] = {
    NULL,
    FUniforms_FBox2InnerShadow,
    FUniforms_FBox2InnerShadowHighlight,
    FUniforms_FBox2InnerShadowMul,
    FUniforms_FBox2InnerShadowMulHighlight,
    FUniforms_FBox2InnerShadowKnockout,
    FUniforms_FBox2InnerShadowHighlightKnockout,
    FUniforms_FBox2InnerShadowMulKnockout,
    FUniforms_FBox2InnerShadowMulHighlightKnockout,
    FUniforms_FBox2Shadow,
    FUniforms_FBox2ShadowHighlight,
    FUniforms_FBox2ShadowMul,
    FUniforms_FBox2ShadowMulHighlight,
    FUniforms_FBox2ShadowKnockout,
    FUniforms_FBox2ShadowHighlightKnockout,
    FUniforms_FBox2ShadowMulKnockout,
    FUniforms_FBox2ShadowMulHighlightKnockout,
    FUniforms_FBox2Shadowonly,
    FUniforms_FBox2ShadowonlyHighlight,
    FUniforms_FBox2ShadowonlyMul,
    FUniforms_FBox2ShadowonlyMulHighlight,
    FUniforms_FBox1Blur,
    FUniforms_FBox2Blur,
    FUniforms_FBox1BlurMul,
    FUniforms_FBox2BlurMul,
    FUniforms_FCMatrix,
    FUniforms_FCMatrixMul,
};

static VertexShaderType2 VShaderForFShader[FS2_Count] = {
    VS2_None,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
};

} // namespace D3D10
