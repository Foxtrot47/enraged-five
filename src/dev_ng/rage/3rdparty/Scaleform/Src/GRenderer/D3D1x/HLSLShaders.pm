
package Shaders::D3D10;

@ISA = qw/Shaders/;

sub new { bless {} }

sub PlatformNames { qw/D3D10 D3D11/ }

sub Caps { 'Ureg' }

sub VarSize
{
	my $s = 1, $align = 1;
	if ($_[1]->{type} =~ /mat|x/) { $s = 16; $align = 4; }
	elsif ($_[1]->{type} =~ /([0-9])/) { $s = $1; $align = $1 > 2 ? 4 : $1; }
	return ($s * ($_[1]->{dim} ? $_[1]->{dim} : 1), $align);
}

sub GetSem
{
  my ($vt,$vn,$vs) = ($_[0]->{type},$_[0]->{name},$_[0]->{sem});
  if ($vs eq '') {
    if ($vn =~ /^a?tc([0-9]*)/) { $vs = 'TEXCOORD'.(0+$1); }
    elsif ($vn eq 'color') { $vs = 'COLOR0'; }
    elsif ($vn eq 'factor') { $vs = 'COLOR1'; }
    elsif ($vn =~ '^v?pos') { $vs = 'POSITION'; }
  }
  if (lc($vs) eq 'position' && $_[0]->{vary} eq 'varying') { $vs = 'SV_Position'; }
  elsif (!$vs =~ /^SV_/) { $vs = uc($vs); }
  return $vs;
}

sub Param
{
  $_ = $_[1];

  if ($_->{type} =~ /^sampler(.*)$/) {
    my $s = "Texture$1 $_->{name} : register(t\$texn);\nsampler   $_->{name}_s : register(s\$texn)".($_[2]?';':'');
	$_->{uofs} = $texn;
    return $s, 2, {qw/texn $$texn++/};
  }

  my ($vt,$vn,$vs) = ($_->{type},$_->{name},GetSem($_));
  $vt =~ s/vec/float/g;
  if (exists $_->{dim}) {
    $vn .= "[$_->{dim}]";
  }

  if ($_[0]->{stage} eq 'v' && $_->{vary} eq 'varying') { $vt = "out $vt"; }
  elsif ($_[0]->{stage} eq 'f' && $_->{vary} eq 'fragout') { $vt = "out $vt"; }
  my $s = "$vt $vn";
  $s.= " : $vs" if $vs;

  $s .= (($_->{vary} eq 'uniform') ? ';' : ',') if $_[2];

  return $s, (($_->{vary} eq 'uniform') ? 1 : 0);
}

sub ParamSort
{
  my $c = $Shaders::VarySort{$_[1]->{vary}} <=> $Shaders::VarySort{$_[2]->{vary}};
  return $c if ($c);
  my $as = GetSem($_[1]).$_[1]->{name}; $as =~ s/^SV_/zzzSV_/;
  my $bs = GetSem($_[2]).$_[2]->{name}; $bs =~ s/^SV_/zzzSV_/;
  return $as cmp $bs;
}

sub ParamCategory
{
  if ($_->{type} =~ /^sampler(.*)$/) {
    return 2;
  }
  elsif ($_[1]->{vary} eq 'uniform') {
    return 1;
  }
  else {
    return 0;
  }
}

sub BuiltinParams
{
  return ($Shaders::stage eq 'f' ? ({vary => 'fragout', type => 'float4', name => 'fcolor', sem => 'SV_Target'}) : ());
}

sub BeginUniforms { ("cbuffer ".($_[0]->{stage} eq 'f' ? 'P' : 'V')."SConstants {\n",'    '); }
sub EndUniforms { "}\n" }

sub Block
{
  my ($this, $stage, $argsr, $b) = @_;
  my %args = %{$argsr};

  #$b = Shaders::BlockD3D($this, $stage, $argsr, $b);
  Shaders::ParamD3D($b);
  $b =~ s/(^|\W)mix(\W)/\1lerp\2/g;
  $b =~ s/(^|\W)texture([1-3]D\W)/\1tex\2/g;
  $b =~ s/(^|\W)(?:float|half|int)[1-4] *\(([^,()]*)\)/\1\2/g;

  $b =~ s/tex(?:ture)?2D *\((\w+) *,(.*)\)/$1.Sample(\1_s, $2)/g;
  $b =~ s/tex(?:ture)?2DLod *\((\w+) *,(.*)\)/$1.SampleLevel(\1_s, $2)/g;

  $b =~ s/float([234]) *\($Shaders::subexpr\)/"float$1(".join(',',($2)x$1).")"/ge;

  return $b;
}

sub FileExt
{
  return 'hlsl';
}

new();
