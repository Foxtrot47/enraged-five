/**********************************************************************

Filename    :   GFxXMLExpat.h
Content     :   XML Parser implementation using expat
Created     :   March 6, 2008
Authors     :   Prasad Silva
Copyright   :   (c) 2005-2008 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#ifndef INC_GFxXMLExpat_H
#define INC_GFxXMLExpat_H

#include "GConfig.h"

#ifndef GFC_NO_XML_SUPPORT

#include <GFxXMLParser.h>


//
// This is an implementation of the SAX2 pluggable parser interface 
// using the expat XML parser library found at http://expat.sourceforge.net/.
// It has been verified to support Win32, Win64, Linux, OSX, PS2, PS3, 
// PSP, XBOX360 and Wii platforms. 
//
// The FxPlayer included in GFx distributions install an instance of 
// the expat XML parser delegate as the GFxLoader parser state. This 
// exposes functionality to parse XML files for the application and 
// loaded GFx movies.
//

//
// Expat XML parser instance
//
//
class GFxXMLParserExpat : public GFxXMLParser
{
public:
    GFxXMLParserExpat() {}

    //
    // Implementation of GFxXMLParser
    //
    bool            ParseFile(const char* pfilename, GFxFileOpenerBase* pfo, 
                              GFxXMLParserHandler* pparseHandler);
    bool            ParseString(const char* pdata, UPInt len, 
                                GFxXMLParserHandler* pparseHandler);
};


#endif  // #ifndef GFC_NO_XML_SUPPORT

#endif  // #define INC_GFxXMLExpat_H
