/**********************************************************************

Filename    :   GFxSound.h
Content     :   SWF (Shockwave Flash) player library
Created     :   September, 2008
Authors     :   Maxim Didenko

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#include "GFxSoundResource.h"
#ifndef GFC_NO_SOUND

#include "GFxLoader.h"
#include "GFxPlayerStats.h"
#include "GHeapNew.h"
#include "GFxMovieDef.h"
#include "GFile.h"

// ***** GFxSoundFileKeyData implementation


class GFxSoundFileInfoKeyData : public GRefCountBase<GFxSoundFileInfoKeyData,GFxStatMV_Other_Mem>
{
    // Image States.  
    GPtr<GFxFileOpener>     pFileOpener;

public:
    // Key File Info - provides file name and image dimensions.
    // Note that this key data is potentially different from the ResourceFileInfo
    // specified in the export because its filename could have been translated
    // and/or adjusted to have a different relative/absolute path.
    GPtr<GFxSoundFileInfo>  pFileInfo;

    GFxSoundFileInfoKeyData(GFxSoundFileInfo* pfileInfo, GFxFileOpener* pfileOpener)
    {        
        pFileInfo   = pfileInfo;
        pFileOpener = pfileOpener;
    }

    bool operator == (GFxSoundFileInfoKeyData& other) const
    {
        return (pFileOpener == other.pFileOpener &&
                pFileInfo->FileName == other.pFileInfo->FileName);
    }
    bool operator != (GFxSoundFileInfoKeyData& other) const
    {
        return !operator == (other);
    }

    UPInt  GetHashCode() const
    {
        return pFileInfo->GetHashCode() ^
            ((UPInt)pFileOpener.GetPtr()) ^ (((UPInt)pFileOpener.GetPtr()) >> 7);
    }
};

class GFxSoundFileKeyInterface : public GFxResourceKey::KeyInterface
{
public:
    typedef GFxResourceKey::KeyHandle KeyHandle;    

    // GFxResourceKey::KeyInterface implementation.    
    virtual void                AddRef(KeyHandle hdata);  
    virtual void                Release(KeyHandle hdata);
    virtual GFxResourceKey::KeyType GetKeyType(KeyHandle hdata) const;
    virtual UPInt               GetHashCode(KeyHandle hdata) const;
    virtual bool                KeyEquals(KeyHandle hdata, const GFxResourceKey& other);
    // const  GFxResourceFileInfo* GetFileInfo(KeyHandle hdata) const;

    const  char* GetFileURL(KeyHandle hdata) const;
};

static GFxSoundFileKeyInterface GFxSoundFileKeyInterface_Instance;


// Reference counting on the data object, if necessary.
void    GFxSoundFileKeyInterface::AddRef(KeyHandle hdata)
{
    GFxSoundFileInfoKeyData* pdata = (GFxSoundFileInfoKeyData*) hdata;
    GASSERT(pdata);
    pdata->AddRef();
}

void    GFxSoundFileKeyInterface::Release(KeyHandle hdata)
{
    GFxSoundFileInfoKeyData* pdata = (GFxSoundFileInfoKeyData*) hdata;
    GASSERT(pdata);
    pdata->Release();
}

// Key/Hash code implementation.
GFxResourceKey::KeyType GFxSoundFileKeyInterface::GetKeyType(KeyHandle hdata) const
{
    GUNUSED(hdata);
    return GFxResourceKey::Key_File;
}

UPInt  GFxSoundFileKeyInterface::GetHashCode(KeyHandle hdata) const
{
    GFxSoundFileInfoKeyData* pdata = (GFxSoundFileInfoKeyData*) hdata;
    return pdata->GetHashCode();    
}

bool    GFxSoundFileKeyInterface::KeyEquals(KeyHandle hdata, const GFxResourceKey& other)
{
    if (this != other.GetKeyInterface())
        return 0;

    GFxSoundFileInfoKeyData* pthisData = (GFxSoundFileInfoKeyData*) hdata;
    GFxSoundFileInfoKeyData* potherData = (GFxSoundFileInfoKeyData*) other.GetKeyData();
    GASSERT(pthisData);
    GASSERT(potherData);
    return (*pthisData == *potherData);
}


const char* GFxSoundFileKeyInterface::GetFileURL(KeyHandle hdata) const
{
    GFxSoundFileInfoKeyData* pdata = (GFxSoundFileInfoKeyData*) hdata;
    GASSERT(pdata);
    return pdata->pFileInfo->FileName.ToCStr();
}


// ***** GFxImageResource

GFxResourceKey  GFxSoundResource::CreateSoundFileKey(GFxSoundFileInfo* pfileInfo,
                                                     GFxFileOpener* pfileOpener)
{
    GPtr<GFxSoundFileInfoKeyData> pdata =
        *new GFxSoundFileInfoKeyData(pfileInfo, pfileOpener);

    return GFxResourceKey(&GFxSoundFileKeyInterface_Instance,
        (GFxResourceKey::KeyHandle)pdata.GetPtr() );
}



//////////////////////////////////////////////////////////////////////////


// Creates/Loads resource based on GSoundData during binding process
bool    GFxSoundResourceCreator::CreateResource(DataHandle hdata, GFxResourceBindData *pbindData,
                                                GFxLoadStates *pls, GMemoryHeap *pbindHeap) const
{
    GUNUSED(pls);
    GUNUSED(pbindHeap); // Don't use bindHeap for shared resources.

    GSoundData* psound = (GSoundData*) hdata;
    GASSERT(psound);
    if (!pbindData->pResource)
    {
        // Our GFxMovieDefImpl's pBinding should have been provided by caller.
        //GPtr<GSoundInfo> sinfo = *GHEAP_NEW(pbindHeap) GSoundInfo(psound);
        //pbindData->pResource = *GHEAP_NEW(pbindHeap) GFxSoundResource(sinfo);
        GPtr<GSoundInfo> sinfo = *GNEW GSoundInfo(psound);
        pbindData->pResource = *GNEW GFxSoundResource(sinfo);
    }

    return pbindData->pResource ? 1 : 0;
}

//static
GFxResourceData GFxSoundResourceCreator::CreateSoundResourceData(GSoundData* psound)
{
    static GFxSoundResourceCreator static_inst;
    return GFxResourceData(&static_inst, psound);
}

// Creates/Loads resource based on data and loading process
bool GFxSoundFileResourceCreator::CreateResource(DataHandle hdata, GFxResourceBindData *pbindData,
                                                 GFxLoadStates *pls, GMemoryHeap *pbindHeap) const
{
    GFxSoundFileInfo *prfi = (GFxSoundFileInfo*) hdata;
    GASSERT(prfi);
    GUNUSED(pbindHeap); // Don't use bindHeap for shared resources.


    // Make a new fileInfo so that it can have a modified filename.   
    GPtr<GFxSoundFileInfo> psoundFileInfo = *GNEW GFxSoundFileInfo(*prfi);

    // Translate filename.
    GFxURLBuilder::LocationInfo loc(GFxURLBuilder::File_Sound,
                                        prfi->FileName, pls->GetRelativePath());
    pls->BuildURL(&psoundFileInfo->FileName, loc);
    // Now that we have a correct file object we need to match it against
    // the library so as to check if is is already loaded
    GFxResourceKey soundKey = GFxSoundResource::CreateSoundFileKey(psoundFileInfo, pls->GetFileOpener());

    GString                     errorMessage;
    GFxResourceLib::BindHandle  bh;
    GPtr<GFxSoundResource>      psoundRes = 0;

    if (pls->GetLib()->BindResourceKey(&bh, soundKey) == GFxResourceLib::RS_NeedsResolve)
    {

        //UInt sf = (prfi->Bits == 0 ? GSoundData::Sample_8 : GSoundData::Sample_16) |
        //    (prfi->Channels == 1 ? GSoundData::Sample_Stereo : 0);

        GPtr<GFile> pf = *pls->GetFileOpener()->OpenFile(psoundFileInfo->FileName);
        if (pf && pf->IsValid())
        {
            //GPtr<GSoundData> psoundData = 
                //*new GSoundData(sf,prfi->SampleRate, prfi->SampleCount, pf->GetLength());
            GPtr<GSoundFile> psoundData = 
                *new GSoundFile(psoundFileInfo->FileName,prfi->SampleRate, prfi->SampleCount,false);
            psoundData->SetSeekSample(prfi->SeekSample);
            //pf->Read(psoundData->GetData(),psoundData->GetDataSize());
            GPtr<GSoundInfo> sinfo = *GNEW GSoundInfo(psoundData);
            psoundRes = *GNEW GFxSoundResource(sinfo, soundKey);
        }

        if (psoundRes)
            bh.ResolveResource(psoundRes);
        else
        {
            errorMessage = "Failed to load sound '";
            errorMessage += psoundFileInfo->FileName;
            errorMessage += "'";

            bh.CancelResolve(errorMessage.ToCStr());
        }
    }
    else
    {
        // If Available and Waiting resources will be resolved here.
        if ((psoundRes = *(GFxSoundResource*)bh.WaitForResolve()).GetPtr() == 0)
        {
            errorMessage = bh.GetResolveError();
        }
    }

    // If there was an error, display it
    if (!psoundRes)
    {
        pls->pLog->LogError("Error: %s\n", errorMessage.ToCStr());
        return 0;
    }

    // Pass resource ownership to BindData.
    pbindData->pResource = psoundRes;
    return 1;    
}

//static 
GFxResourceData GFxSoundFileResourceCreator::CreateSoundFileResourceData(GFxSoundFileInfo * prfi)
{
    static GFxSoundFileResourceCreator static_inst;
    return GFxResourceData(&static_inst, prfi);
}


#endif // GFC_NO_SOUND
