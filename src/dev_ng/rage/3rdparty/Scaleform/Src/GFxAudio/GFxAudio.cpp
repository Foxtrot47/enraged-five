/**********************************************************************

Filename    :   GFxAudio.h
Content     :   

Created     :   September 2008
Authors     :   Maxim Didenko

Copyright   :   (c) 2001-2007 Scaleform Corp. All Rights Reserved.

Notes       :   

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#include "GFxAudio.h"
#ifndef GFC_NO_SOUND

#include "GFxAction.h"
#include "GASSoundObject.h"
#include "GFxSoundTagsReader.h"


GFxAudio::GFxAudio(GSoundRenderer* pplayer, Float maxTimeDiffernce, 
                             UInt checkFrameInterval, SyncTypeFlags syncType)
: GFxAudioBase(maxTimeDiffernce,checkFrameInterval,syncType),pPlayer(pplayer), pTagsReader(NULL)
{
    pTagsReader = new GFxSoundTagsReader;
}

GFxAudio::~GFxAudio()
{
    if (pTagsReader)
        pTagsReader->Release();
}

GSoundRenderer* GFxAudio::GetRenderer() const 
{ 
    return pPlayer.GetPtr(); 
}

GFxSoundTagsReader* GFxAudio::GetSoundTagsReader() const
{
    return pTagsReader;
}

void GFxAudio::RegisterASClasses(GASGlobalContext& gc, GASStringContext& sc)
{
    gc.AddBuiltinClassRegistry<GASBuiltin_Sound, GASSoundCtorFunction>(sc, gc.pGlobal);
}

#endif // GFC_NO_SOUND
