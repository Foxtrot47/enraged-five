/**********************************************************************

Filename    :   GFxSoundTagsReader.h
Content     :   SWF (Shockwave Flash) player library
Created     :   February, 2009
Authors     :   

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#ifndef INC_GFXSOUNDTAGSREADER_H
#define INC_GFXSOUNDTAGSREADER_H

#include "GConfig.h"
#ifndef GFC_NO_SOUND

#include "GFxStream.h"

class GFxLoadProcess;
class GFxButtonSoundDef;

class GFxSoundTagsReader : public GRefCountBaseNTS<GFxSoundTagsReader,GFxStatMD_Sounds_Mem>
{
public:

    virtual GFxButtonSoundDef* ReadButtonSoundDef(GFxLoadProcess* p);
    virtual void               ReadDefineSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo);
    virtual void               ReadStartSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo);
    virtual void               ReadButtonSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo);
    virtual void               ReadDefineExternalSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo);
    virtual void               ReadDefineExternalStreamSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo);
    virtual void               ReadSoundStreamHeadTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo);
    virtual void               ReadSoundStreamBlockTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo);

};
#endif // GFC_NO_SOUND


#endif // INC_GFXSOUNDTAGSREADER_H

