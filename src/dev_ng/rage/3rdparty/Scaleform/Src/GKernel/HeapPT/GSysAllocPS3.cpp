/**********************************************************************

Filename    :   GSysAllocPS3.cpp
Content     :   PS3 system allocator
Created     :   2009
Authors     :   Vlad Merker

Notes       :   PS3 system allocator that uses sys_memory_* functions

Copyright   :   (c) 1998-2009 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#include <sys/memory.h>
#include "GSysAllocPS3.h"

//------------------------------------------------------------------------
GSysAllocPS3::GSysAllocPS3(UPInt granularity)
{
    GASSERT(granularity);
    Granularity = SysAlignment = (granularity < MaxPageSize ?
        MinPageSize :
        MaxPageSize);
    Footprint = 0;
}

//------------------------------------------------------------------------
void GSysAllocPS3::GetInfo(Info* i) const
{
    i->MinAlign    = i->MaxAlign = SysAlignment;
    i->Granularity = Granularity;
    i->HasRealloc  = false;
}

//------------------------------------------------------------------------
void* GSysAllocPS3::Alloc(UPInt size, UPInt)
{
    size = (size + SysAlignment - 1) & ~(SysAlignment - 1);

    sys_addr_t addr = 0;
    uint64_t flags = (Granularity == MaxPageSize ?
        SYS_MEMORY_PAGE_SIZE_1M :
        SYS_MEMORY_PAGE_SIZE_64K);

    int ret = sys_memory_allocate(size, flags, &addr);
    GASSERT(ret == CELL_OK);
    GUNUSED(ret);
    if(addr)
        Footprint += size;

    return (void *)addr;
}

//------------------------------------------------------------------------
bool GSysAllocPS3::Free(void* ptr, UPInt size, UPInt)
{
    int ret = sys_memory_free((sys_addr_t)ptr);
    GASSERT(ret == CELL_OK);
    GUNUSED(ret);
    Footprint -= size;

    return true;
}
