#ifndef _PORTCULLIS_H_
#define _PORTCULLIS_H_

#if __WIN32PC
#define PORTCULLIS_ENABLE 0	// Change to 1 to turn Portcullis on
#else
#define PORTCULLIS_ENABLE 0
#endif

#if PORTCULLIS_ENABLE

#define SINGLE_THREAD_PORTCULLIS 0

#include "system/ipc.h"
#include "system/threadpool.h"
#include "net/net.h"


namespace rage
{
	//////////////////////////////////////////////////////////////////////////
	class portcullisWorkItem : public sysThreadPool::WorkItem
	{
	public:
		portcullisWorkItem()
			: m_checkDone(false)
			, m_checkStarted(false)
			, m_result(true)
		{}
		virtual ~portcullisWorkItem(){}

		virtual void DoWork();

		void	Started()				{	m_checkStarted = true;	}

		bool	CheckDone() const		{	return m_checkDone;		}
		bool	CheckStarted() const	{	return m_checkStarted;	}
		bool	CheckSuccessful()		{	m_checkDone = m_checkStarted = false;	return m_result;		}

	private:
		bool	m_checkDone;
		bool	m_checkStarted;
		bool	m_result;
	};


	//////////////////////////////////////////////////////////////////////////
	class portcullis
	{
	public:
		static void				Initialise();

		static void 			DoHeartbeat();

		static void 			Shutdown();

		static void				ThreadFunc(void*);

		static void				DoCheck(portcullisWorkItem* workItem);
		static bool				DoCheck();

	private:
		static bool				m_initialised;

#if SINGLE_THREAD_PORTCULLIS
		static bool				m_closeThread;

		static sysIpcThreadId	m_threadID;
		static sysIpcSema		m_triggerCheck;
		static sysIpcSema		m_threadClosed;
#endif

		static bool				m_checkInProgress;
		static bool				m_checkSuccess;

		static int				m_failTolerance;
		static netRandom		m_random;
		
	};
}

#endif // PORTCULLIS_ENABLE


#endif // _PORTCULLIS_H_
