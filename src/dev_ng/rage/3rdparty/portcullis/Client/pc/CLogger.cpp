//-------------------
// CLogger.cpp
// Jason Hughes
//-------------------
#if __WIN32PC

#include "First.h"
#include "CLogger.h"

#include <stdarg.h>
#include <vector>

//-------------------
// Different platforms require different interfaces for maximum error checking.
#if AXPLATFORM==AXPLATFORM_Windows || AXPLATFORM_Windows64
#elif AXPLATFORM==AXPLATFORM_Wii
	#include <revolution/os.h>
#else
	#error "What to do about unknown platforms?"
#endif

//-------------------

static uint32          const kMaxPrintTargets = 10;

// These are the list of callbacks we must send printed strings to.  There's no way to remove a target, only add.
// This is sort of by design, because it's a logging interface for warnings and errors.  That's it.
static uint32                sTopPrintTargetIndex = 0;
static Logger::PrintCallback sPrintTargets[kMaxPrintTargets];

// this is allocated inside the executable.  Used only for the Sprintf function.
static uint            const kErrBufferSize = 1024;
static char                  sBuffer[kErrBufferSize];  

// this is used for the errors that throw, so they have a place to put the string.  In case sprintf is 
// used to form the error, we made this buffer separate.
static char                  sErrMsg[kErrBufferSize];  
static char                  sThrowMsg[kErrBufferSize];  // ONLY throws use this buffer

// we use this critical section to make sure we don't all clobber each other with multiple threads
// HOWEVER, since it's a complex type with a constructor, we have to be careful that it gets initialized in
// Logger::Initialize only, and never again.
static bool                  sLoggerInitialized = false;

//-------------------
// This is the default logging target, which is added automatically as the first target.
static void PrintToStdout(char const *msg, Logger::Severity sev)
{
#if AXPLATFORM==AXPLATFORM_Windows || AXPLATFORM_Windows64
	if (sev==Logger::kL_Error)
	{
		fprintf(stderr, "*** Error: %s\n", msg);
		fflush(stdout);	
		fflush(stderr);		
	}		
	else if (sev==Logger::kL_Warning)
	{
		fprintf(stderr, "Warning: %s\n", msg);
		fflush(stdout);	
		fflush(stderr);			
	}
	else
	{
		fprintf(stdout, "%s", msg);
	}
#elif AXPLATFORM==AXPLATFORM_Wii
	if (sev==Logger::kL_Error)
	{
#if !defined(_SHIPPING)  			
		OSReport("*** Error: %s\n", msg);
#endif
		GXColor fg,bg;
		fg.r = fg.g = fg.b = fg.a = 255;
		bg.r = bg.g = bg.b = bg.a = 0;
		DBREAKPOINT;
		OSFatal(fg, bg, msg);
	}		
	else if (sev==Logger::kL_Warning)
	{
#if !defined(_SHIPPING)  			
		OSReport("Warning: %s\n", msg);
#endif
	}
	else
	{
#if !defined(_SHIPPING)  			
		OSReport("%s", msg);
#endif
	}
#else
	#error "What to do about other platforms?"
#endif
}

//-------------------

static void PrintfToTargets(char const *msg, Logger::Severity sev)
{
	// run through all targets
	for (uint loop=0; loop<sTopPrintTargetIndex; loop++)
	{
		(*sPrintTargets[loop])(msg, sev);  // dispatch
	}	
}

//-------------------
// This can be called as a result of AxMem or AxThread doing some error checking.  So it's likely the system automatically starts on its own,
// so I support that.
void Logger::Initialize(void)
{
	if (!sLoggerInitialized)
	{
		sLoggerInitialized = true;
	
		if (sTopPrintTargetIndex!=0)  // if this is non-zero, we know we can call assert, because logger is initialized!
		{
			AssertAlways("Multiple Logger::Initialize calls are illegal.");
		}
	
		// initialize stdout as the first output stream
		if (stdout)
		{
			AddTarget(PrintToStdout);
		}	
	}
}

//-------------------

void Logger::Shutdown(void)
{
	sTopPrintTargetIndex = 0;
	sLoggerInitialized = false;
}

//-------------------

void Logger::LogPrintf(char const *fmt, ...)
{
	if (!sLoggerInitialized) 
		Initialize();
	
	va_list parmlist;
	va_start(parmlist,fmt);
	vsprintf_s(sBuffer, kErrBufferSize, fmt, parmlist);
	va_end(parmlist);
	PrintfToTargets(sBuffer, kL_Print);
}

//-------------------

char *Logger::Sprintf(char const *fmt, ...)
{
	if (!sLoggerInitialized) 
		Initialize();
	
	va_list parmlist;
	va_start(parmlist,fmt);
	vsprintf_s(sBuffer, kErrBufferSize, fmt, parmlist);
	va_end(parmlist);
	return sBuffer;
}

//-------------------

void Logger::AssertAlways(char const *fmt, ...)
{
	if (!sLoggerInitialized) 
		Initialize();
	
	// print to streams
	va_list parmlist;
	va_start(parmlist, fmt);
	vsprintf_s(sErrMsg, kErrBufferSize, fmt, parmlist);
	va_end(parmlist);
	PrintfToTargets(sErrMsg, kL_Error);

	// throw an exception
	memcpy_s(sThrowMsg, kErrBufferSize, sErrMsg, kErrBufferSize);
	throw sThrowMsg;
}

//-------------------

void Logger::DoAssert(bool const condition, char const *fmt, ...) // AC: avoid conflict with Assert
{
	if (condition)
		return;

	if (!sLoggerInitialized) 
		Initialize();
	
	// print to streams
	va_list parmlist;
	va_start(parmlist, fmt);
	vsprintf_s(sErrMsg, kErrBufferSize, fmt, parmlist);
	va_end(parmlist);
	PrintfToTargets(sErrMsg, kL_Error);

	// throw an exception
	memcpy_s(sThrowMsg, kErrBufferSize, sErrMsg, kErrBufferSize);
	throw sThrowMsg;
}

//-------------------

void Logger::DoAssert(bool const condition) // AC: avoid conflict with Assert
{
	if (condition)
		return;

	if (!sLoggerInitialized) 
		Initialize();
	
	throw "anonymous assert";
}

//-------------------

void Logger::Warn(bool const condition, char const *fmt, ...)
{
	if (condition)
		return;

	if (!sLoggerInitialized) 
		Initialize();
	
	// print to streams
	va_list parmlist;
	va_start(parmlist,fmt);
	vsprintf_s(sErrMsg, kErrBufferSize, fmt, parmlist);
	va_end(parmlist);	
	PrintfToTargets(sErrMsg, kL_Warning);
}

//-------------------

void Logger::WarnAlways(char const *fmt, ...)
{
	if (!sLoggerInitialized) 
		Initialize();
	
	// print to streams
	va_list parmlist;
	va_start(parmlist,fmt);
	vsprintf_s(sErrMsg, kErrBufferSize, fmt, parmlist);
	va_end(parmlist);	
	PrintfToTargets(sErrMsg, kL_Warning);
}

//-------------------

void Logger::AddTarget(PrintCallback pcb)
{
	if (!sLoggerInitialized) 
		Initialize();
	
	DASSERT(sTopPrintTargetIndex<kMaxPrintTargets, "Failed to keep print targets under limit (%d).", sTopPrintTargetIndex);

// disable all targets on shipping builds, no matter what
#if !defined(_SHIPPING)  
	sPrintTargets[sTopPrintTargetIndex] = pcb;
	sTopPrintTargetIndex++;
#endif
}

//-------------------

void Logger::RemoveTarget(PrintCallback pcb)
{
	if (!sLoggerInitialized) 
		Initialize();
	
// we disabled all targets on shipping builds
#if !defined(_SHIPPING)  
	for (uint iTarget=0; iTarget<sTopPrintTargetIndex; iTarget++)
	{
		if (sPrintTargets[iTarget]==pcb)
		{
			// copy all remaining targets back one notch
			for (uint iDel=iTarget; iDel<sTopPrintTargetIndex; iDel++)
			{
				sPrintTargets[iDel] = sPrintTargets[iDel+1];
			}
			sTopPrintTargetIndex--;
			return;
		}
	}
	AssertAlways("No such print target found to remove from Logger.");
#endif
}

//-------------------
#endif // __WIN32PC