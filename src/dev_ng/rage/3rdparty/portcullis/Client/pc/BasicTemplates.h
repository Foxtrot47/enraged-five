//-------------------
// BasicTemplates.h
// Jason Hughes
// Flare Development Group
// Copyright 2006
//-------------------

#ifndef BASICTEMPLATES_H
#define BASICTEMPLATES_H

//-------------------
// This is the fanciest unref macro on earth.  Even LINT doesn't complain about it.
template<class T>
void UNREF(T const &)
{
}

//-------------------

template<class T, class U>
T const Min(T const a,U const b)
{
	return ((a<b)?a:b);
}

//-------------------

template<class T, class U, class V>
T const Mid(T const a, U const b, V const c)
{
	return ((a<b)? ((a<c)? ((c<b)? c:b) : a) : ((b<c)? ((c<a)? c:a) : b));
}

//-------------------

template<class T, class U>
T const Max(T const a,U const b)
{
	return ((b<a)?a:b);
}

//-------------------
// really fancy integer version of min/max that does not branch!!!  See http://bob.allegronetwork.com/prog/tricks.html
template <>
inline int32 const Min(int32 const a, int32 const b)
{
	int ret = a - b;
	ret &= ret >> 31;
	return ret + b;
}

template <>
inline int32 const Max(int32 const a, int32 const b)
{
	int ret = a - b;
	ret &= (~ret) >> 31;
	return ret + b;
}

//-------------------

template<class T, class U, class V>
U const Clamp(T const a,U const b,V const c)
{
	DASSERT(a<=c, "Clamp bounds are inverted.");
	return (U)Max(Min(b,c),a);
}

//-------------------

template<class T>
void Swap(T &a, T &b)
{
	T c(a);
	a = b;
	b = c;
}

//-------------------

template<class T>
T const Round(T const a)
{
	return T(floor(a+0.5f));
}

//-------------------
// You can use this to align values to the next evenly divisible modulus, eg. RoundUpToNext(13, 6)==18.  
// This probably only works for positive modulus values.
template <typename T>
T RoundUpToNext(T const value, T const modulus)
{
	T const remainder = value % modulus;
	if (remainder>0)
		return value + modulus - remainder;
	return value;
}

//-------------------

template <>
inline float RoundUpToNext(float const value, float const modulus)
{
	float const remainder = fmodf(value, modulus);
	if (remainder!=0)
		return value + modulus - remainder;
	return value;
}

//-------------------

template <typename T>
T const sqr(T const a)
{
	return a*a;
}

//-------------------

#endif
