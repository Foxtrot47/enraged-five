//-------------------
// First.h
// Jason Hughes
//-------------------

#ifndef FIRST_H
#define FIRST_H

#if __WIN32PC

//-------------------
// Useful Contants
float const gkEpsilon           = 0.0001f;  // this is the precision to which I consider equality/near zero
float const gkEpsilonSqrt       = 0.01f;    // this is same as above, but used when dealing with squared values
float const gkPi                = 3.1415926535897932384626433832795028841971693993751f;
float const gkE                 = 2.71828182845904523536028747135266249775724709369995f;
float const gkDegToRad          = gkPi/180.0f;
float const gkRadToDeg          = 180.0f/gkPi;

//-------------------
// Platform/compiler definitions
//-------------------
#define AXPLATFORM_Unknown   0
#define AXPLATFORM_Windows   1
#define AXPLATFORM_Wii       2
#define AXPLATFORM_X360      3
#define AXPLATFORM_PS3       4
#define AXPLATFORM_Windows64 5

#define AXPLATFORM_LittleEndian 1
#define AXPLATFORM_BigEndian    2

#if defined(__PPCBROADWAY__)
	#define AXPLATFORM AXPLATFORM_Wii
	#define AXENDIAN   AXPLATFORM_BigEndian
#elif defined(_M_IX86)
	#define AXPLATFORM AXPLATFORM_Windows
	#define AXENDIAN   AXPLATFORM_LittleEndian	
#elif defined(_M_X64)
	#define AXPLATFORM AXPLATFORM_Windows64
	#define AXENDIAN   AXPLATFORM_LittleEndian
#else
	#define AXPLATFORM AXPLATFORM_Unknown
	#define AXENDIAN   AXPLATFORM_Unknown
#endif

//-------------------
// safe types to use (portability)
//-------------------
#if AXPLATFORM==AXPLATFORM_Wii
	#include <inttypes.h>
	typedef int64_t                int64;
	typedef int32_t                int32;
	typedef int16_t                int16;
	typedef int8_t                 int8;
	typedef uint32_t               uint;
	typedef uint64_t               uint64;
	typedef uint32_t               uint32;
	typedef uint16_t               uint16;
	typedef uint8_t                uint8;
#elif AXPLATFORM==AXPLATFORM_Windows
	typedef __int64                int64;
	typedef __int32                int32;
	typedef __int16                int16;
	typedef __int8                 int8;
	typedef unsigned __int32       uint;
	typedef unsigned __int64       uint64;
	typedef unsigned __int32       uint32;
	typedef unsigned __int16       uint16;
	typedef unsigned __int8        uint8;
#elif AXPLATFORM==AXPLATFORM_Windows64
	typedef __int64                int64;
	typedef __int32                int32;
	typedef __int16                int16;
	typedef __int8                 int8;
	typedef unsigned __int32       uint;
	typedef unsigned __int64       uint64;
	typedef unsigned __int32       uint32;
	typedef unsigned __int16       uint16;
	typedef unsigned __int8        uint8;	
#else
	#error "What to do with unknown platforms?"
#endif

//-------------------

#if AXPLATFORM==AXPLATFORM_Windows || AXPLATFORM==AXPLATFORM_Windows64
	#pragma warning(disable: 4505)  // unreferenced local function has been removed
	#pragma warning(disable: 4514)  // unreferenced inline function has been removed
	#define FAILED_ERR(x, y) { uint32 const kErrCode = (x); DASSERTU(!FAILED(kErrCode), y); }
	#define RETURN_ADDRESS _ReturnAddress()
	#define PACK_STRUCT
	#define AXFUNCTION_NAME __FUNCTION__ 

	// these must be applied before and after the struct definition, ie. CACHE_ALIGN_PRE struct foo {} CACHE_ALIGN_POST;
	// If used around a static definition, it should look like CACHE_ALIGN_PRE uint8 const bar[] CACHE_ALIGN_POST = {};
	#define CACHE_ALIGN_PRE __declspec(align(32))
	#define CACHE_ALIGN_POST

//	#define SSCANF sscanf_s
#pragma warning(disable: 4668)	// AC: Added to get to compile with warnings as errors
	#include <intrin.h>
	// at least set up windows.h to use XP macros (or more modern)
#ifndef _WIN32_WINNT	
	#define _WIN32_WINNT 0x501	
#endif
	#define NODRAWTEXT
	#include <windows.h>
#pragma warning(error: 4668)

	// macros to convert color values to U32
	#define RGBACOLOR_FROM_U8(r, g, b, a) D3DCOLOR_RGBA(r,g,b,a)
	#define RGBACOLOR_FROM_F32(r, g, b, a) RGBACOLOR_FROM_U8((uint8)(Clamp(0,r*255,255)),(uint8)(Clamp(0,g*255,255)),(uint8)(Clamp(0,b*255,255)),(uint8)(Clamp(0,a*255,255)))	

#elif AXPLATFORM==AXPLATFORM_Wii
	#pragma warning off (10208)  // obnoxious complaints about casting things to void or taking sizeof arguments.
	// Note, PACK_STRUCT is required by GCC to pack correctly in all cases.  
	// Using '#pragma pack' must still be used to make it work on VC++.
	#define PACK_STRUCT __attribute__ ((packed))
	#define AXFUNCTION_NAME __PRETTY_FUNCTION__ 	

	// these must be applied before and after the struct definition, ie. CACHE_ALIGN_PRE struct foo {} CACHE_ALIGN_POST;	
	// If used around a static definition, it should look like CACHE_ALIGN_PRE uint8 const bar[] CACHE_ALIGN_POST = {};
	#define CACHE_ALIGN_PRE
	#define CACHE_ALIGN_POST __attribute__ ((aligned(32)))	
	#define RETURN_ADDRESS NULL
	typedef uint32 HWND;

	// prevent old graphics enumerations from compiling, because they are misleading and wrong
	#define GX_NO_LEGACY_HW1

	// macros to convert color values to U32
	#define RGBACOLOR_FROM_U8(r, g, b, a) (( ((uint32)(r)) << 24) | ( ((uint32)(g)) << 16) | ( ((uint32)(b)) << 8) | (((uint32)(a))))
	#define RGBACOLOR_FROM_F32(r, g, b, a) RGBACOLOR_FROM_U8((uint8)(Clamp(0,r*255,255)),(uint8)(Clamp(0,g*255,255)),(uint8)(Clamp(0,b*255,255)),(uint8)(Clamp(0,a*255,255)))	

#else
	#error "What to do with unknown platforms?"
#endif

//-------------------
// Configuration-specific settings
#ifdef _DEBUG
	#define USE_LOGGER   1
	#define USE_DASSERT  1
#elif defined(_SHIPPING)
	// shipping builds get NO ASSERTS or logging
	#define USE_LOGGER   0
	#define USE_DASSERT  0
#else  
	// release builds only log, no asserts
	#define USE_LOGGER   1
	#define USE_DASSERT  0
#endif

//-------------------
// Configure the print, warn, and assert macros
#if USE_LOGGER
	// this is a shortcut to the logger system, which compiles out entirely in release
	#define DPRINTF(...)                { static bool skip = false; if (!skip) { Logger::LogPrintf(__VA_ARGS__); } }
	#define DWARN(COND, ...)            { static bool skip = false; if (!skip) { Logger::Warn((bool)(COND), __VA_ARGS__); } }
	#define DWARNONCE(COND, ...)        { static bool skip = false; if (!skip) { skip = true; Logger::Warn((bool)(COND), __VA_ARGS__); } }
	#define WARNONCE(COND, ...)         { static bool skip = false; if (!skip) { skip = true; Logger::Warn((bool)(COND), __VA_ARGS__); } }
#else
	#define DPRINTF                     {} sizeof
	#define DWARN                       {} sizeof
	#define DWARNONCE                   {} sizeof
	#define WARNONCE                    {} sizeof
#endif

#if USE_DASSERT
	#define DASSERT(COND, ...)          { static bool skip = false; if (!skip) Logger::DoAssert((bool)(COND), __VA_ARGS__); } // AC: avoid conflict with Assert
	#define DASSERTU(COND, ...)         { static bool skip = false; if (!skip) Logger::DoAssert((bool)(COND), __VA_ARGS__); } // AC: avoid conflict with Assert
#else
	// these completely compile out to nothing, because sizeof(anything, anything else) 
	// becomes a compile-time constant.
	#define DASSERT                     {} sizeof
	#define DASSERTU					{}
#endif

//-------------------
// In Maya plugins, we disable breakpoints, because it kills the plugin.
#if !defined(AX_MAYA_PLUGIN)
	#if defined(_DEBUG)
		#define DBREAKPOINT		{ static bool skip = false; if (!skip && AxPlatform::IsDebuggerPresent()) AxPlatform::Breakpoint(); }	
	#else
		#define DBREAKPOINT
	#endif
#else
	#define DBREAKPOINT	
#endif	

//-------------------

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#pragma warning(disable: 4985)
#include <math.h>
#pragma warning(error: 4985)
#include <time.h>
#include <ctype.h>

//-------------------
// This comes AFTER the macros above are defined.  These includes are in a critical order.
#include "BasicTemplates.h"
#include "CLogger.h"
//#include "AxPlatform.h"

//-------------------

#endif // __WIN32PC

#endif
