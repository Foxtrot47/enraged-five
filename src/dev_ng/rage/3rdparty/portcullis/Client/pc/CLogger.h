//-------------------
// CLogger.h
// Jason Hughes
//-------------------

#ifndef CLOGGER_H
#define CLOGGER_H

#if __WIN32PC
//-------------------

#include <string>

//-------------------

class Logger
{
public:
	enum Severity
	{
		kL_Print,	
		kL_Warning,	
		kL_Error,
	};

	static void Initialize(void);
	static void Shutdown  (void);

	// This is a typical printf, except that it dumps output to every registered file stream.
	// So, you can do file logging or stdout or redirection trivially.
	static void LogPrintf(char const *fmt, ...);		

	// This performs a sprintf to an internal buffer and returns the pointer.  Handy.
	static char *Sprintf(char const *fmt, ...);

	// Assert will throw a 'char const *' exception with the message provided.  In the conditional
	// cases, it will only assert if the condition is FALSE.  In the unconditional case, it throws.
	static void AssertAlways(char const *fmt, ...);  // always assert--for default cases in switches, mainly
	static void DoAssert    (bool const condition, char const *fmt, ...); // AC: avoid conflict with Assert
	static void DoAssert    (bool const condition);  // the beauty of these macros is we can overload them

	// Warnings always print out, but do not throw exceptions.  The conditional
	// call only prints when the conditional is FALSE, like an assert.
	static void WarnAlways(char const *fmt, ...);
	static void WarnOnce  (int const line, char const *file, char const *fmt, ...);
	static void Warn      (bool const condition, char const *fmt, ...);

	// This allows you to add a callback that gets a chance to send printf's somewhere, with appropriate prefixing.
	typedef void (*PrintCallback)(char const *, Severity);
	static void AddTarget   (PrintCallback pcb);
	static void RemoveTarget(PrintCallback pcb);
};

//-------------------
#endif // __WIN32PC

#endif
