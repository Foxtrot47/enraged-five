//-------------------
// Heartbeat.h
// Jason Hughes
//-------------------
#if __WIN32PC
namespace Portcullis
{
	// Call this at the beginning of the game, once.  See integration document for details per platform.
	// For Xbox360 these parameters are used, can be null/false for everything else.
	void Initialize(void (*flipCallback)(), bool useSecurity, char const *sgServer);
	
	// Only call this every minute or so.  This might be a good idea to call from a separate thread, 
	// as it is a completely synchronous operation. Compare the returned string to what you expected 
	// to receive.  If NULL, there was a NETWORK error.  If string is zero length but not NULL, it was
	// a rejected authentication attempt.
	char *SendHeartbeat(char const *gameName);

	// This is never called by game code, only in the launcher.
	char *SendAuth(char const *gameName, char const *auth);
	
	// This completely shuts down the network.  Only call this at the very end of the game.
	void Shutdown(void);
}

//-------------------
#endif // __WIN32PC
