//-------------------
// Portcullis.cpp
// Jason Hughes
//-------------------

#if __WIN32PC

#include "../First.h"
#include "heartbeat.h"

#include "net/nethardware.h"

#pragma warning(disable:4668)
#include <iphlpapi.h> // AC: Not need as we use the rage::nethardware to obtain mac address
#pragma warning(default:4668)
#pragma comment(lib,"Iphlpapi.lib")
#include "curl/curl.h"

//-------------------

static char const *kServerURL   = "https://ts.rockstargames.com/index.php";// Apache checks this on HTTP receipts--it should be whatever the hostname is for the LAMP server
static char const *kPlatform     = "PC";   // this should match a platform name in the database.  Do not change!

int const kVerifyPeer     = 0;  // turning this on is only possible AFTER a proper SSL certificate is on the server.
int const kVerifyHostname = 2;  // 2 means name must match.

static char macaddress[255];
static char userData[255];

//-------------------

// Must define a reader function that appends the resulting fetch data to a string in memory
size_t fromCurl(void *buffer, size_t /*size AC: Unused var*/, size_t nmemb, void *str)
{
	char *s = (char *)str;
	int const initialLength = (int)strlen(s);
	memcpy_s(s + initialLength, 254 - initialLength, buffer, nmemb);  // copy whatever we get for the string
	s[initialLength + nmemb] = 0;  // null terminate it
	return nmemb;
}

//-------------------
// Post something to a web page using cURL, and fetch the resulting page back again.
// postData should look like this: "name=daniel&project=curl";
static char serverResults[255];
static bool PostPage(char const *url, char const *payload)
{
    serverResults[0] = 0;

	CURL *curl;
	CURLcode success;
	std::string pageData;
	curl = curl_easy_init();
	Logger::DoAssert(curl!=NULL, "cURL failed to init"); // AC: avoid conflict with Assert	
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload);

    /*
     * If you want to connect to a site who isn't using a certificate that is
     * signed by one of the certs in the CA bundle you have, you can skip the
     * verification of the server's certificate. This makes the connection
     * A LOT LESS SECURE.
     *
     * If you have a CA cert for the server stored someplace else than in the
     * default bundle, then the CURLOPT_CAPATH option might come handy for
     * you.
     */ 
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, kVerifyPeer);
 
    /*
     * If the site you're connecting to uses a different host name that what
     * they have mentioned in their server certificate's commonName (or
     * subjectAltName) fields, libcurl will refuse to connect. You can skip
     * this check, but this will make the connection less secure.
     */ 
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, kVerifyHostname);

	curl_easy_setopt(curl, CURLOPT_SSLVERSION, 1L);


	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fromCurl); 		
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &serverResults);
	curl_easy_setopt(curl, CURLOPT_URL, url);

	// Enable these lines for fiddler
//    	curl_easy_setopt(curl, CURLOPT_HTTPPROXYTUNNEL, 1);
//    	curl_easy_setopt(curl, CURLOPT_PROXY, "127.0.0.1");
//    	curl_easy_setopt(curl, CURLOPT_PROXYPORT, 8888);

	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

	success = curl_easy_perform(curl);
	curl_easy_cleanup(curl);	
	return (success == CURLE_OK);
}

//-------------------
// Fetches the first MAC address available.
static void GetMACAddress(void)
{
#if 1	// AC: Change to use the rage::nethardware utility function for obtaining the mac address... rage may return a different mac address to the registration tool though.
	IP_ADAPTER_INFO AdapterInfo[16];
	DWORD dwBufLen = sizeof(AdapterInfo);
	DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);
	Logger::DoAssert(dwStatus == ERROR_SUCCESS); // AC: avoid conflict with Assert
	Logger::DoAssert(AdapterInfo[0].AddressLength==6);  // not a MAC address?

	sprintf_s(macaddress, "%02X:%02X:%02X:%02X:%02X:%02X", AdapterInfo[0].Address[0], AdapterInfo[0].Address[1], AdapterInfo[0].Address[2], AdapterInfo[0].Address[3], AdapterInfo[0].Address[4], AdapterInfo[0].Address[5]);
#else
	rage::u8 mac[6];
	rage::netHardware::GetMacAddress(mac);	// Uses 'UuidCreateSequential' to obtain mac address on PC
	sprintf_s(macaddress, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
#endif
}

//-------------------

static void UrlEncode(char const *s, char *d)
{
	// pass 1, convert specific characters to hex
	char *r = d;
	for (uint i=0; s[i]!=0; i++)
	{
		if (s[i]=='+' || s[i]=='=' || s[i]=='&' || s[i]=='%' || s[i]<' ')
		{
			*r = '%';
			r++;
			*r = "0123456789ABCDEF"[s[i] >> 4];  // quick hex encoding
			r++;			
			*r = "0123456789ABCDEF"[s[i] & 15];
			r++;			
		}
		else
		{
			if (s[i]==' ')
				*r = '+';
			else *r = s[i];
			r++;
		}
	}
	*r = 0;  // null terminate string.
}

//-------------------

void Portcullis::Initialize(void (*flipCallback)(), bool useSecurity, char const *sgServer)
{
	UNREF(flipCallback);
	UNREF(useSecurity);
	UNREF(sgServer);

	// test cURL here, and fetch a web page.
	GetMACAddress();
	size_t trash;
	Logger::DoAssert(getenv_s(&trash, userData, 255, "USERNAME")==0 && trash!=0, "Failed to get user name."); // AC: avoid conflict with Assert
	strcat_s(userData, 255-strlen(userData), "_");
	Logger::DoAssert(getenv_s(&trash, userData + strlen(userData), 255 - strlen(userData), "COMPUTERNAME")==0 && trash!=0, "Failed to get computer name.");
}

//-------------------

char *Portcullis::SendHeartbeat(char const *gameName)
{
	char initialArgs[1024];
	char uueUsername[1024];
	char uueGame[255];
	UrlEncode(userData, uueUsername);
	UrlEncode(gameName, uueGame);
	sprintf_s(initialArgs, 1024, "&user=%s&mac=%s&platform=%s&game=%s", uueUsername, macaddress, kPlatform, uueGame);
	serverResults[0] = 0;	
	if (!PostPage(kServerURL, initialArgs))
		return NULL;

	return serverResults;
}

//-------------------

char *Portcullis::SendAuth(char const *gameName, char const *auth)
{
	char initialArgs[1024];
	char uueUsername[1024];
	char uueGame[255];
	UrlEncode(userData, uueUsername);
	UrlEncode(gameName, uueGame);
	sprintf_s(initialArgs, 1024, "&user=%s&mac=%s&platform=%s&game=%s&key=%s", uueUsername, macaddress, kPlatform, uueGame, auth);
	serverResults[0] = 0;
	if (!PostPage(kServerURL, initialArgs))
		return NULL;

	return serverResults;
}

//-------------------

void Portcullis::Shutdown(void)
{
}

//-------------------
#endif // __WIN32PC
