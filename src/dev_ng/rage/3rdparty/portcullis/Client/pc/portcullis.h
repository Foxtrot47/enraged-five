
#ifdef WIN32
#define PORTCULLIS_ENABLE 1
#else
#define PORTCULLIS_ENABLE 0
#endif

#if PORTCULLIS_ENABLE




namespace rage
{
	class portcullis
	{
	public:
		static void	Initialise();

		static void DoHeartbeat();

		static void Shutdown();
	};
}

#endif // PORTCULLIS_ENABLE