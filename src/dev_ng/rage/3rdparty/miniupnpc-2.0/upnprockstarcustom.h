#ifndef UPNPROCKSTARCUSTOM_H_INCLUDED
#define UPNPROCKSTARCUSTOM_H_INCLUDED

#include "file/file_config.h"

#define NET_ENABLE_UPNP ((RSG_PC || RSG_ORBIS || RSG_DURANGO || (0 && RSG_LINUX)) && !__RGSC_DLL && !RSG_MOBILE && !__TOOL && !__RESOURCECOMPILER)

#if NET_ENABLE_UPNP

#include "system/xtl.h"

#ifdef _MSC_VER
#pragma warning(disable:4996)   // 'inet_addr': Use inet_pton() or InetPton() instead or define _WINSOCK_DEPRECATED_NO_WARNINGS to disable deprecated API warnings
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if RSG_ORBIS
typedef int SOCKET;
#define sockaddr_in6 sockaddr_in
typedef struct timeval TIMEVAL;
#define NO_GETADDRINFO (1)
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <net.h>

struct  hostent {
	char h_addr[4];
};

int OrbisCreateNetPool();
unsigned long inet_addr(const char *addr);
struct hostent* gethostbyname(const char *name);
#endif

char* uPnPStrDup(const char* str);

#if !__NO_OUTPUT
void uPnPPrintf(const char *fmt, ...);
#endif //  !__NO_OUTPUT


#ifdef __cplusplus
}
#endif

#endif // NET_ENABLE_UPNP
#endif // UPNPROCKSTARCUSTOM_H_INCLUDED
