/* $Id: igd_desc_parse.c,v 1.17 2015/09/15 13:30:04 nanard Exp $ */
/* Project : miniupnp
 * http://miniupnp.free.fr/
 * Author : Thomas Bernard
 * Copyright (c) 2005-2015 Thomas Bernard
 * This software is subject to the conditions detailed in the
 * LICENCE file provided in this distribution. */

#include "upnprockstarcustom.h"

#if NET_ENABLE_UPNP

#include "igd_desc_parse.h"
#include <stdio.h>
#include <string.h>

/* Start element handler :
 * update nesting level counter and copy element name */
void IGDstartelt(void * d, const char * name, int l)
{
	struct IGDdatas * datas = (struct IGDdatas *)d;
	if(l >= MINIUPNPC_URL_MAXSIZE)
		l = MINIUPNPC_URL_MAXSIZE-1;
	memcpy(datas->cureltname, name, l);
	datas->cureltname[l] = '\0';
	datas->level++;
	if( (l==7) && !memcmp(name, "service", l) ) {
		datas->tmp.controlurl[0] = '\0';
		datas->tmp.eventsuburl[0] = '\0';
		datas->tmp.scpdurl[0] = '\0';
		datas->tmp.servicetype[0] = '\0';
	}
}

#define COMPARE(str, cstr) (0==memcmp(str, cstr, sizeof(cstr) - 1))

/* End element handler :
 * update nesting level counter and update parser state if
 * service element is parsed */
void IGDendelt(void * d, const char * name, int l)
{
	struct IGDdatas * datas = (struct IGDdatas *)d;
	datas->level--;
	/*uPnPPrintf("endelt %2d %.*s\n", datas->level, l, name);*/
	if( (l==7) && !memcmp(name, "service", l) )
	{
		if(COMPARE(datas->tmp.servicetype,
		           "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:")) {
			memcpy(&datas->CIF, &datas->tmp, sizeof(struct IGDdatas_service));
		} else if(COMPARE(datas->tmp.servicetype,
			                "urn:schemas-upnp-org:service:WANIPv6FirewallControl:")) {
			memcpy(&datas->IPv6FC, &datas->tmp, sizeof(struct IGDdatas_service));
		} else if(COMPARE(datas->tmp.servicetype,
		                  "urn:schemas-upnp-org:service:WANIPConnection:")
		         || COMPARE(datas->tmp.servicetype,
		                    "urn:schemas-upnp-org:service:WANPPPConnection:") ) {
			if(datas->first.servicetype[0] == '\0') {
				memcpy(&datas->first, &datas->tmp, sizeof(struct IGDdatas_service));
			} else {
				memcpy(&datas->second, &datas->tmp, sizeof(struct IGDdatas_service));
			}
		}
	}
}

/* Data handler :
 * copy data depending on the current element name and state */
void IGDdata(void * d, const char * data, int l)
{
	struct IGDdatas * datas = (struct IGDdatas *)d;
	char * dstmember = 0;
	/*uPnPPrintf("%2d %s : %.*s\n",
           datas->level, datas->cureltname, l, data);	*/
	if( !strcmp(datas->cureltname, "URLBase") )
		dstmember = datas->urlbase;
	else if( !strcmp(datas->cureltname, "presentationURL") )
		dstmember = datas->presentationurl;
	else if( !strcmp(datas->cureltname, "serviceType") )
		dstmember = datas->tmp.servicetype;
	else if( !strcmp(datas->cureltname, "controlURL") )
		dstmember = datas->tmp.controlurl;
	else if( !strcmp(datas->cureltname, "eventSubURL") )
		dstmember = datas->tmp.eventsuburl;
	else if( !strcmp(datas->cureltname, "SCPDURL") )
		dstmember = datas->tmp.scpdurl;

	// R* CHANGE - added UPnP device strings
	else if((datas->friendlyName[0] == '\0') && !strcmp(datas->cureltname, "friendlyName"))
		dstmember = datas->friendlyName;
	else if((datas->manufacturer[0] == '\0') && !strcmp(datas->cureltname, "manufacturer"))
		dstmember = datas->manufacturer;
	else if((datas->modelName[0] == '\0') && !strcmp(datas->cureltname, "modelName"))
		dstmember = datas->modelName;
	else if((datas->modelNumber[0] == '\0') && !strcmp(datas->cureltname, "modelNumber"))
		dstmember = datas->modelNumber;

/*	else if( !strcmp(datas->cureltname, "deviceType") )
		dstmember = datas->devicetype_tmp;*/
	if(dstmember)
	{
		if(l>=MINIUPNPC_URL_MAXSIZE)
			l = MINIUPNPC_URL_MAXSIZE-1;
		memcpy(dstmember, data, l);
		dstmember[l] = '\0';
	}
}

#if !__NO_OUTPUT
void printIGD(struct IGDdatas * d)
{
	uPnPPrintf("urlbase = '%s'\n", d->urlbase);

	// R* CHANGE - added UPnP device strings
	uPnPPrintf(" friendlyName = '%s'\n", d->friendlyName);
	uPnPPrintf(" manufacturer = '%s'\n", d->manufacturer);
	uPnPPrintf(" modelName = '%s'\n", d->modelName);
	uPnPPrintf(" modelNumber = '%s'\n", d->modelNumber);

	uPnPPrintf("WAN Device (Common interface config) :\n");
	/*uPnPPrintf(" deviceType = '%s'\n", d->CIF.devicetype);*/
	uPnPPrintf(" serviceType = '%s'\n", d->CIF.servicetype);
	uPnPPrintf(" controlURL = '%s'\n", d->CIF.controlurl);
	uPnPPrintf(" eventSubURL = '%s'\n", d->CIF.eventsuburl);
	uPnPPrintf(" SCPDURL = '%s'\n", d->CIF.scpdurl);
	uPnPPrintf("primary WAN Connection Device (IP or PPP Connection):\n");
	/*uPnPPrintf(" deviceType = '%s'\n", d->first.devicetype);*/
	uPnPPrintf(" servicetype = '%s'\n", d->first.servicetype);
	uPnPPrintf(" controlURL = '%s'\n", d->first.controlurl);
	uPnPPrintf(" eventSubURL = '%s'\n", d->first.eventsuburl);
	uPnPPrintf(" SCPDURL = '%s'\n", d->first.scpdurl);
	uPnPPrintf("secondary WAN Connection Device (IP or PPP Connection):\n");
	/*uPnPPrintf(" deviceType = '%s'\n", d->second.devicetype);*/
	uPnPPrintf(" servicetype = '%s'\n", d->second.servicetype);
	uPnPPrintf(" controlURL = '%s'\n", d->second.controlurl);
	uPnPPrintf(" eventSubURL = '%s'\n", d->second.eventsuburl);
	uPnPPrintf(" SCPDURL = '%s'\n", d->second.scpdurl);
	uPnPPrintf("WAN IPv6 Firewall Control :\n");
	/*uPnPPrintf(" deviceType = '%s'\n", d->IPv6FC.devicetype);*/
	uPnPPrintf(" servicetype = '%s'\n", d->IPv6FC.servicetype);
	uPnPPrintf(" controlURL = '%s'\n", d->IPv6FC.controlurl);
	uPnPPrintf(" eventSubURL = '%s'\n", d->IPv6FC.eventsuburl);
	uPnPPrintf(" SCPDURL = '%s'\n", d->IPv6FC.scpdurl);
}
#endif /* !__NO_OUTPUT */

#endif // NET_ENABLE_UPNP
