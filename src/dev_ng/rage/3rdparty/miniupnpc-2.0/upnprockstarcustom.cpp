#include "upnprockstarcustom.h"

#if NET_ENABLE_UPNP

#if RSG_WIN32
#pragma warning(push)
#pragma warning(disable: 4668)
#include <vadefs.h>
#endif

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

#if RSG_WIN32
#pragma warning(pop)
#endif

#include "diag/channel.h"
#include "diag/output.h"
#include "net/netdiag.h"


#if RSG_ORBIS
#ifdef __cplusplus
	extern "C" {
#endif

static int s_OrbisUpNpMemPoolId = -1;

int OrbisCreateNetPool()
{
	// Check if pool has been created
	if (s_OrbisUpNpMemPoolId >= 0)
		return 0;

	// Otherwise, create a new pool
	s_OrbisUpNpMemPoolId = sceNetPoolCreate("netResolver", 4*1024, 0);
	if(s_OrbisUpNpMemPoolId < 0)
	{
		return 1;
	}

	return 0;
}

void OrbisShutdownNetPool()
{
	if(s_OrbisUpNpMemPoolId >= 0)
	{
		sceNetPoolDestroy(s_OrbisUpNpMemPoolId);
		s_OrbisUpNpMemPoolId = -1;
	}
}

unsigned long inet_addr(const char *addr)
{
	unsigned long result = INADDR_NONE;
	unsigned int a0, a1, a2, a3;

	if((addr == NULL) || (addr[0] == '\0'))
	{
		return result;
	}

	if(sscanf(addr, "%u.%u.%u.%u", &a0, &a1, &a2, &a3) == 4)
	{
		result = (unsigned int)((((a3) & 0xFF) << 24)
			| (((a2) & 0xFF) << 16)
			| (((a1) & 0xFF) << 8)
			| (((a0) & 0xFF) << 0));		
	}

	return result;
}

struct hostent* gethostbyname(const char* name)
{
	static struct hostent s_Hostent;
	memset(&s_Hostent, 0, sizeof(s_Hostent));

	// first test to see if this is just an IP in string form
	int err = 1;
	unsigned long addr = inet_addr(name);
	if(addr != INADDR_NONE)
	{
		memcpy(s_Hostent.h_addr, &addr, 4);
		err = 0;
	}
	else
	{
		// resolve hostname
		err = OrbisCreateNetPool();

		if(err == 0)
		{
			err = 1;
			SceNetId resolverId = sceNetResolverCreate("netUpNpResolver", s_OrbisUpNpMemPoolId, 0);
			if(resolverId >= 0)
			{
				SceNetInAddr ina;
				if(0 <= sceNetResolverStartNtoa(resolverId, name, &ina, 0, 0, 0))
				{
					// miniupnp only cares about the first address
					unsigned int hina = (unsigned int)ina.s_addr;
					memcpy(s_Hostent.h_addr, &hina, 4);
					err = 0;
				}

				sceNetResolverDestroy(resolverId);
			}
		}

		OrbisShutdownNetPool();
	}

	return (err == 0) ? &s_Hostent : NULL;
}
#ifdef __cplusplus
}
#endif

#endif

char* uPnPStrDup(const char* str)
{
	if( !str )
		return 0;
	int siz = (int)strlen( str ) + 1;
	return ( char* ) memcpy( malloc(siz), str, sizeof(char) * siz ); 
}


// miniupnp is verbose, only enable when debugging UPnP
#define ENABLE_MINIUPNP_OUTPUT (!__NO_OUTPUT && 1)

#if ENABLE_MINIUPNP_OUTPUT
namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, miniupnp)
#undef __rage_channel
#define __rage_channel ragenet_miniupnp

void uPnPNetDebug(const char* msg)
{
	netDebug3("%s", msg);
}

}

void uPnPPrintf(const char *fmt, ...)
{
	char message[4096];
	char* msg = &message[0];
	char* newline = NULL;

	va_list args;
	va_start(args, fmt);
#if RSG_WIN32
	_vsnprintf_s(msg, sizeof(message) - 1, _TRUNCATE, fmt, args);
#else
	vsnprintf(msg, sizeof(message), fmt, args);
#endif
	va_end(args);

	newline = strrchr(msg, '\n');
	if(newline)
	{
		*newline = '\0';
	}

	rage::uPnPNetDebug(msg);
}
#else
void uPnPPrintf(const char* UNUSED_PARAM(fmt), ...)
{

}
#endif // ENABLE_MINIUPNP_OUTPUT

#endif // NET_ENABLE_UPNP
