#define _CRT_SECURE_NO_WARNINGS
#ifdef _XBOX
#include <xtl.h>
#include <xbdm.h>
#include <stdio.h>
#pragma warning(disable: 4509)
#elif defined _WIN32
#include <process.h>
#pragma warning(push)
#pragma warning(disable:4005)
#include <windows.h>
#pragma warning(pop)
#endif

#ifdef _WIN32
#define EXMUTEX HANDLE
#define EXSEM   HANDLE
#define EXTHREAD HANDLE
#define MUTEXID(x) (x)
#define SEMID(x)   (x)
#define LOCK_MUTEX(m) WaitForSingleObject( m, INFINITE )
#define UNLOCK_MUTEX(m) ReleaseMutex(m)
#define SEMAPHORE_WAIT(s) ( WaitForSingleObject( s, 0 ) == WAIT_OBJECT_0 )
#define SEMAPHORE_RELEASE(s) ReleaseSemaphore(s,1,NULL)
#define WAIT_THREAD(t) WaitForSingleObject(t,INFINITE)

#endif

#include <math.h>
#include <string.h>

#if defined (__linux__) || defined (__APPLE__)
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#endif

#ifdef _SEKRIT
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef __psp2__
#include <string.h>
#endif

#ifdef __CELLOS_LV2__
#include <sys/timer.h>
#include <netex/libnetctl.h>
#include <string.h>
#include <PSGL/psgl.h>
#include <PSGL/psglu.h>
#include <cell/sysmodule.h>
#include <cell/cell_fs.h>
#include <cell/pad.h>
#include <sys/spu_initialize.h>
#include <sysutil/sysutil_sysparam.h>
#include <sys/sys_time.h>
#include <pthread.h>
#include <semaphore.h>
#endif

#ifdef CAFE
#include <stdlib.h>
#include <string.h>

#include <cafe.h>
#include <cafe/os.h>

typedef OSMutex EXMUTEX;
typedef OSSemaphore EXSEM;
typedef OSThread EXTHREAD;

#define MUTEXID(x) (&(x))
#define SEMID(x) (&(x))
#define LOCK_MUTEX(m) OSLockMutex(&(m))
#define UNLOCK_MUTEX(m) OSUnlockMutex(&(m))
#define SEMAPHORE_WAIT(s) OSWaitSemaphore(&(s))
#define SEMAPHORE_RELEASE(s) OSSignalSemaphore(&(s))
#define WAIT_THREAD(t) OSJoinThread(&(t), NULL)
#endif

#if defined __CELLOS_LV2__ || defined __linux__ || defined __APPLE__
typedef pthread_mutex_t EXMUTEX;
typedef sem_t EXSEM;
typedef pthread_t EXTHREAD;

#define MUTEXID(x) (&(x))
#define SEMID(x) (&(x))
#define LOCK_MUTEX(m) pthread_mutex_lock( MUTEXID(m) )
#define UNLOCK_MUTEX(m) pthread_mutex_unlock( MUTEXID(m))
#define SEMAPHORE_WAIT(s) ( sem_trywait( SEMID(s) ) == 0 )
#define SEMAPHORE_RELEASE(s) sem_post(SEMID(s))
#define WAIT_THREAD(t) pthread_join(t,NULL)
#endif

#if defined __psp2__
#include <kernel.h>

typedef SceKernelLwMutexWork EXMUTEX;
typedef SceUID EXSEM;
typedef SceUID EXTHREAD;
#define MUTEXID(x) &(x)
#define SEMID(x) &(x)
#define LOCK_MUTEX(x) sceKernelLockLwMutex(&(x),2,NULL)
#define UNLOCK_MUTEX(x) sceKernelUnlockLwMutex(&(x),2)
#define SEMAPHORE_WAIT(x) sceKernelWaitSema( (x), 1, NULL )
#define SEMAPHORE_RELEASE(x) sceKernelSignalSema( (x), 1 )
#define WAIT_THREAD(t) sceKernelWaitThreadEnd(t,NULL,NULL)
#endif

char const *kpHost = "localhost";
char const *kpName = "example";

#ifdef _WIN32
#define mysleep(x) Sleep(x)
#elif defined __CELLOS_LV2__
#define mysleep(x) sys_timer_usleep((x)*1000)
#elif defined (__linux__) || defined (__APPLE__)
#define mysleep(x) usleep((x)*1000)
#elif defined CAFE
#define mysleep(x) OSWaitMicroseconds((x)*1000)
#elif defined __psp2__
#define mysleep(x) sceKernelDelayThread(( SceUInt32 ) ( (x)*1000) )
#else
#define mysleep(x)
//#error not implemented
#endif

#ifndef __RADSPU__
static TmU64 TEST_CLOCKS_PER_SEC;
static TmU64 TEST_CLOCKS_PER_MSEC;
static TmU64 TEST_CLOCKS_PER_USEC;
static TmConnectionType TEST_INTERFACE_TYPE = TMCT_TCP;
static int CHECKED_BUILD = 0;

void busywait( TmU64 const kDuration)
{
    TmU64 const kStart = tmFastTime();
    TmI64 elapsed;

    for ( ;; )
    {
        elapsed = tmFastTime() - kStart;
        if ( elapsed >= ( TmI64 ) kDuration )
            break;
    }
}
#endif

#ifdef __CELLOS_LV2__
static void init_system()
{
    cellSysmoduleLoadModule( CELL_SYSMODULE_NETCTL );

    // let the debugger settle down (wait one second)
    sys_timer_usleep( 1000 * 1000 * 1 );

    // Get net state working on PS3
    cellNetCtlInit();
    int ret = 0, connection_status;

    int done = 0;
    while ( !done )
    {
        ret = cellNetCtlGetState( &connection_status );
        if(ret < 0)
        {
            break;
        }
        switch ( connection_status ) 
        {
        case CELL_NET_CTL_STATE_Disconnected:
            /* Disconnected */
            break;
        case CELL_NET_CTL_STATE_Connecting:
            /* Connecting */
            break;
        case CELL_NET_CTL_STATE_IPObtaining:
            /* IPObtaining (obtaining IP address) */
            break;
        case CELL_NET_CTL_STATE_IPObtained:
            done = true;
            /* IPObtained (connected) */
            break;
        }
        sys_timer_usleep(1000*1000);
    }

    cellNetCtlTerm();
}
static void create_mutex(EXMUTEX*m)
{
    pthread_mutex_init(m,NULL);
}
static void create_sem(EXSEM*s)
{
    sem_init(s,0,0);
}
static void create_thread( EXTHREAD* pThread, void *(*fnc)(void*), void *arg )
{
    pthread_create( pThread, NULL, fnc, arg );
}
static void yield_thread()
{    
}
#elif defined __RADSEKRIT__
static void init_system()
{
}
static void create_mutex(EXMUTEX*m)
{
    *m = CreateMutex(NULL,FALSE,NULL);
}
static void create_sem(EXSEM*s)
{
    *s = CreateSemaphoreExW(0, 0, 255, NULL, 0, 0);
}
static void create_thread( EXTHREAD* pThread, DWORD ( WINAPI *fnc)(void*), void *arg )
{
    *pThread = CreateThread( NULL, 0, ( LPTHREAD_START_ROUTINE ) fnc, arg, 0, NULL );
}
static void yield_thread()
{    
}
#elif defined _WIN32
static void init_system()
{
}
static void create_mutex(EXMUTEX*m)
{
    *m = CreateMutex(NULL,FALSE,NULL);
}
static void create_sem(EXSEM*s)
{
    *s = CreateSemaphore( NULL, 0, 255, NULL );
}
static void create_thread( EXTHREAD* pThread, DWORD ( WINAPI *fnc)(void*), void *arg )
{
    *pThread = CreateThread( NULL, 0, ( LPTHREAD_START_ROUTINE ) fnc, arg, 0, NULL );
}
static void yield_thread()
{    
}
#elif defined __linux__ || defined __APPLE__
static void init_system()
{
}
static void create_mutex(EXMUTEX*m)
{
    pthread_mutex_init(m,NULL);
}
static void create_sem(EXSEM*s)
{
    sem_init(s,0,0);
}
static void create_thread( EXTHREAD* pThread, void *(*fnc)(void*), void *arg )
{
    pthread_create( pThread, NULL, fnc, arg );
}
static void yield_thread()
{    
}
#elif defined CAFE
static void init_system()
{
}
static void create_mutex(EXMUTEX*m)
{
    OSInitMutex(m);
}
static void create_sem(EXSEM*s)
{
    OSInitSemaphore(s, 0);
}
static void create_thread( EXTHREAD* pThread, int (*fnc)(int, void*), void *arg )
{
    static u16 affinities[] = {OS_THREAD_ATTR_AFFINITY_CORE0, OS_THREAD_ATTR_AFFINITY_CORE1, OS_THREAD_ATTR_AFFINITY_CORE2};
    static int affinity = 0;
    const int stack_size = 100 * 1024;
    char *stack = (char *) malloc(stack_size);
    OSCreateThread(pThread, (OSThread_Proc) fnc, 1, arg, stack + stack_size, stack_size, OSGetThreadPriority(OSGetCurrentThread()), 0);
    OSSetThreadAffinity(pThread, affinities[affinity++]);
    if (affinity == 3) affinity = 0;
    OSResumeThread(pThread);
}
static void yield_thread()
{    
    OSYieldThread();
}
#elif defined __psp2__
static void init_system()
{
}
static void create_mutex(EXMUTEX*m)
{
    char buffer[ 1024 ];
    sprintf( buffer, "ExMutex %p", m );
    sceKernelCreateLwMutex( m, buffer, SCE_KERNEL_LW_MUTEX_ATTR_TH_FIFO | SCE_KERNEL_LW_MUTEX_ATTR_RECURSIVE, 0, NULL );
}
static void create_sem(EXSEM*s)
{
    char buffer[ 1024 ];
    sprintf( buffer, "ExSema %p", s );
    *s = sceKernelCreateSema( buffer, SCE_KERNEL_SEMA_ATTR_TH_FIFO, 0, 255, NULL );
}
static void create_thread( EXTHREAD* pThread, SceInt32 (*fnc)(SceSize, void*), void *arg )
{
    char buffer[ 1024 ];
    sprintf( buffer, "ExThread %p", pThread );
    *pThread = sceKernelCreateThread( buffer, ( SceKernelThreadEntry ) fnc, SCE_KERNEL_DEFAULT_PRIORITY_USER, 32*1024, 0, SCE_KERNEL_CPU_MASK_USER_ALL, NULL );
    int err = sceKernelStartThread( *pThread, sizeof( arg ), arg );
    printf( "Err = %d\n", err );
}
static void yield_thread()
{    
    mysleep(1);
}

#elif defined __RADSEKRIT2__
static void init_system()
{
}
#else
#error not implemented
#endif

#ifdef __RADXENON__
FILE *logfile;
#endif

static void debug_printer(const char *s)
{ 
#ifdef __RADXENON__
    fputs(s, logfile);
    fflush(logfile);
#endif
}

#ifdef __RADSEKRIT__
int main(Platform::Array<Platform::String^>^ args)
{
    int argc = args->Length;
    char **argv = new char*[argc];
    for (int i = 0; i < argc; i++) {
        size_t size = 2 * (args[i]->Length() + 1);
        argv[i] = new char[size];
        wcstombs_s(NULL, argv[i], size, args[i]->Begin(), size);
    }
#else
int main(int argc, char **argv)
{
#endif
    extern int test_main(int argc, char **argv);
    extern const char *EXAMPLE_NAME;

    kpName = EXAMPLE_NAME;

#ifdef __RADXENON__
    char buf[1024] = {0};
    char *bufp = buf;
    char *newargv[32] = {NULL};
    char **newargp = newargv;
    char *cmdline = GetCommandLineA();
    while (*cmdline) {
        while (isspace(*cmdline))
            cmdline++;
        *newargp++ = bufp;
        while (*cmdline && !isspace(*cmdline))
            *bufp++ = *cmdline++;
        *bufp++ = 0;
    }
    argc = newargp - newargv;
    argv = newargv;
#endif

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-host") == 0 && i+1 < argc) {
            kpHost = argv[++i];
            continue;
        }

        if (strcmp(argv[i], "-name") == 0 && i+1 < argc) {
            kpName = argv[++i];
            continue;
        }

        if (strcmp(argv[i], "-file") == 0 ) {
            TEST_INTERFACE_TYPE = TMCT_FILE;
            continue;
        }

        if (strcmp(argv[i], "-checked") == 0 ) {
            CHECKED_BUILD = 1;
            continue;
        }
    }

    init_system();

#ifdef __psp2__
    tmStartup();
#endif

    // Figure out how long many clock cycles each zone should be
    TmU64 a = tmFastTime();
    mysleep(1000);
    TmU64 b = tmFastTime();
    TEST_CLOCKS_PER_SEC = b - a;
    TEST_CLOCKS_PER_MSEC = ( TEST_CLOCKS_PER_SEC ) / 1000;
    TEST_CLOCKS_PER_USEC = ( TEST_CLOCKS_PER_SEC ) / 1000000;

#ifdef __RADXENON__
    DmMapDevkitDrive();

    char lockfilename[256];
    sprintf_s(lockfilename, sizeof(lockfilename), "e:\\%s.lock", kpName);
    DeleteFile(lockfilename);

    char logfilename[256];
    sprintf_s(logfilename, sizeof(logfilename), "e:\\%s.log", kpName);
    logfile = fopen(logfilename, "wt");

    __try {
#endif
    return test_main(argc, argv);
#ifdef __RADXENON__
    } __finally {
        fclose(logfile);
        fclose(fopen(lockfilename, "w")); // create empty lock file
    }
#endif
}