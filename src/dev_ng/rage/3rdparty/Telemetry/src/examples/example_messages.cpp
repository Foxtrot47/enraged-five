#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include "telemetry.h"
#include "telemetry_example.h"

/* DEFTUTORIAL EXPGROUP(TutorialMessages) (TutorialMessages_Intro,Introduction) */
/* 
This is a basic example of using messages in a Telemetry application.
$-
*/

char const *EXAMPLE_NAME = "(examples)messages";
static HTELEMETRY cx;
static TmU8 context_buffer[ 1024 * 1024 ]; // Static context buffer

static void sTestVarArgs( char const *kpFmt, ... )
{
    va_list vl;
    va_start(vl,kpFmt);
    tmMessage( cx, TMMF_SEVERITY_LOG, TM_VA_LIST, kpFmt, &vl );
    va_end(vl);
}

/* DEFTUTORIAL EXPGROUP(TutorialMessages) (TutorialMessages_main, main) */
int test_main(int argc, char **argv)
{
    // First we load the Telemetry DLLs.  0 parameter means 'not debug'
    tmLoadTelemetry(CHECKED_BUILD);

    // Startup Telemetry
    tmStartup();

    // We have our memory for a context, but Telemetry needs to initialize it
    tmInitializeContext( &cx, context_buffer, sizeof( context_buffer ) );

    // Connect to server at the default port.  We also tell tmOpen to initialize any 
    // OS networking services.  We specify an indefinite timeout.
    if ( tmOpen( cx, EXAMPLE_NAME, __DATE__ __TIME__, kpHost, TEST_INTERFACE_TYPE, TELEMETRY_DEFAULT_PORT, TMOF_INIT_NETWORKING, -1 ) != TM_OK )
    {
        printf( "...failed, exiting\n" );
        return 1;
    }

    // Send a message upon connection.  This should show up in frame 0 of your message log.
    tmMessage( cx, TMMF_SEVERITY_LOG, "First message!" );

    // This has a 'magic' Telemetry message in it for sending callstacks
    tmMessage( cx, TMMF_SEVERITY_LOG, "Callstack: %t", tmSendCallStack(cx,0) );

    tmMessage( cx, TMMF_SEVERITY_LOG, "Test %p:%d", cx, 998 );

    tmMessage( cx, TMMF_SEVERITY_LOG, "%s/%s (%u)(%u)(%u)", "foo", "bar", 1024, 1025, 1026 );

    // An example that uses a va_list
    sTestVarArgs("testing varargs %p:%d", cx, 999 );

    // Normal zone definition
    tmEnter( cx, TMZF_NONE, "main" );

    for ( int i = 0; i < 30; i++ )
    {
        // C++ style zone definition object.  tmEnter is called at declaration and tmLeave is called when
        // when the underlying object goes out of scope and its destructor is called
        tmZone( cx, TMZF_NONE, "loop %d", i );

        {
            tmZone( cx, TMZF_IDLE, "(foo/bar)wasting time" );
            mysleep(5);
        }

        // Putting in a zone in which we can substitute the zone labels with messages
        tmEnter( cx, TMZF_NONE, "this should never display" );

        tmMessage( cx, TMMF_ZONE_LABEL, "I replaced the zone's label!" );
        tmMessage( cx, TMMF_ZONE_SUBLABEL, "Using context 0x%p", cx );
        mysleep(5);

        tmLeave( cx );

        /* Telemetry supports a subset of printf-style specifiers, including %I32/%I64 with 
        Microsoft Visual C++ */
        tmMessage( cx, TMMF_SEVERITY_LOG, "(tree00)This is a %%d: %d\n", i );
        tmMessage( cx, TMMF_SEVERITY_LOG, "(hello/world/one)This is a %%x: 0x%x\n", i );
        tmMessage( cx, TMMF_SEVERITY_LOG, "(hello/world/two)This is a %%x: 0x%x\n", i );
        tmMessage( cx, TMMF_SEVERITY_LOG, "(hello/world/three)This is a %%x: 0x%x\n", i );
        tmMessage( cx, TMMF_SEVERITY_LOG, "(tree00)This is a %%I64d: %I64d\n", ( ( TmU64) i ) << 32 );
        tmMessage( cx, TMMF_SEVERITY_LOG, "(tree00)This is a %%I64x: 0x%I64x\n", ( ( TmU64) i ) << 32 );
        tmMessage( cx, TMMF_SEVERITY_LOG, "(tree00)This is a %%f: %f\n", ( float ) i );

        tmMessage( cx, TMMF_SEVERITY_WARNING, "This is using a dynamic string (%s)\n", tmDynamicString(cx,"I'm dynamic!!!" ) );

        static char s_static_string[ 1024 ] = "changing static";
        tmClearStaticString( cx, s_static_string );
        tmMessage( cx, TMMF_SEVERITY_ERROR, "(path00/path01)This is using a static string that is constantly updated (%s)\n", s_static_string );
        sprintf( s_static_string, "changing static: %d\n", i );

        {
            tmZone( cx, TMZF_IDLE, "wasting more time" );
            mysleep(2);
        }

        // Send a message each tick, but flag it as a warning.  It should be a different color in the message window.
        tmMessage( cx, TMMF_SEVERITY_WARNING, "About to tick!!" );

        mysleep(1);
        tmMessage( cx, TMMF_ICON_NOTE, "icon note" );
        mysleep(1);
        tmMessage( cx, TMMF_ICON_WTF, "icon wtf" );
        mysleep(1);
        tmMessage( cx, TMMF_ICON_EXCLAMATION, "icon exclamation" );
        mysleep(1);
        tmMessage( cx, TMMF_ICON_EXTRA00, "icon extra00" );
        mysleep(1);
        tmMessage( cx, TMMF_ICON_EXTRA01, "icon extra01" );
        mysleep(1);
        tmMessage( cx, TMMF_ICON_EXTRA02, "icon extra02" );
        mysleep(1);
        tmMessage( cx, TMMF_ICON_EXTRA03, "icon extra03" );
        mysleep(1);
        tmMessage( cx, TMMF_ICON_EXTRA04, "icon extra04" );
        mysleep(1);

        // Do our tick
        tmTick(cx);
    }

    // Close the original 'main' zone
    tmLeave( cx );

    // Send a message that will show up as an icon in at the top of the zone view right at 
    // the end of frame 99.
    tmMessage( cx, TMMF_SEVERITY_LOG|TMMF_ICON_NOTE, "(ending)Out of the main loop and about to shut down" );

    // Close our connection
    tmClose( cx );

    // Shutdown the context
    tmShutdownContext( cx );

    // Unload Telemetry
    tmShutdown();

    return 0;
}

/* DEFTUTORIALEND */
// @cdep pre $DefaultsTMExample
// @cdep post $BuildTMExample 
