#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

/* DEFTUTORIAL EXPGROUP(TutorialMemory) (TutorialMemory_Intro,Introduction) */
/* 
This is the most basic example of a Telemetrized application showing memory
management.
$-
*/
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "telemetry.h"
#include "telemetry_example.h"


char const *EXAMPLE_NAME = "example_memory";
static HTELEMETRY cx;
static TmU8 context_buffer[ 1024 * 1024 ]; // Static context buffer

/* DEFTUTORIAL EXPGROUP(TutorialMemory) (TutorialMemory_main, main) */
int test_main(int argc, char **argv)
{
    // First we load the Telemetry DLLs.  0 parameter means 'not the checked build'
    tmLoadTelemetry(CHECKED_BUILD);

    // Startup Telemetry
    tmStartup();

    // We have our memory for a context, but Telemetry needs to initialize it
    tmInitializeContext( &cx, context_buffer, sizeof( context_buffer ) );

    // Connect to server at the default port.  We also tell tmOpen to initialize any 
    // OS networking services.  We specify an indefinite timeout.
    if ( tmOpen( cx, EXAMPLE_NAME, __DATE__ " " __TIME__, kpHost, TEST_INTERFACE_TYPE,  TELEMETRY_DEFAULT_PORT, TMOF_INIT_NETWORKING, -1 ) != TM_OK )
    {
        printf( "...failed, exiting\n" );
        return 1;
    }

    for ( int i = 0; i < 100; i++ )
    {
        // C++ style zone definition object.  tmEnter is called at declaration and tmLeave is called when
        // when the underlying object goes out of scope and its destructor is called
        tmZone( cx, TMZF_NONE, "loop %d", i );

        tmEnter( cx, TMZF_IDLE, "sleeping" );
        mysleep(1);
        tmLeave( cx );

        // Allocate some memory
        size_t kSize = ( i + 1 ) * 1024;
        char *q = new char[ kSize ];
        tmAlloc( cx, q, kSize, "%t", tmSendCallStackR( cx, 0 ) );
        tmMessage( cx, TMMF_SEVERITY_LOG, "allocated at %p\n", q );

        // Leak 32K on the first frame, this should show up as a red mem event in the memory event track
        if ( i == 0 )
        { 
            char *p = (char*)malloc(1024*32);
            tmAlloc( cx, p, 32*1024, "leak!" );
        }
        // These are just some hierarchical allocations
        if ( i == 1 )
        {
            int const kSize = 128*1024;
            tmAlloc( cx, malloc(kSize), kSize, "top0" );
            tmAlloc( cx, malloc(kSize/2), kSize/2, "top1" );
            tmAlloc( cx, malloc(kSize/4), kSize/4, "top2" );
            tmAlloc( cx, malloc(kSize/8), kSize/8, "top3" );
            tmAlloc( cx, malloc(kSize/8), kSize/8, "top3/alpha" );
            tmAlloc( cx, malloc(kSize/8), kSize/32, "top3/bravo" );
            tmAlloc( cx, malloc(kSize/8), kSize/16, "top3/bravo/00" );
            tmAlloc( cx, malloc(kSize/8), kSize/16, "top3/bravo/01" );
            tmAlloc( cx, malloc(kSize), kSize, "agg0" );
            tmAlloc( cx, malloc(kSize), kSize, "agg0" );
            tmAlloc( cx, malloc(kSize), kSize, "agg0" );
        }

        mysleep(4);

        // Free at end of frame
        tmFree( cx, q );
        delete []q;

        // Do our tick
        tmTick(cx);
    }

    // Close our connection
    tmClose( cx );

    // Shutdown the context
    tmShutdownContext( cx );

    // Unload Telemetry
    tmShutdown();

    return 0;
}

/* DEFTUTORIALEND */

// @cdep pre $DefaultsTMExample
// @cdep post $BuildTMExample 
