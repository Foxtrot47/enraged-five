#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include "telemetry.h"
#include "telemetry_example.h"

/* DEFTUTORIAL EXPGROUP(TutorialBasic) (TutorialBasic_Intro,Introduction) */
/* 
This is the most basic example of a Telemetrized application.  It initializes
Telemetry, connects to a server, runs a loop with a zone and tick, then shuts
everything back down.
$-
*/

#define _EXAMPLE_NAME "(examples/basic)basic"
char const *EXAMPLE_NAME = _EXAMPLE_NAME;

static HTELEMETRY cx;
static TmU8 context_buffer[ 2* 1024 * 1024 ]; // Static context buffer
float dummy;

static float test(int d)
{
    //    tmZone( cx, TMZF_NONE, "test %d %t", d, tmSendCallStack(cx,0) );

    float f = 1.0f;

    if ( d == 0 )
        return f;

    for ( int i = 0; i < 1000; i++ )
    {
        f += i * f;
    }
    for ( int q = 0; q < d; q++ )
    {
        f += test(d-1);
    }
    dummy = f;
    return f;
}

/* DEFTUTORIAL EXPGROUP(TutorialBasic) (TutorialBasic_main, main) */
int test_main(int argc, char **argv)
{
    printf( _EXAMPLE_NAME " starting\n" );
    // First we load the Telemetry DLLs.  0 parameter means 'not the checked build'
    if ( !tmLoadTelemetry(CHECKED_BUILD) )
    {
        printf( "Could not load telemetry\n" );
        return -1;
    }

    // Startup Telemetry
    tmStartup();

    // We have our memory for a context, but Telemetry needs to initialize it
    tmInitializeContext( &cx, context_buffer, sizeof( context_buffer ) );

    if ( tmCheckVersion( cx, TelemetryMajorVersion, TelemetryMinorVersion, TelemetryBuildNumber, TelemetryCustomization ) != TM_OK )
    {
        printf( "tmCheckVersion failed\n" );
        return -1;
    }

    tmEnable( cx, TMO_OUTPUT_DEBUG_INFO, 1 );
    tmSetParameter( cx, TMP_DEBUG_PRINTER, ( void * ) debug_printer );

    // Connect to server at the default port.  We also tell tmOpen to initialize any 
    // OS networking services.  We specify an indefinite timeout.
    printf( "Connecting to %s\n", kpHost );
    int error = 1234;
    char const *build_info = "This is a very long comment that you would include as part of your build info.  This information\
 might include username, more system information, and other data that might be relevant to the particular recorded session!";

    if ( (error = tmOpen( cx, EXAMPLE_NAME, build_info, kpHost, TEST_INTERFACE_TYPE,
        TELEMETRY_DEFAULT_PORT, TMOF_INIT_NETWORKING|TMOF_MAXIMUM_CONTEXT_SWITCHES, -1 )) != TM_OK )
    {
        printf( "...failed (%d), exiting\n", error );
        return 1;
    }

    tmEnable( cx, TMO_SUPPORT_MEMORY, 0 );
    printf( "%d\n", tmIsEnabled( cx, TMO_SUPPORT_MEMORY ) );

    tmEnter( cx, TMZF_NONE, "BIG LOOP" );
    for ( int i = 0; i < 150; i++ )
    {
        // C++ style zone definition object.  tmEnter is called at declaration and tmLeave is called when
        // when the underlying object goes out of scope and its destructor is called
        tmZone( cx, TMZF_NONE, "loop %d", i );

        // Set timeline section names
        if ( i > 20 )
        {
            tmSetTimelineSectionName( cx, "20+" );
        }
        else if ( i > 10 )
        {
            tmSetTimelineSectionName( cx, "10+" );
        }
        else 
        {
            tmSetTimelineSectionName( cx, "0+" );
        }

        // Here we're doing a bunch of zones but filtering them out by specifying a 1000us (1ms) 
        // threshold.  Most of these should not show up in the Visualizer (the filtering is
        // conservative, so it's possible some will sneak through)
        {
            tmZone( cx, TMZF_NONE, "tmZone filtered" );

            for ( int j = 0; j < 5000; j++ )
            {
                tmZoneFiltered( cx, 1000, TMZF_NONE, "tmZone filtered!" );
            }
        }

        // Here we're doing a bunch of zones but filtering them out by specifying a 1000us (1ms) 
        // threshold.  Most of these should not show up in the Visualizer (the filtering is
        // conservative, so it's possible some will sneak through)
        {
            tmZone( cx, TMZF_NONE, "tmZone overhead" );

            for ( int j = 0; j < 5000; j++ )
            {
                tmZone( cx, TMZF_NONE, "overhead!" );
            }
        }

        // This is identical to the above, however we're doing it with the explicit tmEnterEx
        // and tmLeaveEx APIs which require passing a 'matchid' variable
        {
            tmZone( cx, TMZF_NONE, "tmEnterEx filtered" );
            for ( int j = 0; j < 5000; j++ )
            {
                TmU64 matchid=0;
                tmEnterEx(cx,&matchid,0,1000,__FILE__,__LINE__, TMZF_NONE, "tmEnterEx filtered!" );
                tmLeaveEx( cx, matchid,0, __FILE__, __LINE__ );
            }
        }

        // This is the same as above, but we have the filter level set to 0 so these should
        // all still come across
        {
            tmZone( cx, TMZF_NONE, "tmEnterEx unfiltered" );
            for ( int j = 0; j < 1000; j++ )
            {
                tmEnterEx(cx,NULL,0,0,__FILE__,__LINE__, TMZF_NONE, "tmEnterEx unfiltered!" );
                test(2);
                tmLeaveEx( cx, 0, 0, __FILE__, __LINE__ );
            }
        }

        // Finally, these are filtered but with a 1ms threshold but we're sleeping for 2ms, so
        // these should always appear
        {
            tmZone( cx, TMZF_NONE, "tmZoneFiltered but not filtered out!" );
            for ( int j = 0; j < 10; j++ )
            {
                tmZoneFiltered( cx, 100, TMZF_NONE, "visible!!!" );
                mysleep(1);
            }
        }

        // Normal zone definition
        tmEnter( cx, TMZF_NONE, "stuff" );
        mysleep(5);
        tmLeave( cx );

        // Idle zone definition
        tmEnter( cx, TMZF_IDLE, "sleeping" );
        mysleep(5);
        tmLeave( cx );

        // Do our tick
        tmTick(cx);
    }
    tmLeave( cx );

    // Close our connection
    tmClose( cx );

    // Shutdown the context
    tmShutdownContext( cx );

    // Unload Telemetry
    tmShutdown();

    return 0;
}

/* DEFTUTORIALEND */

// @cdep pre $DefaultsTMExample
// @cdep post $if($equals($buildplatform,linuxARM32), $requiresbinary(-lrt), )
// @cdep post $BuildTMExample 
