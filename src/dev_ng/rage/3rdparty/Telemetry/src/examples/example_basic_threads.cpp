#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "telemetry.h"
#include "telemetry_example.h"

#ifdef _MSC_VER
#pragma warning(disable:4996)
#endif

/* DEFTUTORIAL EXPGROUP(TutorialBasicThreads) (TutorialBasicThreads_Intro,Introduction) */
/* 
This is a basic example of a Telemetrized application that has multiple threads.  It initializes
Telemetry, connects to a server, creates some threads, runs a loop with some zones, ticks, then shuts
everything back down.
$-
There is some pretty gross abstraction of pthreads vs. Win32 threads in telemetry_example.h. 
$-
*/
#define _EXAMPLE_NAME "(examples/basic)threads"
char const *EXAMPLE_NAME = _EXAMPLE_NAME;

static HTELEMETRY cx;
static TmU8 context_buffer[ 1024 * 1024 ]; // Static context buffer
static volatile bool g_quitting;

/* Contrived example of some mutexes */
static EXMUTEX g_mutex[ 2 ];
static EXSEM   g_semaphore;

/* These are two nearly identical thread functions designed to overlap somewhat, but still have one thread
   exit early. In the Visualizer view you can see where thread B starts and also where they're fighting
   over the mutex (you'll see this as stall zones).  In addition, the locks track will show when they're
   contending a lot since there won't be any points of 'no ownership'. */
#ifdef _WIN32
static DWORD WINAPI thread_function( void *p )
#elif defined _PTHREAD_H_ || defined _PTHREAD_H
static void *thread_function( void *p )
#elif defined CAFE
static int thread_function(int, void *p)
#elif defined __psp2__
static SceInt32 thread_function( SceSize kArgSize, void *p )
#else
#error not implemented
#endif
{
    printf("thread starting\n");

    while ( !g_quitting )
    {
        tmZone( cx, TMZF_NONE, "Loop iteration %t", tmSendCallStack(cx,0) );

        // 'Randomly' choose a mutex to try to lock
        int const m = ( tmFastTime() ) & 1;

        TmU64 matcher=0;
        tmTryLockEx(cx,&matcher,500,__FILE__,__LINE__,MUTEXID(g_mutex[ m ]),"Grabbing lock %d", m );
        LOCK_MUTEX(g_mutex[ m ]);
        tmEndTryLockEx(cx,matcher,__FILE__,__LINE__,MUTEXID(g_mutex[ m ]),TMLR_SUCCESS);
        tmSetLockState(cx,MUTEXID(g_mutex[m]),TMLS_LOCKED, "locking mutex");

        // Then randomly choose some chunk of work
        int loops = ( (  tmFastTime() ) & 0x1F ) + 1;
        tmEnter( cx, TMZF_NONE, "do work on %d", m );
        busywait(TEST_CLOCKS_PER_SEC/10);
        tmLeave( cx );

        UNLOCK_MUTEX( g_mutex[ m ] );
        tmSetLockState(cx,MUTEXID(g_mutex[m]),TMLS_RELEASED, "Releasing");

        // Simulate being a worker thread
        if ( SEMAPHORE_WAIT( g_semaphore ) )
        {
            tmSetLockState( cx, SEMID(g_semaphore), TMLS_LOCKED, "locking semaphore %p", SEMID(g_semaphore) );
            {
                tmZone( cx, TMZF_NONE, "doing job" );
                int n = ( tmFastTime() & 0x1F ) + 1;
                busywait(n);
            }
            tmSetLockState( cx, SEMID(g_semaphore), TMLS_RELEASED, "releasing semaphore %p", SEMID(g_semaphore) ); 
        }
        else
        {

            tmZone( cx, TMZF_IDLE, "sleeping" );
            mysleep(loops&0x3);
        }
        yield_thread();
    }

#if 0
    // Show changing thread name
    tmThreadName( cx, 0, "changed %x", GetCurrentThreadId() );
    for ( int i = 0; i < 10; i++ )
    {
        tmZone( cx, TMZF_NONE, "some zone" );
        mysleep(1);
    }
#endif

    return 0;
}
static float test(int d)
{
    tmZone( cx, TMZF_NONE, "test %d %t", d, tmSendCallStack(cx,0) );

    float f = 1.0f;

    if ( d == 0 )
        return f;

    for ( int i = 0; i < 1000; i++ )
    {
        f += i * f;
    }
    for ( int q = 0; q < d; q++ )
    {
        f += test(d-1);
    }
    return f;
}

/* DEFTUTORIAL EXPGROUP(TutorialBasicThreads) (TutorialBasicThreads_main, main) */
int test_main(int argc, char **argv)
{
#ifdef CAFE
    EXTHREAD hThread1, hThread2;
#else
    EXTHREAD hThread1, hThread2 = 0;
#endif 

    init_system();

    // First we load the Telemetry DLLs
    (void) tmLoadTelemetry(CHECKED_BUILD);

    // Startup Telemetry
    tmStartup();

    // We have our memory for a context, but Telemetry needs to initialize it
    tmInitializeContext( &cx, context_buffer, sizeof( context_buffer ) );
    tmEnable( cx, TMO_OUTPUT_DEBUG_INFO, 1 );

    printf( "Connecting to %s\n", kpHost );

    if ( tmOpen( cx, EXAMPLE_NAME, __DATE__ __TIME__, kpHost, TEST_INTERFACE_TYPE,  TELEMETRY_DEFAULT_PORT, TMOF_INIT_NETWORKING|TMOF_MODERATE_CONTEXT_SWITCHES, -1 ) != TM_OK )
    {
        printf( "Failed to tmOpen to %s\n", kpHost );
        return -1;
    }
    tmThreadName( cx, 0, "main" );

    // Create locks
    for ( size_t i = 0; i < sizeof( g_mutex ) / sizeof( g_mutex[ 0 ] ); i++ )
    {
        create_mutex(&g_mutex[ i ] );
        // Naming the mutex let's us give it a handy textual description
        tmLockName( cx, MUTEXID(g_mutex[i]), "shared mutex %d", i );
    }

    create_sem(&g_semaphore);
    tmLockName( cx, SEMID(g_semaphore), "semaphore" );

#ifdef _WIN32
    HANDLE csq_thread_id = 0;
    tmGetPlatformInformation( cx, TMPI_CONTEXT_SWITCH_THREAD, &csq_thread_id, sizeof( csq_thread_id ) );
#endif

    // Create two threads fighting for the mutexes
    tmBeginTimeSpan( cx, 1, TMTSF_NONE, "timespan 1" );

    create_thread( &hThread1, thread_function, 0 );

    TmU64 time_at_frame_20 = 0, time_at_frame_40 = 0;

    // The main loop is just biding time not doing anything while background threads fight it out
    for ( int i = 0; i < 100; i++ )
    {
        // C++ style zone definition object.  tmEnter is called at declaration and tmLeave is called when
        // when the underlying object goes out of scope and its destructor is called
        tmZone( cx, TMZF_NONE, "loop" );
        tmMessage( cx, TMMF_ZONE_SUBLABEL, "iteration %d", i );

        mysleep( tmFastTime() & 0x3 );

        // These use the 'ex' versions so that we can filter out very short durations
        TmU64 matcher=0;
        tmTryLockEx(cx,&matcher,1000,__FILE__,__LINE__,MUTEXID(g_mutex[ i & 1 ]),"Grabbing lock %d", i&1 );
        LOCK_MUTEX( g_mutex[ i & 1 ] );
        tmEndTryLockEx(cx,matcher,__FILE__,__LINE__,MUTEXID(g_mutex[ i & 1 ]),TMLR_SUCCESS);
        tmSetLockState(cx,MUTEXID(g_mutex[ i & 1 ]),TMLS_LOCKED, "locked %d", i&1);

        // This is a test of min lock times
        if ( i == 5 )
        {
            for ( int j = 0; j < 100; j++ )
            {
                TmU8 buf[ TM_LOCK_MIN_TIME_BUFSIZE ];

                memset( buf, 0, sizeof( buf ) );

                tmSetLockStateMinTime( cx, buf, ( void const * ) 3, TMLS_LOCKED,   "Test mintime discarded" );
                busywait((tmFastTime()&7)+1);
                tmSetLockStateMinTime( cx, buf, ( void const * ) 3, TMLS_RELEASED, "Test mintime discarded" );
            }
            for ( int j = 0; j < 100; j++ )
            {
                TmU8 buf[ TM_LOCK_MIN_TIME_BUFSIZE ];

                memset( buf, 0, sizeof( buf ) );

                // These should always show up
                tmSetLockStateMinTime( cx, buf, ( void const * ) 4, TMLS_LOCKED,   "Test mintime visible" );
                mysleep(2);
                tmSetLockStateMinTime( cx, buf, ( void const * ) 4, TMLS_RELEASED, "Test mintime visible" );
            }
        }

        if ( i == 10 )
        {
            tmEnterEx( cx, NULL, 1999, 0, __FILE__, __LINE__, TMZF_NONE,"Fake thread" );
            mysleep(20);
            tmLeaveEx( cx, 0, 1999, __FILE__, __LINE__ );
        }

        if ( i == 20 )
            time_at_frame_20 = tmFastTime();
        if ( i == 40 )
            time_at_frame_40 = tmFastTime();

        tmEnter( cx, TMZF_NONE, "working on %d", i&1 );
        busywait((tmFastTime()&0x1F)+1);

        test(3);

        if ( i == 5 )
            tmEndTimeSpan( cx, 1, TMTSF_NONE, "timespan 1 end" );
        // Overlaps timespan 1
        if ( i == 4 )
        {
            tmBeginTimeSpan( cx, 999, TMTSF_NONE, "timespan 999 start" );
            tmBeginTimeSpan( cx, 1000, TMTSF_NONE, "timespan 1000 start" );
        }
        if ( i == 9 )
        {
            tmEndTimeSpan( cx, 999, TMTSF_NONE, "timespan 999 end" );
            tmBeginTimeSpan( cx, 1, TMTSF_NONE, "timespan 1 again" );
        }
        if ( i == 15 )
        {
            tmEndTimeSpan( cx, 1, TMTSF_NONE, "timespan 1 end" );
        }
        if ( i == 25 )
            tmEndTimeSpan( cx, 1000, TMTSF_NONE, "timespan 1000 end" );

        tmLeave( cx );

        UNLOCK_MUTEX(g_mutex[ i & 1 ]);

        tmSetLockState(cx,MUTEXID(g_mutex[ i & 1 ]),TMLS_RELEASED, "unlocked %d", i&1); 

        // Increment our semaphore some amount to simulate worker jobs being created
        {
            TmU32 const kNewJobs = ( tmFastTime() & 3 ) + 1;
            for ( int j = 0; j < int(kNewJobs); j++ )
            {
                tmZone( cx, TMZF_NONE, "adding job" );
                tmSignalLockCount( cx, SEMID(g_semaphore), 1, "signaling 1 job" );
                SEMAPHORE_RELEASE(g_semaphore);
//                busywait((tmFastTime()&0x7)+1);
                busywait(1);
            }
        }

        // Show what it looks like when a thread is created in the middle of a session
        if ( i == 25 )
        {
#ifdef _WIN32
            DWORD dwId;
            hThread2 = CreateThread( NULL, 0, thread_function, (void*)1, CREATE_SUSPENDED, &dwId );
            tmThreadName( cx, dwId, "thread 3" );
#ifdef _XBOX
            XSetThreadProcessor( hThread2, 1 );
#endif
            ResumeThread( hThread2 );
#else
            create_thread(&hThread2, thread_function, ( void * ) 1 );

            // On PS3 you'd use the sys_ppu APIs to get a thread ID instead of the pthread stuff
#ifdef CAFE
            tmThreadName( cx, (TmIntPtr) &hThread2, "thread 3" );
#elif !defined __CELLOS_LV2__
            tmThreadName( cx, (TmIntPtr) hThread2, "thread 3" );
#endif
#endif

        }

        // Do our tick
        tmTick(cx);
        yield_thread();
    }

    g_quitting = true;

    {
        tmZone( cx, TMZF_IDLE, "waiting..." );
//        if ( hThread2 )
            WAIT_THREAD( hThread1 );
//        if ( hThread2 )
            WAIT_THREAD( hThread2 );
    }

    tmBeginTimeSpanAt( cx, 2, TMTSF_NONE, time_at_frame_20, "timespan 2" );
    tmEndTimeSpanAt( cx, 2, TMTSF_NONE, time_at_frame_40, "timespan 2" );

    for ( int i = 0; i < 5; i++ )
    {
        tmZone( cx, TMZF_NONE, "ending %d", i );
        mysleep(10);
        tmTick(cx);
    }

    TmU64 const kEnd = tmFastTime();

    for ( int tries = 0; tries < 100; tries++ ) // Should probably timeout...
    {
        TmU64 t = tmGetLastContextSwitchTime(cx);
        if ( t > kEnd )
            break;
        else
            mysleep(10);
    }

    // Close our connection
    tmClose( cx );

    // Shutdown the context
    tmShutdownContext( cx );

    // Unload Telemetry
    tmShutdown();

    return 0;
}
/* DEFTUTORIALEND */

// @cdep pre $DefaultsTMExample
// @cdep post $if($equals($buildplatform,linuxARM32), $requiresbinary(-lrt), )
// @cdep post $BuildTMExample 
