/*
@cdep pre
$if($Telemetry2, 
    $addtocswitches(-DTELEMETRY2=1),
$addtocswitches(-Ic:\devel\projects\telemetry\src\serverlib)
    )
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "telemetry.h"
#include "telemetry_example.h"
#ifdef TELEMETRY2
#include "tms2_api.h"
#else
#include "tms_server.h"
#endif

#ifdef _WIN32
#pragma comment(linker,"/STACK:8192000,8192000")
#endif

/*
@cdep post
$if($equals($BuildPlatform,win32),
    $requiresbinary($builddir/tmserverlib32.lib)
,
    $requiresbinary($builddir/tmserverlib64.lib)
)
*/

/* DEFTUTORIAL EXPGROUP(TutorialEmbeddedServer) (TutorialEmbeddedServer_Intro,Introduction) */
/* 
This is a basic example of using the embedded Telemetry server.
$-
*/

char const *EXAMPLE_NAME = "example_basic_embedded_server";
static HTELEMETRY cx;
static TmU8 context_buffer[ 1024 * 1024 ]; // Static context buffer

static float test(int d)
{
    tmZone( cx, TMZF_NONE, "test %d %t", d, tmSendCallStack(cx,0) );

    float f = 1.0f;

    if ( d == 0 )
        return f;

    for ( int i = 0; i < 1000; i++ )
    {
        f += i * f;
    }
    for ( int q = 0; q < d; q++ )
    {
        f += test(d-1);
    }
    return f;
}

/* DEFTUTORIAL EXPGROUP(TutorialEmbeddedServer) (TutorialEmbeddedServer_main, main) */
int test_main(int argc, char **argv)
{
    // To get an embedded server rolling we need to initialize it
    TmsError kResult = TMS_OK;

    if ( ( kResult = tmsInit( TMSINT_TCP, NULL, 0, 0, 0 ) ) != TMS_OK )
    {
        printf( "Could not initialize embedded server\n" );
        return -1;
    }

   // And then start it
    char server_name[ 512 ], server_ip[ 512 ];
    if ( ( kResult = tmsStart( server_name, 
        sizeof( server_name ), 
        server_ip, 
        sizeof( server_ip ), 
        TELEMETRY_DEFAULT_PORT, 0, 
        ".", 
        TMSF_DEFAULT, 
        10*60, -1 ) ) != TMS_OK )
    {
        printf( "Could not start embedded server\n" );
        return -1;
    }

    // First we load the Telemetry DLLs.  0 parameter means 'not the checked build'
    if ( !tmLoadTelemetry(CHECKED_BUILD) )
    {
        printf( "Could not load telemetry\n" );
        return -1;
    }

    // Startup Telemetry
    tmStartup();

    // We have our memory for a context, but Telemetry needs to initialize it
    tmInitializeContext( &cx, context_buffer, sizeof( context_buffer ) );
    tmEnable( cx, TMO_OUTPUT_DEBUG_INFO, 1 );

    // Connect to server at the default port.  We also tell tmOpen to initialize any 
    // OS networking services.  We specify an indefinite timeout.
    printf( "Connecting to %s\n", kpHost );
    if ( tmOpen( cx, EXAMPLE_NAME, __DATE__ __TIME__, kpHost, TMCT_TCP, TELEMETRY_DEFAULT_PORT, TMOF_INIT_NETWORKING, -1 ) != TM_OK )
    {
        printf( "...failed, exiting\n" );
        return 1;
    }

    for ( int i = 0; i < 100; i++ )
    {
        // C++ style zone definition object.  tmEnter is called at declaration and tmLeave is called when
        // when the underlying object goes out of scope and its destructor is called
        tmZone( cx, TMZF_NONE, "loop %d", i );

        test(3);

        // Normal zone definition
        tmEnter( cx, TMZF_IDLE, "sleeping" );
        mysleep(10);
        tmLeave( cx );

        for ( int j = 0; j < 1000; j++ )
        {
            tmZone( cx, TMZF_NONE, "busy stuff" );
            test(2);
        }

        // Do our tick
        tmTick(cx);
    }

    // Close our connection
    tmClose( cx );

    // Shutdown the context
    tmShutdownContext( cx );

    // Unload Telemetry
    tmShutdown();

    while ( tmsIsStillCooking() )
        ;
    tmsHalt();
    tmsShutdown();

    return 0;
}

/* DEFTUTORIALEND */

// @cdep pre $DefaultsTMExample

// @cdep pre $replacewith(c8switches,$c8switches,-EHs-,-EHsc)
// @cdep pre $replacewith(c8switches,$c8switches,-EHc-, )
// @cdep pre $replacewith(c8switches,$c8switches,-QIfist, )

// @cdep post $BuildTMExample 
