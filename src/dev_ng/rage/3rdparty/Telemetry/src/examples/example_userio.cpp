#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include "telemetry.h"
#include "telemetry_example.h"

struct CallbackData
{
    FILE *cb_fp;
};
static CallbackData callback_data;

#define _EXAMPLE_NAME "example_userio"
char const *EXAMPLE_NAME = _EXAMPLE_NAME;

static HTELEMETRY cx;
static TmU8 context_buffer[ 2* 1024 * 1024 ]; // Static context buffer
float dummy;

int user_close( void *d )
{
    CallbackData* cb = ( CallbackData * ) d;
    if ( cb->cb_fp )
    {
        fclose( cb->cb_fp );
        cb->cb_fp = 0;
    }
    return 1;
}
int user_open( void *d, char const *n )
{
    CallbackData* cb = ( CallbackData * ) d;
    char fname[ 1024 ];

#if defined(_XENON) || ( defined(_XBOX_VER) && (_XBOX_VER == 200) )
    wsprintfA( fname, "e:\\%s.000", n );
#else
    sprintf( fname, "%s.000", n );
#endif
    cb->cb_fp = fopen( fname, "wb" );
    return 1;
}
int user_write( void *d, void const *kpSrc, int s )
{
    CallbackData* cb = ( CallbackData * ) d;
    return fwrite( kpSrc, s, 1, cb->cb_fp ) == 1;
}

int test_main(int argc, char **argv)
{
    printf( _EXAMPLE_NAME " starting\n" );
    // First we load the Telemetry DLLs.  0 parameter means 'not the checked build'
    if ( !tmLoadTelemetry(CHECKED_BUILD) )
    {
        printf( "Could not load telemetry\n" );
        return -1;
    }

    // Startup Telemetry
    tmStartup();

    // We have our memory for a context, but Telemetry needs to initialize it
    tmInitializeContext( &cx, context_buffer, sizeof( context_buffer ) );
    tmEnable( cx, TMO_OUTPUT_DEBUG_INFO, 1 );
    tmSetParameter( cx, TMP_DEBUG_PRINTER, ( void * ) debug_printer );

    // Connect to server at the default port.  We also tell tmOpen to initialize any 
    // OS networking services.  We specify an indefinite timeout.
    int error = 1234;
    char const *build_info = "This is a very long comment that you would include as part of your build info.  This information\
 might include username, more system information, and other data that might be relevant to the particular recorded session!";

    tmSetParameter( cx, TMP_USERCB_DATA, &callback_data );
    tmSetParameter( cx, TMP_USERCB_CLOSE, ( void * ) user_close );
    tmSetParameter( cx, TMP_USERCB_OPEN, ( void * ) user_open );
    tmSetParameter( cx, TMP_USERCB_WRITE, ( void * ) user_write );

    if ( (error = tmOpen( cx, EXAMPLE_NAME, build_info, kpHost, TMCT_USER_PROVIDED,
        TELEMETRY_DEFAULT_PORT, TMOF_INIT_NETWORKING, -1 )) != TM_OK )
    {
        printf( "...failed (%d), exiting\n", error );
        return 1;
    }

    tmEnable( cx, TMO_SUPPORT_MEMORY, 0 );
    printf( "%d\n", tmIsEnabled( cx, TMO_SUPPORT_MEMORY ) );

    tmEnter( cx, TMZF_NONE, "BIG LOOP" );
    for ( int i = 0; i < 300; i++ )
    {
        // C++ style zone definition object.  tmEnter is called at declaration and tmLeave is called when
        // when the underlying object goes out of scope and its destructor is called
        tmZone( cx, TMZF_NONE, "loop %d", i );

        for ( int j = 0; j < 10000; j++ )
        {
            tmZone( cx, TMZF_NONE, "stuff" );
        }
        // Normal zone definition
        tmEnter( cx, TMZF_NONE, "stuff" );
        mysleep(5);
        tmLeave( cx );

        // Idle zone definition
        tmEnter( cx, TMZF_IDLE, "sleeping" );
        mysleep(5);
        tmLeave( cx );

        // Do our tick
        tmTick(cx);
    }
    tmLeave( cx );

    // Close our connection
    tmClose( cx );

    // Shutdown the context
    tmShutdownContext( cx );

    // Unload Telemetry
    tmShutdown();

    return 0;
}

// @cdep pre $DefaultsTMExample
// @cdep post $if($equals($buildplatform,linuxARM32), $requiresbinary(-lrt), )
// @cdep post $BuildTMExample 
