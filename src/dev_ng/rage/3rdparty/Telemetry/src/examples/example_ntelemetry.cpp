#define NTELEMETRY 1
#ifdef _MSC_VER
#pragma warning(disable:4127)
#endif

#include "telemetry.h"
#include <stdio.h>

static HTELEMETRY cx;

#ifndef NTELEMETRY
static TmU8 context_buffer[ 2* 1024 * 1024 ]; // Static context buffer
#endif

#ifdef _SEKRIT
int main(Platform::Array<Platform::String^>^ args)
#else
int main( int argc, char **argv )
#endif
{
    char const *kpHost = "localhost";

    if ( !tmLoadTelemetry( 1 ) )
    {
        printf( "Could not load telemetry\n" );
        return -1;
    }

    // Startup Telemetry
    tmStartup();

    // We have our memory for a context, but Telemetry needs to initialize it
    tmInitializeContext( &cx, context_buffer, sizeof( context_buffer ) );

    // Connect to server at the default port.  We also tell tmOpen to initialize any 
    // OS networking services.  We specify an indefinite timeout.
    printf( "Connecting to %s\n", kpHost );
    int error = 1234;

    if ( (error = tmOpen( cx, "Example NTELEMETRY", "No build info", kpHost, TMCT_FILE,
        TELEMETRY_DEFAULT_PORT, TMOF_INIT_NETWORKING, -1 )) != TM_OK )
    {
        printf( "...failed (%d), exiting\n", error );
        return 1;
    }

    void *p = new char[ 16 ];
    tmAlloc( cx, p, s, 16 );

    tmFree( cx, p );
    delete p;

    tmEnter( cx, TMZF_NONE, "BIG LOOP" );
    for ( int i = 0; i < 30; i++ )
    {
        tmZone( cx, TMZF_NONE, "loop %d", i );

        printf( "loop %d\n", i );
    }
    tmLeave( cx );

    // Close our connection
    tmClose( cx );

    // Shutdown the context
    tmShutdownContext( cx );

    // Unload Telemetry
    tmShutdown();

    return 0;
}

// @cdep pre $DefaultsTMExample
// @cdep post $BuildTMExample 
