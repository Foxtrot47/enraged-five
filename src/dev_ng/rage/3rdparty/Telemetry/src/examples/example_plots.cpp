#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "telemetry.h"
#include "telemetry_example.h"

/* DEFTUTORIAL EXPGROUP(TutorialPlots) (TutorialPlots_Intro,Introduction) */
/* 
This is the most basic example of generating plots in a Telemetrized application.
$-
*/

char const *EXAMPLE_NAME = "(examples/simple)example_plots";
static HTELEMETRY cx;
static TmU8 context_buffer[ 1024 * 1024 ]; // Static context buffer

#ifdef TELEMETRY2
#define PLOTPATH(a,b) "(" a ")" b
#else
#define PLOTPATH(a,b) a "/" b
#endif

/* DEFTUTORIAL EXPGROUP(TutorialPlots) (TutorialPlots_main, main) */
int test_main(int argc, char **argv)
{
    // First we load the Telemetry DLLs.  0 parameter means 'not the checked build'
    tmLoadTelemetry(CHECKED_BUILD);

    // Startup Telemetry
    tmStartup();

    // We have our memory for a context, but Telemetry needs to initialize it
    tmInitializeContext( &cx, context_buffer, sizeof( context_buffer ) );
 
    // Connect to server at the default port.  We also tell tmOpen to initialize any 
    // OS networking services.  We specify an indefinite timeout.
    if ( tmOpen( cx, EXAMPLE_NAME, __DATE__ " " __TIME__, kpHost, TEST_INTERFACE_TYPE,  TELEMETRY_DEFAULT_PORT, TMOF_INIT_NETWORKING, -1 ) != TM_OK )
    {
        printf( "...failed, exiting\n" );
        return 1;
    }

    int const kNumLoops = 3000;

    for ( int i = 0; i < kNumLoops; i++ )
    {
        // C++ style zone definition object.  tmEnter is called at declaration and tmLeave is called when
        // when the underlying object goes out of scope and its destructor is called
        tmZone( cx, TMZF_IDLE, "loop %d", i );

        // Plot a sine and cosine.  There is no path so they'll
        // show up on the top level of the plot tree.
        tmPlotF64( cx, TMPT_NONE, 0, sin((float)i/10), "sine" );

        tmPlotF64( cx, TMPT_NONE, 0, cos((float)i/10), "cosine" );
        tmPlotF64( cx, TMPT_NONE, 0, cos((float)i/3)+sin((float)i/10), "cosine 2" );

        // Hex value of the frame number
        tmPlotU32( cx, TMPT_HEX, 0, i, PLOTPATH( "game", "hex frame number" ) );

        tmPlotU32( cx, TMPT_PERCENTAGE_COMPUTED, 0, i, PLOTPATH("game","frame number as percentage computed" ) );

        tmPlotF32( cx, TMPT_PERCENTAGE_DIRECT, 0, float(i)/kNumLoops, PLOTPATH("game", "frame number as direct percentage" ) );
        tmPlotF64( cx, TMPT_PERCENTAGE_DIRECT, 0, double(i)/kNumLoops, PLOTPATH( "game", "frame number as direct percentage double" ) );
        tmPlotU32( cx, TMPT_PERCENTAGE_DIRECT, 0, ( i * 100 ) /kNumLoops, PLOTPATH( "game", "frame number as direct percentage u32" ) );
        tmPlotI32( cx, TMPT_PERCENTAGE_DIRECT, 0, ( i * 100 ) /kNumLoops, PLOTPATH( "game", "frame number as direct percentage i32" ) );

        // Value with negative and positive values
        tmPlotI32( cx, TMPT_INTEGER, 0, i-50, PLOTPATH("game","signed frame number(frame)" ) );

        // Tie two plots into the same scale
        tmPlot( cx, TMPT_INTEGER, 0, (float)i*i, PLOTPATH( "game", "frame^2(frame)" ) );
        tmPlot( cx, TMPT_INTEGER, 0, (float)i, PLOTPATH( "game", "frame number(frame)" ) );

        // We can format things as memory.  These should show up as
        // bytes, KB, and MB under their own "mem" node of the 
        // plot tree in the timeline view.
        tmPlot( cx, TMPT_MEMORY, 0, (float)i, PLOTPATH("mem","mem bytes" ) );
        tmPlot( cx, TMPT_MEMORY, 0, (float)i*1024, PLOTPATH( "mem", "mem kbytes//second" ) ); // Note that escaped double slashes
        tmPlot( cx, TMPT_MEMORY, 0, (float)i*1024*1024, PLOTPATH( "mem", "mem mbytes" ) );

        // Plot out very large values
        tmPlotU64( cx, TMPT_HEX, 0, ( TmU64 ) i << 32, "big number" );

        // Do a very fast triangle wave
        switch ( i & 3 )
        {
        case 0:
            tmPlotI32( cx, TMPT_INTEGER, 0, 0, "fast" );
            break;
        case 1:
        case 3:
            tmPlotI32( cx, TMPT_INTEGER, 0, 1, "fast" );
            break;
        case 2:
            tmPlotI32( cx, TMPT_INTEGER, 0, 2, "fast" );
            break;
        }

        // Example of using varargs
        tmPlot( cx, TMPT_NONE, 0, (float)(tmFastTime()&0xFFFF), "%s%s%s%d", "Random", "Number","Here", 42 );

        // Sleep some 'random' amount
        tmEnter( cx, TMZF_PLOT_TIME_EXPERIMENTAL, "plot zone time!" );
        mysleep(1);
        tmLeave( cx );

        // Do our tick
        tmTick(cx);
    }

    // Close our connection
    tmClose( cx );

    // Shutdown the context
    tmShutdownContext( cx );

    // Unload Telemetry
    tmShutdown();

    return 0;
}

/* DEFTUTORIALEND */
// @cdep pre $DefaultsTMExample
// @cdep post $BuildTMExample 
