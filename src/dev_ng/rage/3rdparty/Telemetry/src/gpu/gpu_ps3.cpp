#include <cell/gcm.h>
#include <sys/timer.h>
#include <cell/spurs.h>

#include <telemetry.h>
#include <tmxgpu.h>

#define MAX_QUERY_COUNT		32

#define TMXGPU_ENABLE_SYNC 0x1

struct TmxGpuContextPS3
{
	HTELEMETRY cx;
	TmxGpuFunctions api;

	struct Query
	{
		TmU32 preQuery;
		TmU32 postQuery;
		const char *name; // NULL means unused
		TmU32 index;
	};

	TmU64 options;

	int queryCount;
	int queryCurrent;

	TmU32 fenceLabel;
	TmU32 volatile* fencePtr;
	TmU32 fenceNextIndex;

	Query queries[MAX_QUERY_COUNT];

	TmI64 gpuToCpuTimeOffset;
	TmF64 gpuToCpuTimeRatio;

	TmxGpuContextPS3() { }

	void tick()
	{
		if (!cx) return;

		resolveAllQueries();
	}

	void reset()
	{
		if (!cx) return;
	}

	void enable(TmxGpuOption option, int value)
	{
		if (!cx) return;

		if (option == TMXGPU_SYNC) {
			resolveAllQueries();

			if (value)
				options |= TMXGPU_ENABLE_SYNC;
			else
				options &= ~TMXGPU_ENABLE_SYNC;
		}
	}

	void drawCallBegin(const char *nameFormat, va_list *args)
	{
		if (!cx) return;

		Query *query = makeQuery();
		gcmQueryIssue(query->preQuery);

		char buffer[1024];
		vsprintf_s(buffer, 1024, nameFormat, *args);
		query->name = tmDynamicString(cx, buffer);
	}

	void drawCallEnd()
	{
		if (!cx) return;

		gcmQueryIssue(queries[queryCurrent].postQuery);
		putFenceQuery(&queries[queryCurrent]);
		maybeResolveQuery(&queries[queryCurrent]);
	}

	TmU64 convertTime(TmU64 gpuTimestamp)
	{
		return gpuToCpuTimeOffset + TmU64(TmF64(gpuTimestamp) * gpuToCpuTimeRatio);
	}

	void gcmQueryIssue(TmU32 query)
	{
		cellGcmSetTimeStamp(gCellGcmCurrentContext, query);
	}

	TmU64 gcmQueryWait(TmU32 query)
	{
		return cellGcmGetTimeStamp(query);
	}

	void gcmWaitForIdle()
	{
		cellGcmSetWaitForIdle(gCellGcmCurrentContext);
	}

	TmU64 gcmQueryIssueAndWait(TmU32 query)
	{
		gcmQueryIssue(query);
		gcmWaitForIdle();
		return gcmQueryWait(query);
	}

	void gcmFlush()
	{
		cellGcmFlush(gCellGcmCurrentContext);
	}

	void waitForQuery(Query *query)
	{
		gcmFlush();
		while (*fencePtr < query->index)
			sys_timer_usleep(30);
	}

	void resolveQuery(Query* query)
	{
		if (!query->name)
			return;

		waitForQuery(query);

		TmU64 preTime = gcmQueryWait(query->preQuery);
		TmU64 postTime = gcmQueryWait(query->postQuery);

		TmU64 cpuPreTime = convertTime(preTime);
		TmU64 cpuPostTime = convertTime(postTime);

		tmBeginTimeSpanAt(cx, 1, 0, convertTime(preTime), "Draw call: %s", query->name);
		tmEndTimeSpanAt(cx, 1, 0, convertTime(postTime), "Draw call: %s", query->name);

		query->name = NULL;
	}

	void maybeResolveQuery(Query* query)
	{
		if (options & TMXGPU_ENABLE_SYNC) {
			gcmWaitForIdle();
			resolveQuery(query);
		}
	}

	void resolveAllQueries()
	{
		for (int i = 0; i < queryCount; i++)
			resolveQuery(&queries[i]);
	}

	Query *makeQuery()
	{
		queryCurrent++;
		if (queryCurrent == queryCount)
			queryCurrent = 0;

		Query *query = &queries[queryCurrent];
		if (query->name)
			resolveQuery(query);

		return query;
	}

	void putFenceQuery(Query *query)
	{
		query->index = ++fenceNextIndex;
		cellGcmSetWriteBackEndLabel(gCellGcmCurrentContext, fenceLabel, query->index);
	}

	void calibrateTiming()
	{
		Query *query = makeQuery();

		const TmU64 kStartCpuTimestamp = tmFastTime();
		gcmQueryIssue(query->preQuery);
		gcmFlush();

		sys_timer_usleep(100000); // 100 milliseconds

		const TmU64 kEndCpuTimestamp = tmFastTime();
		gcmQueryIssue(query->postQuery);

		putFenceQuery(query);
		waitForQuery(query);

		const TmU64 kStartGpuTimestamp = gcmQueryIssueAndWait(query->preQuery);
		const TmU64 kEndGpuTimestamp = gcmQueryIssueAndWait(query->postQuery);

		// Compare CPU and GPU timestamps to find the offset and frequency ratio.
		gpuToCpuTimeRatio = TmF64(kEndCpuTimestamp - kStartCpuTimestamp) / TmF64(kEndGpuTimestamp - kStartGpuTimestamp);
		gpuToCpuTimeOffset = kStartCpuTimestamp - TmU64(kStartGpuTimestamp * gpuToCpuTimeRatio);
	}
};

extern "C" {

#define BEGIN_WRAPPER(rettype, func, ...) static rettype func (TmxGpuContext* cx_, ##__VA_ARGS__) { TmxGpuContextPS3* cx = (TmxGpuContextPS3*) cx_;
#define END_WRAPPER }

BEGIN_WRAPPER(void, tick)
	cx->tick();
END_WRAPPER

BEGIN_WRAPPER(void, reset)
	cx->reset();
END_WRAPPER

BEGIN_WRAPPER(void, enable, TmxGpuOption option, int value)
    cx->enable(option, value);
END_WRAPPER

BEGIN_WRAPPER(void, drawCallBegin, const char *nameFormat, ...)
	va_list args;
	va_start(args, nameFormat);
	cx->drawCallBegin(nameFormat, &args);
	va_end(args);
END_WRAPPER

BEGIN_WRAPPER(void, drawCallEnd)
	cx->drawCallEnd();
END_WRAPPER

#undef END_WRAPPER
#undef BEGIN_WRAPPER

TmxGpuErrorCode tmxGpuInitPS3(TmxGpuContext* dest, HTELEMETRY tmcx, void* device, TmU32, int queryCount, void *queryResources)
{
	TmxGpuContextPS3* cx = (TmxGpuContextPS3*) dest;

	if (!tmcx) {
		cx->cx = NULL;
		return TMXERR_GPU_INVALID_CONTEXT;
	}

	if (queryCount > 2*MAX_QUERY_COUNT || queryCount % 2 != 0)
		return TMXERR_GPU_INSUFFICIENT_RESOURCES;

	if (!queryResources)
		return TMXERR_GPU_INSUFFICIENT_RESOURCES;

	cx->api.tick = tick;
	cx->api.reset = reset;
	cx->api.enable = enable;
	cx->api.drawCallBegin = drawCallBegin;
	cx->api.drawCallEnd = drawCallEnd;

	cx->cx = tmcx;
	cx->options = 0;

	TmU32* labels = (TmU32*) queryResources;

	cx->fenceLabel = labels[0];
	cx->fencePtr = cellGcmGetLabelAddress(cx->fenceLabel);
	cx->fenceNextIndex = 0;
	*cx->fencePtr = cx->fenceNextIndex;

	queryCount -= 2;
	labels += 2;

	cx->queryCurrent = 0;
	cx->queryCount = queryCount / 2;
	for (int i = 0; i < cx->queryCount; i++) {
		cx->queries[i].preQuery = *labels++;
		cx->queries[i].postQuery = *labels++;
		cx->queries[i].name = NULL;
	}

	cx->calibrateTiming();

	return TMX_OK;
}

}
