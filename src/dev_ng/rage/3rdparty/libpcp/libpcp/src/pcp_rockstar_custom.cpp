#include "pcp_rockstar_custom.h"

#if NET_ENABLE_PCP

#if RSG_WIN32
#pragma warning(push)
#pragma warning(disable: 4668)
#include <vadefs.h>
#endif

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#if RSG_WIN32
#pragma warning(pop)
#endif

#if RSG_ORBIS
#ifdef __cplusplus
	extern "C" {
#endif

int
IN6_IS_ADDR_V4MAPPED(const IN6_ADDR *a)
{
	return (int)((a->s6_words[0] == 0) &&
		(a->s6_words[1] == 0) &&
		(a->s6_words[2] == 0) &&
		(a->s6_words[3] == 0) &&
		(a->s6_words[4] == 0) &&
		(a->s6_words[5] == 0xffff));
}

int
IN6_IS_ADDR_LOOPBACK(const IN6_ADDR *a)
{
    return (int)((a->s6_words[0] == 0) &&
				(a->s6_words[1] == 0) &&
				(a->s6_words[2] == 0) &&
				(a->s6_words[3] == 0) &&
				(a->s6_words[4] == 0) &&
				(a->s6_words[5] == 0) &&
				(a->s6_words[6] == 0) &&
				(a->s6_words[7] == 0x0100));
}

int
IN6_IS_ADDR_UNSPECIFIED(const IN6_ADDR *a)
{
    //
    // We can't use the in6addr_any variable, since that would
    // require existing callers to link with a specific library.
    //
    return (int)((a->s6_words[0] == 0) &&
				(a->s6_words[1] == 0) &&
				(a->s6_words[2] == 0) &&
				(a->s6_words[3] == 0) &&
				(a->s6_words[4] == 0) &&
				(a->s6_words[5] == 0) &&
				(a->s6_words[6] == 0) &&
				(a->s6_words[7] == 0));
}

int
IN6_ADDR_EQUAL(const IN6_ADDR *x, const IN6_ADDR *y)
{
	return (memcmp(x, y, sizeof(IN6_ADDR)) == 0);
}

#ifdef __cplusplus
}
#endif

#endif // RSG_ORBIS

#endif // NET_ENABLE_PCP
