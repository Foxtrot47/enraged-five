#ifndef PCP_ROCKSTAR_CUSTOM_H_INCLUDED
#define PCP_ROCKSTAR_CUSTOM_H_INCLUDED

#include "file/file_config.h"

// disabled NAT-PMP/PCP on Xbox One. Requires a custom lib from Microsoft that GTA V doesn't have.
#define NET_ENABLE_PCP ((RSG_PC || RSG_ORBIS || (0 && RSG_LINUX)) && !__RGSC_DLL && !RSG_MOBILE && !__TOOL && !__RESOURCECOMPILER)

#if NET_ENABLE_PCP

#ifdef _MSC_VER
#pragma warning(disable: 4996)  // 'WSAAddressToStringA': Use WSAAddressToStringW() instead or define _WINSOCK_DEPRECATED_NO_WARNINGS to disable deprecated API warnings
#endif

#include "system/xtl.h"

#ifdef __cplusplus
extern "C" {
#endif

#if RSG_LINUX
	typedef unsigned short ADDRESS_FAMILY;

#elif RSG_ORBIS
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net.h>
#include <rtc.h>
#include <string.h>
#include <time.h>
#include <arpa/inet.h>

#define HAVE_GETTIMEOFDAY (1)

//
// IPv6 Internet address (RFC 2553)
// This is an 'on-wire' format structure.
//
typedef struct in6_addr {
	union {
		unsigned char Byte[16];
		unsigned short Word[8];
	} u;
} IN6_ADDR;

#define in_addr6 in6_addr

//
// Defines to match RFC 2553.
//
#define _S6_un      u
#define _S6_u8      Byte
#define s6_addr     _S6_un._S6_u8

//
// Defines for our implementation.
//
#define s6_bytes    u.Byte
#define s6_words    u.Word

typedef struct sockaddr_in6 {
	short   sin6_family;        /* AF_INET6 */
	unsigned short sin6_port;          /* Transport level port number */
	unsigned long  sin6_flowinfo;      /* IPv6 flow information */
	struct in6_addr sin6_addr;  /* IPv6 address */
	unsigned long sin6_scope_id;       /* set of interfaces for a scope */
} SOCKADDR_IN6;

typedef unsigned short ADDRESS_FAMILY;


#define IN6_ARE_ADDR_EQUAL IN6_ADDR_EQUAL

#define IN6ADDR_ANY_INIT { 0 }

#define IPPROTO_IPV6 (41)
#define IPV6_V6ONLY (27)

int IN6_IS_ADDR_V4MAPPED(const IN6_ADDR *a);
int IN6_IS_ADDR_LOOPBACK(const IN6_ADDR *a);
int IN6_IS_ADDR_UNSPECIFIED(const IN6_ADDR *a);
int IN6_ADDR_EQUAL(const IN6_ADDR *x, const IN6_ADDR *y);
#endif

#ifdef __cplusplus
}
#endif

#endif // NET_ENABLE_PCP

#endif // PCP_ROCKSTAR_CUSTOM_H_INCLUDED
