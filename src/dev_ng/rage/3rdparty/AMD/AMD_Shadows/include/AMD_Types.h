//
// Copyright 2014 ADVANCED MICRO DEVICES, INC.  All Rights Reserved.
//
// AMD is granting you permission to use this software and documentation (if
// any) (collectively, the “Materials”) pursuant to the terms and conditions
// of the Software License Agreement included with the Materials.  If you do
// not have a copy of the Software License Agreement, contact your AMD
// representative for a copy.
// You agree that you will not reverse engineer or decompile the Materials,
// in whole or in part, except as allowed by applicable law.
//
// WARRANTY DISCLAIMER: THE SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND.  AMD DISCLAIMS ALL WARRANTIES, EXPRESS, IMPLIED, OR STATUTORY,
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, THAT THE SOFTWARE
// WILL RUN UNINTERRUPTED OR ERROR-FREE OR WARRANTIES ARISING FROM CUSTOM OF
// TRADE OR COURSE OF USAGE.  THE ENTIRE RISK ASSOCIATED WITH THE USE OF THE
// SOFTWARE IS ASSUMED BY YOU.
// Some jurisdictions do not allow the exclusion of implied warranties, so
// the above exclusion may not apply to You.
//
// LIMITATION OF LIABILITY AND INDEMNIFICATION:  AMD AND ITS LICENSORS WILL
// NOT, UNDER ANY CIRCUMSTANCES BE LIABLE TO YOU FOR ANY PUNITIVE, DIRECT,
// INCIDENTAL, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING FROM USE OF
// THE SOFTWARE OR THIS AGREEMENT EVEN IF AMD AND ITS LICENSORS HAVE BEEN
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// In no event shall AMD's total liability to You for all damages, losses,
// and causes of action (whether in contract, tort (including negligence) or
// otherwise) exceed the amount of $100 USD.  You agree to defend, indemnify
// and hold harmless AMD and its licensors, and any of their directors,
// officers, employees, affiliates or agents from and against any and all
// loss, damage, liability and other expenses (including reasonable attorneys'
// fees), resulting from Your use of the Software or violation of the terms and
// conditions of this Agreement.
//
// U.S. GOVERNMENT RESTRICTED RIGHTS: The Materials are provided with "RESTRICTED
// RIGHTS." Use, duplication, or disclosure by the Government is subject to the
// restrictions as set forth in FAR 52.227-14 and DFAR252.227-7013, et seq., or
// its successor.  Use of the Materials by the Government constitutes
// acknowledgement of AMD's proprietary rights in them.
//
// EXPORT RESTRICTIONS: The Materials may be subject to export restrictions as
// stated in the Software License Agreement.
//

#ifndef __AMD_TYPES_H__
#define __AMD_TYPES_H__

namespace AMD
{
  typedef          int   sint32;
  typedef          short sint16;
  typedef          char  sint8;
  typedef          int   sint;
  typedef          short sshort;
  typedef          char  sbyte;

  typedef unsigned int   uint32;
  typedef unsigned short uint16;
  typedef unsigned char  uint8;
  typedef unsigned int   uint;
  typedef unsigned short ushort;
  typedef unsigned char  ubyte;

  // The common return codes
  typedef enum _RETURN_CODE
  {
    RETURN_CODE_SUCCESS,
    RETURN_CODE_FAIL,
    RETURN_CODE_INVALID_DEVICE,
    RETURN_CODE_INVALID_DEVICE_CONTEXT,
    RETURN_CODE_COUNT,
  } RETURN_CODE;
}

#ifndef AMD_DECLARE_BASIC_VECTOR_TYPE
# define AMD_DECLARE_BASIC_VECTOR_TYPE                                                 \
  struct float2   { union { struct { float x,y; }; float v[2]; }; };                   \
  struct float3   { union { struct { float x,y,z; }; float v[3]; }; };                 \
  struct float4   { union { struct { float x,y,z,w; }; float v[4]; }; };               \
  struct float4x4 { union { float4 r[4]; float m[16]; }; };                            \
  struct uint2    { union { struct { unsigned int x,y; }; unsigned int v[2]; }; };     \
  struct uint3    { union { struct { unsigned int x,y,z; }; unsigned int v[3]; }; };   \
  struct uint4    { union { struct { unsigned int x,y,z,w; }; unsigned int v[4]; }; }; \
  typedef unsigned int uint;
#endif // AMD_DECLARE_BASIC_VECTOR_TYPE

#ifndef AMD_DECLARE_CAMERA_TYPE
# define AMD_DECLARE_CAMERA_TYPE                                                      \
  typedef struct _CAMERA                                                              \
  {                                                                                   \
    float4x4                           m_View;                                        \
    float4x4                           m_Projection;                                  \
    float4x4                           m_ViewProjection;                              \
    float4x4                           m_View_Inv;                                    \
    float4x4                           m_Projection_Inv;                              \
    float4x4                           m_ViewProjection_Inv;                          \
    float3                             m_Position;                                    \
    float                              m_Fov;                                         \
    float3                             m_Direction;                                   \
    float                              m_FarPlane;                                    \
    float3                             m_Right;                                       \
    float                              m_NearPlane;                                   \
    float3                             m_Up;                                          \
    float                              m_Aspect;                                      \
    float4                             m_Color;                                       \
  } CAMERA;
#endif // AMD_DECLARE_CAMERA_TYPE

#ifndef AMD_SAFE_DELETE
#define AMD_SAFE_DELETE(p)        { delete (p);     (p) = nullptr; }
#endif
#ifndef AMD_SAFE_DELETE_ARRAY
#define AMD_SAFE_DELETE_ARRAY(p)  { delete[] (p);   (p) = nullptr; }
#endif
#ifndef AMD_SAFE_RELEASE
#define AMD_SAFE_RELEASE(p)       { if (p) { (p)->Release(); } (p) = nullptr; }
#endif

#define AMD_FUNCTION_WIDEN2(x)    L ## x
#define AMD_FUNCTION_WIDEN(x)     AMD_FUNCTION_WIDEN2(x)
#define AMD_FUNCTION_WIDE_NAME    AMD_FUNCTION_WIDEN(__FUNCTION__)

#define AMD_PITCHED_SIZE(x, y)    ( (x+y-1) / y )

#define AMD_ARRAY_SIZE( arr )     ( sizeof(arr) / sizeof(arr[0]) )

#define AMD_PI                    3.141592654f
#define AMD_ROT_ANGLE             ( AMD_PI / 180.f )

#endif //_AMD_TYPES_H_