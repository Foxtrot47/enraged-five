//
// Copyright 2014 ADVANCED MICRO DEVICES, INC.  All Rights Reserved.
//
// AMD is granting you permission to use this software and documentation (if
// any) (collectively, the “Materials”) pursuant to the terms and conditions
// of the Software License Agreement included with the Materials.  If you do
// not have a copy of the Software License Agreement, contact your AMD
// representative for a copy.
// You agree that you will not reverse engineer or decompile the Materials,
// in whole or in part, except as allowed by applicable law.
//
// WARRANTY DISCLAIMER: THE SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND.  AMD DISCLAIMS ALL WARRANTIES, EXPRESS, IMPLIED, OR STATUTORY,
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, THAT THE SOFTWARE
// WILL RUN UNINTERRUPTED OR ERROR-FREE OR WARRANTIES ARISING FROM CUSTOM OF
// TRADE OR COURSE OF USAGE.  THE ENTIRE RISK ASSOCIATED WITH THE USE OF THE
// SOFTWARE IS ASSUMED BY YOU.
// Some jurisdictions do not allow the exclusion of implied warranties, so
// the above exclusion may not apply to You.
//
// LIMITATION OF LIABILITY AND INDEMNIFICATION:  AMD AND ITS LICENSORS WILL
// NOT, UNDER ANY CIRCUMSTANCES BE LIABLE TO YOU FOR ANY PUNITIVE, DIRECT,
// INCIDENTAL, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING FROM USE OF
// THE SOFTWARE OR THIS AGREEMENT EVEN IF AMD AND ITS LICENSORS HAVE BEEN
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
// In no event shall AMD's total liability to You for all damages, losses,
// and causes of action (whether in contract, tort (including negligence) or
// otherwise) exceed the amount of $100 USD.  You agree to defend, indemnify
// and hold harmless AMD and its licensors, and any of their directors,
// officers, employees, affiliates or agents from and against any and all
// loss, damage, liability and other expenses (including reasonable attorneys'
// fees), resulting from Your use of the Software or violation of the terms and
// conditions of this Agreement.
//
// U.S. GOVERNMENT RESTRICTED RIGHTS: The Materials are provided with "RESTRICTED
// RIGHTS." Use, duplication, or disclosure by the Government is subject to the
// restrictions as set forth in FAR 52.227-14 and DFAR252.227-7013, et seq., or
// its successor.  Use of the Materials by the Government constitutes
// acknowledgement of AMD's proprietary rights in them.
//
// EXPORT RESTRICTIONS: The Materials may be subject to export restrictions as
// stated in the Software License Agreement.
//

#ifndef __AMD_SHADOWS_H__
#define __AMD_SHADOWS_H__

#define AMD_SHADOWS_VERSION_MAJOR                    1
#define AMD_SHADOWS_VERSION_MINOR                    2

#define AMD_SHADOWS_COMPILE_STATIC_LIB

#ifdef AMD_SHADOWS_COMPILE_STATIC_LIB
#define AMD_SHADOWS_DLL_API
#else // AMD_COMPILE_STATIC
#ifdef AMD_DLL_EXPORTS
#define AMD_SHADOWS_DLL_API __declspec(dllexport)
#else // AMD_DLL_EXPORTS
#define AMD_SHADOWS_DLL_API __declspec(dllimport)
#endif // AMD_DLL_EXPORTS
#endif // AMD_COMPILE_STATIC

#include "AMD_Types.h" 

namespace AMD
{
  // The Return codes
  typedef enum _SHADOWS_RETURN_CODE
  {
    SHADOWS_RETURN_CODE_SUCCESS,
    SHADOWS_RETURN_CODE_FAIL,
    SHADOWS_RETURN_CODE_INVALID_DEVICE,
    SHADOWS_RETURN_CODE_INVALID_DEVICE_CONTEXT,
    SHADOWS_RETURN_CODE_COUNT,
  } SHADOWS_RETURN_CODE;

  // The CHS Filtering types
  typedef enum _SHADOWS_FILTERING
  {
    SHADOWS_FILTERING_UNIFORM              = 0,
    SHADOWS_FILTERING_CONTACT_HARDENING    = 1,
    SHADOWS_FILTERING_COUNT                = 2,
  } SHADOWS_FILTERING;

  typedef enum _SHADOWS_NORMAL_OPTION
  {
    SHADOWS_NORMAL_OPTION_NONE            = 0,
    SHADOWS_NORMAL_OPTION_READ_FROM_SRV   = 1,
    SHADOWS_NORMAL_OPTION_COUNT           = 2,
  } SHADOWS_NORMAL_OPTION;

  typedef enum _SHADOWS_RADIUS
  {
    SHADOWS_RADIUS_7                       = 7,
    SHADOWS_RADIUS_9                       = 9,
    SHADOWS_RADIUS_11                      = 11,
    //SHADOWS_RADIUS_13                      = 13,
    SHADOWS_RADIUS_15                      = 15,
    SHADOWS_RADIUS_COUNT                   = 4,
  } SHADOWS_RADIUS;

  typedef enum _SHADOWS_OUTPUT_CHANNEL
  {
    SHADOWS_OUTPUT_CHANNEL_R               = 1,
    SHADOWS_OUTPUT_CHANNEL_G               = 2,
    SHADOWS_OUTPUT_CHANNEL_B               = 4,
    SHADOWS_OUTPUT_CHANNEL_A               = 8,
    SHADOWS_OUTPUT_CHANNEL_COUNT           = 16
  } SHADOWS_OUTPUT_CHANNEL;

  struct SHADOWS_OPAQUE_DESC;

  struct SHADOWS_DESC
  {
    AMD_DECLARE_BASIC_VECTOR_TYPE;
    AMD_DECLARE_CAMERA_TYPE;

    typedef struct _PARAMETERS
    {
      CAMERA                                m_Viewer;
      CAMERA                                m_Light;
      float4                                m_DepthSize;
      float4                                m_ShadowSize;
      float4                                m_ShadowRegion;

      float                                 m_SunWidth;
      float                                 m_DepthTestOffset;
      float                                 m_NormalOffsetScale;
      uint                                  _pad;
    } PARAMETERS;

    typedef struct _UNIT_CUBE_PARAMETERS
    {
      float4x4                              m_Transform;
      float4                                m_Color;
      float                                 m_Scale;
      uint                                  _pad[3];
    } UNIT_CUBE_PARAMETERS;

    SHADOWS_OPAQUE_DESC*                    m_pOpaque;

    ID3D11Device*                           m_pDevice;
    ID3D11DeviceContext*                    m_pContext;

    bool                                    m_EnableVerboseLogging;
    bool                                    m_EnableCapture;
    bool                                    m_EnableStateGuard;
    bool                                    m_EnableOutputChannelFlags;
    bool                                    m_EnableStencilMasking;
    int                                     m_CascadeIndex;
    int                                     m_NumCascades;

    PARAMETERS                              m_Parameters;
    UNIT_CUBE_PARAMETERS                    m_UnitCubeParameters;
    UNIT_CUBE_PARAMETERS                    m_PreviousUnitCubeParameters;

    SHADOWS_FILTERING                       m_Filtering;
    SHADOWS_RADIUS                          m_Radius;

    ID3D11ShaderResourceView*               m_pSrvDepth;  // input: scene zbuffer
    ID3D11ShaderResourceView*               m_pSrvShadow; // input: shadow map
    ID3D11ShaderResourceView*               m_pSrvNormal; // input: optional scene normals (for Deferred Renderers)
    ID3D11RenderTargetView*                 m_pRtvOutput; // output: shadow results

    ID3D11DepthStencilView*                 m_pDsvOutput;     // read-only depth-stencil view for the scene
    ID3D11DepthStencilState*                m_pDssOutput;     // can specify stencil test when applying shadows
    unsigned int                            m_DssReference;   // reference value for stencil test

    ID3D11DepthStencilState*                m_pDssCleanUpStencil; // optional cleanup pass after last cascade

    ID3D11DepthStencilView*                 m_pDsvStencilMasking;   // depth-stencil view for the scene (not read-only)
    ID3D11DepthStencilState*                m_pDssStencilMasking;   // dss to mark scene pixels for shadows

    ID3D11BlendState*                       m_pBsOutput;      // output bs can specify how to write to rtv
    unsigned int                            m_OutputChannels; // output channels flags (not used if bs != null or m_EnableOutputChannelFlags = false)

    AMD_SHADOWS_DLL_API                     SHADOWS_DESC();

    AMD_SHADOWS_DLL_API SHADOWS_RETURN_CODE Serialize(const char * params) const;

    AMD_SHADOWS_DLL_API SHADOWS_RETURN_CODE Deserialize(const char * params,
                                                        ID3D11Texture2D ** ppT2D[],
                                                        ID3D11ShaderResourceView** ppSRV[]);
  };

  extern "C"
  {
    AMD_SHADOWS_DLL_API SHADOWS_RETURN_CODE SHADOWS_Initialize  (const SHADOWS_DESC & desc);
    AMD_SHADOWS_DLL_API SHADOWS_RETURN_CODE SHADOWS_Render      (const SHADOWS_DESC & desc);
    AMD_SHADOWS_DLL_API SHADOWS_RETURN_CODE SHADOWS_Release     (const SHADOWS_DESC & desc);
  }

}

#endif