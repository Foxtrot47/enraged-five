//-----------------------------------------------------------------------------
// File: AGS Sample.cpp
// Copyright (c) 2008 - 2009 Advanced Micro Devices, Inc. All rights reserved.
//-----------------------------------------------------------------------------

#include <iostream>
#include <tchar.h>
#include <conio.h>

#include <amd_ags.h>

int _tmain(int argc, _TCHAR* argv[])
{
	// iOSDisplayIndex maps to the iDevNum input variable used by Windows to identify a display using the EnumDisplayDevices() API
	int iOSDisplayIndex = 0;
	
	if ( AGS_SUCCESS != AGSInit() )
	{
		printf ( "\nFailed to initialize AGS Library\n" );
		_getch();
	}

	// Get the index for the default display
	if ( AGS_SUCCESS != AGSGetDefaultDisplayIndex( &iOSDisplayIndex ) )
	{
		printf ( "\nFailed to get default display index.\n" );
		_getch();
	}

	// Get driver version info
	{
		AGSDriverVersionInfoStruct driverVersionInfo = { NULL };
		
		if ( AGS_SUCCESS == AGSDriverGetVersionInfo( &driverVersionInfo ) )
		{
			printf ( "\nDRIVER version info.\n");
			printf ( " Catalyst version %s.\n", driverVersionInfo.strCatalystVersion );
			printf ( " Driver version %s.\n", driverVersionInfo.strDriverVersion );
			printf ( " Driver URL is %s.\n", driverVersionInfo.strCatalystWebLink );
		}
		else
		{
			printf ( "\nDRIVER version info query failed.\n" );
		}
	}

	// Get the latest driver version and URL
	{
		AGSLatestDriverVersionInfoStruct Info = { NULL };

		if ( AGS_SUCCESS == AGSDriverGetLatestVersionInfo(&Info))
		{
			printf ( "\nLatest DRIVER version info.\n" );
			printf ( " Latest Catalyst version %s.\n", Info.strDriverVersion );
			printf ( " Latest Driver URL is %s.\n", Info.strDriverWebLink );
		}				
		else
		{
			printf ( "\nLatest DRIVER version info query failed.\n" );
		}
	}

	// Get Crossfire config info
	{
		int CrossfireGPUCount = 1;
		
		if ( AGS_SUCCESS == AGSCrossfireGetGPUCount( iOSDisplayIndex, &CrossfireGPUCount ) )
		{
			printf ( "\nCROSSFIRE GPU count %i.\n", CrossfireGPUCount );
		}
		else
		{
			printf ( "\nCROSSFIRE GPU count query failed.\n" );
		}
	}

	// Get Eyefinity config info
	{
		AGSEyefinityInfoStruct eyefinityInfo = {0};
		int iNumDisplaysInfo = 0;
		AGSDisplayInfoStruct *pDisplaysInfo = NULL;

		// The amd_ags.h supports testing Eyefinity support without needing to enable Eyefinity
		// in AMD's Catalyst Control Center application. This is useful for testing Eyefinity
		// support for a windowed application or if the necessary hardware is not available, when
		// testing support for a 6 display setup when using a graphic card that only supports up
		// to 3 displays for example.
		//
		// This is accomplished by pre-loading the AGSEyefinityInfoStruct structure passed into
		// the AGSEyefinityGetConfigInfo() call. The iSLSWidth, iSLSHeight, iSLSGridWidth, 
		// and iSLSGridHeight must *all* contain non-zero values in order for this functionality
		// to be triggered. When this is set, the amd_ags library is not used, and the values
		// loaded into the pDisplaysInfo array of structures are simply computed based on the 
		// values passed into through the AGSEyefinityInfoStruct structure.
		//
		// This functionality is only enabled in the amd_ags.h file if _DEBUG is defined to avoid
		// having shipping applications use this debug feature.
		//
		// To use this functionality, uncomment the code below and set appropriate values for the
		// tests that need to be run.
		//
		// In the example below, a 3 wide by 2 tall 2400x1200 is simulated, with each hypothetical
		// display resolution being 800x600. Ideally, the developer using these kinds of values
		// would then create a 2400x1200 window for rendering and display.
		/*
		{
			eyefinityInfo.iSLSWidth = 2400;
			eyefinityInfo.iSLSHeight = 1200;
			eyefinityInfo.iSLSGridWidth = 3;
			eyefinityInfo.iSLSGridHeight = 2;
			eyefinityInfo.iBezelCompensatedDisplay = TRUE;
		}
		*/

		// Find out if this display has an Eyefinity config enabled
		if ( AGS_SUCCESS == AGSEyefinityGetConfigInfo( iOSDisplayIndex, &eyefinityInfo, &iNumDisplaysInfo, &pDisplaysInfo ) )
		{
			if ( TRUE == eyefinityInfo.iSLSActive )
			{
				int iCurrentDisplaysInfo = 0;

				printf ( "\nEYEFINITY ENABLED for display index %i:\n", iOSDisplayIndex);
				printf ( " SLS grid is %i displays wide by %i displays tall.\n", eyefinityInfo.iSLSGridWidth, eyefinityInfo.iSLSGridHeight );
				printf ( " SLS resolution is %ix%i pixels.\n", eyefinityInfo.iSLSWidth, eyefinityInfo.iSLSHeight );

				if ( TRUE == eyefinityInfo.iBezelCompensatedDisplay )
				{
					printf ( " SLS is bezel-compensated.\n" );
				}

				for ( iCurrentDisplaysInfo=0; iCurrentDisplaysInfo<iNumDisplaysInfo; iCurrentDisplaysInfo++ )
				{
					printf ( "\nDisplay %i\n", iCurrentDisplaysInfo);

					if ( TRUE == pDisplaysInfo[iCurrentDisplaysInfo].iPreferredDisplay )
					{
						printf ( " Preferred/main monitor\n");
					}

					printf ( " SLS grid coord [%i,%i]\n", pDisplaysInfo[iCurrentDisplaysInfo].iGridXCoord, pDisplaysInfo[iCurrentDisplaysInfo].iGridYCoord );
					printf ( " Base coord [%i,%i]\n", pDisplaysInfo[iCurrentDisplaysInfo].displayRect.iXOffset, pDisplaysInfo[iCurrentDisplaysInfo].displayRect.iYOffset );
					printf ( " Dimensions [%ix%i]\n", pDisplaysInfo[iCurrentDisplaysInfo].displayRect.iWidth, pDisplaysInfo[iCurrentDisplaysInfo].displayRect.iHeight );
					printf ( " Visible base coord [%i,%i]\n", pDisplaysInfo[iCurrentDisplaysInfo].displayRectVisible.iXOffset, pDisplaysInfo[iCurrentDisplaysInfo].displayRectVisible.iYOffset );
					printf ( " Visible dimensions [%ix%i]\n", pDisplaysInfo[iCurrentDisplaysInfo].displayRectVisible.iWidth, pDisplaysInfo[iCurrentDisplaysInfo].displayRectVisible.iHeight );
				}
			}
			else
			{
				printf ( "\nEYEFINITY DISABLED for display index %i.\n", iOSDisplayIndex);
			}

			AGSEyefinityReleaseConfigInfo ( &pDisplaysInfo );
		}
		else
		{
			printf ( "\nEYEFINITY configuration query failed for display index %i.\n", iOSDisplayIndex);
		}			
	}

	if ( AGS_SUCCESS != AGSDeInit() )
	{
		printf ( "\nFailed to cleanup AGS Library\n" );
		_getch();
	}

	printf ( "\ndone\n" );

	_getch();

	return 0;
}

