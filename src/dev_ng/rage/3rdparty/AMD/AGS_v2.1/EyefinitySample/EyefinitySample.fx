//--------------------------------------------------------------------------------------
// File: EyefinitySample.fx
//
// Copyright � AMD Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

struct VSSceneIn
{
	float3 pos	: POSITION;			//position
	float3 norm : NORMAL;			//normal
	float2 tex	: TEXTURE0;			//texture coordinate
};

struct PSSceneIn
{
	float4 pos : SV_Position;
	float2 tex : TEXTURE0;
};

// Textures
Texture2D g_txDiffuse		: register( t0 );

// Samplers
SamplerState g_SampleLinear : register( s0 );

// Comstant
cbuffer cb0 : register( b0 )
{
	float4x4 g_mWorldViewProj;
};

PSSceneIn VSScenemain(VSSceneIn input)
{
	PSSceneIn output;
	
	output.pos = mul( float4(input.pos,1.0), g_mWorldViewProj );
	output.tex = input.tex;
	
	return output;
}

float4 PSScenemain(PSSceneIn input) : SV_Target
{	
	return g_txDiffuse.Sample( g_SampleLinear, input.tex );
}