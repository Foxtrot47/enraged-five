/*  Berkelium - Embedded Chromium
 *  ResourceInterceptor.hpp
 *
 *  Copyright (c) 2010
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  * Neither the name of Sirikata nor the names of its contributors may
 *    be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _BERKELIUM_RESOURCE_INTERCEPTOR_HPP_
#define _BERKELIUM_RESOURCE_INTERCEPTOR_HPP_

#include <string>
#include <windows.h>

namespace Berkelium {

class BERKELIUM_EXPORT ResourceRequestJob {
public:
	ResourceRequestJob() {

	}
    virtual ~ResourceRequestJob() {

	}

	virtual bool GetResponse(char** headers, unsigned int* headersLength, char** body, unsigned int* bodyLength) = 0;
	virtual void Release() = 0;
};

class BERKELIUM_EXPORT ResourceInterceptor {
public:
	ResourceInterceptor() {

	}
    virtual ~ResourceInterceptor() {

	}
    
    /** Handle an incoming request from the chromium network stack.
     *  \param url the url of the incoming request.
     *  \param urlLength the number of characters in the incoming request url.
     *  \returns NULL if the request should be handled in the normal fashion, or a valid ResourceRequestJob if it's being intercepted.
     */
    virtual ResourceRequestJob* MaybeIntercept(const char* url, unsigned int urlLength) = 0;
};

}

#endif
