/*  Berkelium Implementation
 *  ContextImpl.cpp
 *
 *  Copyright (c) 2009, Patrick Reiter Horn
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  * Neither the name of Sirikata nor the names of its contributors may
 *    be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "berkelium/Platform.hpp"
#include "berkelium/ResourceInterceptor.hpp"

#include "chrome/browser/renderer_host/site_instance.h"
#include "Root.hpp"
#include "ContextImpl.hpp"
#include "chrome/browser/profile.h"
#include "chrome/browser/in_process_webkit/session_storage_namespace.h"
#include "chrome/browser/in_process_webkit/webkit_context.h"
#include "net/url_request/url_request.h"
#include "net/url_request/url_request_job.h"
#include "net/url_request/url_request_job_manager.h"
#include "net/http/http_response_headers.h"
#include "net/base/net_errors.h"
#include "net/base/io_buffer.h"
#include "base/singleton.h"
#include "net/base/mime_util.h"
#include "googleurl/src/gurl.h"
#include "base/utf_string_conversions.h"

namespace Berkelium {
ContextImpl::ContextImpl(const ContextImpl&other) {
    init(other.mProfile, other.mSiteInstance, other.mSessionNamespace);
	m_ResourceInterceptorInternal = other.m_ResourceInterceptorInternal;
}
ContextImpl::ContextImpl(Profile *prof, SiteInstance*si, SessionStorageNamespace *ssn) {
    init(prof, si, ssn);
}
ContextImpl::ContextImpl(Profile *prof) {
    init(prof,
         SiteInstance::CreateSiteInstance(prof),
         new SessionStorageNamespace(prof));
}

void ContextImpl::init(Profile*prof, SiteInstance*si, SessionStorageNamespace *ssn) {
    mSiteInstance = si;
    mProfile = prof;
    mSessionNamespace = ssn;
	m_ResourceInterceptorInternal = NULL;
}


ContextImpl::~ContextImpl() {
}
Context* ContextImpl::clone() const{
    return new ContextImpl(*this);
}
ContextImpl* ContextImpl::getImpl() {
    return this;
}
const ContextImpl* ContextImpl::getImpl() const{
    return this;
}

class ResourceRequestJobInternal : public URLRequestJob {
public:
	explicit ResourceRequestJobInternal(URLRequest* request, ResourceRequestJob* job)
		: URLRequestJob(request)
		, m_Job(job)
		, mReadPos(0)
		, mURL(request->url()) {
// 			printf("ResourceRequestJobInternal contructed\n");
// 			fflush(stdout);
		}

	~ResourceRequestJobInternal()
	{
		if(m_Job)
		{
			m_Job->Release();
		}
// 		printf("ResourceRequestJobInternal destructed\n");
// 		fflush(stdout);
	}

	void Start () {
		char* headers = NULL;
		unsigned int headersLen = 0;
		mBufferLength = 0;

		bool result = m_Job->GetResponse(&headers, &headersLen, &mBuffer, &mBufferLength);

		if (result) {
			if(headers)
			{
				mHeaders = new net::HttpResponseHeaders(std::string((const char *)headers, headersLen));
			}

			this->NotifyHeadersComplete();
		} else {
			NotifyDone(URLRequestStatus(URLRequestStatus::FAILED, net::ERR_FILE_NOT_FOUND));
		}
	}

	bool ReadRawData (net::IOBuffer* buf, int buf_size, int *bytes_read) {
		DCHECK(bytes_read);

		unsigned size = std::max<unsigned>(std::min<unsigned>(buf_size, mBufferLength - mReadPos), 0);

		if (size) {
			memcpy(buf->data(), mBuffer + mReadPos, size);
			mReadPos += size;
		}

		*bytes_read = (int)size;
		return true;
	}

	void GetResponseInfo (net::HttpResponseInfo* info) {
		if (mHeaders)
			info->headers = mHeaders;
	}

	bool GetMoreData() {
		if (mReadPos < mBufferLength)
			return true;
		else
			return false;
	}

	bool GetMimeType(std::string* mime_type) const {
		bool hasMimeType = mHeaders->GetMimeType(mime_type);
		if(hasMimeType == false)
		{
			char fileScheme[] = "file://" ;
			bool isFileScheme = strncmp(mURL.spec().c_str(), fileScheme, sizeof(fileScheme) - 1) == 0;
			if(isFileScheme)
			{
				std::wstring urlWideString;
				UTF8ToWide(mURL.spec().c_str(), mURL.spec().length(), &urlWideString);
				FilePath f(urlWideString);
				std::wstring extension = f.Extension();
				if(extension.length() > 0)
				{
					hasMimeType = net::GetMimeTypeFromExtension(extension.substr(1), mime_type);
				}
			}
		}
		return hasMimeType;
	}

	bool GetURL(GURL *gurl) const {
		*gurl = mURL;
		return true;
	}

	virtual int GetResponseCode() const {
		return mHeaders->response_code();
	}

	virtual bool GetCharset(std::string* charset) {
		return mHeaders->GetCharset(charset);
	}

protected:
	ResourceRequestJob* m_Job;
	scoped_refptr<net::HttpResponseHeaders> mHeaders;
	unsigned int mBufferLength;
	char* mBuffer;
	unsigned mReadPos;
	GURL mURL;
};

class ResourceInterceptorInternal : public URLRequest::Interceptor {
public:
	ResourceInterceptorInternal(ResourceInterceptor* interceptor) : m_Interceptor(interceptor) {
		URLRequestJobManager* jm = Singleton<URLRequestJobManager>::get();
		jm->RegisterRequestInterceptor(this);
	}
	~ResourceInterceptorInternal() {
// 		printf("~ResourceInterceptorInternal()\n");
// 		fflush(stdout);
//		URLRequestJobManager* jm = Singleton<URLRequestJobManager>::get();
//		jm->UnregisterRequestInterceptor(this);
	}

	void Unregister()
	{
		URLRequestJobManager* jm = Singleton<URLRequestJobManager>::get();
		jm->UnregisterRequestInterceptor(this);
	}

	virtual URLRequestJob* MaybeIntercept(URLRequest* request) {
		ResourceRequestJob* job = m_Interceptor->MaybeIntercept(request->url().spec().c_str(), request->url().spec().length());
		if(job)
		{
			return new ResourceRequestJobInternal(request, job);
		}
		return NULL;
	}

private:
	ResourceInterceptor* m_Interceptor;
};

void ContextImpl::registerResourceInterceptor(ResourceInterceptor* interceptor) {
	m_ResourceInterceptorInternal = new ResourceInterceptorInternal(interceptor);
}

void ContextImpl::unregisterResourceInterceptor() {
	if(m_ResourceInterceptorInternal)
	{
		m_ResourceInterceptorInternal->Unregister();
		delete m_ResourceInterceptorInternal;
		m_ResourceInterceptorInternal = NULL;
	}
}

}
