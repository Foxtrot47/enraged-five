﻿using Microsoft.VisualStudio.Shell;
using System;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;

namespace MotionTreeVisualizer
{
    /// <summary>
    /// Motion Tree visualizer service exposed by the package
    /// </summary>
    [Guid("5452AFEA-3DF6-46BB-9177-C0B08F318025")]
    public interface IMotionTreeVisualizerService { }

    [PackageRegistration(UseManagedResourcesOnly = true)]
    [ProvideService(typeof(IMotionTreeVisualizerService), ServiceName = "MotionTreeVisualizerService")]
    [InstalledProductRegistration("Motion Tree Sample", "Motion Tree Sample", "1.0")]
    [Guid("C37A4CFC-670F-454A-B40E-AC08578ABABD")]
    public sealed class MotionTreeVisualizerPackage : Package
    {
        /// <summary>
        /// Initialization of the package; register motion tree visualizer service
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            IServiceContainer serviceContainer = (IServiceContainer)this;

            if (serviceContainer != null)
            {
                serviceContainer.AddService(typeof(IMotionTreeVisualizerService), new MotionTreeVisualizerService(), true);
            }
        }
    }
}
