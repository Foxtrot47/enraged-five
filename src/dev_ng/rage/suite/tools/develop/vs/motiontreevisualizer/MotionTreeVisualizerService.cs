namespace MotionTreeVisualizer
{
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Debugger.Interop;
    using Microsoft.VisualStudio.Shell.Interop;
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Windows.Interop;
    using System.Windows.Controls;
    using EnvDTE;

    /// <summary>
    /// Visualizer service
    /// </summary>
    internal class MotionTreeVisualizerService : IVsCppDebugUIVisualizer, IMotionTreeVisualizerService
    {
        private EnvDTE.Debugger m_Debugger;

        public int DisplayValue(uint ownerHwnd, uint visualizerId, IDebugProperty3 debugProperty)
        {
            int hr = VSConstants.S_OK;

            EnvDTE.DTE dte = Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SDTE)) as EnvDTE.DTE;
            Debug.Assert(dte != null, "Package.GetGlobalService failed");
            m_Debugger = dte.Debugger;

            DEBUG_PROPERTY_INFO[] propertyInfo = new DEBUG_PROPERTY_INFO[1];
            hr = debugProperty.GetPropertyInfo(
                enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NAME | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_ATTRIB | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_VALUE | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_PROP | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_VALUE_RAW, 
                16, 
                10000, 
                new IDebugReference2[] { }, 
                0, 
                propertyInfo);

            Debug.Assert(hr == VSConstants.S_OK, "IDebugProperty3.GetPropertyInfo failed");
            DEBUG_PROPERTY_INFO rootInfo = GetChildPropertyAt(4, propertyInfo[0]);
            ulong root = ulong.Parse(rootInfo.bstrValue.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
            if (root != 0)
            {
                // fill tree view
                TreeViewItem node, sibling;
                SerializeNode(rootInfo, out node, out sibling);
                PlotterWindow plotterWindow = new PlotterWindow();
                WindowInteropHelper helper = new WindowInteropHelper(plotterWindow);
                helper.Owner = (IntPtr)ownerHwnd;
                plotterWindow.ShowText(node);

                // dump inside the output window
                string output = DumpNode(node, 0);
                Window w = (Window)dte.Windows.Item(EnvDTE.Constants.vsWindowKindOutput);
                OutputWindow ow = (OutputWindow)w.Object;
                const string windowName = "Motion Tree Visualizer";
                OutputWindowPane owp = null;
                foreach (OutputWindowPane window in ow.OutputWindowPanes)
                {
                    if (window.Name == windowName)
                        owp = window;
                }
                if (owp == null)
                {
                    owp = ow.OutputWindowPanes.Add(windowName);
                }
                owp.OutputString(output);
            }

            return hr;
        }

        private void SerializeNode(DEBUG_PROPERTY_INFO info, out TreeViewItem node, out TreeViewItem sibling)
        {
            DEBUG_PROPERTY_INFO attributes = GetChildPropertyAt(0, info);
            DEBUG_PROPERTY_INFO disabled = GetChildPropertyAt(6, info);
            DEBUG_PROPERTY_INFO silent = GetChildPropertyAt(7, info);
            DEBUG_PROPERTY_INFO childInfo = GetChildPropertyAt(8, info);
            DEBUG_PROPERTY_INFO siblingInfo = GetChildPropertyAt(9, info);

            const string prefix = "rage::";
            string substring = SerializeAttributes(attributes);
            string type = attributes.bstrType;
            if (type.StartsWith(prefix))
            {
                type = type.Substring(prefix.Length);
            }
 
            node = new TreeViewItem();
            node.Header = type + " " + info.bstrValue + " " + substring;
  
            // read node child
            ulong childPtr = ulong.Parse(childInfo.bstrValue.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
            if (childPtr != 0)
            {
                TreeViewItem child, childSibling;
                SerializeNode(childInfo, out child, out childSibling);

                node.Items.Add(child);
                if (childSibling != null)
                    node.Items.Add(childSibling);
            }

            // read node sibling
            ulong siblingPtr = ulong.Parse(siblingInfo.bstrValue.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
            if (siblingPtr != 0)
            {
                TreeViewItem child, childSibling;
                SerializeNode(siblingInfo, out child, out childSibling);

                sibling = child;
            }
            else
            {
                sibling = null;
            }

            const string empty = "'\\0'";
            node.FontStyle = disabled.bstrValue == empty ? System.Windows.FontStyles.Normal : System.Windows.FontStyles.Italic;
        }

        private string SerializeAttributes(DEBUG_PROPERTY_INFO info)
        {
            switch (info.bstrType)
            {
                case "rage::crmtNodeClip":
                    DEBUG_PROPERTY_INFO clipPlayer = GetChildPropertyAt(1, info);
                    DEBUG_PROPERTY_INFO clipTime = GetChildPropertyAt(2, clipPlayer);
                    DEBUG_PROPERTY_INFO clipPhase = GetChildPropertyAt(9, clipPlayer);
                    DEBUG_PROPERTY_INFO clipRef = GetChildPropertyAt(0, clipPlayer);
                    DEBUG_PROPERTY_INFO clip = GetChildPropertyAt(0, clipRef);
                    ulong clipPtr = ulong.Parse(clip.bstrValue.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
                    if (clipPtr != 0)
                    {
                        DEBUG_PROPERTY_INFO nameString = GetChildPropertyAt(3, clip);
                        DEBUG_PROPERTY_INFO name = GetChildPropertyAt(0, nameString);
                        return GetPropertyString(name) + " Time:" + clipTime.bstrValue;
                    }
                    break;

                case "rage::crmtNodeExpression":
                    DEBUG_PROPERTY_INFO expressionWeight = GetChildPropertyAt(4, info);
                    DEBUG_PROPERTY_INFO expressionPlayer = GetChildPropertyAt(1, info);
                    DEBUG_PROPERTY_INFO expressionTime = GetChildPropertyAt(4, expressionPlayer);
                     DEBUG_PROPERTY_INFO expressionsRef = GetChildPropertyAt(0, expressionPlayer);
                    DEBUG_PROPERTY_INFO expressions = GetChildPropertyAt(0, expressionsRef);
                    ulong expressionsPtr = ulong.Parse(expressions.bstrValue.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
                    if (expressionsPtr != 0)
                    {
                        DEBUG_PROPERTY_INFO nameString = GetChildPropertyAt(7, expressions);
                        DEBUG_PROPERTY_INFO name = GetChildPropertyAt(0, nameString);
                        return GetPropertyString(name) + " Time:" + expressionTime.bstrValue + " Weight:" + expressionWeight.bstrValue;
                    }
                    break;

                case "rage::crmtNodeBlend":
                case "rage::crmtNodeAddSubtract":
                    DEBUG_PROPERTY_INFO nodePairWeighted = GetChildPropertyAt(0, info);
                    DEBUG_PROPERTY_INFO blendWeight = GetChildPropertyAt(1, nodePairWeighted);
                    DEBUG_PROPERTY_INFO blendRate = GetChildPropertyAt(2, nodePairWeighted);
                    return "Weight:" + blendWeight.bstrValue + " Rate:" + blendRate.bstrValue;

                case "rage::mvNodeStateMachine":
                    DEBUG_PROPERTY_INFO stateDef = GetChildPropertyAt(2, info);
                    DEBUG_PROPERTY_INFO nodeDef = GetChildPropertyAt(0, stateDef);
                    DEBUG_PROPERTY_INFO id = GetChildPropertyAt(2, nodeDef);
                    return "Id:" + GetHashString(id.bstrValue);
            }

            return null;
        }

        private string DumpNode(TreeViewItem node, int indent)
        {
            string output = null;
            for (int i = 0; i < indent; i++)
            {
                output += '\t';
            }

            output += node.Header;
            output += '\n';
            foreach (TreeViewItem item in node.Items)
            {
                output += DumpNode(item, indent + 1);
            }

            return output;
        }

        private string GetPropertyString(DEBUG_PROPERTY_INFO debugPropertyInfo)
        {
            IDebugMemoryContext2 memoryContext;
            int hr = debugPropertyInfo.pProperty.GetMemoryContext(out memoryContext);
            Debug.Assert(hr == VSConstants.S_OK, "IDebugProperty.GetMemoryContext failed");

            IDebugMemoryBytes2 memoryBytes;
            hr = debugPropertyInfo.pProperty.GetMemoryBytes(out memoryBytes);
            Debug.Assert(hr == VSConstants.S_OK, "IDebugProperty.GetMemoryBytes failed");

            uint maxLength = 64;
            byte[] buffer = new byte[maxLength];
            uint read = 0;
            uint unreadable = 0;

            hr = memoryBytes.ReadAt(memoryContext, maxLength, buffer, out read, ref unreadable);
            Debug.Assert(hr == VSConstants.S_OK, "IDebugMemoryBytes.ReadAt failed");

            string result = null;
            for (int i = 0; i < buffer.Length && buffer[i] != 0; i++)
                result += (char)buffer[i];

            return result;
        }

        private string GetHashString(string str)
        {
            uint hash = uint.Parse(str.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);

            const string refStrings = "rage::g_refdStrings.";
            Expression slots = m_Debugger.GetExpression(refStrings + "m_Slots");
            uint slotCount = Convert.ToUInt32(slots.Value);
            uint index = hash % slotCount;

            Expression curr = m_Debugger.GetExpression(refStrings + "m_Hash[" + index + "]");
            while (true)
            {
                ulong ptr = ulong.Parse(curr.Value.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
                if (ptr == 0)
                    break;

                string entry = "((rage::atMapEntry<unsigned int,char const *> *)" + curr.Value + ")->";

                Expression key = m_Debugger.GetExpression(entry + "key");
                if (Convert.ToUInt32(key.Value) == hash)
                {
                    Expression data = m_Debugger.GetExpression(entry + "data", true);
                    int start = data.Value.IndexOf('"');
                    int end = data.Value.LastIndexOf('"');
                    if (start != end)
                    {
                        return data.Value.Substring(start, end - start + 1);
                    }
                    break;
                }

                curr = m_Debugger.GetExpression(entry + "next");
            }

            return str;
        }

        private DEBUG_PROPERTY_INFO GetChildPropertyAt(int index, DEBUG_PROPERTY_INFO debugPropertyInfo)
        {
            int hr = VSConstants.S_OK;
            DEBUG_PROPERTY_INFO[] childInfo = new DEBUG_PROPERTY_INFO[1];
            IEnumDebugPropertyInfo2 enumDebugPropertyInfo;
            Guid guid = Guid.Empty;

            hr = debugPropertyInfo.pProperty.EnumChildren(
              enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NAME | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_TYPE | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_ATTRIB | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_VALUE | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_PROP | enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_VALUE_RAW,
                16,
                ref guid,
                enum_DBG_ATTRIB_FLAGS.DBG_ATTRIB_CHILD_ALL,
                null,
                10000,
                out enumDebugPropertyInfo);

            Debug.Assert(hr == VSConstants.S_OK, "GetChildPropertyAt: EnumChildren failed");

            if (enumDebugPropertyInfo != null)
            {
                uint childCount;
                hr = enumDebugPropertyInfo.GetCount(out childCount);
                Debug.Assert(hr == VSConstants.S_OK, "GetChildPropertyAt: IEnumDebugPropertyInfo2.GetCount failed");
                Debug.Assert(childCount > index, "Given child index out of bounds");

                hr = enumDebugPropertyInfo.Skip((uint)index);
                Debug.Assert(hr == VSConstants.S_OK, "GetChildPropertyAt: IEnumDebugPropertyInfo2.Skip failed");

                uint fetched;
                hr = enumDebugPropertyInfo.Next(1, childInfo, out fetched);
                Debug.Assert(hr == VSConstants.S_OK, "GetChildPropertyAt: IEnumDebugPropertyInfo2.Next failed");
            }

            return childInfo[0];
        }
    }
}
