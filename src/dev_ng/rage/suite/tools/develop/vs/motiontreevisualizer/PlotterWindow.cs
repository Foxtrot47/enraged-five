namespace MotionTreeVisualizer
{
    using System.Drawing;
    using System.Windows;
    using System.Windows.Forms.DataVisualization.Charting;
    using System.Windows.Input;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for PlotterWindow.xaml
    /// </summary>
    public partial class PlotterWindow : Window
    {
        private static readonly Color TranslucentGray = Color.FromArgb(64, 64, 64, 64);

        public PlotterWindow()
        {
            InitializeComponent();
            this.KeyDown += new KeyEventHandler((o, e) =>
            {
                if (e.Key == Key.Escape)
                {
                    this.Close();
                }
            });
        }

        public bool? ShowText(TreeViewItem item)
        {
            item.ExpandSubtree();
            output.Items.Add(item);
            return this.ShowDialog();
        }
    }
}
