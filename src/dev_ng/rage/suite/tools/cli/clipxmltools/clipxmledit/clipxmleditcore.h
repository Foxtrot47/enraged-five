#include "atl/string.h"
#include "atl/array.h"

using namespace rage;

namespace ClipXMLEditCore
{
	bool ClipXMLToClip(atArray<atString>& inputClipList);
	bool ClipToClipXML(atArray<atString>& inputClipList);
}
