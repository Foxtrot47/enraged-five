// 
// /cutfclipconcatxml.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "file/asset.h"
#include "system/exec.h"
#include "system/param.h"

//#include "RsULog/ULogger.h"

#include "clipxml/clipxml.h"
#include "clipxml/util.h"
#include "clipxmleditcore.h"

#include "cranimation/animation.h"
#include "crclip/clipanimation.h"

using namespace rage;

REQ_PARAM( clips, "[cutfclipconcat] The list of comma-separated input files." );
PARAM( toClip, "");
PARAM( toXml, "");
PARAM( hideAlloc, "[cutfclipconcat] Hides the allocation spam in the log." );
PARAM( ulog, "[cutfanimcombine] Universal logging." );

#define BLOCK_TAG_PAD 0.001f

int Main()
{
	INIT_PARSER;

	crAnimation::InitClass();
	crClip::InitClass();

	sysMemSimpleAllocator::HideAllocatorOutput(true);

    ASSET.SetPath( RAGE_ASSET_ROOT );

    // get the input directories
    const int maxItems = 100;
    const int maxItemsBufSize = maxItems * RAGE_MAX_PATH;

    // get the inputClips
    int numInputClips = 0;
    const char* inputClips[maxItems];
    char inputClipsBuf[maxItemsBufSize];

    numInputClips = PARAM_clips.GetArray( inputClips, maxItems, inputClipsBuf, maxItemsBufSize );

	ClipXML* c = rage_new ClipXML();
	TagXML* t = rage_new TagXML();
	PropertyAttributeFloatXML* p = rage_new PropertyAttributeFloatXML();
	p->GetFloat() = 2.0f;
	t->GetProperty().AddAttribute(p);
	c->GetTags().PushAndGrow(t);

	TagXML* clone = t->Clone();
	p->GetFloat() = 3;

	atArray<atString> inputClipList;

	inputClipList.Reserve( numInputClips );
	for ( int i = 0; i < numInputClips; ++i )
	{
		inputClipList.Append() = inputClips[i];
		inputClipList[i].Trim();
	}

	bool result = false;

	if(PARAM_toClip.Get())
	{
		result = ClipXMLEditCore::ClipXMLToClip(inputClipList);
	}

	if(PARAM_toXml.Get())
	{
		result = ClipXMLEditCore::ClipToClipXML(inputClipList);
	}

    return result ? 0 : 1;
}
