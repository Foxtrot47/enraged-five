#include "clipxmleditcore.h"
#include "clipxml/clipxml.h"
#include "cranimation/animation.h"
#include "crclip/clipanimation.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/properties.h"
#include "vectormath/legacyconvert.h"

bool ClipXMLEditCore::ClipToClipXML(atArray<atString>& inputClipList)
{
	atArray<crClip*> clipList;
	clipList.Reserve( inputClipList.GetCount() );

	for ( int i = 0; i < inputClipList.GetCount(); ++i )
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg, "Loading '%s'.", inputClipList[i].c_str() );
		//ULOGGER.SetProgressMessage(cMsg);

		crClip *pClip = crClip::AllocateAndLoad( inputClipList[i].c_str() );
		if ( pClip == NULL )
		{
			for ( int j = 0; j < i; ++j )
			{
				delete clipList[j];
			}

			return false;
		}

		clipList.Append() = pClip;
	}

	for ( int i = 0; i < clipList.GetCount(); ++i )
	{
		crClip* srcClip = clipList[i];

		char cAnimFilename[RAGE_MAX_PATH];
		safecpy( cAnimFilename, inputClipList[i].c_str(), sizeof(cAnimFilename) ); 
		ASSET.RemoveExtensionFromPath( cAnimFilename, sizeof(cAnimFilename), cAnimFilename );

		ClipXML* pClip = rage_new ClipXML();

		if(srcClip->HasProperties())
		{
			for(int j=0; j < srcClip->GetProperties()->GetNumProperties(); ++j)
			{
				crProperty* pCurrentProperty = srcClip->GetProperties()->FindPropertyByIndex(j);
				if(pCurrentProperty)
				{
					PropertyXML* pProperty = rage_new PropertyXML();
					pProperty->SetName(pCurrentProperty->GetName());

					for(int k=0; k < pCurrentProperty->GetNumAttributes(); ++k)
					{
						crPropertyAttributeString* pStringAttribute = dynamic_cast<crPropertyAttributeString*>(pCurrentProperty->GetAttributeByIndex(k));
						if(pStringAttribute)
						{
							PropertyAttributeStringXML* pAttribute = rage_new PropertyAttributeStringXML();
							pAttribute->SetName(pStringAttribute->GetName());
							atString& attrStr = pAttribute->GetString();
							attrStr = pStringAttribute->GetString();

							pProperty->AddAttribute(pAttribute);
						}
						crPropertyAttributeFloat* pFloatAttribute = dynamic_cast<crPropertyAttributeFloat*>(pCurrentProperty->GetAttributeByIndex(k));
						if(pFloatAttribute)
						{
							PropertyAttributeFloatXML* pAttribute = rage_new PropertyAttributeFloatXML();
							pAttribute->SetName(pFloatAttribute->GetName());
							float& attrFloat = pAttribute->GetFloat();
							attrFloat = pFloatAttribute->GetFloat();
	
							pProperty->AddAttribute(pAttribute);

						}
						crPropertyAttributeBool* pBoolAttribute = dynamic_cast<crPropertyAttributeBool*>(pCurrentProperty->GetAttributeByIndex(k));
						if(pBoolAttribute)
						{
							PropertyAttributeBoolXML* pAttribute = rage_new PropertyAttributeBoolXML();
							pAttribute->SetName(pBoolAttribute->GetName());
							bool& attrBool = pAttribute->GetBool();
							attrBool = pBoolAttribute->GetBool();

							pProperty->AddAttribute(pAttribute);
						}
						crPropertyAttributeInt* pIntAttribute = dynamic_cast<crPropertyAttributeInt*>(pCurrentProperty->GetAttributeByIndex(k));
						if(pIntAttribute)
						{
							PropertyAttributeIntXML* pAttribute = rage_new PropertyAttributeIntXML();
							pAttribute->SetName(pIntAttribute->GetName());
							int& attrInt = pAttribute->GetInt();
							attrInt = pIntAttribute->GetInt();

							pProperty->AddAttribute(pAttribute);
						}
						crPropertyAttributeVector3* pVector3Attribute = dynamic_cast<crPropertyAttributeVector3*>(pCurrentProperty->GetAttributeByIndex(k));
						if(pVector3Attribute)
						{
							PropertyAttributeVector3XML* pAttribute = rage_new PropertyAttributeVector3XML();
							pAttribute->SetName(pVector3Attribute->GetName());
							Vec3V& attrVector3 = pAttribute->GetVector3();
							attrVector3 = pVector3Attribute->GetVector3();

							pProperty->AddAttribute(pAttribute);
						}
						crPropertyAttributeQuaternion* pQuaternionAttribute = dynamic_cast<crPropertyAttributeQuaternion*>(pCurrentProperty->GetAttributeByIndex(k));
						if(pQuaternionAttribute)
						{
							PropertyAttributeQuaternionXML* pAttribute = rage_new PropertyAttributeQuaternionXML();
							pAttribute->SetName(pVector3Attribute->GetName());
							pAttribute->FromQuaternion(pQuaternionAttribute->GetQuaternion());

							pProperty->AddAttribute(pAttribute);
						}
					}

					pClip->AddProperty(pProperty);
				}
			}
		}

		if(srcClip->HasTags())
		{
			for(int j=0; j < srcClip->GetTags()->GetNumTags(); ++j)
			{
				crTag* pCurrentTag = srcClip->GetTags()->GetTag(j);
				if(pCurrentTag)
				{
					TagXML* pTag = rage_new TagXML();
					pTag->SetStart(pCurrentTag->GetStart());
					pTag->SetEnd(pCurrentTag->GetEnd());

					pClip->GetTags().PushAndGrow(pTag);
				}
			}
		}

		pClip->Save(cAnimFilename);

		delete pClip;
	}

	for ( int i = 0; i < clipList.GetCount(); ++i )
	{
		delete clipList[i];
	}

	return true;
}

bool ClipXMLEditCore::ClipXMLToClip(atArray<atString>& inputClipList)
{
	atArray<ClipXML*> clipList;
	clipList.Reserve( inputClipList.GetCount() );

	for ( int i = 0; i < inputClipList.GetCount(); ++i )
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg, "Loading '%s'.", inputClipList[i].c_str() );
		//ULOGGER.SetProgressMessage(cMsg);

		ClipXML* pClip = rage_new ClipXML();
		if(!pClip->Load( inputClipList[i].c_str() ))
		{
			for ( int j = 0; j < i; ++j )
			{
				delete clipList[j];
			}

			return false;
		}

		clipList.Append() = pClip;
	}

	for ( int i = 0; i < clipList.GetCount(); ++i )
	{
		ClipXML* srcClip = clipList[i];

		char cAnimFilename[RAGE_MAX_PATH];
		safecpy( cAnimFilename, inputClipList[i].c_str(), sizeof(cAnimFilename) ); 
		ASSET.RemoveExtensionFromPath( cAnimFilename, sizeof(cAnimFilename), cAnimFilename );

		crAnimation* pAnim = crAnimation::AllocateAndLoad(cAnimFilename);
		
		if(pAnim == NULL)
		{
			return false;
		}

		crClipAnimation* pClipAnim = crClipAnimation::AllocateAndCreate(*pAnim);
		if(pClipAnim)
		{
			if(srcClip->HasProperties())
			{
				for(int j=0; j < srcClip->GetNumProperties(); ++j)
				{
					PropertyXML* pCurrentProperty = srcClip->FindPropertyByIndex(j);
					if(pCurrentProperty)
					{
						crProperty pProperty;
						pProperty.SetName(pCurrentProperty->GetName());

						for(int k=0; k < pCurrentProperty->GetNumAttributes(); ++k)
						{
							PropertyAttributeStringXML* pStringAttribute = dynamic_cast<PropertyAttributeStringXML*>(pCurrentProperty->GetAttributeByIndex(k));
							if(pStringAttribute)
							{
								crPropertyAttributeString pAttribute;
								pAttribute.SetName(pCurrentProperty->GetName());
								atString& attrString = pAttribute.GetString();
								attrString = pStringAttribute->GetString();

								pProperty.AddAttribute(pAttribute);
							}
							PropertyAttributeFloatXML* pFloatAttribute = dynamic_cast<PropertyAttributeFloatXML*>(pCurrentProperty->GetAttributeByIndex(k));
							if(pFloatAttribute)
							{
								crPropertyAttributeFloat pAttribute;
								pAttribute.SetName(pCurrentProperty->GetName());
								float& attrFloat = pAttribute.GetFloat();
								attrFloat = pFloatAttribute->GetFloat();

								pProperty.AddAttribute(pAttribute);
							}
							PropertyAttributeBoolXML* pBoolAttribute = dynamic_cast<PropertyAttributeBoolXML*>(pCurrentProperty->GetAttributeByIndex(k));
							if(pBoolAttribute)
							{
								crPropertyAttributeBool pAttribute;
								pAttribute.SetName(pCurrentProperty->GetName());
								bool& attrBool = pAttribute.GetBool();
								attrBool = pBoolAttribute->GetBool();

								pProperty.AddAttribute(pAttribute);
							}
							PropertyAttributeIntXML* pIntAttribute = dynamic_cast<PropertyAttributeIntXML*>(pCurrentProperty->GetAttributeByIndex(k));
							if(pIntAttribute)
							{
								crPropertyAttributeInt pAttribute;
								pAttribute.SetName(pCurrentProperty->GetName());
								int& attrInt = pAttribute.GetInt();
								attrInt = pIntAttribute->GetInt();

								pProperty.AddAttribute(pAttribute);
							}
							PropertyAttributeVector3XML* pVector3Attribute = dynamic_cast<PropertyAttributeVector3XML*>(pCurrentProperty->GetAttributeByIndex(k));
							if(pVector3Attribute)
							{
								crPropertyAttributeVector3 pAttribute;
								pAttribute.SetName(pCurrentProperty->GetName());
								Vec3V& attrVec3 = pAttribute.GetVector3();
								attrVec3 = pVector3Attribute->GetVector3();

								pProperty.AddAttribute(pAttribute);
							}
							PropertyAttributeQuaternionXML* pQuatAttribute = dynamic_cast<PropertyAttributeQuaternionXML*>(pCurrentProperty->GetAttributeByIndex(k));
							if(pQuatAttribute)
							{
								crPropertyAttributeQuaternion pAttribute;
								pAttribute.SetName(pCurrentProperty->GetName());
								QuatV& attrQuat = pAttribute.GetQuaternion();
								pQuatAttribute->GetQuaternion(attrQuat);

								pProperty.AddAttribute(pAttribute);
							}
						}

						pClipAnim->GetProperties()->AddProperty(pProperty);
					}
				}
			}

			if(srcClip->HasTags())
			{
				for(int i=0; i < srcClip->GetTags().GetCount(); ++i)
				{
					TagXML* pCurrentTag = srcClip->GetTags()[i];
					if(pCurrentTag)
					{
						crTag pTag;
						pTag.GetProperty().SetName( pCurrentTag->GetProperty().GetName() );
						pTag.SetStart( pCurrentTag->GetStart() );
						pTag.SetEnd( pCurrentTag->GetEnd() );
						pClipAnim->GetTags()->AddUniqueTag(pTag);
					}
				}
			}

			if(!pClipAnim->Save(cAnimFilename))
			{
				return false;
			}

			delete pClipAnim;
		}
	}

	for ( int i = 0; i < clipList.GetCount(); ++i )
	{
		delete clipList[i];
	}

	return true;
}