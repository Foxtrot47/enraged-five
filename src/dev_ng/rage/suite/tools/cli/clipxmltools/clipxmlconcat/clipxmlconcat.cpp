// 
// /cutfclipconcatxml.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "file/asset.h"
#include "system/exec.h"
#include "system/param.h"

//#include "RsULog/ULogger.h"

#include "clipxml/clipxml.h"
#include "clipxml/util.h"

using namespace rage;

REQ_PARAM( clips, "[cutfclipconcat] The list of comma-separated input files." );
REQ_PARAM( out, "[cutfclipconcat] The output file." );
PARAM( hideAlloc, "[cutfclipconcat] Hides the allocation spam in the log." );
PARAM( ulog, "[cutfanimcombine] Universal logging." );

#define BLOCK_TAG_PAD 0.001f

int Main()
{
	INIT_PARSER;

	if(PARAM_hideAlloc.Get())
	{
		sysMemSimpleAllocator::HideAllocatorOutput(true);
	}

    ASSET.SetPath( RAGE_ASSET_ROOT );

    // get the input directories
    const int maxItems = 100;
    const int maxItemsBufSize = maxItems * RAGE_MAX_PATH;

    // get the inputClips
    int numInputClips = 0;
    const char* inputClips[maxItems];
    char inputClipsBuf[maxItemsBufSize];

    numInputClips = PARAM_clips.GetArray( inputClips, maxItems, inputClipsBuf, maxItemsBufSize );

    atArray<atString> inputClipList;

    inputClipList.Reserve( numInputClips );
    for ( int i = 0; i < numInputClips; ++i )
    {
        inputClipList.Append() = inputClips[i];
        inputClipList[i].Trim();
    }

    // get the output filename
    const char *pOutputFilename;
    PARAM_out.Get( pOutputFilename );

    // load the input clips and get the durations
    atArray<ClipXML*> clipList;
    clipList.Reserve( inputClipList.GetCount() );

    for ( int i = 0; i < inputClipList.GetCount(); ++i )
    {
		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg, "Loading '%s'.", inputClipList[i].c_str() );
		//ULOGGER.SetProgressMessage(cMsg);

		ClipXML* pClip = rage_new ClipXML();
		if(!pClip->Load( inputClipList[i].c_str() ))
		{
			for ( int j = 0; j < i; ++j )
			{
				delete clipList[j];
			}

			return 1;
		}

		clipList.Append() = pClip;
    }

	ClipXML* pConcattedClip = ClipXMLUtil::ConcatClipXMLs(clipList);

	//ULOGGER.SetProgressMessage("Saving '%s'.", pOutputFilename );

    int result = 0;
	if ( !pConcattedClip->Save( pOutputFilename ) )
	{
	   result = 1;
	}

    for ( int i = 0; i < clipList.GetCount(); ++i )
    {
        delete clipList[i];
    }

    return result;
}
