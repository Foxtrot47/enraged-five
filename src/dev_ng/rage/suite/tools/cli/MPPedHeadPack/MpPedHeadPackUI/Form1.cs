﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace MpPedHeadPackUI
{
    public partial class Form1 : Form
    {
        private const int max_dests = 15;
        private static Queue<String> outputStream = null;
        private static List<TextBox> txtDests = null;
        private static List<Button> btnDests = null;
        private Process proc = null;
        public Form1()
        {
            InitializeComponent();
            
            txtSrc.Text = String.Format(@"X:\gta5\art\peds\Multiplayer\MP_Heads\mp_heads_NG.ped.zip");
            txtDest.Text = String.Format(@"X:\gta5\assets_ng\export\models\cdimages\streamedpeds\mp_f_freemode_01.ped.zip");
            txtTarget.Text = String.Format(@"X:\gta5\assets_ng\export\models\cdimages\streamedpeds\mp_headtargets.ped.zip");
            lblInfo.Text = "";
            txtDests = new List<TextBox>();
            btnDests = new List<Button>();
        }

        private int RunProcess(string strExe, string strArgs, ref string output)
        {
            if (!File.Exists(strExe))
            {
                output = String.Format("Error: Executable not found at: {0}", strExe);
                return 1;
            }

            proc = new Process();
            proc.StartInfo.Arguments = strArgs;
            proc.StartInfo.FileName = strExe;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = true;
            outputStream = new Queue<String>();
            outputStream.Enqueue(String.Format("Starting Process: {0}",Path.GetFileNameWithoutExtension(strExe)));
            proc.OutputDataReceived += new DataReceivedEventHandler(ProcOutputHandler);
            timer1.Interval = 32;
            timer1.Start();
            btnPack.Enabled = false;
            lstOutput.Items.Clear();
            lstOutput.Visible = true;
            proc.Start();
            proc.BeginOutputReadLine();

            return 0;
        }

        private void ProcOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            // Collect the sort command output. 
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                outputStream.Enqueue(outLine.Data);
            }
        }

        private bool validateInput()
        {
            bool result = true;
            result = result && txtSrc.Text != String.Empty;
            result = result && txtDest.Text != String.Empty;
            result = result && txtTarget.Text != String.Empty;
            if (!result)
            {
                MessageBox.Show("One or more input parameters are empty");
                return result;
            }
            return result;
        }

        private void btnPack_Click(object sender, EventArgs e)
        {
            lblInfo.ForeColor = Color.FromArgb(0, 0, 0);
            lblInfo.Text = "Processing...";
            lblInfo.Invalidate();
            lblInfo.Refresh();
            lblInfo.Update();

            if (!validateInput())
            {
                lblInfo.ForeColor = Color.FromArgb(255, 0, 0);
                lblInfo.Text = "Failed";
                return;
            }

            string headPackBinary = String.Format(@"{0}\bin\Peds\MPPedHeadPacker\MpPedHeadPack.exe", Environment.GetEnvironmentVariable("RS_TOOLSROOT"));
            if (!File.Exists(headPackBinary))
            {
                headPackBinary = String.Format(@"{0}\MpPedHeadPack.exe", Application.StartupPath);
            }

            string destinations = txtDest.Text;
            foreach (TextBox dest in txtDests)
            {
                if(dest.Text != String.Empty)
                    destinations += "," + dest.Text;
            }
            string args = String.Format(@"--srczip {0} --destzips {1} --headtargetzip {2}", txtSrc.Text, destinations, txtTarget.Text);
            string output = String.Empty;

            int result = RunProcess(headPackBinary, args, ref output);

            if (result != 0)
            {
                lblInfo.ForeColor = Color.FromArgb(255, 0, 0);
                lblInfo.Text = "Failed";
                if (output != String.Empty)
                {
                    MessageBox.Show(output);
                }
            }
        }

        private void btnSrc_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Source Zip";
            openFileDialog.Multiselect = false;
            if (txtSrc.Text != String.Empty)
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(txtSrc.Text);
                openFileDialog.FileName = Path.GetFileName(txtSrc.Text);
            }

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtSrc.Text = openFileDialog.FileName;
            }
        }

        private void btnTarget_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Head Target Zip";
            openFileDialog.Multiselect = false;
            if (txtTarget.Text != String.Empty)
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(txtTarget.Text);
                openFileDialog.FileName = Path.GetFileName(txtTarget.Text);
            }

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtTarget.Text = openFileDialog.FileName;
            }
        }

        private void btnDest_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Destination Zip(s)";
            openFileDialog.Multiselect = true;
            if (txtDest.Text != String.Empty)
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(txtDest.Text);
                openFileDialog.FileName = Path.GetFileName(txtDest.Text);
            }

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtDest.Text = openFileDialog.FileNames[0];
                for (int i = 1; i < openFileDialog.FileNames.Length; i++)
                {
                    AddDestination(openFileDialog.FileNames[i]);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            while (outputStream.Count > 0)
            {
                String output = outputStream.Dequeue();
                lstOutput.Items.Add(output);
                lstOutput.TopIndex = lstOutput.Items.Count - 1;
                if (output.StartsWith("Error: "))
                    MessageBox.Show(output);
            }
            if (proc!=null && proc.HasExited)
            {
                lblInfo.Text = (proc.ExitCode==0)?"Success":"Failed";
                lblInfo.ForeColor = Color.FromArgb((proc.ExitCode == 0) ? 0 : 255, (proc.ExitCode == 0) ? 255 : 0, 0);
                btnPack.Enabled = true;
                proc = null;
                timer1.Stop();
            }
            if (proc == null)
                timer1.Stop();
        }

        void dynamicBtnClick(object sender, EventArgs e)
        {
            int destIndex = btnDests.IndexOf((Button)sender);

            openFileDialog.Title = "Destination Zip(s)";
            openFileDialog.Multiselect = true;
            if (txtDests[destIndex].Text != String.Empty)
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(txtDests[destIndex].Text);
                openFileDialog.FileName = Path.GetFileName(txtDests[destIndex].Text);
            }

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtDests[destIndex].Text = openFileDialog.FileNames[0];
                for (int i = 1; i < openFileDialog.FileNames.Length; i++)
                {
                    AddDestination(openFileDialog.FileNames[i]);
                }
            }
        }

        private void AddDestination(string dst)
        {
            if (txtDests.Count >= max_dests)
                return;

            int heightIncrease = (int)((float)txtDest.Size.Height * 1.5f);
            int drawheight = txtDest.Location.Y + heightIncrease;
            if (txtDests.Count != 0)
            {
                drawheight = txtDests[txtDests.Count - 1].Location.Y + heightIncrease;
            }

            TextBox tmpTxt = new TextBox();
            tmpTxt.Text = dst;
            tmpTxt.Location = new System.Drawing.Point(txtDest.Location.X, drawheight);
            tmpTxt.Size = txtDest.Size;
            txtDests.Add(tmpTxt);
            Controls.Add(txtDests[txtDests.Count - 1]);

            Button tmpBtn = new Button();
            tmpBtn.Text = "...";
            tmpBtn.Location = new System.Drawing.Point(btnDest.Location.X, drawheight);
            tmpBtn.Size = btnDest.Size;
            tmpBtn.Click += new EventHandler(dynamicBtnClick);
            btnDests.Add(tmpBtn);
            Controls.Add(btnDests[btnDests.Count - 1]);

            this.Height += heightIncrease;
            txtTarget.Top += heightIncrease;
            btnTarget.Top += heightIncrease;
            lblTarget.Top += heightIncrease;
            btnAdd.Top += heightIncrease;
            btnDelete.Top += heightIncrease;
            lstOutput.Top += heightIncrease;
            lblInfo.Top += heightIncrease;
            btnPack.Top += heightIncrease;
        }

        private void DeleteDestination()
        {
            if (txtDests.Count == 0)
                return;

            int heightDecrease = (int)((float)txtDest.Size.Height * 1.5f);

            Controls.Remove(txtDests[txtDests.Count - 1]);
            txtDests.Remove(txtDests[txtDests.Count - 1]);
            Controls.Remove(btnDests[btnDests.Count - 1]);
            btnDests.Remove(btnDests[btnDests.Count - 1]);

            this.Height -= heightDecrease;
            txtTarget.Top -= heightDecrease;
            btnTarget.Top -= heightDecrease;
            lblTarget.Top -= heightDecrease;
            btnAdd.Top -= heightDecrease;
            btnDelete.Top -= heightDecrease;
            lstOutput.Top -= heightDecrease;
            lblInfo.Top -= heightDecrease;
            btnPack.Top -= heightDecrease;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddDestination(txtDest.Text);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteDestination();
        }

    }
}
