﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using RSG.ManagedRage;
using RSG.SourceControl.Perforce;
using Ionic.Zip;
using RSG.Base.OS;

namespace MPPedHeadPack
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_SOURCEZIP = "srczip";
        private static readonly String OPTION_DESTINATIONZIPS = "destzips";
        private static readonly String OPTION_HEADTARGETZIP = "headtargetzip";
        #endregion // Constants

        static List<string> lstMultiplayerCharacters = new List<string>();

        static string PROJECT_ROOT = Environment.GetEnvironmentVariable("RS_PROJROOT");
        //static string MP_HEADS_FILE = Path.Combine(PROJECT_ROOT, "art", "peds", "Multiplayer", "MP_Heads", "mp_heads.ped.zip");
        //static string MP_HEADTARGETS_FILE = Path.Combine(PROJECT_ROOT, "assets", "export", "models", "cdimages", "streamedpeds", "mp_headtargets.ped.zip");
        //static string PED_TYPE_FILE = Path.Combine(PROJECT_ROOT, "tools", "etc", "config", "characters", "ped_type_list.xml");
        static string STREAMED_PED_DIR = Path.Combine(PROJECT_ROOT, "assets", "export", "models", "cdimages", "streamedpeds");

        static string TEMP_PED_DIR = Path.Combine(Path.GetTempPath(), "MP_PED");
        static string TEMP_MP_HEAD_DIR = Path.Combine(Path.GetTempPath(), "MP_HEAD");
        static string TEMP_MP_HEAD_IFT_DIR = Path.Combine(Path.GetTempPath(), "MP_HEAD_IFT");
        static string TEMP_MP_HEAD_IDD_DIR = Path.Combine(Path.GetTempPath(), "MP_HEAD_IDD");
        static string TEMP_MP_HEAD_TARGET_DIR = Path.Combine(Path.GetTempPath(), "MP_HEAD_TARGET");
        static string TEMP_MP_HEAD_TARGET_IDD_DIR = Path.Combine(Path.GetTempPath(), "MP_HEAD_TARGET_IDD");
        static string TEMP_MP_HEAD_TARGET_IFT_DIR = Path.Combine(Path.GetTempPath(), "MP_HEAD_TARGET_IFT");

        static void DeleteDirectory(string dir)
        {
            try
            {
                while (Directory.Exists(dir))
                {
                    Directory.Delete(dir, true);
                }
            }
            catch(Exception /*e*/)
            {
                DeleteDirectory(dir);
            }
        }

        //static void GatherPedTypeData()
        //{
        //    var xmlDoc = new XmlDocument();
        //    xmlDoc.Load(PED_TYPE_FILE);

        //    XmlNodeList mpFiles = xmlDoc.SelectNodes("/PedTypes/multiplayerpeds/file");

        //    foreach (XmlNode node in mpFiles)
        //    {
        //        Console.WriteLine("found mp ped '{0}'", node.Attributes["asset_name"].InnerText.ToLower());
        //        lstMultiplayerCharacters.Add(node.Attributes["asset_name"].InnerText.ToLower());
        //    }

        //    Console.WriteLine("found '{0}' mp ped(s)", lstMultiplayerCharacters.Count);
        //}

        static void AddDirectoryToZip(ZipFile zip, string rootDir)
        {
            foreach (string dir in Directory.GetDirectories(rootDir))
            {
                foreach (string file in Directory.GetFiles(dir))
                {
                    zip.AddFile(file, Path.GetFileName(dir));
                }
            }

            foreach (string file in Directory.GetFiles(rootDir))
            {
                zip.AddFile(file, "");
            }

            
        }

        static bool ExtractMPHeadsFile(string sourceZip)
        {
            if (File.Exists(sourceZip))
            {
                DeleteDirectory(TEMP_MP_HEAD_DIR);

                Console.WriteLine("extracting '{0}'", sourceZip);

                using (ZipFile zip = new ZipFile(sourceZip))
                {
                    zip.ExtractAll(TEMP_MP_HEAD_DIR, ExtractExistingFileAction.OverwriteSilently);
                }

                foreach (string file in Directory.GetFiles(Path.Combine(TEMP_MP_HEAD_DIR, "mp_heads"), "*.*", SearchOption.AllDirectories))
                {
                    if (!Path.GetFileName(file.ToLower()).StartsWith("head_"))
                    {
                        Console.WriteLine("delete '{0}'", Path.GetFileName(file));
                        File.Delete(file);
                    }
                }
            }
            else
            {
                Console.WriteLine("Error: '{0}' does not exist", sourceZip);
                return false;
            }

            return true;
        }

        static bool UpdateMPFile(P4 p4, string strMultiplayerPed)
        {
            string ped = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(strMultiplayerPed));

            Console.WriteLine("\nped '{0}'", ped);
            DeleteDirectory(TEMP_PED_DIR);

            //String strMultiplayerPed = Path.Combine(STREAMED_PED_DIR, ped) + ".ped.zip";
            
            p4.Run("sync", strMultiplayerPed);
            p4.Run("edit", strMultiplayerPed);

            if (File.Exists(strMultiplayerPed))
            {
                Console.WriteLine("extracting '{0}'", strMultiplayerPed);

                using (ZipFile zip = new ZipFile(strMultiplayerPed))
                {
                    zip.ExtractAll(TEMP_PED_DIR, ExtractExistingFileAction.OverwriteSilently);
                }

                foreach (string file in Directory.GetFiles(TEMP_PED_DIR, "*.*", SearchOption.AllDirectories))
                {
                    if (Path.GetFileName(file.ToLower()).StartsWith("head_"))
                    {
                        Console.WriteLine("delete '{0}'", Path.GetFileName(file));
                        File.Delete(file);
                    }
                }

                foreach (string file in Directory.GetFiles(Path.Combine(TEMP_MP_HEAD_DIR, "mp_heads"), "*.*", SearchOption.AllDirectories))
                {
                    string strDestinationFile = Path.Combine(TEMP_PED_DIR, ped, Path.GetFileName(file));
                    Console.WriteLine("copy '{0}'", Path.GetFileName(file));
                    File.Copy(file, strDestinationFile);
                }

                using (ZipFile z = new ZipFile())
                {
                    z.AddDirectory(TEMP_PED_DIR);
                    Console.WriteLine("save zip '{0}'", strMultiplayerPed);
                    File.SetAttributes(strMultiplayerPed, FileAttributes.Normal);
                    z.Save(strMultiplayerPed);
                }
            }
            else
            {
                Console.WriteLine("Error: '{0}' does not exist", strMultiplayerPed);
                return false;
            }

            return true;
        }

        static bool UpdateHeadTargets(P4 p4, string sourceFile)
        {
            p4.Run("sync", sourceFile);
            p4.Run("edit", sourceFile);

            DeleteDirectory(TEMP_MP_HEAD_TARGET_IDD_DIR);

            // Dump the contents of the head target file to the temp dir
            if (File.Exists(sourceFile))
            {
                Console.WriteLine("extracting '{0}'", sourceFile);

                using (ZipFile zip = new ZipFile(sourceFile))
                {
                    zip.ExtractAll(TEMP_MP_HEAD_TARGET_DIR, ExtractExistingFileAction.OverwriteSilently);
                }
            }
            else
            {
                Console.WriteLine("Error: '{0}' does not exist", sourceFile);
                return false;
            }

            using (ZipFile zip = new ZipFile(Path.Combine(TEMP_MP_HEAD_DIR, "mp_heads") + ".ift.zip"))
            {
                zip.ExtractAll(TEMP_MP_HEAD_IFT_DIR, ExtractExistingFileAction.OverwriteSilently);
            }

            String strHeadTargetsIftFile = Path.Combine(TEMP_MP_HEAD_TARGET_DIR, "mp_headtargets") + ".ift.zip";
            using (ZipFile zip = new ZipFile(strHeadTargetsIftFile))
            {
                zip.ExtractAll(TEMP_MP_HEAD_TARGET_IFT_DIR, ExtractExistingFileAction.OverwriteSilently);
            }

            String strDummyMeshSrc = Path.Combine(TEMP_MP_HEAD_IFT_DIR, "meshdummyforfrag.mesh");
            String strDummyMeshDest = Path.Combine(TEMP_MP_HEAD_TARGET_IFT_DIR, "meshdummyforfrag.mesh");

            File.Copy(strDummyMeshSrc, strDummyMeshDest, true);
            Console.WriteLine("copy '{0}'", Path.GetFileName(strDummyMeshSrc));

            String strSkeletonSrc = Path.Combine(TEMP_MP_HEAD_IFT_DIR, "skeleton.skel");
            String strSkeletonDest = Path.Combine(TEMP_MP_HEAD_TARGET_IFT_DIR, "skeleton.skel");

            File.Copy(strSkeletonSrc, strSkeletonDest, true);
            Console.WriteLine("copy '{0}'", Path.GetFileName(strSkeletonSrc));

            using (ZipFile zip = new ZipFile())
            {
                AddDirectoryToZip(zip, TEMP_MP_HEAD_TARGET_IFT_DIR);
                Console.WriteLine("save zip '{0}'", Path.GetFileName(strHeadTargetsIftFile));
                File.SetAttributes(strHeadTargetsIftFile, FileAttributes.Normal);
                zip.Save(strHeadTargetsIftFile);
            }

            // We are only copying head_000 from head_dir
            string headZero = Path.Combine(TEMP_MP_HEAD_DIR, "mp_heads", "head_000_r.idd.zip");
            DeleteDirectory(TEMP_MP_HEAD_IDD_DIR);
            Console.WriteLine("extracting '{0}'", Path.GetFileName(headZero));
            // Extract the contents of the idd file to the temp folder
            using (ZipFile zip = new ZipFile(headZero))
            {
                zip.ExtractAll(TEMP_MP_HEAD_IDD_DIR, ExtractExistingFileAction.OverwriteSilently);
            }

            //loop through all heads in target - FT b*:1913003
            //this will still match head_diff's
            string[] files = Directory.GetFiles(TEMP_MP_HEAD_TARGET_DIR, "head_*.idd.zip", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                // copy the mesh from head_000 to target
                String strTargetHead = Path.Combine(TEMP_MP_HEAD_TARGET_DIR, "mp_headtargets", Path.GetFileName(file));
                if (File.Exists(strTargetHead))
                {
                    DeleteDirectory(TEMP_MP_HEAD_TARGET_IDD_DIR);

                    Console.WriteLine("extracting '{0}'", Path.GetFileName(strTargetHead));

                    using (ZipFile zip = new ZipFile(strTargetHead))
                    {
                        zip.ExtractAll(TEMP_MP_HEAD_TARGET_IDD_DIR, ExtractExistingFileAction.OverwriteSilently);
                    }

                    // get all meshes including lods
                    string[] meshes = Directory.GetFiles(Path.Combine(TEMP_MP_HEAD_IDD_DIR, Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(headZero))), "*.mesh");
                    string[] target_meshes = Directory.GetFiles(Path.Combine(TEMP_MP_HEAD_TARGET_IDD_DIR, Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(file))), "*.mesh");
                    foreach (string mesh in meshes)
                    {
                        String strHeadMeshSrc = Path.Combine(TEMP_MP_HEAD_IDD_DIR, Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(headZero)), Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(mesh)) + ".mesh");
                        String strHeadMeshDest = Path.Combine(TEMP_MP_HEAD_TARGET_IDD_DIR, Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(file)), Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(mesh)) + ".mesh");
                        strHeadMeshDest = strHeadMeshDest.Replace("head_000_r", Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(file)));

                        File.Copy(strHeadMeshSrc, strHeadMeshDest, true);
                        Console.WriteLine("copy '{0}'", Path.GetFileName(strHeadMeshSrc));
                    }

                    strSkeletonSrc = Path.Combine(TEMP_MP_HEAD_IDD_DIR, Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(headZero)), "skeleton.skel");
                    strSkeletonDest = Path.Combine(TEMP_MP_HEAD_TARGET_IDD_DIR, Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(file)), "skeleton.skel");

                    File.Copy(strSkeletonSrc, strSkeletonDest, true);
                    Console.WriteLine("copy '{0}'", Path.GetFileName(strSkeletonSrc));

                    String strCustomRbsSrc = Path.Combine(TEMP_MP_HEAD_IDD_DIR, "custom.rbs");
                    String strCustomRbsDest = Path.Combine(TEMP_MP_HEAD_TARGET_IDD_DIR, "custom.rbs");

                    File.Copy(strCustomRbsSrc, strCustomRbsDest, true);
                    Console.WriteLine("copy '{0}'", Path.GetFileName(strCustomRbsSrc));

                    String strCustomFinishSrc = Path.Combine(TEMP_MP_HEAD_IDD_DIR, "custom_finish.rbs");
                    String strCustomFinishDest = Path.Combine(TEMP_MP_HEAD_TARGET_IDD_DIR, "custom_finish.rbs");

                    File.Copy(strCustomFinishSrc, strCustomFinishDest, true);
                    Console.WriteLine("copy '{0}'", Path.GetFileName(strCustomFinishSrc));

                    using (ZipFile zip = new ZipFile())
                    {
                        AddDirectoryToZip(zip, TEMP_MP_HEAD_TARGET_IDD_DIR);
                        Console.WriteLine("save zip '{0}'", Path.GetFileName(strTargetHead));
                        File.SetAttributes(strTargetHead, FileAttributes.Normal);
                        zip.Save(strTargetHead);
                    }
                }
                //else
                //{
                //    Console.WriteLine("Error: '{0}' does not exist", strTargetHead);
                //    return false;
                //}
            }

            using (ZipFile zip = new ZipFile())
            {
                AddDirectoryToZip(zip, TEMP_MP_HEAD_TARGET_DIR);
                Console.WriteLine("save zip '{0}'", sourceFile);
                File.SetAttributes(sourceFile, FileAttributes.Normal);
                zip.Save(sourceFile);
            }

            return true;
        }

        static int Main(string[] args)
        {
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_SOURCEZIP, LongOption.ArgType.Required, "Source Zip."),
                new LongOption(OPTION_DESTINATIONZIPS, LongOption.ArgType.Required, "Zips Destination."),
                new LongOption(OPTION_HEADTARGETZIP, LongOption.ArgType.Required, "Head Zip Target."),
                });
                
                String sourceZip = options[OPTION_SOURCEZIP] as String;
                String destZips = options[OPTION_DESTINATIONZIPS] as String;
                String headtargetsZip = options[OPTION_HEADTARGETZIP] as String;

                string[] zips = destZips.Split(',');

                Console.WriteLine("Source: {0}", sourceZip);
                Console.WriteLine("Destination File(s): ({0}) {1}", zips.Length, destZips);
                Console.WriteLine("HeadTargets File: {0}", headtargetsZip);
                Console.WriteLine("");

                using (P4 p4 = new P4())
                {
                    p4.Connect();

                    //GatherPedTypeData();

                    if (!ExtractMPHeadsFile(sourceZip)) return 1;

                    foreach (string ped in zips)
                    {
                        if (!UpdateMPFile(p4, ped)) return 1;
                    }

                    if (!UpdateHeadTargets(p4, headtargetsZip)) return 1;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 1;
            }

            return 0;
        }
    }
}
