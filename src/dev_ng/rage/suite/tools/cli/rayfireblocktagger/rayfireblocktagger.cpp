#include "system/main.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"
#include "crclip/clip.h"
#include "cranimation/animation.h"

using namespace rage;

REQ_PARAM( inputClip, "input clip to copy block tags from" );
REQ_PARAM( outputClip, "output clip to copy block tags too" );
PARAM( skipValidation, "skip duration validation");

int Main()
{
	crAnimation::InitClass();
	crClip::InitClass();

    const char* pInputClip = "";
    if(!PARAM_inputClip.Get( pInputClip ))
	{
		Errorf("Input clip not specified");
		return -1;
	}

	const char* pOutputClip = "";
	if(!PARAM_outputClip.Get( pOutputClip ))
	{
		Errorf("Output clip not specified");
		return -1;
	}

	crClip* outputClip = crClip::AllocateAndLoad(pOutputClip);
	if(outputClip)
	{
		for( int i = 0; i < outputClip->GetTags()->GetNumTags(); ++i)
		{
			crTag* blockTag = const_cast<crTag*>(outputClip->GetTags()->GetTag(i));
			if(stricmp(blockTag->GetName(),"block") == 0)
			{
				Displayf("Wiped block tag @ %f/%f/%f", blockTag->GetStart(), blockTag->GetMid(), blockTag->GetEnd());
				outputClip->GetTags()->RemoveTag(*blockTag);
				--i;
			}
		}

		crClip* inputClip = crClip::AllocateAndLoad(pInputClip);
		if(inputClip)
		{
			if(!PARAM_skipValidation.Get())
			{
				if(outputClip->GetDuration() != inputClip->GetDuration())
				{
					Errorf("Input and Output clips have different durations");
					return -1;
				}
			}
			
			for( int i = 0; i < inputClip->GetTags()->GetNumTags(); ++i)
			{
				crTag* blockTag = const_cast<crTag*>(inputClip->GetTags()->GetTag(i));
				if(stricmp(blockTag->GetName(),"block") == 0)
				{
					outputClip->GetTags()->AddTag(*blockTag);
					Displayf("Added block tag @ %f/%f/%f", blockTag->GetStart(), blockTag->GetMid(), blockTag->GetEnd());
				}
			}

			if(!outputClip->Save(pOutputClip))
			{
				Errorf("Failed to save '%s', readonly?", pOutputClip);
				return -1;
			}
		}
	}

    return 0;
}
