﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.OS;
using RSG.ManagedRage.ClipAnimation;
using System.IO;
using System.Xml;

namespace RSG.Pipeline.Animation.CutsceneZipCreator
{
    sealed class Program
    {

        #region Constants
        const string PROPERTY_SUFFIX = "_DO_NOT_RESOURCE";
        const string PROPERTY_FBX_FILE = "FBXFile" + PROPERTY_SUFFIX;

        const string EXT_CLIP = ".clip";
        #endregion // Constants

        static bool RunProcess(string strExe, string strArguments)
        {
            try
            {
                System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                pExe.StartInfo.FileName = strExe;
                pExe.StartInfo.Arguments = strArguments;
                pExe.StartInfo.CreateNoWindow = false;
                pExe.StartInfo.UseShellExecute = false;
                pExe.StartInfo.RedirectStandardOutput = false ;
                pExe.Start();

                if (true)
                {
                    pExe.WaitForExit();

                    if (pExe.ExitCode != 0)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                return false;
            }
        }

        [STAThread]
        static int Main(string[] args)
        {
            MClip.Init();

            string[] cutDirs = Directory.GetDirectories(String.Format("{0}\\assets\\cuts",Environment.GetEnvironmentVariable("RS_PROJROOT")));
            string[] sceneSplit = String.Format("{0}\\art\\animation\\cutscene\\!!scenes",Environment.GetEnvironmentVariable("RS_PROJROOT")).Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            if (!Directory.Exists(String.Format("{0}\\assets\\export\\anim\\cutscene", Environment.GetEnvironmentVariable("RS_PROJROOT"))))
                Directory.CreateDirectory(String.Format("{0}\\assets\\export\\anim\\cutscene", Environment.GetEnvironmentVariable("RS_PROJROOT")));

            using (XmlWriter writer = XmlWriter.Create(String.Format("{0}\\assets\\export\\anim\\cutscene\\master_icd_list.xml", Environment.GetEnvironmentVariable("RS_PROJROOT")), settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Assets");

                foreach (string dir in cutDirs)
                {
                    string[] clipFiles = Directory.GetFiles(dir, ("*" + EXT_CLIP));

                    bool foundClipProperty = false;

                    foreach (string clip in clipFiles)
                    {
                        MClip rageClip = new MClip();
                        bool clipLoaded = (bool)rageClip.Load(clip);
                        if (clipLoaded)
                        {
                            MProperty propFBX = rageClip.FindProperty(PROPERTY_FBX_FILE);
                            if (propFBX != null)
                            {
                                foundClipProperty = true;

                                string fbx = (propFBX.GetPropertyAttributes()[0] as MPropertyAttributeString).GetString();
                                fbx = fbx.Replace("/", "\\"); // some of the paths use different slashes...

                                string[] assetSplit = fbx.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

                                string missionName = String.Empty;
                                try
                                {
                                    missionName = assetSplit[sceneSplit.Length];
                                }
                                catch (Exception e) // Catch if we have a test scene exported from Desktop for example
                                {
                                    Console.WriteLine(fbx);
                                    Console.WriteLine(dir);
                                    Console.WriteLine(e.Message);
                                    break;
                                }

                                writer.WriteStartElement("ZipArchive");
                                writer.WriteAttributeString("path", String.Format(@"{0}\assets\export\anim\cutscene\{1}\{2}.icd.zip", Environment.GetEnvironmentVariable("RS_PROJROOT"), missionName, Path.GetFileName(dir)));

                                writer.WriteStartElement("Directory");
                                writer.WriteAttributeString("path", dir);
                                writer.WriteEndElement();

                                string[] subDirs = Directory.GetDirectories(dir);
                                foreach (string subDir in subDirs)
                                {
                                    if (Path.GetFileName(subDir).ToLower() == "obj" ||
                                        Path.GetFileName(subDir).ToLower() == "toybox") continue;

                                    writer.WriteStartElement("Directory");
                                    writer.WriteAttributeString("path", dir + "\\" + Path.GetFileName(subDir));
                                    writer.WriteAttributeString("destination", "\\" + Path.GetFileName(subDir));
                                    writer.WriteEndElement();

                                    string[] subsubDirs = Directory.GetDirectories(subDir);
                                    foreach (string subsubDir in subsubDirs)
                                    {
                                        if (Path.GetFileName(subsubDir).ToLower() == "toybox") continue;

                                        writer.WriteStartElement("Directory");
                                        writer.WriteAttributeString("path", subDir + "\\" + Path.GetFileName(subsubDir));
                                        writer.WriteAttributeString("destination", Path.GetFileName(subDir) + "\\" + Path.GetFileName(subsubDir));
                                        writer.WriteEndElement();
                                    }
                                }

                                writer.WriteEndElement();

                                break;
                            }
                        }
                    }

                    if (foundClipProperty == false)
                    {
                        // the directory contains no clips with an fbx property so they need re-exporting
                        Console.WriteLine("No clips within " + dir + " have " + PROPERTY_FBX_FILE + " clip properties");
                    }
                }
            }

            RunProcess(String.Format("{0}\\ironlib\\prompt.bat", Environment.GetEnvironmentVariable("RS_TOOLSROOT")), String.Format("{0}\\bin\\ironruby\\bin\\ir.exe {0}\\ironlib\\util\\data_convert_file.rb {1}\\assets\\export\\anim\\cutscene\\master_icd_list.xml", Environment.GetEnvironmentVariable("RS_TOOLSROOT"), Environment.GetEnvironmentVariable("RS_PROJROOT")));

            return ( 1 );
        }
    }
}
