// 
// cliptools/clipcombinecore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#ifndef CLIPTOOLS_CLIPCOMBINECORE_H
#define CLIPTOOLS_CLIPCOMBINECORE_H

#include "atl/array.h"

namespace rage
{

class crClip;

namespace crClipCombine
{

// PURPOSE: Combine clips
extern bool CombineClips(const atArray<const crClip*>& srcClips, crClip*& destClip);


}; // namespace crClipCombine

}; // namespace rage

#endif // CLIPTOOLS_CLIPCOMBINECORE_H
