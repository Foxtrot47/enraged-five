// 
// cliptools/clipcoalescecore.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "clipcoalescecore.h"

#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "cranimation/frame.h"
#include "crclip/clipanimation.h"
#include "crclip/clipanimations.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tags.h"
#include "crtools/animconcatcore.h"
#include "crtools/animcompresscore.h"
#include "file/limits.h"
#include "file/asset.h"


namespace rage
{

namespace crClipCoalesce
{

////////////////////////////////////////////////////////////////////////////////

struct Section
{
	Section()
	{
	}

	~Section()
	{
		const int numClips = m_Clips.GetCount();
		for(int i=0; i<numClips; ++i)
		{
			if(m_Clips[i].m_Clip)
			{
				m_Clips[i].m_Clip->Release();
				m_Clips[i].m_Clip = NULL;
			}
		}
		m_Clips.Reset();
	}

	struct ClipHandle
	{
		ClipHandle()
			: m_Clip(NULL)
			, m_Index(-1)
		{
		}

		crAnimation* GetAnimation() const
		{
			return const_cast<crAnimation*>((m_Index>=0)?static_cast<crClipAnimations*>(m_Clip)->GetAnimation(m_Index):static_cast<crClipAnimation*>(m_Clip)->GetAnimation());
		}
		void SetAnimation(crAnimation& anim) const
		{
			(m_Index>=0)?static_cast<crClipAnimations*>(m_Clip)->SetAnimation(m_Index, anim):static_cast<crClipAnimation*>(m_Clip)->SetAnimation(anim);
		}
		void GetStartEndTimes(float& start, float& end) const 
		{ 
			(m_Index>=0)?static_cast<crClipAnimations*>(m_Clip)->GetStartEndTimes(m_Index, start, end):static_cast<crClipAnimation*>(m_Clip)->GetStartEndTimes(start, end); 
		}
		void SetStartEndTimes(float start, float end) 
		{ 
			(m_Index>=0)?static_cast<crClipAnimations*>(m_Clip)->SetStartEndTimes(m_Index, start, end):static_cast<crClipAnimation*>(m_Clip)->SetStartEndTimes(start, end); 
		}
		float GetRate() const 
		{ 
			return (m_Index>=0)?static_cast<crClipAnimations*>(m_Clip)->GetRate(m_Index):static_cast<crClipAnimation*>(m_Clip)->GetRate(); 
		}
		void SetRate(float rate) 
		{ 
			(m_Index>=0)?static_cast<crClipAnimations*>(m_Clip)->SetRate(m_Index, rate):static_cast<crClipAnimation*>(m_Clip)->SetRate(rate); 
		}

		crClip* m_Clip;
		int m_Index;
	};

	atArray<ClipHandle> m_Clips;

	const crAnimation* m_Anim;
	u32 m_DefaultSize;

	u32 m_StartFrame;
	u32 m_NumFrames;

	u32 m_Signature;
	float m_FrameRate;
	
	float m_Start;
	float m_End;
};

////////////////////////////////////////////////////////////////////////////////

int SectionCompareCb(Section* const* pps0, Section* const* pps1)
{
	const Section& s0 = **pps0;
	const Section& s1 = **pps1;

	if(s0.m_NumFrames > s1.m_NumFrames)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

////////////////////////////////////////////////////////////////////////////////

void GetValues(const crFrame& frame, float values[4])
{
	crFrame::Dof frameDof(frame, 0);

	for(int i=0; i<4; ++i)
	{
		values[i] = (&frameDof.GetUnsafe<float>())[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

bool CheckConstant(const crAnimTrack& track, const crAnimation& anim, u32& match, float values[4])
{
	crFrameDataSingleDof frameData(track.GetTrack(), track.GetId(), track.GetType());

	int numChannels = 0;
	switch(track.GetType())
	{
	case kFormatTypeVector3:
		numChannels = 3;
		break;
	case kFormatTypeQuaternion:
		numChannels = 4;
		break;
	case kFormatTypeFloat:
		numChannels = 1;
		break;
	default:
		return false;
	}

	match = (1<<numChannels)-1;

	crFrameSingleDof frame(frameData);
	frame.Composite(anim, 0.f);
	GetValues(frame, values);

	const int numFrames = anim.GetNumInternalFrames();
	for(int f=1; match && (f<numFrames); ++f)
	{
		const float time = anim.GetDuration()*float(f)/float(anim.GetNumInternalFrames()-1);
		frame.Composite(anim, time);

		float v[4];
		GetValues(frame, v);

		for(int i=0; i<numChannels; ++i)
		{
			if(!IsClose(values[i], v[i]))
			{
				match &= ~(1<<i);
			}
		}
	}

	return match != 0;
}

////////////////////////////////////////////////////////////////////////////////

int CalcScore(const crAnimation& anim0, const crAnimation& anim1)
{
	const int valueMatchScore = 48;
	const int constMatchScore = 12;

	if((anim0.GetSignature() != anim1.GetSignature()) || (anim0.GetNumTracks() != anim1.GetNumTracks()))
	{
		return 0;
	}

	int score = 0;

	const int numTracks = anim0.GetNumTracks();
	for(int i=0; i<numTracks; ++i)
	{
		const crAnimTrack& track0 = *anim0.GetTrack(i);
		const crAnimTrack& track1 = *anim1.GetTrack(i);

		if((track0.GetTrack() != track1.GetTrack()) || (track0.GetId() != track1.GetId()) || (track0.GetType() != track1.GetType()))
		{
			return 0;
		}

		int numChannels = 0;
		switch(track0.GetType())
		{
		case kFormatTypeQuaternion:
			numChannels = 1;
			// break intentionally omitted
		case kFormatTypeVector3:
			numChannels += 2;
			// break intentionally omitted
		case kFormatTypeFloat:
			numChannels += 1;
			{
				u32 match0;
				u32 match1;
				float value0[4];
				float value1[4];

				if(CheckConstant(track0, anim0, match0, value0) && CheckConstant(track1, anim1, match1, value1))
				{
					for(int b=0; b<numChannels; ++b)
					{
						u32 mask = 1<<b;
						if((mask&match0&match1))
						{
							if(IsClose(value0[b], value1[b]))
							{
								score += valueMatchScore / numChannels;
							}
							else
							{
								score += constMatchScore / numChannels;
							}
						}
					}
				}
			}
			break;

		default:
			break;
		}
	}

	return score;
}

////////////////////////////////////////////////////////////////////////////////

int CalcMemory(const crAnimation& anim0, const crAnimation& anim1, const crAnimTolerance& tolerance, u32 maxBlockSize)
{
	float frameRate = anim0.GetDuration() / float(anim0.GetNumInternalFrames()-1);

	crAnimation anim01;
	crAnimConcatCore::ConcatAnimations(anim0, anim1, anim01, frameRate, frameRate);

	crAnimation dest01;
	crAnimCompress::CompressAnimation(anim01, dest01, tolerance, maxBlockSize, false, false);

	return dest01.ComputeSize();
}

////////////////////////////////////////////////////////////////////////////////

bool ResampleAnim(const crAnimation& srcAnim, crAnimation& destAnim, float maxFrequency)
{
	const float duration = srcAnim.GetDuration();

	int numFrames = int(ceilf((srcAnim.GetDuration() * maxFrequency) - SMALL_FLOAT)) + 1;

	float frequency = float(numFrames-1)/duration;
	if(IsClose(frequency, maxFrequency))
	{
		frequency = maxFrequency;
	}

	float reportedDuration = duration;
	if(frequency != maxFrequency)
	{
		reportedDuration = float(numFrames-1)/maxFrequency;
	}

	destAnim.Create(u16(numFrames), reportedDuration, srcAnim.IsLooped(), srcAnim.HasMoverTracks(), crAnimation::sm_DefaultFramesPerChunk, srcAnim.IsRaw());
	destAnim.SetProjectFlags(srcAnim.GetProjectFlags());
	destAnim.SetName(srcAnim.GetName());

	atArray<crFrame> frames(0, numFrames);
	atArray<const crFrame*> framePtrs(0, numFrames);
	for(int i=0; i<numFrames; ++i)
	{
		float time = Clamp(float(i)/frequency, 0.f, duration);
		if(IsNearZero(time))
		{
			time = 0.f;
		}
		else if(IsClose(time, duration))
		{
			time = srcAnim.GetDuration();
		}

		crFrame& fi = frames.Grow();
		fi.InitCreateAnimationDofs(srcAnim);
		fi.Composite(srcAnim, time);

		framePtrs.Grow() = &fi;
	}
	destAnim.CreateFromFramesFast(framePtrs, CR_LOSSLESS_ANIM_TOLERANCE);

	return true;
}	

////////////////////////////////////////////////////////////////////////////////

float NormalizeFrameRate(float frameRate)
{
	float frameFreq = 1.f/frameRate;
	frameFreq = floorf(frameFreq+0.5f);

	float normFrameRate = 1.f/frameFreq;
	if(AreNearlyEqual(normFrameRate, frameRate, 0.0001f))
	{
		return normFrameRate;
	}

	return frameRate;
}

////////////////////////////////////////////////////////////////////////////////

bool ResampleClip(Section::ClipHandle& sectionClip, float newFrameRate)
{
	const crAnimation* anim = sectionClip.GetAnimation();

	float start, end, rate;
	sectionClip.GetStartEndTimes(start, end);
	rate = sectionClip.GetRate();

	float frameRate = anim->GetDuration()/float(anim->GetNumInternalFrames()-1);
	frameRate = NormalizeFrameRate(frameRate);

	newFrameRate = NormalizeFrameRate(newFrameRate);
	if(newFrameRate > 0.f && !IsClose(newFrameRate, frameRate))
	{
		crAnimation* newAnim = rage_new crAnimation;
		if(ResampleAnim(*anim, *newAnim, 1.f/newFrameRate))
		{
			anim->AddRef();
			sectionClip.SetAnimation(*newAnim);

			if(!AreNearlyEqual(anim->GetDuration(), newAnim->GetDuration()))
			{
				float adjustment = newAnim->GetDuration() / anim->GetDuration();

				start = Clamp(start*adjustment, 0.f, newAnim->GetDuration());
				if(IsNearZero(start))
				{
					start = 0.f;
				}

				end = Clamp(end*adjustment, 0.f, newAnim->GetDuration());
				if(IsClose(end, newAnim->GetDuration()))
				{
					end = newAnim->GetDuration();
				}

				rate *= adjustment;

				sectionClip.SetStartEndTimes(start, end);
				sectionClip.SetRate(rate);
			}
			else
			{
				sectionClip.SetStartEndTimes(start, end);
				sectionClip.SetRate(rate);
			}

			anim->Release();
			anim = newAnim;

			frameRate = newFrameRate;
		}

		newAnim->Release();
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

Section* CreateSection(Section::ClipHandle& sectionClip, bool memory, const crAnimTolerance& tolerance, u32 maxBlockSize)
{
	const crAnimation* anim = sectionClip.GetAnimation();

	float frameRate = anim->GetDuration()/float(anim->GetNumInternalFrames()-1);
	frameRate = NormalizeFrameRate(frameRate);

	float start, end, rate;
	sectionClip.GetStartEndTimes(start, end);
	rate = sectionClip.GetRate();

	Section* section = rage_new Section;

	section->m_Clips.Grow() = sectionClip;

	section->m_Anim = anim;

	section->m_StartFrame = u32(floorf(start / frameRate));
	section->m_NumFrames = Min(u32(ceilf((end - (float(section->m_StartFrame)*frameRate) - SMALL_FLOAT) / frameRate))+1, u32(anim->GetNumInternalFrames()));
	if(IsClose(start, 0.f))
	{
		section->m_StartFrame = 0;
		if(IsClose(end, anim->GetDuration()))
		{
			section->m_NumFrames = anim->GetNumInternalFrames();
		}
	}

	section->m_FrameRate = frameRate;
	section->m_Signature = anim->GetSignature() ^ u32((1.f/section->m_FrameRate) + 0.5f);

	if(memory)
	{
		crAnimation dest;
		crAnimCompress::CompressAnimation(*section->m_Anim, dest, tolerance, maxBlockSize, false, false);
		section->m_DefaultSize = dest.ComputeSize();
	}

	return section;
}

////////////////////////////////////////////////////////////////////////////////

bool TransferTracks(atArray<u32> trackIds, crAnimation& srcAnim, crAnimation& destAnim)
{
	destAnim.Create(srcAnim.GetNumInternalFrames(), srcAnim.GetDuration(), srcAnim.IsLooped(), srcAnim.HasMoverTracks(), srcAnim.GetNumInternalFramesPerChunk(), srcAnim.IsRaw());

	int numTransferred = 0;
	for(int i=0; i<trackIds.GetCount(); ++i)
	{
		u8 track = u8(trackIds[i]>>16);
		u16 id = u16(trackIds[i]&0xffff);

		int idx;
		crAnimTrack* srcTrack = srcAnim.FindTrackIndex(track, id, idx);
		if(srcTrack)
		{
			crAnimTrack* destTrack = rage_new crAnimTrack(track, id);
			destTrack->CopyTrack(*srcTrack);

			destAnim.CreateTrack(destTrack);

			srcAnim.DeleteTrack(idx);

			numTransferred++;
		}
	}

	return (numTransferred > 0);
}

////////////////////////////////////////////////////////////////////////////////

bool GenerateSections(atArray<Section*>& sections, const atArray<const char*>& clipFilenames, bool memory, const crAnimTolerance& defaultTolerance, u32 maxBlockSize)
{
	const int numClips = clipFilenames.GetCount();
	sections.Reserve(numClips);

	// load all the clips, gather the section information
	for(int i=0; i<numClips; ++i)
	{
		crClip* clip = crClip::AllocateAndLoad(clipFilenames[i]);
		if(clip)
		{
			switch(clip->GetType())
			{
			case crClip::kClipTypeAnimation:
			case crClip::kClipTypeAnimationExpression:
				{
					Section::ClipHandle sectionClip;
					sectionClip.m_Clip = clip;

					crAnimation* anim = sectionClip.GetAnimation();
					if(anim)
					{
						float frameRate = anim->GetDuration()/float(anim->GetNumInternalFrames()-1);
						frameRate = NormalizeFrameRate(frameRate);

						float resampleFrameRate = frameRate;

						if(clip->GetProperties())
						{
							crAnimToleranceComplex tolerance;
							if(crAnimCompress::ProcessPropertyComplexCompressionSettings(tolerance, *clip->GetProperties()))
							{
								atArray<float> frequencies;
								for(u32 t=0; t<anim->GetNumTracks(); ++t)
								{
									crAnimTrack* track = anim->GetTrack(t);
									float freq = tolerance.GetMinFrequency(track->GetTrack(), track->GetId(), track->GetType());

									bool found = false;
									for(int f=0; f<frequencies.GetCount(); ++f)
									{
										if(IsClose(frequencies[f], freq))
										{
											found = true;
											break;
										}
										else if(frequencies[f] < freq)
										{
											frequencies.Insert(f) = freq;
											found = true;
											break;
										}
									}
									if(!found)
									{
										frequencies.Grow() = freq;
									}
								}

								int maxFrequencies = 3;
								if(clip->GetType() == crClip::kClipTypeAnimationExpression)
								{
									maxFrequencies = 1;
								}

								// too many frequencies, truncate (by repeatedly removing second highest frequency)
								while(frequencies.GetCount() > maxFrequencies)
								{
									Warningf("WARNING - too many different frequencies specified on '%s' changing %f hz -> %f hz", clip->GetName(), frequencies[1], frequencies[0]);
									frequencies.Delete(1);
								}

								if(frequencies.GetCount() > 1)
								{
									// create new parallel clip
									crClipAnimations* newClip = crClipAnimations::AllocateAndCreate();
									newClip->SetParallel(true);
									newClip->SetName(clip->GetName());
									newClip->SetLooped(clip->IsLooped());
									newClip->GetProperties()->CopyProperties(*clip->GetProperties());
									newClip->GetTags()->CopyTags(*clip->GetTags());

									// get existing clip section settings
									float start, end, rate;
									sectionClip.GetStartEndTimes(start, end);
									rate = sectionClip.GetRate();
									
									// set up new parallel clip
									sectionClip.m_Clip = newClip;
									sectionClip.m_Index = 0;
									newClip->AddAnimation(*anim);
									sectionClip.SetRate(rate);
									sectionClip.SetStartEndTimes(start, end);
									sectionClip.SetRate(rate);

									delete clip;
									clip = newClip;

									int numParallel = 1;
									for(int f=1; f<frequencies.GetCount(); ++f)
									{
										atArray<u32> trackIds;
										for(u32 t=0; t<anim->GetNumTracks(); ++t)
										{
											crAnimTrack* track = anim->GetTrack(t);
											float freq = tolerance.GetMinFrequency(track->GetTrack(), track->GetId(), track->GetType());

											if(IsClose(frequencies[f], freq))
											{
												switch(track->GetTrack())
												{
												case kTrackMoverTranslation:
												case kTrackMoverRotation:
												case kTrackMoverScale:
													Warningf("WARNING - unable to resample mover tracks on '%s' to %f hz, they must remain together at highest frequency %f hz", clip->GetName(), frequencies[f], frequencies[0]);
													break;
												default:
													trackIds.Grow() = (u32(track->GetTrack())<<16) | u32(track->GetId());
													break;
												}
											}
										}

										crAnimation* parallelAnim = rage_new crAnimation;
										parallelAnim->SetName(anim->GetName());
										parallelAnim->Create(anim->GetNumInternalFrames(), anim->GetDuration(), anim->IsLooped(), false, anim->GetNumInternalFramesPerChunk(), anim->IsRaw());
										parallelAnim->SetProjectFlags(anim->GetProjectFlags());

										if(TransferTracks(trackIds, *anim, *parallelAnim))
										{
											Section::ClipHandle parallelClip;
											parallelClip.m_Clip = clip;
											parallelClip.m_Index = numParallel++;

											newClip->AddAnimation(*parallelAnim);
											parallelAnim->Release();
											parallelClip.SetStartEndTimes(start, end);
											parallelClip.SetRate(rate);

											clip->AddRef();

											float resampleFrameRate = NormalizeFrameRate(1.f/frequencies[f]);
											if(!IsClose(frameRate, resampleFrameRate))
											{
												ResampleClip(parallelClip, resampleFrameRate);
											}

											sections.Grow() = CreateSection(parallelClip, memory, defaultTolerance, maxBlockSize);
										}
										else
										{
											delete parallelAnim;
										}
									}
								}
								resampleFrameRate = NormalizeFrameRate(1.f/frequencies[0]);
							}

							if(!IsClose(frameRate, resampleFrameRate))
							{
								ResampleClip(sectionClip, resampleFrameRate);
							}

							clip->GetProperties()->RemoveProperty("Compression_DO_NOT_RESOURCE");
						}

						sections.Grow() = CreateSection(sectionClip, memory, defaultTolerance, maxBlockSize);
					}
					else
					{
						Errorf("ERROR - failed to load animation associated with clip '%s'", clip->GetName());
						return false;
					}
				}
				break;

			default:
				{
					Errorf("ERROR - '%s' (%d) type clip '%s' found (coalesce only supports animation and expression type clips)", clip->GetTypeName(), int(clip->GetType()), clip->GetName());
					delete clip; 
					return false;
				}
			}
		}
		else
		{
			Errorf("ERROR - failed to load clip '%s'", clipFilenames[i]);
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool OptimizeSections(atArray<Section*>& sections)
{
	// search for overlapping sections
	for(int i=0; i<(sections.GetCount()-1); ++i)
	{
		Section& si = *sections[i];

		bool merged = false;
		do 
		{
			merged = false;
			for(int j=i+1; j<sections.GetCount(); ++j)
			{
				Section& sj = *sections[j];
				if(!stricmp(si.m_Anim->GetName(), sj.m_Anim->GetName()) && (si.m_Anim->GetNumInternalFrames() == sj.m_Anim->GetNumInternalFrames()) && (si.m_Signature == sj.m_Signature))
				{
					if((si.m_StartFrame <= sj.m_StartFrame)?((si.m_StartFrame+si.m_NumFrames-1)>=sj.m_StartFrame):((sj.m_StartFrame+sj.m_NumFrames-1)>=si.m_StartFrame))
					{
						u32 startFrame = Min(si.m_StartFrame, sj.m_StartFrame);
						si.m_NumFrames = Max(si.m_StartFrame+si.m_NumFrames, sj.m_StartFrame+sj.m_NumFrames)-startFrame;
						si.m_StartFrame = startFrame;

						for(int k=0; k<sj.m_Clips.GetCount(); ++k)
						{
							si.m_Clips.Grow() = sj.m_Clips[k];
							sj.m_Clips[k].m_Clip = NULL;
						}
						sj.m_Clips.Reset();

						Section& sn = *sections[sections.GetCount()-1];
						if(&sj != &sn)
						{
							sj = sn;
							sn.m_Clips.Reset();
						}
						delete sections.Top();
						sections.DeleteFast(sections.GetCount()-1);

						merged = true;
						break;
					}
				}
			}
		} 
		while(merged);
	}

	// limit matching signatures for performance
	const int numSections = sections.GetCount();
	for(int i=0; i<numSections; ++i)
	{
		const u32 signature = sections[i]->m_Signature;

		int numMatch = 0;
		for(int j=0; j<numSections; ++j)
		{
			if(sections[j]->m_Signature == signature)
			{
				numMatch++;
			}
		}

		const int maxMatch = 32;
		if(numMatch > maxMatch)
		{
			bool sign = true;
			for(int j=0; j<numSections; ++j)
			{
				if(sections[j]->m_Signature == signature)
				{
					if(sign)
					{
						sections[j]->m_Signature ^= i;
					}
					sign = !sign;
				}
			}
		}
	}

	// sort the sections
	sections.QSort(0, -1, SectionCompareCb);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool CoalesceSections(atArray<Section*> sections, u32 frameLimit, const char* outDir, const char* outPrefix, bool score, bool memory, const crAnimTolerance& tolerance, u32 maxBlockSize, bool verbose, bool saveClipsWithAnims)
{
	atArray<crClip*> clips;

	// build the concatenated animations
	int numConcat = 0;
	while(sections.GetCount())
	{
		Section& firstSection = *sections.Top();
		const float frameRate = firstSection.m_FrameRate;

		firstSection.m_Start = 0.f;
		int numFrames = firstSection.m_NumFrames;
		firstSection.m_End = float(numFrames-1)*frameRate;

		atArray<Section*> concatSections;
		concatSections.PushAndGrow(&firstSection);

		sections.Delete(sections.GetCount()-1);

		// add subsequent sections, up to frame limit
		if(memory)
		{
			Section* bestSection = NULL;
			do 
			{
				int bestMemory = 0;
				int bestSectionIdx = -1;
				bestSection = NULL;

				for(int i=sections.GetCount()-1; i>=0; --i)
				{
					Section& nextSection = *sections[i];
					if(firstSection.m_Signature == nextSection.m_Signature)
					{
						if((numFrames + nextSection.m_NumFrames) <= frameLimit)
						{
							int nextMemory = CalcMemory(*nextSection.m_Anim, *concatSections.Top()->m_Anim, tolerance, maxBlockSize) - (nextSection.m_DefaultSize+concatSections.Top()->m_DefaultSize);
							if(nextMemory < bestMemory)
							{
								bestMemory = nextMemory;
								bestSection = &nextSection;	
								bestSectionIdx = i;
							}
						}
					}
				}

				if(bestSection)
				{
					bestSection->m_Start = float(numFrames)*frameRate;
					numFrames += bestSection->m_NumFrames;
					bestSection->m_End = float(numFrames-1)*frameRate;

					concatSections.PushAndGrow(bestSection);

					sections.Delete(bestSectionIdx);
				}
			}
			while(bestSection);
		}
		else if(score)
		{
			Section* bestSection = NULL;
			do 
			{
				int bestScore = -1;
				int bestSectionIdx = -1;
				bestSection = NULL;

				for(int i=sections.GetCount()-1; i>=0; --i)
				{
					Section& nextSection = *sections[i];
					if(firstSection.m_Signature == nextSection.m_Signature)
					{
						if((numFrames + nextSection.m_NumFrames) <= frameLimit)
						{
							int nextScore = CalcScore(*nextSection.m_Anim, *concatSections.Top()->m_Anim);
							if(nextScore > bestScore)
							{
								bestScore = nextScore;
								bestSection = &nextSection;	
								bestSectionIdx = i;
							}
						}
					}
				}

				if(bestSection)
				{
					bestSection->m_Start = float(numFrames)*frameRate;
					numFrames += bestSection->m_NumFrames;
					bestSection->m_End = float(numFrames-1)*frameRate;

					concatSections.PushAndGrow(bestSection);

					sections.Delete(bestSectionIdx);
				}
			}
			while(bestSection);
		}
		else
		{
			// simple, keep adding longest signature match until exhausted
			for(int i=sections.GetCount()-1; i>=0; --i)
			{
				Section& nextSection = *sections[i];
				if(firstSection.m_Signature == nextSection.m_Signature)
				{
					if((numFrames + nextSection.m_NumFrames) <= frameLimit)
					{
						nextSection.m_Start = float(numFrames)*frameRate;
						numFrames += nextSection.m_NumFrames;
						nextSection.m_End = float(numFrames-1)*frameRate;

						concatSections.PushAndGrow(&nextSection);

						sections.Delete(i);
					}
				}
			}
		}

		// TODO --- special case single section?

		// create and save the concatenated animation
		const int numConcatSections = concatSections.GetCount();
		atArray<crAnimConcatCore::AnimationSection> accSections(numConcatSections, numConcatSections);
		for(int i=0; i<numConcatSections; ++i)
		{
			Section& section = *concatSections[i];
			crAnimConcatCore::AnimationSection& accSection = accSections[i];

			accSection.m_Start = section.m_StartFrame * frameRate;
			accSection.m_End = (section.m_StartFrame + section.m_NumFrames - 1) * frameRate;
			accSection.m_Overlap = -frameRate;
			accSection.m_SrcAnim = section.m_Anim; 
		}

		crAnimation* destAnim = rage_new crAnimation;
		if(crAnimConcatCore::ConcatAnimations(accSections, *destAnim, frameRate, false, 0.f, false, true))
		{
			char buf[RAGE_MAX_PATH];
			sprintf(buf, "%s%s%s%d.anim", outDir?outDir:"", (outDir&&outDir[0])?"\\":"", outPrefix?outPrefix:"", numConcat);
			destAnim->SetName(buf);

			if(verbose)
			{
				Displayf("Concatenated animation '%s' referenced by:", destAnim->GetName());
			}

			if(!destAnim->Save(buf))
			{
				Errorf("ERROR - failed to store concatenated animation '%s'", destAnim->GetName());
				delete destAnim;
				return false;
			}
		}
		else
		{
			Errorf("ERROR - failed to concatenate sequence (starting with animation '%s')", concatSections[0]->m_Anim->GetName());
			delete destAnim;
			return false;
		}

		// adjust the clips
		for(int i=0; i<concatSections.GetCount(); ++i)
		{
			Section& section = *concatSections[i];
			const float offset = section.m_Start - float(section.m_StartFrame)*frameRate;

			for(int j=0; j<section.m_Clips.GetCount(); ++j)
			{
				Section::ClipHandle& sectionClip = section.m_Clips[j];

				float start, end, rate;
				sectionClip.GetStartEndTimes(start, end);
				rate = sectionClip.GetRate();

				sectionClip.SetAnimation(*destAnim);
				
				start = Clamp(start+offset, section.m_Start, section.m_End);
				end = Clamp(end+offset, section.m_Start, section.m_End);

				sectionClip.SetStartEndTimes(start, end);
				sectionClip.SetRate(rate);
	
				if(verbose)
				{
					Displayf("'%s' [%d] start %f end %f rate %f duration %f", sectionClip.m_Clip->GetName(), sectionClip.m_Index, start, end, rate, (end-start)/rate);
				}

				bool found = false;
				for(int i=0; i<clips.GetCount(); ++i)
				{
					if(clips[i] == sectionClip.m_Clip)
					{
						found = true;
						break;
					}
				}
				if(!found)
				{
					clips.Grow() = sectionClip.m_Clip;
				}
			}
		}

		destAnim->Release();
		numConcat++;
	}

	// serialize the clips
	for(int i=0; i<clips.GetCount(); ++i)
	{
		crClip* clip = clips[i];

		bool success = false;
		if(saveClipsWithAnims)
		{
			ASSET.PushFolder(outDir);
			success = clip->Save(ASSET.FileName(clip->GetName()));
			ASSET.PopFolder();
		}
		else
		{
			success = clip->Save(clip->GetName());
		}
		if(!success)
		{
			Errorf("ERROR - failed to store adjusted clip '%s'", clip->GetName());
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool CoalesceClips(const atArray<const char*>& clipFilenames, u32 frameLimit, const char* outDir, const char* outPrefix, bool score, bool memory, bool verbose, bool saveClipsWithAnims)
{
	bool success = false;

	const crAnimTolerance& tolerance = CR_DEFAULT_ANIM_TOLERANCE;
	u32 maxBlockSize = CR_DEFAULT_MAX_BLOCK_SIZE;

	atArray<Section*> sections;
	if(GenerateSections(sections, clipFilenames, memory, tolerance, maxBlockSize))
	{
		OptimizeSections(sections);
		success = CoalesceSections(sections, frameLimit, outDir, outPrefix, score, memory, tolerance, maxBlockSize, verbose, saveClipsWithAnims);
	}

	// clean up
	for(int i=0; i<sections.GetCount(); ++i)
	{
		if(sections[i])
		{
			delete sections[i];
		}
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////

}; // namespace crClipCoalesce

}; // namespace rage


