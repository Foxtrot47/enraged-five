// 
// /clipconcat.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "clipconcatcore.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipserialanimations.h"
#include "file/asset.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
REQ_PARAM(clip, "Source clips (Example: clip0.clip,clip1.clip,clip3.clip)");
REQ_PARAM(out, "Output filename (Example: out.clip)");


int Main()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	const int bufSize = 8192;
	char buf[bufSize];
	const int maxClipNames = 128;
	const char* clipNames[maxClipNames];

	int numClipNames = PARAM_clip.GetArray(clipNames, maxClipNames, buf, bufSize);
	if(numClipNames < 2)
	{
		Quitf("Insufficient animation names provided");
	}

	// load clips:
	atArray< const crClip* > clips;
	for(int i=0; i<numClipNames; ++i)
	{
		crClip* clip = crClip::AllocateAndLoad(clipNames[i]);
		if(clip)
		{
			clips.Grow() = clip;
		}
		else
		{
			Quitf("ERROR - Failed to load clip '%s'", clipNames[i]);
		}
	}

	// combine and concatenation to destination clip:
	crClipAnimations* destClip = crClipAnimations::AllocateAndCreate();
	crClipConcat::ConcatClipMetadata(clips, destClip);
	crClipConcat::ConcatClipAnimation(clips, destClip);

	// save output clip:
	const char* OutFilename = NULL;
	PARAM_out.Get(OutFilename);
	if(!destClip->Save(OutFilename))
	{
		Quitf("ERROR - Failed to save clip file '%s'\n", OutFilename);
	}

	// cleanup:
	delete destClip;
	for(int i=0; i<clips.GetCount(); ++i)
	{
		delete clips[i];
	}
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}