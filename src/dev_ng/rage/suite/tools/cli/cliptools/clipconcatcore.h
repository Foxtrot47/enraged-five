// 
// cliptools/clipconcatcore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CLIPTOOLS_CLIPCONCATCORE_H
#define CLIPTOOLS_CLIPCONCATCORE_H

#include "atl/array.h"

namespace rage
{

class crClip;
class crClipAnimations;

namespace crClipConcat
{

// PURPOSE: Concatenate clip metadata, properties and tags
extern void ConcatClipMetadata(const atArray<const crClip*>& srcClips, crClip* destClip);

// PURPOSE: Concatenate clips animation with its start time, end time and rate
extern void ConcatClipAnimation(const atArray<const crClip*>& srcClips, crClipAnimations* destClip);

}; // namespace crClipConcat

}; // namespace rage

#endif // CLIPTOOLS_CLIPCONCATCORE_H
