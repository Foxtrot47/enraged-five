// 
// cliptools/clipcoalescecore.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CLIPTOOLS_CLIPCONCATCOALESCECORE_H
#define CLIPTOOLS_CLIPCONCATCOALESCECORE_H

#include "atl/array.h"
#include "cranimation/animtolerance.h"

namespace rage
{

class crClip;

namespace crClipCoalesce
{

// PURPOSE: Coalesce clips, concatenate their underlying animations
extern bool CoalesceClips(const atArray<const char*>& clipFilenames, u32 frameLimit, const char* outDir, const char* outPrefix, bool score, bool memory=false, const bool verbose=false, bool saveClipsWithAnims=false);

}; // namespace crClipCoalesce

}; // namespace rage

#endif // CLIPTOOLS_CLIPCONCATCOALESCECORE_H
