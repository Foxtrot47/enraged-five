// 
// cliptools/clipcoalescezip.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "clipcoalescecore.h"

#include "atl/array.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "file/asset.h"
#include "file/token.h"
#include "file/zipfile.h"
#include "system/main.h"
#include "system/param.h"

#if !__TOOL
#error "ClipCoalesceZip needs to use the default memory allocator. Compile a tool build instead."
#endif

using namespace rage;

PARAM(help, "Get command line help");
PARAM(zip, "Source zip containing clips");
PARAM(cliplist, "Source clip list (Example: clips.cliplist)");
PARAM(framelimit, "Maximum number of internal frames in concatenated anims (Example: 65535)");
PARAM(outdir, "Output directory for new concatenated animations (Example: C:\\out)");
PARAM(outprefix, "Output prefix for new concatenated animation files (Example: concat_)");
PARAM(score, "Use scoring to improve packing, selects adjacent anims that will compress together better");
PARAM(memory, "Use memory stats to improve packing, selects adjacent anims that will compress together better");
PARAM(verbose, "Verbose output (Example: concat_)");

int Main()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	atArray<atString> clipFilenames;

	u32 frameLimit = 65535;
	PARAM_framelimit.Get(frameLimit);

	const char* outDir = "$";
	PARAM_outdir.Get(outDir);

	const char* outPrefix = NULL;
	PARAM_outprefix.Get(outPrefix);

	bool score = PARAM_score.Get();
	bool memory = PARAM_memory.Get();

	bool verbose = PARAM_verbose.Get();

	// get list of clips
	fiZipfile zip;
	if(PARAM_zip.Get() && PARAM_cliplist.Get())
	{
		// process zip
		const char* zipBuf;

		const char* mountAs = "pack:/";

		char dir[MAX_PATH];
		ASSET.FullPath( dir, sizeof(dir), mountAs, "" );
		ASSET.RemoveNameFromPath(dir, sizeof(dir), dir );
		strcat(dir,"/");

		PARAM_zip.Get( zipBuf );

		if (!zip.Init(zipBuf))
		{
			Errorf("cannot load zip '%s'",zipBuf);
			return false;
		}

		if (!zip.MountAs(dir))
		{
			Errorf("cannot mount zip '%s' at '%s'",zipBuf,dir);
			return false;
		}

		const char* clipListFilename = NULL;
		PARAM_cliplist.Get(clipListFilename);

		fiSafeStream f(ASSET.Open(clipListFilename, "cliplist"));
		if(f)
		{
			fiTokenizer T(clipListFilename, f);	

			const int maxBufSize = RAGE_MAX_PATH;
			char buf[maxBufSize];
			while(T.GetLine(buf, maxBufSize) > 0)
			{
				char clipfile[maxBufSize];
				sprintf( clipfile, "%s%s.clip", dir, buf );
				clipFilenames.Grow() = clipfile;
			}
		}
		else
		{
			Quitf("ERROR - failed to open clip list file '%s'", clipListFilename);
		}
	}
	else
	{
		Quitf("ERROR - no clips supplied, either provide -clips or -cliplist on the command line");
	}

	const int numClips = clipFilenames.GetCount();
	atArray<const char*> clipFilenamePtrs(numClips, numClips);
	for(int i=0; i<numClips; ++i)
	{
		clipFilenamePtrs[i] = clipFilenames[i].c_str();
	}

	// coalesce clips
	ASSET.CreateLeadingPath( outDir );
	if(!crClipCoalesce::CoalesceClips(clipFilenamePtrs, frameLimit, outDir, outPrefix, score, memory, verbose, true))
	{
		Quitf("ERROR - failed to coalesce clips");
	}

	// cleanup:
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}