// 
// /clipedit.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "clipeditcore.h"

#include "cranimation/animation.h"
#include "cranimation/animdictionary.h"
#include "crclip/clip.h"
#include "crmetadata/dumpoutput.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "crmetadata/tagiterators.h"
#include "crtools/animcmdlinehelper.h"
#include "system/main.h"
#include "system/param.h"

#include "RsULog/Ulogger.h"

rage::atString GetTempLogFileName( const char* pPrefix );

using namespace rage;

PARAM(help, "Get command line help");
PARAM(clip, "Source clip(s) (Example: clip0.clip,clip1.clip ...)");
PARAM(cliplist, "Source clip(s) provided by .cliplist file (text file, one clip per line)");
PARAM(out, "Destination clip(s) (Example: out0.clip,out1.clip ...)");
PARAM(outlist, "Destination clip(s) provided by .cliplist file (text file, one clip per line)");

PARAM(converttype, "Convert clip to new clip type (Example: clipanimation|clipanimationexpression etc)");

PARAM(looped, "Set/clear clip loop status (Example: 0|1)");

PARAM(addproperty, "Add a new property attribute (Example: Kinematics,Velocity,crPropertyAttributeVector3,0.0,1.0,0.1)");
PARAM(editproperty, "Edit a property value - (Example: Kinematics,Velocity,0.0,1.0,0.1)");
PARAM(removeproperty, "Remove a property - or leave blank to remove all (Example: Speed)");

PARAM(addtag, "Add a new tag (Example: Pickup,0.0,0.1,Weapon,crPropertyAttributeString,AK47)");
PARAM(edittag, "Edit a tag's value (Example: Pickup,0.0,0.1,Weapon,AK48)");
PARAM(removetag, "Remove tag(s) - omit index to remove all of type, or leave blank to remove all (Example: crTagAnchor)");

PARAM(autoaddblocktag, "Automatically add block tags (based on discontinuities in camera root movement, (Example (20cm): 0.2)");

PARAM(batchaddtag, "Path to a file containing multiple sequences in the addtag format (1 per line)");
PARAM(batchedittag, "Path to a file containing multiple sequences in the edittag format (1 per line)");
PARAM(batchremovetag, "Path to a file containing multiple sequences in the removetag format (1 per line)");

PARAM(clipanimation, "Change crClipAnimation properties (start,end,rate,anim) - omit unchanged value(s) (Example: 0.0,3.0,1.0,new.anim");
PARAM(clipexpression, "Change crClipAnimationExpression expression");

PARAM(nullanimloader, "Indicates that the null animation loader should be used when loading a clip");
PARAM(ulog, "Universal logging." );

const int MAX_CLIPS = 256;
const int MAX_CLIP_BUF_SIZE = 65536;

void Init()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("clipedit").c_str(), "clipedit");
	}
}

void Shutdown()
{
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	ULOGGER.PostExport();
}

void ProcessParamSequence(const sysParam& param, int paramId, crClip*& clip)
{
	if(param.Get())
	{
		const int maxSeq = 64;
		const int maxSeqBufSize = 8192;

		const char* seq[maxSeq];
		char seqBuf[maxSeqBufSize];
		for(int i=0; i<maxSeq; ++i)
		{
			seq[i] = NULL;
		}

		int numSeq = param.GetArray(seq, maxSeq, seqBuf, maxSeqBufSize);
		crClipEdit::ProcessSequence(numSeq, seq, paramId, clip);
	}
}

void BatchProcessCommandFile(const char* szBatchCmdFile, sysParam& param, int paramId, crClip*& clip)
{
	fiStream *pBatchFileStream = ASSET.Open(szBatchCmdFile, "");
	if(pBatchFileStream)
	{
		//Read the file line by line and process each sequence
		char lineBuffer[1024];
		while(fgetline(lineBuffer, 1024, pBatchFileStream))
		{
			if(lineBuffer[0] == '#')
				continue;

			param.Set(lineBuffer);
			ProcessParamSequence(param, paramId, clip);
		}
		pBatchFileStream->Close();
	}
	else
	{
		ULOGGER.SetProgressMessage("ERROR: Failed to open batch command file : '%s'", szBatchCmdFile);
		Quitf("ERROR - Failed to open batch command file : '%s'", szBatchCmdFile);
	}
}

int Main()
{
	// initialization
	Init();
	
	// process help
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process clip list:
	const int maxClips = MAX_CLIPS;
	const int maxClipBufSize = MAX_CLIP_BUF_SIZE;

	int numClips = 0;
	const char* clipFilenames[maxClips];
	char clipBuf[maxClipBufSize];

	const char* clipListFile = NULL;
	if(PARAM_cliplist.Get(clipListFile))
	{
		numClips = crAnimCmdLineHelper::ProcessListFile(clipListFile, clipFilenames, maxClips, clipBuf, maxClipBufSize);
	}
	else
	{
		numClips = PARAM_clip.GetArray(clipFilenames, maxClips, clipBuf, maxClipBufSize);
	}
	if(numClips <= 0)
	{
		ULOGGER.SetProgressMessage("ERROR: No source clip(s) specified\n");
		Shutdown();
		Quitf("No source clip(s) specified\n");
	}

	// process out list:
	int numOut = 0;
	const char* outFilenames[maxClips];
	char outBuf[maxClipBufSize];

	const char* outListFile = NULL;
	if(PARAM_outlist.Get(outListFile))
	{
		numOut = crAnimCmdLineHelper::ProcessListFile(outListFile, outFilenames, maxClips, outBuf, maxClipBufSize);
	}
	else
	{
		numOut = PARAM_out.GetArray(outFilenames, maxClips, outBuf, maxClipBufSize);
	}
	if(numOut <= 0)
	{
		ULOGGER.SetProgressMessage("ERROR: No destination clip(s) specified\n");
		Shutdown();
		Quitf("No destination clip(s) specified\n");
	}
	if(numOut != numClips)
	{
		ULOGGER.SetProgressMessage("ERROR: Number of source clip(s) doesn't match number of destination clip(s)\n");
		Shutdown();
		Quitf("Number of source clip(s) doesn't match number of destination clip(s)\n");
	}

	// for each clip
	for(int i=0; i<numClips; ++i)
	{
		// load clip
		crAnimLoaderNull nullLoader;

		crClip* clip = NULL;
		
		if(PARAM_nullanimloader.Get())
		{
			clip = crClip::AllocateAndLoad(clipFilenames[i], &nullLoader);
		}
		else
		{
			clip = crClip::AllocateAndLoad(clipFilenames[i]);
		}

		if(!clip)
		{
			ULOGGER.SetProgressMessage("ERROR: failed to load clip '%s'", clipFilenames[i]);
			Errorf("ERROR - failed to load clip '%s'", clipFilenames[i]);
			continue;
		}

		// process looped
		int looped = false;
		if(PARAM_looped.Get(looped))
		{
			clip->SetLooped(looped != 0);
		}
		
		// process convert arguments
		ProcessParamSequence(PARAM_converttype, crClipEdit::kConvertType, clip);

		// process property arguments
		ProcessParamSequence(PARAM_editproperty, crClipEdit::kEditProperty, clip);
		ProcessParamSequence(PARAM_removeproperty, crClipEdit::kRemoveProperty, clip);
		ProcessParamSequence(PARAM_addproperty, crClipEdit::kAddProperty, clip);

		// process tag arguments
		ProcessParamSequence(PARAM_edittag, crClipEdit::kEditTag, clip);
		ProcessParamSequence(PARAM_removetag, crClipEdit::kRemoveTag, clip);
		ProcessParamSequence(PARAM_addtag, crClipEdit::kAddTag, clip);

		ProcessParamSequence(PARAM_autoaddblocktag, crClipEdit::kAutoAddBlockTag, clip);

		// process clip arguments
		ProcessParamSequence(PARAM_clipanimation, crClipEdit::kClipAnimation, clip);
		ProcessParamSequence(PARAM_clipexpression, crClipEdit::kClipExpression, clip);


		// process any batch commands
		const char* szBatchAddTagPath = NULL;
		if(PARAM_batchaddtag.Get(szBatchAddTagPath))
		{
			BatchProcessCommandFile(szBatchAddTagPath,PARAM_addtag, crClipEdit::kAddTag, clip);
		}

		const char* szBatchEditTagPath = NULL;
		if(PARAM_batchedittag.Get(szBatchEditTagPath))
		{
			BatchProcessCommandFile(szBatchEditTagPath,PARAM_edittag, crClipEdit::kEditTag, clip);
		}

		const char* szBatchRemoveTagPath = NULL;
		if(PARAM_batchaddtag.Get(szBatchRemoveTagPath))
		{
			BatchProcessCommandFile(szBatchRemoveTagPath,PARAM_removetag, crClipEdit::kRemoveTag, clip);
		}

		// save clip
		if(!clip->Save(outFilenames[i]))
		{
			ULOGGER.SetProgressMessage("ERROR: failed to save clip '%s'", outFilenames[i]);
			Errorf("ERROR - failed to save clip '%s'", outFilenames[i]);
			continue;
		}

		delete clip;
	}

	// shutdown
	Shutdown();

	return 0;
}

// Used by tools, creates a unique filename so we can xge animedit and have unique
// logs.
rage::atString GetTempLogFileName( const char* pPrefix )
{
	UUID uuid; 
	char* uuidAsString; 
	::UuidCreate(&uuid); 
	::UuidToString(&uuid, reinterpret_cast<RPC_CSTR*>(&uuidAsString)); 

	char cToolsRoot[RAGE_MAX_PATH];
	GetEnvironmentVariable("RS_TOOLSROOT",cToolsRoot,RAGE_MAX_PATH );

	char cLogPath[RAGE_MAX_PATH];
	sprintf(cLogPath, "%s\\logs\\cutscene\\process\\%s_%s.ulog", cToolsRoot, pPrefix, uuidAsString);

	::RpcStringFree(reinterpret_cast<RPC_CSTR*>(&uuidAsString));

	Displayf(cLogPath);

	return atString(cLogPath);
}