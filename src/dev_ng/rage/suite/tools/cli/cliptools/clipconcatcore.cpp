// 
// cliptools/clipconcatcore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "clipconcatcore.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crclip/clipserialanimations.h"
#include "crmetadata/properties.h"
#include "crmetadata/tags.h"

namespace rage
{

namespace crClipConcat
{

	
////////////////////////////////////////////////////////////////////////////////

void ConcatClipMetadata(const atArray<const crClip*>& srcClips, crClip* destClip)
{
	// compute total duration
	const int numClips = srcClips.GetCount();
	float totalDuration = 0.f;
	for(int i=0; i<numClips; ++i)
	{
		totalDuration += srcClips[i]->GetDuration();
	}

	// merge properties and tags
	float accumulatedDuration = 0.f;
	for(int i=0; i<numClips; ++i)
	{
		if(srcClips[i])
		{
			const crClip* srcClip = srcClips[i];

			if(srcClip->HasProperties() && destClip->HasProperties())
			{
				for(int p=0; p<srcClip->GetProperties()->GetNumProperties(); ++p)
				{
					bool match = false;
					for(int q=0; q<destClip->GetProperties()->GetNumProperties(); ++q)
					{
						if(srcClip->GetProperties()->FindPropertyByIndex(p)->GetKey() == destClip->GetProperties()->FindPropertyByIndex(q)->GetKey())
						{
							match = true;
							break;
						}
					}

					if(!match)
					{
						destClip->GetProperties()->AddProperty(*srcClip->GetProperties()->FindPropertyByIndex(p));
					}
				}
			}

			if(srcClip->HasTags() && destClip->HasTags())
			{
				for(int t=0; t<srcClip->GetTags()->GetNumTags(); t++)
				{
					// TODO --- may need to detect duplicates
					const crTag* tag = srcClip->GetTags()->GetTag(t);
					crTag newTag(*tag);

					// Translate the phase as if the clips was played back to back
					float scaling = srcClip->GetDuration() / totalDuration;
					float translation = accumulatedDuration / totalDuration;

					float start = scaling*newTag.GetStart() + translation;
					float end = scaling*newTag.GetEnd() + translation;

					Assert(start >= 0.f && start <= 1.f);
					Assert(end >= 0.f && end <= 1.f);

					newTag.SetStart(start);
					newTag.SetEnd(end);

					destClip->GetTags()->AddTag(newTag);
				}
			}

			accumulatedDuration += srcClip->GetDuration();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void ConcatClipAnimation(const atArray<const crClip*>& srcClips, crClipAnimations* destClip)
{
	for(int i=0; i<srcClips.GetCount(); ++i)
	{
		const crClip* clip = srcClips[i];

		// concatenate a animation from a clip
		if(clip->GetType() == crClip::kClipTypeAnimation)
		{
			const crClipAnimation* clipAnimation = static_cast<const crClipAnimation*>(clip);
			const crAnimation* anim = clipAnimation->GetAnimation();
			if(anim)
			{
				float start, end;
				clipAnimation->GetStartEndTimes(start, end);
				float rate = clipAnimation->GetRate();
				int animIdx = destClip->GetNumAnimations();

				destClip->AddAnimation(*anim);
				destClip->SetStartEndTimes(animIdx, start, end);
				destClip->SetRate(animIdx, rate);
			}
		}

		// concatenate each animation from a serial clip
		else if(clip->GetType() == crClip::kClipTypeAnimations)
		{
			const crClipAnimations* clipAnimations = static_cast<const crClipAnimations*>(clip);
			for(u32 j=0; j<clipAnimations->GetNumAnimations(); ++j)
			{
				const crAnimation* anim = clipAnimations->GetAnimation(j);
				if(anim)
				{
					float start, end;
					clipAnimations->GetStartEndTimes(j, start, end);
					float rate = clipAnimations->GetRate(j);
					int animIdx = destClip->GetNumAnimations();

					destClip->AddAnimation(*anim);
					destClip->SetStartEndTimes(animIdx, start, end);
					destClip->SetRate(animIdx, rate);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////


} // namespace crClipConcat

} // namespace rage

