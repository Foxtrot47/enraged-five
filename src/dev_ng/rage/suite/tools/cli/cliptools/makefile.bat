set ARCHIVE=cliptools
set FILES=clipconcatcore clipcombinecore clipcoalescecore clipeditcore
set TESTERS=
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% %RAGE_PH_LIBS%
set LIBS=%LIBS% cranimation crclip crextra creature crtools crparameterizedmotion
set LIBS=%LIBS% %RAGE_AUD_LIBS% event eventtypes rmptfx curve 
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\suite\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\samples %RAGE_DIR%\base\tools\cli
