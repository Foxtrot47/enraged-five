// 
// cliptools/clipeditcore.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CLIPTOOLS_CLIPEDITCORE_H
#define CLIPTOOLS_CLIPEDITCORE_H

namespace rage
{

class crClip;
class crPropertyAttribute;
class crTag;

namespace crClipEdit
{

// PURPOSE: Concatenate clip metadata, properties and tags
//extern void ConcatClipMetadata(const atArray<const crClip*>& srcClips, crClip* destClip);

// PURPOSE: Concatenate clips animation with its start time, end time and rate
//extern void ConcatClipAnimation(const atArray<const crClip*>& srcClips, crClipSerialAnimations* destClip);

enum
{
	kConvertType,
	kAddProperty,
	kEditProperty,
	kRemoveProperty,
	kAddTag,
	kEditTag,
	kRemoveTag,
	kClipAnimation,
	kClipExpression,
	kAutoAddBlockTag,
};

extern crPropertyAttribute* CreateAttribute(const char* attrName, const char* attrType);

extern void ProcessAttributeValue(crPropertyAttribute& prop, const char* seq[]);

extern void ProcessTagValue(crTag& tag, const char* seq[]);

extern void ProcessSequence(int numSeq, const char* seq[], int paramId, crClip*& clip);


}; // namespace crClipEdit

}; // namespace rage

#endif // CLIPTOOLS_CLIPEDITCORE_H
