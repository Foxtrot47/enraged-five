// 
// /clipdump.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crmetadata/dumpoutput.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "file/zipfile.h"
#include "system/main.h"
#include "system/param.h"


using namespace rage;

PARAM(help, "Get command line help");
PARAM(zip, "Source zip we are going to load clips from");
PARAM(outputdir, "Output location of clip dumps");

typedef struct EnumFilesParams
{
	atArray<atString>	m_clipFiles;
} EnumFilesParams;

void EnumFilesCB(const fiFindData& data, void* userCB)
{
	EnumFilesParams* params = reinterpret_cast<EnumFilesParams*>(userCB);

	if ( !(data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) )
	{
		const char* extn = ASSET.FindExtensionInPath(data.m_Name);
		if (extn && !strcmp(extn, ".clip"))
		{
			params->m_clipFiles.PushAndGrow( atString( data.m_Name ) );
		}
	}
}

int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process zip
	const char* zipBuf;
	const char* outputDirBuf;

	const char* mountAs = "pack:/";

	char dir[MAX_PATH];
	ASSET.FullPath( dir, sizeof(dir), mountAs, "" );
	ASSET.RemoveNameFromPath(dir, sizeof(dir), dir );
	strcat(dir,"/");

	PARAM_zip.Get( zipBuf );

	fiZipfile zip;

	if (!zip.Init(zipBuf))
	{
		Errorf("cannot load zip '%s'",zipBuf);
		return false;
	}

	if (!zip.MountAs(dir))
	{
		Errorf("cannot mount zip '%s' at '%s'",zipBuf,dir);
		return false;
	}

	EnumFilesParams data;
	ASSET.EnumFiles( dir, EnumFilesCB, &data );

	for(int i=0; i<data.m_clipFiles.GetCount(); ++i)
	{
		char clipPath[MAX_PATH];
		sprintf( clipPath, "%s%s", dir, data.m_clipFiles[i].c_str() );
		crClip* clip = crClip::AllocateAndLoad( clipPath );
		if(!clip)
		{
			Quitf( "ERROR - Failed to load clip '%s'", data.m_clipFiles[i] );
		}

		atString clipText;
		crDumpOutputString output;
		output.SetVerbosity(3);
		clip->Dump(output);
		clipText += output.GetString();

		if( PARAM_outputdir.Get( outputDirBuf ) )
		{
			char outputFile[MAX_PATH];

			sprintf( outputFile, "%s/%s.txt", outputDirBuf, data.m_clipFiles[i] );
			ASSET.CreateLeadingPath( outputFile );
			fiStream* fs =  fiStream::Create(outputFile);
			const char* ch = clipText.c_str();
			fs->WriteByte("\n", 1);
			while(*ch)
			{
				fs->WriteByte(ch,1);
				++ch;
			}
			fs->WriteByte("\n", 1);
			fs->Close();	
		}

		delete clip;
	}

	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}
