// 
// cliptools/clipcombinecore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#include "clipcombinecore.h"

#include "crclip/clip.h"
#include "crmetadata/properties.h"
#include "crmetadata/tags.h"

namespace rage
{

namespace crClipCombine
{

////////////////////////////////////////////////////////////////////////////////

bool CombineClips(const atArray<const crClip*>& srcClips, crClip*& destClip)
{
	const int numClips = srcClips.GetCount();
	for(int i=0; i<numClips; ++i)
	{
		if(srcClips[i])
		{
			const crClip* srcClip = srcClips[i];

			if(!destClip)
			{
				destClip = srcClip->Clone();
			}
			else
			{
				if(srcClip->HasProperties() && destClip->HasProperties())
				{
					for(int p=0; p<srcClip->GetProperties()->GetNumProperties(); ++p)
					{
						bool match = false;
						for(int q=0; q<destClip->GetProperties()->GetNumProperties(); ++q)
						{
							if(srcClip->GetProperties()->FindPropertyByIndex(p)->GetKey() == destClip->GetProperties()->FindPropertyByIndex(q)->GetKey())
							{
								match = true;
								break;
							}
						}

						if(!match)
						{
							destClip->GetProperties()->AddProperty(*srcClip->GetProperties()->FindPropertyByIndex(p));
						}
					}
				}

				if(srcClip->HasTags() && destClip->HasTags())
				{
					for(int t=0; t<srcClip->GetTags()->GetNumTags(); t++)
					{
						const crTag* tag = srcClip->GetTags()->GetTag(t);

						// TODO --- may need to detect duplicates, or phase adjust for differences in clip duration
						destClip->GetTags()->AddTag(*tag);
					}
				}
			}
		}
	}

	return destClip != NULL;
}

////////////////////////////////////////////////////////////////////////////////


} // namespace crClipCombine

} // namespace rage

