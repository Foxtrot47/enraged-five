// 
// /clipdump.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crmetadata/dumpoutput.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"


using namespace rage;

PARAM(help, "Get command line help");
PARAM(clip, "Source clip(s) (Example: clip0.clip,clip1.clip ...)");
PARAM(serialise, "Dump to a text file");
//PARAM(csv, "Produce output in comma separated format");


int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process clip list:
	const int maxClips = 64;
	const int maxClipBufSize = 8192;

	int numClips = 0;
	const char* clipFilenames[maxClips];
	char clipBuf[maxClipBufSize];

	numClips = PARAM_clip.GetArray(clipFilenames, maxClips, clipBuf, maxClipBufSize);
	if(numClips <= 0)
	{
		Quitf("ERROR - No source clip(s) specified\n");
	}

	for(int i=0; i<numClips; ++i)
	{
		crClip* clip = crClip::AllocateAndLoad(clipFilenames[i]);
		if(!clip)
		{
			Quitf("ERROR - Failed to load clip '%s'", clipFilenames[i]);
		}

		atString clipText;
		crDumpOutputString output;
		output.SetVerbosity(3);
		clip->Dump(output);
		clipText += output.GetString();
		/*FILE* out_fp = NULL;
		if(PARAM_serialise.Get())
		{
			char outputFile[MAX_PATH];
			strcpy(outputFile, clipFilenames[i]);
			strcat(outputFile, ".txt");
			out_fp = freopen(outputFile,"w",stdout);
		}*/
		if(PARAM_serialise.Get())
		{
			char outputFile[MAX_PATH];
			strcpy(outputFile, clipFilenames[i]);
			strcat(outputFile, ".txt");
			fiStream* fs =  fiStream::Create(outputFile);
			const char* ch = clipText.c_str();
			fs->WriteByte("\n", 1);
			while(*ch)
			{
				fs->WriteByte(ch,1);
				++ch;
			}
			fs->WriteByte("\n", 1);
			fs->Close();	
		}
		else
		{
			Printf("\n");
			const char* ch = clipText;
			while(*ch)
			{
				Printf("%c", *ch);
				++ch;
			}
			Printf("\n");
		}
		/*if(PARAM_serialise.Get())
		{
			fclose (out_fp);
		}*/

		delete clip;
	}

	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}
