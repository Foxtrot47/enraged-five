// 
// /cliprename.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "cranimation/animation.h"
#include "cranimation/animdictionary.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "system/main.h"
#include "system/param.h"
#include <iostream>

using namespace rage;

PARAM(help, "Get command line help");
REQ_PARAM(dir, "Source dir of clips");

void Init()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();
}

void Shutdown()
{
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();
}

void FindClipFilesCallback(const fiFindData &data, void *userArg)
{
	atArray<atString>& atFiles = *((atArray<atString>*)userArg);

	const char *pExtension = ASSET.FindExtensionInPath( data.m_Name );
	if ( pExtension == NULL )
	{
		return;
	}

	if ( stricmp( pExtension, ".clip" ) == 0 )
	{
		atFiles.PushAndGrow( atString(data.m_Name) );
	}
}

int Main()
{
	Init();

	const char* pClipDir=NULL;
	if(PARAM_dir.Get(pClipDir))
	{
		atArray<atString> clipFiles;
		int iHandle = ASSET.EnumFiles( pClipDir, &FindClipFilesCallback, &clipFiles );
		if ( iHandle )
		{
			for(int i=0; i < clipFiles.GetCount(); ++i)
			{
				char cClipFile[RAGE_MAX_PATH];
				sprintf(cClipFile, "%s\\%s", pClipDir, clipFiles[i].c_str());

				crAnimLoaderNull nullLoader;
				crClip* pClip = crClip::AllocateAndLoad(cClipFile, &nullLoader);

				if(pClip)
				{
					crClipAnimation* pClipAnim = static_cast<crClipAnimation*>(pClip);

					if(!pClipAnim->GetAnimation())
					{
						char cAnim[RAGE_MAX_PATH];
						ASSET.RemoveExtensionFromPath(cAnim, RAGE_MAX_PATH, cClipFile);

						crAnimation* pAnimCorrect = crAnimation::AllocateAndLoad(cAnim);

						if(pAnimCorrect)
						{
							pClipAnim->SetAnimation(*pAnimCorrect);

							if(!pClipAnim->Save(cClipFile))
							{
								std::cout << cClipFile << " failed." << std::endl;
							}
							else
							{
								std::cout << cClipFile << " successfully updated." << std::endl;
							}
						}
					}
				}
			}
		}		
	}

	Shutdown();
	return 0;
}