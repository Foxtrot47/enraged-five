@echo off

setlocal
pushd "%~dp0"

CALL setenv.bat

set BUILD_FOLDER=%RS_BUILDBRANCH%
set RAGE_DIR=X:/GTA5/src/dev_ng/rage
set SCE_PS3_ROOT=X:/usr/local/300_001/cell

ECHO LOAD_SLN ENVIRONMENT
ECHO BUILD_FOLDER: 	%BUILD_FOLDER%
ECHO RAGE_DIR: 		%RAGE_DIR%
ECHO SCE_PS3_ROOT: 	%SCE_PS3_ROOT%
ECHO END LOAD_SLN ENVIRONMENT

SET VISUAL_STUDIO="C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe"
SET VISUAL_STUDIO_X86="C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe"

SET SOLUTION="%CD%\clip_tool_suite_2008.sln"

IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 
	ECHO x64 detected, starting Visual Studio 2008
	start "" %VISUAL_STUDIO_X86% %SOLUTION%
) ELSE (
	ECHO x86 detected, starting Visual Studio 2008
	start "" %VISUAL_STUDIO% %SOLUTION%
)


popd
