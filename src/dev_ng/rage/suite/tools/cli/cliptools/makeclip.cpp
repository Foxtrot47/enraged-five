// 
// /makeclip.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crclip/clipanimationexpression.h"
#include "crclip/clipserialanimations.h"
#include "crextra/expressions.h"
#include "file/asset.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(anim, "Animation file to wrap inside a clip (Example: in1.anim,in2.anim,in3.anim)");
PARAM(parallel, "If multiple animations supplied, are they parallel or serial");
PARAM(out, "Output clip filename (Example: out.clip)");
PARAM(expr, "Expression added to the clip (Example: in.expr)");


int Main()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process animation:
	const int bufSize = 8192;
	char buf[bufSize];
	const int maxAnimNames = 128;
	const char* animNames[maxAnimNames];

	int numAnimNames = PARAM_anim.GetArray(animNames, maxAnimNames, buf, bufSize);
	if(numAnimNames == 0)
	{
		Quitf("Insufficient animation names provided");
	}

	// load animations:
	atArray< const crAnimation* > anims;
	for(int i=0; i<numAnimNames; i++)
	{
		crAnimation* anim = crAnimation::AllocateAndLoad(animNames[i]);
		if(anim)
		{
			anims.Grow() = anim;
		}
		else
		{
			Quitf("ERROR - Failed to load animation '%s'", animNames[i]);
		}
	}

	// load expression if requested:
	crExpressions* exprs = NULL;
	const char* exprName = NULL;
	if(PARAM_expr.Get(exprName))
	{
		if(anims.GetCount() == 1)
		{
			exprs = crExpressions::AllocateAndLoad(exprName, false);
			if(!exprs)
			{
				Quitf("ERROR - Failed to load expression '%s'", exprName);
			}
		}
		else
		{
			Quitf("ERROR - Expression support only one animation");
		}
	}

	// create clip:
	crClip* clip;
	if(anims.GetCount() == 1)
	{
		if(exprs)
		{
			clip = crClipAnimationExpression::AllocateAndCreate(*anims[0], *exprs);
		}
		else
		{
			clip = crClipAnimation::AllocateAndCreate(*anims[0]);
		}
	}
	else
	{
		crClipAnimations* clipAnims = crClipAnimations::AllocateAndCreate();

		bool parallel = PARAM_parallel.Get();
		clipAnims->SetParallel(parallel);

		for(int i=0; i<numAnimNames; i++)
		{
			clipAnims->AddAnimation(*anims[i]);
		}
		clip = clipAnims;
	}

	// process output:
	const char* OutFilename = NULL;
	if(!PARAM_out.Get(OutFilename))
	{
		Quitf("ERROR - Failed to specify an output filename\n");
	}

	// save clip:
	if(!clip->Save(OutFilename))
	{
		Quitf("ERROR - Failed to save clip file '%s'\n", OutFilename);
	}

	for(int i=0; i<anims.GetCount(); ++i)
	{
		delete anims[i];
	}
	delete clip;

	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}

