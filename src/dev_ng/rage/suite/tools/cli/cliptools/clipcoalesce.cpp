// 
// cliptools/clipcoalesce.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "clipcoalescecore.h"

#include "atl/array.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "file/asset.h"
#include "file/token.h"
#include "system/main.h"
#include "system/param.h"

#if !__TOOL
#error "ClipCoalesce needs to use the default memory allocator. Compile a tool build instead."
#endif

using namespace rage;

PARAM(help, "Get command line help");
PARAM(clips, "Source clips (Example: clip0.clip,clip1.clip,clip3.clip)");
PARAM(cliplist, "Source clip list (Example: clips.cliplist)");
PARAM(framelimit, "Maximum number of internal frames in concatenated anims (Example: 65535)");
PARAM(outdir, "Output directory for new concatenated animations (Example: C:\\out)");
PARAM(outprefix, "Output prefix for new concatenated animation files (Example: concat_)");
PARAM(score, "Use scoring to improve packing, selects adjacent anims that will compress together better");
PARAM(memory, "Use memory stats to improve packing, selects adjacent anims that will compress together better");
PARAM(verbose, "Verbose output (Example: concat_)");

int Main()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	atArray<atString> clipFilenames;

	u32 frameLimit = 65535;
	PARAM_framelimit.Get(frameLimit);

	const char* outDir = "$";
	PARAM_outdir.Get(outDir);

	const char* outPrefix = NULL;
	PARAM_outprefix.Get(outPrefix);

	bool score = PARAM_score.Get();
	bool memory = PARAM_memory.Get();

	bool verbose = PARAM_verbose.Get();

	// get list of clips
	if(PARAM_clips.Get())
	{
		const int bufSize = 8192;
		char buf[bufSize];
		const int maxClipNames = 128;
		const char* clipNames[maxClipNames];

		int numClipNames = PARAM_clips.GetArray(clipNames, maxClipNames, buf, bufSize);
		
		clipFilenames.Resize(numClipNames);
		for(int i=0; i<numClipNames; ++i)
		{
			clipFilenames[i] = clipNames[i];
		}
	}
	else if(PARAM_cliplist.Get())
	{
		const char* clipListFilename = NULL;
		PARAM_cliplist.Get(clipListFilename);
		
		fiSafeStream f(ASSET.Open(clipListFilename, "cliplist"));
		if(f)
		{
			fiTokenizer T(clipListFilename, f);	

			const int maxBufSize = RAGE_MAX_PATH;
			char buf[maxBufSize];
			while(T.GetLine(buf, maxBufSize) > 0)
			{
				clipFilenames.Grow() = buf;
			}
		}
		else
		{
			Quitf("ERROR - failed to open clip list file '%s'", clipListFilename);
		}
	}
	else
	{
		Quitf("ERROR - no clips supplied, either provide -clips or -cliplist on the command line");
	}

	const int numClips = clipFilenames.GetCount();
	atArray<const char*> clipFilenamePtrs(numClips, numClips);
	for(int i=0; i<numClips; ++i)
	{
		clipFilenamePtrs[i] = clipFilenames[i].c_str();
	}

	// coalesce clips
	if(!crClipCoalesce::CoalesceClips(clipFilenamePtrs, frameLimit, outDir, outPrefix, score, memory, verbose, true))
	{
		Quitf("ERROR - failed to coalesce clips");
	}

	// cleanup:
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}
