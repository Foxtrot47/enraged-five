// 
// /clipcombine.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#include "clipcombinecore.h"

#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crclip/clipdictionary.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(clips, "Source clips (Example: clip0.clip,clip1.clip)");
PARAM(out, "Output clip (Example: out.clip)");
PARAM(onlyloadfirstanim, "Suppress loading of animations from all except first clip listed");


int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process clip list:
	const int maxClips = 64;
	const int maxClipBufSize = 8192;

	int numClips = 0;
	const char* clipFilenames[maxClips];
	char clipBuf[maxClipBufSize];

	numClips = PARAM_clips.GetArray(clipFilenames, maxClips, clipBuf, maxClipBufSize);
	if(numClips <= 0)
	{
		Quitf("ERROR - No source clip(s) specified\n");
	}
	else if(numClips != 2)
	{
		Quitf("ERROR - Currently exactly 2 clips must be specified\n");
	}

	// process output filename:
	const char* outputFilename = NULL;
	PARAM_out.Get(outputFilename);
	if(!outputFilename)
	{
		Quitf("ERROR - No output filename specified\n");
	}

	bool onlyLoadFirstAnim = PARAM_onlyloadfirstanim.Get();

	// load clips:
	atArray<const crClip*> srcClips;
	for(int i=0; i<numClips; ++i)
	{
		crAnimLoaderNull nullLoader;
		crClip* clip = crClip::AllocateAndLoad(clipFilenames[i], (onlyLoadFirstAnim && (i>0))?&nullLoader:NULL);
		if(!clip)
		{
			Quitf("ERROR - Failed to load clip '%s'", clipFilenames[i]);
		}

		srcClips.Grow() = clip;
	}


	// initialize dest clip:
	crClip* destClip = NULL;

	// combine clips:
	if(crClipCombine::CombineClips(srcClips, destClip))
	{
		// save dest clip:
		if(!destClip->Save(outputFilename))
		{
			Quitf("ERROR - Failed to save clip '%s'", outputFilename);
		}	
	}
	else
	{
		Quitf("ERROR - Failed to combine clips");
	}

	if(destClip)
	{
		delete destClip;
		destClip = NULL;
	}

	for(int i = 0; i < srcClips.GetCount(); i++)
	{
		delete srcClips[i];
	}

	// shutdown:
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}

