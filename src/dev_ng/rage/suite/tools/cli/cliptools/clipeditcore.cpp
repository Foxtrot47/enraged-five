// 
// cliptools/clipeditcore.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "clipeditcore.h"

#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crclip/clipanimationexpression.h"
#include "crclip/clipserialanimations.h"
#include "crextra/expressions.h"
#include "crmetadata/tags.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "file/asset.h"
#include "system/param.h"

#include "RsULog/ULogger.h"


namespace rage
{

namespace crClipEdit
{

crPropertyAttribute* CreateAttribute(const char* attrName, const char* attrType)
{
	for(int i=crPropertyAttribute::kTypeNone+1; i<crPropertyAttribute::kTypeNum; ++i)
	{
		if(!stricmp(crPropertyAttribute::FindTypeInfo(crPropertyAttribute::eType(i))->GetName(), attrType))
		{
			crPropertyAttribute* attr = crPropertyAttribute::Allocate(crPropertyAttribute::eType(i));
			attr->SetName(attrName);
			return attr;
		}
	}

	return NULL;
}

void ProcessAttributeValue(crPropertyAttribute& attr, const char* seq[], int numSeq)
{
	switch(attr.GetType())
	{
	case crPropertyAttribute::kTypeFloat:
		if(*seq[0])
		{
			static_cast<crPropertyAttributeFloat&>(attr).GetFloat() = float(atof(seq[0]));
		}
		break;

	case crPropertyAttribute::kTypeMatrix34:
		for(int i=0; i<12; ++i)
		{
			if(*seq[i])
			{
				static_cast<crPropertyAttributeMatrix34&>(attr).GetMatrix34().SetElemf(i/3,i%3, float(atof(seq[i])));
			}
		}
		break;

	case crPropertyAttribute::kTypeQuaternion:
		for(int i=0; i<4; ++i)
		{
			if(*seq[i])
			{
				static_cast<crPropertyAttributeQuaternion&>(attr).GetQuaternion()[i] = float(atof(seq[i]));
			}
		}
		break;

	case crPropertyAttribute::kTypeVector3:
		for(int i=0; i<3; ++i)
		{
			if(*seq[i])
			{
				static_cast<crPropertyAttributeVector3&>(attr).GetVector3()[i] = float(atof(seq[i]));
			}
		}
		break;

	case crPropertyAttribute::kTypeInt:
		static_cast<crPropertyAttributeInt&>(attr).GetInt() = atoi(seq[0]);
		break;

	case crPropertyAttribute::kTypeString:
		static_cast<crPropertyAttributeString&>(attr).GetString() = seq[0];

		// temporary solution to get around parsing of ',' characters earlier in process
		for(int i=1; i<numSeq; ++i)
		{
			static_cast<crPropertyAttributeString&>(attr).GetString() += ',';
			static_cast<crPropertyAttributeString&>(attr).GetString() += seq[i];
		}
		break;

	case crPropertyAttribute::kTypeHashString:
		static_cast<crPropertyAttributeHashString&>(attr).GetHashString().SetFromString(seq[0]);
		break;

	case crPropertyAttribute::kTypeData:
		Assert(0);  // TODO --- provide interface for data properties
		break;

	default:
		// TODO --- unknown type
		break;
	}
}

void ProcessSequence(int numSeq, const char* seq[], int paramId, crClip*& clipPtr)
{
	crClip& clip = *clipPtr;

	switch(paramId)
	{
	case kConvertType:
		if(*seq[0])
		{
			if(!stricmp(seq[0], "clipanimation"))
			{
				switch(clip.GetType())
				{
				case crClip::kClipTypeAnimation:
					break;

				case crClip::kClipTypeAnimationExpression:
					{
						crClipAnimationExpression& clipAnimExpr = static_cast<crClipAnimationExpression&>(clip);
						crClipAnimation* newClipAnim = rage_new crClipAnimation;
						if(clipAnimExpr.GetAnimation())
						{
							newClipAnim->Create(*clipAnimExpr.GetAnimation());
							newClipAnim->SetStartEndTimes(clipAnimExpr.GetStartTime(), clipAnimExpr.GetEndTime());
							newClipAnim->SetRate(clipAnimExpr.GetRate());
						}
						newClipAnim->GetProperties()->CopyProperties(*clipAnimExpr.GetProperties());
						newClipAnim->GetTags()->CopyTags(*clipAnimExpr.GetTags());

						delete clipPtr;
						clipPtr = newClipAnim;
					}
					break;

				default:
					ULOGGER.SetProgressMessage("ERROR: unknown/unsupported clip type conversion '%s' -> '%s'", clip.GetTypeName(), seq[0]);
					Quitf("ERROR - unknown/unsupported clip type conversion '%s' -> '%s'", clip.GetTypeName(), seq[0]);
					break;
				}
			}
			else if(!stricmp(seq[0], "clipanimationexpression"))
			{
				switch(clip.GetType())
				{
				case crClip::kClipTypeAnimationExpression:
					break;

				case crClip::kClipTypeAnimation:
					{
						crClipAnimation& clipAnim = static_cast<crClipAnimation&>(clip);
						crClipAnimationExpression* newClipAnimExpr = rage_new crClipAnimationExpression;
						if(clipAnim.GetAnimation())
						{
							newClipAnimExpr->crClipAnimation::Create(*clipAnim.GetAnimation());
							newClipAnimExpr->SetStartEndTimes(clipAnim.GetStartTime(), clipAnim.GetEndTime());
							newClipAnimExpr->SetRate(clipAnim.GetRate());
						}
						newClipAnimExpr->GetProperties()->CopyProperties(*clipAnim.GetProperties());
						newClipAnimExpr->GetTags()->CopyTags(*clipAnim.GetTags());

						delete clipPtr;
						clipPtr = newClipAnimExpr;
					}
					break;

				default:
					ULOGGER.SetProgressMessage("ERROR: unknown/unsupported clip type conversion '%s' -> '%s'", clip.GetTypeName(), seq[0]);
					Quitf("ERROR - unknown/unsupported clip type conversion '%s' -> '%s'", clip.GetTypeName(), seq[0]);
					break;
				}

			}
			else
			{
				ULOGGER.SetProgressMessage("ERROR: unknown/unsupported clip type conversion '%s'", seq[0]);
				Quitf("ERROR - unknown/unsupported clip type conversion '%s'", seq[0]);
			}
		}
		break;

	case kAddProperty:
		if(clip.HasProperties() && numSeq>=4)
		{
			crProperty prop;
			prop.SetName(seq[0]);

			crPropertyAttribute* attr = CreateAttribute(seq[1], seq[2]);
			if(attr)
			{
				ProcessAttributeValue(*attr, &seq[3], numSeq-3);
				prop.AddAttribute(*attr);
				clip.GetProperties()->AddProperty(prop);
				delete attr;
			}
		}
		break;

	case kEditProperty:
		if(clip.HasProperties() && numSeq>=2)
		{
			crProperty* prop = clip.GetProperties()->FindProperty(seq[0]);
			if(prop)
			{
				crPropertyAttribute* attr = prop->GetAttribute(seq[1]);
				if(attr)
				{
					ProcessAttributeValue(*attr, &seq[2], numSeq-2);
				}
			}
		}
		break;


	case kRemoveProperty:
		if(clip.HasProperties())
		{
			if(numSeq==0)
			{
				clip.GetProperties()->RemoveAllProperties();
			}
			else if(seq[0])
			{
				clip.GetProperties()->RemoveProperty(seq[0]);
			}
		}
		break;

	case kAddTag:
		if(clip.HasTags() && numSeq>=6)
		{
			crTag tag;
			tag.GetProperty().SetName(seq[0]);
			tag.SetStart(float(atof(seq[1])));
			tag.SetEnd(float(atof(seq[2])));

			crPropertyAttribute* attr = CreateAttribute(seq[3], seq[4]);
			if(attr)
			{
				ProcessAttributeValue(*attr, &seq[5], numSeq-5);
				tag.GetProperty().AddAttribute(*attr);
				clip.GetTags()->AddTag(tag);
				delete attr;
			}
		}
		break;

	case kEditTag:
		if(clip.HasTags() && numSeq>=5)
		{
			crTag::Key key = crTag::CalcKey(seq[0]);
			for(int i=0; i<clip.GetTags()->GetNumTags(); ++i)
			{
				crTag* tag = clip.GetTags()->GetTag(i);
				if(tag->GetKey() == key)
				{
					tag->SetStart(float(atof(seq[1])));
					tag->SetEnd(float(atof(seq[2])));

					crPropertyAttribute* attr = tag->GetProperty().GetAttribute(seq[3]);
					if(attr)
					{
						ProcessAttributeValue(*attr, &seq[4], numSeq-4);
					}

					clip.GetTags()->SortTags();
					break;
				}
			}
		}
		break;

	case kRemoveTag:
		if(clip.HasTags())
		{
			if(numSeq==0)
			{
				clip.GetTags()->RemoveAllTags();
			}
			else
			{
				clip.GetTags()->RemoveAllTags(seq[0]);
			}
		}
		break;


	case kClipAnimation:
		{
			if(clip.GetType() == crClip::kClipTypeAnimation || clip.GetType() == crClip::kClipTypeAnimationExpression)
			{
				crClipAnimation& clipAnimation = static_cast<crClipAnimation&>(clip);

				if(*seq[0] || *seq[1])	
				{
					float originalStart, originalEnd, originalDuration;
					clipAnimation.GetStartEndTimes(originalStart, originalEnd);
					originalDuration = clipAnimation.GetDuration();
                    
					float start = originalStart;
					if(*seq[0])
					{
						start = float(atof(seq[0]));
					}

					float end = originalEnd;
					if(*seq[1])
					{
						end = float(atof(seq[1]));
					}

					clipAnimation.SetStartEndTimes(start, end);

					if(clipAnimation.GetTags())
					{
						clipAnimation.GetStartEndTimes(start, end);

						crTags newTags;
						for(int i=0; i < clipAnimation.GetTags()->GetNumTags(); ++i)
						{
							crTag *tag = clipAnimation.GetTags()->GetTag(i);
							float tagStart = (tag->GetStart() * originalDuration + originalStart - start) / clipAnimation.GetDuration();
							float tagEnd = (tag->GetEnd() * originalDuration + originalStart - start) / clipAnimation.GetDuration();

							// test for intersection with 0->1
							if(InRange(tagStart, 0.f, 1.f) || InRange(tagEnd, 0.f, 1.f) || (tagStart < tagEnd && tagStart < 0.f && tagEnd > 1.f))
							{
								crTag newTag(*tag);
								newTag.SetStartEnd(Clamp(tagStart, 0.f, 1.f), Clamp(tagEnd, 0.f, 1.f));

								newTags.AddTag(newTag);
							}
						}

						clipAnimation.GetTags()->CopyTags(newTags);
					}
				}

				if(seq[2] && *seq[2])
				{
					float rate = float(atof(seq[2]));
					clipAnimation.SetRate(rate);
				}

				if(seq[3] && *seq[3])
				{
					crAnimation* anim = crAnimation::AllocateAndLoad(seq[3]);
					if(anim)
					{
						float originalStart, originalEnd, originalDuration;
						clipAnimation.GetStartEndTimes(originalStart, originalEnd);
						originalDuration = clipAnimation.GetDuration();

						clipAnimation.SetAnimation(*anim);

						// release here, because crAnimation will start us at 1, then SetAnimation gets us to 2, when we really just want 1
						anim->Release();

						if(clipAnimation.HasTags() && !IsClose(clipAnimation.GetDuration(), originalDuration))
						{
							float start, end;
							clipAnimation.GetStartEndTimes(start, end);

							crTags newTags;
							for(int i=0; i < clipAnimation.GetTags()->GetNumTags(); ++i)
							{
								crTag *tag = clipAnimation.GetTags()->GetTag(i);
								float tagStart = tag->GetStart() * originalDuration / clipAnimation.GetDuration();
								float tagEnd = tag->GetEnd() * originalDuration / clipAnimation.GetDuration();

								// test for intersection with 0->1
								if(InRange(tagStart, 0.f, 1.f) || InRange(tagEnd, 0.f, 1.f) || (tagStart < tagEnd && tagStart < 0.f && tagEnd > 1.f))
								{
									crTag newTag(*tag);
									newTag.SetStartEnd(Clamp(tagStart, 0.f, 1.f), Clamp(tagEnd, 0.f, 1.f));

									newTags.AddTag(newTag);
								}
							}

							clipAnimation.GetTags()->CopyTags(newTags);							
						}
					}
				}
			}
			else if(clip.GetType() == crClip::kClipTypeAnimations)
			{
				crClipAnimations& clipAnimations = static_cast<crClipAnimations&>(clip);
				int animIdx = atoi(seq[0]);
				if(animIdx < 0 || animIdx >= int(clipAnimations.GetNumAnimations()))
				{
					ULOGGER.SetProgressMessage("ERROR: invalid animindex %d to edit clip of animations '%s'", animIdx, clip.GetTypeName());
					Quitf("ERROR - invalid animindex %d to edit clip of animations '%s'", animIdx, clip.GetTypeName());
				}

				// TODO --- need to adjust the tags phase
				if(*seq[1] || *seq[2])	
				{
					float originalStart, originalEnd;
					clipAnimations.GetStartEndTimes(animIdx, originalStart, originalEnd);

					float start = originalStart;
					if(*seq[1])
					{
						start = float(atof(seq[1]));
					}

					float end = originalEnd;
					if(*seq[2])
					{
						end = float(atof(seq[2]));
					}

					clipAnimations.SetStartEndTimes(animIdx, start, end);
				}

				if(*seq[3])
				{
					float rate = float(atof(seq[2]));
					clipAnimations.SetRate(animIdx, rate);
				}

				if(*seq[4])
				{
					crAnimation* anim = crAnimation::AllocateAndLoad(seq[4]);
					if(anim)
					{
						clipAnimations.SetAnimation(animIdx, *anim);

						// release here, because crAnimation will start us at 1, then SetAnimation gets us to 2, when we really just want 1
						anim->Release();
					}
				}
			}
			else
			{
				ULOGGER.SetProgressMessage("ERROR: trying to modify clip animation properties on clip of type '%s'", clip.GetTypeName());
				Quitf("ERROR - trying to modify clip animation properties on clip of type '%s'", clip.GetTypeName());
			}
		}
		break;

	case kClipExpression:
		{
			if(clip.GetType() == crClip::kClipTypeAnimationExpression)
			{
				crClipAnimationExpression& clipExpression = static_cast<crClipAnimationExpression&>(clip);

				if(*seq[0])	
				{
					crExpressions* expr = crExpressions::AllocateAndLoad(seq[0]);
					if(expr)
					{
						clipExpression.SetExpressions(*expr);

						expr->Release();
					}
				}
			}
			else
			{
				ULOGGER.SetProgressMessage("ERROR: trying to modify clip expression properties on clip of type '%s'", clip.GetTypeName());
				Quitf("ERROR - trying to modify clip expression properties on clip of type '%s'", clip.GetTypeName());
			}
		}
		break;

	case kAutoAddBlockTag:
		{
			float threshold = 1.f;
			if(*seq[0])
			{
				threshold = float(atof(seq[0]));
			}

			// strip existing block tags
			crTags blockTags;
			if(clip.GetTags())
			{
				for(int i=0; i<clip.GetTags()->GetNumTags();)
				{		
					crTag* tag = clip.GetTags()->GetTag(i);
					if(tag && tag->GetKey() == crTag::CalcKey("Block", 0xE433D77D))
					{
						blockTags.AddTag(*tag);
						clip.GetTags()->RemoveTag(*tag);
						continue;
					}
					++i;
				}
			}

			crFrameDataSingleDof frameData(kTrackCameraTranslation, 0, kFormatTypeVector3);
			crFrameSingleDof frame(frameData);

			clip.Composite(frame, 0.f);

			int numFrames = int(clip.GetNum30Frames());
			for(int n=1; n<numFrames; ++n)
			{
				Vec3V last;
				frame.GetValue(last);

				clip.Composite(frame, clip.Convert30FrameToTime(float(n)));

				Vec3V curr;
				frame.GetValue(curr);

				if(IsTrue(DistSquared(last, curr) >= ScalarV(threshold*threshold)))
				{
					crTag newTag;
					newTag.SetName("Block");
					newTag.SetStartEnd(Max(clip.Convert30FrameToPhase(float(n-1)-0.001f), 0.f), Min(clip.Convert30FrameToPhase(float(n)+0.001f), 1.f));

					Displayf("Found discontinuity at: phase %f -> %f, time %f -> %f, frame 30Hz %f -> %f", newTag.GetStart(), newTag.GetEnd(), clip.ConvertPhaseToTime(newTag.GetStart()), clip.ConvertPhaseToTime(newTag.GetEnd()), clip.ConvertPhaseTo30Frame(newTag.GetStart()), clip.ConvertPhaseTo30Frame(newTag.GetEnd()));

					bool alreadyExists = false;
					for(int i=0; i<blockTags.GetNumTags(); ++i)
					{
						crTag* tag = blockTags.GetTag(i);
						if(tag)
						{
							const float epsilon = clip.Convert30FrameToPhase(0.01f);
							if(IsClose(tag->GetStart(), newTag.GetStart(), epsilon) && IsClose(tag->GetEnd(), newTag.GetEnd(), epsilon))
							{
								Displayf("Block tag already exists there: phase %f -> %f, time %f -> %f, frame 30Hz %f -> %f", tag->GetStart(), tag->GetEnd(), clip.ConvertPhaseToTime(tag->GetStart()), clip.ConvertPhaseToTime(tag->GetEnd()), clip.ConvertPhaseTo30Frame(tag->GetStart()), clip.ConvertPhaseTo30Frame(tag->GetEnd()));
								alreadyExists = true;
								break;
							}
						}
					}

					if(!alreadyExists)
					{
						Displayf("Auto adding new block tag: phase %f -> %f, time %f -> %f, frame 30Hz %f -> %f", newTag.GetStart(), newTag.GetEnd(), clip.ConvertPhaseToTime(newTag.GetStart()), clip.ConvertPhaseToTime(newTag.GetEnd()), clip.ConvertPhaseTo30Frame(newTag.GetStart()), clip.ConvertPhaseTo30Frame(newTag.GetEnd()));
						blockTags.AddTag(newTag);
					}
				}
			}

			// add block tags back
			for(int i=0; i<blockTags.GetNumTags(); ++i)
			{
				crTag* tag = blockTags.GetTag(i);
				if(tag)
				{
					clip.GetTags()->AddTag(*tag);
				}
			}
		}
		break;
	}
}

} // namespace crClipEdit

} // namespace rage
