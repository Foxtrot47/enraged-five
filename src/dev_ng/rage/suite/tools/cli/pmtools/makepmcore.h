// 
// pmtools/makepmcore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PMTOOLS_MAKEPMCORE_H
#define PMTOOLS_MAKEPMCORE_H

#include "crparameterizedmotion/blenddatabasecalculator.h"


namespace rage
{

class crAnimation;
class crClips;
class crProperties;
class crSkeletonData;
class crTags;
class crWeightSet;
class crpmBlendDatabase;
class crpmParameterizedMotionSimpleData;
class crpmParameterizedMotionVariableData;
class crpmParameterSampler;
class crpmRegistrationCurveData;

namespace crpmMakePm
{

// PURPOSE: Parse the variation string
// RETURNS: true - successfully parsed into variation structure, false - failed to parse (outVariations undefined)
extern bool ParseVariations(const char* variationsString, const crClips& clips, crpmBlendDatabaseCalculator::Variations& variations);

// PURPOSE: Make the registration curve data
// RETURNS: true - successfully generated rc data, false - failed to generate (outRcData state undefined)
extern bool MakeRc(crpmRegistrationCurveData& outRcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* weightSet, const crWeightSet* filterWeightSet, bool filterMover, float sampleRate, int slopeLimit, int windowSize);

// PURPOSE: Make the blend database
// RETURNS: true - successfully generated bd, false - failed to generate (outBd state undefined)
extern bool MakeBd(crpmBlendDatabase& outBd, crpmParameterSampler& sampler, float sampleDistanceThreshold, float sourceDistanceThreshold, bool stochastic, const crpmBlendDatabaseCalculator::Variations* variations, crClips* clips, crpmRegistrationCurveData* rcData);

// PURPOSE: Make parameterized motion
// RETURNS: true - successfully generated pm, false - failed to generate (destPm state undefined)
extern bool MakePm(crpmParameterizedMotionSimpleData& outPmData, const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags, const crProperties* properties);

// PURPOSE: Make parameterized motion variations
// RETURNS: true - successfully generated pm variations, false - failed to generate (destPm state undefined)
extern bool MakePmVariations(crpmParameterizedMotionVariableData& outPmData, const crpmBlendDatabaseCalculator::Variations& variations);

}; // namespace crpmMakePm

}; // namespace rage

#endif // PMTOOLS_MAKEPMCORE_H
