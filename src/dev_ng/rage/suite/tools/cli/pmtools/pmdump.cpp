// 
// pmtools/pmdump.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "pmdumpcore.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"


using namespace rage;

PARAM(help, "Get command line help");
PARAM(pm, "Source pm(s) (Example: pm0.spm,pm1.cpm ...)");
PARAM(verbose, "Verbosity level [0..3]");


int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();
	crpmParameterizedMotion::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// procsess verbosity:
	int verbose = 1;
	PARAM_verbose.Get(verbose);

	// process pm list:
	const int maxPms = 64;
	const int maxPmBufSize = 8192;

	int numPms = 0;
	const char* pmFilenames[maxPms];
	char pmBuf[maxPmBufSize];

	numPms = PARAM_pm.GetArray(pmFilenames, maxPms, pmBuf, maxPmBufSize);
	if(numPms <= 0)
	{
		Quitf("ERROR - No source pm(s) specified\n");
	}

	for(int i=0; i<numPms; ++i)
	{
		crpmParameterizedMotionData* pm = crpmParameterizedMotionData::AllocateAndLoad(pmFilenames[i]);
		if(!pm)
		{
			Quitf("ERROR - Failed to load clip '%s'", pmFilenames[i]);
		}

		crpmPmDump::DumpParameterizedMotion(*pm, verbose);

		delete pm;
	}

	crpmParameterizedMotion::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}