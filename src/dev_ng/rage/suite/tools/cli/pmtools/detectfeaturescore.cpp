// 
// pmtools/detectfeaturescore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "detectfeaturescore.h"

#include "crclip/clip.h"
#include "crparameterizedmotion/motioncontroller.h"
#include "crskeleton/bonedata.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tags.h"
#include "crskeleton/skeletondata.h"

namespace rage
{

namespace crpmDetectFeatures
{

////////////////////////////////////////////////////////////////////////////////

bool DetectFeatures(crClip& clip, const char* anchors, u16 moverId, const crSkeletonData& skelData, float sampleRate)
{
	bool success = true;

	success = DetectDisplacement(clip, moverId, skelData, sampleRate) && success;
	success = DetectAnchors(clip, anchors, skelData, sampleRate) && success;

	return success;
}

////////////////////////////////////////////////////////////////////////////////

bool DetectAnchors(crClip& clip, const char* anchors, const crSkeletonData& skelData, float sampleRate)
{
	if(anchors)
	{
		bool success = true;
		while(*anchors)
		{
			char buf[256];
			char* ch = buf;

			while(*anchors)
			{
				if(*anchors == ',')
				{
					anchors++;
					break;
				}
				*ch = *anchors;
				ch++;
				anchors++;
			}
			*ch = '\0';

			const crBoneData* bd = skelData.FindBoneData(buf);
			success = (bd != NULL) && DetectAnchor(clip, bd->GetBoneId(), skelData, sampleRate) && success;
		}

		return success;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool DetectAnchor(crClip& clip, u16 anchorId, const crSkeletonData& skelData, float sampleRate)
{
	const float anchorHeightThreshold = FLT_MAX;
	const float anchorSpeedThreshold = 1.f;
	const float anchorDistanceThreshold = 0.01f;

	// TODO --- needs to support tags that loop!

	crpmMotionControllerClip mc(clip, skelData);

	int anchorIdx;
	if(!skelData.ConvertBoneIdToIndex(anchorId, anchorIdx))
	{
		return false;
	}

	const int numFrames = int((clip.GetDuration() / sampleRate) + 0.5f) + 1;

	atArray<Vec3V> anchorPos(numFrames, numFrames);
	for(int i=0; i<numFrames; ++i)
	{
		Mat34V anchorMtx;
		mc.GetSkeleton().GetGlobalMtx(anchorId, anchorMtx);
		anchorPos[i] = anchorMtx.GetCol3();

		mc.Advance(sampleRate);
	}

	atArray<float> anchorSpeed(numFrames, numFrames);
	for(int i=0; i<numFrames; ++i)
	{
		Vec3V anchorVel(V_ZERO);

		if(i>0)
		{
			anchorVel = anchorPos[i] - anchorPos[i-1];
		}
		if(i<(numFrames-1))
		{
			anchorVel = (anchorVel + anchorPos[i+1] - anchorPos[i]) * ScalarV(V_HALF);
		}

		anchorVel *= ScalarVFromF32(sampleRate);

		anchorSpeed[i] = Mag(anchorVel).Getf();
	}

	atArray<bool> anchored(numFrames, numFrames);
	for(int i=0; i<numFrames; ++i)
	{
		float anchorHeight;
		GetTranslationVertical(anchorPos[i], anchorHeight);
		
		anchored[i] = (anchorHeight<anchorHeightThreshold) && (anchorSpeed[i]<anchorSpeedThreshold);
	}

	int anchorStart = -1;
	for(int i=0; i<numFrames; ++i)
	{
		if(anchored[i] && anchorStart < 0)
		{
			anchorStart = i;
		}
		if((!anchored[i] || (i == (numFrames-1))) && (anchorStart >= 0))
		{
			int anchorEnd = i-1;
			int anchorCenter = (anchorStart+anchorEnd)/2;

			while(anchorStart > 0)
			{
				float distance = Mag(anchorPos[anchorStart-1] - anchorPos[anchorCenter]).Getf();
				if(distance <= anchorDistanceThreshold)
				{
					anchorStart--;
				}
				else
				{
					break;
				}
			}

			while(anchorEnd < (numFrames-1))
			{
				float distance = Mag(anchorPos[anchorEnd-1] - anchorPos[anchorCenter]).Getf();
				if(distance <= anchorDistanceThreshold)
				{
					anchorEnd++;
				}
				else
				{
					break;
				}
			}

			if(clip.GetTags())
			{
				crTag anchorTag;
				anchorTag.GetProperty().SetName("Anchor");
			
				crPropertyAttributeInt propBoneId;
				propBoneId.SetName("BoneId");
				propBoneId.GetInt() = int(anchorId);
				anchorTag.GetProperty().AddAttribute(propBoneId);

				crPropertyAttributeVector3 propOffset;
				propOffset.SetName("Offset");
				propOffset.GetVector3() = anchorPos[anchorCenter];
				anchorTag.GetProperty().AddAttribute(propOffset);

				clip.GetTags()->AddTag(anchorTag);
			}
			else
			{
				return false;
			}

			anchorStart = -1;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool DetectDisplacement(crClip& clip, u16 UNUSED_PARAM(moverId), const crSkeletonData& skelData, float sampleRate)
{
	crpmMotionControllerClip mc(clip, skelData);
	mc.SetLooped(false);

	Mat34V initialMtx = *mc.GetSkeleton().GetParentMtx();
	do 
	{
		mc.Advance(sampleRate);
	}
	while(!mc.IsFinished());

	Mat34V displacementMtx = *mc.GetSkeleton().GetParentMtx();
	UnTransformOrtho(displacementMtx, initialMtx, displacementMtx);

	crProperty propDisplacement;
	propDisplacement.SetName("Displacement");
	
	crPropertyAttributeSituation attribDisplacement;
	TransformVFromMat34V(attribDisplacement.GetSituation(), displacementMtx);

	if(clip.HasProperties())
	{
		clip.GetProperties()->AddProperty(propDisplacement);
	}

	return true;
}


////////////////////////////////////////////////////////////////////////////////


} // namespace crpmDetectFeatures

} // namespace rage





