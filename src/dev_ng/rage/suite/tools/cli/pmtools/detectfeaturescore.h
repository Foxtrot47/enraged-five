// 
// pmtools/detectfeaturescore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#ifndef PMTOOLS_DETECTFEATURESCORE_H
#define PMTOOLS_DETECTFEATURESCORE_H


namespace rage
{

class crClip;
class crSkeletonData;

namespace crpmDetectFeatures
{

// PURPOSE: Detect features within a motion
// RETURNS: true - successful detection
extern bool DetectFeatures(crClip& clip, const char* anchors, u16 moverId, const crSkeletonData& skelData, float sampleRate);

// PURPOSE: Detect anchors
extern bool DetectAnchors(crClip& clip, const char* anchors, const crSkeletonData& skelData, float sampleRate);

// PURPOSE: Detect anchor
extern bool DetectAnchor(crClip& clip, u16 anchorId, const crSkeletonData& skelData, float sampleRate);

// PURPOSE: Detect mover displacement
extern bool DetectDisplacement(crClip& clip, u16 moverId, const crSkeletonData& skelData, float sampleRate);


}; // namespace crpmDetectFeatures

}; // namespace rage

#endif // PMTOOLS_DETECTFEATURESCORE_H
