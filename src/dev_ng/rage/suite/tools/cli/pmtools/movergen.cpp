// 
// /movergen.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

#include "movergencore.h"

#include "cranimation/animation.h"
#include "cranimation/weightset.h"
#include "crclip/clip.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "system/main.h"
#include "system/param.h"
#include "vector/vector3.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(anim, "Input animation file (Example: input.anim)");
PARAM(out, "Input animation file (Example: output.anim)");
PARAM(skel, "Skeleton filename (Example: abc.skel)");
PARAM(rawonly, "Only accept raw input animation, generate error (not warning) on non-raw");
PARAM(zup, "Z axis (rather than Y axis) is world up");
PARAM(weightset, "Weight set filename (Example: lowerbody.weightset)");


int Main()
{
	crAnimation::InitClass();
	crClip::InitClass();

    // process help
    if(PARAM_help.Get())
    {
        sysParam::Help("");
        return 0;
    }

	// process z up
	if(PARAM_zup.Get())
	{
		g_UnitUp = ZAXIS;
	}

    // process input animation
    const char* animFilename = NULL;
    if(!PARAM_anim.Get(animFilename))
    {
        Quitf("No input animation file specified");
    }

    // process output animation
    const char* outFilename = NULL;
    if (!PARAM_out.Get(outFilename))
    {
        Quitf("No output animation file specified");
    }

    // process skeleton file
    const char* skelFilename = NULL;
    if(!PARAM_skel.Get(skelFilename))
    {
        Quitf("No skeleton filename specified\n");
    }

	// load skeleton
	crSkeletonData* skelData = crSkeletonData::AllocateAndLoad(skelFilename);
	if(!skelData)
	{
		Quitf("Unable to load .skel file '%s%s'", ASSET.GetPath(), skelFilename);
	}

	// process weight set file
	crWeightSet* weightSet = NULL;
	const char* weightSetFilename = NULL;
	if(!PARAM_weightset.Get(weightSetFilename))
	{
		Warningf("No weightset file specified, will fall back to using root to generate mover");
	}
	else
	{
		weightSet = crWeightSet::AllocateAndLoad(weightSetFilename, *skelData);
		if(!weightSet)
		{
			Quitf("Failed to load weightset file '%s'", weightSetFilename);
		}
	}
/*
    // process windowsize
    int windowsize = 6;
    PARAM_windowsize.Get(windowsize);

    // process projectOnGround
    int projectOnGround = 1;
    PARAM_windowsize.Get(projectOnGround);
*/

	// load animation
	crAnimation anim;
	if(!anim.Load(animFilename, NULL, false, false, false))
	{
		Quitf("Unable to load .anim file '%s%s'", ASSET.GetPath(), animFilename);
	}
	else if(!anim.IsRaw())
	{
		if(PARAM_rawonly.Get())
		{
			Quitf("ERROR - Source animation '%s' is not marked as raw, and raw only is specified", animFilename);
		}
		else
		{
			Warningf("WARNING - Source animation '%s' is not marked as raw, may have already been compressed", animFilename);
		}
	}

	crAnimation out;
	if(!crpmMoverGen::GenerateMover(anim, out, *skelData, weightSet))
	{
		Quitf("Failed to generate mover");
	}

	// save animation
	if(!out.Save(outFilename))
	{
		Quitf("Unable to save .anim file '%s%s'", ASSET.GetPath(), outFilename);
	}

	delete weightSet;
	delete skelData;

	crAnimation::ShutdownClass();
	crClip::ShutdownClass();

    return 0;
}


