set ARCHIVE=pmtools
set FILES=movergencore makepmcore detectfeaturescore pmdumpcore
set TESTERS=movergen makepm detectfeatures pmdump
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% curve event eventtypes %RAGE_AUD_LIBS% rmptfx pheffects
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\suite\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\samples
