// 
// pmtools/pmdumpcore.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef PMTOOLS_PMDUMPCORE_H
#define PMTOOLS_PMDUMPCORE_H

namespace rage
{

class crpmParameterizedMotionData;

namespace crpmPmDump
{

// PURPOSE: Dump parameterized motion data to output
extern bool DumpParameterizedMotion(const crpmParameterizedMotionData& pmData, int verbosity=1);


}; // namespace crpmPmDump

}; // namespace rage

#endif // PMTOOLS_PMDUMPCORE_H
