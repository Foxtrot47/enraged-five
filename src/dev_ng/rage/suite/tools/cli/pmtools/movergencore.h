// 
// pmtools/movergencore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PMTOOLS_MOVERGENCORE_H
#define PMTOOLS_MOVERGENCORE_H


namespace rage
{

class crAnimation;
class crSkeletonData;
class crWeightSet;

namespace crpmMoverGen
{

// PURPOSE: Generate mover track
// RETURNS: true - successfully generated mover, false - failed to generate (destAnim state undefined)
extern bool GenerateMover(const crAnimation& srcAnim, crAnimation& destAnim, const crSkeletonData& skelData, const crWeightSet* weightSet=NULL);


}; // namespace crpmMoverGen

}; // namespace rage

#endif // PMTOOLS_MOVERGENCORE_H
