// 
// pmtools/movergencore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "movergencore.h"

#include "atl/array.h"
#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"
#include "cranimation/weightset.h"
#include "crclip/clipanimation.h"
#include "crparameterizedmotion/motioncontroller.h"
#include "crparameterizedmotion/angle.h"
#include "crskeleton/skeletondata.h"
#include "math/angmath.h"

namespace rage
{

namespace crpmMoverGen
{

////////////////////////////////////////////////////////////////////////////////

bool GenerateMover(const crAnimation& srcAnim, crAnimation& destAnim, const crSkeletonData& skelData, const crWeightSet* weightSet)
{
	bool looped = srcAnim.IsLooped();
	bool uniform = looped;
	bool floor = true;
	const u16 moverId = 0;

	const int numSamples = int(srcAnim.GetNumInternalFrames());
	const float sampleRate = srcAnim.GetDuration()/float(numSamples-1);

	atArray<Mat34V> rootMtxs;
	rootMtxs.Resize(numSamples);

	atArray<Mat34V> moverMtxs;
	moverMtxs.Resize(numSamples);

	atArray<crFrame> sampleFrames;
	atArray<const crFrame*> sampleFramePtrs;
	sampleFrames.Resize(numSamples);
	sampleFramePtrs.Resize(numSamples);
	for(int i=0; i<numSamples; ++i)
	{
		sampleFramePtrs[i] = &sampleFrames[i];
	}

	crClipAnimation srcClip;
	srcClip.Create(srcAnim);

	crpmMotionControllerClip mc;
	mc.Init(srcClip, skelData);	
	mc.SetLooped(false);

	for(int i=0; i<numSamples; ++i)
	{
		crFrame& destFrame = sampleFrames[i];
		destFrame.Init(mc.GetFrame());
		destFrame.InitCreateMoverDofs(moverId, false);

		destFrame.Set(mc.GetFrame());

		Mat34V rootMtx;
		mc.GetSkeleton().GetGlobalMtx(0, rootMtx);

		rootMtxs[i] = rootMtx;
		moverMtxs[i] = rootMtx;

		if(weightSet)
		{
			Vec3V pos(V_ZERO);
			float sum = 0.f;

			for(int b=0; b<skelData.GetNumBones(); ++b)
			{
				const float w = weightSet->GetAnimWeight(b);
				if(w > 0.f)
				{
					Mat34V boneMtx;
					mc.GetSkeleton().GetGlobalMtx(b, boneMtx);

					pos += boneMtx.GetCol3() * ScalarVFromF32(w);
					sum += w;
				}
			}

			if(sum > 0.f)
			{
				moverMtxs[i].SetCol3(pos / ScalarVFromF32(sum));
			}
		}

		mc.Advance(sampleRate);
	} 

	Vec3V standardizeDisplacement = (rootMtxs[numSamples-1].GetCol3() - rootMtxs[0].GetCol3()) - (moverMtxs[numSamples-1].GetCol3() - moverMtxs[0].GetCol3());
	SetTranslationVertical(standardizeDisplacement, 0.f);

	// drop mover samples to floor - horizontal rotation only
	for(int i=0; i<numSamples; ++i)
	{
		if(floor)
		{
			SetTranslationVertical(moverMtxs[i].GetCol3Ref(), 0.f);
			SetRotationHorizontal(moverMtxs[i], 0.f, 0.f);

//			moverMtxs[i].ToEulersZXY(e);
//			e.x = e.y = 0.f;
//			moverMtxs[i].FromEulersZXY(e);
		}

		// if looped, standardize displacement total displacement of root and mover 
		if(looped)
		{
			moverMtxs[i].SetCol3(standardizeDisplacement * ScalarVFromF32(float(i) / float(numSamples-1)) + moverMtxs[i].GetCol3());
		}
	}

	// if uniform, smooth rotation and translation over entire animation
	if(uniform)
	{
		Vec3V translate(V_ZERO);
		float rotate = 0.f;

		for(int i=1; i<numSamples; ++i)
		{
			Vec3V ePrev, e;
			if(IsZUpAxis())
			{
				ePrev = Mat33VToEulersZXY(moverMtxs[i-1].GetMat33());
				e = Mat33VToEulersZXY(moverMtxs[i].GetMat33());

				Vec3V t = moverMtxs[i].GetCol3() - moverMtxs[i-1].GetCol3();
				t = RotateAboutZAxis(t, ScalarVFromF32(-rotate));

				rotate += CanonicalizeAngle(e.GetZf()-ePrev.GetZf());
				translate += t;
			}
			else
			{
				ePrev = Mat33VToEulersYXZ(moverMtxs[i-1].GetMat33());
				e = Mat33VToEulersYXZ(moverMtxs[i].GetMat33());

				Vec3V t = moverMtxs[i].GetCol3() - moverMtxs[i-1].GetCol3();
				t = RotateAboutYAxis(t, ScalarVFromF32(-rotate));

				rotate += CanonicalizeAngle(e.GetYf()-ePrev.GetYf());
				translate += t;
			}
		}

		bool uniformtrans = uniform;
		Vec3V originalEnd = moverMtxs[numSamples-1].GetCol3();

		for(int i=1; i<numSamples; ++i)
		{
			if(IsZUpAxis())
			{
				Vec3V e0 = Mat33VToEulersZXY(moverMtxs[0].GetMat33());

				float r = rotate * float(i) / float(numSamples-1);

				Vec3V e(0.f, 0.f, e0.GetZf() + r);
				Mat33VFromEulersZXY(moverMtxs[i].GetMat33Ref(), e);

				if(uniformtrans)
				{
					Vec3V t = translate / ScalarVFromF32(float(numSamples-1));
					t = RotateAboutZAxis(t, ScalarVFromF32(rotate * float(i-1) / float(numSamples-1)));

					moverMtxs[i].SetCol3(moverMtxs[i-1].GetCol3() + t);
				}
			}
			else
			{
				Vec3V e0 = Mat33VToEulersYXZ(moverMtxs[0].GetMat33());

				float r = rotate * float(i) / float(numSamples-1);

				Vec3V e(0.f, e0.GetZf() + r, 0.f);
				Mat33VFromEulersYXZ(moverMtxs[i].GetMat33Ref(), e);

				if(uniformtrans)
				{
					Vec3V t = translate / ScalarVFromF32(float(numSamples-1));
					t = RotateAboutYAxis(t, ScalarVFromF32(rotate * float(i-1) / float(numSamples-1)));

					moverMtxs[i].SetCol3(moverMtxs[i-1].GetCol3() + t);
				}
			}
		}

		if(uniformtrans)
		{
			Vec3V recorrectEnd = originalEnd-moverMtxs[numSamples-1].GetCol3();
			for(int i=0; i<numSamples; ++i)
			{
				moverMtxs[i].SetCol3(moverMtxs[i].GetCol3() + recorrectEnd * ScalarVFromF32(float(i) / float(numSamples-1)));
			}
		}
	}
	
	// if looped, match first and last frame root transform
	if(looped)
	{
		Mat34V driftMtx0 = rootMtxs[0];
		UnTransformOrtho(driftMtx0, moverMtxs[0], driftMtx0);

		Mat34V driftMtx1 = rootMtxs[numSamples-1];
		UnTransformOrtho(driftMtx1, moverMtxs[numSamples-1], driftMtx1);

		Mat34V driftMtx = driftMtx1;
		UnTransformOrtho(driftMtx, driftMtx0, driftMtx);
		driftMtx.SetCol3(driftMtx1.GetCol3()-driftMtx0.GetCol3());
		
		if(IsZUpAxis())
		{
			Mat33VFromXAxisAngle(driftMtx.GetMat33Ref(), ScalarV(V_ZERO));
			Mat33VFromYAxisAngle(driftMtx.GetMat33Ref(), ScalarV(V_ZERO));
			driftMtx.GetCol3Ref().SetZf(0.f);
		}
		else
		{
			Mat33VFromXAxisAngle(driftMtx.GetMat33Ref(), ScalarV(V_ZERO));
			Mat33VFromZAxisAngle(driftMtx.GetMat33Ref(), ScalarV(V_ZERO));
			driftMtx.GetCol3Ref().SetYf(0.f);
		}

		for(int i=1; i<numSamples; ++i)
		{
			ScalarV w = ScalarVFromF32(float(i)/float(numSamples-1));
			QuatV src(V_IDENTITY);
			QuatV dest = QuatVFromMat33V(driftMtx.GetMat33());
			dest = PrepareSlerp( src, dest );				
			QuatV q = Nlerp(w, src, dest);

			Mat34V mtx;
			Mat34VFromQuatV(mtx, q, w*driftMtx.GetCol3());
			Transform(moverMtxs[i], moverMtxs[i], mtx);
		}
	}

	// normalize root from mover
	for(int i=0; i<numSamples; ++i)
	{
		crFrame& destFrame = sampleFrames[i];
		const Mat34V& moverMtx = moverMtxs[i];

		destFrame.SetMoverMatrix(moverId, moverMtx);

		Mat34V rootMtx = rootMtxs[i];
		UnTransformOrtho(rootMtx, moverMtx, rootMtx);
		destFrame.SetBoneMatrix(0, rootMtx);
	}

	destAnim.Create(srcAnim.GetNumInternalFrames(), srcAnim.GetDuration(), looped, true, crAnimation::sm_DefaultFramesPerChunk, srcAnim.IsRaw());
	destAnim.CreateFromFrames(sampleFramePtrs, LOSSLESS_ANIM_TOLERANCE);

	return true;
}


////////////////////////////////////////////////////////////////////////////////

}; // namespace crAnimCombineCore

}; // namespace rage

