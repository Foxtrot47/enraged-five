// 
// pmtools/makepmcore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#include "makepmcore.h"

#include "crclip/clip.h"
#include "crclip/clips.h"
#include "crparameterizedmotion/blenddatabasecalculator.h"
#include "crparameterizedmotion/parameterizedmotionsimple.h"
#include "crparameterizedmotion/parameterizedmotionvariable.h"
#include "crparameterizedmotion/parametersampler.h"
#include "crparameterizedmotion/registrationcurvecalculator.h"

namespace rage
{

namespace crpmMakePm
{

////////////////////////////////////////////////////////////////////////////////

bool ParseVariation(const char*& vs, crpmBlendDatabaseCalculator::Variations& v, bool top=false)
{
	while(*vs)
	{
		if((*vs>='a' && *vs<='z') || (*vs>='A' && *vs<='Z'))
		{
			crpmBlendDatabaseCalculator::Variations newv;
			do 
			{
				newv.first += *vs++;
			} 
			while((*vs>='a' && *vs<='z') || (*vs>='A' && *vs<='Z') || (*vs>='0' && *vs<='9') || *vs=='_');

			if(*(vs++)=='(')
			{
				newv.second.resize(1);
				if(!ParseVariation(vs, newv))
				{
					return false;
				}
			}
			else
			{
				//ERROR
				return false;
			}
			v.second.back().second.push_back(newv);
		}
		else if(*vs>='0' && *vs<='9')
		{
			std::string n;
			do
			{
				n += *(vs++);
			}
			while(*vs>='0' && *vs<='9');

			v.second.back().first.push_back(atoi(n.c_str()));
		}
		else
		{
			// ERROR
			return false;
		}

		if(*vs=='|' && !top)
		{
			vs++;
			v.second.push_back();
		}
		else if(*vs==')' && !top)
		{
			vs++;
			return true;
		}
		else if(*vs==',')
		{
			vs++;
		}
		else if(*vs)
		{
			// ERROR
			return false;
		}
	}

	return top;

	// IF ALPHABETIC
		// READ KEYWORD
		// CREATE NEW VARIATION, FILL OUT KEYWORD
		// READ IN ( - ERROR IF MISSING
			// PARSE VARIATION
	
	// ELSE IF NUMBER 
		// READ NUMBER
		// PLACE IN ARRAY

	// ELSE ERROR

	// IF | THEN PUSH NEW VARIATION

	// IF ) THEN RETURN

	// IF , THEN LOOP
	// ELSE ERROR
}

////////////////////////////////////////////////////////////////////////////////

bool ValidateVariation(const crpmBlendDatabaseCalculator::Variations& v, std::vector<int>& counts, int& num, bool top)
{
	if(v.second.size() > 0)
	{
		if(!top && v.second.size() == 1)
		{
			return false;
		}

		num += v.second.size();
		for(u32 i=0; i<v.second.size(); ++i)
		{
			if(v.second[i].first.size() > 0 || v.second[i].second.size() > 0)
			{
				for(u32 j=0; j<v.second[i].first.size(); ++j)
				{
					int n = v.second[i].first[j];
					if(n>=0 && n<int(counts.size()))
					{
						counts[n]++;				
					}
					else
					{
						return false;
					}
				}
				for(u32 j=0; j<v.second[i].second.size(); ++j)
				{
					if(!ValidateVariation(v.second[i].second[j], counts, num, false))
					{
						return false;
					}
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool ValidateVariations(crpmBlendDatabaseCalculator::Variations& variations, const crClips& clips)
{
	std::vector<int> counts(clips.GetNumClips(), 0);
	int num = -1;

	if(ValidateVariation(variations, counts, num, true) && num <= 32)
	{
		for(u32 i=0; i<counts.size(); ++i)
		{
			if(counts[i] > 1)
			{
				return false;
			}
			else if(counts[i] == 0)
			{
				variations.second[0].first.push_back(i);
			}
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool ParseVariations(const char* variationsString, const crClips& clips, crpmBlendDatabaseCalculator::Variations& variations)
{
	Assert(variationsString && variationsString[0]);

	variations.first.clear();
	variations.second.clear();
	variations.second.resize(1);

	return ParseVariation(variationsString, variations, true) && ValidateVariations(variations, clips);
}

////////////////////////////////////////////////////////////////////////////////

bool MakeRc(crpmRegistrationCurveData& outRcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* weightSet, const crWeightSet* filterWeightSet, bool filterMover, float sampleRate, int slopeLimit, int windowSize)
{
	const char* debugPath = NULL;
	crpmRegistrationCurveCalculator RCC(skelData, weightSet, filterWeightSet, filterMover, sampleRate, slopeLimit, windowSize, debugPath);

	bool looped = clips.GetClip(0)->IsLooped();
	return RCC.CalculateByTimeAlignments(outRcData, clips, looped);
}

////////////////////////////////////////////////////////////////////////////////

bool MakeBd(crpmBlendDatabase& outBd, crpmParameterSampler& sampler, float sampleDistanceThreshold, float sourceDistanceThreshold, bool stochastic, const crpmBlendDatabaseCalculator::Variations* variations, crClips* clips, crpmRegistrationCurveData* rcData)
{
	outBd.Init(sampler.GetParameterization(), sampler.GetNumWeightDimensions());

	int sampleCount = 1;
	for(int i=0; i<sampler.GetNumParameterDimensions(); ++i)
	{
		sampleCount *= int((sampler.GetParameterization().GetDimensionInfo(i).GetSpaceScale() / sampleDistanceThreshold) + 0.5f) + 1;
	}
	
	crpmBlendDatabaseCalculator BDC(sampleCount, sampleDistanceThreshold, sourceDistanceThreshold, 0.2f, stochastic);
	return BDC.Calculate(outBd, sampler, clips, rcData, variations);
}

////////////////////////////////////////////////////////////////////////////////

bool MakePm(crpmParameterizedMotionSimpleData& outPmData, const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags, const crProperties* properties)
{
	outPmData.Init(clips, rcData, bd, tags, properties);
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool AddVariation(crpmParameterizedMotionVariableData& outPmData, const crpmBlendDatabaseCalculator::Variations& v, int variation, int varient)
{
	int nv = outPmData.AddVariation(v.first.c_str(), v.second.size(), variation, varient);

	for(u32 i=0; i<v.second.size(); ++i)
	{
		for(u32 j=0; j<v.second[i].first.size(); ++j)
		{
			outPmData.AddVariationClip(v.second[i].first[j], nv, i);
		}

		for(u32 j=0; j<v.second[i].second.size(); ++j)
		{
			if(!AddVariation(outPmData, v.second[i].second[j], nv, i))
			{
				return false;
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool MakePmVariations(crpmParameterizedMotionVariableData& outPmData, const crpmBlendDatabaseCalculator::Variations& variations)
{	
	return AddVariation(outPmData, variations, -1, -1);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace crpmMakePm

}// // namespace rage

