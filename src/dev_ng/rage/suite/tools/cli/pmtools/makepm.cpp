// 
// pmtools/makepm.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "makepmcore.h"

#include "cranimation/animation.h"
#include "cranimation/weightset.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crclip/clips.h"
#include "crmetadata/dumpoutput.h"
#include "crmetadata/tags.h"
#include "crparameterizedmotion/blenddatabase.h"
#include "crparameterizedmotion/blenddatabasecalculator.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "crparameterizedmotion/parameterizedmotionsimple.h"
#include "crparameterizedmotion/parameterizedmotionvariable.h"
#include "crparameterizedmotion/parametersampler.h"
#include "crparameterizedmotion/registrationcurve.h"
#include "crparameterizedmotion/registrationcurvecalculator.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crskeleton/skeletondata.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(clips, "Input clip files (Example: input0.clip,input1.clip)");
PARAM(out, "Output parameterized motion file (Example: output.pm)");
PARAM(skel, "Skeleton filename (Example: abc.skel)");
PARAM(weightset, "Alignment weight set filename (Example: simple.weightset)");
PARAM(sampler, "Sampler configuration string (Example: ????)");
PARAM(windowsize, "Window size property (Example: 0)");
PARAM(samplerate, "Sample rate (Example: 0.0333333)");
PARAM(samplefreq, "Sample frequency (Example: 30.0)");
PARAM(framemode, "Generate clip per frame of input animations");
PARAM(framesamplerate, "Frame mode sample rate (Example: 0.0333333)");
PARAM(framesamplefreq, "Frame mode sample frequency (Example: 30.0)");
PARAM(slopelimit, "Slope limit property (Example: 2)");
PARAM(samplethreshold, "Sample threshold (Example: 0.2)");
PARAM(sampledensity, "Legacy, see -samplethreshold");
PARAM(sourcethreshold, "Threshold between sources (Example: 0.0)");
PARAM(zup, "Z axis (rather than Y axis) is world up");
PARAM(skiplastframe, "Skip last frame when in frame mode");
PARAM(filterweightset, "Filter weight set filename (Example: upper.weightset)");
PARAM(filtermover, "Filter mover (Example: 0)");
PARAM(nometadata, "No meta data properties added");
PARAM(stochastic, "Populate blend database using stochastic method");
PARAM(barycentric, "Populate blend database using regular barycentric method");
PARAM(variations, "Describe possible varaitions");
PARAM(verbose, "Verbose debugging output of output parameterized motion");


int Main()
{
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();
	crpmParameterizedMotion::InitClass();

	// process help
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process z up
	if(PARAM_zup.Get())
	{
		g_UnitUp = ZAXIS;
	}

	// process sample freq/rate:
	float sampleFreq = 30.f;
	PARAM_samplefreq.Get(sampleFreq);
	float sampleRate = 1.f/sampleFreq;
	PARAM_samplerate.Get(sampleRate);

	float frameSampleFreq = sampleFreq;
	PARAM_framesamplefreq.Get(frameSampleFreq);
	float frameSampleRate = 1.f/frameSampleFreq;
	PARAM_framesamplerate.Get(frameSampleRate);

	// process frame mode
	bool frameMode = PARAM_framemode.Get();

	// process stochastic/barycentric (barycentric now assumed default)
	bool stochastic = PARAM_stochastic.Get();

	// process clips
	crClips* clips = rage_new crClips;

	const int bufSize = 8192;
	char buf[bufSize];
	const int maxClipNames = 128;
	const char* clipNames[maxClipNames];

	int numClipNames = PARAM_clips.GetArray(clipNames, maxClipNames, buf, bufSize);
	if(numClipNames <= 1)
	{
		Quitf("Insufficient clip names provided");
	}

	for(int i=0; i<numClipNames; i++)
	{
		crClip* clip = NULL;
		if(!stricmp(fiAssetManager::FindExtensionInPath(clipNames[i]), ".anim"))
		{
			crAnimation* anim = crAnimation::AllocateAndLoad(clipNames[i]);
			if(!anim)
			{
				Quitf("Unable to load animation '%s'", clipNames[i]);
			}

			if(frameMode)
			{
				int numAnimFrames = int((anim->GetDuration() / frameSampleRate) + 0.5f);
				if(!PARAM_skiplastframe.Get())
				{
					numAnimFrames++;
				}

				for(int j=0; j<numAnimFrames; ++j)
				{
					crClipAnimation* clipAnim = crClipAnimation::AllocateAndCreate(*anim);

					float time = Clamp(float(j)*frameSampleRate, 0.f, anim->GetDuration()-sampleRate);
					float duration = sampleRate;
					clipAnim->SetStartEndTimes(time, time+duration);

					// insert block tag to force pose
					crTag blockTag;
					blockTag.GetProperty().SetName("Block");
					blockTag.SetStart(0.f);
					blockTag.SetEnd(1.f);
					
					crPropertyAttributeFloat propDivision;
					propDivision.SetName("Division");
					propDivision.GetFloat() = 1.f;
					blockTag.GetProperty().AddAttribute(propDivision);

					clipAnim->GetTags()->AddTag(blockTag);

					char clipName[RAGE_MAX_PATH];
					ASSET.RemoveExtensionFromPath(clipName, RAGE_MAX_PATH, (const char*)clipAnim->GetName());
					sprintf(clipName, "%s_%d.clip", clipName, j);

					if(!clipAnim->Save(clipName))
					{
						Quitf("Unable to save newly generated frame mode clip '%s'", (const char*)clip->GetName());
					}

					clips->AddClip(clipAnim);
					clipAnim->Release();
				}
			}
			else
			{
				clip = crClipAnimation::AllocateAndCreate(*anim);
				if(!clip->Save((const char*)clip->GetName()))
				{
					Quitf("Unable to save newly generated clip '%s'", (const char*)clip->GetName());
				}
			}
			anim->Release();
		}
		else
		{		
			clip = crClip::AllocateAndLoad(clipNames[i]);
			if(!clip)
			{
				Quitf("Unable to load clip '%s'", clipNames[i]);
			}
		}

		if(clip)
		{
			clips->AddClip(clip);
			clip->Release();
		}
	}

	// process output animation
	const char* outFilename = NULL;
	if(!PARAM_out.Get(outFilename))
	{
		Quitf("No output parameterized motion file specified");
	}

	// process skeleton file
	const char* skelFilename = NULL;
	if(!PARAM_skel.Get(skelFilename))
	{
		Quitf("No skeleton filename specified");
	}

	crSkeletonData* skelData = crSkeletonData::AllocateAndLoad(skelFilename);
	if(!skelData)
	{
		Quitf("Unable to load .skel file '%s%s'", ASSET.GetPath(), skelFilename);
	}

	// process weight set file
	crWeightSet weightSet;
	crWeightSet* weightSetPtr = NULL;

	const char* weightSetFilename = NULL;
	if(!PARAM_weightset.Get(weightSetFilename))
	{
		Warningf("No alignment weight set filename, poor quality alignment likely");
	}
	else if(!weightSet.Load(weightSetFilename, *skelData))
	{
		Quitf("Unable to load .weightset file '%s%s'", ASSET.GetPath(), weightSetFilename);
	}
	else
	{
		weightSetPtr = &weightSet;
	}

	// process filter mover
	bool filterMover = PARAM_filtermover.Get();

	// process filter weight set file
	crWeightSet filterWeightSet;
	crWeightSet* filterWeightSetPtr = NULL;

	const char* filterWeightSetFilename = NULL;
	if(PARAM_filterweightset.Get(filterWeightSetFilename))
	{
		if(!filterWeightSet.Load(filterWeightSetFilename, *skelData))
		{
			Quitf("Unable to load .weightset file '%s%s'", ASSET.GetPath(), filterWeightSetFilename);
		}
		else
		{
			filterWeightSetPtr = &filterWeightSet;
		}
	}

	// process sampler config string
	const char* samplerConfig = NULL;
	if(!PARAM_sampler.Get(samplerConfig))
	{
		Quitf("Missing sampler configuration string");
	}

	// process window size
	int windowSize = 0;
	PARAM_windowsize.Get(windowSize);

	// process slope limit
	int slopeLimit = 2;
	PARAM_slopelimit.Get(slopeLimit);

	// process sample threshold
	float sampleDistanceThreshold = 0.2f;
	if(!PARAM_samplethreshold.Get(sampleDistanceThreshold))
	{
		PARAM_sampledensity.Get(sampleDistanceThreshold);
	}

	// process source threshold
	float sourceDistanceThreshold = 0.f;
	PARAM_sourcethreshold.Get(sourceDistanceThreshold);

	// process meta data
	bool noMetaData = PARAM_nometadata.Get();

	// process verbose
	bool verbose = PARAM_verbose.Get();

	// process variations
	crpmBlendDatabaseCalculator::Variations variations;
	crpmBlendDatabaseCalculator::Variations* variationsPtr = NULL;
	const char* variationsString = NULL;
	if(PARAM_variations.Get(variationsString) && variationsString && variationsString[0])
	{
		if(!crpmMakePm::ParseVariations(variationsString, *clips, variations))
		{
			Quitf("Bad variations string '%s'", variationsString);
		}
		variationsPtr = &variations;
	}

	crpmRegistrationCurveData* rcData = rage_new crpmRegistrationCurveData;
	if(crpmMakePm::MakeRc(*rcData, *clips, *skelData, weightSetPtr, filterWeightSetPtr, filterMover, sampleRate, slopeLimit, windowSize))
	{
		crpmParameterSampler* sampler = crpmParameterSampler::BuildSampler(samplerConfig, rcData, *clips, *skelData, filterWeightSetPtr, filterMover);
		if(sampler)
		{
			crpmBlendDatabase* bd = rage_new crpmBlendDatabase;
			if(crpmMakePm::MakeBd(*bd, *sampler, sampleDistanceThreshold, sourceDistanceThreshold, stochastic, variationsPtr, clips, rcData))
			{
				const crClip* srcClip = clips->GetClip(rcData->GetReferenceClipIndex());
				crTags* tags = (srcClip&&srcClip->GetTags())?srcClip->GetTags()->Clone():NULL;
				crProperties* properties = (srcClip&&srcClip->GetTags())?srcClip->GetProperties()->Clone():NULL;

				crpmParameterizedMotionSimpleData* pmSimpleData = NULL;
				crpmParameterizedMotionVariableData* pmVariableData = NULL;
				if(variationsPtr)
				{
					pmVariableData = rage_new crpmParameterizedMotionVariableData;
					pmSimpleData = pmVariableData;
				}
				else
				{
					pmSimpleData = rage_new crpmParameterizedMotionSimpleData;
				}

				if(crpmMakePm::MakePm(*pmSimpleData, *clips, rcData, *bd, tags, properties))
				{
					if(pmVariableData && variationsPtr)
					{
						if(!crpmMakePm::MakePmVariations(*pmVariableData, *variationsPtr))
						{
							Quitf("Failed to set up parameterized motion variations");
						}
					}

					if(!noMetaData)
					{
						crProperty propPmSampler;
						propPmSampler.SetName("PmSamplerConfig");
					
						crPropertyAttributeString attribConfig;
						attribConfig.SetName("PmSamplerConfig");
						attribConfig.GetString() = samplerConfig;
						propPmSampler.AddAttribute(attribConfig);

						pmSimpleData->GetProperties()->AddProperty(propPmSampler);
					}

					if(!pmSimpleData->Save(outFilename))
					{
						Quitf("Failed to save parameterized motion data '%s'", outFilename);
					}
					
					if(verbose)
					{
						crDumpOutputStdIo dumpStdIo;
						pmSimpleData->Dump(dumpStdIo);
					}

					clips = NULL;
					rcData = NULL;
					bd = NULL;
				}
				else
				{
					Quitf("Failed to generate simple parameterized motion data");
				}

				delete pmSimpleData;
			}
			else
			{
				Quitf("Failed to generate blend database");
			}
			
			if(bd)
			{
				delete bd;
				bd = NULL;
			}

			delete sampler;
		}
		else
		{
			Quitf("Failed to create sampler, check sampler config string");
		}
	}
	else
	{
		Quitf("Failed to generate registration curve data");
	}

	delete skelData;
	delete rcData;
	delete clips;

	crpmParameterizedMotion::ShutdownClass();
	crAnimation::ShutdownClass();
	crClip::ShutdownClass();

	return 0;
}

