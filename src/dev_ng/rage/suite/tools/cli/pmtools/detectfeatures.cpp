// 
// detectfeatures.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 


#include "detectfeaturescore.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(clip, "Source clip (Example: clip0.clip)");
PARAM(out, "Output clip (Example: out.clip)");
PARAM(skel, "Skeleton file (Example: abc.skel)");
PARAM(samplerate, "Sample rate (Example: 0.033333)");
PARAM(samplefreq, "Sample frequency (Example: 30.0)");
PARAM(anchors, "Bone names for anchor search (Example: Foot_L,Foot_R)");


int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crClip::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process clip filename:
	const char* clipFilename = NULL;
	PARAM_clip.Get(clipFilename);
	if(!clipFilename)
	{
		Quitf("ERROR - No clip filename specified\n");
	}

	// process output filename:
	const char* outputFilename = NULL;
	PARAM_out.Get(outputFilename);
	if(!outputFilename)
	{
		Quitf("ERROR - No output filename specified\n");
	}
	
	// process skeleton filename:
	const char* skelFilename = NULL;
	PARAM_skel.Get(skelFilename);
	if(!skelFilename)
	{
		Quitf("ERROR - No skeleton filename specified\n");
	}

	// process sample freq/rate:
	float sampleFreq = 30.f;
	PARAM_samplefreq.Get(sampleFreq);
	float sampleRate = 1.f/sampleFreq;
	PARAM_samplerate.Get(sampleRate);

	// process anchors:
	const char* anchors = NULL;
	PARAM_anchors.Get(anchors);

	// load clip:
	crClip* clip = crClip::AllocateAndLoad(clipFilename);
	if(!clip)
	{
		Quitf("ERROR - Failed to load clip '%s'", clipFilename);
	}

	// load skeleton:
	crSkeletonData* skelData = crSkeletonData::AllocateAndLoad(skelFilename);
	if(!skelData)
	{
		Quitf("ERROR - Failed to load skeleton '%s'", skelFilename);
	}
	
	// detect features:
	if(crpmDetectFeatures::DetectFeatures(*clip, anchors, 0, *skelData, sampleRate))
	{
		// save dest clip:
		if(!clip->Save(outputFilename))
		{
			Quitf("ERROR - Failed to save clip '%s'", outputFilename);
		}	
	}
	else
	{
		Quitf("ERROR - Failed to detect features");
	}

	// cleanup:
	delete skelData;
	delete clip;

	// shutdown:
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}