// 
// pmtools/pmdumpcore.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "pmdumpcore.h"

#include "crmetadata/dumpoutput.h"
#include "crparameterizedmotion/parameterizedmotion.h"


namespace rage
{

namespace crpmPmDump
{

////////////////////////////////////////////////////////////////////////////////

bool DumpParameterizedMotion(const crpmParameterizedMotionData& pmData, int verbosity)
{
	crDumpOutputStdIo output;
	output.SetVerbosity(verbosity);
	
	Printf("\n");
	pmData.Dump(output);
	Printf("\n");

	return false;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace crpmPmDump

} // namespace rage


