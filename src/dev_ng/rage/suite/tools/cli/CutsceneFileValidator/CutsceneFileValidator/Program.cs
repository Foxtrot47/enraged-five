﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using RSG.ManagedRage;
using RSG.SourceControl.Perforce;
using RSG.Base.OS;
using System.Collections;
using RSG.Base.Configuration;

namespace CutsceneFileValidator
{
    class ConcatEntry
    {
        public List<string> missions = new List<String>();
        public List<string> scenes = new List<String>();
    }

    class Program
    {
        static Dictionary<string, string> sceneToMission = new Dictionary<string, string>();
        static Dictionary<string, ConcatEntry> concatToScenes = new Dictionary<string, ConcatEntry>();
        static Dictionary<string, string> rpfOverload = new Dictionary<string, string>();
        static List<string> excludeList = new List<string>();

        static string PROJECT_ROOT = Environment.GetEnvironmentVariable("RS_PROJROOT");

        static void GatherConcatData(CommandOptions options)
        {
            string[] files = Directory.GetFiles(String.Format(@"{0}\cuts\!!Cutlists", options.Branch.Assets), "*.concatlist");
            
            foreach (string file in files)
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(file);

                XmlNodeList partNodes = xmlDoc.SelectNodes("/RexRageCutscenePartFile/parts/Item/filename");

                XmlNode pathNode = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/path");

                string m_OutputPath = String.Empty;

                if (pathNode != null)
                    m_OutputPath = pathNode.InnerText;

                for (int i = 0; i < partNodes.Count; ++i)
                {
                    if (concatToScenes.ContainsKey(Path.GetFileName(m_OutputPath).ToLower()))
                    {
                        concatToScenes[Path.GetFileName(m_OutputPath).ToLower()].scenes.Add(Path.GetFileNameWithoutExtension(Path.GetDirectoryName(partNodes[i].InnerText)).ToLower());
                    }
                    else
                    {
                        ConcatEntry entry = new ConcatEntry();
                        entry.scenes.Add(Path.GetFileNameWithoutExtension(Path.GetDirectoryName(partNodes[i].InnerText)).ToLower());
                        concatToScenes.Add(Path.GetFileName(m_OutputPath).ToLower(), entry);
                    }
               }
            }

            for (int i = 0; i < concatToScenes.Count; ++i)
            {
                for (int j = 0; j < concatToScenes.ElementAt(i).Value.scenes.Count; ++j)
                {
                    foreach (KeyValuePair<string, string> pair in sceneToMission)
                    {
                        if (pair.Key == concatToScenes.ElementAt(i).Value.scenes[j])
                        {
                            if(!concatToScenes.ElementAt(i).Value.missions.Contains(pair.Value))
                                concatToScenes.ElementAt(i).Value.missions.Add(pair.Value);
                        }
                    }
                }
            }
        }

        static void GatherZipData(CommandOptions options)
        {
            string[] zipFiles = Directory.GetFiles(String.Format(@"{0}\anim\cutscene", options.Branch.Export), "*.icd.zip", SearchOption.AllDirectories);

            foreach (string zipFile in zipFiles)
            {
                string sceneName = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(zipFile));
                string missionName = Path.GetFileNameWithoutExtension(Path.GetDirectoryName(zipFile));

                if (!sceneToMission.ContainsKey(sceneName))
                {
                    sceneToMission.Add(sceneName, missionName);
                }
            }
        }

        static void GatherExcludeListData(CommandOptions options)
        {
            excludeList.AddRange(File.ReadAllLines(String.Format(@"{0}\etc\config\cutscene\{1}\excludedscenes.txt", options.Config.ToolsRoot, options.Branch.Name)));
        }

        static void GatherRPFOverloadData(CommandOptions options)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(String.Format(@"{0}\etc\config\cutscene\{1}\missionoverload.xml", options.Config.ToolsRoot, options.Branch.Name));

            XmlNodeList rpfOverloads = xmlDoc.SelectNodes("/MissionOverloads/RPFOverload");

            foreach (XmlNode node in rpfOverloads)
            {
                rpfOverload.Add(node.Attributes["name"].InnerText.ToLower(), node.Attributes["mission"].InnerText);
            }
        }

        static string GetZipForScene(string scene, CommandOptions options)
        {
            string[] zipFiles = Directory.GetFiles(String.Format(@"{0}\anim\cutscene", options.Branch.Export), "*.icd.zip", SearchOption.AllDirectories);

            foreach (string zipFile in zipFiles)
            {
                if (Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(zipFile)).ToLower() == scene)
                {
                    return zipFile;
                }
            }

            return String.Empty;
        }

        static void RevertZipFile(P4 p4, string scene, CommandOptions options)
        {
            string zip = GetZipForScene(scene, options);
            if (zip != String.Empty)
            {
                Console.WriteLine("Reverting file '{0}'", zip);

                p4.Run("revert", zip);
            }
        }

        static bool IsInExcludeList(string scene)
        {
            foreach (string file in excludeList)
            {
                if (scene == file.ToLower())
                    return true;
            }

            return false;
        }

        [STAThread]
        static void Main(string[] args)
        {
            LongOption[] opts = new LongOption[] { };
            String[] args2 = {};
            ArrayList lst = new ArrayList();

            CommandOptions options = new CommandOptions(args2, opts);

            GatherZipData(options);
            GatherConcatData(options);
            GatherExcludeListData(options);
            GatherRPFOverloadData(options);

            string[] platforms = {"pc", "ps3", "x64", "xbox360", "ps4", "xboxone"};

            P4 p4 = new P4();
            p4.Connect();

            foreach (string mission in Directory.GetDirectories(String.Format(@"{0}\export\anim\cutscene", options.Branch.Assets)))
            {
                foreach (string platform in platforms)
                {
                    Console.WriteLine("Processing '{0}'", platform);

                    string rpf = String.Format(@"{0}\non_final\build\{1}\{2}\anim\cutscene\cuts_{3}.rpf", options.Branch.Assets, options.Branch.Name, platform, Path.GetFileName(mission));

                    Packfile rpfFile = new Packfile();
                    if (rpfFile.Load(rpf))
                    {
                        Console.WriteLine("Loaded rpf '{0}'", rpf);
                        foreach (KeyValuePair<string, ConcatEntry> pair in concatToScenes)
                        {
                            if (pair.Value.missions.Contains(Path.GetFileName(mission).ToLower()))
                            {
                                // concat should be in this rpf
                                string entry = pair.Key + ".cut";

                                if (rpfOverload.ContainsKey(pair.Key))
                                {
                                    if (rpfOverload[pair.Key] != mission)
                                        continue;
                                }

                                if (rpfFile.FindEntry(entry) == null)
                                {
                                    //if (IsInExcludeList(pair.Key) && useExcludeList) continue;

                                    Console.WriteLine("'Error: {0}' not found in rpf '{1}'", pair.Key, rpf);

                                    // not found then revert all parts of this concat
                                    foreach (string part in pair.Value.scenes)
                                    {
                                        RevertZipFile(p4, part, options);
                                    }
                                }
                            }
                        }

                        foreach (KeyValuePair<string, string> pair in sceneToMission)
                        {
                            if (pair.Value == Path.GetFileName(mission).ToLower())
                            {
                                // concat should be in this rpf
                                string entry = pair.Key + ".cut";

                                if (rpfFile.FindEntry(entry) == null)
                                {
                                    //if (IsInExcludeList(pair.Key)) continue;

                                    Console.WriteLine("'Error: {0}' not found in rpf '{1}'", pair.Key, rpf);

                                    RevertZipFile(p4, pair.Key, options);
                                }
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error: RPF '{0}' does not exist", rpf);
                    }

                    Console.WriteLine("");
                }
            }

            p4.Disconnect();
        }
    }
}
