﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using P4API;
using System.Xml;
using RSG.Base.Configuration;

namespace cutscene_content_generator
{
    class Program
    {
        static string CUTSCENE_SOURCE_PATH = @"x:\gta5\art\ng\animation\cutscene\!!scenes\";
        static string ART_SOURCE_PATH = String.Format("X:\\{0}\\art\\ng", Environment.GetEnvironmentVariable("RS_PROJECT"));

        static string CUTSCENE_CONTENT_FILE = String.Format("{0}\\etc\\content\\content_animation_cutscene.xml", Environment.GetEnvironmentVariable("RS_TOOLSROOT"));
        static string CUTSCENE_CONTENT_FBX_FILE = String.Format("{0}\\etc\\content\\content_animation_cutscene_fbx.xml", Environment.GetEnvironmentVariable("RS_TOOLSROOT"));

        static void WriteNode(XmlTextWriter writer, string id, string content_type, string path, bool wildcard)
        {
            writer.WriteStartElement("node");
            writer.WriteAttributeString("id", id);

            writer.WriteStartElement("data");
            writer.WriteAttributeString("key", "content_type");
            writer.WriteString(content_type);
            writer.WriteEndElement();

            writer.WriteStartElement("data");
            writer.WriteAttributeString("key", "path");
            writer.WriteString(path);
            writer.WriteEndElement();

            if (wildcard)
            {
                writer.WriteStartElement("data");
                writer.WriteAttributeString("key", "wildcard");
                writer.WriteString("*.icd.zip");
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        static void WriteEdge(XmlTextWriter writer, string source, string target, string processor)
        {
            writer.WriteStartElement("edge");
            writer.WriteAttributeString("source", source);
            writer.WriteAttributeString("target", target);

            writer.WriteStartElement("data");
            writer.WriteAttributeString("key", "processor");
            writer.WriteString(processor);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        static void WriteContent()
        {
            P4 p4 = new P4();
            p4.Connect();

            p4.Run("edit", CUTSCENE_CONTENT_FILE);

            //FileMapping fm = new FileMapping(p4, CUTSCENE_SOURCE_PATH);

            FileState[] fileStates = RSG.SourceControl.Perforce.FileState.Create(p4, new String[] { CUTSCENE_SOURCE_PATH + "/....fbx" });

            Dictionary<string, string> dictionaryNodes = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            string[] split = CUTSCENE_SOURCE_PATH.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (FileState f in fileStates)
            {
                if (f.HeadAction == FileAction.MoveAndDelete || f.HeadAction == FileAction.Delete) continue;

                string[] split2 = f.ClientFilename.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

                string mission = split2[split.Length];

                if (!dictionaryNodes.ContainsKey(mission))
                {
                    dictionaryNodes.Add(mission, "$(export)/anim/cutscene/" + mission);
                }
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (XmlTextWriter writer = new XmlTextWriter(CUTSCENE_CONTENT_FILE, Encoding.UTF8))
            {
                writer.Formatting = Formatting.Indented;

                writer.WriteStartDocument();

                writer.WriteComment(" GTA5 Cutscene Animation Content Tree; defining the assets and processes for the cutscene animation pipeline. ");

                writer.WriteStartElement("graphml");

                writer.WriteStartElement("graph");
                writer.WriteAttributeString("id", "animation::cutscene");
                writer.WriteAttributeString("edgedefault", "directed");

                // Write ZIPXML Node

                writer.WriteComment(" The zipxml for zipping up raw anims/clips ");

                WriteNode(writer,
                                "animation::cutscene::export::ZIPXML",
                                "file",
                                "$(export)/anim/cutscene/master_icd_list.xml",
                                 false);

                // Write FAKE NODE

                writer.WriteComment(" Define fake node used as output for zip generation. ");

                WriteNode(writer,
                                "animation::cutscene::zipxml::target",
                                "dummy",
                                "$(target)/anim/cutscene/clip_$(name).rpf",
                                false);

                // Zip Processor

                writer.WriteComment(" Define the processor for the zip generation. ");

                WriteEdge(writer,
                           "animation::cutscene::export::ZIPXML",
                           "animation::cutscene::zipxml::target",
                           "RSG.Pipeline.Processor.Common.AssetPackProcessor");

                foreach (KeyValuePair<string, string> pair in dictionaryNodes)
                {
                    WriteNode(writer,
                                    "animation::cutscene::export::" + pair.Key,
                                    "directory",
                                    pair.Value,
                                    true);
                }

                writer.WriteComment(" Define fake node used as output for all preprocess processes. ");

                WriteNode(writer,
                                "animation::cutscene::target",
                                "file",
                                "$(target)/anim/cutscene/cuts_$(name).rpf",
                                false);

                foreach (KeyValuePair<string, string> pair in dictionaryNodes)
                {
                    WriteEdge(writer,
                            "animation::cutscene::export::" + pair.Key,
                            "animation::cutscene::target",
                            "RSG.Pipeline.Processor.Animation.Cutscene.PreProcess");
                }

                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        static IBranch GetBranch(IConfig config, IProject project)
        {
            //if (project.Branches.ContainsKey(config.CoreProject.DefaultBranchName))
            //{
            //    return project.Branches[config.CoreProject.DefaultBranchName];
            //}

            return project.Branches[project.DefaultBranchName];
        }

        static bool SyncFolderForInspection(P4 p4, string root)
        {
            // func to avoid having to sync entire cuts dir, just sync one .clip file from each folder
            Console.WriteLine("Syncing from folder '{0}'", root);
            P4RecordSet dirs = p4.Run("dirs", root);
            
            if (dirs.Records.Length==0)
                return false;
            foreach (var record in dirs.Records)
            {
                string dir = record.Fields["dir"];
                dir = dir + "/*.clip";
                p4.Run("sync", dir);
            }
            return true;
        }

        static void CreateFbxContentFile(P4 p4, string rootPath, string sourcePath, string artPath, string contentFbxFile, bool doOverrides)
        {
            if (doOverrides)
            {
                bool projHasCuts = false;
                //Sync cg and ng cuts folders for later on
                if (SyncFolderForInspection(p4, Path.Combine(rootPath, "assets_ng\\cuts\\*")))
                    projHasCuts = true;
                if (SyncFolderForInspection(p4, Path.Combine(rootPath, "assets\\cuts\\*")))
                    projHasCuts = true;
                if (!projHasCuts)
                    return;
            }

            String content_file = contentFbxFile;
            p4.Run("edit", content_file);
            p4.Run("add", content_file);

            using (XmlTextWriter writer = new XmlTextWriter(contentFbxFile, Encoding.UTF8))
            {
                writer.Formatting = Formatting.Indented;

                writer.WriteStartDocument();

                writer.WriteComment("GTA5 Cutscene Animation Content Tree; defining the assets and processes for the cutscene animation pipeline. ");

                writer.WriteStartElement("graphml");
                writer.WriteAttributeString("xmlns", "http://graphml.graphdrawing.org/xmlns");
                writer.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns:xi", "http://www.w3.org/2003/XInclude");
                writer.WriteAttributeString("xsi:schemaLocation", "http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd");

                writer.WriteStartElement("xi:include");
                writer.WriteAttributeString("href", "%RS_TOOLSROOT%/etc/content/content_attributes.xml");
                writer.WriteEndElement();

                writer.WriteStartElement("graph");
                writer.WriteAttributeString("id", "animation::cutscene::FBX");
                writer.WriteAttributeString("edgedefault", "directed");

                WriteFBXContent(p4, writer, rootPath, sourcePath, artPath, doOverrides);

                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        static void WriteFBXContentFile( bool doOverrides, string target)
        {
            using (P4 p4 = new P4())
            {
                p4.Connect();

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;

                IConfig config = ConfigFactory.CreateConfig();

                
                if (target == "")
                {
                    foreach (KeyValuePair<String, IProject> pair in config.DLCProjects)
                    {
                        CreateFbxContentFile(p4, pair.Value.Root, Path.Combine(GetBranch(config, pair.Value).Art, "animation", "cutscene", "!!scenes"), GetBranch(config, pair.Value).Art, Path.Combine(pair.Value.Root, "content_animation_cutscene_fbx.xml"), doOverrides);
                    }
                    CreateFbxContentFile(p4, config.CoreProject.Root,CUTSCENE_SOURCE_PATH, ART_SOURCE_PATH, CUTSCENE_CONTENT_FBX_FILE, doOverrides);
                }
                else
                {
                    //We've been supplied a Target project (either dlc name or 'root')
                    if (target.ToLower() == "root")
                        CreateFbxContentFile(p4, config.CoreProject.Root, CUTSCENE_SOURCE_PATH, ART_SOURCE_PATH, CUTSCENE_CONTENT_FBX_FILE, doOverrides);
                    else
                    {
                        if (config.DLCProjects.ContainsKey(target))
                        {
                            IProject val;
                            if (config.DLCProjects.TryGetValue(target, out val))
                                CreateFbxContentFile(p4, val.Root, Path.Combine(GetBranch(config, val).Art, "animation", "cutscene", "!!scenes"), GetBranch(config, val).Art, Path.Combine(val.Root, "content_animation_cutscene_fbx.xml"), doOverrides);
                        }
                        else
                            Console.WriteLine("ERROR: Target project '{0}' not found!");
                    }
                }
            }
        }

        static void WriteFBXContent(P4 p4, XmlTextWriter writer, string rootPath, string sourcePath, string artPath, bool doOverrides)
        {
            FileState[] fileStates = RSG.SourceControl.Perforce.FileState.Create(p4, new String[] { sourcePath + "....fbx" });

            if (fileStates.Length == 0) return;

            Dictionary<string, string> dictionaryNodes = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, string> fbxFiles = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, string> fbxOverrides = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            string[] split = sourcePath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (FileState f in fileStates)
            {
                if (f.HeadAction == FileAction.MoveAndDelete || f.HeadAction == FileAction.Delete) continue;

                string[] split2 = f.ClientFilename.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

                string mission = split2[split.Length];

                if (!dictionaryNodes.ContainsKey(mission.ToLower()))
                {
                    dictionaryNodes.Add(mission.ToLower(), ("$(export)/anim/cutscene/" + mission).ToLower());
                }

                // Ignore fbx files in stem and backup folders
                if (f.ClientFilename.ToLower().Contains("_stems") ||
                    f.ClientFilename.ToLower().Contains("\\backup") ||
                    f.ClientFilename.ToLower().Contains("\\face")) continue;

                string path = f.ClientFilename.ToLower().Replace(artPath.ToLower(), "$(art)");

                if (!fbxFiles.ContainsKey(path))
                {
                    fbxFiles.Add(path, mission.ToLower());
                }
            }

            // We loop through each folder in this proj cuts folder, and fill dict with mission/fbx name mappings
            // We need this as not all scenes are named the same as fbx's on gta5
            if (doOverrides)
            {
                List<string> dirsToSearch = new List<string>();
                dirsToSearch.Add(Path.Combine(rootPath, @"assets_ng\cuts"));
                dirsToSearch.Add(Path.Combine(rootPath, @"assets\cuts"));
                foreach (var root in dirsToSearch)
                {
                    if (Directory.Exists(root))
                    {
                        List<string> dirs = new List<string>(Directory.EnumerateDirectories(root));
                        foreach (var dir in dirs)
                        {
                            string realMissionName = Path.GetFileName(dir).ToLower();
                            if (!fbxOverrides.ContainsKey(realMissionName))
                            {
                                //in dir, if we find one file with .clip, open and pull out fbx name
                                List<string> files = new List<string>(Directory.GetFiles(dir));
                                foreach (var f in files)
                                {
                                    if (Path.GetExtension(f) == ".clip")
                                    {
                                        string text = System.IO.File.ReadAllText(f);
                                        int endIdx = text.IndexOf(".fbx");
                                        if (endIdx == -1)
                                            continue;
                                        int startIdx = endIdx - 1;
                                        while (text[startIdx] != '\\' && text[startIdx] != '/')
                                            startIdx -= 1;
                                        startIdx += 1;
                                        string fbxName = text.Substring(startIdx, (endIdx - startIdx));
                                        if (realMissionName != fbxName.ToLower())
                                            fbxOverrides.Add(realMissionName, fbxName.ToLower());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // We need these nodes if we loading the fbx content independantly, otherwise they are defined in the general cutscene content tree

            foreach (KeyValuePair<string, string> pair in dictionaryNodes)
            {
                WriteNode(writer,
                                "animation::cutscene::export::" + pair.Key,
                                "directory",
                                pair.Value,
                                true);
            }

            writer.WriteComment(" Define FBX Nodes. ");

            foreach (KeyValuePair<string, string> pair in fbxFiles)
            {
                WriteNode(writer,
                                "animation::cutscene::export::" + Path.GetFileNameWithoutExtension(pair.Key),
                                "file",
                                pair.Key,
                                false);

                string missionName = Path.GetFileNameWithoutExtension(pair.Key);
                if (fbxOverrides.ContainsValue(missionName.ToLower()))
                {
                    string realName = fbxOverrides.FirstOrDefault(x => x.Value == missionName.ToLower()).Key;
                    writer.WriteComment(" Define Asset Override Nodes. ");

                    WriteNode(writer,
                                "animation::cutscene::export::" + missionName + "_data",
                                "file",
                                Path.Combine("$(assets)", "cuts", realName, "data.cutpart"),
                                false);

                    WriteEdge(writer,
                        "animation::cutscene::export::" + missionName + "_data",
                        "animation::cutscene::export::" + missionName,
                        "RSG.Pipeline.Processor.Common.DccMotionbuilderExportProcessor");
                }
            }

            writer.WriteComment(" Define FBX Edges. ");

            foreach (KeyValuePair<string, string> pair in fbxFiles)
            {
                WriteEdge(writer,
                        "animation::cutscene::export::" + Path.GetFileNameWithoutExtension(pair.Key),
                        "animation::cutscene::export::" + pair.Value,
                        "RSG.Pipeline.Processor.Common.DccMotionbuilderExportProcessor");
            }
        }

        [STAThread]
        static int Main(string[] args)
        {
            bool doFbx = false;
            bool doOverrides = false;
            string target = "";
            for (int i = 0; i < args.Length; i++)
            {
                string[] arg = args[i].Split('=');
                if (arg[0] == "--fbx")
                    doFbx = true;
                else if (arg[0] == "--overrides")
                    doOverrides = true;
                else if (arg[0] == "--target")
                    if (arg.Length > 1)
                        target = arg[1];
            }

            if (doFbx)
            {
                Console.WriteLine("Generating cutscene fbx content tree '{0}'", CUTSCENE_CONTENT_FBX_FILE);
                if (doOverrides)
                    Console.WriteLine("Also generating fbx content tree overrides");
                WriteFBXContentFile(doOverrides, target);
            }
            else
            {
                Console.WriteLine("Generating cutscene content tree '{0}'", CUTSCENE_CONTENT_FILE);
                WriteContent();
            }

            return 0;
        }
    }
}
