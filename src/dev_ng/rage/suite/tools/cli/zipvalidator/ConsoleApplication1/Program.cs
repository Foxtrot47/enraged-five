﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;

namespace ConsoleApplication1
{
    class Program
    {
        static void AnalyseZipData(string zipDir, Dictionary<string, List<string> > dict)
        {
            string[] files = Directory.GetFiles(zipDir, "*.icd.zip", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                //Console.WriteLine(file);
                using (ZipFile zf = ZipFile.Read(file))
                {
                    foreach (ZipEntry ze in zf.Entries)
                    {
                        string key = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(file)).ToLower();
                        if(!dict.ContainsKey(key))
                        {
                            List<string> lstFiles = new List<string>();
                            dict.Add(key, lstFiles);
                        }

                        if(ze.FileName.ToLower().EndsWith(".anim"))
                            dict[key].Add(ze.FileName.ToLower());
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            // Iterate the current gen zips and build a list of animations per dictionary
            Dictionary<string, List<string> > current_gen_dict = new Dictionary<string, List<string> >();
            Dictionary<string, List<string> > next_gen_dict = new Dictionary<string, List<string> >();

            AnalyseZipData(@"X:\gta5\assets\export\anim\ingame", current_gen_dict);
            AnalyseZipData(@"X:\gta5\assets_ng\export\anim\ingame", next_gen_dict);

            //AnalyseZipData(@"X:\gta5_dlc\spPacks\dlc_relationship\assets\export\anim\ingame", current_gen_dict);
            //AnalyseZipData(@"X:\gta5_dlc\spPacks\dlc_relationship\assets_ng\export\anim\ingame", next_gen_dict);

            //AnalyseZipData(@"X:\gta5_dlc\spPacks\dlc_agentTrevor\assets\export\anim\ingame", current_gen_dict);
            //AnalyseZipData(@"X:\gta5_dlc\spPacks\dlc_agentTrevor\assets_ng\export\anim\ingame", next_gen_dict);

            //AnalyseZipData(@"X:\gta5_dlc\mpPacks\mpBusiness\assets\export\anim\ingame", current_gen_dict);
            //AnalyseZipData(@"X:\gta5_dlc\mpPacks\mpBusiness\assets_ng\export\anim\ingame", next_gen_dict);

            //AnalyseZipData(@"X:\gta5_dlc\mpPacks\mpBusiness2\assets\export\anim\ingame", current_gen_dict);
            //AnalyseZipData(@"X:\gta5_dlc\mpPacks\mpBusiness2\assets_ng\export\anim\ingame", next_gen_dict);

            //AnalyseZipData(@"X:\gta5_dlc\mpPacks\mpPilot\assets\export\anim\ingame", current_gen_dict);
            //AnalyseZipData(@"X:\gta5_dlc\mpPacks\mpPilot\assets_ng\export\anim\ingame", next_gen_dict);

            Console.WriteLine("");
            Console.WriteLine("--existing dictionaries");
            Console.WriteLine("");

            foreach (KeyValuePair<string, List<string>> pair in next_gen_dict)
            {
                if (current_gen_dict.ContainsKey(pair.Key))
                {
                    List<string> missingAnims = new List<string>();
                    foreach (string file in next_gen_dict[pair.Key])
                    {
                        if (!current_gen_dict[pair.Key].Contains(file))
                        {
                            missingAnims.Add(file);
                        }
                    }

                    if (missingAnims.Count > 0)
                    {
                        Console.WriteLine("-{0}", pair.Key);
                        foreach (string file in missingAnims)
                        {
                            Console.WriteLine("{0}", file);
                        }
                        Console.WriteLine("");
                    }
                }
            }

            Console.WriteLine("\n--new dictionaries\n");
            Console.WriteLine("");

            foreach(KeyValuePair<string, List<string>> pair in next_gen_dict)
            {
                if (!current_gen_dict.ContainsKey(pair.Key))
                {
                    List<string> missingAnims = new List<string>();
                    foreach (string file in next_gen_dict[pair.Key])
                    {
                        missingAnims.Add(file);
                    }

                    if (missingAnims.Count > 0)
                    {
                        Console.WriteLine("-{0}", pair.Key);
                        foreach (string file in missingAnims)
                        {
                            Console.WriteLine("{0}", file);
                        }
                        Console.WriteLine("");
                    }
                }
            }

            // Compare
            //foreach (KeyValuePair<string, List<string>> pair in current_gen_dict)
            //{
            //    if (next_gen_dict.ContainsKey(pair.Key))
            //    {
            //        List<string> missingAnims = new List<string>();
            //        foreach (string file in next_gen_dict[pair.Key])
            //        {
            //            if (!current_gen_dict[pair.Key].Contains(file))
            //            {
            //                missingAnims.Add(file);
            //            }
            //        }

            //        if (missingAnims.Count > 0)
            //        {
            //            Console.WriteLine("-{0}", pair.Key);
            //            foreach (string file in missingAnims)
            //            {
            //                Console.WriteLine("{0}", file);
            //            }
            //            Console.WriteLine("");
            //        }
            //    }
            //    else
            //    {
            //        Console.WriteLine("{0} is missing", pair.Key);
            //        // Zip does not exist
            //    }
            //}
        }
    }
}
