// 
// exprtools/dumpexpressions.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "makeexpressionscore.h"

#include "cranimation/animation.h"
#include "crextra/expressions.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(expr, "Expression output file (Example out.expr)");
PARAM(verbose, "Verbose level (default 2)");

int Main()
{
	crAnimation::InitClass();
	crExpressions::InitClass();

	// process help parameter
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// dump expression file
	const char* exprFilename = NULL;
	PARAM_expr.Get(exprFilename);
	if(exprFilename && exprFilename[0])
	{
		crExpressions expressions;
		
		if(expressions.Load(exprFilename))
		{
			int verbosity = 2;
			PARAM_verbose.Get(verbosity);

			expressions.Dump(verbosity);
		}
		else
		{
			Quitf("ERROR - failed to load expression file '%s'", exprFilename);
		}
	}
	else
	{
		Quitf("ERROR - no expression data file specified");
	}

	// shutdown
	crExpressions::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}
