// 
// exprtools/makeexpressionscore.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#ifndef EXPRTOOLS_MAKEEXPRESSIONSCORE_H
#define EXPRTOOLS_MAKEEXPRESSIONSCORE_H

#include "crextra/expressionops.h"

namespace rage
{

class AeFile;
class crExpressions;


namespace MakeExpressions
{

// PURPOSE:
extern float g_SpecialCurveOptimizationTranslationThreshold;

// PURPOSE:
extern float g_SpecialCurveOptimizationRotationThreshold;

// PURPOSE:
extern float g_ConstantFloatOptimizationThreshold;

// PURPOSE:
extern float g_TranslationScale;

// PURPOSE:
extern bool g_RelativeTranslation;
extern bool g_RelativeScale;

// PURPOSE:
extern void OptimizeExpressionData(crExpressions& expressions, bool collapse, bool useRules);

// PURPOSE:
extern bool ConvertExpressionData(const AeFile& exprData, crExpressions& expressions);

// PURPOSE:
extern bool AddInputOutputValidation(crExpressions& expressions, bool input, bool output, bool all, float blendRate);

// PURPOSE:
extern void PackExpressionData(crExpressions& expressions, u32 maxPackSize);

// PURPOSE:
struct OptimizeIterator : public crExpressionOp::ModifyingIterator
{
	OptimizeIterator(bool useRules);
	virtual ~OptimizeIterator();

	struct OptimizeContext
	{
		OptimizeContext()
			: m_Radians(false)
		{
		}

		void Init(const OptimizeContext& oc)
		{
			m_Radians = oc.m_Radians;
		}

		bool m_Radians;
	};


	virtual crExpressionOp* Visit(crExpressionOp& op);

	crExpressionOp* Optimize(crExpressionOp& op, OptimizeContext& oc);

	bool m_Changed;
	bool m_UseRules;
	atArray<OptimizeContext> m_OptimizeContexts;
};


}; // namespace MakeExpressions

}; // namespace rage

#endif // EXPRTOOLS_MAKEEXPRESSIONSCORE_H


