// 
// exprtools/makeexpressionsrules.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EXPRTOOLS_MAKEEXPRESSIONSRULES_H
#define EXPRTOOLS_MAKEEXPRESSIONSRULES_H

#include "atl/array.h"

namespace rage {

class crExpressions;

namespace MakeExpressionsRules
{
	struct ExpressionOptimizerRule;

	void BuildOptimizerRuleList(atArray<ExpressionOptimizerRule*>& rules);
	void ReleaseOptimizerRuleList(atArray<ExpressionOptimizerRule*>& optimizerRules);

	void OptimizeIntraExpressionDataWithRules(crExpressions& expressions, atArray<ExpressionOptimizerRule*>& rules);

} // namespace MakeExpressionsRules

} // namespace rage

#endif // EXPRTOOLS_MAKEEXPRESSIONSRULES_H