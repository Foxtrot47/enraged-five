// 
// exprtools/applyexpressionscore.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#include "applyexpressionscore.h"

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"
#include "crextra/expressions.h"
#include "crskeleton/skeletondata.h"
#include "string/string.h"


namespace rage
{

namespace crApplyExpressionsCore
{

////////////////////////////////////////////////////////////////////////////////

bool ApplyExpressions(const crExpressions& expressions, const crAnimation& srcAnim, crAnimation& destAnim, const crSkeletonData* skelData, float sampleFreq)
{
	destAnim.Shutdown();

	const int numFrames = int((srcAnim.GetDuration() * sampleFreq) + 0.5f) + 1;

	crFrameData frameData;
	expressions.InitializeFrameData(frameData, true, true, false);

	atArray<crFrame> destFrames;
	destFrames.Resize(numFrames);

	atArray<const crFrame*> destFramePtrs;
	destFramePtrs.Resize(numFrames);

	for(int i=0; i<numFrames; ++i)
	{
		float time = float(i)/sampleFreq;

		crFrame& frame = destFrames[i];
		frame.Init(frameData);

		frame.Composite(srcAnim, time);
		expressions.Process(frame, time, 1.f/sampleFreq, NULL, skelData);

		destFramePtrs[i] = &frame;
	}

	destAnim.Create(numFrames, srcAnim.GetDuration(), srcAnim.IsLooped(), srcAnim.HasMoverTracks(), crAnimation::sm_DefaultFramesPerChunk, srcAnim.IsRaw());
	destAnim.CreateFromFramesFast(destFramePtrs, LOSSLESS_ANIM_TOLERANCE);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

}; // namespace crApplyExpressionsCore

}; // namespace rage

