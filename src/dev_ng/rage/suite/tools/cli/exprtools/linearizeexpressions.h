//
// exprtools/linearizeexpressions.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef EXPRTOOLS_LINEARIZEEXPRESSIONS_H
#define EXPRTOOLS_LINEARIZEEXPRESSIONS_H

#include "crextra/expressionops.h"

namespace rage 
{

// PURPOSE: Expression iterator to create special blend vector
// NOTES: Should be executed before all other optimization phases
class Linearizer : public crExpressionOp::ModifyingIterator
{
public:

	// PURPOSE: Default constructor
	Linearizer();

	// PURPOSE: Visit operation
	virtual crExpressionOp* Visit(crExpressionOp& op);
	
	// PURPOSE: Print statistics
	void PrintStats();

private:

	// PURPOSE: Variable used by functions
	struct Variable
	{
		Variable();
		bool operator!=(const Variable& other) const;
		bool IsValid() const;

		u16 m_Id;
		u8 m_Track, m_Type, m_Component;
		bool m_Valid;
	};

	// PURPOSE: Partial linear function, mapping [begin,end] => R
	// f(x) = mult*x + add
	struct PartialFunction
	{
		PartialFunction();
		bool IsConstant() const;

		float m_Begin, m_End;
		float m_Mult, m_Add;
	};

	typedef atArray<PartialFunction> PartialFunctions;

	 // PURPOSE: Linear scalar function, mapping R => R
	struct ScalarFunction
	{
		bool IsConstant() const;
		bool IsEmpty() const;

		Variable m_Variable;
		PartialFunctions m_Partials;
	};

	// PURPOSE: Linear vector function, mapping R^3 => R^3
	struct VectorFunction
	{
		static const u32 kNumComponents = 3;
		ScalarFunction m_Components[kNumComponents];
	};

	typedef atArray<VectorFunction> VectorFunctions;

	// PURPOSE: Build vector functions
	// RETURN: true - if conversion was successful
	static bool Build(const crExpressionOpSpecialBlend* op, VectorFunctions& results);

	// PURPOSE: Merge variables inside vector functions
	// RETURN: true - if conversion was successful
	static bool Merge(VectorFunctions& results);

	// PURPOSE: Create vector blend operation, mapping R => R^3
	// RETURN: pointer to created operation
	static crExpressionOpSpecialLinear* Create(crExpressionOpSpecialBlend::eBlendMode mode, VectorFunctions& results);

	// PURPOSE: Convert expression tree to a linear scalar function
	// RETURN: true - if conversion was successful
	static bool Convert(const crExpressionOp* op, ScalarFunction& result);
	static bool ConvertSpecialBlend(const crExpressionOpSpecialBlend* op, ScalarFunction& result);
	static bool ConvertGet(const crExpressionOpGet* op, ScalarFunction& result);
	static bool ConvertConstantFloat(const crExpressionOpConstantFloat* op, ScalarFunction& result);
	static bool ConvertNullary(const crExpressionOpNullary* op, ScalarFunction& result);
	static bool ConvertUnary(const crExpressionOpUnary* op, ScalarFunction& result);
	static bool ConvertBinary(const crExpressionOpBinary* op, ScalarFunction& result);
	static bool ConvertTernary(const crExpressionOpTernary* op, ScalarFunction& result);

	u32 m_Success, m_Total;
};

} // namespace rage

#endif // EXPRTOOLS_LINEARIZEEXPRESSIONS_H