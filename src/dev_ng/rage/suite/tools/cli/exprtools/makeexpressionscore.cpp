// 
// exprtools/makeexpressionscore.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "makeexpressionscore.h"

#include "linearizeexpressions.h"
#include "makeexpressionsrules.h"

#include "animExprData/animExprData.h"
#include "animExprData/exprTree.h"
#include "atl/array.h"
#include "crextra/expression.h"
#include "crextra/expressionopmacros.h"
#include "crextra/expressionops.h"
#include "crextra/expressions.h"


#define USE_NARY_LOGICAL_OPS (0)

namespace rage
{

namespace MakeExpressions
{

float g_SpecialCurveOptimizationTranslationThreshold = 0.001f;
float g_SpecialCurveOptimizationRotationThreshold = 0.00315f;
float g_ConstantFloatOptimizationThreshold = 0.0000005f;
float g_TranslationScale = 1.f;
bool g_RelativeTranslation = false;
bool g_RelativeScale = false;


bool GetTrackTypeAndComponent(const char* componentString, u8& track, u8& type, bool& all, u8& component)
{
	all = false;
	component = 0;

	if(!stricmp(componentString, "transX") || !stricmp(componentString, "translateX"))
	{
		track = kTrackBoneTranslation;
		type = kFormatTypeVector3;
		component = 0;
		return true;
	}
	else if(!stricmp(componentString, "transY") || !stricmp(componentString, "translateY"))
	{
		track = kTrackBoneTranslation;
		type = kFormatTypeVector3;
		component = 1;
		return true;
	}
	else if(!stricmp(componentString, "transZ") || !stricmp(componentString, "translateZ"))
	{
		track = kTrackBoneTranslation;
		type = kFormatTypeVector3;
		component = 2;
		return true;
	}
	else if(!stricmp(componentString, "trans") || !stricmp(componentString, "translate"))
	{
		track = kTrackBoneTranslation;
		type = kFormatTypeVector3;
		all = true;
		return true;
	}
	else if(!stricmp(componentString, "rotX") || !stricmp(componentString, "rotateX"))
	{
		track = kTrackBoneRotation;
		type = kFormatTypeQuaternion;
		component = 0;
		return true;
	}
	else if(!stricmp(componentString, "rotY") || !stricmp(componentString, "rotateY"))
	{
		track = kTrackBoneRotation;
		type = kFormatTypeQuaternion;
		component = 1;
		return true;
	}
	else if(!stricmp(componentString, "rotZ") || !stricmp(componentString, "rotateZ"))
	{
		track = kTrackBoneRotation;
		type = kFormatTypeQuaternion;
		component = 2;
		return true;
	}
	else if(!stricmp(componentString, "rot") || !stricmp(componentString, "rotate"))
	{
		track = kTrackBoneRotation;
		type = kFormatTypeQuaternion;
		all = true;
		return true;
	}
	else if(!stricmp(componentString, "scaleX"))
	{
		track = kTrackBoneScale;
		type = kFormatTypeVector3;
		component = 0;
		return true;
	}
	else if(!stricmp(componentString, "scaleY"))
	{
		track = kTrackBoneScale;
		type = kFormatTypeVector3;
		component = 1;
		return true;
	}
	else if(!stricmp(componentString, "scaleZ"))
	{
		track = kTrackBoneScale;
		type = kFormatTypeVector3;
		component = 2;
		return true;
	}
	else if(!stricmp(componentString, "scale"))
	{
		track = kTrackBoneScale;
		type = kFormatTypeVector3;
		all = true;
		return true;
	}
	else if(!stricmp(componentString, "float"))
	{
		track = kTrackGenericControl;
		type = kFormatTypeFloat;
		all = true;
		return true;
	}

	return false;
}

bool GetTrackType(u8 track, u8& type)
{
	switch(track)
	{
	case kTrackBoneTranslation:
		type = kFormatTypeVector3;
		return true;

	case kTrackBoneRotation:
		type = kFormatTypeQuaternion;
		return true;

	default:
		break;
	}

	return false;
}

crExpressionOp* ScaleExpression(crExpressionOp* op, float scaleFactor, bool vectorized=false)
{
	if(IsClose(scaleFactor, 1.f))
	{
		return op;
	}
	else
	{
		if(vectorized)
		{
			crExpressionOpConstant* opConstant = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
			opConstant->SetValue(Vec3V(scaleFactor, scaleFactor, scaleFactor));

			crExpressionOpBinary* opBinaryVectorMultiply = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
			opBinaryVectorMultiply->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeVectorMultiply);
			opBinaryVectorMultiply->SetSource(0, op);
			opBinaryVectorMultiply->SetSource(1, opConstant);

			return opBinaryVectorMultiply;
		}

		if(IsClose(scaleFactor, RtoD))
		{
			crExpressionOpUnary* opUnaryRtoD = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
			opUnaryRtoD->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees);
			opUnaryRtoD->SetSource(op);

			return opUnaryRtoD;
		}
		else if(IsClose(scaleFactor, DtoR))
		{
			crExpressionOpUnary* opUnaryDtoR = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
			opUnaryDtoR->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeDegreesToRadians);
			opUnaryDtoR->SetSource(op);

			return opUnaryDtoR;
		}
		else
		{
			crExpressionOpConstantFloat* opConstantFloat = static_cast<crExpressionOpConstantFloat*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstantFloat));
			opConstantFloat->SetFloat(scaleFactor);

			crExpressionOpBinary* opBinaryMultiply = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
			opBinaryMultiply->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMultiply);
			opBinaryMultiply->SetSource(0, op);
			opBinaryMultiply->SetSource(1, opConstantFloat);

			return opBinaryMultiply;
		}
	}
}

u8 ConvertTrackType(eComponentType componentType)
{
	switch(componentType)
	{
	case kFloat:
		return kFormatTypeFloat;
	case kVector:
		return kFormatTypeVector3;
	case kQuat:
		return kFormatTypeQuaternion;

	default:
		Warningf("WARNING: Unknown track format type %d, using float format type", int(componentType));
		return kFormatTypeFloat;
	}
}

u8 ConvertOrder(eComponentRotationOrder componentRotationOrder)
{
	switch(componentRotationOrder)
	{
	case kXYZ:
		return EULER_XYZ;
	case kYZX:
		return EULER_YZX;
	case kZXY:
		return EULER_ZXY;
	case kXZY:
		return EULER_XZY;
	case kYXZ:
		return EULER_YXZ;
	case kZYX:
		return EULER_ZYX;

	default:
		Warningf("WARNING: Unknown rotation order %d, using xyz", int(componentRotationOrder));
		return EULER_XYZ;
	}
}

u8 ConvertComponent(eComponentAxis componentAxis, bool allowNeg)
{
	switch(componentAxis)
	{
	case kAxisNone:
	case kXAxis:
		return crExpressionOpSpecialLookAt::kXAxis;
	case kYAxis:
		return crExpressionOpSpecialLookAt::kYAxis;
	case kZAxis:
		return crExpressionOpSpecialLookAt::kZAxis;
	case kNegXAxis:
		if(allowNeg)
		{
			return crExpressionOpSpecialLookAt::kXAxisNeg;
		}
		// break; // intentionally ommited
	case kNegYAxis:
		if(allowNeg)
		{
			return crExpressionOpSpecialLookAt::kYAxisNeg;
		}
		// break; // intentionally ommited
	case kNegZAxis:
		if(allowNeg)
		{
			return crExpressionOpSpecialLookAt::kZAxisNeg;
		}
		// break; // intentionally ommited
	case kAxisAll:
	default:
		Warningf("WARNING: Unknown/unexpected component axis %d, using x axis", int(componentAxis));
		return 0;
	}
}

crExpressionOp* ConvertInput(const AeDrivenControllerInput* input, bool os)
{
	AeDriver* aeDriver = input->m_pDriver;
	AeDriverComponent* aeDriverComp = input->m_pDriverComp;

	bool relative = (aeDriverComp->m_evalSpace == kZeroed);

	bool all = false;
	crExpressionOpGetSet* opGetSet = NULL;
	switch(aeDriverComp->m_axis)
	{
	case kAxisAll:
		all = true;
		if(os)
		{
			opGetSet = static_cast<crExpressionOpGetSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeObjectSpaceGet));
		}
		else
		{
			crExpressionOpGet* opGet = static_cast<crExpressionOpGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeGet));
			opGet->SetRelative(relative);
			opGetSet = opGet;
		}
		break;

	default:
		{
			crExpressionOpComponentGet* opComponentGet = static_cast<crExpressionOpComponentGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentGet));
			opComponentGet->SetComponent(ConvertComponent(aeDriverComp->m_axis, false));
			opComponentGet->SetOrder(ConvertOrder(aeDriverComp->m_rotOrder));
			opComponentGet->SetRelative(relative);
			opGetSet = opComponentGet;
		}
		break;
	}

	opGetSet->SetTrack(u8(aeDriverComp->m_trackId));
	opGetSet->SetType(ConvertTrackType(aeDriverComp->m_type));
	opGetSet->SetId(u16(aeDriver->m_boneId));

	float scale = input->m_unitConversionFactor;
	switch(opGetSet->GetTrack())
	{
	case kTrackBoneTranslation:
		if(g_RelativeTranslation)
		{
			opGetSet->SetRelative(true);
		}
		// break intentionally omitted
	case kTrackMoverTranslation:
		scale /= g_TranslationScale;
		break;

	case kTrackBoneScale:
		if(g_RelativeScale)
		{
			opGetSet->SetRelative(true);
		}
		break;

	default:
		break;
	}

	return ScaleExpression(opGetSet, scale, (opGetSet->GetType()!=kFormatTypeFloat)?all:false);
}

crExpressionOp* ConvertExpressionNode(const ExprNodeBase* node)
{
	switch(node->GetSuperType())
	{
	case ExprNodeBase::kOperand:
		{
			const ExprNodeOperand* nodeOperand = dynamic_cast<const ExprNodeOperand*>(node);
			if(nodeOperand)
			{
				switch(nodeOperand->GetOperandType())
				{
				case ExprNodeOperand::kConstant:
					{
						const ExprNodeConstant* nodeConstant = dynamic_cast<const ExprNodeConstant*>(nodeOperand);
						if(nodeConstant)
						{
							if(IsNearZero(nodeConstant->m_value))
							{
								crExpressionOpNullary* opNullary = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
								opNullary->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);
								return opNullary;
							}
							else if(IsClose(nodeConstant->m_value, 1.f))
							{
								crExpressionOpNullary* opNullary = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
								opNullary->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeOne);
								return opNullary;
							}
							else if(IsClose(nodeConstant->m_value, PI))
							{
								crExpressionOpNullary* opNullary = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
								opNullary->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypePi);
								return opNullary;
							}
							else
							{
								crExpressionOpConstantFloat* opConstantFloat = static_cast<crExpressionOpConstantFloat*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstantFloat));
								opConstantFloat->SetFloat(nodeConstant->m_value);
								return opConstantFloat;
							}
						}
					}
					break;

				case ExprNodeOperand::kNameAttrPair:
					{
						const ExprNodeNameAttrPair* nodeNameAttrPair = dynamic_cast<const ExprNodeNameAttrPair*>(nodeOperand);
						if(nodeNameAttrPair)
						{
							return ConvertInput(nodeNameAttrPair->m_pInput, false);
						}
					}
					break;

				case ExprNodeOperand::kTime:
					{
						const ExprNodeTime* nodeTime = dynamic_cast<const ExprNodeTime*>(node);
						if(nodeTime)
						{
							float units = 1.f;
							switch(nodeTime->m_units)
							{
							case ExprNodeTime::kFrame30:
								units = 30.f;
								break;
							case ExprNodeTime::kFrame60:
								units = 60.f;
								break;
							case ExprNodeTime::kSeconds:
							default:
								units = 1.f;
								break;
							}

							crExpressionOpNullary* opNullaryTime = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
							opNullaryTime->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeTime);
							return ScaleExpression(opNullaryTime, units, false);
						}
					}
					break;

				default:
					return NULL;
				}
			}
		}
		break;

	case ExprNodeBase::kUnary:
		{
			const ExprNodeUnaryOperator* nodeUnary = dynamic_cast<const ExprNodeUnaryOperator*>(node);
			if(nodeUnary)
			{
				crExpressionOp* opChild = ConvertExpressionNode(nodeUnary->GetChild(0));

				if(opChild)
				{
					crExpressionOpUnary* opUnary = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));

					bool radiansIn = false;
					bool radiansOut = false;

					switch(nodeUnary->GetOperatorType())
					{
					case ExprNodeUnaryOperator::kNegate:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeNegate);
						break;

					case ExprNodeUnaryOperator::kAbs:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeAbsolute);
						break;

					case ExprNodeUnaryOperator::kAcos:
					case ExprNodeUnaryOperator::kAcosD:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeArcCos);
						radiansOut = (nodeUnary->GetOperatorType() == ExprNodeUnaryOperator::kAcosD);
						break;

					case ExprNodeUnaryOperator::kAsin:
					case ExprNodeUnaryOperator::kAsinD:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeArcSin);
						radiansOut = (nodeUnary->GetOperatorType() == ExprNodeUnaryOperator::kAsinD);
						break;

					case ExprNodeUnaryOperator::kATan:
					case ExprNodeUnaryOperator::kAtanD:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeArcTan);
						radiansOut = (nodeUnary->GetOperatorType() == ExprNodeUnaryOperator::kAtanD);
						break;

					case ExprNodeUnaryOperator::kCeil:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeCeil);
						break;

					case ExprNodeUnaryOperator::kCos:
					case ExprNodeUnaryOperator::kCosD:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeCos);
						radiansIn = (nodeUnary->GetOperatorType() == ExprNodeUnaryOperator::kCosD);
						break;

					case ExprNodeUnaryOperator::kCosH:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeCosH);
						radiansIn = false;
						break;

					case ExprNodeUnaryOperator::kDToR:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeDegreesToRadians);
						break;

					case ExprNodeUnaryOperator::kExp:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeExp);
						break;

					case ExprNodeUnaryOperator::kFloor:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeFloor);
						break;

					case ExprNodeUnaryOperator::kLn:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeLn);
						break;

					case ExprNodeUnaryOperator::kLog:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeLog);
						break;

					case ExprNodeUnaryOperator::kRToD:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees);
						break;

					case ExprNodeUnaryOperator::kSin:
					case ExprNodeUnaryOperator::kSinD:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeSin);
						radiansIn = (nodeUnary->GetOperatorType() == ExprNodeUnaryOperator::kSinD);
						break;

					case ExprNodeUnaryOperator::kSinH:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeSinH);
						radiansIn = false;
						break;

					case ExprNodeUnaryOperator::kSqrt:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeSqrt);
						break;

					case ExprNodeUnaryOperator::kTan:
					case ExprNodeUnaryOperator::kTanD:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeTan);
						radiansIn = (nodeUnary->GetOperatorType() == ExprNodeUnaryOperator::kTanD);
						break;

					case ExprNodeUnaryOperator::kTanH:
						opUnary->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeTanH);
						radiansIn = false;
						break;

					default:
						return NULL;
					}

					if(radiansIn || radiansOut)
					{
						const float rotationScaleFactor = DtoR; //g_RotationScaleFactor;
						opUnary->SetSource(ScaleExpression(opChild, radiansIn ? rotationScaleFactor : 1.f, false));
						return ScaleExpression(opUnary, radiansOut ? (1.f/rotationScaleFactor) : 1.f, false);
					}

					opUnary->SetSource(opChild);
					return opUnary;
				}
			}
		}
		break;

	case ExprNodeBase::kBinary:
		{
			const ExprNodeBinaryOperator* nodeBinary = dynamic_cast<const ExprNodeBinaryOperator*>(node);
			if(nodeBinary)
			{
				if(nodeBinary->GetOperatorType() == ExprNodeBinaryOperator::kAssign)
				{
					crExpressionOp* opChild = ConvertExpressionNode(nodeBinary->GetChild(1));

					// ASSUMPTION - left side of assignment ultimately same as final assignment
					// intermediate assignments not supported
					// left hand side of assignments are ignored - right side treated like value
					return opChild;
				}

				crExpressionOp* opChild[2];
				opChild[0] = ConvertExpressionNode(nodeBinary->GetChild(0));
				opChild[1] = ConvertExpressionNode(nodeBinary->GetChild(1));

				if(opChild[0] && opChild[1])
				{
					crExpressionOpBinary* opBinary = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));

					switch(nodeBinary->GetOperatorType())
					{
					case ExprNodeBinaryOperator::kAdd:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeAdd);
						break;

					case ExprNodeBinaryOperator::kSubtract:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeSubtract);
						break;

					case ExprNodeBinaryOperator::kMultiply:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMultiply);
						break;

					case ExprNodeBinaryOperator::kDivide:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeDivide);
						break;

					case ExprNodeBinaryOperator::kGreaterThan:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeGreaterThan);
						break;

					case ExprNodeBinaryOperator::kGreaterThanEqualTo:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeGreaterThanEqualTo);
						break;

					case ExprNodeBinaryOperator::kLessThan:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeLessThan);
						break;

					case ExprNodeBinaryOperator::kLessThanEqualTo:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeLessThanEqualTo);
						break;

					case ExprNodeBinaryOperator::kEqualTo:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeEqualTo);
						break;

					case ExprNodeBinaryOperator::kNotEqualTo:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeNotEqualTo);
						break;

					case ExprNodeBinaryOperator::kIfThen:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeLogicalAnd);
						break;

					case ExprNodeBinaryOperator::kMax:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMax);
						break;

					case ExprNodeBinaryOperator::kMin:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMin);
						break;

					case ExprNodeBinaryOperator::kMod:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeModulus);
						break;

					case ExprNodeBinaryOperator::kPow:
						opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeExponent);
						break;

					default:
						return NULL;
					}

					opBinary->SetSource(0, opChild[0]);
					opBinary->SetSource(1, opChild[1]);

					return opBinary;
				}
			}	
		}
		break;

	case ExprNodeBase::kTernary:
		{
			const ExprNodeTernaryOperator* nodeTernary = dynamic_cast<const ExprNodeTernaryOperator*>(node);
			if(nodeTernary)
			{
				crExpressionOp* opChild[3];
				opChild[0] = ConvertExpressionNode(nodeTernary->GetChild(0));
				opChild[1] = ConvertExpressionNode(nodeTernary->GetChild(1));
				opChild[2] = ConvertExpressionNode(nodeTernary->GetChild(2));

				if(opChild[0] && opChild[1] && opChild[2])
				{
					crExpressionOpTernary* opTernary = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));

					bool exchange = false;

					switch(nodeTernary->GetOperatorType())
					{
					case ExprNodeTernaryOperator::kIfThenElse:
						opTernary->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeConditional);
						break;

					case ExprNodeTernaryOperator::kClamp:
						opTernary->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeClamp);
						exchange = true;
						break;

					default:
						return NULL;
					}

					if(exchange)
					{
						opTernary->SetSource(0, opChild[2]);
						opTernary->SetSource(1, opChild[0]);
						opTernary->SetSource(2, opChild[1]);
					}
					else
					{
						opTernary->SetSource(0, opChild[0]);
						opTernary->SetSource(1, opChild[1]);
						opTernary->SetSource(2, opChild[2]);
					}

					return opTernary;
				}
			}
		}
		break;

	default:
		return NULL;
	}

	return NULL;
}

crExpressionOp* ConvertExpressionController(const AeController& aeController)
{
	const AeFloatConstantController* aeFloatConstantController = dynamic_cast<const AeFloatConstantController*>(&aeController);
	if(aeFloatConstantController)
	{
		crExpressionOpConstantFloat* opConstantFloat = static_cast<crExpressionOpConstantFloat*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstantFloat));
		opConstantFloat->SetFloat(aeFloatConstantController->m_value);
		return opConstantFloat;
	}

	const AeExpressionController* aeExpressionController = dynamic_cast<const AeExpressionController*>(&aeController);
	if(aeExpressionController)
	{
		return ScaleExpression(ConvertExpressionNode(aeExpressionController->m_pExprTree->m_pRoot), aeExpressionController->m_outputUnitConversionFactor);
	}

	const AeLookAtController* aeLookAtController = dynamic_cast<const AeLookAtController*>(&aeController);
	if(aeLookAtController)
	{
		crExpressionOpSpecialLookAt* opSpecialLookAt = static_cast<crExpressionOpSpecialLookAt*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSpecialLookAt));
		opSpecialLookAt->SetLookAtPosSource(ConvertExpressionController(*aeLookAtController->m_pBlendController));
		if(aeLookAtController->m_pUpnodeOSController)
		{
			opSpecialLookAt->SetLookAtRotSource(ConvertExpressionController(*aeLookAtController->m_pUpnodeOSController));
		}
		else
		{
			// TODO - questionable, really identity in object space, not world space
			crExpressionOpConstant* opConstantQuatIdentity = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
			opConstantQuatIdentity->GetValue() = QuatV(V_IDENTITY);
			opSpecialLookAt->SetLookAtRotSource(opConstantQuatIdentity);
		}
		opSpecialLookAt->SetOriginPosSource(ConvertExpressionController(*aeLookAtController->m_pBoneOSController));
		opSpecialLookAt->SetOffset(QuatV(aeLookAtController->m_offset.GetIntrin128()));
		opSpecialLookAt->SetLookAtAxis(ConvertComponent(aeLookAtController->m_boneAxis, true));
		opSpecialLookAt->SetLookAtUpAxis(ConvertComponent(aeLookAtController->m_upnodeUpAxis, false));
		opSpecialLookAt->SetOriginUpAxis(ConvertComponent(aeLookAtController->m_boneUpAxis, true));
		
		crExpressionOpObjectSpaceConvertFrom* opOsConvertFrom = static_cast<crExpressionOpObjectSpaceConvertFrom*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeObjectSpaceConvertFrom));
		opOsConvertFrom->SetTrack(kTrackBoneRotation);
		opOsConvertFrom->SetId(u16(aeLookAtController->m_pBoneOSController->GetInput(0)->m_pDriver->m_boneId));
		opOsConvertFrom->SetType(kFormatTypeQuaternion);
		opOsConvertFrom->SetSource(opSpecialLookAt);		

		return opOsConvertFrom;
	}

	const AeXYZController* aeXyzController = dynamic_cast<const AeXYZController*>(&aeController);
	if(aeXyzController)
	{
		bool isQuaternion = (aeXyzController->m_type==kQuat);

		crExpressionOpTernary* opTernaryTo = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		opTernaryTo->SetTernaryOpType(isQuaternion ? crExpressionOpTernary::kTernaryOpTypeToQuaternion : crExpressionOpTernary::kTernaryOpTypeToVector);

		for(int i=0; i<3; ++i)
		{
			AeController* ae = NULL;
			switch(i)
			{
			case 0: 
				ae = aeXyzController->m_pXController;
				break;
			case 1: 
				ae = aeXyzController->m_pYController;
				break;
			case 2: 
				ae = aeXyzController->m_pZController;
				break;
			}

			if(ae)
			{
				crExpressionOp* op = ConvertExpressionController(*ae);
				opTernaryTo->SetSource(i, op);
			}
			else
			{
				crExpressionOpNullary* opZero = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
				opZero->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);

				opTernaryTo->SetSource(i, opZero);
			}
		}

		return opTernaryTo;
	}

	const AeBlendController* aeBlendController = dynamic_cast<const AeBlendController*>(&aeController);
	if(aeBlendController)
	{
		const int numBlend = aeBlendController->m_inputs.size();

		crExpressionOpSpecialBlend* opSpecialBlend = static_cast<crExpressionOpSpecialBlend*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSpecialBlend));
		opSpecialBlend->SetNumSourceWeights(numBlend);

		for(int i=0; i<numBlend; ++i)
		{
			const AeBlendControllerItem& aeBlendItem = *aeBlendController->m_inputs[i];
			opSpecialBlend->SetWeight(i, aeBlendItem.m_weight);
			opSpecialBlend->SetSource(i, ConvertExpressionController(*aeBlendItem.m_pController));
			if(!opSpecialBlend->GetSource(i))
			{
				delete opSpecialBlend;
				return NULL;
			}
		}

		switch(aeBlendController->m_type)
		{
		case kVector:
			opSpecialBlend->SetBlendMode(crExpressionOpSpecialBlend::kBlendModeVector);
			break;

		case kQuat:
			opSpecialBlend->SetBlendMode(crExpressionOpSpecialBlend::kBlendModeQuaternion);
			break;

		default:
			opSpecialBlend->SetBlendMode(crExpressionOpSpecialBlend::kBlendModeFloat);
			break;
		}

		const AeObjectSpaceBlendController* aeOsBlendController = dynamic_cast<const AeObjectSpaceBlendController*>(&aeController);
		if(aeOsBlendController)
		{
			crExpressionOpObjectSpaceConvertFrom* opOsConvertFrom = static_cast<crExpressionOpObjectSpaceConvertFrom*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeObjectSpaceConvertFrom));
			opOsConvertFrom->SetTrack(u8(aeOsBlendController->m_trackId));
			opOsConvertFrom->SetId(u16(aeOsBlendController->m_hashId));

			u8 type;
			if(!GetTrackType(u8(aeOsBlendController->m_trackId), type))
			{
				delete opSpecialBlend;
				return NULL;
			}

			opOsConvertFrom->SetType(type);
			opOsConvertFrom->SetSource(opSpecialBlend);

			return opOsConvertFrom;
		}

		return opSpecialBlend;
	}
	else
	{
		const AeDrivenController* aeDrivenController = dynamic_cast<const AeDrivenController*>(&aeController);
		if(aeDrivenController)
		{
			crExpressionOp* op;

			if(aeDrivenController->GetInputCount())
			{
				bool os = false;
				const AeObjectSpaceController* aeObjectSpaceController  = dynamic_cast<const AeObjectSpaceController*>(&aeController);
				if(aeObjectSpaceController)
				{
					os = true;
				}

				op = ConvertInput(aeDrivenController->GetInput(0), os);
			}
			else
			{
				crExpressionOpNullary* opZero = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
				opZero->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);

				op = opZero;
			}

			const AeFloatCurveController* aeFloatCurveController = dynamic_cast<const AeFloatCurveController*>(&aeController);
			if(aeFloatCurveController)
			{
				const int numKeys = aeFloatCurveController->m_keys.size();

				crExpressionOpSpecialCurve* opSpecialCurve = static_cast<crExpressionOpSpecialCurve*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSpecialCurve));
				opSpecialCurve->SetNumKeys(numKeys);

				for(int i=0; i<numKeys; ++i)
				{
					const AeCurveKey& aeKey = aeFloatCurveController->m_keys[i];
					opSpecialCurve->SetKey(i, aeKey.m_in, aeKey.m_out);
				}

				opSpecialCurve->SetSource(op);

				return opSpecialCurve;
			}
			else
			{
				const AeDirectController* aeDirectController = dynamic_cast<const AeDirectController*>(aeDrivenController);
				if(aeDirectController)
				{
					return ScaleExpression(op, aeDirectController->m_scale, false);
				}
				else
				{
					return op;
				}
			}
		}
	}

	return NULL;
}

bool ConvertExpressionData(const AeFile& exprData, crExpressions& expressions)
{
	const int numAeBones = exprData.m_boneLibrary.size();
	for(int i=0; i<numAeBones; ++i)
	{
		const AeBone& aeBone = *exprData.m_boneLibrary[i];
		u16 id = u16(aeBone.m_hashId); 

		const int numComponents = aeBone.m_transform.size();
		for(int j=0; j<numComponents; ++j)
		{
			const AeBoneTransformComponent& aeBoneTransformComponent = *aeBone.m_transform[j];
			bool all;
			u8 track, type, component;
			if(!GetTrackTypeAndComponent(aeBoneTransformComponent.m_id.c_str(), track, type, all, component))
			{
				return false;
			}

			if(aeBoneTransformComponent.m_trackId >= 0)
			{
				track = (u8)aeBoneTransformComponent.m_trackId;
			}

			crExpressionOp* op = ConvertExpressionController(*aeBoneTransformComponent.m_pController);
			if(!op)
			{
				return false;
			}

			float scale = aeBoneTransformComponent.m_unitConversionFactor;
			switch(track)
			{
			case kTrackBoneTranslation:
			case kTrackMoverTranslation:
				scale *= g_TranslationScale;
				break;

			default:
				break;
			}

			bool vectorized = (type != kFormatTypeFloat) && all;
			op = ScaleExpression(op, scale, vectorized);

			crExpressionOpGetSet* opGetSet;
			if(all)
			{
				crExpressionOpSet* opSet = static_cast<crExpressionOpSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSet));
				opSet->SetSource(op);
				opGetSet = opSet;
			}
			else
			{
				crExpressionOpComponentSet* opComponentSet = static_cast<crExpressionOpComponentSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentSet));
				opComponentSet->SetComponent(component);
				opComponentSet->SetSource(op);
				opGetSet = opComponentSet;
			}

			opGetSet->SetTrack(track);
			opGetSet->SetType(type);
			opGetSet->SetId(id);

			switch(track)
			{
			case kTrackBoneTranslation:
				// TODO --- could use actual parent offset supplied, rather than just assuming non-zero implies relative
				if(g_RelativeTranslation || !IsZeroAll(aeBone.m_vParentOffset))
				{
					opGetSet->SetRelative(true);
				}
				break;

			case kTrackBoneRotation:
				if(!IsZeroAll(aeBone.m_vParentOrient))
				{
					opGetSet->SetRelative(true);
				}
				break;

			case kTrackBoneScale:
				if(g_RelativeScale || !IsZeroAll(aeBone.m_vParentScale))
				{
					opGetSet->SetRelative(true);
				}
				break;

			default:
				break;
			}

			crExpression expr;
			expr.SetExpressionOp(opGetSet);
			
			expressions.AppendExpression(expr);
		}
	}

	const int numAeMeshes = exprData.m_meshLibrary.size();
	for(int i=0; i<numAeMeshes; ++i)
	{
		const AeMesh& aeMesh = *exprData.m_meshLibrary[i];

		const int numBlendTargets = aeMesh.m_blendTargets.size();
		for(int j=0; j<numBlendTargets; ++j)
		{
			const AeMeshBlendTarget& aeBlendTarget = *aeMesh.m_blendTargets[j];
			u16 id = u16(aeBlendTarget.m_hashId);

			crExpressionOp* op = ConvertExpressionController(*aeBlendTarget.m_pController);
			if(!op)
			{
				return false;
			}

			op = ScaleExpression(op, g_TranslationScale, false);

			crExpressionOpSet* opSet = static_cast<crExpressionOpSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSet));
			opSet->SetTrack(kTrackBlendShape);
			opSet->SetType(kFormatTypeFloat);
			opSet->SetId(id);
			opSet->SetSource(op);

			crExpression expr;
			expr.SetExpressionOp(opSet);

			expressions.AppendExpression(expr);
		}

		const int numAnimNorms = aeMesh.m_animNrmMults.size();
		for(int j=0; j<numAnimNorms; ++j)
		{
			const AeMeshAnimNormalMapMult& aeAnimNorm = *aeMesh.m_animNrmMults[j];
			u16 id = u16(aeAnimNorm.m_hashId);

			crExpressionOp* op = ConvertExpressionController(*aeAnimNorm.m_pController);
			if(!op)
			{
				return false;
			}

			crExpressionOpSet* opSet = static_cast<crExpressionOpSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSet));
			opSet->SetTrack(kTrackAnimatedNormalMaps);
			opSet->SetType(kFormatTypeFloat);
			opSet->SetId(id);
			opSet->SetSource(op);

			crExpression expr;
			expr.SetExpressionOp(opSet);

			expressions.AppendExpression(expr);
		}
	}

	const int numAeMotions = exprData.m_motionLibrary.size();
	for(int i=0; i<numAeMotions; ++i)
	{
		const AeMotion& aeMotion = *exprData.m_motionLibrary[i];
		crExpressionOpMotion* op = static_cast<crExpressionOpMotion*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeMotion));
		crMotionDescription& descr = op->m_Descr;
		descr.m_Linear.m_Strength = aeMotion.m_strengthLinear;
		descr.m_Linear.m_Damping = aeMotion.m_dampingLinear;
		descr.m_Linear.m_MinConstraint = aeMotion.m_minConstraintLinear;
		descr.m_Linear.m_MaxConstraint = aeMotion.m_maxConstraintLinear;
		descr.m_Angular.m_Strength = aeMotion.m_strengthAngular;
		descr.m_Angular.m_Damping = aeMotion.m_dampingAngular;
		descr.m_Angular.m_MinConstraint = aeMotion.m_minConstraintAngular;
		descr.m_Angular.m_MaxConstraint = aeMotion.m_maxConstraintAngular;
		descr.m_Gravity.SetXf(aeMotion.m_gravity.GetXf());
		descr.m_Gravity.SetYf(aeMotion.m_gravity.GetYf());
		descr.m_Gravity.SetZf(aeMotion.m_gravity.GetZf());
		descr.m_Gravity.SetWi(aeMotion.m_boneId);
		descr.m_Direction = aeMotion.m_direction;

		crExpression expr;
		expr.SetExpressionOp(op);

		expressions.AppendExpression(expr);
	}

	return true;
}

struct InputOutputValidationIterator : public crExpressionOp::InputOutputIterator<true, true>
{
	InputOutputValidationIterator(bool input, bool output, bool all, float blendRate)
		: m_ValidateInputs(input)
		, m_ValidateOutput(output)
		, m_ValidateAllInputs(all)
		, m_ValidateBlendRate(blendRate)
	{
	}

	virtual ~InputOutputValidationIterator()
	{
	}

	virtual bool Callback(u8 track, u16 id, u8 type, bool isInput)
	{
		if(isInput)
		{
			for(int i=0; i<m_Inputs.GetCount(); ++i)
			{
				if(m_Inputs[i].m_Track == track && m_Inputs[i].m_Id == id)
				{
					return false;
				}
			}

			InputOutput& input = m_Inputs.Grow();
			input.m_Track = track;
			input.m_Id = id;
			input.m_Type = type;
		}
		else
		{
			m_Output.m_Track = track;
			m_Output.m_Id = id;
			m_Output.m_Type = type;
		}

		return false;
	}

	crExpressionOp* AddValidation(crExpressionOp& op, u32 idx)
	{
		crExpressionOp* validOp = NULL;
		if(m_ValidateOutput)
		{
			validOp = crMakeExpressionOp::Unary(crExpressionOpUnary::kUnaryOpTypeNegate, crMakeExpressionOp::Valid(m_Output.m_Track, m_Output.m_Id, m_Output.m_Type));
		}

		if(m_ValidateInputs && m_Inputs.GetCount() > 0)
		{
#if USE_NARY_LOGICAL_OPS
			crExpressionOp* inOp = NULL;
			if(m_Inputs.GetCount() > 1)
			{
				crExpressionOpNary* naryOp = rage_new crExpressionOpNary();
				naryOp->SetNaryOpType(m_ValidateAllInputs?crExpressionOpNary::kNaryOpTypeLogicalAnd:crExpressionOpNary::kNaryOpTypeLogicalOr);
				for(int i=0; i<m_Inputs.GetCount(); ++i)
				{
					naryOp->AppendSource(crMakeExpressionOp::Valid(m_Inputs[i].m_Track, m_Inputs[i].m_Id, m_Inputs[i].m_Type));
				}
				inOp = naryOp;
			}
			else
			{
				inOp = crMakeExpressionOp::Valid(m_Inputs[0].m_Track, m_Inputs[0].m_Id, m_Inputs[0].m_Type);
			}
#else // USE_NARY_LOGICAL_OPS
			atArray<crExpressionOp*> ops;
			for(int i=0; i<m_Inputs.GetCount(); ++i)
			{
				ops.Grow() = crMakeExpressionOp::Valid(m_Inputs[i].m_Track, m_Inputs[i].m_Id, m_Inputs[i].m_Type);
			}

			while(ops.GetCount() > 1)
			{
				for(int i=0; i<(ops.GetCount()-1); ++i)
				{
					crExpressionOp* binaryOp = crMakeExpressionOp::Binary(
						m_ValidateAllInputs?crExpressionOpBinary::kBinaryOpTypeLogicalAnd:crExpressionOpBinary::kBinaryOpTypeLogicalOr, 
						ops[i],	ops[i+1]);
					
					ops.Delete(i+1);
					ops[i] = binaryOp;
				}
			}
			crExpressionOp* inOp = ops[0];
#endif // USE_NARY_LOGICAL_OPS

			if(validOp)
			{
				validOp = crMakeExpressionOp::Binary(
					crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
					inOp,
					validOp);
			}
			else
			{
				validOp = inOp;
			}
		}

		if(validOp)
		{
			if(m_ValidateBlendRate > 0.f)
			{
				char varName[16];
				formatf(varName, "blend%d", idx);
				u32 varHash = atStringHash(varName);  

				crExpressionOp* setVarOp = crMakeExpressionOp::SetVariable(
					varHash,
					crMakeExpressionOp::Unary(
						crExpressionOpUnary::kUnaryOpTypeClamp01,
						crMakeExpressionOp::Binary(
							crExpressionOpBinary::kBinaryOpTypeAdd,
							crMakeExpressionOp::Binary(
								crExpressionOpBinary::kBinaryOpTypeMultiply,
								crMakeExpressionOp::Ternary(
									crExpressionOpTernary::kTernaryOpTypeConditional,
									validOp,
									crMakeExpressionOp::ConstantFloat(m_ValidateBlendRate),
									crMakeExpressionOp::ConstantFloat(-m_ValidateBlendRate)
								),
								crMakeExpressionOp::Nullary(crExpressionOpNullary::kNullaryOpTypeDeltaTime)
							),
							crMakeExpressionOp::GetVariable(varHash)
						)
					)
				);

				crExpressionOp* lerpOp = op.Clone();
				switch(lerpOp->GetOpType())
				{
				case crExpressionOp::kOpTypeSet:
				case crExpressionOp::kOpTypeComponentSet:
					{
						bool component = (lerpOp->GetOpType() == crExpressionOp::kOpTypeComponentSet);
						bool quaternion = (m_Output.m_Type == kFormatTypeQuaternion) && !component;
	
						lerpOp->SetChild(0, *crMakeExpressionOp::Ternary(
							quaternion?crExpressionOpTernary::kTernaryOpTypeQuaternionLerp:crExpressionOpTernary::kTernaryOpTypeLerp,
							crMakeExpressionOp::GetVariable(varHash),
							crMakeExpressionOp::Ternary(
								crExpressionOpTernary::kTernaryOpTypeConditional,
								crMakeExpressionOp::Valid(m_Output.m_Track, m_Output.m_Id, m_Output.m_Type),
								component?
									crMakeExpressionOp::ComponentGet(
										m_Output.m_Track, m_Output.m_Id, m_Output.m_Type, 
										static_cast<crExpressionOpComponentGet*>(lerpOp)->GetComponent(),
										u8(static_cast<crExpressionOpComponentGet*>(lerpOp)->GetOrder())):
									crMakeExpressionOp::Get(m_Output.m_Track, m_Output.m_Id, m_Output.m_Type),
								crMakeExpressionOp::Nullary(quaternion?crExpressionOpNullary::kNullaryOpTypeQuatIdentity:crExpressionOpNullary::kNullaryOpTypeZero)
							),
							lerpOp->GetChild(0)
						));
					}
					break;
				default:
					break;
				}

				return crMakeExpressionOp::Nary(
					crExpressionOpNary::kNaryOpTypeList,
					setVarOp,
					crMakeExpressionOp::Binary(
						crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
						crMakeExpressionOp::Binary(
							crExpressionOpBinary::kBinaryOpTypeGreaterThan,
							crMakeExpressionOp::GetVariable(varHash),
							crMakeExpressionOp::ConstantFloat(0.f)
						),
						lerpOp
					)
				);
			}
			else
			{
				return crMakeExpressionOp::Binary(
					crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
					validOp,
					op.Clone());
			}
		}
		else
		{
			return op.Clone();
		}
	}

	struct InputOutput
	{
		u8 m_Track;
		u16 m_Id;
		u8 m_Type;
	};

	atArray<InputOutput> m_Inputs;
	InputOutput m_Output;

	bool m_ValidateInputs;
	bool m_ValidateOutput;
	bool m_ValidateAllInputs;
	float m_ValidateBlendRate;
};

bool AddInputOutputValidation(crExpressions& expressions, bool input, bool output, bool all, float blendRate)
{
	for(u32 i=0; i<u32(expressions.GetNumExpressions()); ++i)
	{
		crExpressionOp* op = expressions.GetExpression(i)->GetExpressionOp();
		if(op)
		{
			InputOutputValidationIterator it(input, output, all, blendRate);
			it.Visit(*op);
			op = it.AddValidation(*op, i);
			expressions.GetExpression(i)->SetExpressionOp(op);
		}
	}

	return true;
}

bool IsConstantFloatExpressionOp(crExpressionOp& op, float& outValue)
{
	// TODO --- modify to perform deeper search - arithmetic, trig etc etc

	switch(op.GetOpType())
	{
	case crExpressionOp::kOpTypeConstantFloat:
		{
			crExpressionOpConstantFloat& opConstantFloat = static_cast<crExpressionOpConstantFloat&>(op);
			outValue = opConstantFloat.GetFloat();
			return true;
		}

	case crExpressionOp::kOpTypeNullary:
		{
			crExpressionOpNullary& opNullary = static_cast<crExpressionOpNullary&>(op);
			switch(opNullary.GetNullaryOpType())
			{
			case crExpressionOpNullary::kNullaryOpTypeZero:
				outValue = 0.f;
				return true;

			case crExpressionOpNullary::kNullaryOpTypeOne:
				outValue = 1.f;
				return true;

			case crExpressionOpNullary::kNullaryOpTypePi:
				outValue = PI;
				return true;

			default:
				break;
			}
		}

	default:
		break;
	}

	return false;
}

bool IsConstantQuaternionExpressionOp(crExpressionOp& op, QuatV_InOut outQuaternion)
{
	switch(op.GetOpType())
	{
	case crExpressionOp::kOpTypeConstant:
		{
			crExpressionOpConstant& opConstant = static_cast<crExpressionOpConstant&>(op);
			outQuaternion = opConstant.GetValue().GetQuaternion();
			return true;
		}
		
	default:
		break;
	}

	return false;
}

crExpressionOp* VectorizeExpressionOps(crExpressionOp& op0, crExpressionOp& op1, crExpressionOp& op2)
{
	crExpressionOpTernary* opTernaryToVector = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
	opTernaryToVector->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeToVector);

	opTernaryToVector->SetSource(0, op0.Clone());
	opTernaryToVector->SetSource(1, op1.Clone());
	opTernaryToVector->SetSource(2, op2.Clone());

	return opTernaryToVector;
}

OptimizeIterator::OptimizeIterator(bool useRules)
	: m_Changed(false)
	, m_UseRules(useRules)
{
}

OptimizeIterator::~OptimizeIterator()
{
}

crExpressionOp* OptimizeIterator::Visit(crExpressionOp& op)
{
	OptimizeContext& oc = m_OptimizeContexts.Grow();
	if(m_OptimizeContexts.GetCount() > 1)
	{
		oc.Init(m_OptimizeContexts[m_OptimizeContexts.GetCount()-2]);
	}
		
	crExpressionOp* newOp = Optimize(op, oc);
	if(newOp != &op)
	{
		m_Changed = true;
	}

	crExpressionOp* retOp = crExpressionOp::ModifyingIterator::Visit(*newOp);

	m_OptimizeContexts.Pop();

	return retOp;
}

crExpressionOp* OptimizeIterator::Optimize(crExpressionOp& op, OptimizeContext& oc)
{
	if (!m_UseRules)
	{
		switch(op.GetOpType())
		{
		case crExpressionOp::kOpTypeSet:
			{
				crExpressionOpSet& opSet = static_cast<crExpressionOpSet&>(op);
				oc.m_Radians = (opSet.GetType() == kFormatTypeQuaternion);
			}
			break;

		case crExpressionOp::kOpTypeComponentSet:
			{
				crExpressionOpComponentSet& opComponentSet = static_cast<crExpressionOpComponentSet&>(op);
				if(opComponentSet.GetType() == kFormatTypeFloat)
				{
					crExpressionOpSet* opSet = static_cast<crExpressionOpSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSet));
					opSet->SetTrack(opComponentSet.GetTrack());
					opSet->SetId(opComponentSet.GetId());
					opSet->SetType(opComponentSet.GetType());
					opSet->SetSource(opComponentSet.GetSource()->Clone());

					delete &op;
					return opSet;
				}
				
				oc.m_Radians = (opComponentSet.GetType() == kFormatTypeQuaternion);
			}
			break;

		case crExpressionOp::kOpTypeComponentGet:
			{
				crExpressionOpComponentGet& opComponentGet = static_cast<crExpressionOpComponentGet&>(op);
				if(opComponentGet.GetType() == kFormatTypeFloat)
				{
					crExpressionOpGet* opGet = static_cast<crExpressionOpGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeGet));
					opGet->SetTrack(opComponentGet.GetTrack());
					opGet->SetId(opComponentGet.GetId());
					opGet->SetType(opComponentGet.GetType());

					delete &op;
					return opGet;
				}
			}
			break;

		case crExpressionOp::kOpTypeConstantFloat:
			{
				crExpressionOpConstantFloat& opConstantFloat = static_cast<crExpressionOpConstantFloat&>(op);
				float val = opConstantFloat.GetFloat();
				if(IsClose(val, 0.f, g_ConstantFloatOptimizationThreshold))
				{
					crExpressionOpNullary* opZero = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
					opZero->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);

					delete &op;
					return opZero;
				}
				else if(IsClose(val, 1.f, g_ConstantFloatOptimizationThreshold))
				{
					crExpressionOpNullary* opOne = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
					opOne->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeOne);

					delete &op;
					return opOne;
				}
				else if(IsClose(val, PI, g_ConstantFloatOptimizationThreshold))
				{
					crExpressionOpNullary* opPi = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
					opPi->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypePi);

					delete &op;
					return opPi;
				}
			}
			break;

		case crExpressionOp::kOpTypeUnary:
			{
				crExpressionOpUnary& opUnary = static_cast<crExpressionOpUnary&>(op);
				switch(opUnary.GetUnaryOpType())
				{
				case crExpressionOpUnary::kUnaryOpTypeDegreesToRadians:
				case crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees:
					{
						crExpressionOp* opSource = opUnary.GetSource();
						if(opSource->GetOpType() == crExpressionOp::kOpTypeUnary)
						{
							crExpressionOpUnary* opSourceUnary = static_cast<crExpressionOpUnary*>(opSource);

							crExpressionOpUnary::eUnaryOpTypes unaryType = opUnary.GetUnaryOpType();
							crExpressionOpUnary::eUnaryOpTypes sourceUnaryType = opSourceUnary->GetUnaryOpType();

							if((unaryType == crExpressionOpUnary::kUnaryOpTypeDegreesToRadians && sourceUnaryType == crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees) ||
								(unaryType == crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees && sourceUnaryType == crExpressionOpUnary::kUnaryOpTypeDegreesToRadians))
							{
								crExpressionOp* opSourceSource = opSourceUnary->GetSource()->Clone();
								delete &op;
								return opSourceSource;
							}
						}
					}
					break;

					// TODO - NEGATE NEGATE, REMOVE

				default:
					break;
				}
			}
			break;

		case crExpressionOp::kOpTypeBinary: 
			{
				crExpressionOpBinary& opBinary = static_cast<crExpressionOpBinary&>(op);
				switch(opBinary.GetBinaryOpType())
				{
				case crExpressionOpBinary::kBinaryOpTypeDivide:
					{
						crExpressionOp* opSource0 = opBinary.GetSource(0);
						crExpressionOp* opSource1 = opBinary.GetSource(1);

						float val0;
						if(IsConstantFloatExpressionOp(*opSource0, val0))
						{
							if(IsClose(val0, 0.f, g_ConstantFloatOptimizationThreshold))
							{
								crExpressionOpNullary* opZero = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
								opZero->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);

								delete &op;
								return opZero;
							}
							else if(IsClose(val0, 1.f, g_ConstantFloatOptimizationThreshold))
							{
								crExpressionOpUnary* opUnaryReciprocal = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
								opUnaryReciprocal->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeReciprocal);
								opUnaryReciprocal->SetSource(opSource0->Clone());

								delete &op;
								return opUnaryReciprocal;
							}
						}

						float val1;
						if(IsConstantFloatExpressionOp(*opSource1, val1) && !IsClose(val1, 0.f, g_ConstantFloatOptimizationThreshold))
						{
							crExpressionOpConstantFloat* opConstantFloat = static_cast<crExpressionOpConstantFloat*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstantFloat));
							opConstantFloat->SetFloat(1.f / val1);
							
							opBinary.SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMultiply);
							opBinary.SetSource(1, opConstantFloat);

							delete opSource1;
							m_Changed = true;
							break;
						}
					}
					break;

				case crExpressionOpBinary::kBinaryOpTypeMultiply:
					{
						for(int i=0; i<2; ++i)
						{
							crExpressionOp* opSource = opBinary.GetSource(i);

							float val;
							if(IsConstantFloatExpressionOp(*opSource, val))
							{
								if(IsClose(val, 0.f, g_ConstantFloatOptimizationThreshold))
								{
									crExpressionOpNullary* opZero = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
									opZero->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);

									delete &op;
									return opZero;									
								}
								else if(IsClose(val, 1.f, g_ConstantFloatOptimizationThreshold))
								{
									crExpressionOp* opOtherSource = opBinary.GetSource((i+1)%2)->Clone();

									delete &op;
									return opOtherSource;
								}
								else if(IsClose(val, -1.f, g_ConstantFloatOptimizationThreshold))
								{
									crExpressionOpUnary* opUnaryNegate = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
									opUnaryNegate->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeNegate);

									opUnaryNegate->SetSource(opBinary.GetSource((i+1)%2)->Clone());

									delete &op;
									return opUnaryNegate;
								}
							}
						}
					}
					break;

				case crExpressionOpBinary::kBinaryOpTypeAdd:
					{
						for(int i=0; i<2; ++i)
						{
							crExpressionOp* opSource = opBinary.GetSource(i);

							float val;
							if(IsConstantFloatExpressionOp(*opSource, val))
							{
								if(IsClose(val, 0.f, g_ConstantFloatOptimizationThreshold))
								{
									crExpressionOp* opOtherSource = opBinary.GetSource((i+1)%2)->Clone();

									delete &op;
									return opOtherSource;
								}
							}
							else if(opSource->GetOpType() == crExpressionOp::kOpTypeBinary)
							{
								crExpressionOpBinary* opSourceBinary = static_cast<crExpressionOpBinary*>(opSource);
								switch(opSourceBinary->GetBinaryOpType())
								{
								case crExpressionOpBinary::kBinaryOpTypeMultiply:
									{
										crExpressionOpTernary* opTernaryMultiplyAdd = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
										opTernaryMultiplyAdd->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeMultiplyAdd);
			
										crExpressionOp* opOtherSource = opBinary.GetSource((i+1)%2)->Clone();

										opTernaryMultiplyAdd->SetSource(0, opSourceBinary->GetSource(0)->Clone());
										opTernaryMultiplyAdd->SetSource(1, opSourceBinary->GetSource(1)->Clone());
										opTernaryMultiplyAdd->SetSource(2, opOtherSource);

										delete &op;
										return opTernaryMultiplyAdd;
									}
									break;

								default:
									break;
								}
							}
						}
					}
					break;

				case crExpressionOpBinary::kBinaryOpTypeSubtract:
					{	
						// 0 second op, promote first
						// 0 first op, replace with negate


					}
					break;

				case crExpressionOpBinary::kBinaryOpTypeQuatMultiply:
					{
						for(int i=0; i<2; ++i)
						{
							crExpressionOp* opSource = opBinary.GetSource(i);

							QuatV q;
							if(IsConstantQuaternionExpressionOp(*opSource, q))
							{
								if(IsClose(q.GetXf(), 0.f, g_ConstantFloatOptimizationThreshold) && IsClose(q.GetYf(), 0.f, g_ConstantFloatOptimizationThreshold) && IsClose(q.GetZf(), 0.f, g_ConstantFloatOptimizationThreshold) && (IsClose(q.GetWf(), 1.f, g_ConstantFloatOptimizationThreshold) || IsClose(q.GetWf(), -1.f, g_ConstantFloatOptimizationThreshold)))
								{
									crExpressionOp* opOtherSource = opBinary.GetSource((i+1)%2)->Clone();

									delete &op;
									return opOtherSource;
								}
							}
						}
					}
					break;

				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	// If we're using rules then we still want to apply these.
	switch (op.GetOpType())
	{
		case crExpressionOp::kOpTypeTernary:
			{
				crExpressionOpTernary& opTernary = static_cast<crExpressionOpTernary&>(op);
				switch(opTernary.GetTernaryOpType())
				{
				case crExpressionOpTernary::kTernaryOpTypeToVector:
					{
						crExpressionOp* opSource[3];
						for(int i=0; i<3; ++i)
						{
							opSource[i] = opTernary.GetSource(i);
						}

						if(opSource[0]->GetOpType() == opSource[1]->GetOpType() && opSource[0]->GetOpType() == opSource[2]->GetOpType())
						{
							switch(opSource[0]->GetOpType())
							{
							case crExpressionOp::kOpTypeGet:
								{
									crExpressionOpComponentGet* opSourceComponentGet[3];
									for(int i=0; i<3; ++i)
									{
										opSourceComponentGet[i] = static_cast<crExpressionOpComponentGet*>(opSource[i]);
									}

									// TODO --- if float, and if identical, get once and splat
								}
								break;

							case crExpressionOp::kOpTypeComponentGet:
								{
									crExpressionOpComponentGet* opSourceComponentGet[3];
									for(int i=0; i<3; ++i)
									{
										opSourceComponentGet[i] = static_cast<crExpressionOpComponentGet*>(opSource[i]);
									}

									if(opSourceComponentGet[0]->GetTrack() == opSourceComponentGet[1]->GetTrack() && opSourceComponentGet[0]->GetTrack() == opSourceComponentGet[2]->GetTrack())
									{
										if(opSourceComponentGet[0]->GetId() == opSourceComponentGet[1]->GetId() && opSourceComponentGet[0]->GetId() == opSourceComponentGet[2]->GetId())
										{
											if(opSourceComponentGet[0]->GetType() == opSourceComponentGet[1]->GetType() && opSourceComponentGet[0]->GetType() == opSourceComponentGet[2]->GetType())
											{
												if(opSourceComponentGet[0]->GetType() == kFormatTypeVector3)
												{
													if(opSourceComponentGet[0]->GetComponent() == 0 && opSourceComponentGet[1]->GetComponent() == 1 && opSourceComponentGet[2]->GetComponent() == 2)
													{
														crExpressionOpGet* opGet = static_cast<crExpressionOpGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeGet));
														opGet->SetTrack(opSourceComponentGet[0]->GetTrack());
														opGet->SetId(opSourceComponentGet[0]->GetId());
														opGet->SetType(kFormatTypeVector3);
													
														delete &op;
														return opGet;
													}
												}
											}
										}
									}

									// TODO --- if component wise and vector, and in sequence, get vector
								}
								break;

							case crExpressionOp::kOpTypeBinary:
								{
									crExpressionOpBinary* opSourceBinary[3];
									for(int i=0; i<3; ++i)
									{
										opSourceBinary[i] = static_cast<crExpressionOpBinary*>(opSource[i]);
									}
									
									if(opSourceBinary[0]->GetBinaryOpType() == opSourceBinary[1]->GetBinaryOpType() && opSourceBinary[0]->GetBinaryOpType() == opSourceBinary[2]->GetBinaryOpType())
									{
										switch(opSourceBinary[0]->GetBinaryOpType())
										{
										case crExpressionOpBinary::kBinaryOpTypeAdd:
											{
												crExpressionOpBinary* opBinaryVectorAdd = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
												opBinaryVectorAdd->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeVectorAdd);

												opBinaryVectorAdd->SetSource(0, VectorizeExpressionOps(*opSourceBinary[0]->GetSource(0), *opSourceBinary[1]->GetSource(0), *opSourceBinary[2]->GetSource(0)));
												opBinaryVectorAdd->SetSource(1, VectorizeExpressionOps(*opSourceBinary[0]->GetSource(1), *opSourceBinary[1]->GetSource(1), *opSourceBinary[2]->GetSource(1)));

												delete &op;
												return opBinaryVectorAdd;
											}
											break;

										case crExpressionOpBinary::kBinaryOpTypeMultiply:
											{
												crExpressionOpBinary* opBinaryVectorMultiply = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
												opBinaryVectorMultiply->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeVectorMultiply);

												opBinaryVectorMultiply->SetSource(0, VectorizeExpressionOps(*opSourceBinary[0]->GetSource(0), *opSourceBinary[1]->GetSource(0), *opSourceBinary[2]->GetSource(0)));
												opBinaryVectorMultiply->SetSource(1, VectorizeExpressionOps(*opSourceBinary[0]->GetSource(1), *opSourceBinary[1]->GetSource(1), *opSourceBinary[2]->GetSource(1)));

												delete &op;
												return opBinaryVectorMultiply;
											}
											break;

										default:
											break;
										}
									}
								}
								break;

							case crExpressionOp::kOpTypeTernary:
								{
									crExpressionOpTernary* opSourceTernary[3];
									for(int i=0; i<3; ++i)
									{
										opSourceTernary[i] = static_cast<crExpressionOpTernary*>(opSource[i]);
									}

									if(opSourceTernary[0]->GetTernaryOpType() == opSourceTernary[1]->GetTernaryOpType() && opSourceTernary[0]->GetTernaryOpType() == opSourceTernary[2]->GetTernaryOpType())
									{
										switch(opSourceTernary[0]->GetTernaryOpType())
										{
										case crExpressionOpTernary::kTernaryOpTypeMultiplyAdd:
											{
												crExpressionOpTernary* opTernaryVectorMultiplyAdd = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
												opTernaryVectorMultiplyAdd->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeVectorMultiplyAdd);

												opTernaryVectorMultiplyAdd->SetSource(0, VectorizeExpressionOps(*opSourceTernary[0]->GetSource(0), *opSourceTernary[1]->GetSource(0), *opSourceTernary[2]->GetSource(0)));
												opTernaryVectorMultiplyAdd->SetSource(1, VectorizeExpressionOps(*opSourceTernary[0]->GetSource(1), *opSourceTernary[1]->GetSource(1), *opSourceTernary[2]->GetSource(1)));
												opTernaryVectorMultiplyAdd->SetSource(2, VectorizeExpressionOps(*opSourceTernary[0]->GetSource(2), *opSourceTernary[1]->GetSource(2), *opSourceTernary[2]->GetSource(2)));

												delete &op;
												return opTernaryVectorMultiplyAdd;
											}
											break;

										default:
											break;
										}
									}
								}
								break;

							default:
								break;
							}
						}

						float val[3];
						if(IsConstantFloatExpressionOp(*opSource[0], val[0]) && IsConstantFloatExpressionOp(*opSource[1], val[1]) && IsConstantFloatExpressionOp(*opSource[2], val[2]))
						{
							crExpressionOpConstant* opConstant = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
							
							crExpressionOp::Value v;
							v.Set(Vec3V(val[0], val[1], val[2]));

							opConstant->SetValue(v);

							delete &op;
							return opConstant;
						}
					}
					break;

				case crExpressionOpTernary::kTernaryOpTypeToQuaternion:
					{
						crExpressionOp* opSource[3];
						for(int i=0; i<3; ++i)
						{
							opSource[i] = opTernary.GetSource(i);
						}

						if(opSource[0]->GetOpType() == opSource[1]->GetOpType() && opSource[0]->GetOpType() == opSource[2]->GetOpType())
						{
							switch(opSource[0]->GetOpType())
							{
							case crExpressionOp::kOpTypeComponentGet:
								// TODO - IF THREE COMPONENT GETS, IN SEQUENCE, REPLACE WITH GET
								break;

							default:
								break;
							}
						}

						float val[3];
						if(IsConstantFloatExpressionOp(*opSource[0], val[0]) && IsConstantFloatExpressionOp(*opSource[1], val[1]) && IsConstantFloatExpressionOp(*opSource[2], val[2]))
						{
							crExpressionOpConstant* opConstant = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));

							Vec3V e(val[0], val[1], val[2]);
							
							QuatV q = QuatVFromEulersXYZ(e);

							crExpressionOp::Value v;
							v.Set(q);

							opConstant->SetValue(v);

							delete &op;
							return opConstant;
						}
					}

				default:
					break;
				}
			}
			break;

		case crExpressionOp::kOpTypeSet:
			break;

		case crExpressionOp::kOpTypeNary:
			{
				crExpressionOpNary& opNary = static_cast<crExpressionOpNary&>(op);
				switch(opNary.GetNaryOpType())
				{
				case crExpressionOpNary::kNaryOpTypeSum:
//				case crExpressionOpNary::kNaryOpTypeComma:
					{
						const int numSources = opNary.GetNumSources();
						if(numSources < 1)
						{
							crExpressionOpNullary* opZero = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
							opZero->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);

							delete &opNary;
							return opZero;
						}
						else if(numSources == 1)
						{
							crExpressionOp* opNarySource = opNary.GetSource(0)->Clone();
							delete &op;
							return opNarySource;
						}
						else if(numSources == 2)
						{
							crExpressionOp* opNarySource0 = opNary.GetSource(0)->Clone();
							crExpressionOp* opNarySource1 = opNary.GetSource(1)->Clone();
		
							crExpressionOpBinary* opBinaryAdd = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
							opBinaryAdd->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeAdd);
							opBinaryAdd->SetSource(0, opNarySource0);
							opBinaryAdd->SetSource(1, opNarySource1);

							delete &op;
							return opBinaryAdd;
						}
						else 
						{
							int constantIdx = -1;
							float constantVal = 0.f;
							for(int i=0; i<opNary.GetNumSources(); ++i)
							{
								crExpressionOp* opSource = opNary.GetSource(i);
								if(opSource->GetOpType() == crExpressionOp::kOpTypeNullary)
								{
									if(static_cast<crExpressionOpNullary*>(opSource)->GetNullaryOpType() == crExpressionOpNullary::kNullaryOpTypeZero)
									{
										opNary.RemoveSource(i--);
										m_Changed = true;
									}
								}
								else
								{
									float val;
									if(IsConstantFloatExpressionOp(*opSource, val))
									{
										if(constantIdx >= 0)
										{
											opNary.RemoveSource(i);
											opNary.RemoveSource(constantIdx);

											crExpressionOpConstantFloat* opConstantFloat = static_cast<crExpressionOpConstantFloat*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstantFloat));
											opConstantFloat->SetFloat(constantVal+val);

											opNary.AppendSource(opConstantFloat);

											constantIdx = -1;
											constantVal = 0.f;

											i-=2;
											m_Changed = true;
										}
										else
										{
											constantIdx = i;
											constantVal = val;
										}
									}
								}
							}	
						}
					}
					break;

				default:
					break;
				}
			}
			break;

		case crExpressionOp::kOpTypeSpecialBlend:
			{
				crExpressionOpSpecialBlend& opSpecialBlend = static_cast<crExpressionOpSpecialBlend&>(op);

				const int numSourceWeights = opSpecialBlend.GetNumSourceWeights();
				if(numSourceWeights < 1)
				{
					crExpressionOpNullary* opZero = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
					opZero->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);

					delete &op;
					return opZero;
				}
				else
				{
					float weight0 = opSpecialBlend.GetWeight(0);
					if(IsClose(weight0, 1.f, g_ConstantFloatOptimizationThreshold))
					{
						if(numSourceWeights == 1)
						{
							crExpressionOp* opSource = opSpecialBlend.GetSource(0)->Clone();

							delete &op;
							return opSource;
						}

						if(numSourceWeights == 2)
						{
							float weight1 = opSpecialBlend.GetWeight(1);
							if(IsClose(weight1, 1.f, g_ConstantFloatOptimizationThreshold))
							{
								crExpressionOpBinary* opBinary = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
								switch(opSpecialBlend.GetBlendMode())
								{
								case crExpressionOpSpecialBlend::kBlendModeFloat:
									opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeAdd);
									break;

								case crExpressionOpSpecialBlend::kBlendModeVector:
									opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeVectorAdd);
									break;

								case crExpressionOpSpecialBlend::kBlendModeQuaternion:
									opBinary->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeQuatMultiply);
									break;

								default:
									break;
								}

								opBinary->SetSource(0, opSpecialBlend.GetSource(0)->Clone());
								opBinary->SetSource(1, opSpecialBlend.GetSource(1)->Clone());

								delete &op;
								return opBinary;
							}
						}

						if(opSpecialBlend.GetBlendMode() == crExpressionOpSpecialBlend::kBlendModeFloat)
						{
							int i=1;
							for(; i<numSourceWeights; ++i)
							{
								float weight = opSpecialBlend.GetWeight(i);
								if(!IsClose(weight, weight0))
								{
									break;
								}
							}
							if(i==numSourceWeights)
							{
								// TODO --- if all identical, but not one, could replace with sum and multiply?
								crExpressionOpNary* opNarySum = static_cast<crExpressionOpNary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNary));
								opNarySum->SetNaryOpType(crExpressionOpNary::kNaryOpTypeSum);

								for(int i=0; i<numSourceWeights; ++i)
								{
									opNarySum->AppendSource(opSpecialBlend.GetSource(i)->Clone());
								}

								delete &op;
								return opNarySum;
							}
						}
					}

					for(int i=0; i<opSpecialBlend.GetNumSourceWeights(); ++i)
					{
						float weight = opSpecialBlend.GetWeight(i);
						if(IsClose(weight, 0.f, g_ConstantFloatOptimizationThreshold))
						{
							opSpecialBlend.RemoveSourceWeight(i--);
							m_Changed = true;
							continue;
						}
						else
						{
							crExpressionOp* opSource = opSpecialBlend.GetSource(i);
							if(opSource->GetOpType() == crExpressionOp::kOpTypeNullary)
							{
								if(static_cast<crExpressionOpNullary*>(opSource)->GetNullaryOpType() == crExpressionOpNullary::kNullaryOpTypeZero)
								{
									opSpecialBlend.RemoveSourceWeight(i--);
									m_Changed = true;
									continue;
								}
							}
						}
					}
				}
			}
			break;

		case crExpressionOp::kOpTypeSpecialCurve:
			{
				crExpressionOpSpecialCurve& opSpecialCurve = static_cast<crExpressionOpSpecialCurve&>(op);
				
				float specialCurveOptimizationThreshold = oc.m_Radians?g_SpecialCurveOptimizationRotationThreshold:g_SpecialCurveOptimizationTranslationThreshold;

				const int numKeys = opSpecialCurve.GetNumKeys();
				if(numKeys < 1)
				{
					crExpressionOpNullary* opZero = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
					opZero->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);

					delete &op;
					return opZero;
				}
				else
				{
					float in0, out0;
					opSpecialCurve.GetKey(0, in0, out0);

					int i=1;
					for(; i<numKeys; ++i)
					{
						float in, out;
						opSpecialCurve.GetKey(i, in, out);

						if(!IsClose(out, out0, specialCurveOptimizationThreshold))
						{
							break;
						}
					}
					if(i == numKeys)
					{
						crExpressionOpConstantFloat* opConstantFloat = static_cast<crExpressionOpConstantFloat*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstantFloat));
						opConstantFloat->SetFloat(out0);  // funnily enough, using out0 better than average of outs (for IM data)

						delete &op;
						return opConstantFloat;
					}

					if(numKeys > 2)
					{
						float in1, out1;
						opSpecialCurve.GetKey(1, in1, out1);

						if(IsClose(out0, out1, specialCurveOptimizationThreshold))
						{
							opSpecialCurve.RemoveKey(0);
							m_Changed = true;
						}
						else
						{
							float inN1, outN1;
							opSpecialCurve.GetKey(numKeys-1, inN1, outN1);

							float inN2, outN2;
							opSpecialCurve.GetKey(numKeys-2, inN2, outN2);

							if(IsClose(outN1, outN2, specialCurveOptimizationThreshold))
							{
								opSpecialCurve.RemoveKey(numKeys-1);
								m_Changed = true;
							}
							else
							{
								for(int i=0; i<(numKeys-2); ++i)
								{
									float inI0, outI0;
									opSpecialCurve.GetKey(i, inI0, outI0);

									float inI1, outI1;
									opSpecialCurve.GetKey(i+1, inI1, outI1);

									float inI2, outI2;
									opSpecialCurve.GetKey(i+2, inI2, outI2);

									if(IsClose(outI1, Lerp((inI1-inI0)/(inI2-inI0), outI0, outI2), specialCurveOptimizationThreshold))
									{
										opSpecialCurve.RemoveKey(i+1);
										m_Changed = true;
										break;
									}
								}
							}
						}
					}
				}
			}
			// TODO - CONSTANT INPUT, DECODE OUTPUT REPLACE WITH NEW CONSTANT?
			break;

		default:
			break;
	}

	return &op;
}


struct DependencyIterator : public crExpressionOp::ConstIterator
{
	DependencyIterator(u8 track, u16 id, u8 component)
		: m_Track(track)
		, m_Id(id)
		, m_Component(component)
		, m_HasDependency(false)
	{
	}

	virtual ~DependencyIterator()
	{
	}

	virtual void Visit(const crExpressionOp& op)
	{
		if(!m_HasDependency)
		{
			switch(op.GetOpType())
			{
			case crExpressionOp::kOpTypeGet:
			case crExpressionOp::kOpTypeComponentGet:
				{					
					const crExpressionOpGetSet& opGetSet = static_cast<const crExpressionOpGetSet&>(op);
					if(opGetSet.GetTrack() == m_Track && opGetSet.GetId() == m_Id)
					{
						switch(op.GetOpType())
						{
						case crExpressionOp::kOpTypeGet:
							m_HasDependency = true;
							break;

						case crExpressionOp::kOpTypeComponentGet:
							{
								const crExpressionOpComponentGet& opComponentGet = static_cast<const crExpressionOpComponentGet&>(op);
								if(opComponentGet.GetComponent() < m_Component)
								{
									m_HasDependency = true;
								}
							}
							break;

						default:
							break;
						}
					}
				}
				break;

			default:
				ConstIterator::Visit(op);
				break;
			}
		}
	}

	u8 m_Track;
	u16 m_Id;
	u8 m_Component;

	bool m_HasDependency;
};

void OptimizeIntraExpressionData(crExpressions& expressions)
{
	// intra-expression optimization
	const int numExpressions = expressions.GetNumExpressions();
	for(int i=0; i<numExpressions; ++i)
	{
		crExpression& expression = *expressions.GetExpression(i);

		// multi pass optimization
		do 
		{
			OptimizeIterator it(false);
			expression.SetExpressionOp(it.Visit(*expression.GetExpressionOp()));

			if(!it.m_Changed)
			{
				break;
			}
		} 
		while(1);
	}
}

void OptimizeExpressionData(crExpressions& expressions, bool collapse, bool useRules)
{
	using namespace MakeExpressionsRules;

	// start with vectorization
	Linearizer vectorize;
	for(int i=0; i<expressions.GetNumExpressions(); ++i)
	{
		vectorize.Visit(*expressions.GetExpression(i)->GetExpressionOp());
	}
	vectorize.PrintStats();

	// intra-expression optimization (first pass)
	atArray<ExpressionOptimizerRule*> rules;
	if (useRules)
	{
		BuildOptimizerRuleList(rules);
		OptimizeIntraExpressionDataWithRules(expressions, rules);
	}
	else 
	{
		OptimizeIntraExpressionData(expressions);
	}

	// inter-expression optimization
	if(collapse)
	{
		// check for triples
		for(int i=2; i<expressions.GetNumExpressions(); ++i)
		{
			crExpressionOp* op[3];
			for(int j=0; j<3; ++j)
			{
				op[j] = expressions.GetExpression(i-(2-j))->GetExpressionOp();
			}

			if(op[0]->GetOpType() == crExpressionOp::kOpTypeComponentSet && op[1]->GetOpType() == crExpressionOp::kOpTypeComponentSet && op[2]->GetOpType() == crExpressionOp::kOpTypeComponentSet)
			{
				crExpressionOpComponentSet* opComponentSet[3];
				u8 track[3];
				u16 id[3];
				u8 type[3];
				u8 component[3];
				EulerAngleOrder order[3];

				for(int j=0; j<3; ++j)
				{
					opComponentSet[j] = static_cast<crExpressionOpComponentSet*>(op[j]);
					track[j] = opComponentSet[j]->GetTrack();
					id[j] = opComponentSet[j]->GetId();
					type[j] = opComponentSet[j]->GetType();
					component[j] = opComponentSet[j]->GetComponent();
					order[j] = opComponentSet[j]->GetOrder();
				}

				if(track[0] == track[1] && track[1] == track[2] && id[0] == id[1] && id[1] == id[2] && type[0] == type[1] && type[1] == type[2] && component[0] == 0 && component[1] == 1 && component[2] == 2 && order[0] == 0 && order[1] == 0 && order[2] == 0)
				{
					// found triple, check for a direct copy triple
					crExpressionOp* opSource[3];
					for(int j=0; j<3; ++j)
					{
						opSource[j] = opComponentSet[j]->GetSource();
					}

					if(opSource[0]->GetOpType() == crExpressionOp::kOpTypeComponentGet && opSource[1]->GetOpType() == crExpressionOp::kOpTypeComponentGet && opSource[2]->GetOpType() == crExpressionOp::kOpTypeComponentGet)
					{
						crExpressionOpComponentGet* opComponentGet[3];
						u8 sourceTrack[3];
						u16 sourceId[3];
						u8 sourceType[3];
						u8 sourceComponent[3];
						EulerAngleOrder sourceOrder[3];

						for(int j=0; j<3; ++j)
						{
							opComponentGet[j] = static_cast<crExpressionOpComponentGet*>(opSource[j]);
							sourceTrack[j] = opComponentGet[j]->GetTrack();
							sourceId[j] = opComponentGet[j]->GetId();
							sourceType[j] = opComponentGet[j]->GetType();
							sourceComponent[j] = opComponentGet[j]->GetComponent();
							sourceOrder[j] = opComponentGet[j]->GetOrder();
						}

						if(sourceTrack[0] == sourceTrack[1] && sourceTrack[1] == sourceTrack[2] && sourceId[0] == sourceId[1] && sourceId[1] == sourceId[2] && sourceType[0] == sourceType[1] && sourceType[1] == sourceType[2] && sourceComponent[0] == 0 && sourceComponent[1] == 1 && sourceComponent[2] == 2 && sourceOrder[0] == 0 && sourceOrder[1] == 0 && sourceOrder[2] == 0 && type[0] == sourceType[0])
						{
							// found copy triple
							crExpressionOpGet* opGet = static_cast<crExpressionOpGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeGet));
							opGet->SetTrack(sourceTrack[0]);
							opGet->SetId(sourceId[0]);
							opGet->SetType(sourceType[0]);

							crExpressionOpSet* opSet = static_cast<crExpressionOpSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSet));
							opSet->SetTrack(track[0]);
							opSet->SetId(id[0]);
							opSet->SetType(type[0]);
							opSet->SetSource(opGet);

							expressions.RemoveExpression(i);
							expressions.RemoveExpression(i-1);

							expressions.GetExpression(i-2)->SetExpressionOp(opSet);

							i=1;
							continue;
						}
					}

					// not a copy triple, just a regular triple
					DependencyIterator itDepend1(track[0], id[0], 1);
					itDepend1.Visit(*opSource[1]);

					DependencyIterator itDepend2(track[0], id[0], 2);
					itDepend2.Visit(*opSource[2]);

					if(!itDepend1.m_HasDependency && !itDepend2.m_HasDependency)
					{
						crExpressionOpTernary* opTernary = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
						opTernary->SetTernaryOpType((type[0]==kFormatTypeVector3)?crExpressionOpTernary::kTernaryOpTypeToVector:crExpressionOpTernary::kTernaryOpTypeToQuaternion);
						opTernary->SetSource(0, opSource[0]->Clone());
						opTernary->SetSource(1, opSource[1]->Clone());
						opTernary->SetSource(2, opSource[2]->Clone());

						crExpressionOpSet* opSet = static_cast<crExpressionOpSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSet));
						opSet->SetTrack(track[0]);
						opSet->SetId(id[0]);
						opSet->SetType(type[0]);
						opSet->SetSource(opTernary);

						expressions.RemoveExpression(i);
						expressions.RemoveExpression(i-1);

						expressions.GetExpression(i-2)->SetExpressionOp(opSet);

						i=1;
						continue;
					}
				}
			}
		}

		// intra-expression optimization (second pass)
		if (useRules)
		{
			OptimizeIntraExpressionDataWithRules(expressions, rules);
		}
		else 
		{
			OptimizeIntraExpressionData(expressions);
		}
	}

	ReleaseOptimizerRuleList(rules);

	expressions.SetOptimized(true);
}

void PackExpressionData(crExpressions& expressions, u32 maxPackSize)
{
	if(expressions.GetNumExpressions() > 1)
	{
		atArray<u32> packedSizes;
		packedSizes.Resize(expressions.GetNumExpressions());
		{
			crExpressions packedExpressions;
			for(int i=0; i<expressions.GetNumExpressions(); ++i)
			{
				packedExpressions.AppendExpression(*expressions.GetExpression(i));
			}
			packedExpressions.CalcInputOutputDofs();
			packedExpressions.Pack(packedSizes.GetElements());
		}

		int i=0;
		do
		{
			crExpressionOpNary* opNary = static_cast<crExpressionOpNary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNary));
			opNary->SetNaryOpType(crExpressionOpNary::kNaryOpTypeList);

			u32 packedSizeSum = 0;
			for(; i<expressions.GetNumExpressions(); ++i)
			{
				crExpression& expression = *expressions.GetExpression(i);
				packedSizeSum += packedSizes[i];

				if(packedSizeSum<=maxPackSize)
				{
					opNary->AppendSource(expression.GetExpressionOp()->Clone());
					packedSizes.Delete(i);
					expressions.RemoveExpression(i--);
				}
				else if(opNary->GetNumSources())
				{
					break;
				}
				else 
				{
					if(packedSizes[i] > maxPackSize)
					{
						Warningf("WARNING: Unable to satisfy maxpacksize of %d, expression[%d] is %d bytes when packed", maxPackSize, i, packedSizes[i]);
					}
					packedSizeSum = 0;
				}
			}

			if(opNary->GetNumSources() > 0)
			{
				crExpression expression;
				expression.SetExpressionOp((opNary->GetNumSources() > 1) ? opNary->Clone() : opNary->GetSource(0)->Clone());
				packedSizes.Insert(i) = packedSizeSum;
				expressions.InsertExpression(expression, i++);
			}

			delete opNary;
		}
		while(i < expressions.GetNumExpressions());
	}
}

}; // MakeExpresions

}; // rage
