set ARCHIVE=exprtools
set FILES=makeexpressionscore applyexpressionscore makeexpressionsrules linearizeexpressions
set TESTERS=makeexpressions dumpexpressions applyexpressions
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% animexprdata
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\suite\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\samples
