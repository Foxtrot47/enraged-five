//
// exprtools/linearizeexpressions.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "linearizeexpressions.h"
#include "crextra/expressions.h"

#define exprDebug(fmt,...) RAGE_DEBUGF1(expr,fmt,##__VA_ARGS__)

namespace rage
{

RAGE_DEFINE_CHANNEL(expr, DIAG_SEVERITY_DEBUG1, __DEV ? DIAG_SEVERITY_DEBUG1 : DIAG_SEVERITY_DISPLAY)

////////////////////////////////////////////////////////////////////////////////

Linearizer::Linearizer()
: m_Success(0)
, m_Total(0)
{
}

////////////////////////////////////////////////////////////////////////////////

void Linearizer::PrintStats()
{
	if(m_Success > 0)
	{
		exprDebug("%u/%u sources linearized (%.0f%%)", m_Success, m_Total, float(m_Success)/float(m_Total)*100.f);
	}
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* Linearizer::Visit(crExpressionOp& op)
{
	if(op.GetOpType() == crExpressionOp::kOpTypeSpecialBlend)
	{
		const crExpressionOpSpecialBlend* blendOp = static_cast<const crExpressionOpSpecialBlend*>(&op);
		m_Total += blendOp->GetNumChildren();

		VectorFunctions results;
		if(Build(blendOp, results) && Merge(results))
		{
			crExpressionOpSpecialLinear* newOp = Create(blendOp->GetBlendMode(), results);
			m_Success += blendOp->GetNumChildren();

#if CR_TEST_EXPRESSIONS
			newOp->m_Reference = blendOp->Clone();
#endif
			delete blendOp;
			return newOp;
		}
		else
		{
			return &op;
		}
	}
	else
	{
		return crExpressionOp::ModifyingIterator::Visit(op);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::Build(const crExpressionOpSpecialBlend* op, VectorFunctions& results)
{
	crExpressionOpSpecialBlend::eBlendMode mode = op->GetBlendMode();
	if(mode == crExpressionOpSpecialBlend::kBlendModeFloat)
		return false;

	crExpressionOpTernary::eTernaryOpTypes type = mode == crExpressionOpSpecialBlend::kBlendModeVector ? crExpressionOpTernary::kTernaryOpTypeToVector : crExpressionOpTernary::kTernaryOpTypeToQuaternion;

	int numSources = op->GetNumChildren();
	if(!numSources)
		return false;

	results.Resize(numSources);

	// try to convert each source of the special blend
	for(int i=0; i < numSources; i++)
	{
		// try to convert each branch of the ternary
		VectorFunction& vector = results[i];

		const crExpressionOp* child = op->GetSource(i);
		if(child->GetOpType() != crExpressionOp::kOpTypeTernary)
			return false;

		const crExpressionOpTernary* ternary = static_cast<const crExpressionOpTernary*>(child);
		if(ternary->GetTernaryOpType() != type)
			return false;

		float weight = op->GetWeight(i);
		for(int j=0; j < VectorFunction::kNumComponents; j++)
		{
			ScalarFunction& scalar = vector.m_Components[j];
			if(!Convert(child->GetChild(j), scalar))
				return false;

			// apply source weight on function
			PartialFunctions& partials = scalar.m_Partials;
			for(int k=0; k < partials.GetCount(); k++)
			{
				partials[k].m_Add *= weight;
				partials[k].m_Mult *= weight;
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::Merge(VectorFunctions& results)
{
	// create expanded buffer
	int count = results.GetCount();
	VectorFunctions dest;
	dest.Reserve(count*VectorFunction::kNumComponents);

	// create new vectors if different variable
	for(int i=0; i < count; i++)
	{
		const VectorFunction& vector = results[i];

		// copy components backward because eulerXYZ = Mz*My*Mz
		int lastIdx = VectorFunction::kNumComponents - 1;
		VectorFunction* current = &dest.Append();
		Variable last = vector.m_Components[lastIdx].m_Variable;
		current->m_Components[lastIdx] = vector.m_Components[lastIdx];

		for(int j=lastIdx-1; j >= 0; j--)
		{
			const ScalarFunction& scalar = vector.m_Components[j];
			const Variable& variable = scalar.m_Variable;
			if(variable.IsValid())
			{
				if(last.IsValid() && variable != last)
				{
					current = &dest.Append();
				}
				last = variable;
			}

			current->m_Components[j] = scalar;
		}
	}

	// complete missing component
	for(int i=0; i < dest.GetCount(); i++)
	{
		for(int j=0; j < VectorFunction::kNumComponents; j++)
		{
			ScalarFunction& scalar = dest[i].m_Components[j];
			if(scalar.IsEmpty())
			{
				scalar.m_Partials.Grow();
			}
		}
	}

	results.Swap(dest);
	return true;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionOpSpecialLinear* Linearizer::Create(crExpressionOpSpecialBlend::eBlendMode mode, VectorFunctions& results)
{
	// compute number of intervals
	int intervalPerSource = 0;
	int numVectors = results.GetCount();
	for(int i=0; i < numVectors; i++)
	{
		VectorFunction& vector = results[i];

		for(int j=0; j < VectorFunction::kNumComponents; j++)
		{
			ScalarFunction& scalar = vector.m_Components[j];
			PartialFunctions& partials = scalar.m_Partials;

			// sort intervals
			Assert(partials[0].m_Begin == -FLT_MAX && partials.Top().m_End == FLT_MAX);

			// try to merge intervals
			for(int k=1; k < partials.GetCount(); k++)
			{
				PartialFunction& curr = partials[k];
				PartialFunction& prev = partials[k-1];
				Assert(curr.m_Begin == prev.m_End);

				if(curr.m_Add == prev.m_Add && curr.m_Mult == prev.m_Mult)
				{
					prev.m_End = curr.m_End;
					partials.Delete(k--);
				}
			}

			intervalPerSource = Max(intervalPerSource, partials.GetCount());
		}
	}

	// create new expression operation
	crExpressionOpSpecialLinear* specialLinear = rage_new crExpressionOpSpecialLinear();
	specialLinear->m_Sources.Resize(numVectors);
	specialLinear->m_Intervals.Resize(numVectors*intervalPerSource);

	for(int i=0; i < numVectors; i++)
	{
		crExpressionOpSpecialLinear::Source& src = specialLinear->m_Sources[i];
		const VectorFunction& vector = results[i];

		Variable curr;
		for(int j=0; j < VectorFunction::kNumComponents; j++)
		{
			const ScalarFunction& scalar = vector.m_Components[j];
			const PartialFunctions& partials = scalar.m_Partials;
			if(scalar.m_Variable.IsValid())
			{
				Assert(!(curr.IsValid() && scalar.m_Variable != curr));
				curr = scalar.m_Variable;
			}

			for(int k=0; k < intervalPerSource; k++)
			{
				crExpressionOpSpecialLinear::Interval& interval = specialLinear->m_Intervals[i*intervalPerSource+k];
				if(k < partials.GetCount())
				{
					const PartialFunction& partial = partials[k];
					interval.m_Begin[j] = partial.m_Begin;
					interval.m_Mult[j] = partial.m_Mult;
					interval.m_Add[j] = partial.m_Add;
				}
				else
				{
					interval.m_Begin[j] = FLT_MAX;
					interval.m_Mult[j] = 0.f;
					interval.m_Add[j] = 0.f;
				}
			}
		}

		const Variable& var = curr;
		src.m_Id = var.m_Id;
		src.m_Track = var.m_Track;
		src.m_Component = var.m_Component;
		src.m_Type = var.m_Type;
		src.m_AccIdx = 0;
	}

	// add padding to sources for multiple of 4
	while(numVectors % 4)
	{
		crExpressionOpSpecialLinear::Source& src = specialLinear->m_Sources.Grow();
		memset(&src, 0, sizeof(crExpressionOpSpecialLinear::Source));
		numVectors++;

		for(int i=0; i < intervalPerSource; i++)
		{
			crExpressionOpSpecialLinear::Interval& interval = specialLinear->m_Intervals.Grow();
			interval.m_Begin = Vec3V(V_NEG_FLT_MAX);
			interval.m_Mult = Vec3V(V_ZERO);
			interval.m_Add = Vec3V(V_ZERO);
		}
	}

	specialLinear->m_IntervalPerSource = intervalPerSource;
	specialLinear->m_Mode = mode;
	return specialLinear;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::Convert(const crExpressionOp* op, ScalarFunction& result)
{
	switch(op->GetOpType())
	{
	case crExpressionOp::kOpTypeSpecialBlend:
		if(!ConvertSpecialBlend(static_cast<const crExpressionOpSpecialBlend*>(op), result))
			return false;
		break;

	case crExpressionOp::kOpTypeGet:
	case crExpressionOp::kOpTypeComponentGet:
		if(!ConvertGet(static_cast<const crExpressionOpGet*>(op), result))
			return false;
		break;

	case crExpressionOp::kOpTypeConstantFloat:
		if(!ConvertConstantFloat(static_cast<const crExpressionOpConstantFloat*>(op), result))
			return false;
		break;

	case crExpressionOp::kOpTypeNullary:
		if(!ConvertNullary(static_cast<const crExpressionOpNullary*>(op), result))
			return false;
		break;

	case crExpressionOp::kOpTypeUnary:
		if(!ConvertUnary(static_cast<const crExpressionOpUnary*>(op), result))
			return false;
		break;

	case crExpressionOp::kOpTypeBinary:
		if(!ConvertBinary(static_cast<const crExpressionOpBinary*>(op), result))
			return false;
		break;

	case crExpressionOp::kOpTypeTernary:
		if(!ConvertTernary(static_cast<const crExpressionOpTernary*>(op), result))
			return false;
		break;

	default:
		exprDebug("crExpressionOp - type %u is unsupported", op->GetOpType()); 
		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ConvertSpecialBlend(const crExpressionOpSpecialBlend* op, ScalarFunction& result)
{
	if(op->GetBlendMode() != crExpressionOpSpecialBlend::kBlendModeFloat)
		return false;

	int numSources = op->GetNumSourceWeights();
	for(int i=0; i < numSources; i++)
	{
		ScalarFunction child;
		if(!Convert(op->GetChild(i), child))
			return false;

		float add = 0.f;
		if(result.IsEmpty())
		{
			result = child;
		}
		else if(result.IsConstant())
		{
			add = result.m_Partials[0].m_Add;
			result = child;
		} 
		else if(child.IsConstant())
		{
			add = child.m_Partials[0].m_Add;
		}
		else
		{
			exprDebug("crExpressionOpSpecialBlend - support only one variable");
			return false;
		}

		float weight = op->GetWeight(i);
		for(int j=0; j < result.m_Partials.GetCount(); j++)
		{
			result.m_Partials[j].m_Add += weight * add;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ConvertGet(const crExpressionOpGet* op, ScalarFunction& result)
{
	if(op->IsForceDefault())
	{
		exprDebug("crExpressionOpGet - can't use force default for track %u id %u", op->GetTrack(), op->GetId());
		return false;
	}

	if(op->IsRelative())
	{
		exprDebug("crExpressionOpGet - can't use relative for track %u id %u", op->GetTrack(), op->GetId());
		return false;
	}

	// we don't do conversion from quaternion to angles
	if(op->GetOpType() == crExpressionOp::kOpTypeComponentGet && op->GetType() == kFormatTypeQuaternion)
	{
		exprDebug("crExpressionOpGet - can't get quaternion component for track %u id %u comp %u", op->GetTrack(), op->GetId(), op->GetComponent());
		return false;
	}

	// we only support tracks having zero has default value
	switch(op->GetTrack())
	{
		case kTrackBoneTranslation:
		case kTrackBoneRotation:
		case kTrackBoneScale:
		case kTrackMoverTranslation:
		case kTrackMoverRotation:
		case kTrackMoverScale:
			exprDebug("crExpressionOpGet - track %u is unsupported", op->GetTrack()); 
			return false;
	}

	Variable getVar;
	getVar.m_Valid = true;
	getVar.m_Id = op->GetId();
	getVar.m_Track = op->GetTrack();
	getVar.m_Component = op->GetComponent();
	getVar.m_Type = op->GetType();

	Assert(!result.m_Variable.IsValid());
	result.m_Variable = getVar;

	PartialFunction& partial = result.m_Partials.Grow();
	partial.m_Mult = 1.f;
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ConvertConstantFloat(const crExpressionOpConstantFloat* op, ScalarFunction& result)
{
	PartialFunction& partial = result.m_Partials.Grow();
	partial.m_Add = op->GetFloat();
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ConvertNullary(const crExpressionOpNullary* op, ScalarFunction& result)
{
	float add;
	switch(op->GetNullaryOpType())
	{
	case crExpressionOpNullary::kNullaryOpTypePi: add = PI; break;
	case crExpressionOpNullary::kNullaryOpTypeZero: add = 0.f; break;
	case crExpressionOpNullary::kNullaryOpTypeOne: add = 1.f; break;
	default: 
		exprDebug("crExpressionOpNullary - subtype %u is unsupported", op->GetNullaryOpType()); 
		return false;
	}

	PartialFunction& partial = result.m_Partials.Grow();
	partial.m_Add = add;
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ConvertUnary(const crExpressionOpUnary* op, ScalarFunction& result)
{
	float mult;
	switch(op->GetUnaryOpType())
	{
	case crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees: mult = RtoD; break;
	case crExpressionOpUnary::kUnaryOpTypeDegreesToRadians: mult = DtoR; break;
	case crExpressionOpUnary::kUnaryOpTypeNegate:			mult = -1.f; break;
	default: 
		exprDebug("crExpressionOpUnary - subtype %u is unsupported", op->GetUnaryOpType()); 
		return false;
	}

	if(!Convert(op->GetChild(0), result))
		return false;

	for(int i=0; i < result.m_Partials.GetCount(); i++)
	{
		PartialFunction& partial = result.m_Partials[i];
		partial.m_Add *= mult;
		partial.m_Mult *= mult;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ConvertBinary(const crExpressionOpBinary* op, ScalarFunction& result)
{
	ScalarFunction left, right;
	if(!Convert(op->GetChild(0), left))
		return false;

	if(!Convert(op->GetChild(1), right))
		return false;

	// must have same variable
	if(left.m_Variable.IsValid() && right.m_Variable.IsValid() && left.m_Variable != right.m_Variable)
	{
		exprDebug("crExpressionOpBinary - sides use different variable: track %u id %u comp %u != track %u id %u comp %u", 
			left.m_Variable.m_Track, left.m_Variable.m_Id, left.m_Variable.m_Component,
			right.m_Variable.m_Track, right.m_Variable.m_Id, right.m_Variable.m_Component); 
		return false;
	}

	if(left.m_Variable.IsValid())
	{
		result.m_Variable = left.m_Variable;
	}
	if(right.m_Variable.IsValid())
	{
		result.m_Variable = right.m_Variable;
	}

	// right side must be a constant
	bool invert = false;
	if(!right.IsConstant())
	{
		SwapEm(left, right);
		invert = true;

		if(!right.IsConstant())
		{
			exprDebug("crExpressionOpBinary - one child must be a constant"); 
			return false; 
		}
	}

	// apply binary op on left children
	float fval = right.m_Partials[0].m_Add;
	int count = left.m_Partials.GetCount();
	result.m_Partials.Reserve(count);

	for(int i=0; i < count; i++)
	{
		PartialFunction& partial = result.m_Partials.Append();
		partial = left.m_Partials[i];
		switch(op->GetBinaryOpType())
		{
		case crExpressionOpBinary::kBinaryOpTypeAdd:
			partial.m_Add += fval;
			break;

		case crExpressionOpBinary::kBinaryOpTypeSubtract:
			if(!invert)
			{
				partial.m_Add -= fval;
			}
			else
			{
				partial.m_Add = fval - 1.f * partial.m_Add;
				partial.m_Mult *= -1.f;
			}
			break;

		case crExpressionOpBinary::kBinaryOpTypeMultiply:
			partial.m_Mult *= fval;
			partial.m_Add *= fval;
			break;

		case crExpressionOpBinary::kBinaryOpTypeDivide:
			if(invert)
			{
				exprDebug("crExpressionOpBinary - inverted divide is unsupported"); 
				return false;
			}

			Assert(fval != 0.f);
			partial.m_Mult /= fval;
			partial.m_Add /= fval;
			break;

		default: 
			exprDebug("crExpressionOpBinary - subtype %u is unsupported", op->GetBinaryOpType()); 
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ConvertTernary(const crExpressionOpTernary* op, ScalarFunction& result)
{
	if(op->GetTernaryOpType() != crExpressionOpTernary::kTernaryOpTypeConditional)
	{
		exprDebug("crExpressionOpTernary - subtype %u is unsupported", op->GetTernaryOpType()); 
		return false;
	}

	// check condition
	const crExpressionOpBinary* condition = static_cast<const crExpressionOpBinary*>(op->GetChild(0));
	if(condition->GetOpType() != crExpressionOp::kOpTypeBinary)
	{
		exprDebug("crExpressionOpTernary - child 0 must be condition"); 
		return false;
	}

	ScalarFunction get, constant;
	if(!Convert(condition->GetChild(0), get))
		return false;

	if(get.m_Partials.GetCount()!=1 || get.m_Partials[0].IsConstant())
	{
		exprDebug("crExpressionOpTernary - condition child 0 must be variable"); 
		return false;
	}

	if(!Convert(condition->GetChild(1), constant))
		return false;

	if(!constant.IsConstant())
	{
		exprDebug("crExpressionOpTernary - condition child 1 must be constant"); 
		return false;
	}

	// mul*x+add > pivot => x > (pivot-add)/ mult
	const PartialFunction& getInterval = get.m_Partials[0];
	Assert(getInterval.m_Mult != 0.f);
	float pivot = (constant.m_Partials[0].m_Add - getInterval.m_Add) / getInterval.m_Mult;

	// append ranges from both sides
	ScalarFunction left;
	if(!Convert(op->GetChild(1), left))
		return false;

	ScalarFunction right;
	if(!Convert(op->GetChild(2), right))
		return false;

	// if the multiplier is negative, we need to swap the inequality sides
	// for example, -2x < -8 should be simplified to x > 4
	if(getInterval.m_Mult < 0.f)
	{
		SwapEm(left, right); 
	}

	switch(condition->GetBinaryOpType())
	{
	case crExpressionOpBinary::kBinaryOpTypeGreaterThan:
	case crExpressionOpBinary::kBinaryOpTypeGreaterThanEqualTo:
		break;

	case crExpressionOpBinary::kBinaryOpTypeLessThan:
	case crExpressionOpBinary::kBinaryOpTypeLessThanEqualTo:
		SwapEm(left, right); 
		break;

	default: 
		exprDebug("crExpressionOpTernary - condition subtype %u is unsupported", condition->GetBinaryOpType()); 
		return false;
	}

	int rightCount = right.m_Partials.GetCount();
	int leftCount = left.m_Partials.GetCount();
	result.m_Partials.Reserve(rightCount + leftCount);

	for(int i=0; i < rightCount; i++)
	{
		if(right.m_Variable.IsValid() && right.m_Variable != get.m_Variable)
		{
			exprDebug("crExpressionOpTernary - condition right side must use the same variable"); 
			return false;
		}

		PartialFunction& partial = result.m_Partials.Append();
		partial = right.m_Partials[i];
		partial.m_End = FPMin(pivot, partial.m_End);
	}

	for(int i=0; i < leftCount; i++)
	{
		if(left.m_Variable.IsValid() && left.m_Variable != get.m_Variable)
		{
			exprDebug("crExpressionOpTernary - condition left side must use the same variable"); 
			return false;
		}

		PartialFunction& partial = result.m_Partials.Append();
		partial = left.m_Partials[i];
		partial.m_Begin = FPMax(pivot, partial.m_Begin);
	}

	result.m_Variable = get.m_Variable;

	return true;
}

////////////////////////////////////////////////////////////////////////////////

Linearizer::Variable::Variable() 
: m_Id(0)
, m_Track(0)
, m_Type(0)
, m_Component(0)
, m_Valid(false)
{}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::Variable::operator!=(const Linearizer::Variable& other) const
{
	return m_Id != other.m_Id || m_Track != other.m_Track || m_Type != other.m_Type || m_Component != other.m_Component;
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::Variable::IsValid() const 
{ 
	return m_Valid; 
}

////////////////////////////////////////////////////////////////////////////////

Linearizer::PartialFunction::PartialFunction()
: m_Begin(-FLT_MAX)
, m_End(FLT_MAX)
, m_Add(0.f)
, m_Mult(0.f) 
{}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::PartialFunction::IsConstant() const 
{ 
	return m_Mult == 0.f; 
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ScalarFunction::IsConstant() const 
{ 
	return m_Partials.GetCount() == 1 && m_Partials[0].IsConstant(); 
}

////////////////////////////////////////////////////////////////////////////////

bool Linearizer::ScalarFunction::IsEmpty() const 
{ 
	return m_Partials.GetCount() == 0; 
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
