// 
// /makeexpressions.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "makeexpressionscore.h"

#include "animExprData/animExprData.h"
#include "cranimation/animation.h"
#include "crextra/expressions.h"
#include "crextra/expression.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(exprdata, "Expression data file (Example in.xml)");
PARAM(out, "Expression output file (Example out.expr)");
PARAM(verbose, "Optionally output expression structure");
PARAM(nooptimize, "Disable optimization step");
PARAM(nocollapse, "Disable collapse and pack step");
PARAM(nopack, "Disable pack step");
PARAM(maxpacksize, "max pack size for pack step optimization (default 65536)");
PARAM(scale, "translation scale (default 1.0)");
PARAM(translationthreshold, "special curve translation threshold (default 0.001)");
PARAM(rotationthreshold, "special curve rotation threshold (default 0.00315)");
PARAM(relativetranslation, "make translation expressions relative, not absolute (default false)");
PARAM(relativescale, "make scale expressions relative, not absolute (default false)");
PARAM(validateanyinputs, "wrap expression with input valiation, auto self disables if all inputs missing (default false)");
PARAM(validateallinputs, "wrap expression with input valiation, auto self disables if any inputs missing (default false)");
PARAM(validateoutputs, "wrap expression with output valiation, auto self disables if outputs already present (default false)");
PARAM(validateblendrate, "if expression wrapped with validation, set rate of enable/disable - (e.g. 4.0 rate == 0.25 second interpolation, default 0.0)");

#if HACK_GTA4
PARAM(exprspineshadow, "GTA5 special code generated expression");
PARAM(exprspinefixup, "GTA5 special code generated expression");
PARAM(exprvisibilityscaleclip, "GTA5 special visibility scale clip expression");
PARAM(exprmoverindependent, "GTA5 special independent mover expression");
PARAM(exprmoverindependentshadow, "GTA5 special independent mover shadow expression");
PARAM(exprrootheightfixup, "GTA5 special root height fixup expression");

#include "crclip/clip.h"
#include "crextra/expressionopmacros.h"

const int numSpineBones = 6;
u16 spineBoneIds[numSpineBones] = { 0, 57597, 23553, 24816, 24817, 24818 };
float spineBoneWeights[numSpineBones] = { 0.00f, 0.00f, 0.25f, 0.50f, 0.75f, 1.00f };
u16 facingDirBoneId = 56604;

u8 shadowTrack = kTrackBoneRotation+kTrackFirstReservedForProjectUse+0x20;
u8 scratchTrack = kTrackBoneRotation+kTrackFirstReservedForProjectUse+0x30;
u8 weightTrack = kTrackFirstReservedForProjectUse+6;
u8 independentMoverWeightTrack = kTrackFirstReservedForProjectUse+9;

void CreateSpineShadowExpression(crExpressions& expressions)
{
	// copy spine3->0, spine_root and root to shadow
	for(int i=numSpineBones-1; i>=0; --i)
	{		
		crExpressionOp* op = NULL;

		// if root not valid, use default rotated 180
		if(i==0)
		{
			crExpressionOp::Value valuePiZ;
			valuePiZ.GetVector() = Vec3V(0.f, 0.f, PI);

			op = crMakeExpressionOp::Set(
				shadowTrack, 
				spineBoneIds[i], 
				kFormatTypeQuaternion,
				crMakeExpressionOp::Ternary(
					crExpressionOpTernary::kTernaryOpTypeConditional,
					crMakeExpressionOp::ValidBoneRotation(spineBoneIds[i]),
					crMakeExpressionOp::GetBoneRotation(spineBoneIds[i]), 
					crMakeExpressionOp::Unary(
						crExpressionOpUnary::kUnaryOpTypeFromEuler,
						crMakeExpressionOp::Binary(
							crExpressionOpBinary::kBinaryOpTypeVectorAdd,
							crMakeExpressionOp::Unary(
								crExpressionOpUnary::kUnaryOpTypeToEuler,
								crMakeExpressionOp::GetDefaultBoneRotation(spineBoneIds[i])
							),
							crMakeExpressionOp::Constant(valuePiZ)
						)
					)
				)
			);
		}
		else
		{
			op = crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
				crMakeExpressionOp::ValidBoneRotation(spineBoneIds[i]),
				crMakeExpressionOp::Set(
					shadowTrack, 
					spineBoneIds[i], 
					kFormatTypeQuaternion,
					crMakeExpressionOp::GetBoneRotation(spineBoneIds[i]) 
				)
			);
		}

		crExpression expr;
		expr.SetExpressionOp(op);

		expressions.AppendExpression(expr);
	}

	crExpressionOp* op = NULL;
	op = crMakeExpressionOp::SetFloat(
		weightTrack, 
		0, 
		crMakeExpressionOp::ConstantFloat(1.0f) 
		);

	crExpression expr;
	expr.SetExpressionOp(op);

	expressions.AppendExpression(expr);
}


void CreateSpineFixupExpression(crExpressions& expressions)
{
	crExpressionOpNary* opList = rage_new crExpressionOpNary;
	opList->SetNaryOpType(crExpressionOpNary::kNaryOpTypeList);

	// copy OS spine3->0, spine_root, root to scratch
	for(int i=numSpineBones-1; i>=0; --i)
	{
		crExpressionOp* op =
			crMakeExpressionOp::Set(
				scratchTrack, 
				spineBoneIds[i], 
				kFormatTypeQuaternion,
				crMakeExpressionOp::ObjectSpaceGetBoneRotation(spineBoneIds[i]) 
			);

		opList->AppendSource(op);
	}

	// facing_dir represents the upper body rotation delta from the mover direction
	// need to apply the same delta to the shadow spine3->0 and spine_root bones before interpolating
	{
		crExpressionOp* op = crMakeExpressionOp::Binary(
			crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
			crMakeExpressionOp::ValidBoneRotation(facingDirBoneId),
			crMakeExpressionOp::Set(
				shadowTrack, 
				spineBoneIds[0], 
				kFormatTypeQuaternion,
				crMakeExpressionOp::Binary(
					crExpressionOpBinary::kBinaryOpTypeQuatMultiply,
					crMakeExpressionOp::GetBoneRotation(facingDirBoneId),
					crMakeExpressionOp::Get(
						shadowTrack,
						spineBoneIds[0],
						kFormatTypeQuaternion
						)
					)
				)
			);

		opList->AppendSource(op);
	}

	// copy LS shadow spine3->0, spine_root and root into bones
	for(int i=numSpineBones-1; i>=0; --i)
	{
		crExpressionOp* op =
			crMakeExpressionOp::SetBoneRotation(
				spineBoneIds[i], 
				crMakeExpressionOp::Get(
					shadowTrack,
					spineBoneIds[i],
					kFormatTypeQuaternion
					) 
				);

		opList->AppendSource(op);
	}

	// copy OS shadow spine3->0, spine_root and root back
	for(int i=numSpineBones-1; i>=0; --i)
	{
		crExpressionOp* op =
			crMakeExpressionOp::Set(
			shadowTrack, 
			spineBoneIds[i], 
			kFormatTypeQuaternion,
			crMakeExpressionOp::ObjectSpaceGetBoneRotation(spineBoneIds[i])
			);

		opList->AppendSource(op);
	}


	// restore root from scratch into bones
	for(int i=0; i>=0; --i)
	{
		crExpressionOp* op =
			crMakeExpressionOp::SetBoneRotation(
				spineBoneIds[i], 
				crMakeExpressionOp::Get(
					scratchTrack,
					spineBoneIds[i],
					kFormatTypeQuaternion
				) 
			);

		opList->AppendSource(op);
	}

	// interpolate OS spine3->0, spine_root with scratch
	for(int i=1; i<numSpineBones; ++i)
	{
		crExpressionOp* op = 
			crMakeExpressionOp::ObjectSpaceSetBoneRotation(
				spineBoneIds[i],
				crMakeExpressionOp::Ternary(
					crExpressionOpTernary::kTernaryOpTypeQuaternionLerp,
					crMakeExpressionOp::Get(
						scratchTrack, 
						spineBoneIds[i], 
						kFormatTypeQuaternion
						),
						crMakeExpressionOp::Get(
						shadowTrack, 
						spineBoneIds[i], 
						kFormatTypeQuaternion
						),
						crMakeExpressionOp::Binary(
						crExpressionOpBinary::kBinaryOpTypeMultiply,
						crMakeExpressionOp::ConstantFloat(spineBoneWeights[i]),
						crMakeExpressionOp::GetFloat(weightTrack, 0)
						)
					
				)
			);

		opList->AppendSource(op);
	}
	
	crExpressionOp* opCondition = NULL;
	for(int i=numSpineBones-1; i>=0; --i)
	{
		crExpressionOp* opValid = crMakeExpressionOp::Valid(shadowTrack, spineBoneIds[i], kFormatTypeQuaternion);
		if(opCondition)
		{
			opCondition = crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
				opValid,
				opCondition
				);
		}
		else
		{
			opCondition = opValid;
		}
	}

	crExpression expr;
	expr.SetExpressionOp(
		crMakeExpressionOp::Binary(
			crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
			opCondition,
			opList
		)
	);

	expressions.AppendExpression(expr);
}

void CreateVisibilityScaleClipExpression(const crClip& clip, crExpressions& expressions)
{
	crExpressionOpNary opList;
	opList.SetNaryOpType(crExpressionOpNary::kNaryOpTypeList);

	crFrameData frameData;
	clip.InitDofs(frameData);

	for(u32 i=0; i<frameData.GetNumDofs(); ++i)
	{
		u32 trackId = frameData.GetDof(i).GetTrackId();

		u16 track = u16(trackId>>16);
		u16 id = u16(trackId&0xffff);

		switch(track)
		{
		case kTrackVisibility:
			if(frameData.HasDof(kTrackBoneScale, id))
			{
				crExpressionOp* op = 
					crMakeExpressionOp::Binary(crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
						crMakeExpressionOp::Valid(kTrackVisibility, id, kFormatTypeFloat),
							crMakeExpressionOp::Binary(crExpressionOpBinary::kBinaryOpTypeLogicalAnd, 
								crMakeExpressionOp::Binary(crExpressionOpBinary::kBinaryOpTypeLessThan,
									crMakeExpressionOp::Get(kTrackVisibility, id, kFormatTypeFloat),
									crMakeExpressionOp::ConstantFloat(0.5f)
								),
							crMakeExpressionOp::SetBoneScale(id, 
								crMakeExpressionOp::Constant(crExpressionOp::Value(Vec3V(V_ZERO))
							)
						)
					)
				);

				opList.AppendSource(op);
			}
			break;

		default:
			break;
		}
	}

	if(opList.GetNumChildren() > 0)
	{
		crExpression expr;
		expr.SetExpressionOp(opList.Clone());

		expressions.AppendExpression(expr);
	}
}

u8 independentMoverTrack = kTrackFirstReservedForProjectUse+7;

enum {
	kIndependentMoverRotFixup=0 // the rotation fixup to apply to the mover (and counter on the root)
};

void CreateIndependentMoverExpression(crExpressions& expressions)
{
	crExpression expr;
	crExpressionOp* op = NULL;

	const u32 bRotationFixupInitializedHash = atStringHash("bRotationFixupInitialized");
	const u32 qRotationFixupHash = atStringHash("qRotationFixup");
	const u32 fRotationFixupAngleHash = atStringHash("fRotationFixupAngle");
	const u8 kComponentZ = 2;

	//	If bRotationFixupInitialized = True Then
	//		If kNullaryOpTypeDeltaTime > 0 And fRotationFixupAngle != qRotationFixup_In.GetAngle() Then
	//			qRotationFixup = qRotationFixup * qRotationFixup_In
	//			fRotationFixupAngle = qRotationFixup_In.GetAngle()
	//		End
	//	Else
	//		qRotationFixup = qRotationFixup_In
	//		fRotationFixupAngle = qRotationFixup_In.GetAngle()
	//		bRotationFixupInitialized = True
	//	End

	crExpressionOpNary* opListRotationFixupAccumulate = rage_new crExpressionOpNary;
	opListRotationFixupAccumulate->SetNaryOpType(crExpressionOpNary::kNaryOpTypeList);

	op = crMakeExpressionOp::SetVariable(
			qRotationFixupHash,
			crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeQuatMultiply,
				crMakeExpressionOp::GetVariable(qRotationFixupHash),
				crMakeExpressionOp::Get(independentMoverTrack, kIndependentMoverRotFixup, kFormatTypeQuaternion)
				)
			);
	opListRotationFixupAccumulate->AppendSource(op);

	op = crMakeExpressionOp::SetVariable(fRotationFixupAngleHash, crMakeExpressionOp::ComponentGet(independentMoverTrack, kIndependentMoverRotFixup, kFormatTypeQuaternion, kComponentZ, EULER_XYZ));
	opListRotationFixupAccumulate->AppendSource(op);

	crExpressionOpNary* opListRotationFixupInitialized = rage_new crExpressionOpNary;
	opListRotationFixupInitialized->SetNaryOpType(crExpressionOpNary::kNaryOpTypeList);

	op = crMakeExpressionOp::Binary(
			crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
			crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeGreaterThan,
				crMakeExpressionOp::Nullary(crExpressionOpNullary::kNullaryOpTypeDeltaTime),
				crMakeExpressionOp::Nullary(crExpressionOpNullary::kNullaryOpTypeZero)
				),
			crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
				crMakeExpressionOp::Binary(
					crExpressionOpBinary::kBinaryOpTypeNotEqualTo,
					crMakeExpressionOp::GetVariable(fRotationFixupAngleHash),
					crMakeExpressionOp::ComponentGet(independentMoverTrack, kIndependentMoverRotFixup, kFormatTypeQuaternion, kComponentZ, EULER_XYZ)
					),
				opListRotationFixupAccumulate
				)
			);
	opListRotationFixupInitialized->AppendSource(op);

	crExpressionOpNary* opListRotationFixupNotInitialized = rage_new crExpressionOpNary;
	opListRotationFixupNotInitialized->SetNaryOpType(crExpressionOpNary::kNaryOpTypeList);

	op = crMakeExpressionOp::SetVariable(qRotationFixupHash, crMakeExpressionOp::Get(independentMoverTrack, kIndependentMoverRotFixup, kFormatTypeQuaternion));
	opListRotationFixupNotInitialized->AppendSource(op);

	op = crMakeExpressionOp::SetVariable(fRotationFixupAngleHash, crMakeExpressionOp::ComponentGet(independentMoverTrack, kIndependentMoverRotFixup, kFormatTypeQuaternion, kComponentZ, EULER_XYZ));
	opListRotationFixupNotInitialized->AppendSource(op);

	op = crMakeExpressionOp::SetVariable(bRotationFixupInitializedHash, crMakeExpressionOp::Nullary(crExpressionOpNullary::kNullaryOpTypeOne));
	opListRotationFixupNotInitialized->AppendSource(op);

	op = crMakeExpressionOp::Ternary(
			crExpressionOpTernary::kTernaryOpTypeConditional,
			crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeGreaterThan,
				crMakeExpressionOp::GetVariable(bRotationFixupInitializedHash),
				crMakeExpressionOp::Nullary(crExpressionOpNullary::kNullaryOpTypeZero)
				),
			opListRotationFixupInitialized,
			opListRotationFixupNotInitialized
			);
	expr.SetExpressionOp(op);
	expressions.AppendExpression(expr);

	// Accumulate skeleton ops
	crExpressionOpNary* opListSkeleton = rage_new crExpressionOpNary;
	opListSkeleton->SetNaryOpType(crExpressionOpNary::kNaryOpTypeList);

	// Align the mover translation to the desired rotation
	op = crMakeExpressionOp::SetMoverTranslation(
			crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeVectorTransform,
				crMakeExpressionOp::GetMoverTranslation(),
				crMakeExpressionOp::Unary(
					crExpressionOpUnary::kUnaryOpTypeQuatInverse,
					crMakeExpressionOp::GetVariable(qRotationFixupHash)
					)
				)
			);
	opListSkeleton->AppendSource(op);

	// Counter rotate the root to compensate
	op = crMakeExpressionOp::SetBoneRotation(
			0,
			crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeQuatMultiply,
				crMakeExpressionOp::Unary(
					crExpressionOpUnary::kUnaryOpTypeQuatInverse,
					crMakeExpressionOp::GetVariable(qRotationFixupHash)
					),
				crMakeExpressionOp::GetBoneRotation(0)
				)
			);
	opListSkeleton->AppendSource(op);

	// Counter rotate the facing_dir to compensate
	op = crMakeExpressionOp::Binary(
			crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
			crMakeExpressionOp::ValidBoneRotation(facingDirBoneId),
			crMakeExpressionOp::SetBoneRotation(
				facingDirBoneId,
				crMakeExpressionOp::Binary(
					crExpressionOpBinary::kBinaryOpTypeQuatMultiply,
					crMakeExpressionOp::Unary(
						crExpressionOpUnary::kUnaryOpTypeQuatInverse,
						crMakeExpressionOp::GetVariable(qRotationFixupHash)
						),
					crMakeExpressionOp::GetBoneRotation(facingDirBoneId)
					)
				)
			);
	opListSkeleton->AppendSource(op);

	// Transform the root bone translation as well
	op = crMakeExpressionOp::SetBoneTranslation(
			0,
			crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeVectorTransform,
				crMakeExpressionOp::GetBoneTranslation(0),
				crMakeExpressionOp::Unary(
					crExpressionOpUnary::kUnaryOpTypeQuatInverse,
					crMakeExpressionOp::GetVariable(qRotationFixupHash)
					)
				)
			);
	opListSkeleton->AppendSource(op);

	// Only apply the skeleton ops if the expression has advanced in TIME (expression could have been started with a timestep of 0 so do nothing)
	op = crMakeExpressionOp::Binary(
			crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
			crMakeExpressionOp::Binary(
				crExpressionOpBinary::kBinaryOpTypeGreaterThan,
				crMakeExpressionOp::Nullary(crExpressionOpNullary::kNullaryOpTypeTime),
				crMakeExpressionOp::Nullary(crExpressionOpNullary::kNullaryOpTypeZero)
				),
			opListSkeleton
			);

	expr.SetExpressionOp(op);
	expressions.AppendExpression(expr);
}

// just sets the weight of the independent mover in the tree (so we can blend down the 
// mover pop accordingly when the pose pop is being dominated by higher level animation).
void CreateIndependentMoverShadowExpression(crExpressions& expressions)
{
	crExpression expr;
	crExpressionOp* op = NULL;

	// Set the independent mover weight dof
	op = crMakeExpressionOp::Set( independentMoverWeightTrack, 0, kFormatTypeFloat, crMakeExpressionOp::ConstantFloat(1.0f) );
	
	expr.SetExpressionOp(op);
	expressions.AppendExpression(expr);
}

void CreateRootHeightFixupExpression(crExpressions& expressions)
{
	const u16 kRootID = 0;
	const u16 kRootHeightID = 0;
	const u8  kTrackRootHeight = 136;
	const u8  kComponentZ = 2;

	crExpression expr;
	crExpressionOp* op = NULL;

	op = crMakeExpressionOp::Binary(
			crExpressionOpBinary::kBinaryOpTypeLogicalAnd,
			crMakeExpressionOp::Valid(kTrackRootHeight, kRootHeightID, kFormatTypeFloat),
			crMakeExpressionOp::ComponentSetBoneTranslation(
				kRootID, 
				kComponentZ, 
				crMakeExpressionOp::Binary(
					crExpressionOpBinary::kBinaryOpTypeAdd,
					crMakeExpressionOp::ComponentGetBoneTranslation(kRootID, kComponentZ),
					crMakeExpressionOp::Binary(
						crExpressionOpBinary::kBinaryOpTypeSubtract,
						crMakeExpressionOp::ComponentGetDefaultBoneTranslation(kRootID, kComponentZ),
						crMakeExpressionOp::GetFloat(kTrackRootHeight, kRootHeightID)
						)
					)
				)
			);

	expr.SetExpressionOp(op);
	expressions.AppendExpression(expr);
}
#endif // HACK_GTA4

int Main()
{
	INIT_PARSER;

	crAnimation::InitClass();
	crExpressions::InitClass();
#if HACK_GTA4
	crClip::InitClass();
#endif // HACK_GTA4

	// process help parameter
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process scale parameter
	float scale = 1.f;
	if(PARAM_scale.Get(scale))
	{
		if(scale <= 0.f)
		{
			Quitf("ERROR - bad scale value %f, must be > 0.0", scale);
		}
		MakeExpressions::g_TranslationScale = scale;
	}

	// process translation threshold parameter
	float translationThreshold = 0.001f;
	if(PARAM_translationthreshold.Get(translationThreshold))
	{
		if(translationThreshold <= 0.f)
		{
			Quitf("ERROR - bad translation threshold value %f, must be > 0.0", translationThreshold);
		}
		MakeExpressions::g_SpecialCurveOptimizationTranslationThreshold = translationThreshold;
	}

	// process rotation threshold parameter
	float rotationThreshold = 0.00315f;
	if(PARAM_rotationthreshold.Get(rotationThreshold))
	{
		if(rotationThreshold <= 0.f)
		{
			Quitf("ERROR - bad rotation threshold value %f, must be > 0.0", rotationThreshold);
		}
		MakeExpressions::g_SpecialCurveOptimizationRotationThreshold = rotationThreshold;
	}

	// process relative parameters
	MakeExpressions::g_RelativeTranslation = PARAM_relativetranslation.Get();
	MakeExpressions::g_RelativeScale = PARAM_relativescale.Get();

	// process expression data parameter
	const char* exprDataFilename = NULL;
	if(!PARAM_exprdata.Get(exprDataFilename))
	{
#if HACK_GTA4
		if(!PARAM_exprspineshadow.Get() && !PARAM_exprspinefixup.Get() && !PARAM_exprvisibilityscaleclip.Get() && !PARAM_exprmoverindependent.Get() && !PARAM_exprrootheightfixup.Get()&& !PARAM_exprmoverindependentshadow.Get())
#endif // HACK_GTA4
		Quitf("ERROR - no expression data file specified");
	}

	// load the expression data file
	AeFile exprData;
#if HACK_GTA4
	if(!PARAM_exprspineshadow.Get() && !PARAM_exprspinefixup.Get() && !PARAM_exprvisibilityscaleclip.Get() && !PARAM_exprmoverindependent.Get() && !PARAM_exprrootheightfixup.Get() && !PARAM_exprmoverindependentshadow.Get())
#endif // HACK_GTA4
	if(!PARSER.LoadObject<AeFile>(exprDataFilename, "", exprData))
	{
		Quitf("ERROR - failed to load expression data '%s'", exprDataFilename);
	}

	bool saveExpression = true;

	// parse the expression data
	crExpressions expressions;
#if HACK_GTA4
	if(PARAM_exprspineshadow.Get())
	{
		CreateSpineShadowExpression(expressions);
	}
	else if(PARAM_exprspinefixup.Get())
	{
		CreateSpineFixupExpression(expressions);
	}
	else if(PARAM_exprmoverindependent.Get())
	{
		CreateIndependentMoverExpression(expressions);
	}
	else if (PARAM_exprmoverindependentshadow.Get())
	{
		CreateIndependentMoverShadowExpression(expressions);
	}
	else if(PARAM_exprvisibilityscaleclip.Get())
	{
		const char* clipFilename = NULL;
		if(!PARAM_exprvisibilityscaleclip.Get(clipFilename))
		{
			Quitf("ERROR - no visibility scale clip file specified");
		}

		crClip* clip = crClip::AllocateAndLoad(clipFilename);
		if(clip)
		{
			CreateVisibilityScaleClipExpression(*clip, expressions);
			delete clip;

			if(expressions.GetNumExpressions() == 0)
			{
				Displayf("No visibility/scale tracks found in '%s', no visibility scale clip expression needed", clipFilename);
				saveExpression = false;
			}
		}
		else
		{
			Quitf("ERROR - failed to load clip '%s'", clipFilename);
		}
	}
	else if (PARAM_exprrootheightfixup.Get())
	{
		CreateRootHeightFixupExpression(expressions);
	}
	else
#endif // HACK_GTA4
	if(!MakeExpressions::ConvertExpressionData(exprData, expressions))
	{
		Quitf("ERROR - failed to parse the expression data");
	}
	
	// optionally optimize the expression data
	if(!PARAM_nooptimize.Get())
	{
		bool noCollapse = PARAM_nocollapse.Get();

		MakeExpressions::OptimizeExpressionData(expressions, !noCollapse, true);
	}

	// optionally wrap with input validation
	if(PARAM_validateallinputs.Get() || PARAM_validateanyinputs.Get() || PARAM_validateoutputs.Get())
	{
		bool input = PARAM_validateallinputs.Get() || PARAM_validateanyinputs.Get();
		bool output = PARAM_validateoutputs.Get();
		bool all = PARAM_validateallinputs.Get();

		float blendRate = 0.f;
		PARAM_validateblendrate.Get(blendRate);

		MakeExpressions::AddInputOutputValidation(expressions, input, output, all, blendRate);
	}

	// optionally pack the expression data
	if(!PARAM_nopack.Get())
	{
		u32 maxPackSize = 64*1024;
		PARAM_maxpacksize.Get(maxPackSize);

		MakeExpressions::PackExpressionData(expressions, maxPackSize);
	}

	// optionally dump expression structures to output
	if(PARAM_verbose.Get())
	{
		int verbosity = 2;
		PARAM_verbose.Get(verbosity);

		expressions.Dump(verbosity);
	}


	//  process expression output parameter
	const char* outFilename = NULL;
	if(!PARAM_out.Get(outFilename))
	{
		Quitf("ERROR - no expression output file specified");
	}

	// serialize the expressions
	if(saveExpression)
	{ 
		if(!expressions.Save(outFilename))
		{
			Quitf("ERROR - failed to save expression output '%s'", outFilename);
		}
	}

#if HACK_GTA4
	crClip::ShutdownClass();
#endif // HACK_GTA4
	crExpressions::ShutdownClass();
	crAnimation::ShutdownClass();

	SHUTDOWN_PARSER;
	
	return 0;
}
