#include "makeexpressionsrules.h"

#include "makeexpressionscore.h"

#include "atl/string.h"
#include "cranimation/animtrack.h"
#include "crextra/expression.h"
#include "crextra/expressions.h"
#include "crextra/expressionops.h"

#include <string>
#include <sstream>
#include <iostream>

namespace rage
{

namespace MakeExpressionsRules 
{

////////////////////////////////////////////////////////////////////////////////

enum AttributeSetResult
{
	kAttributeSetSucceeded,
	kAttributeNotFound,
	kAttributeIndexOutOfRange,
	kAttributeInvalidOp,
};

////////////////////////////////////////////////////////////////////////////////

struct ArithmeticOperation;

class ExpressionRuleContext;
class Variant;

////////////////////////////////////////////////////////////////////////////////

static AttributeSetResult SetAttribute(crExpressionOp& op, const char* attribute, Variant* variant);
static AttributeSetResult SetAttributeFromIndex(crExpressionOp& op, int index, Variant* variant);
static bool HasAttribute(const crExpressionOp& op, const char* attribute, Variant*& variant);

Variant* ExecuteArithmetic(atArray<ArithmeticOperation*>&, ExpressionRuleContext&);

////////////////////////////////////////////////////////////////////////////////

const float g_OperatorValuesMatchThreshold = 0.0001F;

////////////////////////////////////////////////////////////////////////////////
/*
	Rules are of the form '[Rule]*:[Substitution]' where a substitution of the tree on the right hand side
	is inserted into the expression tree if all rules are passed. Each rule generates a list of expression ops
	for each operation that passed the rule. This is the input into the following rule. The Substitution cannot
	create any new lists, therefor all items it requires must be addressable from the resultant expressions op
	lists from the rules evaluation. If a rule failed to generate any results further testing on the current
	expression op with the current rule is terminated and a substitution will not be made.

	Notation (Rule)

	The first rule must be a type. 
	
	Available types are:
		OpTypeConstant
		OpTypeConstantFloat
		OpTypeGet
		OpTypeSet
		OpTypeInvalid
		OpTypeComponentGet
		OpTypeComponentSet
		OpTypeObjectSpaceGet
		OpTypeObjectSpaceSet
		OpTypeObjectSpaceConvertTo
		OpTypeObjectSpaceConvertFrom
		NullaryOpTypeZero
		NullaryOpTypeOne
		NullaryOpTypePi
		NullaryOpTypeTime
		NullaryOpTypeRandom
		NullaryOpTypeVectorZero
		NullaryOpTypeVectorOne
		UnaryOpTypeLogicalNot
		UnaryOpTypeNegate
		UnaryOpTypeReciprocal
		UnaryOpTypeSquare
		UnaryOpTypeSqrt
		UnaryOpTypeAbsolute
		UnaryOpTypeFloor
		UnaryOpTypeCeil
		UnaryOpTypeLog
		UnaryOpTypeLn
		UnaryOpTypeExp
		UnaryOpTypeClamp01
		UnaryOpTypeCos
		UnaryOpTypeSin
		UnaryOpTypeTan
		UnaryOpTypeArcCos
		UnaryOpTypeArcSin
		UnaryOpTypeArcTan
		UnaryOpTypeCosH
		UnaryOpTypeSinH
		UnaryOpTypeTanH
		UnaryOpTypeDegreesToRadians
		UnaryOpTypeRadiansToDegrees
		UnaryOpTypeFromEuler
		UnaryOpTypeToEuler
		UnaryOpTypeSplat
		UnaryOpTypeVectorClamp01
		UnaryOpTypeQuatInverse
		BinaryOpTypeEqualTo
		BinaryOpTypeNotEqualTo
		BinaryOpTypeGreaterThan
		BinaryOpTypeLessThan
		BinaryOpTypeGreaterThanEqualTo
		BinaryOpTypeLessThanEqualTo
		BinaryOpTypeAdd
		BinaryOpTypeSubtract
		BinaryOpTypeMultiply
		BinaryOpTypeDivide
		BinaryOpTypeModulus
		BinaryOpTypeExponent
		BinaryOpTypeMax
		BinaryOpTypeMin
		BinaryOpTypeLogicalAnd
		BinaryOpTypeLogicalOr
		BinaryOpTypeLogicalXor
		BinaryOpTypeQuatMultiply
		BinaryOpTypeVectorAdd
		BinaryOpTypeVectorMultiply
		BinaryOpTypeVectorTrasform
		TernaryOpTypeMultiplyAdd
		TernaryOpTypeClamp
		TernaryOpTypeLerp
		TernaryOpTypeToQuaternion
		TernaryOpTypeToVector
		TernaryOpTypeConditional
		TernaryOpTypeVectorMultiplyAdd
		TernaryOpTypeVectorLerpVector
		NaryOpTypeComma
		NaryOpTypeSum
		NaryOpTypeList

	following the first rule you can have any number of rules either filtering again by type, attribute, special keyword or
	referencing preceding rule results.

	The special keywords are:
		children	- expands the child input of each of the expression op items in the rules input list
		length		- passes through the input list to the rule as the output list if it's length == length
		union		- performs a union of the input list with keywords parameter list (% : see below)
		xunion		- performs an exclusive union of the input list with the keywords parameter list (% : see below)

	Attributes are specific to node type. If an attribute for a list item doesn't exists it is marked as a fail for that
	item and it will no longer be involved in the rule processing. Attribute addressing is of the form @attribute=value.
	value is dependent on the attribute involved. @attribute= is a special case form to test the equivalency of all list
	items.

	Attributes available on nodes are:

		OpTypeConstant:
			vec3, quat, float, int

		OpTypeConstantFloat:
			float

		OpTypeGet:
			track, type, id, accelerated_index, relative
		OpTypeSet:
			track, type, id, accelerated_index, relative
		OpTypeObjectSpaceGet:
			track, type, id, accelerated_index, relative
		OpTypeObjectSpaceSet:
			track, type, id, accelerated_index, relative
		OpTypeObjectSpaceConvertTo:
			track, type, id, accelerated_index, relative
		OpTypeObjectSpaceConvertFrom:
			track, type, id, accelerated_index, relative

		OpTypeComponentGet:
			track, type, id, accelerated_index, relative, component
		OpTypeComponentSet:
			track, type, id, accelerated_index, relative, component

		Valid values for attributes are:

		Special operators that can be used to reference preceding rule results as list or list elements are:
			- The list operation %. Used of the form %x where x is a zero based index referring to the 
			  list you want to index. The result is a list
		   - The previous list operation $. used of the form $x where x is a zero based index into the input list
		     to the rule. Eg. $0 will return the first item from the input list to the rule. The result is a list
			 of 1 item.

		   Note: Both operators are also valid in the substitution side of the rule.

	Sometimes it is convenient for processing to continue if a rule fails. Using the ~[] operator will achieve this.

	Notation (Substitution)

	The substitution build a tree op operations. It does this with a combination of [] and ->. [A->[B,C,D]] would
	create a tree with A as the root node and B,C,D as its children. Attributes can be initialized using the form
	A{attribute=value, attribute2=value2} where value(2) can be a constant, or refer to another items attribute
	in a rule result list somewhere.

	Created nodes can either come from an operator or node type. If it is via node type it will be either of the form of
	the previous list index ($0) or indexed item from a rule result list (%4.(0) - get the first item from the result of the
	forth rule. Available node types are:

		OpTypeConstant
		OpTypeConstantFloat
		OpTypeGet
		OpTypeSet
		OpTypeComponentGet
		OpTypeComponentSet
		NullaryOpTypeZero
		NullaryOpTypeOne
		NullaryOpTypePi
		NullaryOpTypeTime
		NullaryOpTypeRandom
		NullaryOpTypeVectorZero
		NullaryOpTypeVectorOne
		UnaryOpTypeLogicalNot
		UnaryOpTypeNegate
		UnaryOpTypeReciprocal
		UnaryOpTypeSquare
		UnaryOpTypeSqrt
		UnaryOpTypeAbsolute
		UnaryOpTypeFloor
		UnaryOpTypeCeil
		UnaryOpTypeLog
		UnaryOpTypeLn
		UnaryOpTypeExp
		UnaryOpTypeClamp01
		UnaryOpTypeCos
		UnaryOpTypeSin
		UnaryOpTypeTan
		UnaryOpTypeArcCos
		UnaryOpTypeArcSin
		UnaryOpTypeArcTan
		UnaryOpTypeCosH
		UnaryOpTypeSinH
		UnaryOpTypeTanH
		UnaryOpTypeDegreesToRadians
		UnaryOpTypeRadiansToDegrees
		UnaryOpTypeFromEuler
		UnaryOpTypeToEuler
		UnaryOpTypeSplat
		UnaryOpTypeVectorClamp01
		UnaryOpTypeQuatInverse
		BinaryOpTypeEqualTo
		BinaryOpTypeNotEqualTo
		BinaryOpTypeGreaterThan
		BinaryOpTypeLessThan
		BinaryOpTypeGreaterThanEqualTo
		BinaryOpTypeLessThanEqualTo
		BinaryOpTypeAdd
		BinaryOpTypeSubtract
		BinaryOpTypeMultiply
		BinaryOpTypeDivide
		BinaryOpTypeModulus
		BinaryOpTypeExponent
		BinaryOpTypeMax
		BinaryOpTypeMin
		BinaryOpTypeLogicalAnd
		BinaryOpTypeLogicalOr
		BinaryOpTypeLogicalXor
		BinaryOpTypeQuatMultiply
		BinaryOpTypeVectorAdd
		BinaryOpTypeVectorMultiply
		BinaryOpTypeVectorTransform
		TernaryOpTypeMultiplyAdd
		TernaryOpTypeClamp
		TernaryOpTypeLerp
		TernaryOpTypeToQuaternion
		TernaryOpTypeToVector
		TernaryOpTypeConditional
		TernaryOpTypeVectorMultiplyAdd
		TernaryOpTypeVectorLerpVector
		NaryOpTypeComma
		NaryOpTypeSum
		NaryOpTypeList
		OpTypeConstantVector
		OpTypeConstantQuat

	Arithmetic is also possible within an initialization. It takes the form float=[expression].
	[] is required notation for the parser to differentiate between a regular assignment and an arithmetic expression.

	Example:	Transform and Add followed by a Multiply into a MultiplyAdd

	[BinaryOpTypeAdd][children][BinaryOpTypeMultiply][$0][xunion(%1)][%3][children] : [TernaryOpTypeMultiplyAdd->[$0, $1, %4.(0)]]

	1) We want to only operate on BinaryAdd operators [BinaryOpTypeAdd]
	2) Multiply will be a child of the Binary Add so we create a list of children: [children]
	3) If one of those children is a BinaryMultiply continue: [BinaryOpTypeMultiply]
	4) Both inputs might be of type BinaryOpTypeMultiply, so take the first: [$0]
	5) The non-BinaryOpTypeMultiply input into BinaryOpTypeAdd is required to build the MultiplyAdd, so make sure we can get at it: [xunion(%1)]
	6) The children to the BinaryOpTypeMultily are also needed for the MulitplyAdd, so reinsert the BinaryOpTypeMultiply so it can be operated on: [%3]
	7) Expand the Multiply's children so they're accessible: [children]
	8) TernaryOpTypeMulitplyAdd takes 3 inputs. The two multiplication operands followed by the addition operand. The multiplication inputs
		are in the last list generated by the last rule, so they can be accessed through $ ($0 and $1 specifically). The other input
		to the existing addition is available in the 4th list and is the first element, so it can be accessed through %4.(0). Putting all this
		together we get the resulting substitution: [TernaryOpTypeMultiplyAdd->[$0, $1, %4.(0)]]

*/

static const char* g_Rules[] = 
{
/*y*/	"[OpTypeComponentGet][@type=Float] : [OpTypeGet{track=$0.@track, id=$0.@id, type=$0.@type}]",
		"[OpTypeComponentSet][@type=Float] : [OpTypeSet {track=$0.@track, id=$0.@id, type=$0.@type} ]",
/*y*/	"[OpTypeConstantFloat][@float=0.0] : [NullaryOpTypeZero]",
/*y*/	"[OpTypeConstantFloat][@float=1.0] : [NullaryOpTypeOne]",
/*y*/	"[OpTypeConstantFloat][@float=PI] : [NullaryOpTypePi]",
		"[UnaryOpTypeDegreesToRadians][children][UnaryOpTypeRadiansToDegrees][children] : [$0]",
		"[UnaryOpTypeRadiansToDegrees][children][UnaryOpTypeDegreesToRadians][children] : [$0]",
		"[BinaryOpTypeDivide][children][$0][OpTypeConstantFloat][@float=0.0] : [NullaryOpZero]",
/*y*/	"[BinaryOpTypeDivide][children][$1][OpTypeConstantFloat][@float!=0.0] : [BinaryOpTypeMultiply->[%1.(0), OpTypeConstantFloat {float=[1.0/$0.@float]}]]",
/*y*/	"[BinaryOpTypeMultiply][children][OpTypeConstantFloat][@float=0.0] : [NullaryOpTypeZero]",
		"[BinaryOpTypeMultiply][children][OpTypeConstantFloat][@float=1.0][$0][xunion(%1)] : [$0]",
/*y*/	"[BinaryOpTypeMultiply][children][OpTypeConstantFloat][@float=-1.0][$0][xunion(%1)] : [UnaryOpTypeNegate->[$0]]",
/*y*/	"[BinaryOpTypeAdd][children][OpTypeConstantFloat][@float=0.0][$0][xunion(%1)] : [$0]",
/*y*/	"[BinaryOpTypeAdd][children][BinaryOpTypeMultiply][$0][xunion(%1)][%3][children] : [TernaryOpTypeMultiplyAdd->[$0, $1, %4.(0)]]",
		"[BinaryOpTypeSubtract][children][$1][OpTypeConstantFloat][@float=0.0] : [%1.(0)]",
		"[BinaryOpTypeSubtract][children][$0][OpTypeConstantFloat][@float=0.0] : [UnaryOpTypeNegate->[%1.(1)]]",
/*y*/	"[BinaryOpTypeQuatMultiply][children][OpTypeConstant][@quat={0.0, 0.0, 0.0, 1.0}][$0][xunion(%1)] : [$0]",
		"[BinaryOpTypeVectorAdd][children][$0][OpTypeConstant][@fx=0.0][@fy=0.0][@fz=0.0][xunion(%1)] : [$0]",
		"[BinaryOpTypeVectorAdd][children][$1][OpTypeConstant][@fx=0.0][@fy=0.0][@fz=0.0][xunion(%0)] : [$0]",
/*y*/	"[TernaryOpTypeToVector][children][OpTypeComponentGet][@track=][@id=][@type=][length=3][$0.@component=0][%5][$1.@component=1][%5][$2.@component=2] : [OpTypeGet {track=$0.@track, id=$0.@id, type=$0.@type}]",
/*y*/	"[TernaryOpTypeToVector][children][BinaryOpTypeAdd][length=3][children] : [BinaryOpTypeVectorAdd->[TernaryOpTypeToVector->[$0, $2, $4], TernaryOpTypeToVector->[$1, $3, $5]]]",
/*y*/	"[TernaryOpTypeToVector][children][BinaryOpTypeMultiply][length=3][children] : [BinaryOpTypeVectorMultiply->[TernaryOpTypeToVector->[$0, $2, $4], TernaryOpTypeToVector->[$1, $3, $5]]]",
		"[TernaryOpTypeToVector][children][TernaryOpTypeMultiplyAdd][length=3][children] : [TernaryOpTypeVectorMultiplyAdd->[TernaryOpTypeToVector->[$0, $3, $6], TernaryOpTypeToVector->[$1, $4, $7], TernaryOpTypeToVector->[$2, $5, $8]]]",
/*y*/	"[TernaryOpTypeToVector][children][OpTypeConstantFloat][length=3] : [OpTypeConstantVector {$0.@float, $1.@float, $2.@float}]",
/*y*/	"[TernaryOpTypeToQuaternion][children][OpTypeConstantFloat][length=3] : [OpTypeConstantQuat {$0.@float, $1.@float, $2.@float}]",
		"[NaryOpTypeSum]~[children] : [NullaryOpTypeZero]",
/*y*/	"[NaryOpTypeSum][children][length=1] : [$0]",
/*y*/	"[NaryOpTypeSum][children][length=2] : [BinaryOpTypeAdd->[$0,$1]]",
/*y*/	"[NaryOpTypeSum][children][NullaryOpTypeZero][xunion(%1)] : [NaryOpTypeSum->($)]",
		"[NaryOpTypeSum][children][OpTypeConstantFloat][xunion(%1)][%2.sum][union(%3)] : [NaryOpTypeSum->($)]",

		/* Add these to the end so they don't throw off my debugging indexes. This starts at index 28*/
/*y*/	"[BinaryOpTypeDivide][children][$1][NullaryOpTypeOne][xunion(%1)] : [$0]",
/*y*/	"[BinaryOpTypeMultiply][children][NullaryOpTypeZero] : [NullaryOpTypeZero]",
		/* TODO: If one of the ternary multiplies in Zero, promote the add */
		/* TODO: If one of the ternary multiplies is one, just do a binary add */
/*y*/	"[TernaryOpTypeMultiplyAdd][children][$2][NullaryOpTypeZero][xunion(%1)] : [BinaryOpTypeMultiply->[$0, $1]]",
		"[TernaryOpTypeMultiplyAdd][children][$1][NullaryOpTypeOne][xunion(%1)] : [BinaryOpTypeAdd->[$0, $1]]",
/*y*/	"[BinaryOpTypeAdd][children][NullaryOpTypeZero][$0][xunion(%1)] : [$0]",
/*y*/	"[BinaryOpTypeMultiply][children][NullaryOpTypeOne][$0][xunion(%1)] : [$0]",
/*y*/	"[BinaryOpTypeQuatMultiply][children][OpTypeConstant][@quat={0.0, 0.0, 0.0, -1.0}][$0][xunion(%1)] : [$0]",
/*y*/	"[BinaryOpTypeSubtract][children][$1][NullaryOpTypeZero][xunion(%1)] : [$0]",
/*y*/	"[TernaryOpTypeMultiplyAdd][children][OpTypeConstantFloat][length=3] : [OpTypeConstantFloat{float=[($0.@float*$1.@float)+$2.@float]}]",
/*y*/	"[BinaryOpTypeMultiply][children][OpTypeConstantFloat][length=2] : [OpTypeConstantFloat{float=[$0.@float*$1.@float]}]",
/*y*/	"[BinaryOpTypeSubtract][children][OpTypeConstantFloat][length=2] : [OpTypeConstantFloat{float=[$0.@float-$1.@float]}]",
/*y*/	"[BinaryOpTypeMultiply][children][BinaryOpTypeMultiply][children][OpTypeConstantFloat][xunion(%3)][%2][xunion(%1)][OpTypeConstantFloat] : [BinaryOpTypeMultiply->[OpTypeConstantFloat{float=[%4(0).@float * $0.@float]}, %5.(0)]]",
/*y*/	"[BinaryOpTypeMultiply][children][OpTypeConstantFloat][xunion(%1)][UnaryOpTypeNegate][children] : [BinaryOpTypeMultiply->[OpTypeConstantFloat{float=-%2(0).@float}, $0]]",
/*y*/	"[BinaryOpTypeMin][children][BinaryOpTypeMax][children][OpTypeConstantFloat][xunion(%3)][%2][xunion(%1)] : [TernaryOpTypeClamp->[%5.(0), %4.(0), $0]]",
/*y*/	"[UnaryOpTypeNegate][children][NullaryOpTypeZero] : [$0]",
/*y*/	"[UnaryOpTypeDegreesToRadians][children][NullaryOpTypeZero] : [$0]",
		"[UnaryOpTypeRadiansToDegrees][children][NullaryOpTypeZero] : [$0]",
/*y*/	"[TernaryOpTypeConditional][children][$0][xunion(%1)][NullaryOpTypeZero][length=2] : [NullaryOpTypeZero]",
/*y*/	"[TernaryOpTypeConditional][children][$0][children][$0][BinaryOpTypeMultiply][children][OpTypeConstantFloat][@float>=0.0][%3][$1][NullaryOpTypeZero][%6][xunion(%7)][%1][xunion(%2)] : [%0.(0)->[%2.(0)->[%13.(0) , %11.(0)] , $0, $1]]",
#if 0
/*y*/	"[TernaryOpTypeConditional][children][$0][children][$0][BinaryOpTypeMultiply][children][OpTypeConstantFloat][%3][$1][OpTypeConstantFloat][%6][xunion(%7)][%1][xunion(%2)] : [%0.(0)->[%2.(0)->[%12.(0) , OpTypeConstantFloat{float=[%10(0).@float / %7(0).@float]}] , $0, $1]]",
#endif
/*y*/	"[UnaryOpTypeDegreesToRadians][children][BinaryOpTypeMultiply][children][OpTypeConstantFloat][xunion(%3)] : [BinaryOpTypeMultiply->[OpTypeConstantFloat{float=[%4(0).@float * 3.141592653/180.0]}, $0]]",
		"[UnaryOpTypeRadiansToDegrees][children][BinaryOpTypeMultiply][children][OpTypeConstantFloat][xunion(%3)] : [BinaryOpTypeMultiply->[OpTypeConstantFloat{float=[%4(0).@float * 180.0/3.141592653]}, $0]]",

"[UnaryOpTypeDegreesToRadians][children][TernaryOpTypeMultiplyAdd][children][$0][OpTypeConstantFloat][%3][$2][OpTypeConstantFloat][%3][$1] : [TernaryOpTypeMultiplyAdd->[OpTypeConstantFloat{float=[%5(0).@float * 3.141592653/180.0]}, $0, OpTypeConstantFloat{float=[%8(0).@float * 3.141592653/180.0]}]]",
"[UnaryOpTypeDegreesToRadians][children][TernaryOpTypeMultiplyAdd][children][$1][OpTypeConstantFloat][%3][$2][OpTypeConstantFloat][%3][$0] : [TernaryOpTypeMultiplyAdd->[OpTypeConstantFloat{float=[%5(0).@float * 3.141592653/180.0]}, $0, OpTypeConstantFloat{float=[%8(0).@float * 3.141592653/180.0]}]]",

#if 0
"[TernaryOpTypeConditional][children][$0][BinaryOpTypeLessThan][children] : [TernaryOpTypeConditional->[BinaryOpTypeGreaterThan->[$0, $1], %1.(2), %1.(1)]]",
#endif
"[UnaryOpTypeNegate][children][BinaryOpTypeMultiply][children][$1][OpTypeConstantFloat][%3][$0] : [BinaryOpTypeMultiply->[OpTypeConstantFloat{float=[!1.0 * %5(0).@float]}, $0]]",
"[TernaryOpTypeToVector][children][OpTypeComponentGet][@track=][@id=][@type=][@component=][@relative=][@order=][length=3] : [UnaryOpTypeSplat->[OpTypeComponentGet{track=$0.@track, id=$0.@id, type=$0.@type, component=$0.@component, relative=$0.@relative, order=$0.@order}]]",
#if 1
/*73*/"[TernaryOpTypeConditional][children][$1][match(%1(2))] : [$0]",
#endif
};

////////////////////////////////////////////////////////////////////////////////

class Token
{
public:
	enum Symbol
	{
		kInvalidSymbol,
		kDescriptor,
		kFilter,
		kLeftBracket,		// [
		kRightBracket,		// ]
		kLeftParen,			// (
		kRightParen,		// )
		kIdentifier,
		kListIndex,
		kItemIndex,
		kPeriod,
		kComma,
		kChildExpansion,
		kLengthCheck,
		kUnion,
		kExclusiveUnion,
		kAttribute,
		kEquals,
		kNotEquals,
		kNegation,
		kLessThanEqualTo,
		kLessThan,
		kGreaterThanEqualTo,
		kGreaterThan,
		kColon,
		kLeftBrace,			// {
		kRightBrace,		// }
		kInteger,
		kFloatingPoint,
		kChildren,
		kPlus,
		kSubtract,
		kDivide,
		kMultiply,
		kTilde,
		kMatch,
	};

	Symbol GetSymbol() const
	{
		return m_Symbol;
	}
	virtual ~Token() {}
protected:
	Token(Symbol symbol) : m_Symbol(symbol) {}
	Symbol m_Symbol;
};

////////////////////////////////////////////////////////////////////////////////

class DescriptorToken : public Token
{
public:
	DescriptorToken() 
		: Token (kDescriptor) { }
};

////////////////////////////////////////////////////////////////////////////////

class FilterToken : public Token
{
public:
	FilterToken() 
		: Token (kFilter) { }
};

////////////////////////////////////////////////////////////////////////////////

class LeftBracketToken : public Token
{
public:
	LeftBracketToken() 
		: Token (kLeftBracket) { }
};

////////////////////////////////////////////////////////////////////////////////

class RightBracketToken : public Token
{
public:
	RightBracketToken() 
		: Token (kRightBracket) { }
};

////////////////////////////////////////////////////////////////////////////////

class LeftParenToken : public Token
{
public:
	LeftParenToken() 
		: Token (kLeftParen) { }
};

////////////////////////////////////////////////////////////////////////////////

class RightParenToken : public Token
{
public:
	RightParenToken() 
		: Token (kRightParen) { }
};

////////////////////////////////////////////////////////////////////////////////

class IdentifierToken : public Token
{
public:
	IdentifierToken(const char* identifier) 
		: Token (kIdentifier), m_Identifier (identifier) { }
	atString m_Identifier;
};

////////////////////////////////////////////////////////////////////////////////

class ListIndexToken : public Token
{
public:
	ListIndexToken() 
		: Token (kListIndex) { }
};

////////////////////////////////////////////////////////////////////////////////

class ItemIndexToken : public Token
{
public:
	ItemIndexToken() 
		: Token (kItemIndex) { }
};

////////////////////////////////////////////////////////////////////////////////

class PeriodToken : public Token 
{
public:
	PeriodToken() 
		: Token (kPeriod) { }
};

////////////////////////////////////////////////////////////////////////////////

class CommaToken : public Token
{
public:
	CommaToken() 
		: Token (kComma) { }
};

////////////////////////////////////////////////////////////////////////////////

class ChildExpansionToken : public Token
{
public:
	ChildExpansionToken() 
		: Token (kChildExpansion) { }
};

////////////////////////////////////////////////////////////////////////////////

class LengthCheckToken : public Token
{
public:
	LengthCheckToken() 
		: Token (kLengthCheck) { }
};

////////////////////////////////////////////////////////////////////////////////

class UnionToken : public Token
{
public:
	UnionToken() 
		: Token (kUnion) { }
};

////////////////////////////////////////////////////////////////////////////////

class ExclusiveUnionToken : public Token
{
public:
	ExclusiveUnionToken() 
		: Token (kExclusiveUnion) { }
};

////////////////////////////////////////////////////////////////////////////////

class MatchToken : public Token
{
public:
	MatchToken()
		: Token (kMatch) { }
};

////////////////////////////////////////////////////////////////////////////////

class AttributeToken : public Token
{
public:
	AttributeToken() 
		: Token(kAttribute) { }
};

////////////////////////////////////////////////////////////////////////////////

class EqualsToken : public Token
{
public:
	EqualsToken() 
		: Token (kEquals) { }
};

////////////////////////////////////////////////////////////////////////////////

class NotEqualsToken : public Token
{
public:
	NotEqualsToken() 
		: Token (kNotEquals) { }
};

////////////////////////////////////////////////////////////////////////////////

class NegationToken : public Token
{
public:
	NegationToken()
		: Token (kNegation) {}
};

////////////////////////////////////////////////////////////////////////////////

class LessThanEqualToToken : public Token
{
public:
	LessThanEqualToToken()
		: Token (kLessThanEqualTo) {}
};

////////////////////////////////////////////////////////////////////////////////

class LessThanToken : public Token
{
public:
	LessThanToken()
		: Token (kLessThan) {}
};

////////////////////////////////////////////////////////////////////////////////

class GreaterThanEqualToToken : public Token
{
public:
	GreaterThanEqualToToken()
		: Token (kGreaterThanEqualTo) {}
};

////////////////////////////////////////////////////////////////////////////////

class GreaterThanToken : public Token
{
public:
	GreaterThanToken()
		: Token (kGreaterThan) {}
};

////////////////////////////////////////////////////////////////////////////////

class ColonToken : public Token 
{
public:
	ColonToken() 
		: Token (kColon) { }
};

////////////////////////////////////////////////////////////////////////////////

class LeftBraceToken : public Token 
{
public:
	LeftBraceToken() 
		: Token (kLeftBrace) { }
};

////////////////////////////////////////////////////////////////////////////////

class RightBraceToken : public Token
{
public:
	RightBraceToken() 
		: Token (kRightBrace) { }
};

////////////////////////////////////////////////////////////////////////////////

class IntegerToken : public Token
{
public:
	IntegerToken(int integer) 
		: Token (kInteger), m_Integer (integer) { }
	int m_Integer;
};

////////////////////////////////////////////////////////////////////////////////

class FloatingPointToken : public Token
{
public:
	FloatingPointToken(float fp) 
		: Token (kFloatingPoint), m_Float (fp) { }
	float m_Float;
};

////////////////////////////////////////////////////////////////////////////////

class ChildrenToken : public Token
{
public:
	ChildrenToken() 
		: Token (kChildren) { }
};

////////////////////////////////////////////////////////////////////////////////

class PlusToken : public Token
{
public: 
	PlusToken() 
		: Token (kPlus) { }
};

////////////////////////////////////////////////////////////////////////////////

class SubtractToken : public Token
{
public:
	SubtractToken() 
		: Token (kSubtract) { }
};

////////////////////////////////////////////////////////////////////////////////

class DivideToken : public Token
{
public:
	DivideToken() 
		: Token (kDivide) { }
};

////////////////////////////////////////////////////////////////////////////////

class MultiplyToken : public Token
{
public:
	MultiplyToken() 
		: Token (kMultiply) { }
};

////////////////////////////////////////////////////////////////////////////////

class TildeToken : public Token
{
public:
	TildeToken() 
		: Token (kTilde) { }
};

////////////////////////////////////////////////////////////////////////////////

class Tokenizer
{
public:
	void Tokenize(const char* rule, atArray<Token*>& tokens);

	bool Accept(Token::Symbol s)
	{
		if (m_Symbol == s) 
		{
			GetSymbol();
			return true;
		}
		return false;
	}

	bool Expect(Token::Symbol s) 
	{
		if (Accept(s))
		{
			return true;
		}
		Errorf("Expect: unexpected symbol");
		Assert(false);
		return false;
	}

	Tokenizer() : c (NULL) { }
private:
	void GetSymbol();
private:
	atArray<Token*>* m_Tokens;
	Token::Symbol m_Symbol;
	const char* c;
};

////////////////////////////////////////////////////////////////////////////////

class Variant 
{
public:
	enum 
	{
		kAnimationTrackIndex,
		kAnimationTrackType,
		kAnimationTrackId,
		kVector3,
		kQuaternion,
		kFloat,
		kInt,
		kBool,
	};

	Variant(u32 type)
		: m_Type (type) 
	{ 
	}

	virtual ~Variant()
	{
	}

	u32 GetType() const
	{
		return m_Type;
	}

	bool Match(Variant* v) const
	{
		if (m_Type == v->m_Type) 
		{
			return DoMatch(v);
		}
		return false;
	}
protected:
	virtual bool DoMatch(Variant* other) const = 0;
protected:
	u32 m_Type;
};

////////////////////////////////////////////////////////////////////////////////

enum AttributeCompareOperation
{
	kAttributeCompareOpEqual,
	kAttributeCompareOpNotEqual,
	kAttributeCompareOpLessThan,
	kAttributeCompareOpLessThanEqual,
	kAttributeCompareOpGreaterThan,
	kAttributeCompareOpGreaterThanEqual,

	kAttributeCompareOpNone,
};

////////////////////////////////////////////////////////////////////////////////

class AnimationTrackIndexVariant : public Variant 
{
public:
	AnimationTrackIndexVariant(u8 trackIndex)
		: Variant (Variant::kAnimationTrackIndex)
		, m_TrackIndex (trackIndex) { }

	bool IsSatisfied(atString& id, bool equals)
	{
		if (id == "BoneTranslation") 
		{
			return (equals ? m_TrackIndex == kTrackBoneTranslation : m_TrackIndex != kTrackBoneTranslation);
		} 
		else if (id == "BoneRotation") 
		{
			return (equals ? m_TrackIndex == kTrackBoneRotation : m_TrackIndex != kTrackBoneRotation);
		} 
		else if (id == "BoneScale") 
		{
			return (equals ? m_TrackIndex == kTrackBoneScale : m_TrackIndex != kTrackBoneScale);
		} 
		else if (id == "BoneConstraint") 
		{
			return (equals ? m_TrackIndex == kTrackBoneConstraint : m_TrackIndex != kTrackBoneConstraint);
		} 
		else if (id == "Visibility") 
		{
			return (equals ? m_TrackIndex == kTrackVisibility : m_TrackIndex != kTrackVisibility);
		} 
		else if (id == "MoverTranslation") 
		{
			return (equals ? m_TrackIndex == kTrackMoverTranslation : m_TrackIndex != kTrackMoverTranslation);
		}
		else if (id == "MoverRotation") 
		{
			return (equals ? m_TrackIndex == kTrackMoverRotation : m_TrackIndex != kTrackMoverRotation);
		} 
		else if (id == "CameraTranslation") 
		{
			return (equals ? m_TrackIndex == kTrackCameraTranslation : m_TrackIndex != kTrackCameraTranslation);
		} 
		else if (id == "CameraRotation") 
		{
			return (equals ? m_TrackIndex == kTrackCameraRotation : m_TrackIndex != kTrackCameraRotation);
		} 
		else if (id == "CameraScale") 
		{
			return (equals ? m_TrackIndex == kTrackCameraScale : m_TrackIndex != kTrackCameraScale);
		} 
		else if (id == "CameraFocalLength") 
		{
			return (equals ? m_TrackIndex == kTrackCameraFocalLength : m_TrackIndex != kTrackCameraFocalLength);
		} 
		else if (id == "CameraHorizontalFilmAperture") 
		{
			return (equals ? m_TrackIndex == kTrackCameraHorizontalFilmAperture : m_TrackIndex != kTrackCameraHorizontalFilmAperture);
		} 
		else if (id == "CameraAperture") 
		{
			return (equals ? m_TrackIndex == kTrackCameraAperture : m_TrackIndex != kTrackCameraAperture);
		} 
		else if (id == "CameraFocalPoint") 
		{
			return (equals ? m_TrackIndex == kTrackCameraFocalPoint : m_TrackIndex != kTrackCameraFocalPoint);
		} 
		else if (id == "CameraFStop") 
		{
			return (equals ? m_TrackIndex == kTrackCameraFStop : m_TrackIndex != kTrackCameraFStop);
		} 
		else if (id == "CameraFocusDistance") 
		{
			return (equals ? m_TrackIndex == kTrackCameraFocusDistance : m_TrackIndex != kTrackCameraFocusDistance);
		} 
		else if (id == "ShaderFrameIndex") 
		{
			return (equals ? m_TrackIndex == kTrackShaderFrameIndex : m_TrackIndex != kTrackShaderFrameIndex);
		} 
		else if (id == "ShaderSlideU") 
		{
			return (equals ? m_TrackIndex == kTrackShaderSlideU : m_TrackIndex != kTrackShaderSlideU);
		} 
		else if (id == "ShaderSlideV") 
		{
			return (equals ? m_TrackIndex == kTrackShaderSlideV : m_TrackIndex != kTrackShaderSlideV);
		} 
		else if (id == "ShaderRotateUV") 
		{
			return (equals ? m_TrackIndex == kTrackShaderRotateUV : m_TrackIndex != kTrackShaderRotateUV);
		} 
		else if (id == "MoverScale") 
		{
			return (equals ? m_TrackIndex == kTrackMoverScale : m_TrackIndex != kTrackMoverScale);
		} 
		else if (id == "BlendShape") 
		{
			return (equals ? m_TrackIndex == kTrackBlendShape : m_TrackIndex != kTrackBlendShape);
		} 
		else if (id == "Visemes") 
		{
			return (equals ? m_TrackIndex == kTrackVisemes : m_TrackIndex != kTrackVisemes);
		} 
		else if (id == "AnimatedNormalMaps") 
		{
			return (equals ? m_TrackIndex == kTrackAnimatedNormalMaps : m_TrackIndex != kTrackAnimatedNormalMaps);
		} 
		else if (id == "FacialControl") 
		{
			return (equals ? m_TrackIndex == kTrackFacialControl : m_TrackIndex != kTrackFacialControl);
		} 
		else if (id == "FacialTranslation") 
		{
			return (equals ? m_TrackIndex == kTrackFacialTranslation : m_TrackIndex != kTrackFacialTranslation);
		} 
		else if (id == "FacialRotation") 
		{
			return (equals ? m_TrackIndex == kTrackFacialRotation : m_TrackIndex != kTrackFacialRotation);
		} 
		else if (id == "CameraFieldOfView") 
		{
			return (equals ? m_TrackIndex == kTrackCameraFieldOfView : m_TrackIndex != kTrackCameraFieldOfView);
		} 
		else if (id == "CameraDepthOfField") 
		{
			return (equals ? m_TrackIndex == kTrackCameraDepthOfField : m_TrackIndex != kTrackCameraDepthOfField);
		} 
		else if (id == "Color") 
		{
			return (equals ? m_TrackIndex == kTrackColor : m_TrackIndex != kTrackColor);
		} 
		else if (id == "LightIntensity") 
		{
			return (equals ? m_TrackIndex == kTrackLightIntensity : m_TrackIndex != kTrackLightIntensity);
		} 
		else if (id == "LightFallOff") 
		{
			return (equals ? m_TrackIndex == kTrackLightFallOff : m_TrackIndex != kTrackLightFallOff);
		} 
		else if (id == "LightConeAngle") 
		{
			return (equals ? m_TrackIndex == kTrackLightConeAngle : m_TrackIndex != kTrackLightConeAngle);
		} 
		else if (id == "GenericControl") 
		{
			return (equals ? m_TrackIndex == kTrackGenericControl : m_TrackIndex != kTrackGenericControl);
		} 
		else if (id == "GenericTranslation") 
		{
			return (equals ? m_TrackIndex == kTrackGenericTranslation : m_TrackIndex != kTrackGenericTranslation);
		} 
		else if (id == "GenericRotation") 
		{
			return (equals ? m_TrackIndex == kTrackGenericRotation : m_TrackIndex != kTrackGenericRotation);
		} 
		else if (id == "CameraDepthOfFieldStrength") 
		{
			return (equals ? m_TrackIndex == kTrackCameraDepthOfFieldStrength : m_TrackIndex != kTrackCameraDepthOfFieldStrength);
		}
	}

	u8 GetTrackIndex() const
	{
		return m_TrackIndex;
	}
protected:
	virtual bool DoMatch(Variant* other) const
	{
		Assert(other->GetType() == kAnimationTrackIndex);
		return m_TrackIndex == static_cast<AnimationTrackIndexVariant*>(other)->m_TrackIndex;
	}
private:
	u8 m_TrackIndex;
};

////////////////////////////////////////////////////////////////////////////////

class AnimationTrackTypeVariant : public Variant 
{
public:
	AnimationTrackTypeVariant(u8 trackType) 
		: Variant (Variant::kAnimationTrackType)
		, m_TrackType (trackType) { }

	bool IsSatisfied(const atString& id, AttributeCompareOperation op)
	{
		Assert(op == kAttributeCompareOpEqual || op == kAttributeCompareOpNotEqual);
		bool equals = true;
		if (op == kAttributeCompareOpNotEqual)
		{
			equals = false;
		}

		if (id == "Vector3") 
		{
			return (equals ? m_TrackType == kFormatTypeVector3 : m_TrackType != kFormatTypeVector3);
		} 
		else if (id == "Quat") 
		{
			return (equals ? m_TrackType == kFormatTypeQuaternion : m_TrackType != kFormatTypeQuaternion);
		} 
		else if (id == "Float") 
		{
			return (equals ? m_TrackType == kFormatTypeFloat : m_TrackType != kFormatTypeFloat);
		} 

		return false;
	}
	u8 GetTrackType() const
	{
		return m_TrackType;
	}
protected:
	virtual bool DoMatch(Variant* other) const
	{
		Assert(other->GetType() == kAnimationTrackType);
		return m_TrackType == static_cast<AnimationTrackTypeVariant*>(other)->m_TrackType;
	}
private:
	u8 m_TrackType;
};

////////////////////////////////////////////////////////////////////////////////

class AnimationTrackIdVariant : public Variant 
{
public:
	AnimationTrackIdVariant(u16 trackId)
		: Variant (Variant::kAnimationTrackId)
		, m_TrackId (trackId) { }

	bool IsSatisfied(u16 id, bool equals)
	{
		return (equals ? m_TrackId == id : m_TrackId != id);
	}

	u16 GetTrackId() const
	{
		return m_TrackId;
	}
protected:
	virtual bool DoMatch(Variant* other) const
	{
		Assert(other->GetType() == kAnimationTrackId);
		return m_TrackId == static_cast<AnimationTrackIdVariant*>(other)->m_TrackId;
	}
private:
	u16 m_TrackId;
};

////////////////////////////////////////////////////////////////////////////////

class Vector3Variant : public Variant 
{
public:
	Vector3Variant(atArray<float>& vec3) 
		: Variant (Variant::kVector3)
		, m_Vec (vec3) { }
	virtual ~Vector3Variant() {}

	bool IsSatisfied(const atArray<float>& vec3, AttributeCompareOperation op)
	{
		using namespace MakeExpressions;

		Assert(op == kAttributeCompareOpEqual || op == kAttributeCompareOpNotEqual);
		bool equals = true;
		if (op == kAttributeCompareOpNotEqual)
		{
			equals = false;
		}

		bool match = (IsClose(vec3[0], m_Vec[0], g_ConstantFloatOptimizationThreshold) && 
			IsClose(vec3[1], m_Vec[1], g_ConstantFloatOptimizationThreshold) && 
			IsClose(vec3[2], m_Vec[2], g_ConstantFloatOptimizationThreshold));
		if (equals && match) 
		{
			return true;
		} 
		else if (!equals && !match) 
		{
			return true;
		}
		return false;
	}

	const atArray<float>& GetVec() const
	{
		return m_Vec;
	}
protected:
	virtual bool DoMatch(Variant* other) const
	{
		using namespace MakeExpressions;

		Assert(other->GetType() == kVector3);
		Vector3Variant* variant = static_cast<Vector3Variant*>(other);
		return (IsClose(m_Vec[0], variant->m_Vec[0], g_ConstantFloatOptimizationThreshold) && 
			IsClose(m_Vec[1], variant->m_Vec[1], g_ConstantFloatOptimizationThreshold) &&
			IsClose(m_Vec[2], variant->m_Vec[2], g_ConstantFloatOptimizationThreshold)); 
	}
private:
	atArray<float> m_Vec;
};

////////////////////////////////////////////////////////////////////////////////

class QuaternionVariant : public Variant 
{
public:
	QuaternionVariant(atArray<float>& quat) 
		: Variant (Variant::kQuaternion)
		, m_Quat (quat){ }
	virtual ~QuaternionVariant() {}


	bool IsSatisfied(const atArray<float>& quat, AttributeCompareOperation op)
	{
		using namespace MakeExpressions;

		Assert(op == kAttributeCompareOpEqual || op == kAttributeCompareOpNotEqual);
		bool equals = true;
		if (op == kAttributeCompareOpNotEqual)
		{
			equals = false;
		}

		bool match = (IsClose(quat[0], m_Quat[0], g_ConstantFloatOptimizationThreshold) && 
			IsClose(quat[1], m_Quat[1], g_ConstantFloatOptimizationThreshold) && 
			IsClose(quat[2], m_Quat[2], g_ConstantFloatOptimizationThreshold) && 
			IsClose(quat[3], m_Quat[3], g_ConstantFloatOptimizationThreshold));
		if (equals && match) 
		{
			return true;
		} 
		else if (!equals && !match) 
		{
			return true;
		}
		return false;
	}
	const atArray<float>& GetQuat() const
	{
		return m_Quat;
	}
protected:
	virtual bool DoMatch(Variant* other) const
	{
		using namespace MakeExpressions;

		Assert(other->GetType() == kQuaternion);
		QuaternionVariant* variant = static_cast<QuaternionVariant*>(other);
		return (IsClose(m_Quat[0], variant->m_Quat[0], g_ConstantFloatOptimizationThreshold) &&
			IsClose(m_Quat[1], variant->m_Quat[1], g_ConstantFloatOptimizationThreshold) &&
			IsClose(m_Quat[2], variant->m_Quat[2], g_ConstantFloatOptimizationThreshold) &&
			IsClose(m_Quat[3], variant->m_Quat[3], g_ConstantFloatOptimizationThreshold));
	}
private:
	atArray<float> m_Quat;
};

////////////////////////////////////////////////////////////////////////////////

class FloatVariant : public Variant 
{
public:
	FloatVariant(float fp) 
		: Variant (Variant::kFloat)
		, m_Float (fp) { }

	bool IsSatisfied(float fp, AttributeCompareOperation op)
	{
		using namespace MakeExpressions;

		if ((op == kAttributeCompareOpEqual) && IsClose(fp, m_Float, g_ConstantFloatOptimizationThreshold)) 
		{
			return true;
		} 
		else if ((op == kAttributeCompareOpNotEqual) && !IsClose(fp, m_Float, g_ConstantFloatOptimizationThreshold)) 
		{
			return true;
		}
		else if ((op == kAttributeCompareOpLessThan) && (m_Float < fp))
		{
			return true;
		}
		else if ((op == kAttributeCompareOpLessThanEqual) && (m_Float <= fp))
		{
			return true;
		}
		else if ((op == kAttributeCompareOpGreaterThan) && (m_Float > fp))
		{
			return true;
		}
		else if ((op == kAttributeCompareOpGreaterThanEqual) && (m_Float >= fp))
		{
			return true;
		}

		return false;
	}

	void Negate()
	{
		m_Float *= -1.f;
	}

	float GetFloat() const
	{
		return m_Float;
	}
protected:
	virtual bool DoMatch(Variant* other) const
	{
		Assert(other->GetType() == Token::kFloatingPoint);
		return IsClose(m_Float, static_cast<FloatVariant*>(other)->m_Float, MakeExpressions::g_ConstantFloatOptimizationThreshold);
	}
private:
	float m_Float;
};

class BoolVariant : public Variant
{
public:
	BoolVariant(bool value)
		: Variant(Variant::kBool)
		, m_Value (value) { }

	bool IsSatisfied(bool comparand, AttributeCompareOperation op)
	{
		if ((op == kAttributeCompareOpEqual) && comparand == m_Value)
		{
			return true;
		}
		else if ((op == kAttributeCompareOpNotEqual) && comparand != m_Value)
		{
			return true;
		}
		
		return false;
	}

	bool GetValue() const
	{
		return m_Value;
	}
protected:
	virtual bool DoMatch(Variant* other) const 
	{
		Assert(other->GetType() == kBool);
		return m_Value == static_cast<BoolVariant*>(other)->m_Value;
	}
private:
	bool m_Value;
};

////////////////////////////////////////////////////////////////////////////////

class IntVariant : public Variant 
{
public:
	IntVariant(int i) 
		: Variant (Variant::kInt)
		, m_Int (i) { }

	bool IsSatisfied(int i, AttributeCompareOperation op)
	{
		if ((op == kAttributeCompareOpEqual) && i == m_Int) 
		{
			return true;
		} 
		else if ((op == kAttributeCompareOpNotEqual) && i != m_Int) 
		{
			return true;
		}
		else if ((op == kAttributeCompareOpLessThan) && m_Int < i)
		{
			return true;
		}
		else if ((op == kAttributeCompareOpLessThanEqual) && m_Int <= i)
		{
			return true;
		}
		else if ((op == kAttributeCompareOpGreaterThan) && m_Int > i)
		{
			return true;
		}
		else if ((op == kAttributeCompareOpGreaterThanEqual) && (m_Int >= i))
		{
			return true;
		}

		return false;
	}

	int GetInt() const
	{
		return m_Int;
	}
protected:
	virtual bool DoMatch(Variant* other) const
	{
		Assert(other->GetType() == kInt);
		return m_Int == static_cast<IntVariant*>(other)->m_Int;
	}
private:
	int m_Int;
};


////////////////////////////////////////////////////////////////////////////////

class ExpressionRuleContext
{
public:
	typedef atArray<crExpressionOp*> ExpressionOpList;

	static void Free(ExpressionOpList*& p)
	{
#if 0
		for (int j = 0; j < p->GetCount(); ++j)
		{
			delete (*p)[j]; // delete the expression op
		}
#endif

		delete p;
		p = NULL;
	}

	void Append(ExpressionOpList& list)
	{
		m_Items.PushAndGrow(&list);
	}

	~ExpressionRuleContext()
	{
		// free the memory used for rule evaluation
		for (int i = 0; i < m_Items.GetCount(); ++i) 
		{
			Free(m_Items[i]);
		}
	}

	const ExpressionOpList& GetLastList() const
	{
		return *m_Items[m_Items.GetCount()-1];
	}

	bool GetList(int index, const ExpressionOpList*& out) const
	{
		if (!(0 <= index && index < m_Items.GetCount())) 
		{
			Warningf("rule is attempting to index outside available range!");
			out = NULL;
			return false;
		}

		out = m_Items[index];
		return true;
	}

	void PushFilterResult(crExpressionOp* op)
	{
		Assert(m_CurrentNewList);
		m_CurrentNewList->PushAndGrow(op);
	}

	void ClearFilterResults()
	{
		Assert(m_CurrentNewList);
		m_CurrentNewList->Reset();
	}

	bool HasNewItems() const 
	{
		return m_CurrentNewList->GetCount() != 0;
	}

private:
	typedef atArray<ExpressionOpList*> ItemList;
	ItemList m_Items;

	ExpressionOpList* m_CurrentNewList;

	friend struct RuleBasedOptimizeIterator;
};


////////////////////////////////////////////////////////////////////////////////

class ExpressionOpInitializer 
{
public:
	enum Result
	{
		kInitializationSucceeded,
		kIndexOutOfRange,
		kMissingAttribute,
		kAttributeSetFailed,
	};

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext& context) = 0;

	virtual ~ExpressionOpInitializer() { }
};

////////////////////////////////////////////////////////////////////////////////

class InitializeFromConstantFloat : public ExpressionOpInitializer
{
public:
	InitializeFromConstantFloat(const char* destAttribute, float fp)
		: m_DestAttribute (destAttribute)
		, m_Float (fp)
	{
	}

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext&)
	{
		FloatVariant variant (m_Float);
		AttributeSetResult result = SetAttribute(op, m_DestAttribute.c_str(), &variant);

		if (result != kAttributeSetSucceeded)
		{
			return kAttributeSetFailed;
		}

		return kInitializationSucceeded;
	}
private:
	atString m_DestAttribute;
	float m_Float;
};

////////////////////////////////////////////////////////////////////////////////

class InitializeFromConstantInteger : public ExpressionOpInitializer
{
public:
	InitializeFromConstantInteger(const char* destAttribute, int integer)
		: m_DestAttribute (destAttribute)
		, m_Integer (integer)
	{
	}

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext&)
	{
		IntVariant variant (m_Integer);
		AttributeSetResult result = SetAttribute(op, m_DestAttribute.c_str(), &variant);

		if (result != kAttributeSetSucceeded)
		{
			return kAttributeSetFailed;
		}

		return kInitializationSucceeded;
	}
private:
	atString m_DestAttribute;
	int m_Integer;
};

////////////////////////////////////////////////////////////////////////////////

class InitializeFromIndexAttribute : public ExpressionOpInitializer 
{
public:
	InitializeFromIndexAttribute(int itemIndex, const char* attribute, int initializerIndex)
		: m_ItemIndex (itemIndex)
		, m_Attribute (attribute)
		, m_InitializerIndex (initializerIndex)
	{
	}

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext& context)
	{
		const ExpressionRuleContext::ExpressionOpList& last = context.GetLastList();
		if (!(0 <= m_ItemIndex && m_ItemIndex < last.GetCount())) 
		{
			Assert(false);
			return kIndexOutOfRange;
		}

		crExpressionOp* indexedOp = last[m_ItemIndex];
		Variant* variant = NULL;
		if (HasAttribute(*indexedOp, m_Attribute.c_str(), variant)) 
		{
			AttributeSetResult result = SetAttributeFromIndex(op, m_InitializerIndex, variant);
			delete variant;

			if (result != kAttributeSetSucceeded)
			{
				return kAttributeSetFailed;
			}
		}

		return kInitializationSucceeded;
	}

	virtual ~InitializeFromIndexAttribute()
	{
	}

private:
	atString m_Attribute;
	int m_ItemIndex;
	int m_InitializerIndex;
};

////////////////////////////////////////////////////////////////////////////////

class InitializeFromItemAttribute : public ExpressionOpInitializer
{
public:
	InitializeFromItemAttribute(const char* destAttrib, int sourceIndex, const char* sourceAttrib) 
		: m_DestAttrib (destAttrib)
		, m_SourceIndex (sourceIndex)
		, m_SourceAttrib (sourceAttrib) 
	{ 
	}

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext& context)
	{
		const ExpressionRuleContext::ExpressionOpList& last =  context.GetLastList();
		crExpressionOp* indexedOp = last[m_SourceIndex];
		Variant* variant = NULL;
		if (HasAttribute(*indexedOp, m_SourceAttrib.c_str(), variant)) 
		{
			AttributeSetResult result = SetAttribute(op, m_DestAttrib, variant);
			delete variant;

			if (result != kAttributeSetSucceeded)
			{
				return kAttributeSetFailed;
			}
		}

		return kInitializationSucceeded;
	}

	virtual ~InitializeFromItemAttribute()
	{
	}

private:
	atString m_DestAttrib;
	int m_SourceIndex;
	atString m_SourceAttrib;
};

////////////////////////////////////////////////////////////////////////////////

class InitializeAttributeFromIndexedListItemAttribute : public ExpressionOpInitializer
{
public:
	InitializeAttributeFromIndexedListItemAttribute(int listIndex, int itemIndex, const char* source, const char* dest, bool negate)
		: m_ListIndex(listIndex)
		, m_ItemIndex(itemIndex)
		, m_Source(source)
		, m_Dest(dest)
		, m_Negate(negate)
	{}

	virtual ~InitializeAttributeFromIndexedListItemAttribute()
	{
	}

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext& context)
	{
		const ExpressionRuleContext::ExpressionOpList* list = NULL;
		context.GetList(m_ListIndex, list); // TODO: Warning/Error handling

		if (!(0 <= m_ItemIndex && m_ItemIndex < list->GetCount()))
		{
			Assert(false);
			return kIndexOutOfRange;
		}

		crExpressionOp* indexedOp = (*list)[m_ItemIndex];
		Variant* variant = NULL;
		if (HasAttribute(*indexedOp, m_Source.c_str(), variant))
		{
			if (variant->GetType() == Variant::kFloat && m_Negate)
			{
				static_cast<FloatVariant*>(variant)->Negate();
			}

			AttributeSetResult result = SetAttribute(op, m_Dest.c_str(), variant);
			delete variant;

			if (result != kAttributeSetSucceeded)
			{
				return kAttributeSetFailed;
			}
		}

		return kInitializationSucceeded;
	}
private:
	int m_ListIndex;
	int m_ItemIndex;
	atString m_Source;
	atString m_Dest;
	bool m_Negate;
};

////////////////////////////////////////////////////////////////////////////////

class InitializeAttributeFromArithmeticOperation : public ExpressionOpInitializer
{
public:
	InitializeAttributeFromArithmeticOperation(atString& attribute, atArray<ArithmeticOperation*>& ops)
		: m_Attribute (attribute)
		, m_Ops (ops)
	{
	}
	~InitializeAttributeFromArithmeticOperation();

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext& context)
	{
		Variant* variant = ExecuteArithmetic(m_Ops, context);
		if (variant != NULL) 
		{
			AttributeSetResult result = SetAttribute(op, m_Attribute.c_str(), variant);
			delete variant;

			if (result != kAttributeSetSucceeded)
			{
				return kAttributeSetFailed;
			}
		}

		return kInitializationSucceeded;
	}

private:
	atString m_Attribute;
	atArray<ArithmeticOperation*> m_Ops;
};

////////////////////////////////////////////////////////////////////////////////

class InitializeVectorFromIndexedAttributes : public ExpressionOpInitializer
{
public:
	InitializeVectorFromIndexedAttributes(int idx0, atString& attr0, int idx1, atString& attr1, int idx2, atString& attr2)
		: m_Index0 (idx0)
		, m_Attribute0 (attr0)
		, m_Index1 (idx1)
		, m_Attribute1 (attr1)
		, m_Index2 (idx2)
		, m_Attribute2 (attr2)
	{
	}

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext& context)
	{
		const ExpressionRuleContext::ExpressionOpList& last = context.GetLastList(); 
		if (!(0 <= m_Index0 && m_Index0 < last.GetCount())) 
		{
			Assert(false);
			return kIndexOutOfRange;
		}

		if (!(0 <= m_Index1 && m_Index1 < last.GetCount())) 
		{
			Assert(false);
			return kIndexOutOfRange;
		}

		if (!(0 <= m_Index2 && m_Index2 < last.GetCount())) 
		{ 
			Assert(false);
			return kIndexOutOfRange;
		}

		crExpressionOp* op0 = last[m_Index0];
		crExpressionOp* op1 = last[m_Index1];
		crExpressionOp* op2 = last[m_Index2];

		float values[3];
		Variant* varx = NULL;
		if (HasAttribute(*op0, m_Attribute0.c_str(), varx)) 
		{
			FloatVariant* x = static_cast<FloatVariant*>(varx);
			values[0] = x->GetFloat();
			delete x;
		}
		else 
		{
			return kMissingAttribute;
		}
		Variant* vary = NULL;
		if (HasAttribute(*op1, m_Attribute1.c_str(), vary)) 
		{
			FloatVariant* y = static_cast<FloatVariant*>(vary);
			values[1] = y->GetFloat();
			delete y;
		}
		else
		{
			return kMissingAttribute;
		}
		Variant* varz = NULL;
		if (HasAttribute(*op2, m_Attribute2.c_str(), varz)) 
		{
			FloatVariant* z = static_cast<FloatVariant*>(varz);
			values[2] = z->GetFloat();
			delete z;
		}
		else 
		{
			return kMissingAttribute;
		}
		crExpressionOpConstant::Value val;
		val.Set(Vec3V(values[0], values[1], values[2]));
		crExpressionOpConstant& o = static_cast<crExpressionOpConstant&>(op);
		o.SetValue(val);

		return kInitializationSucceeded;
	}

private:
	atString m_Attribute0;
	atString m_Attribute1;
	atString m_Attribute2;
	int m_Index0;
	int m_Index1;
	int m_Index2;
};

////////////////////////////////////////////////////////////////////////////////

class InitializeQuaternionFromIndexedAttributes : public ExpressionOpInitializer
{
public:
	InitializeQuaternionFromIndexedAttributes(int x, atString& attrX, int y, atString& attrY, int z, atString& attrZ)
		: m_IndexX (x), m_AttributeX (attrX)
		, m_IndexY (y), m_AttributeY (attrY)
		, m_IndexZ (z), m_AttributeZ (attrZ)
	{
	}

	virtual Result Init(crExpressionOp& op, ExpressionRuleContext& context)
	{
		const ExpressionRuleContext::ExpressionOpList& last = context.GetLastList();
		if (!(0 <= m_IndexX && m_IndexX < last.GetCount())) 
		{
			Assert(false);
			return kIndexOutOfRange;
		}

		if (!(0 <= m_IndexY && m_IndexY < last.GetCount())) 
		{
			Assert(false);
			return kIndexOutOfRange;
		}

		if (!(0 <= m_IndexZ && m_IndexZ < last.GetCount())) 
		{ 
			Assert(false);
			return kIndexOutOfRange;
		}

		crExpressionOp* op0 = last[m_IndexX];
		crExpressionOp* op1 = last[m_IndexY];
		crExpressionOp* op2 = last[m_IndexZ];

		Vec3V e;

		Variant* x = NULL;
		if (HasAttribute(*op0, m_AttributeX.c_str(), x)) 
		{
			FloatVariant* f = static_cast<FloatVariant*>(x);
			e.SetXf(f->GetFloat());
			delete x;
		} else 
		{
			Assert(false);
			return kMissingAttribute;
		}
		Variant* y = NULL;
		if (HasAttribute(*op1, m_AttributeY.c_str(), y)) 
		{
			FloatVariant* f = static_cast<FloatVariant*>(y);
			e.SetYf(f->GetFloat());
			delete y;
		} else 
		{
			Assert(false);
			return kMissingAttribute;
		}
		Variant* z = NULL;
		if (HasAttribute(*op2, m_AttributeZ.c_str(), z)) 
		{
			FloatVariant* f = static_cast<FloatVariant*>(z);
			e.SetZf(f->GetFloat());
			delete z;
		} else 
		{
			Assert(false);
			return kMissingAttribute;
		}

		QuatV q = QuatVFromEulersXYZ(e);

		crExpressionOpConstant::Value val;
		val.Set(q);
		crExpressionOpConstant& o = static_cast<crExpressionOpConstant&>(op);
		o.SetValue(val);

		return kInitializationSucceeded;
	}

private:
	atString m_AttributeX;
	atString m_AttributeY;
	atString m_AttributeZ;
	int m_IndexX;
	int m_IndexY;
	int m_IndexZ;
};

////////////////////////////////////////////////////////////////////////////////

class ExpressionOptimizerNode 
{
public:
	virtual crExpressionOp* CreateExpressionOp(ExpressionRuleContext& context) const = 0;

	virtual void CreateList(ExpressionRuleContext& context, atArray<crExpressionOp*>& list) const = 0;
	virtual bool IsList() const = 0;

	virtual ~ExpressionOptimizerNode() 
	{ 
		for (int i = 0; i < m_Children.GetCount(); ++i)
		{
			delete m_Children[i];
		}

		for (int i = 0; i < m_Initializers.GetCount(); ++i)
		{
			delete m_Initializers[i];
		}
	}	

	void AddChild(ExpressionOptimizerNode* child) 
	{
		m_Children.PushAndGrow(child);
	}

	void AddInitializer(ExpressionOpInitializer* initializer)
	{
		m_Initializers.PushAndGrow(initializer);
	}

	crExpressionOp* Create(ExpressionRuleContext& context) const
	{
		crExpressionOp* op = CreateExpressionOp(context);
		for (int i = 0; i < m_Initializers.GetCount(); ++i) 
		{
			ExpressionOpInitializer* initializer = m_Initializers[i];
			initializer->Init(*op, context);
		}
		for (int i = 0; i < m_Children.GetCount(); ++i) 
		{
			// If the child is a list it has to be a leaf anyway, so just check whether it's a list and if it is use a different code path

			if (m_Children[i]->IsList()) 
			{
				atArray<crExpressionOp*> children;
				m_Children[i]->CreateList(context, children);
				for (int c = 0; c < children.GetCount(); ++c) 
				{
					if (op->GetOpType() == crExpressionOp::kOpTypeNary) 
					{
						crExpressionOpNary* nary = static_cast<crExpressionOpNary*>(op);
						nary->AppendSource(children[c]);
					} 
					else 
					{
						delete op->GetChild(c);
						op->SetChild(c, *children[c]);
					}
				}
			} 
			else 
			{
				if (op->GetOpType() == crExpressionOp::kOpTypeNary) 
				{
					crExpressionOpNary* nary = static_cast<crExpressionOpNary*>(op);
					nary->AppendSource(m_Children[i]->Create(context));
				} 
				else 
				{
					delete op->GetChild(i);
					op->SetChild(i, *m_Children[i]->Create(context));
				}
			}
		}

		return op;
	}

private:
	atArray<ExpressionOptimizerNode*> m_Children;
	atArray<ExpressionOpInitializer*> m_Initializers;
};

////////////////////////////////////////////////////////////////////////////////

class NodeByIdentifier : public ExpressionOptimizerNode
{
public:
	NodeByIdentifier(const char* type) : m_Type (type) { }
	virtual ~NodeByIdentifier() { }

	virtual crExpressionOp* CreateExpressionOp(ExpressionRuleContext& context) const;

	virtual void CreateList(ExpressionRuleContext&, atArray<crExpressionOp*>&) const
	{ 
		Assert(false);
	}

	virtual bool IsList() const 
	{
		return false;
	}

private:
	atString m_Type;
};

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* NodeByIdentifier::CreateExpressionOp(ExpressionRuleContext& ) const
{
	if (m_Type == "OpTypeConstant") 
	{
		crExpressionOpConstant* op = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
		return op;
	} 
	else if (m_Type == "OpTypeConstantFloat") 
	{
		crExpressionOpConstantFloat* op = static_cast<crExpressionOpConstantFloat*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstantFloat));
		return op;
	} 
	else if (m_Type == "OpTypeGet") 
	{
		crExpressionOpGet* op = static_cast<crExpressionOpGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeGet));
		return op;
	} 
	else if (m_Type == "OpTypeSet") 
	{
		crExpressionOpSet* op = static_cast<crExpressionOpSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeSet));
		return op;
	} 
	else if (m_Type == "OpTypeComponentGet") 
	{ 
		crExpressionOpComponentGet* op = static_cast<crExpressionOpComponentGet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentGet));
		return op;
	} 
	else if (m_Type == "OpTypeComponentSet") 
	{
		crExpressionOpComponentSet* op = static_cast<crExpressionOpComponentSet*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeComponentSet));
		return op;
	} 
	else if (m_Type == "NullaryOpTypeZero") 
	{
		crExpressionOpNullary* op = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		op->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeZero);
		return op;
	} 
	else if (m_Type == "NullaryOpTypeOne") 
	{
		crExpressionOpNullary* op = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		op->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeOne);
		return op;
	} 
	else if (m_Type == "NullaryOpTypePi") 
	{
		crExpressionOpNullary* op = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		op->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypePi);
		return op;
	} 
	else if (m_Type == "NullaryOpTypeTime") 
	{
		crExpressionOpNullary* op = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		op->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeTime);
		return op;
	} 
	else if (m_Type == "NullaryOpTypeRandom") 
	{
		crExpressionOpNullary* op = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		op->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeRandom);
		return op;
	} 
	else if (m_Type == "NullaryOpTypeVectorZero") 
	{
		crExpressionOpNullary* op = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		op->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeVectorZero);
		return op;
	} 
	else if (m_Type == "NullaryOpTypeVectorOne") 
	{
		crExpressionOpNullary* op = static_cast<crExpressionOpNullary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNullary));
		op->SetNullaryOpType(crExpressionOpNullary::kNullaryOpTypeVectorOne);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeLogicalNot") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeLogicalNot);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeNegate") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeNegate);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeReciprocal") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeReciprocal);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeSquare") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeSquare);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeSqrt") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeSqrt);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeAbsolute") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeAbsolute);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeFloor") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeFloor);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeCeil") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeCeil);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeLog") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeLog);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeLn") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeLn);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeExp") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeExp);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeClamp01") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeClamp01);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeCos") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeCos);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeSin") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeSin);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeTan") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeTan);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeArcCos") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeArcCos);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeArcSin") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeArcSin);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeArcTan") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeArcTan);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeCosH") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeCosH);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeSinH") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeSinH);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeTanH") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeTanH);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeDegreesToRadians") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeDegreesToRadians);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeRadiansToDegrees") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeFromEuler") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeFromEuler);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeToEuler") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeToEuler);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeSplat") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeSplat);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeVectorClamp01") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeVectorClamp01);
		return op;
	} 
	else if (m_Type == "UnaryOpTypeQuatInverse") 
	{
		crExpressionOpUnary* op = static_cast<crExpressionOpUnary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeUnary));
		op->SetUnaryOpType(crExpressionOpUnary::kUnaryOpTypeQuatInverse);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeEqualTo") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeEqualTo);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeNotEqualTo") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeNotEqualTo);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeGreaterThan") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeGreaterThan);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeLessThan") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeLessThan);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeGreaterThanEqualTo") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeGreaterThanEqualTo);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeLessThanEqualTo") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeLessThanEqualTo);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeAdd") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeAdd);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeSubtract") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeSubtract);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeMultiply") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMultiply);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeDivide") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeDivide);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeModulus") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeModulus);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeExponent") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeExponent);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeMax") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMax);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeMin") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeMin);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeLogicalAnd") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeLogicalAnd);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeLogicalOr") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeLogicalOr);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeLogicalXor") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeLogicalXor);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeQuatMultiply") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeQuatMultiply);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeVectorAdd") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeVectorAdd);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeVectorMultiply") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeVectorMultiply);
		return op;
	} 
	else if (m_Type == "BinaryOpTypeVectorTransform") 
	{
		crExpressionOpBinary* op = static_cast<crExpressionOpBinary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeBinary));
		op->SetBinaryOpType(crExpressionOpBinary::kBinaryOpTypeVectorTransform);
		return op;
	} 
	else if (m_Type == "TernaryOpTypeMultiplyAdd") 
	{
		crExpressionOpTernary* op = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		op->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeMultiplyAdd);
		return op;
	} 
	else if (m_Type == "TernaryOpTypeClamp") 
	{
		crExpressionOpTernary* op = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		op->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeClamp);
		return op;
	} 
	else if (m_Type == "TernaryOpTypeLerp") 
	{
		crExpressionOpTernary* op = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		op->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeLerp);
		return op;
	} 
	else if (m_Type == "TernaryOpTypeToQuaternion") 
	{
		crExpressionOpTernary* op = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		op->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeToQuaternion);
		return op;
	} 
	else if (m_Type == "TernaryOpTypeToVector") 
	{
		crExpressionOpTernary* op = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		op->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeToVector);
		return op;
	} 
	else if (m_Type == "TernaryOpTypeConditional") 
	{
		crExpressionOpTernary* op = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		op->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeConditional);
		return op;
	} 
	else if (m_Type == "TernaryOpTypeVectorMultiplyAdd") 
	{
		crExpressionOpTernary* op = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		op->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeVectorMultiplyAdd);
		return op;
	} 
	else if (m_Type == "TernaryOpTypeVectorLerpVector") 
	{
		crExpressionOpTernary* op = static_cast<crExpressionOpTernary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeTernary));
		op->SetTernaryOpType(crExpressionOpTernary::kTernaryOpTypeVectorLerpVector);
		return op;
	} 
	else if (m_Type == "NaryOpTypeComma") 
	{
		crExpressionOpNary* op = static_cast<crExpressionOpNary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNary));
		op->SetNaryOpType(crExpressionOpNary::kNaryOpTypeComma);
		return op;
	} 
	else if (m_Type == "NaryOpTypeSum") 
	{
		crExpressionOpNary* op = static_cast<crExpressionOpNary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNary));
		op->SetNaryOpType(crExpressionOpNary::kNaryOpTypeSum);
		return op;
	} 
	else if (m_Type == "NaryOpTypeList") 
	{
		crExpressionOpNary* op = static_cast<crExpressionOpNary*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeNary));
		op->SetNaryOpType(crExpressionOpNary::kNaryOpTypeList);
		return op;
	} 
	else if (m_Type == "OpTypeConstantVector") 
	{
		// Really just an OpTypeConstant
		crExpressionOpConstant* op = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
		return op;
	} 
	else if (m_Type == "OpTypeConstantQuat") 
	{
		crExpressionOpConstant* op = static_cast<crExpressionOpConstant*>(crExpressionOp::CreateExpressionOp(crExpressionOp::kOpTypeConstant));
		return op;
	}

	//		kOpTypeValid,
	//		kOpTypeObjectSpaceGet,
	//		kOpTypeObjectSpaceSet,
	//		kOpTypeObjectSpaceConvertTo,
	//		kOpTypeObjectSpaceConvertFrom,
	Errorf ("No idea what to do with identifier: %s", m_Type.c_str());
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

class NodeFromListIndex : public ExpressionOptimizerNode
{
public:
	NodeFromListIndex(int listIndex, int itemIndex) 
		: m_ListIndex (listIndex)
		, m_ItemIndex (itemIndex) { }
	virtual ~NodeFromListIndex() { }

	virtual crExpressionOp* CreateExpressionOp(ExpressionRuleContext& context) const;

	virtual void CreateList(ExpressionRuleContext&, atArray<crExpressionOp*>&) const
	{ 
		Assert(false);
	}

	virtual bool IsList() const 
	{
		return false;
	}

private:
	int m_ListIndex;
	int m_ItemIndex;
};

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* NodeFromListIndex::CreateExpressionOp(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList* list = NULL;
	context.GetList(m_ListIndex, list);

	if (!(0 <= m_ItemIndex && m_ItemIndex < list->GetCount())) 
	{
		Assert(false);
		return NULL;
	}

	const crExpressionOp* op = (*list)[m_ItemIndex];
	return op->Clone();
}

////////////////////////////////////////////////////////////////////////////////

class NodeFromItemIndex : public ExpressionOptimizerNode
{
public:
	NodeFromItemIndex(int itemIndex) 
		: m_ItemIndex (itemIndex) { }
	virtual ~NodeFromItemIndex() { }

	virtual crExpressionOp* CreateExpressionOp(ExpressionRuleContext& context) const;

	virtual void CreateList(ExpressionRuleContext&, atArray<crExpressionOp*>&) const
	{ 
		Assert(false);
	}

	virtual bool IsList() const 
	{
		return false;
	}

private:
	int m_ItemIndex;
};

////////////////////////////////////////////////////////////////////////////////

crExpressionOp* NodeFromItemIndex::CreateExpressionOp(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& list = context.GetLastList();

	if (!(0 <= m_ItemIndex && m_ItemIndex < list.GetCount())) 
	{
		Assert(false);
		return NULL;
	}

	const crExpressionOp* op = list[m_ItemIndex];
	return op->Clone();
}

////////////////////////////////////////////////////////////////////////////////

class NodeLastList : public ExpressionOptimizerNode
{
public:
	virtual ~NodeLastList() { }

	virtual crExpressionOp* CreateExpressionOp(ExpressionRuleContext& ) const
	{
		Assert(false); // Invalid call
		return NULL;
	}

	virtual void CreateList(ExpressionRuleContext&, atArray<crExpressionOp*>&) const;

	virtual bool IsList() const 
	{
		return true;
	}
};

////////////////////////////////////////////////////////////////////////////////

void NodeLastList::CreateList(ExpressionRuleContext& context, atArray<crExpressionOp*>& newList) const
{
	const ExpressionRuleContext::ExpressionOpList& list = context.GetLastList();
	for (int i = 0; i < list.GetCount(); ++i) 
	{
		newList.PushAndGrow(list[i]->Clone());
	}
}

////////////////////////////////////////////////////////////////////////////////

class NodeListIndex : public ExpressionOptimizerNode
{
public:
	NodeListIndex(int listIndex) 
		: m_ListIndex (listIndex) { }
	virtual ~NodeListIndex() { }

	virtual crExpressionOp* CreateExpressionOp(ExpressionRuleContext& ) const
	{
		Assert(false); // Invalid call
		return NULL;
	}

	virtual void CreateList(ExpressionRuleContext&, atArray<crExpressionOp*>&) const;

	virtual bool IsList() const 
	{
		return true;
	}

private:
	int m_ListIndex;
};

////////////////////////////////////////////////////////////////////////////////

void NodeListIndex::CreateList(ExpressionRuleContext& context, atArray<crExpressionOp*>& newList) const
{
	const ExpressionRuleContext::ExpressionOpList* list = NULL;
	context.GetList(m_ListIndex, list);

	for (int i = 0; i < list->GetCount(); ++i) 
	{
		newList.PushAndGrow((*list)[i]->Clone());
	}
}

////////////////////////////////////////////////////////////////////////////////

void Descriptor(Tokenizer& t)
{
	if (t.Accept(Token::kLeftBracket))
	{
		t.Expect(Token::kIdentifier);
		t.Expect(Token::kRightBracket);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Filter(Tokenizer& t)
{
	if (t.Accept(Token::kListIndex))
	{
		if (t.Accept(Token::kPeriod)) 
		{
			if (t.Accept(Token::kChildExpansion)) 
			{
			} 
			else if (t.Accept(Token::kLengthCheck)) 
			{
			} 
			else if (t.Accept(Token::kUnion)) 
 			{
			} 
			else if (t.Accept(Token::kExclusiveUnion)) 
			{
				t.Expect(Token::kLeftParen);
				t.Expect(Token::kListIndex);
				t.Expect(Token::kInteger);
				t.Expect(Token::kRightParen);
			} 
			else 
			{
				Errorf("error");
			}
		} 
		else 
		{
			// Just the list index as a filter
			t.Expect(Token::kInteger);

			if (t.Accept(Token::kPeriod)) 
			{
				// Then we have an operation on the list, at the moment it can only be sum but we don't check that...
				t.Expect(Token::kIdentifier);
			}
		}

	} 
	else if (t.Accept(Token::kItemIndex)) 
	{
		t.Expect(Token::kInteger);
		if (t.Accept(Token::kPeriod)) 
		{
			if (t.Accept(Token::kAttribute)) 
			{
				t.Expect(Token::kIdentifier);
				if (t.Accept(Token::kEquals)) 
				{
				}

				if (t.Accept(Token::kInteger)) 
				{
				}
			} 
			else 
			{
				// can have other ops like union and shit, but maybe should cull most of this '.' shit out...
			}
		}
	} 
	else if (t.Accept(Token::kAttribute)) 
	{
		t.Expect(Token::kIdentifier);
		if (t.Accept(Token::kEquals)) 
		{
		} 
		else if (t.Accept(Token::kNotEquals))
		{
		}
		else if (t.Accept(Token::kLessThanEqualTo))
		{
		}
		else if (t.Accept(Token::kGreaterThanEqualTo))
		{
		}
		else
		{
			t.Expect(Token::kGreaterThan);
		}

		// Check if it's a float or a constant string
		if (t.Accept(Token::kIdentifier)) 
		{
		} 
		else if (t.Accept(Token::kLeftBrace)) 
		{
			// Probably some kind of array attribute...
			do {
				t.Accept(Token::kSubtract);
				t.Expect(Token::kFloatingPoint);
			} while (t.Accept(Token::kComma));

			t.Expect(Token::kRightBrace);
		} 
		else if (t.Accept(Token::kSubtract)) 
		{
			// For negative numbers
			t.Expect(Token::kFloatingPoint);
		} 
		else if (t.Accept(Token::kFloatingPoint)) 
		{
			// Just the value
		} 
		else if (t.Accept(Token::kInteger))
		{
		}
		else 
		{
			// =' ' is valid and just means that everything should be equal
		}
	} 
	else if (t.Accept(Token::kIdentifier)) 
	{
		// Another type filter
	} 
	else 
	{
		if (t.Accept(Token::kChildExpansion)) 
		{
		} 
		else if (t.Accept(Token::kLengthCheck)) 
		{
			t.Expect(Token::kEquals);
			t.Expect(Token::kInteger); // How long do you expect the list to be?
		} 
		else if (t.Accept(Token::kUnion)) 
		{
			t.Expect(Token::kLeftParen);
			t.Expect(Token::kListIndex);
			t.Expect(Token::kInteger);
			t.Expect(Token::kRightParen);
		} 
		else if (t.Accept(Token::kExclusiveUnion)) 
		{
			t.Expect(Token::kLeftParen);
			t.Expect(Token::kListIndex);
			t.Expect(Token::kInteger);
			t.Expect(Token::kRightParen);
		}
		else if (t.Accept(Token::kMatch))
		{
			t.Expect(Token::kLeftParen);
			t.Expect(Token::kListIndex);
			t.Expect(Token::kInteger);
			t.Expect(Token::kLeftParen);
			t.Expect(Token::kInteger);
			t.Expect(Token::kRightParen);
			t.Expect(Token::kRightParen);
		}
		else 
		{
			Errorf("error");
		}
	}

	t.Expect(Token::kRightBracket);
}

////////////////////////////////////////////////////////////////////////////////

void Filters(Tokenizer& t)
{
	while (t.Accept(Token::kLeftBracket) ) 
	{
		Filter(t);
	}

	if (t.Accept(Token::kTilde)) 
	{
		// Store modifier and continue
		Filters(t);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Child(Tokenizer& t)
{
	if (t.Accept(Token::kIdentifier)) 
	{
		if (t.Accept(Token::kLeftBrace)) 
		{
			// then we can have one or more assignments
			do {
				t.Expect(Token::kIdentifier);
				t.Expect(Token::kEquals);

				// parse an expression
				if (t.Accept(Token::kLeftBracket)) 
				{
					while (!t.Accept(Token::kRightBracket)) 
					{
						// At the moment just consume the expression
						t.Accept(Token::kFloatingPoint);
						t.Accept(Token::kPlus);
						t.Accept(Token::kDivide);
						t.Accept(Token::kMultiply);
						t.Accept(Token::kSubtract);
						t.Accept(Token::kNegation);
						if (t.Accept(Token::kListIndex)) 
						{
							t.Expect(Token::kInteger);
							t.Expect(Token::kLeftParen);
							t.Expect(Token::kInteger);
							t.Expect(Token::kRightParen);
							t.Expect(Token::kPeriod);
							t.Expect(Token::kAttribute);
							t.Expect(Token::kIdentifier);
						}
						if (t.Accept(Token::kItemIndex)) 
						{
							t.Expect(Token::kInteger);
							t.Expect(Token::kPeriod);
							t.Expect(Token::kAttribute);
							t.Expect(Token::kIdentifier);
						}
					}
				} 
				else 
				{
					t.Accept(Token::kSubtract); // Accept that it might be a negation
					if (t.Accept(Token::kListIndex)) 
					{
						t.Expect(Token::kInteger);
						t.Expect(Token::kLeftParen);
						t.Expect(Token::kInteger);
						t.Expect(Token::kRightParen);
						t.Expect(Token::kPeriod);
						t.Expect(Token::kAttribute);
						t.Expect(Token::kIdentifier);
					}
					else
					{
						t.Expect(Token::kItemIndex);
						t.Expect(Token::kInteger);
						t.Expect(Token::kPeriod);
						t.Expect(Token::kAttribute);
						t.Expect(Token::kIdentifier);
					}
				}
			} while(t.Accept(Token::kComma));

			t.Expect(Token::kRightBrace);
		} 
		else if (t.Accept(Token::kChildren)) 
		{
			t.Expect(Token::kLeftBracket);
			do {
				Child(t); // recursively get children
			} while (t.Accept(Token::kComma));
			t.Expect(Token::kRightBracket);
		}
	} 
	else if (t.Accept(Token::kListIndex)) 
	{
		t.Expect(Token::kInteger);
		t.Expect(Token::kPeriod);
		t.Expect(Token::kLeftParen);
		t.Expect(Token::kInteger);
		t.Expect(Token::kRightParen);

		if (t.Accept(Token::kChildren))
		{
			t.Expect(Token::kLeftBracket);
			do
			{
				Child(t);
			} while (t.Accept(Token::kComma));
			t.Expect(Token::kRightBracket);
		}
	} 
	else if (t.Accept(Token::kItemIndex)) 
	{
		t.Expect(Token::kInteger);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Substitution(Tokenizer& t)
{
	t.Expect(Token::kLeftBracket);
	if (t.Accept(Token::kIdentifier)) 
	{
		if (t.Accept(Token::kLeftBrace)) 
		{
			// then we can have one or more assignments
			if (t.Accept(Token::kItemIndex)) 
			{
				// then it looks like we have a vector or quat assignment
				t.Expect(Token::kInteger);
				t.Expect(Token::kPeriod);
				t.Expect(Token::kAttribute);
				t.Expect(Token::kIdentifier);

				while (t.Accept(Token::kComma)) 
				{
					t.Expect(Token::kItemIndex);
					t.Expect(Token::kInteger);
					t.Expect(Token::kPeriod);
					t.Expect(Token::kAttribute);
					t.Expect(Token::kIdentifier);
				}
			} 
			else 
			{
				do {
					t.Expect(Token::kIdentifier);
					t.Expect(Token::kEquals);

					if (t.Accept(Token::kLeftBracket))
					{
						while (!t.Accept(Token::kRightBracket))
						{
							// At the moment just consume the expression
							t.Accept(Token::kFloatingPoint);
							t.Accept(Token::kPlus);
							t.Accept(Token::kDivide);
							t.Accept(Token::kMultiply);
							t.Accept(Token::kSubtract);
							t.Accept(Token::kNegation);
							t.Accept(Token::kLeftParen);
							t.Accept(Token::kRightParen);
							if (t.Accept(Token::kListIndex)) 
							{
								t.Expect(Token::kInteger);
								t.Expect(Token::kLeftParen);
								t.Expect(Token::kInteger);
								t.Expect(Token::kRightParen);
								t.Expect(Token::kPeriod);
								t.Expect(Token::kAttribute);
								t.Expect(Token::kIdentifier);
							}
							if (t.Accept(Token::kItemIndex)) 
							{
								t.Expect(Token::kInteger);
								t.Expect(Token::kPeriod);
								t.Expect(Token::kAttribute);
								t.Expect(Token::kIdentifier);
							}
						}
					}
					else if (t.Accept(Token::kListIndex))
					{
						t.Expect(Token::kInteger);
						t.Expect(Token::kLeftParen);
						t.Expect(Token::kInteger);
						t.Expect(Token::kRightParen);
						t.Expect(Token::kPeriod);
						t.Expect(Token::kAttribute);
						t.Expect(Token::kIdentifier);
					}
					else if (t.Accept(Token::kFloatingPoint))
					{
					}
					else if (t.Accept(Token::kInteger))
					{
					}
					else
					{
						t.Expect(Token::kItemIndex);
						t.Expect(Token::kInteger);
						t.Expect(Token::kPeriod);
						t.Expect(Token::kAttribute);
						t.Expect(Token::kIdentifier);
					}
				} while (t.Accept(Token::kComma)); // then we have more than one assignment
			}

			t.Expect(Token::kRightBrace);
		} 
		else if (t.Accept(Token::kChildren)) 
		{
			if (t.Accept(Token::kLeftParen)) 
			{
				// then we must have a list
				if (t.Accept(Token::kItemIndex)) 
				{
					// it's a list, so there's no index into the item
				} 
				else if (t.Accept(Token::kListIndex)) 
				{
					t.Expect(Token::kInteger);
					// it's a list, so there's no index into the item
				}

				t.Expect(Token::kRightParen);
			} 
			else 
			{
				t.Expect(Token::kLeftBracket);
				do {
					Child(t);
				} while (t.Accept(Token::kComma));
				// Can then have one or more substitutions
				t.Expect(Token::kRightBracket);
			}
		}
	} 
	else if (t.Accept(Token::kItemIndex)) 
	{
		t.Expect(Token::kInteger);
	} 
	else if (t.Accept(Token::kListIndex)) 
	{
		t.Expect(Token::kInteger);
		t.Expect(Token::kPeriod);
		t.Expect(Token::kLeftParen);
		t.Expect(Token::kInteger);
		t.Expect(Token::kRightParen);

		if (t.Accept(Token::kChildren))
		{
			t.Expect(Token::kLeftBracket);
			do
			{
				Child(t);
			} while (t.Accept(Token::kComma));
			t.Expect(Token::kRightBracket);
		}
	}

	t.Expect(Token::kRightBracket);
}

////////////////////////////////////////////////////////////////////////////////

int FromStringToInt(const char* s)
{
	int i;
	if (EOF == sscanf(s, "%d", &i))
	{
		Errorf("Failed to convert to type!");
	}
	return i;
}

////////////////////////////////////////////////////////////////////////////////

float FromStringToFloat(const char* s)
{
	float f;
	if (EOF == sscanf(s, "%f", &f))
	{
		Errorf("Failed to convert to type!");
	}
	return f;
}

////////////////////////////////////////////////////////////////////////////////

void Tokenizer::Tokenize(const char* rule, atArray<Token*>& tokens)
{
	m_Tokens = &tokens;
	c = rule;

	GetSymbol();
	Descriptor(*this);

	Filters(*this);

	Expect(Token::kColon);

	Substitution(*this);

	c = NULL;
	m_Tokens = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void Tokenizer::GetSymbol()
{
	// Just ingnore whitespace
	while (*c == ' ') 
	{
		++c;
	}

	atArray<Token*>& tokens = *m_Tokens;
	if (*c == '[') 
	{
		++c;
		m_Symbol = Token::kLeftBracket;
		tokens.PushAndGrow(rage_new LeftBracketToken);
	} 
	else if (*c == ']') 
	{
		++c;
		m_Symbol = Token::kRightBracket;
		tokens.PushAndGrow(rage_new RightBracketToken);
	}
	else if (isalpha(*c))
	{ 
		// first character needs to be alpha, now can be an alpha or digit...
		const char* s = c;
		while (isalpha(*c) || isdigit(*c)) 
		{
			++c;
		}
		int len = ptrdiff_t_to_int(c-s);
		if (strncmp("children", s, len) == 0) 
		{
			m_Symbol = Token::kChildExpansion;
			tokens.PushAndGrow(rage_new ChildExpansionToken);
		} 
		else if (strncmp("length", s, len) == 0) 
		{
			m_Symbol = Token::kLengthCheck;
			tokens.PushAndGrow(rage_new LengthCheckToken);
		} 
		else if (strncmp("union", s, len) == 0) 
		{
			m_Symbol = Token::kUnion;
			tokens.PushAndGrow(rage_new UnionToken);
		} 
		else if (strncmp("xunion", s, len) == 0) 
		{
			m_Symbol = Token::kExclusiveUnion;
			tokens.PushAndGrow(rage_new ExclusiveUnionToken);
		} 
		else if (strncmp("match", s, len) == 0)
		{
			m_Symbol = Token::kMatch;
			tokens.PushAndGrow(rage_new MatchToken);
		}
		else 
		{
			m_Symbol = Token::kIdentifier; // not a keyword, some other 
			char temp[1024];
			memset(temp, 0, sizeof(temp));
			strncpy(temp, s, c-s);
			tokens.PushAndGrow(rage_new IdentifierToken (temp));
		}
	}
	else if (*c == '@') 
	{
		++c;
		m_Symbol = Token::kAttribute;
		tokens.PushAndGrow(rage_new AttributeToken);
	} 
	else if (*c == '=') 
	{
		++c;
		m_Symbol = Token::kEquals;
		tokens.PushAndGrow(rage_new EqualsToken);
	} 
	else if (*c == '!' && *(c+1) == '=') 
	{
		++c; ++c;
		m_Symbol = Token::kNotEquals;
		tokens.PushAndGrow(rage_new NotEqualsToken);
	} 
	else if (*c == '!')
	{
		++c;
		m_Symbol = Token::kNegation;
		tokens.PushAndGrow(rage_new NegationToken);
	}
	else if (*c == '<' && *(c+1) == '=')
	{
		++c; ++c;
		m_Symbol = Token::kLessThanEqualTo;
		tokens.PushAndGrow(rage_new LessThanEqualToToken);
	}
	else if (*c == '<')
	{
		++c;
		m_Symbol = Token::kLessThan;
		tokens.PushAndGrow(rage_new LessThanToken);
	}
	else if (*c == '>' && *(c+1) == '=')
	{
		++c; ++c;
		m_Symbol = Token::kGreaterThanEqualTo;
		tokens.PushAndGrow(rage_new GreaterThanEqualToToken);
	}
	else if (*c == '>')
	{
		++c;
		m_Symbol = Token::kGreaterThan;
		tokens.PushAndGrow(rage_new GreaterThanToken);
	}
	else if (*c == '-' && *(c+1) == '>') 
	{
		++c; ++c;
		m_Symbol = Token::kChildren;
		tokens.PushAndGrow(rage_new ChildrenToken);
	} 
	else if (*c == ':') 
	{
		++c;
		m_Symbol = Token::kColon;
		tokens.PushAndGrow(rage_new ColonToken);
	} 
	else if (*c == '{') 
	{
		++c;
		m_Symbol = Token::kLeftBrace;
		tokens.PushAndGrow(rage_new LeftBraceToken);
	} 
	else if (*c == '}') 
	{
		++c;
		m_Symbol = Token::kRightBrace;
		tokens.PushAndGrow(rage_new RightBraceToken);
	} 
	else if (*c == '(') 
	{
		++c;
		m_Symbol = Token::kLeftParen;
		tokens.PushAndGrow(rage_new LeftParenToken);
	} 
	else if (*c == ')') 
	{
		++c;
		m_Symbol = Token::kRightParen;
		tokens.PushAndGrow(rage_new RightParenToken);
	} 
	else if (isdigit(*c)) 
	{
		const char* s = c;
		while (isdigit(*c)) 
		{
			++c;
		}
		if (*c == '.' && isdigit(*(c+1))) 
		{
			++c;
			while (isdigit(*c)) 
			{
				++c;
			}
			m_Symbol = Token::kFloatingPoint;
			char temp[32];
			memset(temp, 0, sizeof(temp));
			strncpy(temp, s, c-s);
			tokens.PushAndGrow(rage_new FloatingPointToken(FromStringToFloat(temp))); // Convert to float
		} 
		else 
		{
			m_Symbol = Token::kInteger;
			char temp[32];
			memset(temp, 0, sizeof(temp));
			strncpy(temp, s, c-s);
			tokens.PushAndGrow(rage_new IntegerToken(FromStringToInt(temp))); // Convert to integer
		}
	} 
	else if (*c == '$') 
	{
		++c;
		m_Symbol = Token::kItemIndex;
		tokens.PushAndGrow(rage_new ItemIndexToken);
	} 
	else if (*c == '.') 
	{
		++c;
		m_Symbol = Token::kPeriod;
		tokens.PushAndGrow(rage_new PeriodToken);
	} 
	else if (*c == ',') 
	{
		++c;
		m_Symbol = Token::kComma;
		tokens.PushAndGrow(rage_new CommaToken);
	} 
	else if (*c == '%') 
	{
		++c;
		m_Symbol = Token::kListIndex;
		tokens.PushAndGrow(rage_new ListIndexToken);
	} 
	else if (*c == '+') 
	{
		++c;
		m_Symbol = Token::kPlus;
		tokens.PushAndGrow(rage_new PlusToken);
	} 
	else if (*c == '-') 
	{
		++c;
		m_Symbol = Token::kSubtract;
		tokens.PushAndGrow(rage_new SubtractToken);
	} 
	else if (*c == '/') 
	{
		++c;
		m_Symbol = Token::kDivide;
		tokens.PushAndGrow(rage_new DivideToken);
	} 
	else if (*c == '*') 
	{
		++c;
		m_Symbol = Token::kMultiply;
		tokens.PushAndGrow(rage_new MultiplyToken);
	} 
	else if (*c == '~') 
	{
		++c;
		m_Symbol = Token::kTilde;
		tokens.PushAndGrow(rage_new TildeToken);
	} 
	else 
	{
		m_Symbol = Token::kInvalidSymbol;
	}
}

////////////////////////////////////////////////////////////////////////////////

class ExpressionOptimizerFilterProcess
{
public:
	enum Modifier 
	{
		kNone,
		kContinueOnEmpty,
	};

	enum Result
	{
		kFilterProcessSucceeded,
		kIndexOutOfRange,
	};

	ExpressionOptimizerFilterProcess(Modifier modifier) 
		: m_Modifier (modifier) { }
	virtual ~ExpressionOptimizerFilterProcess() { }

	// return true to continue processing
	virtual Result Process(ExpressionRuleContext& context) const = 0;

	Modifier GetModifier() const 
	{
		return m_Modifier;
	}
private:
	Modifier m_Modifier;
};

////////////////////////////////////////////////////////////////////////////////

class ExpressionTypeFilter : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	ExpressionTypeFilter(const char* type, Modifier modifier) 
		: ExpressionOptimizerFilterProcess (modifier)
		, m_Type (type) { }
	virtual ~ExpressionTypeFilter() { }

	atString m_Type;
};

////////////////////////////////////////////////////////////////////////////////

static const char* ExpressionOpTypeToString(const crExpressionOp& op)
{
	crExpressionOp::eOpTypes opType = op.GetOpType();
	switch (opType) 
	{
		case crExpressionOp::kOpTypeConstant:
			return "OpTypeConstant";
		case crExpressionOp::kOpTypeConstantFloat:
			return "OpTypeConstantFloat";
		case crExpressionOp::kOpTypeGet:
			return "OpTypeGet";
		case crExpressionOp::kOpTypeSet:
			return "OpTypeSet";
		case crExpressionOp::kOpTypeValid:
			return "OpTypeInvalid";
		case crExpressionOp::kOpTypeComponentGet:
			return "OpTypeComponentGet";
		case crExpressionOp::kOpTypeComponentSet:
			return "OpTypeComponentSet";
		case crExpressionOp::kOpTypeObjectSpaceGet:
			return "OpTypeObjectSpaceGet";
		case crExpressionOp::kOpTypeObjectSpaceSet:
			return "OpTypeObjectSpaceSet";
		case crExpressionOp::kOpTypeObjectSpaceConvertTo:
			return "OpTypeObjectSpaceConvertTo";
		case crExpressionOp::kOpTypeObjectSpaceConvertFrom:
			return "OpTypeObjectSpaceConvertFrom";
		case crExpressionOp::kOpTypeNullary:
			{
				const crExpressionOpNullary& nullary = static_cast<const crExpressionOpNullary&>(op);
				crExpressionOpNullary::eNullaryOpTypes nullaryOpType = nullary.GetNullaryOpType();
				switch (nullaryOpType) 
				{
					case crExpressionOpNullary::kNullaryOpTypeZero:
						return "NullaryOpTypeZero";
					case crExpressionOpNullary::kNullaryOpTypeOne:
						return "NullaryOpTypeOne";
					case crExpressionOpNullary::kNullaryOpTypePi:
						return "NullaryOpTypePi";
					case crExpressionOpNullary::kNullaryOpTypeTime:
						return "NullaryOpTypeTime";
					case crExpressionOpNullary::kNullaryOpTypeRandom:
						return "NullaryOpTypeRandom";
					case crExpressionOpNullary::kNullaryOpTypeVectorZero:
						return "NullaryOpTypeVectorZero";
					case crExpressionOpNullary::kNullaryOpTypeVectorOne:
						return "NullaryOpTypeVectorOne";
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeUnary:
			{
				const crExpressionOpUnary& unary = static_cast<const crExpressionOpUnary&>(op);
				crExpressionOpUnary::eUnaryOpTypes unaryOpType = unary.GetUnaryOpType();
				switch (unaryOpType) 
				{
					case crExpressionOpUnary::kUnaryOpTypeLogicalNot:
						return "UnaryOpTypeLogicalNot";
					case crExpressionOpUnary::kUnaryOpTypeNegate:
						return "UnaryOpTypeNegate";
					case crExpressionOpUnary::kUnaryOpTypeReciprocal:
						return "UnaryOpTypeReciprocal";
					case crExpressionOpUnary::kUnaryOpTypeSquare:
						return "UnaryOpTypeSquare";
					case crExpressionOpUnary::kUnaryOpTypeSqrt:
						return "UnaryOpTypeSqrt";
					case crExpressionOpUnary::kUnaryOpTypeAbsolute:
						return "UnaryOpTypeAbsolute";
					case crExpressionOpUnary::kUnaryOpTypeFloor:
						return "UnaryOpTypeFloor";
					case crExpressionOpUnary::kUnaryOpTypeCeil:
						return "UnaryOpTypeCeil";
					case crExpressionOpUnary::kUnaryOpTypeLog:
						return "UnaryOpTypeLog";
					case crExpressionOpUnary::kUnaryOpTypeLn:
						return "UnaryOpTypeLn";
					case crExpressionOpUnary::kUnaryOpTypeExp:
						return "UnaryOpTypeExp";
					case crExpressionOpUnary::kUnaryOpTypeClamp01:
						return "UnaryOpTypeClamp01";
					case crExpressionOpUnary::kUnaryOpTypeCos:
						return "UnaryOpTypeCos";
					case crExpressionOpUnary::kUnaryOpTypeSin:
						return "UnaryOpTypeSin";
					case crExpressionOpUnary::kUnaryOpTypeTan:
						return "UnaryOpTypeTan";
					case crExpressionOpUnary::kUnaryOpTypeArcCos:
						return "UnaryOpTypeArcCos";
					case crExpressionOpUnary::kUnaryOpTypeArcSin:
						return "UnaryOpTypeArcSin";
					case crExpressionOpUnary::kUnaryOpTypeArcTan:
						return "UnaryOpTypeArcTan";
					case crExpressionOpUnary::kUnaryOpTypeCosH:
						return "UnaryOpTypeCosH";
					case crExpressionOpUnary::kUnaryOpTypeSinH:
						return "UnaryOpTypeSinH";
					case crExpressionOpUnary::kUnaryOpTypeTanH:
						return "UnaryOpTypeTanH";
					case crExpressionOpUnary::kUnaryOpTypeDegreesToRadians:
						return "UnaryOpTypeDegreesToRadians";
					case crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees:
						return "UnaryOpTypeRadiansToDegrees";
					case crExpressionOpUnary::kUnaryOpTypeFromEuler:
						return "UnaryOpTypeFromEuler";
					case crExpressionOpUnary::kUnaryOpTypeToEuler:
						return "UnaryOpTypeToEuler";
					case crExpressionOpUnary::kUnaryOpTypeSplat:
						return "UnaryOpTypeSplat";
					case crExpressionOpUnary::kUnaryOpTypeVectorClamp01:
						return "UnaryOpTypeVectorClamp01";
					case crExpressionOpUnary::kUnaryOpTypeQuatInverse:
						return "UnaryOpTypeQuatInverse";
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeBinary:
			{
				const crExpressionOpBinary& binary = static_cast<const crExpressionOpBinary&>(op);
				crExpressionOpBinary::eBinaryOpTypes binaryOpType = binary.GetBinaryOpType();
				switch (binaryOpType) 
				{
					case crExpressionOpBinary::kBinaryOpTypeEqualTo:
						return "BinaryOpTypeEqualTo";
					case crExpressionOpBinary::kBinaryOpTypeNotEqualTo:
						return "BinaryOpTypeNotEqualTo";
					case crExpressionOpBinary::kBinaryOpTypeGreaterThan:
						return "BinaryOpTypeGreaterThan";
					case crExpressionOpBinary::kBinaryOpTypeLessThan:
						return "BinaryOpTypeLessThan";
					case crExpressionOpBinary::kBinaryOpTypeGreaterThanEqualTo:
						return "BinaryOpTypeGreaterThanEqualTo";
					case crExpressionOpBinary::kBinaryOpTypeLessThanEqualTo:
						return "BinaryOpTypeLessThanEqualTo";
					case crExpressionOpBinary::kBinaryOpTypeAdd:
						return "BinaryOpTypeAdd";
					case crExpressionOpBinary::kBinaryOpTypeSubtract:
						return "BinaryOpTypeSubtract";
					case crExpressionOpBinary::kBinaryOpTypeMultiply:
						return "BinaryOpTypeMultiply";
					case crExpressionOpBinary::kBinaryOpTypeDivide:
						return "BinaryOpTypeDivide";
					case crExpressionOpBinary::kBinaryOpTypeModulus:
						return "BinaryOpTypeModulus";
					case crExpressionOpBinary::kBinaryOpTypeExponent:
						return "BinaryOpTypeExponent";
					case crExpressionOpBinary::kBinaryOpTypeMax:
						return "BinaryOpTypeMax";
					case crExpressionOpBinary::kBinaryOpTypeMin:
						return "BinaryOpTypeMin";
					case crExpressionOpBinary::kBinaryOpTypeLogicalAnd:
						return "BinaryOpTypeLogicalAnd";
					case crExpressionOpBinary::kBinaryOpTypeLogicalOr:
						return "BinaryOpTypeLogicalOr";
					case crExpressionOpBinary::kBinaryOpTypeLogicalXor:
						return "BinaryOpTypeLogicalXor";
					case crExpressionOpBinary::kBinaryOpTypeQuatMultiply:
						return "BinaryOpTypeQuatMultiply";
					case crExpressionOpBinary::kBinaryOpTypeVectorAdd:
						return "BinaryOpTypeVectorAdd";
					case crExpressionOpBinary::kBinaryOpTypeVectorMultiply:
						return "BinaryOpTypeVectorMultiply";
					case crExpressionOpBinary::kBinaryOpTypeVectorTransform:
						return "BinaryOpTypeVectorTrasform";
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeTernary:
			{
				const crExpressionOpTernary& ternary = static_cast<const crExpressionOpTernary&>(op);
				crExpressionOpTernary::eTernaryOpTypes ternaryOpType = ternary.GetTernaryOpType();
				switch (ternaryOpType) 
				{
					case crExpressionOpTernary::kTernaryOpTypeMultiplyAdd:
						return "TernaryOpTypeMultiplyAdd";
					case crExpressionOpTernary::kTernaryOpTypeClamp:
						return "TernaryOpTypeClamp";
					case crExpressionOpTernary::kTernaryOpTypeLerp:
						return "TernaryOpTypeLerp";
					case crExpressionOpTernary::kTernaryOpTypeToQuaternion:
						return "TernaryOpTypeToQuaternion";
					case crExpressionOpTernary::kTernaryOpTypeToVector:
						return "TernaryOpTypeToVector";
					case crExpressionOpTernary::kTernaryOpTypeConditional:
						return "TernaryOpTypeConditional";
					case crExpressionOpTernary::kTernaryOpTypeVectorMultiplyAdd:
						return "TernaryOpTypeVectorMultiplyAdd";
					case crExpressionOpTernary::kTernaryOpTypeVectorLerpVector:
						return "TernaryOpTypeVectorLerpVector";
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeNary:
			{
				const crExpressionOpNary& nary = static_cast<const crExpressionOpNary&>(op);
				crExpressionOpNary::eNaryOpTypes naryOpType = nary.GetNaryOpType();
				switch (naryOpType) 
				{
					case crExpressionOpNary::kNaryOpTypeComma:
						return "NaryOpTypeComma";
					case crExpressionOpNary::kNaryOpTypeSum:
						return "NaryOpTypeSum";
					case crExpressionOpNary::kNaryOpTypeList:
						return "NaryOpTypeList";
					default:
						break;
				}
			}
			break;
		default:
			return "Unknown";
	}

	return "Unknown";
}

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result ExpressionTypeFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	for (int i = 0; i < tail.GetCount(); ++i) 
	{
		crExpressionOp* op = tail[i];
		if (m_Type == ExpressionOpTypeToString(*op)) 
		{
			context.PushFilterResult(op);
		}
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class ChildExpander : public ExpressionOptimizerFilterProcess
{
public:
	ChildExpander(Modifier modifier) 
		: ExpressionOptimizerFilterProcess(modifier) { }
	virtual ~ChildExpander() { }

	virtual Result Process(ExpressionRuleContext& context) const;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result ChildExpander::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	for (int i = 0; i < tail.GetCount(); ++i) 
	{
		crExpressionOp* op = tail[i];
		for (u32 c = 0; c < op->GetNumChildren(); ++c) 
		{
			crExpressionOp* child = op->GetChild(c);
			if (child) 
			{
				context.PushFilterResult(child);
			}
		}
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class LengthCheckFilter : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	LengthCheckFilter(int length, Modifier modifier) 
		: ExpressionOptimizerFilterProcess(modifier)
		, m_Length (length) { }
	virtual ~LengthCheckFilter() { }

	int m_Length;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result LengthCheckFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	int length = tail.GetCount();
	if (length == m_Length) 
	{
		for (int i = 0; i < length; ++i) 
		{ 
			context.PushFilterResult(tail[i]);
		}
	}

	return kFilterProcessSucceeded;

}

////////////////////////////////////////////////////////////////////////////////

class Union : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	Union(int index, Modifier modifier) 
		: ExpressionOptimizerFilterProcess(modifier)
		, m_Index (index) { }
	virtual ~Union() { }

	int m_Index;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result Union::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();
	const ExpressionRuleContext::ExpressionOpList* list;
	context.GetList(m_Index, list);

	for (int i = 0; i < tail.GetCount(); ++i) 
	{
		if (list->Find(tail[i]) != -1) 
		{
			context.PushFilterResult(tail[i]);
		}
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class ExclusiveUnion : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	ExclusiveUnion(int index, Modifier modifier) 
		: ExpressionOptimizerFilterProcess(modifier)
		, m_Index (index) { }
	virtual ~ExclusiveUnion() { }

	int m_Index;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result ExclusiveUnion::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	const ExpressionRuleContext::ExpressionOpList* list;
	context.GetList(m_Index, list);

	for (int i = 0; i < tail.GetCount(); ++i) 
	{
		if (list->Find(tail[i]) == -1) 
		{
			context.PushFilterResult(tail[i]);
		}
	}

	for (int i = 0; i < list->GetCount(); ++i) 
	{
		if (tail.Find((*list)[i]) == -1) 
		{
			context.PushFilterResult((*list)[i]);
		}
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class Match : public ExpressionOptimizerFilterProcess 
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	Match(int listIndex, int itemIndex, Modifier modifier)
		: ExpressionOptimizerFilterProcess(modifier)
		, m_ListIndex (listIndex)
		, m_ItemIndex (itemIndex) { }
	virtual ~Match() { }

	int m_ListIndex;
	int m_ItemIndex;
};

////////////////////////////////////////////////////////////////////////////////

struct FlattenIterator : public crExpressionOp::ConstIterator
{
	FlattenIterator() { }
	virtual ~FlattenIterator() { }

	virtual void Visit(const crExpressionOp& op)
	{
		m_Operators.PushAndGrow(&op);
		crExpressionOp::ConstIterator::Visit(op);
	}

	atArray<const crExpressionOp*> m_Operators;
};

ExpressionOptimizerFilterProcess::Result Match::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	const ExpressionRuleContext::ExpressionOpList* list;
	context.GetList(m_ListIndex, list);

	if (!(0 <= m_ItemIndex && m_ItemIndex < list->GetCount()))
	{
		return kIndexOutOfRange;
	}

	const Vec3V threshold = Vec3VFromF32(g_OperatorValuesMatchThreshold);

	FlattenIterator c;
	c.Visit(*((*list)[m_ItemIndex]));

	for (int k = 0; k < tail.GetCount(); ++k)
	{
		FlattenIterator t;
		t.Visit(*(tail[k]));

		if (t.m_Operators.GetCount() != c.m_Operators.GetCount())
		{
			break;
		}

		bool allMatch = true;

		for (int i = 0; i < t.m_Operators.GetCount(); ++i)
		{
			const crExpressionOp* cop = c.m_Operators[i];
			const crExpressionOp* top = t.m_Operators[i];

			if (cop->GetOpType() != top->GetOpType())
			{
				allMatch = false;
				break;
			}

			bool matches = false;

			switch (cop->GetOpType())
			{
			case crExpressionOp::kOpTypeConstant:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeConstant);
					const crExpressionOpConstant* csource = static_cast<const crExpressionOpConstant*>(cop);
					const crExpressionOpConstant* tsource = static_cast<const crExpressionOpConstant*>(top);
					if (csource->GetValue() == tsource->GetValue())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeConstantFloat:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeConstantFloat);
					const crExpressionOpConstantFloat* csource = static_cast<const crExpressionOpConstantFloat*>(cop);
					const crExpressionOpConstantFloat* tsource = static_cast<const crExpressionOpConstantFloat*>(top);
					if (IsClose(csource->GetFloat(), tsource->GetFloat()))
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeGet:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeGet);
					const crExpressionOpGet* csource = static_cast<const crExpressionOpGet*>(cop);
					const crExpressionOpGet* tsource = static_cast<const crExpressionOpGet*>(top);
					if (csource->IsForceDefault() == tsource->IsForceDefault() &&
						csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeSet:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeSet);
					const crExpressionOpSet* csource = static_cast<const crExpressionOpSet*>(cop);
					const crExpressionOpSet* tsource = static_cast<const crExpressionOpSet*>(top);
					if (csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeValid:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeValid);
					const crExpressionOpValid* csource = static_cast<const crExpressionOpValid*>(cop);
					const crExpressionOpValid* tsource = static_cast<const crExpressionOpValid*>(top);
					if (csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeComponentGet:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeComponentGet);
					const crExpressionOpComponentGet* csource = static_cast<const crExpressionOpComponentGet*>(cop);
					const crExpressionOpComponentGet* tsource = static_cast<const crExpressionOpComponentGet*>(top);
					if (csource->IsForceDefault() == tsource->IsForceDefault() &&
						csource->GetComponent() == tsource->GetComponent() &&
						csource->GetOrder() == tsource->GetOrder() &&
						csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeComponentSet:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeComponentSet);
					const crExpressionOpComponentSet* csource = static_cast<const crExpressionOpComponentSet*>(cop);
					const crExpressionOpComponentSet* tsource = static_cast<const crExpressionOpComponentSet*>(top);
					if (csource->GetComponent() == tsource->GetComponent() &&
						csource->GetOrder() == tsource->GetOrder() &&
						csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeObjectSpaceGet:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeObjectSpaceGet);
					const crExpressionOpObjectSpaceGet* csource = static_cast<const crExpressionOpObjectSpaceGet*>(cop);
					const crExpressionOpObjectSpaceGet* tsource = static_cast<const crExpressionOpObjectSpaceGet*>(top);
					if (csource->IsForceDefault() == tsource->IsForceDefault() &&
						csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeObjectSpaceSet:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeObjectSpaceSet);
					const crExpressionOpObjectSpaceSet* csource = static_cast<const crExpressionOpObjectSpaceSet*>(cop);
					const crExpressionOpObjectSpaceSet* tsource = static_cast<const crExpressionOpObjectSpaceSet*>(top);
					if (csource->IsForceDefault() == tsource->IsForceDefault() &&
						csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeObjectSpaceConvertTo:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeObjectSpaceConvertTo);
					const crExpressionOpObjectSpaceConvertTo* csource = static_cast<const crExpressionOpObjectSpaceConvertTo*>(cop);
					const crExpressionOpObjectSpaceConvertTo* tsource = static_cast<const crExpressionOpObjectSpaceConvertTo*>(top);
					if (csource->IsForceDefault() == tsource->IsForceDefault() &&
						csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeObjectSpaceConvertFrom:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeObjectSpaceConvertFrom);
					const crExpressionOpObjectSpaceConvertFrom* csource = static_cast<const crExpressionOpObjectSpaceConvertFrom*>(cop);
					const crExpressionOpObjectSpaceConvertFrom* tsource = static_cast<const crExpressionOpObjectSpaceConvertFrom*>(top);
					if (csource->IsForceDefault() == tsource->IsForceDefault() &&
						csource->GetTrack() == tsource->GetTrack() &&
						csource->GetType() == tsource->GetType() &&
						csource->GetId() == tsource->GetId() &&
						csource->IsRelative() == tsource->IsRelative())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeNullary:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeNullary);
					const crExpressionOpNullary* csource = static_cast<const crExpressionOpNullary*>(cop);
					const crExpressionOpNullary* tsource = static_cast<const crExpressionOpNullary*>(top);
					if (csource->GetNullaryOpType() == tsource->GetNullaryOpType())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeUnary:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeUnary);
					const crExpressionOpUnary* csource = static_cast<const crExpressionOpUnary*>(cop);
					const crExpressionOpUnary* tsource = static_cast<const crExpressionOpUnary*>(top);
					if (csource->GetUnaryOpType() == tsource->GetUnaryOpType())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeBinary:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeBinary);
					const crExpressionOpBinary* csource = static_cast<const crExpressionOpBinary*>(cop);
					const crExpressionOpBinary* tsource = static_cast<const crExpressionOpBinary*>(top);
					if (csource->GetBinaryOpType() == tsource->GetBinaryOpType())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeTernary:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeTernary);
					const crExpressionOpTernary* csource = static_cast<const crExpressionOpTernary*>(cop);
					const crExpressionOpTernary* tsource = static_cast<const crExpressionOpTernary*>(top);
					if (csource->GetTernaryOpType() == tsource->GetTernaryOpType())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeNary:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeNary);
					const crExpressionOpNary* csource = static_cast<const crExpressionOpNary*>(cop);
					const crExpressionOpNary* tsource = static_cast<const crExpressionOpNary*>(top);
					if (csource->GetNaryOpType() == tsource->GetNaryOpType())
					{
						matches = true;
					}
				}
				break;
			case crExpressionOp::kOpTypeSpecialBlend:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeSpecialBlend);
					const crExpressionOpSpecialBlend* csource = static_cast<const crExpressionOpSpecialBlend*>(cop);
					const crExpressionOpSpecialBlend* tsource = static_cast<const crExpressionOpSpecialBlend*>(top);
					if (csource->GetBlendMode() == tsource->GetBlendMode() &&
						csource->GetNumSourceWeights() == tsource->GetNumSourceWeights())
					{
						matches = true;

						for (int i=0; i<csource->GetNumSourceWeights(); ++i)
						{
							if (!IsClose(csource->GetWeight(i), tsource->GetWeight(i), g_OperatorValuesMatchThreshold))
							{
								matches = false;
								break;
							}
						}
					}
				}
				break;
			case crExpressionOp::kOpTypeSpecialCurve:
				{
					FastAssert(top->GetOpType() == crExpressionOp::kOpTypeSpecialCurve);
					const crExpressionOpSpecialCurve* csource = static_cast<const crExpressionOpSpecialCurve*>(cop);
					const crExpressionOpSpecialCurve* tsource = static_cast<const crExpressionOpSpecialCurve*>(top);
					if (csource->GetNumKeys() == tsource->GetNumKeys())
					{
						matches = true;

						for (int i=0; i<csource->GetNumKeys(); ++i)
						{
							float cin, cout;
							csource->GetKey(i, cin, cout);
							float tin, tout;
							tsource->GetKey(i, tin, tout);

							if (cin != tin || cout != tout)
							{
								matches = false;
								break;
							}
						}
					}
				}
				break;
			case crExpressionOp::kOpTypeSpecialLookAt:
				{
					// Nothing, will never match. Not sure what to do about the sources
				}
				break;
			case crExpressionOp::kOpTypeMotion:
				{
					// Nothing, will never match					
				}
				break;
			default:
				break;
			}

			if (!matches)
			{
				allMatch = false;
				break;
			}
		}

		if (allMatch)
		{
			context.PushFilterResult(tail[k]);
		}
	}

	if (context.HasNewItems())
	{
		context.PushFilterResult((*list)[m_ItemIndex]);
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class AttributeFilter : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const = 0;
public:
	AttributeFilter(const char* attribute, AttributeCompareOperation compareOp, Modifier modifier) 
		: ExpressionOptimizerFilterProcess (modifier)
		, m_Attribute (attribute), m_CompareOp (compareOp) { }
	virtual ~AttributeFilter() { }

	atString m_Attribute;
	AttributeCompareOperation m_CompareOp;
};

////////////////////////////////////////////////////////////////////////////////

class AttributeIdentifierFilter : public AttributeFilter
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	AttributeIdentifierFilter(const char* attribute, const char* identifier, AttributeCompareOperation compareOp, Modifier modifier) 
		: AttributeFilter (attribute, compareOp, modifier)
		, m_Identifier (identifier)
	{
	}
	virtual ~AttributeIdentifierFilter() { }

	atString m_Identifier;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result AttributeIdentifierFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	for (int i = 0; i < tail.GetCount(); ++i) 
	{
		Variant* variant = NULL;
		if (HasAttribute(*tail[i], m_Attribute.c_str(), variant)) 
		{
			// convert the attribute to it's actual type (do we have enough info to do this?
			AnimationTrackTypeVariant* v = static_cast<AnimationTrackTypeVariant*>(variant);
			if (v->IsSatisfied(m_Identifier, m_CompareOp)) 
			{
				context.PushFilterResult(tail[i]);
			}

			delete variant;
		}
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class AttributeMatchFilter : public AttributeFilter
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	AttributeMatchFilter(const char* attribute, AttributeCompareOperation compareOp, Modifier modifier) // equals doesn't make sense to this filter
		: AttributeFilter (attribute, compareOp, modifier)
	{
	}
	virtual ~AttributeMatchFilter() {}
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result AttributeMatchFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	Variant* test = NULL;
	for (int i = 0; i < tail.GetCount(); ++i) 
	{
		Variant* variant = NULL;
		if (HasAttribute(*tail[i], m_Attribute.c_str(), variant)) 
		{
			if (!test) 
			{
				test = variant;
			}

			if (test->Match(variant)) 
			{
				context.PushFilterResult(tail[i]);
			} 
			else 
			{ 
				// Something didn't match, get out....
				context.ClearFilterResults();
				delete variant;
				break;
			}

			if (test != variant) 
			{
				delete variant;
			}
		}
	}

	delete test;

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class AttributeFloatFilter : public AttributeFilter
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	AttributeFloatFilter(const char* attribute, float fp, AttributeCompareOperation compareOp, Modifier modifier)
		: AttributeFilter (attribute, compareOp, modifier)
		, m_Float (fp)
	{
	}
	virtual ~AttributeFloatFilter() { }

	float m_Float;
}; 

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result AttributeFloatFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	for (int i = 0; i < tail.GetCount(); ++i) 
	{
		Variant* variant = NULL;
		if (HasAttribute(*tail[i], m_Attribute.c_str(), variant)) 
		{
			FloatVariant* v = static_cast<FloatVariant*>(variant);
			if (v->IsSatisfied(m_Float, m_CompareOp)) 
			{
				context.PushFilterResult(tail[i]);
			}
		}

		delete variant;
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class AttributeBoolFilter : public AttributeFilter 
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	AttributeBoolFilter(const char* attribute, bool value, AttributeCompareOperation compareOp, Modifier modifier)
		: AttributeFilter(attribute, compareOp, modifier)
		, m_Value (value)
	{
	}

	virtual ~AttributeBoolFilter() {}

	bool m_Value;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result AttributeBoolFilter::Process(ExpressionRuleContext& context) const 
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	for (int i = 0; i < tail.GetCount(); ++i)
	{
		Variant* variant = NULL;
		if (HasAttribute(*tail[i], m_Attribute.c_str(), variant))
		{
			BoolVariant* v = static_cast<BoolVariant*>(variant);
			if (v->IsSatisfied(m_Value, m_CompareOp))
			{
				context.PushFilterResult(tail[i]);
			}
		}

		delete variant;
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class AttributeIntegerFilter : public AttributeFilter
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	AttributeIntegerFilter(const char* attribute, int i, AttributeCompareOperation compareOp, Modifier modifier)
		: AttributeFilter (attribute, compareOp, modifier)
		, m_Integer (i)
	{
	}
	virtual ~AttributeIntegerFilter() {}

	int m_Integer;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result AttributeIntegerFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();
	
	for (int i = 0; i < tail.GetCount(); ++i)
	{
		Variant* variant = NULL;
		if (HasAttribute(*tail[i], m_Attribute.c_str(), variant))
		{
			IntVariant* v = static_cast<IntVariant*>(variant);
			if (v->IsSatisfied(m_Integer, m_CompareOp))
			{
				context.PushFilterResult(tail[i]);
			}
		}

		delete variant;
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class AttributeVectorFilter : public AttributeFilter
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	AttributeVectorFilter(const char* attribute, AttributeCompareOperation compareOp, Modifier modifier)
		: AttributeFilter (attribute, compareOp, modifier)
		, m_Floats (0, 4)
	{
	}
	virtual ~AttributeVectorFilter() { }

	atArray<float> m_Floats;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result AttributeVectorFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& tail = context.GetLastList();

	for (int i = 0; i < tail.GetCount(); ++i) 
	{
		Variant* variant = NULL;
		if (HasAttribute(*tail[i], m_Attribute.c_str(), variant)) 
		{
			if (variant->GetType() == Variant::kVector3) 
			{
				Vector3Variant* v = static_cast<Vector3Variant*>(variant);
				if (v->IsSatisfied(m_Floats, m_CompareOp)) 
				{
					context.PushFilterResult(tail[i]);
				}
			} 
			else if (variant->GetType() == Variant::kQuaternion) 
			{
				QuaternionVariant* v = static_cast<QuaternionVariant*>(variant);
				if (v->IsSatisfied(m_Floats, m_CompareOp)) 
				{
					context.PushFilterResult(tail[i]);
				}
			} 
			else 
			{
				Assert(false); // TODO: Better warning/error
			}

		}

		delete variant;
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class ItemIndexFilter : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const; 
public:
	ItemIndexFilter(int itemIndex, Modifier modifier)
		: ExpressionOptimizerFilterProcess (modifier)
		, m_ItemIndex (itemIndex) {}
	virtual ~ItemIndexFilter() {};

	int m_ItemIndex;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result ItemIndexFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& last = context.GetLastList();
	if (!(0 <= m_ItemIndex && m_ItemIndex < last.GetCount())) 
	{
		Assert(false);
		return kIndexOutOfRange;
	}

	context.PushFilterResult(last[m_ItemIndex]);

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class ItemIndexAttributeIntFilter : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	ItemIndexAttributeIntFilter(int itemIndex, const char* attribute, int comparand, AttributeCompareOperation compareOp, Modifier modifier)
		: ExpressionOptimizerFilterProcess(modifier)
		, m_Attribute (attribute)
		, m_ItemIndex (itemIndex)
		, m_Comparand (comparand)
		, m_CompareOp (compareOp)
	{
	}

	atString m_Attribute;
	int m_ItemIndex;
	int m_Comparand;
	AttributeCompareOperation m_CompareOp;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result ItemIndexAttributeIntFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList& last = context.GetLastList();
	if (!(0 <= m_ItemIndex && m_ItemIndex < last.GetCount()))
	{
		Assert(false);
		return kIndexOutOfRange;
	}

	Variant* variant = NULL;
	if (HasAttribute(*last[m_ItemIndex], m_Attribute.c_str(), variant))
	{
		IntVariant* v = static_cast<IntVariant*>(variant);
		if (v->IsSatisfied(m_Comparand, m_CompareOp))
		{
			context.PushFilterResult(last[m_ItemIndex]);
		}
	}

	delete variant;

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class ListIndexFilter : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	ListIndexFilter(int listIndex, Modifier modifier) 
		: ExpressionOptimizerFilterProcess (modifier)
		, m_ListIndex (listIndex) { }
	virtual ~ListIndexFilter() { }

	int m_ListIndex;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result ListIndexFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList* list = NULL;
	context.GetList(m_ListIndex, list);

	for (int i = 0; i < list->GetCount(); ++i) 
	{
		context.PushFilterResult((*list)[i]);
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

class ListIndexOperationFilter : public ExpressionOptimizerFilterProcess
{
public:
	virtual Result Process(ExpressionRuleContext& context) const;
public:
	ListIndexOperationFilter(int listIndex, const char* operation, Modifier modifier) 
		: ExpressionOptimizerFilterProcess (modifier)
		, m_ListIndex (listIndex), m_Operation (operation)
	{
	}
	virtual ~ListIndexOperationFilter() { }

	int m_ListIndex;
	atString m_Operation;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerFilterProcess::Result ListIndexOperationFilter::Process(ExpressionRuleContext& context) const
{
	const ExpressionRuleContext::ExpressionOpList* list = NULL;
	context.GetList(m_ListIndex, list);

	for (int i = 0; i < list->GetCount(); ++i) 
	{
		context.PushFilterResult((*list)[i]);
	}

	return kFilterProcessSucceeded;
}

////////////////////////////////////////////////////////////////////////////////

typedef atArray<Token*> TokenizedRule;
typedef atArray<TokenizedRule*> TokenizedRuleList;

////////////////////////////////////////////////////////////////////////////////

class ExpressionOptimizerNode;

////////////////////////////////////////////////////////////////////////////////

struct ExpressionOptimizerRule
{
	ExpressionOptimizerRule() 
		: m_Filters (0, 36)
		, m_RootNode (NULL) { }
	~ExpressionOptimizerRule();

	void SetRoot(ExpressionOptimizerNode* root)
	{
		m_RootNode = root;
	}

	crExpressionOp* Create(ExpressionRuleContext& context)
	{
		return m_RootNode->Create(context);
	}

	void AppendFilter(ExpressionOptimizerFilterProcess& filter)
	{
		m_Filters.PushAndGrow(&filter);
	}

	const ExpressionOptimizerFilterProcess& GetFilter(int index) const
	{
		return *m_Filters[index];
	}

	int GetFilterCount() const
	{
		return m_Filters.GetCount();
	}
private:
	atArray<ExpressionOptimizerFilterProcess*> m_Filters;
	ExpressionOptimizerNode* m_RootNode;
};

////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerRule::~ExpressionOptimizerRule()
{
	for (int i = 0; i < m_Filters.GetCount(); ++i)
	{
		delete m_Filters[i];
	}

	delete m_RootNode;
}

////////////////////////////////////////////////////////////////////////////////

class Parser
{
public:
	void Parse(TokenizedRuleList& tokenizedRules, atArray<ExpressionOptimizerRule*>& rules);

	template <typename DataType>
	DataType* AcceptAs(Token::Symbol s)
	{
		return static_cast<DataType*>(Accept(s));
	}

	Token* Accept(Token::Symbol s)
	{
		Token* tok = *m_CurrentToken;
		if ((*m_CurrentToken)->GetSymbol() == s) 
		{
			++m_CurrentToken;
			return tok;
		}
		return NULL;
	}

	template <typename DataType>
	DataType* ExpectAs(Token::Symbol s)
	{
		return static_cast<DataType*>(Expect(s));
	}

	Token* Expect(Token::Symbol s)
	{
		if (Token* tok = Accept(s)) 
		{
			return tok;
		}
		Errorf("Expect: unexpected symbol");
		Assert(false);
		return NULL;
	}


	template <typename DataType>
	DataType* AnyAs()
	{
		return static_cast<DataType*>(Any());
	}

	Token* Any()
	{
		Token* tok = *m_CurrentToken;
		++m_CurrentToken;
		return tok;
	}
private:
	TokenizedRuleList* m_TokenizedRules;
	atArray<ExpressionOptimizerRule*>* m_Rules;

	Token** m_CurrentToken;
};

////////////////////////////////////////////////////////////////////////////////

void CreateFilter(Parser& p, ExpressionOptimizerRule& rule, ExpressionOptimizerFilterProcess::Modifier modifier = ExpressionOptimizerFilterProcess::kNone)
{
	if (p.Accept(Token::kListIndex)) 
	{
		// I'm not sure this is ever used...
		if (p.Accept(Token::kPeriod)) 
		{
			if (p.Accept(Token::kChildExpansion)) 
			{
			} 
			else if (p.Accept(Token::kLengthCheck)) 
			{
			} 
			else if (p.Accept(Token::kUnion)) 
			{
			} 
			else if (p.Accept(Token::kExclusiveUnion)) 
			{
				p.Expect(Token::kLeftParen);
				p.Expect(Token::kListIndex);
				p.Expect(Token::kInteger);
				p.Expect(Token::kRightParen);
			} 
			else 
			{
				Errorf("error");
			}
		} 
		else 
		{
			// Just the list index as a filter
			IntegerToken* itok = p.ExpectAs<IntegerToken>(Token::kInteger);

			if (p.Accept(Token::kPeriod)) 
			{
				// Then we have an operation on the list, at the moment it can only be sum but we don't check that...
				IdentifierToken* operation = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
				ExpressionOptimizerFilterProcess* filter = rage_new ListIndexOperationFilter(itok->m_Integer, operation->m_Identifier, modifier);
				rule.AppendFilter(*filter);
			} 
			else 
			{
				// Just the list index...
				ExpressionOptimizerFilterProcess* filter = rage_new ListIndexFilter(itok->m_Integer, modifier);
				rule.AppendFilter(*filter);
			}
		}

	} 
	else if (p.Accept(Token::kItemIndex)) 
	{
		IntegerToken* integer = p.ExpectAs<IntegerToken>(Token::kInteger);
		if (p.Accept(Token::kPeriod)) 
		{
			if (p.Accept(Token::kAttribute)) 
			{
				IdentifierToken* identifier = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
				if (p.Accept(Token::kEquals)) 
				{
					IntegerToken* value = p.AcceptAs<IntegerToken>(Token::kInteger);
					if (value)
					{
						ItemIndexAttributeIntFilter* filter = rage_new ItemIndexAttributeIntFilter(
							integer->m_Integer, identifier->m_Identifier, value->m_Integer, kAttributeCompareOpEqual, modifier);
						rule.AppendFilter(*filter);
					}
				}
			} 
			else 
			{
				// Placeholder else...
			}
		} else {
			// Then this is just and indexer token (eg. Take an element of the last list and create a new list with it)
			ItemIndexFilter* filter = rage_new ItemIndexFilter(integer->m_Integer, modifier);
			rule.AppendFilter(*filter);

		}
	}
	else if (p.Accept(Token::kAttribute)) 
	{
		IdentifierToken* attrib = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
		AttributeCompareOperation compare = kAttributeCompareOpNone;
		if (p.Accept(Token::kEquals)) 
		{
			compare = kAttributeCompareOpEqual;
		} 
		else if (p.Accept(Token::kNotEquals))
		{
			compare = kAttributeCompareOpNotEqual;
		}
		else if (p.Accept(Token::kGreaterThanEqualTo))
		{
			compare = kAttributeCompareOpGreaterThanEqual;
		}
		else if (p.Accept(Token::kGreaterThan))
		{
			compare = kAttributeCompareOpGreaterThan;
		}
		else if (p.Accept(Token::kLessThanEqualTo))
		{
			compare = kAttributeCompareOpLessThanEqual;
		}
		else 
		{
			p.Expect(Token::kLessThan);
			compare = kAttributeCompareOpLessThan;
		}

		// Check if it's a float or a constant string
		if (IdentifierToken* tok = p.AcceptAs<IdentifierToken>(Token::kIdentifier)) 
		{
			// Might have a numeric constant like PI rather than an identifier type. If that's the case it becomes a floating point token
			if (strcmpi(tok->m_Identifier, "PI") == 0) 
			{
				ExpressionOptimizerFilterProcess* filter = rage_new AttributeFloatFilter (attrib->m_Identifier, PI, compare, modifier);
				rule.AppendFilter(*filter);
			} 
			else 
			{
				ExpressionOptimizerFilterProcess* filter = rage_new AttributeIdentifierFilter (attrib->m_Identifier, tok->m_Identifier, compare, modifier);
				rule.AppendFilter(*filter);
			}
		} 
		else if (p.Accept(Token::kLeftBrace)) 
		{
			// Probably some kind of array attribute...
			AttributeVectorFilter* filter = rage_new AttributeVectorFilter (attrib->m_Identifier, compare, modifier);
			rule.AppendFilter(*filter);
			do {
				if (p.Accept(Token::kSubtract)) 
				{
					FloatingPointToken* tok = p.ExpectAs<FloatingPointToken>(Token::kFloatingPoint);
					filter->m_Floats.PushAndGrow(-tok->m_Float);
				} 
				else 
				{
					FloatingPointToken* tok = p.ExpectAs<FloatingPointToken>(Token::kFloatingPoint);
					filter->m_Floats.PushAndGrow(tok->m_Float);
				}
			} while (p.Accept(Token::kComma));

			p.Expect(Token::kRightBrace);
		} 
		else if (p.Accept(Token::kSubtract)) 
		{
			// For negative numbers
			FloatingPointToken* tok = p.ExpectAs<FloatingPointToken>(Token::kFloatingPoint);
			ExpressionOptimizerFilterProcess* filter = rage_new AttributeFloatFilter (attrib->m_Identifier, -tok->m_Float, compare, modifier);
			rule.AppendFilter(*filter);
		} 
		else if (FloatingPointToken* tok = p.AcceptAs<FloatingPointToken>(Token::kFloatingPoint)) 
		{
			ExpressionOptimizerFilterProcess* filter = rage_new AttributeFloatFilter (attrib->m_Identifier, tok->m_Float, compare, modifier);
			rule.AppendFilter(*filter);
			// Just the value
		} 
		else if (IntegerToken* tok = p.AcceptAs<IntegerToken>(Token::kInteger))
		{
			ExpressionOptimizerFilterProcess* filter = rage_new AttributeIntegerFilter (attrib->m_Identifier, tok->m_Integer, compare, modifier);
			rule.AppendFilter(*filter);
		}
		else 
		{
			// =' ' is valid and just means that everything should be equal
			ExpressionOptimizerFilterProcess* filter = rage_new AttributeMatchFilter (attrib->m_Identifier, compare, modifier);
			rule.AppendFilter(*filter);
		}
	} 
	else if (IdentifierToken* tok = p.AcceptAs<IdentifierToken>(Token::kIdentifier)) 
	{
		ExpressionOptimizerFilterProcess* filter = rage_new ExpressionTypeFilter(tok->m_Identifier, modifier);
		rule.AppendFilter(*filter);
	} 
	else 
	{
		if (p.Accept(Token::kChildExpansion)) 
		{
			ExpressionOptimizerFilterProcess* filter = rage_new ChildExpander(modifier);
			rule.AppendFilter(*filter);
		} 
		else if (p.Accept(Token::kLengthCheck)) 
		{
			p.Expect(Token::kEquals);
			IntegerToken* tok = p.ExpectAs<IntegerToken>(Token::kInteger); // How long do you expect the list to be?
			ExpressionOptimizerFilterProcess* filter = rage_new LengthCheckFilter(tok->m_Integer, modifier);
			rule.AppendFilter(*filter);
		} 
		else if (p.Accept(Token::kUnion)) 
		{
			p.Expect(Token::kLeftParen);
			p.Expect(Token::kListIndex);
			IntegerToken* tok = p.ExpectAs<IntegerToken>(Token::kInteger);
			p.Expect(Token::kRightParen);
			ExpressionOptimizerFilterProcess* filter = rage_new Union(tok->m_Integer, modifier);
			rule.AppendFilter(*filter);
		} 
		else if (p.Accept(Token::kExclusiveUnion)) 
		{
			p.Expect(Token::kLeftParen);
			p.Expect(Token::kListIndex);
			IntegerToken* tok = p.ExpectAs<IntegerToken>(Token::kInteger);
			p.Expect(Token::kRightParen);
			ExpressionOptimizerFilterProcess* filter = rage_new ExclusiveUnion(tok->m_Integer, modifier);
			rule.AppendFilter(*filter);
		} 
		else if (p.Accept(Token::kMatch))
		{
			p.Expect(Token::kLeftParen);
			p.Expect(Token::kListIndex);
			IntegerToken* listIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
			p.Expect(Token::kLeftParen);
			IntegerToken* itemIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
			p.Expect(Token::kRightParen);
			p.Expect(Token::kRightParen);
			ExpressionOptimizerFilterProcess* filter = rage_new Match(listIndex->m_Integer, itemIndex->m_Integer, modifier);
			rule.AppendFilter(*filter);
		}
		else 
		{
			Errorf("error");
		}
	}

	p.Expect(Token::kRightBracket);
}

////////////////////////////////////////////////////////////////////////////////

void CreateFilters(Parser& p, ExpressionOptimizerRule& rule)
{
resume:
	while (p.Accept(Token::kLeftBracket)) 
	{
		CreateFilter(p, rule);
	}

	if (p.Accept(Token::kTilde)) 
	{
		// Store modifier and continue
		p.Expect(Token::kLeftBracket);
		CreateFilter(p, rule, ExpressionOptimizerFilterProcess::kContinueOnEmpty);
		goto resume;
	}
}

////////////////////////////////////////////////////////////////////////////////

int OperatorPrecedence(Token* t)
{
	switch (t->GetSymbol()) 
	{
		case Token::kMultiply:
		case Token::kDivide:
			return 3;
		case Token::kPlus:
		case Token::kSubtract:
			return 2;
		default:
			return 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

struct ArithmeticOperation 
{
	enum Op {
		kAddOp,
		kSubtractOp,
		kMultiplyOp,
		kDivideOp,
		kConstantOp,
		kItemIndexOp,
		kLeftParenOp,
		kRightParenOp,
		kItemFromListOp,
		kNegateOp,
	};

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation** stack, unsigned int& top, ExpressionRuleContext& context) = 0;

	ArithmeticOperation(Op type) : m_Type (type) {}
	virtual ~ArithmeticOperation() {}

	virtual ArithmeticOperation* Clone() const = 0;

	Op m_Type;
};

////////////////////////////////////////////////////////////////////////////////

InitializeAttributeFromArithmeticOperation::~InitializeAttributeFromArithmeticOperation()
{
	for (int i = 0; i < m_Ops.GetCount(); ++i)
	{
		delete m_Ops[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

int OperatorPrecedence(ArithmeticOperation* op)
{
	switch (op->m_Type) {
		case ArithmeticOperation::kNegateOp:
			return 4;
		case ArithmeticOperation::kMultiplyOp:
		case ArithmeticOperation::kDivideOp:
			return 3;
		case ArithmeticOperation::kAddOp:
		case ArithmeticOperation::kSubtractOp:
			return 2;
		default:
			return 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool IsOperatorLeftAssociative(Token* t)
{
	switch (t->GetSymbol()) 
	{
		case Token::kMultiply:
		case Token::kDivide:
		case Token::kPlus:
		case Token::kSubtract:
			return true;
		case Token::kNegation:
			return false;
		default:
			return false;
	}
}

////////////////////////////////////////////////////////////////////////////////

unsigned int OperatorArgumentCount(ArithmeticOperation* op)
{
	switch (op->m_Type) 
	{
		case ArithmeticOperation::kAddOp:
		case ArithmeticOperation::kSubtractOp:
		case ArithmeticOperation::kMultiplyOp:
		case ArithmeticOperation::kDivideOp:
			return 2;
		case ArithmeticOperation::kNegateOp:
			return 1;
		default:
			return 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

#define IS_OPERATOR(c)  (c == '+' || c == '-' || c == '/' || c == '*' || c == '!' || c == '%' || c == '=')
#define IS_FUNCTION(c)  (c >= 'A' && c <= 'Z')
#define IS_IDENT(c)     ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z'))

////////////////////////////////////////////////////////////////////////////////

static bool IsOperator(Token* t)
{
	Token::Symbol s = t->GetSymbol();
	return (s == Token::kPlus) || 
		(s == Token::kSubtract) || 
		(s == Token::kDivide) || 
		(s == Token::kMultiply) ||
		(s == Token::kNegation);
}

////////////////////////////////////////////////////////////////////////////////

static bool IsOperator(ArithmeticOperation* op)
{
	return (op->m_Type == ArithmeticOperation::kAddOp) || 
		(op->m_Type == ArithmeticOperation::kSubtractOp) || 
		(op->m_Type == ArithmeticOperation::kMultiplyOp) || 
		(op->m_Type == ArithmeticOperation::kDivideOp) ||
		(op->m_Type == ArithmeticOperation::kNegateOp);
}

////////////////////////////////////////////////////////////////////////////////

static bool IsFunction(ArithmeticOperation* op)
{
	return (op->m_Type == ArithmeticOperation::kItemIndexOp) || (op->m_Type == ArithmeticOperation::kItemFromListOp);
}

////////////////////////////////////////////////////////////////////////////////

struct ConstantOperation : public ArithmeticOperation 
{
	ConstantOperation(float constant) 
		: ArithmeticOperation (kConstantOp)
		, m_Constant (constant) {}
	virtual ~ConstantOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation**, unsigned int&, ExpressionRuleContext&)
	{
		Assert(false);
		return NULL;
	}

	virtual ArithmeticOperation* Clone() const
	{
		ConstantOperation* clone = rage_new ConstantOperation(m_Constant);
		return clone;
	}

	float m_Constant;
};

////////////////////////////////////////////////////////////////////////////////

struct NegateOperation : public ArithmeticOperation
{
	NegateOperation()
		: ArithmeticOperation(kNegateOp) {}
	virtual ~NegateOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation** stack, unsigned int& top, ExpressionRuleContext&)
	{
		ArithmeticOperation* op = stack[top - 1];
		Assert(op->m_Type == kConstantOp);
		ConstantOperation* c = static_cast<ConstantOperation*>(op);
		top--;

		ConstantOperation* ret = rage_new ConstantOperation(c->m_Constant * -1.0F);

		delete c;

		return ret;
	}

	virtual ArithmeticOperation* Clone() const
	{
		NegateOperation* clone = rage_new NegateOperation();
		return clone;
	}
};

////////////////////////////////////////////////////////////////////////////////

struct AddOperation : public ArithmeticOperation 
{
	AddOperation() 
		: ArithmeticOperation (kAddOp) {}
	virtual ~AddOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation** stack, unsigned int& top, ExpressionRuleContext&)
	{
		ArithmeticOperation* op0 = stack[top - 1];
		Assert(op0->m_Type == kConstantOp);
		ConstantOperation* c0 = static_cast<ConstantOperation*>(op0);
		top--;
		ArithmeticOperation* op1 = stack[top - 1];
		Assert(op1->m_Type == kConstantOp);
		ConstantOperation* c1 = static_cast<ConstantOperation*>(op1);
		top--;

		ConstantOperation* ret = rage_new ConstantOperation(c1->m_Constant + c0->m_Constant);

		delete c0;
		delete c1;

		return ret;
	}

	virtual ArithmeticOperation* Clone() const
	{
		AddOperation* clone = rage_new AddOperation();
		return clone;
	}
};

////////////////////////////////////////////////////////////////////////////////

struct SubtractOperation : public ArithmeticOperation 
{
	SubtractOperation() 
		:  ArithmeticOperation (kSubtractOp) {}
	virtual ~SubtractOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation** stack, unsigned int& top, ExpressionRuleContext&)
	{
		ArithmeticOperation* op0 = stack[top-1];
		Assert(op0->m_Type == kConstantOp);
		ConstantOperation* c0 = static_cast<ConstantOperation*>(op0);
		top--;
		ArithmeticOperation* op1 = stack[top-1];
		Assert(op1->m_Type == kConstantOp);
		ConstantOperation* c1 = static_cast<ConstantOperation*>(op1);
		top--;

		ConstantOperation* ret = rage_new ConstantOperation(c1->m_Constant - c0->m_Constant);

		delete c0;
		delete c1;

		return ret;
	}

	virtual ArithmeticOperation* Clone() const
	{
		SubtractOperation* clone = rage_new SubtractOperation;
		return clone;
	}
};

////////////////////////////////////////////////////////////////////////////////

struct MultiplyOperation : public ArithmeticOperation 
{
	MultiplyOperation() 
		: ArithmeticOperation (kMultiplyOp) {}
	virtual ~MultiplyOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation** stack, unsigned int& top, ExpressionRuleContext&)
	{
		ArithmeticOperation* op0 = stack[top-1];
		Assert(op0->m_Type == kConstantOp);
		ConstantOperation* c0 = static_cast<ConstantOperation*>(op0);
		top--;
		ArithmeticOperation* op1 = stack[top-1];
		Assert(op1->m_Type == kConstantOp);
		ConstantOperation* c1 = static_cast<ConstantOperation*>(op1);
		top--;

		ConstantOperation* ret = rage_new ConstantOperation(c1->m_Constant * c0->m_Constant);

		delete c0;
		delete c1;

		return ret;
	}

	virtual ArithmeticOperation* Clone() const
	{
		MultiplyOperation* clone = rage_new MultiplyOperation;
		return clone;
	}
};

////////////////////////////////////////////////////////////////////////////////

struct DivideOperation : public ArithmeticOperation 
{
	DivideOperation() 
		: ArithmeticOperation (kDivideOp) {}
	virtual ~DivideOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation** stack, unsigned int& top, ExpressionRuleContext&)
	{
		ArithmeticOperation* op0 = stack[top-1];
		Assert(op0->m_Type == kConstantOp);
		ConstantOperation* c0 = static_cast<ConstantOperation*>(op0);
		top--;
		ArithmeticOperation* op1 = stack[top-1];
		Assert(op1->m_Type == kConstantOp);
		ConstantOperation* c1 = static_cast<ConstantOperation*>(op1);
		top--;

		ConstantOperation* ret = rage_new ConstantOperation(c1->m_Constant / c0->m_Constant);

		delete c0;
		delete c1;

		return ret;
	}

	virtual ArithmeticOperation* Clone() const
	{
		DivideOperation* clone = rage_new DivideOperation;
		return clone;
	}
};

////////////////////////////////////////////////////////////////////////////////

struct ItemIndexOperation : public ArithmeticOperation 
{
	ItemIndexOperation(int itemIndex, const atString& attribute) 
		: ArithmeticOperation (kItemIndexOp)
		, m_ItemIndex (itemIndex)
		, m_Attribute (attribute) {}
	virtual ~ItemIndexOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation**, unsigned int&, ExpressionRuleContext& context)
	{
		const ExpressionRuleContext::ExpressionOpList& list = context.GetLastList();
		if (!(0 <= m_ItemIndex && m_ItemIndex < list.GetCount())) 
		{
			Assert(false); // TODO: Better warning/error
			return NULL;
		}

		Variant* variant = NULL;
		if (HasAttribute(*(list[m_ItemIndex]), m_Attribute.c_str(), variant)) 
		{
			// Assumes that it's a float variant. Should really check first
			ConstantOperation* op = rage_new ConstantOperation(static_cast<FloatVariant*>(variant)->GetFloat());
			delete variant;

			return op;
		}

		return NULL;
	}

	virtual ArithmeticOperation* Clone() const
	{
		ItemIndexOperation* clone = rage_new ItemIndexOperation(m_ItemIndex, m_Attribute);
		return clone;
	}
	
	int m_ItemIndex;
	atString m_Attribute;
};

////////////////////////////////////////////////////////////////////////////////

struct ItemFromListOperation : public ArithmeticOperation
{
	ItemFromListOperation(int listIndex, int itemIndex, const atString& attribute)
		: ArithmeticOperation(kItemFromListOp)
		, m_ListIndex(listIndex)
		, m_ItemIndex(itemIndex)
		, m_Attribute(attribute)
	{}
	~ItemFromListOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation**, unsigned int&, ExpressionRuleContext& context)
	{
		const ExpressionRuleContext::ExpressionOpList* list = NULL;
		context.GetList(m_ListIndex, list);

		if (!(0 <= m_ItemIndex && m_ItemIndex < list->GetCount()))
		{
			Assert(false);
			return NULL;
		}

		Variant* variant = NULL;
		if (HasAttribute(*((*list)[m_ItemIndex]), m_Attribute.c_str(), variant))
		{
			// Assumes that it's a float variant. Should really check first
			ConstantOperation* op = rage_new ConstantOperation(static_cast<FloatVariant*>(variant)->GetFloat());
			delete variant;

			return op;
		}

		return NULL;
	}

	virtual ArithmeticOperation* Clone() const
	{
		ItemFromListOperation* clone = rage_new ItemFromListOperation( m_ListIndex, m_ItemIndex, m_Attribute);
		return clone;
	}

	int m_ListIndex;
	int m_ItemIndex;
	atString m_Attribute;
};

////////////////////////////////////////////////////////////////////////////////

struct LeftParenOperation : public ArithmeticOperation 
{
	LeftParenOperation() : ArithmeticOperation (kLeftParenOp) {}
	virtual ~LeftParenOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation**, unsigned int&, ExpressionRuleContext&)
	{
		Assert(false);
		return NULL;
	}

	virtual ArithmeticOperation* Clone() const
	{
		LeftParenOperation* clone = rage_new LeftParenOperation;
		return clone;
	}
};

////////////////////////////////////////////////////////////////////////////////

struct RightParenOperation : public ArithmeticOperation 
{
	RightParenOperation() 
		: ArithmeticOperation (kRightParenOp) {}
	virtual ~RightParenOperation() {}

	virtual ArithmeticOperation* Evaluate(ArithmeticOperation**, unsigned int&, ExpressionRuleContext&)
	{
		Assert(false);
		return NULL;
	}

	virtual ArithmeticOperation* Clone() const
	{
		RightParenOperation* clone = rage_new RightParenOperation;
		return clone;
	}
};

////////////////////////////////////////////////////////////////////////////////

static ArithmeticOperation* CreateOperatorFromToken(Token* t)
{
	switch (t->GetSymbol()) 
	{
		case Token::kPlus:
			return rage_new AddOperation();
		case Token::kSubtract:
			return rage_new SubtractOperation();
		case Token::kMultiply:
			return rage_new MultiplyOperation();
		case Token::kDivide:
			return rage_new DivideOperation();
		case Token::kNegation:
			return rage_new NegateOperation();
		default:
			break;
	}

	Assert(false);
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool ShuntingYard(Parser& p, atArray<ArithmeticOperation*>& output)
{
	ArithmeticOperation* stack[32], *sc;
	unsigned int sl = 0;
	Token* c = p.Any();
	while (c->GetSymbol() != Token::kRightBracket) 
	{
		if (c->GetSymbol() == Token::kFloatingPoint) 
		{
			output.PushAndGrow(rage_new ConstantOperation(static_cast<FloatingPointToken*>(c)->m_Float));
		} 
		else if (c->GetSymbol() == Token::kListIndex) 
		{
			IntegerToken* listIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
			p.Expect(Token::kLeftParen);
			IntegerToken* itemIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
			p.Expect(Token::kRightParen);
			p.Expect(Token::kPeriod);
			p.Expect(Token::kAttribute);

			c = p.Expect(Token::kIdentifier);
			IdentifierToken* id = static_cast<IdentifierToken*>(c);
			output.PushAndGrow(rage_new ItemFromListOperation(listIndex->m_Integer, itemIndex->m_Integer, id->m_Identifier));

		} 
		else if (c->GetSymbol() == Token::kItemIndex) 
		{

			IntegerToken* integerTok = p.ExpectAs<IntegerToken>(Token::kInteger);
			p.Expect(Token::kPeriod);
			p.Expect(Token::kAttribute);
			c = p.Expect(Token::kIdentifier);
			IdentifierToken* idToken = static_cast<IdentifierToken*>(c);
//			stack[sl] = rage_new ItemIndexOperation(integerTok->m_Integer, idToken->m_Identifier);
//			++sl;
			output.PushAndGrow(rage_new ItemIndexOperation(integerTok->m_Integer, idToken->m_Identifier));
		} 
		else if (IsOperator(c)) 
		{
			while(sl > 0)    
			{
				sc = stack[sl - 1];
				if(IsOperator(sc) &&
					((IsOperatorLeftAssociative(c) && (OperatorPrecedence(c) <= OperatorPrecedence(sc))) ||
					(!IsOperatorLeftAssociative(c) && (OperatorPrecedence(c) < OperatorPrecedence(sc)))))   
				{
					output.PushAndGrow(sc);
					sl--;
				} 
				else   
				{
					break;
				}
			}
			stack[sl] = CreateOperatorFromToken(c);
			++sl;
		}
		// If the token is a left parenthesis, then push it onto the stack.
		else if (c->GetSymbol() == Token::kLeftParen) 
		{
			stack[sl] = rage_new LeftParenOperation();
			++sl;
		}
		// If the token is a right parenthesis:
		else if (c->GetSymbol() == Token::kRightParen) 
		{
			bool pe = false;
			// Until the token at the top of the stack is a left parenthesis,
			// pop operators off the stack onto the output queue
			while(sl > 0)     
			{
				sc = stack[sl - 1];
				if (sc->m_Type == ArithmeticOperation::kLeftParenOp) 
				{
					pe = true;
					delete sc;
					break;
				} 
				else  
				{
					output.PushAndGrow(sc);
					sl--;
				}
			}
			// If the stack runs out without finding a left parenthesis, then there are mismatched parentheses.
			if(!pe)  
			{
				Displayf("Error: parentheses mismatched\n");
				return false;
			}
			// Pop the left parenthesis from the stack, but not onto the output queue.
			sl--;
			// If the token at the top of the stack is a function token, pop it onto the output queue.
			if(sl > 0)   
			{
				sc = stack[sl - 1];
				if(IsFunction(sc))   
				{
					output.PushAndGrow(sc);
					sl--;
				}
			}
		} 
		else  
		{
			Displayf("Unknown token %c\n", c);
			return false; // Unknown token
		}
		c = p.Any();
	}
	// When there are no more tokens to read:
	// While there are still operator tokens in the stack:
	while(sl > 0)  
	{
		sc = stack[sl - 1];
		if (sc->m_Type == ArithmeticOperation::kLeftParenOp || sc->m_Type == ArithmeticOperation::kRightParenOp) 
		{
			Displayf("Error: parentheses mismatched\n");
			return false;
		}
		output.PushAndGrow(sc);
		--sl;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

Variant* ExecuteArithmetic(atArray<ArithmeticOperation*>& input, ExpressionRuleContext& context) 
{
	ArithmeticOperation* c;
	ArithmeticOperation* res;
	unsigned int sl = 0, rn = 0;
	ArithmeticOperation* stack[32];
	ArithmeticOperation* sc;

	for (int i = 0; i < input.GetCount(); ++i) 
	{
		// Read the next token from input.
		c = input[i]->Clone();
		
		if (c->m_Type == ArithmeticOperation::kConstantOp) 
		{
			stack[sl] = c;
			++sl;
		}
		// Otherwise, the token is an operator  (operator here includes both operators, and functions).
		else if(IsOperator(c) || IsFunction(c))    
		{
			++rn;
			// It is known a priori that the operator takes n arguments.
			unsigned int nargs = OperatorArgumentCount(c);
			// If there are fewer than n values on the stack
			if(sl < nargs) 
			{
				// (Error) The user has not input sufficient values in the expression.
				delete c;
				return false;
			}
			// Else, Pop the top n values from the stack.
			// Evaluate the operator, with the values as arguments.
			if(IsFunction(c)) 
			{
				res = c->Evaluate(stack, sl, context); // Will need to take the operation context
			} 
			else 
			{
				res = c->Evaluate(stack, sl, context); // Will do what it needs to the stack
			}
			// Push the returned results, if any, back onto the stack.
			delete c;
			stack[sl] = res;
			++sl;
		}
	}
	// If there is only one value in the stack
	// That value is the result of the calculation.
	if(sl == 1) 
	{
		sc = stack[sl - 1];
		sl--;
		Variant* variant = rage_new FloatVariant(static_cast<ConstantOperation*>(sc)->m_Constant);
		delete sc;
		return variant;
	}
	// If there are more values in the stack
	// (Error) The user input has too many values.
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void CreateChildTree(ExpressionOptimizerNode* parent, Parser& p);


////////////////////////////////////////////////////////////////////////////////

ExpressionOptimizerNode* CreateTree(Parser& p)
{
	ExpressionOptimizerNode* root = NULL;
	p.Expect(Token::kLeftBracket);
	if (IdentifierToken* itok = p.AcceptAs<IdentifierToken>(Token::kIdentifier)) 
	{
		root = rage_new NodeByIdentifier(itok->m_Identifier);
		if (p.Accept(Token::kLeftBrace)) 
		{
			// then we can have one or more assignments
			// each node can have multiple 'initializers'
			if (p.Accept(Token::kItemIndex)) 
			{
				// then it looks like we have a vector or quat assignment
				if (strcmpi(itok->m_Identifier, "OpTypeConstantVector") == 0) 
				{
					// then expect 3 initializers
					atRangeArray<int, 3> indexes;
					atRangeArray<atString, 3> identifiers;

					int idx = 0;
					IntegerToken* listItem = p.ExpectAs<IntegerToken>(Token::kInteger);
					p.Expect(Token::kPeriod);
					p.Expect(Token::kAttribute);
					IdentifierToken* identifier = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
					indexes[idx] = listItem->m_Integer;
					identifiers[idx] = identifier->m_Identifier;
					++idx;
					while (p.Accept(Token::kComma)) 
					{
						p.Expect(Token::kItemIndex);
						IntegerToken* listItem = p.ExpectAs<IntegerToken>(Token::kInteger);
						p.Expect(Token::kPeriod);
						p.Expect(Token::kAttribute);
						IdentifierToken* identifier = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
						indexes[idx] = listItem->m_Integer;
						identifiers[idx] = identifier->m_Identifier;
						++idx;
					}

					Assert(idx == 3);

					ExpressionOpInitializer* init = rage_new InitializeVectorFromIndexedAttributes(indexes[0], identifiers[0], indexes[1], identifiers[1], indexes[2], identifiers[2]);
					root->AddInitializer(init);
				} 
				else if (strcmpi(itok->m_Identifier, "OpTypeConstantQuat") == 0) 
				{
					// then expect 3 initializers
					atRangeArray<int, 3> indexes;
					atRangeArray<atString, 3> identifiers;

					int idx = 0;
					IntegerToken* listItem = p.ExpectAs<IntegerToken>(Token::kInteger);
					p.Expect(Token::kPeriod);
					p.Expect(Token::kAttribute);
					IdentifierToken* identifier = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
					indexes[idx] = listItem->m_Integer;
					identifiers[idx] = identifier->m_Identifier;
					++idx;
					while (p.Accept(Token::kComma)) {
						p.Expect(Token::kItemIndex);
						IntegerToken* listItem = p.ExpectAs<IntegerToken>(Token::kInteger);
						p.Expect(Token::kPeriod);
						p.Expect(Token::kAttribute);
						IdentifierToken* identifier = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
						indexes[idx] = listItem->m_Integer;
						identifiers[idx] = identifier->m_Identifier;
						++idx;
					}

					Assert(idx == 3);

					ExpressionOpInitializer* init = rage_new InitializeQuaternionFromIndexedAttributes(indexes[0], identifiers[0], indexes[1], identifiers[1], indexes[2], identifiers[2]);
					root->AddInitializer(init);
				} 
				else 
				{
					Assert(false);
				}
			} 
			else 
			{
				do {
					IdentifierToken* identifier = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
					p.Expect(Token::kEquals);

					if (p.Accept(Token::kLeftBracket))
					{
						atArray<ArithmeticOperation*> ops;
						ShuntingYard(p, ops);
						ExpressionOpInitializer* init = rage_new InitializeAttributeFromArithmeticOperation(identifier->m_Identifier, ops);
						root->AddInitializer(init);
					}
					else if (p.Accept(Token::kListIndex))
					{
						IntegerToken* listIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
						p.Expect(Token::kLeftParen);
						IntegerToken* itemIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
						p.Expect(Token::kRightParen);
						p.Expect(Token::kPeriod);
						p.Expect(Token::kAttribute);
						IdentifierToken* attribute = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
						ExpressionOpInitializer* init = rage_new InitializeAttributeFromIndexedListItemAttribute(listIndex->m_Integer, itemIndex->m_Integer, attribute->m_Identifier, identifier->m_Identifier, false);
						root->AddInitializer(init);
					}
					else if (FloatingPointToken* fp = p.AcceptAs<FloatingPointToken>(Token::kFloatingPoint))
					{
						ExpressionOpInitializer* init = rage_new InitializeFromConstantFloat(identifier->m_Identifier, fp->m_Float);
						root->AddInitializer(init);
					}
					else if (IntegerToken* intToken = p.AcceptAs<IntegerToken>(Token::kInteger))
					{
						ExpressionOpInitializer* init = rage_new InitializeFromConstantInteger(identifier->m_Identifier, intToken->m_Integer);
						root->AddInitializer(init);
					}
					else 
					{
						p.Expect(Token::kItemIndex);
						IntegerToken* itemIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
						p.Expect(Token::kPeriod);
						p.Expect(Token::kAttribute);
						IdentifierToken* itemId = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
						ExpressionOpInitializer* init = rage_new InitializeFromItemAttribute(identifier->m_Identifier, itemIndex->m_Integer, itemId->m_Identifier);
						root->AddInitializer(init);
					}
				} while (p.Accept(Token::kComma)); // then we have more than one assignment
			}
			p.Expect(Token::kRightBrace);
		} 
		else if (p.Accept(Token::kChildren)) 
		{
			if (p.Accept(Token::kLeftParen)) 
			{
				// then we must have a list
				if (p.Accept(Token::kItemIndex)) 
				{
					// it's a list, so there's no index into the item
					root->AddChild(rage_new NodeLastList);
				} 
				else if (p.Accept(Token::kListIndex)) 
				{
					IntegerToken* listIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
					root->AddChild(rage_new NodeListIndex(listIndex->m_Integer));
					// it's a list, so there's no index into the item
				}

				p.Expect(Token::kRightParen);
			} 
			else 
			{
				p.Expect(Token::kLeftBracket);
				do {
					CreateChildTree(root, p);
				} while (p.Accept(Token::kComma));
				// Can then have one or more substitutions
				p.Expect(Token::kRightBracket);
			}
		}
	} 
	else if (p.Accept(Token::kItemIndex)) 
	{
		IntegerToken* tok = p.ExpectAs<IntegerToken>(Token::kInteger);
		root = rage_new NodeFromItemIndex(tok->m_Integer); 
	} 
	else if (p.Accept(Token::kListIndex)) 
	{
		IntegerToken* list = p.ExpectAs<IntegerToken>(Token::kInteger);
		p.Expect(Token::kPeriod);
		p.Expect(Token::kLeftParen);
		IntegerToken* item = p.ExpectAs<IntegerToken>(Token::kInteger);
		p.Expect(Token::kRightParen);
		root = rage_new NodeFromListIndex(list->m_Integer, item->m_Integer);

		if (p.Accept(Token::kChildren))
		{
			p.Expect(Token::kLeftBracket);
			do {
				CreateChildTree(root, p);
			} while (p.Accept(Token::kComma));
			p.Expect(Token::kRightBracket);
		}
	}

	p.Expect(Token::kRightBracket);

	return root;
}

////////////////////////////////////////////////////////////////////////////////

void CreateChildTree(ExpressionOptimizerNode* parent, Parser& p)
{
	if (IdentifierToken* itok = p.AcceptAs<IdentifierToken>(Token::kIdentifier)) 
	{
		ExpressionOptimizerNode* node = rage_new NodeByIdentifier(itok->m_Identifier);
		parent->AddChild(node);
		if (p.Accept(Token::kLeftBrace)) 
		{
			// then we can have one or more assignments
			do {
				IdentifierToken* attrib = p.ExpectAs<IdentifierToken>(Token::kIdentifier);
				p.Expect(Token::kEquals);

				// parse an expression
				if (p.Accept(Token::kLeftBracket)) { // Was kLeftParen but you could have parens in the expression
					atArray<ArithmeticOperation*> ops;
					ShuntingYard(p, ops);
					ExpressionOpInitializer* init = rage_new InitializeAttributeFromArithmeticOperation(attrib->m_Identifier, ops);
					node->AddInitializer(init);
				} 
				else 
				{
					bool negate = false;
					if (p.Accept(Token::kSubtract))
					{ 
						negate = true;
					}

					if (p.Accept(Token::kListIndex))
					{
						IntegerToken* listIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
						p.Expect(Token::kLeftParen);
						IntegerToken* itemIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
						p.Expect(Token::kRightParen);
						p.Expect(Token::kPeriod);
						p.Expect(Token::kAttribute);
						IdentifierToken* id = p.ExpectAs<IdentifierToken>(Token::kIdentifier);

						ExpressionOpInitializer* init = rage_new InitializeAttributeFromIndexedListItemAttribute(listIndex->m_Integer, itemIndex->m_Integer, id->m_Identifier, attrib->m_Identifier, negate);
						node->AddInitializer(init);
					}
					else
					{
						// This is an assignment that clearly isn't doing anything?
						p.Expect(Token::kItemIndex);
						IntegerToken* itemIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
						p.Expect(Token::kPeriod);
						p.Expect(Token::kAttribute);
						IdentifierToken* id = p.ExpectAs<IdentifierToken>(Token::kIdentifier);

						ExpressionOpInitializer* init = rage_new InitializeFromItemAttribute(attrib->m_Identifier, itemIndex->m_Integer, id->m_Identifier);
						node->AddInitializer(init);
					}
				}
			} while (p.Accept(Token::kComma));

			p.Expect(Token::kRightBrace);
		} 
		else if (p.Accept(Token::kChildren)) 
		{
			p.Expect(Token::kLeftBracket);
			do {
				CreateChildTree(node, p); // recursively get children
			} while (p.Accept(Token::kComma));
			p.Expect(Token::kRightBracket);
		}
	} 
	else if (p.Accept(Token::kListIndex)) 
	{
		IntegerToken* listIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
		p.Expect(Token::kPeriod);
		p.Expect(Token::kLeftParen);
		IntegerToken* itemIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
		p.Expect(Token::kRightParen);
		ExpressionOptimizerNode* node = rage_new NodeFromListIndex(listIndex->m_Integer, itemIndex->m_Integer);
		parent->AddChild(node);

		if (p.Accept(Token::kChildren))
		{
			p.Expect(Token::kLeftBracket);
			do {
				CreateChildTree(node, p);
			} while (p.Accept(Token::kComma));
			p.Expect(Token::kRightBracket);
		}
	} 
	else if (p.Accept(Token::kItemIndex)) 
	{
		IntegerToken* itemIndex = p.ExpectAs<IntegerToken>(Token::kInteger);
		ExpressionOptimizerNode* node = rage_new NodeFromItemIndex(itemIndex->m_Integer);
		parent->AddChild(node);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Parser::Parse(TokenizedRuleList& tokenizedRules, atArray<ExpressionOptimizerRule*>& rules)
{
	m_TokenizedRules = &tokenizedRules;
	m_Rules = &rules;

	for (int i = 0; i < tokenizedRules.GetCount(); ++i) 
	{
		m_CurrentToken = &((*tokenizedRules[i])[0]);

		ExpressionOptimizerRule* rule = rage_new ExpressionOptimizerRule;
		rules.PushAndGrow(rule);

#if 0
		if (i == NELEM(g_Rules)-1)
		{
			int j = 0;
			j += 1;
		}
#endif

		CreateFilters(*this, *rule);

		Expect(Token::kColon);

		rule->SetRoot(CreateTree(*this));
	}

	m_CurrentToken = NULL;
	m_Rules = NULL;
	m_TokenizedRules = NULL;
}

////////////////////////////////////////////////////////////////////////////////

struct RuleBasedOptimizeIterator : public crExpressionOp::ModifyingIterator
{
	RuleBasedOptimizeIterator(atArray<ExpressionOptimizerRule*>& ruleList)
		: m_Changed(false)
		, m_Rules (ruleList)
	{
	}

	virtual ~RuleBasedOptimizeIterator()
	{
	}

	struct OptimizeContext
	{
		OptimizeContext()
			: m_Radians(false)
		{
		}

		void Init(const OptimizeContext& oc)
		{
			m_Radians = oc.m_Radians;
		}

		float m_Radians;
	};


	virtual crExpressionOp* Visit(crExpressionOp& op)
	{
		OptimizeContext& oc = m_OptimizeContexts.Grow();
		if(m_OptimizeContexts.GetCount() > 1)
		{
			oc.Init(m_OptimizeContexts[m_OptimizeContexts.GetCount()-2]);
		}

		crExpressionOp* newOp = Optimize(op, oc);
		if(newOp != &op)
		{
			m_Changed = true;
		}

		crExpressionOp* retOp = crExpressionOp::ModifyingIterator::Visit(*newOp);

		m_OptimizeContexts.Pop();

		return retOp;
	}

	crExpressionOp* Optimize(crExpressionOp& op, OptimizeContext&)
	{
		crExpressionOp* result = NULL;
		for (int i = 0; i < m_Rules.GetCount(); ++i) 
		{
			ExpressionRuleContext context; // resulting lists exist in here...
			ExpressionOptimizerRule& rule = *m_Rules[i];
			bool passed = true;

			const ExpressionTypeFilter& filter = static_cast<const ExpressionTypeFilter&>(rule.GetFilter(0));
			if (strcmpi(filter.m_Type.c_str(), ExpressionOpTypeToString(op)) == 0) 
			{
				ExpressionRuleContext::ExpressionOpList* f = rage_new ExpressionRuleContext::ExpressionOpList;
				f->PushAndGrow(&op);
				context.m_Items.PushAndGrow(f);
				for (int fidx = 1; fidx < rule.GetFilterCount(); ++fidx)
				{
					context.m_CurrentNewList = rage_new ExpressionRuleContext::ExpressionOpList;

					rule.GetFilter(fidx).Process(context);

					if (rule.GetFilter(fidx).GetModifier() == ExpressionOptimizerFilterProcess::kContinueOnEmpty) 
					{
						if (context.m_CurrentNewList->GetCount() == 0) 
						{
							// still append the list, but there won't be anything in it
							context.Append(*context.m_CurrentNewList);
						} 
						else 
						{
							ExpressionRuleContext::Free(context.m_CurrentNewList);
							context.m_CurrentNewList = NULL;
							passed = false;
							break;
						}
					}
					else if (context.m_CurrentNewList && context.m_CurrentNewList->GetCount() != 0) 
					{
						context.Append(*context.m_CurrentNewList);
					}
					else
					{
						ExpressionRuleContext::Free(context.m_CurrentNewList);
						context.m_CurrentNewList = NULL;
						passed = false;
						break;
					}
				}

				if (passed) 
				{
#if 0
					if (i == NELEM(g_Rules)-1)
					{
						Displayf("break");
					}
#endif
					result = rule.Create(context);
#if 0
					if (i == NELEM(g_Rules)-1)
					{
						Displayf("Rule '%d'", i);
						Displayf("Replaced:");
						crExpressionOp::DumpHelper from;
						from.Visit(op);
						Displayf("With:");
						crExpressionOp::DumpHelper to;
						to.Visit(*result);
					}
#endif
					break;
				}
			}
		}

		if (result) 
		{
			delete &op;
			return result;
		}

		return &op;
	}

	bool m_Changed;
	atArray<OptimizeContext> m_OptimizeContexts;
	atArray<ExpressionOptimizerRule*> m_Rules;
};

////////////////////////////////////////////////////////////////////////////////

typedef atArray<Token*> ExpressionOptimizerRuleTokens;
typedef atArray<ExpressionOptimizerRuleTokens*> ExpressionOptimizerRuleTokenList;

////////////////////////////////////////////////////////////////////////////////

void BuildOptimizerRuleList(atArray<ExpressionOptimizerRule*>& optimizerRules)
{
	ExpressionOptimizerRuleTokenList tokenizedRules;
	Tokenizer tokenizer;

	for (int i = 0; i < sizeof(g_Rules)/sizeof(g_Rules[0]); ++i) 
	{
		ExpressionOptimizerRuleTokens* tokens = rage_new ExpressionOptimizerRuleTokens;
		tokenizedRules.PushAndGrow(tokens);
#if 0
		if (i == NELEM(g_Rules)-1)
		{
			int j = 0;
			j += 1;
		}
#endif

		tokenizer.Tokenize(g_Rules[i], *tokens);
	}

	Parser parser;
	parser.Parse(tokenizedRules, optimizerRules);

	// free allocated tokens
	for (int i = 0; i < tokenizedRules.GetCount(); ++i)
	{
		ExpressionOptimizerRuleTokens* tokens = tokenizedRules[i];
		for (int j = 0; j < tokens->GetCount(); ++j)
		{
			delete (*tokens)[j];
		}
		delete tokens;
	}
}

////////////////////////////////////////////////////////////////////////////////

void ReleaseOptimizerRuleList(atArray<ExpressionOptimizerRule*>& optimizerRules)
{
	for (int i = 0; i < optimizerRules.GetCount(); ++i)
	{
		delete optimizerRules[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

void OptimizeIntraExpressionDataWithRules(crExpressions& expressions, atArray<ExpressionOptimizerRule*>& rules)
{
	// intra-expression optimization
	const int numExpressions = expressions.GetNumExpressions();
	for(int i=0; i<numExpressions; ++i)
	{
		crExpression& expression = *expressions.GetExpression(i);

		// multi pass optimization
		int test = 0;
		do {
			RuleBasedOptimizeIterator rit (rules);
			expression.SetExpressionOp(rit.Visit(*expression.GetExpressionOp()));

			rage::MakeExpressions::OptimizeIterator nrit(true);
			expression.SetExpressionOp(nrit.Visit(*expression.GetExpressionOp()));

			if(!rit.m_Changed && !nrit.m_Changed)
			{
				break;
			}
			++test;
		} 
		while(1);
	}
}

////////////////////////////////////////////////////////////////////////////////

static bool HasAttribute(const crExpressionOp& op, const char* attribute, Variant*& variant) 
{
	variant = NULL;

	crExpressionOp::eOpTypes opType = op.GetOpType();
	switch (opType) 
	{
		case crExpressionOp::kOpTypeConstant:
			{
				const crExpressionOpConstant& o = static_cast<const crExpressionOpConstant&>(op);
				if (strcmpi(attribute, "vec3") == 0) {
					crExpressionOp::Value value = o.GetValue(); // copy to get around const'ness
					atArray<float> vec3 (3, 3);
					vec3[0] = value.GetVector().GetXf();
					vec3[1] = value.GetVector().GetYf();
					vec3[2] = value.GetVector().GetZf();
					variant = rage_new Vector3Variant (vec3);
					return true;
				} 
				else if (strcmpi(attribute, "quat") == 0) 
				{
					crExpressionOp::Value value = o.GetValue();
					atArray<float> quat (4, 4);
					quat[0] = value.GetQuaternion().GetXf();
					quat[1] = value.GetQuaternion().GetYf();
					quat[2] = value.GetQuaternion().GetZf();
					quat[3] = value.GetQuaternion().GetWf();
					variant = rage_new QuaternionVariant (quat);
					return true;
				} 
				else if (strcmpi(attribute, "float") == 0) 
				{
					crExpressionOp::Value value = o.GetValue();
					variant = rage_new FloatVariant (value.GetFloat());
					return true;
				} 
				else if (strcmpi(attribute, "int") == 0) 
				{
					crExpressionOp::Value value = o.GetValue();
					variant = rage_new IntVariant (value.GetInt());
					return true;
				} else if (strcmpi(attribute, "fx") == 0) {
					crExpressionOp::Value val = o.GetValue();
					QuatV values = val.GetQuaternion();
					variant = rage_new FloatVariant(values.GetXf());
					return true;
				} else if (strcmpi(attribute, "fy") == 0) {
					crExpressionOp::Value val = o.GetValue();
					QuatV values = val.GetQuaternion();
					variant = rage_new FloatVariant(values.GetYf());
					return true;
				} else if (strcmpi(attribute, "fz") == 0) {
					crExpressionOp::Value val = o.GetValue();
					QuatV values = val.GetQuaternion();
					variant = rage_new FloatVariant(values.GetZf());
					return true;
				} else if (strcmpi(attribute, "fw") == 0) {
					crExpressionOp::Value val = o.GetValue();
					QuatV values = val.GetQuaternion();
					variant = rage_new FloatVariant(values.GetWf());
					return true;
				}
			}
			return false;
			break;
		case crExpressionOp::kOpTypeConstantFloat:
			{
				const crExpressionOpConstantFloat& o = static_cast<const crExpressionOpConstantFloat&>(op);
				if (strcmpi(attribute, "float") == 0) {
					variant = rage_new FloatVariant(o.GetFloat());
					return true;
				}
				return false;
			}
			break;
		case crExpressionOp::kOpTypeValid:
			return false;
		case crExpressionOp::kOpTypeGet:
		case crExpressionOp::kOpTypeSet:

		case crExpressionOp::kOpTypeObjectSpaceGet:
		case crExpressionOp::kOpTypeObjectSpaceSet:
		case crExpressionOp::kOpTypeObjectSpaceConvertTo:
		case crExpressionOp::kOpTypeObjectSpaceConvertFrom:
			{
				const crExpressionOpGetSet& o = static_cast<const crExpressionOpGetSet&>(op);
				if (strcmpi(attribute, "track") == 0) 
				{
					variant = rage_new AnimationTrackIndexVariant (o.GetTrack());
					return true;
				} 
				else if (strcmpi(attribute, "type") == 0) 
				{
					variant = rage_new AnimationTrackTypeVariant (o.GetType());
					return true;
				} 
				else if (strcmpi(attribute, "id") == 0) 
				{
					variant = rage_new AnimationTrackIdVariant (o.GetId());
					return true;
				} 
				else if (strcmpi(attribute, "accelerated_index") == 0) 
				{
					return false;
				} 
				else if (strcmpi(attribute, "relative") == 0) 
				{
					variant = rage_new BoolVariant(o.IsRelative());
					return true;
				}
			}
			return false;
		case crExpressionOp::kOpTypeComponentGet:
		case crExpressionOp::kOpTypeComponentSet:
			{
				const crExpressionOpComponentGetSet& o = static_cast<const crExpressionOpComponentGetSet&>(op);
				if (strcmpi(attribute, "track") == 0) 
				{
					variant = rage_new AnimationTrackIndexVariant (o.GetTrack());
					return true;
				} 
				else if (strcmpi(attribute, "type") == 0) 
				{
					variant = rage_new AnimationTrackTypeVariant (o.GetType());
					return true;
				} 
				else if (strcmpi(attribute, "id") == 0) 
				{
					variant = rage_new AnimationTrackIdVariant (o.GetId());
					return true;
				} 
				else if (strcmpi(attribute, "accelerated_index") == 0) 
				{
					return false;
				} 
				else if (strcmpi(attribute, "relative") == 0) 
				{
					variant = rage_new BoolVariant(o.IsRelative());
					return true;
				}
				else if (strcmpi(attribute, "component") == 0)
				{
					variant = rage_new IntVariant(o.GetComponent());
					return true;
				}
				else if (strcmpi(attribute, "order") == 0)
				{
					variant = rage_new IntVariant(o.GetOrder());
					return true;
				}
			}
			return false;
		case crExpressionOp::kOpTypeNullary:
			{
				const crExpressionOpNullary& nullary = static_cast<const crExpressionOpNullary&>(op);
				crExpressionOpNullary::eNullaryOpTypes nullaryOpType = nullary.GetNullaryOpType();
				switch (nullaryOpType) 
				{
					case crExpressionOpNullary::kNullaryOpTypeZero:
						return false;
					case crExpressionOpNullary::kNullaryOpTypeOne:
						return false;
					case crExpressionOpNullary::kNullaryOpTypePi:
						return false;
					case crExpressionOpNullary::kNullaryOpTypeTime:
						return false;
					case crExpressionOpNullary::kNullaryOpTypeRandom:
						return false;
					case crExpressionOpNullary::kNullaryOpTypeVectorZero:
						return false;
					case crExpressionOpNullary::kNullaryOpTypeVectorOne:
						return false;
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeUnary:
			{
				const crExpressionOpUnary& unary = static_cast<const crExpressionOpUnary&>(op);
				crExpressionOpUnary::eUnaryOpTypes unaryOpType = unary.GetUnaryOpType();
				switch (unaryOpType) 
				{
					case crExpressionOpUnary::kUnaryOpTypeLogicalNot:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeNegate:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeReciprocal:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeSquare:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeSqrt:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeAbsolute:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeFloor:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeCeil:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeLog:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeLn:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeExp:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeClamp01:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeCos:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeSin:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeTan:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeArcCos:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeArcSin:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeArcTan:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeCosH:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeSinH:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeTanH:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeDegreesToRadians:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeFromEuler:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeToEuler:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeSplat:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeVectorClamp01:
						return false;
					case crExpressionOpUnary::kUnaryOpTypeQuatInverse:
						return false;
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeBinary:
			{
				const crExpressionOpBinary& binary = static_cast<const crExpressionOpBinary&>(op);
				crExpressionOpBinary::eBinaryOpTypes binaryOpType = binary.GetBinaryOpType();
				switch (binaryOpType) 
				{
					case crExpressionOpBinary::kBinaryOpTypeEqualTo:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeNotEqualTo:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeGreaterThan:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeLessThan:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeGreaterThanEqualTo:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeLessThanEqualTo:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeAdd:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeSubtract:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeMultiply:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeDivide:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeModulus:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeExponent:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeMax:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeMin:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeLogicalAnd:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeLogicalOr:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeLogicalXor:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeQuatMultiply:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeVectorAdd:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeVectorMultiply:
						return false;
					case crExpressionOpBinary::kBinaryOpTypeVectorTransform:
						return false;
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeTernary:
			{
				const crExpressionOpTernary& ternary = static_cast<const crExpressionOpTernary&>(op);
				crExpressionOpTernary::eTernaryOpTypes ternaryOpType = ternary.GetTernaryOpType();
				switch (ternaryOpType) 
				{
					case crExpressionOpTernary::kTernaryOpTypeMultiplyAdd:
						return false;
					case crExpressionOpTernary::kTernaryOpTypeClamp:
						return false;
					case crExpressionOpTernary::kTernaryOpTypeLerp:
						return false;
					case crExpressionOpTernary::kTernaryOpTypeToQuaternion:
						return false;
					case crExpressionOpTernary::kTernaryOpTypeToVector:
						return false;
					case crExpressionOpTernary::kTernaryOpTypeConditional:
						return false;
					case crExpressionOpTernary::kTernaryOpTypeVectorMultiplyAdd:
						return false;
					case crExpressionOpTernary::kTernaryOpTypeVectorLerpVector:
						return false;
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeNary:
			{
				const crExpressionOpNary& nary = static_cast<const crExpressionOpNary&>(op);
				crExpressionOpNary::eNaryOpTypes naryOpType = nary.GetNaryOpType();
				switch (naryOpType) 
				{
				case crExpressionOpNary::kNaryOpTypeComma:
					return false;
				case crExpressionOpNary::kNaryOpTypeSum:
					return false;
				case crExpressionOpNary::kNaryOpTypeList:
					return false;
				default:
					break;
				}
			}
			break;
		default:
			return false;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

AttributeSetResult SetAttributeFromIndex(crExpressionOp& op, int index, Variant* variant)
{
	crExpressionOp::eOpTypes opType = op.GetOpType();
	switch (opType) 
	{
		case crExpressionOp::kOpTypeConstant:
			{
				crExpressionOpConstant& o = static_cast<crExpressionOpConstant&>(op);
				if (0 <= index && index < 4) 
				{
					Vec3V vec = o.GetValue().GetVector();
					FloatVariant* f = static_cast<FloatVariant*>(variant);
					vec[index] = f->GetFloat();
					o.SetValue(vec);

					return kAttributeSetSucceeded;
				}
				else
				{
					return kAttributeIndexOutOfRange;
				}
			}
			break;
		default:
			break;
	}

	return kAttributeNotFound;
}

////////////////////////////////////////////////////////////////////////////////

AttributeSetResult SetAttribute(crExpressionOp& op, const char* attribute, Variant* variant)
{
	crExpressionOp::eOpTypes opType = op.GetOpType();
	switch (opType) 
	{
		case crExpressionOp::kOpTypeConstant:
			{
				crExpressionOpConstant& o = static_cast<crExpressionOpConstant&>(op);
				if (strcmpi(attribute, "vec3") == 0) 
				{
					Vector3Variant* v = static_cast<Vector3Variant*>(variant);
					crExpressionOp::Value value;
					const atArray<float>& vec = v->GetVec();
					value.Set(Vec3V(vec[0], vec[1], vec[2]));
					o.SetValue(value);
					return kAttributeSetSucceeded;
				} 
				else if (strcmpi(attribute, "quat") == 0) 
				{
					QuaternionVariant* v = static_cast<QuaternionVariant*>(variant);
					crExpressionOp::Value value;
					const atArray<float>& quat = v->GetQuat();
					value.Set(QuatV(quat[0], quat[1], quat[2], quat[3]));
					o.SetValue(value);
					return kAttributeSetSucceeded;
				} 
				else if (strcmpi(attribute, "float") == 0) 
				{
					FloatVariant* v = static_cast<FloatVariant*>(variant);
					crExpressionOp::Value value;
					value.Set(v->GetFloat());
					o.SetValue(value);
					return kAttributeSetSucceeded;
				} 
				else if (strcmpi(attribute, "int") == 0) 
				{
					IntVariant* v = static_cast<IntVariant*>(variant);
					crExpressionOp::Value value;
					value.Set(v->GetInt());
					o.SetValue(value);
					return kAttributeSetSucceeded;
				} else if (strcmpi(attribute, "fx") == 0) {
					QuaternionVariant* v = static_cast<QuaternionVariant*>(variant);
					crExpressionOp::Value value;
					value.Set(v->GetQuat()[0]);
					o.SetValue(value);
					return kAttributeSetSucceeded;
				} else if (strcmpi(attribute, "fy") == 0) {
					QuaternionVariant* v = static_cast<QuaternionVariant*>(variant);
					crExpressionOp::Value value;
					value.Set(v->GetQuat()[1]);
					o.SetValue(value);
					return kAttributeSetSucceeded;
				} else if (strcmpi(attribute, "fz") == 0) {
					QuaternionVariant* v = static_cast<QuaternionVariant*>(variant);
					crExpressionOp::Value value;
					value.Set(v->GetQuat()[2]);
					o.SetValue(value);
					return kAttributeSetSucceeded;
				} else if (strcmpi(attribute, "fw") == 0) {
					QuaternionVariant* v = static_cast<QuaternionVariant*>(variant);
					crExpressionOp::Value value;
					value.Set(v->GetQuat()[3]);
					o.SetValue(value);
					return kAttributeSetSucceeded;
				}
			}
			return kAttributeNotFound;
		case crExpressionOp::kOpTypeConstantFloat:
			{
				crExpressionOpConstantFloat& o = static_cast<crExpressionOpConstantFloat&>(op);
				if (strcmpi(attribute, "float") == 0) 
				{
					FloatVariant* v = static_cast<FloatVariant*>(variant);
					o.SetFloat(v->GetFloat());

					return kAttributeSetSucceeded;
				}
				return kAttributeNotFound;
			}
		case crExpressionOp::kOpTypeGet:
		case crExpressionOp::kOpTypeSet:
		case crExpressionOp::kOpTypeComponentGet:
		case crExpressionOp::kOpTypeComponentSet:
		case crExpressionOp::kOpTypeObjectSpaceGet:
		case crExpressionOp::kOpTypeObjectSpaceSet:
		case crExpressionOp::kOpTypeObjectSpaceConvertTo:
		case crExpressionOp::kOpTypeObjectSpaceConvertFrom:
			{
				crExpressionOpGetSet& o = static_cast<crExpressionOpGetSet&>(op);
				if (strcmpi(attribute, "track") == 0) 
				{
					AnimationTrackIndexVariant* v = static_cast<AnimationTrackIndexVariant*>(variant);
					o.SetTrack(v->GetTrackIndex());
					return kAttributeSetSucceeded;
				} 
				else if (strcmpi(attribute, "type") == 0) 
				{
					AnimationTrackTypeVariant* v = static_cast<AnimationTrackTypeVariant*>(variant);
					o.SetType(v->GetTrackType());
					return kAttributeSetSucceeded;
				} 
				else if (strcmpi(attribute, "id") == 0) 
				{
					AnimationTrackIdVariant* v = static_cast<AnimationTrackIdVariant*>(variant);
					o.SetId(v->GetTrackId());
					return kAttributeSetSucceeded;
				}
				else if (strcmpi(attribute, "relative") == 0)
				{
					BoolVariant* v = static_cast<BoolVariant*>(variant);
					o.SetRelative(v->GetValue());
					return kAttributeSetSucceeded;
				}
				
				switch (opType)
				{
				case crExpressionOp::kOpTypeComponentGet:
				case crExpressionOp::kOpTypeComponentSet:
					{
						crExpressionOpComponentGetSet& co = static_cast<crExpressionOpComponentGetSet&>(op);
						if (strcmpi(attribute, "component") == 0)
						{
							IntVariant* v = static_cast<IntVariant*>(variant);
							Assert(v->GetInt() < CHAR_MAX);
							co.SetComponent(u8(v->GetInt()));
							return kAttributeSetSucceeded;
						} 
						else if (strcmpi(attribute, "order") == 0)
						{
							IntVariant* v = static_cast<IntVariant*>(variant);
							Assert(v->GetInt() < CHAR_MAX);
							co.SetOrder(u8(v->GetInt()));
							return kAttributeSetSucceeded;
						}
					}
				default:
					break;
				}
			}
			return kAttributeNotFound;
		case crExpressionOp::kOpTypeValid:
			return kAttributeInvalidOp;
		case crExpressionOp::kOpTypeNullary:
			{
				crExpressionOpNullary& nullary = static_cast<crExpressionOpNullary&>(op);
				crExpressionOpNullary::eNullaryOpTypes nullaryOpType = nullary.GetNullaryOpType();
				switch (nullaryOpType) 
				{
					case crExpressionOpNullary::kNullaryOpTypeZero:
					case crExpressionOpNullary::kNullaryOpTypeOne:
					case crExpressionOpNullary::kNullaryOpTypePi:
					case crExpressionOpNullary::kNullaryOpTypeTime:
					case crExpressionOpNullary::kNullaryOpTypeRandom:
					case crExpressionOpNullary::kNullaryOpTypeVectorZero:
					case crExpressionOpNullary::kNullaryOpTypeVectorOne:
						return kAttributeInvalidOp;
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeUnary:
			{
				crExpressionOpUnary& unary = static_cast<crExpressionOpUnary&>(op);
				crExpressionOpUnary::eUnaryOpTypes unaryOpType = unary.GetUnaryOpType();
				switch (unaryOpType) 
				{
				case crExpressionOpUnary::kUnaryOpTypeLogicalNot:
				case crExpressionOpUnary::kUnaryOpTypeNegate:
				case crExpressionOpUnary::kUnaryOpTypeReciprocal:
				case crExpressionOpUnary::kUnaryOpTypeSquare:
				case crExpressionOpUnary::kUnaryOpTypeSqrt:
				case crExpressionOpUnary::kUnaryOpTypeAbsolute:
				case crExpressionOpUnary::kUnaryOpTypeFloor:
				case crExpressionOpUnary::kUnaryOpTypeCeil:
				case crExpressionOpUnary::kUnaryOpTypeLog:
				case crExpressionOpUnary::kUnaryOpTypeLn:
				case crExpressionOpUnary::kUnaryOpTypeExp:
				case crExpressionOpUnary::kUnaryOpTypeClamp01:
				case crExpressionOpUnary::kUnaryOpTypeCos:
				case crExpressionOpUnary::kUnaryOpTypeSin:
				case crExpressionOpUnary::kUnaryOpTypeTan:
				case crExpressionOpUnary::kUnaryOpTypeArcCos:
				case crExpressionOpUnary::kUnaryOpTypeArcSin:
				case crExpressionOpUnary::kUnaryOpTypeArcTan:
				case crExpressionOpUnary::kUnaryOpTypeCosH:
				case crExpressionOpUnary::kUnaryOpTypeSinH:
				case crExpressionOpUnary::kUnaryOpTypeTanH:
				case crExpressionOpUnary::kUnaryOpTypeDegreesToRadians:
				case crExpressionOpUnary::kUnaryOpTypeRadiansToDegrees:
				case crExpressionOpUnary::kUnaryOpTypeFromEuler:
				case crExpressionOpUnary::kUnaryOpTypeToEuler:
				case crExpressionOpUnary::kUnaryOpTypeSplat:
				case crExpressionOpUnary::kUnaryOpTypeVectorClamp01:
				case crExpressionOpUnary::kUnaryOpTypeQuatInverse:
					return kAttributeInvalidOp;
				default:
					break;
				}
			}
			break;
		case crExpressionOp::kOpTypeBinary:
			{
				crExpressionOpBinary& binary = static_cast<crExpressionOpBinary&>(op);
				crExpressionOpBinary::eBinaryOpTypes binaryOpType = binary.GetBinaryOpType();
				switch (binaryOpType) 
				{
					case crExpressionOpBinary::kBinaryOpTypeEqualTo:
					case crExpressionOpBinary::kBinaryOpTypeNotEqualTo:
					case crExpressionOpBinary::kBinaryOpTypeGreaterThan:
					case crExpressionOpBinary::kBinaryOpTypeLessThan:
					case crExpressionOpBinary::kBinaryOpTypeGreaterThanEqualTo:
					case crExpressionOpBinary::kBinaryOpTypeLessThanEqualTo:
					case crExpressionOpBinary::kBinaryOpTypeAdd:
					case crExpressionOpBinary::kBinaryOpTypeSubtract:
					case crExpressionOpBinary::kBinaryOpTypeMultiply:
					case crExpressionOpBinary::kBinaryOpTypeDivide:
					case crExpressionOpBinary::kBinaryOpTypeModulus:
					case crExpressionOpBinary::kBinaryOpTypeExponent:
					case crExpressionOpBinary::kBinaryOpTypeMax:
					case crExpressionOpBinary::kBinaryOpTypeMin:
					case crExpressionOpBinary::kBinaryOpTypeLogicalAnd:
					case crExpressionOpBinary::kBinaryOpTypeLogicalOr:
					case crExpressionOpBinary::kBinaryOpTypeLogicalXor:
					case crExpressionOpBinary::kBinaryOpTypeQuatMultiply:
					case crExpressionOpBinary::kBinaryOpTypeVectorAdd:
					case crExpressionOpBinary::kBinaryOpTypeVectorMultiply:
					case crExpressionOpBinary::kBinaryOpTypeVectorTransform:
						return kAttributeInvalidOp;
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeTernary:
			{
				crExpressionOpTernary& ternary = static_cast<crExpressionOpTernary&>(op);
				crExpressionOpTernary::eTernaryOpTypes ternaryOpType = ternary.GetTernaryOpType();
				switch (ternaryOpType) 
				{
					case crExpressionOpTernary::kTernaryOpTypeMultiplyAdd:
					case crExpressionOpTernary::kTernaryOpTypeClamp:
					case crExpressionOpTernary::kTernaryOpTypeLerp:
					case crExpressionOpTernary::kTernaryOpTypeToQuaternion:
					case crExpressionOpTernary::kTernaryOpTypeToVector:
					case crExpressionOpTernary::kTernaryOpTypeConditional:
					case crExpressionOpTernary::kTernaryOpTypeVectorMultiplyAdd:
					case crExpressionOpTernary::kTernaryOpTypeVectorLerpVector:
						return kAttributeInvalidOp;
					default:
						break;
				}
			}
			break;
		case crExpressionOp::kOpTypeNary:
			{
				crExpressionOpNary& nary = static_cast<crExpressionOpNary&>(op);
				crExpressionOpNary::eNaryOpTypes naryOpType = nary.GetNaryOpType();
				switch (naryOpType) 
				{
					case crExpressionOpNary::kNaryOpTypeComma:
					case crExpressionOpNary::kNaryOpTypeSum:
					case crExpressionOpNary::kNaryOpTypeList:
						return kAttributeInvalidOp;
					default:
						break;
				}
			}
			break;
		default:
			return kAttributeInvalidOp;
	}

	return kAttributeInvalidOp;
}

} // namespace MakeExpressionsRules

} // namespace rage
