// 
// exprtools/applyexpressionscore.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EXPRTOOLS_APPLYEXPRESSIONSCORE_H
#define EXPRTOOLS_APPLYEXPRESSIONSCORE_H

namespace rage
{

class crAnimation;
class crExpressions;
class crSkeletonData;

namespace crApplyExpressionsCore
{

// PURPOSE: Apply expressions, using an input control animation - store results in output animation	
extern bool ApplyExpressions(const crExpressions& expressions, const crAnimation& srcAnim, crAnimation& destAnim, const crSkeletonData* skelData=NULL, float sampleFreq=30.f);

}; // namespace crApplyExpressionsCore

}; // namespace rage

#endif // EXPRTOOLS_APPLYEXPRESSIONSCORE_H

