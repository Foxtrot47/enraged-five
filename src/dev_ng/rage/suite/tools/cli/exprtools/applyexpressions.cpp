// 
// exprtools/applyexpressions.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "applyexpressionscore.h"

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"
#include "crextra/expressions.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(anim, "Source animation (Example: in.anim)");
PARAM(out, "Output animation (Example: out.anim)");
PARAM(expr, "Expression file (Example: expression.expr)");
PARAM(skel, "Optional skeleton file (Example: character.skel)");
PARAM(samplerate, "Rate to use when sampling (Example: 0.033333)");
PARAM(samplefreq, "Sample frequency (Example: 30.0)");
//PARAM(rawonly, "Only accept raw input animation, generate error (not warning) on non-raw");


int Main()
{
	// initialization:
	ASSET.SetPath(RAGE_ASSET_ROOT);
	crAnimation::InitClass();
	crExpressions::InitClass();

	// process help:
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process input animation:
	const char* srcAnimFilename = NULL;
	if(!PARAM_anim.Get(srcAnimFilename) || !srcAnimFilename || !srcAnimFilename[0])
	{
		Quitf("ERROR - No source animation specified");
	}

	crAnimation srcAnim;
	if(!srcAnim.Load(srcAnimFilename))
	{
		Quitf("ERROR - Failed to load source animation '%s'", srcAnimFilename);
	}

	// process output animation:
	const char* destAnimFilename = NULL;
	if(!PARAM_out.Get(destAnimFilename) || !destAnimFilename || !destAnimFilename[0])
	{
		Quitf("ERROR - No output animation specified");
	}

	// process expression:
	const char* expressionsFilename = NULL;
	if(!PARAM_expr.Get(expressionsFilename) || !expressionsFilename || !expressionsFilename[0])
	{
		Quitf("ERROR - No expression filename specified");
	}

	crExpressions expressions;
	if(!expressions.Load(expressionsFilename))
	{
		Quitf("ERROR - Failed to load expressions '%s'", expressionsFilename);
	}

	// process skeleton:
	const char* skelFilename = NULL;
	PARAM_skel.Get(skelFilename);

	crSkeletonData skelData;
	crSkeletonData* skelDataPtr = NULL;
	if(skelFilename && skelFilename[0])
	{
		if(skelData.Load(skelFilename))
		{
			skelDataPtr = &skelData;
		}
		else
		{
			Quitf("ERROR - failed to load skeleton '%s'", skelFilename);
		}
	}

	// process sample freq/rate:
	float sampleFreq = 30.f;
	PARAM_samplefreq.Get(sampleFreq);
	float sampleRate = 1.f/sampleFreq;
	if(PARAM_samplerate.Get(sampleRate))
	{
		sampleFreq = 1.f/sampleRate;
	}

	// apply expressions:
	crAnimation destAnim;
	if(crApplyExpressionsCore::ApplyExpressions(expressions, srcAnim, destAnim, skelDataPtr, sampleFreq))
	{
		if(!destAnim.Save(destAnimFilename))
		{
			Quitf("ERROR - Failed to save output animation '%s'", destAnimFilename);
		}
	}
	else
	{
		Quitf("ERROR - Failed to apply expressions");
	}

	// shutdown:
	crExpressions::ShutdownClass();
	crAnimation::ShutdownClass();

	return 0;
}

 

