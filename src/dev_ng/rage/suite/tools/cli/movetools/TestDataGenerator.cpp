#include "TestDataGenerator.h"

#include "parser/manager.h"

using namespace rage;

void TestDataGenerator::BuildAbsoluteMoverNetwork()
{
	Network network;
	network.m_Root = "Root";

	MotionTreeNode* root = rage_new MotionTreeNode;
	root->m_Name = "Root";
	root->m_Initial = "Blend 2";
	root->m_OnEnterEvent.m_Enabled = false;
	root->m_OnExitEvent.m_Enabled = false;
	network.m_Nodes.PushAndGrow(root);
	root->m_Children.PushAndGrow(::rage::atString("Blend 2"));
	root->m_Children.PushAndGrow(::rage::atString("Clip"));
	root->m_Children.PushAndGrow(::rage::atString("moverClip"));

	BlendNode* blend2 = rage_new BlendNode;
	network.m_Nodes.PushAndGrow(blend2);
	blend2->m_Name = "Blend 2";
	blend2->m_Child0 = "Clip";
	blend2->m_Child1 = "moverClip";
	blend2->m_Weight.m_Type = kF32AttributeValue;
	blend2->m_Weight.m_Value = 1.f;
	blend2->m_Filter.m_Type = kFilterFilename;
	blend2->m_Filter.m_FilterName = "MoverOnly_Filter.xml";

	ClipNode* clip = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(clip);
	clip->m_Name = "Clip";
	clip->m_Clip.m_Type = kClipAttributeParameter;
	clip->m_Clip.m_Parameter = "clip";
	clip->m_Phase.m_Type = kF32AttributeParameter;
	clip->m_Phase.m_Parameter = "phase";
	clip->m_Delta.m_Type = kF32AttributeParameter;
	clip->m_Delta.m_Parameter = "delta";
	clip->m_Looped.m_Type = kBoolAttributeParameter;
	clip->m_Looped.m_Parameter = "looped";
	clip->m_Rate.m_Type = kF32AttributeValue;
	clip->m_Rate.m_Value = 0.f;

	ClipNode* moverClip = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(moverClip);
	moverClip->m_Name = "moverClip";
	moverClip->m_Clip.m_Type = kClipAttributeParameter;
	moverClip->m_Clip.m_Parameter = "clip";
	moverClip->m_Delta.m_Type = kF32AttributeParameter;
	moverClip->m_Delta.m_Parameter = "moverDelta";
	moverClip->m_Phase.m_Type = kF32AttributeValue;
	moverClip->m_Phase.m_Value = 0.f;
	moverClip->m_Rate.m_Type = kF32AttributeValue;
	moverClip->m_Rate.m_Value = 0.f;

	fiStream* stream = fiStream::Create("absolutemover.xml");
	Assert(stream);
	PARSER.SaveObject(stream, &network);
	stream->Close();
}

void TestDataGenerator::BuildBlender4Network()
{
	Network network;
	network.m_Root = "Root";

	MotionTreeNode* root = rage_new MotionTreeNode;
	root->m_Name = "Root";
	root->m_Initial = "N-Way Blend";
	root->m_OnEnterEvent.m_Enabled = false;
	root->m_OnExitEvent.m_Enabled = false;
	network.m_Nodes.PushAndGrow(root);
	root->m_Children.PushAndGrow(::rage::atString("N-Way Blend"));
	root->m_Children.PushAndGrow(::rage::atString("Animation 0"));
	root->m_Children.PushAndGrow(::rage::atString("Animation 1"));
	root->m_Children.PushAndGrow(::rage::atString("Animation 2"));
	root->m_Children.PushAndGrow(::rage::atString("Animation 3"));
	root->m_Children.PushAndGrow(::rage::atString("Animation 4"));
	root->m_Children.PushAndGrow(::rage::atString("Animation 5"));
	root->m_Children.PushAndGrow(::rage::atString("Animation 6"));
	root->m_Children.PushAndGrow(::rage::atString("Animation 7"));

	BlendNwayNode* nway = rage_new BlendNwayNode;
	nway->m_Name = "N-Way Blend";
	network.m_Nodes.PushAndGrow(nway);
	nway->m_Children.Reserve(8);
	nway->m_Children.Resize(8);

	AnimationNode* anim0 = rage_new AnimationNode;
	anim0->m_Name = "Animation 0";
	anim0->m_Animation.m_Type = kAnimationAttributeParameter;
	anim0->m_Animation.m_Parameter = "Animation0";
	anim0->m_Phase.m_Type = kF32AttributeParameter;
	anim0->m_Phase.m_Parameter = "Phase0";
	anim0->m_Delta.m_Type = kF32AttributeParameter;
	anim0->m_Delta.m_Parameter = "Delta0";
	network.m_Nodes.PushAndGrow(anim0);
	nway->m_Children[0].m_Input = "Animation 0";
	nway->m_Children[0].m_InputFilter.m_Type = kFilterParameter;
	nway->m_Children[0].m_InputFilter.m_Parameter = "Filter0";
	nway->m_Children[0].m_Weight.m_Type = kF32AttributeParameter;
	nway->m_Children[0].m_Weight.m_Parameter = "Weight0";

	AnimationNode* anim1 = rage_new AnimationNode;
	anim1->m_Name = "Animation 1";
	anim1->m_Animation.m_Type = kAnimationAttributeParameter;
	anim1->m_Animation.m_Parameter = "Animation1";
	anim1->m_Phase.m_Type = kF32AttributeParameter;
	anim1->m_Phase.m_Parameter = "Phase1";
	anim1->m_Delta.m_Type = kF32AttributeParameter;
	anim1->m_Delta.m_Parameter = "Delta1";
	network.m_Nodes.PushAndGrow(anim1);
	nway->m_Children[1].m_Input = "Animation 1";
	nway->m_Children[1].m_InputFilter.m_Type = kFilterParameter;
	nway->m_Children[1].m_InputFilter.m_Parameter = "Filter1";
	nway->m_Children[1].m_Weight.m_Type = kF32AttributeParameter;
	nway->m_Children[1].m_Weight.m_Parameter = "Weight1";

	AnimationNode* anim2 = rage_new AnimationNode;
	anim2->m_Name = "Animation 2";
	anim2->m_Animation.m_Type = kAnimationAttributeParameter;
	anim2->m_Animation.m_Parameter = "Animation2";
	anim2->m_Phase.m_Type = kF32AttributeParameter;
	anim2->m_Phase.m_Parameter = "Phase2";
	anim2->m_Delta.m_Type = kF32AttributeParameter;
	anim2->m_Delta.m_Parameter = "Delta2";
	network.m_Nodes.PushAndGrow(anim2);
	nway->m_Children[2].m_Input = "Animation 2";
	nway->m_Children[2].m_InputFilter.m_Type = kFilterParameter;
	nway->m_Children[2].m_InputFilter.m_Parameter = "Filter2";
	nway->m_Children[2].m_Weight.m_Type = kF32AttributeParameter;
	nway->m_Children[2].m_Weight.m_Parameter = "Weight2";

	AnimationNode* anim3 = rage_new AnimationNode;
	anim3->m_Name = "Animation 3";
	anim3->m_Animation.m_Type = kAnimationAttributeParameter;
	anim3->m_Animation.m_Parameter = "Animation3";
	anim3->m_Phase.m_Type = kF32AttributeParameter;
	anim3->m_Phase.m_Parameter = "Phase3";
	anim3->m_Delta.m_Type = kF32AttributeParameter;
	anim3->m_Delta.m_Parameter = "Delta3";
	network.m_Nodes.PushAndGrow(anim3);
	nway->m_Children[3].m_Input = "Animation 3";
	nway->m_Children[3].m_InputFilter.m_Type = kFilterParameter;
	nway->m_Children[3].m_InputFilter.m_Parameter = "Filter3";
	nway->m_Children[3].m_Weight.m_Type = kF32AttributeParameter;
	nway->m_Children[3].m_Weight.m_Parameter = "Weight3";

	AnimationNode* anim4 = rage_new AnimationNode;
	anim4->m_Name = "Animation 4";
	anim4->m_Animation.m_Type = kAnimationAttributeParameter;
	anim4->m_Animation.m_Parameter = "Animation4";
	anim4->m_Phase.m_Type = kF32AttributeParameter;
	anim4->m_Phase.m_Parameter = "Phase4";
	anim4->m_Delta.m_Type = kF32AttributeParameter;
	anim4->m_Delta.m_Parameter = "Delta4";
	network.m_Nodes.PushAndGrow(anim4);
	nway->m_Children[4].m_Input = "Animation 4";
	nway->m_Children[4].m_InputFilter.m_Type = kFilterParameter;
	nway->m_Children[4].m_InputFilter.m_Parameter = "Filter4";
	nway->m_Children[4].m_Weight.m_Type = kF32AttributeParameter;
	nway->m_Children[4].m_Weight.m_Parameter = "Weight4";

	AnimationNode* anim5 = rage_new AnimationNode;
	anim5->m_Name = "Animation 5";
	anim5->m_Animation.m_Type = kAnimationAttributeParameter;
	anim5->m_Animation.m_Parameter = "Animation5";
	anim5->m_Phase.m_Type = kF32AttributeParameter;
	anim5->m_Phase.m_Parameter = "Phase5";
	anim5->m_Delta.m_Type = kF32AttributeParameter;
	anim5->m_Delta.m_Parameter = "Delta5";
	network.m_Nodes.PushAndGrow(anim5);
	nway->m_Children[5].m_Input = "Animation 5";
	nway->m_Children[5].m_InputFilter.m_Type = kFilterParameter;
	nway->m_Children[5].m_InputFilter.m_Parameter = "Filter5";
	nway->m_Children[5].m_Weight.m_Type = kF32AttributeParameter;
	nway->m_Children[5].m_Weight.m_Parameter = "Weight5";

	AnimationNode* anim6 = rage_new AnimationNode;
	anim6->m_Name = "Animation 6";
	anim6->m_Animation.m_Type = kAnimationAttributeParameter;
	anim6->m_Animation.m_Parameter = "Animation6";
	anim6->m_Phase.m_Type = kF32AttributeParameter;
	anim6->m_Phase.m_Parameter = "Phase6";
	anim6->m_Delta.m_Type = kF32AttributeParameter;
	anim6->m_Delta.m_Parameter = "Delta6";
	network.m_Nodes.PushAndGrow(anim6);
	nway->m_Children[6].m_Input = "Animation 6";
	nway->m_Children[6].m_InputFilter.m_Type = kFilterParameter;
	nway->m_Children[6].m_InputFilter.m_Parameter = "Filter6";
	nway->m_Children[6].m_Weight.m_Type = kF32AttributeParameter;
	nway->m_Children[6].m_Weight.m_Parameter = "Weight6";

	AnimationNode* anim7 = rage_new AnimationNode;
	anim7->m_Name = "Animation 7";
	anim7->m_Animation.m_Type = kAnimationAttributeParameter;
	anim7->m_Animation.m_Parameter = "Animation7";
	anim7->m_Phase.m_Type = kF32AttributeParameter;
	anim7->m_Phase.m_Parameter = "Phase7";
	anim7->m_Delta.m_Type = kF32AttributeParameter;
	anim7->m_Delta.m_Parameter = "Delta7";
	network.m_Nodes.PushAndGrow(anim7);
	nway->m_Children[7].m_Input = "Animation 7";
	nway->m_Children[7].m_InputFilter.m_Type = kFilterParameter;
	nway->m_Children[7].m_InputFilter.m_Parameter = "Filter7";
	nway->m_Children[7].m_Weight.m_Type = kF32AttributeParameter;
	nway->m_Children[7].m_Weight.m_Parameter = "Weight7";

	fiStream* stream = fiStream::Create("blender4.xml");
	Assert(stream);
	PARSER.SaveObject(stream, &network);
	stream->Close();
}

void TestDataGenerator::BuildGenericClipNetwork()
{
	Network network;
	network.m_Root = "Root";

	MotionTreeNode* root = rage_new MotionTreeNode;
	root->m_Name = "Root";
	root->m_Initial = "Blend 2";
	root->m_OnEnterEvent.m_Enabled = false;
	root->m_OnExitEvent.m_Enabled = false;
	network.m_Nodes.PushAndGrow(root);
	root->m_Children.PushAndGrow(::rage::atString("Blend 2"));
	root->m_Children.PushAndGrow(::rage::atString("Clip"));
	root->m_Children.PushAndGrow(::rage::atString("moverClip"));

	BlendNode* blend2 = rage_new BlendNode;
	network.m_Nodes.PushAndGrow(blend2);
	blend2->m_Name = "Blend 2";
	blend2->m_Child0 = "moverClip";
	blend2->m_Child1 = "Clip";
	blend2->m_Weight.m_Type = kF32AttributeValue;
	blend2->m_Weight.m_Value = 1.f;
	blend2->m_Filter.m_Type = kFilterIgnored;

	ClipNode* clip = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(clip);
	clip->m_Name = "Clip";
	clip->m_Clip.m_Type = kClipAttributeParameter;
	clip->m_Clip.m_Parameter = "clip";
	clip->m_Phase.m_Type = kF32AttributeParameter;
	clip->m_Phase.m_Parameter = "phase";
	clip->m_Delta.m_Type = kF32AttributeParameter;
	clip->m_Delta.m_Parameter = "delta";
	clip->m_Looped.m_Type = kBoolAttributeParameter;
	clip->m_Looped.m_Parameter = "looped";
	clip->m_Rate.m_Type = kF32AttributeValue;
	clip->m_Rate.m_Value = 0.f;

	ClipNode* moverClip = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(moverClip);
	moverClip->m_Name = "moverClip";
	moverClip->m_Clip.m_Type = kClipAttributeParameter;
	moverClip->m_Clip.m_Parameter = "clip";
	moverClip->m_Delta.m_Type = kF32AttributeParameter;
	moverClip->m_Delta.m_Parameter = "moverDelta";
	moverClip->m_Phase.m_Type = kF32AttributeParameter;
	moverClip->m_Phase.m_Parameter = "moverPhase";
	moverClip->m_Rate.m_Type = kF32AttributeValue;
	moverClip->m_Rate.m_Value = 0.f;

	fiStream* stream = fiStream::Create("genericclip.xml");
	Assert(stream);
	PARSER.SaveObject(stream, &network);
	stream->Close();
}

void TestDataGenerator::BuildGenericTaskNetwork()
{
	Network network;
	network.m_Root = "Root";

	MotionTreeNode* root = rage_new MotionTreeNode;
	root->m_Name = "Root";
	root->m_Initial = "HighBlend";
	root->m_OnEnterEvent.m_Enabled = false;
	root->m_OnExitEvent.m_Enabled = false;
	network.m_Nodes.PushAndGrow(root);
	root->m_Children.PushAndGrow(::rage::atString("HighBlend"));
	root->m_Children.PushAndGrow(::rage::atString("MediumBlend"));
	root->m_Children.PushAndGrow(::rage::atString("Filter"));
	root->m_Children.PushAndGrow(::rage::atString("TaskHigh"));
	root->m_Children.PushAndGrow(::rage::atString("TaskMedium"));
	root->m_Children.PushAndGrow(::rage::atString("TaskLow"));

	BlendNode* highBlend = rage_new BlendNode;
	network.m_Nodes.PushAndGrow(highBlend);
	highBlend->m_Name = "HighBlend";
	highBlend->m_Child0 = "MediumBlend";
	highBlend->m_Child1 = "TaskHigh";
	highBlend->m_Weight.m_Type = kF32AttributeParameter;
	highBlend->m_Weight.m_Parameter = "weight_high";
	highBlend->m_Filter.m_Type = kFilterParameter;
	highBlend->m_Filter.m_Parameter = "filter_high";

	BlendNode* medBlend = rage_new BlendNode;
	network.m_Nodes.PushAndGrow(medBlend);
	medBlend->m_Name = "MediumBlend";
	medBlend->m_Child0 = "Filter";
	medBlend->m_Child1 = "TaskMedium";
	medBlend->m_Weight.m_Type = kF32AttributeParameter;
	medBlend->m_Weight.m_Parameter = "weight_medium";
	medBlend->m_Filter.m_Type = kFilterParameter;
	medBlend->m_Filter.m_Parameter = "filter_medium";

	ClipNode* taskHigh = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(taskHigh);
	taskHigh->m_Name = "TaskHigh";
	taskHigh->m_Clip.m_Type = kClipAttributeParameter;
	taskHigh->m_Clip.m_Parameter = "clip_high";
	taskHigh->m_Phase.m_Type = kF32AttributeParameter;
	taskHigh->m_Phase.m_Parameter = "phase_high";
	taskHigh->m_Rate.m_Type = kF32AttributeParameter;
	taskHigh->m_Rate.m_Parameter = "rate_high";
	taskHigh->m_Delta.m_Type = kF32AttributeParameter;
	taskHigh->m_Delta.m_Parameter = "delta_high";
	taskHigh->m_Looped.m_Type = kBoolAttributeParameter;
	taskHigh->m_Looped.m_Parameter = "looped_high";

	ClipNode* taskMedium = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(taskMedium);
	taskMedium->m_Name = "TaskMedium";
	taskMedium->m_Clip.m_Type = kClipAttributeParameter;
	taskMedium->m_Clip.m_Parameter = "clip_medium";
	taskMedium->m_Phase.m_Type = kF32AttributeParameter;
	taskMedium->m_Phase.m_Parameter = "phase_medium";
	taskMedium->m_Rate.m_Type = kF32AttributeParameter;
	taskMedium->m_Rate.m_Parameter = "rate_medium";
	taskMedium->m_Delta.m_Type = kF32AttributeParameter;
	taskMedium->m_Delta.m_Parameter = "delta_medium";
	taskMedium->m_Looped.m_Type = kBoolAttributeParameter;
	taskMedium->m_Looped.m_Parameter = "looped_medium";

	ClipNode* taskLow = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(taskLow);
	taskLow->m_Name = "TaskLow";
	taskLow->m_Clip.m_Type = kClipAttributeParameter;
	taskLow->m_Clip.m_Parameter = "clip_low";
	taskLow->m_Phase.m_Type = kF32AttributeParameter;
	taskLow->m_Phase.m_Parameter = "phase_low";
	taskLow->m_Rate.m_Type = kF32AttributeParameter;
	taskLow->m_Rate.m_Parameter = "rate_low";
	taskLow->m_Delta.m_Type = kF32AttributeParameter;
	taskLow->m_Delta.m_Parameter = "delta_low";
	taskLow->m_Looped.m_Type = kBoolAttributeParameter;
	taskLow->m_Looped.m_Parameter = "looped_low";

	fiStream* stream = fiStream::Create("generictask.xml");
	Assert(stream);
	PARSER.SaveObject(stream, &network);
	stream->Close();
}

void TestDataGenerator::BuildMainNetwork()
{
	Network network;
	network.m_Root = "Root";

	MotionTreeNode* root = rage_new MotionTreeNode;
	root->m_Name = "Root";
	root->m_Initial = "Blend 2";
	root->m_OnEnterEvent.m_Enabled = false;
	root->m_OnExitEvent.m_Enabled = false;
	network.m_Nodes.PushAndGrow(root);
	root->m_Children.PushAndGrow(::rage::atString("Blend 2"));
	root->m_Children.PushAndGrow(::rage::atString("Merge 2"));
	root->m_Children.PushAndGrow(::rage::atString("pose"));
	root->m_Children.PushAndGrow(::rage::atString("Frame"));
	root->m_Children.PushAndGrow(::rage::atString("Mover Filter"));
	root->m_Children.PushAndGrow(::rage::atString("Lod Filter"));
	root->m_Children.PushAndGrow(::rage::atString("Add"));
	root->m_Children.PushAndGrow(::rage::atString("Additive"));
	root->m_Children.PushAndGrow(::rage::atString("High Prio"));
	root->m_Children.PushAndGrow(::rage::atString("Medium Prio"));
	root->m_Children.PushAndGrow(::rage::atString("Low Prio"));
	root->m_Children.PushAndGrow(::rage::atString("Registry0Network0"));
	root->m_Children.PushAndGrow(::rage::atString("Registry0Network1"));
	root->m_Children.PushAndGrow(::rage::atString("Registry0Network2"));
	root->m_Children.PushAndGrow(::rage::atString("Registry0Network3"));
	root->m_Children.PushAndGrow(::rage::atString("Registry1Network0"));
	root->m_Children.PushAndGrow(::rage::atString("Registry1Network1"));
	root->m_Children.PushAndGrow(::rage::atString("Registry1Network2"));
	root->m_Children.PushAndGrow(::rage::atString("Registry1Network3"));
	root->m_Children.PushAndGrow(::rage::atString("Registry2Network0"));
	root->m_Children.PushAndGrow(::rage::atString("Registry2Network1"));
	root->m_Children.PushAndGrow(::rage::atString("Registry2Network2"));
	root->m_Children.PushAndGrow(::rage::atString("Registry2Network3"));
	root->m_Children.PushAndGrow(::rage::atString("Registry3Network0"));
	root->m_Children.PushAndGrow(::rage::atString("Registry3Network1"));
	root->m_Children.PushAndGrow(::rage::atString("Registry3Network2"));
	root->m_Children.PushAndGrow(::rage::atString("Registry3Network3"));
	root->m_Parameters.Reserve(22);
	root->m_Parameters.Resize(22);
	root->m_Parameters[0].m_Target = "Blend 2";
	root->m_Parameters[0].m_Attribute = "Weight";
	root->m_Parameters[0].m_Parameter = "RagdollWeight";
	root->m_Parameters[0].m_Index = 0;
	root->m_Parameters[1].m_Target = "Frame";
	root->m_Parameters[1].m_Attribute = "Frame";
	root->m_Parameters[1].m_Parameter = "ExternallyDrivenDOFFrame";
	root->m_Parameters[1].m_Index = 0;
	root->m_Parameters[2].m_Target = "Mover Filter";
	root->m_Parameters[2].m_Attribute = "Filter";
	root->m_Parameters[2].m_Parameter = "MoverFilter";
	root->m_Parameters[2].m_Index = 0;
	root->m_Parameters[3].m_Target = "Lod Filter";
	root->m_Parameters[3].m_Attribute = "Filter";
	root->m_Parameters[3].m_Parameter = "LodFilter";
	root->m_Parameters[3].m_Index = 0;
	root->m_Parameters[4].m_Target = "Low Prio";
	root->m_Parameters[4].m_Attribute = "Weight";
	root->m_Parameters[4].m_Parameter = "Registry0NetworkWeight0";
	root->m_Parameters[4].m_Index = 0;
	root->m_Parameters[5].m_Target = "Low Prio";
	root->m_Parameters[5].m_Attribute = "Weight";
	root->m_Parameters[5].m_Parameter = "Registry0NetworkWeight1";
	root->m_Parameters[5].m_Index = 1;
	root->m_Parameters[6].m_Target = "Low Prio";
	root->m_Parameters[6].m_Attribute = "Weight";
	root->m_Parameters[6].m_Parameter = "Registry0NetworkWeight2";
	root->m_Parameters[6].m_Index = 2;
	root->m_Parameters[7].m_Target = "Low Prio";
	root->m_Parameters[7].m_Attribute = "Weight";
	root->m_Parameters[7].m_Parameter = "Registry0NetworkWeight3";
	root->m_Parameters[7].m_Index = 3;
	root->m_Parameters[8].m_Target = "Medium Prio";
	root->m_Parameters[8].m_Attribute = "Weight";
	root->m_Parameters[8].m_Parameter = "Registry0Weight";
	root->m_Parameters[8].m_Index = 0;
	root->m_Parameters[9].m_Target = "Medium Prio";
	root->m_Parameters[9].m_Attribute = "Weight";
	root->m_Parameters[9].m_Parameter = "Registry1NetworkWeight0";
	root->m_Parameters[9].m_Index = 1;
	root->m_Parameters[10].m_Target = "Medium Prio";
	root->m_Parameters[10].m_Attribute = "Weight";
	root->m_Parameters[10].m_Parameter = "Registry1NetworkWeight1";
	root->m_Parameters[10].m_Index = 2;
	root->m_Parameters[11].m_Target = "Medium Prio";
	root->m_Parameters[11].m_Attribute = "Weight";
	root->m_Parameters[11].m_Parameter = "Registry1NetworkWeight2";
	root->m_Parameters[11].m_Index = 3;
	root->m_Parameters[12].m_Target = "Medium Prio";
	root->m_Parameters[12].m_Attribute = "Weight";
	root->m_Parameters[12].m_Parameter = "Registry1NetworkWeight3";
	root->m_Parameters[12].m_Index = 4;
	root->m_Parameters[13].m_Target = "High Prio";
	root->m_Parameters[13].m_Attribute = "Weight";
	root->m_Parameters[13].m_Parameter = "Registry1Weight";
	root->m_Parameters[13].m_Index = 0;
	root->m_Parameters[14].m_Target = "High Prio";
	root->m_Parameters[14].m_Attribute = "Weight";
	root->m_Parameters[14].m_Parameter = "Registry2NetworkWeight0";
	root->m_Parameters[14].m_Index = 1;
	root->m_Parameters[15].m_Target = "High Prio";
	root->m_Parameters[15].m_Attribute = "Weight";
	root->m_Parameters[15].m_Parameter = "Registry2NetworkWeight1";
	root->m_Parameters[15].m_Index = 2;
	root->m_Parameters[16].m_Target = "High Prio";
	root->m_Parameters[16].m_Attribute = "Weight";
	root->m_Parameters[16].m_Parameter = "Registry2NetworkWeight2";
	root->m_Parameters[16].m_Index = 3;
	root->m_Parameters[17].m_Target = "High Prio";
	root->m_Parameters[17].m_Attribute = "Weight";
	root->m_Parameters[17].m_Parameter = "Registry2NetworkWeight3";
	root->m_Parameters[17].m_Index = 4;
	root->m_Parameters[18].m_Target = "Additive";
	root->m_Parameters[18].m_Attribute = "Weight";
	root->m_Parameters[18].m_Parameter = "Registry3NetworkWeight0";
	root->m_Parameters[18].m_Index = 0;
	root->m_Parameters[19].m_Target = "Additive";
	root->m_Parameters[19].m_Attribute = "Weight";
	root->m_Parameters[19].m_Parameter = "Registry3NetworkWeight1";
	root->m_Parameters[19].m_Index = 1;
	root->m_Parameters[20].m_Target = "Additive";
	root->m_Parameters[20].m_Attribute = "Weight";
	root->m_Parameters[20].m_Parameter = "Registry3NetworkWeight2";
	root->m_Parameters[20].m_Index = 2;
	root->m_Parameters[21].m_Target = "Additive";
	root->m_Parameters[21].m_Attribute = "Weight";
	root->m_Parameters[21].m_Parameter = "Registry3NetworkWeight3";
	root->m_Parameters[21].m_Index = 3;

	BlendNode* blend2 = rage_new BlendNode;
	network.m_Nodes.PushAndGrow(blend2);
	blend2->m_Name = "Blend 2";
	blend2->m_Child0 = "Merge 2";
	blend2->m_Child1 = "pose";
	blend2->m_Weight.m_Type = kF32AttributeParameter;
	blend2->m_Weight.m_Parameter = "RagdollWeight";

	MergeNode* merge2 = rage_new MergeNode;
	network.m_Nodes.PushAndGrow(merge2);
	merge2->m_Name = "Merge 2";
	merge2->m_Source0 = "Frame";
	merge2->m_Source1 = "Mover Filter";

	PoseNode* pose = rage_new PoseNode;
	network.m_Nodes.PushAndGrow(pose);
	pose->m_Name = "pose";
	pose->m_Normalize = true;

	FrameNode* frame = rage_new FrameNode;
	network.m_Nodes.PushAndGrow(frame);
	frame->m_Name = "Frame";
	frame->m_Owner.m_Type = kBoolAttributeValue;
	frame->m_Owner.m_Value = true;
	frame->m_Frame.m_Type = kFrameParameter;
	frame->m_Frame.m_Parameter = "ExternallyDrivenDOFFrame";

	FilterNode* moverFilter = rage_new FilterNode;
	network.m_Nodes.PushAndGrow(moverFilter);
	moverFilter->m_Name = "Mover Filter";
	moverFilter->m_Filter.m_Type = kFilterParameter;
	moverFilter->m_Filter.m_Parameter = "MoverFilter";
	moverFilter->m_Source = "Lod Filter";

	FilterNode* lodFilter = rage_new FilterNode;
	network.m_Nodes.PushAndGrow(lodFilter);
	lodFilter->m_Name = "Lod Filter";
	lodFilter->m_Filter.m_Type = kFilterParameter;
	lodFilter->m_Filter.m_Parameter = "LodFilter";
	lodFilter->m_Source = "Add";

	AddNode* add = rage_new AddNode;
	network.m_Nodes.PushAndGrow(add);
	add->m_Name = "Add";
	add->m_Child0 = "High Prio";
	add->m_Child1 = "Additive";
	add->m_Weight.m_Type = kF32AttributeValue;
	add->m_Weight.m_Value = 1.f;

	SubnetworkNode* registry3network0 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry3network0);
	registry3network0->m_Name = "Registry3Network0";

	SubnetworkNode* registry3network1 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry3network1);
	registry3network1->m_Name = "Registry3Network1";

	SubnetworkNode* registry3network2 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry3network2);
	registry3network2->m_Name = "Registry3Network2";

	SubnetworkNode* registry3network3 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry3network3);
	registry3network3->m_Name = "Registry3Network3";

	AddNwayNode* additive = rage_new AddNwayNode;
	network.m_Nodes.PushAndGrow(additive);
	additive->m_Name = "Additive";
	additive->m_Children.Reserve(4);
	additive->m_Children.Resize(4);
	additive->m_Children[0].m_Input = "Registry3Network0";
	additive->m_Children[0].m_Weight.m_Type = kF32AttributeParameter;
	additive->m_Children[0].m_Weight.m_Parameter = "Registry3NetworkWeight0";
	additive->m_Children[1].m_Input = "Registry3Network1";
	additive->m_Children[1].m_Weight.m_Type = kF32AttributeParameter;
	additive->m_Children[1].m_Weight.m_Parameter = "Registry3NetworkWeight1";
	additive->m_Children[2].m_Input = "Registry3Network2";
	additive->m_Children[2].m_Weight.m_Type = kF32AttributeParameter;
	additive->m_Children[2].m_Weight.m_Parameter = "Registry3NetworkWeight2";
	additive->m_Children[3].m_Input = "Registry3Network3";
	additive->m_Children[3].m_Weight.m_Type = kF32AttributeParameter;
	additive->m_Children[3].m_Weight.m_Parameter = "Registry3NetworkWeight3";

	SubnetworkNode* registry2network0 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry2network0);
	registry2network0->m_Name = "Registry2Network0";

	SubnetworkNode* registry2network1 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry2network1);
	registry2network1->m_Name = "Registry2Network1";

	SubnetworkNode* registry2network2 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry2network2);
	registry2network2->m_Name = "Registry2Network2";

	SubnetworkNode* registry2network3 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry2network3);
	registry2network3->m_Name = "Registry2Network3";

	BlendNwayNode* highPrio = rage_new BlendNwayNode;
	network.m_Nodes.PushAndGrow(highPrio);
	highPrio->m_Name = "High Prio";
	highPrio->m_Children.Reserve(5);
	highPrio->m_Children.Resize(5);
	highPrio->m_Children[0].m_Input = "Medium Prio";
	highPrio->m_Children[0].m_Weight.m_Type = kF32AttributeParameter;
	highPrio->m_Children[0].m_Weight.m_Parameter = "Registry1Weight";
	highPrio->m_Children[1].m_Input = "Registry2Network0";
	highPrio->m_Children[1].m_Weight.m_Type = kF32AttributeParameter;
	highPrio->m_Children[1].m_Weight.m_Parameter = "Registry2NetworkWeight0";
	highPrio->m_Children[2].m_Input = "Registry2Network1";
	highPrio->m_Children[2].m_Weight.m_Type = kF32AttributeParameter;
	highPrio->m_Children[2].m_Weight.m_Parameter = "Registry2NetworkWeight1";
	highPrio->m_Children[3].m_Input = "Registry2Network2";
	highPrio->m_Children[3].m_Weight.m_Type = kF32AttributeParameter;
	highPrio->m_Children[3].m_Weight.m_Parameter = "Registry2NetworkWeight2";
	highPrio->m_Children[4].m_Input = "Registry2Network3";
	highPrio->m_Children[4].m_Weight.m_Type = kF32AttributeParameter;
	highPrio->m_Children[4].m_Weight.m_Parameter = "Registry2NetworkWeight3";

	SubnetworkNode* registry1network0 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry1network0);
	registry1network0->m_Name = "Registry1Network0";

	SubnetworkNode* registry1network1 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry1network1);
	registry1network1->m_Name = "Registry1Network1";

	SubnetworkNode* registry1network2 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry1network2);
	registry1network2->m_Name = "Registry1Network2";

	SubnetworkNode* registry1network3 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry1network3);
	registry1network3->m_Name = "Registry1Network3";

	BlendNwayNode* medPrio = rage_new BlendNwayNode;
	network.m_Nodes.PushAndGrow(medPrio);
	medPrio->m_Name = "Medium Prio";
	medPrio->m_Children.Reserve(5);
	medPrio->m_Children.Resize(5);
	medPrio->m_Children[0].m_Input = "Low Prio";
	medPrio->m_Children[0].m_Weight.m_Type = kF32AttributeParameter;
	medPrio->m_Children[0].m_Weight.m_Parameter = "Registry0Weight";
	medPrio->m_Children[1].m_Input = "Registry1Network0";
	medPrio->m_Children[1].m_Weight.m_Type = kF32AttributeParameter;
	medPrio->m_Children[1].m_Weight.m_Parameter = "Registry1NetworkWeight0";
	medPrio->m_Children[2].m_Input = "Registry1Network1";
	medPrio->m_Children[2].m_Weight.m_Type = kF32AttributeParameter;
	medPrio->m_Children[2].m_Weight.m_Parameter = "Registry1NetworkWeight1";
	medPrio->m_Children[3].m_Input = "Registry1Network2";
	medPrio->m_Children[3].m_Weight.m_Type = kF32AttributeParameter;
	medPrio->m_Children[3].m_Weight.m_Parameter = "Registry1NetworkWeight2";
	medPrio->m_Children[4].m_Input = "Registry1Network3";
	medPrio->m_Children[4].m_Weight.m_Type = kF32AttributeParameter;
	medPrio->m_Children[4].m_Weight.m_Parameter = "Registry1NetworkWeight3";

	SubnetworkNode* registry0network0 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry0network0);
	registry0network0->m_Name = "Registry0Network0";

	SubnetworkNode* registry0network1 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry0network1);
	registry0network1->m_Name = "Registry0Network1";

	SubnetworkNode* registry0network2 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry0network2);
	registry0network2->m_Name = "Registry0Network2";

	SubnetworkNode* registry0network3 = rage_new SubnetworkNode;
	network.m_Nodes.PushAndGrow(registry0network3);
	registry0network3->m_Name = "Registry0Network3";

	BlendNwayNode* lowPrio = rage_new BlendNwayNode;
	network.m_Nodes.PushAndGrow(lowPrio);
	lowPrio->m_Name = "Low Prio";
	lowPrio->m_Children.Reserve(4);
	lowPrio->m_Children.Resize(4);
	lowPrio->m_Children[0].m_Input = "Registry0Network0";
	lowPrio->m_Children[0].m_Weight.m_Type = kF32AttributeParameter;
	lowPrio->m_Children[0].m_Weight.m_Parameter = "Registry0NetworkWeight0";
	lowPrio->m_Children[1].m_Input = "Registry0Network1";
	lowPrio->m_Children[1].m_Weight.m_Type = kF32AttributeParameter;
	lowPrio->m_Children[1].m_Weight.m_Parameter = "Registry0NetworkWeight1";
	lowPrio->m_Children[2].m_Input = "Registry0Network2";
	lowPrio->m_Children[2].m_Weight.m_Type = kF32AttributeParameter;
	lowPrio->m_Children[2].m_Weight.m_Parameter = "Registry0NetworkWeight2";
	lowPrio->m_Children[3].m_Input = "Registry0Network3";
	lowPrio->m_Children[3].m_Weight.m_Type = kF32AttributeParameter;
	lowPrio->m_Children[3].m_Weight.m_Parameter = "Registry0NetworkWeight3";

	fiStream* stream = fiStream::Create("main.xml");
	Assert(stream);
	PARSER.SaveObject(stream, &network);
	stream->Close();
}

void TestDataGenerator::BuildMinigameBenchpressNetwork()
{
	Network network;
	network.m_Requests.PushAndGrow(::rage::atString("IdleRequest"));
	network.m_Requests.PushAndGrow(::rage::atString("OutroRequest"));
	network.m_Requests.PushAndGrow(::rage::atString("SuccessRequest"));
	network.m_Requests.PushAndGrow(::rage::atString("FailureRequest"));

	network.m_Root = "Root";

	StateMachineNode* root = rage_new StateMachineNode;
	network.m_Nodes.PushAndGrow(root);
	root->m_OnEnterEvent.m_Enabled = false;
	root->m_OnExitEvent.m_Enabled = false;
	root->m_Name = "Root";
	root->m_Initial = "Intro";
	root->m_Children.PushAndGrow(::rage::atString("Intro"));
	root->m_Children.PushAndGrow(::rage::atString("intro_loop"));
	root->m_Children.PushAndGrow(::rage::atString("lift_weight"));
	root->m_Children.PushAndGrow(::rage::atString("resting"));
	root->m_Children.PushAndGrow(::rage::atString("lifting"));
	root->m_Children.PushAndGrow(::rage::atString("Success"));
	root->m_Children.PushAndGrow(::rage::atString("Failure"));
	root->m_Transitions.Reserve(9);
	root->m_Transitions.Resize(9);
	root->m_Transitions[0].m_From = "Intro";
	root->m_Transitions[0].m_To = "intro_loop";
	root->m_Transitions[0].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[0].m_Duration = 0.25f;
	root->m_Transitions[0].m_ReEvaluate = false;
	root->m_Transitions[0].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[0].m_Conditions.PushAndGrow(rage_new AtEvent);
	((AtEvent*)root->m_Transitions[0].m_Conditions[0])->m_Event = "IntroFinished";
	root->m_Transitions[1].m_From = "intro_loop";
	root->m_Transitions[1].m_To = "lift_weight";
	root->m_Transitions[1].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[1].m_Duration = 0.5f;
	root->m_Transitions[1].m_ReEvaluate = false;
	root->m_Transitions[1].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[1].m_Conditions.PushAndGrow(rage_new OnRequest);
	((OnRequest*)root->m_Transitions[1].m_Conditions[0])->m_Parameter = "IdleRequest";
	((OnRequest*)root->m_Transitions[1].m_Conditions[0])->m_NotSet = false;
	root->m_Transitions[2].m_From = "lift_weight";
	root->m_Transitions[2].m_To = "resting";
	root->m_Transitions[2].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[2].m_Duration = 0.5f;
	root->m_Transitions[2].m_ReEvaluate = false;
	root->m_Transitions[2].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[2].m_Conditions.PushAndGrow(rage_new AtEvent);
	((AtEvent*)root->m_Transitions[2].m_Conditions[0])->m_Event = "weightLifted";
	root->m_Transitions[3].m_From = "resting";
	root->m_Transitions[3].m_To = "lifting";
	root->m_Transitions[3].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[3].m_Duration = 0.3f;
	root->m_Transitions[3].m_ReEvaluate = false;
	root->m_Transitions[3].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[3].m_Conditions.PushAndGrow(rage_new GreaterThan);
	((GreaterThan*)root->m_Transitions[3].m_Conditions[0])->m_Parameter = "Speed";
	((GreaterThan*)root->m_Transitions[3].m_Conditions[0])->m_Trigger = 0.05f;
	root->m_Transitions[4].m_From = "lifting";
	root->m_Transitions[4].m_To = "resting";
	root->m_Transitions[4].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[4].m_Duration = 0.6f;
	root->m_Transitions[4].m_ReEvaluate = false;
	root->m_Transitions[4].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[4].m_Conditions.PushAndGrow(rage_new LessThanEqual);
	((LessThanEqual*)root->m_Transitions[4].m_Conditions[0])->m_Parameter = "Speed";
	((LessThanEqual*)root->m_Transitions[4].m_Conditions[0])->m_Trigger = 0.05f;
	root->m_Transitions[5].m_From = "resting";
	root->m_Transitions[5].m_To = "Success";
	root->m_Transitions[5].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[5].m_Duration = 0.5f;
	root->m_Transitions[5].m_ReEvaluate = false;
	root->m_Transitions[5].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[5].m_Conditions.PushAndGrow(rage_new OnRequest);
	((OnRequest*)root->m_Transitions[5].m_Conditions[0])->m_Parameter = "SuccessRequest";
	((OnRequest*)root->m_Transitions[5].m_Conditions[0])->m_NotSet = false;
	root->m_Transitions[6].m_From = "resting";
	root->m_Transitions[6].m_To = "Failure";
	root->m_Transitions[6].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[6].m_Duration = 0.5f;
	root->m_Transitions[6].m_ReEvaluate = false;
	root->m_Transitions[6].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[6].m_Conditions.PushAndGrow(rage_new OnRequest);
	((OnRequest*)root->m_Transitions[6].m_Conditions[0])->m_Parameter = "FailureRequest";
	((OnRequest*)root->m_Transitions[6].m_Conditions[0])->m_NotSet = false;
	root->m_Transitions[7].m_From = "lifting";
	root->m_Transitions[7].m_To = "Success";
	root->m_Transitions[7].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[7].m_Duration = 0.5f;
	root->m_Transitions[7].m_ReEvaluate = false;
	root->m_Transitions[7].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[7].m_Conditions.PushAndGrow(rage_new OnRequest);
	((OnRequest*)root->m_Transitions[7].m_Conditions[0])->m_Parameter = "SuccessRequest";
	((OnRequest*)root->m_Transitions[7].m_Conditions[0])->m_NotSet = false;
	root->m_Transitions[8].m_From = "lifting";
	root->m_Transitions[8].m_To = "Failure";
	root->m_Transitions[8].m_Modifier = MODIFIER_LINEAR;
	root->m_Transitions[8].m_Duration = 0.5f;
	root->m_Transitions[8].m_ReEvaluate = false;
	root->m_Transitions[8].m_Filter.m_Type = kFilterIgnored;
	root->m_Transitions[8].m_Conditions.PushAndGrow(rage_new OnRequest);
	((OnRequest*)root->m_Transitions[8].m_Conditions[0])->m_Parameter = "FailureRequest";
	((OnRequest*)root->m_Transitions[8].m_Conditions[0])->m_NotSet = false;

	MotionTreeNode* intro = rage_new MotionTreeNode;
	network.m_Nodes.PushAndGrow(intro);
	intro->m_Name = "Intro";
	intro->m_OnEnterEvent.m_Enabled = false;
	intro->m_OnExitEvent.m_Enabled = false;
	intro->m_Initial = "Intro/Intro";
	intro->m_Children.PushAndGrow(::rage::atString("Intro/Intro"));
	intro->m_Events.Reserve(1);
	intro->m_Events.Resize(1);
	intro->m_Events[0].m_Event = "IntroFinished";
	intro->m_Events[0].m_Target = "Intro/Intro";
	intro->m_Events[0].m_Type = "ClipEnded";

	// Intro
	{
		ClipNode* clip = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(clip);
		clip->m_Name = "Intro/Intro";
		clip->m_Clip.m_Type = kClipAttributeFilename;
		clip->m_Clip.m_Context = kClipContextClipDictionary;
		clip->m_Clip.m_BankName = "benchpress_default";
		clip->m_Clip.m_ClipName = "bench_press_intro";
		clip->m_Phase.m_Type = kF32AttributeValue;
		clip->m_Phase.m_Value = 0.f;
		clip->m_Rate.m_Type = kF32AttributeValue;
		clip->m_Rate.m_Value = 1.f;
		clip->m_Delta.m_Type = kF32AttributeValue;
		clip->m_Delta.m_Value = 0.f;
		clip->m_Looped.m_Type = kBoolAttributeValue;
		clip->m_Looped.m_Value = false;
		//		clip->m_ClipEnded.m_Enabled = true;
		//		clip->m_ClipEnded.m_Id = "IntroFinished";
	}

	MotionTreeNode* introLoop = rage_new MotionTreeNode;
	network.m_Nodes.PushAndGrow(introLoop);
	introLoop->m_Name = "intro_loop";
	introLoop->m_OnEnterEvent.m_Enabled = false;
	introLoop->m_OnExitEvent.m_Enabled = false;
	introLoop->m_Initial = "intro_loop/Clip";
	introLoop->m_Children.PushAndGrow(::rage::atString("intro_loop/Clip"));

	//intro_clip
	{
		ClipNode* clip = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(clip);
		clip->m_Name = "intro_loop/Clip";
		clip->m_Clip.m_Type = kClipAttributeFilename;
		clip->m_Clip.m_Context = kClipContextClipDictionary;
		clip->m_Clip.m_BankName = "benchpress_default";
		clip->m_Clip.m_ClipName = "bench_press_start_loop";
		clip->m_Phase.m_Type = kF32AttributeValue;
		clip->m_Phase.m_Value = 0.f;
		clip->m_Rate.m_Type = kF32AttributeValue;
		clip->m_Rate.m_Value = 1.f;
		clip->m_Delta.m_Type = kF32AttributeValue;
		clip->m_Delta.m_Value = 0.f;
		clip->m_Looped.m_Type = kBoolAttributeValue;
		clip->m_Looped.m_Value = true;
	}


	MotionTreeNode* liftWeight = rage_new MotionTreeNode;
	network.m_Nodes.PushAndGrow(liftWeight);
	liftWeight->m_Name = "lift_weight";
	liftWeight->m_OnEnterEvent.m_Enabled = true;
	liftWeight->m_OnEnterEvent.m_Id = "PickupWeight";
	liftWeight->m_OnExitEvent.m_Enabled = false;
	liftWeight->m_Initial = "lift_weight/Clip";
	liftWeight->m_Children.PushAndGrow(::rage::atString("lift_weight/Clip"));
	liftWeight->m_Events.Reserve(1);
	liftWeight->m_Events.Resize(1);
	liftWeight->m_Events[0].m_Target = "lift_weight/Clip";
	liftWeight->m_Events[0].m_Type = "ClipEnded";
	liftWeight->m_Events[0].m_Event = "weightLifted";

	// lift_weight
	{
		ClipNode* clip = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(clip);
		clip->m_Name = "lift_weight/Clip";
		clip->m_Clip.m_Type = kClipAttributeFilename;
		clip->m_Clip.m_Context = kClipContextClipDictionary;
		clip->m_Clip.m_BankName = "benchpress_streamed";
		clip->m_Clip.m_ClipName = "bench_press_lift_weight";
		clip->m_Phase.m_Type = kF32AttributeValue;
		clip->m_Phase.m_Value = 0.f;
		clip->m_Rate.m_Type = kF32AttributeValue;
		clip->m_Rate.m_Value = 1.f;
		clip->m_Delta.m_Type = kF32AttributeValue;
		clip->m_Delta.m_Value = 0.f;
		clip->m_Looped.m_Type = kBoolAttributeValue;
		clip->m_Looped.m_Value = false;
		//		clip->m_ClipEnded.m_Enabled = true;
		//		clip->m_ClipEnded.m_Id = "weightLifted";
	}

	MotionTreeNode* resting = rage_new MotionTreeNode;
	network.m_Nodes.PushAndGrow(resting);
	resting->m_Name = "resting";
	resting->m_OnEnterEvent.m_Enabled = true;
	resting->m_OnEnterEvent.m_Id = "Idle";
	resting->m_OnExitEvent.m_Enabled = false;
	resting->m_Initial = "resting/BlendN";
	resting->m_Children.PushAndGrow(::rage::atString("resting/BlendN"));
	resting->m_Children.PushAndGrow(::rage::atString("resting/Struggling"));
	resting->m_Children.PushAndGrow(::rage::atString("resting/Normal"));
	resting->m_Children.PushAndGrow(::rage::atString("resting/Strong"));
	resting->m_Operators.Reserve(3);
	resting->m_Operators.Resize(3);
	resting->m_Operators[0].m_Target = "resting/BlendN";
	resting->m_Operators[0].m_Attribute = "Weight";
	resting->m_Operators[0].m_Index = 0;
	resting->m_Operators[0].m_Operations.PushAndGrow(rage_new FindAndPushValueOperator);
	((FindAndPushValueOperator*)resting->m_Operators[0].m_Operations[0])->m_Parameter = "LiftingRate";
	resting->m_Operators[0].m_Operations.PushAndGrow(rage_new CurveOperator);
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_MinClamp = 0.f;
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_MaxClamp = 1.f;
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_Keyframes.Reserve(2);
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_Keyframes.Resize(2);
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_Keyframes[0].m_Key = 0.5f;
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_Keyframes[0].m_M = -1.98228835329012f;
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_Keyframes[0].m_B = 0.993103448275863f;
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_Keyframes[1].m_Key = 1.f;
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_Keyframes[1].m_M = -0.00391854326160179f;
	((CurveOperator*)resting->m_Operators[0].m_Operations[1])->m_Keyframes[1].m_B = 0.00391854326160179f;
	resting->m_Operators[1].m_Target = "resting/BlendN";
	resting->m_Operators[1].m_Attribute = "Weight";
	resting->m_Operators[1].m_Index = 1;
	resting->m_Operators[1].m_Operations.PushAndGrow(rage_new FindAndPushValueOperator);
	((FindAndPushValueOperator*)resting->m_Operators[1].m_Operations[0])->m_Parameter = "LiftingRate";
	resting->m_Operators[1].m_Operations.PushAndGrow(rage_new CurveOperator);
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_MinClamp = 0.f;
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_MaxClamp = 1.f;
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_Keyframes.Reserve(2);
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_Keyframes.Resize(2);
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_Keyframes[0].m_Key = 0.5f;
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_Keyframes[0].m_M = 1.99702199153746f;
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_Keyframes[0].m_B = 0.f;
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_Keyframes[1].m_Key = 1.f;
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_Keyframes[1].m_M = -1.99702199153746f;
	((CurveOperator*)resting->m_Operators[1].m_Operations[1])->m_Keyframes[1].m_B = 1.99702199153746f;
	resting->m_Operators[2].m_Target = "resting/BlendN";
	resting->m_Operators[2].m_Attribute = "Weight";
	resting->m_Operators[2].m_Index = 2;
	resting->m_Operators[2].m_Operations.PushAndGrow(rage_new FindAndPushValueOperator);
	((FindAndPushValueOperator*)resting->m_Operators[2].m_Operations[0])->m_Parameter = "LiftingRate";
	resting->m_Operators[2].m_Operations.PushAndGrow(rage_new CurveOperator);
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_MinClamp = 0.f;
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_MaxClamp = 1.f;
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_Keyframes.Reserve(2);
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_Keyframes.Resize(2);
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_Keyframes[0].m_Key = 0.5f;
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_Keyframes[0].m_M = 0.00393460286513339f;
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_Keyframes[0].m_B = 0.f;
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_Keyframes[1].m_Key = 1.f;
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_Keyframes[1].m_M = 1.9810987875142f;
	((CurveOperator*)resting->m_Operators[2].m_Operations[1])->m_Keyframes[1].m_B = -0.984547063376271f;

	// resting
	{
		BlendNwayNode* blend = rage_new BlendNwayNode;
		network.m_Nodes.PushAndGrow(blend);
		blend->m_Name = "resting/BlendN";
		blend->m_Children.Reserve(3);
		blend->m_Children.Resize(3);
		blend->m_Children[0].m_Input = "resting/Struggling";
		blend->m_Children[0].m_Weight.m_Type = kF32AttributeIgnored;		// Special case that means we should search for a node
		blend->m_Children[0].m_Weight.m_Parameter = "resting/Struggling-Curve";
		blend->m_Children[1].m_Input = "resting/Normal";
		blend->m_Children[1].m_Weight.m_Type = kF32AttributeIgnored;
		blend->m_Children[1].m_Weight.m_Parameter = "resting/Normal-Curve";
		blend->m_Children[2].m_Input = "resting/Strong";
		blend->m_Children[2].m_Weight.m_Type = kF32AttributeIgnored;
		blend->m_Children[2].m_Weight.m_Parameter = "resting/Strong-Curve";
		blend->m_Synchronizer.m_Type = ST_PHASE;
		blend->m_ZeroDestination.m_Value = false;

		ClipNode* struggling = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(struggling);
		struggling->m_Name = "resting/Struggling";
		struggling->m_Clip.m_Type = kClipAttributeFilename;
		struggling->m_Clip.m_Context = kClipContextClipDictionary;
		struggling->m_Clip.m_BankName = "benchpress_streamed";
		struggling->m_Clip.m_ClipName = "bench_press_difficult_idle";
		struggling->m_Phase.m_Type = kF32AttributeValue;
		struggling->m_Phase.m_Value = 0.f;
		struggling->m_Rate.m_Type = kF32AttributeValue;
		struggling->m_Rate.m_Value = 1.f;
		struggling->m_Delta.m_Type = kF32AttributeValue;
		struggling->m_Delta.m_Value = 0.f;
		struggling->m_Looped.m_Type = kBoolAttributeValue;
		struggling->m_Looped.m_Value = true;

		ClipNode* normal = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(normal);
		normal->m_Name = "resting/Normal";
		normal->m_Clip.m_Type = kClipAttributeFilename;
		normal->m_Clip.m_Context = kClipContextClipDictionary;
		normal->m_Clip.m_BankName = "benchpress_streamed";
		normal->m_Clip.m_ClipName = "bench_press_medium_idle";
		normal->m_Phase.m_Type = kF32AttributeValue;
		normal->m_Phase.m_Value = 0.f;
		normal->m_Rate.m_Type = kF32AttributeValue;
		normal->m_Rate.m_Value = 1.f;
		normal->m_Delta.m_Type = kF32AttributeValue;
		normal->m_Delta.m_Value = 0.f;
		normal->m_Looped.m_Type = kBoolAttributeValue;
		normal->m_Looped.m_Value = true;

		ClipNode* strong = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(strong);
		strong->m_Name = "resting/Strong";
		strong->m_Clip.m_Type = kClipAttributeFilename;
		strong->m_Clip.m_Context = kClipContextClipDictionary;
		strong->m_Clip.m_BankName = "benchpress_streamed";
		strong->m_Clip.m_ClipName = "bench_press_easy_idle";
		strong->m_Phase.m_Type = kF32AttributeValue;
		strong->m_Phase.m_Value = 0.f;
		strong->m_Rate.m_Type = kF32AttributeValue;
		strong->m_Rate.m_Value = 1.f;
		strong->m_Delta.m_Type = kF32AttributeValue;
		strong->m_Delta.m_Value = 0.f;
		strong->m_Looped.m_Type = kBoolAttributeValue;
		strong->m_Looped.m_Value = true;
	}

	MotionTreeNode* lifting = rage_new MotionTreeNode;
	network.m_Nodes.PushAndGrow(lifting);
	lifting->m_Name = "lifting";
	lifting->m_OnEnterEvent.m_Enabled = true;
	lifting->m_OnEnterEvent.m_Id = "Lifting";
	lifting->m_OnExitEvent.m_Enabled = false;
	lifting->m_Initial = "lifting/BlendN";
	lifting->m_Children.PushAndGrow(::rage::atString("lifting/BlendN"));
	lifting->m_Children.PushAndGrow(::rage::atString("lifting/Struggling"));
	lifting->m_Children.PushAndGrow(::rage::atString("lifting/Normal"));
	lifting->m_Children.PushAndGrow(::rage::atString("lifting/Strong"));
	lifting->m_Parameters.Reserve(3);
	lifting->m_Parameters.Resize(3);
	lifting->m_Parameters[0].m_Target = "lifting/Struggling";
	lifting->m_Parameters[0].m_Attribute = "Rate";
	lifting->m_Parameters[0].m_Parameter = "Speed";
	lifting->m_Parameters[1].m_Target = "lifting/Normal";
	lifting->m_Parameters[1].m_Attribute = "Rate";
	lifting->m_Parameters[1].m_Parameter = "Speed";
	lifting->m_Parameters[2].m_Target = "lifting/Strong";
	lifting->m_Parameters[2].m_Attribute = "Rate";
	lifting->m_Parameters[2].m_Parameter = "Speed";
	lifting->m_Outputs.Reserve(3);
	lifting->m_Outputs.Resize(3);
	lifting->m_Outputs[0].m_Target = "lifting/Struggling";
	lifting->m_Outputs[0].m_Attribute = "Phase";
	lifting->m_Outputs[0].m_Index = 0;
	lifting->m_Outputs[0].m_Parameter = "height";
	lifting->m_Outputs[1].m_Target = "lifting/Normal";
	lifting->m_Outputs[1].m_Attribute = "Phase";
	lifting->m_Outputs[1].m_Index = 0;
	lifting->m_Outputs[1].m_Parameter = "height";
	lifting->m_Outputs[2].m_Target = "lifting/Strong";
	lifting->m_Outputs[2].m_Attribute = "Phase";
	lifting->m_Outputs[2].m_Index = 0;
	lifting->m_Outputs[2].m_Parameter = "height";
	lifting->m_Operators.Reserve(3);
	lifting->m_Operators.Resize(3);
	lifting->m_Operators[0].m_Target = "lifting/BlendN";
	lifting->m_Operators[0].m_Attribute = "Weight";
	lifting->m_Operators[0].m_Index = 0;
	lifting->m_Operators[0].m_Operations.PushAndGrow(rage_new FindAndPushValueOperator);
	((FindAndPushValueOperator*)lifting->m_Operators[0].m_Operations[0])->m_Parameter = "LiftingRate";
	lifting->m_Operators[0].m_Operations.PushAndGrow(rage_new CurveOperator);
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_MinClamp = 0.f;
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_MaxClamp = 1.f;
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_Keyframes.Reserve(2);
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_Keyframes.Resize(2);
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_Keyframes[0].m_Key = 0.5f;
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_Keyframes[0].m_M = -1.98319f;
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_Keyframes[0].m_B = 0.9965f;
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_Keyframes[1].m_Key = 1.f;
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_Keyframes[1].m_M = 0.0026f;
	((CurveOperator*)lifting->m_Operators[0].m_Operations[1])->m_Keyframes[1].m_B = 0.00767f;
	lifting->m_Operators[1].m_Target = "lifting/BlendN";
	lifting->m_Operators[1].m_Attribute = "Weight";
	lifting->m_Operators[1].m_Index = 1;
	lifting->m_Operators[1].m_Operations.PushAndGrow(rage_new FindAndPushValueOperator);
	((FindAndPushValueOperator*)lifting->m_Operators[1].m_Operations[0])->m_Parameter = "LiftingRate";
	lifting->m_Operators[1].m_Operations.PushAndGrow(rage_new CurveOperator);
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_MinClamp = 0.f;
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_MaxClamp = 1.f;
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_Keyframes.Reserve(2);
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_Keyframes.Resize(2);
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_Keyframes[0].m_Key = 0.5f;
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_Keyframes[0].m_M = 1.9869f;
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_Keyframes[0].m_B = 0.0103f;
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_Keyframes[1].m_Key = 1.f;
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_Keyframes[1].m_M = -1.940100f;
	((CurveOperator*)lifting->m_Operators[1].m_Operations[1])->m_Keyframes[1].m_B = 1.96834f;
	lifting->m_Operators[2].m_Target = "lifting/BlendN";
	lifting->m_Operators[2].m_Attribute = "Weight";
	lifting->m_Operators[2].m_Index = 2;
	lifting->m_Operators[2].m_Operations.PushAndGrow(rage_new FindAndPushValueOperator);
	((FindAndPushValueOperator*)lifting->m_Operators[2].m_Operations[0])->m_Parameter = "LiftingRate";
	lifting->m_Operators[2].m_Operations.PushAndGrow(rage_new CurveOperator);
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_MinClamp = 0.f;
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_MaxClamp = 1.f;
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_Keyframes.Reserve(2);
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_Keyframes.Resize(2);
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_Keyframes[0].m_Key = 0.5f;
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_Keyframes[0].m_M = 0.00423165737864247f;
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_Keyframes[0].m_B = 0.0103448275862073f;
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_Keyframes[1].m_Key = 1.f;
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_Keyframes[1].m_M = 1.96019859719494f;
	((CurveOperator*)lifting->m_Operators[2].m_Operations[1])->m_Keyframes[1].m_B = -0.963646873057012f;

	// lifting
	{
		BlendNwayNode* blend = rage_new BlendNwayNode;
		network.m_Nodes.PushAndGrow(blend);
		blend->m_Name = "lifting/BlendN";
		blend->m_Children.Reserve(3);
		blend->m_Children.Resize(3);
		blend->m_Children[0].m_Input = "lifting/Struggling";
		blend->m_Children[0].m_Weight.m_Type = kF32AttributeIgnored;
		blend->m_Children[0].m_Weight.m_Parameter = "lifting/Struggling-Curve";
		blend->m_Children[1].m_Input = "lifting/Normal";
		blend->m_Children[1].m_Weight.m_Type = kF32AttributeIgnored;
		blend->m_Children[1].m_Weight.m_Parameter = "lifting/Normal-Curve";
		blend->m_Children[2].m_Input = "lifting/Strong";
		blend->m_Children[2].m_Weight.m_Type = kF32AttributeIgnored;
		blend->m_Children[2].m_Weight.m_Parameter = "lifting/Strong-Curve";
		blend->m_Synchronizer.m_Type = ST_TAG;
		// "AEF_LOOPSTART_1" "AEF_LOOPSTOP_1"
		blend->m_Synchronizer.m_Tag = (1<<0) | (1<<1);
		blend->m_ZeroDestination.m_Value = false;

		ClipNode* struggling = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(struggling);
		struggling->m_Name = "lifting/Struggling";
		struggling->m_Clip.m_Type = kClipAttributeFilename;
		struggling->m_Clip.m_Context = kClipContextClipDictionary;
		struggling->m_Clip.m_BankName = "benchpress_streamed";
		struggling->m_Clip.m_ClipName = "bench_press_difficult_lift";
		struggling->m_Phase.m_Type = kF32AttributeValue;
		struggling->m_Phase.m_Value = 0.5f;
		//		struggling->m_Phase.m_Output = "height";
		struggling->m_Rate.m_Type = kF32AttributeParameter;
		struggling->m_Rate.m_Parameter = "Speed";
		struggling->m_Delta.m_Type = kF32AttributeValue;
		struggling->m_Delta.m_Value = 0.f;
		struggling->m_Looped.m_Type = kBoolAttributeValue;
		struggling->m_Looped.m_Value = true;

		ClipNode* normal = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(normal);
		normal->m_Name = "lifting/Normal";
		normal->m_Clip.m_Type = kClipAttributeFilename;
		normal->m_Clip.m_Context = kClipContextClipDictionary;
		normal->m_Clip.m_BankName = "benchpress_streamed";
		normal->m_Clip.m_ClipName = "bench_press_medium_lift";
		normal->m_Phase.m_Type = kF32AttributeValue;
		normal->m_Phase.m_Value = 0.5f;
		//		normal->m_Phase.m_Output = "height";
		normal->m_Rate.m_Type = kF32AttributeParameter;
		normal->m_Rate.m_Parameter = "Speed";
		normal->m_Delta.m_Type = kF32AttributeValue;
		normal->m_Delta.m_Value = 0.f;
		normal->m_Looped.m_Type = kBoolAttributeValue;
		normal->m_Looped.m_Value = true;

		ClipNode* strong = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(strong);
		strong->m_Name = "lifting/Strong";
		strong->m_Clip.m_Type = kClipAttributeFilename;
		strong->m_Clip.m_Context = kClipContextClipDictionary;
		strong->m_Clip.m_BankName = "benchpress_streamed";
		strong->m_Clip.m_ClipName = "bench_press_easy_lift";
		strong->m_Phase.m_Type = kF32AttributeValue;
		strong->m_Phase.m_Value = 0.5f;
		//		strong->m_Phase.m_Output = "height";
		strong->m_Rate.m_Type = kF32AttributeParameter;
		strong->m_Rate.m_Parameter = "Speed";
		strong->m_Delta.m_Type = kF32AttributeValue;
		strong->m_Delta.m_Value = 0.f;
		strong->m_Looped.m_Type = kBoolAttributeValue;
		strong->m_Looped.m_Value = true;
	}

	MotionTreeNode* success = rage_new MotionTreeNode;
	network.m_Nodes.PushAndGrow(success);
	success->m_Name = "Success";
	success->m_OnEnterEvent.m_Enabled = true;
	success->m_OnEnterEvent.m_Id = "Success";
	success->m_OnExitEvent.m_Enabled = false;
	success->m_Initial = "Success/Clip";
	success->m_Children.PushAndGrow(::rage::atString("Success/Clip"));
	success->m_Events.Reserve(1);
	success->m_Events.Resize(1);
	success->m_Events[0].m_Target = "Success/Clip";
	success->m_Events[0].m_Type = "ClipEnded";
	success->m_Events[0].m_Event = "Exit";

	// Success
	{
		ClipNode* clip = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(clip);
		clip->m_Name = "Success/Clip";
		clip->m_Clip.m_Type = kClipAttributeFilename;
		clip->m_Clip.m_Context = kClipContextClipDictionary;
		clip->m_Clip.m_BankName = "benchpress_streamed";
		clip->m_Clip.m_ClipName = "bench_press_success_outro";
		clip->m_Phase.m_Type = kF32AttributeValue;
		clip->m_Phase.m_Value = 0.f;
		clip->m_Rate.m_Type = kF32AttributeValue;
		clip->m_Rate.m_Value = 1.f;
		clip->m_Delta.m_Type = kF32AttributeValue;
		clip->m_Delta.m_Value = 0.f;
		clip->m_Looped.m_Type = kBoolAttributeValue;
		clip->m_Looped.m_Value = false;
		//		clip->m_ClipEnded.m_Enabled = true;
		//		clip->m_ClipEnded.m_Id = "Exit";

	}

	MotionTreeNode* failure = rage_new MotionTreeNode;
	network.m_Nodes.PushAndGrow(failure);
	failure->m_Name = "Failure";
	failure->m_OnEnterEvent.m_Enabled = true;
	failure->m_OnEnterEvent.m_Id = "Failure";
	failure->m_OnExitEvent.m_Enabled = false;
	failure->m_Initial = "Failure/Clip";
	failure->m_Children.PushAndGrow(::rage::atString("Failure/Clip"));
	failure->m_Events.Reserve(1);
	failure->m_Events.Resize(1);
	failure->m_Events[0].m_Target = "Failure/Clip";
	failure->m_Events[0].m_Type = "ClipEnded";
	failure->m_Events[0].m_Event = "Exit";

	// Failure
	{
		ClipNode* clip = rage_new ClipNode;
		network.m_Nodes.PushAndGrow(clip);
		clip->m_Name = "Failure/Clip";
		clip->m_Clip.m_Type = kClipAttributeFilename;
		clip->m_Clip.m_Context = kClipContextClipDictionary;
		clip->m_Clip.m_BankName = "benchpress_streamed";
		clip->m_Clip.m_ClipName = "bench_press_fail_choke_outro";
		clip->m_Phase.m_Type = kF32AttributeValue;
		clip->m_Phase.m_Value = 0.f;
		clip->m_Rate.m_Type = kF32AttributeValue;
		clip->m_Rate.m_Value = 1.f;
		clip->m_Delta.m_Type = kF32AttributeValue;
		clip->m_Delta.m_Value = 0.f;
		clip->m_Looped.m_Type = kBoolAttributeValue;
		clip->m_Looped.m_Value = false;
		//		clip->m_ClipEnded.m_Enabled = true;
		//		clip->m_ClipEnded.m_Id = "Exit";
	}

	fiStream* stream = fiStream::Create("MiniGame_BenchPress.xml");
	Assert(stream);
	PARSER.SaveObject(stream, &network);
	stream->Close();
}

void TestDataGenerator::BuildReferenceNetwork()
{
	Network network;
	network.m_Root = "Root";

	MotionTreeNode* root = rage_new MotionTreeNode;
	network.m_Nodes.PushAndGrow(root);
	root->m_Name = "Root";
	root->m_Initial = "Blend 2";
	root->m_OnEnterEvent.m_Enabled = false;
	root->m_OnExitEvent.m_Enabled = false;
	root->m_Children.PushAndGrow(::rage::atString("Blend 2"));
	root->m_Children.PushAndGrow(::rage::atString("Clip 0"));
	root->m_Children.PushAndGrow(::rage::atString("Clip 1"));
	root->m_Parameters.Reserve(3);
	root->m_Parameters.Resize(3);
	root->m_Parameters[0].m_Target = "Blend 2";
	root->m_Parameters[0].m_Attribute = "Weight";
	root->m_Parameters[0].m_Parameter = "weight";
	root->m_Parameters[0].m_Index = 0;
	root->m_Parameters[1].m_Target = "Clip 0";
	root->m_Parameters[1].m_Attribute = "Clip";
	root->m_Parameters[1].m_Parameter = "refClip0";
	root->m_Parameters[1].m_Index = 0;
	root->m_Parameters[2].m_Target = "Clip 1";
	root->m_Parameters[2].m_Attribute = "Clip";
	root->m_Parameters[2].m_Parameter = "refClip1";
	root->m_Parameters[2].m_Index = 0;

	BlendNode* blend2 = rage_new BlendNode;
	network.m_Nodes.PushAndGrow(blend2);
	blend2->m_Name = "Blend 2";
	blend2->m_Child0 = "Clip 0";
	blend2->m_Child1 = "Clip 1";
	blend2->m_Filter.m_Type = kFilterIgnored;
	blend2->m_Synchronizer.m_Type = ST_NONE;
	blend2->m_Weight.m_Type = kF32AttributeParameter;
	blend2->m_Weight.m_Parameter = "weight";

	ClipNode* clip0 = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(clip0);
	clip0->m_Name = "Clip 0";
	clip0->m_Clip.m_Type = kClipAttributeParameter;
	clip0->m_Clip.m_Parameter = "refClip0";
	clip0->m_Phase.m_Type = kF32AttributeValue;
	clip0->m_Phase.m_Value = 0.f;
	clip0->m_Rate.m_Type = kF32AttributeValue;
	clip0->m_Rate.m_Value = 1.f;
	clip0->m_Delta.m_Type = kF32AttributeValue;
	clip0->m_Delta.m_Value = 0.f;
	clip0->m_Looped.m_Type = kBoolAttributeValue;
	clip0->m_Looped.m_Value = true;

	ClipNode* clip1 = rage_new ClipNode;
	network.m_Nodes.PushAndGrow(clip1);
	clip1->m_Name = "Clip 1";
	clip1->m_Clip.m_Type = kClipAttributeParameter;
	clip1->m_Clip.m_Parameter = "refClip1";
	clip1->m_Phase.m_Type = kF32AttributeValue;
	clip1->m_Phase.m_Value = 0.f;
	clip1->m_Rate.m_Type = kF32AttributeValue;
	clip1->m_Rate.m_Value = 1.f;
	clip1->m_Delta.m_Type = kF32AttributeValue;
	clip1->m_Delta.m_Value = 0.f;
	clip1->m_Looped.m_Type = kBoolAttributeValue;
	clip1->m_Looped.m_Value = true;

	fiStream* stream = fiStream::Create("reference.xml");
	Assert(stream);
	PARSER.SaveObject(stream, &network);
	stream->Close();

	Network network2;
	network2.m_Root = "Root";
	network2.m_References.PushAndGrow(::rage::atString("reference.mrf"));

	MotionTreeNode* root2 = rage_new MotionTreeNode;
	network2.m_Nodes.PushAndGrow(root2);
	root2->m_Name = "Root";
	root2->m_Initial = "Reference";
	root2->m_OnEnterEvent.m_Enabled = false;
	root2->m_OnExitEvent.m_Enabled = false;
	root2->m_Children.PushAndGrow(::rage::atString("Reference"));

	ReferenceNode* reference = rage_new ReferenceNode;
	network2.m_Nodes.PushAndGrow(reference);
	reference->m_Name = "Reference";
	reference->m_Reference = "reference.mrf";
	reference->m_InitData.PushAndGrow(rage_new ClipInitData);
	((ClipInitData*)reference->m_InitData[0])->m_Key = "refClip0";
	((ClipInitData*)reference->m_InitData[0])->m_Filename = "walk";
	reference->m_InitData.PushAndGrow(rage_new ClipInitData);
	((ClipInitData*)reference->m_InitData[1])->m_Key = "refClip1";
	((ClipInitData*)reference->m_InitData[1])->m_Filename = "idle";
	reference->m_InitData.PushAndGrow(rage_new FloatInitData);
	((FloatInitData*)reference->m_InitData[2])->m_Key = "weight";
	((FloatInitData*)reference->m_InitData[2])->m_Value = 0.f;

	fiStream* stream2 = fiStream::Create("parent.xml");
	Assert(stream2);
	PARSER.SaveObject(stream2, &network2);
	stream2->Close();	
}
