#include "NodeWriter.h"

#include "move/move_types_internal.h"

#include "writer.h"
#include "PropertyWriter.h"

extern LookupTable* g_LookupTable;
extern int FindIndexOfParameter(const char* parameter);

void NodeWriter::WriteAnimationNode(Writer* writer, const AnimationNode* n, int index)
{
	writer->AppendU16(u16(NT_ANIMATION));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_Animation.m_Type | 
		n->m_Phase.m_Type << 2 | 
		n->m_Rate.m_Type << 4 | 
		n->m_Delta.m_Type << 6 | 
		n->m_Looped.m_Type << 8 | 
		n->m_Absolute.m_Type << 10;
	writer->AppendU32(flags);
	PropertyWriter::AppendAnimation(writer, n->m_Animation);
	PropertyWriter::AppendFloat(writer, n->m_Phase);
	PropertyWriter::AppendFloat(writer, n->m_Rate);
	PropertyWriter::AppendFloat(writer, n->m_Delta);
	PropertyWriter::AppendBool(writer, n->m_Looped);
	PropertyWriter::AppendBool(writer, n->m_Absolute);
}

void NodeWriter::WriteAddNode(Writer* writer, const AddNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_ADD));
	writer->AppendU16(u16(index)); 
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_Weight.m_Type |
		n->m_Filter.m_Type << 2 |
		(n->m_Transitional?1<<6:0) |
		(n->m_Source0Immutable?1<<7:0) |
		(n->m_Source1Immutable?1<<8:0) |
		n->m_Source0InfluenceFlag<<12 |
		n->m_Source1InfluenceFlag<<14 |
		n->m_Synchronizer.m_Type << 19;
	writer->AppendU32(flags);
	PropertyWriter::AppendOffset(writer, n->m_Child0, nodes);
	PropertyWriter::AppendOffset(writer, n->m_Child1, nodes);
	if (n->m_Synchronizer.m_Type == ST_TAG) {
		writer->AppendU32(n->m_Synchronizer.m_Tag);
	}
	PropertyWriter::AppendFloat(writer, n->m_Weight);
	PropertyWriter::AppendFilter(writer, n->m_Filter);
}

void NodeWriter::WriteNwayAddNode(Writer* writer, const AddNwayNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_ADDN));
	writer->AppendU16(u16(index));
	writer->AppendU32(atStringHash(n->m_Name.c_str()));

	u32 flags = n->m_FilterN.m_Type |
		n->m_Filter.m_Type << 2 |
		(n->m_ZeroDestination.m_Value ? 1 << 4 : 0) |
		n->m_Synchronizer.m_Type << 19 |
		n->m_Children.size() << 26;
	// write in child immutable flags
	for (int i = 0; i < n->m_Children.size(); ++i) {
		int bit = i+7;

		if (bit<19 && n->m_Children[i].m_Immutable)
		{
			flags |= (1<<bit);
		}
	}
	writer->AppendU32(flags);
	if (n->m_Synchronizer.m_Type == ST_TAG) {
		writer->AppendU32(n->m_Synchronizer.m_Tag);
	}
	PropertyWriter::AppendFilterN(writer, n->m_FilterN);
	PropertyWriter::AppendFilter(writer, n->m_Filter);
	for (int i = 0; i < n->m_Children.size(); ++i) {
		PropertyWriter::AppendOffset(writer, n->m_Children[i].m_Input, nodes);
	}
	u32 words = ((n->m_Children.size() * 2 | 7)+1)>>3;
	u32* inputs = Alloca(u32, words);
	sysMemSet(inputs, 0, words*sizeof(u32));
	for (int i = 0; i < n->m_Children.size(); ++i) {
		int idx = (i*2 & ~7)>>2;
		int set = (i*2&7)*4;
		inputs[idx] |= (n->m_Children[i].m_Weight.m_Type << set);
		inputs[idx] |= (n->m_Children[i].m_InputFilter.m_Type << (set + 4));
	}
	for (u32 i = 0; i < words; ++i) {
		writer->AppendU32(inputs[i]);
	}
	for (int i = 0; i < n->m_Children.size(); ++i) {
		PropertyWriter::AppendFloat(writer, n->m_Children[i].m_Weight);
		PropertyWriter::AppendFilter(writer, n->m_Children[i].m_InputFilter);
	}
}

void NodeWriter::WriteBlendNode(Writer* writer, const BlendNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_BLEND));
	writer->AppendU16(u16(index));
	writer->AppendU32(atStringHash(n->m_Name.c_str()));

	u32 flags = n->m_Weight.m_Type |
		n->m_Filter.m_Type << 2 |
		(n->m_Source0Immutable?1<<7:0) |
		(n->m_Source1Immutable?1<<8:0) |
		n->m_Source0InfluenceFlag<<12 |
		n->m_Source1InfluenceFlag<<14 |
		n->m_Synchronizer.m_Type << 19 |
		(n->m_MergeBlend?1<<31:0);
	writer->AppendU32(flags);
	PropertyWriter::AppendOffset(writer, n->m_Child0, nodes);
	PropertyWriter::AppendOffset(writer, n->m_Child1, nodes);
	if (n->m_Synchronizer.m_Type == ST_TAG) {
		writer->AppendU32(n->m_Synchronizer.m_Tag);
	}
	PropertyWriter::AppendFloat(writer, n->m_Weight);
	PropertyWriter::AppendFilter(writer, n->m_Filter);
}

void NodeWriter::WriteBlendNwayNode(Writer* writer, const BlendNwayNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_BLENDN));
	writer->AppendU16(u16(index));
	writer->AppendU32(atStringHash(n->m_Name.c_str()));

	u32 flags = n->m_FilterN.m_Type |
		n->m_Filter.m_Type << 2 | 
		(n->m_ZeroDestination.m_Value ? 1 << 4 : 0) |
		n->m_Synchronizer.m_Type << 19 |
		n->m_Children.size() << 26;

	// write in child immutable flags
	for (int i = 0; i < n->m_Children.size(); ++i) {
		int bit = i+7;
		
		if (bit<19 && n->m_Children[i].m_Immutable)
		{
			flags |= (1<<bit);
		}
	}

	writer->AppendU32(flags);
	if (n->m_Synchronizer.m_Type == ST_TAG) {
		writer->AppendU32(n->m_Synchronizer.m_Tag);
	}
	//	writer->AppendU32(int(0)); // I think this is meant to be a tag is
	PropertyWriter::AppendFilterN(writer, n->m_FilterN);
	PropertyWriter::AppendFilter(writer, n->m_Filter);
	for (int i = 0; i < n->m_Children.size(); ++i) {
		PropertyWriter::AppendOffset(writer, n->m_Children[i].m_Input, nodes);
	}
	u32 words = ((n->m_Children.size() * 2 | 7)+1)>>3;
	u32* inputs = Alloca(u32, words);
	sysMemSet(inputs, 0, words*sizeof(u32));
	for (int i = 0; i < n->m_Children.size(); ++i) {
		int idx = (i*2 & ~7)>>3;
		int set = (i*2&7)*4;
		inputs[idx] |= (n->m_Children[i].m_Weight.m_Type << set);
		inputs[idx] |= (n->m_Children[i].m_InputFilter.m_Type << (set+4));
	}
	for (u32 i = 0; i < words; ++i) {
		writer->AppendU32(inputs[i]);
	}
	for (int i = 0; i < n->m_Children.size(); ++i) {
		PropertyWriter::AppendFloat(writer, n->m_Children[i].m_Weight);
		PropertyWriter::AppendFilter(writer, n->m_Children[i].m_InputFilter);
	}
}

void NodeWriter::WriteClipNode(Writer* writer, const ClipNode* n, int index)
{
	writer->AppendU16(u16(NT_CLIP));
	writer->AppendU16(u16(index));
	writer->AppendU32(atStringHash(n->m_Name.c_str()));

	u32 flags = n->m_Clip.m_Type |
		n->m_Phase.m_Type << 2 |
		n->m_Rate.m_Type << 4 |
		n->m_Delta.m_Type << 6 |
		n->m_Looped.m_Type << 8;
	
	if (n->m_ZeroWeightSync.m_Value)
	{ 
		flags |= (1 << 10);
	}
	
	writer->AppendU32(flags);
	PropertyWriter::AppendClip(writer, n->m_Clip);
	PropertyWriter::AppendFloat(writer, n->m_Phase);
	PropertyWriter::AppendFloat(writer, n->m_Rate);
	PropertyWriter::AppendFloat(writer, n->m_Delta);
	PropertyWriter::AppendBool(writer, n->m_Looped);
}

void NodeWriter::WriteCaptureNode(Writer* writer, const CaptureNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_CAPTURE));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_Frame.m_Type |
		n->m_Owner.m_Type << 4;
	writer->AppendU32(flags);
	PropertyWriter::AppendOffset(writer, n->m_Source, nodes);
	PropertyWriter::AppendFrame(writer, n->m_Frame);
	PropertyWriter::AppendBool(writer, n->m_Owner);
}

void NodeWriter::WriteExpressionNode(Writer* writer, const ExpressionNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_EXPRESSION));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_Expressions.m_Type |
		n->m_Weight.m_Type << 2 | 
		n->m_Variables.size() << 28;
	for(int i=0; i<n->m_Variables.size(); ++i)
	{
		flags |= (n->m_Variables[i].m_Value.m_Type)<<(i*2+4);
	}

	writer->AppendU32(flags);

	PropertyWriter::AppendOffset(writer, n->m_Source, nodes);
	PropertyWriter::AppendExpressions(writer, n->m_Expressions);
	PropertyWriter::AppendFloat(writer, n->m_Weight);

	for(int i=0; i<n->m_Variables.size(); ++i)
	{
		writer->AppendU32(atStringHash(n->m_Variables[i].m_Name.c_str()));
		PropertyWriter::AppendFloat(writer, n->m_Variables[i].m_Value);
	}
}

void NodeWriter::WriteExtrapolateNode(Writer* writer, const ExtrapolateNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_EXTRAPOLATE));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_Damping.m_Type;
	writer->AppendU32(flags);

	PropertyWriter::AppendOffset(writer, n->m_Source, nodes);
	PropertyWriter::AppendFloat(writer, n->m_Damping);
}

void NodeWriter::WriteFilterNode(Writer* writer, const FilterNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_FILTER));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_Filter.m_Type;
	writer->AppendU32(flags);
	PropertyWriter::AppendOffset(writer, n->m_Source, nodes);
	PropertyWriter::AppendFilter(writer, n->m_Filter);
}

void NodeWriter::WriteFrameNode(Writer* writer, const FrameNode* n, int index)
{
	writer->AppendU16(u16(NT_FRAME));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_Frame.m_Type |
		n->m_Owner.m_Type << 4;
	writer->AppendU32(flags);
	PropertyWriter::AppendFrame(writer, n->m_Frame);
	PropertyWriter::AppendBool(writer, n->m_Owner);
}

void NodeWriter::WriteIdentityNode(Writer* writer, const IdentityNode* n, int index)
{
	writer->AppendU16(u16(NT_IDENTITY));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());
}

void NodeWriter::WriteInvalidNode(Writer* writer, const InvalidNode* n, int index)
{
	writer->AppendU16(u16(NT_INVALID));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());
}

void NodeWriter::WriteJointLimitNode(Writer* writer, const JointLimitNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_JOINTLIMIT));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	//PropertyWriter::AppendOffset(writer, n->m_Source, nodes);
}

void NodeWriter::WriteMergeNode(Writer* writer, const MergeNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_MERGE));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_Filter.m_Type |
		(n->m_Transitional?1<<6:0) |
		(n->m_Source0Immutable?1<<7:0) |
		(n->m_Source1Immutable?1<<8:0) |
		n->m_Synchronizer.m_Type << 19;

	flags |= (n->m_Source0InfluenceFlag<<2);
	flags |= (n->m_Source1InfluenceFlag<<4);

	writer->AppendU32(flags);
	PropertyWriter::AppendOffset(writer, n->m_Source0, nodes);
	PropertyWriter::AppendOffset(writer, n->m_Source1, nodes);
	if (n->m_Synchronizer.m_Type == ST_TAG) {
		writer->AppendU32(n->m_Synchronizer.m_Tag);
	}
	PropertyWriter::AppendFilter(writer, n->m_Filter);
}

void NodeWriter::WriteMergeNwayNode(Writer* writer, const MergeNWayNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_MERGEN));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 flags = n->m_FilterN.m_Type |
		n->m_Filter.m_Type << 2 |
		(n->m_ZeroDestination.m_Value ? 1 << 4 : 0) |
		n->m_Synchronizer.m_Type << 19 |
		n->m_Children.size() << 26;
	// write in child immutable flags
	for (int i = 0; i < n->m_Children.size(); ++i) {
		int bit = i+7;

		if (bit<19 && n->m_Children[i].m_Immutable)
		{
			flags |= (1<<bit);
		}
	}
	writer->AppendU32(flags);
	if (n->m_Synchronizer.m_Type == ST_TAG) {
		writer->AppendU32(n->m_Synchronizer.m_Tag);
	}
	PropertyWriter::AppendFilterN(writer, n->m_FilterN);
	PropertyWriter::AppendFilter(writer, n->m_Filter);
	for (int i = 0; i < n->m_Children.size(); ++i) {
		PropertyWriter::AppendOffset(writer, n->m_Children[i].m_Input, nodes);
	}
	u32 words = ((n->m_Children.size() * 2 | 7)+1)>>3;
	u32* inputs = Alloca(u32, words);
	sysMemSet(inputs, 0, words*sizeof(u32));
	for (int i = 0; i < n->m_Children.size(); ++i) {
		int idx = (i*2 & ~7)>>2;
		int set = (i*2&7)*4;
		inputs[idx] |= (n->m_Children[i].m_Weight.m_Type << set);
		inputs[idx] |= (n->m_Children[i].m_InputFilter.m_Type << (set+4));
	}
	for (u32 i = 0; i < words; ++i) {
		writer->AppendU32(inputs[i]);
	}
	for (int i = 0; i < n->m_Children.size(); ++i) {
		PropertyWriter::AppendFloat(writer, n->m_Children[i].m_Weight);
		PropertyWriter::AppendFilter(writer, n->m_Children[i].m_InputFilter);
	}
}

void NodeWriter::WriteParameterizedMotionNode(Writer* writer, const ParameterizedMotionNode* n, int index)
{
	writer->AppendU16(u16(NT_PM));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	u32 parameters = 0;
	for (int i = 0; i < n->m_Parameters.size(); ++i) {
		parameters |= (n->m_Parameters[i].m_Type << i*2);
	}
	u32 flags = n->m_ParameterizedMotion.m_Type |
		n->m_Rate.m_Type << 2 |
		n->m_Phase.m_Type << 4 |
		n->m_Delta.m_Type << 6 |
		n->m_Parameters.size() << 10 |
		parameters << 19;
	writer->AppendU32(flags);
	PropertyWriter::AppendParameterizedMotion(writer, n->m_ParameterizedMotion);
	PropertyWriter::AppendFloat(writer, n->m_Rate);
	PropertyWriter::AppendFloat(writer, n->m_Phase);
	PropertyWriter::AppendFloat(writer, n->m_Delta);
	for (int i = 0; i < n->m_Parameters.size(); ++i) {
		PropertyWriter::AppendFloat(writer, n->m_Parameters[i]);
	}
}

void NodeWriter::WritePoseNode(Writer* writer, const PoseNode* n, int index)
{
	writer->AppendU16(u16(NT_POSE));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	// old pipeline hard coded this
	bool normalize = true;
	writer->AppendU32(normalize ? 1 : 0);
	if(normalize)
	{
		writer->AppendU32(0x01000000); // woo, magic number ahoy!
	}
}

void NodeWriter::WriteProxyNode(Writer* writer, const ProxyNode* n, Map& nodes, int index)
{
	writer->AppendU16(u16(NT_PROXY));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	(void)sizeof(writer);
	(void)sizeof(n);
	(void)sizeof(nodes);
}

void NodeWriter::WriteMotionTreeNode(Writer* writer, const MotionTreeNode* n, const BaseNode* parent, Map& nodes, int index, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flags)
{
	writer->AppendU16(u16(NT_STATE));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	const ExportDef** initial = nodes.SafeGet(n->m_Initial);
	Assert(initial);
	writer->AppendOffset((*initial)->label);

	writer->AppendU32(n->m_DeferBlockUpdate?1:0); // Should we defer disabling transitions for a frame when using block update on transition

	writer->AppendU8(n->m_OnEnterEvent.m_Enabled);
	writer->AppendU8(n->m_OnExitEvent.m_Enabled);

	// Count number of non-Tail children
	u8 childCount = 0;
	for(int i = 0; i < n->m_Children.size(); i ++)
	{
		const ExportDef** def = nodes.SafeGet(atStringHash(n->m_Children[i]));
		Assert(def);

		// Check node is not a Tail node
		if((*def)->node->parser_GetStructure() != TailNode::parser_GetStaticStructure())
		{
			childCount ++;
		}
	}
	writer->AppendU8(childCount);

	// Get the parent and find any transitions from this node
	std::vector<const Transition*> transitions;
	if (parent) { // safety check for root node which wont have a parent
		for (int i = 0; i < n->m_Transitions.size(); ++i) {
			const Transition* t = &n->m_Transitions[i];
			if(t->m_Enabled)
				transitions.push_back(t);
		}
	}

	writer->AppendU8(u8(transitions.size()));

	if (n->m_OnEnterEvent.m_Enabled) {
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_OnEnterEvent.m_Id.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_OnEnterEvent.m_Id.c_str());
		}
	} else {
		writer->AppendU32(0);
	}

	if (n->m_OnExitEvent.m_Enabled) {
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_OnExitEvent.m_Id.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_OnExitEvent.m_Id.c_str());
		}
	} else {
		writer->AppendU32(0);
	}

	Writer::Identifier transitionLabel = Writer::ReserveLabel();
	writer->AppendOffset(transitionLabel); // Transition offset place holder

	Writer::Identifier parameterLabel = Writer::ReserveLabel();
	writer->AppendOffset(parameterLabel); // signal offset
	writer->AppendU32(n->m_Parameters.size()); // signal count
	Writer::Identifier eventLabel = Writer::ReserveLabel();
	writer->AppendOffset(eventLabel); // event offset
	writer->AppendU32(n->m_Events.size()); // event count
	Writer::Identifier outputLabel = Writer::ReserveLabel();
	writer->AppendOffset(outputLabel); // output offset
	writer->AppendU32(n->m_Outputs.size()); // output count
	Writer::Identifier operatorLabel = Writer::ReserveLabel();
	writer->AppendOffset(operatorLabel); // op offset
	writer->AppendU32(n->m_Operators.size()); // op count

	// Order is implicit in how the transitions are exported
	writer->AppendLabel(transitionLabel);
	AppendTransitions(writer, transitions, nodes, requests, flags);

	writer->AppendLabel(parameterLabel);
	for (int i = 0; i < n->m_Parameters.size(); ++i) {
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_Parameters[i].m_Parameter.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_Parameters[i].m_Parameter.c_str());
		}
		writer->AppendU16(IndexOf(n->m_Children, n->m_Parameters[i].m_Target.c_str()));
		writer->AppendU16(TypeOf(nodes, n->m_Parameters[i].m_Target, n->m_Parameters[i].m_Attribute));
		writer->AppendU32(n->m_Parameters[i].m_Index);
	}

	writer->AppendLabel(eventLabel);
	for (int i = 0; i < n->m_Events.size(); ++i) {
		writer->AppendU16(IndexOf(n->m_Children, n->m_Events[i].m_Target.c_str()));
		writer->AppendU16(EventOf(nodes, n->m_Events[i].m_Target, n->m_Events[i].m_Type));
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_Events[i].m_Event.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_Events[i].m_Event.c_str());
		}
	}

	writer->AppendLabel(outputLabel);
	for (int i = 0; i < n->m_Outputs.size(); ++i) {
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_Outputs[i].m_Parameter.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_Outputs[i].m_Parameter.c_str());
		}
		writer->AppendU16(IndexOf(n->m_Children, n->m_Outputs[i].m_Target.c_str()));
		writer->AppendU16(TypeOf(nodes, n->m_Outputs[i].m_Target, n->m_Outputs[i].m_Attribute));
		writer->AppendU32(u16(n->m_Outputs[i].m_Index));
	}

	writer->AppendLabel(operatorLabel);
	for (int i = 0; i < n->m_Operators.size(); ++i) {
		writer->AppendU16(IndexOf(n->m_Children, n->m_Operators[i].m_Target.c_str()));
		writer->AppendU16(TypeOf(nodes, n->m_Operators[i].m_Target, n->m_Operators[i].m_Attribute));
		u32 numOperations = n->m_Operators[i].m_Operations.size();
		u16 operationsSize = 8 * (static_cast<u16>(numOperations) + 1);// all operators are 8 bytes (+1 for the end operation)
		writer->AppendU16(operationsSize);
		writer->AppendU16(u16(n->m_Operators[i].m_Index));
		for (int o = 0; o < n->m_Operators[i].m_Operations.size(); ++o) {
			PropertyWriter::AppendOperation(writer, n->m_Operators[i].m_Operations[o]);
		}
		// The end operation
		writer->AppendU32(0);
		writer->AppendU32(0);
	}
}

void NodeWriter::WriteStateMachineNode(Writer* writer, const StateMachineNode* n, const BaseNode* parent, Map& nodes, int index, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flagset)
{
	writer->AppendU16(u16(NT_STATEMACHINE));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	PropertyWriter::AppendOffset(writer, n->m_Initial, nodes);

	writer->AppendU32(n->m_DeferBlockUpdate?1:0); // Should we defer disabling transitions for a frame when using block update on transition

	writer->AppendU8(n->m_OnEnterEvent.m_Enabled?1:0); // enter event enabled
	writer->AppendU8(n->m_OnExitEvent.m_Enabled?1:0); // exit event enabled

	writer->AppendU8(u8(n->m_Children.size())); // child count

	std::vector<const Transition*> transitions;
	if (parent) { // safety check for root node which wont have a parent
		for (int i = 0; i < n->m_Transitions.size(); ++i) {
			const Transition* t = &n->m_Transitions[i];
			if (t->m_Enabled && strcmp(n->m_Name, t->m_From) == 0) {
				transitions.push_back(t);
			}
		}
	}

	writer->AppendU8(u8(transitions.size())); // transition count

	if (n->m_OnEnterEvent.m_Enabled) {
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_OnEnterEvent.m_Id.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_OnEnterEvent.m_Id.c_str());
		}
	} else {
		writer->AppendU32(0);
	}

	if (n->m_OnExitEvent.m_Enabled) {
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_OnExitEvent.m_Id.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_OnExitEvent.m_Id.c_str());
		}
	} else {
		writer->AppendU32(0);
	}

	bool isLeafSM = parent && parent->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure();

	Writer::Identifier transitionLabel = NULL;
	if(!isLeafSM)
	{
		transitionLabel = Writer::ReserveLabel();
		writer->AppendOffset(transitionLabel);
	}
	else
	{
		writer->AppendU32(0);
	}

	// spit out the child node data
	for (int i = 0; i < n->m_Children.size(); ++i) {
		writer->AppendStringHash(n->m_Children[i].c_str());
		const ExportDef** child = nodes.SafeGet(n->m_Children[i]);
		Assert(child);
		writer->AppendOffset((*child)->label);
	}

	// spit out the transitions
	if(!isLeafSM)
	{
		writer->AppendLabel(transitionLabel);
	}
	AppendTransitions(writer, transitions, nodes, requests, flagset);
}

void NodeWriter::WriteTailNode(Writer* writer, const TailNode* n, int index)
{
	writer->AppendU16(u16(NT_TAIL));
	//writer->AppendU16(u16(index));
	writer->AppendU16(0);
	//writer->AppendStringHash(n->m_Name.c_str());
	writer->AppendU32(0);
}

void NodeWriter::WriteInlineStateMachineNode(Writer* writer, const InlineStateMachineNode* n, const BaseNode* parent, Map& nodes, int index, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flagset)
{
	writer->AppendU16(u16(NT_INLINE));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	PropertyWriter::AppendOffset(writer, n->m_Initial, nodes);

	writer->AppendU32(n->m_DeferBlockUpdate?1:0); // Should we defer disabling transitions for a frame when using block update on transition

	writer->AppendU8(n->m_OnEnterEvent.m_Enabled?1:0); // enter event enabled
	writer->AppendU8(n->m_OnExitEvent.m_Enabled?1:0); // exit event enabled

	writer->AppendU8(u8(n->m_Children.size())); // child count

	std::vector<const Transition*> transitions;
	if (parent) { // safety check for root node which wont have a parent
		for (int i = 0; i < n->m_Transitions.size(); ++i) {
			const Transition* t = &n->m_Transitions[i];
			if (t->m_Enabled && strcmp(n->m_Name, t->m_From) == 0) {
				transitions.push_back(t);
			}
		}
	}

	writer->AppendU8(u8(transitions.size())); // transition count

	if (n->m_OnEnterEvent.m_Enabled) {
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_OnEnterEvent.m_Id.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_OnEnterEvent.m_Id.c_str());
		}
	} else {
		writer->AppendU32(0);
	}

	if (n->m_OnExitEvent.m_Enabled) {
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(n->m_OnExitEvent.m_Id.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(n->m_OnExitEvent.m_Id.c_str());
		}
	} else {
		writer->AppendU32(0);
	}

	bool isLeafSM = parent && parent->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure();

	Writer::Identifier transitionLabel = NULL;
	if(!isLeafSM)
	{
		transitionLabel = Writer::ReserveLabel();
		writer->AppendOffset(transitionLabel);
	}
	else
	{
		writer->AppendU32(0);
	}

	const ExportDef** input = nodes.SafeGet(n->m_Input);
	Assert(input);
	writer->AppendOffset((*input)->label);

	// spit out the child node data
	for (int i = 0; i < n->m_Children.size(); ++i) {
		writer->AppendStringHash(n->m_Children[i].c_str());
		const ExportDef** child = nodes.SafeGet(n->m_Children[i]);
		Assert(child);
		writer->AppendOffset((*child)->label);
	}

	// spit out the transitions
	if(!isLeafSM)
	{
		writer->AppendLabel(transitionLabel);
	}
	AppendTransitions(writer, transitions, nodes, requests, flagset);
}

void NodeWriter::WriteReferenceNode(Writer* writer, const ReferenceNode* n, int index, atArray<Writer::Identifier>& identifiers, atArray<const char*>& filenames)
{
	writer->AppendU16(u16(NT_REFERENCE));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_Name.c_str());

	// Should be the filename without a path
	writer->AppendStringHash(n->m_Reference.c_str());
	Writer::Identifier label = Writer::ReserveLabel();
	writer->AppendOffset(label);
	writer->AppendU32(n->m_InitData.size());
	writer->AppendU32(n->m_Parameters.size());
	writer->AppendU32(n->m_Flags.size());
	writer->AppendU32(n->m_Requests.size());
	for (int i = 0; i < n->m_Parameters.size(); ++i) {
		const char* from = n->m_Parameters[i].m_From;
		writer->AppendStringHash(from);
		const char* to = n->m_Parameters[i].m_To;
		writer->AppendStringHash(to);
	}
	for (int i = 0; i < n->m_Flags.size(); ++i) {
		const char* from = n->m_Flags[i].m_From;
		writer->AppendStringHash(from);
		const char* to = n->m_Flags[i].m_To;
		writer->AppendStringHash(to);
	}
	for (int i = 0; i < n->m_Requests.size(); ++i) {
		const char* from = n->m_Requests[i].m_From;
		writer->AppendStringHash(from);
		const char* to = n->m_Requests[i].m_To;
		writer->AppendStringHash(to);
	}

	//	atArray<Writer::Identifier> identifiers;
	//	atArray<const char*> filenames;

	writer->AppendLabel(label);
	for (int i = 0; i < n->m_InitData.size(); ++i) {
		InitializerData* data = n->m_InitData[i];
		const parStructure* info = data->parser_GetStructure();
		if (info == AnimationInitData::parser_GetStaticStructure()) {
			AnimationInitData* d = (AnimationInitData*)data;
			writer->AppendU32(0);
			writer->AppendStringHash(n->m_InitData[i]->m_Key.c_str());
			Writer::Identifier identifier = Writer::ReserveLabel();
			writer->AppendOffset(identifier);
			identifiers.PushAndGrow(identifier);
			filenames.PushAndGrow(d->m_Filename);
		} else if (info == ClipInitData::parser_GetStaticStructure()) {
			ClipInitData* d = (ClipInitData*)data;
			writer->AppendU32(1);
			writer->AppendStringHash(n->m_InitData[i]->m_Key.c_str());
			Writer::Identifier identifier = Writer::ReserveLabel();
			writer->AppendOffset(identifier);
			identifiers.PushAndGrow(identifier);
			filenames.PushAndGrow(d->m_Filename);
		} else if (info == ExpressionInitData::parser_GetStaticStructure()) {
			ExpressionInitData* d = (ExpressionInitData*)data;
			writer->AppendU32(2);
			writer->AppendStringHash(n->m_InitData[i]->m_Key.c_str());
			Writer::Identifier identifier = Writer::ReserveLabel();
			writer->AppendOffset(identifier);
			identifiers.PushAndGrow(identifier);
			filenames.PushAndGrow(d->m_Filename);
		} else if (info == ParameterizedMotionInitData::parser_GetStaticStructure()) {
			ParameterizedMotionInitData* d = (ParameterizedMotionInitData*)data;
			writer->AppendU32(6);
			writer->AppendStringHash(n->m_InitData[i]->m_Key.c_str());
			Writer::Identifier identifier = Writer::ReserveLabel();
			writer->AppendOffset(identifier);
			identifiers.PushAndGrow(identifier);
			filenames.PushAndGrow(d->m_Filename);
		} else if (info == FloatInitData::parser_GetStaticStructure()) {
			FloatInitData* d = (FloatInitData*)data;
			writer->AppendU32(7);
			writer->AppendStringHash(n->m_InitData[i]->m_Key.c_str());
			writer->AppendFloat(d->m_Value);
		} else {
			Quitf("Unsupported init data");
		}
	}
}

void NodeWriter::WriteSubnetworkNode(Writer* writer, const SubnetworkNode* n, int index)
{
	writer->AppendU16(u16(NT_SUBNETWORK));
	writer->AppendU16(u16(index));
	writer->AppendStringHash(n->m_ParameterName.c_str());

#if USE_INDICES
	if (g_LookupTable) {
		int index = FindIndexOfParameter(n->m_ParameterName.c_str());
		Assert(index >= 0);
		writer->AppendI32(index);
	} else 
#endif // USE_INDICES
	{
		writer->AppendStringHash(n->m_ParameterName.c_str());
	}
}

void NodeWriter::AppendTransitions(Writer* writer, std::vector<const Transition*>& transitions, Map& nodes, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flagset)
{
	for (u32 i = 0; i < transitions.size(); ++i) {
		const Transition* t = transitions[i];
		if(t->m_Enabled) {
			u32 size = sizeof(u32)*5 + sizeof(float); // flags + tag + target + duration
			for (int j = 0; j < t->m_Conditions.size(); ++j) {
				size += GetConditionSize(t->m_Conditions[j]);
			}
			bool dynamic = (t->m_Filter.m_Type == kFilterFilename) ? (t->m_IsFilterDynamic ? true : false) : false;
			bool filtered = (t->m_Filter.m_Type == kFilterFilename) ? true : false;
			if (filtered)  {
				size += sizeof(u32)*2;
			}
			u32 type = 0; // not used at the moment
			u32 enableTransitionWeightSignal = t->m_TransitionWeightOutputParameter.GetLength() > 0;
			u32 blockUpdateAfterTransition = t->m_BlockUpdateOnTransition;
			u32 durationFromSignal = t->m_DurationParameter.GetLength() > 0;
			u32 flags = type |
				(enableTransitionWeightSignal << 1) |
				(blockUpdateAfterTransition << 2) |
				(durationFromSignal << 3) |
				(size << 4) |
				(t->m_Transitional?1<<18:0) |
				(t->m_Immutable?1<<19:0) |
				t->m_Conditions.size() << 20 |
				t->m_Modifier << 24 |
				(t->m_ReEvaluate ? 1 << 27 : 0) |
				t->m_Synchronizer.m_Type << 28 |
				(filtered ? 1 << 30 : 0) |
				(dynamic ? 1 << 31 : 0);
			writer->AppendU32(flags);
			u32 sync = 0xffffffff;
			if (t->m_Synchronizer.m_Type == ST_TAG) {
				sync = t->m_Synchronizer.m_Tag;
			}
			writer->AppendU32(sync);
			writer->AppendFloat(t->m_Duration);
#if USE_INDICES
			writer->AppendU32(durationFromSignal ? FindIndexOfParameter(t->m_DurationParameter.c_str()) : 0);
			writer->AppendU32(enableTransitionWeightSignal ? FindIndexOfParameter(t->m_TransitionWeightOutputParameter.c_str()) : 0);
#else
			writer->AppendU32(durationFromSignal ? atStringHash(t->m_DurationParameter.c_str()) : 0);
			writer->AppendU32(enableTransitionWeightSignal ? atStringHash(t->m_TransitionWeightOutputParameter.c_str()) : 0);
#endif // USE_INDICES
			atString toString = t->m_To; toString.Replace("__Restart_", "");
			const ExportDef** to = nodes.SafeGet(toString);
			if (!to)
			{
				Quitf("Network invalid! To state '%s'(unmodified string '%s') does not exist (in transition from state '%s')!", toString.c_str(), t->m_To.c_str(), t->m_From.c_str());
			}
			writer->AppendOffset((*to)->label);
			for (int j = 0; j < t->m_Conditions.size(); ++j) {
				const Condition* condition = t->m_Conditions[j];
				const parStructure* info = condition->parser_GetStructure();
				if (info == InRange::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_IN_RANGE));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const InRange* c = static_cast<const InRange*>(condition);
#if USE_INDICES
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Parameter.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else 
#endif // USE_INDICES
					{
						writer->AppendStringHash(c->m_Parameter.c_str());
					}
					writer->AppendFloat(c->m_Upper);
					writer->AppendFloat(c->m_Lower);
				} else if (info == NotInRange::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_OUT_OF_RANGE));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const NotInRange* c = static_cast<const NotInRange*>(condition);
#if USE_INDICES
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Parameter.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else 
#endif // USE_INDICES
					{
						writer->AppendStringHash(c->m_Parameter.c_str());
					}
					writer->AppendFloat(c->m_Upper);
					writer->AppendFloat(c->m_Lower);
				} else if (info == OnRequest::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_ON_REQUEST));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const OnRequest* c = static_cast<const OnRequest*>(condition);
					writer->AppendU32(IndexOf(requests, c->m_Parameter));
					writer->AppendU32(u32(c->m_NotSet ? 1: 0));
				} else if (info == OnFlag::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_ON_FLAG));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const OnFlag* c = static_cast<const OnFlag*>(condition);
					writer->AppendU32(IndexOf(flagset, c->m_Parameter));
					writer->AppendU32(u32(c->m_NotSet ? 1 : 0));
				} else if (info == OnTag::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_ON_TAG));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const OnTag* c = static_cast<const OnTag*>(condition);
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Tag.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else {
						writer->AppendStringHash(c->m_Tag.c_str());
					}
					writer->AppendU32(u32(c->m_NotSet ? 1 : 0));
				} else if (info == AtEvent::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_AT_EVENT));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const AtEvent* c = static_cast<const AtEvent*>(condition);
#if USE_INDICES
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Event.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else 
#endif // USE_INDICES
					{
						writer->AppendStringHash(c->m_Event.c_str());
					}
					writer->AppendU32(u32(c->m_NotSet ? 1: 0));
				} else if (info == GreaterThan::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_GREATER_THAN));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const GreaterThan* c = static_cast<const GreaterThan*>(condition);
#if USE_INDICES
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Parameter.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else 
#endif // USE_INDICES
					{
						writer->AppendStringHash(c->m_Parameter.c_str());
					}
					writer->AppendFloat(c->m_Trigger);
				} else if (info == GreaterThanEqual::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_GREATER_THAN_EQUAL));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const GreaterThanEqual* c = static_cast<const GreaterThanEqual*>(condition);
#if USE_INDICES
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Parameter.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else 
#endif // USE_INDICES
					{
						writer->AppendStringHash(c->m_Parameter.c_str());
					}
					writer->AppendFloat(c->m_Trigger);
				} else if (info == LessThan::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_LESS_THAN));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const LessThan* c = static_cast<const LessThan*>(condition);
#if USE_INDICES
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Parameter.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else 
#endif // USE_INDICES
					{
						writer->AppendStringHash(c->m_Parameter.c_str());
					}
					writer->AppendFloat(c->m_Trigger);
				} else if (info == LessThanEqual::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_LESS_THAN_EQUAL));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const LessThanEqual* c = static_cast<const LessThanEqual*>(condition);
#if USE_INDICES
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Parameter.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else 
#endif // USE_INDICES
					{
						writer->AppendStringHash(c->m_Parameter.c_str());
					}
					writer->AppendFloat(c->m_Trigger);
				} else if (info ==  BoolEquals::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_BOOL_EQUALS));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const BoolEquals* c = static_cast<const BoolEquals*>(condition);
#if USE_INDICES
					if (g_LookupTable) {
						int index = FindIndexOfParameter(c->m_Parameter.c_str());
						Assert(index >= 0);
						writer->AppendI32(index);
					} else 
#endif // USE_INDICES
					{
						writer->AppendStringHash(c->m_Parameter.c_str());
					}
					writer->AppendU32(u32(c->m_Value ? 1: 0)); // Bool's aren't supported, so treat it as a U32 which the runtime also does.
				} else if (info == LifetimeLessThan::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_LIFETIME_LESS_THAN));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const LifetimeLessThan* c = static_cast<const LifetimeLessThan*>(condition);
					writer->AppendFloat(c->m_Trigger);
				} else if (info == LifetimeGreaterThan::parser_GetStaticStructure()) {
					writer->AppendU16(u16(CONDITION_LIFETIME_GREATER_THAN));
					writer->AppendU16(u16(condition->m_FlagWhenPassed));
					const LifetimeGreaterThan* c = static_cast<const LifetimeGreaterThan*>(condition);
					writer->AppendFloat(c->m_Trigger);
				} else {
					Assert(false);
				}
			}
			if(filtered)
			{
				PropertyWriter::AppendFilter(writer, t->m_Filter);
			}
		}
	}
}

u32 NodeWriter::GetConditionSize(const Condition* base )
{
	const parStructure* info = base->parser_GetStructure();
	if (info == InRange::parser_GetStaticStructure()) {
		return sizeof(mvRange);
	} else if (info == NotInRange::parser_GetStaticStructure()) {
		return sizeof(mvRange);
	} else if (info == OnRequest::parser_GetStaticStructure()) {
		return sizeof(mvOnRequest);
	} else if (info == OnFlag::parser_GetStaticStructure()) {
		return sizeof(mvOnFlag);
	} else if (info == AtEvent::parser_GetStaticStructure()) {
		return sizeof(mvAtEvent);
	} else if (info == GreaterThan::parser_GetStaticStructure()) {
		return sizeof(mvValue);
	} else if (info == GreaterThanEqual::parser_GetStaticStructure()) {
		return sizeof(mvValue);
	} else if (info == LessThan::parser_GetStaticStructure()) {
		return sizeof(mvValue);
	} else if (info == LessThanEqual::parser_GetStaticStructure()) {
		return sizeof(mvValue);
	} else if (info == LifetimeLessThan::parser_GetStaticStructure()) {
		return sizeof(mvConditionLifetime);
	} else if (info == LifetimeGreaterThan::parser_GetStaticStructure()) {
		return sizeof(mvConditionLifetime);
	} else if (info == OnTag::parser_GetStaticStructure()) {
		return sizeof(mvOnMoveEventTag);
	} else if (info == BoolEquals::parser_GetStaticStructure()) {
		return sizeof(mvBoolValue);
	}

	Assert(false);
	return 0;
}

u16 NodeWriter::IndexOf(const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& children, const char* name)
{
	for (u16 i = 0; i < children.size(); ++i) {
		if (strcmp(children[i], name) == 0) {
			return i;
		}
	}

	Assert(false);
	return 0;
}

u16 NodeWriter::TypeOf(Map& nodes, const char* node, const char* attribute)
{
	const ExportDef** def = nodes.SafeGet(node);
	Assert(def);

	const parStructure* info = (*def)->node->parser_GetStructure();
	if (info == AnimationNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Animation") == 0) {
			return 0;
		} 
		else if (strcmp(attribute, "Phase") == 0) {
			return 1;
		}
		else if (strcmp(attribute, "Rate") == 0) {
			return 2;
		}
		else if (strcmp(attribute, "Delta") == 0) {
			return 3;
		}
		else if (strcmp(attribute, "Looped") == 0) {
			return 4;
		}
		else if (strcmp(attribute, "Absolute") == 0) {
			return 5;
		}
	} else if (info == AddNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Filter") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return 1;
		}
	} else if (info == AddNwayNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "FilterN") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "Filter") == 0) {
			return 1;
		}
		else if (strcmp(attribute, "InputFilter") == 0) {
			return 3;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return 2;
		}
	} else if (info == BlendNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Filter") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return 1;
		}
	} else if (info == BlendNwayNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "FilterN") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "Filter") == 0) {
			return 1;
		}
		else if (strcmp(attribute, "InputFilter") == 0) {
			return 3;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return 2;
		}
	} else if (info == ClipNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Clip") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "Phase") == 0) {
			return 1;
		}
		else if (strcmp(attribute, "Rate") == 0) {
			return 2;
		}
		else if (strcmp(attribute, "Delta") == 0) {
			return 3;
		}
		else if (strcmp(attribute, "Looped") == 0) {
			return 4;
		}
	} else if (info == ExpressionNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Expressions") == 0) {
			return 0;
		} else if (strcmp(attribute, "Weight") == 0) {
			return 1;
		}
	} else if (info == ExtrapolateNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Damping") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "DeltaTime") == 0) {
			return 5;
		}
		else if (strcmp(attribute, "LastFrame") == 0) {
			return 1;
		}
		else if (strcmp(attribute, "OwnLastFrame") == 0) {
			return 2;
		}
		else if (strcmp(attribute, "DeltaFrame") == 0) {
			return 3;
		}
		else if (strcmp(attribute, "OwnDeltaFrame") == 0) {
			return 4;
		}
	} else if (info == FilterNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Filter") == 0) {
			return 0;
		}
	} else if (info == FrameNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Frame") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "Owner") == 0) {
			return 1;
		}
	} else if (info == IdentityNode::parser_GetStaticStructure()) {

	} else if (info == MergeNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Filter") == 0) {
			return 0;
		}
	} else if (info == MergeNWayNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "FilterN") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "Filter") == 0) {
			return 1;
		}
		else if (strcmp(attribute, "InputFilter") == 0) {
			return 3;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return 2;
		}
	} else if (info == ParameterizedMotionNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "ParameterizedMotion") == 0) {
			return 0;
		}
		else if (strcmp(attribute, "Phase") == 0) {
			return 1;
		}
		else if (strcmp(attribute, "Delta") == 0) {
			return 2;
		}
		else if (strcmp(attribute, "Rate") == 0) {
			return 3;
		}
		else if (strcmp(attribute, "ParameterValue") == 0) {
			return 4;
		}
	} else if (info == PoseNode::parser_GetStaticStructure()) {

	} else if (info == ProxyNode::parser_GetStaticStructure()) {

	} else if (info == ReferenceNode::parser_GetStaticStructure()) {

	} else if (info == SubnetworkNode::parser_GetStaticStructure()) {

	}

	return 0;
}

u16 NodeWriter::EventOf(Map& nodes, const char* node, const char* event)
{
	const ExportDef** def = nodes.SafeGet(node);
	Assert(def);

	const parStructure* info = (*def)->node->parser_GetStructure();
	if (info == AnimationNode::parser_GetStaticStructure()) {

	} else if (info == AddNode::parser_GetStaticStructure()) {

	} else if (info == AddNwayNode::parser_GetStaticStructure()) {

	} else if (info == BlendNode::parser_GetStaticStructure()) {

	} else if (info == BlendNwayNode::parser_GetStaticStructure()) {

	} else if (info == ClipNode::parser_GetStaticStructure()) {
		if (strcmp(event, "ClipLooped") == 0) {
			return 0;
		}
		else if (strcmp(event, "ClipEnded") == 0) {
			return 1;
		}
		else if (strcmp(event, "ClipTagEnter") == 0) {
			return 2;
		}
		else if (strcmp(event, "ClipTagExit") == 0) {
			return 3;
		}
		else if (strcmp(event, "ClipTagUpdate") == 0) {
			return 4;
		}
	} else if (info == ExpressionNode::parser_GetStaticStructure()) {

	} else if (info == ExtrapolateNode::parser_GetStaticStructure()) {

	} else if (info == FilterNode::parser_GetStaticStructure()) {

	} else if (info == FrameNode::parser_GetStaticStructure()) {

	} else if (info == IdentityNode::parser_GetStaticStructure()) {

	} else if (info == MergeNode::parser_GetStaticStructure()) {

	} else if (info == MergeNWayNode::parser_GetStaticStructure()) {

	} else if (info == ParameterizedMotionNode::parser_GetStaticStructure()) {

	} else if (info == PoseNode::parser_GetStaticStructure()) {

	} else if (info == ProxyNode::parser_GetStaticStructure()) {

	} else if (info == ReferenceNode::parser_GetStaticStructure()) {

	} else if (info == SubnetworkNode::parser_GetStaticStructure()) {

	}

	return 0;
}
