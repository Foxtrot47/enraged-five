#pragma once

#include "Nodes.h"

namespace TestDataGenerator
{
	void BuildAbsoluteMoverNetwork();
	void BuildBlender4Network();
	void BuildGenericClipNetwork();
	void BuildGenericTaskNetwork();
	void BuildMainNetwork();
	void BuildMinigameBenchpressNetwork();
	void BuildReferenceNetwork();
}