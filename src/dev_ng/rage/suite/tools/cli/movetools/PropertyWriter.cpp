#include "PropertyWriter.h"

#include "NodeWriter.h"
#include "writer.h"

extern LookupTable* g_LookupTable;
extern int FindIndexOfParameter(const char* parameter);

void PropertyWriter::AppendFilename(Writer* writer, const char* filename)
{
	int length = ((strlen(filename)+1+4) & (~(4-1)));
	writer->AppendI32(length);
	writer->AppendData(filename, length);
}

void PropertyWriter::AppendParameterCrc(Writer* writer, const char* parameter)
{
	u32 hash = atStringHash(parameter);
	writer->AppendU32(hash);
}

void PropertyWriter::AppendAnimation(Writer* writer, const Animation& data)
{
	switch (data.m_Type) {
	case kAnimationAttributeIgnored:
		// Do nothing
		break;
	case kAnimationAttributeFilename:
		{
			AppendFilename(writer, data.m_Filename);
		}
		break;
	case kAnimationAttributeParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendClip(Writer* writer, const Clip& data)
{
	switch (data.m_Type) {
	case kClipAttributeIgnored:
		// Do nothing
		break;
	case kClipAttributeFilename:
		{
			writer->AppendU32( static_cast<u32>(data.m_Context));

			switch( data.m_Context ) {
			case kClipContextVariableClipSet:
			case kClipContextClipDictionary:
			case kClipContextAbsoluteClipSet:
				{
					AppendParameterCrc(writer, data.m_BankName);
					AppendParameterCrc(writer, data.m_ClipName);
				}
				break;
			case kClipContextLocalFile:
				{
					AppendParameterCrc(writer, data.m_ClipName);
				}
				break;
			}
		}
		break;
	case kClipAttributeParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendExpressions(Writer* writer, const Expressions& data)
{
	switch (data.m_Type) {
	case kExpressionsIgnored:
		// Do nothing
		break;
	case kExpressionsFilename:
		{
			atString bankName, clipName;
			if(data.m_Filename.GetLength() > 0)
			{
				atArray< atString > split;
				atString filename = data.m_Filename;
				filename.Split(split, "\\");
				Assert(split.GetCount() == 2);
				if(split.GetCount() == 2)
				{
					AppendParameterCrc(writer, split[0]);
					AppendParameterCrc(writer, split[1]);
				}
				else
				{
					AppendParameterCrc(writer, "");
					AppendParameterCrc(writer, "");
				}
			}
			else
			{
				AppendParameterCrc(writer, data.m_BankName);
				AppendParameterCrc(writer, data.m_ExpressionName);
			}
		}
		break;
	case kExpressionsParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendFilter(Writer* writer, const Filter& data)
{
	switch (data.m_Type) {
	case kFilterIgnored:
		// Do nothing
		break;
	case kFilterFilename:
		{
			atString filterDictionary = data.m_FilterDictionary;
			if(filterDictionary.GetLength() == 0)
			{
				filterDictionary = "player";
			}
			atString filterName = data.m_FilterName;
			if(filterName.EndsWith(".xml"))
			{
				filterName.Replace(".xml", "");
			}
			AppendParameterCrc(writer, filterDictionary);
			AppendParameterCrc(writer, filterName);
		}
		break;
	case kFilterParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendFilterN(Writer* writer, const FilterN& data)
{
	switch (data.m_Type) {
	case kFilterNIgnored:
		// Do nothing
		break;
	case kFilterNParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendFrame(Writer* writer, const Frame& data)
{
	switch (data.m_Type) {
	case kFrameIgnored:
		// Do nothing
		break;
	case kFrameParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendParameterizedMotion(Writer* writer, const ParameterizedMotion& data)
{
	switch (data.m_Type) {
	case kParameterizedMotionIgnored:
		// Do nothing
		break;
	case kParameterizedMotionFilename:
		{
			AppendFilename(writer, data.m_Filename);
		}
		break;
	case kParameterizedMotionParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendFloat(Writer* writer, const Float& data)
{
	switch (data.m_Type) {
	case kF32AttributeIgnored:
		// Do nothing
		break;
	case kF32AttributeValue:
		{
			writer->AppendFloat(data.m_Value);
		}
		break;
	case kF32AttributeParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendBool(Writer* writer, const Bool& data)
{
	switch (data.m_Type) {
	case kBoolAttributeIgnored:
		// Do nothing
		break;
	case kBoolAttributeValue:
		{
			writer->AppendBool(data.m_Value);
		}
		break;
	case kBoolAttributeParameter:
		{
#if USE_INDICES
			if (g_LookupTable) {
				int index = FindIndexOfParameter(data.m_Parameter.c_str());
				Assert(index >= 0);
				writer->AppendI32(index);
			} else 
#endif // USE_INDICES
			{
				AppendParameterCrc(writer, data.m_Parameter);
			}
		}
		break;
	};
}

void PropertyWriter::AppendOffset(Writer* writer, const char* name, Map& nodes)
{
	const ExportDef** target = nodes.SafeGet(name);
	if (!target) {
		Quitf("Can't have a non-existent node as input: name:%s", name);
	}
	writer->AppendOffset((*target)->label);
}

void PropertyWriter::AppendOperation(Writer* writer, BaseOperator* op)
{
	const parStructure* info = op->parser_GetStructure();
	if (info == PushValueOperator::parser_GetStaticStructure()) {
		writer->AppendU32(1);
		writer->AppendFloat(((PushValueOperator*)op)->m_Value);
	} else if (info == FindAndPushValueOperator::parser_GetStaticStructure()) {
		FindAndPushValueOperator* o = (FindAndPushValueOperator*)op;
		writer->AppendU32(2);
#if USE_INDICES
		if (g_LookupTable) {
			int index = FindIndexOfParameter(o->m_Parameter.c_str());
			Assert(index >= 0);
			writer->AppendI32(index);
		} else 
#endif // USE_INDICES
		{
			writer->AppendStringHash(o->m_Parameter.c_str());
		}
	} else if (info == AddOperator::parser_GetStaticStructure()) {
		writer->AppendU32(3);
		writer->AppendU32(0);
	} else if (info == MultiplyOperator::parser_GetStaticStructure()) {
		writer->AppendU32(4);
		writer->AppendU32(0);
	} else if (info == CurveOperator::parser_GetStaticStructure()) {
		CurveOperator* o = (CurveOperator*)op;
		writer->AppendU32(5);
		Writer::Identifier padding = Writer::ReserveLabel();
		writer->AppendOffset(padding);
		writer->AppendLabel(padding);
		writer->AppendFloat(o->m_MinClamp);
		writer->AppendFloat(o->m_MaxClamp);
		writer->AppendI32(o->m_Keyframes.size());
		Writer::Identifier keyframes = Writer::ReserveLabel();
		writer->AppendOffset(keyframes);
		writer->AppendLabel(keyframes);
		for (int i = 0; i < o->m_Keyframes.size(); ++i) {
			writer->AppendU32(0); // place holder for type, at the moment this represents kcurveLinear
			writer->AppendFloat(o->m_Keyframes[i].m_Key);
			writer->AppendFloat(o->m_Keyframes[i].m_M);
			writer->AppendFloat(o->m_Keyframes[i].m_B);
		}
	}
}
