#pragma once

#include "ExportDef.h"
#include "nodes.h"

class Writer;

enum ParameterType
{
	kParameterTypeAnimation,
	kParameterTypeClip,
	kParameterTypeExpressions,
	kParameterTypeFilter,
	kParameterTypeFilterN,
	kParameterTypeFrame,
	kParameterTypeParameterizedMotion,
	kParameterTypeBoolean,
	kParameterTypeReal,
	kParameterTypeInt,
	kParameterTypeData,
	kParameterTypeNode,
	kParameterTypeObserver,
	kParameterTypeNetwork,
	kParameterTypeReference,
	kParameterTypeProperty,
	kParameterTypeNodeId,
	kParameterTypeNodeName,

	kParameterTypeCount,
};

struct LookupTableItem
{
	atString name;
	ParameterType type;
};

typedef atArray<LookupTableItem> LookupTable;

namespace NodeWriter
{
	void WriteAnimationNode(Writer* writer, const AnimationNode* n, int index);
	void WriteAddNode(Writer* writer, const AddNode* n, Map& nodes, int index);
	void WriteNwayAddNode(Writer* writer, const AddNwayNode* n, Map& nodes, int index);
	void WriteBlendNode(Writer* writer, const BlendNode* n, Map& nodes, int index);
	void WriteBlendNwayNode(Writer* writer, const BlendNwayNode* n, Map& nodes, int index);
	void WriteClipNode(Writer* writer, const ClipNode* n, int index);
	void WriteCaptureNode(Writer* writer, const CaptureNode* n, Map& nodes, int index);
	void WriteExpressionNode(Writer* writer, const ExpressionNode* n, Map& nodes, int index);
	void WriteExtrapolateNode(Writer* writer, const ExtrapolateNode* n, Map& nodes,int index);
	void WriteFilterNode(Writer* writer, const FilterNode* n, Map& nodes, int index);
	void WriteFrameNode(Writer* writer, const FrameNode* n, int index);
	void WriteIdentityNode(Writer* writer, const IdentityNode* n, int index);
	void WriteInvalidNode(Writer* writer, const InvalidNode* n, int index);
	void WriteJointLimitNode(Writer* writer, const JointLimitNode* n, Map& nodes, int index);
	void WriteMergeNode(Writer* writer, const MergeNode* n, Map& nodes, int index);
	void WriteMergeNwayNode(Writer* writer, const MergeNWayNode* n, Map& nodes, int index);
	void WriteParameterizedMotionNode(Writer* writer, const ParameterizedMotionNode* n, int index);
	void WritePoseNode(Writer* writer, const PoseNode* n, int index);
	void WriteProxyNode(Writer* writer, const ProxyNode* n, Map& nodes, int index);
	void WriteMotionTreeNode(Writer* writer, const MotionTreeNode* n, const BaseNode* parent, Map& nodes, int index, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flags);
	void WriteStateMachineNode(Writer* writer, const StateMachineNode* n, const BaseNode* parent, Map& nodes, int index, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flagset);
	void WriteTailNode(Writer* writer, const TailNode* n, int index);
	void WriteInlineStateMachineNode(Writer* writer, const InlineStateMachineNode* n, const BaseNode* parent, Map& nodes, int index, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flagset);
	void WriteReferenceNode(Writer* writer, const ReferenceNode* n, int index, atArray<Writer::Identifier>& identifiers, atArray<const char*>& filenames);
	void WriteSubnetworkNode(Writer* writer, const SubnetworkNode* n, int index);

	void AppendTransitions(Writer* writer, std::vector<const Transition*>& transitions, Map& nodes, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flagset);

	u32 GetConditionSize(const Condition* base );
	u16 IndexOf(const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& children, const char* name);
	u16 TypeOf(Map& nodes, const char* node, const char* attribute);
	u16 EventOf(Map& nodes, const char* node, const char* event);
}