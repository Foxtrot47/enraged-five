#include <vector>

#include "system/main.h"
#include "system/param.h"
#include "string/stringutil.h"
#include "atl/map.h"
#include "parser/manager.h"

#include "move/move_types_internal.h"
#include "move/move_config.h"
#include "move/move_version.h"

#include "ExportDef.h"
#include "HashArrayBuilder.h"
#include "Nodes.h"
#include "NodeWriter.h"
#include "TestDataGenerator.h"
#include "Writer.h"

using namespace rage;

struct MotionTreeParentData {
	MotionTreeParentData(const BaseNode* n) : node (n) {}
	atArray<const char*> children;
	const BaseNode* node;
};

static int IndexOf(MotionTreeParentData* indexList, const char* name)
{
	for (int i = 0; i < indexList->children.size(); ++i) {
		if (strcmp(indexList->children[i], name) == 0) {
			return i;
		}
	}
	return -1;
}

void AppendNode(const ExportDef* def, atStringMap<const ExportDef*>& nodes, atStringMap<MotionTreeParentData*>& parents, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& requests, const ::rage::atArray< ::rage::atString, 0, ::rage::u16>& flags,
				atArray<Writer::Identifier>& identifiers, atArray<const char*>& filenames )
{
	Writer* data = def->writer->AppendChild();
	data->AppendLabel(def->label);

	const BaseNode* base = def->node;

	MotionTreeParentData** indexList = parents.SafeGet(base->m_Name);
	Assert(indexList);
	int childIndex = IndexOf(*indexList, base->m_Name);

	const parStructure* info = def->node->parser_GetStructure();
	if (info == AnimationNode::parser_GetStaticStructure()) {
		NodeWriter::WriteAnimationNode(data->AppendChild(), static_cast<const AnimationNode*>(def->node), childIndex);
	} else if (info == AddNode::parser_GetStaticStructure()) {
		NodeWriter::WriteAddNode(data->AppendChild(), static_cast<const AddNode*>(def->node), nodes, childIndex);
	} else if (info == AddNwayNode::parser_GetStaticStructure()) {
		NodeWriter::WriteNwayAddNode(data->AppendChild(), static_cast<const AddNwayNode*>(def->node), nodes, childIndex);
	} else if (info == BlendNode::parser_GetStaticStructure()) {
		NodeWriter::WriteBlendNode(data->AppendChild(), static_cast<const BlendNode*>(base), nodes, childIndex);
	} else if (info == BlendNwayNode::parser_GetStaticStructure()) {
		NodeWriter::WriteBlendNwayNode(data->AppendChild(), static_cast<const BlendNwayNode*>(base), nodes, childIndex);
	} else if (info == ClipNode::parser_GetStaticStructure()) {
		NodeWriter::WriteClipNode(data->AppendChild(), static_cast<const ClipNode*>(base), childIndex);
	} else if (info == CaptureNode::parser_GetStaticStructure()) {
		NodeWriter::WriteCaptureNode(data->AppendChild(), static_cast<const CaptureNode*>(base), nodes, childIndex);
	} else if (info == ExpressionNode::parser_GetStaticStructure()) {
		NodeWriter::WriteExpressionNode(data->AppendChild(), static_cast<const ExpressionNode*>(base), nodes, childIndex);
	} else if (info == ExtrapolateNode::parser_GetStaticStructure()) {
		NodeWriter::WriteExtrapolateNode(data->AppendChild(), static_cast<const ExtrapolateNode*>(base), nodes, childIndex);
	} else if (info == FilterNode::parser_GetStaticStructure()) {
		NodeWriter::WriteFilterNode(data->AppendChild(), static_cast<const FilterNode*>(base), nodes, childIndex);
	} else if (info == FrameNode::parser_GetStaticStructure()) {
		NodeWriter::WriteFrameNode(data->AppendChild(), static_cast<const FrameNode*>(base), childIndex);
	} else if (info == IdentityNode::parser_GetStaticStructure()) {
		NodeWriter::WriteIdentityNode(data->AppendChild(), static_cast<const IdentityNode*>(base), childIndex);
	} else if (info == InvalidNode::parser_GetStaticStructure()) {
		NodeWriter::WriteInvalidNode(data->AppendChild(), static_cast<const InvalidNode*>(base), childIndex);
	} else if (info == JointLimitNode::parser_GetStaticStructure()) {
		NodeWriter::WriteJointLimitNode(data->AppendChild(), static_cast<const JointLimitNode*>(base), nodes, childIndex);
	} else if (info == MergeNode::parser_GetStaticStructure()) {
		NodeWriter::WriteMergeNode(data->AppendChild(), static_cast<const MergeNode*>(base), nodes, childIndex);
	} else if (info == MergeNWayNode::parser_GetStaticStructure()) {
		NodeWriter::WriteMergeNwayNode(data->AppendChild(), static_cast<const MergeNWayNode*>(base), nodes, childIndex);
	} else if (info == ParameterizedMotionNode::parser_GetStaticStructure()) {
		NodeWriter::WriteParameterizedMotionNode(data->AppendChild(), static_cast<const ParameterizedMotionNode*>(base), childIndex);
	} else if (info == PoseNode::parser_GetStaticStructure()) {
		NodeWriter::WritePoseNode(data->AppendChild(), static_cast<const PoseNode*>(base), childIndex);
	} else if (info == ProxyNode::parser_GetStaticStructure()) {
		NodeWriter::WriteProxyNode(data->AppendChild(), static_cast<const ProxyNode*>(base), nodes, childIndex);
	} else if (info == MotionTreeNode::parser_GetStaticStructure()) {
		const MotionTreeNode* n = static_cast<const MotionTreeNode*>(base);
		MotionTreeParentData** mtpd = parents.SafeGet(n->m_Name);
		if (mtpd) {
			const BaseNode* parent = (*mtpd)->node;
			if(parent->parser_GetStructure() == StateMachineNode::parser_GetStaticStructure() ||
				parent->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure() ||
				parent->parser_GetStructure() == InlineStateMachineNode::parser_GetStaticStructure())
			{
				NodeWriter::WriteMotionTreeNode(data->AppendChild(), n, parent, nodes, childIndex, requests, flags);
			}
			else { Assert(false); }
		}
	} else if (info == StateMachineNode::parser_GetStaticStructure()) {
		const StateMachineNode* n = static_cast<const StateMachineNode*>(base);
		MotionTreeParentData** mtpd = parents.SafeGet(n->m_Name);
		if (mtpd) {
			const BaseNode* parent = (*mtpd)->node;
			if(parent->parser_GetStructure() == StateMachineNode::parser_GetStaticStructure() ||
				parent->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure() ||
				parent->parser_GetStructure() == InlineStateMachineNode::parser_GetStaticStructure())
			{
				NodeWriter::WriteStateMachineNode(data->AppendChild(), n, parent, nodes, childIndex, requests, flags);
			}
			else { Assert(false); }
		}
	} else if (info == TailNode::parser_GetStaticStructure()) {
		NodeWriter::WriteTailNode(data->AppendChild(), static_cast<const TailNode*>(base), childIndex);
	} else if (info == InlineStateMachineNode::parser_GetStaticStructure()) {
		const InlineStateMachineNode* n = static_cast<const InlineStateMachineNode*>(base);
		MotionTreeParentData** mtpd = parents.SafeGet(n->m_Name);
		if (mtpd) {
			const BaseNode* parent = (*mtpd)->node;
			if(parent->parser_GetStructure() == StateMachineNode::parser_GetStaticStructure() ||
				parent->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure() ||
				parent->parser_GetStructure() == InlineStateMachineNode::parser_GetStaticStructure())
			{
				NodeWriter::WriteInlineStateMachineNode(data->AppendChild(), n, parent, nodes, childIndex, requests, flags);
			}
			else { Assert(false); }
		}
	} else if (info == ReferenceNode::parser_GetStaticStructure()) {
		NodeWriter::WriteReferenceNode(data->AppendChild(), static_cast<const ReferenceNode*>(base), childIndex, identifiers, filenames);
	} else if (info == SubnetworkNode::parser_GetStaticStructure()) {
		NodeWriter::WriteSubnetworkNode(data->AppendChild(), static_cast<const SubnetworkNode*>(base), childIndex);
	} else {
		// Don't know what type this is!
		Quitf("Unknown node type");
	}
}

static void InsertHierarchyRelationship(const BaseNode* node, atStringMap<MotionTreeParentData*>& parents, atArray< MotionTreeParentData *> &allocated)
{
	const parStructure* info = node->parser_GetStructure();

	if (info == MotionTreeNode::parser_GetStaticStructure()) {
		const MotionTreeNode* n = static_cast<const MotionTreeNode*>(node);
		MotionTreeParentData* data = rage_new MotionTreeParentData(n);
		allocated.PushAndGrow( data );
		data->children.Reserve(n->m_Children.size());
		for (int i = 0; i < n->m_Children.size(); ++i) {
			data->children.Push(n->m_Children[i]);
			parents.Insert(n->m_Children[i], data);
		}
	} else if (info == StateMachineNode::parser_GetStaticStructure()) {
		const StateMachineNode* n = static_cast<const StateMachineNode*>(node);
		MotionTreeParentData* data = rage_new MotionTreeParentData(n);
		allocated.PushAndGrow( data );
		data->children.Reserve(n->m_Children.size());
		for (int i = 0; i < n->m_Children.size(); ++i) {
			data->children.Push(n->m_Children[i]);
			parents.Insert(n->m_Children[i], data);
		}
	} else if (info == InlineStateMachineNode::parser_GetStaticStructure()) {
		const InlineStateMachineNode* n = static_cast<const InlineStateMachineNode*>(node);
		MotionTreeParentData* data = rage_new MotionTreeParentData(n);
		allocated.PushAndGrow( data );
		data->children.Reserve(n->m_Children.size());
		for (int i = 0; i < n->m_Children.size(); ++i) {
			data->children.Push(n->m_Children[i]);
			parents.Insert(n->m_Children[i], data);
		}
	}
}

// Given a base node, iterate its children (if it has any) and add them to the container depth first
static void AppendChildHashesRecursive(const BaseNode* node, std::vector<u32>& nodeHashes, const atStringMap<const ExportDef*>& nodeMap)
{
	u32 nodeHash = atStringHash(node->m_Name);
	nodeHashes.push_back(nodeHash);

	const parStructure* info = node->parser_GetStructure();

	if (info == MotionTreeNode::parser_GetStaticStructure()) 
	{
		const MotionTreeNode* motionTreeNode = static_cast<const MotionTreeNode*>(node);
		for (int i = 0; i < motionTreeNode->m_Children.size(); ++i) 
		{
			const char* childNodeName = motionTreeNode->m_Children[i];
			const BaseNode* childNode = (*nodeMap.SafeGet(childNodeName))->node;
			AppendChildHashesRecursive(childNode, nodeHashes, nodeMap);
		}
	} 
	else if (info == StateMachineNode::parser_GetStaticStructure()) 
	{
		const StateMachineNode* stateMachineNode = static_cast<const StateMachineNode*>(node);
		for (int i = 0; i < stateMachineNode->m_Children.size(); ++i) 
		{
			const char* childNodeName = stateMachineNode->m_Children[i];
			const BaseNode* childNode = (*nodeMap.SafeGet(childNodeName))->node;
			AppendChildHashesRecursive(childNode, nodeHashes, nodeMap);
		}
	}
	else if (info == InlineStateMachineNode::parser_GetStaticStructure()) 
	{
		const InlineStateMachineNode* inlineStateMachineNode = static_cast<const InlineStateMachineNode*>(node);
		for (int i = 0; i < inlineStateMachineNode->m_Children.size(); ++i) 
		{
			const char* childNodeName = inlineStateMachineNode->m_Children[i];
			const BaseNode* childNode = (*nodeMap.SafeGet(childNodeName))->node;
			AppendChildHashesRecursive(childNode, nodeHashes, nodeMap);
		}
	}
}

PARAM(file, "Input XML file");
PARAM(out, "MoVE mrf file");

PARAM(parampath, "Path to write out parameter h/cpp files to. These take on the name [file]ControlParams");

PARAM(useindexes, "Use index lookup rather than hash lookup.");

#define SAVE_ABSOLUTE_MOVER 0
#define SAVE_BLENDER4 0
#define SAVE_GENERIC_CLIP 0
#define SAVE_GENERIC_TASK 0
#define SAVE_MAIN 0
#define SAVE_MINIGAME_BENCHPRESS 0
#define SAVE_REFERENCE 0

#define EXPORT_ABSOLUTE_MOVER 0
#define EXPORT_BLENDER4 0
#define EXPORT_GENERIC_CLIP 0
#define EXPORT_GENERIC_TASK 0
#define EXPORT_MAIN 0
#define EXPORT_MINIGAME_BENCHPRESS 0
#define EXPORT_REFERENCE 0
#define EXPORT_REFERENCE_PARENT 0

static const char* GetFrameworkRuntimeType(ParameterType parameterType)
{
	switch (parameterType)
	{
	case kParameterTypeAnimation:
		return "fwMvAnimId";
	case kParameterTypeClip:
		return "fwMvClipId";
	case kParameterTypeExpressions:
		return "fwMvExpressionsId";
	case kParameterTypeFilter:
		return "fwMvFilterId";
	case kParameterTypeFilterN:
		return "fwMvFilterNId";
	case kParameterTypeFrame:
		return "fwMvFrameId";
	case kParameterTypeParameterizedMotion:
		return "fwMvParaemterizedMotionId";
	case kParameterTypeBoolean:
		return "fwMvBooleanId";
	case kParameterTypeReal:
		return "fwMvFloatId";
	case kParameterTypeInt:
		return "fwMvIntId";
	case kParameterTypeData:
		return "fwMvDataId";
	case kParameterTypeNode:
		return "fwMvNodeId";
	case kParameterTypeObserver:
		return "fwMvObserverId";
	case kParameterTypeNetwork:
		return "fwMvNetworkId";
	case kParameterTypeReference:
		return "fwMvReferenceId";
	case kParameterTypeProperty:
		return "fwMvPropertyId";
	case kParameterTypeNodeId:
		return "fwMvNodeId";
	case kParameterTypeNodeName:
		return "fwMvNodeNameId";
	default:
		return "Unsupported Type";
	}
}

static ParameterType GetParameterType(const BaseNode* node, const char* attribute)
{
	const parStructure* info = node->parser_GetStructure();
	if (info == AnimationNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Animation") == 0) {
			return kParameterTypeAnimation;
		} 
		else if (strcmp(attribute, "Phase") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "Rate") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "Delta") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "Looped") == 0) {
			return kParameterTypeBoolean;
		}
		else if (strcmp(attribute, "Absolute") == 0) {
			return kParameterTypeBoolean;
		}
	} else if (info == AddNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Filter") == 0) {
			return kParameterTypeFilter;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return kParameterTypeReal;
		}
	} else if (info == AddNwayNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "FilterN") == 0) {
			return kParameterTypeFilterN;
		}
		else if (strcmp(attribute, "Filter") == 0) {
			return kParameterTypeFilter;
		}
		else if (strcmp(attribute, "InputFilter") == 0) {
			return kParameterTypeFilter;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return kParameterTypeReal;
		}
	} else if (info == BlendNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Filter") == 0) {
			return kParameterTypeFilter;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return kParameterTypeReal;
		}
	} else if (info == BlendNwayNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "FilterN") == 0) {
			return kParameterTypeFilterN;
		}
		else if (strcmp(attribute, "Filter") == 0) {
			return kParameterTypeFilter;
		}
		else if (strcmp(attribute, "InputFilter") == 0) {
			return kParameterTypeFilter;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return kParameterTypeReal;
		}
	} else if (info == ClipNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Clip") == 0) {
			return kParameterTypeClip;
		}
		else if (strcmp(attribute, "Phase") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "Rate") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "Delta") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "Looped") == 0) {
			return kParameterTypeBoolean;
		}
	} else if (info == ExpressionNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Expressions") == 0) {
			return kParameterTypeExpressions;
		} else if (strcmp(attribute, "Weight") == 0) {
			return kParameterTypeReal;
		}
	} else if (info == ExtrapolateNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Damping") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "DeltaTime") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "LastFrame") == 0) {
			return kParameterTypeFrame;
		}
		else if (strcmp(attribute, "OwnLastFrame") == 0) {
			return kParameterTypeBoolean;
		}
		else if (strcmp(attribute, "DeltaFrame") == 0) {
			return kParameterTypeFrame;
		}
		else if (strcmp(attribute, "OwnDeltaFrame") == 0) {
			return kParameterTypeBoolean;
		}
	} else if (info == FilterNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Filter") == 0) {
			return kParameterTypeFilter;
		}
	} else if (info == FrameNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Frame") == 0) {
			return kParameterTypeFrame;
		}
		else if (strcmp(attribute, "Owner") == 0) {
			return kParameterTypeBoolean;
		}
	} else if (info == IdentityNode::parser_GetStaticStructure()) {

	} else if (info == MergeNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "Filter") == 0) {
			return kParameterTypeFilter;
		}
	} else if (info == MergeNWayNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "FilterN") == 0) {
			return kParameterTypeFilterN;
		}
		else if (strcmp(attribute, "Filter") == 0) {
			return kParameterTypeFilter;
		}
		else if (strcmp(attribute, "InputFilter") == 0) {
			return kParameterTypeFilter;
		}
		else if (strcmp(attribute, "Weight") == 0) {
			return kParameterTypeReal;
		}
	} else if (info == ParameterizedMotionNode::parser_GetStaticStructure()) {
		if (strcmp(attribute, "ParameterizedMotion") == 0) {
			return kParameterTypeParameterizedMotion;
		}
		else if (strcmp(attribute, "Phase") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "Delta") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "Rate") == 0) {
			return kParameterTypeReal;
		}
		else if (strcmp(attribute, "ParameterValue") == 0) {
			return kParameterTypeReal;
		}
	} else if (info == PoseNode::parser_GetStaticStructure()) {

	} else if (info == ProxyNode::parser_GetStaticStructure()) {

	} else if (info == ReferenceNode::parser_GetStaticStructure()) {

	} else if (info == SubnetworkNode::parser_GetStaticStructure()) {

	}

	return kParameterTypeCount;
}

template <class T>
struct FindParameterByName
{
	FindParameterByName(const char* name)
		: m_Name (name) {}

	const char* m_Name;

	bool operator() (const T*& controller) const
	{
		return strcmp(controller->m_Parameter.c_str(), m_Name) == 0;
	}
};

struct FindByName
{
	FindByName(const char* name)
		: m_Name (name) {}

	const char* m_Name;

	bool operator() (const char* str) const
	{
		return strcmp(str, m_Name) == 0;
	}
};

struct FindEventByName
{
	FindEventByName(const char* name)
		: m_Name (name) {}

	const char* m_Name;

	bool operator() (const EventController*& controller) const
	{
		return strcmp(controller->m_Event.c_str(), m_Name) == 0;
	}
};

template <typename T>
static void PushParameter(const char* parameter, T& parameters, atArray<const ParameterController*>& controllers)
{
	if (parameters.end() == std::find_if(parameters.begin(), parameters.end(), FindByName(parameter)) && 
		controllers.end() == std::find_if(controllers.begin(), controllers.end(), FindParameterByName<ParameterController>(parameter))) {
		parameters.PushAndGrow(parameter);
	}
}

static void FindParametersFromTransitions(const Transition& transition, /*const*/atArray<const ParameterController*>& parameters, atArray<const char*>& boolParameters, atArray<const char*>& floatParameters, atArray<const EventController*>& events, atArray<const char*>& fireableEvents)
{
	if (transition.m_DurationParameter.GetLength()) {
		PushParameter(transition.m_DurationParameter.c_str(), floatParameters, parameters);
	}
	if (transition.m_TransitionWeightOutputParameter.GetLength()) {
		PushParameter(transition.m_TransitionWeightOutputParameter.c_str(), floatParameters, parameters);
	}
	for (int c=0; c<transition.m_Conditions.GetCount(); ++c) {
		const Condition* condition = transition.m_Conditions[c];
		parStructure* structure = condition->parser_GetStructure();
		if (structure == AtEvent::parser_GetStaticStructure()) {
			const AtEvent* c = static_cast<const AtEvent*>(condition);
			if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(c->m_Event.c_str())) &&
				fireableEvents.end() == std::find_if(fireableEvents.begin(), fireableEvents.end(), FindByName(c->m_Event.c_str()))) {
					fireableEvents.PushAndGrow(c->m_Event.c_str());
			}
		} else if (structure == BoolEquals::parser_GetStaticStructure()) {
			const BoolEquals* c = static_cast<const BoolEquals*>(condition);
			PushParameter(c->m_Parameter.c_str(), boolParameters, parameters);
		} else if (structure == GreaterThan::parser_GetStaticStructure()) {
			const GreaterThan* c = static_cast<const GreaterThan*>(condition);
			PushParameter(c->m_Parameter.c_str(), floatParameters, parameters);
		} else if (structure == GreaterThanEqual::parser_GetStaticStructure()) {
			const GreaterThanEqual* c = static_cast<const GreaterThanEqual*>(condition);
			PushParameter(c->m_Parameter.c_str(), floatParameters, parameters);
		} else if (structure == InRange::parser_GetStaticStructure()) {
			const InRange* c = static_cast<const InRange*>(condition);
			PushParameter(c->m_Parameter.c_str(), floatParameters, parameters);
		} else if (structure == NotInRange::parser_GetStaticStructure()) {
			const NotInRange* c = static_cast<const NotInRange*>(condition);
			PushParameter(c->m_Parameter.c_str(), floatParameters, parameters);
		} else if (structure == LessThan::parser_GetStaticStructure()) {
			const LessThan* c = static_cast<const LessThan*>(condition);
			PushParameter(c->m_Parameter.c_str(), floatParameters, parameters);
		} else if (structure == LessThanEqual::parser_GetStaticStructure()) {
			const LessThanEqual* c = static_cast<const LessThanEqual*>(condition);
			PushParameter(c->m_Parameter.c_str(), floatParameters, parameters);
		} else if (structure == OnRequest::parser_GetStaticStructure()) {
			// Don't need to do anything here... all the requests are already known
		}
	}
}

typedef atArray<const ParameterController*> ParameterArray;
typedef atArray<const EventController*> EventArray;
typedef atArray<const OutputController*> OutputArray;
typedef atArray<const SubnetworkNode*> SubnetworkArray;
typedef atArray<const char*> TransitionParameterArray;
typedef atArray<const char*> FireableEventArray;
typedef atArray<const char*> NodeIdArray;

struct ParameterEntry
{
	const char*		name;
	ParameterType	type;
};

typedef atArray<ParameterEntry> ParameterEntryArray;

struct FindParameterEntry
{
	FindParameterEntry(const char* name)
		: m_Name (name) {}

	const char* m_Name;

	bool operator() (const ParameterEntry& entry) const
	{
		return strcmp(entry.name, m_Name) == 0;
	}
};


struct ControlParameterInfo
{
	ParameterArray parameters;
	EventArray events;
	OutputArray outputs;
	SubnetworkArray subnetworks;
	TransitionParameterArray floats;
	TransitionParameterArray bools;
	FireableEventArray fireables;
	ParameterEntryArray entries;
	NodeIdArray ids; // Because nodes can be 'forced'
};

static ControlParameterInfo* g_ControlParameterInfo = NULL;

LookupTable* g_LookupTable = NULL;

int FindIndexOfParameter(const char* parameter)
{
	for (int i=0, count=g_LookupTable->GetCount(); i<count; ++i) {
		const LookupTableItem* item = &((*g_LookupTable)[i]);
		if (strcmp(item->name.c_str(), parameter) == 0) {
			return i;
		}
	}

	return -1;
}

static void InsertParameterEntry(ParameterEntryArray& entries, const char* name, ParameterType type, ParameterArray& parameters, OutputArray& outputs, TransitionParameterArray& floats, TransitionParameterArray& bools)
{
	if (outputs.end() == std::find_if(outputs.begin(), outputs.end(), FindParameterByName<OutputController>(name)) &&
		parameters.end() == std::find_if(parameters.begin(), parameters.end(), FindParameterByName<ParameterController>(name)) &&
		entries.end() == std::find_if(entries.begin(), entries.end(), FindParameterEntry(name))) {
			bool check = true;
			if (type == kParameterTypeReal) {
				check = (floats.end() == std::find_if(floats.begin(), floats.end(), FindByName(name)));
			} else if (type == kParameterTypeBoolean) {
				check = (bools.end() == std::find_if(bools.begin(), bools.end(), FindByName(name)));
			}

			if (check) {
				ParameterEntry& entry = entries.Grow();
				entry.name = name;
				entry.type = type;
			}
	}
}

static void DiscoverControlParameterInfo(atStringMap<const ExportDef*>& nodes)
{
	Assert(!g_ControlParameterInfo);
	g_ControlParameterInfo = rage_new ControlParameterInfo;

	// Write out optional header/cpp file
	atArray<const ParameterController*> parameters;
	atArray<const EventController*> events;
	atArray<const OutputController*> outputs;
	atArray<const SubnetworkNode*> subnetworks;
	atArray<const char*> fireableEvents;

	for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
		const BaseNode* node = (*iter)->node;
		if (node->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure()) {
			const MotionTreeNode* motionTreeNode = static_cast<const MotionTreeNode*>(node);
			for (int i=0; i<motionTreeNode->m_Parameters.GetCount(); ++i) {
				if (parameters.end() == std::find_if(parameters.begin(), parameters.end(), FindParameterByName<ParameterController>(motionTreeNode->m_Parameters[i].m_Parameter.c_str()))) {
					parameters.PushAndGrow(&motionTreeNode->m_Parameters[i]);
				}
			}
			for (int i=0; i<motionTreeNode->m_Events.GetCount(); ++i) {
				if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(motionTreeNode->m_Events[i].m_Event.c_str()))) {
					events.PushAndGrow(&motionTreeNode->m_Events[i]);
				}
			}

			if (motionTreeNode->m_OnEnterEvent.m_Enabled) {
				if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(motionTreeNode->m_OnEnterEvent.m_Id.c_str())) &&
					fireableEvents.end() == std::find_if(fireableEvents.begin(), fireableEvents.end(), FindByName(motionTreeNode->m_OnEnterEvent.m_Id.c_str()))) {
						fireableEvents.PushAndGrow(motionTreeNode->m_OnEnterEvent.m_Id.c_str());
				}
			}

			if (motionTreeNode->m_OnExitEvent.m_Enabled) {
				if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(motionTreeNode->m_OnExitEvent.m_Id.c_str())) &&
					fireableEvents.end() == std::find_if(fireableEvents.begin(), fireableEvents.end(), FindByName(motionTreeNode->m_OnExitEvent.m_Id.c_str()))) {
						fireableEvents.PushAndGrow(motionTreeNode->m_OnExitEvent.m_Id.c_str());
				}
			}

		}
		if (node->parser_GetStructure() == StateMachineNode::parser_GetStaticStructure()) {
			const StateMachineNode* stateMachineNode = static_cast<const StateMachineNode*>(node);
			if (stateMachineNode->m_OnEnterEvent.m_Enabled) {
				if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(stateMachineNode->m_OnEnterEvent.m_Id.c_str())) &&
					fireableEvents.end() == std::find_if(fireableEvents.begin(), fireableEvents.end(), FindByName(stateMachineNode->m_OnEnterEvent.m_Id.c_str()))) {
						fireableEvents.PushAndGrow(stateMachineNode->m_OnEnterEvent.m_Id.c_str());
				}
			}

			if (stateMachineNode->m_OnExitEvent.m_Enabled) {
				if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(stateMachineNode->m_OnExitEvent.m_Id.c_str())) &&
					fireableEvents.end() == std::find_if(fireableEvents.begin(), fireableEvents.end(), FindByName(stateMachineNode->m_OnExitEvent.m_Id.c_str()))) {
						fireableEvents.PushAndGrow(stateMachineNode->m_OnExitEvent.m_Id.c_str());
				}
			}
		}
		if (node->parser_GetStructure() == InlineStateMachineNode::parser_GetStaticStructure()) {
			const InlineStateMachineNode* inlineStateMachineNode = static_cast<const InlineStateMachineNode*>(node);
			if (inlineStateMachineNode->m_OnEnterEvent.m_Enabled) {
				if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(inlineStateMachineNode->m_OnEnterEvent.m_Id.c_str())) &&
					fireableEvents.end() == std::find_if(fireableEvents.begin(), fireableEvents.end(), FindByName(inlineStateMachineNode->m_OnEnterEvent.m_Id.c_str()))) {
						fireableEvents.PushAndGrow(inlineStateMachineNode->m_OnEnterEvent.m_Id.c_str());
				}
			}

			if (inlineStateMachineNode->m_OnExitEvent.m_Enabled) {
				if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(inlineStateMachineNode->m_OnExitEvent.m_Id.c_str())) &&
					fireableEvents.end() == std::find_if(fireableEvents.begin(), fireableEvents.end(), FindByName(inlineStateMachineNode->m_OnExitEvent.m_Id.c_str()))) {
						fireableEvents.PushAndGrow(inlineStateMachineNode->m_OnExitEvent.m_Id.c_str());
				}
			}
		}
		if (node->parser_GetStructure() == SubnetworkNode::parser_GetStaticStructure()) {
			subnetworks.PushAndGrow((const SubnetworkNode*)node);
		}
	}

	// Do a second pass over the nodes, now checking the parameters in the transitions.
	// Easier than trying to remove the duplicates across lists.

	atArray<const char*> floatParameters; // parameters that we find by inspecting transitions
	atArray<const char*> boolParameters; // parameters that we find by inspecting transitions

	for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
		const BaseNode* node = (*iter)->node;
		if (node->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure()) {
			const MotionTreeNode* motionTreeNode = static_cast<const MotionTreeNode*>(node);
			for (int i=0; i<motionTreeNode->m_Transitions.GetCount(); ++i) {
				const Transition& transition = motionTreeNode->m_Transitions[i];
				FindParametersFromTransitions(transition, parameters, boolParameters, floatParameters, events, fireableEvents);
			}


		}
		else if (node->parser_GetStructure() == StateMachineNode::parser_GetStaticStructure()) {
			const StateMachineNode* stateMachineNode = static_cast<const StateMachineNode*>(node);
			for (int i=0; i<stateMachineNode->m_Transitions.GetCount(); ++i) {
				const Transition& transition = stateMachineNode->m_Transitions[i];
				FindParametersFromTransitions(transition, parameters, boolParameters, floatParameters, events, fireableEvents);
			}
		}
		else if (node->parser_GetStructure() == InlineStateMachineNode::parser_GetStaticStructure()) {
			const InlineStateMachineNode* stateMachineNode = static_cast<const InlineStateMachineNode*>(node);
			for (int i=0; i<stateMachineNode->m_Transitions.GetCount(); ++i) {
				const Transition& transition = stateMachineNode->m_Transitions[i];
				FindParametersFromTransitions(transition, parameters, boolParameters, floatParameters, events, fireableEvents);
			}
		}
	}
		
	// Do a third pass collecting up all the output parameters, making sure no duplicates are present

	for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
		const BaseNode* node = (*iter)->node;
		if (node->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure()) {
			const MotionTreeNode* motionTreeNode = static_cast<const MotionTreeNode*>(node);

			for (int i=0; i<motionTreeNode->m_Outputs.GetCount(); ++i) {
				if (outputs.end() == std::find_if(outputs.begin(), outputs.end(), FindParameterByName<OutputController>(motionTreeNode->m_Outputs[i].m_Parameter.c_str())) &&
					parameters.end() == std::find_if(parameters.begin(), parameters.end(), FindParameterByName<ParameterController>(motionTreeNode->m_Outputs[i].m_Parameter.c_str())) &&
					floatParameters.end() == std::find_if(floatParameters.begin(), floatParameters.end(), FindByName(motionTreeNode->m_Outputs[i].m_Parameter.c_str())) &&
					boolParameters.end() == std::find_if(boolParameters.begin(), boolParameters.end(), FindByName(motionTreeNode->m_Outputs[i].m_Parameter.c_str()))) {
						outputs.PushAndGrow(&motionTreeNode->m_Outputs[i]);
				}
			}
		}
	}

	NodeIdArray nodeIds;

	for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
		const BaseNode* node = (*iter)->node;
		if (node->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure()) {
			const MotionTreeNode* motionTreeNode = static_cast<const MotionTreeNode*>(node);

			for (int i=0; i<motionTreeNode->m_Operators.GetCount(); ++i) {
				const OperatorController& operatorController = motionTreeNode->m_Operators[i];
				for (int o=0; o<operatorController.m_Operations.GetCount(); ++o) {
					const BaseOperator* op = operatorController.m_Operations[o];
					if (op->parser_GetStructure() == FindAndPushValueOperator::parser_GetStaticStructure()) {
						const FindAndPushValueOperator* oper = static_cast<const FindAndPushValueOperator*>(op);
						if (outputs.end() == std::find_if(outputs.begin(), outputs.end(), FindParameterByName<OutputController>(oper->m_Parameter.c_str())) &&
							parameters.end() == std::find_if(parameters.begin(), parameters.end(), FindParameterByName<ParameterController>(oper->m_Parameter.c_str())) &&
							floatParameters.end() == std::find_if(floatParameters.begin(), floatParameters.end(), FindByName(oper->m_Parameter.c_str()))) {
								floatParameters.PushAndGrow(oper->m_Parameter.c_str());
						}
					}
				}
			}
		} 

		// TODO: Ideally we'd only use the node names that we know we need!
		if (node->parser_GetStructure() == StateMachineNode::parser_GetStaticStructure() ||
//			node->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure() || /* MotionTreeNode can't be used to force because it won't have states inside it! */
			node->parser_GetStructure() == InlineStateMachineNode::parser_GetStaticStructure()) {
			if (nodeIds.end() == std::find_if(nodeIds.begin(), nodeIds.end(), FindByName(node->m_Name.c_str()))) {
				nodeIds.PushAndGrow(node->m_Name.c_str());
			}
		}
	}

	ParameterEntryArray entries; // lame name!

	// We must go through every node and check it for signals too
	for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
		const BaseNode* node = (*iter)->node;
		if (node->parser_GetStructure() == AnimationNode::parser_GetStaticStructure()) {
			const AnimationNode* n = static_cast<const AnimationNode*>(node);
			if (n->m_Animation.m_Type == kAnimationAttributeParameter) {
				InsertParameterEntry(entries, n->m_Animation.m_Parameter.c_str(), kParameterTypeAnimation, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Phase.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Phase.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Rate.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Rate.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Delta.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Delta.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Looped.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_Looped.m_Parameter.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Absolute.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_Absolute.m_Parameter.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
		}
		if (node->parser_GetStructure() == AddNode::parser_GetStaticStructure()) {
			const AddNode* n = static_cast<const AddNode*>(node);
			if (n->m_Filter.m_Type == kFilterParameter) {
				InsertParameterEntry(entries, n->m_Filter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Weight.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Weight.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Synchronizer.m_Type == ST_TAG) {
				// TODO?
			}
		}
		if (node->parser_GetStructure() == AddNwayNode::parser_GetStaticStructure()) {
			const AddNwayNode* n = static_cast<const AddNwayNode*>(node);
			if (n->m_FilterN.m_Type == kFilterNParameter) {
				InsertParameterEntry(entries, n->m_FilterN.m_Parameter.c_str(), kParameterTypeFilterN, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Filter.m_Type == kFilterParameter) {
				InsertParameterEntry(entries, n->m_Filter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_ZeroDestination.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_ZeroDestination.m_Parameter.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Synchronizer.m_Type == ST_TAG) {
				// TODO?
			}
			for (int c=0, count=n->m_Children.GetCount(); c<count; ++c) {
				if (n->m_Children[c].m_Weight.m_Type == kF32AttributeParameter) {
					InsertParameterEntry(entries, n->m_Children[c].m_Weight.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
				}
				if (n->m_Children[c].m_InputFilter.m_Type == kFilterParameter) {
					InsertParameterEntry(entries, n->m_Children[c].m_InputFilter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
				}
			}
		}
		if (node->parser_GetStructure() == BlendNode::parser_GetStaticStructure()) {
			const BlendNode* n = static_cast<const BlendNode*>(node);
			if (n->m_Filter.m_Type == kFilterParameter) {
				InsertParameterEntry(entries, n->m_Filter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Weight.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Weight.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Synchronizer.m_Type == ST_TAG) {
				// TODO?
			}
		}
		if (node->parser_GetStructure() == BlendNwayNode::parser_GetStaticStructure()) {
			const BlendNwayNode* n = static_cast<const BlendNwayNode*>(node);
			if (n->m_FilterN.m_Type == kFilterNParameter) {
				InsertParameterEntry(entries, n->m_FilterN.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Filter.m_Type == kFilterParameter) {
				InsertParameterEntry(entries, n->m_Filter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_ZeroDestination.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_ZeroDestination.m_Parameter.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Synchronizer.m_Type == ST_TAG) {
				// TODO?
			}
			for (int c=0, count=n->m_Children.GetCount(); c<count; ++c) {
				if (n->m_Children[c].m_Weight.m_Type == kF32AttributeParameter) {
					InsertParameterEntry(entries, n->m_Children[c].m_Weight.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
				}
				if (n->m_Children[c].m_InputFilter.m_Type == kFilterParameter) {
					InsertParameterEntry(entries, n->m_Children[c].m_InputFilter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
				}
			}
		}
		if (node->parser_GetStructure() == ClipNode::parser_GetStaticStructure()) {
			const ClipNode* n = static_cast<const ClipNode*>(node);
			if (n->m_Clip.m_Type == kClipAttributeParameter) {
				InsertParameterEntry(entries, n->m_Clip.m_Parameter.c_str(), kParameterTypeClip, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Phase.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Phase.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Rate.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Rate.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Delta.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Delta.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Looped.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_Looped.m_Parameter.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_ZeroWeightSync.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_ZeroWeightSync.m_Parameter.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_ClipLooped.m_Enabled && n->m_ClipLooped.m_Id.c_str() != NULL) {
				// TODO: check that it's not already in the events list!
				InsertParameterEntry(entries, n->m_ClipLooped.m_Id.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_ClipEnded.m_Enabled && n->m_ClipEnded.m_Id.c_str() != NULL) {
				// TODO: Check that it's not already in the events list!
				InsertParameterEntry(entries, n->m_ClipEnded.m_Id.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
		}
		if (node->parser_GetStructure() == CaptureNode::parser_GetStaticStructure()) {
			const CaptureNode* n = static_cast<const CaptureNode*>(node);
			if (n->m_Frame.m_Type == kFrameParameter) {
				InsertParameterEntry(entries, n->m_Frame.m_Parameter.c_str(), kParameterTypeFrame, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Owner.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_Owner.m_Parameter.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
		}
		if (node->parser_GetStructure() == ExpressionNode::parser_GetStaticStructure()) {
			const ExpressionNode* n = static_cast<const ExpressionNode*>(node);
			if (n->m_Expressions.m_Type == kExpressionsParameter) {
				InsertParameterEntry(entries, n->m_Expressions.m_Parameter.c_str(), kParameterTypeExpressions, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Weight.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Weight.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			for (int c=0, count=n->m_Variables.GetCount(); c<count; ++c) {
				if (n->m_Variables[c].m_Value.m_Type == kF32AttributeParameter) {
					InsertParameterEntry(entries, n->m_Variables[c].m_Value.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
				}
			}
		}
		if (node->parser_GetStructure() == ExtrapolateNode::parser_GetStaticStructure()) {
			const ExtrapolateNode* n = static_cast<const ExtrapolateNode*>(node);
			if (n->m_Damping.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Damping.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
		}
		if (node->parser_GetStructure() == FilterNode::parser_GetStaticStructure()) {
			const FilterNode* n = static_cast<const FilterNode*>(node);
			if (n->m_Filter.m_Type == kFilterParameter) {
				InsertParameterEntry(entries, n->m_Filter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
			}
		}
		if (node->parser_GetStructure() == FrameNode::parser_GetStaticStructure()) {
			const FrameNode* n = static_cast<const FrameNode*>(node);
			if (n->m_Frame.m_Type == kFrameParameter) {
				InsertParameterEntry(entries, n->m_Frame.m_Parameter.c_str(), kParameterTypeFrame, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Owner.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_Owner.m_Parameter.c_str(), kParameterTypeBoolean, parameters, outputs, floatParameters, boolParameters);
			}
		}
		if (node->parser_GetStructure() == IdentityNode::parser_GetStaticStructure()) {
			// Nothing
		}
		if (node->parser_GetStructure() == InvalidNode::parser_GetStaticStructure()) {
			// Nothing
		}
		if (node->parser_GetStructure() == JointLimitNode::parser_GetStaticStructure()) {
			// Nothing
		}
		if (node->parser_GetStructure() == MergeNode::parser_GetStaticStructure()) {
			const MergeNode* n = static_cast<const MergeNode*>(node);
			if (n->m_Filter.m_Type == kFilterParameter) {
				InsertParameterEntry(entries, n->m_Filter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Synchronizer.m_Type == ST_TAG) {
				// TODO?
			}
		}
		if (node->parser_GetStructure() == MergeNWayNode::parser_GetStaticStructure()) {
			const MergeNWayNode* n = static_cast<const MergeNWayNode*>(node);
			if (n->m_FilterN.m_Type == kFilterNParameter) {
				InsertParameterEntry(entries, n->m_FilterN.m_Parameter.c_str(), kParameterTypeFilterN, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Filter.m_Type == kFilterParameter) {
				InsertParameterEntry(entries, n->m_Filter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_ZeroDestination.m_Type == kBoolAttributeParameter) {
				InsertParameterEntry(entries, n->m_ZeroDestination.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Synchronizer.m_Type == ST_TAG) {
				// TODO?
			}
			for (int c=0, count=n->m_Children.GetCount(); c<count; ++c) {
				if (n->m_Children[c].m_Weight.m_Type == kF32AttributeParameter) {
					InsertParameterEntry(entries, n->m_Children[c].m_Weight.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
				}
				if (n->m_Children[c].m_InputFilter.m_Type == kFilterParameter) {
					InsertParameterEntry(entries, n->m_Children[c].m_InputFilter.m_Parameter.c_str(), kParameterTypeFilter, parameters, outputs, floatParameters, boolParameters);
				}
			}
		}
		if (node->parser_GetStructure() == ParameterizedMotionNode::parser_GetStaticStructure()) {
			const ParameterizedMotionNode* n = static_cast<const ParameterizedMotionNode*>(node);
			if (n->m_ParameterizedMotion.m_Type == kParameterizedMotionParameter) {
				InsertParameterEntry(entries, n->m_ParameterizedMotion.m_Parameter.c_str(), kParameterTypeParameterizedMotion, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Phase.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Phase.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Delta.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Delta.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			if (n->m_Rate.m_Type == kF32AttributeParameter) {
				InsertParameterEntry(entries, n->m_Rate.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
			}
			for (int p=0, count=n->m_Parameters.GetCount(); p<count; ++p) {
				const Float& param = n->m_Parameters[p];
				if (param.m_Type == kF32AttributeParameter) {
					InsertParameterEntry(entries, param.m_Parameter.c_str(), kParameterTypeReal, parameters, outputs, floatParameters, boolParameters);
				}
			}
		}
		if (node->parser_GetStructure() == PoseNode::parser_GetStaticStructure()) {
			// Nothing
		}
		if (node->parser_GetStructure() == ProxyNode::parser_GetStaticStructure()) {
			// Nothing
		}
	}

	g_ControlParameterInfo->parameters = parameters;
	g_ControlParameterInfo->events = events;
	g_ControlParameterInfo->outputs = outputs;
	g_ControlParameterInfo->subnetworks = subnetworks;
	g_ControlParameterInfo->floats = floatParameters;
	g_ControlParameterInfo->bools = boolParameters;
	g_ControlParameterInfo->fireables = fireableEvents;
	g_ControlParameterInfo->entries = entries;
	g_ControlParameterInfo->ids = nodeIds;
}

inline bool operator<(const LookupTableItem& a, const LookupTableItem& b)
{
	return atStringHash(a.name) < atStringHash(b.name);
}


int Main() 
{
	INIT_PARSER;

#if SAVE_ABSOLUTE_MOVER
	TestDataGenerator::BuildAbsoluteMoverNetwork();
#elif SAVE_BLENDER4
	TestDataGenerator::BuildBlender4Network();
#elif SAVE_GENERIC_CLIP
	TestDataGenerator::BuildGenericClipNetwork();
#elif SAVE_GENERIC_TASK
	TestDataGenerator::BuildGenericTaskNetwork();
#elif SAVE_MAIN
	TestDataGenerator::BuildMainNetwork();
#elif SAVE_MINIGAME_BENCHPRESS
	TestDataGenerator::BuildMinigameBenchpressNetwork();
#elif SAVE_REFERENCE
	TestDataGenerator::BuildReferenceNetwork();
#else

	const char* filename = NULL;
#if EXPORT_ABSOLUTE_MOVER
	filename = "absolutemover.xml";
#elif EXPORT_BLENDER4
	filename = "blender4.xml";
#elif EXPORT_GENERIC_CLIP
	filename = "genericclip.xml";
#elif EXPORT_GENERIC_TASK
	filename = "generictask.xml";
#elif EXPORT_MAIN
	filename = "main.xml";
#elif EXPORT_MINIGAME_BENCHPRESS
	filename = "MiniGame_BenchPress.xml";
#elif EXPORT_REFERENCE
	filename = "reference.xml";
#elif EXPORT_REFERENCE_PARENT
	filename = "parent.xml";
#else
	if (!PARAM_file.Get(filename)) {
		Quitf("Need a filename to process");
	}
#endif

	const char* outfile = NULL;
#if EXPORT_ABSOLUTE_MOVER
	outfile = "absolutemover.mrf";
#elif EXPORT_BLENDER4
	outfile = "blender4.mrf";
#elif EXPORT_GENERIC_CLIP
	outfile = "genericclip.mrf";
#elif EXPORT_GENERIC_TASK
	outfile = "generictask.mrf";
#elif EXPORT_MAIN
	outfile = "main.mrf";
#elif EXPORT_MINIGAME_BENCHPRESS
	outfile = "MiniGame_BenchPress.mrf";
#elif EXPORT_REFERENCE
	outfile = "reference.mrf";
#elif EXPORT_REFERENCE_PARENT
	outfile = "parent.mrf";
#else
	if (!PARAM_out.Get(outfile)) {
		Quitf("Need an output filename");
	}
#endif

	Network network;
	PARSER.LoadObject(filename, "imvf", network);

	Writer writer;
	Writer* header = writer.AppendChild();

	// write header data
	header->AppendData("MoVE", 4);
	header->AppendI32(int(MOVE_MAJOR_VERSION));
	header->AppendI32(int(MOVE_MINOR_VERSION));
	header->AppendI32(int(MOVE_PATCH_VERSION));
	header->AppendI32(int(MOVE_REVISION_VERSION));

	header->AppendI32(int(0)); // size
	header->AppendI32(int(0)); // string size

	int headerSize = 32;

	header->AppendI32(int(network.m_References.size()));

	for (int i = 0; i < network.m_References.size(); ++i) {
		int length = ((strlen(network.m_References[i]) + 1 + 4) & (~(4 - 1)));
		header->AppendI32(length);
		headerSize += 4;
		header->AppendString(network.m_References[i], length);
		headerSize += length;
	}

	if (network.m_Requests.size() > 32)
	{
		Quitf("Too many requests used in network. Must be 32 or less. '%s'.", filename);
	}
	HashArrayBuilder requestBuilder (network.m_Requests.size() + 1);
	for (int i = 0; i < network.m_Requests.size(); ++i) {
		requestBuilder.Insert(network.m_Requests[i], i);
	}

	if (requestBuilder.GetLength() > 1) {
		header->AppendI32(requestBuilder.GetLength());
		for (int e = 0; e < requestBuilder.GetLength(); ++e) {
			header->AppendU32(requestBuilder.GetEntry(e).m_Key);
			header->AppendU32(requestBuilder.GetEntry(e).m_Index);
		}
	} else {
		header->AppendI32(0);
	}

	if (network.m_Flags.size() > 32)
	{
		Quitf("Too many flags used in network. Must be 32 or less. '%s'.", filename);
	}
	HashArrayBuilder flagBuilder (network.m_Flags.size() + 1);
	for (int i = 0; i < network.m_Flags.size(); ++i) {
		flagBuilder.Insert(network.m_Flags[i], i);
	}

//	Writer* flags = header->AppendChild();

	if (flagBuilder.GetLength() > 1) {
		header->AppendI32(flagBuilder.GetLength());
		for (int e = 0; e < flagBuilder.GetLength(); ++e) {
			header->AppendU32(flagBuilder.GetEntry(e).m_Key);
			header->AppendU32(flagBuilder.GetEntry(e).m_Index);
		}
	} else {
		header->AppendI32(0);
	}

	Writer* data = writer.AppendChild();
	Writer* footer = writer.AppendChild();

	// Load all the nodes into a map for faster lookup
	// 3 passes. Load the node into the network, creating a label. generate the node data, then write it out.
	atStringMap<const ExportDef*> nodes;
	atStringMap<MotionTreeParentData*> parents;
	atArray< MotionTreeParentData * > allocated; // Array used to track all allocations made by InsertHierarchyRelationship
	for (int i = 0; i < network.m_Nodes.size(); ++i) {
		nodes.Insert(network.m_Nodes[i]->m_Name, rage_new ExportDef(network.m_Nodes[i], data));
		InsertHierarchyRelationship(network.m_Nodes[i], parents, allocated);
	}
	nodes.FinishInsertion();
	parents.FinishInsertion();

	if (PARAM_useindexes.Get()) {
		DiscoverControlParameterInfo(nodes); // This will allocate g_ControlParameterInfo;

		int capacity = 
			g_ControlParameterInfo->parameters.GetCount() +
			g_ControlParameterInfo->events.GetCount() +
			g_ControlParameterInfo->outputs.GetCount() +
			g_ControlParameterInfo->subnetworks.GetCount() +
			g_ControlParameterInfo->floats.GetCount() +
			g_ControlParameterInfo->bools.GetCount() +
			g_ControlParameterInfo->ids.GetCount() +
			1 + // move_network_observer
			1 + // csimpleblend_total_blend
			1 + // csimpleblend_combined_events
			1 + // Interruptible
			1 + // CAN_EARLY_OUT_FOR_MOVEMENT
			1 + // AEF_FOOT_HEEL_L
			1 + // AEF_FOOT_HEEL_R
			1 + // MoveEvent
			1 + // Foot
			1 + // Audio
			1 // BLEND_OUT_IDLE_TURN
			;  

		g_LookupTable = rage_new LookupTable(0, capacity);
		
		for (int i=0, count=g_ControlParameterInfo->parameters.GetCount(); i<count; ++i) {
			const ParameterController* parameter = g_ControlParameterInfo->parameters[i];

			const ExportDef** target = nodes.SafeGet(parameter->m_Target);
			Assert(target);

			LookupTableItem& item = g_LookupTable->Grow();
			item.name = parameter->m_Parameter;
			item.type = GetParameterType((*target)->node, parameter->m_Attribute);
		}

		for (int i=0, count=g_ControlParameterInfo->bools.GetCount(); i<count; ++i) {
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = g_ControlParameterInfo->bools[i];
			item.type = kParameterTypeBoolean;
		}

		for (int i=0, count=g_ControlParameterInfo->floats.GetCount(); i<count; ++i) {
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = g_ControlParameterInfo->floats[i];
			item.type = kParameterTypeReal;
		}

		for (int i=0, count=g_ControlParameterInfo->outputs.GetCount(); i<count; ++i) {
			const OutputController* output = g_ControlParameterInfo->outputs[i];

			const ExportDef** target = nodes.SafeGet(output->m_Target);
			Assert(target);

			LookupTableItem& item = g_LookupTable->Grow();
			item.name = output->m_Parameter;
			item.type = GetParameterType((*target)->node, output->m_Attribute);
		}

		for (int i=0, count=g_ControlParameterInfo->subnetworks.GetCount(); i<count; ++i) {
			const SubnetworkNode* node = g_ControlParameterInfo->subnetworks[i];

			LookupTableItem& item = g_LookupTable->Grow();
			item.name = node->m_ParameterName;
			item.type = kParameterTypeNetwork;
		}

		for (int i=0, count=g_ControlParameterInfo->events.GetCount(); i<count; ++i) {
			const EventController* event = g_ControlParameterInfo->events[i];

			LookupTableItem& item = g_LookupTable->Grow();
			item.name = event->m_Event;
			item.type = kParameterTypeBoolean;
		}

		for (int i=0, count=g_ControlParameterInfo->fireables.GetCount(); i<count; ++i) {
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = g_ControlParameterInfo->fireables[i];
			item.type = kParameterTypeBoolean;
		}

		for (int i=0, count=g_ControlParameterInfo->entries.GetCount(); i<count; ++i) {
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = g_ControlParameterInfo->entries[i].name;
			item.type = g_ControlParameterInfo->entries[i].type;
		}
#if 0 // TODO: remove the code that builds the id list. This was a temp hack to get around forced state, now there's a little force state buffer in move that's used instead.
		for (int i=0, count=g_ControlParameterInfo->ids.GetCount(); i<count; ++i) {
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = g_ControlParameterInfo->ids[i];
			item.type = kParameterTypeNodeId;
		}
#endif

		// START hack lookups

		// move_network_observer
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "move_network_observer";
			item.type = kParameterTypeObserver;
		}

		// csimpleblend_total_blend
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "csimpleblend_total_blend";
			item.type = kParameterTypeReal;
		}

		// csimpleblend_combined_events
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "csimpleblend_combined_events";
			item.type = kParameterTypeData;
		}

		// Interruptible
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Interruptible";
			item.type = kParameterTypeBoolean;
		}

		// CAN_EARLY_OUT_FOR_MOVEMENT
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CAN_EARLY_OUT_FOR_MOVEMENT";
			item.type = kParameterTypeBoolean;
		}

		// AEF_FOOT_HEEL_L
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "AEF_FOOT_HEEL_L";
			item.type = kParameterTypeBoolean;
		}

		// AEF_FOOT_HEEL_R
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "AEF_FOOT_HEEL_R";
			item.type = kParameterTypeBoolean;
		}

		// MoveEvent
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "MoveEvent";
			item.type = kParameterTypeProperty;
		}

		// Foot
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Foot";
			item.type = kParameterTypeProperty;
		}

		// Audio
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Audio";
			item.type = kParameterTypeProperty;
		}

		// BLEND_OUT_IDLE_TURN
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BLEND_OUT_IDLE_TURN";
			item.type = kParameterTypeProperty;
		}

		// TakeOff
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "TakeOff";
			item.type = kParameterTypeProperty;
		}

		// Fire
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Fire";
			item.type = kParameterTypeProperty;
		}

		// USE_LEFT_FOOT_TRANSITION
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "USE_LEFT_FOOT_TRANSITION";
			item.type = kParameterTypeProperty;
		}

		// EnterCoverInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "EnterCoverInterrupt";
			item.type = kParameterTypeProperty;
		}

		// TurnInterruptToAim
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "TurnInterruptToAim";
			item.type = kParameterTypeProperty;
		}

		// TurnInterruptToMoving
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "TurnInterruptToMoving";
			item.type = kParameterTypeProperty;
		}

		// TurnInterruptToTurn
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "TurnInterruptToTurn";
			item.type = kParameterTypeProperty;
		}

		// EdgeTurnInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "EdgeTurnInterrupt";
			item.type = kParameterTypeProperty;
		}

		// Interrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Interrupt";
			item.type = kParameterTypeProperty;
		}

		// TurnInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "TurnInterrupt";
			item.type = kParameterTypeProperty;
		}

		// AimIntroInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "AimIntroInterrupt";
			item.type = kParameterTypeProperty;
		}

		// BlendInUpperBodyAim
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BlendInUpperBodyAim";
			item.type = kParameterTypeProperty;
		}

		// BlendOutUpperBodyAim
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BlendOutUpperBodyAim";
			item.type = kParameterTypeProperty;
		}

		// OutroAimInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "OutroAimInterrupt";
			item.type = kParameterTypeProperty;
		}

		// CockInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CockInterrupt";
			item.type = kParameterTypeProperty;
		}

		// ExitLaunch
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "ExitLaunch";
			item.type = kParameterTypeProperty;
		}

		// CanFireWeapon
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CanFireWeapon";
			item.type = kParameterTypeProperty;
		}

		// Detach
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Detach";
			item.type = kParameterTypeProperty;
		}

		// Alternate_Walk_Finished
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Alternate_Walk_Finished";
			item.type = kParameterTypeProperty;
		}

		// Alternate_Idle_Finished
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Alternate_Idle_Finished";
			item.type = kParameterTypeProperty;
		}

		// BLEND_OUT_RUN_TO_WALK
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BLEND_OUT_RUN_TO_WALK";
			item.type = kParameterTypeProperty;
		}

		// BLEND_OUT_STOP
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BLEND_OUT_STOP";
			item.type = kParameterTypeProperty;
		}

		// BLEND_OUT_START
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BLEND_OUT_START";
			item.type = kParameterTypeProperty;
		}

		// BLEND_OUT_WALK_TO_RUN
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BLEND_OUT_WALK_TO_RUN";
			item.type = kParameterTypeProperty;
		}

		// Allow_Direction_Change
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Allow_Direction_Change";
			item.type = kParameterTypeProperty;
		}

		// Start_Locomotion
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Start_Locomotion";
			item.type = kParameterTypeProperty;
		}

		// RunInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "RunInterrupt";
			item.type = kParameterTypeProperty;
		}

		// BLEND_OUT_180
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BLEND_OUT_180";
			item.type = kParameterTypeProperty;
		}

		// Transition_Breakpoint
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Transition_Breakpoint";
			item.type = kParameterTypeProperty;
		}

		// Finished_Idle_Turn
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Finished_Idle_Turn";
			item.type = kParameterTypeProperty;
		}

		// BLEND_OUT_IDLE_INTRO
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BLEND_OUT_IDLE_INTRO";
			item.type = kParameterTypeProperty;
		}

		// Finished_Move_Turn
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Finished_Move_Turn";
			item.type = kParameterTypeProperty;
		}

		// Attach_Rope
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Attach_Rope";
			item.type = kParameterTypeProperty;
		}

		// BlendOut
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "BlendOut";
			item.type = kParameterTypeProperty;
		}

		// SprintStart
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "SprintStart";
			item.type = kParameterTypeProperty;
		}

		// Freewheel_Breakout
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "Freewheel_Breakout";
			item.type = kParameterTypeProperty;
		}

		// ExitSeatInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "ExitSeatInterrupt";
			item.type = kParameterTypeProperty;
		}

		// CreateHelmet
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CreateHelmet";
			item.type = kParameterTypeProperty;
		}

		// WalkStartDetach
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "WalkStartDetach";
			item.type = kParameterTypeProperty;
		}

		// RunStartDetach
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "RunStartDetach";
			item.type = kParameterTypeProperty;
		}

		// StartEngine
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "StartEngine";
			item.type = kParameterTypeProperty;
		}

		// NoDoorInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "NoDoorInterrupt";
			item.type = kParameterTypeProperty;
		}

		// CloseDoorStartEngineInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CloseDoorStartEngineInterrupt";
			item.type = kParameterTypeProperty;
		}

		// CloseDoorShuffleInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CloseDoorShuffleInterrupt";
			item.type = kParameterTypeProperty;
		}

		// CloseDoorInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CloseDoorInterrupt";
			item.type = kParameterTypeProperty;
		}

		// GetInShuffleInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "GetInShuffleInterrupt";
			item.type = kParameterTypeProperty;
		}

		// StartEngineInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "StartEngineInterrupt";
			item.type = kParameterTypeProperty;
		}

		// ShuffleInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "ShuffleInterrupt";
			item.type = kParameterTypeProperty;
		}

		// CriticalFrame
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CriticalFrame";
			item.type = kParameterTypeProperty;
		}

		// StartDescent
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "StartDescent";
			item.type = kParameterTypeProperty;
		}

		// ReleaseAmmoClip
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "ReleaseAmmoClip";
			item.type = kParameterTypeProperty;
		}

		// CreateAmmoClipInLeftHand
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CreateAmmoClipInLeftHand";
			item.type = kParameterTypeProperty;
		}

		// CreateAmmoClip
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CreateAmmoClip";
			item.type = kParameterTypeProperty;
		}

		// ShellEjected
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "ShellEjected";
			item.type = kParameterTypeProperty;
		}

		// HoldIfOutOfAmmo
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "HoldIfOutOfAmmo";
			item.type = kParameterTypeProperty;
		}

		// CreateOrdnance
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "CreateOrdnance";
			item.type = kParameterTypeProperty;
		}

		// StandUpInterrupt
		{
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = "StandUpInterrupt";
			item.type = kParameterTypeProperty;
		}

/*
			hash=2333245417
			hash=2657142123
			hash=1343211426
			hash=3987668454
			hash=0
			hash=1757796244
			hash=1702790717
			hash=412524277
			hash=1095665917
			hash=786023491
			hash=3474678343
			hash=3767607023
			hash=2311074236
			hash=140052809
			hash=3683290680
			hash=237868189
			hash=584569423
			hash=2813542760
			hash=2983033385
			hash=3722965159
			hash=1528708457
*/

		// END hack lookups

		for (int i=0, count=network.m_Flags.GetCount(); i<count; ++i) {
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = network.m_Flags[i];
			item.type = kParameterTypeBoolean;
		}

		for (int i=0, count=network.m_Requests.GetCount(); i<count; ++i) {
			LookupTableItem& item = g_LookupTable->Grow();
			item.name = network.m_Requests[i];
			item.type = kParameterTypeBoolean;
		}

		std::sort(g_LookupTable->begin(), g_LookupTable->end());

		// append the lookup table.
		header->AppendU32(g_LookupTable->GetCount());
		headerSize += 4;
		for (int i=0, count=g_LookupTable->GetCount(); i<count; ++i) {
			LookupTableItem* item = &((*g_LookupTable)[i]);
			header->AppendU32(atStringHash(item->name));
			header->AppendU32(item->type);

			// NOTE: only increment header size if the memory shouldn't be allocated by the runtime!
//			headerSize += 8;
		}
	}

	const ExportDef** root = nodes.SafeGet(network.m_Root);
	Assert(root); // Can't have a non-root

	const BaseNode* base = (*root)->node;

	const parStructure* info = (*root)->node->parser_GetStructure();
	if (info == MotionTreeNode::parser_GetStaticStructure()) {
		NodeWriter::WriteMotionTreeNode(data->AppendChild(), static_cast<const MotionTreeNode*>(base), NULL, nodes, 0, network.m_Requests, network.m_Flags);
	} else if (info == StateMachineNode::parser_GetStaticStructure()) {
		NodeWriter::WriteStateMachineNode(data->AppendChild(), static_cast<const StateMachineNode*>(base), NULL, nodes, 0, network.m_Requests, network.m_Flags);
	}

	// The order we want is depth first: root->children->grandchildren, etc.  The order in 'nodes' is string hash (ascending).
	std::vector<u32> orderedNodeHashes;
	AppendChildHashesRecursive(base, orderedNodeHashes, nodes);
	std::vector<u32>::const_iterator hashIter = orderedNodeHashes.begin();
	++hashIter;// skip the root, which we've already exported

	atArray<Writer::Identifier> identifiers;
	atArray<const char*> filenames;
	for( ; hashIter != orderedNodeHashes.end() ; ++hashIter)
	{
		AppendNode(*(nodes.SafeGet(*hashIter)), nodes, parents, network.m_Requests, network.m_Flags, identifiers, filenames);
	}

	Assert(identifiers.size() == filenames.size()); // You did what?

	u32 footerSize = 0;
	for (int i = 0; i < identifiers.size(); ++i) {
		footer->AppendLabel(identifiers[i]);
		footer->AppendString(filenames[i], strlen(filenames[i])+1); // just make sure there's a null terminator
		footerSize += strlen(filenames[i])+1; // string doesn't need a size
	}

	fiStream* out = fiStream::Create(outfile);
	writer.Write(out);

	int length = out->Tell() - headerSize - footerSize;
	out->Seek(20);
	out->WriteInt(&length, 1);
	out->Seek(24);
	out->WriteInt(&footerSize, 1);
	out->Close();

	const char* parampath = NULL;
	if (PARAM_parampath.Get(parampath)) {
		// Write out optional header/cpp file
		atArray<const ParameterController*> parameters;
		atArray<const EventController*> events;
		atArray<const OutputController*> outputs;
		atArray<const SubnetworkNode*> subnetworks;
		for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
			const BaseNode* node = (*iter)->node;
			if (node->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure()) {
				const MotionTreeNode* motionTreeNode = static_cast<const MotionTreeNode*>(node);
				for (int i=0; i<motionTreeNode->m_Parameters.GetCount(); ++i) {
					if (parameters.end() == std::find_if(parameters.begin(), parameters.end(), FindParameterByName<ParameterController>(motionTreeNode->m_Parameters[i].m_Parameter.c_str()))) {
						parameters.PushAndGrow(&motionTreeNode->m_Parameters[i]);
					}
				}
				for (int i=0; i<motionTreeNode->m_Events.GetCount(); ++i) {
					if (events.end() == std::find_if(events.begin(), events.end(), FindEventByName(motionTreeNode->m_Events[i].m_Event.c_str()))) {
						events.PushAndGrow(&motionTreeNode->m_Events[i]);
					}
				}
/*
				for (int i=0; i<motionTreeNode->m_Outputs.GetCount(); ++i) {
					if (outputs.end() == std::find_if(outputs.begin(), outputs.end(), FindParameterByName<OutputController>(motionTreeNode->m_Outputs[i].m_Parameter.c_str())) &&
						// This might not work. I might have to do another pass instead.
						parameters.end() == std::find_if(parameters.begin(), parameters.end(), FindParameterByName<ParameterController>(motionTreeNode->m_Outputs[i].m_Parameter.c_str()))) {
						outputs.PushAndGrow(&motionTreeNode->m_Outputs[i]);
					}
				}
				*/

			}
			if (node->parser_GetStructure() == SubnetworkNode::parser_GetStaticStructure()) {
				subnetworks.PushAndGrow((const SubnetworkNode*)node);
			}
		}

		// Do a second pass over the nodes, now checking the parameters in the transitions.
		// Easier than trying to remove the duplicates across lists.

		atArray<const char*> floatParameters; // parameters that we find by inspecting transitions
		atArray<const char*> boolParameters; // parameters that we find by inspecting transitions
		atArray<const char*> fireableEvents;

		for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
			const BaseNode* node = (*iter)->node;
			if (node->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure()) {
				const MotionTreeNode* motionTreeNode = static_cast<const MotionTreeNode*>(node);
				for (int i=0; i<motionTreeNode->m_Transitions.GetCount(); ++i) {
					const Transition& transition = motionTreeNode->m_Transitions[i];
					FindParametersFromTransitions(transition, parameters, boolParameters, floatParameters, events, fireableEvents);
				}
			}
			else if (node->parser_GetStructure() == StateMachineNode::parser_GetStaticStructure()) {
				const StateMachineNode* stateMachineNode = static_cast<const StateMachineNode*>(node);
				for (int i=0; i<stateMachineNode->m_Transitions.GetCount(); ++i) {
					const Transition& transition = stateMachineNode->m_Transitions[i];
					FindParametersFromTransitions(transition, parameters, boolParameters, floatParameters, events, fireableEvents);
				}
			}
			else if (node->parser_GetStructure() == InlineStateMachineNode::parser_GetStaticStructure()) {
				const InlineStateMachineNode* stateMachineNode = static_cast<const InlineStateMachineNode*>(node);
				for (int i=0; i<stateMachineNode->m_Transitions.GetCount(); ++i) {
					const Transition& transition = stateMachineNode->m_Transitions[i];
					FindParametersFromTransitions(transition, parameters, boolParameters, floatParameters, events, fireableEvents);
				}
			}
		}

		
		// Do a third pass collecting up all the output parameters, making sure no duplicates are present

		for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
			const BaseNode* node = (*iter)->node;
			if (node->parser_GetStructure() == MotionTreeNode::parser_GetStaticStructure()) {
				const MotionTreeNode* motionTreeNode = static_cast<const MotionTreeNode*>(node);

				for (int i=0; i<motionTreeNode->m_Outputs.GetCount(); ++i) {
					if (outputs.end() == std::find_if(outputs.begin(), outputs.end(), FindParameterByName<OutputController>(motionTreeNode->m_Outputs[i].m_Parameter.c_str())) &&
						parameters.end() == std::find_if(parameters.begin(), parameters.end(), FindParameterByName<ParameterController>(motionTreeNode->m_Outputs[i].m_Parameter.c_str())) &&
						floatParameters.end() == std::find_if(floatParameters.begin(), floatParameters.end(), FindByName(motionTreeNode->m_Outputs[i].m_Parameter.c_str())) &&
						boolParameters.end() == std::find_if(boolParameters.begin(), boolParameters.end(), FindByName(motionTreeNode->m_Outputs[i].m_Parameter.c_str()))) {
							outputs.PushAndGrow(&motionTreeNode->m_Outputs[i]);
					}
				}

			}
		}


		char filepart[512];
		const char* s = strrstr(filename, "\\");
		++s;
		const char* e = strrstr(filename, ".");

		int len = e-s;
		strncpy(filepart, s, len);
		filepart[len] = '\0';

		char buffer[512];

		sprintf(buffer, "%s\\%sControlParams.h", parampath, filepart);
		FILE* h = fopen(buffer, "w");
		sprintf(buffer, "#ifndef %sControlParams_h\n#define %sControlParams_h\n", filepart, filepart);
		fwrite(buffer, strlen(buffer), 1, h);
		fwrite("\n", 1, 1, h);

		sprintf(buffer, "#include \"fwanimation/animdefines.h\"\n");
		fwrite(buffer, strlen(buffer), 1, h);

		fwrite("\n", 1, 1, h);

		sprintf(buffer, "namespace %sControlParams\n{\n", filepart);
		fwrite(buffer, strlen(buffer), 1, h);

		if (parameters.GetCount()) {
			sprintf(buffer, "\t// Parameters\n");
			fwrite(buffer, strlen(buffer), 1, h);
		}

		for (int i=0; i<parameters.GetCount(); ++i) {
			const ParameterController* parameter = parameters[i];
			const ExportDef** target = nodes.SafeGet(parameter->m_Target);
			Assert(target);
			ParameterType paramType = GetParameterType((*target)->node, parameter->m_Attribute);
			const char* runtimeType = GetFrameworkRuntimeType(paramType);

			sprintf(buffer, "\textern const %s %sId;\n", runtimeType, parameter->m_Parameter.c_str(), parameter->m_Parameter.c_str());
			fwrite(buffer, strlen(buffer), 1, h);
		}

		for (int i=0; i<boolParameters.GetCount(); ++i) {
			const char* parameter = boolParameters[i];
			sprintf(buffer, "\textern const fwMvBooleanId %sId;\n", parameter, parameter);
			fwrite(buffer, strlen(buffer), 1, h);
		}

		for (int i=0; i<floatParameters.GetCount(); ++i) {
			const char* parameter = floatParameters[i];
			sprintf(buffer, "\textern const fwMvFloatId %sId;\n", parameter, parameter);
			fwrite(buffer, strlen(buffer), 1, h);
		}

		if (outputs.GetCount()) {
			sprintf(buffer, "\t// Outputs\n");
			fwrite(buffer, strlen(buffer), 1, h);
		}
		for (int i=0; i<outputs.GetCount(); ++i) {
			const OutputController* output = outputs[i];
			const ExportDef** target = nodes.SafeGet(output->m_Target);
			Assert(target);
			ParameterType paramType = GetParameterType((*target)->node, output->m_Attribute);
			const char* runtimeType = GetFrameworkRuntimeType(paramType);

			sprintf(buffer, "\textern const %s %sId;\n", runtimeType, output->m_Parameter.c_str(), output->m_Parameter.c_str());
			fwrite(buffer, strlen(buffer), 1, h);
		}

		if (subnetworks.GetCount()) {
			sprintf(buffer, "\t// Subnetworks\n");
			fwrite(buffer, strlen(buffer), 1, h);
		}
		for (int i=0; i<subnetworks.GetCount(); ++i) {
			const SubnetworkNode* node = subnetworks[i];
			sprintf(buffer, "\textern const fwMvNetworkId %sId;\n", node->m_ParameterName.c_str(), node->m_ParameterName.c_str());
			fwrite(buffer, strlen(buffer), 1, h);
		}

		if (events.GetCount()) {
			sprintf(buffer, "\t// Events\n");
			fwrite(buffer, strlen(buffer), 1, h);
		}
		for (int i=0; i<events.GetCount(); ++i) {
			const EventController* event = events[i];
			sprintf(buffer, "\textern const fwMvBooleanId %sId;\n", event->m_Event.c_str(), event->m_Event.c_str());
			fwrite(buffer, strlen(buffer), 1, h);
		}

		if (network.m_Flags.GetCount()) {
			sprintf(buffer, "\t// Flags\n");
			fwrite(buffer, strlen(buffer), 1, h);
		}
		for (int i=0; i<network.m_Flags.GetCount(); ++i) {
			const char* flag = network.m_Flags[i].c_str();
			sprintf(buffer, "\textern const fwMvFlagId %sId;\n", flag, flag);
			fwrite(buffer, strlen(buffer), 1, h);
		}

		if (network.m_Requests.GetCount()) {
			sprintf(buffer, "\t// Requests\n");
			fwrite(buffer, strlen(buffer), 1, h);
		}
		for (int i=0; i<network.m_Requests.GetCount(); ++i) {
			const char* request = network.m_Requests[i].c_str();
			sprintf(buffer, "\textern const fwMvRequestId %sId;\n", request, request);
			fwrite(buffer, strlen(buffer), 1, h);
		}

		fwrite("}\n", 2, 1, h);

		fclose(h);

		sprintf(buffer, "%s\\%sControlParams.cpp", parampath, filepart);
		FILE* body = fopen(buffer, "w");
		sprintf(buffer, "#include %sControlParams.h\n", filepart);
		fwrite(buffer, strlen(buffer), 1, body);
		fwrite("\n\n", 1, 1, body);

		sprintf(buffer, "namespace %sControlParams\n{\n", filepart);
		fwrite(buffer, strlen(buffer), 1, body);

		for (int i=0; i<parameters.GetCount(); ++i) {
			const ParameterController* parameter = parameters[i];
			const ExportDef** target = nodes.SafeGet(parameter->m_Target);
			Assert(target);
			ParameterType paramType = GetParameterType((*target)->node, parameter->m_Attribute);
			const char* runtimeType = GetFrameworkRuntimeType(paramType);

			sprintf(buffer, "\tconst %s %sId(\"%s\");\n", runtimeType, parameter->m_Parameter.c_str(), parameter->m_Parameter.c_str());
			fwrite(buffer, strlen(buffer), 1, body);
		}

		for (int i=0; i<boolParameters.GetCount(); ++i) {
			const char* parameter = boolParameters[i];
			sprintf(buffer, "\tconst fwMvBooleanId %sId(\"%s\");\n", parameter, parameter);
			fwrite(buffer, strlen(buffer), 1, body);
		}

		for (int i=0; i<floatParameters.GetCount(); ++i) {
			const char* parameter = floatParameters[i];
			sprintf(buffer, "\tconst fwMvFloatId %sId(\"%s\");\n", parameter, parameter);
			fwrite(buffer, strlen(buffer), 1, body);
		}

		for (int i=0; i<outputs.GetCount(); ++i) {
			const OutputController* output = outputs[i];
			const ExportDef** target = nodes.SafeGet(output->m_Target);
			Assert(target);
			ParameterType paramType = GetParameterType((*target)->node, output->m_Attribute);
			const char* runtimeType = GetFrameworkRuntimeType(paramType);

			sprintf(buffer, "\tconst %s %sId(\"%s\");\n", runtimeType, output->m_Parameter.c_str(), output->m_Parameter.c_str());
			fwrite(buffer, strlen(buffer), 1, body);
		}

		for (int i=0; i<subnetworks.GetCount(); ++i) {
			const SubnetworkNode* node = subnetworks[i];
			sprintf(buffer, "\tconst fwMvNetworkId %sId(\"%s\");\n", node->m_ParameterName.c_str(), node->m_ParameterName.c_str());
			fwrite(buffer, strlen(buffer), 1, h);
		}

		for (int i=0; i<events.GetCount(); ++i) {
			const EventController* event = events[i];
			sprintf(buffer, "\tconst fwMvBooleanId %sId(\"%s\");\n", event->m_Event.c_str(), event->m_Event.c_str());
			fwrite(buffer, strlen(buffer), 1, body);
		}

		for (int i=0; i<network.m_Flags.GetCount(); ++i) {
			const char* flags = network.m_Flags[i].c_str();
			sprintf(buffer, "\tconst fwMvFlagId %sId(\"%s\");\n", flags, flags);
			fwrite(buffer, strlen(buffer), 1, body);
		}

		for (int i=0; i<network.m_Requests.GetCount(); ++i) {
			const char* request = network.m_Requests[i].c_str();
			sprintf(buffer, "\tconst fwMvRequestId %sId(\"%s\");\n", request, request);
			fwrite(buffer, strlen(buffer), 1, body);
		}

		fwrite("}\n", 2, 1, h);

		fclose(body);
	}

	for(int i = 0, count = allocated.GetCount(); i < count; i ++) {
		MotionTreeParentData *data = allocated[i];
		delete data;
	}

	for (atStringMap<const ExportDef*>::Iterator iter = nodes.Begin(); iter != nodes.End(); ++iter) {
		const ExportDef* def = *iter;
		delete def;
	}
#endif

	delete g_LookupTable;
	delete g_ControlParameterInfo;

	SHUTDOWN_PARSER;

	return 0;
}
