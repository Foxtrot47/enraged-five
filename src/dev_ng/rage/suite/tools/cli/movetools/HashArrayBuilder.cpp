#include "HashArrayBuilder.h"

#include "system/new.h"

#include "string/stringhash.h"

using namespace rage;

HashArrayBuilder::HashArrayBuilder(int count)
{
	m_Entries = rage_new Entry[count];
	m_Length = count;
};

HashArrayBuilder::~HashArrayBuilder()
{
	delete[] m_Entries;
}

void HashArrayBuilder::Insert(const char* key, u32 index)
{
	u32 hash = atStringHash(key);
	Insert(hash, index);
}

void HashArrayBuilder::Insert(u32 key, u32 index)
{
	u32 idx;
	Get(key, &idx);
	m_Entries[idx].m_Key = key;
	m_Entries[idx].m_Index = index;
}

bool HashArrayBuilder::Get(const char* key, u32* idx) const
{
	u32 hash = atStringHash(key);
	return Get(hash, idx);
}

bool HashArrayBuilder::Get(u32 key, u32* idx) const
{
	if (!m_Length) return false;
	u32 i = key % m_Length;
	while (m_Entries[i].m_Key != 0xFFFFFFFF && m_Entries[i].m_Key != key)
	{
		i = (i + 1) % m_Length;
	}

	*idx = i;
	return true;
}