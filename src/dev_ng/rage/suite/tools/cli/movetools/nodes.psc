<ParserSchema generate="class">

	<enumdef type="NodeType">
    <enumval name="NT_NONE" value="0"/>
    <enumval name="NT_STATEMACHINE" value="1"/>
    <enumval name="NT_TAIL" value="2"/>
    <enumval name="NT_INLINE" value="3"/>
    <enumval name="NT_ANIMATION" value="4"/>
    <enumval name="NT_BLEND" value="5"/>
    <enumval name="NT_ADD" value="6"/>
    <enumval name="NT_FILTER" value="7"/>
    <enumval name="NT_MIRROR" value="8"/>
    <enumval name="NT_FRAME" value="9"/>
    <enumval name="NT_IK" value="10"/>
    <enumval name="NT_RAGDOLL" value="11"/>
    <enumval name="NT_MOVER" value="12"/>
    <enumval name="NT_BLENDN" value="13"/>
    <enumval name="NT_PONYTAIL" value="14"/>
    <enumval name="NT_CLIP" value="15"/>
    <enumval name="NT_STYLE" value="16"/>
    <enumval name="NT_PM" value="17"/>
    <enumval name="NT_EXTRAPOLATE" value="18"/>
    <enumval name="NT_EXPRESSION" value="19"/>
    <enumval name="NT_CAPTURE" value="20"/>
    <enumval name="NT_PROXY" value="21"/>
    <enumval name="NT_ADDN" value="22"/>
    <enumval name="NT_IDENTITY" value="23"/>
    <enumval name="NT_MERGE" value="24"/>
    <enumval name="NT_POSE" value="25"/>
    <enumval name="NT_MERGEN" value="26"/>
    <enumval name="NT_STATE" value="27"/>
    <enumval name="NT_INVALID" value="28"/>
    <enumval name="NT_JOINTLIMIT" value="29"/>
    <enumval name="NT_SUBNETWORK" value="30"/>
    <enumval name="NT_REFERENCE" value="31"/>
  </enumdef>

  <structdef type="Event">
    <bool name="m_Enabled" init="false"/>
    <string name="m_Id" type="atString"/>
  </structdef>

  <enumdef type="SynchronizerType">
    <enumval name="ST_PHASE"/>
    <enumval name="ST_TAG"/>
    <enumval name="ST_NONE"/>
  </enumdef>

  <structdef type="Synchronizer">
    <enum name="m_Type" type="SynchronizerType" init="ST_NONE"/>
<!--
    <array name="m_Tags" type="atArray">
      <string type="atString"/>
    </array>
-->
    <u32 name="m_Tag" init="0"/>
  </structdef>

  <structdef type="BaseNode">
    <string name="m_Name" type="atString"/>
  </structdef>

  <enumdef type="AnimationAttributeType">
    <enumval name="kAnimationAttributeIgnored"/>
    <enumval name="kAnimationAttributeFilename"/>
    <enumval name="kAnimationAttributeParameter"/>
  </enumdef>

  <structdef type="Animation">
    <enum type="AnimationAttributeType" name="m_Type" init="kAnimationAttributeIgnored"/>
    <string name="m_Filename" type="atString"/>
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <enumdef type="ClipAttributeType">
    <enumval name="kClipAttributeIgnored"/>
    <enumval name="kClipAttributeFilename"/>
    <enumval name="kClipAttributeParameter"/>
  </enumdef>

  <enumdef type="ClipContextType">
    <enumval name="kClipContextVariableClipSet"/>
    <enumval name="kClipContextClipDictionary"/>
    <enumval name="kClipContextAbsoluteClipSet"/>
    <enumval name="kClipContextLocalFile"/>
  </enumdef>
  
  <structdef type="Clip">
    <enum type="ClipAttributeType" name="m_Type"/>
    <enum type="ClipContextType" name="m_Context"/>
    <string name="m_BankName" type="atString"/>
    <string name="m_ClipName" type="atString"/>
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <enumdef type="ExpressionsType">
    <enumval name="kExpressionsIgnored"/>
    <enumval name="kExpressionsFilename"/>
    <enumval name="kExpressionsParameter"/>
  </enumdef>

  <structdef type="Expressions">
    <enum type="ExpressionsType" name="m_Type"/>
    <string name="m_Filename" type="atString"/>
    <string name="m_BankName" type="atString"/>
    <string name="m_ExpressionName" type="atString"/>
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <enumdef type="FilterType">
    <enumval name="kFilterIgnored"/>
    <enumval name="kFilterFilename"/>
    <enumval name="kFilterParameter"/>
  </enumdef>

  <structdef type="Filter">
    <enum type="FilterType" name="m_Type"/>
    <string name="m_FilterDictionary" type="atString"/>
    <string name="m_FilterName" type="atString"/>
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <enumdef type="FilterNType">
    <enumval name="kFilterNIgnored"/>
    <enumval name="kFilterNParameter" value="2"/>
  </enumdef>

  <structdef type="FilterN">
    <enum type="FilterNType" name="m_Type"/>
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <enumdef type="FrameAttributeType">
    <enumval name="kFrameIgnored"/>
    <enumval name="kFrameParameter" value="2"/>
  </enumdef>

  <structdef type="Frame">
    <enum type="FrameAttributeType" name="m_Type"/>
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <enumdef type="ParameterizedMotionType">
    <enumval name="kParameterizedMotionIgnored"/>
    <enumval name="kParameterizedMotionFilename"/>
    <enumval name="kParameterizedMotionParameter"/>
  </enumdef>

  <structdef type="ParameterizedMotion">
    <enum type="ParameterizedMotionType" name="m_Type"/>
    <string name="m_Filename" type="atString"/>
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <enumdef type="F32AttributeType">
    <enumval name="kF32AttributeIgnored"/>
    <enumval name="kF32AttributeValue"/>
    <enumval name="kF32AttributeParameter"/>
  </enumdef>

  <structdef type="Float">
    <enum type="F32AttributeType" name="m_Type"/>
    <float name="m_Value"/>
    <string name="m_Parameter" type="atString"/>
    <string name="m_Output" type="atString"/>
  </structdef>

  <enumdef type="BoolAttributeType">
    <enumval name="kBoolAttributeIgnored"/>
    <enumval name="kBoolAttributeValue"/>
    <enumval name="kBoolAttributeParameter"/>
  </enumdef>

  <structdef type="Bool">
    <enum type="BoolAttributeType" name="m_Type"/>
    <bool name="m_Value"/>
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <structdef type="NData">
    <string name="m_Input" type="atString"/>
    <struct type="Float" name="m_Weight"/>
    <struct type="Float" name="m_WeightRate"/>
    <struct type="Filter" name="m_InputFilter"/>
    <bool name="m_Immutable"/>
  </structdef>

  <structdef type="ExprVarData">
    <string name="m_Name" type="atString"/>
    <struct type="Float" name="m_Value"/>
  </structdef>

  <structdef type="AnimationNode" base="BaseNode">
    <struct type="Animation" name="m_Animation"/>
    <struct type="Float" name="m_Phase"/>
    <struct type="Float" name="m_Rate"/>
    <struct type="Float" name="m_Delta"/>
    <struct type="Bool" name="m_Looped"/>
    <struct type="Bool" name="m_Absolute"/>
  </structdef>

  <enumdef type="ChildInfluenceFlag">
    <enumval name="kChildInfluenceOverrideDefault" value="0"/>
    <enumval name="kChildInfluenceOverrideZero" value="1"/>
    <enumval name="kChildInfluenceOverrideOne" value="2"/>
  </enumdef>

  <structdef type="AddNode" base="BaseNode">
    <struct type="Filter" name="m_Filter"/>
    <struct type="Float" name="m_Weight"/>
    <bool name="m_Transitional" init="false"/>
    <bool name="m_Immutable" init="false"/>
    <struct type="Synchronizer" name="m_Synchronizer"/>
    <bool name="m_Source0Immutable" />
    <bool name="m_Source1Immutable" />
    <string name="m_Child0" type="atString"/>
    <string name="m_Child1" type="atString"/>
    <enum name="m_Source0InfluenceFlag" type="ChildInfluenceFlag" init="kChildInfluenceOverrideDefault"/>
    <enum name="m_Source1InfluenceFlag" type="ChildInfluenceFlag" init="kChildInfluenceOverrideDefault"/>
  </structdef>

  <structdef type="AddNwayNode" base="BaseNode">
    <struct type="FilterN" name="m_FilterN"/>
    <struct type="Filter" name="m_Filter"/>
    <struct type="Bool" name="m_ZeroDestination"/>
    <struct type="Synchronizer" name="m_Synchronizer"/>
    <array name="m_Children" type="atArray">
      <struct type="NData"/>
    </array>
  </structdef>

  <structdef type="BlendNode" base="BaseNode">
    <struct type="Filter" name="m_Filter"/>
    <struct type="Float" name="m_Weight"/>
    <bool name="m_MergeBlend" init="false"/>
    <struct type="Synchronizer" name="m_Synchronizer"/>
    <bool name="m_Source0Immutable" />
    <bool name="m_Source1Immutable" />
    <string name="m_Child0" type="atString"/>
    <string name="m_Child1" type="atString"/>
    <enum name="m_Source0InfluenceFlag" type="ChildInfluenceFlag" init="kChildInfluenceOverrideDefault"/>
    <enum name="m_Source1InfluenceFlag" type="ChildInfluenceFlag" init="kChildInfluenceOverrideDefault"/>
  </structdef>

  <structdef type="BlendNwayNode" base="BaseNode">
    <struct type="FilterN" name="m_FilterN"/>
    <struct type="Filter" name="m_Filter"/>
    <struct type="Bool" name="m_ZeroDestination"/>
    <struct type="Synchronizer" name="m_Synchronizer"/>
    <array name="m_Children" type="atArray">
      <struct type="NData"/>
    </array>
  </structdef>

  <structdef type="CaptureNode" base="BaseNode">
    <string name="m_Source" type="atString"/>
    <struct type="Frame" name="m_Frame"/>
    <struct type="Bool" name="m_Owner"/>
  </structdef>

  <structdef type="ClipNode" base="BaseNode">
    <struct type="Clip" name="m_Clip"/>
    <struct type="Float" name="m_Phase"/>
    <struct type="Float" name="m_Rate"/>
    <struct type="Float" name="m_Delta"/>
    <struct type="Bool" name="m_Looped"/>
    <struct type="Bool" name="m_ZeroWeightSync"/>
    <struct type="Event" name="m_ClipLooped"/>
    <struct type="Event" name="m_ClipEnded"/>
  </structdef>

  <structdef type="ExpressionNode" base="BaseNode">
    <struct type="Expressions" name="m_Expressions"/>
    <string name="m_Source" type="atString"/>
    <struct type="Float" name="m_Weight"/>
    <array name="m_Variables" type="atArray">
      <struct type="ExprVarData"/>
    </array>
  </structdef>

  <structdef type="ExtrapolateNode" base="BaseNode">
    <string name="m_Source" type="atString"/>
    <struct type="Float" name="m_Damping"/>
  </structdef>

  <structdef type="FilterNode" base="BaseNode">
    <struct type="Filter" name="m_Filter"/>
    <string name="m_Source" type="atString"/>
  </structdef>

  <structdef type="FrameNode" base="BaseNode">
    <struct type="Frame" name="m_Frame"/>
    <struct type="Bool" name="m_Owner"/>
  </structdef>

  <structdef type="IdentityNode" base="BaseNode">
  </structdef>

  <structdef type="InvalidNode" base="BaseNode">
  </structdef>

  <structdef type="JointLimitNode" base="BaseNode">
    <string name="m_Source" type="atString"/>
  </structdef>

  <structdef type="MergeNode" base="BaseNode">
    <struct type="Filter" name="m_Filter"/>
    <bool name="m_Transitional" init="false"/>
    <bool name="m_Immutable" init="false"/>
    <struct type="Synchronizer" name="m_Synchronizer"/>
    <string name="m_Source0" type="atString"/>
    <string name="m_Source1" type="atString"/>
    <enum name="m_Source0InfluenceFlag" type="ChildInfluenceFlag" init="kChildInfluenceOverrideDefault"/>
    <enum name="m_Source1InfluenceFlag" type="ChildInfluenceFlag" init="kChildInfluenceOverrideDefault"/>
    <bool name="m_Source0Immutable" />
    <bool name="m_Source1Immutable" />
  </structdef>

  <structdef type="MergeNWayNode" base="BaseNode">
    <struct type="FilterN" name="m_FilterN"/>
    <struct type="Filter" name="m_Filter"/>
    <struct type="Bool" name="m_ZeroDestination"/>
    <struct type="Synchronizer" name="m_Synchronizer"/>
    <array name="m_Children" type="atArray">
      <struct type="NData"/>
    </array>
  </structdef>

  <structdef type="ParameterizedMotionNode" base="BaseNode">
    <struct type="ParameterizedMotion" name="m_ParameterizedMotion"/>
    <struct type="Float" name="m_Phase"/>
    <struct type="Float" name="m_Delta"/>
    <struct type="Float" name="m_Rate"/>
    <array name="m_Parameters" type="atArray">
      <struct type="Float"/>
    </array>
  </structdef>

  <structdef type="PoseNode" base="BaseNode">
    <bool name="m_Normalize"/>
  </structdef>

  <structdef type="ProxyNode" base="BaseNode">
  </structdef>

  <structdef type="BaseOperator">
  </structdef>

  <structdef type="PushValueOperator" base="BaseOperator">
    <float name="m_Value"/>
  </structdef>

  <structdef type="FindAndPushValueOperator" base="BaseOperator">
    <string name="m_Parameter" type="atString"/>
  </structdef>

  <structdef type="AddOperator" base="BaseOperator">
  </structdef>

  <structdef type="MultiplyOperator" base="BaseOperator">
  </structdef>

  <structdef type="CurveOperatorKeyframe">
    <float name="m_Key"/>
    <float name="m_M"/>
    <float name="m_B"/>
  </structdef>
  <structdef type="CurveOperator" base="BaseOperator">
    <float name="m_MinClamp"/>
    <float name="m_MaxClamp"/>
    <array name="m_Keyframes" type="atArray">
      <struct type="CurveOperatorKeyframe"/>
    </array>
  </structdef>

  <structdef type="ParameterController">
    <string name="m_Parameter" type="atString"/>
    <string name="m_Target" type="atString"/> <!-- the target node -->
    <string name="m_Attribute" type="atString"/> <!-- the attribute that is being driven -->
    <u32 name="m_Index" init="0"/> <!-- if the attribute is an array, what are we driving? -->
  </structdef>

  <structdef type="EventController">
    <string name="m_Target" type="atString"/>
    <string name="m_Type" type="atString"/>
    <string name="m_Event" type="atString"/>
<!--
    <string name="m_Tag" type="atString"/>
-->
  </structdef>

  <structdef type="OutputController">
    <string name="m_Parameter" type="atString"/>
    <string name="m_Target" type="atString"/>
    <string name="m_Attribute" type="atString"/>
    <u32 name="m_Index" init="0"/>
  </structdef>

  <structdef type="OperatorController">
    <string name="m_Target" type="atString"/>
    <string name="m_Attribute" type="atString"/>
    <u32 name="m_Index" init="0"/>
    <array name="m_Operations" type="atArray">
      <pointer type="BaseOperator" policy="owner"/>
    </array>
  </structdef>

  <enumdef type="Modifier">
    <enumval name="MODIFIER_LINEAR" value="0"/>
    <enumval name="MODIFIER_EASE_IN_OUT" value="1"/>
    <enumval name="MODIFIER_EASE_OUT" value="2"/>
    <enumval name="MODIFIER_EASE_IN" value="3"/>
    <enumval name="MODIFIER_STEP" value="4"/>
  </enumdef>

  <enumdef type="ConditionType">
    <enumval name="CONDITION_IN_RANGE" value ="0"/>
    <enumval name="CONDITION_OUT_OF_RANGE" value ="1"/>
    <enumval name="CONDITION_ON_REQUEST" value ="2"/>
    <enumval name="CONDITION_ON_FLAG" value ="3"/>
    <enumval name="CONDITION_AT_EVENT" value ="4"/>
    <enumval name="CONDITION_GREATER_THAN" value ="5"/>
    <enumval name="CONDITION_GREATER_THAN_EQUAL" value ="6"/>
    <enumval name="CONDITION_LESS_THAN" value ="7"/>
    <enumval name="CONDITION_LESS_THAN_EQUAL" value ="8"/>
    <enumval name="CONDITION_LIFETIME_GREATER_THAN" value ="9"/>
    <enumval name="CONDITION_LIFETIME_LESS_THAN" value ="10"/>
    <enumval name="CONDITION_ON_TAG" value ="11"/>
    <enumval name="CONDITION_BOOL_EQUALS" value="12"/>
  </enumdef>

  <structdef type="Condition">
    <bool name="m_FlagWhenPassed"/>
  </structdef>

  <structdef type="AtEvent" base="Condition">
    <string name="m_Event" type="atString"/>
    <bool name="m_NotSet" init="false"/>
  </structdef>

  <structdef type="BoolEquals" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <bool name="m_Value" init="false"/>
  </structdef>

  <structdef type="GreaterThan" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <float name="m_Trigger" />
  </structdef>

  <structdef type="GreaterThanEqual" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <float name="m_Trigger"/>
  </structdef>

  <structdef type="InRange" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <float name="m_Upper"/>
    <float name="m_Lower"/>
  </structdef>

  <structdef type="LessThan" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <float name="m_Trigger"/>
  </structdef>

  <structdef type="LessThanEqual" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <float name="m_Trigger"/>
  </structdef>

  <structdef type="NotInRange" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <float name="m_Upper"/>
    <float name="m_Lower"/>
  </structdef>

  <structdef type="OnFlag" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <bool name="m_NotSet" init="false"/>
  </structdef>

  <structdef type="OnTag" base="Condition">
    <string name="m_Tag" type="atString"/>
    <bool name="m_NotSet" init="false"/>
  </structdef>

  <structdef type="OnRequest" base="Condition">
    <string name="m_Parameter" type="atString"/>
    <bool name="m_NotSet" init="false"/>
  </structdef>

  <structdef type="LifetimeLessThan" base="Condition">
    <float name="m_Trigger"/>
  </structdef>

  <structdef type="LifetimeGreaterThan" base="Condition">
    <float name="m_Trigger"/>
  </structdef>

  <structdef type="Transition">
    <bool name="m_Enabled"/>
    <string name="m_From" type="atString"/>
    <string name="m_To" type="atString"/>
    <float name="m_Duration" init="0.5"/>
    <string name="m_DurationParameter" type="atString"/>
    <enum name="m_Modifier" type="Modifier" init="MODIFIER_LINEAR"/>
    <struct type="Synchronizer" name="m_Synchronizer"/>
    <bool name="m_ReEvaluate"/>
    <struct name="m_Filter" type="Filter"/>
    <bool name="m_IsFilterDynamic"/>
    <bool name="m_BlockUpdateOnTransition"/>
    <bool name="m_Transitional" init="true"/>
    <bool name="m_Immutable" init="false"/>
    <string name="m_TransitionWeightOutputParameter" type="atString"/>
    <array name="m_Conditions" type="atArray">
      <pointer type="Condition" policy="owner"/>
    </array>
  </structdef>

  <structdef type="MotionTreeNode" base="BaseNode">
    <string name="m_Initial" type="atString"/>
    <struct type="Event" name="m_OnEnterEvent"/>
    <struct type="Event" name="m_OnExitEvent"/>
    <array name="m_Children" type="atArray">
      <string type="atString"/>
    </array>
    <array name="m_Transitions" type="atArray">
      <struct type="Transition"/>
    </array>
    <array name="m_Parameters" type="atArray">
      <struct type="ParameterController"/>
    </array>
    <array name="m_Events" type="atArray">
      <struct type="EventController"/>
    </array>
    <array name="m_Outputs" type="atArray">
      <struct type="OutputController"/>
    </array>
    <array name="m_Operators" type="atArray">
      <struct type="OperatorController"/>
    </array>
    <bool name="m_DeferBlockUpdate"/>
  </structdef>

  <structdef type="StateMachineNode" base="BaseNode">
    <string name="m_Initial" type="atString"/>
    <struct type="Event" name="m_OnEnterEvent"/>
    <struct type="Event" name="m_OnExitEvent"/>
    <array name="m_Children" type="atArray">
      <string type="atString"/>
    </array>
    <array name="m_Transitions" type="atArray">
      <struct type="Transition"/>
    </array>
    <bool name="m_DeferBlockUpdate"/>
  </structdef>

  <structdef type="TailNode" base="BaseNode">
  </structdef>

  <structdef type="InlineStateMachineNode" base="BaseNode">
    <string name="m_Initial" type="atString"/>
    <struct type="Event" name="m_OnEnterEvent"/>
    <struct type="Event" name="m_OnExitEvent"/>
    <array name="m_Children" type="atArray">
      <string type="atString"/>
    </array>
    <array name="m_Transitions" type="atArray">
      <struct type="Transition"/>
    </array>
    <string name="m_Input" type="atString"/>
    <bool name="m_DeferBlockUpdate"/>
  </structdef>

  <structdef type="ReferenceRemapping">
    <string name="m_From" type="atString"/>
    <string name="m_To" type="atString"/>
  </structdef>

  <structdef type="InitializerData">
    <string name="m_Key" type="atString"/>
  </structdef>

  <structdef type="AnimationInitData" base="InitializerData">
    <string name="m_Filename" type="atString"/>
  </structdef>

  <structdef type="ClipInitData" base="InitializerData">
    <string name="m_Filename" type="atString"/>
  </structdef>

  <structdef type="ExpressionInitData" base="InitializerData">
    <string name="m_Filename" type="atString"/>
  </structdef>

  <structdef type="ParameterizedMotionInitData" base="InitializerData">
    <string name="m_Filename" type="atString"/>
  </structdef>

  <structdef type="FloatInitData" base="InitializerData">
    <float name="m_Value"/>
  </structdef>

  <structdef type="ReferenceNode" base="BaseNode">
    <string name="m_Reference" type="atString"/>
    <array name="m_Parameters" type="atArray">
      <struct type="ReferenceRemapping"/>
    </array>
    <array name="m_Requests" type="atArray">
      <struct type="ReferenceRemapping"/>
    </array>
    <array name="m_Flags" type="atArray">
      <struct type="ReferenceRemapping"/>
    </array>
    <array name="m_InitData" type="atArray">
      <pointer type="InitializerData" policy="owner"/>
    </array>
  </structdef>

  <structdef type="SubnetworkNode" base="BaseNode">
    <string name="m_Id" type="atString"/>
    <string name="m_ParameterName" type="atString"/>
  </structdef>

  <structdef type="Network">
    <array name="m_Nodes" type="atArray">
      <pointer type="BaseNode" policy="owner"/>
    </array>
    <array name="m_References" type="atArray">
      <string type="atString"/>
    </array>
    <array name="m_Requests" type="atArray">
      <string type="atString"/>
    </array>
    <array name="m_Flags" type="atArray">
      <string type="atString"/>
    </array>
    <string name="m_Root" type="atString"/>
  </structdef>

</ParserSchema>
