#include "Writer.h"

#include <map>

#include "math/amath.h"

using namespace rage;

Writer::Identifier Writer::s_NewLabel = 0;
std::map<Writer::Identifier, unsigned> Writer::Label::m_Offsets;

void Writer::ReleaseWriter(Writable *node)
{
	switch(node->m_Type)
	{
	case kChild:
		{
			Child *child = static_cast< Child * >(node);

			Writer *writer = child->m_Child;

			delete writer;
		} break;
	case kData:
		{
			Data *data = static_cast< Data * >(node);

			for(std::vector< Data::ElementDef * >::iterator iterator = data->m_Elements.begin(); iterator != data->m_Elements.end(); iterator ++)
			{
				Data::ElementDef *elementDef = (*iterator);

				delete elementDef;
			}
		} break;
	default:
		{
		} break;
	};
}

Writer::~Writer()
{
	for(std::vector< Writable * >::const_iterator iterator = m_Contents.begin(); iterator != m_Contents.end(); iterator ++)
	{
		Writable *node = (*iterator);

		ReleaseWriter(node);

		delete node;
	}
}

Writer* Writer::AppendChild()
{
	if (m_Type == kOffsetData) {
		Errorf("Cannot AppendChild to data node");
		exit(1);
	}

	m_Type = kOffsetContainer;
	Writer* result = rage_new Writer();
	m_Contents.push_back(rage_new Child(result));
	return result;
}

void Writer::AppendData(void* data, unsigned length, unsigned elementSize)
{
	if (m_Type == kOffsetContainer) {
		Errorf("Can't append data to a container");
		exit(1);
	}

	m_Type = kOffsetData;
	Data* block = NULL;
	if (m_Contents.size() == 0 || m_Contents[m_Contents.size()-1]->m_Type != kData) {
		block = rage_new Data;
		m_Contents.push_back(block);
	} else {
		block = static_cast<Data*>(m_Contents[m_Contents.size()-1]);
	}
	block->AppendData(data, length, elementSize);
}

void Writer::AppendData(const char* data, int length)
{
	if (data == NULL) {
		data = "";
	}
	int count = Min(int(strlen(data)), length);
	for (int i = 0; i < count; ++i) {
		AppendData((void*)(&data[i]), 1, 1);
	}
	u8 nullByte = 0x00;
	for (int i = count; i < length; ++i) {
		AppendData(&nullByte, 1, 1);
	}
}

void Writer::AppendString(const char* str, int length)
{
	if (str == NULL) {
		str = "";
	}
	int count = Min(int(strlen(str)), length);
	for (int i = 0; i < count; ++i) {
		AppendData((void*)(&str[i]), 1, 1);
	}
	u8 nullByte = 0x00;
	for (int i = count; i < length; ++i) {
		AppendData(&nullByte, 1, 1);
	}
}

void Writer::AppendStringHash(const char* const str)
{
	AppendU32(atStringHash(str));
}

void Writer::AlignData(unsigned alignment)
{
	if (m_Type == kOffsetContainer) {
		Errorf("Can't align a container");
		exit(1);
	}
	m_Type = kOffsetData;
	m_Contents.push_back(rage_new Align(alignment));
}

Writer::Identifier Writer::AppendLabel()
{
	Identifier label = s_NewLabel;
	m_Contents.push_back(rage_new Label(label));
	++s_NewLabel;
	return label;
}

void Writer::AppendLabel(Identifier label)
{
	m_Contents.push_back(rage_new Label(label));
}

Writer::Identifier Writer::ReserveLabel()
{
	return s_NewLabel++;
}

Writer::Offset* Writer::AppendOffset(Identifier target, OffsetOrigin origin, unsigned bytes, unsigned stride)
{
	Offset* offset = rage_new Offset(target, origin, bytes, stride);
	m_Contents.push_back(offset);
	return offset;
}

Writer::Offset* Writer::AppendOffset(Identifier target)
{
	Offset* offset = rage_new Offset(target, kOffsetAddress, 4, 1);
	m_Contents.push_back(offset);
	return offset;
}

Writer::Offset* Writer::AppendOffset(OffsetOrigin origin, unsigned bytes, unsigned stride)
{
	Offset* offset = rage_new Offset(~0, origin, bytes, stride);
	m_Contents.push_back(offset);
	return offset;
}

void Writer::SetOffset(Offset* offset, Identifier target)
{
	offset->SetLabel(target);
}

void Writer::SetStartAlignment(unsigned alignment)
{
	m_StartAlignment = alignment;
}

void Writer::SetTagOffsetType(OffsetOrigin origin)
{
	m_Offset = origin;
}

void Writer::Write(fiStream* stream)
{
	Label::ClearOffsets();
	WriteData(stream);
#if 0
	if (m_IffTag != 0) {
		WriteAlignmentPad(stream, 4);
		unsigned tagOffset = unsigned(stream->Tell());
		WriteTag(stream, m_Offset);
		if ((stream->Tell() & 0x3)!= 0) {
			Errorf("Invalid stream position");
		}
		switch (m_Offset) {
		case kFile:
			break;
		case kOffsetAddress:
			tagOffset -= unsigned(stream->Tell());
			break;
		default:
			Errorf("Unknown offset type");
		}
		stream->WriteInt(&tagOffset, 1);
	}
#endif
	stream->Flush();
	int pos = stream->Tell();
	PatchOffset(stream);
	stream->Seek(pos);
	Label::ClearOffsets();
}

unsigned Writer::GetDataSize(unsigned initialOffset)
{
	unsigned result = 0;
	for (unsigned i = 0; i < m_Contents.size(); ++i) {
		unsigned element = m_Contents[i]->GetDataSize(initialOffset);
		result += element;
		initialOffset += element;
	}
	return result;
}


void Writer::WriteData(fiStream* stream)
{
	WriteAlignmentPad(stream, m_StartAlignment);
	m_DataPosInFile = stream->Tell();
	for (unsigned i = 0; i < m_Contents.size(); ++i) {
		m_Contents[i]->WriteData(stream);
	}
	long pos = stream->Tell();
	m_DataSizeInFile = pos - m_DataPosInFile;
}

unsigned Writer::GetDescendantCount()
{
	unsigned result = 0;
	for (unsigned i = 0; i < m_Contents.size(); ++i) {
		if (m_Contents[i]->m_Type == kChild) {
			++result;
			Child* child = static_cast<Child*>(m_Contents[i]);
			result += child->m_Child->GetDescendantCount();
		}
	}
	return result;
}

void Writer::PatchOffset(fiStream* stream)
{
	for (unsigned i = 0; i < m_Contents.size(); ++i) {
		if (m_Contents[i]->m_Type == kOffset) {
			Offset* offset = static_cast<Offset*>(m_Contents[i]);
			offset->PatchOffset(stream);
		} else if (m_Contents[i]->m_Type == kChild) {
			Child* child = static_cast<Child*>(m_Contents[i]);
			child->m_Child->PatchOffset(stream);
		}
	}
}
