#pragma once

#include "nodes.h"
#include "writer.h"

struct ExportDef {
	ExportDef(const BaseNode* n, Writer* data)
		: label (Writer::ReserveLabel())
		, writer (data)
		, node (n)
	{}

	Writer::Identifier label;
	Writer* writer;
	const BaseNode* node;
};

typedef atStringMap<const ExportDef*> Map;