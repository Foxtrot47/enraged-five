#include "parser/manager.h"

class AnimationNode {
public:
	AnimationNode()
		: m_Animation (NULL)
		, m_Phase (0.f)
		, m_Rate (1.f)
		, m_Delta (0.f)
		, m_Looped (true)
		, m_Absolute (false) 
	{}

	const char* m_Animation;
	float m_Phase;
	float m_Rate;
	float m_Delta;
	bool m_Looped;
	bool m_Absolute;

	PAR_PARSABLE;
};
