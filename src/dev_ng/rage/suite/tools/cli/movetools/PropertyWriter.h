#pragma once

#include "exportdef.h"
#include "nodes.h"

class Writer;

#define USE_INDICES (0)

namespace PropertyWriter
{
	void AppendFilename(Writer* writer, const char* filename);
	void AppendParameterCrc(Writer* writer, const char* parameter);
	void AppendAnimation(Writer* writer, const Animation& data);
	void AppendClip(Writer* writer, const Clip& data);
	void AppendExpressions(Writer* writer, const Expressions& data);
	void AppendFilter(Writer* writer, const Filter& data);
	void AppendFilterN(Writer* writer, const FilterN& data);
	void AppendFrame(Writer* writer, const Frame& data);
	void AppendParameterizedMotion(Writer* writer, const ParameterizedMotion& data);
	void AppendFloat(Writer* writer, const Float& data);
	void AppendBool(Writer* writer, const Bool& data);
	void AppendOffset(Writer* writer, const char* name, Map& nodes);
	void AppendOperation(Writer* writer, BaseOperator* op);
}
