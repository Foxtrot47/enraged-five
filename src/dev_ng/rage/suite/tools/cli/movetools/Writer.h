#pragma once

#include <map>

#include "parser/manager.h"

using namespace rage;

class Writer {
public:

	class Writable;

	enum OffsetType { kOffsetNone, kOffsetData, kOffsetContainer };
	enum OffsetOrigin { kFile, kOffsetAddress };
	Writer()
		: m_Type (kOffsetNone)
		, m_Tag (0)
		, m_Offset (kFile)
		, m_StartAlignment (1)
	{}

	void ReleaseWriter(Writable *node);

	virtual ~Writer();

	typedef int Identifier;

	enum Descriptor {
		kChild,
		kData,
		kAlign,
		kLabel,
		kOffset,
	};

	class Offset;

	Writer* AppendChild();

	void AppendData(void* data, unsigned length, unsigned elementSize);
	void AppendData(const char* data, int elementSize);

	void AppendBool(bool data) { 
		u8 bytes[4] = {0,0,0,0};
		bytes[3] = data?1:0;
		AppendData(&bytes, 4, sizeof(bytes)); 
	}
	void AppendString(const char* str, int length);

	void AppendFloat(float data)
	{
		if(!_isnan(data))
		{
			AppendData(&data, 4, sizeof(data));
		}
		else
		{
			// Hot fix Rage NANs to look like .NET NANs
			u32 UNAN = 0xFFC00000;
			float FNAN = *reinterpret_cast< float * >(&UNAN);
			AppendData(&FNAN, 4, sizeof(data));
		}
	}
	void AppendU8(u8 data) { AppendData(&data, 1, sizeof(data)); }
	void AppendI8(s8 data) { AppendData(&data, 1, sizeof(data)); }
	void AppendU16(u16 data) {	AppendData(&data, 2, sizeof(data));	}
	void AppendI16(s16 data) {	AppendData(&data, 2, sizeof(data));	}
	void AppendU32(u32 data) { AppendData(&data, 4, sizeof(data));}
	void AppendI32(s32 data) { AppendData(&data, 4, sizeof(data)); }

	void AppendStringHash(const char* const str);

	void AlignData(unsigned alignment);

	Identifier AppendLabel();
	void AppendLabel(Identifier label);

	static Identifier ReserveLabel();

	Offset* AppendOffset(Identifier target, OffsetOrigin origin, unsigned bytes, unsigned stride);
	Offset* AppendOffset(Identifier target);
	Offset* AppendOffset(OffsetOrigin origin, unsigned bytes, unsigned stride);

	void SetOffset(Offset* offset, Identifier target);

	void SetStartAlignment(unsigned alignment);
	void SetTagOffsetType(OffsetOrigin origin);

	void Write(fiStream* stream);

	unsigned GetDataSize(unsigned initialOffset);

	bool IsEmpty() const { return m_Contents.size() == 0; }

	void WriteData(fiStream* stream);
	void WriteTag(fiStream*, OffsetOrigin) {}

	unsigned GetDescendantCount();

	void PatchOffset(fiStream* stream);

	static void WriteAlignmentPad(fiStream* stream, unsigned alignment)
	{
		const u8 zero = 0;
		while ((stream->Tell() % alignment) != 0) {
			stream->WriteByte(&zero, 1);
		}
	}

	class Writable {
	public:
		Writable(Descriptor type)
			: m_Type (type)
		{}
		virtual ~Writable() {}

		virtual void WriteData(fiStream* stream) = 0;
		virtual unsigned GetDataSize(unsigned offset) = 0;

		virtual void WriteTag(fiStream*, OffsetOrigin) {}

		Descriptor m_Type;
	};

	class Child : public Writable {
	public:
		Child(Writer* child)
			: Writable (kChild)
			, m_Child (child)
		{}

		virtual void WriteData(fiStream* stream)
		{
			m_Child->WriteData(stream);
		}

		virtual void WriteTag(fiStream* stream, OffsetOrigin origin)
		{
			m_Child->WriteTag(stream, origin);
		}

		virtual unsigned GetDataSize(unsigned offset)
		{
			return m_Child->GetDataSize(offset);
		}

		Writer* m_Child;
	};

	class Data : public Writable {
	public:
		Data()
			: Writable (kData)
		{}

		struct ElementDef {
			unsigned size;
			unsigned count;
		};

		virtual void WriteData(fiStream* stream)
		{
			if (m_Data.size() == 0) {
				return;
			}

			stream->Write(&m_Data[0], m_Data.size());
		}

		virtual unsigned GetDataSize(unsigned)
		{
			return unsigned(m_Data.size());
		}

		void AppendData(u8 data)
		{
			m_Data.push_back(data);
			ElementDef* def = rage_new ElementDef;
			def->size = sizeof(u8);
			def->count = 1;
			m_Elements.push_back(def);
		}

		void AppendData(void* data, unsigned length, unsigned elementSize)
		{
			if (16 < elementSize) {
				Errorf("Cannot handle data elements greater than 16 bytes");
				exit(1);
			}

			u8* curr = (u8*)data;
			for (unsigned i = 0; i < length; ++i) {
				m_Data.push_back(*curr);
				++curr;
			}
			ElementDef* def = rage_new ElementDef;
			def->size = elementSize;
			def->count = length / elementSize;
			m_Elements.push_back(def);
		}

		std::vector<u8> m_Data;
		std::vector<ElementDef*> m_Elements;
	};

	class Align : public Writable {
	public:
		Align(unsigned alignment)
			: Writable (kAlign)
			, m_Alignment (alignment)
		{
		}

		virtual void WriteData(fiStream* stream)
		{
			Writer::WriteAlignmentPad(stream, m_Alignment);
		}

		virtual unsigned GetDataSize(unsigned offset)
		{
			unsigned modulo = offset % m_Alignment;
			unsigned result = modulo != 0 ? m_Alignment - modulo : 0;
			return result;
		}

		unsigned m_Alignment;
	};

	class Label : public Writable {
	public:
		Label(Identifier label)
			: Writable (kLabel)
			, m_Label (label)
		{}

		virtual void WriteData(fiStream* stream)
		{
			std::map<Identifier, unsigned>::iterator iter = m_Offsets.find(m_Label);
			if (iter != m_Offsets.end()) {
				Errorf("Already written label!");
				exit(1);
			}
			m_Offsets.insert(std::make_pair(m_Label, unsigned(stream->Tell())));
		}

		virtual unsigned GetDataSize(unsigned)
		{
			return 0;
		}

		static bool LabelWasWritten(Identifier label) 
		{
			return m_Offsets.find(label) != m_Offsets.end();
		}

		static unsigned GetOffset(Identifier label)
		{
			std::map<Identifier, unsigned>::iterator iter = m_Offsets.find(label);
			if (iter != m_Offsets.end()) {
				return iter->second;
			}

			Errorf("Failed to find the offset for label");
			return 0;
		}

		static void ClearOffsets()
		{
			m_Offsets.clear();
		}

		Identifier m_Label;
		static std::map<Identifier, unsigned> m_Offsets;
	};

	class Offset : public Writable {
	public:
		Offset(Identifier target, OffsetOrigin origin, unsigned bytes, unsigned stride)
			: Writable (kOffset)
			, m_Label (target)
			, m_OffsetPosition (0)
			, m_Origin (origin)
			, m_OffsetByteSize (bytes)
			, m_OffsetStride (stride)
		{
			if (m_OffsetByteSize < 1) {
				Errorf("Offset storage must be atleast one byte");
				exit (1);
			}
			if (4 < m_OffsetByteSize) {
				Errorf("Offset storage of more that 4 bytes isn't supported");
				exit (1);
			}
			if (m_OffsetStride < 1) {
				Errorf("Offset strides must be at least 1 byte");
				exit (1);
			}
		}

		virtual void WriteData(fiStream* stream)
		{
			m_OffsetPosition = unsigned(stream->Tell());
			u8 nullOffset = 0x00;
			switch (m_Origin) {
	case kFile:
		nullOffset = 0xff;
		break;
	case kOffsetAddress:
		nullOffset = 0x00;
		break;
	default:
		Errorf("Unknown offset base type");
		exit(1);
			}
			for (unsigned i = 0; i < m_OffsetByteSize; ++i) {
				stream->WriteByte(&nullOffset, 1);
			}
		}

		virtual unsigned GetDataSize(unsigned)
		{
			return m_OffsetByteSize;
		}

		void PatchOffset(fiStream* stream)
		{
			if (Label::LabelWasWritten(m_Label) != true) {
				return;
			}
			int labelPos = int(Label::GetOffset(m_Label));
			if (labelPos % m_OffsetStride != 0) {
				Errorf("Target of offset in not correctly aligned");
				exit(1);
			}
			switch (m_Origin) {
	case kOffsetAddress:
		labelPos -= int(m_OffsetPosition - (m_OffsetPosition % m_OffsetStride));
		break;
	case kFile:
		break;
	default:
		Errorf("Unknown offset origin method");
		exit(1);
			}
			int rounding = int(m_OffsetStride)-1;
			if (0 < labelPos) {
				rounding = 1 - int(m_OffsetStride);
			}
			labelPos = int(labelPos + rounding);
			labelPos = labelPos / int(m_OffsetStride);

			int bitCount = 0;
			if (m_Origin == kOffsetAddress) {
				int absolute = abs(labelPos);
				for (int bit = 30; -1 < bit; --bit) {
					if ((absolute & (1 << bit)) != 0) {
						bitCount = bit + 2;
						break;
					}
				}
			} else {
				for (int bit = 31; -1 < bit; --bit) {
					if ((labelPos & (1<<bit)) != 0) {
						bitCount = bit + 1;
						break;
					}
				}
			}
			if (int(m_OffsetByteSize) * 8 < bitCount) {
				Errorf("Offset is too large to fit into given storage");
				exit(1);
			}
			stream->Seek(m_OffsetPosition);
			stream->WriteInt(&labelPos, 1);
		}

		void SetLabel(Identifier target)
		{
			m_Label = target;
		}

		Identifier m_Label;
		unsigned m_OffsetPosition;
		OffsetOrigin m_Origin;
		unsigned m_OffsetByteSize;
		unsigned m_OffsetStride;
	};

	OffsetType m_Type;
	unsigned m_Tag;
	OffsetOrigin m_Offset;
	std::vector<Writable*> m_Contents;
	unsigned m_StartAlignment;
	size_t m_DataPosInFile;
	size_t m_DataSizeInFile;

	static Identifier s_NewLabel;
};
