#pragma once

using namespace rage;

class HashArrayBuilder
{
public:
	struct Entry 
	{
		Entry()
		{
			m_Key = 0xFFFFFFFF;
			m_Index = 0;
		}

		u32 m_Key;
		u32 m_Index;
	};

	HashArrayBuilder(int count);
	~HashArrayBuilder();

	void Insert(const char* key, u32 index);
	void Insert(u32 key, u32 index);
	bool Get(const char* key, u32* idx) const;
	bool Get(u32 key, u32* idx) const;

	int GetLength() const { return m_Length; }
	const Entry& GetEntry(const int index) const { return m_Entries[index]; }

private:
	Entry* m_Entries;
	int m_Length;
};
