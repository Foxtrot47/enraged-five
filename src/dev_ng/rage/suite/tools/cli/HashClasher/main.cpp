#include "atl/string.h"
#include <vector>
#include "system/main.h"
#include "system/param.h"
#include "atl/map.h"
#include <map>
#include <stdio.h>
#include <fstream>

using namespace rage;
using namespace std;

REQ_PARAM( input, "[hashclasher] The attributes file." );

std::map<unsigned short, atString> mapHashes;

void SplitString(const char* str, std::vector<atString>& vec)
{
	char* pch = strtok ((char*)str,",");
	while (pch != NULL)
	{
		atString p(pch);
		vec.push_back(p);
		pch = strtok (NULL, ",");
	}
}

void LoadAttributeFile(const atString& filename)
{
	if(filename == "") return;

	std::fstream file_op(filename.c_str(), std::ios::in);
	while(!file_op.eof()) 
	{
		char str[MAX_PATH*2];
		file_op.getline(str,MAX_PATH*2);

		std::vector<atString> vec;
		SplitString(str, vec);

		if(vec.size() != 2) continue;

		unsigned short h = rage::atHash16U(vec[1].c_str());

		std::map<unsigned short, atString>::iterator itr = mapHashes.find(h);
		if(itr != mapHashes.end())
		{
			Displayf("clash: %s and %s", vec[1].c_str(), itr->second.c_str());
			continue;
		}

		mapHashes[h] = vec[1];
	}         

	file_op.close();
}
int Main()
{
	Displayf("Hash clasher :)\n");

	const char* pFile;
	PARAM_input.Get( pFile );

	LoadAttributeFile(atString(pFile));
	Displayf("done..\n");

	return 1;
}