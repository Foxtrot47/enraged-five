// 
// /applystyle.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#define SIMPLE_HEAP_SIZE (248*1024)

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"
#include "crskeleton/skeletondata.h"
#include "crstyletransfer/style.h"
#include "crstyletransfer/stylegenerator.h"
#include "crstyletransfer/styleplayer.h"
#include "system/main.h"
#include "system/param.h"
#include "vectormath/legacyconvert.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(skel, ".skel filename (Example: abc.skel)");
PARAM(anims, "Source animation(s) to be stylized (Example: unstylized0.anim,unstyalized1.anim ...)");
PARAM(out, "Destination sylized animation(s) (Example: sylized0.anim,sylized1.anim ...)");
PARAM(style, "Style filename (Example: my.style)");
PARAM(compression, "Compression tolerance (Example: 0.0)");
PARAM(blendweight, "Optional:  The global blend weight to use on the style (default is 1.0)");
PARAM(heuristic, "Optional:  Flag for enabling/disabling heuristic blending (default is 0)");
PARAM(confidence, "Optional:  Used with heuristic blending.  Confidence in the system (default is 20)");
PARAM(preference, "Optional:  Used with heuristic blending.  Preference for stylization (default is 2)");
PARAM(zup, "Optional:  Signal that the z-axis, not the y-axis is the vertical axis");


int Main(void)
{
	// initialize animation system
	crAnimation::InitClass();

	const u16 moverId = 0;

	// process help parameter
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process .skel file parameter
	const char* skelFilename = NULL;
	if(!PARAM_skel.Get(skelFilename))
	{
		Quitf("ERROR - No skeleton filename specified\n");
	}

	// process animation list parameter
	const int maxAnims = 10;
	const int maxAnimsBufSize = 4096;

	int numAnims = 0;
	const char* animFilenames[maxAnims];
	char animsBuf[maxAnimsBufSize];

	numAnims = PARAM_anims.GetArray(animFilenames, maxAnims, animsBuf, 
		maxAnimsBufSize);
	if(numAnims <= 0)
	{
		Quitf("ERROR - No source animations specified\n");
	}


	// process out list parameter
	int numOut = 0;
	const char* outFilenames[maxAnims];
	char outBuf[maxAnimsBufSize];

	numOut = PARAM_out.GetArray(outFilenames, maxAnims, outBuf, 
		maxAnimsBufSize);
	if(numOut <= 0)
	{
		Quitf("ERROR - No source animations specified\n");
	}
	else if(numAnims != numOut)
	{
		Quitf("ERROR - Different number of animation vs output filenames specified\n");
	}

	// process style parameter:
	const char* styleFilename = NULL;
	if(!PARAM_style.Get(styleFilename))
	{
		Quitf("ERROR - No style filename specified\n");
	}

	// process compression tolerance:
	float tolerance = -1.f;
	PARAM_compression.Get(tolerance);

	// find the value of the heuristic flag
	int heuristic = 0;
	PARAM_heuristic.Get(heuristic);

	// find the value of the confidence term
	int confidence = 20;
	PARAM_confidence.Get(confidence);

	// find the value of the preference term
	int preference = 2;
	PARAM_preference.Get(preference);

	// find blend weight
	float blendWeight = 1.f;
	PARAM_blendweight.Get(blendWeight);

	// vertical axis
	int upAxis = 1; // y-axis
	if(PARAM_zup.Get())
	{
		upAxis = 2; // z-axis
	}

	// load skeleton
	crSkeletonData* skelData = crSkeletonData::AllocateAndLoad(skelFilename);
	if(!skelData)
	{
		Quitf("ERROR - Unable to load .skel file '%s'", skelFilename);
	}

	// load the animations
	atArray<crAnimation*> anims;
	anims.Resize(numAnims);
	for(int i=0; i<numAnims; ++i)
	{
		anims[i] = crAnimation::AllocateAndLoad(animFilenames[i]);
		if(!anims[i])
		{
			Quitf("ERROR - Unable to load .skel file '%s'", animFilenames[i]);
		}
	}

	// load the style
	crstStyle style;
	if(!style.Load(styleFilename))
	{
		Quitf("ERROR - Failed to load .style file '%s'", styleFilename);
	}

	// create compression tolerance object
	crAnimToleranceSimple compression;
	if(tolerance < 0.f)
	{
		compression.Init();
	}
	else
	{
		compression.Init(tolerance, tolerance, tolerance);
	}

	// initialize the style generator
	for(int iAnim=0; iAnim<numAnims; ++iAnim)
	{
		const u32 numFrames = anims[iAnim]->GetNumInternalFrames();
		const float duration = anims[iAnim]->GetDuration();
		float sampleRate = duration / float(numFrames-1);
		bool hasMover = anims[iAnim]->HasMoverTracks();

		atArray<crFrame> frames;
		atArray<const crFrame*> framePtrs;
	
		frames.Reserve(numFrames*2);
		framePtrs.Reserve(numFrames*2);

		Matrix34 moverMtx(M34_IDENTITY);
		if(hasMover)
		{
			crFrame moverFrame;
			moverFrame.InitCreateMoverDofs();
			anims[iAnim]->CompositeFrame(0.f, moverFrame);

			moverFrame.GetMoverMatrix(moverId, RC_MAT34V(moverMtx));
		}

		crstStylePlayer player;
		player.Init(&style, *skelData, upAxis, !heuristic, confidence, preference);
		player.SetBlendWeight(blendWeight);

		float time = 0.f;
		float multiplier = 1.f;
		u16 iFrame = 0;

		do
		{
			time += sampleRate * multiplier;

			frames.Grow();
			iFrame++;

			frames[iFrame].InitCreateBoneAndMoverDofs(*skelData, hasMover, moverId);

			if(hasMover)
			{
				anims[iAnim]->CompositeFrameWithMover(time, sampleRate*multiplier, moverId, frames[iFrame]);
			}
			else
			{
				anims[iAnim]->CompositeFrame(time, frames[iFrame]);
			}

			multiplier = 1.f / player.Stylize(frames[iFrame], sampleRate);

			if(hasMover)
			{
				Matrix34 mtx;
				frames[iFrame].GetMoverMatrix(moverId, RC_MAT34V(mtx));

				moverMtx.DotFromLeft(mtx);

				frames[iFrame].SetMoverMatrix(moverId, RC_MAT34V(moverMtx));
			}

			framePtrs.Grow();
			framePtrs[iFrame] = &frames[iFrame];
		}
		while(time <= duration);

		crAnimation outAnim;
		outAnim.Create(iFrame, float(iFrame-1)*sampleRate, anims[iAnim]->IsLooped(), hasMover);
		outAnim.CreateFromFramesFast(framePtrs, compression);

		if(!outAnim.Save(outFilenames[iAnim]))
		{
			Quitf("ERROR - Failed to save animation file '%s'", outFilenames[iAnim]);
		}
	}
	
	// cleanup 
	for(int i=0; i<numAnims; ++i)
	{
		delete anims[i];
	}
	delete skelData;

	// shutdown animation system
	crAnimation::ShutdownClass();

	return 0;
}

