// 
// /makestyle.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define SIMPLE_HEAP_SIZE (248*1024)

#include "cranimation/animation.h"
#include "crskeleton/skeletondata.h"
#include "crstyletransfer/style.h"
#include "crstyletransfer/stylegenerator.h"
#include "system/main.h"
#include "system/param.h"
#include "cranimation/weightset.h"

using namespace rage;

PARAM(help, "Get command line help");
PARAM(skel, ".skel filename (Example: abc.skel)");
PARAM(anims, "Source animations used to generate style (Example: unstylized.anim,stylized.anim)");
PARAM(style, "Destination style filename (Example: dest.style)");
PARAM(weightset, "Optional .weightset filename (Example: weights.weightset)");
PARAM(stylename, "Optional name for the style (Example: mystyle - Default: \"\")");
PARAM(upindex, "Optional:  Index for up axis (Default: 1 for y-axis)");
PARAM(slopelimit, "Optional integer slope limit used when aligning the source animations in time (Default:  2)");
PARAM(imwflag, "Optional:  Whether or not to use Iterative Motion Warping instead of point cloud distances (Default: 0)");
PARAM(imwiterations, "Optional:  Only used for Iterative Motion Warping.  Determines the number of interations to perform (Default: 1)");
PARAM(imwsmoothness, "Optional:  Only used for Iterative Motion Warping.  Float smoothness weight (Default: 100)");


int Main(void)
{
	// initialize animation system
	crAnimation::InitClass();

	// process help parameter
	if(PARAM_help.Get())
	{
		sysParam::Help("");
		return 0;
	}

	// process .skel file parameter
	const char* skelFilename = NULL;
	if(!PARAM_skel.Get(skelFilename))
	{
		Quitf("ERROR - No skeleton filename specified\n");
	}

	// process animation list parameter
	const int maxAnims = 10;
	const int maxAnimsBufSize = 4096;

	int numAnims = 0;
	const char* animFilenames[maxAnims];
	char animsBuf[maxAnimsBufSize];

	numAnims = PARAM_anims.GetArray(animFilenames, maxAnims, animsBuf, 
		maxAnimsBufSize);
	if(numAnims <= 0)
	{
		Quitf("ERROR - No source animations specified\n");
	}
	else if(numAnims != 2)
	{
		Quitf("ERROR - expected exactly two animations\n");
	}

	// process style parameter
	const char* styleFilename = NULL;
	if(!PARAM_style.Get(styleFilename))
	{
		Quitf("ERROR - No style filename specified\n");
	}

	// load skeleton
	crSkeletonData* skelData = crSkeletonData::AllocateAndLoad(skelFilename);
	if(!skelData)
	{
		Quitf("ERROR - Unable to load .skel file '%s'", skelFilename);
	}

	// process weightset parameter
	const char* weightSetFilename = NULL;
	crWeightSet styleWeightSet(*skelData);
	if(PARAM_weightset.Get(weightSetFilename))
	{
		if(!styleWeightSet.Load(weightSetFilename, *skelData))
		{
			Quitf("ERROR - Unable to load .weightset file '%s'", weightSetFilename);
		}
	}
	else
	{
		styleWeightSet.SetAnimWeight(0, 1, true);
	}

	// process stylename parameter
	const char* styleName = NULL;
	bool suppliedName = PARAM_stylename.Get(styleName);

	// process slope limit parameter
	int slopeLimit = 2;
	PARAM_slopelimit.Get(slopeLimit);

	// process up index parameter
	int upIndex = 1;
	PARAM_upindex.Get(upIndex);

	// load the animations
	atArray<crAnimation*> anims;
	anims.Resize(numAnims);
	for(int i=0; i<numAnims; ++i)
	{
		anims[i] = crAnimation::AllocateAndLoad(animFilenames[i]);
		if(!anims[i])
		{
			Quitf("ERROR - Unable to load .skel file '%s'", animFilenames[i]);
		}
	}

	// process imw flag parameter
	int imwFlag = 0;
	PARAM_imwflag.Get(imwFlag);

	// process imw iterations parameter
	int imwIters = 1;
	PARAM_imwiterations.Get(imwIters);

	// process imw smoothness parameter
	float imwWeight = 100;
	PARAM_imwsmoothness.Get(imwWeight);

	// intialize the style generator
	crstStyleGenerator styleGenerator(*skelData);
	
	// generate style
	crstStyle* style = styleGenerator.CreateStyle(*anims[0], *anims[1], styleWeightSet, 
		upIndex, (short) slopeLimit, imwFlag != 0, (short) imwIters, imwWeight);
	if(!style)
	{
		Quitf("ERROR - failed to create style");
	}

	// If supplied, set the style's name
	if(suppliedName) style->SetName(styleName);
    
	// store .style file
	if(!style->Save(styleFilename))
	{
		Quitf("ERROR - Unable to store style file '%s'", styleFilename);
	}

	// cleanup 
	for(int i=0; i<numAnims; ++i)
	{
		delete anims[i];
	}
	delete style;
	delete skelData;

	// shutdown animation system
	crAnimation::ShutdownClass();

	return 0;
}
