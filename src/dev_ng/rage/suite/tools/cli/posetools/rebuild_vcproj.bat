@echo off

if not defined RS_TOOLSCONFIG (
	@echo.
	call setenv
	@echo.
)

call %RS_TOOLSROOT%\script\util\projGen\sync.bat

call %RS_TOOLSROOT%\script\util\projGen\generate.bat %RS_TOOLSCONFIG%\projgen\rageTools.build makeposedb.txt
call %RS_TOOLSROOT%\script\util\projGen\generate.bat %RS_TOOLSCONFIG%\projgen\rageTools.build makeposedb.slndef