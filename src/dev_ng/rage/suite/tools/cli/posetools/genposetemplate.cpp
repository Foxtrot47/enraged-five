#include "crposedb/posedb.h"
#include "file/asset.h"
#include "init/ini.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

PARAM(config, "Config file containing animation names (eg. posedb.ini)");
PARAM(out, "Output file to write the pose database xml (eg. posedb.xml)");

using namespace rage;

int Main()
{
	INIT_PARSER;

	const char* configFile = NULL;
	if (!PARAM_config.Get(configFile))
	{
		Quitf("ERROR - No config file specified!");
	}

	const char* outFile = NULL;
	if (!PARAM_out.Get(outFile))
	{
		Quitf("ERROR - No output file specified!");
	}

	crPoseFile posedb;

	initIni config;
	config.Load(configFile);

	// [Samples]
	initIniSection* samples = config.GetSection("Samples");
	const atMap<atString, initIniItem*>& sampleItems = samples->GetItems();
	for (atMap<atString, initIniItem*>::ConstIterator iter = sampleItems.CreateIterator(); !iter.AtEnd(); iter.Next())
	{
		const atString& key = (*iter)->GetKey();
		int idx = key.LastIndexOf("/");
		atString lo;
		lo.Set(key, idx+1);
		int len = lo.LastIndexOf(".");
		lo.Truncate(len);
		atString dictionary;
		AssertVerify((*iter)->GetString(dictionary));
		if (key.EndsWith("anim"))
		{
			crAnimationPoseSample* sample = rage_new crAnimationPoseSample;
			sample->m_Filename = key;
			sample->m_Key.m_Hi = atStringHash(dictionary.c_str());
			sample->m_Key.m_Lo = atStringHash(lo.c_str());
			sample->m_Time = 0.f;  // TODO: Have a time section in the ini file
			posedb.m_Samples.PushAndGrow(sample);

		} 
		else if (key.EndsWith("clip"))
		{
			crClipPoseSample* sample = rage_new crClipPoseSample;
			sample->m_Filename = key;
			sample->m_Key.m_Hi = atStringHash(dictionary.c_str());
			sample->m_Key.m_Lo = atStringHash(lo.c_str());
			sample->m_Time = 0.f; // TODO: Have a time section in the ini file
			posedb.m_Samples.PushAndGrow(sample);
		}
		else
		{
			// ?
		}
	}

	// [Weights]
	initIniSection* weights = config.GetSection("Weights");
	const atMap<atString, initIniItem*>& weightItems = weights->GetItems();
	for (atMap<atString, initIniItem*>::ConstIterator iter = weightItems.CreateIterator(); !iter.AtEnd(); iter.Next())
	{
		crAnimWeight* weight = rage_new crAnimWeight;
		weight->m_BoneName = (*iter)->GetKey();
		AssertVerify((*iter)->GetFloat(weight->m_Weight));

		posedb.m_Weights.PushAndGrow(weight);
	}

	// [Options]
	initIniSection* options = config.GetSection("Options");
	if (initIniItem* windowSize = options->GetItem("WindowSize"))
	{
		AssertVerify(windowSize->GetInt(posedb.m_WindowSize));
	}
	if (initIniItem* sampleRate = options->GetItem("SampleRate"))
	{
		AssertVerify(sampleRate->GetFloat(posedb.m_SampleRate));
	}

	PARSER.SaveObject(outFile, "xml", &posedb);

	SHUTDOWN_PARSER;

	return 0;
}