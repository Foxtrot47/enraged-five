#include "crposedb/posedb.h"

#include "cranimation/animation.h"
#include "cranimation/weightset.h"
#include "crclip/clip.h"
#include "crextra/posematcher.h"
#include "crskeleton/skeletondata.h"

#include "file/asset.h"
#include "file/token.h"

#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(def, "Pose database sample definition file (Example posedb.xml)");
PARAM(skel, "Skel file to initialize pose matcher with (Example entity.skel)");
PARAM(out, "Pose database output file (Example posedb.pmdb)");

#if CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
PARAM(animfolders, "[makeposedb] one or more comma-separated paths that are searched (in order) when looking for anim files; after these, makeposedb falls back to what was passed in with -path, or the default");
PARAM(animfoldersfile, "[makeposedb] file containing a path on each line; these paths are searched (in order) when looking for anim files; after these, makeposedb falls back to what was passed in with -path, or the default");
#endif // CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS

namespace asset {
	XPARAM(path);
}

#if CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
bool LoadAnimFoldersFile(const char* szAnimFoldersFile, atArray<atString>& animFolders)
{
	char buffer[512];

	fiSafeStream f(ASSET.Open(szAnimFoldersFile, ""));
	if (f)
	{
		fiTokenizer T(szAnimFoldersFile, f);

		while (T.GetLine(buffer, NELEM(buffer)) > 0)
		{
			atString n(buffer);
			n.Trim();
			if (n.GetLength() != 0)
			{
				animFolders.Grow() = n;
			}
		}

		return true;
	}
	else
	{
		Errorf("LoadAnimFoldersFile - failed to open anim folders file '%s'", szAnimFoldersFile);
		return false;
	}
}
#endif // CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS

int Main()
{
	INIT_PARSER;

	crAnimation::InitClass();
	crClip::InitClass();

	const char* defFile = NULL;
	if (!PARAM_def.Get(defFile)) {
		Quitf("ERROR - No input file specified");
	}

	const char* skelFile = NULL;
	if (!PARAM_skel.Get(skelFile)) {
		Quitf("ERROR - No skel file specified");
	}

	const char* outFile = NULL;
	if (!PARAM_out.Get(outFile)) {
		Quitf("ERROR - No output file specified");
	}

	crPoseFile posedb;
	if (!PARSER.LoadObject(defFile, "", posedb)) {
		Quitf("ERROR - Failed to load pose sample data file '%s'", defFile);
	}

#if CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS
	atArray<atString> animFolders;

	const char* animFoldersFilename = NULL;
	if (PARAM_animfoldersfile.Get(animFoldersFilename))
	{
		if (PARAM_animfolders.Get())
		{
			Errorf("Passing both -animfolders and -animfoldersfile parameters is not allowed");
			return false;
		}

		if (!LoadAnimFoldersFile(animFoldersFilename, animFolders))
		{
			return false;
		}
	}

	const char* animFoldersParam = NULL;
	if (PARAM_animfolders.Get(animFoldersParam))
	{
		atString(animFoldersParam).Split(animFolders, ',', /*removeEmptyStrings=*/true);

		if (animFolders.GetCount() == 0)
		{
			Errorf("-animfolders parameter has no anim folders specified");
			return false;
		}
	}

	crPoseFile::SetAnimSearchFolders(animFolders);
#endif // CR_INCLUDE_EXTRA_ANIM_SEARCH_FOLDERS

	const char* path = NULL;
	if (asset::PARAM_path.Get(path)) {
		ASSET.PushFolder(path);
	}

	if (!posedb.Load()) {
		Quitf("ERROR - Cannot continue because of previous errors!");
	}

	if (path) {
		ASSET.PopFolder();
	}

	crSkeletonData skelData;
	if (!skelData.Load(skelFile)) {
		Quitf("ERROR - Failed to load skel file '%s'", skelFile);
	}

	crWeightSet weightSet (skelData);

	crPoseMatcher m;
	m.Init(skelData, &weightSet, 0 /*whatever, it won't be used*/, posedb.m_WindowSize, posedb.m_SampleRate);

	bool passed = true;

	atArray<crAnimWeight*>& weights =  posedb.m_Weights;
	for (int i=0; i<weights.GetCount(); ++i) {
		u16 boneId = skelData.ConvertBoneNameToId(weights[i]->m_BoneName);
		int boneIdx;
		if (!skelData.ConvertBoneIdToIndex(boneId, boneIdx)) {
			Errorf("ERROR - Failed to find bone '%s' in skel '%s'", weights[i]->m_BoneName.c_str(), skelFile);
			passed = false;
		} else {
			m.m_WeightSet->SetAnimWeight(boneIdx, weights[i]->m_Weight);
		}
	}

	if (!passed) {
		Quitf("ERROR - Connot continue because of previous errors!");
	}

	for (int i=0; i<posedb.m_Samples.GetCount(); ++i) {
		posedb.m_Samples[i]->AddSample(m);
	}

	m.Save(outFile);

	crPoseMatcher test;
	test.Load(outFile);

	crClip::ShutdownClass();
	crAnimation::ShutdownClass();

	SHUTDOWN_PARSER;

	return 0;
}