#include "crposedb/posedb.h"

#include "cranimation/animation.h"
#include "cranimation/weightset.h"
#include "crclip/clip.h"
#include "crextra/posematcher.h"
#include "crmetadata/dumpoutput.h"
#include "crskeleton/skeletondata.h"

#include "file/asset.h"

#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(database, "Pose database (Example database.pmdb)");

int Main()
{
	const char* database = NULL;
	if (!PARAM_database.Get(database)) {
		Quitf("ERROR - No input database specified");
	}

	crPoseMatcher m;
	if (!m.Load(database))
	{
		Quitf("ERROR - failed to load database '%s'", database);
	}

	crDumpOutputGrowBuffer output;
	m.Dump(output);

	output.PrintBuffer();

	return 0;
}