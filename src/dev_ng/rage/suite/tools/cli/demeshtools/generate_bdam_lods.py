import os
import sys
import os.path
import optparse
import math
import time

GridSize = 128
CurrGridSize = GridSize
Basename = ""
BdamSimplifyExec = os.getenv("BDAM_SIMPLIFY", "T:/rage/tools/Modules/base/rage/exes/demesh_bdam_simplify.exe")
SnapDist = None
NameTemplate = "%(Basename)s_L%L_c%C_r%R_%D.dmsh"
NoExec = False
NoFileTest = False
KeepPercent = 50
KeepList = None

def makeFileName(lod, row, col, dir):
    global Basename, NameTemplate

    expandedNameTemplate = NameTemplate
    for (cStyleStr, pyStyleStr) in (("%L", str(lod)), ("%C", str(col)), ("%R", str(row)), ("%D", dir)):
        expandedNameTemplate = expandedNameTemplate.replace(cStyleStr, pyStyleStr)

    return expandedNameTemplate

def findExistingFiles(lod, row, col, dirList):
    global NoFileTest
    files = []
    for i in dirList:
        name = makeFileName(lod, row, col, i)
        if NoFileTest or os.path.exists(name):
            files.append(name)            
    return files

def runBdamSimplify(files, points, outfiles):
    global SnapDist, BdamSimplifyExec, NoExec, KeepPercent
    # could do a timestamp check here. Only regenerate the output files if one of the inputs is more recent
    sys.stderr.write("Generating files %s and %s\n" % (outfiles[0], outfiles[1]))
    print ""
    print ""
    print "bdam_simplify"
    print "in:   " + ("\nin:   ".join(files))
    print " -outA " + outfiles[0]
    print " -outB " + outfiles[1]
    print " Points (%.1f %.1f), (%.1f %.1f), (%.1f %.1f), (%.1f %.1f)" % (
        points[0][0], points[0][1],
        points[1][0], points[1][1],
        points[2][0], points[2][1],
        points[3][0], points[3][1])
    print time.strftime("%d %b %Y %H:%M:%S")
    print ""

    cmdString = BdamSimplifyExec + " %s -outA=%s -outB=%s -point1=%f,%f -point2=%f,%f -point3=%f,%f -point4=%f,%f -keeppercent=%d" % (
        " ".join(files),
        outfiles[0], outfiles[1],
        points[0][0], points[0][1],
        points[1][0], points[1][1],
        points[2][0], points[2][1],
        points[3][0], points[3][1],
        KeepPercent)

    if SnapDist:
        cmdString += " -snap=%f" % (SnapDist)

    print cmdString    

    if not NoExec:
        os.system(cmdString)

# each row, col index maps 4 tris to 2
def evenToOddLODGroup(lod, row, col):
    # for each row, column
    # if r even and c even:
    #  N + E -> r/2 c/2 NW
    #  S + W -> r/2 c/2 WN
    # if r odd, c even
    #  N + W -> WS
    #  S + E -> SW
    # if r even, c odd
    #  N + W -> NE
    #  S + E -> EN
    # if r odd, c odd
    #  N + E -> ES
    #  S + W -> SE

    files = findExistingFiles(lod, row, col, ["N", "E", "W", "S"])

    if len(files) == 0:
        return

    newFileNames = []
    offsets = []

    if (row % 2 == 0 and col % 2 == 0):
        newFileNames = [makeFileName(lod + 1, row / 2, col / 2, "WN"), makeFileName(lod + 1, row / 2, col / 2, "NW")]
        offsets = [
            (1, 0),
            (0, 0),
            (1, 1),
            (0, 1)]

    elif (row % 2 == 1 and col % 2 == 0):
        newFileNames = [makeFileName(lod + 1, (row - 1) / 2, col / 2, "WS"), makeFileName(lod + 1, (row - 1) / 2, col / 2, "SW")]
        offsets = [
            (0, 0),
            (1, 0),
            (0, 1),
            (1, 1)]

    elif (row % 2 == 0 and col % 2 == 1):
        newFileNames = [makeFileName(lod + 1, row / 2, (col - 1) / 2, "NE"), makeFileName(lod + 1, row / 2, (col - 1) / 2, "EN")]
        offsets = [
            (0, 0),
            (1, 0),
            (0, 1),
            (1, 1)]

    else:
        newFileNames = [makeFileName(lod + 1, (row - 1) / 2, (col - 1) / 2, "SE"), makeFileName(lod + 1, (row - 1) / 2, (col - 1) / 2, "ES")]
        offsets = [
            (1, 0),
            (0, 0),
            (1, 1),
            (0, 1)]

    points = [(CurrGridSize * (x + col), CurrGridSize * (y + row)) for (x,y) in offsets]    

    runBdamSimplify(files, points, newFileNames)

def evenToOddLOD(lod, startRow, endRow, startCol, endCol):
    for i in range(startRow, endRow):
        for j in range(startCol, endCol):
            evenToOddLODGroup(lod, i, j)

# each row, col index maps 8 tris to 4
# why not 4->2? Because in even->odd we created half the number of triangles but used 1/4 the number of
# indices for the output (note that each index also maps to twice as many tris too). So here for
# each of the quarter density indices we need to make twice as many outputs
def oddToEvenLODGroup(lod, row, col):
    #  c-1 EN + c-1 ES -> c-1 E
    #  WN + WS -> W
    #  r-1 SW + r-1 SE -> r-1 S
    #  NW + NE -> N

    points = []    
    
    files = findExistingFiles(lod, row, col-1, ["EN", "ES"])
    files.extend(findExistingFiles(lod, row, col, ["WN", "WS"]))

    if len(files) != 0:
        newFileNames = [makeFileName(lod + 1, row, col-1, "E"), makeFileName(lod + 1, row, col, "W")]
        offsets = [
            (-0.5, 0.5),
            (0, 0),
            (0, 1),
            (0.5, 0.5)]
        points = [(CurrGridSize * (x + col), CurrGridSize * (y + row)) for (x,y) in offsets]    
        runBdamSimplify(files, points, newFileNames)

    files = findExistingFiles(lod, row-1, col, ["SE", "SW"])
    files.extend(findExistingFiles(lod, row, col, ["NE", "NW"]))

    if len(files) != 0:
        newFileNames = [makeFileName(lod + 1, row, col, "N"), makeFileName(lod + 1, row - 1, col, "S")]
        offsets = [
            (0.5, -0.5),
            (0, 0),
            (1, 0),
            (0.5, 0.5)]
        points = [(CurrGridSize * (x + col), CurrGridSize * (y + row)) for (x,y) in offsets]    
        runBdamSimplify(files, points, newFileNames)

def oddToEvenLOD(lod, startRow, endRow, startCol, endCol):
    for i in range(startRow, endRow):
        for j in range(startCol, endCol):
            oddToEvenLODGroup(lod, i, j)

def main(inArgs):
    global GridSize, CurrGridSize, Basename, SnapDist, NameTemplate, NoExec, NoFileTest, KeepPercent, KeepList

    options = optparse.OptionParser()
    options.add_option("-b", "--basename", dest="basename", type="string")
    options.add_option("-s", "--startlod", dest="startLod", type="int", default=0)
    options.add_option("-l", "--endlod", dest="endLod", type="int")
    options.add_option("-r", "--rect", dest="rect", type="float", nargs=4)
    options.add_option("-p", "--point", dest="point", type="float", nargs=2)
    options.add_option("-g", "--grid", dest="gridSize", type="float", default=128.0)
    options.add_option("-k", "--keeppercent", dest="keeppercent", type="int", default=50)
    options.add_option("--keeplist", dest="keeplist", type="string")
    options.add_option("--noexec", dest="noexec", action="store_true")
    options.add_option("--nofiletest", dest="nofiletest", action="store_true")
    options.add_option("--nametemplate", dest="nametemplate", type="string", default=None)
    options.add_option("--snap", dest="snap", type="float")

    (opts, args) = options.parse_args(inArgs)

    # Check for required args:
    if not opts.endLod:
        print "Missing required argument --endlod"
        options.print_help()
        sys.exit(1)

    if not opts.rect and not opts.point:
        print "--rect or --point must be specified"
        options.print_help()
        sys.exit(1)

    if not opts.basename and not opts.nametemplate:
        print "--basename or --nametemplate must be specified"
        options.print_help()
        sys.exit(1)

    print "Arguments are:"
    print " ".join(inArgs)

    if opts.noexec:
        NoExec = True
    if opts.nofiletest:
        NoFileTest = True

    if hasattr(opts, "gridSize"):
        GridSize = opts.gridSize

    Basename = opts.basename

    if opts.keeplist:
        KeepList = [int(x) for x in opts.keeplist.split(",")]

    if opts.keeppercent:
        KeepPercent = opts.keeppercent

    if opts.nametemplate:
        NameTemplate = opts.nametemplate

    if opts.snap:
        SnapDist = opts.snap

    if opts.rect:
        (minX, minY, maxX, maxY) = opts.rect
    else:
        (minX, minY) = opts.point
        maxX = minX + 0.0001
        maxY = minY + 0.0001
        minX = minX - 0.0001
        minY = minY - 0.0001

    for lod in range(opts.startLod, opts.endLod):
        if KeepList:
            KeepPercent = KeepList[lod]

        # figure out what the effective grid size is

        currLodGrid = int(math.floor(0.5 * (lod + 1)))

        CurrGridSize = GridSize * math.pow(2, currLodGrid)

        startRow = 0
        endRow = 0
        startCol = 0
        endCol = 0

        # could find better bounds. Especially for odd LODs
        startRow = int(math.floor(minY / CurrGridSize))
        endRow = int(math.ceil(maxY / CurrGridSize))
        startCol = int(math.floor(minX / CurrGridSize))
        endCol = int(math.ceil(maxX / CurrGridSize))

        if lod % 2 == 1:
            endRow += 1
            endCol += 1

        print "------------------"
        print lod, CurrGridSize, startRow, endRow-1, startCol, endCol-1
        print ""
        
        if lod % 2 == 0:
            evenToOddLOD(lod, startRow, endRow, startCol, endCol)
        else:
            oddToEvenLOD(lod, startRow, endRow, startCol, endCol)
            

if __name__ == "__main__":
    main(sys.argv)