import os
import sys
import os.path
import optparse
import math
import time
import glob
import token
import tokenize

# Alternate technique is to start with a large mesh and then do a number of (simplify, dice) operations on it
# instead of starting from small meshes and doing (merge, simplify) on them

GridSize = 128
CurrGridSize = GridSize
SimplifyExec = os.getenv("DEMESH_SIMPLIFY", "T:/rage/tools/Modules/base/rage/exes/demesh_simplify.exe")
MergeExec = os.getenv("DEMESH_MERGE", "T:/rage/tools/Modules/base/rage/exes/demesh_merge.exe")
SkirtExec = os.getenv("DEMESH_ADD_SKIRT", "T:/rage/tools/Modules/base/rage/exes/demesh_add_skirt.exe")
EntityExec =  os.getenv("DEMESH_TO_ENTITY", "T:/rage/tools/Modules/base/rage/exes/demesh_to_entity.exe")
DiceExec = os.getenv("DEMESH_DICE", "T:/rage/tools/Modules/base/rage/exes/demesh_dice.exe")
MeshModExec = os.getenv("MESHMODIFIER", "T:/rage/tools/Modules/base/rage/exes/meshModifier.exe")
TextureConvertExec = os.getenv("TEXTURECONVERT", "T:/rage/tools/Modules/base/rage/exes/rageTextureConvert.exe")
SnapDist = None
NameTemplate = None
NoExec = False
NoFileTest = False
KeepPercent = 50
KeepList = None
CreateSkirts = False
EntityTemplate = None
InputSize = 512

def makeFileName(template, lod, row, col, dir):
    filename = template
    for (cStyleStr, pyStyleStr) in (("%L", str(lod)), ("%C", str(col)), ("%R", str(row)), ("%D", dir)):
        filename = filename.replace(cStyleStr, pyStyleStr)

    return filename

def runCommand(commandString, captureOutput=False):
    global NoExec
    print commandString
    if not NoExec:
        if captureOutput:
            output = os.popen(commandString)
            outString = output.read()
            print outString
            return outString
        else:
            os.system(commandString)
            return ""

def nextToken(tok):
    (id, data, spos, epos, line) = tok.next()
    # newlines are showing up as token # 54. Don't know what that number means
    while id in [token.INDENT, token.DEDENT, token.NEWLINE, 54]:
        (id, data, spos, epos, line) = tok.next()
    return data

def popBlock(tok):
    count = 1
    while count > 0:
        t = nextToken(tok)
        if t == '{':
            count += 1
        elif t == '}':
            count -= 1

def FindDDSName(entityName, textureToClip):
    texName = None # the name of the .dds goes here

    # look in the .type file for the name of the texture we want to clip
    typefile = file(entityName + "\\entity.type")

    tok = tokenize.generate_tokens(typefile.readline)

    nextToken(tok) # Version
    nextToken(tok) # :
    nextToken(tok) # 103
    blockName = nextToken(tok).lower()
    while(blockName != "shadinggroup"):
        nextToken(tok) # {
        popBlock(tok) # ... }
        blockName = nextToken(tok).lower()
        
    if blockName == "shadinggroup":
        nextToken(tok) # {
        nextToken(tok) # shadinggroup
        nextToken(tok) # {
        t = nextToken(tok)
        if t == "Count": # Count is optional?
            nextToken(tok) # number
            t = nextToken(tok) # Shaders
        nextToken(tok) # N
        nextToken(tok) # {
        nextToken(tok) # Vers
        nextToken(tok) # :
        versNumber = int(nextToken(tok)) # N

        if versNumber == 1:
            nextToken() # part of the sps name
            # get the whole next line
            (id, data, spos, epos, line) = tok.next()
            (spsName, one, svaName) = line.split()

            # now scan the sva looking for the textureToCLip
            found = False            
            svaFile = file(entityName + "\\" + svaName)
            svaTok = tokenize.generate_tokens(svaFile.readline)
            t = nextToken(svaTok)
            while t != textureToClip:
                t = nextToken(svaTok)
            if t == textureToClip:
                nextToken(svaTok) # {
                nextToken(svaTok) # grcTexture
                (id, data, spos, epos, line) = tok.next()
                (grcTex, texName) = line.split()
            svaFile.close()

        elif versNumber == 2:
            # keep going in this file until textureToClip
            t = nextToken(tok)
            while t != textureToClip:
                t = nextToken(tok)
            if t == textureToClip:
                nextToken(tok) # {
                nextToken(tok) # grcTexture
                (id, data, spos, epos, line) = tok.next()
                (grcTex, texName) = line.split()

    typefile.close()
    return texName

def CreateClippedTexture(sourceFileName, entityName, textureToClip, texFormat, level):
    texName = FindDDSName(entityName, textureToClip)
 
    if not texName:
        print "Couldn't find a texture for shader variable", textureToClip
        return

    # take the output mesh and remap the UVs to 0-1.
    runCommand(MeshModExec + " -input=%s\\mesh.mesh -output=%s\\optimized.mesh -optimizeoutput=C:\\temp\\meshopt.txt -forceoutputmesh -optimizeuntiledmaterial=0 -texcoordset=0" % (entityName, entityName))

    # find how much of the texture the mesh was using    
    optinfofile = file("C:\\temp\\meshopt.txt")
    minU = 0.0
    maxU = 1.0
    minV = 0.0
    maxV = 1.0
    for optline in optinfofile:
        if optline.startswith("src_min_uv"):
            (crap, minUstr, minVstr) = optline.split()
            minU = float(minUstr)
            minV = float(minVstr)
        if optline.startswith("src_max_uv"):
            (crap, maxUstr, maxVstr) = optline.split()
            maxU = float(maxUstr)
            maxV = float(maxVstr)
    optinfofile.close()

    # then take the original dds and cut out just the part of it
    # that the original mesh used.
    (sourceDir, sourceType) = os.path.split(sourceFileName)
    inTexFullName = os.path.join(sourceDir, texName)
    outTexFullName = os.path.join(entityName, texName)

#    runCommand(DevilExec + " -src=%s -dst=%s -cropUmin=%f -cropVmin=%f -cropUmax=%f -cropVmax=%f -filter=nearest -fmt=none" % (inTexFullName, outTexFullName, minU, minV, maxU, maxV))
    runCommand(TextureConvertExec + " -crop %f %f %f %f -cropnearest -f %s -cropscale %f %s %s" % (minU, minV, maxU, maxV, texFormat, 1.0 / (1 << level), inTexFullName, outTexFullName))

def main(inArgs):
    global GridSize, CurrGridSize, SnapDist, NameTemplate, NoExec
    global  NoFileTest, KeepPercent, KeepList, CreateSkirts, EntityTemplate, InputSize
    global DiceExec
    options = optparse.OptionParser()
    options.add_option("-s", "--inputsize", dest="inputsize", type="float", default=512.0)
    options.add_option("-g", "--grid", dest="gridSize", type="float", default=128.0)
    options.add_option("-k", "--keeppercent", dest="keeppercent", type="int", default=50)
    options.add_option("--keeplist", dest="keeplist", type="string")
    options.add_option("--noexec", dest="noexec", action="store_true")
    options.add_option("--nofiletest", dest="nofiletest", action="store_true")
    options.add_option("--nametemplate", dest="nametemplate", type="string", default=None)
    options.add_option("--entitytemplate", dest="entitytemplate", type="string", default=None)
    options.add_option("--snap", dest="snap", type="float")
    options.add_option("--skirts", dest="skirts", action="store_true")
    options.add_option("--cliptex", dest="cliptex", type="string")
    options.add_option("--texformat", dest="texformat", type="string", default="L8")

    (opts, args) = options.parse_args(inArgs)

    # Check for required args:
    if not opts.nametemplate and not opts.entitytemplate:
        print "--nametemplate or --entitytemplate must be specified"
        options.print_help()
        sys.exit(1)

    print "Arguments are:"
    print " ".join(inArgs)

    if opts.noexec:
        NoExec = True
    if opts.nofiletest:
        NoFileTest = True
    if opts.skirts:
        CreateSkirts = True
    if opts.entitytemplate:
        EntityTemplate = opts.entitytemplate

    if hasattr(opts, "gridSize"):
        GridSize = opts.gridSize

    if hasattr(opts, "inputSize"):
        InputSize = opts.inputSize

    if opts.keeplist:
        KeepList = [int(x) for x in opts.keeplist.split(",")]

    if opts.keeppercent:
        KeepPercent = opts.keeppercent

    if opts.nametemplate:
        NameTemplate = opts.nametemplate

    if opts.snap:
        SnapDist = opts.snap

    files = []
    for globName in args[1:]:
        files.extend(glob.glob(globName))

    for fileName in files:
        CurrGridSize = GridSize

        sourceFile = fileName
        level = 0
        while CurrGridSize <= InputSize:

            if NameTemplate:
                levelledNameTemplate = NameTemplate.replace("%L", str(level))

            extraArgs = ""
            if CreateSkirts:
                extraArgs += " -skirt=10 "
            if EntityTemplate:
                extraArgs += " -entity "
                levelledNameTemplate = EntityTemplate.replace("%L", str(level))
            
            output = runCommand(DiceExec + " %s %s -snap=%f -grid=%f -square %s" % (sourceFile, levelledNameTemplate, SnapDist, CurrGridSize, extraArgs), True)

            if opts.cliptex:
                for i in output.split('\n'):
                    if i.startswith("output_loc (lrcd):"):
                        (lod, row, col, dir) = i[18:].split()
                        outname = makeFileName(EntityTemplate, level, int(row), int(col), dir)
                        if (opts.cliptex):
                            CreateClippedTexture(fileName, outname, opts.cliptex, opts.texformat, level)
                                

            if (CurrGridSize < InputSize):           
                # simplify
                newSourceFile = "C:\\temp\\simplified%d.dmsh" % (level)
                runCommand(SimplifyExec + " %s %s -keeppercent=%d -snap=%f -bound=1000 " % (sourceFile, newSourceFile, KeepPercent, SnapDist))
                sourceFile = newSourceFile
            
            level += 1
            CurrGridSize *= 2.0            


if __name__ == "__main__":
    main(sys.argv)
