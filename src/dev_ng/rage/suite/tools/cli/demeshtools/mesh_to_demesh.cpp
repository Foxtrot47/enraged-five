// 
// demeshtools/mesh_to_demesh.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "demesh/demesh.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

FPARAM(1, in, "Name of the input .msh file");
FPARAM(2, out, "Name of the output .dmsh file");
PARAM(snap, "Snap distance for vertices");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");

int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}

	const char* inName = NULL;
	const char* outName = NULL;

	PARAM_in.Get(inName);
	PARAM_out.Get(outName);

	float snap;
	if (PARAM_snap.Get(snap)) {
		deWedge::SetVertSnapDist(snap);
	}

	deMesh* mesh = deMesh::CreateFromFile(inName);

	mesh->Save(outName);

	delete mesh;
	
	deMesh::CloseErrorStream();

	SHUTDOWN_PARSER;

	return 0;
}


