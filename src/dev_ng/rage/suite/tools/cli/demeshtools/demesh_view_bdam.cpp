// 
// demeshtools/demesh_view_bdam.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/atfunctor.h"
#include "atl/cache.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "demesh/demesh.h"
#include "file/device.h"
#include "file/token.h"
#include "gizmo/gizmo.h"
#include "gizmo/translation.h"
#include "grcore/im.h"
#include "grmodel/shader.h"
#include "math/random.h"
#include "parser/manager.h"
#include "profile/element.h"
#include "profile/group.h"
#include "rmcore/drawable.h"
#include "sample_rmcore/sample_rmcore.h"
#include "spatialdata/aainfcylinder.h"
#include "spatialdata/bdamtree.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"
#include "vector/geometry.h"
#include "vector/vector3.h"

#include <set>
#include <string>
#include <vector>

#if __WIN32PC
#include <direct.h> //RAGE_QA: INCLUDE_OK
#endif

using namespace rage;
using namespace ragesamples;

PF_GROUP(DirEdge);

PF_TIMER(LoadTimer, DirEdge);

REQ_PARAM(lod, "Lowest detail LOD");
REQ_PARAM(grid, "Grid size to use");
REQ_PARAM(nametemplate, "The name template to use to find tiles");
PARAM(radius, "Radius (in tiles at the lowest LOD) around camera to draw");
PARAM(tex, "Texture to use for preview");
PARAM(startpos, "Starting position");
PARAM(range, "Range of tiles to load in (minX, minY, maxX, maxY)");
PARAM(highlod, "Highest detail LOD");
PARAM(tunelods, "Just tune LOD transition ranges");
PARAM(drawable, "Load drawables (meshes) instead of demeshes");
PARAM(textures, "List of textures to apply to the terrain");
PARAM(texturePath, "Path to the textures");
PARAM(squares, "Draw squares instead of BDAM");
PARAM(tunesquares, "Draw the tuning grid for the squares");

PARAM(file, "don't use this");

Color32 LevelColors[] = {
Color_gold,
Color_gold,
Color_purple,
Color_sienna,
Color_SeaGreen,
Color_azure,
Color_gold,
Color_purple,
Color_sienna,
Color_SeaGreen,
Color_azure
};

template<typename _Key, typename _Value>
class atMultiCache
{
public:
	atMultiCache()
	: m_InsertionCache(0)
	{
	}

	// Check a particular key and get the value back if it is in the cache.
	// Returns true if the key was found, false otherwise. Value is only changed when true.
	bool Read(const _Key& key, _Value& value)
	{
		for(int i = 0; i < 4; i++) {
			if (m_SubCache[i].Read(key, value))
			{
				return true;
			}
		}
		return false;
	}

	// Stores a value into the cache.
	// Replaces oldest entry if the number of items is _Size.
	void Store(const _Key& key, const _Value& value)
	{
		m_SubCache[m_InsertionCache].Store(key, value);
		m_InsertionCache++;
		m_InsertionCache %= sm_NumCaches;
	}


private:
	static const int sm_NumCaches = 4;
	atCache<_Key, _Value, 127> m_SubCache[sm_NumCaches];
	int m_InsertionCache;
};

#if 0
// Cache with LRU replacement that holds N deMeshes indexed by spdBdamLocation
template<typename _Type>
class bdamCache {
public:
	void Init(int size, atFunctor1<_Type*, const spdBdamLocation&> loadFunctor) {
		m_LoadFunctor = loadFunctor;
		m_Cache.Resize(size);
		for(int i = 0; i < m_Cache.GetCount(); i++) {
			m_Cache[i].m_LastAccessedFrame = 0;
			m_Cache[i].m_Location.Set(-1, -1, -1, spdBdamLocation::DIR_N);
			m_Cache[i].m_Mesh = NULL;
		}
	}

	// Loads the mesh if it's not found
	deMesh* Access(spdBdamLocation loc, u32 frame) {
		for(int i = 0; i < m_Cache.GetCount(); i++) {
			if (loc == m_Cache[i].m_Location) {
				m_Cache[i].m_LastAccessedFrame = frame;
				return m_Cache[i].m_Mesh;
			}
		}

		int lru = FindLru();

		_Type* newMesh = m_LoadFunctor(loc);
		if (newMesh) {
			delete m_Cache[lru].m_Mesh;

			m_Cache[lru].m_LastAccessedFrame = frame;
			m_Cache[lru].m_Location = loc;
			m_Cache[lru].m_Mesh = newMesh;
		}
		return newMesh;
	}
	
protected:

	int FindLru() {
		u32 minFrame = m_Cache[0].m_LastAccessedFrame;
		int minIndex = 0;

		int i = 0;
		for(i = 0; i < m_Cache.GetCount(); i++) {
			if (m_Cache[i].m_LastAccessedFrame < minFrame) {
				minFrame = m_Cache[i].m_LastAccessedFrame;
				minIndex = i;
			}
		}

		return minIndex;
	}

	u32 m_Frame;
	
	struct CacheData {
		u32 m_LastAccessedFrame;
		spdBdamLocation m_Location;
		deMesh* m_Mesh;
	};

	atFunctor1<_Type*, const spdBdamLocation&> m_LoadFunctor;

	atArray<CacheData> m_Cache;
};

typedef bdamCache<deMesh> demeshCache;
typedef bdamCache<rmcDrawable> drawableCache;

#endif

typedef atMultiCache<spdBdamLocation, deMesh*> DemeshCache;
typedef atMultiCache<spdBdamLocation, rmcDrawable*> DrawableCache;

class demeshViewBdam : public rmcSampleManager
{
public:
	demeshViewBdam() 
	: m_DiffuseTexture(NULL)
	, m_DrawStyle(WIREFRAME)
	, m_LodState(LOD_0)
	, m_PoiAxes(NULL)
	, m_NameTemplate(NULL)
	, m_LowestLod(4)
	, m_HighestLod(0)
	, m_GridSize(128.0f)
	, m_DrawRadius(4)
	, m_RadiusBias(1.0f)
	, m_MinX(INT_MIN)
	, m_MaxX(INT_MAX)
	, m_MinY(INT_MIN)
	, m_MaxY(INT_MAX)
	, m_UseCameraAsPoi(false)
	, m_DrawTheoreticalGrid(false)
	, m_DrawTheoreticalSquareGrid(false)
	, m_DrawDemesh(true)
	, m_DrawDrawable(false)
	{
		m_PointOfInterest.Identity();
		m_DirName[0] = '\0';
		for(int i = 0; i < 10; i++) 
		{
			m_RadiusLevelFactors[i] = 1.0f;
		}
	}

	void InitCamera()
	{
		m_ClipFar = 10000.0f;
	
		float pos[3];
		pos[0] = 0.0f;
		pos[1] = 0.0f;
		pos[2] = 0.0f;
		PARAM_startpos.GetArray(pos, 3);

		Vector3 startPos(pos[0], pos[1], pos[2]);

		grcSampleManager::InitSampleCamera(Vector3(-34.0f, 17.0f, 47.0f) + startPos, startPos);
	}

/*
	static bool PointNearOrigin(const spdBdamLocation& loc) {
		return loc.GetOverlappingBoundingCylinder<spdAAInfCylinderY>(m_GridSize).Contains(Vector3(0.0f, 0.0f, 0.0f));
	}
*/
	void InitClient()
	{
		const char* tex = NULL;
		PARAM_tex.Get(tex);

		if (tex) {
			m_DiffuseTexture = grcTextureFactory::GetInstance().Create(tex);
		}
		else {
			m_DiffuseTexture = grcTextureFactory::GetInstance().Create("none");
		}

		m_DrawTheoreticalGrid = PARAM_tunelods.Get();
		m_DrawTheoreticalSquareGrid = PARAM_tunesquares.Get();

		m_DrawActualSquareGrid = PARAM_squares.Get();

		m_Mapper.Reset();
		m_Mapper.Map(IOMS_KEYBOARD, KEY_W, m_DrawWireframe);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_T, m_DrawTopology);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_F, m_DrawSolid);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_B, m_DrawSharps);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_S, m_DrawSmooth);

		m_Mapper.Map(IOMS_KEYBOARD, KEY_1, m_DrawLod[0]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_2, m_DrawLod[1]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_3, m_DrawLod[2]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_4, m_DrawLod[3]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_5, m_DrawLod[4]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_6, m_DrawLod[5]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_7, m_DrawLod[6]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_T, m_DrawLodTransitions);

		m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LX, m_PoiLeft);
		m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LY, m_PoiUp);

		m_Mapper.Map(IOMS_KEYBOARD, KEY_C, m_ToggleCameraPoi);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_L, m_PrintLocations);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_G, m_DrawGridKey);

		PARAM_nametemplate.Get(m_NameTemplate);

		m_LowestLod = 0;
		PARAM_lod.Get(m_LowestLod);

		m_HighestLod = 0;
		PARAM_highlod.Get(m_HighestLod);

		PARAM_grid.Get(m_GridSize);

		if (PARAM_drawable.Get())
		{
			m_DrawDrawable = true;
			m_DrawDemesh = false;
		}
		else
		{
			m_DrawDrawable = false;
			m_DrawDemesh = true;
		}

		PARAM_radius.Get(m_DrawRadius);

		int range[4];
		if (PARAM_range.GetArray(range, 4)) {
			m_MinX = range[0];
			m_MinY = range[1];
			m_MaxX = range[2];
			m_MaxY = range[3];
		}

		// setup input gizmo

		m_PointOfInterest.Identity3x3();

		float pos[3];
		pos[0] = 0.0f;
		pos[1] = 0.0f;
		pos[2] = 0.0f;
		PARAM_startpos.GetArray(pos, 3);

		m_PointOfInterest.d.Set(pos[0], pos[1], pos[2]);

		m_PoiAxes = new gzTranslation(m_PointOfInterest);
		m_PoiAxes->SetSize(30.0f);

		const char* textureList = NULL;
		if (PARAM_textures.Get(textureList))
		{
			Printf("Loading terrain textures\n");

			const char* texturePath = NULL;
			if (PARAM_texturePath.Get(texturePath))
			{
				ASSET.PushFolder(texturePath);
			}

			fiSafeStream texList(ASSET.Open(textureList, "texlist"));
			if (texList)
			{
				fiTokenizer tok;
				tok.Init("texlist", texList);
				char textureName[RAGE_MAX_PATH];
				while(tok.GetToken(textureName, RAGE_MAX_PATH))
				{
					const char* shortTexName = fiAssetManager::FileName(textureName);
					grcTexture* tex = grcTextureFactory::GetInstance().Create(textureName);
					grcTextureFactory::GetInstance().RegisterTextureReference(shortTexName, tex);
				}
			}

			if (PARAM_texturePath.Get(texturePath))
			{
				ASSET.PopFolder();
			}
		}

	}

	deMesh* LoadLocation(const spdBdamLocation& loc) {
		deMesh* mesh = NULL;
		char buf[256];
		loc.FormatLocationString(buf, m_NameTemplate);
		if (ASSET.Exists(buf, "dmsh")) {
			mesh = deMesh::CreateFromFile(buf);
		}
		return mesh;
	}

	void LoadFile(const char* baseName, spdBdamNode<deMesh>* node)
	{
		char buf[256];
		sprintf(buf, "%s_L%d_c%d_r%d_%s.dmsh", baseName, 
			node->m_Location.m_Level, 
			node->m_Location.m_Col, 
			node->m_Location.m_Row,
			node->m_Location.GetDirName());
		ASSET.PushFolder(m_DirName);
		if (ASSET.Exists(buf, "dmsh")) {
			node->m_Data = deMesh::CreateFromFile(buf);
		}
		ASSET.PopFolder();
	}

	void UpdateClient()
	{
		m_Mapper.Update();

		if (m_DrawWireframe.IsPressed()) {
			m_DrawStyle = WIREFRAME;	
		}
		if (m_DrawTopology.IsPressed()) {
			m_DrawStyle = TOPOLOGY;
		}
		if (m_DrawSolid.IsPressed()) {
			m_DrawStyle = SOLID;
		}
		if (m_DrawSharps.IsPressed()) {
			m_DrawStyle = SHARPS;
		}
		if (m_DrawSmooth.IsPressed()) {
			m_DrawStyle = SMOOTH;
		}

		if (m_DrawLod[0].IsPressed()) {
			m_LodState = LOD_0;
		}
		if (m_DrawLod[1].IsPressed()) {
			m_LodState = LOD_1;
		}
		if (m_DrawLod[2].IsPressed()) {
			m_LodState = LOD_2;
		}
		if (m_DrawLod[3].IsPressed()) {
			m_LodState = LOD_3;
		}
		if (m_DrawLod[4].IsPressed()) {
			m_LodState = LOD_4;
		}
		if (m_DrawLod[5].IsPressed()) {
			m_LodState = LOD_5;
		}
		if (m_DrawLod[6].IsPressed()) {
			m_LodState = LOD_6;
		}
		if (m_DrawLodTransitions.IsPressed()) {
			m_LodState = LOD_TRANSITIONS;
		}

		if (m_ToggleCameraPoi.IsPressed()) {
			m_UseCameraAsPoi = !m_UseCameraAsPoi;
		}

		m_PointOfInterest.d.Add(Vector3(m_PoiLeft.GetNorm(0.3f) * TIME.GetSeconds() * 100.0f, 0.0, m_PoiUp.GetNorm(0.3f) * TIME.GetSeconds() * 100.0f));
	}

	void DrawMesh(deMesh* mesh)
	{
		switch (m_DrawStyle) {
		case WIREFRAME:
			mesh->DrawFast();
			break;
		case TOPOLOGY:
			mesh->DebugDraw();
			break;
		case SHARPS:
			mesh->DrawSharps();
			break;
		case SOLID:
			mesh->DrawSolid();
			break;
		case SMOOTH:
			grcBindTexture( m_DiffuseTexture);
			mesh->DrawSmooth();
			break;
		}
	}

	struct PointOfInterestCheck {
//		bool IsNodeNear(spdBdamNode<deMesh>* node) {
//			return node->m_Cylinder.Contains(m_Point);
//		}
		bool IsNodeNear(const spdBdamLocation& loc) {
			if (loc.m_Level <= m_HighestLod) {
				return false;
			}
			return loc.GetOverlappingBoundingCylinder<spdAAInfCylinderY>(256.0f).Contains(m_Point);
		}
		Vector3 m_Point;
		int m_HighestLod;
	};

	void DrawNode(spdBdamNode<deMesh>* node)
	{
		// draw something
		grcState::Default();
		grcLightState::SetEnabled(false);
		grcState::Default();

		grcColor(Color32(255, 255, 255));

		if (node->m_Data) {
			DrawMesh(node->m_Data);
		}

		Color32 baseColor;

		DrawLocation(node->m_Location);
	}


	void DrawLocation(spdBdamLocation loc)
	{
		grcState::Default();
		grcLightState::SetEnabled(false);
		grcState::Default();
		Matrix34 iden;
		iden.Identity();
		grcViewport::SetCurrentWorldMtx(iden);

		Color32 thisColor = LevelColors[loc.m_Level];
		thisColor.SetAlpha(128);
		Color32 nextColor = LevelColors[loc.m_Level+1];
		nextColor.SetAlpha(128);

		Color32 avgColor;//.) = thisColor; //thisColor * Color32(128, 128, 128) + nextColor * Color32(128, 128, 128);

		Vector2 a,b,c;
		loc.GetCornerLocations(m_GridSize, a,b,c);

		Vector3 edgeA, edgeB, edgeC;
		edgeA.Set(a.x, 0.0f, a.y);
		edgeB.Set(b.x, 0.0f, b.y);
		edgeC.Set(c.x, 0.0f, c.y);

		avgColor.Setf(
			0.5f * (thisColor.GetRedf() + nextColor.GetRedf()),
			0.5f * (thisColor.GetGreenf() + nextColor.GetGreenf()),
			0.5f * (thisColor.GetBluef() + nextColor.GetBluef()),
			thisColor.GetAlphaf());

		Vector3 edgeBC;
		edgeBC.Average(edgeB, edgeC);

		grcBegin(drawTriFan, 4);

		grcColor(nextColor);

		grcVertex3f(edgeBC);

		grcColor(avgColor);
		grcVertex3f(edgeB);

		grcColor(thisColor);
		grcVertex3f(edgeA);

		grcColor(avgColor);
		grcVertex3f(edgeC); 
		//		grcVertex3f(edgeA);
		grcEnd();

		Vector3 midA, midB, midC;
		midA.Average(edgeB, edgeC);
		midB.Average(edgeA, edgeC);
		midC.Average(edgeA, edgeB);

		Vector3 innerA, innerB, innerC;
		innerA.Average(midB, midC);
		innerB.Average(midA, midC);
		innerC.Average(midA, midB);

		midA.Lerp(0.2f, edgeA, innerA);
		midB.Lerp(0.2f, edgeB, innerB);
		midC.Lerp(0.2f, edgeC, innerC);

		grcColor(thisColor);

		midA.y = midB.y = midC.y = 0.2f;

        grcBegin(drawLineStrip, 4);
		grcVertex3f(midA);
		grcVertex3f(midB);
		grcVertex3f(midC);
		grcVertex3f(midA);
		grcEnd();

		Vector3 realMid;
		realMid.Average(midA, midB);
		realMid.Average(midC);

		grcColor3f(1.0f, 1.0f, 1.0f);
		grcDrawLabelf(realMid, "L%d %d,%d %s", loc.m_Level, loc.m_Row, loc.m_Col, loc.GetDirName());
	}

	void DrawTheoreticalGrid(spdBdamLocation loc)
	{
		/*
		for(int i = 0; i < 10; i++) {
			splitter.m_RadiusFactors[i] = m_RadiusLevelFactors[i];
		}
		*/

		Vector3 pointOfInterest;
		if (m_UseCameraAsPoi)
		{
			pointOfInterest = GetCameraMatrix().d;
		}
		else
		{
			pointOfInterest = m_PointOfInterest.d;

		}

		atFunctor1<bool, const spdBdamLocation&> functor;

		PointOfInterestCheck p;
		p.m_Point = pointOfInterest;
		p.m_HighestLod = m_HighestLod;

		functor.Reset<PointOfInterestCheck, &PointOfInterestCheck::IsNodeNear>(&p);

		spdBdamTreelessLocationIterator iter;
		iter.Init(loc, functor);
		do
		{
			DrawLocation(iter.GetCurrent());
		}
		while(iter.MoveNext());
	}

	bool ShouldSplitSquare(int lod, int row, int col, Vector3::Vector3Param poi)
	{
		Vector3 squareCenter;
		squareCenter.x = ((float)col + 0.5f) * (m_GridSize * (1 << lod));
		squareCenter.y = 0.0f;
		squareCenter.z = ((float)row + 0.5f) * (m_GridSize * (1 << lod));

//		float squareRadius = SQRT2DIV2 * 1.5f * m_GridSize * (1 << lod);

		float squareDim = m_GridSize * (1 << lod);

		if (fabsf(squareCenter.x - poi.x) < squareDim && fabsf(squareCenter.z - poi.z) < squareDim)
		{
			return true;
		}

//		if (poi.Dist2(squareCenter) < squareRadius * squareRadius)
//		{
//			return true;
//		}
		return false;
	}

	void DrawTheoreticalSquareGrid_Helper(int lod, int row, int col, Vector3::Vector3Param poi)
	{
		if (lod > 0 && ShouldSplitSquare(lod, row, col, poi))
		{
			DrawTheoreticalSquareGrid_Helper(lod-1, row*2, col*2, poi);
			DrawTheoreticalSquareGrid_Helper(lod-1, row*2, col*2+1, poi);
			DrawTheoreticalSquareGrid_Helper(lod-1, row*2+1, col*2+1, poi);
			DrawTheoreticalSquareGrid_Helper(lod-1, row*2+1, col*2, poi);
		}
		else
		{
			grcState::Default();
			grcLightState::SetEnabled(false);
			grcState::Default();
			Matrix34 iden;
			iden.Identity();
			grcViewport::SetCurrentWorldMtx(iden);

			Color32 thisColor = LevelColors[lod+1];
			thisColor.SetAlpha(128);

			grcBegin(drawTriFan, 4);

			grcColor(thisColor);

			float scale = (1 << lod) * m_GridSize;

			grcVertex3f(col * scale, 0.0f, row * scale);
			grcVertex3f(col * scale, 0.0f, (row+1) * scale);
			grcVertex3f((col+1) * scale, 0.0f, (row+1) * scale);
			grcVertex3f((col+1) * scale, 0.0f, row * scale);
			grcEnd();
		}
	}

	void DrawActualSquareGrid_Helper(int lod, int row, int col, Vector3::Vector3Param poi)
	{
		if (lod > 0 && ShouldSplitSquare(lod, row, col, poi))
		{
			DrawActualSquareGrid_Helper(lod-1, row*2, col*2, poi);
			DrawActualSquareGrid_Helper(lod-1, row*2, col*2+1, poi);
			DrawActualSquareGrid_Helper(lod-1, row*2+1, col*2+1, poi);
			DrawActualSquareGrid_Helper(lod-1, row*2+1, col*2, poi);
		}
		else
		{
			// make a spdBdamLocation we can use to draw with
			spdBdamLocation loc(lod, row, col, spdBdamLocation::DIR_N);

			DrawAtLocation(loc);
		}
	}

	void DrawTheoreticalSquareGrid(int lod, int row, int col)
	{
		Vector3 pointOfInterest;
		if (m_UseCameraAsPoi)
		{
			pointOfInterest = GetCameraMatrix().d;
		}
		else
		{
			pointOfInterest = m_PointOfInterest.d;
		}

		DrawTheoreticalSquareGrid_Helper(lod, row, col, pointOfInterest);

	}
	
	void DrawAtLocation(spdBdamLocation loc)
	{
		if (m_DrawDemesh)
		{
			DrawDemeshAtLocation(loc);
		}
		if (m_DrawDrawable)
		{
			DrawDrawableAtLocation(loc);
		}

	}

	void DrawDemeshAtLocation(spdBdamLocation loc)
	{
		deMesh* mesh = NULL;
		bool found = m_DemeshCache.Read(loc, mesh);
		if (!found)
		{
			// load a mesh, add it to the cache
			char fileName[RAGE_MAX_PATH];
			loc.FormatLocationString(fileName, m_NameTemplate);

			if (ASSET.Exists(fileName, "dmsh"))
			{
				mesh = new deMesh;
				mesh->Load(fileName);
				m_DemeshCache.Store(loc, mesh);
			}
			else
			{
				mesh = NULL;
			}
		}

		if (mesh)
		{
			spdBdamNode<deMesh> node;
			node.m_Data = mesh;
			node.m_Location = loc;
			DrawNode(&node);
			m_numMeshesThisFrame++;
		}
	}

	void DrawDrawableAtLocation(spdBdamLocation loc)
	{
		if (m_KnownNonExistantLocations.find(loc) != m_KnownNonExistantLocations.end())
		{
			return;
		}

		char fileName[RAGE_MAX_PATH];

		rmcDrawable* drawable = NULL;
		bool found = m_DrawableCache.Read(loc, drawable);
		if (!found)
		{
			// load a drawable, add it to the cache
			loc.FormatLocationString(fileName, m_NameTemplate);

			if (ASSET.Exists(fileName, "type"))
			{
				char dirName[RAGE_MAX_PATH];
				fiAssetManager::RemoveNameFromPath(dirName, RAGE_MAX_PATH, fileName);
				ASSET.PushFolder(dirName);

				drawable = new rmcDrawable();
				if (drawable->Load(fileName))
				{
					m_DrawableCache.Store(loc, drawable);
				}
				else
				{
					delete drawable;
					drawable = NULL;
				}

				ASSET.PopFolder();
			}
			else
			{
				m_KnownNonExistantLocations.insert(loc);
			}

		}

		if (drawable)
		{
			if (m_PrintLocations.IsPressed())
			{
				loc.FormatLocationString(fileName, m_NameTemplate);
				Printf("%s\n", fileName);
			}
			drawable->Draw(M34_IDENTITY, 0, 0);
			m_numMeshesThisFrame++;
		}
	}

	void DrawClient()
	{
		if (m_DrawTheoreticalGrid || m_DrawGridKey.IsDown())
		{
			for(int i = -10; i < 10; i++) {
				for(int j = -10; j < 10; j++) {
					DrawTheoreticalGrid(spdBdamLocation(4, i, j, spdBdamLocation::DIR_N));
					DrawTheoreticalGrid(spdBdamLocation(4, i, j, spdBdamLocation::DIR_E));
					DrawTheoreticalGrid(spdBdamLocation(4, i, j, spdBdamLocation::DIR_W));
					DrawTheoreticalGrid(spdBdamLocation(4, i, j, spdBdamLocation::DIR_S));
				}
			}
			return;
		}

		if (m_DrawTheoreticalSquareGrid)
		{
			for(int i = -10; i < 10; i++) {
				for(int j = -10; j < 10; j++) {
					DrawTheoreticalSquareGrid(4, i, j);
				}
			}
			return;
		}

		Vector3 pointOfInterest;
		if (m_UseCameraAsPoi)
		{
			pointOfInterest = GetCameraMatrix().d;
		}
		else
		{
			pointOfInterest = m_PointOfInterest.d;
		}

		if (m_DrawActualSquareGrid)
		{
			grcViewport::SetCurrentWorldIdentity();

			spdBdamLocation loc;
			loc.m_Dir = spdBdamLocation::DIR_N;
			loc.m_Level = m_LowestLod;
			loc.m_Col = (int)(pointOfInterest.x / (m_GridSize * (1 << m_LowestLod)));
			loc.m_Row = (int)(pointOfInterest.z / (m_GridSize * (1 << m_LowestLod)));

			// Draw +/- radius cells
			for(int row = loc.m_Row-m_DrawRadius; row < loc.m_Row+m_DrawRadius; row++)
			{
				for(int col = loc.m_Col-m_DrawRadius; col < loc.m_Col+m_DrawRadius; col++)
				{
					DrawActualSquareGrid_Helper(m_LowestLod, row, col, pointOfInterest);
				}
			}

			return;
		}



		spdBdamLocation loc = spdBdamLocation::FindLocationContainingPoint(Vector2(pointOfInterest.x, pointOfInterest.z), m_LowestLod, m_GridSize);

		int beginDir, endDir;
		if (loc.m_Level % 2 == 0)
		{
			beginDir = spdBdamLocation::DIR_N;
			endDir = spdBdamLocation::DIR_W;
		}
		else
		{
			beginDir = spdBdamLocation::DIR_NE;
			endDir = spdBdamLocation::DIR_NW;
		}


		PointOfInterestCheck p;
		p.m_Point = pointOfInterest;
		p.m_HighestLod = m_HighestLod;

		atFunctor1<bool, const spdBdamLocation&> fn;
		fn.Reset<PointOfInterestCheck, &PointOfInterestCheck::IsNodeNear>(&p);

		m_numMeshesThisFrame = 0;

		// Draw +/- radius cells
		for(int row = loc.m_Row-m_DrawRadius; row < loc.m_Row+m_DrawRadius; row++)
		{
			for(int col = loc.m_Col-m_DrawRadius; col < loc.m_Col+m_DrawRadius; col++)
			{
				for(int dir = beginDir; dir <= endDir; ++dir)
				{
					spdBdamLocation drawLoc(loc.m_Level, row, col, (spdBdamLocation::Direction)dir);
					
					spdBdamTreelessLocationIterator treeIter;
					treeIter.Init(drawLoc, fn);

					do
					{
						DrawAtLocation(treeIter.GetCurrent());
					}
					while(treeIter.MoveNext());
				}
			}

			if (m_numMeshesThisFrame > 126)
			{
				Warningf("Need to draw %d meshes this frame. Too big for cache", m_numMeshesThisFrame);
			}
		}

#if 0
		for(std::vector<spdBdamLocation>::iterator ti = m_TopLevelLocations.begin(); ti != m_TopLevelLocations.end(); ++ti) {
			switch(m_LodState) {
				case LOD_0:
				case LOD_1:
				case LOD_2:
				case LOD_3:
				case LOD_4:
				case LOD_5:
				case LOD_6:
					{
						PointOfInterestCheck p;
						if (m_UseCameraAsPoi) {
							p.m_Point = m_CamMgr.GetWorldMtx().d;		
						}
						else {
							p.m_Point = m_PointOfInterest.d;
						}
						p.m_HighestLod = m_HighestLod;

						atFunctor1<bool, const spdBdamLocation&> fn;
						fn.Reset<PointOfInterestCheck, &PointOfInterestCheck::IsNodeNear>(&p);

						spdBdamTreelessLocationIterator treeIter;
						treeIter.Init(*ti, fn);

						do {
							deMesh* mesh = m_MeshCache.Access(treeIter.GetCurrent(), TIME.GetFrameCount());
							if (mesh) {
								spdBdamNode<deMesh> node;
								node.m_Data = mesh;
								node.m_Location = treeIter.GetCurrent();
								DrawNode(&node);
							}
						} while(treeIter.MoveNext());
					}
					break;
				case LOD_TRANSITIONS:
					{
#if 0 // TODO
						dlList drawList;
						dlList splitList;

						spdBdamNode<deMesh>* node = (*ti);

//						node->m_Left->Split(drawList, splitList, m_PointOfInterest.d);
//						node->m_Right->Split(drawList, splitList, m_PointOfInterest.d);

						node->Split(drawList, splitList, m_PointOfInterest.d);

						while( splitList.pNext )
						{
							spdBdamNode<deMesh>* pSplit = (spdBdamNode<deMesh>*)splitList.pNext->pData;
							node->ForceSplit(pSplit->m_SplitVtx, drawList, splitList, m_PointOfInterest.d, pSplit);
							delete splitList.pNext;
						}

						dlList* pIter = drawList.pNext;
						while( pIter )
						{
							spdBdamNode<deMesh>* pNode = (spdBdamNode<deMesh>*)pIter->pData;
							if( pNode->RemoveParent(drawList) )
							{
								pIter = &drawList;
							}
							pIter = pIter->pNext;
						}

						pIter = drawList.pNext;
						while(pIter) {
							spdBdamNode<deMesh>* pNode = (spdBdamNode<deMesh>*)pIter->pData;
							pNode->Draw();
							pIter = pIter->pNext;
						}
#endif
					}
//					(*ti)->Draw(m_PointOfInterest.d);
					break;
				default:
					// Draw nothing
					break;
			}
		}
#endif
	}

#if __BANK
	void AddWidgetsClient()
	{
		bkBank& bank = BANKMGR.CreateBank("BDAM");
		bank.AddSlider("Radius", &m_DrawRadius, 1, 10, 1);
		bank.AddSlider("Radius Bias", &m_RadiusBias, 0.0f, 1000.0f, 0.01f);
		for(int i = 0; i < 10; i++) {
			bank.AddSlider("Radius Level Factor", &m_RadiusLevelFactors[i], 0.0f, 1000.0f, 0.01f);
		}
	}
#endif

private:
	enum {
		MAX_MESHES = 10
	};


	ioMapper	m_Mapper;
	ioValue		m_DrawWireframe;
	ioValue		m_DrawTopology;
	ioValue		m_DrawSolid;
	ioValue		m_DrawSharps;
	ioValue		m_DrawSmooth;

	ioValue		m_DrawLod[7];
	ioValue		m_DrawLodTransitions;

	ioValue		m_PoiLeft;
	ioValue		m_PoiUp;

	ioValue		m_ToggleCameraPoi;

	ioValue		m_PrintLocations;

	ioValue		m_DrawGridKey;

	grcTexture* m_DiffuseTexture;

	enum DrawStyle {
		WIREFRAME,
		TOPOLOGY,
		SOLID,
		SHARPS,
		SMOOTH,
	} ;

	enum LodState {
		LOD_0,
		LOD_1,
		LOD_2,
		LOD_3,
		LOD_4,
		LOD_5,
		LOD_6,
		LOD_TRANSITIONS
	};

	DrawStyle	m_DrawStyle;
	LodState	m_LodState;

	Matrix34	m_PointOfInterest;
	gzTranslation*		m_PoiAxes;

	char		m_DirName[256];

	const char*		m_NameTemplate;
	int			m_LowestLod;
	int			m_HighestLod;
	float		m_GridSize;
	int			m_DrawRadius;
	float		m_RadiusBias;
	float		m_RadiusLevelFactors[10];

	int			m_MinX, m_MaxX, m_MinY, m_MaxY;

	bool		m_UseCameraAsPoi;
	bool		m_DrawTheoreticalGrid;
	bool		m_DrawTheoreticalSquareGrid;
	bool		m_DrawActualSquareGrid;

	int			m_numMeshesThisFrame;

	bool		m_DrawDemesh;
	bool		m_DrawDrawable;

	DemeshCache	m_DemeshCache;
	DrawableCache m_DrawableCache;

	std::set<spdBdamLocation>	m_KnownNonExistantLocations;
};

int Main()
{
	INIT_PARSER;

	demeshViewBdam de;

	

#if __WIN32PC
	char cwd[256];
	_getcwd(cwd, 256);

	de.Init(cwd);
#else
	de.Init();
#endif

	de.UpdateLoop();

	de.Shutdown();

	SHUTDOWN_PARSER;

	return 0;
}
