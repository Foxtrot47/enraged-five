// 
// demeshtools/demesh_to_entity.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "demesh/demesh.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

FPARAM(1, in, "Name of the input file");
FPARAM(2, out, "Name of the output entity directory");
PARAM(snap, "Snap distance for vertices");
PARAM(replacecpv, "Replaces a particular CPV: E.g. -replacecpv=0,255,0,255,255,255 replaces green with white");
PARAM(tristrip, "Outputs tristripped meshes instead of triangles");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");


int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}

	const char* inName = NULL;
	const char* outName = NULL;

	PARAM_in.Get(inName);
	PARAM_out.Get(outName);

	float snap;
	if (PARAM_snap.Get(snap)) {
		deWedge::SetVertSnapDist(snap);
	}

	deMesh* mesh = deMesh::CreateFromFile(inName);

	if (PARAM_replacecpv.Get())
	{
		int colors[6];
		PARAM_replacecpv.GetArray(colors, 6);
		Vector4 fromColor;
		fromColor.Set((float)colors[0], (float)colors[1], (float)colors[2], 1.0f);
		fromColor.Scale(1.0f / 255.0f);


		Vector4 toColor;
		toColor.Set((float)colors[3], (float)colors[4], (float)colors[5], 1.0f);
		toColor.Scale(1.0f / 255.0f);

		for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi)
		{
			deVertex* v = (*vi);
			for(int i = 0; i < v->GetNumWedges(); i++) {
				deWedge& w = v->GetWedge(i);
				if (w.GetVertexData().Cpv.IsClose(fromColor, 3.0f / 255.0f)) { 
					w.GetVertexData().Cpv = toColor;
				}
			}
		}

	}

	/*
	char demeshName[256];
	formatf(demeshName, 256, "%s/%s", outName, "themesh.dmsh");
	mesh->Save(demeshName);
	*/

//	mesh->SaveEntity(outName);
	char demeshName[256];
	formatf(demeshName, 256, "%s/%s", outName, "themesh.dmsh");
	ASSET.CreateLeadingPath(demeshName);
	ASSET.PushFolder(outName);
	mesh->SaveEntity(outName, "entity.type", "themesh.mesh", "mesh", PARAM_tristrip.Get());
	ASSET.PopFolder();

	delete mesh;

	deMesh::CloseErrorStream();
	
	SHUTDOWN_PARSER;

	return 0;
}

