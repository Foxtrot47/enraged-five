// 
// demeshtools/dllist.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEMESHTOOLS_DLLIST_H
#define DEMESHTOOLS_DLLIST_H

//RAGE_QA: NO_NAMESPACE_RAGE

typedef struct _dlList
{
	_dlList*	pPrev;
	_dlList*	pNext;
	void*		pData;

	_dlList()	{pPrev = 0; pNext = 0; pData = 0;}
	~_dlList()
	{
		if( pPrev )
			pPrev->pNext = pNext;
		if( pNext )
			pNext->pPrev = pPrev;
	}

	_dlList* Find(void* data, _dlList* pAfter = NULL)
	{
		_dlList* pIter = this;
		if( !pAfter )
		{
			// start at the beginning
			while( pIter->pPrev )
				pIter = pIter->pPrev;
		}
		else
		{
			pIter = pAfter->pNext;
		}

		// Search for the mesh
		while( pIter )
		{
			if( data == pIter->pData )
				return pIter;

			pIter = pIter->pNext;
		}
		return 0;
	}

	_dlList* Add(void* data)
	{
		// Seek to the end
		_dlList* pIter = this;
		while( pIter->pNext )
			pIter = pIter->pNext;

		_dlList* pNew = new _dlList();
		pNew->pData = data;
		pNew->pPrev = pIter;
		pIter->pNext = pNew;
		return pNew;
	}
} dlList;

#endif
