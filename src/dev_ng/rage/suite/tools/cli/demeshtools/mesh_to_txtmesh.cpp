// 
// demeshtools/mesh_to_demesh.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"

using namespace rage;

FPARAM(1, in, "Name of the input .msh file");
FPARAM(2, out, "Name of the output .msh file");

int Main()
{
	INIT_PARSER;

	const char* inName = NULL;
	const char* outName = NULL;

	PARAM_in.Get(inName);
	PARAM_out.Get(outName);

	//deMesh* mesh = deMesh::CreateFromFile(inName);
	mshMesh* mesh = rage_new mshMesh;
	if ( SerializeFromFile(inName, *mesh, "mesh") == false ) 
	{
		Errorf("Can't load mesh file %s", inName);
		delete mesh;
		return -1;
	}

    fiSafeStream stream(ASSET.Create(outName, "mesh"));
	stream->PutCh(13);
	stream->PutCh(10);
	fiAsciiTokenizer tok;
	mshSerializer serializer(tok,true);
	tok.Init(outName,stream);
	mesh->Serialize(serializer);

	delete mesh;
	
	SHUTDOWN_PARSER;

	return 0;
}


