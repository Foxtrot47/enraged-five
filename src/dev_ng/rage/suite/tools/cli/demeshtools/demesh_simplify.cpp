// 
// demeshtools/demesh_simplify.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define SIMPLE_HEAP_SIZE (1024 * 1024)

#include "demesh/demesh.h"
#include "demesh/desimplify.h"
#include "parser/manager.h"
#include "profile/element.h"
#include "profile/group.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PF_GROUP(DirEdge);

PF_TIMER(LoadTimer, DirEdge);
PF_TIMER(HeapTimer, DirEdge);
PF_TIMER(EcolTimer, DirEdge);

PF_TIMER(EcolCollapseTimer, DirEdge);
PF_TIMER(EcolErrorEvalTimer, DirEdge);
PF_TIMER(EcolFoldoverTimer, DirEdge);
PF_TIMER(EcolTopoCheckTimer, DirEdge);
PF_TIMER(EcolHeapOpsTimer, DirEdge);

FPARAM(1, in, "Name of the input .dmsh file");
FPARAM(2, out, "Name of the output .dmsh file");
PARAM(dihed, "Sets the dihedral plane scale. Default = 0.01. (higher = fewer skinny triangles, but less concentrated simplification)");
PARAM(bound, "Sets the boundary plane scale. Default = 100.0. (higher = less likely to move verts away from a boundary edge)");
PARAM(bounddi, "Sets the dihedral plane scale for boundary verts. Default = 20.0. (higher = less likely to move verts along a boundary edge)");
PARAM(disc, "Sets the discontinuity scale. Default = 1.0 (higher = preserve material boundaries better)");
PARAM(error, "Collapse until the error is >= this value");
PARAM(keeppercent, "Collapse until N% of the polys remain");
PARAM(removepolys, "Collapse until we've removed N polys");
PARAM(keeppolys, "Collapse until N polys remain");
PARAM(fixborders, "If set, border edges won't be simplified at all (normally they just have a low chance of being simplified)");
PARAM(snap, "Snap distance for vertices");
PARAM(yscale, "An additional Y scale factor, to increase height error");
PARAM(cube, "Scale the mesh to a unit cube before simplification");
PARAM(fixborderverts,"If set, border verts will not be moved away from the border, but simplification is allowed along the border");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");

int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}

	deMesh* mesh = NULL;

	float snap;
	if (PARAM_snap.Get(snap)) {
		deWedge::SetVertSnapDist(snap);
	}

	const char* inName = NULL;
	PARAM_in.Get(inName);

	const char* outName = NULL;
	PARAM_out.Get(outName);

	if (!inName || !outName)
	{
		sysParam::Help();
		return 1;
	}
	PF_START(LoadTimer);
	mesh = deMesh::CreateFromFile(inName);
	PF_STOP(LoadTimer);
	Assert(mesh);

#if __STATS
	Printf("Loaded in %fms\n", PF_READ_TIME_MS(LoadTimer));
#endif

	if (PARAM_fixborders.Get()) {
		for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi) {
			(*vi)->SetCollapseAllowed(!(*vi)->IsOnBoundary());
		}
	}

	if (PARAM_fixborderverts.Get()) {
		for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi) {
			(*vi)->SetFixed((*vi)->IsOnBoundary());
		}
	}

	Vector3 minBounds, maxBounds;
	mesh->FindBounds(minBounds, maxBounds);
	Vector3 center;
	center.Average(minBounds, maxBounds);
	mesh->OffsetMesh(-center);

	Vector3 scale(1.0f, 1.0f, 1.0f);
	Vector3 invScale;
	if (PARAM_cube.Get())
	{
		scale = (maxBounds - minBounds);
	}

	float yScale = 1.0f;
	PARAM_yscale.Get(yScale);
	scale.y /= yScale;

	invScale.Invert(scale);

	mesh->ScaleMesh(invScale);

	PF_START(HeapTimer);
	deSimplify* simplify = rage_new deSimplify;
	simplify->Init(mesh);
	PF_STOP(HeapTimer);

#if __STATS
	Printf("Built heap in %fms\n", PF_READ_TIME_MS(HeapTimer));
#endif

	float errScale;
	if (PARAM_dihed.Get(errScale))
	{
		simplify->SetDihedralScale(errScale);
	}
	if (PARAM_bound.Get(errScale))
	{
		simplify->SetBoundaryScale(errScale);
	}
	if (PARAM_bounddi.Get(errScale))
	{
		simplify->SetBoundaryDihedralScale(errScale);
	}
	if (PARAM_disc.Get(errScale))
	{
		simplify->SetDiscontinuityScale(errScale);
	}
	
	int removedPolys = 0;
	int numEcols = 0;

	if (PARAM_error.Get()) {
		Assert(!PARAM_keeppercent.Get());
		Assert(!PARAM_removepolys.Get());
		Assert(!PARAM_keeppolys.Get());

		float maxError = 0.0f;
		PARAM_error.Get(maxError);

		PF_START(EcolTimer);
		while(maxError > simplify->GetNextCollapseError()) {
			int justRemoved = simplify->CollapseMinEdge();
			if (justRemoved < 0) {
				break;
			}
			else {
				removedPolys += justRemoved;
			}
			if (justRemoved > 0) {
				numEcols++;
			}
		}
		PF_STOP(EcolTimer);
	}
	else {
		int numToRemove = 0;

		if (PARAM_removepolys.Get()) {
			Assert(!PARAM_keeppercent.Get());
			Assert(!PARAM_keeppolys.Get());
			PARAM_removepolys.Get(numToRemove);
		}
		else {
			// count total polys
			int numPolys = 0;
			for(deMesh::FaceEdgeIterator fi = mesh->BeginFaceEdges(); fi != mesh->EndFaceEdges(); ++fi)
			{
				numPolys++;
			}

			if (PARAM_keeppercent.Get()) {
				Assert(!PARAM_keeppolys.Get());
				float pct;
				PARAM_keeppercent.Get(pct);
				numToRemove = (int)((1.0f - (pct * 0.01f)) * numPolys);
			}
			else if (PARAM_keeppolys.Get()) {
				int kept;
				PARAM_keeppolys.Get(kept);
				numToRemove = numPolys - kept;
			}
			else {
				Assert("One of -error, -keeppercent, -removepolys, or -keeppolys must be specified");
			}
		}

		PF_START(EcolTimer);
		while(removedPolys < numToRemove) {
			int justRemoved = simplify->CollapseMinEdge();
			if (justRemoved < 0) {
				break;
			}
			else {
				removedPolys += justRemoved;
			}
			if (justRemoved > 0) {
				numEcols++;
			}
		}
		PF_STOP(EcolTimer);
	}

#if __STATS
	Printf("Removed %d polys, %d edges in %f ms (%f ecols/sec)\n", removedPolys, numEcols, PF_READ_TIME_MS(EcolTimer), (float)numEcols / PF_READ_TIME(EcolTimer));
	Printf("Ecol time = %f, Err. prop time = %f\n", PF_READ_TIME_MS(EcolCollapseTimer), PF_READ_TIME_MS(EcolErrorEvalTimer));
#endif
	Printf("Final terrain err. value is %f\n", simplify->GetNextCollapseError());

	mesh->ScaleMesh(scale);

	mesh->OffsetMesh(center);

	mesh->Save(outName);

	delete simplify;
	delete mesh;

	deMesh::CloseErrorStream();
	
	SHUTDOWN_PARSER;

	return 0;
}
