// 
// /demesh_add_skirt.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "demesh/demesh.h"
#include "demesh/deedge.h"
#include "demesh/devertex.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

FPARAM(1, in, "Name of the input .dmsh");
FPARAM(2, out, "Name of the output .dmsh");
PARAM(height, "Height of the skirt");
PARAM(snap, "Snap distance for vertices");
PARAM(justskirt, "Just write the skirt to the output .dmsh file, not the combined mesh");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");

bool IsProcessed(deEdge* edge)
{
	return edge->m_Flags.IsSet(deEdge::FLAG_PROCESSED);
}

int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}

	const char* in = NULL;
	PARAM_in.Get(in);
	Assert(in);

	const char* out = NULL;
	PARAM_out.Get(out);
	Assert(out);

	float snap;
	if (PARAM_snap.Get(snap)) {
		deWedge::SetVertSnapDist(snap);
	}

	deMesh* mesh = deMesh::CreateFromFile(in);

	float minX, maxX, minZ, maxZ;
	mesh->FindApproxBounds(minX, maxX, minZ, maxZ);

	mesh->InitGrid(minX, maxX, minZ, maxZ, 100, 100);

	// mark all boundary edges as unprocessed
	// for each boundary edge: if it's unprocessed, add a skirt poly (with edges marked as processed)

	mesh->MarkVertsUnprocessed();

	float skirtHeight = 10.0f;
	PARAM_height.Get(skirtHeight);
	Vector3 skirtOffset(0.0f, -skirtHeight, 0.0f);

	deMesh* skirtMesh = new deMesh("skirt");
	skirtMesh->m_Materials.Resize(mesh->m_Materials.GetCount()); // TODO: Only copy over materials in use
	for(int i = 0; i < mesh->m_Materials.GetCount(); i++) {
		skirtMesh->m_Materials[i] = mesh->m_Materials[i];
	}

	for(deMesh::EdgeIterator ei = mesh->BeginEdges(); ei != mesh->EndEdges(); ++ei)
	{
		if ((*ei).IsOnBoundary())
		{
			deEdge* edge = &*ei;


			deMesh::FaceInfo face1, face2;

			face1.inoutVtx[0] = NULL;
			face1.inoutVtx[1] = NULL;
			face1.inoutVtx[2] = NULL;
			
			face1.inData[0] = edge->GetStartWedge();
			face1.inData[1] = edge->GetStartWedge();
			face1.inData[1].SetPosition(face1.inData[1].GetPosition() + skirtOffset);
			face1.inData[2] = edge->GetEndWedge();

			skirtMesh->AddFace(face1);

			face2.inoutVtx[0] = NULL;
			face2.inoutVtx[1] = NULL;
			face2.inoutVtx[2] = NULL;
			face2.inData[0] = edge->GetEndWedge();
			face2.inData[1] = edge->GetStartWedge();
			face2.inData[1].SetPosition(face2.inData[1].GetPosition() + skirtOffset);
			face2.inData[2] = edge->GetEndWedge();
			face2.inData[2].SetPosition(face2.inData[2].GetPosition() + skirtOffset);

			skirtMesh->AddFace(face2);
		}

	}

//	mesh->ReduceEdgeSets();

	skirtMesh->ReduceEdgeSets();

	if (PARAM_justskirt.Get())
	{
		skirtMesh->Save(out);
	}
	else
	{
		mesh->Stitch(*skirtMesh);
		mesh->Save(out);
	}

	delete skirtMesh;
	delete mesh;

	deMesh::CloseErrorStream();

	SHUTDOWN_PARSER;

	return 0;
}

