// 
// demeshtools/demesh_to_bound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "demesh/demesh.h"
#include "parser/manager.h"
#include "phbound/boundbvh.h"
#include "phbound/boundgeom.h"
#include "phbound/boundgrid.h"
#include "phcore/materialmgrflag.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

FPARAM(1, in, "Name of the input .dmsh file");
FPARAM(2, out, "Name of the output .bnd file");
PARAM(snap, "Snap distance for vertices");
PARAM(grid, "Octree grid size");
PARAM(geom, "Just create a phBoundGeometry, not an octtreegrid");
PARAM(maxverts, "Max verts per cell (default 65535)");
PARAM(maxpolys, "Max polys per cell (default 65535)");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");

int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}
	
	phMaterialMgrImpl<phSurface, phMaterialMgrFlag>::Create();

	const char* inName = NULL;
	const char* outName = NULL;

	PARAM_in.Get(inName);
	PARAM_out.Get(outName);

	float snap;
	if (PARAM_snap.Get(snap)) {
		deWedge::SetVertSnapDist(snap);
	}

	deMesh* mesh = deMesh::CreateFromFile(inName);

	int numVerts = mesh->m_Vertices.size();

	for(int i = 0; i < numVerts; i++) 
	{
		mesh->m_Vertices[i]->m_Index = i;
	}

	int numPolys = 0;
	for(deMesh::FaceEdgeIterator fei = mesh->BeginFaceEdges(); fei != mesh->EndFaceEdges(); ++fei)
	{
		numPolys++;
	}

    // build a phBoundGeometry
	phBoundGeometry* boundGeom = new phBoundGeometry();
#if HACK_GTA4
	boundGeom->Init(numVerts, 0, 0, numPolys, 0);
#else
	boundGeom->Init(numVerts, 0, numPolys);
#endif

	for(int i = 0; i < numVerts; i++)
	{
		boundGeom->SetVertex(i, mesh->m_Vertices[i]->GetPosition());
	}

	int polyNum = 0;
	for(deMesh::FaceEdgeIterator fei = mesh->BeginFaceEdges(); fei != mesh->EndFaceEdges(); ++fei)
	{
		phPolygon& poly = *const_cast<phPolygon*>(&boundGeom->GetPolygon(polyNum));
		deVertex *a,*b,*c;
		fei->GetFaceVerts(a,b,c);
		Vector3 vertA(boundGeom->GetVertex(a->m_Index));
		Vector3 vertB(boundGeom->GetVertex(b->m_Index));
		Vector3 vertC(boundGeom->GetVertex(c->m_Index));
		poly.InitTriangle((phPolygon::Index)a->m_Index, (phPolygon::Index)b->m_Index, (phPolygon::Index)c->m_Index, vertA, vertB, vertC);

		polyNum++;
	}

	boundGeom->CalculateGeomExtents();

	if (PARAM_geom.Get())
	{
		phBound::Save(outName, boundGeom);
	}
	else
	{
		int maxVerts = 65535;
		PARAM_maxverts.Get(maxVerts);

		int maxPolys = 65535;
		PARAM_maxpolys.Get(maxPolys);

		float gridSize = 128.0f;
		PARAM_grid.Get(gridSize);

		Vector3 min, max;
		mesh->FindBounds(min, max);

		int startX = (int)floorf(min.GetX() / gridSize);
		int endX = (int)ceilf(max.GetX() / gridSize);
		int startZ = (int)floorf(min.GetZ() / gridSize);
		int endZ = (int)ceilf(max.GetZ() / gridSize);

		phBoundGrid* grid = new phBoundGrid();

		grid->InitGrid<phBoundBVH>(gridSize, startX, endX, startZ, endZ);

#if __TOOL
		grid->AddGeometry(boundGeom);
#endif

		phBound::Save(outName, grid);

		delete grid;
	}

	delete boundGeom;
	delete mesh;

	deMesh::CloseErrorStream();
	
	SHUTDOWN_PARSER;
	
	return 0;
}
