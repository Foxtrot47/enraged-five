// 
// demeshtools/demesh_view.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define SIMPLE_HEAP_SIZE (1024 * 1024)

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/slider.h"
#include "demesh/demesh.h"
#include "demesh/desimplify.h"
#include "grcore/device.h"
#include "grcore/im.h"
#include "input/keyboard.h"
#include "input/mouse.h"
#include "math/random.h"
#include "parser/manager.h"
#include "profile/element.h"
#include "profile/group.h"
#include "sample_grcore/sample_grcore.h"
#include "system/main.h"
#include "system/param.h"
#include "vector/colors.h"
#include "vector/vector3.h"

#if __WIN32PC
#include <direct.h> //RAGE_QA: INCLUDE_OK
#endif

using namespace rage;
using namespace ragesamples;

PF_GROUP(DirEdge);

PF_TIMER(LoadTimer, DirEdge);

PF_TIMER(EcolCollapseTimer, DirEdge);
PF_TIMER(EcolErrorEvalTimer, DirEdge);
PF_TIMER(EcolFoldoverTimer, DirEdge);
PF_TIMER(EcolTopoCheckTimer, DirEdge);
PF_TIMER(EcolHeapOpsTimer, DirEdge);


FPARAM(1, in, "File to view");
PARAM(tex, "Texture to use for preview");
PARAM(nocenter, "Don't center the meshes in the view (leave them in world space)");
PARAM(snap, "Snap distance for vertices");
PARAM(showerrors, "Shows errors saved in a file");
PARAM(simplify, "Interactive simplification for the first mesh you're viewing");
PARAM(findholes, "Merges all of the input meshes together and shows where all the holes smaller than Xm^2 are");

const int kNumMaterialColors = 20;
Color32 g_MaterialColors[kNumMaterialColors];

deSimplify* g_Simplify = NULL;


enum DrawStyle {
	WIREFRAME,
	TOPOLOGY,
	SOLID,
	SHARPS,
	SHARPS_PLUS_WIREFRAME,
	BOUNDARIES_PLUS_WIREFRAME,
	SMOOTH,
	NORMALS,
	TEX_COORDS,
	STARS,
	MATERIALS,
	WEDGE,

	NUM_DRAW_STYLES
} ;

const char* DrawStyleNames[] = {
	"Wireframe",
	"Topology (slow)",
	"Solid (flat shaded)",
	"Boundaries and Sharp Edges",
	"Boundaries and Sharp Edges with Wireframe",
	"Boundaries only with Wireframe",
	"Smooth shaded with CPVs",
	"Smooth shaded with Normals",
	"Smooth shaded with Tex coords",
	"Half-edges",
	"Materials",
	"Materials and Wedges"
};

void InitMaterialColors()
{
	g_MaterialColors[0] = Color_PaleTurquoise3;
	g_MaterialColors[1] = Color_DarkSlateGray2;
	g_MaterialColors[2] = Color_OliveDrab4;
	g_MaterialColors[3] = Color_bisque2;
	g_MaterialColors[4] = Color_IndianRed3;
	g_MaterialColors[5] = Color_LightGoldenrod;
	g_MaterialColors[6] = Color_copper;
	g_MaterialColors[7] = Color_ivory;
	g_MaterialColors[8] = Color_MediumOrchid;
	g_MaterialColors[9] = Color_purple2;
	g_MaterialColors[10] = Color_pink;
	g_MaterialColors[11] = Color_green3;
	g_MaterialColors[12] = Color_grey50;
	g_MaterialColors[13] = Color_orange2;
	g_MaterialColors[14] = Color_turquoise2;
	g_MaterialColors[15] = Color_tan3;
	g_MaterialColors[16] = Color_DarkOliveGreen3;
	g_MaterialColors[17] = Color_cornsilk2;
	g_MaterialColors[18] = Color_grey80;
	g_MaterialColors[19] = Color_PaleGreen;
}

inline Color32 GetMaterialColor(int index)
{
	return g_MaterialColors[index % kNumMaterialColors];
}

class demeshView : public grcSampleManager
{
public:
	demeshView() 
	: m_DiffuseTexture(NULL)
	, m_DrawStyle(SHARPS_PLUS_WIREFRAME)
	, m_DepthBias(-3.f)
	, m_WedgeSize(0.25f)
	, m_RandomAmount(0.3f)
	, m_PickedEdge(NULL)
	, m_PickedMesh(NULL)
	, m_ErrorList(NULL)
	, m_SimplificationPerFrame(0)
	{
		m_PickedVertexPos.Zero();
		m_LightMode = true;
		m_Offset.Zero();

		for(int i = 0; i < MAX_MESHES; i++)
		{
			m_TopoMesh[i] = NULL;
		}
	}

	void InitSetup(const char *p1,const char *p2)
	{
		KEYBOARD.SetUseKeyboardInBackground(false);
#if __WIN32PC
		GRCDEVICE.SetBlockOnLostFocus(false);
#endif

		grcSampleManager::InitSetup(p1, p2);
	}

	void InitCamera()
	{
		m_ClipFar = 10000.0f;
		grcSampleManager::InitSampleCamera(Vector3(-34.0f, 17.0f, 47.0f), Vector3(0.0f, 0.0f, 0.0f));
	}

#if __BANK
	virtual void AddWidgetsClient()
	{
		bkBank& bank = BANKMGR.CreateBank("DeMesh");
		bank.PushGroup("Debug Draw");
		bank.AddCombo("Draw Mode", (int*)&m_DrawStyle, NUM_DRAW_STYLES, DrawStyleNames);

		bank.PushGroup("Topology mode options");
		bank.AddSlider("Arrow Scale", &deMesh::sm_DebugDrawArrowScale, 0.0f, 100.0f, 0.01f);
		bank.AddSlider("Neighbor Sep", &deMesh::sm_DebugDrawNeighborSeperation, 0.0f, 100.0f, 0.01f);
		bank.AddSlider("Pointer Dist", &deMesh::sm_DebugDrawPointerScale, 0.0f, 100.0f, 0.01f);
		bank.PopGroup();

		bank.AddSlider("Material Depth Bias", &m_DepthBias, -100.0f, 100.0f, 0.01f);
		bank.AddSlider("Wedge Size", &m_WedgeSize, 0.0f, 1.0f, 0.01f);

		bank.PopGroup();

		bank.PushGroup("Mesh operations");
		bank.AddButton("Split Edge", datCallback(MFA(demeshView::SplitEdgeCb), this));
		bank.AddButton("Collapse Edge", datCallback(MFA(demeshView::CollapseEdgeCb), this));
		bank.AddButton("Cut Mesh", datCallback(MFA(demeshView::CutMeshCb), this));
		bank.AddButton("Randomize Mesh Vertices", datCallback(MFA(demeshView::RandomizeVertsCb), this));
		bank.AddSlider("Randomization Amount", &m_RandomAmount, 0.0f, 10.0f, 0.01f);
		bank.AddButton("Save Mesh As...", datCallback(MFA(demeshView::SaveMeshCb), this));

		bank.AddVector("Vertex Pos", &m_PickedVertexPos, bkSlider::GetFloatMinValue(), bkSlider::GetFloatMaxValue(), 0.01f, datCallback(MFA(demeshView::UpdatePickedVertexPos), this));

		bank.PopGroup();

		if (PARAM_simplify.Get())
		{
			bank.PushGroup("Simplification");
			bank.AddButton("Simplify 1", datCallback(MFA1(demeshView::SimplifyCountCb), this, (void*)1));
			bank.AddButton("Simplify 10", datCallback(MFA1(demeshView::SimplifyCountCb), this, (void*)10));
			bank.AddButton("Simplify 100", datCallback(MFA1(demeshView::SimplifyCountCb), this, (void*)100));
			bank.AddButton("Simplify 10%", datCallback(MFA1(demeshView::SimplifyPercentCb), this, (void*)10));
			bank.AddButton("Simplify 50%", datCallback(MFA1(demeshView::SimplifyPercentCb), this, (void*)50));
			bank.AddButton("Simplify 90%", datCallback(MFA1(demeshView::SimplifyPercentCb), this, (void*)90));
		}
	}
#endif

	void UpdatePickedVertexPos()
	{
		if (m_PickedEdge)
		{
			m_PickedMesh->MoveVertex(*m_PickedEdge->GetStart(), m_PickedVertexPos + m_Offset);
		}
	}

	void SimplifyCountCb(int count)
	{
		for(int i = 0; i < count; i++)
		{
			g_Simplify->CollapseMinEdge();
		}
	}

	void SimplifyPercentCb(int percent)
	{
		int numFaces =0;
		for(deMesh::FaceEdgeIterator fi = m_TopoMesh[0]->BeginFaceEdges(); fi != m_TopoMesh[0]->EndFaceEdges(); ++fi)
		{
			numFaces++;
		}

		SimplifyCountCb((int)(numFaces * (0.01f * (float)percent)));
	}

	void SplitEdgeCb()
	{
		if (!m_PickedEdge) return;
		if (!m_PickedMesh) return;
		m_PickedMesh->SplitEdge(m_PickedEdge, 0.5f);
		m_PickedEdge = NULL;
	}

	void CollapseEdgeCb()
	{
		if (!m_PickedEdge) return;
		if (!m_PickedMesh) return;
		if (!m_PickedMesh->CanCollapse(m_PickedEdge))
		{
			return;
		}
		m_PickedMesh->Collapse(m_PickedEdge, 0.5f);
		m_PickedEdge = NULL;
	}

	void CutMeshCb()
	{
		if (!m_PickedEdge) return;
		if (!m_PickedMesh) return;

		Vector3 edge1 = m_PickedEdge->GetStart()->GetPosition() - m_PickedEdge->GetEnd()->GetPosition();
		Vector3 edge2 = m_PickedEdge->GetFaceNormal();

		Vector3 edgePerp;
		edgePerp.Cross(edge1, edge2);

		Vector4 edgePlane;
		edgePlane.ComputePlane(m_PickedEdge->GetStart()->GetPosition(), edgePerp);

		for(int i = 0; i < MAX_MESHES; i++)
		{
			if (m_TopoMesh[i])
			{
				m_TopoMesh[i]->CutWithPlane(edgePlane, true);
				deMesh* discard = m_TopoMesh[i]->Partition();
				delete discard;
			}
		}
	}

	void RandomizeVertsCb()
	{
		RandomizeVerts(0.3f);
	}

	void RandomizeVerts(float amount = 0.3f)
	{
		for(int i = 0; i < MAX_MESHES; i++) {
			if (m_TopoMesh[i]) {
				for(deMesh::VertexIterator vi = m_TopoMesh[i]->BeginVertices(); vi != m_TopoMesh[i]->EndVertices(); ++vi) {
					Vector3 pos = (*vi)->GetPosition();
					pos.x += g_ReplayRand.GetVaried(amount);
					pos.y += g_ReplayRand.GetVaried(amount);
					pos.z += g_ReplayRand.GetVaried(amount);
					(*vi)->SetPosition(pos);
				}
			}
		}

	}

	void SaveMeshCb()
	{
#if __BANK
		if (!m_PickedEdge) return;
		if (!m_PickedMesh) return;
		char saveName[RAGE_MAX_PATH];
		formatf(saveName, "saved.dmsh");
		if ( BANKMGR.OpenFile( saveName, 256, ".dmsh",	true, "Directed Edge Mesh (*.dmsh)") )
		{
			m_PickedMesh->OffsetMesh(-m_Offset);
			m_PickedMesh->Save(saveName);
			m_PickedMesh->OffsetMesh(m_Offset);
		}
#endif
	}

	void LoadErrorFile()
	{
		const char* errorFile = NULL;
		if (PARAM_showerrors.Get(errorFile))
		{
			PARSER.LoadObjectPtr(errorFile, "dmerr", m_ErrorList);
		}
	}

	// Finds all the holes in a mesh (smaller than a specified area)
	// and adds them to the error list.
	void FindHoles()
	{
		float areaThreshold = 40.0f;
		PARAM_findholes.Get(areaThreshold);

		deMesh* mesh = m_TopoMesh[0];
		if (!mesh)
		{
			return;
		}

		if (!m_ErrorList)
		{
			m_ErrorList = rage_new deMeshErrorList;
		}

		int faceCount = 1;

		mesh->MarkVertsUnprocessed();
		for(deMesh::EdgeIterator ei = mesh->BeginEdges(); ei != mesh->EndEdges(); ++ei)
		{
			if (ei->GetStart()->m_Flags.IsSet(deVertex::FLAG_PROCESSED))
			{
				continue;
			}

			ei->GetStart()->m_Flags.Set(deVertex::FLAG_PROCESSED);

			Vector3 startVtx;
			startVtx.Zero();


			if (ei->IsOnBoundary())
			{
				Vector3 averageVtx;
				averageVtx.Zero();
				float vtxCount = 0.0f;

				startVtx = ei->GetStart()->GetPosition();
				float trifanArea = 0.0f;

				// walk along boundary, adding positions
				deEdge* e = ei->GetNextAlongBoundary();
				while(e && e != &(*ei))
				{
					Vector3 cross;
					cross.Cross(
						e->GetStart()->GetPosition() - startVtx,
						e->GetEnd()->GetPosition() - startVtx
						);

					float faceArea = cross.Mag() * 0.5f;
					trifanArea += faceArea;

					averageVtx += e->GetStart()->GetPosition();
					vtxCount += 1.0f;

					e->GetStart()->m_Flags.Set(deVertex::FLAG_PROCESSED);
					e = e->GetNextAlongBoundary();
				}

				averageVtx.InvScale(vtxCount);

				if (trifanArea < areaThreshold)
				{
					char buf[256];
					formatf(buf, "%d: Hole is %fm^2\n%.3f, %.3f, %.3f", faceCount, trifanArea,
						averageVtx.x, averageVtx.y, averageVtx.z);
					deMeshErrorPoint* faceCenterErr = rage_new deMeshErrorPoint(mesh, buf, averageVtx);

					m_ErrorList->m_Errors.PushAndGrow(faceCenterErr);

					// walk along boundary, adding error messages

					deMeshErrorEdge* err = rage_new deMeshErrorEdge(mesh, "", &*ei);
					m_ErrorList->m_Errors.PushAndGrow(err);

					deEdge* e = ei->GetNextAlongBoundary();
					while(e && e != &(*ei))
					{
						err = rage_new deMeshErrorEdge(mesh, "", e);
						m_ErrorList->m_Errors.PushAndGrow(err);
						e = e->GetNextAlongBoundary();
					}

					faceCount++;
				}
			}
		}
	}

	void LoadInputTerrainMesh()
	{
//		deMesh::OpenErrorStream("C:\\demesh_errors");

		PF_START(LoadTimer);

		const char* tex = NULL;
		PARAM_tex.Get(tex);

		if (tex) {
			m_DiffuseTexture = grcTextureFactory::GetInstance().Create(tex);
		}
		else {
			m_DiffuseTexture = grcTextureFactory::GetInstance().Create("none");
		}

		float allMinX=0.0f;
		float allMaxX=0.0f;
		float allMinZ=0.0f;
		float allMaxZ=0.0f;

		for(int i = 1; i < sysParam::GetArgCount(); i++) {
			if (sysParam::GetArg(i)[0] == '-') {
				break;
			}

			const char* fileName = sysParam::GetArg(i);

			if (m_TopoMesh[i-1]) {
				delete m_TopoMesh[i-1];
			}
			m_TopoMesh[i-1] = deMesh::CreateFromFile(fileName);

			if (!PARAM_nocenter.Get()) {
				float minX, maxX, minZ, maxZ;
				m_TopoMesh[i-1]->FindApproxBounds(minX, maxX, minZ, maxZ);
				if (i == 1) {
					allMinX = minX;
					allMaxX = maxX;
					allMinZ = minZ;
					allMaxZ = maxZ;
				}
				else {
					allMinX = Min(allMinX, minX);
					allMaxX = Max(allMaxX, maxX);
					allMinZ = Min(allMinZ, minZ);
					allMaxZ = Max(allMaxZ, maxZ);
				}
			}
		}

		if (PARAM_findholes.Get() && m_TopoMesh[0])
		{
			// Merge all of the meshes into mesh 0
			for(int i = 1; i < MAX_MESHES; i++)
			{
				if (m_TopoMesh[i])
				{
					float minX, maxX, minY, maxY;
					m_TopoMesh[i]->FindApproxBounds(minX, maxX, minY, maxY);
					m_TopoMesh[0]->ExpandGrid(minX, maxX, minY, maxY, 100, 100);
					m_TopoMesh[0]->Stitch(*m_TopoMesh[i]);
					delete m_TopoMesh[i];
					m_TopoMesh[i] = NULL;
				}
			}

			// Unsharpen (for less distraction)
			for(deMesh::VertexIterator vi = m_TopoMesh[0]->BeginVertices(); vi != m_TopoMesh[0]->EndVertices(); ++vi)
			{
				(*vi)->Unsharpen();
			}

			FindHoles();

			m_DrawStyle = BOUNDARIES_PLUS_WIREFRAME;
		}


		if (!PARAM_nocenter.Get()) {
			Vector3 offsetVec(-0.5f * (allMaxX + allMinX), -0.5f * (allMaxZ + allMinZ), 0.0f);
			m_Offset = offsetVec;

			for(int i = 0; i < MAX_MESHES; i++) {
				if (m_TopoMesh[i]) {
					m_TopoMesh[i]->OffsetMesh(offsetVec);
				}
			}
		}

		PF_STOP(LoadTimer);
#if __STATS
		Printf("Loaded in %f ms\n", PF_READ_TIME_MS(LoadTimer));
#endif

		if (m_TopoMesh[0] && PARAM_simplify.Get())
		{
			g_Simplify = rage_new deSimplify();
			g_Simplify->Init(m_TopoMesh[0]);
		}

//		deMesh::CloseErrorStream();
	}

	void InitClient()
	{
		Printf("----------------------------------------------------\n");
		Printf("Keyboard commands:\n");
		Printf("W - Draw Wireframe\n");
		Printf("F - Draw solid faces\n");
		Printf("A - Draw half edges\n");
		Printf("\n");
		Printf("S - Draw smooth faces\n");
		Printf("N - Draw smooth, show normals as color\n");
		Printf("E - Draw smooth, show tex coords as color\n");
		Printf("\n");
		Printf("B - Draw boundary and sharp edges\n");
		Printf("H - Draw wireframe with boundary and sharp edges\n");
		Printf("T - Draw Topology (slow)\n");
		Printf("M - Draw materials\n");
		Printf("D - Draw wedges\n");
		Printf("\n");
		Printf("R - Jitter vertices\n");
		Printf("\n");
		Printf("1-9 - Draw only selected mesh\n");
		Printf("----------------------------------------------------\n");


		m_Mapper.Reset();
		m_Mapper.Map(IOMS_KEYBOARD, KEY_W, m_DrawWireframe);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_T, m_DrawTopology);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_F, m_DrawSolid);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_B, m_DrawSharps);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_S, m_DrawSmooth);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_N, m_DrawNormals);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_A, m_DrawStars);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_H, m_DrawSharpsAndWireframe);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_E, m_DrawTexCoords);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_M, m_DrawMaterials);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_D, m_DrawWedge);

		m_Mapper.Map(IOMS_KEYBOARD, KEY_R, m_RandomizeVerts);

		m_Mapper.Map(IOMS_KEYBOARD, KEY_1, m_DrawMesh[0]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_2, m_DrawMesh[1]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_3, m_DrawMesh[2]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_4, m_DrawMesh[3]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_5, m_DrawMesh[4]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_6, m_DrawMesh[5]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_7, m_DrawMesh[6]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_8, m_DrawMesh[7]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_9, m_DrawMesh[8]);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_0, m_DrawMesh[9]);

		m_Mapper.Map(IOMS_MOUSE_BUTTON, ioMouse::MOUSE_LEFT, m_MousePick);

		m_Mapper.Map(IOMS_KEYBOARD, KEY_LEFT, m_NextEdgeCCW);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_RIGHT, m_NextEdgeCW);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_UP, m_NextEdge);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_DOWN, m_NeighborEdge);

		m_Mapper.Map(IOMS_KEYBOARD, KEY_NUMPAD1, m_Simplify1);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_NUMPAD2, m_Simplify10);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_NUMPAD3, m_Simplify100);

		m_Mapper.Map(IOMS_KEYBOARD, KEY_NUMPAD4, m_AnimateSimplify1);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_NUMPAD5, m_AnimateSimplify10);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_NUMPAD6, m_AnimateSimplify100);

		m_Mapper.Map(IOMS_KEYBOARD, KEY_RETURN, m_Frame);

		float snap;
		if (PARAM_snap.Get(snap)) {
			deWedge::SetVertSnapDist(snap);
		}

		LoadInputTerrainMesh();

		LoadErrorFile();
	}

	void UpdateClient()
	{
		m_Mapper.Update();

		if (m_PickedEdge)
		{
			deEdge* newEdge = m_PickedEdge;
			if (m_NextEdgeCCW.IsPressed())
			{
				newEdge = m_PickedEdge->GetNextCCW();
			}
			if (m_NextEdgeCW.IsPressed())
			{
				newEdge = m_PickedEdge->GetNextCW();
			}
			if (m_NextEdge.IsPressed())
			{
				newEdge = m_PickedEdge->GetNext();
			}
			if (m_NeighborEdge.IsPressed())
			{
				newEdge = m_PickedEdge->GetNeighbor();
			}
			if (newEdge)
			{
				m_PickedEdge = newEdge;
				m_PickedVertexPos = m_PickedEdge->GetStart()->GetPosition() - m_Offset;
			}
		}

		if (m_DrawWireframe.IsPressed()) {
			m_DrawStyle = WIREFRAME;	
		}
		if (m_DrawTopology.IsPressed()) {
			m_DrawStyle = TOPOLOGY;
		}
		if (m_DrawSolid.IsPressed()) {
			m_DrawStyle = SOLID;
		}
		if (m_DrawSharps.IsPressed()) {
			m_DrawStyle = SHARPS;
		}
		if (m_DrawSmooth.IsPressed()) {
			m_DrawStyle = SMOOTH;
		}
		if (m_DrawNormals.IsPressed()) {
			m_DrawStyle = NORMALS;
		}
		if (m_DrawStars.IsPressed()) {
			m_DrawStyle = STARS;
		}
		if (m_DrawSharpsAndWireframe.IsPressed()) {
			m_DrawStyle = SHARPS_PLUS_WIREFRAME;
		}
		if (m_DrawTexCoords.IsPressed()) {
			m_DrawStyle = TEX_COORDS;
		}
		if (m_DrawMaterials.IsPressed()) {
			m_DrawStyle = MATERIALS;
		}
		if (m_DrawWedge.IsPressed()) {
			m_DrawStyle = WEDGE;
		}

		m_DrawSingleMesh = false;
		for(int i = 0; i < MAX_MESHES; i++) {
			if (m_DrawMesh[i].IsDown()) {
				m_DrawSingleMesh = true;
			}
		}

		if (m_RandomizeVerts.IsPressed()) {
			RandomizeVerts(m_RandomAmount);
		}

		if (g_Simplify)
		{

			int animTarget = -1;
			if (m_AnimateSimplify1.IsPressed())
			{
				animTarget = 1;
			}
			if (m_AnimateSimplify10.IsPressed())
			{
				animTarget = 10;
			}
			if (m_AnimateSimplify100.IsPressed())
			{
				animTarget = 100;
			}

			if (m_SimplificationPerFrame == animTarget)
			{
				m_SimplificationPerFrame = 0;
			}
			else if (animTarget > 0)
			{
				m_SimplificationPerFrame = animTarget;
			}


			int numToSimplify = m_SimplificationPerFrame;
			if (m_Simplify1.IsPressed())
			{
				numToSimplify += 1;
			}
			if (m_Simplify10.IsPressed())
			{
				numToSimplify += 10;
			}
			if (m_Simplify100.IsPressed())
			{
				numToSimplify += 100;
			}
			for(int i =0; i < numToSimplify; i++)
			{
				g_Simplify->CollapseMinEdge();
				m_PickedEdge = NULL;
			}
		}

		if (m_TopoMesh[0] && m_Frame.IsPressed())
		{
			Vector3 bMin, bMax;
			m_TopoMesh[0]->FindBounds(bMin, bMax);
			Vector3 center = 0.5f * (bMin + bMax);
			float radius = rage::Max(center.x - bMin.x, center.y - bMin.y);
			radius = rage::Max(radius, center.z - bMin.z);
			spdSphere bounds(RCC_VEC3V(center), ScalarV(radius));
			GetCamMgr().Frame(bounds);
		}
	}

	void DrawClient()
	{
		// Draw the mesh
		grcStateBlock::SetBlendState(grcStateBlock::BS_Default);
		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_Default);
		grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);
		grcLightState::SetEnabled(false);

		grcColor3f(1.0f, 1.0f, 1.0f);

		if (m_MousePick.IsPressed())
		{
			PickFace();
		}

		DrawMeshes();

		grcLightState::SetEnabled(false);

		DrawErrors();
		DrawPick();

		DrawSimplify();
	}

	void DrawMeshes()
	{
		for(int i = 0; i < MAX_MESHES; i++) 
		{
			grcStateBlock::SetBlendState(grcStateBlock::BS_Default);
			grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_Default);
			grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);

			if (m_TopoMesh[i] && (!m_DrawSingleMesh || m_DrawMesh[i].IsDown())) {
				switch (m_DrawStyle) {
				case WIREFRAME:
					m_TopoMesh[i]->DrawFast();
					break;
				case TOPOLOGY:
					m_TopoMesh[i]->DebugDraw();
					break;
				case SHARPS:
					m_TopoMesh[i]->DrawSharps();
					break;
				case SOLID:
					m_TopoMesh[i]->DrawSolid();
					break;
				case STARS:
					m_TopoMesh[i]->DrawStars();
					break;
				case SMOOTH:
					grcBindTexture( m_DiffuseTexture);
					m_TopoMesh[i]->DrawSmooth();
					break;
				case NORMALS:
					m_TopoMesh[i]->DrawNormals();
					break;
				case TEX_COORDS:
					m_TopoMesh[i]->DrawTexCoords();
					break;
				case SHARPS_PLUS_WIREFRAME:
					grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_TestOnly);
					grcColor4f(0.5f, 0.5f, 0.5f, 0.5f);
					m_TopoMesh[i]->DrawFast();
					m_TopoMesh[i]->DrawSharps();
					break;
				case BOUNDARIES_PLUS_WIREFRAME:
					grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_TestOnly);
					grcColor4f(0.5f, 0.5f, 0.5f, 0.5f);
					m_TopoMesh[i]->DrawFast();
					m_TopoMesh[i]->DrawSharps(true, false);
					break;
				case MATERIALS:
				case WEDGE:
					DrawMaterials(i);

					grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_TestOnly_LessEqual);
					//grcState::SetDepthBias(m_DepthBias * 0.00001f);

					if (m_DrawStyle == WEDGE)
					{
						DrawWedges(i);
					}

					grcColor4f(0.0f, 0.0f, 0.0f, 0.5f);
					m_TopoMesh[i]->DrawStars();
					m_TopoMesh[i]->DrawSharps();
					break;
				default:
					break;
				}
			}
		}
	}

	void DrawPick()
	{

		if (m_PickedEdge && m_PickedMesh)
		{
			grcStateBlock::SetBlendState(grcStateBlock::BS_Default);
			grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_Default);
			grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);
			grcLightState::SetEnabled(false);

			// Draw the face triangle (inset and transparent)
			Vector3 v1, v2, v3;
			Vector3 inV1, inV2, inV3;
			m_PickedEdge->GetFaceVertPositions(v1, v2, v3);


			Vector3 avg = v1;
			avg += v2;
			avg += v3;
			avg.Scale(1.0f / 3.0f);
			inV1.Lerp(0.25f, v1, avg);
			inV2.Lerp(0.25f, v2, avg);
			inV3.Lerp(0.25f, v3, avg);

			grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_Default);
			//grcState::SetDepthBias(m_DepthBias * 0.00001f * 2.0f);
			grcColor4f(1.0f, 1.0f, 0.0f, 0.5f);
			grcBegin(drawTris, 6); // Draw face twice (front facing and back facing)
			grcVertex3f(inV1);
			grcVertex3f(inV2);
			grcVertex3f(inV3);

			grcVertex3f(inV1);
			grcVertex3f(inV3);
			grcVertex3f(inV2);
			grcEnd();

			grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);
			grcBegin(drawLineStrip, 4);
			grcColor4f(0.8f, 0.8f, 0.0f, 1.0f);
			grcVertex3f(v1);
			grcVertex3f(v2);
			grcVertex3f(v3);
			grcVertex3f(v1);
			grcEnd();

			m_PickedMesh->DebugDrawEdge(m_PickedEdge, Color_OrangeRed);	

			grcStateBlock::SetBlendState(grcStateBlock::BS_Default);
			grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_Default);
			grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);
			grcLightState::SetEnabled(false);

			Vector3 nrm = m_PickedEdge->GetStartWedge().GetVertexData().Nrm;
			Vector4 cpv = m_PickedEdge->GetStartWedge().GetVertexData().Cpv;

			char addtlInfo[256];
			formatf(addtlInfo, 
				"Wedge Id: %d\n"
				"Wedge Mtl: %d\n"
				"Mtl Name: %s\n"
				"Nrm0: %.3f, %.3f, %.3f\n"
				"CPV0: %.3f, %.3f, %.3f, %.3f\n"
				"Error: %.3f\n",
				m_PickedEdge->GetStartWedge().GetIndex(),
				m_PickedEdge->GetStartWedge().GetMaterial(),
				m_PickedMesh->m_Materials[m_PickedEdge->GetStartWedge().GetMaterial()].m_MtlFileName.c_str(),
				nrm.x, nrm.y, nrm.z,
				cpv.x, cpv.y, cpv.z, cpv.w,
				m_PickedEdge->GetPrimaryEdge()->m_Error);

			DrawCoord(m_PickedEdge->GetStart()->GetPosition(), addtlInfo);
			DrawCoord(m_PickedEdge->GetEnd()->GetPosition());

		}
	}

	void DrawCoord(const Vector3& pos, const char* addtlInfo = "")
	{
		Vector3 cameraLoc = VEC3V_TO_VECTOR3(grcViewport::GetCurrent()->GetCameraPosition());
		Color32 transparent(255,255,255,0);

		Vector3 realPos = pos - m_Offset;
		char buf[256];
		formatf(buf, "(%.3f, %.3f, %.3f)\n%s", realPos.x, realPos.y, realPos.z, addtlInfo);

		Color32 labelColor = Lerp(ClampRange(cameraLoc.Dist(pos), 4.0f, 30.0f), Color_white, transparent);
		grcColor(labelColor);
		grcDrawLabel(pos, buf);
	}

	void DrawErrors()
	{
		if (!m_ErrorList) 
		{
			return;
		}

		Vector3 cameraLoc = VEC3V_TO_VECTOR3(grcViewport::GetCurrent()->GetCameraPosition());
		Color32 transparent(255,255,255,0);

		// Draw the error indicators first
		for(int i = 0; i < m_ErrorList->m_Errors.GetCount(); i++)
		{
			switch(m_ErrorList->m_Errors[i]->GetType())
			{
			case deMeshError::GENERAL:
				// draw soemthing to the hud?
				break;
			case deMeshError::POINT:
				{
					deMeshErrorPoint* errorPoint = smart_cast<deMeshErrorPoint*>(m_ErrorList->m_Errors[i]);

					Vector3 loc = errorPoint->m_Point;
					loc += m_Offset;

					Matrix34 mtx;
					mtx.Identity();
					mtx.d = loc;

					Vector3 errorBoxSize(0.2f, 0.2f, 0.2f);

					grcDrawSolidBox(errorBoxSize, mtx, Color_red);
					grcViewport::SetCurrentWorldIdentity();
				}
				break;
			case deMeshError::EDGE:
				{
					deMeshErrorEdge* errorEdge = smart_cast<deMeshErrorEdge*>(m_ErrorList->m_Errors[i]);

					grcDrawLine(errorEdge->m_EdgeBegin + m_Offset, errorEdge->m_EdgeEnd + m_Offset, Color_red);
				}
				break;
			case deMeshError::FACE:
				{
					deMeshErrorFace* errorFace = smart_cast <deMeshErrorFace*>(m_ErrorList->m_Errors[i]);


					Color32 red(Color_red);
					red.SetAlpha(128);
					grcColor(red);
					grcBegin(drawTris, 6);
					grcVertex3f(errorFace->m_PointA + m_Offset);
					grcVertex3f(errorFace->m_PointB + m_Offset);
					grcVertex3f(errorFace->m_PointC + m_Offset);

					grcVertex3f(errorFace->m_PointA + m_Offset);
					grcVertex3f(errorFace->m_PointC + m_Offset);
					grcVertex3f(errorFace->m_PointB + m_Offset);

					grcEnd();


				}
				break;
			}
		}

		// Then draw the labels
		for(int i = 0; i < m_ErrorList->m_Errors.GetCount(); i++)
		{
			Vector3 loc;

			switch(m_ErrorList->m_Errors[i]->GetType())
			{
			case deMeshError::GENERAL:
				// draw soemthing to the hud?
				break;
			case deMeshError::POINT:
				{
					deMeshErrorPoint* errorPoint = smart_cast<deMeshErrorPoint*>(m_ErrorList->m_Errors[i]);

					loc = errorPoint->m_Point;
				}
				break;
			case deMeshError::EDGE:
				{
					deMeshErrorEdge* errorEdge = smart_cast<deMeshErrorEdge*>(m_ErrorList->m_Errors[i]);

					loc = errorEdge->m_EdgeBegin + errorEdge->m_EdgeEnd;
					loc.Scale(0.5f);
				}
				break;
			case deMeshError::FACE:
				{
					deMeshErrorFace* errorFace = smart_cast <deMeshErrorFace*>(m_ErrorList->m_Errors[i]);
					loc = (errorFace->m_PointA + errorFace->m_PointB + errorFace->m_PointC);
					loc.Scale(1.0f / 3.0f);
				}
				break;
			}

			loc += m_Offset;

			Color32 labelColor = Lerp(ClampRange(cameraLoc.Dist(loc), 4.0f, 30.0f), Color_white, transparent);
			grcColor(labelColor);
			grcDrawLabel(loc, m_ErrorList->m_Errors[i]->m_Description);
		}

	}

	void DrawSimplify()
	{
		if (g_Simplify)
		{
			g_Simplify->DrawNextCollapse();
		}
	}

	void PickFace()
	{
		deEdge* minEdge = NULL;
		deMesh* minMesh = NULL;
		float minT = FLT_MAX;

		Vector3 origin, destination, direction;
		grcViewport::GetCurrent()->ReverseTransform((float)ioMouse::GetX(), (float)ioMouse::GetY(), (Vec3V&)origin, (Vec3V&)destination);
		direction = destination - origin;

		for(int i = 0; i < MAX_MESHES; i++) 
		{
			if (m_TopoMesh[i] && (!m_DrawSingleMesh || m_DrawMesh[i].IsDown())) // only pick visible meshes
			{ 
				float t;
				deEdge* edge = m_TopoMesh[i]->PickNearest(origin, direction, &t);
				if (edge && t < minT)
				{
					minT = t;
					minEdge = edge;
					minMesh = m_TopoMesh[i];
				}
			}
		}
		m_PickedEdge = minEdge;
		m_PickedMesh = minMesh;
		if (m_PickedEdge)
		{
			m_PickedVertexPos = m_PickedEdge->GetStart()->GetPosition() - m_Offset;
		}
		else
		{
			m_PickedVertexPos.Zero();
		}
	}

	void DrawWedges( int i )
	{
		m_TopoMesh[i]->GatherDrawData();
		if (m_TopoMesh[i]->m_DrawData.m_TotalEdges == 0)
		{
			return;
		}

		const grcDrawMode drawMode = drawTris;
		const int vertsPerLoop = 3;// 3 verts per face
		int numLoops = m_TopoMesh[i]->m_DrawData.m_TotalEdges;// 1 wedge per edge, 3 verts per wedge

		int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
		int vertsRemaining = numLoops * vertsPerLoop; 
		int vertsRemainingInBatch = Min(batchSize, vertsRemaining);

		grcBegin(drawMode, vertsRemainingInBatch);

		for(deMesh::EdgeIterator edge = m_TopoMesh[i]->BeginEdges(); edge != m_TopoMesh[i]->EndEdges(); ++edge)
		{
			if (Unlikely(vertsRemainingInBatch == 0))
			{
				vertsRemaining -= batchSize;
				vertsRemainingInBatch = Min(batchSize, vertsRemaining);
				if (vertsRemainingInBatch > 0)
				{
					grcEnd();
					grcBegin(drawMode, vertsRemainingInBatch);
				}
			}
			vertsRemainingInBatch -= vertsPerLoop;

			int wedgeID = edge->m_Wedge;
			Vector3 verts[3];
			verts[0] = edge->GetStart()->GetPosition();
			verts[1] = edge->GetEnd()->GetPosition();
			verts[2] = edge->GetPrev()->GetStart()->GetPosition();

			verts[1].Lerp((1.0f - m_WedgeSize), verts[0]);
			verts[2].Lerp((1.0f - m_WedgeSize), verts[0]);

			grcNormal3f(0.0f, 1.0f, 0.0f);
			Color32 color = GetMaterialColor(wedgeID + 5);
			color.SetRed(255 - color.GetRed());
			grcColor(color);
			grcVertex3f(verts[0]);
			grcVertex3f(verts[1]);
			grcVertex3f(verts[2]);
		}
		grcEnd();
	}
	void DrawMaterials( int i )
	{
		m_TopoMesh[i]->GatherDrawData();
		if (m_TopoMesh[i]->m_DrawData.m_TotalFaces == 0)
		{
			return;
		}

		const grcDrawMode drawMode = drawTris;
		const int vertsPerLoop = 3;// 3 verts per face
		int numLoops = m_TopoMesh[i]->m_DrawData.m_TotalFaces;

		int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
		int vertsRemaining = numLoops * vertsPerLoop; 
		int vertsRemainingInBatch = Min(batchSize, vertsRemaining);

		grcBegin(drawMode, vertsRemainingInBatch);

		for(deMesh::FaceEdgeIterator face = m_TopoMesh[i]->BeginFaceEdges(); face != m_TopoMesh[i]->EndFaceEdges(); ++face)
		{
			if (Unlikely(vertsRemainingInBatch == 0))
			{
				vertsRemaining -= batchSize;
				vertsRemainingInBatch = Min(batchSize, vertsRemaining);
				if (vertsRemainingInBatch > 0)
				{
					grcEnd();
					grcBegin(drawMode, vertsRemainingInBatch);
				}
			}
			vertsRemainingInBatch -= vertsPerLoop;

			Vector3 verts[3];
			face->GetFaceVertPositions(verts[0], verts[1], verts[2]);
			int materials[3];
			materials[0] = face->GetStartWedge().GetMaterial();
			materials[1] = face->GetNext()->GetStartWedge().GetMaterial();
			materials[2] = face->GetPrev()->GetStartWedge().GetMaterial();
			grcNormal3f(face->GetFaceNormal());
			grcColor(GetMaterialColor(materials[0]));
			grcVertex3f(verts[0]);
			grcColor(GetMaterialColor(materials[1]));
			grcVertex3f(verts[1]);
			grcColor(GetMaterialColor(materials[2])); 
			grcVertex3f(verts[2]);
		}
		grcEnd();
	}
	void Shutdown()
	{
		m_DiffuseTexture->Release();

		for(int i = 0; i < MAX_MESHES; i++) {
			delete m_TopoMesh[i];
		}

		grcSampleManager::Shutdown();
	}

private:
	enum {
		MAX_MESHES = 80
	};

	deMesh*		m_TopoMesh[MAX_MESHES];

	deMesh::DrawData* m_TopoMeshDraw[MAX_MESHES];

	ioMapper	m_Mapper;
	ioValue		m_DrawWireframe;
	ioValue		m_DrawTopology;
	ioValue		m_DrawSolid;
	ioValue		m_DrawSharps;
	ioValue		m_DrawSmooth;
	ioValue		m_DrawNormals;
	ioValue		m_DrawStars;
	ioValue		m_DrawSharpsAndWireframe;
	ioValue		m_DrawTexCoords;
	ioValue		m_DrawMaterials;
	ioValue		m_DrawWedge;

	ioValue		m_RandomizeVerts;

	ioValue		m_DrawMesh[MAX_MESHES];

	ioValue		m_MousePick;

	ioValue		m_NextEdgeCW;
	ioValue		m_NextEdgeCCW;
	ioValue		m_NeighborEdge;
	ioValue		m_NextEdge;
	ioValue		m_PrevEdge;

	ioValue		m_Simplify1;
	ioValue		m_Simplify10;
	ioValue		m_Simplify100;
	ioValue		m_AnimateSimplify1;
	ioValue		m_AnimateSimplify10;
	ioValue		m_AnimateSimplify100;

	ioValue		m_Frame;

	bool		m_DrawSingleMesh;
	
	int			m_SimplificationPerFrame;

	grcTexture* m_DiffuseTexture;

	deEdge*		m_PickedEdge;
	deMesh*		m_PickedMesh;

	Vector3		m_Offset;

	deMeshErrorList* m_ErrorList;


	DrawStyle	m_DrawStyle;

	float		m_DepthBias;
	float		m_WedgeSize;
	float		m_RandomAmount;

	Vector3		m_PickedVertexPos;
};

int Main()
{
	INIT_PARSER;

	InitMaterialColors();

	demeshView de;

#if __WIN32PC
	char cwd[256];
	_getcwd(cwd, 256);

	de.Init(cwd);
#else
	de.Init();
#endif

	de.UpdateLoop();

	de.Shutdown();

	SHUTDOWN_PARSER;

	return 0;
}
