// 
// demeshtools/demesh_merge.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define SIMPLE_HEAP_SIZE (1024 * 1024)

#include "demesh/demesh.h"
#include "demesh/devertex.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"

using namespace rage;

REQ_PARAM(out, "Name of the output file");
FPARAM(1, in, "Input files (one or more). Must come before any other options");
PARAM(markfixed, "Mark the verts along the joined edges as fixed");
PARAM(snap, "Snap distance for vertices");
PARAM(polyLimit, "If the merge results in a mesh with more than this limit an error will be printed.");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");
PARAM(stitch, "Use Stitch() instead of Combine()");
PARAM(unsharpen, "Unsharpen as many verts as possible");
PARAM(fillholes, "Fills mesh holes");

int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}

	deMesh* mesh = NULL;

	bool markFixed = PARAM_markfixed.Get();

	float snap;
	if (PARAM_snap.Get(snap)) {
		deWedge::SetVertSnapDist(snap);
	}


	for(int i = 1; i < sysParam::GetArgCount(); i++) {
		const char* arg = sysParam::GetArg(i);
		if ((arg[0] == '-')) {
			break;
		}

		if (!mesh) {
			mesh = deMesh::CreateFromFile(arg);
			if (markFixed) {
				mesh->MarkVertsUnprocessed();
			}
		}
		else {
			sysTimer loadTimer;
			deMesh* other = deMesh::CreateFromFile(arg);
			Displayf("Time to load mesh %s = %fms", arg, loadTimer.GetMsTime());	
			Assert(other);
			sysTimer stitchTimer;
//			mesh->Stitch(*other);
			float minX, maxX, minY, maxY;
			other->FindApproxBounds(minX, maxX, minY, maxY);
			mesh->ExpandGrid(minX, maxX, minY, maxY, 100, 100);
			if (PARAM_stitch.Get())
			{
				mesh->Stitch(*other);
			}
			else
			{
				mesh->Combine(*other);
			}
			Displayf("Time to stitch mesh %s = %fms", arg, stitchTimer.GetMsTime());	
			delete other;
		}
	}

	if (!mesh)
	{
		demErrorf("No input files to merge");
	}

	if (markFixed) {
		mesh->MarkProcessedVerts(deVertex::FLAG_FIXED_POSITION);
	}

	if (PARAM_fillholes.Get())
	{
		float params[2];
		params[0] = 0.1f;
		params[1] = 0.1f;
		PARAM_fillholes.GetArray(params, 2);

		sysTimer timer;
		int numStitched = mesh->RemoveHoles(params[0], params[1]);
		Displayf("Stitched %d edges to fill holes in %fms", numStitched, timer.GetMsTime());
	}

	if (PARAM_unsharpen.Get())
	{
		sysTimer timer;
		int unsharpenedVerts = 0;
		for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi)
		{
			bool didSomething = (*vi)->Unsharpen();
			if (didSomething)
			{
				unsharpenedVerts++;
			}
		}
		Displayf("Unsharpened %d vertices in %fms", unsharpenedVerts, timer.GetMsTime());
	}

	int polyLimit = 0;
	if (PARAM_polyLimit.Get(polyLimit))
	{
		int numPolys = 0;
		// count total polys
		for(deMesh::FaceEdgeIterator fi = mesh->BeginFaceEdges(); fi != mesh->EndFaceEdges(); ++fi)
		{
			numPolys++;
		}

		if (polyLimit < numPolys)
		{
			Errorf("Merging the input files resulted in more than the command line specified polyLimit [%d], The merged mesh has [%d] polygons.", polyLimit, numPolys);
		}
	}

	const char* outName = NULL;
	PARAM_out.Get(outName);
	mesh->Save(outName);

	delete mesh;

	deMesh::CloseErrorStream();

	SHUTDOWN_PARSER;

	return 0;
}
