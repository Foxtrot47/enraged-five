// 
// demeshtools/demesh_find_holes.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define SIMPLE_HEAP_SIZE (760 * 1024)

#include "demesh/demesh.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"


using namespace rage;

FPARAM(1, in, "Name of input mesh");
PARAM(max, "Max number of verts in a hole");
PARAM(normals, "Check to see if a vert has two normals and all other attributes are the same");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");

int Main()
{
	INIT_PARSER;
	
	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}


	const char* inFile;
	PARAM_in.Get(inFile);

	int maxVerts = INT_MAX;
	PARAM_max.Get(maxVerts);

	deMesh* mesh = deMesh::CreateFromFile(inFile);

	if (!mesh)
	{
		SHUTDOWN_PARSER;
		return 1;
	}

	mesh->MarkVertsUnprocessed();

	for(deMesh::EdgeIterator ei = mesh->BeginEdges(); ei != mesh->EndEdges(); ++ei)
	{
		if (ei->GetStart()->m_Flags.IsSet(deVertex::FLAG_PROCESSED))
		{
			continue;
		}

		ei->GetStart()->m_Flags.Set(deVertex::FLAG_PROCESSED);

		if (ei->IsOnBoundary())
		{
			Vector3 avg;
			avg = ei->GetStart()->GetPosition();
			int numVerts = 1;

			// walk along boundary, adding positions
			deEdge* e = ei->GetNextAlongBoundary();
			while(e && e != &(*ei))
			{
				avg.Add(e->GetStart()->GetPosition());
				numVerts++;
				e->GetStart()->m_Flags.Set(deVertex::FLAG_PROCESSED);

				e = e->GetNextAlongBoundary();
			}

			avg.Scale(1.0f / (float)numVerts);

			if (numVerts < maxVerts)
			{
				Errorf("In file %s: Loop with %d verts at (%f, %f, %f). This may be a T intersection or an unintentional hole in the mesh", inFile, numVerts, avg.x, avg.y, avg.z);
				char errorMsg[128];
				formatf(errorMsg, "Loop with %d verts starts here. This may be a T intersection or an unintentional hole in the mesh", numVerts);
				deMeshErrorEdge error(mesh, errorMsg, e);
				deMesh::ReportError(error);
			}
		}

	}

	// Check for normal discontinuities too.
	// If a vert has 2 wedges, see if wedge A == wedge B after setting the normals on the two wedges to be the same.
	// If so, report the error.
	
	if (PARAM_normals.Get())
	{
		for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi)
		{
			if ((*vi)->GetNumWedges() == 2)
			{
				deWedge& wa = (*vi)->GetWedge(0);
				deWedge& wb = (*vi)->GetWedge(1);

				if (wa.GetVertexData().TanBiCount != wa.GetVertexData().TanBiCount)
				{
					continue;
				}

				Vector3 oldNormal = wa.GetVertexData().Nrm;
				wa.GetVertexData().Nrm = wb.GetVertexData().Nrm;

				Vector3 oldTangents[mshMaxTanBiSets];
				Vector3 oldBinormals[mshMaxTanBiSets];

				for(int i = 0; i < wa.GetVertexData().TanBiCount; i++)
				{
					oldTangents[i] = wa.GetVertexData().TanBi[i].T;
					wa.GetVertexData().TanBi[i].T = wb.GetVertexData().TanBi[i].T;

					oldBinormals[i] = wa.GetVertexData().TanBi[i].B;
					wa.GetVertexData().TanBi[i].B = wb.GetVertexData().TanBi[i].B;
				}

				if (wa == wb)
				{
					// verts differ by normal (and tangent/binormals) only
					Vector3 pos = (*vi)->GetPosition();
					Printf("Normal mismatch: (%f, %f, %f)\n", pos.x, pos.y, pos.z);
					deMeshErrorPoint error(mesh, "Normal mismatch", pos);
					deMesh::ReportError(error);
				}

				wa.GetVertexData().Nrm = oldNormal;

				for(int i = 0; i < wa.GetVertexData().TanBiCount; i++)
				{
					wa.GetVertexData().TanBi[i].T = oldTangents[i];
					wa.GetVertexData().TanBi[i].B = oldBinormals[i];
				}
			}
		}
	}

	delete mesh;

	deMesh::CloseErrorStream();
	
	SHUTDOWN_PARSER;
	return 0;
}
