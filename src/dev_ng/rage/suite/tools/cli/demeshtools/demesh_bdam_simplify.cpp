// 
// demeshtools/demesh_bdam_simplify.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "demesh/demesh.h"
#include "demesh/desimplify.h"
#include "parser/manager.h"
#include "profile/element.h"
#include "profile/group.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PF_GROUP(DirEdge);

PF_TIMER(LoadTimer, DirEdge);
PF_TIMER(HeapTimer, DirEdge);
PF_TIMER(EcolTimer, DirEdge);

PF_TIMER(EcolCollapseTimer, DirEdge);
PF_TIMER(EcolErrorEvalTimer, DirEdge);
PF_TIMER(EcolFoldoverTimer, DirEdge);
PF_TIMER(EcolTopoCheckTimer, DirEdge);
PF_TIMER(EcolHeapOpsTimer, DirEdge);

FPARAM(1, in, "List of input meshes");
REQ_PARAM(outA, "Name of output mesh A");
REQ_PARAM(outB, "Name of output mesh B");
REQ_PARAM(point1, "One of the four points that make up this cell (x,z coords)");
REQ_PARAM(point2, "One of the four points that make up this cell (x,z coords)");
REQ_PARAM(point3, "One of the four points that make up this cell (x,z coords)");
REQ_PARAM(point4, "One of the four points that make up this cell (x,z coords)");
PARAM(dihed, "Sets the dihedral plane scale. Default = 0.01. (higher = fewer skinny triangles, but less concentrated simplification)");
PARAM(bound, "Sets the boundary plane scale. Default = 100.0. (higher = less likely to move verts away from a boundary edge)");
PARAM(bounddi, "Sets the dihedral plane scale for boundary verts. Default = 20.0. (higher = less likely to move verts along a boundary edge)");
PARAM(disc, "Sets the discontinuity scale. Default = 1.0 (higher = preserve material boundaries better)");
PARAM(error, "Collapse until the error is >= this value");
PARAM(keeppercent, "Collapse until N% of the polys remain");
PARAM(removepolys, "Collapse until we've removed N polys");
PARAM(keeppolys, "Collapse until N polys remain");
PARAM(snap, "Grid snap distance");
PARAM(writetemp, "Write out temp files C:\\Meshes\\marked.dmsh and C:\\Meshes\\simplified.dmsh");
PARAM(lockcpv, "Lock any vertex with the specified CPV (e.g. -lockcpv=0,255,0");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");

// Check for floating point exceptions
#if !__PPU
int _matherr(struct _exception *e) {return MathErr(e);}
#endif

int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}

	deMesh* mesh = NULL;

	float minX, maxX, minZ, maxZ;

	float xz[2];
	Vector3 point1, point2, point3, point4;

	PARAM_point1.GetArray(xz, 2);
	point1.Set(xz[0], 0.0f, xz[1]);
	minX = maxX = xz[0];
	minZ = maxZ = xz[1];

	PARAM_point2.GetArray(xz, 2);
	point2.Set(xz[0], 0.0f, xz[1]);
	minX = Min(minX, xz[0]);
	maxX = Max(maxX, xz[0]);
	minZ = Min(minZ, xz[1]);
	maxZ = Max(maxZ, xz[1]);

	PARAM_point3.GetArray(xz, 2);
	point3.Set(xz[0], 0.0f, xz[1]);
	minX = Min(minX, xz[0]);
	maxX = Max(maxX, xz[0]);
	minZ = Min(minZ, xz[1]);
	maxZ = Max(maxZ, xz[1]);

	PARAM_point4.GetArray(xz, 2);
	point4.Set(xz[0], 0.0f, xz[1]);
	minX = Min(minX, xz[0]);
	maxX = Max(maxX, xz[0]);
	minZ = Min(minZ, xz[1]);
	maxZ = Max(maxZ, xz[1]);

	Printf("X = (%f, %f) Z = (%f, %f)\n", minX, maxX, minZ, maxZ);

	float snap = deWedge::GetVertSnapDist();
	PARAM_snap.Get(snap);
	deWedge::SetVertSnapDist(snap);

	for(int i = 1; i < sysParam::GetArgCount(); i++) {
		const char* arg = sysParam::GetArg(i);
		if ((arg[0] == '-')) {
			break;
		}

		if (!mesh) {
			mesh = deMesh::CreateFromFile(arg);
			mesh->InitGrid(minX - 10.0f, maxX + 10.0f, minZ - 10.0f, maxZ + 10.0f, 100, 100);
		}
		else {
			deMesh* other = deMesh::CreateFromFile(arg);	
			Assert(other);
			mesh->Stitch(*other);
			delete other;
		}
	}

	mesh->DestroyGrid();

	// Mark the mesh edges and diagonals

	Vector4 plane;

	// Diag from 1 to 4 gets vert attrs cleared
	plane.ComputePlane(point1, point4, point1 + Vector3(0.0f, 1.0f, 0.0f));
	mesh->MarkVertsOnPlane(plane, deVertex::FLAG_NO_COLLAPSE, false);
	mesh->MarkVertsOnPlane(plane, deVertex::FLAG_FIXED_POSITION, false);

	// Diag from 2 to 3 gets FIXED_POSITION
	plane.ComputePlane(point2, point3, point2 + Vector3(0.0f, 1.0f, 0.0f));
	mesh->MarkVertsOnPlane(plane, deVertex::FLAG_NO_COLLAPSE, false);
	mesh->MarkVertsOnPlane(plane, deVertex::FLAG_FIXED_POSITION, true);

	// Border edges get NO_COLLAPSE
	plane.ComputePlane(point1, point2, point1 + Vector3(0.0f, 1.0f, 0.0f));
	mesh->MarkVertsOnPlane(plane, deVertex::FLAG_NO_COLLAPSE, true);

	plane.ComputePlane(point2, point4, point2 + Vector3(0.0f, 1.0f, 0.0f));
	mesh->MarkVertsOnPlane(plane, deVertex::FLAG_NO_COLLAPSE, true);

	plane.ComputePlane(point4, point3, point4 + Vector3(0.0f, 1.0f, 0.0f));
	mesh->MarkVertsOnPlane(plane, deVertex::FLAG_NO_COLLAPSE, true);

	plane.ComputePlane(point3, point1, point3 + Vector3(0.0f, 1.0f, 0.0f));
	mesh->MarkVertsOnPlane(plane, deVertex::FLAG_NO_COLLAPSE, true);

	if (PARAM_lockcpv.Get()) {
		int colorVal[3];
		PARAM_lockcpv.GetArray(colorVal, 3);
		Vector4 color;
		color.x = (1.0f / 255.0f) * colorVal[0];
		color.y = (1.0f / 255.0f) * colorVal[1];
		color.z = (1.0f / 255.0f) * colorVal[2];
		color.w = 1.0f;
		for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi)
		{
			deVertex* v = (*vi);
			for(int i = 0; i < v->GetNumWedges(); i++) {
				deWedge& w = v->GetWedge(i);
				if (w.GetVertexData().Cpv.IsClose(color, 3.0f / 255.0f)) { 
					v->SetCollapseAllowed(false);
				}
			}
		}
	}

	if (PARAM_writetemp.Get()) {
		mesh->Save("C:/Meshes/marked.dmsh");
	}

	Vector3 offsetVec(-0.5f * (maxX - minX) - minX, 0.0f, -0.5f * (maxZ - minZ) - minZ);
	mesh->OffsetMesh(offsetVec);

#if __STATS
	Printf("Loaded in %fms\n", PF_READ_TIME_MS(LoadTimer));
#endif

	PF_START(HeapTimer);
	deSimplify* simplify = new deSimplify;
	simplify->Init(mesh);
	PF_STOP(HeapTimer);

#if __STATS
	Printf("Built heap in %fms\n", PF_READ_TIME_MS(HeapTimer));
#endif

	float data = 0.0f;
	if (PARAM_dihed.Get(data))
	{
		simplify->m_DihedralScale.Setf(data);
	}
	if (PARAM_bound.Get(data))
	{
		simplify->m_BoundaryScale.Setf(data);
	}
	if (PARAM_bounddi.Get(data))
	{
		simplify->m_BoundaryDihedralScale.Setf(data);
	}
	if (PARAM_disc.Get(data))
	{
		simplify->m_DiscontinuityScale.Setf(data);
	}

	int removedPolys = 0;
	int numEcols = 0;

	if (PARAM_error.Get()) {
		Assert(!PARAM_keeppercent.Get());
		Assert(!PARAM_removepolys.Get());
		Assert(!PARAM_keeppolys.Get());

		float maxError = 0.0f;
		PARAM_error.Get(maxError);

		PF_START(EcolTimer);
		while(maxError > simplify->GetNextCollapseError()) {
			int justRemoved = simplify->CollapseMinEdge();
			if (justRemoved < 0) {
				break;
			}
			else {
				removedPolys += justRemoved;
			}
			if (justRemoved > 0) {
				numEcols++;
			}
		}
		PF_STOP(EcolTimer);
	}
	else {
		int numToRemove = 0;

		if (PARAM_removepolys.Get()) {
			Assert(!PARAM_keeppercent.Get());
			Assert(!PARAM_keeppolys.Get());
			PARAM_removepolys.Get(numToRemove);
		}
		else {
			// count total polys
			int numPolys = 0;
			for(deMesh::FaceEdgeIterator fi = mesh->BeginFaceEdges(); fi != mesh->EndFaceEdges(); ++fi)
			{
				numPolys++;
			}

			if (PARAM_keeppercent.Get()) {
				Assert(!PARAM_keeppolys.Get());
				float pct;
				PARAM_keeppercent.Get(pct);
				numToRemove = (int)((1.0f - (pct * 0.01f)) * numPolys);
			}
			else if (PARAM_keeppolys.Get()) {
				int kept;
				PARAM_keeppolys.Get(kept);
				numToRemove = numPolys - kept;
			}
			else {
				Assert("One of -error, -keeppercent, -removepolys, or -keeppolys must be specified");
			}
		}

		PF_START(EcolTimer);
		while(removedPolys < numToRemove) {
			int justRemoved = simplify->CollapseMinEdge();
			if (justRemoved < 0) {
				break;
			}
			else {
				removedPolys += justRemoved;
			}
			if (justRemoved > 0) {
				numEcols++;
			}
		}
		PF_STOP(EcolTimer);
	}

#if __STATS
	Printf("Removed %d polys, %d edges in %f ms (%f ecols/sec)\n", removedPolys, numEcols, PF_READ_TIME_MS(EcolTimer), (float)numEcols / PF_READ_TIME(EcolTimer));
	Printf("Ecol time = %f, Error prop time = %f, Foldover check time = %f, Topo check time = %f , Heap ops = %f\n", 
		PF_READ_TIME_MS(EcolCollapseTimer), 
		PF_READ_TIME_MS(EcolErrorEvalTimer), 
		PF_READ_TIME_MS(EcolFoldoverTimer), 
		PF_READ_TIME_MS(EcolTopoCheckTimer),
		PF_READ_TIME_MS(EcolHeapOpsTimer));
#endif

	if (PARAM_writetemp.Get()) {
		mesh->Save("C:\\meshes\\simplified.dmsh");
	}

	mesh->OffsetMesh(-offsetVec);

	delete simplify;

	plane.ComputePlane(point2, point3, point2 + Vector3(0.0f, 1.0f, 0.0f));

	mesh->CutWithPlane(plane, true);
	deMesh* meshA = mesh;
	meshA->m_Name = "cut_mesh";

	if (PARAM_writetemp.Get()) {
		mesh->Save("C:\\meshes\\cut.dmsh");
	}

	deMesh* meshB = meshA->Partition();

	const char* outA = NULL;
	PARAM_outA.Get(outA);
	Assert(outA);
	ASSET.CreateLeadingPath(outA);
	meshA->Save(outA);

	const char* outB = NULL;
	PARAM_outB.Get(outB);
	Assert(outB);
	ASSET.CreateLeadingPath(outB);
	meshB->Save(outB);

	delete meshA;
	delete meshB;

	deMesh::CloseErrorStream();

	SHUTDOWN_PARSER;

	return 0;
}
