import os
import sys
import os.path
import optparse
import math
import time

GridSize = 128
CurrGridSize = GridSize
Basename = ""
SimplifyExec = os.getenv("DEMESH_SIMPLIFY", "T:/rage/tools/Modules/base/rage/exes/demesh_simplify.exe")
MergeExec = os.getenv("DEMESH_MERGE", "T:/rage/tools/Modules/base/rage/exes/demesh_merge.exe")
SkirtExec = os.getenv("DEMESH_ADD_SKIRT", "T:/rage/tools/Modules/base/rage/exes/demesh_add_skirt.exe")
EntityExec =  os.getenv("DEMESH_TO_ENTITY", "T:/rage/tools/Modules/base/rage/exes/demesh_to_entity.exe")
SnapDist = None
NameTemplate = "%(Basename)s_L%L_c%C_r%R.dmsh"
NoExec = False
NoFileTest = False
KeepPercent = 50
KeepList = None
CreateSkirts = False
EntityTemplate = None

def makeFileName(lod, row, col):
    global Basename, NameTemplate

    expandedNameTemplate = NameTemplate
    for (cStyleStr, pyStyleStr) in (("%L", str(lod)), ("%C", str(col)), ("%R", str(row))):
        expandedNameTemplate = expandedNameTemplate.replace(cStyleStr, pyStyleStr)

    return expandedNameTemplate

def makeEntityName(lod, row, col):
    global Basename, EntityTemplate

    if not EntityTemplate:
        return None

    expandedNameTemplate = EntityTemplate
    for (cStyleStr, pyStyleStr) in (("%L", str(lod)), ("%C", str(col)), ("%R", str(row))):
        expandedNameTemplate = expandedNameTemplate.replace(cStyleStr, pyStyleStr)

    return expandedNameTemplate

def findExistingFiles(lod, row, col):
    global NoFileTest
    files = []
    name = makeFileName(lod, row, col)
    if NoFileTest or os.path.exists(name):
        files.append(name)            
    return files

def runSimplify(files, outfile, entityfile):
    global SnapDist, SimplifyExec, MergeExec, NoExec, KeepPercent, CreateSkirts, EntityExec
    # could do a timestamp check here. Only regenerate the output files if one of the inputs is more recent
#    sys.stderr.write("Generating file %s\n" % (outfile))
    print ""
    print ""
    print "simplify"
    print "in:   " + ("\nin:   ".join(files))
    print "out: " + outfile
    print time.strftime("%d %b %Y %H:%M:%S")
    print ""

    commands = []

    if CreateSkirts:
        commands.append(MergeExec + " %s -out=merged.dmsh" % (" ".join(files)))
        if EntityTemplate:
            commands.append(SimplifyExec + " merged.dmsh %s -keeppercent=%d " % (outfile, KeepPercent))
            commands.append(SkirtExec + " %s skirted.dmsh " % (outfile))
            commands.append(EntityExec + " skirted.dmsh %s " % (entityfile))
        else:
            commands.append(SimplifyExec + " merged.dmsh simplified.dmsh -keeppercent=%d " % (KeepPercent))
            commands.append(SkirtExec + " simplified.dmsh %s " % (outfile))
    else:
        commands.append(MergeExec + " %s -out=merged.dmsh" % (" ".join(files)))
        commands.append(SimplifyExec + " merged.dmsh %s -keeppercent=%d -fixborders " % (outfile, KeepPercent))
        if EntityTemplate:
            commands.append(EntityExec + " %s %s " % (outfile, entityfile))

    if SnapDist:
        for i in range(len(commands)):
            commands[i] += " -snap=%f " % (SnapDist)

    for i in range(len(commands)):
        print commands[i]
        if not NoExec:
            os.system(commands[i])

def generateOneLOD(lod, row, col):
    files = findExistingFiles(lod, row, col)
    files.extend(findExistingFiles(lod, row, col+1))
    files.extend(findExistingFiles(lod, row+1, col+1))
    files.extend(findExistingFiles(lod, row+1, col))

    if len(files) != 0:
        newFileName = makeFileName(lod + 1, row/2, col/2)
        entityFileName = makeEntityName(lod + 1, row/2, col/2)
        runSimplify(files, newFileName, entityFileName)        

def generateLOD(lod, startRow, endRow, startCol, endCol):
    for i in range(startRow, endRow, 2):
        for j in range(startCol, endCol, 2):
            generateOneLOD(lod, i, j)

def main(inArgs):
    global GridSize, CurrGridSize, Basename, SnapDist, NameTemplate, NoExec
    global  NoFileTest, KeepPercent, KeepList, CreateSkirts, EntityTemplate

    options = optparse.OptionParser()
    options.add_option("-b", "--basename", dest="basename", type="string")
    options.add_option("-s", "--startlod", dest="startLod", type="int", default=0)
    options.add_option("-l", "--endlod", dest="endLod", type="int")
    options.add_option("-r", "--rect", dest="rect", type="float", nargs=4)
    options.add_option("-p", "--point", dest="point", type="float", nargs=2)
    options.add_option("-g", "--grid", dest="gridSize", type="float", default=128.0)
    options.add_option("-k", "--keeppercent", dest="keeppercent", type="int", default=50)
    options.add_option("--keeplist", dest="keeplist", type="string")
    options.add_option("--noexec", dest="noexec", action="store_true")
    options.add_option("--nofiletest", dest="nofiletest", action="store_true")
    options.add_option("--nametemplate", dest="nametemplate", type="string", default=None)
    options.add_option("--entitytemplate", dest="entitytemplate", type="string", default=None)
    options.add_option("--snap", dest="snap", type="float")
    options.add_option("--skirts", dest="skirts", action="store_true")

    (opts, args) = options.parse_args(inArgs)

    # Check for required args:
    if not opts.endLod:
        print "Missing required argument --endlod"
        options.print_help()
        sys.exit(1)

    if not opts.rect and not opts.point:
        print "--rect or --point must be specified"
        options.print_help()
        sys.exit(1)

    if not opts.basename and not opts.nametemplate:
        print "--basename or --nametemplate must be specified"
        options.print_help()
        sys.exit(1)

    print "Arguments are:"
    print " ".join(inArgs)

    if opts.noexec:
        NoExec = True
    if opts.nofiletest:
        NoFileTest = True
    if opts.skirts:
        CreateSkirts = True
    if opts.entitytemplate:
        EntityTemplate = opts.entitytemplate

    if hasattr(opts, "gridSize"):
        GridSize = opts.gridSize

    Basename = opts.basename

    if opts.keeplist:
        KeepList = [int(x) for x in opts.keeplist.split(",")]

    if opts.keeppercent:
        KeepPercent = opts.keeppercent

    if opts.nametemplate:
        NameTemplate = opts.nametemplate

    if opts.snap:
        SnapDist = opts.snap

    if opts.rect:
        (minX, minY, maxX, maxY) = opts.rect
    else:
        (minX, minY) = opts.point
        maxX = minX + 0.0001
        maxY = minY + 0.0001
        minX = minX - 0.0001
        minY = minY - 0.0001

    for lod in range(opts.startLod, opts.endLod):
        if KeepList:
            KeepPercent = KeepList[lod]

        CurrGridSize = GridSize * math.pow(2, lod)

        startRow = 0
        endRow = 0
        startCol = 0
        endCol = 0

        # could find better bounds. Especially for odd LODs
        startRow = (int(math.floor((minY / CurrGridSize) + 0.5)) / 2) * 2
        endRow = (int(math.ceil((maxY / CurrGridSize) - 0.5)) / 2) * 2
        startCol = (int(math.floor((minX / CurrGridSize) + 0.5 )) / 2) * 2
        endCol = (int(math.ceil((maxX / CurrGridSize) - 0.5)) / 2) * 2
#        if lod % 2 == 1:
#            endRow += 1
#            endCol += 1

        print "------------------"
        print lod, CurrGridSize, startRow, endRow-1, startCol, endCol-1
        print ""

        generateLOD(lod, startRow, endRow, startCol, endCol)        
            

if __name__ == "__main__":
    main(sys.argv)
