// 
// demeshtools/demesh_offset.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "demesh/demesh.h"
#include "parser/manager.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

FPARAM(1, in, "Name of the input .dmsh file");
FPARAM(2, offsetX, "Amount to offset mesh in X");
FPARAM(3, offsetZ, "Amount to offset mesh in Z");
FPARAM(4, out, "Name of the output .dmsh file");
PARAM(snap, "Snap distance for vertices");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");

int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}
	
	const char* in = NULL;
	PARAM_in.Get(in);
	Assert(in);

	const char* out = NULL;
	PARAM_out.Get(out);
	Assert(out);

	float xOff = 0.0f;
	PARAM_offsetX.Get(xOff);

	float zOff = 0.0f;
	PARAM_offsetZ.Get(zOff);

	float snap;
	if (PARAM_snap.Get(snap)) {
		deWedge::SetVertSnapDist(snap);
	}

	deMesh* mesh = deMesh::CreateFromFile(in);

	mesh->OffsetMesh(Vector3(xOff, 0.0f, zOff));

	mesh->Save(out);

	delete mesh;

	deMesh::CloseErrorStream();
	
	SHUTDOWN_PARSER;

	return 0;
}
