set ARCHIVE=
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% demesh spatialdata 

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\suite\src %RAGE_DIR%\base\samples 

set LIBS_demesh_view=%LIBS% %RAGE_SAMPLE_GRCORE_LIBS%
set LIBS_demesh_view_bdam=%LIBS% sample_rmcore %RAGE_SAMPLE_GRCORE_LIBS%
set LIBS_demesh_to_bound=%LIBS% %RAGE_PH_LIBS% curve

set TESTERS=demesh_merge demesh_to_mesh mesh_to_demesh demesh_view demesh_to_entity demesh_to_bound
set TESTERS=%TESTERS% demesh_simplify demesh_dice demesh_offset demesh_bdam_simplify demesh_view_bdam
set TESTERS=%TESTERS% demesh_find_holes demesh_add_skirt mesh_to_txtmesh

