// 
// demeshtools/demesh_dice.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define SIMPLE_HEAP_SIZE (1024 * 1024)

#include "demesh/demesh.h"
#include "mathext/linearalgebra.h"
#include "parser/manager.h"
#include "spatialdata/bdamtree.h"
#include "system/main.h"
#include "system/param.h"
#include "vector/geometry.h"
#include "vectormath/legacyconvert.h"

using namespace rage;

FPARAM(1, in, "Name of the input .dmsh");
FPARAM(2, outformat, "Format for the output .dmsh file names. %L will be replaced by the LOD, %R by row, %C by column, and %D by direction");
REQ_PARAM(grid, "Grid spacing");
PARAM(offsetX, "X Offset for this mesh");
PARAM(offsetZ, "Z Offset for this mesh");
PARAM(gridoffsetX, "X Offset for this mesh, in grid units");
PARAM(gridoffsetZ, "Z Offset for this mesh, in grid units");
PARAM(marknocollapse, "Cut edges will be marked as uncollapsable");
PARAM(snap, "Snap distance for vertices");
PARAM(writetemp, "Write temp meshes to C:\\Meshes");
PARAM(plane, "Just cut the mesh with the specified plane (output directions will be E and W)");
PARAM(square, "Cut the mesh into square tiles (output direction will be N)");
PARAM(skirt, "Add a skirt to the borders of the cut meshes");
PARAM(entity, "Write the output files as entities instead of demeshes");
PARAM(lod, "LOD to use for output format string");
PARAM(minarea, "Minimum area for output mesh");
PARAM(tristrip, "Outputs tristripped meshes instead of triangles");
PARAM(zup, "Cuts along the X and Y axes, instead of X and Z");
PARAM(demesherrors, "Save all demesh errors to FILE.dmerr");
PARAM(diceinfo, "Save all dice info to FILE.diceinfo");

fiStream* g_DiceInfoFile = NULL;

void AddSkirt(deMesh* mesh, float size)
{
	Vector3 skirtOffset(g_UnitUp);
	skirtOffset.Scale(-size);

	deMesh* skirtMesh = rage_new deMesh("skirt");
	skirtMesh->m_Materials.Resize(mesh->m_Materials.GetCount()); // TODO: Only copy over materials in use
	for(int i = 0; i < mesh->m_Materials.GetCount(); i++) {
		skirtMesh->m_Materials[i] = mesh->m_Materials[i];
	}

	for(deMesh::EdgeIterator ei = mesh->BeginEdges(); ei != mesh->EndEdges(); ++ei)
	{
		deEdge* edge = &*ei;

		// Get vertices
		deVertex* pStartVert = edge->GetStart();
		deVertex* pEndVert = edge->GetEnd();

		if (pStartVert->IsOnCutEdge() && pEndVert->IsOnCutEdge() && edge->IsOnBoundary())
		{

			// We want to "continue" the attributes on the adjacent mesh face on to the 
			// other end of the skirt, rather than just copying them
			// I.e. if we had a repeating texture on the tile we want the texture to 
			// continue repeating on the skirt.

			// So the idea is to build the skirt as if its extending the existing mesh
			// and then drop it down later

			deEdge* prevEdge = edge->GetPrevAlongBoundary();
			deEdge* nextEdge = edge->GetNextAlongBoundary();

			// Get face-area-weighted average normals at the vertices of this edge
			Vector3 vStartNormal;
			vStartNormal.Zero();

			float totalStartArea = pStartVert->GetNeighborhoodArea();
			for(deVertex::EdgeIterator e = pStartVert->BeginEdges(); e != pStartVert->EndEdges(); ++e)
			{
				vStartNormal += e->GetFaceNormal() * (e->GetFaceArea() / totalStartArea);
			}
			vStartNormal.Normalize();

			Vector3 vEndNormal;
			vEndNormal.Zero();
			float totalEndArea = pEndVert->GetNeighborhoodArea();
			for(deVertex::EdgeIterator e = pEndVert->BeginEdges(); e != pEndVert->EndEdges(); ++e)
			{
				vEndNormal += e->GetFaceNormal() * (e->GetFaceArea() / totalEndArea);
			}
			vEndNormal.Normalize();

			Vector3 vStartPos = edge->GetStartWedge().GetPosition();
			Vector3 vEndPos = edge->GetEndWedge().GetPosition();
			Vector3 vEdgeDirection = vEndPos - vStartPos;

			Vector3 vPrevEdgeDirection = vEdgeDirection;
			if (prevEdge != NULL)
			{
				vPrevEdgeDirection = prevEdge->GetEndWedge().GetPosition() - prevEdge->GetStartWedge().GetPosition();
			}
			vPrevEdgeDirection.Normalize();

			Vector3 vNextEdgeDirection = vEdgeDirection;
			if (nextEdge != NULL)
			{
				vNextEdgeDirection = nextEdge->GetEndWedge().GetPosition() - nextEdge->GetStartWedge().GetPosition();
			}
			vNextEdgeDirection.Normalize();

			// Get direction away from mesh

			Vector3 cDirectionAwayFromEdge;
			cDirectionAwayFromEdge.Cross(edge->GetFaceNormal(), vEdgeDirection);
			cDirectionAwayFromEdge.Normalize();

			Vector3 cStartDirectionAwayFromMesh = cDirectionAwayFromEdge;
			if (prevEdge != NULL)
			{
				Vector3 cDirectionAwayFromPrevEdge;
				cDirectionAwayFromPrevEdge.Cross(prevEdge->GetFaceNormal(), vPrevEdgeDirection);
				cDirectionAwayFromPrevEdge.Normalize();
				cStartDirectionAwayFromMesh += cDirectionAwayFromPrevEdge;
			}
			cStartDirectionAwayFromMesh.y = 0.0f;
			cStartDirectionAwayFromMesh.Normalize();

			Vector3 cEndDirectionAwayFromMesh = cDirectionAwayFromEdge;
			if (nextEdge != NULL)
			{
				Vector3 cDirectionAwayFromNextEdge;
				cDirectionAwayFromNextEdge.Cross(nextEdge->GetFaceNormal(), vNextEdgeDirection);
				cDirectionAwayFromNextEdge.Normalize();
				cEndDirectionAwayFromMesh += cDirectionAwayFromNextEdge;
			}
			cEndDirectionAwayFromMesh.y = 0.0f;
			cEndDirectionAwayFromMesh.Normalize();

			// Skirt direction is the average between the direction away from mesh and the normal to the mesh
			Vector3 cStartSkirtDirection; cStartSkirtDirection.Average(cStartDirectionAwayFromMesh, vStartNormal);
			Vector3 cEndSkirtDirection; cEndSkirtDirection.Average(cEndDirectionAwayFromMesh, vEndNormal);

			cStartSkirtDirection.Scale(size);
			cEndSkirtDirection.Scale(size);


			// Generate the skirt polys
			deMesh::FaceInfo face1, face2;

			face1.inoutVtx[0] = NULL;
			face1.inoutVtx[1] = NULL;
			face1.inoutVtx[2] = NULL;
			face1.inData[0] = edge->GetStartWedge();
			face1.inData[1] = edge->GetStartWedge();
			face1.inData[1].SetPosition(face1.inData[1].GetPosition() - cStartSkirtDirection);
			face1.inData[2] = edge->GetEndWedge();

			skirtMesh->AddFace(face1);

			face2.inoutVtx[0] = NULL;
			face2.inoutVtx[1] = NULL;
			face2.inoutVtx[2] = NULL;
			face2.inData[0] = edge->GetEndWedge();
			face2.inData[1] = edge->GetStartWedge();
			face2.inData[1].SetPosition(face2.inData[1].GetPosition() - cStartSkirtDirection);
			face2.inData[2] = edge->GetEndWedge();
			face2.inData[2].SetPosition(face2.inData[2].GetPosition() - cEndSkirtDirection);

			skirtMesh->AddFace(face2);
		}
	}

	skirtMesh->CollectGarbage();
	mesh->Combine(*skirtMesh);

	delete skirtMesh;
}

#define DICEINFO(fmt,...) if (g_DiceInfoFile) { fprintf(g_DiceInfoFile, fmt "\n",##__VA_ARGS__); }

struct PerMtlRanges
{
	int numTexCoords;
	atArray<Vector2> minUVs;
	atArray<Vector2> maxUVs;
	Vector3 minPos;
	Vector3 maxPos;

	void Init(int uvSets)
	{
		numTexCoords = 0;
		minPos.Set(FLT_MAX);
		maxPos.Set(-FLT_MAX);

		minUVs.Resize(uvSets);
		maxUVs.Resize(uvSets);
		for(int i = 0; i < uvSets; i++)
		{
			minUVs[i].Set(FLT_MAX);
			maxUVs[i].Set(-FLT_MAX);
		}
	}
};

void WriteMesh(deMesh* mesh, spdBdamLocation loc, const char* formatString)
{
	if (mesh->m_Vertices.size() == 0)
	{
		char errorMsg[RAGE_MAX_PATH];

		loc.FormatLocationString(errorMsg, "no data at loc (lrcd): %L %R %C %D\n");
		deMeshError error(mesh, errorMsg);
		deMesh::ReportError(error);

		Printf(errorMsg);

		// no data, don't write anything
		return;
	}

	if (PARAM_minarea.Get())
	{
		float area = 0;
		for(deMesh::FaceEdgeIterator fi = mesh->BeginFaceEdges(); fi != mesh->EndFaceEdges(); ++fi)
		{
			area += fi->GetFaceArea();
		}

		float minarea;
		PARAM_minarea.Get(minarea);

		if (area < minarea)
		{
			char errorMsg[RAGE_MAX_PATH];
			char formatString[RAGE_MAX_PATH];
			

			loc.FormatLocationString(formatString, "area %f too small at loc (lrcd): %L %R %C %D\n");
			formatf(errorMsg, formatString, area);
			
			Warningf(errorMsg);

			deMeshError error(mesh, errorMsg);
			deMesh::ReportError(error);

			return;
		}
	}

	if (PARAM_skirt.Get())
	{
		if (PARAM_writetemp.Get())
		{
			mesh->Save("C:\\Meshes\\preskirt.dmsh");
		}
		// add a skirt to the mesh
		float height=10.0f;
		PARAM_skirt.Get(height);

		AddSkirt(mesh, height);
	}

	char newFileName[RAGE_MAX_PATH];
	loc.FormatLocationString(newFileName, "output_loc (lrcd): %L %R %C %D\n");
	Printf(newFileName);

	loc.FormatLocationString(newFileName, formatString);
	Printf("output_file: %s\n", newFileName);

	if (PARAM_diceinfo.Get())
	{
		mesh->CollectGarbage();

		DICEINFO("Writing mesh %s", newFileName);

		// Collect vertex location and min/max UV ranges
		Vector3 minPos, maxPos;



		atArray<PerMtlRanges> ranges;
		ranges.Resize(mesh->m_Materials.GetCount());
		for(int i = 0; i < mesh->m_Materials.GetCount(); i++)
		{
			ranges[i].Init(mshMaxTexCoordSets);
		}

		for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi)
		{
			minPos.Min(minPos, (*vi)->GetPosition());
			maxPos.Max(maxPos, (*vi)->GetPosition());

			for(int wedgeIndex = 0; wedgeIndex < (*vi)->GetNumWedges(); wedgeIndex++)
			{
				deWedge& wedge = (*vi)->GetWedge(wedgeIndex);
				int numTexCoords = wedge.GetVertexData().TexCount;

				PerMtlRanges& range = ranges[wedge.GetMaterial()];
				range.numTexCoords = Max(range.numTexCoords, numTexCoords);
				range.minPos.Min(range.minPos, (*vi)->GetPosition());
				range.maxPos.Max(range.maxPos, (*vi)->GetPosition());

				for(int i = 0; i < numTexCoords; i++)
				{
					geomVectors::Min(range.minUVs[i], wedge.GetVertexData().Tex[i]);
					geomVectors::Max(range.maxUVs[i], wedge.GetVertexData().Tex[i]);
				}
			}
		}

		DICEINFO("TILE %s     DiceMinPos = %f %f %f", newFileName, minPos.x, minPos.y, minPos.z);
		DICEINFO("TILE %s     DiceMaxPos = %f %f %f", newFileName, maxPos.x, maxPos.y, maxPos.z);
		DICEINFO("TILE %s     MtlCount = %d", newFileName, mesh->m_Materials.GetCount());
		for(int i = 0; i < mesh->m_Materials.GetCount(); i++)
		{
			DICEINFO("TILE %s         MTL %d MtlName %s", newFileName, i, mesh->m_Materials[i].m_MtlFileName);
			DICEINFO("TILE %s         MTL %d SpsName %s", newFileName, i, mesh->m_Materials[i].m_ShaderVarFilename);
			DICEINFO("TILE %s         MTL %d MinPos = %f %f %f", newFileName, i, ranges[i].minPos.x, ranges[i].minPos.y, ranges[i].minPos.z);
			DICEINFO("TILE %s         MTL %d MaxPos = %f %f %f", newFileName, i, ranges[i].maxPos.x, ranges[i].maxPos.y, ranges[i].maxPos.z);
			DICEINFO("TILE %s         MTL %d UVCount = %d", newFileName, i, ranges[i].numTexCoords);
			for(int j = 0; j < ranges[i].numTexCoords; j++)
			{
				DICEINFO("TILE %s             MTL %d UV %d MinUV = %f %f", newFileName, i, j, ranges[i].minUVs[j].x, ranges[i].minUVs[j].y);
				DICEINFO("TILE %s             MTL %d UV %d MaxUV = %f %f", newFileName, i, j, ranges[i].maxUVs[j].x, ranges[i].maxUVs[j].y);
			}
		}

		DICEINFO("Finished mesh %s", newFileName);
	}

	if (PARAM_entity.Get())
	{
		char tempName[RAGE_MAX_PATH];
		formatf(tempName, RAGE_MAX_PATH, "%s\\entity.type", newFileName);
		ASSET.CreateLeadingPath(tempName);
		ASSET.PushFolder(newFileName);
		mesh->SaveEntity(newFileName, "entity.type", "mesh.mesh", "mesh", PARAM_tristrip.Get());		
		ASSET.PopFolder();
	}
	else
	{
		ASSET.CreateLeadingPath(newFileName);
		mesh->Save(newFileName);
	}
}

int Main()
{
	INIT_PARSER;

	const char* errorLog = NULL;
	if (PARAM_demesherrors.Get(errorLog))
	{
		deMesh::OpenErrorStream(errorLog);
	}
	
    const char* diceInfo = NULL;
    if (PARAM_diceinfo.Get(diceInfo))
    {
        g_DiceInfoFile = ASSET.Create(diceInfo, "diceinfo");
    }
    
	const char* in = NULL;
	PARAM_in.Get(in);
	Assert(in);

	const char* formatString = NULL;
	PARAM_outformat.Get(formatString);
	Assert(formatString);

	float snap;
	if (PARAM_snap.Get(snap)) {
		deWedge::SetVertSnapDist(snap);
	}

	int lod = 0;
	PARAM_lod.Get(lod);

	bool markNoCollpase = PARAM_marknocollapse.Get();

	deMesh* mesh = deMesh::CreateFromFile(in);

	float gridStep = 128.0f;
	PARAM_grid.Get(gridStep);

	float xOff = 0.0f;
	float zOff = 0.0f;

	if (PARAM_gridoffsetX.Get()) {
		PARAM_gridoffsetX.Get(xOff);
		xOff *= gridStep;
	}
	else {
		PARAM_offsetX.Get(xOff);
	}

	if (PARAM_gridoffsetZ.Get()) {
		PARAM_gridoffsetZ.Get(zOff);
		zOff *= gridStep;
	}
	else {
		PARAM_offsetZ.Get(zOff);
	}
	
	float minX, maxX, minZ, maxZ;
	Vector3 mins, maxes;
	mesh->FindBounds(mins, maxes);
	minX = mins.x;
	maxX = maxes.x;
	minZ = PARAM_zup.Get() ? mins.y : mins.z;
	maxZ = PARAM_zup.Get() ? maxes.y : maxes.z;

	// move the bounds to their final position
	minX += xOff;
	maxX += xOff;
	minZ += zOff;
	maxZ += zOff;

	// Flag all the edges as if they were cut so I know to add skirts later
	for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi)
	{
		float x = (*vi)->GetPosition().x;
		float z = (*vi)->GetPosition().z;

		if( (fabs(x - minX) < FLT_EPSILON) || (fabs(x - maxX) < FLT_EPSILON) || (fabs(z - minZ) < FLT_EPSILON) || (fabs(z - maxZ) < FLT_EPSILON))
		{
			// On an existing edge, so flag it for later
			(*vi)->SetOnCutEdge(true);
		}
	}
	

	// shrink the mesh bounds a bit so that if part of the mesh is just outside the boundary 
	// but still within the mesh snap dist we don't add an unnecessary row or column
	minX += deWedge::GetVertSnapDist();
	minZ += deWedge::GetVertSnapDist();
	maxX -= deWedge::GetVertSnapDist();
	maxZ -= deWedge::GetVertSnapDist();

	Printf("Mesh bounds are: (%f %f) -> (%f %f)\n", minX, minZ, maxX, maxZ);

	// simple version: cut with just one plane
	if (PARAM_plane.Get())
	{
		float planeCoeffs[4];
		PARAM_plane.GetArray(planeCoeffs, 4);

		Vector4 plane(planeCoeffs[0], planeCoeffs[1], planeCoeffs[2], planeCoeffs[3]);

		mesh->CutWithPlane(plane, true);
		if (markNoCollpase)
		{
			mesh->MarkProcessedVerts(deVertex::FLAG_NO_COLLAPSE, true);
		}
		mesh->MarkProcessedVerts(deVertex::FLAG_ON_CUT_EDGE, false);

		deMesh* east = mesh->Partition();
		
		spdBdamLocation loc(lod, 0, 0, spdBdamLocation::DIR_E);

		WriteMesh(east, loc, formatString);

		loc.m_Dir = spdBdamLocation::DIR_W;

		WriteMesh(mesh, loc, formatString);

		delete east;
		delete mesh;

		return 0;
	}

	// Compute the grid indices we'll be creating
	int beginCellX = (int)(floorf(minX / gridStep));
	int endCellX = (int)(ceilf(maxX / gridStep));
	int beginCellZ = (int)(floorf(minZ / gridStep));
	int endCellZ = (int)(ceilf(maxZ / gridStep));

	Printf("Creating meshes from column %d to %d and row %d to %d\n", beginCellX, endCellX, beginCellZ, endCellZ);

	// To work around possible accuracy problems dicing far from the origin, translate everything to somewhere around the origin.
	int cellOffsetX = ((endCellX + beginCellX) / 2);
	int cellOffsetZ = ((endCellZ + beginCellZ) / 2);

	Vector3 centeringOffset;
	if (PARAM_zup.Get())
	{
		centeringOffset.Set(-gridStep * cellOffsetX, -gridStep * cellOffsetZ, 0.0f);
	}
	else
	{
		centeringOffset.Set(-gridStep * cellOffsetX, 0.0f, -gridStep * cellOffsetZ);
	}
	mesh->OffsetMesh(centeringOffset);

	Vector3 finalMeshLocation;

	if (PARAM_zup.Get())
	{
		finalMeshLocation.Set(xOff, zOff, 0.0f);
	}
	else
	{
		finalMeshLocation.Set(xOff, 0.0f, zOff);
	}

	finalMeshLocation.Subtract(centeringOffset);

	// For each grid column, do a cut and partition to create a column mesh and the remaining chunk.

	// For each grid row, do a cut and parition on the column mesh to create a cell mesh and the remaining chunk

	// For each cell mesh, do a cut and partition to get the NE and SW meshes.

	// For the NE and SW mesh, do a cut and partition to create the N, E, S, and W meshes.

	// This _may_ be more efficient than doing all the cuts first because all the cuts after the first are done on smaller meshes
	// then again re-paritioning might be slow

	for(int i = beginCellX - cellOffsetX; i < endCellX - cellOffsetX; i++)
	{
		Printf("Cutting column %d...\n", i + cellOffsetX);

		// Make the vertical plane
		Vector4 vertPlane(1.0f, 0.0f, 0.0f, (float)(i+1) * gridStep);
		mesh->CutWithPlane(vertPlane, true);
		if (markNoCollpase) {
			mesh->MarkProcessedVerts(deVertex::FLAG_NO_COLLAPSE, false);
		}
		mesh->MarkProcessedVerts(deVertex::FLAG_ON_CUT_EDGE, false);

		deMesh* column = mesh->Partition();
		char colName[128];
		formatf(colName, "column_%d", i);
		column->m_Name = colName;

		if (PARAM_writetemp.Get())
		{
			column->Save("C:/Meshes/column.dmsh");
			mesh->Save("C:/Meshes/remaining.dmsh");
		}

		for(int j = beginCellZ - cellOffsetZ; j < endCellZ - cellOffsetZ; j++) {
			Vector4 horizPlane;
			if (PARAM_zup.Get())
			{
				horizPlane.Set(0.0f, 1.0f, 0.0f, (float)(j+1) * gridStep);
			}
			else
			{
				horizPlane.Set(0.0f, 0.0f, 1.0f, (float)(j+1) * gridStep);
			}

			column->CutWithPlane(horizPlane, true);
			if (markNoCollpase) {
				column->MarkProcessedVerts(deVertex::FLAG_NO_COLLAPSE, false);
			}
			column->MarkProcessedVerts(deVertex::FLAG_ON_CUT_EDGE, false);

			deMesh* cell = column->Partition();
			char cellName[128];
			formatf(cellName, "cell_%d_%d", i, j);
			cell->m_Name = cellName;

			if (PARAM_square.Get())
			{
				spdBdamLocation loc;

				loc.Set(lod, j + cellOffsetZ, i + cellOffsetX, spdBdamLocation::DIR_N);

				cell->OffsetMesh(finalMeshLocation);
				WriteMesh(cell, loc, formatString);

				delete cell;

				continue;
			}

			// Offset cell to near the origin
			Vector3 cellToOrigin;
			if (PARAM_zup.Get())
			{
				cellToOrigin.Set(-i * gridStep - 0.5f * gridStep, - j * gridStep - 0.5f * gridStep, 0.0f);
			}
			else
			{
				cellToOrigin.Set(-i * gridStep - 0.5f * gridStep, 0.0f, - j * gridStep - 0.5f * gridStep);
			}
			cell->OffsetMesh(cellToOrigin);

			if (PARAM_writetemp.Get())
			{
				cell->Save("C:/Meshes/cell.dmsh");
			}

			// Cut cell into NE and SW halves

			Vector4 diag1;
//			diag1.ComputePlane(Vector3((float)(i) * gridStep, 0.0f, (float)(j) * gridStep), Vector3(M_SQRT2 * 0.5f, 0.0f, -M_SQRT2 * 0.5f));
			if (PARAM_zup.Get())
			{
				diag1.Set(1.0f, -1.0f, 0.0f, 0.0f); // not technically a valid plane, but since w is 0.0f, a non-normalized a,b,c should be OK
			}
			else
			{
				diag1.Set(1.0f, 0.0f, -1.0f, 0.0f); // not technically a valid plane, but since w is 0.0f, a non-normalized a,b,c should be OK
			}

			cell->CutWithPlane(diag1, true);
			if (markNoCollpase) {
				cell->MarkProcessedVerts(deVertex::FLAG_NO_COLLAPSE, false);
			}
			cell->MarkProcessedVerts(deVertex::FLAG_ON_CUT_EDGE, false);

			deMesh* sw = cell->Partition();

			formatf(cellName, "cell_%d_%d_sw", i, j);
			sw->m_Name = cellName;
			
			if (PARAM_writetemp.Get())
			{
				cell->Save("C:/Meshes/ne.dmsh");
				sw->Save("C:/Meshes/sw.dmsh");
			}

			Vector4 diag2;
//			diag2.ComputePlane(Vector3((float)(i+1) * gridStep, 0.0f, (float)(j) * gridStep), Vector3(M_SQRT2 * 0.5f, 0.0f, M_SQRT2 * 0.5f));
			if (PARAM_zup.Get())
			{
				diag2.Set(1.0f, 1.0f, 0.0f, 0.0f);
			}
			else
			{
				diag2.Set(1.0f, 0.0f, 1.0f, 0.0f);
			}

			sw->CutWithPlane(diag2, true);
			if (markNoCollpase) {
				sw->MarkProcessedVerts(deVertex::FLAG_NO_COLLAPSE, false);
			}
			sw->MarkProcessedVerts(deVertex::FLAG_ON_CUT_EDGE, false);

			deMesh* w = sw->Partition();
			formatf(cellName, "cell_%d_%d_w", i, j);
			w->m_Name = cellName;

			cell->CutWithPlane(diag2, true);
			if (markNoCollpase) {
				cell->MarkProcessedVerts(deVertex::FLAG_NO_COLLAPSE, false);
			}
			cell->MarkProcessedVerts(deVertex::FLAG_ON_CUT_EDGE, false);

			deMesh* n = cell->Partition();

			formatf(cellName, "cell_%d_%d_n", i, j);
			n->m_Name = cellName;

			deMesh* s = sw;
			formatf(cellName, "cell_%d_%d_s", i, j);
			s->m_Name = cellName;
			
			deMesh* e = cell;
			formatf(cellName, "cell_%d_%d_e", i, j);
			e->m_Name = cellName;
			
			Vector3 cellOffset;
			cellOffset.Set(finalMeshLocation);
			cellOffset.Subtract(cellToOrigin);

			n->OffsetMesh(cellOffset);
			e->OffsetMesh(cellOffset);
			s->OffsetMesh(cellOffset);
			w->OffsetMesh(cellOffset);


			spdBdamLocation loc;

			loc.Set(lod, j + cellOffsetZ, i + cellOffsetX, spdBdamLocation::DIR_N);

			WriteMesh(n, loc, formatString);
			WriteMesh(e, loc, formatString);
			WriteMesh(s, loc, formatString);
			WriteMesh(w, loc, formatString);
			
			delete n;
			delete e;
			delete s;
			delete w;
		}

		delete column;
	}

	delete mesh;

	deMesh::CloseErrorStream();

	SHUTDOWN_PARSER;

    if (g_DiceInfoFile)
    {
        g_DiceInfoFile->Close();
    }
    
	return 0;
}
