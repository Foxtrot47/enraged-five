﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;

namespace AnimConcatDriver
{
    class Program
    {
        static void PrintHelp()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("note: x amount of input folders can be provided, comma delimited.");
            Console.WriteLine("\t 2) animconcatdriver.exe concatList");
            Console.WriteLine("\t 2) animconcatdriver.exe concatList -perforce");
        }

        [STAThread]
        static int Main(string[] args)
        {
            string strFile = "";
            bool bPerforce = false;

            if (args.Length >= 1)
            {
                strFile = args[0];
                if(args.Length > 1)
                    bPerforce = args[1] == "-perforce";
            }
            else
            {
                PrintHelp();
                return -1;
            }

            AnimConcat ac = new AnimConcat();
            ac.m_P4Integration = bPerforce;

            return ac.PreProcess(strFile);
        }
    }
}
