﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Threading;
using System.Xml;
using RSG.SourceControl.Perforce;
using P4API;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;

namespace AnimConcatDriver
{
    #region Additional Classes

    class Log
    {
        private IUniversalLog log = null;
        private IUniversalLogTarget logTarget = null;

        public Log(string strLogFilename)
        {
            log = LogFactory.CreateUniversalLog(strLogFilename);
            logTarget = LogFactory.CreateUniversalLogFile(log);
        }

        ~Log()
        {
            CloseFile();
        }

        public void CloseFile()
        {
            if (logTarget != null && log != null)
            {
                ((UniversalLogFile)logTarget).Flush();
                logTarget.Disconnect(log);
            }
        }

        public void LogLine(string strLine)
        {
            strLine = strLine.Replace("\\", "/"); // Easier to copy lines out to debug in commandline
            if (logTarget != null && log !=null)
            {
                lock (log)
                {
                    if (strLine.StartsWith("ERROR:"))
                    {
                        log.Error(strLine.Substring(7));
                    }
                    else if (strLine.StartsWith("WARNING:"))
                    {
                        log.Warning(strLine.Substring(9));
                    }
                    else if (strLine.StartsWith("DEBUG:"))
                    {
                        log.Debug(strLine.Substring(7));
                    }
                    else
                    {
                        log.Message(strLine);
                    }
                }
            }
        }
    }

    class AnimConcatEntry
    {
        public string CutFile
        {
            get { return strCutFile; }
            set { strCutFile = value; }
        }

        public ArrayList AnimFiles
        {
            get { return lstAnimFiles; }
            set { lstAnimFiles = value; }
        }

        public void AddAnim(string strAnimPath)
        {
            lstAnimFiles.Add(strAnimPath);
        }

        private string strCutFile = String.Empty;
        private ArrayList lstAnimFiles = new ArrayList();
    }

    class ClipEntry
    {
        public string ClipName
        {
            get { return strClipName; }
            set { strClipName = value; }
        }

        public ArrayList ClipFiles
        {
            get { return lstClipFiles; }
            set { lstClipFiles = value; }
        }

        public void AddClip(string strClipPath)
        {
            lstClipFiles.Add(strClipPath);
        }

        private string strClipName = String.Empty;
        private ArrayList lstClipFiles = new ArrayList();
    }

    class AnimRun
    {
        public ArrayList Operations
        {
            get { return lstOperations; }
            set { lstOperations = value; }
        }

        public string AnimName
        {
            get { return strAnimName; }
            set { strAnimName = value; }
        }

        private string strAnimName = String.Empty;
        private ArrayList lstOperations = new ArrayList();
    }

    #endregion

    class AnimConcat
    {
        public bool m_P4Integration = false;
        private P4 m_P4 = null;
        private ArrayList lstAnimConcatEntries = new ArrayList();
        private ArrayList lstEveryPossibleAnim = new ArrayList();
        private ArrayList lstClipEntries = new ArrayList();
        private ArrayList lstRuns = new ArrayList();
        private Log lgLog = null;
        private string strAnimConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/animconcat.exe";
        private string strClipConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/cutscene/cutfclipconcat.exe";
        private string strCutXMLConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/cutscene/cutfconcat.exe";

        private string TempFileNamePrefix = "animconcatdriver";
        private int m_UniqueFileIdentifier;
        private object m_FileIdentifierLock;
        public int UniqueFileIdentifier
        {
            get { return m_UniqueFileIdentifier++; }
        }

        private string OutputFolder;
        private List<Thread> ActiveThreads;
        private int MaximumThreads;
        private bool ProcessRunsSuccess;
        public bool Multithreaded;
        public AnimConcat()
        {
            ActiveThreads = new List<Thread>();
            MaximumThreads = Environment.ProcessorCount + 2;
            OutputFolder = null;
            Multithreaded = true;
        }

        //Threading
        public void ProcessRunThread(object run)
        {
            AnimRun arRun = (AnimRun)run;
            bool processResult = ProcessRun(arRun, OutputFolder);
            if (processResult == false)
            {
                ProcessRunsSuccess = false;
            }
        }

        public void ProcessClipThread(object clip)
        {
            ClipEntry clipEntry = (ClipEntry)clip;
            int result = ConcatClip(clipEntry, OutputFolder); 
            if ( result == -1) // Concat the clips
            {
                ProcessRunsSuccess = false;
            }
        }

        public int PreProcess(string strFile)
        {
            List<string> folderList = new List<string>();
            string strOutputFolder = String.Empty;
            string strAudio = String.Empty;
            int iSectionMethod = 0;
            int iSectionDuration = 0;

            using (XmlTextReader textReader = new XmlTextReader(strFile))
            {
                textReader.ReadToFollowing("parts");

                XmlReader objectReader = textReader.ReadSubtree();

                while (objectReader.ReadToFollowing("Item"))
                {
                    XmlReader objectIdReader = objectReader.ReadSubtree();

                    if (objectIdReader.ReadToFollowing("filename"))
                    {
                        string strFilename = objectIdReader.ReadInnerXml();
                        strFilename = strFilename.Replace(Path.GetExtension(strFilename), "");
                        folderList.Add(strFilename);
                    }
                }
            }

            using (XmlTextReader textReader = new XmlTextReader(strFile))
            {
                if (textReader.ReadToFollowing("path"))
                {
                    strOutputFolder = textReader.ReadInnerXml();
                }
            }

            using (XmlTextReader textReader = new XmlTextReader(strFile))
            {
                if (textReader.ReadToFollowing("audio"))
                {
                    strAudio = textReader.ReadInnerXml();
                }
            }

            using (XmlTextReader textReader = new XmlTextReader(strFile))
            {
                if (textReader.ReadToFollowing("sectionMethodIndex"))
                {
                    Int32.TryParse(textReader.GetAttribute(0), out iSectionMethod);
                }
            }

            using (XmlTextReader textReader = new XmlTextReader(strFile))
            {
                if (textReader.ReadToFollowing("sectionTimeDuration"))
                {
                    Int32.TryParse(textReader.GetAttribute(0), out iSectionDuration);
                }
            }

            return Process(folderList.ToArray(), strOutputFolder, strAudio, iSectionMethod, iSectionDuration);
        }

        // So it begins!
        public int Process(string[] folderList, string strOutputFolder, string strAudio, int iSectionMethod, int iSectionDuration)
        {
            lgLog = new Log( System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/logs/cutscene/process/animconcatdriver" );

            if (m_P4Integration)
            {
                m_P4 = new P4();
                m_P4.Connect();
            }

            m_UniqueFileIdentifier = 0;
            m_FileIdentifierLock = new object();
            DeleteAllTempFiles();
            OutputFolder = strOutputFolder;

            DateTime startTime = DateTime.Now;
            try
            {
                Console.WriteLine("processing..");
                PrintCommandLine(folderList, strOutputFolder);

                if (CreateOutputFolder(strOutputFolder) == false)
                {
                    lgLog.CloseFile();
                    return -1;
                }
                
				if (!strOutputFolder.EndsWith("\\") && !strOutputFolder.EndsWith("/"))
	            {
	                strOutputFolder += "/";
	            }

                if (folderList.Length < 2)
                {
                    lgLog.LogLine("ERROR: 2 or more folders must be specified");
                    lgLog.CloseFile();
                    return -1;
                }

                if (ProcessFolders(folderList) == false) // Process the folders to create lists of all the anims
                {
                    lgLog.CloseFile();
                    return -1;
                }

                //Synchronize the concatenation folder to the latest.
                if (!RunP4Command("sync", strOutputFolder, "", "..."))
                {
                    lgLog.LogLine("ERROR: Unable to synchronize " + strOutputFolder + "\\" + "... to the latest.");
                    return -1;
                }

                ProcessIntoRuns(); // Process into runs
                CompressRuns(); // Compress the runs so if we had multiple paddings, add them up into one

                lgLog.LogLine("Processing " + lstRuns.Count + " Runs");

                if ( Multithreaded == true )
                {
                    int currentThreadIndex = 0;
                    ProcessRunsSuccess = true;
                    ActiveThreads.Clear();

                    while (currentThreadIndex < lstRuns.Count)
                    {
                        for (int threadIndex = 0; threadIndex < ActiveThreads.Count; ++threadIndex)
                        {
                            Thread currentThread = ActiveThreads[threadIndex];
                            if (currentThread.IsAlive == false)
                            {
                                //Remove this from the list.
                                ActiveThreads.RemoveAt(threadIndex);
                                threadIndex--;
                            }
                        }

                        while (ActiveThreads.Count < MaximumThreads && currentThreadIndex < lstRuns.Count)
                        {
                            Thread newRunThread = new Thread(ProcessRunThread);
                            //newRunThread.SetApartmentState(ApartmentState.STA);
                            newRunThread.Name = "Process Run Thread " + currentThreadIndex;
                            ActiveThreads.Add(newRunThread);

                            AnimRun arRun = (AnimRun)lstRuns[currentThreadIndex];
                            lgLog.LogLine("Processing run: " + arRun.AnimName + " - " + arRun.Operations.Count + " entries - " + (currentThreadIndex + 1) + "/" + lstRuns.Count);
                            newRunThread.Start(arRun);
                            currentThreadIndex++;
                        }
                    }

                    foreach (Thread runThread in ActiveThreads)
                    {
                        runThread.Join();
                    }

                    if (!ProcessRunsSuccess)
                    {
                        lgLog.CloseFile();
                        return -1;
                    }

                    //Now parallelize all of the clips.
                    currentThreadIndex = 0;
                    ProcessRunsSuccess = true;
                    while (currentThreadIndex < lstClipEntries.Count)
                    {
                        for (int threadIndex = 0; threadIndex < ActiveThreads.Count; ++threadIndex)
                        {
                            Thread currentThread = ActiveThreads[threadIndex];
                            if (currentThread.IsAlive == false)
                            {
                                //Remove this from the list.
                                ActiveThreads.RemoveAt(threadIndex);
                                threadIndex--;
                            }
                        }

                        while (ActiveThreads.Count < MaximumThreads && currentThreadIndex < lstClipEntries.Count)
                        {
                            Thread newClipThread = new Thread(ProcessClipThread);
                            //newClipThread.SetApartmentState(ApartmentState.STA);
                            newClipThread.Name = "Process Clip Thread " + currentThreadIndex;
                            ActiveThreads.Add(newClipThread);

                            ClipEntry clipEntry = (ClipEntry)lstClipEntries[currentThreadIndex];

                            lgLog.LogLine("Concatting clips for: " + clipEntry.ClipName + " - " + (currentThreadIndex + 1) + "/" + lstClipEntries.Count);
                            newClipThread.Start(clipEntry);
                            currentThreadIndex++;
                        }
                    }

                    foreach (Thread runThread in ActiveThreads)
                    {
                        runThread.Join();
                    }

                    if (!ProcessRunsSuccess)
                    {
                        lgLog.CloseFile();
                        return -1;
                    }
                }
                else
                {
                    for (int iRun = 0; iRun < lstRuns.Count; ++iRun)
                    {
                        AnimRun arRun = (AnimRun)lstRuns[iRun];
                        lgLog.LogLine("Processing run: " + arRun.AnimName + " - " + arRun.Operations.Count + " entries - " + (iRun + 1) + "/" + lstRuns.Count);
                        if (!ProcessRun(arRun, strOutputFolder))
                        {
                            lgLog.CloseFile();
                            return -1;
                        }
                    }

                    lgLog.LogLine("Concatenating Clips");

                    if (ConcatClips(strOutputFolder) == -1) // Concat the clips
                    {
                        lgLog.CloseFile();
                        return -1;
                    }
                }

          
                lgLog.LogLine("Concatenating Cut Files");

                if (ConcatCutXML(folderList, strOutputFolder, strAudio, iSectionMethod, iSectionDuration) == -1) // Concat the cutxmls
                {
                    lgLog.CloseFile();
                    return -1;
                }
            }
            catch (Exception ex)
            {
                if (Multithreaded == true)
                {
                    foreach (Thread runThread in ActiveThreads)
                    {
                        runThread.Join();
                    }
                }

                lgLog.LogLine("**EXCEPTION** : " + ex.Message);
                lgLog.CloseFile();
                return -1;
            }

            DateTime endTime = DateTime.Now;
            TimeSpan elapsed = endTime - startTime;
            Console.WriteLine("Time Elapsed: " + elapsed.TotalSeconds);

            PerformMarkForDelete(strOutputFolder);
            lgLog.CloseFile();

            if (m_P4Integration)
            {
                m_P4.Disconnect();
            }
            return 0;
        }

        private string[] GenerateCommands(string[] strArgs, string strFile)
        {
            List<string> alCommands = new List<string>();

            foreach (string arg in strArgs)
            {
                alCommands.Add(arg);
            }

            alCommands.Add(strFile);

            return alCommands.ToArray();
        }

        private bool RunP4Command(string strCommand, string strOutputFolder, string strAdditionalFolder, string strFilename, params string[] strArgs)
        {
            if (!m_P4Integration) return true;

            P4RecordSet recordSet = null;
            string strFullPath = Path.Combine(strOutputFolder, strAdditionalFolder, strFilename);
            try
            {
                lgLog.LogLine("DEBUG: P4 " + strCommand + ", " + strFullPath);
                string localPath = Path.Combine(strOutputFolder, strAdditionalFolder);
                localPath = Path.Combine(localPath, strFilename);

                lock (m_P4)
                {
                    m_P4.Connect();
                    m_P4.CWD = strOutputFolder;
                    recordSet = m_P4.Run(strCommand, GenerateCommands(strArgs, localPath));
                }
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf("chmod") != -1)
                {
                    lgLog.LogLine("\twarning:" + e.Message);
                }
                else
                {
                    throw e;
                }
            }

            if (recordSet != null && strCommand != "add" && strFilename != "...") // check for "add" so we dont go in a loop
            {
                // we fstat the file, if perforce says its all good then we then check if the head is delete. we can miss files if the file was in p4 and was deleted then needs adding.
                lgLog.LogLine("DEBUG: P4 " + "fstat" + ", " + strFullPath);

                P4RecordSet fstatRecordSet = null;

                lock (m_P4)
                {
                    fstatRecordSet = m_P4.Run("fstat", strFullPath);
                }

                if (fstatRecordSet != null)
                {
                    foreach (P4Record record in fstatRecordSet.Records)
                    {
                        if (record.Fields.ContainsKey("headAction"))
                        {
                            if (record.Fields["headAction"] == "delete")
                            {
                                string strAddArgs = "-tbinary+m";

                                if (strFilename.IndexOf(".cutxml") != -1 || strFilename.IndexOf(".txt") != -1)
                                {
                                    strAddArgs = "-ttext";
                                }

                                if (!RunP4Command("add", strOutputFolder, strAdditionalFolder, strFilename, "-f", strAddArgs))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }

                if (recordSet.HasWarnings() == true)
                {
                    foreach (string warning in recordSet.Warnings)
                    {
                        lgLog.LogLine("\twarning:" + warning);

                        if (warning.IndexOf("not on client") != -1 || warning.IndexOf("no such file") != -1)
                        {
                            string strAddArgs2 = "-tbinary+m";

                            if (strFilename.IndexOf(".cutxml") != -1 || strFilename.IndexOf(".txt") != -1)
                            {
                                strAddArgs2 = "-ttext";
                            }

                            if (!RunP4Command("add", strOutputFolder, strAdditionalFolder, strFilename, "-f", strAddArgs2))
                            {
                                return false;
                            }
                        }
                    }
                }

                if (recordSet.HasErrors() == true)
                {
                    foreach (string error in recordSet.Errors)
                    {
                        lgLog.LogLine("ERROR:" + error);
                    }

                    return false;
                }
            }

            return true;
        }

        private bool CreateOutputFolder(string strOutputFolder)
        {
            if (strOutputFolder == "")
            {
                lgLog.LogLine("Unable to process blank output directory.");
                return false;
            }

            if (!Directory.Exists(strOutputFolder))
            {
                try
                {
                    Directory.CreateDirectory(strOutputFolder);
                }
                catch(Exception /*e*/)
                {
                    lgLog.LogLine("Unable to create directory '" + strOutputFolder + "'");
                    return false;
                }
            }

            return true;
        }

        // Process a run, join all the entries into the final anim
        private bool ProcessRun(AnimRun arRun, string strOutputFolder)
        {
            LogRunOperations(arRun);

            if (arRun.Operations.Count == 2)
            {
                string strAnim = CreateInitialAnim((string)arRun.Operations[0], (string)arRun.Operations[1]);
                if (strAnim != String.Empty)
                {
                    if (arRun.AnimName.Contains("_face"))
                    {
                        if (!RunP4Command("edit", strOutputFolder, "faces\\", arRun.AnimName)) return false;
                        if(!CopyFile(strAnim, Path.Combine(strOutputFolder, "faces", arRun.AnimName))) return false;
                    }
                    else
                    {
                        if (!RunP4Command("edit", strOutputFolder, "", arRun.AnimName)) return false;
                        if (!CopyFile(strAnim, Path.Combine(strOutputFolder, arRun.AnimName))) return false;
                    }
                   
                    File.Delete(strAnim);
                }
            }
            else
            {
                string strAnim = CreateInitialAnim((string)arRun.Operations[0], (string)arRun.Operations[1]);
                if (strAnim != String.Empty)
                {
                    for (int i = 2; i < arRun.Operations.Count; ++i)
                    {
                        strAnim = AppendOperation(strAnim, arRun.Operations[i].ToString());
                        if (strAnim == String.Empty)
                        {
                            return true;
                        }
                    }

                    if (arRun.AnimName.Contains("_face"))
                    {
                        if (!RunP4Command("edit", strOutputFolder, "faces\\", arRun.AnimName)) return false;
                        if (!CopyFile(strAnim, Path.Combine(strOutputFolder, "faces", arRun.AnimName))) return false;
                    }
                    else
                    {
                        if (!RunP4Command("edit", strOutputFolder, "", arRun.AnimName)) return false;
                        if (!CopyFile(strAnim, Path.Combine(strOutputFolder, arRun.AnimName))) return false;
                    }

                    File.Delete(strAnim);
                }
            }
            
            return true;
        }

        // Create the initial anim, this is the first 2 entries in the run list, all entries from then on will just be appended
        private string CreateInitialAnim(string strOperation1, string strOperation2)
        {
            string strTempAnim = GetTempAnimFileName();

            if (RunEntryIsPadded(strOperation1) && !RunEntryIsPadded(strOperation2)) // Padding / Anim
            {
                string strAnimToProcess = strOperation2;

                string strOutput = String.Empty;
                if (RunProcess(strAnimConcatPath, "-anims=" + strAnimToProcess + " -leadin=" + strOperation1 + " -overlap=0 -out=" + strTempAnim + " –absolutemover -superset" + " -ulog -output", out strOutput, true))
                {
                    DeleteFileIfTemp(strAnimToProcess);
                    return strTempAnim;
                }

                DeleteFileIfTemp(strAnimToProcess);
                lgLog.LogLine(strOutput);
                return String.Empty;
            }
            else if (!RunEntryIsPadded(strOperation1) && RunEntryIsPadded(strOperation2)) // Anim / Padding
            {
                string strAnimToProcess = strOperation1;

                string strOutput = String.Empty;
                if (RunProcess(strAnimConcatPath, "-anims=" + strAnimToProcess + " -leadout=" + strOperation2 + " -overlap=0 -out=" + strTempAnim + " –absolutemover -superset" + " -ulog -output", out strOutput, true))
                {
                    DeleteFileIfTemp(strAnimToProcess);
                    return strTempAnim;
                }

                DeleteFileIfTemp(strAnimToProcess);
                lgLog.LogLine(strOutput);
                return String.Empty;
            }
            else if (!RunEntryIsPadded(strOperation1) && !RunEntryIsPadded(strOperation2)) // Anim / Anim
            {
                string strAnimToProcess1 = strOperation1;
                string strAnimToProcess2 = strOperation2;

                string strOutput = String.Empty;

                if (RunProcess(strAnimConcatPath, "-anims=" + strAnimToProcess1 + "," + strAnimToProcess2 + " -overlap=0 -out=" + strTempAnim + " –absolutemover -superset" + " -ulog -output", out strOutput, true))
                {
                    DeleteFileIfTemp(strAnimToProcess1);
                    DeleteFileIfTemp(strAnimToProcess2);
                    return strTempAnim;
                }

                DeleteFileIfTemp(strAnimToProcess1);
                DeleteFileIfTemp(strAnimToProcess2);
                lgLog.LogLine(strOutput);
                return String.Empty;
            }

            return String.Empty;
        }

        // Append an operation to the current run anim file, this should always be called after the inital anim has being created
        private string AppendOperation(string strCurrentAnim, string strOperation)
        {
            string strTempAnim = GetTempAnimFileName();

            double dResult=0;
            if (RunEntryIsPadded(strOperation, out dResult))
            {
                string strOutput = String.Empty;
                if (RunProcess(strAnimConcatPath, "-anims=" + strCurrentAnim + " -leadout=" + dResult.ToString() + " -overlap=0 -out=" + strTempAnim + " –absolutemover -superset" + " -ulog -output", out strOutput, true))
                {
                    DeleteFileIfTemp(strCurrentAnim);
                    return strTempAnim;
                }

                DeleteFileIfTemp(strCurrentAnim);
                lgLog.LogLine(strOutput);
                return String.Empty;
            }
            else
            {
                string strAnimToProcess = strOperation;

                string strOutput = String.Empty;
                if (RunProcess(strAnimConcatPath, "-anims=" + strCurrentAnim + "," + strAnimToProcess + " -overlap=0 -out=" + strTempAnim + " –absolutemover -superset" + " -ulog -output", out strOutput, true))
                {
                    DeleteFileIfTemp(strCurrentAnim);
                    DeleteFileIfTemp(strAnimToProcess);
                    return strTempAnim;
                }

                DeleteFileIfTemp(strCurrentAnim);
                DeleteFileIfTemp(strAnimToProcess);
                lgLog.LogLine(strOutput);
                return String.Empty;
            }
        }

        // Check if a run entry is a padding entry or not, this basicly trys to convert the entry to a double. If the conversion is successful then the 
        // entry is a padding as padding entries are just numbers as string
        private bool RunEntryIsPadded(string strRunEntry)
        {
            double dResult = 0;
            if (Double.TryParse(strRunEntry, out dResult))
            {
                return true;
            }

            return false;
        }

        // Check if a run entry is a padding entry or not, this basicly trys to convert the entry to a double. If the conversion is successful then the 
        // entry is a padding as padding entries are just numbers as string 
        // OVERLOADED - Passes the padding value out
        private bool RunEntryIsPadded(string strRunEntry, out double dPadding)
        {
            double dResult = 0;
            if (Double.TryParse(strRunEntry, out dResult))
            {
                dPadding = dResult;
                return true;
            }

            dPadding = 0;
            return false;
        }

        // Compress the runs down so if we have multiple padding we just compress into one
        private void CompressRuns()
        {
            for (int iRun = 0; iRun < lstRuns.Count; ++iRun)
            {
                AnimRun arRun = (AnimRun)lstRuns[iRun];
                CompressARun(arRun);
            }
        }

        // Compress a single run, this compresses the padding down into a single value if they are in a row
        // ie 2,3,4,anim would end up being 9,anim
        // This handles multiple compressions - ie 2,3,4,anim,2,3 would end up being 9,anim,5
        private void CompressARun(AnimRun arRun)
        {
            bool bDone=false;
                       
            while(bDone==false)
            {
                bool bReRun = false;
                for (int i = 0; i < arRun.Operations.Count; ++i)
                {
                    double dPadding = 0;
                    if (RunEntryIsPadded(arRun.Operations[i].ToString(), out dPadding))
                    {
                        if (i + 1 < arRun.Operations.Count)
                        {
                            double dPadding2 = 0;
                            if (RunEntryIsPadded(arRun.Operations[i + 1].ToString(), out dPadding2))
                            {
                                arRun.Operations[i] = (dPadding + dPadding2).ToString();
                                arRun.Operations.RemoveAt(i + 1);
                                bReRun = true;
                                continue;
                            }
                        }
                    }

                    if (bReRun == true)
                    {
                        continue;
                    }
                }

                if(bReRun == false)
                {
                    bDone = true;
                }
            } 
        }

        // Add an anim to the overall list of anims, discard any dupes
        private void AddAnimToList(string strAnim)
        {
            FileInfo fiAnim = new FileInfo(strAnim);

            for (int iAnim = 0; iAnim < lstEveryPossibleAnim.Count; ++iAnim)
            {
                if (String.Compare(fiAnim.Name, lstEveryPossibleAnim[iAnim].ToString(), true) == 0) return;
            }

            lstEveryPossibleAnim.Add(fiAnim.Name);
        }

        // Read the objects from the cut file, these will correspond to the anim names
        private ArrayList ValidateAnimNames(string strCutFile)
        {
            ArrayList lstValidAnims = new ArrayList();

            XmlTextReader textReader = new XmlTextReader(strCutFile);

            if (textReader.ReadToFollowing("pCutsceneObjects"))
            {
                XmlReader objectReader = textReader.ReadSubtree();

                while (objectReader.ReadToFollowing("cName"))
                {
                    string str = objectReader.ReadInnerXml();
                    int index = str.IndexOf(":");
                    if (index != -1)
                    {
                        string strName = str.Substring(0, index);
                        lstValidAnims.Add(strName);
                    }
                    else
                    {
                        lstValidAnims.Add(str);
                    }
                }
            }

            return lstValidAnims;
        }

        // Validate the anim vs the list of valid anims we retrieved from the cutfile
        private bool ValidateAnim(ArrayList lstValidAnims, string strAnimName)
        {
            FileInfo fiAnim = new FileInfo(strAnimName);

            foreach (string strValidAnim in lstValidAnims)
            {
                string replacedValidAnim = strValidAnim.Replace(" ", "_");
                if(String.Compare(replacedValidAnim, fiAnim.Name.Replace(fiAnim.Extension, ""), true) == 0)
                //if (replacedValidAnim.ToLower() == fiAnim.Name.Replace(fiAnim.Extension, "").ToLower())
                {
                    return true;
                }
            }

            return false;
        }

        // Do a check that all the required anim and clip files exist
        private bool ValidateAllFilesExist()
        {
            foreach (AnimConcatEntry entry in lstAnimConcatEntries)
            {
                foreach (string strAnim in entry.AnimFiles)
                {
                    if (!File.Exists(strAnim))
                    {
                        lgLog.LogLine("ERROR: Anim file does not exist: " + strAnim);
                        return false;
                    }
                }
            }

            foreach (ClipEntry entry in lstClipEntries)
            {
                foreach (string strClip in entry.ClipFiles)
                {
                    if (!File.Exists(strClip))
                    {
                        lgLog.LogLine("ERROR: Clip file does not exist: " + strClip);
                        return false;
                    }
                }
            }

            return true;
        }

        // Process each folder and create the concat entry lists and full anim lists
        public bool ProcessFolders(string[] folderList)
        {
            for (int iFolder = 0; iFolder < folderList.Length; ++iFolder)
            {
                if (folderList[iFolder].Replace(" ", "") == String.Empty) continue;

                lgLog.LogLine("Processing folder: " + folderList[iFolder]);

                AnimConcatEntry acEntry = new AnimConcatEntry();

                if (Directory.Exists(folderList[iFolder]) == false)
                {
                    lgLog.LogLine("ERROR:  Folder does not exist: " + folderList[iFolder] + ".");
                    return false;    
                }

                acEntry.CutFile = folderList[iFolder] + "\\data_stream.cutxml";

                if (File.Exists(acEntry.CutFile) == false)
                {
                    lgLog.LogLine("ERROR: Cutfile does not exist: " + acEntry.CutFile);
                    return false;
                }

                ArrayList lstValidAnims = ValidateAnimNames(acEntry.CutFile);

                string[] animList = Directory.GetFiles(folderList[iFolder], "*.anim");

                for (int iAnim = 0; iAnim < animList.Length; ++iAnim)
                {
                    if (!ValidateAnim(lstValidAnims, animList[iAnim]))
                    {
                        lgLog.LogLine("Warning: Found a rogue anim file, ignoring: " + animList[iAnim]);
                        continue;
                    }

                    acEntry.AddAnim(animList[iAnim]);
                    AddAnimToList(animList[iAnim]);
                    AddClip(animList[iAnim]);
                }

                // face anims
                if (Directory.Exists(folderList[iFolder] + "/faces"))
                {
                    string[] faceAnimList = Directory.GetFiles(folderList[iFolder] + "/faces", "*.anim");

                    for (int iAnim = 0; iAnim < faceAnimList.Length; ++iAnim)
                    {
                        acEntry.AddAnim(faceAnimList[iAnim]);
                        AddAnimToList(faceAnimList[iAnim]);
                        AddClip(faceAnimList[iAnim]);
                    }
                }

                lstAnimConcatEntries.Add(acEntry);
            }

            return ValidateAllFilesExist();
        }

        // Create runs from the data, a run is what will make up an output anim
        // ie. A run could be 2,anim,3,4. This would create an anim at the end which is anim but with 2 seconds padding at the front and 7 at the end.
        private void ProcessIntoRuns()
        {
            for (int iAnim = 0; iAnim < lstEveryPossibleAnim.Count; ++iAnim) // Each anim
            {
                AnimRun arRun = new AnimRun();

                for (int iEntry = 0; iEntry < lstAnimConcatEntries.Count; ++iEntry) // Each folder
                {
                    bool bExistsInEntry = false;

                    AnimConcatEntry acEntry = (AnimConcatEntry)lstAnimConcatEntries[iEntry];
                    for (int iEntryAnim = 0; iEntryAnim < acEntry.AnimFiles.Count; ++iEntryAnim)
                    {
                        FileInfo fiAnim = new FileInfo(acEntry.AnimFiles[iEntryAnim].ToString());

                        if (String.Compare(fiAnim.Name, lstEveryPossibleAnim[iAnim].ToString(), true) == 0)
                        {
                            bExistsInEntry = true;
                            arRun.Operations.Add(acEntry.AnimFiles[iEntryAnim].ToString()); // Add operation anim
                            arRun.AnimName = fiAnim.Name;
                            break;
                        }
                    }

                    if (!bExistsInEntry)
                    {
                        double fDuration = 0;
                        if (GetDurationFromCutFile(acEntry.CutFile, out fDuration))
                        {
                            arRun.Operations.Add(fDuration.ToString()); // Add operation padding
                        }
                        else
                        {
                            lgLog.LogLine("ERROR: Unable to retrieve duration from cutfile: " + acEntry.CutFile);
                            return;
                        }
                    }
                }

                lstRuns.Add(arRun);
            }
        }

        // Store lists of the clips we need to concat
        private void AddClip(string strAnimFile)
        {
            for (int i = 0; i < lstClipEntries.Count; ++i)
            {
                ClipEntry ceClip = (ClipEntry)lstClipEntries[i];

                if (String.Compare((Path.GetFileNameWithoutExtension(strAnimFile) + ".clip"), ceClip.ClipName, true) == 0)
                {
                    ceClip.ClipFiles.Add(Path.GetDirectoryName(strAnimFile) + "\\" + Path.GetFileNameWithoutExtension(strAnimFile) + ".clip");
                    return;
                }
            }

            ClipEntry ceClipEntry = new ClipEntry();
            ceClipEntry.ClipName = Path.GetFileNameWithoutExtension(strAnimFile) + ".clip";
            ceClipEntry.ClipFiles.Add(Path.GetDirectoryName(strAnimFile) + "\\" + Path.GetFileNameWithoutExtension(strAnimFile) + ".clip");
            lstClipEntries.Add(ceClipEntry);
        }

        // Add additional padding entries if a clip is missing (used to add blocking tags)
        private void ProcessClipEntry(ClipEntry ceClip)
        {
            ArrayList alNewClipList = new ArrayList();

            for (int iEntry = 0; iEntry < lstAnimConcatEntries.Count; ++iEntry) // Each folder
            {
                AnimConcatEntry acEntry = (AnimConcatEntry)lstAnimConcatEntries[iEntry];

                // Get the dir, look to see if there is a clip entry for this dir, if not enter a padded entry
                FileInfo fiFile = new FileInfo(acEntry.CutFile);

                bool bFoundEntry = false;
                for (int iClipEntry = 0; iClipEntry < ceClip.ClipFiles.Count; ++iClipEntry)
                {
                    string strClipFullPath = (string)ceClip.ClipFiles[iClipEntry];

                    if ( strClipFullPath.Contains(fiFile.DirectoryName + Path.DirectorySeparatorChar ) ||
                         strClipFullPath.Contains(fiFile.DirectoryName + Path.AltDirectorySeparatorChar ) )
                    {
                        alNewClipList.Add(ceClip.ClipFiles[iClipEntry]);
                        bFoundEntry = true;
                        break;
                    }
                }

                if (!bFoundEntry)
                {
                    double fDuration = 0;
                    if (GetDurationFromCutFile(acEntry.CutFile, out fDuration))
                    {
                        alNewClipList.Add(fDuration.ToString()); // Add operation padding
                    }
                    else
                    {
                        lgLog.LogLine("ERROR: Unable to retrieve duration from cutfile: " + acEntry.CutFile);
                    }
                }
            }
            
            ceClip.ClipFiles = alNewClipList;
        }

        private int ConcatClip(ClipEntry ceClip, string strOutputFolder)
        {
            ProcessClipEntry(ceClip);

            ArrayList lstClipCrops = new ArrayList();

            string strArguments = "-clips=";
            for (int iClipFile = 0; iClipFile < ceClip.ClipFiles.Count; ++iClipFile)
            {
                string strClipToProcess = (string)ceClip.ClipFiles[iClipFile];
                lstClipCrops.Add(strClipToProcess);
                strArguments += strClipToProcess;
                if (iClipFile != ceClip.ClipFiles.Count - 1) { strArguments += ","; }
            }

            if (ceClip.ClipName.Contains("_face"))
            {
                if (!RunP4Command("edit", strOutputFolder, "faces/", ceClip.ClipName)) return -1;
                strArguments += (" -out=" + Path.Combine(strOutputFolder, "faces", ceClip.ClipName) + " -ulog -output");
            }
            else
            {
                if (!RunP4Command("edit", strOutputFolder, "", ceClip.ClipName)) return -1;
                strArguments += (" -out=" + Path.Combine(strOutputFolder, ceClip.ClipName) + " -ulog -output");
            }
                
            string strOutput = String.Empty;

            lgLog.LogLine("Concatting clips for: " + ceClip.ClipName);

            if (!RunProcess(strClipConcatPath, strArguments, out strOutput, true))
            {
                for (int i = 0; i < lstClipCrops.Count; ++i)
                {
                    DeleteFileIfTemp((string)lstClipCrops[i]);
                }

                lgLog.LogLine(strOutput);
                return -1;
            }

            for (int i = 0; i < lstClipCrops.Count; ++i)
            {
                DeleteFileIfTemp((string)lstClipCrops[i]);
            }

            return 0;
        }

        // Concat the clips - this will automatically use the concatted anim within the directory to mod the duration
        private int ConcatClips(string strOutputFolder)
        {
            for (int iClip = 0; iClip < lstClipEntries.Count; ++iClip)
            {
                ClipEntry ceClip = (ClipEntry)lstClipEntries[iClip];
                ConcatClip(ceClip, strOutputFolder);
            }

            return 0;
        }

        // Concat the cutxmls - this will concat them and copy them for use with the ruby cutscene script, cutfconcat creates redundant files so we prune these out at the end
        private int ConcatCutXML(string[] folderList, string strOutputFolder, string strAudio, int iSectionMethod, int iSectionDuration)
        {
            string strTempCutFilename = Path.GetTempFileName();
            TextWriter twCutFileList = new StreamWriter(strTempCutFilename);

            // Create the cutfile list which is used by the cutfconcat tool
            for (int iFolder = 0; iFolder < folderList.Length; ++iFolder)
            {
                twCutFileList.WriteLine(folderList[iFolder] + "\\data_stream.cutxml");
            }

            twCutFileList.Close();

            if (!RunP4Command("edit", strOutputFolder, "", "data.cutxml")) return -1;

            string strOutput = String.Empty;
            if (!RunProcess(strCutXMLConcatPath, "-cutlistFile=" + strTempCutFilename + " -outputDir=" + strOutputFolder + " -audioFile=" + strAudio + " -ulog -output -hidealloc -external -sectionMethod=" + iSectionMethod.ToString() + " -sectionDuration=" + iSectionDuration.ToString(), out strOutput, true))
            {
                lgLog.LogLine(strCutXMLConcatPath + " -cutlistFile=" + strTempCutFilename + " -outputDir=" + strOutputFolder + " -ulog -output");
                lgLog.LogLine(strOutput);
                lgLog.LogLine("Deleting file: " + strTempCutFilename);
                File.Delete(strTempCutFilename); // Delete the temp file
                return -1;
            }

            File.Delete(strTempCutFilename); // Delete the temp file

            if (!RunP4Command("edit", strOutputFolder, "", "data_stream.cutxml")) return -1;

            if(File.Exists(strOutputFolder + "data_stream.cutxml"))
            {
                if ((File.GetAttributes(strOutputFolder + "data_stream.cutxml") & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    lgLog.LogLine("ERROR: File is read-only: " + strOutputFolder + "data_stream.cutxml");
                    return -1;
                }

                File.Delete(strOutputFolder + "data_stream.cutxml");
            }

            File.Copy(strOutputFolder + "data.cutxml", strOutputFolder + "data_stream.cutxml");

            // Prune out the redundant files, we only want data.cutxml and section.cutxml
            if (Directory.Exists(strOutputFolder) == true)
            {
                string[] cutxmlFiles = Directory.GetFiles(strOutputFolder, "*.cutxml");
                for (int i = 0; i < cutxmlFiles.Length; ++i)
                {
                    if (Path.GetFileNameWithoutExtension(cutxmlFiles[i]) != "data" &&
                        Path.GetFileNameWithoutExtension(cutxmlFiles[i]) != "data_stream")
                    {
                        File.Delete(cutxmlFiles[i]);
                    }
                }
            }

            return 0;
        }

        private void PerformMarkForDelete(string strPath)
        {
            ArrayList lstFiles = new ArrayList();
            foreach (string file in Directory.GetFiles(strPath))
            {
                lstFiles.Add(Path.GetFileName(file));
            }

            for (int i = 0; i < lstEveryPossibleAnim.Count; ++i)
            {
                if (lstEveryPossibleAnim[i].ToString().Contains(".anim"))
                {
                    lstEveryPossibleAnim.Add(lstEveryPossibleAnim[i].ToString().Replace(".anim", ".clip"));
                }
            }

            foreach (string file1 in lstFiles)
            {
                if (!lstEveryPossibleAnim.Contains(file1))
                {
                    if(!file1.Contains(".cutsub") && !file1.Contains(".lightxml"))
                        RunP4Command("delete", strPath, "", file1);
                }
            }
        }

        #region Helper Functions

        // Create a temp anim filename with path, this file doesnt exist
        private string GetTempAnimFileName()
        {
            string strFilename = TempFileNamePrefix;
            lock (m_FileIdentifierLock)
            {
                strFilename = UniqueFileIdentifier.ToString("D5");
            }

            return System.IO.Path.GetTempPath() + Path.GetFileNameWithoutExtension(strFilename) + ".anim";
        }

        // Create a temp anim filename with path, this file doesnt exist
        private string GetTempClipFileName()
        {
            string strFilename = TempFileNamePrefix;
            lock (m_FileIdentifierLock)
            {
                strFilename = UniqueFileIdentifier.ToString("D5");
            }
            return System.IO.Path.GetTempPath() + Path.GetFileNameWithoutExtension(strFilename) + ".clip";
        }

        private void DeleteAllTempFiles()
        {
            try
            {
                string[] files = Directory.GetFiles(System.IO.Path.GetTempPath(), TempFileNamePrefix + "*.*");
                foreach(string file in files)
                {
                    try
                    {
                        File.SetAttributes(file, FileAttributes.Normal);
                        File.Delete(file);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch(Exception)
            {

            }
        }

        // We query the cut file for the duration of the scene
        private bool GetDurationFromCutFile(string strCutFile, out double fDuration)
        {
            string strLine = String.Empty;
            System.IO.StreamReader srFile = new System.IO.StreamReader(strCutFile);

            while ((strLine = srFile.ReadLine()) != null)
            {
                if (strLine.Contains("fTotalDuration"))
                {
                    int index = strLine.IndexOf("\"");
                    int index2 = strLine.IndexOf("\"", index + 1);
                    string strRangeStart = strLine.Substring(index + 1, index2 - index - 1);

                    double dResult = 0;
                    if (Double.TryParse(strRangeStart, out dResult))
                    {
                        fDuration = (float)dResult;
                        return true;
                    }
                }
            }

            fDuration = 0;
            return false;
        }

        // Copy a file from one location to another
        // If the file exists and is not read-only then stomp it
        // If the file exists and is read-only then error
        private bool CopyFile(string strLocation1, string strLocation2)
        {
            FileInfo fiFile = new FileInfo(strLocation2);
            if (!Directory.Exists(fiFile.DirectoryName))
            {
                Directory.CreateDirectory(fiFile.DirectoryName);
            }

            strLocation2.Replace("\"", "");

            lgLog.LogLine("Copying file from " + strLocation1 + " to " + strLocation2);

            if (File.Exists(strLocation2))
            {
                if ((File.GetAttributes(strLocation2) & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
                {
                    File.Delete(strLocation2);
                    File.Copy(strLocation1, strLocation2);
                }
                else
                {
                    lgLog.LogLine("ERROR: File is read-only: " + strLocation2);
                    return false;
                }
            }
            else
            {
                File.Copy(strLocation1, strLocation2);
            }

            return true;
        }

        private void DeleteFileIfTemp(string strFile)
        {
            if (strFile.Contains(Path.GetTempPath()))
            {
                while (File.Exists(strFile))
                {
                    try
                    {
                        File.Delete(strFile);
                    }
                    catch (Exception) { }
                }
            }
        }

        // Run the animconcat process to join our anims or padding
        private bool RunProcess(string strExe, string strArguments, out string strOutput, bool bWaitForExit)
        {
            try
            {
                System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                pExe.StartInfo.FileName = strExe;
                pExe.StartInfo.Arguments = strArguments;
                pExe.StartInfo.CreateNoWindow = true;
                pExe.StartInfo.UseShellExecute = false;
                pExe.StartInfo.RedirectStandardOutput = true;
                pExe.Start();

                lgLog.LogLine("DEBUG: " + strExe + " " + strArguments);

                strOutput = String.Empty;
                if (bWaitForExit)
                {
                    strOutput = pExe.StandardOutput.ReadToEnd();
                    pExe.WaitForExit();

                    if (pExe.ExitCode != 0)
                    {
                        lgLog.LogLine("ERROR: Executing " + strExe + " " + strArguments + "\n" + strOutput);
                        return false;
                    }
                }

                return true;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                lgLog.LogLine("ERROR: " + e.Message + " :" + strExe + " " + strArguments);
            }

            strOutput = String.Empty;
            return false;
        }

        private void LogRunOperations(AnimRun arRun)
        {
            for (int iOperation = 0; iOperation < arRun.Operations.Count; ++iOperation)
            {
                lgLog.LogLine("\t" + arRun.Operations[iOperation].ToString());
            }
        }

        private void PrintCommandLine(string[] folderList, string strOutputFolder)
        {
            string strFolders = String.Empty;

            for( int i=0; i < folderList.Length; ++i)
            {
                strFolders += folderList[i];

                if (i != folderList.Length - 1)
                {
                    strFolders += ",";
                }
            }

            lgLog.LogLine("animconcatdriver.exe " + strFolders + " " + strOutputFolder + "\r\n");
        }

        #endregion
    }
}
