#include <string>

class TimeCode
{
public:
	virtual ~TimeCode(){}
	atString m_Name;
	u32 m_Hour;
	u32 m_Minute;
	u32 m_Second;
	u32 m_Frame;

	PAR_PARSABLE;
};

class Reel
{
public:
	virtual ~Reel(){}
	s32 m_Index;
	atString m_EditName;
	atString m_ShotName;
	atString m_AudioOrVideo;
	atArray<TimeCode> m_SourceTimeCode;
	//TimeCode m_SourceTimeCodeIn;
	//TimeCode m_SourceTimeCodeOut;
	TimeCode m_EditTimeCodeIn;
	TimeCode m_EditTimeCodeOut;
	s32 m_RangeStart;
	s32 m_RangeEnd;

	PAR_PARSABLE;
};

class ShotDef
{
public:
	virtual ~ShotDef(){}
	atString m_guid;
	atString m_CutsceneName;
	atArray<Reel> m_ReelList;

	PAR_PARSABLE;
};