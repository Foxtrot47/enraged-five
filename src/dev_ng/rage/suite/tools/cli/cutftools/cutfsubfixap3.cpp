// 
// /cutfstream.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"

#include "RsULog/ULogger.h"

using namespace rage;

REQ_PARAM( cutSub, "[cutfsubfix] The cutf file from subtitle tool." );
REQ_PARAM( cutFile, "[cutfsubfix] The cutf file to merge into." );
REQ_PARAM( outputCutfile, "[cutfsubfix] The cutf file to merge into." );
PARAM( hideAlloc, "[cutfsubfix] Hides the allocation spam in the log." );
PARAM( ulog, "[cutfsubfix] Universal logging." );

void Init()
{
	INIT_PARSER;
	crAnimation::InitClass();
	crClip::InitClass();
	cutfEvent::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("cutfsubfix").c_str(), "cutfsubfix");
	}
}

void Shutdown()
{
	cutfEvent::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();        
	SHUTDOWN_PARSER;

	ULOGGER.PostExport();
}

int Main()
{
	// Subtitle files contain loads of objects, one for each speaker. We don't need these, so we strip them and just have one object.
	// They also contain CUTSCENE_SHOW_SUBTITLE_EVENT/CUTSCENE_HIDE_SUBTITLE_EVENT subtitle events, we don't need the hide events so we remove 
	// them and calculate a duration and add this to the event args of the SHOW event.
	// We then merge the new data into an existing cutfile.

	Init();

	const char *pCutSubFilename;
	if(!PARAM_cutSub.Get( pCutSubFilename ))
	{
		Shutdown();
		return -1;
	}

	const char *pCutFilename;
	if(!PARAM_cutFile.Get( pCutFilename ))
	{
		Shutdown();
		return -1;
	}

	const char *pOutputCutFilename;
	if(!PARAM_outputCutfile.Get( pOutputCutFilename ))
	{
		Shutdown();
		return -1;
	}
	
	// Load the cutsub file 
	cutfCutsceneFile2* pCutSubfile = rage_new cutfCutsceneFile2();
	if (!LoadCutfile( pCutSubFilename, *(pCutSubfile) ) )
	{
		ULOGGER.SetProgressMessage("ERROR: Unable to open cutsub file: %s", pCutSubFilename);

		Shutdown();
		return -1;
	} 

	// Load the cut file 
	cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();
	if (!LoadCutfile( pCutFilename, *(pCutfile) ) )
	{
		ULOGGER.SetProgressMessage("ERROR: Unable to open cut file: %s", pCutFilename);

		Shutdown();
		return -1;
	}

	atArray<cutfObject*> atSubObjects;
	pCutSubfile->FindObjectsOfType(CUTSCENE_SUBTITLE_OBJECT_TYPE, atSubObjects);

	if(atSubObjects.GetCount() != 1)
	{
		return -1;
	}

	if(pCutSubfile->GetLoadEventList().GetCount() != 1)
	{
		return -1;
	}

	atArray<cutfEvent*> eventList;
	pCutSubfile->FindEventsForObjectIdOnly(atSubObjects[0]->GetObjectId(), pCutSubfile->GetEventList(), eventList);

	pCutfile->AddObject(atSubObjects[0]);

	for(int i=0 ; i < eventList.GetCount(); ++i)
	{
		cutfObjectIdEvent* pObjectIdEvent = dynamic_cast<cutfObjectIdEvent*>(eventList[i]);
		if(pObjectIdEvent)
		{
			pObjectIdEvent->SetObjectId(atSubObjects[0]->GetObjectId());
			pCutfile->AddEvent(pObjectIdEvent);
		}
	}

	cutfFinalNameEventArgs* pNameArgs = rage_new cutfFinalNameEventArgs(atSubObjects[0]->GetDisplayName());

	pCutSubfile->GetLoadEventList()[0]->SetEventArgs(pNameArgs);
	
	pCutfile->AddLoadEvent(pCutSubfile->GetLoadEventList()[0]);

	pCutfile->SortEvents();
	pCutfile->SortLoadEvents();
	if(!pCutfile->SaveFile(pOutputCutFilename))
	{
		ULOGGER.SetProgressMessage("Unable to save file: %s", pOutputCutFilename);
		return -1;
	}

	Shutdown();

    return 0;
}
