// 
// /cutfanimcombine.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE: The purpose of this tool is to generate the anim_combine.targets file that is used
//  to merge the face and body animations of ped models.

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "crclip/clipanimation.h"
#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/param.h"
#include "rexBase/animExportCtrl.h"
#include "system/exec.h"

using namespace rage;

REQ_PARAM( cutlistFile, "[cutfmerge] The file with the list of cutscenes to concatenate, one per line:  inputDir inputCutfile inputFaceDir (separated by tabs, only inputDir is required)." );
REQ_PARAM( outputFile, "[cutfmerge] The file to output too." );

void Init()
{
	INIT_PARSER;
	crAnimation::InitClass();
	crClip::InitClass();
	cutfEvent::InitClass();
}

void Shutdown()
{
	SHUTDOWN_PARSER;
	cutfEvent::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();        
}

int Main()
{
    Init();

	const char *pCutlistFilename;
	PARAM_cutlistFile.Get( pCutlistFilename );

	const char *pOutputFilename;
	PARAM_outputFile.Get( pOutputFilename );

	// Load the cutlist
	atArray<SCutsceneInputItem> cutsceneInputItemList;
	if ( !LoadCutListFile( pCutlistFilename, cutsceneInputItemList ) )
	{
		Shutdown();
		return -1;
	}

	for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
	{
		cutsceneInputItemList[i].pCutfile = rage_new cutfCutsceneFile2();

		if ( !LoadCutfile( cutsceneInputItemList[i].cutFilename.c_str(), *(cutsceneInputItemList[i].pCutfile) ) )
		{
			for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
			{
				if ( cutsceneInputItemList[i].pCutfile != NULL )
				{
					delete cutsceneInputItemList[i].pCutfile;
				}
			}

			Shutdown();
			return -1;
		} 
	}

	for ( int i = 1; i < cutsceneInputItemList.GetCount(); ++i )
	{
		if(cutsceneInputItemList[0].pCutfile->GetTotalDuration() != cutsceneInputItemList[i].pCutfile->GetTotalDuration())
		{
			char msg[RAGE_MAX_PATH];
			sprintf(msg, "Error: Files have differing durations '%s' / '%s'", cutsceneInputItemList[0].cutFilename.c_str(), cutsceneInputItemList[i].cutFilename.c_str() );
			Errorf(msg);
			return -1;
		}
	}

	cutfCutsceneFile2 *pMergedCutFile = cutsceneInputItemList[0].pCutfile->Clone();
	for ( int i = 1; i < cutsceneInputItemList.GetCount(); ++i )
	{
		pMergedCutFile->MergeFrom(*cutsceneInputItemList[i].pCutfile, false);
	}

	pMergedCutFile->SaveFile(pOutputFilename);

    Shutdown();
    return 0;
}
