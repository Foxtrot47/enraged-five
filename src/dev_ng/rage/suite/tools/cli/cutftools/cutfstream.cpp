// 
// /cutfstream.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"
#include "system/xtl.h"

#include "RsULog/ULogger.h"

#include <fstream>
#include <set>
#include <iostream>

using namespace rage;

REQ_PARAM( cutFile, "[cutfstream] The cutf file to modify." );
REQ_PARAM( inputDir, "[cutfstream] The location where the streaming box memory logs live." );
REQ_PARAM( destDir, "[cutfstream] The location of the cutscene assets." );
PARAM( timeBuffer, "[cutfstream] The buffer in which to remove from each time entry. So the item will stream in time - buffer.");
PARAM( ulog, "[cutfstream] Universal logging." );

int TIME_BUFFER = 0; // buffer to stream

struct SModelInfo
{
	atArray<float> lstLoadTimes;
	atArray<float> lstDropTimes;
	atString strName;
};

struct TimeEntry
{
	bool bLoad;
	float fValue;

	bool operator<(const TimeEntry &rhs) const
	{
		if(this->fValue < rhs.fValue) return true;

		return false;
	}
};

void Init()
{
	INIT_PARSER;
	crAnimation::InitClass();
	crClip::InitClass();
	cutfEvent::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("cutfstream").c_str(), "cutfstream");
	}
}

void Shutdown()
{
	cutfEvent::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();        
	SHUTDOWN_PARSER;

	ULOGGER.PostExport();
}

void FindFirstFileAll(const fiFindData &data, void *userArg)
{
	atArray<atString>& FileList = *((atArray<atString>*)userArg);
	FileList.PushAndGrow( atString(data.m_Name) );
}

bool RetrieveStreamingBoxFiles(const char* pPath, atArray<atString>& FileList)
{
	if(!pPath) return false;

	ASSET.EnumFiles( pPath, FindFirstFileAll, &FileList );

	return (FileList.GetCount() > 0);
}

void GatherModelInfo(const char* pInputDir, const atArray<atString>& FileList, atArray<SModelInfo>& lstModel, cutfCutsceneFile2* pCutFile)
{
	for( int uiFile=0; uiFile < FileList.GetCount(); ++uiFile)
	{
		char fullPath[RAGE_MAX_PATH];
		sprintf(fullPath, "%s/%s", pInputDir, FileList[uiFile].c_str());
		
		SModelInfo model;
		model.strName = atString(FileList[uiFile].c_str());

		int dotPosition = model.strName.LastIndexOf(".");
		model.strName[dotPosition] = 0;
		
		bool bTerminate = false;
		fiStream* pFile = fiStream::Open(fullPath);
		if(pFile)
		{
			char czLine[RAGE_MAX_PATH];
			while(bTerminate == false && fgetline(czLine, RAGE_MAX_PATH, pFile))
			{
				if(stricmp("[Loaded]", czLine) == 0)
				{
					czLine[0] = 0;
					while(fgetline(czLine, RAGE_MAX_PATH, pFile))
					{
						if(stricmp("[Dropped]", czLine) == 0)
						{
							break;
						}
						else if(stricmp("", czLine) != 0)
						{
							int frame = atoi(czLine);
							float fTime = (float)frame / CUTSCENE_FPS;
							if(fTime > TIME_BUFFER)
							{
								fTime -= TIME_BUFFER;
							}

							model.lstLoadTimes.Grow() = fTime;
						}
						else
						{
							break;
						}
					}
					
					bTerminate=true;
					pFile->Close();
					pFile = NULL;
				}
			}
		}

		bTerminate=false;
		pFile = fiStream::Open(fullPath);
		if(pFile)
		{
			char czLine[RAGE_MAX_PATH];
			while(bTerminate == false && fgetline(czLine, RAGE_MAX_PATH, pFile))
			{
				if(stricmp("[Dropped]", czLine) == 0)
				{
					czLine[0] = 0;
					while(fgetline(czLine, RAGE_MAX_PATH, pFile))
					{
						if(stricmp("", czLine) != 0)
						{
							int frame = atoi(czLine);
							if(frame != 0) // we don't want drop frames at 0
							{
								float fTime = (float)frame / CUTSCENE_FPS;

								model.lstDropTimes.Grow() = fTime;
							}
						}
						else
						{
							break;
						}
					}

					bTerminate=true;
					pFile->Close();
					pFile = NULL;
				}
			}
		}

		// FIX_MIKE : End range stays the same but we want to set the duration to include the last frame so we get example. 10-1200 as 1901 frames
		if(model.lstDropTimes.GetCount() == 0)
			model.lstDropTimes.Grow() = (float)(pCutFile->GetRangeEnd() + 1) / CUTSCENE_FPS;
		lstModel.Grow() = model;
	}
}

int Compare( const TimeEntry* pObject1, const TimeEntry* pObject2 )
{
	if ( pObject1->fValue > pObject2->fValue) return 1;

	return 0;
}

void StripModelLists(SModelInfo& model)
{
	// Merge the load and drop lists together

	atArray<TimeEntry> lstMerged;

	for(int i=0; i < model.lstLoadTimes.GetCount(); ++i)
	{
		TimeEntry entry;
		entry.bLoad=true;
		entry.fValue = model.lstLoadTimes[i];
		lstMerged.Grow() = entry;
	}

	for(int i=0; i < model.lstDropTimes.GetCount(); ++i)
	{
		TimeEntry entry;
		entry.bLoad=false;
		entry.fValue = model.lstDropTimes[i];
		lstMerged.Grow() = entry;
	}

	lstMerged.QSort(0,-1,&Compare);

	// Put the list in a set to remove duplicates
	std::set<TimeEntry> setMerged(lstMerged.GetElements(), lstMerged.GetElements()+lstMerged.GetCount());

	// Put the set back into a list
	lstMerged.Reset();
	for(std::set<TimeEntry>::iterator itr = setMerged.begin(); itr != setMerged.end(); ++itr)
	{
		lstMerged.Grow() = *itr;
	}

	// Do two passes on the list, removing unwanted loads and drops

	for(int i=0; i < lstMerged.GetCount(); ++i)
	{
		if(lstMerged[i].bLoad)
		{
			if(i+1 < lstMerged.GetCount())
			{
				if(lstMerged[i+1].bLoad)
				{
					lstMerged.Delete(i+1);
					i=-1;
				}
			}
		}
	}

	// Loop backwards, keep the latest time for the model to unload
	for(int i=lstMerged.GetCount()-1; i >= 0; --i)
	{
		if(!lstMerged[i].bLoad)
		{
			if(i-1 >= 0)
			{
				if(!lstMerged[i-1].bLoad)
				{
					lstMerged.Delete(i-1);
					i=lstMerged.GetCount();
				}
			}
		}
	}

	model.lstDropTimes.Reset();
	model.lstLoadTimes.Reset();

	// split the lists again and add them to the model itself
	for(int i=0; i < lstMerged.GetCount(); ++i)
	{
		if(lstMerged[i].bLoad)
		{
			model.lstLoadTimes.Grow() = lstMerged[i].fValue;
		}
		else
		{
			model.lstDropTimes.Grow() = lstMerged[i].fValue;
		}
	}
}

// Models with ^ in the name are duplicates, they are different instances of the same model
void CheckForDupeModels(atArray<SModelInfo>& lstModel)
{
	atArray<SModelInfo> lstModifiedModel;

	// Loop the models and find the originals, then find any dupes of this model
	for(int i=0; i < lstModel.GetCount(); ++i)
	{
		SModelInfo currentModel = lstModel[i];

		atArray<atString> lstDupeNames;

		if(currentModel.strName.IndexOf("^") != -1) continue;

		// Look for any dupes related to this model
		for(int j=0; j < lstModel.GetCount(); ++j)
		{
			if(currentModel.strName == lstModel[j].strName) continue;

			std::string strDupeCheck = currentModel.strName.c_str();
			strDupeCheck += "^";

			if(lstModel[j].strName.IndexOf(strDupeCheck.c_str()) != -1) // found a dupe of this model
			{
				// Store the dupe names, we are going to add all the dupe load/unload times into the original model
				// We will then replicate this model and change the name so for example model1 will be model1^1 etc.
				atString strModelName = lstModel[j].strName;
				lstDupeNames.Grow() = strModelName;

				for( int k=0; k < lstModel[j].lstDropTimes.GetCount(); ++k)
				{
					currentModel.lstDropTimes.Grow() = lstModel[j].lstDropTimes[k];
				}

				for( int k=0; k < lstModel[j].lstLoadTimes.GetCount(); ++k)
				{
					currentModel.lstLoadTimes.Grow() = lstModel[j].lstLoadTimes[k];
				}
			}
		}

		StripModelLists(currentModel);

		lstModifiedModel.Grow() = currentModel;

		for(int j=0; j < lstDupeNames.GetCount(); ++j)
		{
			SModelInfo model = currentModel;
			model.strName = lstDupeNames[j];
			lstModifiedModel.Grow() = model;
		}
	}

	lstModel = lstModifiedModel;
}

// If the process is run on a processed cutxml we don't want to re-add the events
bool DoesEventExist(cutfObjectIdEvent* pEvent, cutfCutsceneFile2* pCutfile)
{
	const atArray<cutfEvent *>& lstEvents = pCutfile->GetLoadEventList();

	for(int i=0; i < lstEvents.GetCount(); ++i)
	{
		if(lstEvents[i]->GetEventId() == CUTSCENE_LOAD_MODELS_EVENT || lstEvents[i]->GetEventId() == CUTSCENE_UNLOAD_MODELS_EVENT)
		{
			cutfObjectIdEvent* pEventCompare = dynamic_cast<cutfObjectIdEvent*>(lstEvents[i]);
			if(pEventCompare)
			{
				if(*pEvent == *pEventCompare) return true;
			}

		}
	}

	return false;
}

bool CreateLoadAndUnloadLists(cutfCutsceneFile2* pCutfile, const atArray<SModelInfo>& ModelInfo)
{
	const atArray<cutfObject*>& lstObjects = pCutfile->GetObjectList();

	for(int uiModel=0; uiModel < ModelInfo.GetCount(); ++uiModel)
	{
		SModelInfo model = ModelInfo[uiModel];
		s32 iObjectID = -1;

		for(int uiObject=0; uiObject < lstObjects.GetCount(); ++uiObject)
		{
			cutfModelObject* pModelObject = dynamic_cast<cutfModelObject*>(lstObjects[uiObject]);
			if(pModelObject == NULL) continue;

			atString strModelName(model.strName.c_str());
			atString strModelName2(lstObjects[uiObject]->GetDisplayName());
			strModelName.Trim();
			strModelName2.Trim();

			if(strcmpi(strModelName.c_str(), strModelName2.c_str()) == 0)
			{
				iObjectID = lstObjects[uiObject]->GetObjectId();
			}
		}

		if(iObjectID == -1)
		{
			ULOGGER.SetProgressMessage("ERROR: ObjectID is -1, %s not found.", model.strName.c_str());
			return false;
		}

		// Look through the current event args within the cutfile for an arg matching the one we need, if it exists then re-use it,
		// otherwise create a new one. Attach it to the load model event.
		for(int uiLoadTime=0; uiLoadTime < model.lstLoadTimes.GetCount(); ++uiLoadTime)
		{
			cutfObjectIdEvent* pEvent = rage_new cutfObjectIdEvent(0, model.lstLoadTimes[uiLoadTime], CUTSCENE_LOAD_MODELS_EVENT);

			bool bFound = false;
			for(int uiEventArg=0; uiEventArg < pCutfile->GetEventArgsList().GetCount(); ++uiEventArg)
			{
				cutfEventArgs* pEventArgs = pCutfile->GetEventArgsList()[uiEventArg];

				if(pEventArgs->GetType() == CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE)
				{
					cutfObjectIdListEventArgs* pNamedEventArgs = dynamic_cast<cutfObjectIdListEventArgs*>(pEventArgs);
					if(pNamedEventArgs != NULL)
					{
						const atArray<rage::s32>& lstObjectIDs = pNamedEventArgs->GetObjectIdList();
						if(lstObjectIDs.GetCount() > 0)
						{
							if(lstObjectIDs[0] == iObjectID)
							{
								pEvent->SetEventArgs(pEventArgs);
								bFound=true;
								break;
							}
						}
					}
				}
			}

			if(!bFound)
			{
				atArray<s32> objectIDList;
				objectIDList.Grow() = iObjectID;
				cutfObjectIdListEventArgs* pEventArgs = rage_new cutfObjectIdListEventArgs(objectIDList);

				pEvent->SetEventArgs(pEventArgs);
			}

			if(!DoesEventExist(pEvent, pCutfile))
			{
				pCutfile->AddLoadEvent(pEvent);
			}
		}

		// Look through the current event args within the cutfile for an arg matching the one we need, if it exists then re-use it,
		// otherwise create a new one. Attach it to the unload model event.
		for(int uiDropTime=0; uiDropTime < model.lstDropTimes.GetCount(); ++uiDropTime)
		{
			if(model.lstLoadTimes.GetCount() == 0) continue;

			cutfObjectIdEvent* pEvent = rage_new cutfObjectIdEvent(0, model.lstDropTimes[uiDropTime], CUTSCENE_UNLOAD_MODELS_EVENT);

			bool bFound = false;
			for(int uiEventArg=0; uiEventArg < pCutfile->GetEventArgsList().GetCount(); ++uiEventArg)
			{
				cutfEventArgs* pEventArgs = pCutfile->GetEventArgsList()[uiEventArg];

				if(pEventArgs->GetType() == CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE)
				{
					cutfObjectIdListEventArgs* pNamedEventArgs = dynamic_cast<cutfObjectIdListEventArgs*>(pEventArgs);
					if(pNamedEventArgs != NULL)
					{
						const atArray<rage::s32>& lstObjectIDs = pNamedEventArgs->GetObjectIdList();
						if(lstObjectIDs.GetCount() > 0)
						{
							if(lstObjectIDs[0] == iObjectID)
							{
								pEvent->SetEventArgs(pEventArgs);
								bFound=true;
								break;
							}
						}
					}
				}
			}

			if(!bFound)
			{
				atArray<s32> objectIDList;
				objectIDList.Grow() = iObjectID;
				cutfObjectIdListEventArgs* pEventArgs = rage_new cutfObjectIdListEventArgs(objectIDList);

				pEvent->SetEventArgs(pEventArgs);
			}

			if(!DoesEventExist(pEvent, pCutfile))
			{
				pCutfile->AddLoadEvent(pEvent);
			}
			
		}	
	}

	return true;
}

// Remove any model objects from the default load list which is in the cutfile, this just loads everything up front which we dont need anymore
void RemoveModelObjectsFromDefaultLoadList(cutfCutsceneFile2* pCutfile)
{
	const atArray<cutfEvent *>& lstEvents = pCutfile->GetLoadEventList();

	for(int iEvent =0 ; iEvent < lstEvents.GetCount(); ++iEvent)
	{
		if(lstEvents[iEvent]->GetEventId() == CUTSCENE_LOAD_MODELS_EVENT)
		{
			cutfObjectIdListEventArgs* pNamedEventArgs = dynamic_cast<cutfObjectIdListEventArgs*>((cutfEventArgs*)lstEvents[iEvent]->GetEventArgs());
			if(pNamedEventArgs != NULL)
			{
				if(pNamedEventArgs->GetObjectIdList().GetCount() > 1)
				{
					atArray<s32> &objectIDList = const_cast<atArray<s32> &>(pNamedEventArgs->GetObjectIdList() );
					for(int i=0; i < objectIDList.GetCount(); ++i)
					{
						atArray<cutfObject *>& objectList = const_cast<atArray<cutfObject *> &>( pCutfile->GetObjectList() );
						for(int j=0; j < objectList.GetCount(); ++j)
						{
							if(objectList[j]->GetObjectId() == objectIDList[i])
							{
								if(objectList[j]->GetType() == CUTSCENE_MODEL_OBJECT_TYPE)
								{
									objectIDList.Delete(i);
									i=-1;
									break;
								}
							}
						}
					}

					if(objectIDList.GetCount() == 0)
					{
						pCutfile->RemoveLoadEvent(lstEvents[iEvent]);
					}
				}
			}
		}
	}
}

// Go through each even and then compare it vs the others and combine the same events so they share an objectlist and not individual ones per event
void GroupEventArgs(cutfCutsceneFile2* pCutfile, ECutsceneEvent eventType)
{
	atArray<cutfEvent*> &loadEventList = const_cast<atArray<cutfEvent*> &>(pCutfile->GetLoadEventList() );
	for(int i=0; i < loadEventList.GetCount(); ++i)
	{
		bool bSkip=false;
		cutfObjectIdEvent* pIdEvent = dynamic_cast<cutfObjectIdEvent*>(loadEventList[i]);
		if(pIdEvent)
		{
			if(pIdEvent->GetEventId() == eventType)
			{
				cutfEventArgs* pEventArgs = const_cast<cutfEventArgs* >(pIdEvent->GetEventArgs() );
				cutfObjectIdListEventArgs* pIdEventArgs = dynamic_cast<cutfObjectIdListEventArgs*>(pEventArgs);
				if(pIdEventArgs)
				{
					cutfObjectIdListEventArgs* pEventArgsNew = rage_new cutfObjectIdListEventArgs();
					atArray<s32> &objectIDList = const_cast<atArray<s32> &>(pEventArgsNew->GetObjectIdList() );
					objectIDList.Grow() = pIdEventArgs->GetObjectIdList()[0];

					for(int j=0; j < loadEventList.GetCount(); ++j)
					{
						if(j == i) continue;

						cutfObjectIdEvent* pIdEventCompare = dynamic_cast<cutfObjectIdEvent*>(loadEventList[j]);
						if(pIdEvent->GetObjectId() == pIdEventCompare->GetObjectId() &&
							pIdEvent->GetTime() == pIdEventCompare->GetTime() &&
							pIdEvent->GetEventId() == pIdEventCompare->GetEventId())
						{
							cutfObjectIdListEventArgs* pEventArgsCompare = dynamic_cast<cutfObjectIdListEventArgs*>((cutfEventArgs*)pIdEventCompare->GetEventArgs());
							if(pEventArgsCompare)
							{
								if(pEventArgsCompare->GetObjectIdList().GetCount() > 0)
								{
									atArray<s32> &objectIDList2 = const_cast<atArray<s32> &>(pEventArgsNew->GetObjectIdList() );
									//int objectID = pEventArgsCompare->GetObjectIdList()[0];
									objectIDList2.Grow() = pEventArgsCompare->GetObjectIdList()[0];
								}
							}

							pCutfile->AddEventArgs(pEventArgsNew);
							pIdEvent->SetEventArgs(pEventArgsNew);
							pCutfile->RemoveLoadEvent(pIdEventCompare);
							j=-1;
							bSkip=true;
						}
					}
				}
			}
		}

		if(bSkip) i=-1;
	}
}

BOOL FileOrFolderExists(const char* pFolderName)
{   
	return GetFileAttributes(pFolderName) != INVALID_FILE_ATTRIBUTES;   
}

void FindEventsWithObjectID(s32 iObjectId, const atArray<cutfEvent *>& pEventList, atArray<cutfEvent *>& pObjectEventList)
{
	for ( int i = 0; i < pEventList.GetCount(); ++i )
	{        
		if ( pEventList[i]->GetType() == CUTSCENE_OBJECT_ID_EVENT_TYPE )
		{	
			cutfObjectIdListEventArgs* pEventArgs = dynamic_cast<cutfObjectIdListEventArgs*>((cutfEventArgs*)pEventList[i]->GetEventArgs());
			if(pEventArgs)
			{
				atArray<s32> &lstObjectIDs = const_cast<atArray<s32> &>(pEventArgs->GetObjectIdList() );

				if(lstObjectIDs.Find(iObjectId) != -1)
				{
					pObjectEventList.Grow() = pEventList[i];
				}
			}
		}
	}
}

bool AlmostEqual(double nVal1, double nVal2, double nEpsilon)
{
	bool bRet = (((nVal2 - nEpsilon) < nVal1) && (nVal1 < (nVal2 + nEpsilon)));
	return bRet;
}

void CopyObjectIDs(atArray<s32> &To, atArray<s32> &From)
{
	for(int i=0; i < From.GetCount(); ++i)
	{
		To.Grow() = From[i];
	}
}

// Event args might be shared, if we remove ids from the args this will shaft other events
void CheckForSharedEventArgs(cutfObjectIdListEventArgs* pEventArgs, cutfEvent* pEvent, cutfCutsceneFile2* pCutfile)
{
	atArray<s32> lstObjectIDsCopy;
	CopyObjectIDs(lstObjectIDsCopy, const_cast<atArray<s32> &>(pEventArgs->GetObjectIdList()));

	cutfObjectIdListEventArgs* newEventArgs = rage_new cutfObjectIdListEventArgs(lstObjectIDsCopy);

	const atArray<cutfEvent *>& eventLoadList = pCutfile->GetLoadEventList();
	for(int i=0; i < eventLoadList.GetCount(); ++i)
	{
		if(eventLoadList[i]->GetEventArgs() == pEventArgs && eventLoadList[i] != pEvent)
		{
			eventLoadList[i]->SetEventArgs(newEventArgs);
			pCutfile->AddEventArgs(newEventArgs);
		}
	}

	const atArray<cutfEvent *>& eventList = pCutfile->GetEventList();
	for(int i=0; i < eventList.GetCount(); ++i)
	{
		if(eventList[i]->GetEventArgs() == pEventArgs && eventLoadList[i] != pEvent)
		{
			eventList[i]->SetEventArgs(newEventArgs);
			pCutfile->AddEventArgs(newEventArgs);
		}
	}
}

int Main()
{
 	Init();

    const char *pCutFileFilename;
    PARAM_cutFile.Get( pCutFileFilename );

	const char *pInputDir;
	PARAM_inputDir.Get( pInputDir );

	PARAM_timeBuffer.Get( TIME_BUFFER );

	const char *pDestDir;
	PARAM_destDir.Get( pDestDir );

	cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();
	if (!LoadCutfile( pCutFileFilename, *(pCutfile) ) )
	{
		Shutdown();
		return -1;
	}   

	//const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT>& lstConcatScenes = pCutfile->GetConcatDataList();

	//if(lstConcatScenes.size() > 0)
	//{
	//	ULOGGER.SetProgressMessage("Concat scene detected.");

	//	// if this is a concat then we need to check if the models which exist over boundaries are not unloaded
	//	for(int i=0; i < lstConcatScenes.size()-1; ++i)
	//	{
	//		cutfCutsceneFile2::SConcatData data = lstConcatScenes[i+1];
	//		float fSceneEndTime = data.fStartTime;

	//		// look for the unload list at the end time of the scene
	//		const atArray<cutfEvent *>& eventLoadList = pCutfile->GetLoadEventList();

	//		for(int j=0; j < eventLoadList.size(); ++j)
	//		{
	//			cutfEvent* pEvent = eventLoadList[j];

	//			if(pEvent->GetEventId() == CUTSCENE_UNLOAD_MODELS_EVENT)
	//			{
	//				if(AlmostEqual(pEvent->GetTime(), fSceneEndTime, 0.001))
	//				//if(pEvent->GetTime() == fSceneEndTime)
	//				{
	//					// get the objectids attached to this event and then see if they exist in any subsequent events 
	//					// to determine if the object exists in this scene.
	//					
	//					cutfObjectIdListEventArgs* pEventArgs = dynamic_cast<cutfObjectIdListEventArgs*>((cutfEventArgs*)pEvent->GetEventArgs());
	//					if(pEventArgs)
	//					{
	//						atArray<s32> &lstObjectIDs = const_cast<atArray<s32> &>(pEventArgs->GetObjectIdList() );

	//						for(int k=0; k < lstObjectIDs.size(); ++k)
	//						{
	//							atArray<cutfEvent *> objectEventList;
	//							FindEventsWithObjectID( lstObjectIDs[k], eventLoadList, objectEventList );

	//							for(int l=0; l < objectEventList.size(); ++l)
	//							{
	//								cutfEvent* pEvent2 = objectEventList[l];

	//								if(pEvent2->GetTime() > fSceneEndTime)
	//								{
	//									// object exists past the scene time so must exist in the next scene so we delete it from the list
	//									ULOGGER.SetProgressMessage("Removing ObjectID %d from list.", lstObjectIDs[k]);

	//									CheckForSharedEventArgs(pEventArgs, pEvent, pCutfile);
	//									
	//									lstObjectIDs.Delete(k);
	//									--k;
	//									break;
	//								}
	//							}
	//						}

	//						if(lstObjectIDs.size() == 0)
	//						{
	//							pCutfile->RemoveLoadEvent(pEvent);

	//							ULOGGER.SetProgressMessage("Removing CUTSCENE_UNLOAD_MODELS_EVENT @ %f, ObjectID list is empty.", fSceneEndTime);

	//							j--;
	//						}

	//						break;
	//					}
	//				}
	//			}
	//		}
	//	}
	//}
	//else
	//{
	if(FileOrFolderExists(pInputDir))
	{
		atArray<atString> FileList;
		if(RetrieveStreamingBoxFiles(pInputDir, FileList))
		{
			RemoveModelObjectsFromDefaultLoadList(pCutfile);

			atArray<SModelInfo> ModelInfo;
			GatherModelInfo(pInputDir, FileList, ModelInfo, pCutfile);

			CheckForDupeModels(ModelInfo);

			if(!CreateLoadAndUnloadLists(pCutfile, ModelInfo))
			{
				Shutdown();
				return -1;
			}

			GroupEventArgs(pCutfile, CUTSCENE_LOAD_MODELS_EVENT);
			GroupEventArgs(pCutfile, CUTSCENE_UNLOAD_MODELS_EVENT);

			pCutfile->SortLoadEvents();
		}
		else
		{
			ULOGGER.SetProgressMessage("WARNING: No streaming files to process.");
		}
	}

	atString strOutput(pDestDir);
	strOutput += "/data_stream.cutxml";

	const_cast<atBitSet &>( pCutfile->GetCutsceneFlags() ).Set( cutfCutsceneFile2::CUTSCENE_STREAM_PROCESSED );

	if(!pCutfile->SaveFile(strOutput.c_str()))
	{
		ULOGGER.SetProgressMessage("ERROR: '%s' fail.", strOutput.c_str());
		return -1;
	}

    Shutdown();

    return 0;
}
