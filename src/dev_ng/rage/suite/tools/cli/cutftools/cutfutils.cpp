// 
// cutftools/cutfutils.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "cutfutils.h"

#include "configParser/configExportView.h"
#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "cutfile/cutfdefines.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "file/asset.h"
#include "file/device.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "string/string.h"
#include "system/exec.h"
#include "vectormath/legacyconvert.h"

#include "RSULog/ULogger.h"

using namespace rage;
//#############################################################################

rage::atString GetTempLogFileName( const char* pPrefix )
{
	UUID uuid; 
	char* uuidAsString; 
	::UuidCreate(&uuid); 
	::UuidToString(&uuid, reinterpret_cast<RPC_CSTR*>(&uuidAsString)); 

	char cToolsRoot[RAGE_MAX_PATH];
	GetEnvironmentVariable("RS_TOOLSROOT",cToolsRoot,RAGE_MAX_PATH );

	char cLogPath[RAGE_MAX_PATH];
	sprintf(cLogPath, "%s\\logs\\cutscene\\process\\%s_%s.ulog", cToolsRoot, pPrefix, uuidAsString);

	::RpcStringFree(reinterpret_cast<RPC_CSTR*>(&uuidAsString));

	Displayf(cLogPath);

	return atString(cLogPath);
}

bool LoadCutfile( const char* pFilename, cutfCutsceneFile2 &cutfile )
{
    if ( !cutfile.LoadFile( pFilename ) )
    {
        Errorf( "Unable to load '%s'.", pFilename );

        return false;
    }

    return true;
}

void LoadSectioningMethod( cutfCutsceneFile2 &cutfile, int iMethod, float fDuration )
{
    atBitSet &bitSet = const_cast<atBitSet &>( cutfile.GetCutsceneFlags() );

    switch ( iMethod )
    {
    case 0:
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG );
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG );
        break;
    case 1:
        bitSet.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG );
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG );
        break;
    case 2:
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG );
        bitSet.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG );
        break;
    case 3:
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG );
        bitSet.Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
        bitSet.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG );
        break;
    default:
        // leave it as is
        break;
    }

    // get the time-slice duration, if any
    if ( (fDuration > 0.0f) && (fDuration < cutfile.GetTotalDuration()) )
    {
        cutfile.SetSectionByTimeSliceDuration( fDuration );
    }
}

bool ConditionalFileCopy( const char *pSrcFilename, const char *pDestDir, const char *pDestFile, 
                         const rage::atArray<SCutsceneInputItem> &cutsceneInputItemList )
{
    char cDestFilename[RAGE_MAX_PATH];
    sprintf( cDestFilename, "%s\\%s", pDestDir, pDestFile );

    bool bCopyFile = true;
    if ( ASSET.Exists( cDestFilename, NULL ) )
    {
        bool inputsAreNewer = false;
        for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
        {
            char cCompareFilename[RAGE_MAX_PATH];
            sprintf( cCompareFilename, "%s\\%s", cutsceneInputItemList[i].dir.c_str(), pDestFile );

            if ( fiDevice::GetDevice( cCompareFilename )->IsOutOfDate( cCompareFilename, cDestFilename ) 
                && ASSET.Exists( cCompareFilename, NULL ) )
            {
                inputsAreNewer = true;
                break;
            }
        }

        if ( !inputsAreNewer )
        {
            bCopyFile = false;
        }
    }

    if ( bCopyFile )
    {
		ULOGGER.SetProgressMessage("Copying '%s' to '%s'.", pSrcFilename, cDestFilename );
		Displayf("Copying '%s' to '%s'.", pSrcFilename, cDestFilename );

        if ( !fiDevice::GetDevice( pSrcFilename )->CopySingleFile( pSrcFilename, cDestFilename ) )
        {
            return false;
        }
    }

    return true;
}

bool SaveIntermediateFiles( const char *pOutputDir, const char *pOutputCutfile, 
                           const rage::atArray<SCutsceneInputItem> &cutsceneInputItemList )
{
    if ( !ConditionalFileCopy( pOutputCutfile, pOutputDir, "data.cutxml", cutsceneInputItemList ) )
    {
        return false;
    }

    return true;
}

bool ReadLine( fiStream* pStream, char* pDest, int destLen )
{
    bool started = false;

    s32 index = 0;
    while ( pStream->ReadByte( &pDest[index], 1 ) )
    {
        if ( !started && (pDest[index] != '\n') && (pDest[index] != '\r') )
        {
            started = true;
        }

        if ( started )
        {
            if ( (pDest[index] == '\n') || (pDest[index] == '\r') )
            {
                break;
            }

            index++;
            if (index == destLen )
            {
                break;
            }
        }
    }

    if ( index == 0 )
    {
        return false;
    }

    pDest[index] = '\0';

    return true;
}

bool WriteLine( fiStream* pStream, const char* pText )
{
    pStream->Write( pText, strlen(pText) );	
    pStream->WriteByte( "\r\n", 2 );

    return true;
}

//#############################################################################
// cutfcheckmethod
//#############################################################################

void SCheckDataItem::Reset()
{
    cutFilename = "";
    iRangeStart = -1;
    iRangeEnd = -1;
    iMethod = -1;
    fDuration = -1.0f;
    bIsSectioned = false;
}


bool SaveMethodFile( const char *pCheckFilename, const atArray<SCheckDataItem> &checkDataItemList )
{
    fiStream *pStream = NULL;
    if ( ASSET.Exists( pCheckFilename, NULL ) )
    {
        pStream = ASSET.Open( pCheckFilename, "", false, false );
    }
    else
    {
        pStream = ASSET.Create( pCheckFilename, "" );
    }

    if ( pStream == NULL )
    {
		ULOGGER.SetProgressMessage("ERROR: Could not open or create '%s'.", pCheckFilename );
		Errorf("Could not open or create '%s'.", pCheckFilename );

        return false;
    }

    for ( int i = 0; i < checkDataItemList.GetCount(); ++i )
    {
        WriteLine( pStream, checkDataItemList[i].cutFilename.c_str() );
        WriteLine( pStream, "{" );

        char cText[128];
        sprintf( cText, "\tstart=%d", checkDataItemList[i].iRangeStart );
        WriteLine( pStream, cText );

        sprintf( cText, "\tend=%d", checkDataItemList[i].iRangeEnd );
        WriteLine( pStream, cText );

        sprintf( cText, "\tmethod=%d", checkDataItemList[i].iMethod );
        WriteLine( pStream, cText );

        sprintf( cText, "\tduration=%f", checkDataItemList[i].fDuration );
        WriteLine( pStream, cText );

        WriteLine( pStream, "}" );
    }

    pStream->Close();

	ULOGGER.SetProgressMessage("Saved '%s'.", pCheckFilename );
	Displayf("Saved '%s'.", pCheckFilename );
    
    return true;
}

bool LoadMethodFile( const char *pCheckFilename, atArray<SCheckDataItem> &checkDataItemList )
{
    fiStream *pStream = ASSET.Open( pCheckFilename, NULL );
    if ( pStream == NULL )
    {
		ULOGGER.SetProgressMessage("ERROR: Unable to open '%s'.", pCheckFilename );
		Errorf("Unable to open '%s'.", pCheckFilename );

        return false;
    }

    bool bFoundCutFilename = false;
    bool bInSection = false;
    SCheckDataItem currentCheckDataItem;

    int iLineNumber = 1;

    while ( true )
    {
        char cLine[RAGE_MAX_PATH];
        if ( !ReadLine( pStream, cLine, sizeof(cLine) ) )
        {
            break;
        }

        atString line( cLine );
        line.Trim();

        if ( bInSection )
        {
            if ( line == "}" )
            {
                // only add the item if it was formatted correctly
                if ( bFoundCutFilename )
                {
                    checkDataItemList.PushAndGrow( currentCheckDataItem );
                    bFoundCutFilename = false;
                }

                currentCheckDataItem.Reset();
                bInSection = false;
            }
            else if ( bFoundCutFilename )   // only read the info if we found the cutfilename
            {
                atArray<atString> lineSplit;
                line.Split( lineSplit, "=" );

                if ( lineSplit.GetCount() >= 2 )
                {
                    lineSplit[0].Trim();
                    lineSplit[1].Trim();

                    if ( lineSplit[0] == "start" )
                    {
                        currentCheckDataItem.iRangeStart = atoi( lineSplit[1].c_str() );
                    }
                    else if ( lineSplit[0] == "end" )
                    {
                        currentCheckDataItem.iRangeEnd = atoi( lineSplit[1].c_str() );
                    }
                    else if ( lineSplit[0] == "method" )
                    {
                        currentCheckDataItem.iMethod = atoi( lineSplit[1].c_str() );
                    }
                    else if ( lineSplit[0] == "duration" )
                    {
                        currentCheckDataItem.fDuration = (float)atof( lineSplit[1].c_str() );
                    }
                }
            }
        }
        else
        {
            if ( line == "{" )
            {
                if ( !bFoundCutFilename )
                {      
					ULOGGER.SetProgressMessage("ERROR: Invalid format detected (line %d).  No cut filename found before '{'.", iLineNumber );
					Errorf("Invalid format detected (line %d).  No cut filename found before '{'.", iLineNumber );

                    pStream->Close();
                    return false;
                }

                bInSection = true;
            }
            else if ( line.GetLength() > 0 )
            {
                currentCheckDataItem.cutFilename = line;

                bFoundCutFilename = true;
            }
        }

        ++iLineNumber;
    }

    pStream->Close();
    return true;
}

//#############################################################################
// cutfcheckmethod and cutfconcat
//#############################################################################

bool LoadCutListFile( const char *pFilename, atArray<SCutsceneInputItem> &cutceneInputItemList )
{
    fiStream *pStream = ASSET.Open( pFilename, NULL );
    if ( pStream == NULL )
    {
        Errorf( "Unable to open '%s'.", pFilename );
        return false;
    }

    int iLineNumber = 1;
    while ( true )
    {
        char cLine[RAGE_MAX_PATH * 4];
        if ( !ReadLine( pStream, cLine, sizeof(cLine) ) )
        {
            break;
        }

        atString line( cLine );

        atArray<atString> lineSplit;
        line.Split( lineSplit, '\t' );

        SCutsceneInputItem cutsceneInputItem;

        for ( int i = 0; i < lineSplit.GetCount(); ++i )
        {
            lineSplit[i].Trim();

            switch ( i )
            {
            case 0:
                {
                    cutsceneInputItem.cutFilename = lineSplit[i];
                }
                break;
            default:
                break;
            }
        }

        cutceneInputItemList.PushAndGrow( cutsceneInputItem );

        ++iLineNumber;
    }

    pStream->Close();

    return true;
}

//#############################################################################

SRemoveTracksItem::SRemoveTracksItem()
: inputName("")
, inputDir("$(inputDir)")
, outputName("")
, outputDir("$(outputDir)")
, removeTracks("")
{

}

const char* SRemoveTracksItem::GetInputFilename( char *pDest, int destLen ) const
{
    if ( ASSET.IsAbsolutePath( inputName.c_str() ) )
    {
        safecpy( pDest, inputName.c_str(), destLen );
    }
    else
    {
        atString animName( inputName );
        animName.Replace( " ", "_" );

        formatf( pDest, destLen, "%s\\%s", inputDir.c_str(), animName.c_str() );
    }

    const char *pExtension = ASSET.FindExtensionInPath( pDest );
    if ( pExtension == NULL )
    {
        safecat( pDest, ".anim", destLen );
    }

    return pDest;
}

const char* SRemoveTracksItem::GetOutputFilename( char *pDest, int destLen ) const
{
    atString animName( outputName );
    animName.Replace( " ", "_" );

    formatf( pDest, destLen, "%s\\%s", outputDir.c_str(), animName.c_str() );

    const char *pExtension = ASSET.FindExtensionInPath( pDest );
    if ( pExtension == NULL )
    {
        safecat( pDest, ".anim", destLen );
    }

    return pDest;
}

void WriteRemoveTracksItem( rage::parStreamOut *s, const SRemoveTracksItem &removeTracksItem )
{
    Displayf( removeTracksItem.outputName.c_str() );
    s->WriteComment( removeTracksItem.outputName.c_str() );

    char cInputFilename[RAGE_MAX_PATH];
    removeTracksItem.GetInputFilename( cInputFilename, sizeof(cInputFilename));

    parElement RemoveTracksItemElt;
    RemoveTracksItemElt.SetName( "RemoveAnimTracksItem" );
    RemoveTracksItemElt.AddAttribute( "Include", cInputFilename );  // face anim

    s->WriteBeginElement( RemoveTracksItemElt );
    {
        parElement outputFileElt;
        outputFileElt.SetName( "outputFile");
        s->WriteBeginElement( outputFileElt );
        {
            char cOutputFilename[RAGE_MAX_PATH];
            removeTracksItem.GetOutputFilename( cOutputFilename, sizeof(cOutputFilename) );

            s->WriteData( cOutputFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( outputFileElt );

        parElement removeTracksElt;
        removeTracksElt.SetName( "removeTracks" );
        s->WriteBeginElement( removeTracksElt );
        {
            s->WriteData( removeTracksItem.removeTracks.c_str(), -1, true, parStream::ASCII );
        }
        s->WriteEndElement( removeTracksElt );
    }
    s->WriteEndElement( RemoveTracksItemElt );
}

SCombineItem::SCombineItem()
: inputName("")
, inputDir("$(inputDir)")
, outputName("")
, outputDir("$(outputDir)")
, intermediateDir("$(intermediateDir)")
, inputFaceName("")
{

}

const char* SCombineItem::GetInputFilename( char *pDest, int destLen, const char *pExtension ) const
{
    atString animName( inputName );
    animName.Replace( " ", "_" );

    formatf( pDest, destLen, "%s\\%s%s", inputDir.c_str(), animName.c_str(), pExtension );
    return pDest;
}

const char* SCombineItem::GetOutputFilename( char *pDest, int destLen, const char *pExtension ) const
{
    atString animName( outputName );
    animName.Replace( " ", "_" );

    formatf( pDest, destLen, "%s\\%s%s", outputDir.c_str(), outputName.c_str(), pExtension );
    return pDest;
}

const char* SCombineItem::GetInputFaceFilename( char *pDest, int destLen, const char *pExtension ) const
{
    atString animName( inputFaceName );
    animName.Replace( " ", "_" );

    formatf( pDest, destLen, "%s\\%s%s", intermediateDir.c_str(), inputFaceName.c_str(), pExtension );
    return pDest;
}

void WriteCombineItem( rage::parStreamOut *s, const SCombineItem &combineItem )
{
    Displayf( combineItem.outputName.c_str() );
    s->WriteComment( combineItem.outputName.c_str() );

    char cInputFilename[RAGE_MAX_PATH];
    combineItem.GetInputFilename( cInputFilename, sizeof(cInputFilename), ".anim" );

    parElement combineAnimsItemElt;
    combineAnimsItemElt.SetName( "CombineAnimsItem" );
    combineAnimsItemElt.AddAttribute( "Include", cInputFilename );
    s->WriteBeginElement( combineAnimsItemElt );
    {
        parElement bodyClipElt;
        bodyClipElt.SetName( "clipFile" );
        s->WriteBeginElement( bodyClipElt );
        {
            char cInputClipFilename[RAGE_MAX_PATH];
            combineItem.GetInputFilename( cInputClipFilename, sizeof(cInputClipFilename), ".clip" );

            s->WriteData( cInputClipFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( bodyClipElt );

        parElement faceAnimElt;
        faceAnimElt.SetName( "faceAnimFile" );
        s->WriteBeginElement( faceAnimElt );
        {
            char cInputFaceFilename[RAGE_MAX_PATH];
            combineItem.GetInputFaceFilename( cInputFaceFilename, sizeof(cInputFaceFilename), ".anim" );

            s->WriteData( cInputFaceFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( faceAnimElt );

        parElement outputAnimFileElt;
        outputAnimFileElt.SetName( "outputAnimFile" );
        s->WriteBeginElement( outputAnimFileElt );
        {
            // Do NOT include the directory.  We want the clip to reference the anim with a relative path.
            char cOutputFilename[RAGE_MAX_PATH];
            formatf( cOutputFilename, sizeof(cOutputFilename), "%s.anim", combineItem.outputName.c_str() );

            s->WriteData( cOutputFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( outputAnimFileElt );

        parElement outputClipFileElt;
        outputClipFileElt.SetName( "outputClipFile" );
        s->WriteBeginElement( outputClipFileElt );
        {
            char cOutputFilename[RAGE_MAX_PATH];
            combineItem.GetOutputFilename( cOutputFilename, sizeof(cOutputFilename), ".clip" );

            s->WriteData( cOutputFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( outputClipFileElt );
    }
    s->WriteEndElement( combineAnimsItemElt );
}

//#############################################################################

SCropFileItem::SCropFileItem()
: fStartTime(0.0f)
, fEndTime(FLT_MAX)
, bIsFaceAnim(false)
, bIsClip(false)
, bCanTouchInputs(false)
, inputName("")
, inputDir("")
, outputName("")
, outputDir("")
{

}

const char* SCropFileItem::GetInputFilenameWithoutExtension( char *pDest, int destLen ) const
{
    if ( bIsFaceAnim )
    {
        if ( ASSET.IsAbsolutePath( inputName.c_str() ) )
        {
            safecpy( pDest, inputName.c_str(), destLen );
        }
        else
        {
            formatf( pDest, destLen, "%s\\%s", inputDir.c_str(), inputName.c_str() );
        }
    }
    else
    {
        atString animName( inputName );
        animName.Replace( " ", "_" );

        formatf( pDest, destLen, "%s\\%s", inputDir.c_str(), animName.c_str() );
    }

    return pDest;
}

const char* SCropFileItem::GetOutputFilenameWithoutExtension( char *pDest, int destLen ) const
{
    // no crop necessary, so return the input filename
    if ( (fStartTime == 0.0f) && (fEndTime == FLT_MAX) )
    {
        return GetInputFilenameWithoutExtension( pDest, destLen );
    }

    atString animName( outputName );
    if ( !bIsFaceAnim )
    {
        animName.Replace( " ", "_" );
    }

    formatf( pDest, destLen, "%s\\%s", outputDir.c_str(), animName.c_str() );

    return pDest;
}

const char* SCropFileItem::GetClipAnimFilename( char *pDest, int destLen ) const
{
    atString animName( outputName );

    if ( bIsFaceAnim )
    {
        animName.Truncate( animName.GetLength() - 5 );
    }
    else
    {
        animName.Replace( " ", "_" );
    }

    formatf( pDest, destLen, "%s.anim", animName.c_str() );
    
    return pDest;
}

void WriteCropItem( parStreamOut *s, const char *pItemName, const char *pExtension, const SCropFileItem &cropFileItem )
{
    char cInputFilename[RAGE_MAX_PATH];
    cropFileItem.GetInputFilenameWithoutExtension( cInputFilename, sizeof(cInputFilename) );
    safecat( cInputFilename, pExtension, sizeof(cInputFilename) );

    char cOutputFilename[RAGE_MAX_PATH];
    cropFileItem.GetOutputFilenameWithoutExtension( cOutputFilename, sizeof(cOutputFilename) );
    safecat( cOutputFilename, pExtension, sizeof(cOutputFilename) );

    parElement cropItemElt;
    cropItemElt.SetName( pItemName );
    cropItemElt.AddAttribute( "Include", cInputFilename );

    s->WriteBeginElement( cropItemElt );
    {
        parElement outputFileElt;
        outputFileElt.SetName( "outputFile" );
        s->WriteBeginElement( outputFileElt );
        {
            s->WriteData( cOutputFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( outputFileElt );

        parElement startElt;
        startElt.SetName( "start" );
        s->WriteBeginElement( startElt );
        {
            char cStart[128];
            sprintf( cStart, "%f", cropFileItem.fStartTime );
            s->WriteData( cStart, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( startElt );

        parElement endElt;
        endElt.SetName( "end" );
        s->WriteBeginElement( endElt );
        {
            char cEnd[128];
            sprintf( cEnd, "%f", cropFileItem.fEndTime );
            s->WriteData( cEnd, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( endElt );

        parElement canTouchInputsElt;
        canTouchInputsElt.SetName( "canTouchInputs" );
        s->WriteBeginElement( canTouchInputsElt );
        {
            s->WriteData( cropFileItem.bCanTouchInputs ? "true" : "false", -1, true, parStream::ASCII );
        }
        s->WriteEndElement( canTouchInputsElt );

        if ( cropFileItem.bIsClip )
        {
            parElement animFileElt;
            animFileElt.SetName( "animFile" );
            s->WriteBeginElement( animFileElt );
            {
                char cAnimFilename[RAGE_MAX_PATH];
                cropFileItem.GetClipAnimFilename( cAnimFilename, sizeof(cAnimFilename) );

                s->WriteData( cAnimFilename, -1, true, parStream::ASCII );
            }
            s->WriteEndElement( animFileElt );
        }
    }
    s->WriteEndElement( cropItemElt );
}

bool WriteCropItems( parStreamOut *s, const SCropFileItem &cropFileItem )
{
    //Displayf( cropFileItem.outputName.c_str() );
    s->WriteComment( cropFileItem.outputName.c_str() );

    if ( cropFileItem.bIsFaceAnim )
    {
        if ( cropFileItem.bIsClip )
        {
            // We only need to crop the animation, so load the clip and find the anim it references
            SCropFileItem animCropFileItem = cropFileItem;
            animCropFileItem.bIsClip = false;
            
            // Resolve env vars
            const crClip *pClip = crClip::AllocateAndLoad( cropFileItem.inputName.c_str() );
            if ( pClip == NULL )
            {
                Errorf( "Unable to load face animation clip '%s'.", cropFileItem.inputName.c_str() );
                return false;
            }

            if ( pClip->GetType() != crClip::kClipTypeAnimation )
            {
                Errorf( "The face animation clip '%s' is not of type crClip::kClipTypeAnimation.", cropFileItem.inputName.c_str() );
                return false;
            }

            const crClipAnimation *pClipAnimation = dynamic_cast<const crClipAnimation *>( pClip );
            if ( pClipAnimation->GetAnimation() == NULL )
            {
                Errorf( "The face animation clip '%s' does not contain an animation.", cropFileItem.inputName.c_str() );
                return false;
            }

            animCropFileItem.inputName = pClipAnimation->GetAnimation()->GetName();
            animCropFileItem.inputName.Replace( "/", "\\" );

            animCropFileItem.outputName.Truncate( animCropFileItem.outputName.GetLength() - 4 );
            animCropFileItem.outputName += "anim";
            
            // write the anim concat items into the .targets file
            WriteCropItem( s, "CropAnimItem", "", animCropFileItem );

            pClip->Release();

            // write the clip concat items into the .targets file
            WriteCropItem( s, "CropClipItem", "", cropFileItem );
        }
        else
        {
            // write the anim crop items into the .targets file
            WriteCropItem( s, "CropAnimItem", "", cropFileItem );
        }
    }
    else
    {
        SCropFileItem animCropFileItem = cropFileItem;
        animCropFileItem.bIsClip = false;

        // write the anim concat items into the .targets file
        WriteCropItem( s, "CropAnimItem", ".anim", animCropFileItem );
     
        if ( cropFileItem.bIsClip )
        {
            // write the clip concat items into the .targets file
            WriteCropItem( s, "CropClipItem", ".clip", cropFileItem );
        }
    }

    return true;
}

//#############################################################################

SCompressClipItem::SCompressClipItem()
: inputName("")
, inputDir("$(inputDir)")
, outputName("")
, outputDir("$(outputDir)")  
, clipListName("")
, args("")
{ 
}

const char* SCompressClipItem::GetInputFilename( char *pDest, int destLen, const char *pExtension ) const
{
    atString animName( inputName );
    animName.Replace( " ", "_" );

    formatf( pDest, destLen, "%s\\%s%s", inputDir.c_str(), animName.c_str(), pExtension );
    return pDest;
}

const char* SCompressClipItem::GetOutputClipFilename( char *pDest, int destLen ) const
{
    atString animName( outputName );
    animName.Replace( " ", "_" );

    formatf( pDest, destLen, "%s\\%s.clip", outputDir.c_str(), animName.c_str() );
    return pDest;
}

const char* SCompressClipItem::GetOutputAnimFilename( char *pDest, int destLen ) const
{
    atString animName( outputName );
    animName.Replace( " ", "_" );

    // Do not include outputDir, we want the clip to store the animation with a relative path.
    formatf( pDest, destLen, "%s.anim", animName.c_str() );
    return pDest;
}

const char* SCompressClipItem::GetClipListFilename( char *pDest, int destLen ) const
{
    if ( clipListName.GetLength() > 0 )
    {
        formatf( pDest, destLen, "%s\\%s.cliplist", outputDir.c_str(), clipListName.c_str() );
    }
    else
    {
        pDest[0] = 0;
    }

    return pDest;
}

void WriteCompressClipItem( parStreamOut *s, const SCompressClipItem &compressClipItem )
{
    Displayf( compressClipItem.outputName.c_str() );
    s->WriteComment( compressClipItem.outputName.c_str() );

    char cInputClipFilename[RAGE_MAX_PATH];
    compressClipItem.GetInputFilename( cInputClipFilename, sizeof(cInputClipFilename), ".clip" );

    parElement compressItemElt;
    compressItemElt.SetName( "CompressClipItem" );
    compressItemElt.AddAttribute( "Include", cInputClipFilename );

    s->WriteBeginElement( compressItemElt );
    {
        parElement outputDirElt;
        outputDirElt.SetName( "outputDir" );
        s->WriteBeginElement( outputDirElt );
        {
            s->WriteData( compressClipItem.outputDir.c_str(), -1, true, parStream::ASCII );
        }
        s->WriteEndElement( outputDirElt );

        parElement outputFileElt;
        outputFileElt.SetName( "outputFile" );
        s->WriteBeginElement( outputFileElt );
        {
            char cOuputClipFilename[RAGE_MAX_PATH];
            compressClipItem.GetOutputClipFilename( cOuputClipFilename, sizeof(cOuputClipFilename) );

            s->WriteData( cOuputClipFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( outputFileElt );

        parElement animFileElt;
        animFileElt.SetName( "animFile" );
        s->WriteBeginElement( animFileElt );
        {
            char cInputAnimFilename[RAGE_MAX_PATH];
            compressClipItem.GetInputFilename( cInputAnimFilename, sizeof(cInputAnimFilename), ".anim" );

            s->WriteData( cInputAnimFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( animFileElt );

        parElement outputAnimFileElt;
        outputAnimFileElt.SetName( "outputAnimFile" );
        s->WriteBeginElement( outputAnimFileElt );
        {
            char cOuputAnimFilename[RAGE_MAX_PATH];
            compressClipItem.GetOutputAnimFilename( cOuputAnimFilename, sizeof(cOuputAnimFilename) );

            s->WriteData( cOuputAnimFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( outputAnimFileElt );

        parElement clipListFileElt;
        clipListFileElt.SetName( "clipListFile" );
        s->WriteBeginElement( clipListFileElt );
        {
            char cClipListFilename[RAGE_MAX_PATH];
            compressClipItem.GetClipListFilename( cClipListFilename, sizeof(cClipListFilename) );

            s->WriteData( cClipListFilename, -1, true, parStream::ASCII );
        }
        s->WriteEndElement( clipListFileElt );

        parElement argsElt;
        argsElt.SetName( "args" );
        s->WriteBeginElement( argsElt );
        {
            if ( compressClipItem.args.GetLength() > 0 )
            {
                s->WriteData( compressClipItem.args.c_str(), -1, true, parStream::ASCII );
            }
        }
        s->WriteEndElement( argsElt );
    }
    s->WriteEndElement( compressItemElt );
}

//#############################################################################

SDictItem::SDictItem()
: listFile("")
, listDir("")
{

}

const char* SDictItem::GetListFilename( char *pDest, int destLen, const char *pExtension ) const
{
    formatf( pDest, destLen, "%s\\%s%s", listDir.c_str(), listFile.c_str(), pExtension );
    return pDest;
}

void WriteDictItem( parStreamOut *s, const char *pEltName, const char *pExtension, const SDictItem &dictItem )
{
    char cListFilename[RAGE_MAX_PATH];
    dictItem.GetListFilename( cListFilename, sizeof(cListFilename), pExtension );

    Displayf( dictItem.listFile.c_str() );
    s->WriteComment( dictItem.listFile.c_str() );

    parElement dictItemElt;
    dictItemElt.SetName( pEltName );
    dictItemElt.AddAttribute( "Include", cListFilename );

    s->WriteBeginElement( dictItemElt );
    {
        parElement pathItemElt;
        pathItemElt.SetName( "path" );
        s->WriteBeginElement( pathItemElt );
        {
            s->WriteData( dictItem.listDir.c_str() );
        }
        s->WriteEndElement( pathItemElt );
    }
    s->WriteEndElement( dictItemElt );
}

//#############################################################################

void CropCutfile( cutfCutsceneFile2 &cutFile, bool bCropStart, bool bCropEnd )
{
    float fStartTime = bCropStart ? cutFile.GetRangeStart() / CUTSCENE_FPS : 0.0f;
	// FIX_MIKE : End range stays the same but we want to set the duration to include the last frame so we get example. 10-1200 as 1901 frames
    float fEndTime = bCropEnd ? (cutFile.GetRangeEnd() + 1) / CUTSCENE_FPS : cutFile.GetTotalDuration();

	ULOGGER.SetProgressMessage("Trimming cutfile '%s' to time range (%f, %f).", cutFile.GetSceneName(), fStartTime, fEndTime );

    atArray<cutfEvent *> &eventList = const_cast<atArray<cutfEvent *> &>( cutFile.GetEventList() );

    // Look for events that both start and stop between time 0 and fStartTime.  They can be deleted.
    for ( int i = 0; i < eventList.GetCount(); ++i )
    {
		// round frame correctly, this was calculating a frame too high and removing valid events off the end.
		int iEventFrame = int((eventList[i]->GetTime() * CUTSCENE_FPS) + 0.5f);
		//int iEventFrame = (int)ceil(eventList[i]->GetTime() * CUTSCENE_FPS);

        if ( iEventFrame >= cutFile.GetRangeStart() && iEventFrame <= (cutFile.GetRangeEnd() + 1))
        {
            continue;
        }

		// These events come from motionbuilder in 0 space so don't follow the same pattern.
		if (eventList[i]->GetEventId() == CUTSCENE_SET_ANIM_EVENT ||
			eventList[i]->GetEventId() == CUTSCENE_CLEAR_ANIM_EVENT ||
			eventList[i]->GetEventId() == CUTSCENE_PLAY_AUDIO_EVENT ||
			eventList[i]->GetEventId() == CUTSCENE_STOP_AUDIO_EVENT ||
			eventList[i]->GetEventId() == CUTSCENE_SET_LIGHT_EVENT ||
			eventList[i]->GetEventId() == CUTSCENE_CLEAR_LIGHT_EVENT)
			continue;

        s32 iOppositeEventId = eventList[i]->GetOppositeEventId();

        switch ( eventList[i]->GetType() )
        {
        case CUTSCENE_EVENT_TYPE:
            break;
        case CUTSCENE_OBJECT_ID_EVENT_TYPE:
            {
                cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<cutfObjectIdEvent *>( eventList[i] );

				// Find all events associated with this object, use this to look for an opposite event if needed
                atArray<cutfEvent *> objectIdEventList;
                cutFile.FindEventsForObjectIdOnly( pObjectIdEvent->GetObjectId(), eventList, objectIdEventList );

				if ( iOppositeEventId == -1 )
				{
					// We have no opposite so just remove the event
					ULOGGER.SetProgressMessage("Removed event id %d at time %f.", pObjectIdEvent->GetEventId(), pObjectIdEvent->GetTime() );

					 cutFile.RemoveEvent( pObjectIdEvent );
					 --i;
					 break;
				}
				else
				{
					// We are searching for an opposite event
					for(int j=0; j < objectIdEventList.GetCount(); ++j)
					{
						if ( objectIdEventList[j]->GetEventId() == iOppositeEventId )
						{
							ULOGGER.SetProgressMessage("Removed event id %d at time %f.", objectIdEventList[j]->GetEventId(), objectIdEventList[j]->GetTime() );

							cutFile.RemoveEvent( objectIdEventList[j] );
							break;
						}
					}

					ULOGGER.SetProgressMessage("Removed event id %d at time %f.", pObjectIdEvent->GetEventId(), pObjectIdEvent->GetTime() );

					cutFile.RemoveEvent( pObjectIdEvent );
					--i;
					break;
				}
            }
            break;
        case CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE:
            {
                cutfObjectIdListEvent *pObjectIdListEvent = dynamic_cast<cutfObjectIdListEvent *>( eventList[i] );

                for ( int j = i + 1; j < eventList.GetCount(); ++j )
                {
                    if ( eventList[j]->GetTime() >= fStartTime && eventList[j]->GetTime() <= fEndTime)
                    {
                        break;
                    }

                    if ( iOppositeEventId == -1 )
                    {
                        // this shouldn't really ever happen, but is here for completeness
                        if ( eventList[j]->GetEventId() == pObjectIdListEvent->GetEventId() )
                        {   
							// remove the previous event      
							ULOGGER.SetProgressMessage("Removed event id %d at time %f.", pObjectIdListEvent->GetEventId(), pObjectIdListEvent->GetTime() );
							
                            cutFile.RemoveEvent( pObjectIdListEvent );
                            --i;
                            break;
                        }
                    }
                    else if ( eventList[j]->GetEventId() == iOppositeEventId )
                    {
                        // make sure it is really our previous
                        cutfObjectIdListEvent *pPrevObjectIdListEvent = dynamic_cast<cutfObjectIdListEvent *>( eventList[j] );
                        if ( pPrevObjectIdListEvent->GetEventArgs() != pObjectIdListEvent->GetEventArgs() )
                        {
                            continue;
                        }

                        const atArray<s32> &objectIdList = pObjectIdListEvent->GetObjectIdList();
                        const atArray<s32> &prevObjectIdList = pPrevObjectIdListEvent->GetObjectIdList();
                        if ( objectIdList.GetCount() != prevObjectIdList.GetCount() )
                        {
                            continue;
                        }

                        const atArray<cutfEventArgs *> &eventArgsList = pObjectIdListEvent->GetEventArgsList();
                        const atArray<cutfEventArgs *> &prevEventArgsList = pPrevObjectIdListEvent->GetEventArgsList();
                        if ( eventArgsList.GetCount() != prevEventArgsList.GetCount() )
                        {
                            continue;
                        }

                        bool bSame = true;
                        for ( int k = 0; k < objectIdList.GetCount(); ++k )
                        {
                            if ( objectIdList[k] != prevObjectIdList[k] )
                            {
                                bSame = false;
                                break;
                            }

                            if ( eventArgsList[k] != prevEventArgsList[k] )
                            {
                                bSame = false;
                                break;
                            }
                        }

                        if ( !bSame )
                        {
                            continue;
                        }

						ULOGGER.SetProgressMessage("Removed event id %d at time %f and %d at %f.", 
							pObjectIdListEvent->GetEventId(), pObjectIdListEvent->GetTime(),
							eventList[j]->GetEventId(), eventList[j]->GetTime() );


                        cutFile.RemoveEvent( eventList[j] );
                        cutFile.RemoveEvent( pObjectIdListEvent );
                        --i;
                        break;
                    }
                }
            }
            break;
        }
    }

    // Apply an offset to all events so what started at fStartTime now starts at 0
    for ( int i = 0; i < eventList.GetCount(); ++i )
    {
        if ( eventList[i]->GetTime() > 0.0f )
        {       
            if ( eventList[i]->GetTime() > fEndTime )
            {
                eventList[i]->SetTime( fEndTime - fStartTime );
            }
            else
            {
				eventList[i]->SetTime( eventList[i]->GetTime() - fStartTime );
            }
        }

		if(eventList[i]->GetTime() < 0.0f)
		{
			eventList[i]->SetTime(0.0f);
		}
    }

	atArray<cutfEvent *> &loadEventList = const_cast<atArray<cutfEvent *> &>( cutFile.GetLoadEventList() );

	// Apply an offset to all events so what started at fStartTime now starts at 0
	for ( int i = 0; i < loadEventList.GetCount(); ++i )
	{
		if ( loadEventList[i]->GetTime() > 0.0f )
		{       
			if ( loadEventList[i]->GetTime() > fEndTime )
			{
				loadEventList[i]->SetTime( fEndTime - fStartTime );
			}
			else
			{
				loadEventList[i]->SetTime( loadEventList[i]->GetTime() - fStartTime );
			}
		}

		if(loadEventList[i]->GetTime() < 0.0f)
		{
			loadEventList[i]->SetTime(0.0f);
		}
	}

    // finally, change the total duration of the scene to match the new end time
    cutFile.SetTotalDuration( fEndTime - fStartTime );
}

//#############################################################################

void WriteConcatItem( parStreamOut *s, const char *pItemName, const char *pExtension, const SConcatFilesItem &concatFilesData )
{
    char cOutputFilename[RAGE_MAX_PATH];
    formatf( cOutputFilename, sizeof(cOutputFilename), "%s%s", concatFilesData.outputFile.c_str(), pExtension );

    atString inputFilenames( "" );
    atArray<atString> inputFilenameList;
    inputFilenameList.Reserve( concatFilesData.inputFiles.GetCount() );

    for ( int i = 0; i < concatFilesData.inputFiles.GetCount(); ++i )
    {
        char cInputFilename[RAGE_MAX_PATH];
        formatf( cInputFilename, sizeof(cInputFilename), "%s%s", concatFilesData.inputFiles[i].c_str(), pExtension );

        if ( inputFilenames.GetLength() > 0 )
        {
            inputFilenames += ",";
        }

        inputFilenames += cInputFilename;
        inputFilenameList.Append() = cInputFilename;
    }

    parElement concatItemElt;
    concatItemElt.SetName( pItemName );
    concatItemElt.AddAttribute( "Include", cOutputFilename );
    s->WriteBeginElement( concatItemElt );
    {
        parElement commaSeparatedInputsElt;
        commaSeparatedInputsElt.SetName( "Inputs" );
        s->WriteBeginElement( commaSeparatedInputsElt );
        {
            s->WriteData( inputFilenames.c_str(), -1, true, parStream::ASCII );
        }
        s->WriteEndElement( commaSeparatedInputsElt );

        for ( int i = 0; i < inputFilenameList.GetCount(); ++i )
        {
            char cEltName[16];
            sprintf( cEltName, "Input%d", i );

            parElement inputElt;
            inputElt.SetName( cEltName );
            s->WriteBeginElement( inputElt );
            {
                s->WriteData( inputFilenameList[i].c_str(), -1, true, parStream::ASCII );
            }
            s->WriteEndElement( inputElt );
        }

        if ( concatFilesData.bIsClip )
        {
            parElement storeFullPathElt;
            storeFullPathElt.SetName( "StoreFullPath" );
            s->WriteBeginElement( storeFullPathElt );
            {
                s->WriteData( concatFilesData.bIsFaceAnim ? "true" : "false" );
            }
            s->WriteEndElement( storeFullPathElt );
        }
    }
    s->WriteEndElement( concatItemElt );
}


bool WriteConcatItems( parStreamOut *s, const SConcatFilesItem &concatFilesData, 
                      const char *pInputDir, const char *pInputFaceDir, const char *pIntermediateDir, const char *pOutputDir )
{
    Displayf( concatFilesData.outputFile.c_str() );
    s->WriteComment( concatFilesData.outputFile.c_str() );

    if ( concatFilesData.bIsFaceAnim )
    {
        if ( concatFilesData.bIsClip )
        {
            SConcatFilesItem animConcatFilesData;
            animConcatFilesData.bIsFaceAnim = true;

            char cFilename[RAGE_MAX_PATH];
            safecpy( cFilename, concatFilesData.outputFile.c_str(), sizeof(cFilename) );
            ASSET.RemoveExtensionFromPath( cFilename, sizeof(cFilename), cFilename );
            safecat( cFilename, ".anim", sizeof(cFilename) );

            animConcatFilesData.outputFile = cFilename;

            animConcatFilesData.inputFiles.Reserve( concatFilesData.inputFiles.GetCount() );
            for ( int i = 0; i < concatFilesData.inputFiles.GetCount(); ++i )
            {                
                // Resolve env vars
                atString filename( concatFilesData.inputFiles[i] );
                filename.Replace( "$(inputDir)", pInputDir );
                filename.Replace( "$(inputFaceDir)", pInputFaceDir );
                filename.Replace( "$(intermediateDir)", pIntermediateDir );
                filename.Replace( "$(outputDir)", pOutputDir );

                const crClip *pClip = crClip::AllocateAndLoad( filename.c_str() );
                if ( pClip == NULL )
                {
                    Errorf( "Unable to load face animation clip '%s'.", filename.c_str() );
                    return false;
                }

                if ( pClip->GetType() != crClip::kClipTypeAnimation )
                {
                    Errorf( "The face animation clip '%s' is not of type crClip::kClipTypeAnimation.", filename.c_str() );
                    return false;
                }

                const crClipAnimation *pClipAnimation = dynamic_cast<const crClipAnimation *>( pClip );
                if ( pClipAnimation->GetAnimation() == NULL )
                {
                    Errorf( "The face animation clip '%s' does not contain an animation.", filename.c_str() );
                    return false;
                }

                atString animFilename( "" );

                if ( !ASSET.IsAbsolutePath( pClipAnimation->GetAnimation()->GetName() ) )
                {
                    animFilename = "$(outputDir)\\";
                }

                animFilename += pClipAnimation->GetAnimation()->GetName();
                animFilename.Replace( "/", "\\" );
                
                animConcatFilesData.inputFiles.Append() = animFilename;

                pClip->Release();
            }

            // write the anim concat items into the .targets file
            WriteConcatItem( s, "ConcatAnimsItem", "", animConcatFilesData );

            // write the clip concat items into the .targets file
            WriteConcatItem( s, "ConcatClipsItem", "", concatFilesData );
        }
        else
        {
            // write the clip concat items into the .targets file
            WriteConcatItem( s, "ConcatAnimsItem", "", concatFilesData );
        }
    }
    else
    {
        // write the anim concat items into the .targets file
        WriteConcatItem( s, "ConcatAnimsItem", ".anim", concatFilesData );
        
        if ( concatFilesData.bIsClip )
        {
            // write the clip concat items into the .targets file
            WriteConcatItem( s, "ConcatClipsItem", ".clip", concatFilesData );
        }
    }

    return true;
}

void FindEventsOfType(const atArray<cutfEvent *>& eventList, atArray<cutfEvent *>& events, ECutsceneEvent type)
{
	for(int i=0; i < eventList.GetCount(); ++i)
	{
		if(eventList[i]->GetEventId() == type)
		{
			events.PushAndGrow(eventList[i]);
		}
	}
}

//#############################################################################
