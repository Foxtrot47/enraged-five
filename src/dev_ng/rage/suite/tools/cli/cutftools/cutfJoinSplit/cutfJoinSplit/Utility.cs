﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;

namespace cutfJoinSplit
{
    class Part
    {
        public string strName = String.Empty;
        public string strType = String.Empty;
        public string strCutFilename = String.Empty;
        public string strFolder = String.Empty;
        public int iRangeStart = 0;
        public int iRangeEnd = 0;
    }

    class Utility
    {
        private static IUniversalLog log = null;
        private static IUniversalLogTarget logTarget = null;
        private static string strLogFilename = String.Empty;

        public static void OpenLogFile(string strLog)
        {
            if (String.IsNullOrEmpty(strLog)) return;

            strLogFilename = strLog;

            LogFactory.Initialize();
            log = LogFactory.CreateUniversalLog(strLogFilename);
            logTarget = LogFactory.CreateUniversalLogFile(log);
        }

        public static void CloseLogFile()
        {
            if (logTarget != null && log != null)
            {
                logTarget.Flush();
                logTarget.Disconnect(log);
            }
        }

        public static void LogMessage(string strLine)
        {
            Console.WriteLine(strLine);

            strLine = strLine.Replace("\\", "/"); // Easier to copy lines out to debug in commandline
            if (logTarget != null && log != null)
            {
                lock (log)
                {
                    if (strLine.StartsWith("ERROR:"))
                    {
                        log.Error(strLine.Substring(7));
                    }
                    else if (strLine.StartsWith("WARNING:"))
                    {
                        log.Warning(strLine.Substring(9));
                    }
                    else if (strLine.StartsWith("DEBUG:"))
                    {
                        log.Debug(strLine.Substring(7));
                    }
                    else
                    {
                        log.Message(strLine);
                    }
                }
            }
        }

        public static bool GetParts(ArrayList lstParts, string strCutXMLFile, string strAssetDir)
        {
            lstParts.Clear();
            Utility.LogMessage("Processing parts : " + strCutXMLFile);

            try
            {
                using (XmlTextReader textReader = new XmlTextReader(strCutXMLFile))
                {
                    if (textReader.ReadToFollowing("parts"))
                    {
                        XmlReader objectReader = textReader.ReadSubtree();

                        while (objectReader.ReadToFollowing("Item"))
                        {
                            Part part = new Part();

                            XmlReader subItemReader = objectReader.ReadSubtree();

                            if (subItemReader.ReadToFollowing("name"))
                            {
                                part.strName = subItemReader.ReadInnerXml();
                            }

                            if (subItemReader.ReadToFollowing("filename"))
                            {
                                part.strCutFilename = subItemReader.ReadInnerXml();
                                part.strCutFilename = part.strCutFilename.Replace("$(assets)", strAssetDir);

                                part.strFolder = part.strCutFilename.Replace(Path.GetExtension(part.strCutFilename), "");
                            }

                            lstParts.Add(part);
                        }

                        Utility.LogMessage("Processed " + lstParts.Count + " parts");
                    }
                    
                    textReader.Close();
                }
            }
            catch (Exception e)
            {
                Utility.LogMessage("ERROR: Exception thrown: " + e.Message);
                return false;
            }

            return true;
        }

        public static void CreateDirectory(string strDirectory)
        {
            if (!Directory.Exists(strDirectory))
            {
                Directory.CreateDirectory(strDirectory);
            }
        }

        public static bool RunProcess(string strExe, string strArguments, out string strOutput, bool bWaitForExit)
        {
            Utility.LogMessage(strExe + " " + strArguments);

            try
            {
                System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                pExe.StartInfo.FileName = strExe;
                pExe.StartInfo.Arguments = strArguments;
                pExe.StartInfo.CreateNoWindow = true;
                pExe.StartInfo.UseShellExecute = false;
                pExe.StartInfo.RedirectStandardOutput = true;
                pExe.Start();

                strOutput = String.Empty;
                if (bWaitForExit)
                {
                    strOutput = pExe.StandardOutput.ReadToEnd();
                    pExe.WaitForExit();
                    
                    //Utility.OpenLogFile();

                    Utility.LogMessage("Exit Code: " + pExe.ExitCode);
                    Utility.LogMessage("Output" + strOutput);

                    if (pExe.ExitCode != 0)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                Utility.LogMessage("ERROR: " + e.Message + " :" + strExe);
            }

            strOutput = String.Empty;
            return false;
        }
    }
}
