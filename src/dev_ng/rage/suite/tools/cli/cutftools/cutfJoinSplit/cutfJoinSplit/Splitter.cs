﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;
using System.IO;

namespace cutfJoinSplit
{
    class Splitter
    {
        private string strClipEditPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/cutscene/clipedit.exe";
        private string strAnimEditPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/cutscene/animedit.exe";
        private string strCutfStreamCommand = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/cutscene/cutfstream.exe";
        private string strCutfSectionCommand = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/cutscene/cutfsection_n.exe";
        private ArrayList lstParts = new ArrayList();
        private string strMainDestinationFolder = String.Empty;

        public Splitter()
        {
        }

        ~Splitter()
        {
            Utility.CloseLogFile();
        }

        private void ReadRanges()
        {
            for (int i = 0; i < lstParts.Count; ++i)
            {
                Part p = (Part)lstParts[i];

                using (XmlTextReader textReader = new XmlTextReader(p.strCutFilename))
                {
                    if (textReader.ReadToFollowing("iRangeStart"))
                    {
                        if (textReader.HasAttributes)
                        {
                            p.iRangeStart = (int)Convert.ToInt32(textReader.GetAttribute(0));
                        }
                    }

                    if (textReader.ReadToFollowing("iRangeEnd"))
                    {
                        if (textReader.HasAttributes)
                        {
                            p.iRangeEnd = (int)Convert.ToInt32(textReader.GetAttribute(0));
                        }
                    }

                    textReader.Close();
                }
            }
        }

        private bool SplitAnimation(string strSourceDir, string strAdditionalDir, string strExtension)
        {
            Utility.CreateDirectory(strSourceDir);

            string[] files = Directory.GetFiles(strSourceDir, strExtension);

            for (int i = 0; i < files.Length; ++i)
            {
                float fStartTime = 0.0f;

                for (int j = 0; j < lstParts.Count; ++j)
                {
                    Part p = (Part)lstParts[j];

                    string strDestinationFolder = p.strFolder + "/" + strAdditionalDir;

                    Utility.CreateDirectory(strDestinationFolder);

                    float fAnimationLength = ((p.iRangeEnd - p.iRangeStart) + 1) / 30.0f;

                    FileInfo fiAnimationFile = new FileInfo(files[i]);

                    string strOutput = String.Empty;
                    string strArguments = "-anim=" + files[i] + " -out=" + strDestinationFolder + "/" + fiAnimationFile.Name + " -op crop=" + fStartTime + "," + (fStartTime + fAnimationLength);
                    if (!Utility.RunProcess(strAnimEditPath, strArguments, out strOutput, true))
                    {
                        Utility.LogMessage("ERROR: Failed to split animation file: " + fiAnimationFile.Name + " / " + strDestinationFolder + "/" + fiAnimationFile.Name);
                        return false;
                    }

                    fStartTime += fAnimationLength;
                }
            }

            return true;
        }

        private bool SplitClip(string strSourceDir, string strAdditionalDir, string strExtension)
        {
            string[] files = Directory.GetFiles(strSourceDir, strExtension);

            for (int i = 0; i < files.Length; ++i)
            {
                float fStartTime = 0.0f;

                for (int j = 0; j < lstParts.Count; ++j)
                {
                    Part p = (Part)lstParts[j];

                    string strDestinationFolder = p.strFolder + "/" + strAdditionalDir;

                    Utility.CreateDirectory(strDestinationFolder);

                    float fClipLength = ((p.iRangeEnd - p.iRangeStart) + 1) / 30.0f;

                    FileInfo fiClipFile = new FileInfo(files[i]);

                    string strOutput = String.Empty;
                    string strArguments = "-clip=" + files[i] + " -out=" + strDestinationFolder + "/" + fiClipFile.Name + " -clipanimation=" + fStartTime + "," + (fStartTime + fClipLength);
                    if (!Utility.RunProcess(strClipEditPath, strArguments, out strOutput, true))
                    {
                        Utility.LogMessage("ERROR: Failed to split clip file: " + fiClipFile.Name + " / " + strDestinationFolder + "/" + fiClipFile.Name);
                        return false;
                    }

                    fStartTime += fClipLength;
                }
            }

            return true;
        }

        private void SplitToybox(string strSourceDir)
        {
            string[] files = Directory.GetFiles(strSourceDir);

            for (int i = 0; i < files.Length; ++i)
            {
                string strToyboxFile = files[i];

                bool bLoadList = false;
                bool bDropList = false;

                List<int> lstLoadList = new List<int>();
                List<int> lstDropList = new List<int>();

                System.IO.StreamReader file = new System.IO.StreamReader(strToyboxFile);
                if (file != null)
                {
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        if (line == "[Loaded]")
                        {
                            bLoadList = true;
                            continue;
                        }

                        if (line == "[Dropped]")
                        {
                            bLoadList = false;
                            bDropList = true;
                            continue;
                        }

                        if (bLoadList == true)
                        {
                            if (line != String.Empty && line != "0")
                            {
                                lstLoadList.Add(Convert.ToInt32(line));
                            }
                        }

                        if (bDropList == true)
                        {
                            if (line != String.Empty)
                            {
                                lstDropList.Add(Convert.ToInt32(line));
                            }
                        }
                    }

                    file.Close();
                }

                for (int j = 0; j < lstParts.Count; ++j)
                {
                    Part p = (Part)lstParts[j];

                    string strToyboxDir = p.strFolder + "\\toybox";
                    Utility.CreateDirectory(strToyboxDir);

                    FileInfo fiFile = new FileInfo(strToyboxFile);

                    System.IO.StreamWriter file2 = new System.IO.StreamWriter(strToyboxDir + "\\" + fiFile.Name);

                    file2.WriteLine("[Loaded]");
                    file2.WriteLine("0");

                    foreach (int entry in lstLoadList)
                    {
                        if (entry >= p.iRangeStart && entry <= p.iRangeEnd)
                        {
                            file2.WriteLine((entry - p.iRangeStart).ToString());
                        }
                    }

                    file2.WriteLine("");
                    file2.WriteLine("[Dropped]");

                    foreach (int entry in lstDropList)
                    {
                        if (entry >= p.iRangeStart && entry <= p.iRangeEnd)
                        {
                            file2.WriteLine((entry - p.iRangeStart).ToString());
                        }
                    }

                    file2.Close();
                }
            }
        }

        private bool CopyCutFile()
        {
            for (int i = 0; i < lstParts.Count; ++i)
            {
                Part p = (Part)lstParts[i];

                string strDestinationFolder = p.strFolder;

                try
                {
                    File.Copy(p.strCutFilename, (strDestinationFolder + "/data.cutxml"), true);
                    File.SetAttributes((strDestinationFolder + "/data.cutxml"), FileAttributes.Normal);
                }
                catch (Exception e)
                {
                    Utility.LogMessage("ERROR: Exception thrown: " + e.Message);
                    return false;
                }
            }

            return true;
        }

        public bool GetDestinationFolder(string strCutXMLFile)
        {
            try
            {
                using (XmlTextReader textReader = new XmlTextReader(strCutXMLFile))
                {
                    if (textReader.ReadToFollowing("path"))
                    {
                        strMainDestinationFolder = textReader.ReadInnerXml();
                    }

                    textReader.Close();
                }
            }
            catch (Exception e)
            {
                Utility.LogMessage("ERROR: Exception thrown: " + e.Message);
                return false;
            }

            return (strMainDestinationFolder != String.Empty);
        }

        public bool GetPartFilesToSplit(ArrayList lstPartFiles, string strConcatList)
        {
            lstPartFiles.Clear();

            try
            {
                using (XmlTextReader textReader = new XmlTextReader(strConcatList))
                {
                    if (textReader.ReadToFollowing("parts"))
                    {
                        XmlReader objectReader = textReader.ReadSubtree();

                        while (objectReader.ReadToFollowing("Item"))
                        {
                            XmlReader subItemReader = objectReader.ReadSubtree();

                            if (subItemReader.ReadToFollowing("filename"))
                            {
                                string filename = subItemReader.ReadInnerXml();
                                filename = Path.GetDirectoryName(filename) + "\\data.cutpart";

                                bool bFound=false;
                                for (int i = 0; i < lstPartFiles.Count; ++i)
                                {
                                    if ((string)lstPartFiles[i] == filename)
                                        bFound = true;
                                }

                                if (!bFound)
                                {
                                    Utility.LogMessage("Found part: " + filename);
                                    lstPartFiles.Add(filename);
                                }
                            }
                        }
                    }

                    textReader.Close();
                }
            }
            catch (Exception e)
            {
                Utility.LogMessage("ERROR: Exception thrown: " + e.Message);
                return false;
            }

            return true;
        }

        private bool DeletePartFolder(string strFolder)
        {
            Utility.LogMessage("Deleting part folder: " + strFolder);

            try
            {
                if (Directory.Exists(strFolder))
                {
                    Directory.Delete(strFolder, true);
                }
            }
            catch (Exception e)
            {
                Utility.LogMessage("ERROR: Exception thrown: " + e.Message);
                return false;
            }

            return true;
        }

        public int SplitPartFile(string strCutPart, string strAssetDir)
        {
            if (!GetDestinationFolder(strCutPart))
            {
                Utility.LogMessage("ERROR: Unable to retrieve the destination folder");
                return -1;
            }

            if (!Utility.GetParts(lstParts, strCutPart, strAssetDir))
            {
                return -1;
            }

            for (int i = 0; i < lstParts.Count; ++i)
            {
                Part p = (Part)lstParts[i];

                if (!DeletePartFolder(p.strFolder)) { return -1; }
            }

            ReadRanges();

            SplitToybox(strMainDestinationFolder + "\\toybox\\");
            if (!SplitAnimation(strMainDestinationFolder, "", "*.anim")) return -1;
            if (!SplitClip(strMainDestinationFolder, "", "*.clip")) return -1;
            if (!SplitAnimation(strMainDestinationFolder + "\\faces", "faces", "*.anim")) return -1;
            if (!SplitClip(strMainDestinationFolder + "\\faces", "faces", "*.clip")) return -1;
            if (!CopyCutFile()) return -1;

            if (!BuildStream()) return -1;

            return 0;
        }

        public int Process(string strCutList, string strAssetDir)
        {
            Utility.OpenLogFile(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\logs\\cutscene\\process\\cutfjoinsplit_splitter.ulog");
            Utility.LogMessage("Processing : " + strCutList);

            if (Path.GetExtension(strCutList) == ".cutpart")
            {
                return SplitPartFile(strCutList, strAssetDir);
            }
            else if (Path.GetExtension(strCutList) == ".concatlist")
            {
                ArrayList lstPartFiles = new ArrayList();
                if (!GetPartFilesToSplit(lstPartFiles, strCutList))
                {
                    return -1;
                }

                for (int i = 0; i < lstPartFiles.Count; ++i)
                {
                    if (SplitPartFile((string)lstPartFiles[i], strAssetDir) == -1)
                    {
                        return -1;
                    }
                }
                return 0;
            }

            Utility.LogMessage("File not recognised: " + strCutList);

            return -1;
        }

        private bool BuildStream()
        {
            for (int i = 0; i < lstParts.Count; ++i)
            {
                Part p = (Part)lstParts[i];

                // Need this incase there is no toybox so we can still concat
                Utility.LogMessage(String.Format("Copying {0} to {1}", p.strFolder + "/data.cutxml", p.strFolder + "/data_stream.cutxml"));
                try
                {
                    File.Copy(p.strFolder + "/data.cutxml", p.strFolder + "/data_stream.cutxml", true);
                }
                catch (Exception e)
                {
                    Utility.LogMessage("ERROR: Exception during copy: " + e.Message);
                }

                // StreamCutfile - modify the cutfile to optimise for streaming using the toybox folder
		        // Buffer is only applied to data from the toybox folder
                string strArguments = String.Format("-inputDir \"{0}/toybox\" -destDir \"{0}\" -cutFile \"{0}/data_stream.cutxml\" -timeBuffer 0 -output -hideAlloc -ulog", p.strFolder);
                string strOutput = String.Empty;
                if (!Utility.RunProcess(strCutfStreamCommand, strArguments, out strOutput, true))
                {
                    Utility.LogMessage("ERROR: Errors in cutfstream process building: " + p.strFolder);
                    Utility.LogMessage("ERROR: Output: " + strOutput);
                    return false;
                }

                // SectionCutfile - Crop the cut file data to the time range, and save it as *.cutbin
                strArguments = String.Format("-inputDir \"{0}\" -cutsceneFile \"{0}/data_stream.cutxml\" -method 0 -duration 0.0 -nopopups -usevsoutput -hideAlloc -ulog -saveStreamOnly", p.strFolder);
                strOutput = String.Empty;
                if (!Utility.RunProcess(strCutfSectionCommand, strArguments, out strOutput, true))
                {
                    Utility.LogMessage("ERROR: Errors in cutfsection process building: " + p.strFolder);
                    Utility.LogMessage("ERROR: Output: " + strOutput);
                    return false;
                }
            }

            return true;
        }

    }
}
