﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.OS;
using RSG.Base.Configuration;

namespace cutfJoinSplit
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_JOINER = "joiner";
        private static readonly String OPTION_SPLITTER = "splitter";
        private static readonly String OPTION_BUILDCUTONLY = "buildcutonly";
        private static readonly String OPTION_ASSETDIR = "assetdir";
        private static readonly String OPTION_CUTPART = "cutpart";
        #endregion // Constants

        static int Main(string[] args)
        {
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_JOINER, LongOption.ArgType.None,
                    "Joiner option."),
                new LongOption(OPTION_SPLITTER, LongOption.ArgType.None,
                    "Splitter option."),
                new LongOption(OPTION_BUILDCUTONLY, LongOption.ArgType.None,
                    "Build cut file only option."),
                new LongOption(OPTION_CUTPART, LongOption.ArgType.Required,
                    "Cut part filename."),
                new LongOption(OPTION_ASSETDIR, LongOption.ArgType.Required,
                    "Asset Directory."),
                };

                CommandOptions options = new CommandOptions(args, opts);

                bool joiner = options.ContainsOption(OPTION_JOINER);
                bool splitter = options.ContainsOption(OPTION_SPLITTER);
                bool buildcutonly = options.ContainsOption(OPTION_BUILDCUTONLY);
                String assetDir = options[OPTION_ASSETDIR] as String;
                String cutPart = options[OPTION_CUTPART] as String;

                if (joiner)
                {
                    Joiner j = new Joiner();
                    return j.Process(cutPart, assetDir, buildcutonly);
                }
                else if (splitter)
                {
                    Splitter s = new Splitter();
                    return s.Process(cutPart, assetDir);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }

            return -1;

            //Joiner j = new Joiner();
            //j.Process(@"X:\gta5\assets\cuts\mike\data.cutlist");

            //Splitter s = new Splitter();
            //s.Process(@"X:\gta5\assets\cuts\mike\data.cutlist");
        }
    }
}
