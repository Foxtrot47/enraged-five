﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;
using System.IO;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;

namespace cutfJoinSplit
{
    class DataBank
    {
        public string strDestinationFolder = String.Empty;
        public Dictionary<string, ArrayList> dictAnimations = new Dictionary<string, ArrayList>();
        public Dictionary<string, ArrayList> dictClips = new Dictionary<string, ArrayList>();
        public Dictionary<string, ArrayList> dictFaceAnimations = new Dictionary<string, ArrayList>();
        public Dictionary<string, ArrayList> dictFaceClips = new Dictionary<string, ArrayList>();
    }

    class Joiner
    {
        private string strAnimConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/animconcat.exe";
        private string strClipConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/cutscene/cutfclipconcat.exe";
        private string strCutXMLConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/cutscene/cutfconcat.exe";
        private string strCutfStreamCommand = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/cutscene/cutfstream.exe";
        private string strCutfSectionCommand = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/cutscene/cutfsection_n.exe";
        private ArrayList lstParts = new ArrayList();
        private DataBank dbAnimationClipData = new DataBank();
        private ArrayList lstObjectNames = new ArrayList();
        private string strMainDestinationFolder = String.Empty;
        private string strAudio = String.Empty;

        public Joiner()
        {

        }

        ~Joiner()
        {
            Utility.CloseLogFile();
        }

        private bool ReadObjects(string strCutXMLFile)
        {
            lstObjectNames.Clear();

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(strCutXMLFile);

            XmlNodeList objectNodes = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/pCutsceneObjects/Item");

            foreach (XmlNode node in objectNodes)
            {
                foreach (XmlNode child in node.ChildNodes)
                {
                    if (child.Name == "cName")
                    {
                        string objectName = child.InnerText;

                        int index = objectName.IndexOf(":");
                        if (index != -1)
                        {
                            objectName = objectName.Substring(0, index);
                        }

                        objectName = objectName.Replace(" ", "_");

                        lstObjectNames.Add(objectName);
                    }
                }
            }

            return lstObjectNames.Count > 0;
        }

        private void FillDictionary(Dictionary<string, ArrayList> dict, string strFolder, string strExtension)
        {
            if (!Directory.Exists(strFolder)) return;

            string[] lstFiles = Directory.GetFiles(strFolder, strExtension);
            for (int j = 0; j < lstFiles.Length; ++j)
            {
                FileInfo file = new FileInfo(lstFiles[j]);

                string filename = Path.GetFileNameWithoutExtension(file.Name);
                int index = filename.IndexOf("_face");
                if (index != -1)
                {
                    filename = filename.Substring(0, index);
                }

                bool bFound=false;
                for (int i = 0; i < lstObjectNames.Count; ++i)
                {
                    if (String.Compare((string)lstObjectNames[i], filename, true) == 0)
                    //if (((string)lstObjectNames[i]).ToLower() == filename.ToLower())
                        bFound = true;
                }

                if (!bFound)
                {
                    Utility.LogMessage("WARNING: Ignoring '" + file + "' which exists but is not referenced in the cutxml");
                    continue;
                }

                if (dict.ContainsKey(file.Name.ToLower()))
                {
                    ArrayList lst = dict[file.Name.ToLower()];
                    lst.Add(lstFiles[j]);
                }
                else
                {
                    ArrayList lst = new ArrayList();
                    lst.Add(lstFiles[j]);

                    dict[file.Name.ToLower()] = lst;
                }
            }
        }

        private bool GetData()
        {
            for (int i = 0; i < lstParts.Count; ++i)
            {
                Part p = (Part)lstParts[i];

                if (!ReadObjects(p.strCutFilename)) return false;

                FillDictionary(dbAnimationClipData.dictAnimations, p.strFolder, "*.anim");
                FillDictionary(dbAnimationClipData.dictClips, p.strFolder, "*.clip");
                FillDictionary(dbAnimationClipData.dictFaceAnimations, (p.strFolder + "/faces"), "*.anim");
                FillDictionary(dbAnimationClipData.dictFaceClips, (p.strFolder + "/faces"), "*.clip");

                if (dbAnimationClipData.dictAnimations.Count != dbAnimationClipData.dictClips.Count)
                {
                    Utility.LogMessage("ERROR: Number of animations/clips mismatch in part: " + p.strName);
                    return false;
                }

                if (dbAnimationClipData.dictFaceAnimations.Count != dbAnimationClipData.dictFaceClips.Count)
                {
                    Utility.LogMessage("ERROR: Number of face animations/clips mismatch in part: " + p.strName);
                    return false;
                }
            }

            return true;
        }

        private bool ValidateDictionary(Dictionary<string, ArrayList> dict)
        {
            foreach (KeyValuePair<string, ArrayList> entry in dict)
            {
                if (entry.Value.Count != lstParts.Count)
                {
                    Utility.LogMessage("ERROR: Number of Files/Parts mismatch - missing files from a Part?");
                    return false;
                }
            }

            return true;
        }

        private bool ValidateFiles(Dictionary<string, ArrayList> dict)
        {
            foreach (KeyValuePair<string, ArrayList> entry in dict)
            {
                ArrayList lstFiles = entry.Value;

                foreach (string file in lstFiles)
                {
                    if (!File.Exists(file))
                    {
                        Utility.LogMessage("ERROR: File '" + file + "' does not exist.");
                        return false;
                    }
                }
            }

            return true;
        }

        public bool ValidateData()
        {
            if (lstParts.Count == 0)
            {
                Utility.LogMessage("ERROR: No parts have being specified");
                return false;
            }

            for (int i = 0; i < lstParts.Count; ++i)
            {
                Part p = (Part)lstParts[i];

                if (!Directory.Exists(p.strFolder))
                {
                    Utility.LogMessage("ERROR: Folder '" + p.strFolder + "' does not exist");
                    return false;
                }
            }

            if (!Directory.Exists(strMainDestinationFolder))
            {
                Utility.LogMessage("ERROR: Folder '" + strMainDestinationFolder + "' does not exist");
                return false;
            }

            // Check we have same number of animations as parts - if this fails a folder is missing an animation
            if (!ValidateDictionary(dbAnimationClipData.dictAnimations)) return false;
            if (!ValidateDictionary(dbAnimationClipData.dictClips)) return false;
            if (!ValidateDictionary(dbAnimationClipData.dictFaceAnimations)) return false;
            if (!ValidateDictionary(dbAnimationClipData.dictFaceClips)) return false;

            if (!ValidateFiles(dbAnimationClipData.dictAnimations)) return false;
            if (!ValidateFiles(dbAnimationClipData.dictClips)) return false;
            if (!ValidateFiles(dbAnimationClipData.dictFaceAnimations)) return false;
            if (!ValidateFiles(dbAnimationClipData.dictFaceClips)) return false;

            return true;
        }

        public bool GetAudio(string strCutXMLFile)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strCutXMLFile);

                XmlNode node = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/audio");

                strAudio = node.InnerText;
            }
            catch (Exception e)
            {
                Utility.LogMessage("ERROR: Exception thrown: " + e.Message);
                return false;
            }

            return true;
        }

        public bool GetDestinationFolder(string strCutXMLFile, string strAssetDir)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strCutXMLFile);

                XmlNode node = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/path");

                strMainDestinationFolder = node.InnerText;
                strMainDestinationFolder = strMainDestinationFolder.Replace("$(assets)", strAssetDir);
            }
            catch (Exception e)
            {
                Utility.LogMessage("ERROR: Exception thrown: " + e.Message);
                return false;
            }

            return (strMainDestinationFolder != String.Empty);
        }

        public int Process(string strCutList, string strAssetDir, bool bBuildJustCutFile)
        {
            Utility.OpenLogFile(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\logs\\cutscene\\process\\cutfjoinsplit_joiner.ulog");
            Utility.LogMessage("Processing : " + strCutList);

            if (!File.Exists(strCutList))
            {
                Utility.LogMessage(strCutList + " does not exist");
                return -1;
            }

            if (!GetDestinationFolder(strCutList, strAssetDir))
            {
                Utility.LogMessage("ERROR: Unable to retreive the destination folder");
                return -1;
            }

            if (!GetAudio(strCutList))
            {
                Utility.LogMessage("ERROR: Unable to retreive the audio");
                return -1;
            }

            if (!Utility.GetParts(lstParts, strCutList, strAssetDir))
            {
                Utility.LogMessage("ERROR: Unable to retreive the part data");
                return -1;
            }

            if (!bBuildJustCutFile)
            {
                if (!GetData()) { return -1; }

                if (!ValidateData()) { return -1; }

                if (!JoinAnimations(dbAnimationClipData.dictAnimations, strMainDestinationFolder)) { return -1; }

                if (!JoinClips(dbAnimationClipData.dictClips, strMainDestinationFolder)) { return -1; }

                if (!JoinAnimations(dbAnimationClipData.dictFaceAnimations, strMainDestinationFolder + "\\faces")) { return -1; }

                if (!JoinClips(dbAnimationClipData.dictFaceClips, strMainDestinationFolder + "\\faces")) { return -1; }
            }

            if (!BuildStream()) { return -1; }

            if (!JoinCutxml()){ return -1; }

            return 0;
        }

        private bool BuildStream()
        {
            Utility.LogMessage(String.Format("Building stream for {0} parts", lstParts.Count.ToString()));
            for (int i = 0; i < lstParts.Count; ++i)
            {
                Part p = (Part)lstParts[i];

                // Need this incase there is no toybox so we can still concat
                Utility.LogMessage(String.Format("Copying {0} to {1}",p.strFolder + "/data.cutxml", p.strFolder + "/data_stream.cutxml"));
                try
                {
                    File.Copy(p.strFolder + "/data.cutxml", p.strFolder + "/data_stream.cutxml", true);
                }
                catch (Exception e)
                {
                    Utility.LogMessage("ERROR: Exception during copy: " + e.Message);
                }

                // StreamCutfile - modify the cutfile to optimise for streaming using the toybox folder
                // Buffer is only applied to data from the toybox folder
                string strArguments = String.Format("-inputDir \"{0}/toybox\" -destDir \"{0}\" -cutFile \"{0}/data_stream.cutxml\" -timeBuffer 0 -output -hideAlloc -ulog", p.strFolder);
                string strOutput = String.Empty;
                if (!Utility.RunProcess(strCutfStreamCommand, strArguments, out strOutput, true))
                {
                    Utility.LogMessage("ERROR: Errors in cutfstream process building: " + p.strFolder);
                    Utility.LogMessage("ERROR: Output: " + strOutput);
                    return false;
                }

                // SectionCutfile - Crop the cut file data to the time range, and save it as *.cutbin
                strArguments = String.Format("-inputDir \"{0}\" -cutsceneFile \"{0}/data_stream.cutxml\" -method 0 -duration 0.0 -nopopups -usevsoutput -hideAlloc -ulog -saveStreamOnly", p.strFolder);
                strOutput = String.Empty;
                if (!Utility.RunProcess(strCutfSectionCommand, strArguments, out strOutput, true))
                {
                    Utility.LogMessage("ERROR: Errors in cutfsection process building: " + p.strFolder);
                    Utility.LogMessage("ERROR: Output: " + strOutput);
                    return false;
                }
            }

            return true;
        }

        private bool JoinCutxml()
        {
            string strTempCutFilename = Path.GetTempFileName();
            TextWriter twCutFileList = new StreamWriter(strTempCutFilename);

            // Create the cutfile list which is used by the cutfconcat tool
            for (int iFolder = 0; iFolder < lstParts.Count; ++iFolder)
            {
                Part p = (Part)lstParts[iFolder];
                string strFolder = p.strFolder;
                twCutFileList.WriteLine(strFolder + @"\data_stream.cutxml");
            }

            twCutFileList.Close();

            string strOutput = String.Empty;
            if (!Utility.RunProcess(strCutXMLConcatPath, "-cutlistFile=" + strTempCutFilename + " -outputDir=" + strMainDestinationFolder + " -audioFile=" + strAudio + " -output -hidealloc -ulog -internal", out strOutput, true))
            {
                Utility.LogMessage("ERROR: Failed to join cutxml file: " + strMainDestinationFolder);
                File.Delete(strTempCutFilename);
                return false;
            }

            File.Delete(strTempCutFilename);
            return true;
        }

        private bool JoinClips(Dictionary<string, ArrayList> dict, string strDestinationFolder)
        {
            Utility.CreateDirectory(strDestinationFolder);

            foreach (KeyValuePair<string, ArrayList> entry in dict)
            {
                ArrayList lstClips = entry.Value;

                if (lstClips.Count == 1)
                {
                    if (File.Exists(Path.Combine(strDestinationFolder, entry.Key))) File.SetAttributes(Path.Combine(strDestinationFolder, entry.Key), FileAttributes.Normal);
                    File.Copy((string)lstClips[0], Path.Combine(strDestinationFolder, entry.Key), true);
                }
                else
                {
                    string strOutput = String.Empty;
                    string strArguments = "-clips=" + String.Join(",", lstClips.ToArray()) + " -out=" + Path.Combine(strDestinationFolder, entry.Key) + " -ulog";
                    if (!Utility.RunProcess(strClipConcatPath, strArguments, out strOutput, true))
                    {
                        Utility.LogMessage("ERROR: Failed to join clip file: " + entry.Key + " / " + strDestinationFolder + "/" + entry.Key);
                        return false;
                    }
                }
            }

            return true;
        }

        private bool JoinAnimations(Dictionary<string, ArrayList> dict, string strDestinationFolder)
        {
            Utility.CreateDirectory(strDestinationFolder);

            foreach (KeyValuePair<string, ArrayList> entry in dict)
            {
                ArrayList lstAnimations = entry.Value;

                if (lstAnimations.Count == 1)
                {
                    if (File.Exists(Path.Combine(strDestinationFolder, entry.Key))) File.SetAttributes(Path.Combine(strDestinationFolder, entry.Key), FileAttributes.Normal);
                    File.Copy((string)lstAnimations[0], Path.Combine(strDestinationFolder,entry.Key), true);
                }
                else
                {
                    string strOutput = String.Empty;
                    string strArguments = "-anims=" + String.Join(",", lstAnimations.ToArray()) + " –absolutemover -overlap=0 -superset" + " -out=" + Path.Combine(strDestinationFolder, entry.Key) + " -ulog";
                    if (!Utility.RunProcess(strAnimConcatPath, strArguments, out strOutput, true))
                    {
                        Utility.LogMessage("ERROR: Failed to join animation file: " + entry.Key + " / " + strDestinationFolder + "/" + entry.Key);
                        return false;
                    }
                }
            }

            return true;
        }

        
    }
}
