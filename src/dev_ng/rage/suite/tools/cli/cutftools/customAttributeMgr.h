#pragma once

#include <stdio.h>
#include <string.h>
#include "atl/string.h"
#include <fstream>
#include <map>
using namespace rage;
using namespace std;

#define ATTRIBUTE_LINE_ENTRIES 2

// Handles animated custom attributes on a facial model which has being imported from max.
class CustomAttributeMgr
{
public:

	// Load a specific attribute file, this file is created by the max expression exporter when exporting expressions
	// Read in each line of the file and fill in a map, mapping the target string of the expression to the full xml driver attribute name
	// eg. con_browRaise_cl.IM_ATTRS_A.Squeeze,con_browRaise_cl_con_browRaise_cl_Squeeze
	void LoadAttributeFile(const atString& filename)
	{
		if(filename == "") return;

		std::fstream file_op(filename.c_str(), ios::in);
		if(file_op.is_open())
		{
			while(!file_op.eof()) 
			{
				char str[RAGE_MAX_PATH*2];
				file_op.getline(str,RAGE_MAX_PATH*2);
		
				std::vector<atString> vec;
				SplitString(str, vec);

				if(vec.size() != ATTRIBUTE_LINE_ENTRIES) continue;

				m_mapAttributes[vec[0]] = vec[1];
			}         
			
			file_op.close();
		}
	}

	// Look for a specific attribute by name, if found then return true and return the corresponding map entry by reference
	bool FindAttribute(atString find, atString& second)
	{
		std::map<atString, atString>::iterator it = m_mapAttributes.find(find);
		if(it == m_mapAttributes.end())
		{
			return false;
		}

		second = it->second;
		return true;
	}

private:

	void SplitString(const char* str, std::vector<atString>& vec)
	{
		char* pch = strtok ((char*)str,",");
		while (pch != NULL)
		{
			atString p(pch);
			vec.push_back(p);
			pch = strtok (NULL, ",");
		}
	}

	std::map<atString, atString>	m_mapAttributes;
};