<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">
							
	<!--Might be easier to work in straight milliseconds rather than a complex struct? -->
	
	<structdef type="TimeCode">
    <string name="m_Name" type="atString"/>
		<u32 name="m_Hour" min="0" max="24"/>
		<u32 name="m_Minute" min="0" max="59"/>
		<u32 name="m_Second" min="0" max="59"/>
		<u32 name="m_Frame" min="0" max="29"/>
	</structdef> 

	<structdef type="Reel">
		<s32 name="m_Index"/>
		<string name="m_EditName" type="atString"/>
    <string name="m_ShotName" type="atString"/>
		<string name="m_AudioOrVideo" type="atString"/>
    <array name="m_SourceTimeCode" type="atArray">
      <struct type="TimeCode"/>
    </array>
		<s32 name="m_RangeStart"/>
		<s32 name="m_RangeEnd"/> 
	</structdef> 

	<structdef type="ShotDef"> 
		<string name="m_guid" type="atString"/>
		<string name="m_CutsceneName" type="atString"/>
		<array name="m_ReelList" type="atArray">
			<struct type="Reel"/>      
		</array>
	</structdef>

</ParserSchema>
