// 
// cutftools/cutfutils.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTFTOOLS_CUTFUTILS_H
#define CUTFTOOLS_CUTFUTILS_H

#include "atl/array.h"
#include "atl/string.h"
#include "vector/quaternion.h"
#include "vector/vector3.h"

namespace rage
{
    class crClip;
    class cutfCutsceneFile2;
    class cutfEvent;
    class cutfEventArgs;
    class cutfEventDefs;
    class cutfObject;
    class fiStream;
    class parStreamOut;
	enum ECutsceneEvent;
}



//#############################################################################
// misc
//#############################################################################

struct SCutsceneSection
{
public:
	SCutsceneSection() : m_sectionIndex(-1), m_eventIndex(-1), m_startTime(0.0f), m_endTime(0.0f), m_startEvent(NULL), m_endEvent(NULL) { }

	const rage::cutfEvent* GetEvent() const
	{
		if(m_startEvent != NULL) 
			return m_startEvent;

		return m_endEvent;
	}

	int m_sectionIndex;
	int m_eventIndex;
	float m_startTime;
	float m_endTime;

	const rage::cutfEvent *m_startEvent;
	const rage::cutfEvent *m_endEvent;
};


struct SCutsceneInputItem
{
    SCutsceneInputItem()
        : pCutfile(NULL)
    {

    }

    rage::atString dir;
    rage::atString cutFilename;
    rage::atString faceDir;
    rage::cutfCutsceneFile2 *pCutfile;
};

extern rage::atString GetTempLogFileName( const char* pPrefix );
extern bool LoadCutfile( const char* pFilename, rage::cutfCutsceneFile2 &cutfile );
extern void LoadSectioningMethod( rage::cutfCutsceneFile2 &cutfile, int iMethod=-1, float fDuration=0.0f );
extern bool SaveIntermediateFiles( const char *pOutputDir, const char *pOutputCutfile, 
                                  const rage::atArray<SCutsceneInputItem> &cutsceneInputItemList );
extern bool ReadLine( rage::fiStream* pStream, char* pDest, int destLen );
extern bool WriteLine( rage::fiStream* pStream, const char* pText );

struct SCutsceneVisibilityData
{
    float fTime;
    bool bVisible;
};

extern void FilterEventsForObject( const rage::cutfCutsceneFile2 &cutfile, int iObjectId, rage::atArray<rage::cutfEvent *> &objectEventList, 
                                  int iEventId1, int iEventId2, rage::atArray<int> &eventIndexList1, rage::atArray<int> &eventIndexList2 );
extern void RemoveExtraEvents( rage::cutfCutsceneFile2 &cutfile, rage::atArray<rage::cutfEvent *> &objectEventList, 
                              int iStartIndex1, int iStartIndex2, rage::atArray<int> &indexList1, rage::atArray<int> &indexList2 );
extern void CreateVisibilityEvents( rage::cutfCutsceneFile2 &cutFile, rage::atArray<SCutsceneVisibilityData> &visibilityDataList, 
                                   int iObjectId, rage::atArray<rage::cutfEvent *> &objectEventList, 
                                   int iVisibleEventId, int iInvisibleEventId, 
                                   rage::atArray<int> &visibleEventIndexList, rage::atArray<int> &invisibleEventIndexList,
                                   rage::cutfEventArgs *pEventArgs );
extern void CreateAnimEvents( const rage::cutfObject *pObject, rage::cutfCutsceneFile2 &cutfile, 
									const rage::atArray<SCutsceneVisibilityData> &visibilityDataList, const char* pAnimName );
extern void GetCutsceneSections(const rage::cutfCutsceneFile2* pCutFile, const rage::cutfObject* pObject, 
									const rage::atArray<float>& sectionTimeList, const float offset, rage::atArray<SCutsceneSection> &sections);

extern rage::crClip* LoadClip( const rage::cutfObject *pObject, const char *pInputDir );
extern void CompositeClip( const rage::crClip *pClip, float fTime, rage::Vector3 &vPos, rage::Quaternion &quat );

//#############################################################################
// cutfcheckmethod
//#############################################################################

struct SCheckDataItem
{
    SCheckDataItem()
        : cutFilename("")
        , iRangeStart(-1)
        , iRangeEnd(-1)
        , iMethod(-1)
        , fDuration(-1.0f)
        , bIsSectioned(false)
    {

    }

    void Reset();

    rage::atString cutFilename;
    int iRangeStart;
    int iRangeEnd;
    int iMethod;
    float fDuration;
    bool bIsSectioned;
};

extern bool SaveMethodFile( const char *pCheckFilename, const rage::atArray<SCheckDataItem> &checkDataItemList );
extern bool LoadMethodFile( const char *pCheckFilename, rage::atArray<SCheckDataItem> &checkDataItemList );

//#############################################################################
// cutfcheckmethod and cutfconcat
//#############################################################################

/*extern */bool LoadCutListFile( const char *pFilename, rage::atArray<SCutsceneInputItem> &cutceneInputItemList );

//#############################################################################
// cutfanimcombine
//#############################################################################

struct SRemoveTracksItem
{
    SRemoveTracksItem();
    const char* GetInputFilename( char *pDest, int destLen ) const;
    const char* GetOutputFilename( char *pDest, int destLen ) const;

    rage::atString inputName;
    rage::atString inputDir;
    rage::atString outputName;
    rage::atString outputDir;
    rage::atString removeTracks;
};

extern void WriteRemoveTracksItem( rage::parStreamOut *s, const SRemoveTracksItem &removeTracksItem );

struct SCombineItem
{
    SCombineItem();
    const char* GetInputFilename( char *pDest, int destLen, const char *pExtension ) const;
    const char* GetOutputFilename( char *pDest, int destLen, const char *pExtension ) const;
    const char* GetInputFaceFilename( char *pDest, int destLen, const char *pExtension ) const;

    rage::atString inputName;
    rage::atString inputDir;
    
    rage::atString outputName;
    rage::atString outputDir;

    rage::atString intermediateDir;
    rage::atString inputFaceName;
};

extern void WriteCombineItem( rage::parStreamOut *s, const SCombineItem &combineItem );

//#############################################################################
// cutfanimsection and cutfconcat
//#############################################################################

struct SCropFileItem
{
    SCropFileItem();
    const char* GetInputFilenameWithoutExtension( char *pDest, int destLen ) const;
    const char* GetOutputFilenameWithoutExtension( char *pDest, int destLen ) const;
    const char* GetClipAnimFilename( char *pDest, int destLen ) const;
    
    float fStartTime;
    float fEndTime;
    bool bIsFaceAnim;
    bool bIsClip;
    bool bCanTouchInputs;
    rage::atString inputName;
    rage::atString inputDir;
    rage::atString outputName;
    rage::atString outputDir;
};

extern bool WriteCropItems( rage::parStreamOut *s, const SCropFileItem &cropFileItem );

//#############################################################################
// cutfanimcompress
//#############################################################################

struct SCompressClipItem
{
    SCompressClipItem();
    const char* GetInputFilename( char *pDest, int destLen, const char *pExtension ) const;
    const char* GetOutputClipFilename( char *pDest, int destLen ) const;
    const char* GetOutputAnimFilename( char *pDest, int destLen ) const;

    const char* GetClipListFilename( char *pDest, int destLen ) const;

    rage::atString inputName;
    rage::atString inputDir;
        
    rage::atString outputName;
    rage::atString outputDir;

    rage::atString clipListName;

    rage::atString args;
};

extern void WriteCompressClipItem( rage::parStreamOut *s, const SCompressClipItem &compressClipItem );

//#############################################################################
// cutfanimdict
//#############################################################################

struct SDictItem
{
    SDictItem();
    const char* GetListFilename( char *pDest, int destLen, const char *pExtension ) const;

    rage::atString listFile;
    rage::atString listDir;
};

extern void WriteDictItem( rage::parStreamOut *s, const char *pEltName, const char *pExtension, const SDictItem &dictItem );

//#############################################################################
// cutfsection and cutfconcat
//#############################################################################

extern void CropCutfile( rage::cutfCutsceneFile2 &cutFile, bool bCropStart, bool bCropEnd );

//#############################################################################
// cutfconcat
//#############################################################################

struct SConcatFilesItem
{
    SConcatFilesItem()
        : outputFile("")
        , bIsFaceAnim(false)
        , bIsClip(true)
    {

    }

    rage::atArray<rage::atString> inputFiles;
    rage::atString outputFile;
    bool bIsFaceAnim;
    bool bIsClip;
};

extern bool WriteConcatItems( rage::parStreamOut *s, const SConcatFilesItem &concatFilesData,
                             const char *pInputDir, const char *pInputFaceDir, const char *pIntermediateDir, const char *pOutputDir );

//#############################################################################
// cutfvisibility
//##############################################################################

struct SCutsceneVisibilityModelData
{
    rage::atString modelName;
    rage::atArray<SCutsceneVisibilityData> visibilityDataList;
};

extern bool LoadVisibilityFile( const char *pFilename, rage::atArray<SCutsceneVisibilityModelData> &visibilityModelDataList );

extern void FindEventsOfType(const rage::atArray<rage::cutfEvent *>& eventList, rage::atArray<rage::cutfEvent *>& events, rage::ECutsceneEvent type);

//##############################################################################

#endif
