// 
// /cutfeventdefedit.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE: This tool creates and edits an event def xml file, allowing you to 
//    add and remove event defs from it.  Additionally, it can generate
//    a header file with the custom event ids and other relevant information
//    for use by a project.

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "cutfile/cutfeventdef.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "system/param.h"

//#include "cutfile/customCutEvents.h"

using namespace rage;

PARAM( file, "[cutfeventdefedit] The event def file to work with." );
PARAM( headerfile, "[cutfeventdefedit] The name of the header file to create.  If the full path is not provided, the header will be saved next to the event def file." );
PARAM( headerNamespace, "[cutfeventdefedit] The name of the namespace to use in the header file." );
PARAM( add, "[cutfeventdefedit] Provide a comma-separated list of event name, rank, opposite event name.  Opposite events need to be added together, separated by a semi-colon.  Example1: Test1,last,prev.  Example2: Test2,lastUnload-5,none.  Example3: TestStart,last+1;TestEnd,last" );
PARAM( remove, "[cutfeventdefedit] Provide the event id offset or the name of the event def to remove from the file." );
PARAM( eventArgs, "[cutfeventdefedit] Provide the event args for the event def that is being added.  The specification is a name followed by a semi-colon-separated list of name-defaultValue pairs.  Example1 (where the event args already exist in the file): Test1.  Example2: Test2=key1,defaultvalue1;key2,defaultValue2.  The defaultValue will determine the data type (bool, int, float, or string)." );
PARAM( saveHeader, "[cutfeventdefedit] Whether or not to generate the header file." );

void TokenizeString( char *pString, const char *pDelimiter, atArray<ConstString> &tokens )
{
    char* pToken = strtok( pString, pDelimiter );	
    while ( pToken )
    {
        tokens.PushAndGrow( ConstString(pToken) );
        pToken = strtok( NULL, pDelimiter );
    }
}

cutfEventDef* MakeEventDef( cutfEventDefs &eventDefs, const char *pAddText, cutfEventArgsDef *pEventArgsDef )
{
    char cAddText[1024];
    safecpy( cAddText, pAddText, sizeof(cAddText) );

    atArray<ConstString> tokens;
    TokenizeString( cAddText, ",", tokens );

    if ( tokens.GetCount() < 2 )
    {
        Errorf( "Unable to parse event def from '%s'.  Too few parameters.", pAddText );

        return NULL;
    }

    const char *pName = tokens[0].c_str();

    // verify that we have a uniquely named event def
    int count = eventDefs.GetEventDefCount();
    for ( int i = 0; i < count; ++i )
    {
        if ( strcmp( pName, eventDefs.GetEventDef( i )->GetName() ) == 0 )
        {
            Errorf( "Cannot add the event def named '%s'.  An event def by that name already exists.", pName );

            return NULL;
        }
    }

    const char *pRank = tokens[1].c_str();

    int iRank = CUTSCENE_LAST_UNLOAD_EVENT_RANK;
    if ( strlen( pRank ) > 0 )
    {
        if ( isalpha( pRank[0] ) )
        {
            if ( strstr( pRank, "lastUnload" ) == &(pRank[0]) )
            {
                iRank = CUTSCENE_LAST_UNLOAD_EVENT_RANK;

                int len1 = strlen( pRank );
                int len2 = strlen( "lastUnload" );
                if ( len1 > len2 )
                {
                    bool bFoundMinus = false;
                    for ( int i = len2; i < len1; ++i )
                    {
                        if ( !bFoundMinus )
                        {
                            if ( pRank[i] == '-' )
                            {
                                bFoundMinus = true;
                            }
                        }
                        else
                        {
                            if ( isalpha( pRank[i] ) )
                            {
                                iRank -= atoi( &(pRank[i]) );
                                break;
                            }
                        }
                    }
                }
            }
            else if ( strstr( pRank, "last" ) == &(pRank[0]) )
            {
                iRank = CUTSCENE_LAST_EVENT_RANK;

                int len1 = strlen( pRank );
                int len2 = strlen( "last" );
                if ( len1 > len2 )
                {
                    bool bFoundPlus = false;
                    for ( int i = len2; i < len1; ++i )
                    {
                        if ( !bFoundPlus )
                        {
                            if ( pRank[i] == '+' )
                            {
                                bFoundPlus = true;
                            }
                        }
                        else
                        {
                            if ( isdigit( pRank[i] ) )
                            {
                                iRank += atoi( &(pRank[i]) );
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                Errorf( "Could not parse the event rank from '%s'.", pRank );

                return NULL;
            }
        }
        else if ( isdigit( pRank[0] ) )
        {
            iRank = atoi( pRank );
        }
        else
        {
            Errorf( "Could not parse the event rank from '%s'.", pRank );

            return NULL;
        }
    }

    int iOppositeIdOffset = CUTSCENE_NO_OPPOSITE_EVENT;
    if ( tokens.GetCount() >= 3 )
    {
        const char *pOppositeEvent = tokens[2].c_str();

        if ( stricmp( pOppositeEvent, "none" ) == 0 )
        {
            iOppositeIdOffset = CUTSCENE_NO_OPPOSITE_EVENT;
        }
        else if ( stricmp( pOppositeEvent, "prev" ) == 0 )
        {
            iOppositeIdOffset = CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS;
        }
        else
        {
            Errorf( "Could not parse the event's opposite from '%s'.", pOppositeEvent );

            return NULL;
        }
    }

    return rage_new cutfEventDef( -1, pName, iRank, iOppositeIdOffset, pEventArgsDef );
}

cutfEventArgsDef* MakeEventArgsDef( cutfEventDefs &eventDefs, const char *pEventArgsText )
{
    if ( (pEventArgsText == NULL) || (strlen(pEventArgsText) == 0) )
    {
        Displayf( "No event args specified.  Using Generic." );

        // look for the event args named 'Generic'
        int count = eventDefs.GetEventArgsDefCount();
        for ( int i = 0; i < count; ++i )
        {
            const cutfEventArgsDef *pEventArgsDef = eventDefs.GetEventArgsDef( i );
            if ( strcmp( "Generic", pEventArgsDef->GetName() ) == 0 )
            {
                return const_cast<cutfEventArgsDef *>( pEventArgsDef );
            }
        }

        return rage_new cutfEventArgsDef( 0, cutfEventArgs::GetTypeName( CUTSCENE_GENERIC_EVENT_ARGS_TYPE ) );
    }

    char cEventArgsText[4*1024];
    safecpy( cEventArgsText, pEventArgsText, sizeof(cEventArgsText) );

    atArray<ConstString> tokens1;
    TokenizeString( cEventArgsText, "=", tokens1 );
    
    if ( tokens1.GetCount() == 1 )
    {
        const char *pName = tokens1[0].c_str();

        // look for the event args
        int count = eventDefs.GetEventArgsDefCount();
        for ( int i = 0; i < count; ++i )
        {
            const cutfEventArgsDef *pEventArgsDef = eventDefs.GetEventArgsDef( i );
            if ( strcmp( pName, pEventArgsDef->GetName() ) == 0 )
            {
                return const_cast<cutfEventArgsDef *>( pEventArgsDef );
            }
        }

        Errorf( "Could not locate the event args named '%s'.", pName );

        return NULL;
    }
    else if ( tokens1.GetCount() == 2 )
    {
        cutfEventArgsDef *pEventArgsDef = rage_new cutfEventArgsDef( 0, tokens1[0].c_str() );

        safecpy( cEventArgsText, tokens1[1].c_str(), sizeof(cEventArgsText) );

        atArray<ConstString> tokens2;
        TokenizeString( cEventArgsText, ";", tokens2 );

        parAttributeList &atts = const_cast<parAttributeList &>( pEventArgsDef->GetAttributeList() );

        for ( int i = 0; i < tokens2.GetCount(); ++i )
        {
            char cText[256];
            safecpy( cText, tokens2[i].c_str(), sizeof(cText) );

            atArray<ConstString> tokens3;
            TokenizeString( cText, ",", tokens3 );

            // determine the data type and add the argument as a parAttribute
            int len = strlen( tokens3[1].c_str() );
            if ( (tokens3.GetCount() == 2) && (len > 0) )
            {
                const char *pAttName = tokens3[0].c_str();
                const char *pDefaultValue = tokens3[1].c_str();

                if ( isdigit( pDefaultValue[0] ) 
                    || (((pDefaultValue[0] == '-') || (pDefaultValue[0] == '+') || (pDefaultValue[0] == '.')) 
                    && (len > 1) && isdigit( pDefaultValue[1] ))  )
                {
                    if ( strstr( pDefaultValue, "." ) != NULL )
                    {
                        float fDefaultValue = (float)atof( pDefaultValue );
                        atts.AddAttribute( pAttName, fDefaultValue );
                    }
                    else
                    {
                        int iDefaultValue = atoi( pDefaultValue );
                        atts.AddAttribute( pAttName, iDefaultValue );
                    }
                }
                else if ( (stricmp( pDefaultValue, "true" ) == 0)
                    || (stricmp( pDefaultValue, "false" ) == 0) )
                {
                    bool bDefaultValue = stricmp( pDefaultValue, "true" ) == 0;
                    atts.AddAttribute( pAttName, bDefaultValue );
                }
                else
                {
                    atts.AddAttribute( pAttName, pDefaultValue );
                }
            }
            else
            {
                Errorf( "Incorrect format for a key-defaultValue pair: %s", tokens2[i].c_str() );

                return NULL;
            }
        }

        // check for name/format collision
        int count = eventDefs.GetEventArgsDefCount();
        for ( int i = 0; i < count; ++i )
        {
            const cutfEventArgsDef *pEventArgs = eventDefs.GetEventArgsDef( i );
            if ( *pEventArgsDef == *(pEventArgs) )
            {
                delete pEventArgsDef;
                
                Displayf( "The event args def '%s' already exists.  Using that instead of creating a new one.", pEventArgs->GetName() );

                return const_cast<cutfEventArgsDef *>( pEventArgs );
            }
            else if ( strcmp( pEventArgsDef->GetName(), pEventArgs->GetName() ) == 0 )
            {
                delete pEventArgsDef;
                
                Errorf( "An event args def with the name '%s' already exists.", pEventArgs->GetName() );

                return NULL;
            }
        }

        Displayf( "Added the event args named '%s'.", pEventArgsDef->GetName() );
        return pEventArgsDef;
    }
    else
    {
        Errorf( "Could not parse the event args def '%s'.", pEventArgsText );

        return NULL;
    }
}

bool AddEventDef( cutfEventDefs &eventDefs, const char *pAddText, const char *pEventArgsText )
{
    cutfEventArgsDef *pEventArgsDef = MakeEventArgsDef( eventDefs, pEventArgsText );
    if ( (pEventArgsText != NULL) && (strlen(pEventArgsText) > 0) && (pEventArgsDef == NULL) )
    {
        return false;
    }

    char cAddTokenText[1024];
    safecpy( cAddTokenText, pAddText, sizeof(cAddTokenText) );

    atArray<ConstString> addTokens;
    TokenizeString( cAddTokenText, ";", addTokens );

    if ( addTokens.GetCount() >= 2 )
    {
        cutfEventDef *pEventDef1 = MakeEventDef( eventDefs, addTokens[0].c_str(), pEventArgsDef );
        if ( pEventDef1 == NULL )
        {
            return false;
        }

        eventDefs.AddEventDef( pEventDef1 );
        Displayf( "Added the event named '%s'.", pEventDef1->GetName() );

        cutfEventDef *pEventDef2 = MakeEventDef( eventDefs, addTokens[1].c_str(), pEventArgsDef );
        if ( pEventDef2 == NULL )
        {
            return false;
        }

        eventDefs.AddEventDef( pEventDef2 );
        Displayf( "Added the event named '%s'.", pEventDef2->GetName() );

        pEventDef1->SetOppositeEventId( pEventDef2->GetEventId() );
        pEventDef2->SetOppositeEventId( pEventDef1->GetEventId() );
    }
    else if ( addTokens.GetCount() == 1 )
    {
        cutfEventDef *pEventDef1 = MakeEventDef( eventDefs, addTokens[0].c_str(), pEventArgsDef );
        if ( pEventDef1 == NULL )
        {
            return false;
        }
        
        eventDefs.AddEventDef( pEventDef1 );
        Displayf( "Added the event named '%s'.", pEventDef1->GetName() );
    }
    else
    {
        Errorf( "Incorrect format for adding an event def: %s", pAddText );

        return false;
    }
    
    return true;
}

bool RemoveEventDef( cutfEventDefs &eventDefs, const char *pRemoveText )
{
    if ( strlen( pRemoveText ) > 0 )
    {
        if ( (pRemoveText[0] >= '0') && (pRemoveText[0] <= '9') )
        {
            int iEventIdOffset = atoi( pRemoveText );
            eventDefs.RemoveEventDef( iEventIdOffset );
        }
        else
        {
            eventDefs.RemoveEventDef( pRemoveText );
        }
    }
    else
    {
        Warningf( "Nothing was specified to remove." );
    }

    Displayf( "Removed event '%s'", pRemoveText );
    return true;
}

void WriteEventEnumLines( fiStream *pStream, cutfEventDefs &eventDefs, atArray<ConstString> &enumNameList )
{
    WriteLine( pStream, "enum ECutsceneCustomEvent {" );

    int count = eventDefs.GetEventDefCount();

    enumNameList.Reserve( count );

    for ( int i = 0; i < count; ++i )
    {
        const cutfEventDef *pEventDef = eventDefs.GetEventDef( i );

        char cEnum[256];
        sprintf( cEnum, "CUTSCENE_%s_EVENT", pEventDef->GetName() );
        strupr( cEnum );

        // check for illegal characters
        int len = strlen( cEnum );
        for ( int j = 0; j < len; ++j )
        {
            if ( !isalnum( cEnum[j] ) )
            {
                cEnum[j] = '_';
            }
        }

        enumNameList.Append() = cEnum;

        char cEnumLine[512];
        if ( i == 0 )
        {
            sprintf( cEnumLine, "\t%s = CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE,", cEnum );
        }
        else if ( i == count - 1 )
        {
            WriteLine( pStream, "\tCUTSCENE_LAST_CUSTOM_EVENT," );

            sprintf( cEnumLine, "\t%s = CUTSCENE_LAST_CUSTOM_EVENT", cEnum );
        }
        else
        {
            sprintf( cEnumLine, "\t%s,", cEnum );
        }

        WriteLine( pStream, cEnumLine );
    }

    WriteLine( pStream, "};" );
}

void WriteEventDisplayNameLines( fiStream *pStream, cutfEventDefs &eventDefs )
{
    WriteLine( pStream, "static const char* s_cutsceneEventIdDisplayNames[] = {" );

    for ( int i = 0; i < eventDefs.GetEventDefCount(); ++i )
    {
        char cLine[256];
        sprintf( cLine, "\t\"%s\"", eventDefs.GetEventDef( i )->GetName() );

        if ( i < eventDefs.GetEventDefCount() - 1 )
        {
            safecat( cLine, ",", sizeof(cLine) );
        }

        WriteLine( pStream, cLine );
    }

    WriteLine( pStream, "};" );
    WriteLine( pStream, "" );

    // write the event id display name retrieval function
    WriteLine( pStream, "// PURPOSE: Pass this function in to cutfEvent::AddEventIdDisplayNameFunc(...)." );
    WriteLine( pStream, "static inline const char* GetCutsceneCustomEventIdDisplayName( s32 iEventId )" );
    WriteLine( pStream, "{" );
    WriteLine( pStream, "\tif ( (iEventId >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) && (iEventId <= CUTSCENE_LAST_CUSTOM_EVENT) )" );
    WriteLine( pStream, "\t{" );
    WriteLine( pStream, "\t\tint iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;" );
    WriteLine( pStream, "\t\treturn s_cutsceneEventIdDisplayNames[iEventIdOffset];" );
    WriteLine( pStream, "\t}" );
    WriteLine( pStream, "" );
    WriteLine( pStream, "\treturn \"\";" );
    WriteLine( pStream, "}" );
}

void WriteEventIdRankLines( fiStream *pStream, cutfEventDefs &eventDefs, atArray<ConstString> &enumNameList )
{
    WriteLine( pStream, "static const s32 c_cutsceneCustomEventSortingRanks[] = {" );

    for ( int i = 0; i < eventDefs.GetEventDefCount(); ++i )
    {
        char cRankLine[256];
        
        const cutfEventDef *pEventDef = eventDefs.GetEventDef( i );

        int iRankOffset = pEventDef->GetRank() - CUTSCENE_LAST_EVENT_RANK;
        if ( iRankOffset >= 0 )
        {
            if ( iRankOffset == 0 )
            {
                sprintf( cRankLine, "\tCUTSCENE_LAST_EVENT_RANK, // %s", enumNameList[i].c_str() );
            }
            else
            {
                sprintf( cRankLine, "\tCUTSCENE_LAST_EVENT_RANK + %d, // %s", iRankOffset, enumNameList[i].c_str() );
            }
        }
        else
        {
            sprintf( cRankLine, "\t%d, // %s", pEventDef->GetRank(), enumNameList[i].c_str() );
        }

        WriteLine( pStream, cRankLine );
    }

    WriteLine( pStream, "};" );
    WriteLine( pStream, "" );

    // write the rank retrieval function   
    WriteLine( pStream, "// PURPOSE: Pass this function in to cutfEvent::AddEventIdRankFunc(...)." );        
    WriteLine( pStream, "static inline s32 GetCutsceneCustomEventIdRank( s32 iEventId )" );
    WriteLine( pStream, "{" );
    WriteLine( pStream, "\tif ( (iEventId >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) && (iEventId <= CUTSCENE_LAST_CUSTOM_EVENT) )" );
    WriteLine( pStream, "\t{" );
    WriteLine( pStream, "\t\tint iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;" );
    WriteLine( pStream, "\t\treturn c_cutsceneCustomEventSortingRanks[iEventIdOffset];" );
    WriteLine( pStream, "\t}" );
    WriteLine( pStream, "" );
    WriteLine( pStream, "\treturn CUTSCENE_LAST_UNLOAD_EVENT_RANK;" );
    WriteLine( pStream, "}" );
}

void WriteOppositeEventIdLines( fiStream *pStream, cutfEventDefs &eventDefs, atArray<ConstString> &enumNameList )
{
    WriteLine( pStream, "static const s32 c_cutsceneCustomOppositeEventIds[] = {" );

    for ( int i = 0; i < eventDefs.GetEventDefCount(); ++i )
    {
        const cutfEventDef *pEventDef = eventDefs.GetEventDef( i );

        char cOppositeEventIdLine[256];
        switch ( pEventDef->GetOppositeEventId() )
        {
        case CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS:
            sprintf( cOppositeEventIdLine, "\tCUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS, // %s", enumNameList[i].c_str() );
            break;
        case CUTSCENE_NO_OPPOSITE_EVENT:
            sprintf( cOppositeEventIdLine, "\tCUTSCENE_NO_OPPOSITE_EVENT, // %s", enumNameList[i].c_str() );
            break;
        default:
            if ( (pEventDef->GetOppositeEventIdOffset() >= 0) && (pEventDef->GetOppositeEventIdOffset() < eventDefs.GetEventDefCount()) )
            {
                sprintf( cOppositeEventIdLine, "\t%s,", enumNameList[pEventDef->GetOppositeEventIdOffset()].c_str() );
            }
            else
            {
                sprintf( cOppositeEventIdLine, "\t%d,", pEventDef->GetOppositeEventId() );
            }
            break;
        }

        WriteLine( pStream, cOppositeEventIdLine );
    }

    WriteLine( pStream, "};" );
    WriteLine( pStream, "" );

    // write the opposite event id retrieval function
    WriteLine( pStream, "// PURPOSE: Pass this function in to cutfEvent::AddOppositeEventIdFunc(...)." );
    WriteLine( pStream, "static inline s32 GetCutsceneCustomOppositeEventId( s32 iEventId )" );
    WriteLine( pStream, "{" );
    WriteLine( pStream, "\tif ( (iEventId >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) && (iEventId <= CUTSCENE_LAST_CUSTOM_EVENT) )" );
    WriteLine( pStream, "\t{" );
    WriteLine( pStream, "\t\tint iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;" );
    WriteLine( pStream, "\t\treturn c_cutsceneCustomOppositeEventIds[iEventIdOffset];" );
    WriteLine( pStream, "\t}" );
    WriteLine( pStream, "" );
    WriteLine( pStream, "\treturn CUTSCENE_NO_OPPOSITE_EVENT;" );
    WriteLine( pStream, "}" );
}

void WriteEventArgsEnumLines( fiStream *pStream, cutfEventDefs &eventDefs )
{
    WriteLine( pStream, "enum ECutsceneCustomEventArgsType {" );

    for ( int i = 0; i < eventDefs.GetEventArgsDefCount(); ++i )
    {
        const cutfEventArgsDef *pEventArgsDef = eventDefs.GetEventArgsDef( i );

        char cEnum[256];
        sprintf( cEnum, "CUTSCENE_%s_EVENT_ARGS_TYPE", pEventArgsDef->GetName() );
        strupr( cEnum );

        // check for illegal characters
        int len = strlen( cEnum );
        for ( int j = 0; j < len; ++j )
        {
            if ( !isalnum( cEnum[j] ) )
            {
                cEnum[j] = '_';
            }
        }

        char cEnumLine[512];
        if ( i == 0 )
        {
            sprintf( cEnumLine, "\t%s = CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE,", cEnum );
        }
        else if ( i == eventDefs.GetEventArgsDefCount() - 1 )
        {
            WriteLine( pStream, "\tCUTSCENE_LAST_CUSTOM_EVENT_ARGS_TYPE," );
            
            sprintf( cEnumLine, "\t%s = CUTSCENE_LAST_CUSTOM_EVENT_ARGS_TYPE", cEnum );
        }
        else
        {
            sprintf( cEnumLine, "\t%s,", cEnum );
        }

        WriteLine( pStream, cEnumLine );
    }


    WriteLine( pStream, "};" );
}

void WriteEventArgsTypeNameLines( fiStream *pStream, cutfEventDefs &eventDefs )
{
    WriteLine( pStream, "static const char* s_cutsceneEventArgsTypeNames[] = {" );

    for ( int i = 0; i < eventDefs.GetEventArgsDefCount(); ++i )
    {
        char cLine[256];
        sprintf( cLine, "\t\"%s\"", eventDefs.GetEventArgsDef( i )->GetName() );

        if ( i < eventDefs.GetEventDefCount() - 1 )
        {
            safecat( cLine, ",", sizeof(cLine) );
        }

        WriteLine( pStream, cLine );
    }

    WriteLine( pStream, "};" );
    WriteLine( pStream, "" );

    // write the event args type name retrieval function
    WriteLine( pStream, "// PURPOSE: Pass this function in to cutfEventArgs::AddEventArgsTypeNameFunc(...)." );
    WriteLine( pStream, "static inline const char* GetCutsceneCustomEventArgsTypeName( s32 iEventArgsType )" );
    WriteLine( pStream, "{" );
    WriteLine( pStream, "\tif ( (iEventArgsType >= CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE) && (iEventArgsType <= CUTSCENE_LAST_CUSTOM_EVENT_ARGS_TYPE) )" );
    WriteLine( pStream, "\t{" );
    WriteLine( pStream, "\t\tint iEventArgsTypeOffset = iEventArgsType - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE;" );
    WriteLine( pStream, "\t\treturn s_cutsceneEventArgsTypeNames[iEventArgsTypeOffset];" );
    WriteLine( pStream, "\t}" );
    WriteLine( pStream, "" );
    WriteLine( pStream, "\treturn \"\";" );
    WriteLine( pStream, "}" );
}

void WriteEventArgClassLines( fiStream *pStream, const cutfEventArgsDef *pEventArgsDef )
{   
    char cClassName[CUTSCENE_LONG_OBJNAMELEN * 2];
    sprintf( cClassName, "ICutscene%sCustomEventArgs", pEventArgsDef->GetName() );

    char cLine[256];
    sprintf( cLine, "class %s : public cutfEventArgs {", cClassName );
    WriteLine( pStream, cLine );
    WriteLine( pStream, "public:" );

    sprintf( cLine, "\t%s() { }", cClassName );
    WriteLine( pStream, cLine );

    sprintf( cLine, "\t~%s() { }", cClassName );
    WriteLine( pStream, cLine );

    const atArray<parAttribute> &attList = pEventArgsDef->GetAttributeList().GetAttributeArray();
    for ( int i = 0; i < attList.GetCount(); ++i )
    {
        WriteLine( pStream, "" );

        const parAttribute &att = attList[i];
        
        char cName[256];
        safecpy( cName, att.GetName(), sizeof(cName) );

        int len = strlen( cName );
        for ( int j = 0; j < len; ++j )
        {
            if ( !isalnum( cName[j] ) )
            {
                cName[j] = '_';
            }
        }

        char cDefaultValue[256];  
        const char *pDefaultValue = att.GetStringRepr( cDefaultValue, sizeof(cDefaultValue) );

        switch ( att.GetType() )
        {
        case parAttribute::BOOL:
            sprintf( cLine, "\tbool Get%s() const { return m_attributeList.FindAttributeBoolValue( \"%s\", %s ); }", cName, cName, pDefaultValue );
            break;
        case parAttribute::DOUBLE:
            sprintf( cLine, "\tfloat Get%s() const { return m_attributeList.FindAttributeFloatValue( \"%s\", %sf ); }", cName, cName, pDefaultValue );
            break;
        case parAttribute::INT64:
            sprintf( cLine, "\tint Get%s() const { return m_attributeList.FindAttributeIntValue( \"%s\", %s ); }", cName, cName, pDefaultValue );
            break;
        case parAttribute::STRING:
            {
                sprintf( cLine, "\tconst char* Get%s() const", cName );
                WriteLine( pStream, cLine );
                
                WriteLine( pStream, "\t{" );
                WriteLine( pStream, "\t\tchar buffer[256];" );
                
                sprintf( cLine, "\t\treturn m_attributeList.FindAttributeStringValue( \"%s\", \"%s\", buffer, sizeof(buffer) );", cName, pDefaultValue );
                WriteLine( pStream, cLine );

                strcpy( cLine, "\t}" );
            }
            break;
        default:
            break;
        }

        WriteLine( pStream, cLine );
    }

    WriteLine( pStream, "};" );
}

bool GenerateHeaderFile( cutfEventDefs &eventDefs )
{
    if ( strlen( eventDefs.GetHeaderFilename() ) > 0 )
    {
        char cHeaderFilename[RAGE_MAX_PATH];
        ASSET.FullPath( cHeaderFilename, sizeof(cHeaderFilename), eventDefs.GetHeaderFilename(), "" );

        if ( ASSET.FindExtensionInPath( cHeaderFilename ) == NULL )
        {
            strcat( cHeaderFilename, ".h" );
        }

        fiStream *pStream = ASSET.Create( cHeaderFilename, "" );
        if ( pStream == NULL )
        {
            Errorf( "Was not able to create '%s' for writing.", cHeaderFilename );

            return false;
        }

        char cHeaderName[RAGE_MAX_PATH];
        ASSET.RemoveExtensionFromPath( cHeaderName, sizeof(cHeaderName), ASSET.FileName( cHeaderFilename ) );

        char cHeaderDir[RAGE_MAX_PATH];
        ASSET.RemoveNameFromPath( cHeaderDir, sizeof(cHeaderDir), cHeaderFilename );
        safecpy( cHeaderDir, ASSET.FileName( cHeaderDir ), sizeof( cHeaderDir ) );

        char cFilename[RAGE_MAX_PATH];
        sprintf( cFilename, "// %s/%s.h", cHeaderDir, cHeaderName );

        char cMacro[RAGE_MAX_PATH];
        sprintf( cMacro, "%s_%s_H", cHeaderDir, cHeaderName );
        strupr( cMacro );

        char cIfndef[RAGE_MAX_PATH];
        sprintf( cIfndef, "#ifndef %s", cMacro );

        char cDefine[RAGE_MAX_PATH];
        sprintf( cDefine, "#define %s", cMacro );

        char cNamespaceStart[CUTSCENE_LONG_OBJNAMELEN + 16];
        char cNamespaceEnd[CUTSCENE_LONG_OBJNAMELEN + 16];
        if ( strlen( eventDefs.GetHeaderNamespace() ) > 0 )
        {
            sprintf( cNamespaceStart, "namespace %s {", eventDefs.GetHeaderNamespace() );
            sprintf( cNamespaceEnd, "} // namespace %s", eventDefs.GetHeaderNamespace() );
        }
        else
        {
            cNamespaceStart[0] = 0;
            cNamespaceEnd[0] = 0;
        }

        char cEndif[RAGE_MAX_PATH];
        sprintf( cEndif, "#endif // %s", cMacro );

        WriteLine( pStream, "//" );
        WriteLine( pStream, cFilename );
        WriteLine( pStream, "//" );
        WriteLine( pStream, "// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved." );
        WriteLine( pStream, "//" );
		WriteLine( pStream, "// WARNING: THIS IS AN AUTO GENERATED FILE FROM CUTFEVENTDEFEDIT. DO NOT EDIT." );
		WriteLine( pStream, "//" );
        WriteLine( pStream, "" );
        WriteLine( pStream, cIfndef );
        WriteLine( pStream, cDefine );
        WriteLine( pStream, "" );
        WriteLine( pStream, "#include \"cutfile/cutfevent.h\"" );
        WriteLine( pStream, "#include \"cutfile/cutfeventargs.h\"" );
        WriteLine( pStream, "" );
        WriteLine( pStream, "using namespace rage;" );
        WriteLine( pStream, "" );

        if ( strlen( cNamespaceStart ) > 0 )
        {
            WriteLine( pStream, cNamespaceStart );
        }

        WriteLine( pStream, "" );
        WriteLine( pStream, "//##############################################################################" );
        WriteLine( pStream, "" );

        // write the enum list
        atArray<ConstString> enumNameList;

        WriteEventEnumLines( pStream, eventDefs, enumNameList );

        WriteLine( pStream, "" );
        WriteLine( pStream, "//##############################################################################" );
        WriteLine( pStream, "" );

        // write the display name list
        WriteEventDisplayNameLines( pStream, eventDefs );

        WriteLine( pStream, "" );
        WriteLine( pStream, "//##############################################################################" );
        WriteLine( pStream, "" );

        // write the rank list
        WriteEventIdRankLines( pStream, eventDefs, enumNameList );

        WriteLine( pStream, "" );
        WriteLine( pStream, "//##############################################################################" );
        WriteLine( pStream, "" );

        // write the opposite id list
        WriteOppositeEventIdLines( pStream, eventDefs, enumNameList );

        WriteLine( pStream, "" );
        WriteLine( pStream, "//##############################################################################" );
        WriteLine( pStream, "" );

        // write the event args type enums
        WriteEventArgsEnumLines( pStream, eventDefs );

        WriteLine( pStream, "" );
        WriteLine( pStream, "//##############################################################################" );
        WriteLine( pStream, "" );

        // write the event args type names
        WriteEventArgsTypeNameLines( pStream, eventDefs );

        // write the event args classes
        int count = eventDefs.GetEventArgsDefCount();
        for ( int i = 0; i < count; ++i )
        {
            WriteLine( pStream, "" );
            WriteLine( pStream, "//##############################################################################" );
            WriteLine( pStream, "" );

            WriteEventArgClassLines( pStream, eventDefs.GetEventArgsDef( i ) );
        }

        if ( strlen( cNamespaceEnd ) > 0 )
        {
            WriteLine( pStream, "" );
            WriteLine( pStream, "//##############################################################################" );
            WriteLine( pStream, "" );
            
            WriteLine( pStream, cNamespaceEnd );
        }

        WriteLine( pStream, "" );
        WriteLine( pStream, "//##############################################################################" );
        WriteLine( pStream, "" );

        WriteLine( pStream, cEndif );

        pStream->Close();
    }
    else
    {
        Errorf( "No header filename.  Cannot generate header file." );

        return false;
    }

    Displayf( "'%s' generated.", eventDefs.GetHeaderFilename() );
    return true;
}

int Main()
{
    INIT_PARSER;

    if ( sysParam::GetArgCount() < 2 )
    {
        sysParam::Help( "=cutfeventdefedit" );

        SHUTDOWN_PARSER;
        return 1;
    }

    // get and load the event def file
    const char *pEventDefFilename;
    if ( !PARAM_file.Get( pEventDefFilename ) )
    {
        sysParam::Help( "=cutfeventdefedit" );

        SHUTDOWN_PARSER;
        return 1;
    }

    char cEventDefFilename[RAGE_MAX_PATH];
    ASSET.FullPath( cEventDefFilename, sizeof(cEventDefFilename), pEventDefFilename, "" );   

    cutfEventDefs eventDefs;
    
    if ( ASSET.Exists( cEventDefFilename, "" ) )
    {
        if ( !eventDefs.LoadFile( cEventDefFilename ) )
        {
            Errorf( "Could not load '%s'.", cEventDefFilename );

            SHUTDOWN_PARSER;
            return 1;
        }
    }
    
    bool bSaveFile = false;

    // see if a header filename was specified
    const char *pHeaderFilename = NULL;
    if ( PARAM_headerfile.Get( pHeaderFilename ) )
    {
        if ( strcmp( pHeaderFilename, eventDefs.GetHeaderFilename() ) != 0 )
        {
            eventDefs.SetHeaderFilename( pHeaderFilename );
            
            bSaveFile = true;
        }
    }

    // see if the header namespace was specified
    const char *pNamespace = NULL;
    if ( PARAM_headerNamespace.Get( pNamespace ) )
    {
        if ( strcmp( pNamespace, eventDefs.GetHeaderNamespace() ) != 0 )
        {
            eventDefs.SetHeaderNamespace( pNamespace );

            bSaveFile = true;
        }
    }

    // add event def operation
    const char *pAddText = NULL;
    if ( PARAM_add.Get( pAddText ) )
    {
        const char *pEventArgsText = NULL;
        PARAM_eventArgs.Get( pEventArgsText );

        if ( !AddEventDef( eventDefs, pAddText, pEventArgsText ) )
        {
            SHUTDOWN_PARSER;
            return 1;
        }

        bSaveFile = true;
    }

    // remove event def operation
    const char *pRemoveText = NULL;
    if ( PARAM_remove.Get( pRemoveText ) )
    {
        if ( !RemoveEventDef( eventDefs, pRemoveText ) )
        {
            SHUTDOWN_PARSER;
            return 1;
        }

        bSaveFile = true;
    }

    // save the event def file
    if ( bSaveFile )
    {
        if ( !eventDefs.SaveFile( cEventDefFilename ) )
        {
            SHUTDOWN_PARSER;
            return 1;
        }

        Displayf( "Saved '%s'.", cEventDefFilename );
    }

    if ( PARAM_saveHeader.Get() )
    {
        // set the path to the event def file's path, in case the header filename doesn't have a path.
        char cFilePath[RAGE_MAX_PATH];
        ASSET.RemoveNameFromPath( cFilePath, sizeof(cFilePath), cEventDefFilename );
        ASSET.SetPath( cFilePath );

        if ( !GenerateHeaderFile( eventDefs ) )
        {
            SHUTDOWN_PARSER;
            return 1;
        }        
    }

    SHUTDOWN_PARSER;
    return 0;
}
