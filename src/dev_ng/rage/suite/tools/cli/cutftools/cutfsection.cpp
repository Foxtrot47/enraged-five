// 
// /cutfsection.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE: The purpose of this tool is to edit the cut file down to the specified range.  This should
//  only be executed after the animations have been sectioned and the animation dictionaries built.
//  Obviously, if the "method" and/or "duration" command line arguments were specified for the 
//  cutfanimsection tool, the same values need to be provided here.

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crclip/clip.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"
#include "vectormath/legacyconvert.h"
#include "vectormath/quatv.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "RsULog/ULogger.h"

using namespace rage;

PARAM( inputDir, "[cutfsection] The location where the scene's exported animations live." );
PARAM( inputFaceDir, "[cutfsection] The location where the scene's exported face animations live.  Default:  $(inputDir)/faces" );
PARAM( intermediateDir, "[cutfsection] The location where intermediate files should be saved.  Default:  $(inputDir)/obj" );
PARAM( cutscenefile, "[cutfsection] The cut file to create an MSBuild targets file for.  Default:  $(inputDir)/data.cutxml" );
PARAM( outputCutfile, "[cutfsection] The cut file to save the edits to.  Default:  $(inputDir).cutbin" );
PARAM( method, "[cutfsection] Override the sectioning method contained in the cut file:  0=None, 1=Time Slice, 2=Camera Cuts, 3=Section Splits." );
PARAM( duration, "[cutfsection] Overrides the duration (in seconds) for the Time Slice method of sectioning." );
PARAM( saveStreamOnly, "[cutfsection] Save stream only, dont save scene name cutxml or cutbin files." );
PARAM( ulog, "[cutfsection] Universal logging." );

void AddCameraEvents(cutfCutsceneFile2* pCutfile)
{
	//if(pCutfile->GetConcatDataList().GetCount() > 0) return; // dont apply to concat file

	pCutfile->SortEvents();

	const atArray<cutfEvent *>& loadEvents = pCutfile->GetEventList();

	if(pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_FLAG))
	{
		for(int i=0; i < loadEvents.GetCount(); ++i)
		{
			cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent*>(loadEvents[i]);
			if(pEvent)
			{
				if(pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
				{
					cutfObjectIdEventArgs* pNewEventArgs = rage_new cutfObjectIdEventArgs(pEvent->GetObjectId());
					cutfObjectIdEvent* pNewEvent = rage_new cutfObjectIdEvent(pEvent->GetObjectId(), pEvent->GetTime(), CUTSCENE_ENABLE_DOF_EVENT, pNewEventArgs);
					pCutfile->AddEvent(pNewEvent);
					break;
				}
			}
		}
	}

	if(pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_SECOND_CUT_FLAG))
	{
		int iCount=0;
		for(int i=0; i < loadEvents.GetCount(); ++i)
		{
			cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent*>(loadEvents[i]);
			if(pEvent)
			{
				if(pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
				{
					iCount++;

					if(iCount == 2) // Second cut
					{
						cutfObjectIdEventArgs* pNewEventArgs = rage_new cutfObjectIdEventArgs(pEvent->GetObjectId());
						cutfObjectIdEvent* pNewEvent = rage_new cutfObjectIdEvent(pEvent->GetObjectId(), pEvent->GetTime(), CUTSCENE_ENABLE_DOF_EVENT, pNewEventArgs);
						pCutfile->AddEvent(pNewEvent);
						break;
					}
				}
			}
		}
	}

	if(pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_END_FLAG))
	{
		for(int i=loadEvents.GetCount()-1; i > -1; --i) // Backwards
		{
			cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent*>(loadEvents[i]);
			if(pEvent)
			{
				if(pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
				{
					cutfObjectIdEventArgs* pNewEventArgs = rage_new cutfObjectIdEventArgs(pEvent->GetObjectId());
					cutfObjectIdEvent* pNewEvent = rage_new cutfObjectIdEvent(pEvent->GetObjectId(), pEvent->GetTime(), CUTSCENE_DISABLE_DOF_EVENT, pNewEventArgs);
					pCutfile->AddEvent(pNewEvent);
					break;
				}
			}
		}
	}

	if(pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_USE_CATCHUP_CAMERA_FLAG))
	{
		for(int i=loadEvents.GetCount()-1; i > -1; --i) // Backwards
		{
			cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent*>(loadEvents[i]);
			if(pEvent)
			{
				if(pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
				{
					cutfObjectIdEventArgs* pNewEventArgs = rage_new cutfObjectIdEventArgs(pEvent->GetObjectId());
					cutfObjectIdEvent* pNewEvent = rage_new cutfObjectIdEvent(pEvent->GetObjectId(), pEvent->GetTime(), CUTSCENE_CATCHUP_CAMERA_EVENT, pNewEventArgs);
					pCutfile->AddEvent(pNewEvent);
					break;
				}
			}
		}
	}
}

void Init()
{
	INIT_PARSER;
	cutfEvent::InitClass();
	crAnimation::InitClass();
	crClip::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("cutfsection").c_str(), "cutfsection");
	}
}

void Shutdown()
{
	cutfEvent::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();        
	SHUTDOWN_PARSER;

	ULOGGER.PostExport();
}

void FixupDecals(cutfCutsceneFile2* pCutfile)
{
	char cProjRoot[RAGE_MAX_PATH];
	sysGetEnv("RS_BUILDBRANCH", cProjRoot, RAGE_MAX_PATH);

	char cScriptFXFile[RAGE_MAX_PATH];
	sprintf(cScriptFXFile, "%s\\common\\data\\effects\\decals_cs.dat", cProjRoot );

	std::string line;
	std::ifstream infile;
	infile.open (cScriptFXFile);

	atMap<atString, int> decalMap;
	if(infile.is_open())
	{
		while (!infile.eof()) 
		{
			std::getline(infile,line); 

			atString strLine(line.c_str());
			strLine.Trim();
			strLine.Lowercase();

			atArray<atString> atLineEntries;
			strLine.Split(atLineEntries, "\t",true);

			if(atLineEntries.GetCount() == 2)
			{
				atLineEntries[0].Lowercase();

				decalMap.Insert(atLineEntries[0], atoi(atLineEntries[1]));
			}
		}
	}

	atArray<cutfObject *>& objectList = const_cast<atArray<cutfObject *> &>( pCutfile->GetObjectList() );
	for(int j=0; j < objectList.GetCount(); ++j)
	{
		cutfDecalObject* pDecalObject = dynamic_cast<cutfDecalObject*>(objectList[j]);
		if(pDecalObject)
		{
			atString strDecalName(pDecalObject->GetDisplayName().c_str());
			int index = strDecalName.IndexOf("^");
			if(index != -1)
				strDecalName.Set(strDecalName, 0, index);

			strDecalName.Lowercase();

			int* renderId = decalMap.Access(strDecalName.c_str());
			if(renderId)
			{
				pDecalObject->SetRenderId(*renderId);
			}
		}
	}
}

// remove any tags from the audio, ie if they are tagged as _CENTER, _LEFT or _RIGHT
void RemoveAudioTags(cutfCutsceneFile2* pCutFile)
{
	atArray<cutfObject*> &objectList = const_cast<atArray<cutfObject*> &>(pCutFile->GetObjectList() );

	for(int i=0; i < objectList.GetCount(); ++i)
	{
		cutfAudioObject* audioObject = dynamic_cast<cutfAudioObject*>(objectList[i]);
		if(audioObject != NULL)
		{
			atString strName(audioObject->GetName());
			strName.Uppercase();
			strName.Replace("_CENTER","");
			strName.Replace("_LEFT","");
			strName.Replace("_RIGHT","");
			strName.Replace(".C.",".");
			strName.Replace(".L.",".");
			strName.Replace(".R.",".");
			strName.Replace(".MONO.",".");
			strName.Replace(".LS.",".");
			strName.Replace(".RS.",".");
			strName.Replace(".LF.",".");
			strName.Replace(".RF.",".");

			audioObject->SetName(strName.c_str());

			// get events attached to this object, then change their event arg names aswell
			atArray<cutfEvent*>& eventList = const_cast<atArray<cutfEvent*> &>(pCutFile->GetEventList() );

			for(int i=0; i < eventList.GetCount(); ++i)
			{
				cutfObjectIdEvent* pObjectIDEvent = dynamic_cast<cutfObjectIdEvent*>(eventList[i]);
				if(pObjectIDEvent != NULL)
				{
					if(audioObject->GetObjectId() == pObjectIDEvent->GetObjectId())
					{
						// set object id on event args
						cutfNameEventArgs* pEventArgs = dynamic_cast<cutfNameEventArgs*>(const_cast<cutfEventArgs *>( pObjectIDEvent->GetEventArgs() ));
						if(pEventArgs != NULL)
						{
							atString strEventArgsName(pEventArgs->GetName().GetCStr());
							strEventArgsName.Uppercase();
							strEventArgsName.Replace("_CENTER","");
							strEventArgsName.Replace("_LEFT","");
							strEventArgsName.Replace("_RIGHT","");
							pEventArgs->SetName(strEventArgsName.c_str());
						}
					}
				}
			}

			atArray<cutfEvent*>& eventLoadList = const_cast<atArray<cutfEvent*> &>(pCutFile->GetLoadEventList() );

			for(int i=0; i < eventLoadList.GetCount(); ++i)
			{
				cutfObjectIdEvent* pObjectIDEvent = dynamic_cast<cutfObjectIdEvent*>(eventLoadList[i]);
				if(pObjectIDEvent != NULL)
				{
					if(audioObject->GetObjectId() == pObjectIDEvent->GetObjectId())
					{
						// set object id on event args
						cutfNameEventArgs* pEventArgs = dynamic_cast<cutfNameEventArgs*>(const_cast<cutfEventArgs *>( pObjectIDEvent->GetEventArgs() ));
						if(pEventArgs != NULL)
						{
							atString strEventArgsName(pEventArgs->GetName().GetCStr());
							strEventArgsName.Uppercase();
							strEventArgsName.Replace("_CENTER","");
							strEventArgsName.Replace("_LEFT","");
							strEventArgsName.Replace("_RIGHT","");
							pEventArgs->SetName(strEventArgsName.c_str());
						}
					}
				}
			}
		}
	}
}

int Main()
{
    Init();

    // get the input directory
    const char* pInputDir = "";
    PARAM_inputDir.Get( pInputDir );

    // get the face dir
    char cInputFaceDir[RAGE_MAX_PATH];

    const char *pInputFaceDir = NULL;
    if ( !PARAM_inputFaceDir.Get( pInputFaceDir ) )
    {
        sprintf( cInputFaceDir, "%s\\faces", pInputDir );
        pInputFaceDir = cInputFaceDir;
    }

    // get the intermediate directory
    char cIntermediateDir[RAGE_MAX_PATH];

    const char* pIntermediateDir = NULL;
    if ( !PARAM_intermediateDir.Get( pIntermediateDir ) )
    {
        sprintf( cIntermediateDir, "%s\\obj", pInputDir );
        pIntermediateDir = cIntermediateDir;
    }

    // get the cut filename
    char cCutFilename[RAGE_MAX_PATH];

    const char* pCutFilename = NULL;
    if ( !PARAM_cutscenefile.Get( pCutFilename ) )
    {
        sprintf( cCutFilename, "%s\\data.cutxml", pInputDir );
        pCutFilename = cCutFilename;
    }

    // get the output filename
    char cOutputCutFilename[RAGE_MAX_PATH];
	char cOutputCutXMLFilename[RAGE_MAX_PATH];
	char cDataOutputCutFilename[RAGE_MAX_PATH];

    const char* pOutputCutFilename = NULL;
	const char* pDataOutputCutFilename = NULL;
	const char* pNamedCutFile = NULL;
    if ( !PARAM_outputCutfile.Get( pOutputCutFilename ) )
    {
        sprintf( cOutputCutFilename, "%s.cutbin", pInputDir );
        pOutputCutFilename = cOutputCutFilename;

		sprintf( cOutputCutXMLFilename, "%s.cutxml", pInputDir );
		pNamedCutFile = cOutputCutXMLFilename;

		sprintf( cDataOutputCutFilename, "%s/data_stream.cutxml", pInputDir );
		pDataOutputCutFilename = cDataOutputCutFilename;
    }

    // load the cut file
    cutfCutsceneFile2 cutFile;
    if ( !LoadCutfile( pCutFilename, cutFile ) )
    {
		ULOGGER.SetProgressMessage("ERROR: Unable to load '%s'.", pCutFilename );

        Shutdown();
        return 1;
    }   

    // get the sectioning method, if any
    int iMethod = -1;
    PARAM_method.Get( iMethod );

    float fDuration = 0.0f;
    PARAM_duration.Get( fDuration );
    
    //LoadSectioningMethod( cutFile, iMethod, fDuration );

  //  bool bAlreadySectioned = false;
  //  if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) )
  //  {
		//ULOGGER.SetProgressMessage("WARNING: The cut file '%s' has already been sectioned.", pCutFilename );

  //      bAlreadySectioned = true;
  //  }

    if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG ) )
    {
		ULOGGER.SetProgressMessage("Sectioning by Time Slices of %f...", cutFile.GetSectionByTimeSliceDuration() );
    }
    else if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG ) )
    {
		ULOGGER.SetProgressMessage("Sectioning by Camera Cuts...");
    }
    else if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG ) )
    {
		ULOGGER.SetProgressMessage("Sectioning by SectionSplit Property...");
    }
    else
    {
		ULOGGER.SetProgressMessage("No sectioning specified.  Trimming to time range...");
    }

    // Set the "is sectioned" flag
    const_cast<atBitSet &>( cutFile.GetCutsceneFlags() ).Set( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG );

    //if ( !bAlreadySectioned )
    {
        CropCutfile( cutFile, true, true );
    }

    // Find the ped models objects with faces, and indicate that the face and body animations have been combined
    // Find the ped models that have faces
    // Determine the start/end positions
    atArray<cutfObject *> modelObjList;
    cutFile.FindObjectsOfType( CUTSCENE_MODEL_OBJECT_TYPE, modelObjList );

    for ( int i = 0; i < modelObjList.GetCount(); ++i )
    {
        cutfModelObject *pModelObj = dynamic_cast<cutfModelObject *>( modelObjList[i] );
        if ( pModelObj->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
        {
            cutfPedModelObject *pPedModelObject = dynamic_cast<cutfPedModelObject *>( pModelObj );

            char cFaceAnimFilename[RAGE_MAX_PATH];
            
            safecpy( cFaceAnimFilename, pPedModelObject->GetDefaultFaceAnimationFilename( pInputFaceDir ).GetCStr(), sizeof(cFaceAnimFilename) );
            pPedModelObject->SetFoundDefaultFaceAnimation( ASSET.Exists( cFaceAnimFilename, NULL ) );
            
            safecpy( cFaceAnimFilename, pPedModelObject->GetFaceAnimationFilename( pInputFaceDir ).GetCStr(), sizeof(cFaceAnimFilename) );
            if ( strlen( cFaceAnimFilename ) > 0 )
            {
                if ( !ASSET.Exists( cFaceAnimFilename, NULL ) )
                {                            
                    if ( pPedModelObject->FoundDefaultFaceAnimation() || pPedModelObject->OverrideFaceAnimation() )
                    {
						ULOGGER.SetProgressMessage("ERROR: Ped Model '%s', when it was exported from Motion Builder, had the face animation '%s', but the file is now missing.", 
							pPedModelObject->GetDisplayName().c_str(), cFaceAnimFilename );

						Shutdown();
                        return 1;
                    }
                    else
                    {
                        pPedModelObject->SetFaceAndBodyAreMerged( false );
                    }
                }
                else
                {
					ULOGGER.SetProgressMessage("Found %s face animation '%s' for Ped Model '%s'.", 
						pPedModelObject->OverrideFaceAnimation() ? "overridden" : "default", 
						cFaceAnimFilename, pPedModelObject->GetDisplayName().c_str() );

                    pPedModelObject->SetFaceAndBodyAreMerged( true );
                }
            }
            else
            {
				ULOGGER.SetProgressMessage("The face animation for Ped Model '%s', if any, is not being used.", pPedModelObject->GetDisplayName().c_str() );

                pPedModelObject->SetFaceAndBodyAreMerged( false );
            }
        }
    }

	AddCameraEvents(&cutFile);
	RemoveAudioTags(&cutFile);
	FixupDecals(&cutFile);
	cutFile.SortLoadEvents();
	cutFile.SortEvents();

	if(!PARAM_saveStreamOnly.Get())
	{
		// always save as a bin file
		if ( !cutFile.SaveFile( pOutputCutFilename ))
		{
			ULOGGER.SetProgressMessage("ERROR: '%s' save fail", pOutputCutFilename );

			Shutdown();
			return 1;
		}

		if ( !cutFile.SaveFile( pNamedCutFile ))
		{
			ULOGGER.SetProgressMessage("ERROR: '%s' save fail", pNamedCutFile );

			Shutdown();
			return 1;
		}
	}

	if ( !cutFile.SaveFile( pDataOutputCutFilename ))
	{
		ULOGGER.SetProgressMessage("ERROR: '%s' save fail", pDataOutputCutFilename );

		Shutdown();
		return 1;
	}

	Shutdown();
    return 0;
}
