// 
// /cutfstream.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"

#include "RsULog/ULogger.h"

#include "ShotDef.h"
#include "ShotDef_parser.h"

using namespace rage;

REQ_PARAM( cutFile, "[cutfrebuild] The original cutfile." );
REQ_PARAM( masterFile, "[cutfrebuild] The master cutfile." );
REQ_PARAM( outputDir, "[cutfrebuild] The output directory.");
REQ_PARAM( metaFile, "[cutfrebuild] MetaFile.");
REQ_PARAM( cutscene, "[cutfrebuild] Cutscene name.");
REQ_PARAM( shot, "[cutfrebuild] Shot name.");

struct Cut
{
public:
	atString m_TimeInName;
	float m_TimeIn;
	float m_TimeOut;
	float m_AdjustedTimeIn;
	float m_AdjustedTimeOut;
	float m_DroppedTime;
};

void Init()
{
	INIT_PARSER;
	crAnimation::InitClass();
	crClip::InitClass();
	cutfEvent::InitClass();
}

void Shutdown()
{
	cutfEvent::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();        
	SHUTDOWN_PARSER;

	ULOGGER.PostExport();
}

void RemoveEvents(cutfCutsceneFile2* pCutFile, const atArray<cutfEvent *>& events, int eventId)
{
	for(int i=0; i < events.GetCount(); ++i)
	{
		if(events[i]->GetEventId() == eventId)
		{
			pCutFile->RemoveEvent(events[i]);
			i--;
		}
	}
}

bool IsObjectAnimatable( cutfObject *pObject )
{
	return (pObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE)
		|| (pObject->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE)
		|| (pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
		|| (pObject->GetType() == CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE);
}

void ProcessMeta(ShotDef def, atArray<Cut>& ranges, const char* cutscene, const char* shot)
{
	for(int i=0; i < def.m_ReelList.GetCount(); ++i)
	{
		if(stricmp(def.m_ReelList[i].m_EditName.c_str(), cutscene) == 0 &&
			stricmp(def.m_ReelList[i].m_ShotName.c_str(), shot) == 0)
		{
			for(int j=0; j < def.m_ReelList[i].m_SourceTimeCode.GetCount(); j+=2)
			{
				Cut r;

				r.m_TimeIn = (def.m_ReelList[i].m_SourceTimeCode[j].m_Minute * 30.0f) + 
					(def.m_ReelList[i].m_SourceTimeCode[j].m_Second) + 
					(def.m_ReelList[i].m_SourceTimeCode[j].m_Frame/30.0f);

				r.m_TimeOut = (def.m_ReelList[i].m_SourceTimeCode[j+1].m_Minute * 30.0f) + 
					(def.m_ReelList[i].m_SourceTimeCode[j+1].m_Second) + 
					(def.m_ReelList[i].m_SourceTimeCode[j+1].m_Frame/30.0f);

				r.m_TimeInName = def.m_ReelList[i].m_SourceTimeCode[j].m_Name;

				r.m_AdjustedTimeIn = r.m_TimeIn;
				r.m_AdjustedTimeOut = r.m_TimeOut;
				r.m_DroppedTime = 0;

				ranges.PushAndGrow(r);
			}
		}
	}

	float fDiscarded = 0;
	for(int i=1; i < ranges.GetCount(); ++i)
	{
		Cut& r = ranges[i];

		fDiscarded += r.m_TimeIn - ranges[i-1].m_TimeOut;

		r.m_AdjustedTimeIn = ranges[i-1].m_TimeOut;
		r.m_AdjustedTimeOut = r.m_TimeOut - fDiscarded;
		ranges[i-1].m_DroppedTime = fDiscarded;
	}
}

void CreateAnimEvents(cutfCutsceneFile2* pCutfile)
{
	const atArray<cutfObject*>& atObjects = pCutfile->GetObjectList();

	RemoveEvents(pCutfile, pCutfile->GetEventList(), CUTSCENE_SET_ANIM_EVENT);
	RemoveEvents(pCutfile, pCutfile->GetEventList(), CUTSCENE_CLEAR_ANIM_EVENT);

	for(int i=0; i < atObjects.GetCount(); ++i)
	{
		cutfObject* pObject = atObjects[i];
		if(!IsObjectAnimatable(pObject)) continue;

		cutfEvent* pSetAnimEvent = rage_new cutfEvent(0, CUTSCENE_SET_ANIM_EVENT, rage_new cutfObjectIdEventArgs(pObject->GetObjectId()));
		pCutfile->AddEvent(pSetAnimEvent);

		cutfEvent* pClearAnimEvent = rage_new cutfEvent((float)pCutfile->GetRangeEnd()/30.0f, CUTSCENE_CLEAR_ANIM_EVENT, rage_new cutfObjectIdEventArgs(pObject->GetObjectId()));
		pCutfile->AddEvent(pClearAnimEvent);
	}
}

void CreateAudioEvents(cutfCutsceneFile2* pCutfile)
{
	atArray<cutfObject*> atAudioObjects;
	pCutfile->FindObjectsOfType(CUTSCENE_AUDIO_OBJECT_TYPE, atAudioObjects);

	if(atAudioObjects.GetCount())
	{
		RemoveEvents(pCutfile, pCutfile->GetEventList(), CUTSCENE_PLAY_AUDIO_EVENT);

		char cAudioName[MAX_PATH];
		ASSET.RemoveExtensionFromPath(cAudioName, MAX_PATH, atAudioObjects[0]->GetDisplayName().c_str());

		cutfEvent* pPlayEvent = rage_new cutfObjectIdEvent(atAudioObjects[0]->GetObjectId(), 0, CUTSCENE_PLAY_AUDIO_EVENT, rage_new cutfNameEventArgs(cAudioName));
		pCutfile->AddEvent(pPlayEvent);
	}
}

void CreateCameraEvents(cutfCutsceneFile2* pCutfile, atArray<Cut>& m)
{
	atArray<cutfObject*> atCameraObject;
	pCutfile->FindObjectsOfType(CUTSCENE_CAMERA_OBJECT_TYPE, atCameraObject);

	if(atCameraObject.GetCount())
	{
		RemoveEvents(pCutfile, pCutfile->GetEventList(), CUTSCENE_CAMERA_CUT_EVENT);

		for(int i=0; i < m.GetCount(); ++i)
		{
			Cut r = m[i];

			cutfCameraCutEventArgs* pCameraEventArgs = rage_new cutfCameraCutEventArgs();
			pCameraEventArgs->SetName(r.m_TimeInName.c_str());

			cutfEvent* pCameraEvent = rage_new cutfObjectIdEvent(atCameraObject[0]->GetObjectId(), r.m_AdjustedTimeIn, CUTSCENE_CAMERA_CUT_EVENT, pCameraEventArgs);
			pCutfile->AddEvent(pCameraEvent);
		}
	}
}

void CreateLoadSceneEvent(cutfCutsceneFile2* pCutfile)
{
	cutfEvent* pLoadEvent = rage_new cutfEvent(0, CUTSCENE_LOAD_SCENE_EVENT, rage_new cutfLoadSceneEventArgs());
	pCutfile->AddLoadEvent(pLoadEvent);

	cutfEvent* pAnimEvent = rage_new cutfEvent(0, CUTSCENE_LOAD_ANIM_DICT_EVENT, rage_new cutfNameEventArgs("dict"));
	pCutfile->AddLoadEvent(pAnimEvent);
}

void CreateLoadParticlesEvent(cutfCutsceneFile2* pCutfile)
{
	atArray<cutfObject*> atParticleObjects;
	pCutfile->FindObjectsOfType(CUTSCENE_PARTICLE_EFFECT_OBJECT_TYPE, atParticleObjects);
	pCutfile->FindObjectsOfType(CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE, atParticleObjects);

	if(atParticleObjects.GetCount())
	{
		atArray<s32> objectIdList;
		for(int i=0; i < atParticleObjects.GetCount(); ++i)
		{
			objectIdList.PushAndGrow(atParticleObjects[i]->GetObjectId());
		}

		cutfEvent* pEvent = rage_new cutfEvent(0, CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT, new cutfObjectIdListEventArgs(objectIdList));
		pCutfile->AddLoadEvent(pEvent);
	}
}

void CreateLoadAudioEvent(cutfCutsceneFile2* pCutfile)
{
	atArray<cutfObject*> atAudioObjects;
	pCutfile->FindObjectsOfType(CUTSCENE_AUDIO_OBJECT_TYPE, atAudioObjects);

	if(atAudioObjects.GetCount())
	{
		char cAudioName[MAX_PATH];
		ASSET.RemoveExtensionFromPath(cAudioName, MAX_PATH, atAudioObjects[0]->GetDisplayName().c_str());

		cutfEvent* pEvent = rage_new cutfEvent(0, CUTSCENE_LOAD_AUDIO_EVENT, new cutfNameEventArgs(cAudioName));
		pCutfile->AddLoadEvent(pEvent);
	}
}

void CreateLoadModelsEvent(cutfCutsceneFile2* pCutfile)
{
	const atArray<cutfObject*>& atObjects = pCutfile->GetObjectList();

	atArray<s32> objectIdList;
	for(int i=0; i < atObjects.GetCount(); ++i)
	{
		if(!IsObjectAnimatable(atObjects[i])) continue;

		objectIdList.PushAndGrow(atObjects[i]->GetObjectId());
	}

	cutfEvent* pEvent = rage_new cutfEvent(0, CUTSCENE_LOAD_MODELS_EVENT, new cutfObjectIdListEventArgs(objectIdList));
	pCutfile->AddLoadEvent(pEvent);
}

void InjectExistingEvents(cutfCutsceneFile2* pMasterCutfile, cutfCutsceneFile2* pCutfile, atArray<Cut>& m)
{
	const atArray<cutfEvent*>& events = pMasterCutfile->GetEventList();

	for(int i=0; i < events.GetCount(); ++i)
	{
		cutfEvent* pEvent = events[i];

		float fDroppedTime = 0;
		for(int j=0; j < m.GetCount(); ++j)
		{
			if(pEvent->GetTime() >= m[j].m_TimeIn &&
				pEvent->GetTime() <= m[j].m_TimeOut)
			{
				cutfEvent* pNewEvent = pEvent->Clone();
				pNewEvent->SetTime(pEvent->GetTime() - fDroppedTime);
				pCutfile->AddEvent(pNewEvent);
			}

			fDroppedTime += m[j].m_DroppedTime;
		}
	}
}

int Main()
{
	Init();

	// Calculate the EST cut times so we can get the discarded ranges and the camera times
	const char *pMetafile;
	if(!PARAM_metaFile.Get( pMetafile ))
	{
		Shutdown();
		return -1;
	}

	ShotDef def;
	if(!PARSER.LoadObject(pMetafile,"", def))
	{
		return -1;
	}

	const char *pCutscene;
	if(!PARAM_cutscene.Get( pCutscene ))
	{
		Shutdown();
		return -1;
	}

	const char *pShot;
	if(!PARAM_shot.Get( pShot ))
	{
		Shutdown();
		return -1;
	}

	const char *pOutputDir;
	if(!PARAM_outputDir.Get( pOutputDir ))
	{
		Shutdown();
		return -1;
	}

	atArray<Cut> m;
	ProcessMeta(def,m, pCutscene, pShot);

	if(m.GetCount() == 0)
	{
		ULOGGER.SetProgressMessage("ERROR: No meta entry found for cutscene '%s' and shot '%s'", pCutscene, pShot);
		return -1;
	}

	const char *pCutFilename;
	if(!PARAM_cutFile.Get( pCutFilename ))
	{
		Shutdown();
		return -1;
	}
	
	// Load the cut file 
	cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();
	if (!pCutfile->LoadFile(pCutFilename) )
	{
		ULOGGER.SetProgressMessage("ERROR: Unable to open cut file: %s", pCutFilename);

		Shutdown();
		return -1;
	}

	const char *pMasterCutFilename;
	if(!PARAM_masterFile.Get( pMasterCutFilename ))
	{
		Shutdown();
		return -1;
	}

	// Load the cut file 
	cutfCutsceneFile2* pMasterCutfile = rage_new cutfCutsceneFile2();
	if (!pMasterCutfile->LoadFile(pMasterCutFilename) )
	{
		ULOGGER.SetProgressMessage("ERROR: Unable to open cut file: %s", pMasterCutFilename);

		Shutdown();
		return -1;
	}

	pCutfile->SetRangeStart((s32)(m[0].m_TimeIn * 30.0));
	pCutfile->SetRangeEnd((s32)(m[m.GetCount()-1].m_AdjustedTimeOut * 30.0));

	CreateLoadSceneEvent(pCutfile);
	CreateLoadParticlesEvent(pCutfile);
	CreateLoadModelsEvent(pCutfile);
	CreateLoadAudioEvent(pCutfile);

	CreateAnimEvents(pCutfile);
	CreateAudioEvents(pCutfile);
	CreateCameraEvents(pCutfile, m);
	InjectExistingEvents(pMasterCutfile, pCutfile, m);

	char cOutput[RAGE_MAX_PATH];
	sprintf(cOutput, "%s\\data.cutxml", pOutputDir);

	pCutfile->SortLoadEvents();
	pCutfile->SortEvents();
	pCutfile->SaveFile(cOutput);

	Shutdown();

    return 0;
}
