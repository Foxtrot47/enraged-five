// 
// /cutfstream.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"

#include "RsULog/ULogger.h"

#include "animeditcore.h"
#include "clipeditcore.h"

using namespace rage;

REQ_PARAM( cutLight, "[cutflightmerge] The cutf file from light tool." );
PARAM( cutFile, "[cutflightmerge] The cutf file to merge into." );
PARAM( split, "[cutflightmerge] Split a internal concat light file into child light files " );
PARAM( merge, "[cutflightmerge] Merge a light file into a scene cutxml." );
PARAM( out, "[cutflightmerge] Output folder. (Used for split)" );
PARAM( ulog, "[cutflightmerge] Universal logging." );
PARAM( outputCutFile, "[cutflightmerge] Output cutfile for merge." );
PARAM( removeDupes, "[cutflightmerge] Remove dupe lights." );

void Init()
{
	INIT_PARSER;
	crAnimation::InitClass();
	crClip::InitClass();
	cutfEvent::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("cutflightmerge").c_str(), "cutflightmerge");
	}
}

void Shutdown()
{
	cutfEvent::ShutdownClass();      
	SHUTDOWN_PARSER;

	ULOGGER.PostExport();
}

bool AlmostEqual(double nVal1, double nVal2, double nEpsilon)
{
	bool bRet = (((nVal2 - nEpsilon) < nVal1) && (nVal1 < (nVal2 + nEpsilon)));
	return bRet;
}

float Round(float num, float num2)
{
	if(AlmostEqual(num, num2, 0.01))
	{
		return num2;
	}

	return num;
}

bool SplitAnimation(cutfLightObject* pLightObject, const char* pSourceFolder, const char* pOutputFolder, cutfCutsceneFile2::SConcatData& segment)
{
	float segmentStart = segment.fStartTime;
	float segmentEnd = (segment.fStartTime + (((segment.iRangeEnd + 1) - segment.iRangeStart)/CUTSCENE_FPS));

	atArray<atString> atPaths;
	atString strSource(pSourceFolder);
	strSource.Split(atPaths, "\\", true);

	if(pLightObject->GetType() == CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE)
	{
		char cInputFile[RAGE_MAX_PATH];
		sprintf(cInputFile, "%s\\%s", pSourceFolder, pLightObject->GetDisplayName().c_str());

		char cOutputFile[RAGE_MAX_PATH];
		sprintf(cOutputFile, "%s\\%s", pOutputFolder, pLightObject->GetDisplayName().c_str());

		char cTimes[RAGE_MAX_PATH];
		sprintf(cTimes, "%f,%f", segmentStart, segmentEnd);

		char cOpCommand[RAGE_MAX_PATH];
		sprintf(cOpCommand, "crop=%s", cTimes);

		crAnimation* anim = crAnimation::AllocateAndLoad(cInputFile);
		if(!anim)
		{
			ULOGGER.SetProgressMessage("ERROR: failed to load animation '%s'", cInputFile);
			return false;
		}

		crAnimEdit::OpData* ops = crAnimEdit::ParseOps(cOpCommand, NULL);
		if(!crAnimEdit::ExecuteOps(ops, anim, NULL))
		{
			ULOGGER.SetProgressMessage("ERROR: failed to parse ops");
			return false;
		}

		if(!anim->Save(cOutputFile))
		{
			ULOGGER.SetProgressMessage("ERROR: failed to save animation '%s'", cOutputFile);
			return false;
		}

		sysParam param(0, "clipanimation", false, "");
		param.Set(cTimes);

		crClip* clip = crClip::AllocateAndLoad(cInputFile);
		if(!clip)
		{
			ULOGGER.SetProgressMessage("ERROR: failed to load clip '%s'", cInputFile);
			return false;
		}

		const int maxSeq = 64;
		const int maxSeqBufSize = 8192;

		const char* seq[maxSeq];
		char seqBuf[maxSeqBufSize];
		for(int i=0; i<maxSeq; ++i)
		{
			seq[i] = NULL;
		}

		int numSeq = param.GetArray(seq, maxSeq, seqBuf, maxSeqBufSize);
		crClipEdit::ProcessSequence(numSeq, seq, crClipEdit::kClipAnimation, clip);

		//crClipEdit::ProcessSequence(maxSeq, seq, crClipEdit::kClipAnimation, clip);

		if(!clip->Save(cOutputFile))
		{
			ULOGGER.SetProgressMessage("ERROR: failed to save clip '%s'", cOutputFile);
			return false;
		}
	}

	return true;
}

void FilterEvents(int eventId, atArray<cutfEvent*> eventList, atArray<cutfEvent*>& eventListOut)
{
	for(int i=0; i < eventList.GetCount(); ++i)
	{
		if(eventId == eventList[i]->GetEventId())
			eventListOut.PushAndGrow(eventList[i]);
	}
}

bool AddEvents(cutfCutsceneFile2* pLightCutfile, cutfCutsceneFile2* pLightChild, const atArray<cutfEvent*>& eventList, float segmentStart, float segmentEnd, float fOffset)
{
	if(eventList.GetCount() == 0) return false;

	// Need to round the event as we potentially have floating point inaccuracies
	float fEventTime1 = Round(eventList[0]->GetTime(), segmentStart);
	float fEventTime2 = Round(eventList[1]->GetTime(), segmentEnd);

	int iObjectID = pLightChild->GetObjectList().GetCount();

	bool bAddedEvents = false;

	if(fEventTime1 != 0) fEventTime1 += (float)(1.0/30.0);

	if( ((eventList[0]->GetEventId() == CUTSCENE_SET_ANIM_EVENT || eventList[0]->GetEventId() == CUTSCENE_SET_LIGHT_EVENT) && fEventTime1 >= segmentStart && fEventTime1 < segmentEnd) &&
		((eventList[1]->GetEventId() == CUTSCENE_CLEAR_ANIM_EVENT || eventList[1]->GetEventId() == CUTSCENE_CLEAR_LIGHT_EVENT) && fEventTime2 > segmentStart && fEventTime2 <= segmentEnd) )
	{
		cutfEvent *pEventClone1 = eventList[0]->Clone();
		cutfEvent *pEventClone2 = eventList[1]->Clone();

		float fOffsetEventTime1 = pEventClone1->GetTime() - fOffset;
		float fOffsetEventTime2 = pEventClone2->GetTime() - fOffset;
		
		if(fOffsetEventTime1 < 0.0f) 
		{
			fOffsetEventTime1 = 0.0f;
			ULOGGER.SetProgressMessage("WARNING: Event '%i' time is less than 0.0.  Clamping to 0.0", pEventClone1->GetDisplayName());
		}
		if(fOffsetEventTime2 < 0.0f) 
		{
			fOffsetEventTime2 = 0.0f;
			ULOGGER.SetProgressMessage("WARNING: Event '%i' time is less than 0.0.  Clamping to 0.0", pEventClone2->GetDisplayName());
		}

		pEventClone1->SetTime(fOffsetEventTime1);
		pEventClone2->SetTime(fOffsetEventTime2);

		if(eventList[0]->GetEventId() == CUTSCENE_SET_ANIM_EVENT && eventList[1]->GetEventId() == CUTSCENE_CLEAR_ANIM_EVENT)
		{
			cutfObjectIdEventArgs* pEventArgs1 = dynamic_cast<cutfObjectIdEventArgs*>(const_cast<cutfEventArgs *>( pEventClone1->GetEventArgs()->Clone() ));
			cutfObjectIdEventArgs* pEventArgs2 = dynamic_cast<cutfObjectIdEventArgs*>(const_cast<cutfEventArgs *>( pEventClone2->GetEventArgs()->Clone() ));
			pEventArgs1->SetObjectId(iObjectID);
			pEventArgs2->SetObjectId(iObjectID);

			pEventClone1->SetEventArgs(pEventArgs1);
			pEventClone2->SetEventArgs(pEventArgs2);
		}
		else
		{
			cutfObjectIdEvent* pEventId1 = dynamic_cast<cutfObjectIdEvent*>(pEventClone1);
			cutfObjectIdEvent* pEventId2 = dynamic_cast<cutfObjectIdEvent*>(pEventClone2);
			pEventId1->SetObjectId(iObjectID);
			pEventId2->SetObjectId(iObjectID);

			if(pEventId1->GetEventArgs() != NULL)
				pEventId1->SetEventArgs(pEventId1->GetEventArgs()->Clone());
			if(pEventId2->GetEventArgs() != NULL)
				pEventId2->SetEventArgs(pEventId2->GetEventArgs()->Clone());
		}

		pLightChild->AddEvent(pEventClone1);
		pLightChild->AddEvent(pEventClone2);

		bAddedEvents = true;
	}
	else if( ((eventList[0]->GetEventId() == CUTSCENE_SET_ANIM_EVENT || eventList[0]->GetEventId() == CUTSCENE_SET_LIGHT_EVENT) && fEventTime1 == 0.0f) &&
		((eventList[1]->GetEventId() == CUTSCENE_CLEAR_ANIM_EVENT || eventList[1]->GetEventId() == CUTSCENE_CLEAR_LIGHT_EVENT) && fEventTime2 == pLightCutfile->GetTotalDuration()) )
	{
		// On All Time

		cutfEvent *pEventClone1 = eventList[0]->Clone();
		cutfEvent *pEventClone2 = eventList[1]->Clone();

		pEventClone1->SetTime(segmentStart - fOffset);
		pEventClone2->SetTime(segmentEnd - fOffset);

		if(eventList[0]->GetEventId() == CUTSCENE_SET_ANIM_EVENT && eventList[1]->GetEventId() == CUTSCENE_CLEAR_ANIM_EVENT)
		{
			cutfObjectIdEventArgs* pEventArgs1 = dynamic_cast<cutfObjectIdEventArgs*>(const_cast<cutfEventArgs *>( pEventClone1->GetEventArgs()->Clone() ));
			cutfObjectIdEventArgs* pEventArgs2 = dynamic_cast<cutfObjectIdEventArgs*>(const_cast<cutfEventArgs *>( pEventClone2->GetEventArgs()->Clone() ));
			pEventArgs1->SetObjectId(iObjectID);
			pEventArgs2->SetObjectId(iObjectID);

			pEventClone1->SetEventArgs(pEventArgs1);
			pEventClone2->SetEventArgs(pEventArgs2);
		}
		else
		{
			cutfObjectIdEvent* pEventId1 = dynamic_cast<cutfObjectIdEvent*>(pEventClone1);
			cutfObjectIdEvent* pEventId2 = dynamic_cast<cutfObjectIdEvent*>(pEventClone2);
			pEventId1->SetObjectId(iObjectID);
			pEventId2->SetObjectId(iObjectID);

			if(pEventId1->GetEventArgs() != NULL)
				pEventId1->SetEventArgs(pEventId1->GetEventArgs()->Clone());
			if(pEventId2->GetEventArgs() != NULL)
				pEventId2->SetEventArgs(pEventId2->GetEventArgs()->Clone());
		}

		pLightChild->AddEvent(pEventClone1);
		pLightChild->AddEvent(pEventClone2);

		bAddedEvents = true;
	}

	return bAddedEvents;
}

bool Split(const char* pFilename, const char* pCutFilename, const char* pOutputFolder)
{
	if(ASSET.Exists(pFilename, NULL))
	{
		cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();
		cutfCutsceneFile2* pLightCutfile = rage_new cutfCutsceneFile2();
		if (LoadCutfile( pCutFilename, *(pCutfile) ) && LoadCutfile(pFilename, *(pLightCutfile)) )
		{
			if(!IsClose(pCutfile->GetTotalDuration(), pLightCutfile->GetTotalDuration(), (float)0.005))
			{
				ULOGGER.SetProgressMessage("ERROR: Light file '%s' has duration of '%f' which does not match cut file '%s' duration of '%f'", pFilename, pLightCutfile->GetTotalDuration(), pCutFilename, pCutfile->GetTotalDuration());
				return false;
			}

			char cSourceFolder[RAGE_MAX_PATH];
			ASSET.RemoveNameFromPath(cSourceFolder, RAGE_MAX_PATH, pFilename);

			const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT>& concatList = pLightCutfile->GetConcatDataList();
			atArray<cutfObject*> &objectList = const_cast<atArray<cutfObject*> &>(pLightCutfile->GetObjectList() );

			float fOffset = 0.0f;
			
			for(int i=0; i < concatList.GetCount(); ++i)
			{
				cutfCutsceneFile2::SConcatData segment = concatList[i];
				float segmentStart = segment.fStartTime;
				float segmentEnd = (segment.fStartTime + (((segment.iRangeEnd + 1) - segment.iRangeStart)/CUTSCENE_FPS));

				// Move each child back to zero it
				fOffset = segment.fStartTime;

				cutfCutsceneFile2* pLightChild = rage_new cutfCutsceneFile2();

				pLightChild->SetTotalDuration(((segment.iRangeEnd + 1) - segment.iRangeStart) / CUTSCENE_FPS);

				for(int j=0; j < objectList.GetCount(); ++j)
				{
					cutfLightObject* pLightObject = dynamic_cast<cutfLightObject*>(objectList[j]);

					// There should only ever be two events attached to an object
					// This works because raw light file SET/CLEAR events point to themselves, ids match so they get scooped
					atArray<cutfEvent*> eventList;
					pLightCutfile->FindEventsForObjectIdOnly(pLightObject->GetObjectId(), pLightCutfile->GetEventList(), eventList, false);
					pLightCutfile->FindEventsForObjectIdOnly(pLightObject->GetObjectId(), pLightCutfile->GetEventList(), eventList, true);
					
					atArray<cutfEvent*> animEventList;
					FilterEvents(CUTSCENE_SET_ANIM_EVENT, eventList, animEventList);
					FilterEvents(CUTSCENE_CLEAR_ANIM_EVENT, eventList, animEventList);

					atArray<cutfEvent*> lightEventList;
					FilterEvents(CUTSCENE_SET_LIGHT_EVENT, eventList, lightEventList);
					FilterEvents(CUTSCENE_CLEAR_LIGHT_EVENT, eventList, lightEventList);

					Assert(animEventList.GetCount() == 2);
					Assert(lightEventList.GetCount() == 2);

					bool bAddedAnimEvents = AddEvents(pLightCutfile, pLightChild, animEventList, segmentStart, segmentEnd, fOffset);
					bool bAddedLightEvents = AddEvents(pLightCutfile, pLightChild, lightEventList, segmentStart, segmentEnd, fOffset);
					
					if(bAddedAnimEvents || bAddedLightEvents) pLightChild->AddObject(pLightObject->Clone());

					// We have an animated light, we need to split its animation and clip
					if(!SplitAnimation(pLightObject, cSourceFolder, pOutputFolder, segment))
					{
						Shutdown();
						return false;
					}
				}

				atArray<atString> atPaths;
				atString strSource(cSourceFolder);
				strSource.Split(atPaths, "\\", true);

				char cLightFile[RAGE_MAX_PATH];
				sprintf(cLightFile, "%s\\%s_%s.lightxml", pOutputFolder, atPaths[atPaths.GetCount()-1].c_str(), segment.cSceneName.GetCStr());

				ULOGGER.SetProgressMessage("Saving light file '%s'", cLightFile);
				
				pLightChild->SortEvents();

				if(!pLightChild->SaveFile(cLightFile))
				{
					delete pLightChild;
					ULOGGER.SetProgressMessage("ERROR: '%s' save fail", cLightFile);
					return false;
				}

				delete pLightChild;
			}

			Shutdown();
			return true;
		}
	}
	else
	{
		cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();
		if (LoadCutfile( pCutFilename, *(pCutfile) ) )
		{
			const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT>& concatList = pCutfile->GetConcatDataList();

			if(concatList.GetCount() == 0)
			{
				ULOGGER.SetProgressMessage("ERROR: File '%s' contains no concat list entries. Re-export should fix this.", pCutFilename);
				Shutdown();
				return false;
			}

			for(int i=0; i < concatList.GetCount(); ++i)
			{
				cutfCutsceneFile2::SConcatData segment = concatList[i];

				char cSourceFolder[RAGE_MAX_PATH];
				ASSET.RemoveNameFromPath(cSourceFolder, RAGE_MAX_PATH, pFilename);

				cutfCutsceneFile2* pLightChild = rage_new cutfCutsceneFile2();
				
				pLightChild->SetTotalDuration(((segment.iRangeEnd + 1) - segment.iRangeStart) / CUTSCENE_FPS);

				atArray<atString> atPaths;
				atString strSource(cSourceFolder);
				strSource.Split(atPaths, "\\", true);

				char cLightFile[RAGE_MAX_PATH];
				sprintf(cLightFile, "%s\\%s_%s.lightxml", pOutputFolder, atPaths[atPaths.GetCount()-1].c_str(), segment.cSceneName.GetCStr());

				ULOGGER.SetProgressMessage("Saving dummy light file '%s'", cLightFile);

				atBitSet &flags = const_cast<atBitSet &>( pLightChild->GetCutsceneFlags() );
				flags.Reset();

				if(!pLightChild->SaveFile(cLightFile))
				{
					delete pLightChild;
					ULOGGER.SetProgressMessage("ERROR: '%s' save fail", cLightFile);
					return false;
				}

				delete pLightChild;
			}

			Shutdown();
			return true;
		}
	}

	Shutdown();
	return false;
}

void SnapLightsAtEnd(cutfCutsceneFile2* pCutfile)
{
	atArray<cutfEvent *> &eventList = const_cast<atArray<cutfEvent *> &>( pCutfile->GetEventList() );
	for(int i=0; i < eventList.GetCount(); ++i)
	{
		if(eventList[i]->GetEventId() == CUTSCENE_CLEAR_LIGHT_EVENT)
		{
			if(IsClose(pCutfile->GetTotalDuration(), eventList[i]->GetTime(), (float)0.0333333333333333))
			{
				eventList[i]->SetTime(pCutfile->GetTotalDuration());
			}
		}
	}
}

void RemoveDuplicateLights(cutfCutsceneFile2* pCutfile)
{
	for(int i=0; i < pCutfile->GetObjectList().GetCount(); ++i)
	{
		if(pCutfile->GetObjectList()[i]->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
		{
			cutfLightObject* pLightObject = dynamic_cast<cutfLightObject*>(pCutfile->GetObjectList()[i]);

			for(int j=0; j < pCutfile->GetObjectList().GetCount(); ++j)
			{
				if(pCutfile->GetObjectList()[j]->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
				{
					cutfLightObject* pLightObjectCompare = dynamic_cast<cutfLightObject*>(pCutfile->GetObjectList()[j]);

					if(stricmp(pLightObject->GetDisplayName().c_str(), pLightObjectCompare->GetDisplayName().c_str()) == 0) continue;

					if(pLightObject->GetCoronaIntensity() == pLightObjectCompare->GetCoronaIntensity() &&
						pLightObject->GetCoronaSize() == pLightObjectCompare->GetCoronaSize() &&
						pLightObject->GetCoronaZBias() == pLightObjectCompare->GetCoronaZBias() &&
						pLightObject->GetExponentialFallOff() == pLightObjectCompare->GetExponentialFallOff() &&
						pLightObject->GetFlashiness() == pLightObjectCompare->GetFlashiness() &&
						pLightObject->GetInnerConeAngle() == pLightObjectCompare->GetInnerConeAngle() &&
						IsCloseAll(VECTOR3_TO_VEC3V(pLightObject->GetLightColour()),VECTOR3_TO_VEC3V(pLightObjectCompare->GetLightColour()), Vec3VFromF32((float)0.01)) &&
						pLightObject->GetLightConeAngle() == pLightObjectCompare->GetLightConeAngle() &&
						IsCloseAll(VECTOR3_TO_VEC3V(pLightObject->GetLightDirection()),VECTOR3_TO_VEC3V(pLightObjectCompare->GetLightDirection()), Vec3VFromF32((float)0.01)) &&
						pLightObject->GetLightFallOff() == pLightObjectCompare->GetLightFallOff() &&
						pLightObject->GetLightFlags() == pLightObjectCompare->GetLightFlags() &&
						pLightObject->GetLightHourFlags() == pLightObjectCompare->GetLightHourFlags() &&
						pLightObject->GetLightIntensity() == pLightObjectCompare->GetLightIntensity() &&
						IsCloseAll(VECTOR3_TO_VEC3V(pLightObject->GetLightPosition()),VECTOR3_TO_VEC3V(pLightObjectCompare->GetLightPosition()), Vec3VFromF32((float)0.01)) &&
						pLightObject->GetLightProperty() == pLightObjectCompare->GetLightProperty() &&
						pLightObject->GetVolumeIntensity() == pLightObjectCompare->GetVolumeIntensity() &&
						IsCloseAll(VECTOR4_TO_VEC4V(pLightObject->GetVolumeOuterColourAndIntensity()),VECTOR4_TO_VEC4V(pLightObjectCompare->GetVolumeOuterColourAndIntensity()), Vec4VFromF32((float)0.01)) &&
						pLightObject->GetVolumeSizeScale() == pLightObject->GetVolumeSizeScale())
					{
						atArray<cutfEvent*> atEvents;
						pCutfile->FindEventsForObjectIdOnly(pLightObjectCompare->GetObjectId(), pCutfile->GetEventList(), atEvents, false);
						pCutfile->FindEventsForObjectIdOnly(pLightObjectCompare->GetObjectId(), pCutfile->GetEventList(), atEvents, true);

						for(int k=0; k < atEvents.GetCount(); ++k)
						{
							if(atEvents[k]->GetEventId() == CUTSCENE_SET_LIGHT_EVENT || atEvents[k]->GetEventId() == CUTSCENE_CLEAR_LIGHT_EVENT)
							{
								cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent*>(atEvents[k]);
								pEvent->SetObjectId(pLightObject->GetObjectId());
							}
						}

						ULOGGER.SetProgressMessage("Light '%s' is a dupe of '%s', removing '%s'", pLightObjectCompare->GetDisplayName().c_str(), pLightObject->GetDisplayName().c_str(), pLightObjectCompare->GetDisplayName().c_str());

						pCutfile->RemoveObject(pLightObjectCompare);
						j=0;
					}
				}
			}
		}
	}
}

bool Merge(const char* pCutFilename, const char* pLightFilename, const char* pOutputCutFilename)
{
	cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();
	cutfCutsceneFile2* pCutLightfile = rage_new cutfCutsceneFile2();
	if (LoadCutfile( pCutFilename, *(pCutfile) ) && LoadCutfile(pLightFilename, *(pCutLightfile)) )
	{
		ULOGGER.SetProgressMessage("Light file '%s' duration: '%f'. Scene duration: '%f'", pLightFilename, pCutLightfile->GetTotalDuration(), pCutfile->GetTotalDuration());

		// When merging we might have some floating inaccuracies. We use a tolerance of 0.005 as this seems sensible. 
		// A frame is 0.0333333333333333
		if(!IsClose(pCutfile->GetTotalDuration(), pCutLightfile->GetTotalDuration(), (float)0.005))
		{
			ULOGGER.SetProgressMessage("ERROR: Light file '%s' has duration of '%f' which does not match cut file '%s' duration of '%f'", pLightFilename, pCutLightfile->GetTotalDuration(), pCutFilename, pCutfile->GetTotalDuration());
			return false;
		}

		atArray<cutfObject*> &objectList = const_cast<atArray<cutfObject*> &>(pCutLightfile->GetObjectList() );

		for(int i=0; i < objectList.GetCount(); ++i)
		{
			cutfObject *pLightObject = objectList[i]->Clone();

			pCutfile->AddObject( pLightObject );

			// All attributes are used for max, we dont need them for runtime so nuke em
			parAttributeList &attributes = const_cast<parAttributeList &>( pLightObject->GetAttributeList() );
			attributes.Reset();

			// We need to call both methods otherwise we dont grab any SET/CLEAR events 
			atArray<cutfEvent*> eventList;
			pCutLightfile->FindEventsForObjectIdOnly(objectList[i]->GetObjectId(),pCutLightfile->GetEventList(), eventList, true );
			pCutLightfile->FindEventsForObjectIdOnly(objectList[i]->GetObjectId(),pCutLightfile->GetEventList(), eventList, false );

			Assert(eventList.GetCount() == 2);

			for(int j=0; j < eventList.GetCount(); ++j)
			{
				const cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent*>(eventList[j]);
				if(pEvent)
				{
					if(pEvent->GetEventId() == CUTSCENE_SET_ANIM_EVENT || pEvent->GetEventId() == CUTSCENE_CLEAR_ANIM_EVENT)
					{
						cutfObjectIdEventArgs* pEventArgs = dynamic_cast<cutfObjectIdEventArgs*>(pEvent->GetEventArgs()->Clone());
						pEventArgs->SetObjectId(pLightObject->GetObjectId());

						cutfObjectIdEvent* pEventClone = rage_new cutfObjectIdEvent(pEvent->GetObjectId(), pEvent->GetTime(), pEvent->GetEventId(), pEventArgs);
						
						// We have to set the id to be -2, this is unique otherwise we get issues with direction
						if(pEventClone->GetObjectId() == -2)
						{
							pEventClone->SetObjectId(1); // Animation Manager
						}
						
						pCutfile->AddEvent( pEventClone );
					}
					else if(pEvent->GetEventId() == CUTSCENE_SET_LIGHT_EVENT || pEvent->GetEventId() == CUTSCENE_CLEAR_LIGHT_EVENT)
					{
						cutfTriggerLightEffectEventArgs* pEventArgs = NULL;
						if(pEvent->GetEventArgs() != NULL)
						{
							pEventArgs = dynamic_cast<cutfTriggerLightEffectEventArgs*>(pEvent->GetEventArgs()->Clone());

							// Update concat ids so they are valid when attached
							if(pEventArgs->GetAttachParentName() != NULL)
							{
								for(int k=0; k < pCutfile->GetObjectList().GetCount(); ++k)
								{
									if(stricmp(pEventArgs->GetAttachParentName(), pCutfile->GetObjectList()[k]->GetDisplayName().c_str()) == 0)
									{
										pEventArgs->SetAttachParentId(pCutfile->GetObjectList()[k]->GetObjectId());
									}
								}
							}
						}

						cutfObjectIdEvent* pEventClone = rage_new cutfObjectIdEvent(pLightObject->GetObjectId(), pEvent->GetTime(), pEvent->GetEventId(), pEventArgs);
						
						pCutfile->AddEvent( pEventClone );
					}
				}
			}
		}

		if(PARAM_removeDupes.Get())
			RemoveDuplicateLights(pCutfile);

		SnapLightsAtEnd(pCutfile);

		pCutfile->SortEvents();

		if(!pCutfile->SaveFile(pOutputCutFilename))
		{
			ULOGGER.SetProgressMessage("ERROR: '%s' save fail", pOutputCutFilename);
			Shutdown();
			return false;
		}

		Shutdown();
		return true;
	}

	Shutdown();
	return false;
}

int Main()
{
	Init();

	const char *pCutLightFilename;
	if(!PARAM_cutLight.Get( pCutLightFilename ))
	{
		return -1;
	}

	const char *pCutFilename;
	if(!PARAM_cutFile.Get( pCutFilename ))
	{
		return -1;
	}

	if(PARAM_split.Get())
	{
		const char *pOutputFolder;
		if(!PARAM_out.Get( pOutputFolder ))
		{
			return -1;
		}

		return Split(pCutLightFilename, pCutFilename, pOutputFolder) ? 0 : -1;
	}

	const char *pOutputCutFilename;
	if(!PARAM_outputCutFile.Get( pOutputCutFilename ))
	{
		return -1;
	}

	if(PARAM_merge.Get())
	{
		return Merge(pCutFilename, pCutLightFilename, pOutputCutFilename) ? 0 : -1;
	}

    return 0;
}
