// 
// /cutfconcat.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"

#include "RsULog/ULogger.h"

using namespace rage;

REQ_PARAM( cutlistFile, "[cutfconcat] The file with the list of cutscenes to concatenate, one per line:  inputDir inputCutfile inputFaceDir (separated by tabs, only inputDir is required)." );
REQ_PARAM( outputDir, "[cutfconcat] The location where the merged scene's animations will live." );
PARAM( outputCutfile, "[cutfconcat] The merged cut file to save.  Default:  $(outputDir).cutxml" );
PARAM( outputFaceDir, "[cutfconcat] The location where the merged scene's face animations will live.  Default:  $(outputDir)/faces" );
PARAM( outputIntermediateDir, "[cutfconcat] The location where the cropped intermediate files will be saved.  Default:  $(outputDir)/obj" );
PARAM( audioFile, "[cutfconcat] The audio file to use for the conatted scene." );
PARAM( skipExeCheck, "[cutfconcat] Skip the cutfconcat.exe timestamp check (for debugging purposes)." );
PARAM( ulog, "[cutfconcat] Universal logging." );
PARAM( internal, "[cutfconcat] Internal concat flagging." );
PARAM( external, "[cutfconcat] External concat flagging." );
PARAM( sectionMethod, "[cutfconcat] Override sectioning, use the data from the concatdatalist." );
PARAM( sectionDuration, "[cutfconcat] Override sectioning, use the data from the concatdatalist." );

const char* EDITED_AUDIO_SUFFIX = "_edited";
const char* MASTERED_AUDIO_SUFFIX = "_mastered";

struct SEventPair
{
    SEventPair()
        : pFirst(NULL)
        , pSecond(NULL)
    {

    }

    rage::cutfEvent *pFirst;
    rage::cutfEvent *pSecond;
};

void Init()
{
	INIT_PARSER;
	crAnimation::InitClass();
	crClip::InitClass();
	cutfEvent::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("cutfconcat").c_str(), "cutfconcat");
	}
}

void Shutdown()
{
	cutfEvent::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();        
	SHUTDOWN_PARSER;

	ULOGGER.PostExport();
}

int MergeEvents( cutfCutsceneFile2 &cutFile, cutfObject *pObject, const atArray<cutfEvent *> &eventList, int iStartEventId, int iEndEventId,
                const atArray<float> &timeList )
{
    // Find the first Set, last Clear, and all of the Clear/Set pairs in between
    cutfEvent *pFirstSetAnimEvent = NULL;
    cutfEvent *pLastClearAnimEvent = NULL;

    atArray<SEventPair> eventPairList;
    eventPairList.Reserve( timeList.GetCount() - 1 );

    for ( int i = 0; i < eventList.GetCount(); ++i )
    {
        if ( (pFirstSetAnimEvent == NULL) && (eventList[i]->GetTime() == 0.0f)
            && (eventList[i]->GetEventId() == iStartEventId) )
        {
            pFirstSetAnimEvent = eventList[i];
        }
        else if ( (pLastClearAnimEvent == NULL) && (eventList[i]->GetTime() == timeList[timeList.GetCount() - 1]) 
            && (eventList[i]->GetEventId() == iEndEventId) )
        {
            pLastClearAnimEvent = eventList[i];
        }
        else if ( (eventList[i]->GetEventId() == iStartEventId) || (eventList[i]->GetEventId() == iEndEventId) )
        {
            for ( int j = 0; j < timeList.GetCount() - 1; ++j )
            {
                if ( timeList[j] == eventList[i]->GetTime() )
                {
                    while ( eventPairList.GetCount() < j + 1 )
                    {
                        SEventPair eventPair;
                        eventPairList.Append() = eventPair;
                    }

                    if ( eventPairList[j].pFirst == NULL )
                    {
                        eventPairList[j].pFirst = eventList[i];
                    }
                    else
                    {
                        eventPairList[j].pSecond = eventList[i];
                    }

                    break;
                }
            }
        }
    }

    int count = 0;

    // Build the filenames that we need to concat, and remove the Clear/Set event pairs from the cutfile
    for ( int i = 0; i < eventPairList.GetCount(); ++i )
    {
        SEventPair *pPair = &(eventPairList[i]);
        if ( (pPair->pFirst != NULL) && (pPair->pFirst->GetEventId() == iEndEventId)
            && (pPair->pSecond != NULL) && (pPair->pSecond->GetEventId() == iStartEventId) 
            && (pPair->pFirst->GetEventArgs() != NULL) && (pPair->pSecond->GetEventArgs() != NULL) )
        {
            // now remove the events
            cutFile.RemoveEvent( pPair->pFirst );
            cutFile.RemoveEvent( pPair->pSecond );

            ++count;
        }
    }

    Displayf( "Removed %d pair(s) of %s/%s events for %s.", count, 
        cutfEvent::GetDisplayName( iStartEventId ), cutfEvent::GetDisplayName( iEndEventId ), pObject->GetDisplayName().c_str() );

    // Merge the last Clear event's event args into the first Set event's event args, and make the Clear event use that instead.
    if ( (pFirstSetAnimEvent != NULL) && (pLastClearAnimEvent != NULL) 
        && (pFirstSetAnimEvent->GetEventArgs() != NULL) && (pLastClearAnimEvent->GetEventArgs() != NULL)
        && (pFirstSetAnimEvent->GetEventArgs() != pLastClearAnimEvent->GetEventArgs()) )
    {
        cutfEventArgs *pFirstSetAnimEventArgs = const_cast<cutfEventArgs *>( pFirstSetAnimEvent->GetEventArgs() );
        pFirstSetAnimEventArgs->MergeFrom( *(pLastClearAnimEvent->GetEventArgs()) );

        const cutfEventArgs *pLastClearAnimEventArgs = pLastClearAnimEvent->GetEventArgs();
        pLastClearAnimEvent->SetEventArgs( NULL );
        cutFile.RemoveEventArgs( const_cast<cutfEventArgs *>( pLastClearAnimEventArgs ) );
        pLastClearAnimEvent->SetEventArgs( pFirstSetAnimEventArgs );

        Displayf( "Merged the event args for the event pair %s/%s for %s.", 
            cutfEvent::GetDisplayName( iStartEventId ), cutfEvent::GetDisplayName( iEndEventId ), pObject->GetDisplayName().c_str() );
    }

    return 0;
}

int MergeAnimEvents( cutfCutsceneFile2 &cutFile, const char *pOutputFaceDir, const atArray<float> &timeList )
{
    // Find the animation manager
    atArray<cutfObject *> animMgrObjectList;
    cutFile.FindObjectsOfType( CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE, animMgrObjectList );
    if ( animMgrObjectList.GetCount() == 0 )
    {
        return 0;
    }

    // Find its events
    atArray<cutfEvent *> animMgrEventList;
    cutFile.FindEventsForObjectIdOnly( animMgrObjectList[0]->GetObjectId(), cutFile.GetEventList(), animMgrEventList );

    // Sort the anim events into buckets by the event args object id
    atMap<int, atArray<cutfEvent *>> objectAnimMgrEventMap;
    for ( int i = 0; i < animMgrEventList.GetCount(); ++i )
    {
        if ( (animMgrEventList[i]->GetEventId() != CUTSCENE_SET_ANIM_EVENT) && (animMgrEventList[i]->GetEventId() != CUTSCENE_CLEAR_ANIM_EVENT) )
        {
            continue;
        }

        const cutfObjectIdNameEventArgs *pObjectIdNameEventArgs = dynamic_cast<const cutfObjectIdNameEventArgs *>( animMgrEventList[i]->GetEventArgs() );
        atArray<cutfEvent *> *pObjectEventList = objectAnimMgrEventMap.Access( pObjectIdNameEventArgs->GetObjectId() );
        if ( pObjectEventList == NULL )
        {
            atArray<cutfEvent *> objectEventList;
            objectAnimMgrEventMap.Insert( pObjectIdNameEventArgs->GetObjectId(), objectEventList );

            pObjectEventList = objectAnimMgrEventMap.Access( pObjectIdNameEventArgs->GetObjectId() );

            cutfObject *pObject = cutFile.GetObjectList()[pObjectIdNameEventArgs->GetObjectId()];
            if ( pObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE )
            {
                // go ahead and fix the face dir
                cutfModelObject *pModelObject = dynamic_cast<cutfModelObject *>( pObject );
                if ( pModelObject->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
                {
                    cutfPedModelObject *pPedModelObject = dynamic_cast<cutfPedModelObject *>( pModelObject );

                    char cFaceAnimFilename[RAGE_MAX_PATH];

                    safecpy( cFaceAnimFilename, pPedModelObject->GetDefaultFaceAnimationFilename( pOutputFaceDir ).GetCStr(), sizeof(cFaceAnimFilename) );
                    pPedModelObject->SetFoundDefaultFaceAnimation( ASSET.Exists( cFaceAnimFilename, NULL ) );

                    safecpy( cFaceAnimFilename, pPedModelObject->GetFaceAnimationFilename( pOutputFaceDir ).GetCStr(), sizeof(cFaceAnimFilename) );
                    if ( strlen( cFaceAnimFilename ) > 0 )
                    {
                        if ( !ASSET.Exists( cFaceAnimFilename, NULL ) )
                        {                            
                            if ( pPedModelObject->OverrideFaceAnimation() )
                            {
                                Errorf( 
                                    "Ped Model '%s', when it was exported from Motion Builder, had the face animation '%s', but the file is now missing.", 
                                    pPedModelObject->GetDisplayName().c_str(), cFaceAnimFilename );

                                return 1;
                            }
                        }
                        else
                        {
                            Displayf( "Found %s face animation '%s' for Ped Model '%s'.", 
                                pPedModelObject->OverrideFaceAnimation() ? "overridden" : "default", 
                                cFaceAnimFilename, pPedModelObject->GetDisplayName().c_str() );
                        }
                    }
                    else
                    {
                        Displayf( "The face animation for Ped Model '%s', if any, is not being used.", pPedModelObject->GetDisplayName().c_str() );
                    }
                }
            }
        }

        pObjectEventList->Grow() = animMgrEventList[i];
    }

    // now, go through each bucket and merge the event pairs.
    atMap<int, atArray<cutfEvent *>>::Iterator iter = objectAnimMgrEventMap.CreateIterator();
    for ( iter.Start(); !iter.AtEnd(); iter.Next() )
    {
        cutfObject *pObject = cutFile.GetObjectList()[iter.GetKey()];

        int result = MergeEvents( cutFile, pObject, iter.GetData(), CUTSCENE_SET_ANIM_EVENT, CUTSCENE_CLEAR_ANIM_EVENT, timeList );
        if ( result != 0 )
        {
            continue;
        }
    }

    return 0;
}

int MergeNonAnimEvents( cutfCutsceneFile2 &cutFile, const atArray<float> &timeList )
{
    const atArray<cutfObject *> &objectList = cutFile.GetObjectList();
    for ( int i = 0; i < objectList.GetCount(); ++i )
    {
        // Find its events
        atArray<cutfEvent *> eventList;
        cutFile.FindEventsForObjectIdOnly( objectList[i]->GetObjectId(), cutFile.GetEventList(), eventList );

        atMap<int, int> mergedEventIds;
        for ( int j = 0; j < eventList.GetCount(); ++j )
        {
            // Don't merge anim events, we handle that elsewhere
            // Don't merge audio events, the cutfCutsceneFile2::Concat() took care of that already
            if ( (eventList[j]->GetEventId() == CUTSCENE_SET_ANIM_EVENT) || (eventList[j]->GetEventId() == CUTSCENE_CLEAR_ANIM_EVENT)
                || (eventList[j]->GetEventId() == CUTSCENE_PLAY_AUDIO_EVENT) || (eventList[j]->GetEventId() == CUTSCENE_STOP_AUDIO_EVENT) )
            {
                continue;
            }

            int *pInt = mergedEventIds.Access( eventList[j]->GetEventId() );
            if ( pInt != NULL )
            {
                continue;
            }

            int iOppositeEventId = cutfEvent::GetOppositeEventId( eventList[j]->GetEventId() );
            if ( iOppositeEventId < 0 )
            {
                continue;
            }

            int iRank = cutfEvent::GetRank( eventList[j]->GetEventId() );
            int iOppositeRank = cutfEvent::GetRank( iOppositeEventId );
            if ( iOppositeRank >= iRank )
            {
                continue;
            }

            int result = MergeEvents( cutFile, objectList[i], eventList, eventList[j]->GetEventId(), iOppositeEventId, timeList );
            if ( result != 0 )
            {
                return result;
            }

            mergedEventIds.Insert( eventList[j]->GetEventId(), eventList[j]->GetEventId() );

            // refresh the list
            eventList.Reset();
            cutFile.FindEventsForObjectIdOnly( objectList[i]->GetObjectId(), cutFile.GetEventList(), eventList );
            j = -1;
        }
    }

    return 0;
}

void GatherCropData( const SCutsceneInputItem &cutsceneInputItem, atArray<SCropFileItem> &cropFileItemList, 
                   bool bCropStart, bool bCropEnd, int iIndex )
{
    float fStartTime = bCropStart ? cutsceneInputItem.pCutfile->GetRangeStart() / 30.0f : 0.0f;
    float fEndTime = bCropEnd ? cutsceneInputItem.pCutfile->GetRangeEnd() / 30.0f : FLT_MAX;

    // Find all of the animations that need to be cropped
    atArray<cutfObject *> modelObjectList;
    cutsceneInputItem.pCutfile->FindObjectsOfType( CUTSCENE_MODEL_OBJECT_TYPE, modelObjectList );
    for ( int i = 0; i < modelObjectList.GetCount(); ++i )
    {
        SCropFileItem cropFileItem;
        cropFileItem.bIsClip = true;
        cropFileItem.fStartTime = fStartTime;
        cropFileItem.fEndTime = fEndTime;                
        cropFileItem.inputName = modelObjectList[i]->GetDisplayName().c_str();               
        cropFileItem.inputDir = cutsceneInputItem.dir;

        // Add an index to the end, in case we are concatenating the same scene multiple times
        char cOutputName[RAGE_MAX_PATH];
        formatf( cOutputName, sizeof(cOutputName), "%s_crop%d", cropFileItem.inputName.c_str(), iIndex );
        cropFileItem.outputName = cOutputName;

        cropFileItem.outputDir = "$(outputDir)";

        cropFileItemList.PushAndGrow( cropFileItem );

        const cutfModelObject *pModelObject = dynamic_cast<const cutfModelObject *>( modelObjectList[i] );
        if ( pModelObject->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
        {
            const cutfPedModelObject *pPedModelObject = dynamic_cast<const cutfPedModelObject *>( pModelObject );

            char cFaceAnimFilename[RAGE_MAX_PATH];
            safecpy( cFaceAnimFilename, pPedModelObject->GetFaceAnimationFilename( cutsceneInputItem.faceDir.c_str() ).GetCStr(), 
                sizeof(cFaceAnimFilename) );
            if ( (strlen( cFaceAnimFilename ) > 0) && ASSET.Exists( cFaceAnimFilename, "" ) )
            {
                SCropFileItem faceCropFileData;
                faceCropFileData.bIsFaceAnim = true;
                faceCropFileData.fStartTime = fStartTime;
                faceCropFileData.fEndTime = fEndTime;                
                faceCropFileData.inputName = cFaceAnimFilename;
                faceCropFileData.inputDir = cutsceneInputItem.faceDir;
                faceCropFileData.outputDir = "$(outputDir)";

                char cExtension[16];
                safecpy( cExtension, ASSET.FindExtensionInPath( cFaceAnimFilename ), sizeof(cExtension) );
                
                faceCropFileData.bIsClip = stricmp( cExtension, ".clip" ) == 0;

                ASSET.RemoveExtensionFromPath( cFaceAnimFilename, sizeof(cFaceAnimFilename), cFaceAnimFilename );

                // Add an index to the end, in case we are concatenating the same scene multiple times
                char cOutputFilename[RAGE_MAX_PATH];
                formatf( cOutputFilename, sizeof(cOutputName), "%s_crop%d%s", ASSET.FileName( cFaceAnimFilename ), iIndex, cExtension );
                faceCropFileData.outputName = cOutputFilename;

                cropFileItemList.PushAndGrow( faceCropFileData );
            }
        }
    }

    atArray<cutfObject *> lightObjectList;
    cutsceneInputItem.pCutfile->FindObjectsOfType( CUTSCENE_LIGHT_OBJECT_TYPE, lightObjectList );
    for ( int i = 0; i < lightObjectList.GetCount(); ++i )
    {
        SCropFileItem cropFileItem;
        cropFileItem.bIsClip = true;
        cropFileItem.fStartTime = fStartTime;
        cropFileItem.fEndTime = fEndTime;                
        cropFileItem.inputName = lightObjectList[i]->GetDisplayName().c_str();               
        cropFileItem.inputDir = cutsceneInputItem.dir;

        // Add an index to the end, in case we are concatenating the same scene multiple times
        char cOutputName[RAGE_MAX_PATH];
        formatf( cOutputName, sizeof(cOutputName), "%s_crop%d", cropFileItem.inputName.c_str(), iIndex );
        cropFileItem.outputName = cOutputName;

        cropFileItem.outputDir = "$(outputDir)";

        cropFileItemList.PushAndGrow( cropFileItem );
    }

    atArray<cutfObject *> particleObjectList;
    cutsceneInputItem.pCutfile->FindObjectsOfType( CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE, particleObjectList );
    for ( int i = 0; i < particleObjectList.GetCount(); ++i )
    {
        SCropFileItem cropFileItem;
        cropFileItem.bIsClip = true;
        cropFileItem.fStartTime = fStartTime;
        cropFileItem.fEndTime = fEndTime;                
        cropFileItem.inputName = particleObjectList[i]->GetDisplayName().c_str();               
        cropFileItem.inputDir = cutsceneInputItem.dir;

        // Add an index to the end, in case we are concatenating the same scene multiple times
        char cOutputName[RAGE_MAX_PATH];
        formatf( cOutputName, sizeof(cOutputName), "%s_crop%d", cropFileItem.inputName.c_str(), iIndex );
        cropFileItem.outputName = cOutputName;

        cropFileItem.outputDir = "$(outputDir)";

        cropFileItemList.PushAndGrow( cropFileItem );
    }

    atArray<cutfObject *> cameraObjectList;
    cutsceneInputItem.pCutfile->FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );
    for ( int i = 0; i < cameraObjectList.GetCount(); ++i )
    {
        SCropFileItem cropFileItem;
        cropFileItem.bIsClip = true;
        cropFileItem.fStartTime = fStartTime;
        cropFileItem.fEndTime = fEndTime;                
        cropFileItem.inputName = cameraObjectList[i]->GetDisplayName().c_str();               
        cropFileItem.inputDir = cutsceneInputItem.dir;

        // Add an index to the end, in case we are concatenating the same scene multiple times
        char cOutputName[RAGE_MAX_PATH];
        formatf( cOutputName, sizeof(cOutputName), "%s_crop%d", cropFileItem.inputName.c_str(), iIndex );
        cropFileItem.outputName = cOutputName;

        cropFileItem.outputDir = "$(outputDir)";

        cropFileItemList.PushAndGrow( cropFileItem );
    }
}

void FixUpAudio(cutfCutsceneFile2* pCutfile)
{
	// replace all instances of the audio with the concat audio
	const char* pAudio = NULL;
	if ( PARAM_audioFile.Get( pAudio ) )
	{
		atString strAudio(pAudio);
		if(strAudio != "")
		{
			char czDestPath[RAGE_MAX_PATH];
			ASSET.RemoveExtensionFromPath(czDestPath, RAGE_MAX_PATH, strAudio.c_str());

			const atArray<cutfObject *>& atObjectList = pCutfile->GetObjectList();

			int iObjectID = -1;
			for(int i=0; i < atObjectList.GetCount(); ++i)
			{
				cutfAudioObject* pObject = dynamic_cast<cutfAudioObject *>(atObjectList[i]);
				if(pObject)
				{
					iObjectID = pObject->GetObjectId();
					pObject->SetName(strAudio.c_str());
				}
			}

			// find events attached to this object and then modify the args

			atArray<cutfEvent*>& atLoadList = const_cast<atArray<cutfEvent*> &>(pCutfile->GetLoadEventList() );
			for(int i=0; i < atLoadList.GetCount(); ++i)
			{
				cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent *>(atLoadList[i]);
				if(pEvent)
				{
					if(pEvent->GetObjectId() == iObjectID)
					{
						cutfNameEventArgs* pEventArgs = dynamic_cast<cutfNameEventArgs *>( const_cast<cutfEventArgs *>( pEvent->GetEventArgs() ));
						if(pEventArgs)
						{
							pEventArgs->SetName(czDestPath);
						}
					}
				}
			}

			atArray<cutfEvent*>& atList = const_cast<atArray<cutfEvent*> &>(pCutfile->GetEventList() );
			for(int i=0; i < atList.GetCount(); ++i)
			{
				cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent *>(atList[i]);
				if(pEvent)
				{
					if(pEvent->GetObjectId() == iObjectID)
					{
						cutfNameEventArgs* pEventArgs = dynamic_cast<cutfNameEventArgs *>( const_cast<cutfEventArgs *>( pEvent->GetEventArgs() ));
						if(pEventArgs)
						{
							pEventArgs->SetName(czDestPath);
						}
					}
				}
			}
		}
	}

	atArray<cutfObject *> audioObjectList;
	pCutfile->FindObjectsOfType( CUTSCENE_AUDIO_OBJECT_TYPE, audioObjectList );
	if ( audioObjectList.GetCount() > 0 )
	{
		if ( pCutfile->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_ONE_AUDIO_FLAG ))
		{
			// If we're the audio object
			atArray<cutfEvent *> objectEventList;
			pCutfile->FindEventsForObjectIdOnly( audioObjectList[audioObjectList.GetCount()-1]->GetObjectId(), pCutfile->GetEventList(), objectEventList );

			for ( int i = objectEventList.GetCount() - 1; i >= 0; --i )
			{
				if ( (objectEventList[i]->GetEventId() == CUTSCENE_STOP_AUDIO_EVENT))
				{
					// move the Stop Audio event to the end
					objectEventList[i]->SetTime( pCutfile->GetTotalDuration() );
					break;
				}
			}
		}
	}

	// Fix up the audio so we never have gaps - set the stop event time to the next play time
	//atArray<cutfObject *> audioObjectList;
	//pCutfile->FindObjectsOfType( CUTSCENE_AUDIO_OBJECT_TYPE, audioObjectList );
	if ( audioObjectList.GetCount() > 0 )
	{
		// If we're the audio object
		atArray<cutfEvent *> objectEventList;
		pCutfile->FindEventsForObjectIdOnly( audioObjectList[audioObjectList.GetCount()-1]->GetObjectId(), pCutfile->GetEventList(), objectEventList );

		for(int i=0; i < objectEventList.GetCount(); i++)
		{
			if ( (objectEventList[i]->GetEventId() == CUTSCENE_STOP_AUDIO_EVENT))
			{
				if(i+1 < objectEventList.GetCount())
					objectEventList[i]->SetTime(objectEventList[i+1]->GetTime());
			}
		}
	}
}

void CreateSectionTimeList(cutfCutsceneFile2 *pMergedCutFile, const atArray<SCutsceneInputItem>& cutsceneInputItemList, const float fCompleteDuration, atArray<float>& sectionTimeList)
{
	if(PARAM_external.Get())
	{
		int iSectionMethod = 0;
		if(PARAM_sectionMethod.Get( iSectionMethod ))
		{
			if(iSectionMethod != 3) // 0 == none - 3 == use children
			{
				if(iSectionMethod == 0)
				{
					const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
					const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG );
					const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG );

					return;
				}

				if(iSectionMethod == 1) // time slice
				{
					float fDuration=0;
					for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
					{
						if(i != 0)
						{
							sectionTimeList.Grow() = fDuration; // put a section on the concat boundary
						}

						float fSectionTimeSliceDuration = 0;
						PARAM_sectionDuration.Get( fSectionTimeSliceDuration );

						float fAcutalDuration = (float)(cutsceneInputItemList[i].pCutfile->GetRangeEnd()-cutsceneInputItemList[i].pCutfile->GetRangeStart())/30;

						float fTime = fSectionTimeSliceDuration;
						while ( fTime < fAcutalDuration )
						{
							sectionTimeList.Grow() = (fTime + fDuration);
							fTime += fSectionTimeSliceDuration;
						}

						fDuration += cutsceneInputItemList[i].pCutfile->GetTotalDuration();
					}
				}

				// The sectioning is overridden
				if(iSectionMethod == 2) // camera cut
				{
					float fDuration=0;
					for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
					{
						const atArray<float>& cameraCutList = cutsceneInputItemList[i].pCutfile->GetCameraCutList();

						for(int j=0; j < cameraCutList.GetCount(); ++j)
						{				
							float fTime = cameraCutList[j];
							if( fTime < (float)cutsceneInputItemList[i].pCutfile->GetRangeStart()/30)
							{
								continue;
							}

							float fSectionTime = (fTime - (float)cutsceneInputItemList[i].pCutfile->GetRangeStart()/30) + fDuration;
							if(fSectionTime < fCompleteDuration)
							{
								sectionTimeList.Grow() = fSectionTime;
							}
						}
						
						fDuration += cutsceneInputItemList[i].pCutfile->GetTotalDuration();
					}
				}

				const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
				const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG );
				const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG );

				// set the new section list, the concatted scene time list will be in the camera cut list
				atArray<float> &mergedCameraCutList = const_cast<atArray<float> &>( pMergedCutFile->GetCameraCutList() );
				mergedCameraCutList.clear();

				if(sectionTimeList.size() > 0)
				{
					const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
					mergedCameraCutList = sectionTimeList;
				}
				return;
			}
		}
	}

	// Merge the sectioning methods into a list of time, bosh this all in the camera cut list - the cut file will eventually just contain a section
	// time list and wont have individual types of sectioning, as its all time at the end of the day.
	// normally the section times, ie camera cut list will match the camera cuts in an individual scene by list time - start range. in a concat 
	// however the range is zero'd so we need to ammend the times.

	float fDuration=0;
	for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
	{
		if(cutsceneInputItemList[i].pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG))
		{
			const atArray<float>& sectionSplitList = cutsceneInputItemList[i].pCutfile->GetSectionSplitList();

			for(int j=0; j < sectionSplitList.GetCount(); ++j)
			{
				float fTime = sectionSplitList[j];
				if( fTime < (float)cutsceneInputItemList[i].pCutfile->GetRangeStart()/30)
				{
					continue;
				}

				float fSectionTime = (fTime - (float)cutsceneInputItemList[i].pCutfile->GetRangeStart()/30) + fDuration;
				if(fSectionTime < fCompleteDuration)
				{
					sectionTimeList.Grow() = fSectionTime;
				}
			}
		}
		else if(cutsceneInputItemList[i].pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG))
		{
			const atArray<float>& cameraCutList = cutsceneInputItemList[i].pCutfile->GetCameraCutList();

			for(int j=0; j < cameraCutList.GetCount(); ++j)
			{				
				float fTime = cameraCutList[j];
				if( fTime < (float)cutsceneInputItemList[i].pCutfile->GetRangeStart()/30)
				{
					continue;
				}

				float fSectionTime = (fTime - (float)cutsceneInputItemList[i].pCutfile->GetRangeStart()/30) + fDuration;
				if(fSectionTime < fCompleteDuration)
				{
					sectionTimeList.Grow() = fSectionTime;
				}
			}
		}
		else if(cutsceneInputItemList[i].pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG))
		{
			if(i != 0)
			{
				sectionTimeList.Grow() = fDuration; // put a section on the concat boundary
			}

			float fSectionTimeSliceDuration = cutsceneInputItemList[i].pCutfile->GetSectionByTimeSliceDuration();

			float fAcutalDuration = (float)(cutsceneInputItemList[i].pCutfile->GetRangeEnd()-cutsceneInputItemList[i].pCutfile->GetRangeStart())/30;

			float fTime = fSectionTimeSliceDuration;
			while ( fTime < fAcutalDuration )
			{
				sectionTimeList.Grow() = (fTime + fDuration);
				fTime += fSectionTimeSliceDuration;
			}
		}

		fDuration += cutsceneInputItemList[i].pCutfile->GetTotalDuration();
		// force a section on the concat boundary
		/*if(i != cutsceneInputItemList.GetCount()-1)
		{
			sectionTimeList.Grow() = fDuration;
		}*/
	}

	const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
	const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG );
	const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG );

	// set the new section list, the concatted scene time list will be in the camera cut list
	atArray<float> &mergedCameraCutList = const_cast<atArray<float> &>( pMergedCutFile->GetCameraCutList() );
	mergedCameraCutList.clear();

	if(sectionTimeList.size() > 0)
	{
		const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG );
		mergedCameraCutList = sectionTimeList;
	}
}

int CalculateCompleteRange(const atArray<SCutsceneInputItem>& cutsceneInputItemList)
{
	// Calculate the complete duration that all concatted scenes will cover
	int iFullRange=0;
	for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
	{
		iFullRange += ((cutsceneInputItemList[i].pCutfile->GetRangeEnd()-cutsceneInputItemList[i].pCutfile->GetRangeStart()) +1);
	}

	return iFullRange;
}

float CalculateCompleteDuration(const atArray<SCutsceneInputItem>& cutsceneInputItemList)
{
	// Calculate the complete duration that all concatted scenes will cover
	float fFullConcatDuration=0;
	for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
	{
		fFullConcatDuration += cutsceneInputItemList[i].pCutfile->GetTotalDuration();
	}

	return fFullConcatDuration;
}

int EventsComparer( cutfEvent* const ppEvent1, cutfEvent* const ppEvent2 )
{
	return (ppEvent1)->Compare( ppEvent2 );
}

int Main()
{
    Init();

    const char *pCutlistFilename;
    PARAM_cutlistFile.Get( pCutlistFilename );

    // get the output directory
    const char* pOutputDir = "";
    PARAM_outputDir.Get( pOutputDir );

    // get the output face dirs
    char cOutputFaceDir[RAGE_MAX_PATH];

    const char *pOutputFaceDir = NULL;
    if ( !PARAM_outputFaceDir.Get( pOutputFaceDir ) || (strlen( pOutputFaceDir ) == 0) )
    {
        sprintf( cOutputFaceDir, "%s\\faces", pOutputDir );
        pOutputFaceDir = cOutputFaceDir;
    }

    // get the output filename
    char cOutputCutfile[RAGE_MAX_PATH];

    const char* pOutputCutfile = NULL;
    if ( !PARAM_outputCutfile.Get( pOutputCutfile ) || (strlen( pOutputCutfile ) == 0) )
    {
        sprintf( cOutputCutfile, "%s\\data.%s", pOutputDir, PI_CUTSCENE_XMLFILE_EXT );
        pOutputCutfile = cOutputCutfile;
    }
    else
    {
        safecpy( cOutputCutfile, pOutputCutfile, sizeof(cOutputCutfile) );
    }

    char cOutputCutfileName[RAGE_MAX_PATH];

    // Make sure we always save as .cutxml
    ASSET.RemoveExtensionFromPath( cOutputCutfile, sizeof(cOutputCutfile), cOutputCutfile );
    safecpy( cOutputCutfileName, ASSET.FileName( cOutputCutfile ), sizeof(cOutputCutfileName) );

    safecat( cOutputCutfile, ".", sizeof(cOutputCutfile) );
    safecat( cOutputCutfile, PI_CUTSCENE_XMLFILE_EXT, sizeof(cOutputCutfile) );

	// Load the cutlist
    atArray<SCutsceneInputItem> cutsceneInputItemList;
    if ( !LoadCutListFile( pCutlistFilename, cutsceneInputItemList ) )
    {
        Shutdown();
        return -1;
    }

  //  if ( cutsceneInputItemList.GetCount() < 2 )
  //  {
		//char cMsg[RAGE_MAX_PATH];
		//sprintf(cMsg, "ERROR: At least two cutscenes are required. You provided %d.", cutsceneInputItemList.GetCount() );
		//ULOGGER.SetProgressMessage(cMsg);

  //      Shutdown();
  //      return -1;
  //  }

    for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
    {
        char cInputCutFilenameNoExt[RAGE_MAX_PATH];
        ASSET.RemoveExtensionFromPath( cInputCutFilenameNoExt, sizeof(cInputCutFilenameNoExt), cutsceneInputItemList[i].cutFilename.c_str() );
        const char *pInputCutfileName = ASSET.FileName( cInputCutFilenameNoExt );

        if ( stricmp( cOutputCutfileName, pInputCutfileName ) == 0 )
        {
			char cMsg[RAGE_MAX_PATH];
			sprintf(cMsg, "ERROR: The output name matches an input name. Please rename the output." );
			ULOGGER.SetProgressMessage(cMsg);

            Shutdown();
            return -1;
        }
    }

    // do our own timestamp comparisons
    bool bSaveMergedCutfile = true;
    if ( ASSET.Exists( pOutputCutfile, NULL ) )
    {
        bool bInputsAreNewer = false;
        for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
        {
            if ( fiDevice::GetDevice( cutsceneInputItemList[i].cutFilename.c_str() )->IsOutOfDate( 
                cutsceneInputItemList[i].cutFilename.c_str(), pOutputCutfile ) 
                && ASSET.Exists( cutsceneInputItemList[i].cutFilename.c_str(), NULL ) )
            {
                bInputsAreNewer = true;
                break;
            }
        }

        if ( !bInputsAreNewer && !PARAM_skipExeCheck.Get() )
        {
            char **pArgs = sysParam::GetArgArray();
            if ( fiDevice::GetDevice( pArgs[0] )->IsOutOfDate( pArgs[0], pOutputCutfile ) 
                && ASSET.Exists( pArgs[0], NULL ) )
            {
                bInputsAreNewer = true;
            }
        }

        if ( !bInputsAreNewer )
        {
            if ( fiDevice::GetDevice( pCutlistFilename )->IsOutOfDate( pCutlistFilename, pOutputCutfile ) )
            {
                bInputsAreNewer = true;
            }
        }

        if ( !bInputsAreNewer )
        {
            bSaveMergedCutfile = false;
        }
    }

    if ( !bSaveMergedCutfile )
    {
		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg, "Merged cut file '%s' is up to date.", pOutputCutfile );
		ULOGGER.SetProgressMessage(cMsg);

        // make sure we have all of our intermediate files
        //SaveIntermediateFiles( pOutputDir, pOutputCutfile, cutsceneInputItemList );

        Shutdown();
        return 0;
    }

    // load the cut files
    for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
    {
        cutsceneInputItemList[i].pCutfile = rage_new cutfCutsceneFile2();

        if ( !LoadCutfile( cutsceneInputItemList[i].cutFilename.c_str(), *(cutsceneInputItemList[i].pCutfile) ) )
        {
            for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
            {
                if ( cutsceneInputItemList[i].pCutfile != NULL )
                {
                    delete cutsceneInputItemList[i].pCutfile;
                }
            }

            Shutdown();
            return -1;
        } 

		// we dont want multiple audio from this flag on an internal concat, only a external concat
		if(PARAM_internal.Get())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( cutsceneInputItemList[i].pCutfile->GetCutsceneFlags() );
			cutsceneFlags.Set(cutfCutsceneFile2::CUTSCENE_USE_AUDIO_EVENTS_CONCAT_FLAG, false);
		}

		atBitSet &cutsceneFlags = const_cast<atBitSet &>( cutsceneInputItemList[i].pCutfile->GetCutsceneFlags() );
		cutsceneFlags.Set(cutfCutsceneFile2::CUTSCENE_USE_ONE_AUDIO_FLAG, false);
    }

	// Calculate the complete duration that all concatted scenes will cover
	float fFullConcatDuration = CalculateCompleteDuration(cutsceneInputItemList);
	int iFullRange = CalculateCompleteRange(cutsceneInputItemList);

	// clone the first one and merge the rest with it
	cutfCutsceneFile2 *pMergedCutFile = cutsceneInputItemList[0].pCutfile->Clone();
	const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG );
	const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_USE_ONE_AUDIO_FLAG );
	const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_STREAM_PROCESSED );

	const char *pAudio = NULL;
	if ( PARAM_audioFile.Get( pAudio ) )
	{
		atString strAudio(pAudio);
		if(strAudio != "")
		{
			strAudio.Trim();
			strAudio.Lowercase();

			if((strAudio.IndexOf(EDITED_AUDIO_SUFFIX) != -1) || (strAudio.IndexOf(MASTERED_AUDIO_SUFFIX) != -1))
			{
				ULOGGER.SetProgressMessage("ERROR: Audio file must not be the EDITED or MASTERED version '%s'.", pAudio);
				Shutdown();
				return -1;
			}

			const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Set( cutfCutsceneFile2::CUTSCENE_USE_ONE_AUDIO_FLAG );

			atArray<cutfObject*> audioObjectList;
			pMergedCutFile->FindObjectsOfType(CUTSCENE_AUDIO_OBJECT_TYPE, audioObjectList);

			// If we have overriding audio then we only have one object, if the first scene in the concat has no audio object then we have issues.
			// Inject an audio object and basic events for the first scene. The correct audio naming will be setup by FixUpAudio function below.
			if(audioObjectList.GetCount() == 0)
			{
				cutfAudioObject* pAudioObject = rage_new cutfAudioObject(-1, "Object", (pMergedCutFile->GetRangeStart()/CUTSCENE_FPS));
				pMergedCutFile->AddObject(pAudioObject);

				pMergedCutFile->AddLoadEvent(rage_new cutfObjectIdEvent(pAudioObject->GetObjectId(), 0, CUTSCENE_LOAD_AUDIO_EVENT, rage_new cutfNameEventArgs("Object")));
				pMergedCutFile->AddEvent(rage_new cutfObjectIdEvent(pAudioObject->GetObjectId(), 0, CUTSCENE_PLAY_AUDIO_EVENT, rage_new cutfNameEventArgs("Object")));
				pMergedCutFile->AddEvent(rage_new cutfObjectIdEvent(pAudioObject->GetObjectId(), pMergedCutFile->GetTotalDuration(), CUTSCENE_STOP_AUDIO_EVENT, rage_new cutfNameEventArgs("Object")));
			}
		}
	}

	atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT>& concatDataList = const_cast<atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &>(pMergedCutFile->GetConcatDataList() );
	concatDataList.clear();
	cutfCutsceneFile2::SConcatData firstConcatData( pMergedCutFile->GetRealSceneName().c_str(), 0.0f, pMergedCutFile->GetOffset(), pMergedCutFile->GetRotation(), 0.0f, 0.0f, pMergedCutFile->GetRangeStart(), pMergedCutFile->GetRangeEnd() );
	concatDataList.Append() = firstConcatData;

	// concat each scene and pass in the walking duration for timing
	float fDuration=0;
    for ( int i = 1; i < cutsceneInputItemList.GetCount(); ++i )
    {
		fDuration += cutsceneInputItemList[i-1].pCutfile->GetTotalDuration();

        pMergedCutFile->Concat( *(cutsceneInputItemList[i].pCutfile), fDuration, false );
    }

	// Create the concatted section time list
	atArray<float> sectionTimeList;
	CreateSectionTimeList(pMergedCutFile, cutsceneInputItemList, fFullConcatDuration, sectionTimeList);
	
	// Cleanup
    for ( int i = 0; i < cutsceneInputItemList.GetCount(); ++i )
    {
        delete cutsceneInputItemList[i].pCutfile;
        cutsceneInputItemList[i].pCutfile = NULL;
    }

	FixUpAudio(pMergedCutFile);

    if ( bSaveMergedCutfile )
    {
		const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_PART_FLAG );
		const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_INTERNAL_CONCAT_FLAG );
		const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() ).Clear( cutfCutsceneFile2::CUTSCENE_EXTERNAL_CONCAT_FLAG );

		if(PARAM_internal.Get())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() );
			cutsceneFlags.Set(cutfCutsceneFile2::CUTSCENE_INTERNAL_CONCAT_FLAG, true);
		}

		if(PARAM_external.Get())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( pMergedCutFile->GetCutsceneFlags() );
			cutsceneFlags.Set(cutfCutsceneFile2::CUTSCENE_EXTERNAL_CONCAT_FLAG, true);
		}

        // Now that we're done, update the face directory
        pMergedCutFile->SetFaceDirectory( pOutputFaceDir );

		pMergedCutFile->SetTotalDuration(fFullConcatDuration);
		pMergedCutFile->SetRangeStart(0);
		pMergedCutFile->SetRangeEnd(iFullRange - 1); // concat is a merge of all +1 scenes and the range needs to be duration - 1 frame

		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg, "The new total duration is %f.", pMergedCutFile->GetTotalDuration() );
		ULOGGER.SetProgressMessage(cMsg);

        // save the file
		if ( !pMergedCutFile->SaveFile( pOutputCutfile ) )
        {
            delete pMergedCutFile;

            Shutdown();
            return -1;
        }
    }

    delete pMergedCutFile;

    Shutdown();

    return 0;
}
