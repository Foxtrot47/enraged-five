// 
// /cutfclipconcat.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/properties.h"
#include "crmetadata/tags.h"
#include "file/asset.h"
#include "system/exec.h"
#include "system/param.h"
#include "cutfile/cutfdefines.h"
#include "cutfutils.h"

#include "RsULog/ULogger.h"

using namespace rage;

REQ_PARAM( clips, "[cutfclipconcat] The list of comma-separated input files." );
REQ_PARAM( out, "[cutfclipconcat] The output file." );
PARAM( storeFullPath, "[cutfclipconcat] When set, the animation will be stored as the full path." );
PARAM( ulog, "[cutfanimcombine] Universal logging." );

#define BLOCK_TAG_PAD 0.001f

void Init()
{
	crAnimation::InitClass();
	crClip::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("cutfclipconcat").c_str(), "cutfclipconcat");
	}
}

void Shutdown()
{
	crAnimation::ShutdownClass();
	crClip::ShutdownClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PostExport();
	}
}

bool IsEntryNumeric(const char* pEntry)
{
	if(atoi(pEntry) != 0 || atof(pEntry) != 0)
	{
		return true;
	}

	return false;
} 

int Main()
{
	Init();

    ASSET.SetPath( RAGE_ASSET_ROOT );

    // get the input directories
    const int maxItems = CUTSCENE_MAX_CONCAT;
    const int maxItemsBufSize = maxItems * RAGE_MAX_PATH;

    // get the inputClips
    int numInputClips = 0;
    const char* inputClips[maxItems];
    char inputClipsBuf[maxItemsBufSize];

    numInputClips = PARAM_clips.GetArray( inputClips, maxItems, inputClipsBuf, maxItemsBufSize );

    atArray<atString> inputClipList;

    inputClipList.Reserve( numInputClips );
    for ( int i = 0; i < numInputClips; ++i )
    {
        inputClipList.Append() = inputClips[i];
        inputClipList[i].Trim();
    }

    // get the output filename
    const char *pOutputFilename;
    PARAM_out.Get( pOutputFilename );

    crAnimation::InitClass();
    crClip::InitClass();

    // load the output animation
    char cAnimFilename[RAGE_MAX_PATH];

    bool bStoreFullPath = false;

    const char *pValue = NULL;
    if ( PARAM_storeFullPath.Get( pValue ) )
    {
        bStoreFullPath = (stricmp( pValue, "0" ) != 0) && (stricmp( pValue, "f" ) != 0) && (stricmp( pValue, "false" ) != 0);
    }
        
    if ( bStoreFullPath )
    {
        safecpy( cAnimFilename, pOutputFilename, sizeof(cAnimFilename) );    
    }
    else
    {
        safecpy( cAnimFilename, ASSET.FileName( pOutputFilename ), sizeof(cAnimFilename) );
    }

    ASSET.RemoveExtensionFromPath( cAnimFilename, sizeof(cAnimFilename), cAnimFilename );
    safecat( cAnimFilename, ".anim", sizeof(cAnimFilename) );

    char cDirectory[RAGE_MAX_PATH];
    ASSET.RemoveNameFromPath( cDirectory, sizeof(cDirectory), pOutputFilename );

    if ( !bStoreFullPath )
    {
        // Make sure we load the anim from a relative path so the clip stores it relative, too.
        ASSET.PushFolder( cDirectory );
    }

    crAnimation *pAnim = crAnimation::AllocateAndLoad( cAnimFilename );

    if ( !bStoreFullPath )
    {
        ASSET.PopFolder();
    }

    if ( pAnim == NULL )
    {
        Shutdown();
        return 1;
    }

    // create the output clip
    crClipAnimation *pOutputClip = crClipAnimation::AllocateAndCreate( *pAnim );
    if ( pOutputClip == NULL )
    {
        delete pAnim;

        Shutdown();
		return 1;
    }

    // release here, because crAnimation will start us at 1, then crClipAnimation gets us to 2, when we really just want 1
    pAnim->Release();
 
    // load the input clips and get the durations
    atArray<crClip *> clipList;
    clipList.Reserve( inputClipList.GetCount() );

    float totalDuration = 0.0f;
    atArray<float> timeList;
    timeList.Reserve( inputClipList.GetCount() );

    for ( int i = 0; i < inputClipList.GetCount(); ++i )
    {
		// Check if the input clip list has a time within it, ie "10" for padding concat purposes.

		if(IsEntryNumeric(inputClipList[i]))
		{
			clipList.Append() = NULL;
			timeList.Append() = totalDuration;

			totalDuration += (float)atof(inputClipList[i]);
		}
		else
		{
			crClip *pClip = crClip::AllocateAndLoad( inputClipList[i].c_str() );
			if ( pClip == NULL )
			{
				delete pOutputClip;

				for ( int j = 0; j < i; ++j )
				{
					delete clipList[j];
				}

				Shutdown();
				return 1;
			}

			clipList.Append() = pClip;
			timeList.Append() = totalDuration;

			totalDuration += pClip->GetDuration();
		}
    }

    for ( int i = 0; i < clipList.GetCount(); ++i )
    {
		// merge properties
        const crClip* srcClip = clipList[i];
		
		if(srcClip == NULL) continue;

		if(srcClip->HasProperties() && pOutputClip->HasProperties())
		{
			for(int p=0; p<srcClip->GetProperties()->GetNumProperties(); ++p)
			{
				bool match = false;
				for(int q=0; q<pOutputClip->GetProperties()->GetNumProperties(); ++q)
				{
					if(srcClip->GetProperties()->FindPropertyByIndex(p)->GetKey() == pOutputClip->GetProperties()->FindPropertyByIndex(q)->GetKey())
					{
						match = true;
						break;
					}
				}

				if(!match)
				{
					pOutputClip->GetProperties()->AddProperty(*srcClip->GetProperties()->FindPropertyByIndex(p));
				}
			}
		}

        if ( srcClip->HasTags() )
        {
            // transfer the tags, adjusting the phases for the new positions in the concatenated animation
            for ( int j = 0; j < srcClip->GetTags()->GetNumTags(); ++j )
            {
                // clone the tag
                crTag *pTag =  srcClip->GetTags()->GetTag( j )->Clone();

				// adjust the phase intervals
				float start = srcClip->ConvertPhaseToTime( pTag->GetStart() );
				start = pOutputClip->ConvertTimeToPhase( timeList[i] + start );

				float end = srcClip->ConvertPhaseToTime( pTag->GetEnd() );
				end = pOutputClip->ConvertTimeToPhase( timeList[i] + end );

				pTag->SetStartEnd(start, end);

                // add the tag
                pOutputClip->GetTags()->AddTag( *pTag );

				delete pTag;
            }
        }

		// We have an issue in which if the last frame of the shot is on a cut then it will be bad. So when this is concatted the animation cut 
		// looks bad but is in fact correct, just the data is bad. So to fix this we make a tag which goes around the whole thing.
		// For example, the cut should be 136/137 but the bad frame on 136 makes it look like 135/136. 
		// We create a block to go from 134.9 to 138.1 (2 frame +) or 134.9 to 137.1
		//  Value[135]:	-15.717843 -24.226639 -1.000248
		//	Value[136]:	-17.028484 -25.465164 -1.013803
		//	Value[137]:	-17.028889 -25.464323 -1.013826
		//	Value[138]:	-17.029287 -25.463480 -1.013849

        // add a block tag to cover the transition between concatenated anims
        if ( i > 0 )
        {
			//float fHalfFrame = (0.5f * (1.0f / CUTSCENE_FPS));
			float fFullFrame = (1.0f / CUTSCENE_FPS);
			float fTime = timeList[i];

			crTag tag;
			tag.GetProperty().SetName("Block");
			tag.SetStartEnd( pOutputClip->ConvertTimeToPhase( fTime - (fFullFrame /*+ fHalfFrame*/) - BLOCK_TAG_PAD ), pOutputClip->ConvertTimeToPhase( fTime + (fFullFrame /**2*/ /*+ fHalfFrame*/) + BLOCK_TAG_PAD ) );

			crPropertyAttributeFloat fltAttr;
			fltAttr.SetName("division");
			fltAttr.GetFloat() = 0.5f;
			tag.GetProperty().AddAttribute(fltAttr);
			
			pOutputClip->GetTags()->AddTag( tag );
        }
    }

	int result = 0;
    if ( !pOutputClip->Save( pOutputFilename ) )
    {
        result = 1;
    }

    // cleanup and exit
    delete pOutputClip;

    for ( int i = 0; i < clipList.GetCount(); ++i )
    {
        delete clipList[i];
    }

    Shutdown();

    return result;
}
