// 
// /cutfanimcombine.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE: The purpose of this tool is to generate the anim_combine.targets file that is used
//  to merge the face and body animations of ped models.

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "crclip/clipanimation.h"
#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/param.h"
#include "rexBase/animExportCtrl.h"
#include "customAttributeMgr.h"
#include "RsULog/ULogger.h"
#include "system/exec.h"

using namespace rage;

PARAM( inputDir, "[cutfanimcombine] The location where the scene's exported animations live." );
PARAM( inputFaceDir, "[cutfanimcombine] The location where the scene's exported face animations live.  Default:  $(inputDir)/faces" );
PARAM( intermediateDir, "[cutfanimcombine] The location where intermediate files should be saved, including the MSBuild targets file 'anim_combine.targets'.  Default:  $(inputDir)/obj" );
PARAM( assetsDir, "[cutfanimcombine] The location where assets live for the branch." );
PARAM( cutscenefile, "[cutfanimcombine] The cut file to create an MSBuild targets file for.  Default:  $(inputDir)/data.cutxml" );
PARAM( ulog, "[cutfanimcombine] Universal logging." );

void Init()
{
	INIT_PARSER;
	crAnimation::InitClass();
	crClip::InitClass();
	cutfEvent::InitClass();

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("cutfanimcombine").c_str(), "cutfanimcombine");
	}
}

void Shutdown()
{
	cutfEvent::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();        
	SHUTDOWN_PARSER;

	if(PARAM_ulog.Get())
	{
		ULOGGER.PostExport();
	}
}

s32 GetTrackIdFromName( const char* pTrackName )
{
	if(stricmp(pTrackName, "facialTranslation") == 0)
	{
		return kTrackFacialTranslation;
	}
	else if(stricmp(pTrackName, "facialRotation") == 0)
	{
		return kTrackFacialRotation;
	}
	else if(stricmp(pTrackName, "facialscale") == 0)
	{
		return kTrackFacialScale;
	}
	else if(stricmp(pTrackName, "facialControl") == 0)
	{
		return kTrackFacialControl;
	}
	else if(stricmp(pTrackName, "viseme") == 0)
	{
		return kTrackVisemes;
	}
	else if(stricmp(pTrackName, "facialTinting") == 0)
	{
		return kTrackFacialTinting;
	}
	
	return -1;
}

void FindTracksToRemove( const char *pBodyAnimFilename, const char *pFaceAnimFilename, const char* pFaceSpecfile, const char* pFaceAttributeFile, char *pRemoveTracks, int removeTracksLen, const char* pAssetsDir )
{
    pRemoveTracks[0] = 0;

    crAnimation *pBodyAnim = crAnimation::AllocateAndLoad( pBodyAnimFilename );
    if ( pBodyAnim == NULL )
    {
        Warningf( "Unable to open '%s'", pBodyAnimFilename );
        return;
    }

    crAnimation *pFaceAnim = crAnimation::AllocateAndLoad( pFaceAnimFilename );
    if ( pFaceAnim == NULL )
    {
        pBodyAnim->Release();
        return;
    }

	int totalTracks = (int)pFaceAnim->GetNumTracks();
    int count = 0;

    // assuming the body animation has had its face bones removed, go through and remove any bone tracks from the face
    // that are also found in the body
    for ( u32 i = 0; i < pBodyAnim->GetNumTracks(); ++i )
    {
        const crAnimTrack *pBodyAnimTrack = pBodyAnim->GetTrack( i );
        if ( (pBodyAnimTrack == NULL)
            || ((pBodyAnimTrack->GetTrack() != kTrackBoneTranslation)
            && (pBodyAnimTrack->GetTrack() != kTrackBoneRotation)) )
        {
            continue;
        }

        const crAnimTrack *pFaceAnimTrack = pFaceAnim->FindTrack( pBodyAnimTrack->GetTrack(), pBodyAnimTrack->GetId() );
        if ( pFaceAnimTrack == NULL )
        {
            continue;
        }

        char cRemoveTrack[64];
        sprintf( cRemoveTrack, "remove[%d,%d]", pFaceAnimTrack->GetTrack(), pFaceAnimTrack->GetId() );

        if ( (int)strlen( pRemoveTracks ) > 0 )
        {
            safecat( pRemoveTracks, ";", removeTracksLen );
        }

        safecat( pRemoveTracks, cRemoveTrack, removeTracksLen );
        Assert( (int)strlen(pRemoveTracks) < removeTracksLen );
        ++count;
    }

	char cToolsRoot[RAGE_MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsRoot, RAGE_MAX_PATH);

	char cSpec[RAGE_MAX_PATH];
	sprintf(cSpec, "%s\\etc\\config\\anim\\control_templates\\%s", cToolsRoot, pFaceSpecfile);

	char cAttributesFile[RAGE_MAX_PATH] = {0};
	if (pFaceAttributeFile != NULL)
		sprintf(cAttributesFile, pFaceAttributeFile);

	ULOGGER.SetProgressMessage("Attribute File Pre: %s", cAttributesFile );

	atString atAttributeFile(cAttributesFile);
	atAttributeFile.Lowercase();
	atAttributeFile.Replace("$(assets)", pAssetsDir);
	sprintf(cAttributesFile, atAttributeFile.c_str());

	ULOGGER.SetProgressMessage("Attribute File Post: %s", cAttributesFile );

	if(pFaceSpecfile != NULL && (stricmp(pFaceSpecfile, "3Lateral_A_NO_ON_GTAV_OPTIMISED_SPEC.xml") == 0 ||
		stricmp(pFaceSpecfile, "3Lateral_GTAV_SPEC.xml") == 0 ||
		stricmp(pFaceSpecfile, "Ambient_Face_Rig_spec.xml") == 0))
	{
		ULOGGER.SetProgressMessage("%s", cSpec );

		CustomAttributeMgr attributeManager;

		if(pFaceAnimFilename && strlen(cAttributesFile) == 0)
		{
			//Assume that this is a normal ambient ped.  
			//Use a version of the .attribute files that is not bound to the specific cutscene asset.
			sprintf(cAttributesFile, "%s\\anim\\expressions\\ambient\\ambient_facial.xml.attributes", pAssetsDir);
		}

		ULOGGER.SetProgressMessage("Attribute File: %s", cAttributesFile );

		if (ASSET.Exists(cAttributesFile, NULL))
		{
			ULOGGER.SetProgressMessage("Loaded: %s", cAttributesFile );
			attributeManager.LoadAttributeFile(atString(cAttributesFile));
			
			// Load the face ctrl file, check the tracks in the face anim versus the ctrl file, remove any differences
			AnimExportCtrlSpec animSpecFile;
			if(animSpecFile.LoadFromXML(cSpec))
			{
				for(u32 i=0; i < pFaceAnim->GetNumTracks(); ++i)
				{
					const crAnimTrack *pFaceAnimTrack = pFaceAnim->GetTrack(i);

					// Track is the type. ie facialRotation
					// ID is the hash of the bone name

					bool bFoundTrack = false;
					for(s32 j=0 ; j < animSpecFile.GetNumTrackSpecs(); ++j)
					{
						const AnimExportCtrlTrackSpec& animTrackSpec = animSpecFile.GetTrackSpec(j);
						for(s32 k=0; k < animTrackSpec.GetTrackCount(); ++k)
						{
							const AnimExportCtrlTrack& animCtrlTrack = animTrackSpec.GetTrack(k);

							// face bones are normally straight hashed but facialcontrols have a name and an id. The name is in the spec,
							// so we map the data and get the id from the name and hash it.
							u32 hashValue = atHash16U(animTrackSpec.GetNameExpr());
							atString newHash;
							if(attributeManager.FindAttribute(atString(animTrackSpec.GetNameExpr()), newHash))
							{
								hashValue = atHash16U(newHash.c_str());
							}
							
							if((pFaceAnimTrack->GetId() == hashValue &&
								pFaceAnimTrack->GetTrack() == GetTrackIdFromName(animCtrlTrack.GetInputName())) 
								||
								(pFaceAnimTrack->GetTrack() == GetTrackIdFromName(animCtrlTrack.GetInputName()) &&
								pFaceAnimTrack->GetTrack() == kTrackFacialTinting))
							{
								bFoundTrack = true;
								//break;
							}
						}
					}

					if(!bFoundTrack)
					{
						char cRemoveTrack[64];
						sprintf( cRemoveTrack, "remove[%d,%d]", pFaceAnimTrack->GetTrack(), pFaceAnimTrack->GetId() );

						if ( (int)strlen( pRemoveTracks ) > 0 )
						{
							safecat( pRemoveTracks, ";", removeTracksLen );
						}

						safecat( pRemoveTracks, cRemoveTrack, removeTracksLen );
						Assert( (int)strlen(pRemoveTracks) < removeTracksLen );
						++count;
					}
				}
			}
		}
		else
		{
			ULOGGER.SetProgressMessage("File does not exist: %s", cAttributesFile );
		}
	}
	else
	{
		ULOGGER.SetProgressMessage("WARNING: Potentially invalid spec file used: %s", cSpec );
	}

	ULOGGER.SetProgressMessage("Found %d tracks to remove from '%s'", count, pFaceAnimFilename );
	
	if (totalTracks == count)
	{
		ULOGGER.SetProgressMessage("ERROR: All tracks removed from '%s'", pFaceAnimFilename );
	}

    pFaceAnim->Release();
    pBodyAnim->Release();
}

void WriteAnimCombineItem( parStreamOut *s, const char *pInputDir, const char *pInputFaceDir, const char* pAssetDir, const char *pAnimName, const char* pFaceSpecfile, const char* pFaceAttributeFile,
                          const char *pFaceFilenameOverride=NULL )
{  
    SRemoveTracksItem removeTracksItem;

    char cBodyAnimFilename[RAGE_MAX_PATH];
    sprintf( cBodyAnimFilename, "%s\\%s.anim", pInputDir, pAnimName );

    char cFaceFilename[RAGE_MAX_PATH];
    if ( pFaceFilenameOverride != NULL )
    {
        safecpy( cFaceFilename, pFaceFilenameOverride, sizeof(cFaceFilename) );        
    }
    else
    {
        sprintf( cFaceFilename, "%s\\%s_face", pInputFaceDir, pAnimName );
    }

    char cFaceAnimFilename[RAGE_MAX_PATH];
    cFaceAnimFilename[0] = 0;

    // Determine if the file exists and what its extension is
    const char *pExtension = ASSET.FindExtensionInPath( cFaceFilename );
    if ( pExtension == NULL )
    {
        if ( ASSET.Exists( cFaceFilename, "clip" ) )
        {
            safecat( cFaceFilename, ".clip", sizeof(cFaceFilename) );
            
            crClip *pClip = crClip::AllocateAndLoad( cFaceFilename );
            if ( pClip != NULL )
            {
                crClipAnimation *pClipAnim = dynamic_cast<crClipAnimation *>( pClip );
                if ( !ASSET.IsAbsolutePath( pClipAnim->GetAnimation()->GetName() ) )
                {
                    char cDirectory[RAGE_MAX_PATH];
                    ASSET.RemoveNameFromPath( cDirectory, sizeof(cDirectory), cFaceFilename );

                    ASSET.PushFolder( cDirectory );
                    ASSET.FullPath( cFaceAnimFilename, sizeof(cFaceAnimFilename), pClipAnim->GetAnimation()->GetName(), "" );
                    ASSET.PopFolder();
                }
                else
                {
                    safecpy( cFaceAnimFilename, pClipAnim->GetAnimation()->GetName(), sizeof(cFaceAnimFilename) );
                }

                pClip->Release();
            }
        }
        else if ( ASSET.Exists( cFaceFilename, "anim" ) )
        {
            sprintf( cFaceAnimFilename, "%s.anim", cFaceFilename );
        }
        else
        {
			ULOGGER.SetProgressMessage("WARNING: The face file '%s.clip' and '%s.anim' do not exist.", cFaceFilename, cFaceFilename );
        }
    }
    else if ( !ASSET.Exists( cFaceAnimFilename, "" ) )
    {
		ULOGGER.SetProgressMessage("WARNING: The face file '%s' does not exist.", cFaceAnimFilename );
    }

    if ( strlen(cFaceAnimFilename) > 0 )
    {
        char cRemoveTracks[1024 * 6];
        FindTracksToRemove( cBodyAnimFilename, cFaceAnimFilename, pFaceSpecfile, pFaceAttributeFile, cRemoveTracks, sizeof(cRemoveTracks), pAssetDir );

        char cPaths[4][RAGE_MAX_PATH];
        int iPathCount = ASSET.GetPathCount();
        for ( int i = 0; i < iPathCount; ++i )
        {
            safecpy( cPaths[i], ASSET.GetPath( i ) );
        }

        ASSET.SetPath( pInputFaceDir );

        char cRelativePath[RAGE_MAX_PATH];
        ASSET.MakeRelative( cRelativePath, sizeof(cRelativePath), cFaceAnimFilename );

        atString path( cPaths[0] );
        for ( int i = 1; i < iPathCount; ++i )
        {
            path += ";";
            path += cPaths[i];
        }

        ASSET.SetPath( path.c_str() );

        SRemoveTracksItem removeTracksItem;
        if ( !ASSET.IsAbsolutePath( cRelativePath ) )
        {
            removeTracksItem.inputName = cRelativePath;
            removeTracksItem.inputName.Replace( "/", "\\" );
            removeTracksItem.inputName.Replace( "$", "" );

            if ( removeTracksItem.inputName.StartsWith( "\\" ) )
            {
                removeTracksItem.inputName.Set( removeTracksItem.inputName, 1 );
            }
        }
        else
        {
            removeTracksItem.inputName = cFaceAnimFilename;
        }

        removeTracksItem.inputDir = "$(inputFaceDir)";

        removeTracksItem.outputName = pAnimName;
        removeTracksItem.outputName += "_face_trim";
        removeTracksItem.outputDir = "$(intermediateDir)";
        removeTracksItem.removeTracks = cRemoveTracks;

        WriteRemoveTracksItem( s, removeTracksItem );
    }

    SCombineItem combineItem;
    combineItem.inputName = pAnimName;
    combineItem.inputDir = "$(inputDir)";
    combineItem.outputName = pAnimName;
    combineItem.outputName += "_dual";
    combineItem.outputDir = "$(outputDir)";
    combineItem.inputFaceName = pAnimName;
    combineItem.inputFaceName += "_face_trim";
    combineItem.intermediateDir = "$(intermediateDir)";

    WriteCombineItem( s, combineItem );
}

int Main()
{
    Init();

    // get the input directory
    const char* pInputDir = "";
    PARAM_inputDir.Get( pInputDir );

    // get the face dir
    char cInputFaceDir[RAGE_MAX_PATH];

    const char *pInputFaceDir = NULL;
    if ( !PARAM_inputFaceDir.Get( pInputFaceDir ) )
    {
        sprintf( cInputFaceDir, "%s\\faces", pInputDir );
        pInputFaceDir = cInputFaceDir;
    }

    // get the intermediate directory
    char cIntermediateDir[RAGE_MAX_PATH];

    const char* pIntermediateDir = NULL;
    if ( !PARAM_intermediateDir.Get( pIntermediateDir ) )
    {
        sprintf( cIntermediateDir, "%s\\obj", pInputDir );
        pIntermediateDir = cIntermediateDir;
    }

	const char* pAssetDir = NULL;
	if ( !PARAM_assetsDir.Get( pAssetDir ) )
	{
		Shutdown();
		return 1;
	}

    // get the cut filename
    char cCutFilename[RAGE_MAX_PATH];

    const char* pCutFilename = NULL;
    if ( !PARAM_cutscenefile.Get( pCutFilename ) )
    {
        sprintf( cCutFilename, "%s\\data.cutxml", pInputDir );
        pCutFilename = cCutFilename;
    }

    // load the cut file
    cutfCutsceneFile2 cutFile;
    if ( !LoadCutfile( pCutFilename, cutFile ) )
    {
		ULOGGER.SetProgressMessage("ERROR: Unable to load '%s'.", pCutFilename );

        Shutdown();
        return 1;
    }
        
    // start writing out the .targets file
    char cTargetsFilename[RAGE_MAX_PATH];
    sprintf( cTargetsFilename, "%s\\anim_combine.targets", pIntermediateDir );

    ASSET.CreateLeadingPath( cTargetsFilename );

	parSettings settings = PARSER.Settings();
	settings.SetFlag(parSettings::WRITE_XML_HEADER, false);

    parStreamOut *s = PARSER.OpenOutputStream( cTargetsFilename, "", parManager::XML, &settings);
    {
        parElement projectElt;
        projectElt.SetName( "Project", false );
        projectElt.AddAttribute( "xmlns", "http://schemas.microsoft.com/developer/msbuild/2003" );

        s->WriteBeginElement( projectElt );
        {
            parElement itemGroupElt;
            itemGroupElt.SetName( "ItemGroup", false );

            s->WriteBeginElement( itemGroupElt );
            {                
                // Models
                atArray<cutfObject *> modelObjectList;
                cutFile.FindObjectsOfType( CUTSCENE_MODEL_OBJECT_TYPE, modelObjectList );
                for ( int i = 0; i < modelObjectList.GetCount(); ++i )
                {                    
                    const cutfModelObject *pModelObject = dynamic_cast<const cutfModelObject *>( modelObjectList[i] );
                    if ( pModelObject->GetModelType() != CUTSCENE_PED_MODEL_TYPE )
                    {
                        continue;
                    }

                    atString animName( modelObjectList[i]->GetDisplayName().c_str() );
                    animName.Replace( " ", "_" );

                    const cutfPedModelObject *pPedModelObject = dynamic_cast<const cutfPedModelObject *>( pModelObject );

                    char cFaceAnimFilename[RAGE_MAX_PATH];
                    safecpy( cFaceAnimFilename, pPedModelObject->GetFaceAnimationFilename( pInputFaceDir ).GetCStr(), sizeof(cFaceAnimFilename) );
                    if ( strlen( cFaceAnimFilename ) > 0 )
                    {
                        if ( !ASSET.Exists( cFaceAnimFilename, NULL ) )
                        {                            
                            if ( pPedModelObject->FoundDefaultFaceAnimation() || pPedModelObject->OverrideFaceAnimation() )
                            {
								ULOGGER.SetProgressMessage("ERROR: Ped Model '%s', when it was exported from Motion Builder, had the face animation '%s', but the file is now missing.", 
									pPedModelObject->GetDisplayName().c_str(), cFaceAnimFilename );

                                s->Close();

                                const fiDevice *pDevice = fiDevice::GetDevice( cTargetsFilename );
                                if ( pDevice != NULL )
                                {
                                    pDevice->Delete( cTargetsFilename );
                                }

                                Shutdown();
                                return 1;
                            }

							// Skip ped models which have no facial animation
							continue;
                        }
                        else
                        {
							ULOGGER.SetProgressMessage("Found %s face animation '%s' for Ped Model '%s'.", 
								pPedModelObject->OverrideFaceAnimation() ? "overridden" : "default", 
								cFaceAnimFilename, pPedModelObject->GetDisplayName().c_str() );
                        }

                        WriteAnimCombineItem( s, pInputDir, pInputFaceDir, pAssetDir, animName.c_str(), pPedModelObject->GetFaceExportCtrlSpecFile().GetCStr(), pPedModelObject->GetFaceAttributesFilename().GetCStr(),
                            pPedModelObject->OverrideFaceAnimation() ? cFaceAnimFilename : NULL );
                    }
                    else
                    {
						ULOGGER.SetProgressMessage("The face animation for Ped Model '%s', if any, is not being used.", pPedModelObject->GetDisplayName().c_str() );
					}
                }
            }
            s->WriteEndElement( itemGroupElt );
        }
        s->WriteEndElement( projectElt );
    }
    s->Close();
    delete s;

	ULOGGER.SetProgressMessage("Saved '%s'.", cTargetsFilename );

    Shutdown();
    return 0;
}
