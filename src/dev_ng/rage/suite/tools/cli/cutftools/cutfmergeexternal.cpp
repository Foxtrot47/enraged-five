// 
// /cutfstream.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"

#include "RsULog/ULogger.h"

using namespace rage;

PARAM( cutGame, "[cutfmergeexternal] The cutf file from light tool." );
REQ_PARAM( cutFile, "[cutfmergeexternal] The cutf file from light tool." );
PARAM( cutReplay, "[cutfmergeexternal] The cutf file from light tool." );
REQ_PARAM( outputCutfile, "[cutfmergeexternal] The cutf file to merge into." );
PARAM( errorCheck, "[cutfmergeexternal] Hides the allocation spam in the log." );
PARAM( noShadows, "[cutfmergeexternal] Do not merge shadows." );

void Init()
{
	INIT_PARSER;
	crAnimation::InitClass();
	crClip::InitClass();
	cutfEvent::InitClass();
}

void Shutdown()
{
	cutfEvent::ShutdownClass();      
	SHUTDOWN_PARSER;

	ULOGGER.PostExport();
}

s32 GetCameraObjectId(cutfCutsceneFile2* pCutfile)
{
	for(int i=0; i < pCutfile->GetObjectList().GetCount(); ++i)
	{
		if(pCutfile->GetObjectList()[i]->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE)
			return pCutfile->GetObjectList()[i]->GetObjectId();
	}

	return -1;
}

float GetCameraTime(cutfCutsceneFile2* pCutfile, cutfEvent* pShadowEvent, const atArray<cutfEvent*>& events)
{
	atHashString endOfSceneHash("endofscenecut");
	if(endOfSceneHash.GetHash() == pShadowEvent->GetStickyId())
	{
		return pCutfile->GetTotalDuration();
	}

	for(int i=0; i < events.GetCount(); ++i)
	{
		cutfEvent* pEvent = events[i];
		
		if(pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
		{
			cutfCameraCutEventArgs* pCameraArgs = dynamic_cast<cutfCameraCutEventArgs*>(const_cast<cutfEventArgs *>(pEvent->GetEventArgs()));
			if(pCameraArgs)
			{
				if(pCameraArgs->GetName().GetHash() == pShadowEvent->GetStickyId())
				{
					return pEvent->GetTime();
				}
			}
		}
	}

	return pShadowEvent->GetTime();
}

int Main()
{
	Init();

	const char *pCutFilename;
	if(!PARAM_cutFile.Get( pCutFilename ))
	{
		Shutdown();
		return -1;
	}

	// Load the cut file 
	cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();
	if(!LoadCutfile( pCutFilename, *(pCutfile) ))
	{
		Shutdown();
		return -1;
	}

	const char *pOutputCutFilename;
	if(!PARAM_outputCutfile.Get( pOutputCutFilename ))
	{
		Shutdown();
		return -1;
	}

	if(PARAM_cutGame.Get())
	{
		const char *pCutGameFilename;
		if(!PARAM_cutGame.Get( pCutGameFilename ))
		{
			Shutdown();
			return -1;
		}

		// Load the cut file 
		cutfCutsceneFile2* pCutGamefile = rage_new cutfCutsceneFile2();
		if(!LoadCutfile( pCutGameFilename, *(pCutGamefile) ))
		{
			Shutdown();
			return -1;
		}

		if(!IsClose(pCutfile->GetTotalDuration(), pCutGamefile->GetTotalDuration(), (float)0.005))
		{
			if(PARAM_errorCheck.Get())
			{
				ULOGGER.SetProgressMessage("ERROR: Game file '%s' has duration of '%f' which does not match cut file '%s' duration of '%f'", pCutGameFilename, pCutGamefile->GetTotalDuration(), pCutFilename, pCutfile->GetTotalDuration());
				Shutdown();
				return -1;
			}
			else
			{
				ULOGGER.SetProgressMessage("WARNING: Game file '%s' has duration of '%f' which does not match cut file '%s' duration of '%f'", pCutGameFilename, pCutGamefile->GetTotalDuration(), pCutFilename, pCutfile->GetTotalDuration());
			}
		}

		pCutfile->SetDayCoCHours(pCutGamefile->GetDayCoCHours());

		// Merge dof

		atArray<cutfEvent *> originalCameraEvents;
		FindEventsOfType(pCutfile->GetEventList(), originalCameraEvents, CUTSCENE_CAMERA_CUT_EVENT );

		atArray<cutfEvent *> cameraEvents;
		FindEventsOfType(pCutGamefile->GetEventList(), cameraEvents, CUTSCENE_CAMERA_CUT_EVENT );

		for(int i=0; i < cameraEvents.GetCount(); ++i)
		{
			cutfCameraCutEventArgs* pCameraEventArgs = static_cast<cutfCameraCutEventArgs*>((cutfEventArgs*)cameraEvents[i]->GetEventArgs());
			bool bFound = false;

			for(int j=0; j < originalCameraEvents.GetCount(); ++j)
			{
				cutfCameraCutEventArgs* pOriginalCameraEventArgs = static_cast<cutfCameraCutEventArgs*>((cutfEventArgs*)originalCameraEvents[j]->GetEventArgs());

				if(stricmp(pOriginalCameraEventArgs->GetName().GetCStr(), pCameraEventArgs->GetName().GetCStr()) == 0)
				{
					pOriginalCameraEventArgs->SetNearDrawDistance(pCameraEventArgs->GetNearDrawDistance());
					pOriginalCameraEventArgs->SetFarDrawDistance(pCameraEventArgs->GetFarDrawDistance());
					pOriginalCameraEventArgs->SetMapLodScale(pCameraEventArgs->GetMapLodScale());
					pOriginalCameraEventArgs->SetReflectionLodRangeStart(pCameraEventArgs->GetReflectionLodRangeStart());
					pOriginalCameraEventArgs->SetReflectionLodRangeEnd(pCameraEventArgs->GetReflectionLodRangeEnd());
					pOriginalCameraEventArgs->SetReflectionSLodRangeStart(pCameraEventArgs->GetReflectionSLodRangeStart());
					pOriginalCameraEventArgs->SetReflectionSLodRangeEnd(pCameraEventArgs->GetReflectionSLodRangeEnd());
					pOriginalCameraEventArgs->SetLodMultHD(pCameraEventArgs->GetLodMultHD());
					pOriginalCameraEventArgs->SetLodMultOrphanHD(pCameraEventArgs->GetLodMultOrphanHD());
					pOriginalCameraEventArgs->SetLodMultLod(pCameraEventArgs->GetLodMultLod());
					pOriginalCameraEventArgs->SetLodMultSlod1(pCameraEventArgs->GetLodMultSlod1());
					pOriginalCameraEventArgs->SetLodMultSlod2(pCameraEventArgs->GetLodMultSlod2());
					pOriginalCameraEventArgs->SetLodMultSlod3(pCameraEventArgs->GetLodMultSlod3());
					pOriginalCameraEventArgs->SetLodMultSlod4(pCameraEventArgs->GetLodMultSlod4());
					pOriginalCameraEventArgs->SetWaterReflectionFarClip(pCameraEventArgs->GetWaterReflectionFarClip());
					pOriginalCameraEventArgs->SetLightSSAOInten(pCameraEventArgs->GetLightSSAOInten());
					pOriginalCameraEventArgs->SetExposurePush(pCameraEventArgs->GetExposurePush());
					pOriginalCameraEventArgs->SetDisableHighQualityDof(pCameraEventArgs->GetDisableHighQualityDof());
					pOriginalCameraEventArgs->SetDirectionalLightDisabled(pCameraEventArgs->IsDirectionalLightDisabled());
					pOriginalCameraEventArgs->SetReflectionMapFrozen(pCameraEventArgs->IsReflectionMapFrozen());
					pOriginalCameraEventArgs->SetAbsoluteIntensityEnabled(pCameraEventArgs->IsAbsoluteIntensityEnabled());
					pOriginalCameraEventArgs->SetLightFadeDistanceMult(pCameraEventArgs->GetLightFadeDistanceMult());
					pOriginalCameraEventArgs->SetLightShadowFadeDistanceMult(pCameraEventArgs->GetLightShadowFadeDistanceMult());
					pOriginalCameraEventArgs->SetLightSpecularFadeDistMult(pCameraEventArgs->GetLightSpecularFadeDistMult());
					pOriginalCameraEventArgs->SetLightVolumetricFadeDistanceMult(pCameraEventArgs->GetLightVolumetricFadeDistanceMult());

					pOriginalCameraEventArgs->SetDirectionalLightMultiplier(pCameraEventArgs->GetDirectionalLightMultiplier());
					pOriginalCameraEventArgs->SetLensArtefactMultiplier(pCameraEventArgs->GetLensArtefactMultiplier());
					pOriginalCameraEventArgs->SetBloomMax(pCameraEventArgs->GetBloomMax());

					atArray<cutfCameraCutTimeOfDayDofModifier>& atCoCModifiers = pCameraEventArgs->GetCoCModifierList();
					for(int i=0; i < atCoCModifiers.GetCount(); ++i)
					{
						pOriginalCameraEventArgs->AddCoCModifier(atCoCModifiers[i].m_TimeOfDayFlags, atCoCModifiers[i].m_DofStrengthModifier);
					}

					bFound = true;
				}
			}

			if(!bFound)
			{
				if(PARAM_errorCheck.Get())
				{
					ULOGGER.SetProgressMessage("ERROR: Game file '%s' has camera named '%s' which does not exist in the cutfile", pCutGameFilename, pCameraEventArgs->GetName().GetCStr());
					Shutdown();
					return -1;
				}
				else
				{
					ULOGGER.SetProgressMessage("WARNING: Game file '%s' has camera named '%s' which does not exist in the cutfile", pCutGameFilename, pCameraEventArgs->GetName().GetCStr());
				}
			}
		}

		if(!PARAM_noShadows.Get())
		{
			ULOGGER.SetProgressMessage("Merging Shadows");

			// Merge shadow

			atArray<cutfEvent *> originalShadowEvents;
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_ENABLE_CASCADE_SHADOW_BOUNDS_EVENT );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_UPDATE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_AIRCRAFT_MODE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_FLY_CAMERA_MODE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_MINMAX );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_MINMAX );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_DEPTH_BIAS );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_SLOPE_BIAS );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE );
			FindEventsOfType(pCutfile->GetEventList(), originalShadowEvents, CUTSCENE_CASCADE_SHADOWS_RESET_CASCADE_SHADOWS );

			for(int i=0; i < originalShadowEvents.GetCount(); ++i)
			{
				pCutfile->RemoveEvent(originalShadowEvents[i]);
			}

			atArray<cutfEvent *> shadowEvents;
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_ENABLE_CASCADE_SHADOW_BOUNDS_EVENT );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_UPDATE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_AIRCRAFT_MODE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_FLY_CAMERA_MODE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_MINMAX );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_MINMAX );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_DEPTH_BIAS );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_SLOPE_BIAS );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE );
			FindEventsOfType(pCutGamefile->GetEventList(), shadowEvents, CUTSCENE_CASCADE_SHADOWS_RESET_CASCADE_SHADOWS );

			for(int i=0; i < shadowEvents.GetCount(); ++i)
			{
				float fTime = GetCameraTime(pCutfile, shadowEvents[i], pCutfile->GetEventList());
				shadowEvents[i]->SetTime(fTime);
				pCutfile->AddEvent(shadowEvents[i]);
			}

			// First person events
			atArray<cutfEvent *> originalFPEvents;
			FindEventsOfType(pCutfile->GetEventList(), originalFPEvents, CUTSCENE_FIRST_PERSON_BLENDOUT_CAMERA_EVENT );
			FindEventsOfType(pCutfile->GetEventList(), originalFPEvents, CUTSCENE_FIRST_PERSON_CATCHUP_CAMERA_EVENT );

			for(int i=0; i < originalFPEvents.GetCount(); ++i)
			{
				pCutfile->RemoveEvent(originalFPEvents[i]);
			}

			atArray<cutfEvent *> FPEvents;
			FindEventsOfType(pCutGamefile->GetEventList(), FPEvents, CUTSCENE_FIRST_PERSON_BLENDOUT_CAMERA_EVENT );
			FindEventsOfType(pCutGamefile->GetEventList(), FPEvents, CUTSCENE_FIRST_PERSON_CATCHUP_CAMERA_EVENT );

			for(int i=0; i < FPEvents.GetCount(); ++i)
			{
				cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent*>(FPEvents[i]);
				if(pEvent)
				{
					pEvent->SetObjectId(GetCameraObjectId(pCutfile));
					pCutfile->AddEvent(pEvent);
				}
			}
		}
	}

	// Inject the replay events

	if(PARAM_cutReplay.Get())
	{
		const char *pCutReplayFilename;
		PARAM_cutReplay.Get( pCutReplayFilename );

		parTree* pTree = PARSER.LoadTree( pCutReplayFilename, "" );

		if(pTree)
		{
			parTreeNode* pRootNode = pTree->GetRoot();
			parTreeNode* pSourceNode = pRootNode->FindChildWithName("SourceTimeCode");
			if(pSourceNode)
			{
				for(parTreeNode::ChildNodeIterator cIter = pSourceNode->BeginChildren();
					cIter != pSourceNode->EndChildren(); 
					++cIter) 
				{
					parTreeNode* pChildNode = (*cIter);

					if(stricmp(pChildNode->GetElement().GetName(), "Item") == 0)
					{
						parTreeNode* pStartNode = pChildNode->FindChildWithName("FrameStart");
						parTreeNode* pEndNode = pChildNode->FindChildWithName("FrameEnd");

						if(pStartNode && pEndNode)
						{
							parAttribute* pStartAttr = pStartNode->GetElement().FindAttribute("value");
							parAttribute* pEndAttr = pEndNode->GetElement().FindAttribute("value");

							if(pStartAttr && pEndAttr)
							{
								pCutfile->AddEvent(rage_new cutfEvent(pStartAttr->GetIntValue()/CUTSCENE_FPS, CUTSCENE_START_REPLAY_RECORD, false, 0));
								pCutfile->AddEvent(rage_new cutfEvent(pEndAttr->GetIntValue()/CUTSCENE_FPS, CUTSCENE_STOP_REPLAY_RECORD, false, 0));
								
								ULOGGER.SetProgressMessage("Injected CUTSCENE_START_REPLAY_RECORD/CUTSCENE_STOP_REPLAY_RECORD @ '%f/%f'", pStartAttr->GetIntValue()/CUTSCENE_FPS, pEndAttr->GetIntValue()/CUTSCENE_FPS);
							}
						}
					}
				}
			}
		}
	}

	pCutfile->SortEvents();
	if(!pCutfile->SaveFile(pOutputCutFilename))
	{
		Errorf("Failed to save file '%s'", pOutputCutFilename);
		return -1;
	}

    return 0;
}
