using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Command;
using RSG.Base.Logging;

namespace cutfSectionOptimizer
{
    public class OptimizerList
    {
        public OptimizerList()
        {
            m_targetSizeInKB = 0.0f;
            m_intermediateDirName = "__intermediate";
            m_msBuildProject = null;
            m_msBuildTarget = null;
            m_sectionArgument = -1;
            m_noSectionArgument = -1;
            m_workingDirectory = null;
            m_360OutputExtension = null;
            m_PS3OutputExtension = null;
            m_marginError = 0.0f;
            m_startDuration = 0.0f;
            m_maximumAttempts = 0;
            m_outputLogDirectory = null;
            m_tempDirectory = null;
            m_targetMinFiles = 3;
            m_cutscenes = new List<CutsceneEntry>();
            m_templateCutscene = null;
        }

        public float m_targetSizeInKB;
        public string m_intermediateDirName;
        public string m_msBuildProject;
        public string m_msBuildTarget;
        public int m_sectionArgument;
        public int m_noSectionArgument;
        public string m_workingDirectory;
        public string m_360OutputExtension;
        public string m_PS3OutputExtension;
        public float m_marginError;
        public float m_startDuration;
        public int m_maximumAttempts;
        public int m_targetMinFiles;
        public string m_outputLogDirectory;
        public string m_tempDirectory;
        public List<CutsceneEntry> m_cutscenes;
        public CutsceneEntry m_templateCutscene;

        public static OptimizerList Deserialize(string filePath)
        {
            try
            {
                StreamReader reader = new StreamReader(filePath);
                XmlSerializer serializer = new XmlSerializer(typeof(OptimizerList));
                OptimizerList optimizer  = (OptimizerList) serializer.Deserialize(reader);
                reader.Close();

                return optimizer;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        public bool Serialize(string filePath)
        {
            try
            {
                StreamWriter writer = new StreamWriter(filePath);
                XmlSerializer serializer = new XmlSerializer(typeof(OptimizerList));
                serializer.Serialize(writer, this);
                writer.Close();

            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
         
            return true;
        }
    }

    [Serializable()]
    public class CutsceneEntry
    {
        public CutsceneEntry() { }

        public CutsceneEntry(string name, string rawAssetFilePath, string inputDirectory, string inputFaceDirectory, string outputDirectory)
        {
            m_name = name;
            m_rawAssetFilePath = rawAssetFilePath;
            m_inputDirectory = inputDirectory;
            m_inputFaceDirectory = inputFaceDirectory;
            m_outputDirectory = outputDirectory;
            m_outputCutFile = m_name + ".cutbin";

            m_optimizedTest = null;
        }

        public string m_name;
        public string m_rawAssetFilePath;
        public string m_inputDirectory;
        public string m_inputFaceDirectory;
        public string m_outputDirectory;
        public string m_outputCutFile;

        public OptimizerTest m_optimizedTest;
    }

    public class MSBuildSettings
    {
        public MSBuildSettings() 
        {
            m_SectionDuration = 0.0f;
            m_SectionMethod = 0;
        }

        public float m_SectionDuration;
        public int m_SectionMethod;

        //Hardcoded arguments.
        private const string m_SectionDurationArgument = "sectionduration=";
        private const string m_SectionMethodArgument = "sectionmethod=";

        public static MSBuildSettings Parse(string inputFile, rageHtmlLog htmlLog)
        {
            try
            {
                //Parse this file to get the section duration.
                StreamReader reader = new StreamReader(inputFile);
                string arguments = reader.ReadLine();
                MSBuildSettings settings = new MSBuildSettings();

                string[] argumentList = arguments.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string argument in argumentList)
                {
                    if (argument.StartsWith(m_SectionDurationArgument) == true)
                    {
                        int index = argument.LastIndexOf("=");
                        string durationStr = argument.Substring(index + 1);
                        if (Single.TryParse(durationStr, out settings.m_SectionDuration) == false)
                            htmlLog.LogEvent("Unable to parse \"sectionduration\" arguments from settings file.");
                    }
                    else if (argument.StartsWith(m_SectionMethodArgument) == true)
                    {
                        int index = argument.LastIndexOf("=");
                        string sectionMethodStr = argument.Substring(index + 1);
                        if (Int32.TryParse(sectionMethodStr, out settings.m_SectionMethod) == false)
                            htmlLog.LogEvent("Unable to parse \"sectionmethod\" arguments from settings file.");
                    }
                }

                reader.Close();
                return settings;
            }
            catch (Exception e)
            {
                htmlLog.LogEvent(e.Message);
                return null;
            }
        }

        public bool Write(string outputFile, rageHtmlLog htmlLog)
        {
            try
            {
                string leadingDirectory = Path.GetDirectoryName(outputFile);
                if(Directory.Exists(leadingDirectory) == false)
                    Directory.CreateDirectory(leadingDirectory);

                if(File.Exists(outputFile) == true)
                    File.SetAttributes(outputFile, FileAttributes.Normal);

                StreamWriter writer = new StreamWriter(outputFile);

                writer.WriteLine(";" + m_SectionDurationArgument + m_SectionDuration + ";" + m_SectionMethodArgument + m_SectionMethod);

                writer.Close();
            }
            catch(Exception e) 
            {
                htmlLog.LogEvent(e.Message);
                return false;
            }

            return true;
        }
    }

    public class OptimizerTest
    {
        public OptimizerTest() 
        {
            m_cutsceneName = "";
            m_duration = 0.0f;
            m_averageSize360 = 0;
            m_averageSizePS3 = 0;
            m_maxSize360 = -1;
            m_maxSizePS3 = -1;
            m_numFiles360 = -1;
            m_numFilesPS3 = -1;
        }

        public string m_cutsceneName;
        public float m_duration;
        public int m_sectionMethod;
        public long m_averageSize360;
        public long m_averageSizePS3;
        public long m_maxSize360;
        public long m_maxSizePS3;
        public int m_numFiles360;
        public int m_numFilesPS3;
        public string m_suffix;

        private string m_MSBuildExecutable = @"c:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\MSBuild.exe";

        public bool StartTest(OptimizerList optimizerList, CutsceneEntry cutscene, int directorySuffix, rageHtmlLog htmlLog)
        {
            rageStatus status;
            rageExecuteCommand command = new rageExecuteCommand(m_MSBuildExecutable, out status);

            //Folders are now in a temporary folder.
            //
            m_suffix = "_" + directorySuffix; //separate the folders.
            CleanTest(optimizerList, cutscene, htmlLog); //Clean out the previous folders if they exist.

            htmlLog.LogEvent("Starting test " + directorySuffix + " for cutscene " + cutscene.m_name + "...");

            command.WorkingDirectory = optimizerList.m_workingDirectory;
            command.Arguments = optimizerList.m_msBuildProject;
            command.Arguments += " /t:" + optimizerList.m_msBuildTarget;
            command.Arguments += " /p:";
            command.Arguments += "inputDir=" + cutscene.m_inputDirectory;

            string intermediateDir = Path.Combine(optimizerList.m_tempDirectory, cutscene.m_name);
            Path.Combine(intermediateDir, optimizerList.m_intermediateDirName);
            intermediateDir += m_suffix;

            string outputDir = Path.Combine(optimizerList.m_tempDirectory, cutscene.m_name);
            outputDir += m_suffix;

            command.Arguments += ";intermediateDir=" + intermediateDir;
            command.Arguments += ";inputFaceDir=" + cutscene.m_inputFaceDirectory;
            command.Arguments += ";outputDir=" + outputDir;
            command.Arguments += ";outputCutfile=" + Path.Combine(outputDir, cutscene.m_outputCutFile);

            command.Arguments += ";sectionMethod=" + m_sectionMethod;

            command.Arguments += ";sectionDuration=" + m_duration.ToString();

            command.TimeOutInSeconds = 60 * 60; //1 hour
            command.Execute();

            if(Directory.Exists(outputDir) == false)
            {
                //Then the command is up to date.
                //
                outputDir = cutscene.m_outputDirectory;

                if (Directory.Exists(outputDir) == false)
                {
                    return false;
                }
            }

            //Verify the target files are in the right range.
            //
            string[] x360Files = Directory.GetFiles(outputDir, "*" + optimizerList.m_360OutputExtension);
            foreach (string x360File in x360Files)
            {
                FileInfo fileInfo = new FileInfo(x360File);
                m_averageSize360 += fileInfo.Length;

                if (m_maxSize360 < fileInfo.Length)
                    m_maxSize360 = fileInfo.Length;
            }

            m_numFiles360 = x360Files.Length;
            if (x360Files.Length != 0)
                m_averageSize360 /= x360Files.Length;
            else
                m_averageSize360 = 0;

            string[] ps3Files = Directory.GetFiles(outputDir, "*" + optimizerList.m_PS3OutputExtension);
            foreach (string ps3File in ps3Files)
            {
                FileInfo fileInfo = new FileInfo(ps3File);
                m_averageSizePS3 += fileInfo.Length;

                if (m_maxSizePS3 < fileInfo.Length)
                    m_maxSizePS3 = fileInfo.Length;
            }

            m_numFilesPS3 = ps3Files.Length;
            if (ps3Files.Length != 0)
                m_averageSizePS3 /= ps3Files.Length;
            else
                m_averageSizePS3 = 0;

            return true;
        }

        public void PromoteTest(OptimizerList optimizerList, CutsceneEntry cutscene, rageHtmlLog htmlLog)
        {
            try
            {
                string newIntermediateDir = Path.Combine(cutscene.m_inputDirectory, optimizerList.m_intermediateDirName);
                string currentIntermediateDir = Path.Combine(optimizerList.m_tempDirectory, cutscene.m_name);
                currentIntermediateDir += m_suffix;
                currentIntermediateDir = Path.Combine(currentIntermediateDir, optimizerList.m_intermediateDirName);

                htmlLog.LogEvent("Renaming " + currentIntermediateDir + " to " + newIntermediateDir + "...");

                if (Directory.Exists(newIntermediateDir) == true)
                    Directory.Delete(newIntermediateDir, true);

                if (Directory.Exists(currentIntermediateDir) == true)
                    Directory.Move(currentIntermediateDir, newIntermediateDir);

                string newOutputDirectory = cutscene.m_outputDirectory;
                string currentOutputDirectory = Path.Combine(optimizerList.m_tempDirectory, cutscene.m_name);
                currentOutputDirectory += m_suffix;

                htmlLog.LogEvent("Renaming " + currentOutputDirectory + " to " + newOutputDirectory + "...");

                if (Directory.Exists(newOutputDirectory) == true)
                    Directory.Delete(newOutputDirectory, true);

                if (Directory.Exists(currentOutputDirectory) == true)
                    Directory.Move(currentOutputDirectory, newOutputDirectory);
            }
            catch (System.Exception ex)
            {
                htmlLog.LogEvent("Exception reached when promoting test folders: " + ex.Message);
            }
        }

        public void CleanTest(OptimizerList optimizerList, CutsceneEntry cutscene, rageHtmlLog htmlLog)
        {
            try
            {
                string intermediateDir = Path.Combine(optimizerList.m_tempDirectory, cutscene.m_name);
                Path.Combine(intermediateDir, optimizerList.m_intermediateDirName);
                intermediateDir += m_suffix;

                htmlLog.LogEvent("Deleting " + intermediateDir + "...");
                if(Directory.Exists(intermediateDir) == true)
                    Directory.Delete(intermediateDir, true);

                string outputDirectory = Path.Combine(optimizerList.m_tempDirectory, cutscene.m_name);
                outputDirectory += m_suffix;

                htmlLog.LogEvent("Deleting " + outputDirectory + "...");
                if(Directory.Exists(outputDirectory) == true)
                    Directory.Delete(outputDirectory, true);
            }
            catch (System.Exception ex)
            {
                htmlLog.LogEvent("Exception reached when cleaning test: " + ex.Message);
            }
        }
    }
}
