using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base;
using RSG.Base.Logging;

namespace cutfSectionOptimizer
{
    class Program
    {
        static int Main(string[] args)
        {
            rageCommandLineParser parser = new rageCommandLineParser();
            rageCommandLineItem inputFileArgument = new rageCommandLineItem("inputFile", 1, true, "File Path", "Input file containing a list of cutscenes to be optimized.");
            rageCommandLineItem inputCutsceneArgument = new rageCommandLineItem("inputCutscene", 1, false, "Cutscene Name", "Input cutscene name to be optimized.");
            rageCommandLineItem msbuildOutput = new rageCommandLineItem("msbuildoutput", 1, false, "Write MS Build Arguments", "Write the variables for sectioning this cutscene to the resource settings file in the form of MS Build arguments to be appended to a project call.");
            rageCommandLineItem writeOptimizedTests = new rageCommandLineItem("writeOptimizedTests", 0, false, "Write Optimized Tests for Cutscenes", "Write the tests found to be optimize cutscenes back to the input file to be used in subsequent runs of this tool.");
            parser.AddItem(inputFileArgument);
            parser.AddItem(inputCutsceneArgument);
            parser.AddItem(msbuildOutput);
            parser.AddItem(writeOptimizedTests);
            if (parser.Parse(args) == false)
            {
                Console.WriteLine("Unable to parse command line arguments.");
                Console.WriteLine(parser.Usage);
                return -1;
            }

            string inputFilePath = inputFileArgument.Value as string;
            string msBuildSettingsFile = msbuildOutput.Value as string;
            
            //Disabled writing out of input file to validate serialization process.
            //WriteTestOptimizerList(inputFilePath); 

            OptimizerList optimizerList = new OptimizerList();
            optimizerList = OptimizerList.Deserialize(inputFilePath);

            //Process the working directory set by the XML file.
            string workingDirectory = optimizerList.m_workingDirectory;
            string[] splitArray = workingDirectory.Split("%".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string splitString in splitArray)
            {
                string envVar = Environment.GetEnvironmentVariable(splitString);
                if (envVar != null)
                {
                    optimizerList.m_workingDirectory = workingDirectory.Replace("%" + splitString + "%", envVar);
                }
            }

            //NOTE: We may large assumptions about the file structure and naming.  We depend on the folder's name
            //to be the name of the cutscene we are trying to export.
            //For example Cutscenes\GRAVE01 is the folder, we will expect there to be a file GRAVE01.fbx in there.
            //This is to weave around the fact that there are auxiliary FBX files used for testing/unfinished work.
            //
            List<CutsceneEntry> expandedCutscenes = new List<CutsceneEntry>();
            string wildcard = "*";
            if (inputCutsceneArgument.WasSet)
            {
                string rawAssetFilePath = inputCutsceneArgument.Value as string;
                string cutsceneName = Path.GetFileNameWithoutExtension(rawAssetFilePath);

                CutsceneEntry newEntry = null;
                foreach (CutsceneEntry entry in optimizerList.m_cutscenes)
                {
                    if (String.Compare(entry.m_name, cutsceneName, true) == 0)
                    {
                        newEntry = entry;
                        break;
                    }
                }

                if (newEntry == null)
                {
                    CutsceneEntry templateEntry = optimizerList.m_templateCutscene;
                    string inputDirectory = templateEntry.m_inputDirectory.Replace(wildcard, cutsceneName);
                    string inputFaceDirectory = templateEntry.m_inputFaceDirectory.Replace(wildcard, cutsceneName);
                    string outputDirectory = templateEntry.m_outputDirectory.Replace(wildcard, cutsceneName);
                    newEntry = new CutsceneEntry(cutsceneName, rawAssetFilePath, inputDirectory, inputFaceDirectory, outputDirectory);
                }

                expandedCutscenes.Add(newEntry);
            }
            else
            {
                //Expand any instances where there are wildcards in the file paths.
                foreach (CutsceneEntry entry in optimizerList.m_cutscenes)
                {
                    if (entry.m_name.CompareTo(wildcard) == 0)
                    {
                        string cleanedRawDir = Path.GetDirectoryName(entry.m_rawAssetFilePath);
                        cleanedRawDir = cleanedRawDir.Replace(wildcard, "");
                        string[] rawDirectories = Directory.GetDirectories(cleanedRawDir);
                        foreach (string rawDirectory in rawDirectories)
                        {
                            string folderName = rawDirectory.Substring(cleanedRawDir.Length);
                            string cleanedRawAssetFilePath = entry.m_rawAssetFilePath.Replace(wildcard, folderName);
                            string rawAssetDirectory = Path.GetDirectoryName(cleanedRawAssetFilePath);
                            string rawAssetExtension = Path.GetExtension(cleanedRawAssetFilePath);
                            List<string> rawAssetFiles = FindRawAssetFiles(rawAssetDirectory, rawAssetExtension);

                            //For every raw asset file that we find in this directory, export it.
                            //
                            foreach (string rawAssetFilePath in rawAssetFiles)
                            {
                                string cutsceneName = Path.GetFileNameWithoutExtension(rawAssetFilePath);
                                string inputDirectory = entry.m_inputDirectory.Replace(wildcard, cutsceneName);
                                string inputFaceDirectory = entry.m_inputFaceDirectory.Replace(wildcard, cutsceneName);
                                string outputDirectory = entry.m_outputDirectory.Replace(wildcard, cutsceneName);
                                CutsceneEntry newEntry = new CutsceneEntry(cutsceneName, rawAssetFilePath, inputDirectory, inputFaceDirectory, outputDirectory);

                                //Do not add duplicate cutscenes.
                                //
                                bool bAdd = true;
                                foreach (CutsceneEntry cutscene in expandedCutscenes)
                                {
                                    if (String.Compare(cutscene.m_name, newEntry.m_name, true) == 0)
                                    {
                                        bAdd = false;
                                        break;
                                    }
                                }

                                if (bAdd == true)
                                    expandedCutscenes.Add(newEntry);
                            }
                        }
                    }
                    else
                    {
                        expandedCutscenes.Add(entry);
                    }
                }
            }

            rageHtmlLog htmlLog = new rageHtmlLog(optimizerList.m_outputLogDirectory, "Cutscene Section Optimizer Log");
            htmlLog.LogToStandardOutput = true;
            htmlLog.LogEvent("Saving HTML log to: " + optimizerList.m_outputLogDirectory);

            float lowTargetInKB = optimizerList.m_targetSizeInKB - (optimizerList.m_targetSizeInKB * optimizerList.m_marginError);
            float highTargetInKB = optimizerList.m_targetSizeInKB;  //Don't go over the target.

            foreach (CutsceneEntry cutscene in expandedCutscenes)
            {
                htmlLog.LogEvent("Started optimization tests for cutscene " + cutscene.m_name);

                if(Directory.Exists(cutscene.m_inputFaceDirectory) == false)
                {
                    htmlLog.LogEvent("Warning: Input face directory " + cutscene.m_inputFaceDirectory + " does not exist for cutscene " + cutscene.m_name + ".  Facial animations will not be included.");
                }

                if(Directory.Exists(cutscene.m_inputDirectory) == false)
                {
                    htmlLog.LogEvent("Error: Cutscene " + cutscene.m_name + "'s input directory does not exist.  Skipping...");
                    continue;
                }

                if(String.IsNullOrEmpty(cutscene.m_outputDirectory) == true)
                {
                    htmlLog.LogEvent("Error: Cutscene " + cutscene.m_name + "'s output directory is null or empty.  Skipping...");
                    continue;
                }

                if (String.IsNullOrEmpty(cutscene.m_outputCutFile) == true)
                {
                    htmlLog.LogEvent("Error: Cutscene " + cutscene.m_name + "'s output cutfile is null or empty.  Skipping...");
                    continue;
                }

                bool optimized = false;
                bool executeBaselineTest = true;
                List<OptimizerTest> tests = new List<OptimizerTest>();
                OptimizerTest currentTest = null;

                if (cutscene.m_optimizedTest != null)
                {
                    htmlLog.LogEvent("Optimization test from previous runs found!  Verifying this is still an optimal solution....");
                    currentTest = cutscene.m_optimizedTest;
                    tests.Add(currentTest);

                    currentTest.StartTest(optimizerList, cutscene, tests.Count, htmlLog);
                    executeBaselineTest = false;

                    if ( (currentTest.m_maxSize360 > lowTargetInKB && currentTest.m_maxSize360 < highTargetInKB)
                            || (currentTest.m_maxSize360 < highTargetInKB && currentTest.m_numFiles360 == 1))
                    {
                        htmlLog.LogEvent("Cutscene " + cutscene.m_name + " is optimized with duration: " + currentTest.m_duration + "!");
                        optimized = true;
                    }
                }
                else if (msbuildOutput.WasSet == true && File.Exists(msbuildOutput.Value as string) == true)
                {
                    //Use the cutscene-specific settings file.
                    //
                    MSBuildSettings settings = MSBuildSettings.Parse(msBuildSettingsFile, htmlLog);

                    if(settings != null)
                    {
                        OptimizerTest settingsTest = new OptimizerTest();
                        settingsTest.m_cutsceneName = cutscene.m_name;
                        settingsTest.m_duration = settings.m_SectionDuration;
                        settingsTest.m_sectionMethod = settings.m_SectionMethod;
                        tests.Add(settingsTest);

                        settingsTest.StartTest(optimizerList, cutscene, tests.Count, htmlLog);
                        executeBaselineTest = false;

                        //If the baseline test shows that the average size is less than the target, do no sectioning.
                        //
                        if ((settingsTest.m_maxSize360 > lowTargetInKB && settingsTest.m_maxSize360 < highTargetInKB)
                            || (settingsTest.m_maxSize360 < highTargetInKB && settingsTest.m_numFiles360 == 1))
                        {
                            htmlLog.LogEvent("Cutscene " + cutscene.m_name + " is optimized with existing resource settings!");
                            optimized = true;
                            currentTest = settingsTest;
                        }
                    }
                }
                

                if(executeBaselineTest == true)
                {
                    //NOTE:  Because we invoke Momma directly, it will not only export the FBX and the visibility data, but 
                    //export using the cutfile MS Build system with the visibility data.
                    //The below test is likely not necessary but to remain barricaded from whatever Momma decides to do, this
                    //has been left in.
                    //
                    htmlLog.LogEvent("Building cutscene with no sections to get baseline...");
                    OptimizerTest baselineTest = new OptimizerTest();
                    baselineTest.m_cutsceneName = cutscene.m_name;
                    baselineTest.m_duration = 0.0f;
                    baselineTest.m_sectionMethod = optimizerList.m_noSectionArgument;
                    tests.Add(baselineTest);

                    baselineTest.StartTest(optimizerList, cutscene, tests.Count, htmlLog);

                    //If the baseline test shows that the average size is less than the target, do no sectioning.
                    //
                    if (baselineTest.m_maxSize360 < optimizerList.m_targetSizeInKB)
                    {
                        htmlLog.LogEvent("Cutscene " + cutscene.m_name + " is optimized without sectioning!");
                        optimized = true;
                        currentTest = baselineTest;
                    }
                }

                float duration = optimizerList.m_startDuration;
                while (optimized == false)
                {
                    currentTest = new OptimizerTest();
                    currentTest.m_cutsceneName = cutscene.m_name;
                    currentTest.m_duration = duration;
                    currentTest.m_sectionMethod = optimizerList.m_sectionArgument;
                    tests.Add(currentTest);

                    htmlLog.LogEvent("Test " + tests.Count + ": Duration " + duration);

                    if (currentTest.StartTest(optimizerList, cutscene, tests.Count, htmlLog) == false)
                    {
                        htmlLog.LogEvent("Failure encountered when optimizing cutscene " + cutscene.m_name + "!");
                        break;
                    }

                    if (currentTest.m_maxSize360 > lowTargetInKB && currentTest.m_maxSize360 < highTargetInKB)
                    {
                        if (currentTest.m_numFiles360 < optimizerList.m_targetMinFiles)
                        {
                            //We should just take the 
                        }
                        optimized = true;
                    }
                    else if (currentTest.m_maxSize360 > optimizerList.m_targetSizeInKB || currentTest.m_maxSize360 < optimizerList.m_targetSizeInKB)
                    {
                        //Compute a new duration for the next test.
                        //
                        //See by what factor the current size is to the target size.
                        //
                        float initialDuration = duration;
                        float factorDifference = optimizerList.m_targetSizeInKB / currentTest.m_maxSize360;
                        duration *= factorDifference;

                        //Adjust the duration to be on a frame time.
                        float singleFrameTime = 1.0f / 30.0f;
                        float numFramesInDuration = duration / singleFrameTime;
                        duration = (float) Math.Truncate( numFramesInDuration) * singleFrameTime;

                        if (initialDuration == duration)
                        {
                            //The next test won't change anything; so let's say this is close enough to being optimized.
                            optimized = true;
                        }
                    }

                    if(tests.Count == optimizerList.m_maximumAttempts)
                    {
                        //Pick the closest size.
                        int bestTestIndex = 0;
                        for(int testIndex = 1; testIndex < tests.Count; ++testIndex)
                        {
                            float bestDiff = Math.Abs(tests[bestTestIndex].m_maxSize360 - optimizerList.m_targetSizeInKB);
                            float testDiff = Math.Abs(tests[testIndex].m_maxSize360 - optimizerList.m_targetSizeInKB);

                            if(testDiff < bestDiff)
                            {
                                bestTestIndex = testIndex;
                            }
                        }

                        currentTest = tests[bestTestIndex];
                        optimized = true;
                    }
                }

                if (optimized == true)
                {
                    //Found the best test to use.  Erase all the old tests.  
                    for (int testIndex = 0; testIndex < tests.Count; ++testIndex)
                    {
                        if (currentTest == tests[testIndex])
                        {
                            cutscene.m_optimizedTest = tests[testIndex];

                            //Rename these folders
                            tests[testIndex].PromoteTest(optimizerList, cutscene, htmlLog);
                        }
                        else
                        {
                            tests[testIndex].CleanTest(optimizerList, cutscene, htmlLog);
                        }
                    }

                    htmlLog.LogEvent("Cutscene " + cutscene.m_name + " successfully optimized!");
                    htmlLog.LogEvent("Duration: " + currentTest.m_duration);
                    htmlLog.LogEvent("Maximum 360 size: " + currentTest.m_maxSize360);
                    htmlLog.LogEvent("Average 360 size: " + currentTest.m_averageSize360);
                    htmlLog.LogEvent("Maximum PS3 size: " + currentTest.m_maxSizePS3);
                    htmlLog.LogEvent("Average PS3 size: " + currentTest.m_averageSizePS3);
                }
            }

            if (writeOptimizedTests.WasSet == true)
            {
                //Upon completion, write out the scenes that have been successfully optimized.
                //
                foreach (CutsceneEntry cutscene in expandedCutscenes)
                {
                    if (cutscene.m_optimizedTest != null)
                    {
                        optimizerList.m_cutscenes.Add(cutscene);
                    }
                }

                StreamWriter writer = new StreamWriter(inputFilePath);
                XmlSerializer serializer = new XmlSerializer(typeof(OptimizerList));
                serializer.Serialize(writer, optimizerList);
            }

            if (msbuildOutput.WasSet == true)
            {
                //Upon completion, write out the scenes that have been successfully optimized.
                //
                foreach (CutsceneEntry cutscene in expandedCutscenes)
                {
                    if (cutscene.m_optimizedTest != null)
                    {
                        //NOTE:  This writes to one file for many cutscenes.
                        MSBuildSettings settings = new MSBuildSettings();
                        settings.m_SectionDuration = cutscene.m_optimizedTest.m_duration;
                        settings.m_SectionMethod = cutscene.m_optimizedTest.m_sectionMethod;

                        settings.Write(msBuildSettingsFile, htmlLog);
                        break;
                    }
                }
            }

            return 0;
        }

        private static List<string> FindRawAssetFiles(string inputDirectory, string extension)
        {
            List<string> validFbxFiles = new List<string>();
            if (Directory.Exists(inputDirectory) == false)
                return validFbxFiles;

            string[] fbxFiles = Directory.GetFiles(inputDirectory, "*" + extension);

            foreach(string fbxFile in fbxFiles)
            {
                if(fbxFile.Contains("swatch") || fbxFile.Contains(".bck") || fbxFile.Contains("_work") || fbxFile.Contains("unused"))
                    continue;

                validFbxFiles.Add(fbxFile);
            }

            return validFbxFiles;
        }


        private static void WriteTestOptimizerList(string outputFilePath)
        {
            OptimizerList xmlFile = new OptimizerList();
            xmlFile.m_targetSizeInKB = 1024 * 1024;
            xmlFile.m_msBuildProject = "cutscene_anims.proj";
            xmlFile.m_msBuildTarget = "BuildAll";
            xmlFile.m_sectionArgument = 1;
            xmlFile.m_noSectionArgument = 0;
            xmlFile.m_workingDirectory = @"%RAGE_ROOT_FOLDER%\tools\base\exes\CutScene";
            xmlFile.m_360OutputExtension = ".xcdt";
            xmlFile.m_PS3OutputExtension = ".ccdt";
            xmlFile.m_marginError = 0.10f;
            xmlFile.m_startDuration = 30.0f;
            xmlFile.m_maximumAttempts = 10;
            xmlFile.m_targetMinFiles = 3;
            xmlFile.m_outputLogDirectory = "C:\\SectionOptimizerLog.html";
            xmlFile.m_tempDirectory = "T:\\temp\\cutsceneoptimizer"; //Move will not work across volumes.
            xmlFile.m_templateCutscene = new CutsceneEntry("*", @"T:\rdr2\Art\Animation\CutScene\Story\*\*.fbx", @"t:\rdr2\assets\cutsceneFBX\*", @"T:\rdr2\assets\animation\cutscene_face\*", @"t:\rdr2\assets\cutscene\*");
            xmlFile.m_cutscenes.Add(new CutsceneEntry("*", @"T:\rdr2\Art\Animation\CutScene\Story\*\*.fbx", @"t:\rdr2\assets\cutsceneFBX\*", @"T:\rdr2\assets\animation\cutscene_face\*", @"t:\rdr2\assets\cutscene\*"));

//             xmlFile.m_cutscenes.Add(new CutsceneEntry("M2", @"t:\rdr2\assets\cutsceneFBX\M2", @"T:\rdr2\assets\animation\cutscene_face\M2", @"t:\rdr2\assets\cutscene\M2"));
//             xmlFile.m_cutscenes.Add(new CutsceneEntry("GRAVE_01", @"t:\rdr2\assets\cutsceneFBX\GRAVE_01", @"T:\rdr2\assets\animation\cutscene_face\GRAVE_01", @"t:\rdr2\assets\cutscene\GRAVE_01"));
//             xmlFile.m_cutscenes.Add(new CutsceneEntry("M1", @"t:\rdr2\assets\cutsceneFBX\M1", @"T:\rdr2\assets\animation\cutscene_face\M1", @"t:\rdr2\assets\cutscene\M1"));

            StreamWriter writer = new StreamWriter(outputFilePath);
            XmlSerializer serializer = new XmlSerializer(typeof(OptimizerList));
            serializer.Serialize(writer, xmlFile);
            writer.Close();

        }
    }
}
