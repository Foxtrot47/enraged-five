// 
// /cutfanimsectionap3.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// PURPOSE: The purpose of this tool is to generate the anim_section.targets file that is used
//  to slice up the scene's animations for streaming.  If the "method" and/or "duration" command 
//  line arguments are specified here, they also need to be specified when running the 
//  cutfsection tool.

#include "system/main.h"

#include "cutfutils.h"
#include "atl/array.h"
#include "atl/bitset.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parsercore/streamxml.h"
#include "parsercore/versioninfo.h"
#include "system/exec.h"
#include "system/param.h"

#include "RsULog/ULogger.h"

using namespace rage;

PARAM( inputDir, "[cutfanimsection] The location where the scene's exported animations live." );
PARAM( inputFaceDir, "[cutfanimsection] The location where the scene's exported face animations live.  Default:  $(inputDir)/faces" );
PARAM( intermediateDir, "[cutfanimsection] The location where intermediate files should be saved, including the MSBuild targets file 'anim_section.targets'.  Default:  $(inputDir)/obj" );
PARAM( cutscenefile, "[cutfanimsection] The cut file to create an MSBuild targets file for.  Default:  $(inputDir)/anims.cutxml" );
PARAM( useOldMinSectionTime, "[cutfsection] When this arg is present, the tool will use the old minimum section time (3 secs) rather than the new one (1 sec) (bugstar:6519435)." );
PARAM( ulog, "[cutfanimsection] Universal logging." );

#define BLOCK_TAG_PAD 0.001f

void Init()
{
	INIT_PARSER;

	if(PARAM_ulog.Get())
	{
		ULOGGER.PreExport(GetTempLogFileName("cutfanimsection").c_str(), "cutfanimsection");
	}
}

void Shutdown()
{
	SHUTDOWN_PARSER;

	if(PARAM_ulog.Get())
	{
		ULOGGER.PostExport();
	}
}

bool AlmostEqual(double nVal1, double nVal2, double nEpsilon)
{
	bool bRet = (((nVal2 - nEpsilon) < nVal1) && (nVal1 < (nVal2 + nEpsilon)));
	return bRet;
}

bool WriteCropClipItems( parStreamOut *s, const char *pIntermediateDir, const char* pClipName, const atArray<float> &sectionTimeList, int minCutsceneSectionTime)
{    
    for ( int i = 1; i < sectionTimeList.GetCount(); ++i )
    {
        char cOutputFile[CUTSCENE_LONG_OBJNAMELEN];
        sprintf( cOutputFile, "%s-%d", pClipName, i - 1 );

        SCropFileItem cropFileItem;
        cropFileItem.bIsClip = true;
        cropFileItem.bCanTouchInputs = pIntermediateDir != NULL;
        cropFileItem.fStartTime = sectionTimeList[i - 1];
        cropFileItem.fEndTime = sectionTimeList[i];
        cropFileItem.inputName = pClipName;
        cropFileItem.inputDir = (pIntermediateDir != NULL) ? "$(intermediateDir)" : "$(inputDir)";
        cropFileItem.outputName = cOutputFile;
        cropFileItem.outputDir = "$(outputDir)";

		if((cropFileItem.fEndTime - cropFileItem.fStartTime) == 0.0f)
		{
			continue;
		}

		// This should never happen anymore but we will keep it anyway to catch any weirdness
		if((cropFileItem.fEndTime - cropFileItem.fStartTime) <= (float)minCutsceneSectionTime)
		{
			ULOGGER.SetProgressMessage("ERROR: Section time(s) %f-%f are invalid, %f seconds is smaller than the minimum limit of %d seconds.",
				cropFileItem.fStartTime, cropFileItem.fEndTime, (cropFileItem.fEndTime - cropFileItem.fStartTime), minCutsceneSectionTime );

			return false;
		}

        if ( !WriteCropItems( s, cropFileItem ) )
        {
            return false;
        }
    }

    return true;
}

void CheckSectionsAgainstBlocks(atArray<float>& sectionTimeList, cutfCutsceneFile2& cutFile)
{
	// simulate blocking tags - blocks are on cut boundaries
	const atArray<cutfEvent *> &eventList = cutFile.GetEventList();
	for(int i=0; i < eventList.GetCount(); ++i)
	{
		cutfEvent* pEvent = eventList[i];
		if(pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
		{
			if(pEvent->GetTime() <= 0) continue;

			float halfFrame = (0.5f * (1.0f / CUTSCENE_FPS));
			// MPW - Move the cut back half a frame. The incoming cut times need to be brought back in future and this can be removed. The cut times are
			// currently pulled back in the build process to avoid disruption.
			float fTime = pEvent->GetTime() - halfFrame;
			float time = ((fTime) - halfFrame);
			float startTime = (time - BLOCK_TAG_PAD);
			float endTime = (fTime) + halfFrame + BLOCK_TAG_PAD ;

			for(int j=0; j < sectionTimeList.GetCount()-1; ++j)
			{
				if(sectionTimeList[j] >= startTime && sectionTimeList[j] <= endTime)
				{
					ULOGGER.SetProgressMessage("Blocking tag conflict found with tag '%f'/'%f' with time '%f', now '%f'", startTime, endTime, sectionTimeList[j], sectionTimeList[j] + (1.0f / CUTSCENE_FPS));
					sectionTimeList[j] += (1.0f / CUTSCENE_FPS);
				}
			}
		}
	}
}

void CheckSectionSizes(atArray<float>& sectionTimeList, int minCutsceneSectionTime)
{
	// Only perform on more than 2 sections as we dont want to have less than 2 entries.
	if(sectionTimeList.GetCount() > 2)
	{
		// Iterate the section times and make sure no section is less than the minimum
		for(int i=1; i < sectionTimeList.GetCount(); ++i)
		{
			if(sectionTimeList[i] - sectionTimeList[i-1] <= (float)minCutsceneSectionTime)
			{
				if(i == sectionTimeList.GetCount()-1)
				{
					ULOGGER.SetProgressMessage("Removed section with time '%f'", sectionTimeList[i-1] );
					sectionTimeList.Delete(i-1);
				}
				else
				{
					ULOGGER.SetProgressMessage("Removed section with time '%f'", sectionTimeList[i] );
					sectionTimeList.Delete(i);
				}
				i--;
			}
		}
	}
}

void MakeSectionsDivisableByFrame(atArray<float>& sectionTimeList)
{
	// make all the sections divisable by a frame so we get 30fps animations, this fucks the sectioning blocks if we dont have 30fps.
	for(int i=0; i < sectionTimeList.GetCount()-1; ++i)
	{
		sectionTimeList[i] = float(int((sectionTimeList[i] * CUTSCENE_FPS) + 0.5f)) / CUTSCENE_FPS;
	}
}

int Main()
{
    Init();

    // get the input directory
    const char* pInputDir = "";
    PARAM_inputDir.Get( pInputDir );

    // get the face dir
    char cInputFaceDir[RAGE_MAX_PATH];

    const char *pInputFaceDir = NULL;
    if ( !PARAM_inputFaceDir.Get( pInputFaceDir ) )
    {
        sprintf( cInputFaceDir, "%s\\faces", pInputDir );
        pInputFaceDir = cInputFaceDir;
    }

    // get the intermediate directory
    char cIntermediateDir[RAGE_MAX_PATH];

    const char* pIntermediateDir = NULL;
    if ( !PARAM_intermediateDir.Get( pIntermediateDir ) )
    {
        sprintf( cIntermediateDir, "%s\\obj", pInputDir );
        pIntermediateDir = cIntermediateDir;
    }

    // get the cut filename
    char cCutFilename[RAGE_MAX_PATH];

    const char* pCutFilename = NULL;
    if ( !PARAM_cutscenefile.Get( pCutFilename ) )
    {
        sprintf( cCutFilename, "%s\\anims.cutxml", pInputDir );
        pCutFilename = cCutFilename;
    }

	int minCutsceneSectionTime = MINIMUM_CUTSCENE_SECTION_TIME;
	if (PARAM_useOldMinSectionTime.Get())
	{
		minCutsceneSectionTime = MINIMUM_OLD_CUTSCENE_SECTION_TIME;
	}

    // load the cut file
    cutfCutsceneFile2 cutFile;
    if ( !LoadCutfile( pCutFilename, cutFile ) )
    {
		ULOGGER.SetProgressMessage("ERROR: Unable to load '%s'.", pCutFilename );

        Shutdown();
        return 1;
    }

    // trick the cut file into providing the section split times
    const_cast<atBitSet &>( cutFile.GetCutsceneFlags() ).Set( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG );

    atArray<float> sectionTimeList;
    cutFile.GetCurrentSectionTimeList( sectionTimeList );

    if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG ) )
    {
		ULOGGER.SetProgressMessage("Sectioning the anims by Time Slices of %f...", cutFile.GetSectionByTimeSliceDuration() );
    }
    else if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG ) )
    {
		ULOGGER.SetProgressMessage("Sectioning the anims by Camera Cuts..." );
    }
    else if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG ) )
    {
		ULOGGER.SetProgressMessage("Sectioning the anims by SectionSplit Property..." );
    }
    else
    {
		ULOGGER.SetProgressMessage("No sectioning specified.  Trimming to time range..." );
    }

	// add on the end time, so that we can crop the final animation
	// FIX_MIKE : End range stays the same but we want to set the duration to include the last frame so we get example. 10-1200 as 1901 frames
    sectionTimeList.PushAndGrow( (cutFile.GetRangeEnd() + 1) / CUTSCENE_FPS );

	MakeSectionsDivisableByFrame(sectionTimeList);

	CheckSectionsAgainstBlocks(sectionTimeList, cutFile);

	CheckSectionSizes(sectionTimeList, minCutsceneSectionTime);

	if(sectionTimeList.GetCount() <= 1)
	{
		ULOGGER.SetProgressMessage("ERROR: Bad sectioning data found, possible bad camera cuts or blocking tags." );
		Shutdown();
		return 1;
	}

	// if we are sectioned and its a camera cut sectioning then we can bosh the data in the cut list, the camera list needs to match
	// the data we used for sectioning otherwise we get weirdness at runtime.
	// everything is a concat now and all concats that have any sectioning some through as camera cuts.
	if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) )
	{
		if ( cutFile.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG ) )
		{
			atArray<float> &mergedCameraCutList = const_cast<atArray<float> &>( cutFile.GetCameraCutList() );
			mergedCameraCutList.clear();

			mergedCameraCutList = sectionTimeList;
			mergedCameraCutList.Delete(0); // delete the first section as this will be 0
			mergedCameraCutList.Delete(mergedCameraCutList.GetCount()-1); // delete the last as this will be rangeend
		}
	}

	char cOutputCutFile[RAGE_MAX_PATH];
	sprintf(cOutputCutFile, "%s\\data_stream.cutxml", pIntermediateDir);

	if ( !cutFile.SaveFile( cOutputCutFile ) )
	{
		Shutdown();
		return -1;
	}

    // start writing out the .targets file
    char cTargetsFilename[RAGE_MAX_PATH];
    sprintf( cTargetsFilename, "%s\\anim_section.targets", pIntermediateDir );

    ASSET.CreateLeadingPath( cTargetsFilename );

	parSettings settings = PARSER.Settings();
	settings.SetFlag(parSettings::WRITE_XML_HEADER, false);

    parStreamOut *s = PARSER.OpenOutputStream( cTargetsFilename, "", parManager::XML, &settings );
    {
        parElement projectElt;
        projectElt.SetName( "Project", false );
        projectElt.AddAttribute( "xmlns", "http://schemas.microsoft.com/developer/msbuild/2003" );

        s->WriteBeginElement( projectElt );
        {
            parElement itemGroupElt;
            itemGroupElt.SetName( "ItemGroup", false );

            s->WriteBeginElement( itemGroupElt );
            {                
                // Models
                atArray<cutfObject *> modelObjectList;
                cutFile.FindObjectsOfType( CUTSCENE_MODEL_OBJECT_TYPE, modelObjectList );
                for ( int i = 0; i < modelObjectList.GetCount(); ++i )
                {
                    atString animName( modelObjectList[i]->GetDisplayName().c_str() );

                    bool bUseIntermediateDir = false;

                    cutfModelObject *pModelObject = dynamic_cast<cutfModelObject *>( modelObjectList[i] );
                    if ( pModelObject->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
                    {
                        const cutfPedModelObject *pPedModelObject = dynamic_cast<const cutfPedModelObject *>( pModelObject );

                        char cFaceAnimFilename[RAGE_MAX_PATH];
                        safecpy( cFaceAnimFilename, pPedModelObject->GetFaceAnimationFilename( pInputFaceDir ).GetCStr(), sizeof(cFaceAnimFilename) );
                        if ( strlen( cFaceAnimFilename ) > 0 )
                        {
                            if ( !ASSET.Exists( cFaceAnimFilename, NULL ) )
                            {                            
                                if ( pPedModelObject->FoundDefaultFaceAnimation() || pPedModelObject->OverrideFaceAnimation() )
                                {
									ULOGGER.SetProgressMessage("ERROR: Ped Model '%s', when it was exported from Motion Builder, had the face animation '%s', but the file is now missing.", 
										pPedModelObject->GetDisplayName().c_str(), cFaceAnimFilename );

                                    s->Close();

                                    const fiDevice *pDevice = fiDevice::GetDevice( cTargetsFilename );
                                    if ( pDevice != NULL )
                                    {
                                        pDevice->Delete( cTargetsFilename );
                                    }

                                    Shutdown();
                                    return 1;
                                }
                            }
                            else
                            {
								ULOGGER.SetProgressMessage("Found %s face animation '%s' for Ped Model '%s'.", 
									pPedModelObject->OverrideFaceAnimation() ? "overridden" : "default", 
									cFaceAnimFilename, pPedModelObject->GetDisplayName().c_str() );

                                animName += "_dual";
                                bUseIntermediateDir = true;
                            }
                        }
                        else
                        {
							ULOGGER.SetProgressMessage("The face animation for Ped Model '%s', if any, is not being used.", pPedModelObject->GetDisplayName().c_str() );
						}
                    }

                    if ( !WriteCropClipItems( s, bUseIntermediateDir ? pIntermediateDir : NULL, animName.c_str(), sectionTimeList, minCutsceneSectionTime ) )
                    {
                        s->Close();
                        delete s;

                        Shutdown();
                        return 1;
                    }
                }

                // Lights
                atArray<cutfObject *> lightObjectList;
                cutFile.FindObjectsOfType( CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE, lightObjectList );
                for ( int i = 0; i < lightObjectList.GetCount(); ++i )
                {
					WriteCropClipItems( s, NULL, lightObjectList[i]->GetDisplayName().c_str(), sectionTimeList, minCutsceneSectionTime );
                }

                // Particle Effects
                atArray<cutfObject *> particleEffectObjectList;
                cutFile.FindObjectsOfType( CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE, particleEffectObjectList );
                for ( int i = 0; i < particleEffectObjectList.GetCount(); ++i )
                {
                    WriteCropClipItems( s, NULL, particleEffectObjectList[i]->GetDisplayName().c_str(), sectionTimeList, minCutsceneSectionTime);
                }

                // Camera(s)
                atArray<cutfObject *> cameraObjectList;
                cutFile.FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );
                for ( int i = 0; i < cameraObjectList.GetCount(); ++i )
                {
                    WriteCropClipItems( s, NULL, cameraObjectList[i]->GetDisplayName().c_str(), sectionTimeList, minCutsceneSectionTime);
                }
            }
            s->WriteEndElement( itemGroupElt );
        }
        s->WriteEndElement( projectElt );
    }
    s->Close();
    delete s;

	ULOGGER.SetProgressMessage("Saved '%s'.", cTargetsFilename );

    Shutdown();
    return 0;
}
