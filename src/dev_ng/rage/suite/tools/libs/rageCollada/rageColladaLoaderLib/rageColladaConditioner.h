#ifndef __RAGE_COLLADA_CONDITIONER__
#define __RAGE_COLLADA_CONDITIONER__

#include "rageColladaExport.h"
#include "rageColladaLoader.h"

#ifdef DEPRECATED
#undef DEPRECATED
#endif

#pragma warning ( push )
#pragma warning ( disable : 4668 )

#include "FCollada.h"
#include "FCDocument/FCDocument.h"

#pragma warning ( pop )

#include <string>
#include <map>
#include <vector>

namespace rage 
{

	class rageIConditioner;

	class rageConditionerCreator
	{
	public:
		virtual std::string GetBaseName() const = 0;

		virtual std::string GetDescription() const = 0;

		virtual rageIConditioner* Create() const = 0;
	};

	class rageIConditioner
	{
	public:
		template < typename T >
		struct TypedOption
		{
			std::string fullName;
			std::string description;
		
			T value;

			bool flag;
		};

		typedef TypedOption < bool >			BoolOption;
		typedef TypedOption < std::string >		StringOption;
		typedef TypedOption < float >			FloatOption;

		typedef std::map < std::string, BoolOption >	BoolOptionMap;
		typedef std::map < std::string, StringOption >	StringOptionMap;
		typedef std::map < std::string, FloatOption >	FloatOptionMap;

		template < class T >
		class Register : public T 
		{
		public:
			Register()
			{
				rageColladaLoader::RegisterConditioner( this );
			}

			virtual ~Register()
			{
				rageColladaLoader::UnregisterConditioner( this );
			}
		private:
			Register( const Register& );
			Register& operator= ( const Register& );
		};
	public:
		rageIConditioner() {}
		virtual ~rageIConditioner() {}

		virtual rageIConditioner* Create() const = 0;

		virtual bool Init() = 0;

		virtual int Execute() = 0;

		virtual std::string GetBaseName() const = 0;
		virtual std::string GetDescription() const = 0;

		virtual bool SetDocument( FCDocument* fcd ) = 0;

		virtual void SetName( const std::string& name ) = 0;

		virtual void SetInputCount( unsigned int count, bool variable = false ) = 0;

		virtual bool SetInput( unsigned int index, const std::string& input ) = 0;

		virtual void SetOutputCount( unsigned int count ) = 0;

		virtual bool SetOutput( unsigned int index, const std::string& output ) = 0;

		virtual bool SetOutputProperties( unsigned int index, bool linked, bool editable = false ) = 0;

		virtual void AddBoolOption( const std::string& option, const std::string& fullName, const std::string& desc, bool def = false ) = 0;

		virtual void AddStringOption( const std::string& option, const std::string& fullName, const std::string& desc, const std::string& def = "" ) = 0;

		virtual void AddFloatOption( const std::string& option, const std::string& fullName, const std::string& desc, float def = 0.0f ) = 0;

		virtual const std::string& GetName() const = 0;

		virtual int GetInputCount() const = 0;

		virtual const std::string& GetInput( unsigned int index ) const = 0;

		virtual bool HasVariableInputCount() const = 0;

		virtual int GetOutputCount() const = 0;

		virtual const std::string& GetOutput( unsigned int index ) const = 0;

		virtual bool IsOutputLinked( unsigned int index ) const = 0;

		virtual bool IsOutputEditable( unsigned int index ) const = 0;

		virtual bool GetBoolOption( const std::string& option, bool& out ) const = 0;
		virtual bool GetStringOption( const std::string& option, std::string& out ) const = 0;
		virtual bool GetFloatOption( const std::string& option, float& out ) const = 0;

		virtual const BoolOptionMap& GetBoolOptionsMap() const = 0;
		virtual const StringOptionMap& GetStringOptionsMap() const = 0;
		virtual const FloatOptionMap& GetFloatOptionsMap() const = 0;

	};

	class rageConditioner : public rageIConditioner
	{
	public:
		DLL_EXPORT rageConditioner();
		DLL_EXPORT rageConditioner( const rageConditioner& rhs );
		DLL_EXPORT virtual ~rageConditioner();

		DLL_EXPORT virtual rageIConditioner* Create() const;

		DLL_EXPORT virtual bool Init() = 0;
		DLL_EXPORT virtual int Execute() = 0;

		DLL_EXPORT virtual std::string GetBaseName() const = 0;
		DLL_EXPORT virtual std::string GetDescription() const = 0;

		DLL_EXPORT bool SetDocument( FCDocument* fcd ); 

		DLL_EXPORT void SetName( const std::string& name );
		DLL_EXPORT void SetInputCount( unsigned int count, bool variable = false );
		DLL_EXPORT bool SetInput( unsigned int index, const std::string& input );
		DLL_EXPORT void SetOutputCount( unsigned int count );
		DLL_EXPORT bool SetOutput( unsigned int count, const std::string& output );
		DLL_EXPORT bool SetOutputProperties( unsigned int index, bool linked, bool editable = false );

		DLL_EXPORT void AddBoolOption( const std::string& option, const std::string& fullName, const std::string& desc, bool def = false );
		DLL_EXPORT void AddStringOption( const std::string& option, const std::string& fullName, const std::string& desc, const std::string& def = "" );
		DLL_EXPORT void AddFloatOption( const std::string& option, const std::string& fullName, const std::string& desc, float def = 0.0f );

		DLL_EXPORT const std::string& GetName() const;
		DLL_EXPORT int GetInputCount() const;
		DLL_EXPORT const std::string& GetInput( unsigned int index ) const;
		DLL_EXPORT bool HasVariableInputCount() const;
		DLL_EXPORT int GetOutputCount() const;
		DLL_EXPORT const std::string& GetOutput( unsigned int index ) const;
		DLL_EXPORT bool IsOutputLinked( unsigned int index ) const;
		DLL_EXPORT bool IsOutputEditable( unsigned int index ) const;

		DLL_EXPORT bool GetBoolOption( const std::string& option, bool& out ) const;
		DLL_EXPORT bool GetStringOption( const std::string& option, std::string& out ) const;
		DLL_EXPORT bool GetFloatOption( const std::string& option, float& out ) const;

		DLL_EXPORT bool SetBoolOption( const std::string& option, bool val );
		DLL_EXPORT bool SetStringOption( const std::string& option, const std::string& val );
		DLL_EXPORT bool SetFloatOption( const std::string& option, float val );

		DLL_EXPORT const BoolOptionMap& GetBoolOptionsMap() const;
		DLL_EXPORT const StringOptionMap& GetStringOptionsMap() const;
		DLL_EXPORT const FloatOptionMap& GetFloatOptionsMap() const;
	public:
		DLL_EXPORT static void PrintDebugMessage( const std::string& message );
		DLL_EXPORT static void PrintExecutionMessage( const std::string& message );
		DLL_EXPORT static void PrintErrorMessage( const std::string& message );
	protected:
		struct OutputInfo
		{
			std::string output;
			bool linked;
			bool editable;
		};

		std::string m_name;
		
		FCDocument* m_document;
	private:
		unsigned int m_inputCount;
		unsigned int m_outputCount;
		bool m_variableInputs;

		std::vector< std::string > m_inputs;
		std::vector< OutputInfo > m_outputs;

		BoolOptionMap m_boolOptions;
		StringOptionMap m_stringOptions;
		FloatOptionMap m_floatOptions;
	};

} // rage

#endif // __RAGE_COLLADA_CONDITIONER__
