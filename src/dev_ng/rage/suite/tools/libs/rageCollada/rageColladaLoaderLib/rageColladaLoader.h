#ifndef __RAGE_COLLADA_LOADER__
#define __RAGE_COLLADA_LOADER__

#include "rageColladaExport.h"

#include <string>
#include <map>

class FCDocument;

namespace rage
{

	class rageConditionerCreator;
	class rageIConditioner;

	class rageColladaLoader 
	{
	public:
		DLL_EXPORT static void Startup();
		DLL_EXPORT static void Shutdown();

		DLL_EXPORT static void RegisterConditioner( const rageConditionerCreator* cc );
		DLL_EXPORT static void UnregisterConditioner( const rageConditionerCreator* cc );
		DLL_EXPORT static unsigned int GetAvailableConditionerCount();
		DLL_EXPORT static const rageConditionerCreator* GetAvailableConditioner( unsigned int index );
		DLL_EXPORT static const rageConditionerCreator* GetAvailableConditioner( const std::string& key );

		DLL_EXPORT static bool AddConditioner( FCDocument* document, const std::string& name, rageIConditioner* conditioner );
		DLL_EXPORT static bool RemoveConditioner( const std::string& name );
		DLL_EXPORT static rageIConditioner* GetConditioner( const std::string& name );

		DLL_EXPORT static void PrintDebugMessage( const std::string& message );
		DLL_EXPORT static void PrintExecutionMessage( const std::string& message );
		DLL_EXPORT static void PrintErrorMessage( const std::string& message );

		DLL_EXPORT static bool LoadDocument( FCDocument* document, const std::string& doc );
		DLL_EXPORT static bool SaveDocument( FCDocument* document, const std::string& to );
		DLL_EXPORT static bool CloneDocument( const std::string& doc, const std::string& to );

		DLL_EXPORT static void Clear();
	private:
		typedef std::map< std::string, const rageConditionerCreator* > ConditionerCreatorList;
		static ConditionerCreatorList m_availableConditioners;
		typedef std::map< std::string, rageIConditioner* > ConditionerList; 
		static ConditionerList m_conditioners;
	};

} // rage

#endif // __RAGE_COLLADA_LOADER__
