#ifndef __RAGE_COLLADA_EXPORT__
#define __RAGE_COLLADA_EXPORT__
#pragma once

#ifdef STATICALLY_LINK
	#define DLL_EXPORT
#else
	#ifdef _EXPORT
		#define DLL_EXPORT __declspec(dllexport)
	#else
		#define DLL_EXPORT __declspec(dllimport)
	#endif
#endif

#endif // __RAGE_COLLADA_EXPORT__
