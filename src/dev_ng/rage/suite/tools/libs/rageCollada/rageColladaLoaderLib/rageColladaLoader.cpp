#include "rageColladaLoader.h"
#include "rageColladaConditioner.h"

#include <cassert>
#include <cstdio>

namespace rage
{

rageColladaLoader::ConditionerCreatorList	rageColladaLoader::m_availableConditioners;
rageColladaLoader::ConditionerList			rageColladaLoader::m_conditioners;

//static FCDocument* s_document = NULL; 

void rageColladaLoader::Startup()
{
	FCollada::Initialize();

//	if ( NULL == s_document )
//	{
//		s_document = FCollada::NewTopDocument();
//	}
}

void rageColladaLoader::Shutdown()
{
	//s_document->Release();

	//FCollada::Release();

	// TODO: I think I'm leaking memory here, but it saves a crash for now
}

void rageColladaLoader::RegisterConditioner( const rageConditionerCreator* cc )
{
	m_availableConditioners.insert( std::make_pair( cc->GetBaseName(), cc ) );
}

void rageColladaLoader::UnregisterConditioner( const rageConditionerCreator* cc  )
{
	m_availableConditioners.erase( cc->GetBaseName() );
}

unsigned int rageColladaLoader::GetAvailableConditionerCount()
{
	return unsigned int( m_availableConditioners.size() ); 
}

const rageConditionerCreator* rageColladaLoader::GetAvailableConditioner( unsigned int index )
{
	if ( index >= GetAvailableConditionerCount() )
	{
		return NULL;
	}

	ConditionerCreatorList::iterator iter;
	iter = m_availableConditioners.begin();
	for ( unsigned int i = 0; i < index; ++i )
	{
		++iter;
	}

	return iter->second;
}

const rageConditionerCreator* rageColladaLoader::GetAvailableConditioner( const std::string& key )
{
	ConditionerCreatorList::iterator iter;
	iter = m_availableConditioners.find( key );
	if ( iter == m_availableConditioners.end() )
	{
		return NULL;
	}

	return iter->second;
}

bool rageColladaLoader::AddConditioner( FCDocument* document, const std::string& name, rageIConditioner* conditioner )
{
	bool ret = m_conditioners.insert( std::make_pair( name, conditioner ) ).second;
	if ( ret )
	{
		conditioner->SetDocument( document );
	}

	return ret;
}

bool rageColladaLoader::RemoveConditioner( const std::string& name )
{
	return m_conditioners.erase( name ) > 0;
}

rageIConditioner* rageColladaLoader::GetConditioner( const std::string& name )
{
	ConditionerList::iterator iter;
	iter = m_conditioners.find( name );
	if ( iter == m_conditioners.end() )
	{
		return NULL;
	}

	return iter->second;
}

void rageColladaLoader::PrintDebugMessage( const std::string& message )
{
	fprintf( stderr, message.c_str() );
}

void rageColladaLoader::PrintExecutionMessage( const std::string& message )
{
	fprintf( stdout, message.c_str() );
}

void rageColladaLoader::PrintErrorMessage( const std::string& message )
{
	fprintf( stderr, message.c_str() );
}

bool rageColladaLoader::LoadDocument( FCDocument* document, const std::string& doc )
{
	return FCollada::LoadDocument( document, doc.c_str() );
	//return FCollada::LoadDocument( s_document, doc.c_str() );

	//return s_document->LoadFromFile( TO_FSTRING( doc.c_str() ) );
}

bool rageColladaLoader::SaveDocument( FCDocument* document, const std::string& to )
{
	return FCollada::SaveDocument( document, to.c_str() );
	//return FCollada::SaveDocument( s_document, to.c_str() );

	//return s_document->WriteToFile( TO_FSTRING( to.c_str() ) );
}

bool rageColladaLoader::CloneDocument( const std::string& doc, const std::string& to )
{
	assert( false );
	return false;
}

void rageColladaLoader::Clear()
{
	//s_document->Release();
}

} // rage
