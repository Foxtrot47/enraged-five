#include "rageColladaConditioner.h"

#include <cassert>

namespace rage
{

static std::string NULL_STRING = "";

rageConditioner::rageConditioner()
:	m_inputCount ( 1 )
,	m_outputCount ( 1 )
,	m_variableInputs ( false )
{
	m_inputs.resize( 1 );
	m_outputs.resize( 1 );

	m_outputs[ 0 ].linked = true;
	m_outputs[ 0 ].editable = false;
}

rageConditioner::rageConditioner( const rageConditioner &rhs )
{

}

rageConditioner::~rageConditioner()
{

}

bool rageConditioner::SetDocument( FCDocument* doc )
{
	m_document = doc;
	return true;
}

void rageConditioner::SetName( const std::string& name )
{
	m_name = name;
}

void rageConditioner::SetInputCount( unsigned int count, bool variable )
{
	m_inputCount = count;
	m_inputs.resize( m_inputCount );
	m_variableInputs = variable;
}

bool rageConditioner::SetInput( unsigned int index, const std::string& input )
{
	if ( m_inputCount <= index )
	{
		return false;
	}

	m_inputs[ index ] = input;

	return true;
}

void rageConditioner::SetOutputCount( unsigned int count )
{
	m_outputCount = count;
	m_outputs.resize( count );
}

bool rageConditioner::SetOutput( unsigned int index, const std::string& output )
{
	if ( m_outputCount <= index )
	{
		return false;
	}

	if ( m_outputs[ index ].linked )
	{
		return false;
	}

	m_outputs[ index ].output = output;

	return true;
}

bool rageConditioner::SetOutputProperties( unsigned int index, bool linked, bool editable )
{
	if ( m_outputCount <= index )
	{
		return false;
	}

	if ( linked && editable )
	{
		return false;
	}

	if ( linked && m_inputCount <= index )
	{
		return false;
	}

	m_outputs[ index ].editable = editable;
	m_outputs[ index ].linked = linked;

	return true;
}

void rageConditioner::AddBoolOption( const std::string& option, const std::string& fullName, const std::string& desc, bool def )
{
	BoolOption bo;
	bo.fullName = fullName;
	bo.description = desc;
	bo.value = def;
	bo.flag = false;

	m_boolOptions.insert( std::make_pair( option, bo ) );
}

void rageConditioner::AddStringOption( const std::string& option, const std::string& fullName, const std::string& desc, const std::string& def )
{
	StringOption so;
	so.fullName = fullName;
	so.description = desc;
	so.value = def;
	so.flag = false;

	m_stringOptions.insert( std::make_pair( option, so ) );
}

void rageConditioner::AddFloatOption( const std::string& option, const std::string& fullName, const std::string& desc, float def )
{
	FloatOption fo;
	fo.fullName = fullName;
	fo.description = desc;
	fo.value = def;
	fo.flag = false;

	m_floatOptions.insert( std::make_pair( option, fo ) );
}

const std::string& rageConditioner::GetName() const
{
	return m_name;
}

int rageConditioner::GetInputCount() const
{
	return m_inputCount;
}

const std::string& rageConditioner::GetInput( unsigned int index ) const
{
	if ( m_inputCount <= index )
	{
		return NULL_STRING;
	}

	return m_inputs[ index ];
}

bool rageConditioner::HasVariableInputCount() const
{
	return m_variableInputs;
}

int rageConditioner::GetOutputCount() const
{
	return m_outputCount;
}

const std::string& rageConditioner::GetOutput( unsigned int index ) const
{
	if ( m_outputCount <= index )
	{
		return NULL_STRING;
	}

	if ( m_outputs[ index ].linked )
	{
		return m_inputs[ index ];
	}

	return m_outputs[ index ].output;
}

bool rageConditioner::IsOutputLinked( unsigned int index ) const
{
	if ( m_outputCount <= index )
	{
		return false;
	}

	return m_outputs[ index ].linked;
}

bool rageConditioner::IsOutputEditable( unsigned int index ) const
{
	if ( m_outputCount <= index )
	{
		return false;
	}

	return m_outputs[ index ].editable;
}

bool rageConditioner::GetBoolOption( const std::string& option, bool& out ) const
{
	BoolOptionMap::const_iterator iter = m_boolOptions.find( option );
	if ( iter == m_boolOptions.end() )
	{
		return false;
	}

	out = iter->second.value;

	return true;
}

bool rageConditioner::GetStringOption( const std::string& option, std::string& out ) const
{
	StringOptionMap::const_iterator iter = m_stringOptions.find( option );
	if ( iter == m_stringOptions.end() )
	{
		return false;
	}

	out = iter->second.value;

	return true;
}

bool rageConditioner::GetFloatOption( const std::string& option, float& out ) const
{
	FloatOptionMap::const_iterator iter = m_floatOptions.find( option );
	if ( iter == m_floatOptions.end() )
	{
		return false;
	}

	out = iter->second.value;

	return true;
}

bool rageConditioner::SetBoolOption( const std::string& option, bool val )
{
	BoolOptionMap::iterator iter = m_boolOptions.find( option );
	if ( iter == m_boolOptions.end() )
	{
		return false;
	}

	iter->second.value = val;

	return true;
}

bool rageConditioner::SetStringOption( const std::string& option, const std::string& val )
{
	StringOptionMap::iterator iter = m_stringOptions.find( option );
	if ( iter == m_stringOptions.end() )
	{
		return false;
	}

	iter->second.value = val;

	return true;
}

bool rageConditioner::SetFloatOption( const std::string& option, float val )
{
	FloatOptionMap::iterator iter = m_floatOptions.find( option );
	if ( iter == m_floatOptions.end() )
	{
		return false;
	}

	iter->second.value = val;

	return true;
}

const rageConditioner::BoolOptionMap& rageConditioner::GetBoolOptionsMap() const
{
	return m_boolOptions;
}

const rageConditioner::StringOptionMap& rageConditioner::GetStringOptionsMap() const
{
	return m_stringOptions;
}

const rageConditioner::FloatOptionMap& rageConditioner::GetFloatOptionsMap() const
{
	return m_floatOptions;
}

void rageConditioner::PrintDebugMessage( const std::string& message )
{
	rageColladaLoader::PrintDebugMessage( message );
}

void rageConditioner::PrintExecutionMessage( const std::string& message )
{
	rageColladaLoader::PrintExecutionMessage( message );
}

void rageConditioner::PrintErrorMessage( const std::string& message )
{
	rageColladaLoader::PrintErrorMessage( message );
}

rageIConditioner* rageConditioner::Create() const
{
	assert( false );
	
	return NULL;
}

} // rage
