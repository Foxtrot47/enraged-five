// /rageDiplomatTreaty.h

#ifndef __RAGE_DIPLOMAT_TREATY_H__
#define __RAGE_DIPLOMAT_TREATY_H__

#include <string>

#include "atl/functor.h"

using namespace std;
using namespace rage;

//-----------------------------------------------------------------------------

class FCDSceneNode;

//-----------------------------------------------------------------------------

class rageDiplomatTreaty
{
public:
typedef Functor2< FCDSceneNode*, void* >	TREATY_ACCEPTED_FTOR;

public:
	rageDiplomatTreaty(TREATY_ACCEPTED_FTOR accpetedFtor, const char* szTreatyName)
		: m_treatyName(szTreatyName)
		, m_pAcceptedData(NULL)
	{
		m_acceptedFtor = accpetedFtor;
	};

	rageDiplomatTreaty(TREATY_ACCEPTED_FTOR accpetedFtor, const char* szTreatyName, void* pAcceptedData)
		: m_treatyName(szTreatyName)
		, m_pAcceptedData(pAcceptedData)
	{
		m_acceptedFtor = accpetedFtor;
	};

	virtual ~rageDiplomatTreaty() {};

	virtual bool Propose(FCDSceneNode* pNode) = 0;

	const char*	GetTreatyName() const { return m_treatyName.c_str(); }

protected:
//	virtual bool ShouldAccept(FCDSceneNode *pNode) = 0;

protected:
	TREATY_ACCEPTED_FTOR	m_acceptedFtor;
	void*					m_pAcceptedData;

	string					m_treatyName;
};

//-----------------------------------------------------------------------------
#if 0
template < typename TFunctor >
bool rageDiplomatTreaty::Propose(FCDSceneNode* pNode, TFunctor func)
{
	if ( func( pNode ) )
	{
		m_acceptedFtor(pNode, m_pAcceptedData );
		return true;
	}
	else
	{
		return false;
	}

/*
	if(ShouldAccept(pNode))
	{
		m_acceptedFtor(pNode, m_pAcceptedData );
		return true;
	}
	else
		return false;
		*/
}
#endif
//-----------------------------------------------------------------------------

#endif //__RAGE_DIPLOMAT_TREATY_H__
