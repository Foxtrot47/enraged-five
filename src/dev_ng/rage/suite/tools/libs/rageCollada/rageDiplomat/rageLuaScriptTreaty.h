// /rageLuaScriptTreaty.h

#ifndef __RAGE_LUASCRIPT_TREATY_H__
#define __RAGE_LUASCRIPT_TREATY_H__

#ifndef DIPLOMAT_NO_LUA_SCRIPT

#include <string>

#include "rageLuaScriptHost/rageLuaScriptHost.h"
#include "rageDiplomatTreaty.h"

using namespace rage;

//-----------------------------------------------------------------------------

class rageShouldAcceptFtor
{
public:
	rageShouldAcceptFtor( const char* szScriptFnName )
		:	m_scriptFnName( szScriptFnName ) { }
	rageShouldAcceptFtor() {}
public:
	bool operator()( FCDSceneNode* pNode )
	{
		int iResult; 
		iResult = rageLuaScriptHostCall< int , rageLshSwigObjWrap > (m_scriptFnName.c_str()).call( rageLshSwigObjWrap("FCollada", "FCDSceneNode_p_ctor", pNode) );
		if(iResult > 0)
			return true;
		else
			return false;
	}
private:
	std::string m_scriptFnName;
};

//-----------------------------------------------------------------------------

class rageShouldAcceptNodeParamFtor
{
public:
	rageShouldAcceptNodeParamFtor( const char* szScriptFnName, FCDSceneNode* pNode )
		:	m_scriptFnName( szScriptFnName ),
			m_pNode(pNode) { }
	rageShouldAcceptNodeParamFtor() {}
public:
	bool operator()( FCDSceneNode* pNode )
	{
		int iResult; 
		iResult = rageLuaScriptHostCall< int , rageLshSwigObjWrap, rageLshSwigObjWrap >
			(m_scriptFnName.c_str()).call( rageLshSwigObjWrap("FCollada", "FCDSceneNode_p_ctor", pNode), rageLshSwigObjWrap("FCollada", "FCDSceneNode_p_ctor", m_pNode) );
		if(iResult > 0)
			return true;
		else
			return false;
	}
private:
	std::string m_scriptFnName;
	FCDSceneNode* m_pNode;
};

//-----------------------------------------------------------------------------

template < typename TAcceptFtor >
class rageLuaScriptTreaty : public rageDiplomatTreaty
{
public:
	rageLuaScriptTreaty(rageDiplomatTreaty::TREATY_ACCEPTED_FTOR acceptedFtor, void* pAcceptedData)
		: rageDiplomatTreaty(acceptedFtor, "rageLuaScriptTreaty", pAcceptedData)
	{};

	rageLuaScriptTreaty(rageDiplomatTreaty::TREATY_ACCEPTED_FTOR acceptedFtor, const char* szTreatyName, void* pAcceptedData)
		: rageDiplomatTreaty(acceptedFtor, szTreatyName, pAcceptedData)
	{};

	virtual ~rageLuaScriptTreaty() {};

	virtual bool Propose(FCDSceneNode* pNode);

	// HACK: BE CARFUL NOT TO PASS IN SOMETHING THAT WILL GO OUT OF SCOPE BEFORE
	//	IT IS VISITIED. THIS NEEDS TO BE FIXED AT SOME STAGE.
	bool InitializeTreatyScript(const char* szScriptFilePath, TAcceptFtor& acceptFtor);
private:
	TAcceptFtor* m_acceptFtor;
};

template < typename TAcceptFtor >
bool rageLuaScriptTreaty< TAcceptFtor >::Propose(FCDSceneNode* pNode)
{
	if ( ( *m_acceptFtor )( pNode ) )
	{
		m_acceptedFtor(pNode, m_pAcceptedData );
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

template < typename TAcceptFtor >
bool rageLuaScriptTreaty< TAcceptFtor >::InitializeTreatyScript(const char* szScriptFilePath, TAcceptFtor& acceptFtor)
{
	Assert(rageLuaScriptHostSingleton::IsInstantiated());

	if(!LUASCRIPTHOST.LoadAndExecuteScript(szScriptFilePath))
	{
		return false;
	}

	m_acceptFtor = &acceptFtor;

	return true;
}

//-----------------------------------------------------------------------------

#endif //DIPLOMAT_NO_LUA_SCRIPT

#endif //__RAGE_LUASCRIPT_TREATY_H__
