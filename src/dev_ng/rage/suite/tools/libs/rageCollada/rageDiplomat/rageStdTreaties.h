// /rageStdTreaties.h

#ifndef __RAGE_STD_TREATIES_H__
#define __RAGE_STD_TREATIES_H__

#include "rageDiplomatTreaty.h"

//-----------------------------------------------------------------------------

class rageNameMatchTreaty : public rageDiplomatTreaty
{
public:
	rageNameMatchTreaty(rageDiplomatTreaty::TREATY_ACCEPTED_FTOR acceptedFtor, const char* szNameMatchExpr, void* pAcceptedData )
		: rageDiplomatTreaty(acceptedFtor, "NameMatch", pAcceptedData)
		, m_nameMatchExpr(szNameMatchExpr)
	{};

	rageNameMatchTreaty(rageDiplomatTreaty::TREATY_ACCEPTED_FTOR acceptedFtor, const char* szTreatyName, const char* szNameMatchExpr, void* pAcceptedData )
		: rageDiplomatTreaty(acceptedFtor, szTreatyName, pAcceptedData)
		, m_nameMatchExpr(szNameMatchExpr)
	{};

	virtual ~rageNameMatchTreaty() {};

	virtual bool Propose(FCDSceneNode* pNode);

private:
	int StrWildCmp(const char *wild, const char *string) const;

protected:
	string	m_nameMatchExpr;
};

//-----------------------------------------------------------------------------

class rageChildOfTreaty : public rageNameMatchTreaty
{
public:
	rageChildOfTreaty(rageDiplomatTreaty::TREATY_ACCEPTED_FTOR acceptedFtor, const char* szNameMatchExpr, void* pAcceptedData )
		: rageNameMatchTreaty(acceptedFtor, "ChildOf", szNameMatchExpr, pAcceptedData)
	{};
	virtual ~rageChildOfTreaty() {};

	virtual bool Propose(FCDSceneNode* pNode);
};

//-----------------------------------------------------------------------------

#endif //__RAGE_STD_TREATIES_H__
