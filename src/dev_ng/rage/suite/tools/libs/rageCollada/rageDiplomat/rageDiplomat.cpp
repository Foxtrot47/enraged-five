//rageDiplomat.cpp

#include <queue>

#include "rageDiplomat.h"
#include "rageDiplomatTreaty.h"

using namespace std;

//-----------------------------------------------------------------------------

rageDiplomat::~rageDiplomat()
{
	ClearTreaties();
}

//-----------------------------------------------------------------------------

bool rageDiplomat::RegisterTreaty(rageDiplomatTreaty* pTreaty)
{
	m_treaties.push_back(pTreaty);
	return true;
}

//-----------------------------------------------------------------------------

void rageDiplomat::ClearTreaties()
{
	for(TREATY_TABLE::iterator it = m_treaties.begin(); it != m_treaties.end(); ++it)
	{
		delete (*it);
	}

	m_treaties.clear();
}

//-----------------------------------------------------------------------------

void rageDiplomat::VisitDepthFirst(FCDSceneNode* pNode, const Signings bAllowMultipleSignings)
{
	for(TREATY_TABLE::iterator it = m_treaties.begin(); it != m_treaties.end(); ++it)
	{
		rageDiplomatTreaty* pTreaty = (*it);
		if(pTreaty->Propose(pNode))
		{
			//Treaty was signed
			if(SINGLE_SIGNING == bAllowMultipleSignings)
				break;
		}
	}

	size_t nChildren = pNode->GetChildrenCount();
	for(size_t childIdx=0; childIdx<nChildren; childIdx++)
	{
		VisitDepthFirst(pNode->GetChild(childIdx), bAllowMultipleSignings );
	}
}

//-----------------------------------------------------------------------------

void rageDiplomat::VisitBreadthFirst(FCDSceneNode* pNode, const Signings bAllowMultipleSignings)
{
	queue<FCDSceneNode*> traversalQueue;

	traversalQueue.push(pNode);

	while(!traversalQueue.empty())
	{
		FCDSceneNode *pCurrentNode = traversalQueue.front();
		traversalQueue.pop();

		for(TREATY_TABLE::iterator it = m_treaties.begin(); it != m_treaties.end(); ++it)
		{
			rageDiplomatTreaty* pTreaty = (*it);
			if(pTreaty->Propose(pCurrentNode))
			{
				//Treaty was signed
				if(SINGLE_SIGNING == bAllowMultipleSignings)
					break;
			}
		}

		size_t nChildren = pCurrentNode->GetChildrenCount();
		for(size_t childIdx=0; childIdx<nChildren; childIdx++)
		{
			traversalQueue.push(pCurrentNode->GetChild(childIdx));
		}
	}

}

//-----------------------------------------------------------------------------

void rageDiplomat::ForEach(FCDSceneNode** pBeginNode, FCDSceneNode** pEndNode, const Signings bAllowMultipleSignings)
{
	FCDSceneNode** pCurrentNode = pBeginNode;
	while ( *pCurrentNode != *pEndNode )
	{
		for ( TREATY_TABLE::iterator it = m_treaties.begin(); it != m_treaties.end(); ++it )
		{
			rageDiplomatTreaty* pTreaty = (*it);
			if ( pTreaty->Propose( *pCurrentNode) )
			{
				if ( SINGLE_SIGNING == bAllowMultipleSignings )
				{
					break;
				}
			}
		}

		++pCurrentNode;
	}
}

