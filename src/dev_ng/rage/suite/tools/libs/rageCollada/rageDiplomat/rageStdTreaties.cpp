// /rageStdTreaties.cpp

#include <queue>

#include "rageStdTreaties.h"

using namespace std;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

bool rageNameMatchTreaty::Propose(FCDSceneNode* pNode)
{
	if(StrWildCmp(m_nameMatchExpr.c_str(), pNode->GetName().c_str()))
		return true;
	else
		return false;
}

//-----------------------------------------------------------------------------

int rageNameMatchTreaty::StrWildCmp(const char *wild, const char *string) const
{
	const char *cp = NULL, *mp = NULL;

	while ((*string) && (*wild != '*')) 
	{
		if ((*wild != *string) && (*wild != '?')) 
		{
		return 0;
		}
		wild++;
		string++;
	}

	while (*string) 
	{
		if (*wild == '*') 
		{
		if (!*++wild) 
		{
			return 1;
		}
		mp = wild;
		cp = string+1;
		} 
		else if ((*wild == *string) || (*wild == '?')) 
		{
		wild++;
		string++;
		} 
		else 
		{
		wild = mp;
		string = cp++;
		}
	}

	while (*wild == '*') 
	{
		wild++;
	}
	return !*wild;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

bool rageChildOfTreaty::Propose(FCDSceneNode* pNode)
{
	size_t nParents = pNode->GetParentCount();
	if( nParents == 0)
		return false;
	else
	{
		queue<FCDSceneNode*>	m_parentQueue;

		// seed the parent queue
		for(size_t pIdx=0; pIdx < nParents; pIdx++)
		{
			FCDSceneNode *pParentNode = pNode->GetParent(pIdx);
			Assert(pParentNode);
			m_parentQueue.push(pParentNode);
		}

		while(m_parentQueue.size())
		{
			FCDSceneNode *pParentNode = m_parentQueue.front();
			m_parentQueue.pop();

			if(rageNameMatchTreaty::Propose(pParentNode))
				return true;
			else
			{
				size_t nGrandParents = pParentNode->GetParentCount();
				for(size_t gpIdx = 0; gpIdx < nGrandParents; gpIdx++)
					m_parentQueue.push(pParentNode->GetParent(gpIdx));
			}
		}
	}

	return false;
}

//-----------------------------------------------------------------------------
