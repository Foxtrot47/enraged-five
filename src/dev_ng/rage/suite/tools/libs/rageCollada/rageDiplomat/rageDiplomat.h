// /rageDiplomat.h

#ifndef __RAGE_DIPLOMAT_H__
#define __RAGE_DIPLOMAT_H__

#include <vector>
#include <string>

#include "rageDiplomatTreaty.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

class FCDSceneNode;

//-----------------------------------------------------------------------------

class rageDiplomat
{
public:
	typedef vector<rageDiplomatTreaty*>	TREATY_TABLE;

	enum Signings
	{
		SINGLE_SIGNING		= 0,
		MULTIPLE_SIGNING,
	};
public:
	rageDiplomat() {};
	virtual ~rageDiplomat();

	bool	RegisterTreaty(rageDiplomatTreaty* pTreaty);
	void	ClearTreaties();

	void	VisitDepthFirst(FCDSceneNode* pNode, Signings bAllowMultipleSignings = SINGLE_SIGNING);
	void	VisitBreadthFirst(FCDSceneNode* pNode, Signings bAllowMultipleSignings = SINGLE_SIGNING);

	void	ForEach(FCDSceneNode** pBeginNode, FCDSceneNode** pEndNode, Signings bAllowMultipleSignings = SINGLE_SIGNING);
protected:
	TREATY_TABLE	m_treaties;

};

//-----------------------------------------------------------------------------

#endif //__RAGE_DIPLOMAT_H__

