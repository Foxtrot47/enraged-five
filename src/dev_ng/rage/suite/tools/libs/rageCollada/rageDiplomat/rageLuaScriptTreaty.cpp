// rageLuaScriptTreaty.cpp

#ifndef DIPLOMAT_NO_LUA_SCRIPT

#include "rageLuaScriptHost/rageLuaScriptHost.h"
#include "rageLuaScriptTreaty.h"

//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
#if 0 
bool rageLuaScriptTreaty::ShouldAccept(FCDSceneNode *pNode)
{
	int iResult; 
	iResult = rageLuaScriptHostCall< int , rageLshSwigObjWrap > (m_scriptFnName.c_str()).call( rageLshSwigObjWrap("FCollada", "FCDSceneNode_p_ctor", pNode) );
	if(iResult > 0)
		return true;
	else
		return false;
}
#endif // 0
//---------------------------------------------------------------------------

#endif //DIPLOMAT_NO_LUA_SCRIPT
