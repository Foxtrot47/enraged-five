#ifndef __RAGE_COLLADA_REFINERY__
#define __RAGE_COLLADA_REFINERY__
#pragma once

namespace rage 
{

	class rageColladaRefinery
	{
	public:
		void Run( const char* infile, const char* outfile );
	public:
		rageColladaRefinery( const char* conditioners, const char* pipeline );
		~rageColladaRefinery();
	};

} // rage

#endif // __RAGE_COLLADA_REFINERY__
