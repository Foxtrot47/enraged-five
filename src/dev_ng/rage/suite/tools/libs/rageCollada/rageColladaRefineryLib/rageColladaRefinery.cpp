#include "rageColladaRefinery.h"

#pragma warning( disable : 4668 )

#ifdef DEPRECATED
#undef DEPRECATED
#endif

#include "FCollada.h"
#include "FCDocument/FCDocument.h"

#include "rageColladaLoader.h"
#include "rageColladaConditioner.h"

#include <windows.h>

#include <cassert>
#include <cstdio>

#include <map>
#include <string>
#include <vector>

#include "tinyxml.h"

using namespace rage;

static const std::string LIB_EXTENSION = "dll";

static const char* PIPELINE_ELEMENT = "Pipeline";
static const char* CONDITIONER_ELEMENT = "Conditioner";
static const char* LINK_ELEMENT = "Link";
static const char* INPUT_ELEMENT = "Input";
static const char* OUTPUT_ELEMENT = "Output";

struct BuildConditioner
{
	std::string name;
	std::string lib;

	rageIConditioner* conditioner;
};

struct BuildLink
{
	BuildConditioner* start;
	BuildConditioner* end;
};

typedef std::map< std::string, BuildConditioner* > BuildConditionerMap;
BuildConditionerMap g_BuildConditionerMap;

typedef std::vector< BuildLink* > Pipeline;
Pipeline g_Pipeline;

typedef std::vector< HMODULE > ConditionerList;
ConditionerList g_ConditionerList;

typedef std::vector< HMODULE > CompilerList;
CompilerList g_CompilerList;

static void Build_Load( std::vector< HMODULE >& libraries, const std::string& path )
{
	typedef std::vector< std::string > FileList;
	FileList files;

	WIN32_FIND_DATA fdata;
	HANDLE handle;

	char buf[ MAX_PATH ];
	_snprintf( buf, sizeof( buf ), "%s\\*", path.c_str() );

	if ( INVALID_HANDLE_VALUE == ( handle = FindFirstFile( buf, &fdata ) ) )
	{
		printf( "FindFirstFile" );
		exit( 1 );
	}

	std::string filename = fdata.cFileName;

	std::string::size_type pos = filename.find_last_of( "." );
	std::string ext( filename, pos + 1 );
	
	if ( LIB_EXTENSION == ext )
	{
		if ( HMODULE m = LoadLibrary( filename.c_str() ) )
		{
			libraries.push_back( m );
		}
		else
		{
			printf( "failed to load conditioner: %s.\n", filename.c_str() );
			exit( 1 );
		}
	}

	for ( ;; )
	{
		if ( FindNextFile( handle, &fdata ) )
		{
			std::string filename = fdata.cFileName;

			std::string::size_type pos = filename.find_last_of( "." );
			std::string ext( filename, pos + 1 );

			if ( LIB_EXTENSION == ext )
			{
				std::string fullname = path + std::string( "\\" ) + filename;

				if ( HMODULE m = LoadLibrary( fullname.c_str() ) )
				{
					libraries.push_back( m );
				}
				else
				{
					printf( "failed to load conditioner: %s.\n", filename.c_str() );
					exit( 1 );
				}
			}
		}
		else
		if ( ERROR_NO_MORE_FILES == GetLastError() )
		{
			break;
		}
	}

	return;
}

static void Build_Unload( std::vector< HMODULE >& libraries )
{
	for ( ConditionerList::iterator iter = libraries.begin();
			iter != libraries.end(); ++iter )
	{
		FreeLibrary( *iter );
	}

	g_BuildConditionerMap.clear();
	g_ConditionerList.clear();
	g_CompilerList.clear();
	g_Pipeline.clear();
}

static void Build_LoadPipeline( const std::string& filename )
{
	TiXmlDocument doc ( filename.c_str() );
	if ( !doc.LoadFile() )
	{
		printf( "ERROR: could not load file: %s\n", filename.c_str() );
		exit( 1 );
	}

	TiXmlNode* node = NULL;
	TiXmlElement* pipeline = NULL;

	node = doc.FirstChild( PIPELINE_ELEMENT );
	assert( node );
	pipeline = node->ToElement();
	assert( pipeline );

	TiXmlElement* conditioner = NULL;

	conditioner = pipeline->FirstChildElement( CONDITIONER_ELEMENT );
	assert( conditioner );

	while ( conditioner )
	{
		BuildConditioner* c = new BuildConditioner;
		c->name = std::string( conditioner->Attribute( "Name" ) );
		c->lib = std::string( conditioner->Attribute( "LibName" ) );

		// TODO: HANDLE A CONDITIONER NOT BEING AVAILABLE

		const rageConditionerCreator* cc = 
			rageColladaLoader::GetAvailableConditioner( c->lib );
		c->conditioner = cc->Create();

		std::string id = std::string( conditioner->Attribute( "IDName" ) );

		g_BuildConditionerMap.insert( std::make_pair( id, c ) );

		conditioner = conditioner->NextSiblingElement( CONDITIONER_ELEMENT );
	}

	TiXmlElement* input = pipeline->FirstChildElement( INPUT_ELEMENT );
	BuildConditioner* ci = new BuildConditioner;
	ci->name = std::string( input->Attribute( "Name" ) );
	ci->conditioner = NULL;

	g_BuildConditionerMap.insert( std::make_pair( ci->name, ci ) );

	TiXmlElement* output = pipeline->FirstChildElement( OUTPUT_ELEMENT );
	BuildConditioner* co = new BuildConditioner;
	co->name = std::string( output->Attribute( "Name" ) );
	co->conditioner = NULL;

	g_BuildConditionerMap.insert( std::make_pair( co->name, co ) );

	TiXmlElement* link = NULL;

	link = pipeline->FirstChildElement( LINK_ELEMENT );
	assert( link );

	while ( link )
	{
		BuildConditionerMap::iterator si = 
			g_BuildConditionerMap.find( link->Attribute( "Start" ) );
		if ( si == g_BuildConditionerMap.end() )
		{
			printf( "ERROR: Badly formatted pipeline file.\n" );
			exit( 1 );
		}

		BuildConditionerMap::iterator ei =
			g_BuildConditionerMap.find( link->Attribute( "End" ) );
		if ( ei == g_BuildConditionerMap.end() )
		{
			printf( "ERROR: Badly formatted pipeline file.\n" );
			exit( 1 );
		}

		BuildLink* l = new BuildLink;
		l->start = si->second;
		l->end = ei->second;

		g_Pipeline.push_back( l );

		link = link->NextSiblingElement( LINK_ELEMENT );
	}

	// Print the pipeline for _reference_
	for ( Pipeline::iterator it = g_Pipeline.begin(); it != g_Pipeline.end(); ++it )
	{
		printf( "start: %s, end: %s\n", (*it)->start->name.c_str(), (*it)->end->name.c_str() );
	}
}

static void Build_DestroyPipeline()
{
	for ( BuildConditionerMap::iterator i = g_BuildConditionerMap.begin(); 
		i != g_BuildConditionerMap.end(); ++i )
	{
		delete i->second;
	}

	g_BuildConditionerMap.clear();
}

static void Build_RunPipeline( FCDocument* document )
{
	// TODO: CREATE THE CONDITIONERS HERE RATHER THAN WHEN THE PIPELINE IS LOADED
	for ( Pipeline::iterator iter = g_Pipeline.begin();
			iter != g_Pipeline.end(); ++iter )
	{
		if ( ( *iter )->end && ( *iter )->end->conditioner )	
		{
			const std::string& name = ( *iter )->end->name;

			rageIConditioner* conditioner = ( *iter )->end->conditioner;

			rageColladaLoader::AddConditioner( document, name, conditioner );

			conditioner->Init();
			conditioner->Execute();

			rageColladaLoader::RemoveConditioner( name );
		}
	}
}

namespace rage 
{

	rageColladaRefinery::rageColladaRefinery( const char* conditioners, const char* pipeline )
	{
		rageColladaLoader::Startup();

		Build_Load( g_ConditionerList, conditioners );
		Build_LoadPipeline( pipeline );
	}

	rageColladaRefinery::~rageColladaRefinery()
	{
		Build_DestroyPipeline();
		Build_Unload( g_ConditionerList );

		rageColladaLoader::Shutdown();
	}

	void rageColladaRefinery::Run( const char* infile, const char* outfile )
	{
		FCDocument* document = new FCDocument;

		if ( true != rageColladaLoader::LoadDocument( document, infile ) )
		{
			Errorf( "failed to load: %s", infile );
			return;
		}

		Build_RunPipeline( document );

		rageColladaLoader::SaveDocument( document, outfile );

		delete document;
	}

} // rage
