#include "ImageLibraryMerger.h"

//-----------------------------------------------------------------------------
// PURPOSE
//	Overrides the base implementation to handle comparing two FCDImage objects.
// NOTE
//	FCDImage objects are considered identical if they point to the same file.
//-----------------------------------------------------------------------------
bool ImageLibraryMerger::AreEntitiesIdentical( const FCDEntity* entity1, const FCDEntity* entity2 ) const
{
	AssertMsg( entity1!=NULL, "NULL value passed for entity1 in ImageLibraryMerger::AreEntitiesIdentical()" );
	AssertMsg( entity2!=NULL, "NULL value passed for entity2 in ImageLibraryMerger::AreEntitiesIdentical()" );

	// Should be dealing with FCDImage objects
	const FCDImage* image1 = dynamic_cast<const FCDImage*>( entity1 );
	const FCDImage* image2 = dynamic_cast<const FCDImage*>( entity2 );
	AssertMsg( image1!=NULL, "Dynamic cast from FCDEntity to FCDImage failed in ImageLibraryMerger::AreEntitiesIdentical()" );
	AssertMsg( image2!=NULL, "Dynamic cast from FCDEntity to FCDImage failed in ImageLibraryMerger::AreEntitiesIdentical()" );

	// Compare the image filenames
	return ( image1->GetFilename() == image2->GetFilename() );
}
