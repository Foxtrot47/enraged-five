#include "Merger.h"

//-----------------------------------------------------------------------------
// PURPOSE
//	Copies the errors/warnings of a generic Merger into this list of errors/warnings.
// PARAMS
//	merger - The Merger from which to copy the errors/warnings.
//-----------------------------------------------------------------------------
void Merger::CopyErrorsAndWarnings( const Merger& merger )
{
	std::vector<std::string> errors = merger.GetErrors();
	std::vector<std::string> warnings = merger.GetWarnings();

	size_t errorCount = errors.size();
	for ( size_t i = 0; i < errorCount; ++i )
	{
		Error( errors[i] );
	}

	size_t warningCount = warnings.size();
	for ( size_t i = 0; i < warningCount; ++i )
	{
		Warning( warnings[i] );
	}
}
