#include "MaterialLibraryMerger.h"

//-----------------------------------------------------------------------------
// PURPOSE
//	Overrides the base implementation to handle comparing two FCDMaterial objects.
// NOTE
//	FCDMaterial objects are considered identical when they reference the same FCDEffect.
//-----------------------------------------------------------------------------
bool MaterialLibraryMerger::AreEntitiesIdentical( const FCDEntity* entity1, const FCDEntity* entity2 ) const
{
	AssertMsg( entity1!=NULL, "NULL value passed for entity1 in MaterialLibraryMerger::AreEntitiesIdentical()" );
	AssertMsg( entity2!=NULL, "NULL value passed for entity2 in MaterialLibraryMerger::AreEntitiesIdentical()" );

	// Should be dealing with FCDMaterial objects
	const FCDMaterial* material1 = dynamic_cast<const FCDMaterial*>( entity1 );
	const FCDMaterial* material2 = dynamic_cast<const FCDMaterial*>( entity2 );
	AssertMsg( material1!=NULL, "Dynamic cast from FCDEntity to FCDMaterial failed in MaterialLibraryMerger::AreEntitiesIdentical()" );
	AssertMsg( material2!=NULL, "Dynamic cast from FCDEntity to FCDMaterial failed in MaterialLibraryMerger::AreEntitiesIdentical()" );

	// Get the effects for each material
	const FCDEffect* effect1 = material1->GetEffect();
	const FCDEffect* effect2 = material2->GetEffect();

	// Compare the effects
	return ( effect1 != NULL && effect2 != NULL && effect1->GetDaeId() == effect2->GetDaeId() );
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Overrides the base implementation to handle effect entity references.
//-----------------------------------------------------------------------------
void MaterialLibraryMerger::CopyEntityToDestLibrary( const FCDEntity* sourceEntity )
{
	AssertMsg( sourceEntity!=NULL, "NULL value passed for sourceEntity in MaterialLibraryMerger::CopyEntityToDestLibrary()" );

	// We should be operating on FCDMaterials
	const FCDMaterial* sourceMaterial = dynamic_cast<const FCDMaterial*>( sourceEntity );
	AssertMsg( sourceMaterial!=NULL, "Dynamic cast from FCDEntity to FCDMaterial failed in MaterialLibraryMerger::CopyEntityToDestLibrary()" );

	// Create a new FCDMaterial entry in the destination library
	FCDMaterial* destMaterial = m_destLibrary->AddEntity();
	AssertMsg( destMaterial!=NULL, "Failed to add new material to destination library in MaterialLibraryMerger::CopyEntityToDestLibrary()" );

	// Make the destination material and source materials have matching IDs
	// NOTE: We are not cloning the source material here because it doesn't
	// seem to preserve the effect references properly and causes serious
	// problems when trying to manually fixup the references
	destMaterial->SetDaeId( sourceMaterial->GetDaeId() );

	// Get the destination and source effect references
	FCDEntityReference* destEffectReference = destMaterial->GetEffectReference();
	const FCDEntityReference* sourceEffectReference = sourceMaterial->GetEffectReference();
	AssertMsg( destEffectReference!=NULL, "Failed to get the destination effect reference in MaterialLibraryMerger::CopyEntityToDestLibrary()" );
	AssertMsg( sourceEffectReference!=NULL, "Failed to get the source effect reference in MaterialLibraryMerger::CopyEntityToDestLibrary()" );

	// Manually point the destination effect reference at the same entity that
	// the source entity reference points at
	const FCDEntity* sourceEffectEntity = sourceEffectReference->GetEntity();
	if ( sourceEffectEntity != NULL )
	{
		FCDEntity* destEffectEntity = m_destLibrary->GetDocument()->FindEntity( sourceEffectEntity->GetDaeId() );
		if ( destEffectEntity != NULL )
		{
			destEffectReference->SetEntity( destEffectEntity );
		}
		else
		{
			std::string error( "The effect " );
			error.append( sourceEffectEntity->GetDaeId() );
			error.append( " referenced by material " );
			error.append( sourceMaterial->GetDaeId() );
			error.append( " could not be found in the merged document." );
			Error( error );
		}
	}
}
