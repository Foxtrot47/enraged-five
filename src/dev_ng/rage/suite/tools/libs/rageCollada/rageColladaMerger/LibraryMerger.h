#ifndef LIBRARY_MERGER_H
#define LIBRARY_MERGER_H

#include "Merger.h"

#define MAX_GUIDSIZE 256

//-----------------------------------------------------------------------------
// PURPOSE
//	This class is used to merge any two Collada libraries together. Derived
//	classes can override any step in the merge process to handle special cases.
//-----------------------------------------------------------------------------
template <class T>
class LibraryMerger : public Merger
{
public:
	LibraryMerger( T destLibrary, T sourceLibrary );
	virtual ~LibraryMerger();

public:
	virtual void		Merge();

protected:
	virtual bool		AreEntitiesIdentical( const FCDEntity* entity1, const FCDEntity* entity2 ) const;
	virtual void		CopyEntityToDestLibrary( const FCDEntity* sourceEntity );
	virtual FCDEntity*	FindEntityInDestLibrary( const FCDEntity* sourceEntity ) const;

protected:
	static void			MakeIdUnique( FCDEntity* entity );

protected:
	T					m_destLibrary;
	T					m_sourceLibrary;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
template <class T>
LibraryMerger<T>::LibraryMerger( T destLibrary, T sourceLibrary )
{
	AssertMsg( destLibrary!=NULL, "NULL value passed for destLibrary in LibraryMerger::LibraryMerger()" );
	AssertMsg( sourceLibrary!=NULL, "NULL value passed for sourceLibrary in LibraryMerger::LibraryMerger()" );

	m_destLibrary = destLibrary;
	m_sourceLibrary = sourceLibrary;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
template <class T>
LibraryMerger<T>::~LibraryMerger()
{

}

//-----------------------------------------------------------------------------
// PURPOSE
//	Merges the source library into the destination library.
//-----------------------------------------------------------------------------
template <class T>
void LibraryMerger<T>::Merge()
{
	// For each entity in the source library
	size_t sourceEntityCount = m_sourceLibrary->GetEntityCount();
	for ( size_t i = 0; i < sourceEntityCount; ++i )
	{
		FCDEntity* sourceEntity = m_sourceLibrary->GetEntity( i );
		AssertMsg( sourceEntity!=NULL, "Failed to look up entity in source library in LibraryMerger::Merge()" );

		// Search the destination library to see if this entity already exists
		FCDEntity* destEntity = FindEntityInDestLibrary( sourceEntity );
		if ( destEntity == NULL )
		{
			// Make sure the source entity has a unique ID so that it will
			// not conflict with any entities in the destination library
			MakeIdUnique( sourceEntity );

			// Copy it into the destination library
			CopyEntityToDestLibrary( sourceEntity );
		}
		else
		{
			// Force the source entity to have the same ID as the destination entity
			sourceEntity->SetDaeId( destEntity->GetDaeId() );
		}
	}
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Determines if two entities are identical.
// PARAMS
//	entity1 - The first entity to compare.
//	entity2 - The second entity to compare.
// RETURNS
//	True if the two entities are identical, or false otherwise.
// NOTE
//	Because this is the base implementation of this function, we cannot presume
//	to know how to tell if two entities are identical, so always return false.
//-----------------------------------------------------------------------------
template <class T>
bool LibraryMerger<T>::AreEntitiesIdentical( const FCDEntity* entity1, const FCDEntity* entity2 ) const
{
	entity1;
	entity2;
	AssertMsg( entity1!=NULL, "NULL value passed for entity1 in LibraryMerger::AreEntitiesIdentical()" );
	AssertMsg( entity2!=NULL, "NULL value passed for entity2 in LibraryMerger::AreEntitiesIdentical()" );

	return false;
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Copies the specified entity into the destination library.
// PARAMS
//	sourceEntity - The entity to copy.
//-----------------------------------------------------------------------------
template <class T>
void LibraryMerger<T>::CopyEntityToDestLibrary( const FCDEntity* sourceEntity )
{
	AssertMsg( sourceEntity!=NULL, "NULL value passed for sourceEntity in LibraryMerger::CopyEntityToDestLibrary()" );

	FCDEntity* destEntity = m_destLibrary->AddEntity();
	AssertMsg( destEntity!=NULL, "Failed to add new entity to destination library in LibraryMerger::CopyEntityToDestLibrary()" );

	// Perform a simple clone of the entity
	sourceEntity->Clone( destEntity, true );
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Searches the destination library for an entity matching the specified entity.
// PARAMS
//	sourceEntity - The entity to search for.
// RETURNS
//	The matching entity, if found, or NULL otherwise.
//-----------------------------------------------------------------------------
template <class T>
FCDEntity* LibraryMerger<T>::FindEntityInDestLibrary( const FCDEntity* sourceEntity ) const
{
	AssertMsg( sourceEntity!=NULL, "NULL value passed for sourceEntity in LibraryMerger::FindEntityInDestLibrary()" );

	// For each entity in the destination library
	for ( size_t i = 0; i < m_destLibrary->GetEntityCount(); ++i )
	{
		FCDEntity* destEntity = m_destLibrary->GetEntity( i );
		AssertMsg( destEntity!=NULL, "Failed to look up entity in destination library in LibraryMerger::FindEntityInDestLibrary()" );

		// Determine if this entity is identical to the source entity
		if ( AreEntitiesIdentical(destEntity, sourceEntity) )
		{
			// If so, we found a match!
			return destEntity;
		}
	}

	return NULL;
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Ensures the specified entity's DaeId unique by generating a GUID.
// PARAMS
//	entity - The entity to make unique.
//-----------------------------------------------------------------------------
template <class T>
void LibraryMerger<T>::MakeIdUnique( FCDEntity* entity )
{
	// Generate a GUID and convert it to a string
	UUID guid;
	UCHAR* guidString = NULL;
	UuidCreate( &guid );
	UuidToString( &guid, &guidString );

	// Set the entity's new ID
	char uniqueId[ MAX_GUIDSIZE ];
	sprintf( uniqueId, "%s", guidString );
	entity->SetDaeId( uniqueId );

	// Cleanup
	RpcStringFree( &guidString );
}

#endif // #ifndef LIBRARY_MERGER_H
