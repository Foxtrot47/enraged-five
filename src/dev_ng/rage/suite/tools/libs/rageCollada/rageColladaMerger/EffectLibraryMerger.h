#ifndef EFFECT_LIBRARY_MERGER_H
#define EFFECT_LIBRARY_MERGER_H

#include "LibraryMerger.h"

//-----------------------------------------------------------------------------
//PURPOSE
//	Special-case library merger class for merging Collada effect libraries.
//-----------------------------------------------------------------------------
class EffectLibraryMerger : public LibraryMerger<FCDEffectLibrary*>
{
public:
	EffectLibraryMerger( FCDEffectLibrary* destLibrary, FCDEffectLibrary* sourceLibrary )
		: LibraryMerger<FCDEffectLibrary*>( destLibrary, sourceLibrary ) {}

protected:
	virtual bool	AreEntitiesIdentical( const FCDEntity* entity1, const FCDEntity* entity2 ) const;

protected:
	static void		GetImagesFromEffect( std::vector<const FCDImage*>* images, const FCDEffect* effect );
};

#endif // #ifndef EFFECT_LIBRARY_MERGER_H
