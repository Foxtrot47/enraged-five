#ifndef IMAGE_LIBRARY_MERGER_H
#define IMAGE_LIBRARY_MERGER_H

#include "LibraryMerger.h"

//-----------------------------------------------------------------------------
//PURPOSE
//	Special-case library merger class for merging Collada image libraries.
//-----------------------------------------------------------------------------
class ImageLibraryMerger : public LibraryMerger<FCDImageLibrary*>
{
public:
	ImageLibraryMerger( FCDImageLibrary* destLibrary, FCDImageLibrary* sourceLibrary )
		: LibraryMerger<FCDImageLibrary*>( destLibrary, sourceLibrary ) {}

protected:
	virtual bool	AreEntitiesIdentical( const FCDEntity* entity1, const FCDEntity* entity2 ) const;
};

#endif // #ifndef IMAGE_LIBRARY_MERGER_H
