#include "VisualSceneLibraryMerger.h"

//-----------------------------------------------------------------------------
// PURPOSE
//	Overrides the base implementation to account for the fact that there should
//	only be one visual scene to merge.
//-----------------------------------------------------------------------------
void VisualSceneLibraryMerger::Merge()
{
	// Get the visual scene instance from the source document
	FCDSceneNode* sourceScene = m_sourceLibrary->GetDocument()->GetVisualSceneInstance();
	AssertMsg( sourceScene!=NULL, "Failed to get the source visual scene instance in VisualSceneLibraryMerger::Merge()" );

	// For each of the scene nodes in the source visual scene instance
	for ( size_t i = 0; i < sourceScene->GetChildrenCount(); ++i )
	{
		FCDSceneNode* sourceSceneNode = sourceScene->GetChild( i );
		AssertMsg( sourceSceneNode!=NULL, "Failed to look up scene node in source library in VisualSceneLibraryMerger::Merge()" );

		// Make sure the scene node is unique before copying it into
		// the destination visual scene instance
		MakeIdUnique( sourceSceneNode );

		// Copy the scene node into the destination scene
		CopyEntityToDestLibrary( sourceSceneNode );
	}
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Overrides the base implementation to account for the fact that there should
//	only be one visual scene in the destination library.
//-----------------------------------------------------------------------------
void VisualSceneLibraryMerger::CopyEntityToDestLibrary( const FCDEntity* sourceEntity )
{
	AssertMsg( sourceEntity!=NULL, "NULL value passed for sourceEntity in VisualSceneLibraryMerger::CopyEntityToDestLibrary()" );

	// We should be operating on FCDSceneNodes
	const FCDSceneNode* sourceSceneNode = dynamic_cast<const FCDSceneNode*>( sourceEntity );
	AssertMsg( sourceSceneNode!=NULL, "Dynamic cast from FCDEntity to FCDSceneNode failed in VisualSceneLibraryMerger::CopyEntityToDestLibrary()" );

	// Get the visual scene instance from destination document (create one if needed)
	FCDSceneNode* destScene = m_destLibrary->GetDocument()->GetVisualSceneInstance();
	if ( destScene == NULL )
	{
		m_destLibrary->GetDocument()->AddVisualScene();
		destScene = m_destLibrary->GetDocument()->GetVisualSceneInstance();
	}
	AssertMsg( destScene!=NULL, "Failed to get the destination visual scene instance in VisualSceneLibraryMerger::CopyEntityToDestLibrary()" );

	// Clone the source scene node and add it to the destination scene
	// NOTE: We create the new scene node in the source document initially
	// to trick FCollada into keeping all geometry/material references local
	// because it would make them external references otherwise
	FCDSceneNode* destSceneNode = new FCDSceneNode( m_sourceLibrary->GetDocument() );
	sourceSceneNode->Clone( destSceneNode, true );
	destScene->AddChildNode( destSceneNode );
}
