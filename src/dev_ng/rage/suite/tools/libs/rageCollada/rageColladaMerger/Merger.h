#ifndef MERGER_H
#define MERGER_H

#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// PURPOSE
//	Base class for all merging classes.
//-----------------------------------------------------------------------------
class Merger
{
public:
	Merger()																				{}
	virtual ~Merger()																		{}

public:
	const std::vector<std::string>&		GetErrors() const									{ return m_errors; }
	const std::vector<std::string>&		GetWarnings() const									{ return m_warnings; }
	virtual void						Merge()												= 0;

protected:
	virtual void						CopyErrorsAndWarnings( const Merger& merger );
	virtual void						Error( std::string message )						{ m_errors.push_back( message ); }
	virtual void						Warning( std::string message )						{ m_warnings.push_back( message ); }

protected:
	std::vector<std::string>			m_errors;
	std::vector<std::string>			m_warnings;
};

#endif // #ifndef MERGER_H
