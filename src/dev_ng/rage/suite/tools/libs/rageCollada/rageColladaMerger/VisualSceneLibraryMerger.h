#ifndef VISUAL_SCENE_LIBRARY_MERGER_H
#define VISUAL_SCENE_LIBRARY_MERGER_H

#include "LibraryMerger.h"

//-----------------------------------------------------------------------------
//PURPOSE
//	Special-case library merger class for merging Collada visual scene libraries.
//-----------------------------------------------------------------------------
class VisualSceneLibraryMerger : public LibraryMerger<FCDVisualSceneNodeLibrary*>
{
public:
	VisualSceneLibraryMerger( FCDVisualSceneNodeLibrary* destLibrary, FCDVisualSceneNodeLibrary* sourceLibrary )
		: LibraryMerger<FCDVisualSceneNodeLibrary*>( destLibrary, sourceLibrary ) {}

public:
	virtual void	Merge();

protected:
	virtual void	CopyEntityToDestLibrary( const FCDEntity* sourceEntity );
};

#endif // #ifndef VISUAL_SCENE_LIBRARY_MERGER_H
