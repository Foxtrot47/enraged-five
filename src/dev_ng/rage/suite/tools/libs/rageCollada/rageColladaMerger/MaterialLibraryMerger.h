#ifndef MATERIAL_LIBRARY_MERGER_H
#define MATERIAL_LIBRARY_MERGER_H

#include "LibraryMerger.h"

//-----------------------------------------------------------------------------
//PURPOSE
//	Special-case library merger class for merging Collada material libraries.
//-----------------------------------------------------------------------------
class MaterialLibraryMerger : public LibraryMerger<FCDMaterialLibrary*>
{
public:
	MaterialLibraryMerger( FCDMaterialLibrary* destLibrary, FCDMaterialLibrary* sourceLibrary )
		: LibraryMerger<FCDMaterialLibrary*>( destLibrary, sourceLibrary ) {}

protected:
	virtual bool	AreEntitiesIdentical( const FCDEntity* entity1, const FCDEntity* entity2 ) const;
	virtual void	CopyEntityToDestLibrary( const FCDEntity* sourceEntity );
};

#endif // #ifndef MATERIAL_LIBRARY_MERGER_H
