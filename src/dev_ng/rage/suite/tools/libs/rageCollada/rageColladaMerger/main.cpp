#include "DocumentMerger.h"

#include <fstream>
#include <string>
#include <vector>

#define FILEEXT_COLLADA		"dae"
#define MAX_FILEPATH		512
#define VERSION_APP			"1.1"

//-----------------------------------------------------------------------------
// PURPOSE
//	Gets a list of Collada files in the specified directory.
// PARAMS
//	inputFilePaths - A pointer to the list of input file paths to populate.
//	inputDir - The directory to search for Collada files.
// RETURNS
//	True if there weren't any problems searching the directory, or false otherwise.
//-----------------------------------------------------------------------------
bool GetColladaFiles( std::vector<std::string>* inputFilePaths, const char* inputDir )
{
	// Construct a search pattern to search for only Collada files
	char searchPattern[ MAX_FILEPATH ];
	sprintf( searchPattern, "%s\\*.dae", inputDir );

	// Open the directory so we can list its contents
	WIN32_FIND_DATA ffd;
	HANDLE hFind = FindFirstFile( searchPattern, &ffd );
	if ( hFind == INVALID_HANDLE_VALUE )
	{
		rage::Errorf( "Failed to get the contents of directory '%s'. Check to make sure it exists.", inputDir );
		return false;
	}

	// Add each found Collada file to the list of input file paths
	do 
	{
		if ( !(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
		{
			std::string inputFilePath( inputDir );
			inputFilePath.append( "\\" );
			inputFilePath.append( ffd.cFileName );
			inputFilePaths->push_back( inputFilePath );
		}
	} while ( FindNextFile( hFind, &ffd) != 0 );

	// Cleanup
	FindClose( hFind );

	return true;
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Entry point into the application.
//-----------------------------------------------------------------------------
int main( int argc, char** argv )
{
	// Ensure the correct number of command line arguments were supplied
	if ( argc != 3 )
	{
		rage::Displayf( "USAGE: rageColladaMerger.exe <OutputFile> <InputDirectory>" );
		rage::Displayf( "OutputFile: The full path of the merged Collada file to be output." );
		rage::Displayf( "InputFile: The full path of a directory containing the Collada files to be merged." );
		return 0;
	}

	rage::Displayf( "RAGE Collada File Merger - Version %s", VERSION_APP );

	// Get the output file from the command line arguments
	const char* outputFilePath = argv[1];

	// Get all Collada files in the directory specified on the command line
	std::vector<std::string> inputFilePaths;
	if ( !GetColladaFiles(&inputFilePaths, argv[2]) )
	{
		rage::Errorf( "Aborting due to problems finding Collada files in the input directory.  No files were merged." );
		return -1;
	}

	// Initialize Collada
	FCollada::Initialize();
	FCollada::SetDereferenceFlag( false ); // Disable external reference finding
	rage::Displayf( "Collada Initialized - Version %d", FCollada::GetVersion() );
	rage::Displayf( "" );

	// Create a new document that all the input files will be merged into
	FCDocument* mergedDocument = new FCDocument();

	size_t totalErrorCount = 0;
	size_t totalWarningCount = 0;
	size_t inputFilePathCount = inputFilePaths.size();
	rage::Displayf( "Merging %d Collada files...", inputFilePathCount );

	// For each of the input documents
	for ( size_t i = 0; i < inputFilePathCount; ++i )
	{
		const char* inputFilePath = inputFilePaths[i].c_str();

		// Attempt to load the input Collada document
		FCDocument* inputDocument = new FCDocument();
		if ( FCollada::LoadDocumentFromFile(inputDocument, inputFilePath) )
		{
			// If the document was loaded, create the merger object
			DocumentMerger merger( mergedDocument, inputDocument );

			// Copy the asset block from the first document into the merged document
			if ( i == 0 )
			{
				merger.CopyAssetBlock();
			}

			// Merge it!
			merger.Merge();

			// Get the number of errors/warnings that occurred during this merger
			std::vector<std::string> errors = merger.GetErrors();
			std::vector<std::string> warnings = merger.GetWarnings();
			size_t errorCount = errors.size();
			size_t warningCount = warnings.size();

			// Display the errors/warnings (if any)
			if ( errorCount > 0 || warningCount > 0 )
			{
				rage::Displayf( "%d errors, %d warnings when merging file '%s':", errorCount, warningCount, inputFilePath );

				for ( size_t i = 0; i < errorCount; ++i )
				{
					rage::Errorf( errors[i].c_str() );
				}
				for ( size_t i = 0; i < warningCount; ++i )
				{
					rage::Warningf( warnings[i].c_str() );
				}
			}

			// Keep track of the total number of errors/warnings
			totalErrorCount += errorCount;
			totalWarningCount += warningCount;
		}
		else
		{
			rage::Errorf( "Failed to load Collada document '%s'. Check that it exists and is a valid Collada document.", inputFilePath );
		}
	}

	rage::Displayf( "Done merging files - %d Errors, %d Warnings", totalErrorCount, totalWarningCount );
	rage::Displayf( "" );

	// Save the merged Collada document
	rage::Displayf( "Saving merged Collada document..." );
	if ( !FCollada::SaveDocument(mergedDocument, outputFilePath) )
	{
		rage::Errorf( "Failed to save the merged Collada document.  Check that the file doesn't already exist and that you have proper write permission." );
		FCollada::Release();
		return -2;
	}

	// Shutdown Collada
	rage::Displayf( "Save successful!");
	FCollada::Release();

	return 0;
}
