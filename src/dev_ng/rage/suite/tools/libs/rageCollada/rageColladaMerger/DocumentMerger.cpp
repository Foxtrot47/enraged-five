#include "DocumentMerger.h"

#include "ImageLibraryMerger.h"
#include "EffectLibraryMerger.h"
#include "MaterialLibraryMerger.h"
#include "VisualSceneLibraryMerger.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
DocumentMerger::DocumentMerger( FCDocument* destDocument, FCDocument* sourceDocument )
{
	AssertMsg( destDocument!=NULL, "NULL value passed for destDocument in DocumentMerger::DocumentMerger()" );
	AssertMsg( sourceDocument!=NULL, "NULL value passed for sourceDocument in DocumentMerger::DocumentMerger()" );

	m_destDocument = destDocument;
	m_sourceDocument = sourceDocument;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
DocumentMerger::~DocumentMerger()
{

}

//-----------------------------------------------------------------------------
// PURPOSE
//	Copies the asset block from the source document to the destination document.
//-----------------------------------------------------------------------------
void DocumentMerger::CopyAssetBlock()
{
	// Get the source and destination asset blocks
	FCDAsset* destAsset = m_destDocument->GetAsset();
	FCDAsset* sourceAsset = m_sourceDocument->GetAsset();
	AssertMsg( destAsset!=NULL, "Failed to get destination asset block in DocumentMerger::CopyAssetBlock()" );
	AssertMsg( sourceAsset!=NULL, "Failed to get source asset block in DocumentMerger::CopyAssetBlock()" );

	// Copy the parts of the asset block that matter
	destAsset->SetUpAxis( sourceAsset->GetUpAxis() );
	destAsset->SetUnitName( sourceAsset->GetUnitName() );
	destAsset->SetUnitConversionFactor( sourceAsset->GetUnitConversionFactor() );
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Merges the source document into the destination document.
//-----------------------------------------------------------------------------
void DocumentMerger::Merge()
{
	// Determine if the documents can be merged
	if ( AreDocumentsCompatible() )
	{
		// If so, merge each of the supported libraries
		// NOTE: The following libraries are not currently supported:
		// FCDAnimationLibrary
		// FCDAnimationClipLibrary
		// FCDCameraLibrary
		// FCDLightLibrary

		ImageLibraryMerger imageMerger( m_destDocument->GetImageLibrary(), m_sourceDocument->GetImageLibrary() );
		imageMerger.Merge();
		CopyErrorsAndWarnings( imageMerger );

		EffectLibraryMerger effectMerger( m_destDocument->GetEffectLibrary(), m_sourceDocument->GetEffectLibrary() );
		effectMerger.Merge();
		CopyErrorsAndWarnings( effectMerger );

		MaterialLibraryMerger materialMerger( m_destDocument->GetMaterialLibrary(), m_sourceDocument->GetMaterialLibrary() );
		materialMerger.Merge();
		CopyErrorsAndWarnings( materialMerger );

		LibraryMerger<FCDGeometryLibrary*> geometryMerger( m_destDocument->GetGeometryLibrary(), m_sourceDocument->GetGeometryLibrary() );
		geometryMerger.Merge();
		CopyErrorsAndWarnings( geometryMerger );

		VisualSceneLibraryMerger visualSceneMerger( m_destDocument->GetVisualSceneLibrary(), m_sourceDocument->GetVisualSceneLibrary() );
		visualSceneMerger.Merge();
		CopyErrorsAndWarnings( visualSceneMerger );
	}
	else
	{
		Error( "This document is incompatible. It will not be merged." );
	}
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Determines if the destination and source documents can be merged.
// RETURNS
//	True if the documents are compatible, or false otherwise.
//-----------------------------------------------------------------------------
bool DocumentMerger::AreDocumentsCompatible()
{
	// Get the destination document's asset block
	const FCDAsset* destAsset = m_destDocument->GetAsset();
	AssertMsg( destAsset!=NULL, "Failed to get destination asset block in DocumentMerger::AreDocumentsCompatible()" );

	// Get the source document's asset block
	const FCDAsset* sourceAsset = m_sourceDocument->GetAsset();
	if ( sourceAsset != NULL )
	{
		// Compare the source and destination up-axes
		const FMVector3& destUpAxis = destAsset->GetUpAxis();
		const FMVector3& sourceUpAxis = sourceAsset->GetUpAxis();
		if ( destUpAxis.x != sourceUpAxis.x ||
			 destUpAxis.y != sourceUpAxis.y ||
			 destUpAxis.z != sourceUpAxis.z
		   )
		{
			Error( "Cannot merge this document because its up-axis does not match." );
			return false;
		}

		// Compare the source and destination units
		if ( destAsset->GetUnitName() != sourceAsset->GetUnitName() )
		{
			Error( "Cannot merge this document because its units do not match." );
			return false;
		}

		// Compare the source and destination unit conversion factors
		if ( destAsset->GetUnitConversionFactor() != sourceAsset->GetUnitConversionFactor() )
		{
			Error( "Cannot merge this document because its unit conversion factor does not match." );
			return false;
		}
	}
	else
	{
		// Not a terrible problem if the source document has no
		// asset block, but it's still something to tell the user
		Warning( "Could not find asset block in this document. It will be merged anyway, but the up-axis and units may not match." );
	}

	return true;
}
