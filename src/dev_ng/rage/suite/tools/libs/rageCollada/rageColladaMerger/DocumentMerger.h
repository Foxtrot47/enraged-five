#ifndef DOCUMENT_MERGER_H
#define DOCUMENT_MERGER_H

#include "Merger.h"

//-----------------------------------------------------------------------------
// PURPOSE
//	This class is used to merge two Collada documents together.
//-----------------------------------------------------------------------------
class DocumentMerger : public Merger
{
public:
	DocumentMerger( FCDocument* destDocument, FCDocument* sourceDocument );
	virtual ~DocumentMerger();

public:
	virtual void						CopyAssetBlock();
	virtual void						Merge();

protected:
	virtual bool						AreDocumentsCompatible();

protected:
	FCDocument*							m_destDocument;
	FCDocument*							m_sourceDocument;
};

#endif // #ifndef DOCUMENT_MERGER_H
