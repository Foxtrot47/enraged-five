#include "EffectLibraryMerger.h"

//-----------------------------------------------------------------------------
// PURPOSE
//	Overrides the base implementation to handle comparing two FCDEffect objects.
// NOTE
//	FCDEffect objects are considered identical if they both use the same textures.
//	This is admittedly a pretty dumb way to check if two effects are identical,
//	but it will have to do for now.
//-----------------------------------------------------------------------------
bool EffectLibraryMerger::AreEntitiesIdentical( const FCDEntity* entity1, const FCDEntity* entity2 ) const
{
	AssertMsg( entity1!=NULL, "NULL value passed for entity1 in EffectLibraryMerger::AreEntitiesIdentical()" );
	AssertMsg( entity2!=NULL, "NULL value passed for entity2 in EffectLibraryMerger::AreEntitiesIdentical()" );

	// Should be dealing with FCDEffect objects
	const FCDEffect* effect1 = dynamic_cast<const FCDEffect*>( entity1 );
	const FCDEffect* effect2 = dynamic_cast<const FCDEffect*>( entity2 );
	AssertMsg( effect1!=NULL, "Dynamic cast from FCDEntity to FCDEffect failed in EffectLibraryMerger::AreEntitiesIdentical()" );
	AssertMsg( effect2!=NULL, "Dynamic cast from FCDEntity to FCDEffect failed in EffectLibraryMerger::AreEntitiesIdentical()" );

	// Get a list of images used by each of the effects
	std::vector<const FCDImage*> effectImages1;
	std::vector<const FCDImage*> effectImages2;
	GetImagesFromEffect( &effectImages1, effect1 );
	GetImagesFromEffect( &effectImages2, effect2 );
	
	// Compare the lists of images
	if ( effectImages1.size() == effectImages2.size() )
	{
		size_t imageCount = effectImages1.size();
		for ( size_t i = 0; i < imageCount; ++i )
		{
			const FCDImage* image1 = effectImages1[i];
			const FCDImage* image2 = effectImages2[i];

			// Bail out if the images are not the same
			if ( image1->GetDaeId() != image2->GetDaeId() )
			{
				return false;
			}
		}

		// If all the images are the same, success!
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// PURPOSE
//	Gets a list of images used by the specified effects.
// PARAMS
//	images - The list of images to be populated.
//	effect - The effect to search for images.
//-----------------------------------------------------------------------------
void EffectLibraryMerger::GetImagesFromEffect( std::vector<const FCDImage*>* images, const FCDEffect* effect )
{
	AssertMsg( images!=NULL, "NULL value passed for images in EffectLibraryMerger::GetImagesFromEffect()" );
	AssertMsg( effect!=NULL, "NULL value passed for effect in EffectLibraryMerger::GetImagesFromEffect()" );

	// For each of the effect's profiles
	size_t profileCount = effect->GetProfileCount();
	for ( size_t i = 0; i < profileCount; ++i )
	{
		const FCDEffectProfile* profile = effect->GetProfile( i );
		AssertMsg( profile!=NULL, "Failed to look up effect profile in EffectLibraryMerger::GetImagesFromEffect()" );

		// For each of the profiles parameters
		size_t paramCount = profile->GetEffectParameterCount();
		for ( size_t j = 0; j < paramCount; ++j )
		{
			const FCDEffectParameter* effectParam = profile->GetEffectParameter( j );
			AssertMsg( effectParam!=NULL, "Failed to look up effect parameter in EffectLibraryMerger::GetImagesFromEffect()" );

			// If this is a surface effect parameter
			if ( effectParam->GetType() == FCDEffectParameter::SURFACE )
			{
				// Should be able to cast it to its actual type then
				const FCDEffectParameterSurface* surfaceParam = dynamic_cast<const FCDEffectParameterSurface*>( effectParam );
				AssertMsg( surfaceParam!=NULL, "Dynamic cast from FCDEffectParameter to FCDEffectParameterSurface failed in EffectLibraryMerger::GetImagesFromEffect()" );

				// For each of the surface parameter's images
				size_t imageCount = surfaceParam->GetImageCount();
				for ( size_t k = 0; k < imageCount; ++k )
				{
					const FCDImage* image = surfaceParam->GetImage( k );
					AssertMsg( image!=NULL, "Failed to look up effect image in EffectLibraryMerger::GetImagesFromEffect()" );

					// Add the image to the output list
					images->push_back( image );
				}
			}
		}
	}
}
