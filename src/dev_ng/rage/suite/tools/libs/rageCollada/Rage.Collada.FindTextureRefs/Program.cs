using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using OrganicBit.Zip;

using rageUsefulCSharpToolClasses;

namespace Rage.Collada.FindTextureRefs
{
	/// <summary>
	/// Intermediate struct for storing texture information.
	/// </summary>
	class TextureInfo
	{
		private string _path;

		public string Path
		{
			get { return _path; }
			set
			{
				_path = value;
				_path = _path.Replace("file:///", "");
				_path = _path.Replace("/", "\\");
				_path = rageFileUtilities.GetRelativePath(_path, Directory.GetCurrentDirectory());
			}
		}
	}

	class Program
	{
		/// <summary>
		/// Entry point for the application.
		/// </summary>
		static int Main(string[] args)
        {
            // Make sure we have the right command line arguments
            if(( args.Length != 2 ) && ( args.Length != 3 ))
            {
                Console.WriteLine("Usage: <input file> <output file> [mtl list output file]");
                return -1;
            }

            // Read in the command line arguments
            string inputFile = args[0];
            string outputFile = args[1];
			string mtlOutputFile = null;
			if (args.Length == 3)
			{
				mtlOutputFile = args[2];
			}

            try
            {
                // Get a list of textures in the input file
				List<string> astrMtlList = new List<string>();
				List<TextureInfo> aobTextureList = new List<TextureInfo>();
				FindTexturesInColladaFile(inputFile, ref aobTextureList, ref astrMtlList);

				if (outputFile.ToLower().EndsWith(".xml"))
				{
					try
					{
						// Write the list of textures to the output file
						WriteTextureListToXML(outputFile, aobTextureList);
					}
					catch (Exception e)
					{
						Console.WriteLine("Error writing output file: " + e.Message);
						return -1;
					}
				}
				else
				{
					try
					{
						// Write the list of textures to the output file
						WriteTextureListToTXT(outputFile, aobTextureList);
					}
					catch (Exception e)
					{
						Console.WriteLine("Error writing output file: " + e.Message);
						return -1;
					}
				}

				if (mtlOutputFile != null)
				{
					if (mtlOutputFile.ToLower().EndsWith(".xml"))
					{
						try
						{
							// Write the list of textures to the output file
							WriteMtlListToXML(mtlOutputFile, astrMtlList);
						}
						catch (Exception e)
						{
							Console.WriteLine("Error writing output file: " + e.Message);
							return -1;
						}
					}
					else
					{
						try
						{
							// Write the list of textures to the output file
							WriteMtlListToTXT(mtlOutputFile, astrMtlList);
						}
						catch (Exception e)
						{
							Console.WriteLine("Error writing output file: " + e.Message);
							return -1;
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Error reading input file: " + e.Message);
				return -1;
			}
			return 0;
        }

		static bool LoadDocument(ref XmlDocument document, string filename)
		{
			// Does the filename end with dae?
			if (filename.ToLower().EndsWith(".dae"))
			{
				// Given a dae, so just save normally
				document.Load(filename);
				return true;
			}
			else if (filename.ToLower().EndsWith(".zip"))
			{
				// Open the zip file
				ZipReader reader = new ZipReader(filename);

				reader.Reset();
				ZipEntry obColladaFile = null;
				do
				{
					reader.MoveNext();
					obColladaFile = reader.Current;
					if (obColladaFile == null)
					{
						Console.Error.WriteLine("Error occurred locating any dae files in zipfile " + filename);
						return false;
					}
				} while (!obColladaFile.Name.ToLower().EndsWith(".dae"));

				// Got dae, so get data out of it
				int iDataSize = (int)obColladaFile.Length;
				byte[] managedData = new byte[iDataSize];
				reader.Read(managedData, 0, iDataSize);
				reader.Close();

				// Turn data into a string
				string strData = System.Text.Encoding.ASCII.GetString(managedData, 0, iDataSize);

				// Turn that string into an xml doc
				document.LoadXml(strData);

				return true;
			}
			else
			{
				Console.Error.WriteLine("Unknown fileextension while loading " + filename);
				return false;
			}
		}

		/// <summary>
		/// Parses a Collada file and returns a list of textures in the file.
		/// </summary>
		static void FindTexturesInColladaFile(string colladaFile, ref List<TextureInfo> aobTextureList, ref List<string> astrMtlList)
		{
			// Open the Collada file for reading
			XmlDocument doc = new XmlDocument();
			LoadDocument(ref doc, colladaFile);

			// Setup the Collada namespace
			XmlNamespaceManager namespaceMgr = new XmlNamespaceManager(doc.NameTable);
			namespaceMgr.AddNamespace("x", "http://www.collada.org/2005/11/COLLADASchema");

			// Get all the textures referenced by this file
			string xpath = "/x:COLLADA/x:library_images/x:image/x:init_from";
			XmlNodeList nodeList = doc.SelectNodes(xpath, namespaceMgr);

			// Add these textures to the list
			foreach (XmlNode node in nodeList)
			{
				TextureInfo newTextureInfo = new TextureInfo();
				newTextureInfo.Path = node.InnerText;
				aobTextureList.Add(newTextureInfo);
				// TODO: Add new texture information
			}

			// Get all the material files referenced by this file
			xpath = "/x:COLLADA/x:library_effects/x:effect/x:extra/x:technique[@profile=\"RAGE\"]/x:rageShaderMaterial/@path";
			nodeList = doc.SelectNodes(xpath, namespaceMgr);

			// Add all textures referenced by each material file to the list
			foreach (XmlNode node in nodeList)
			{
				string materialFile = node.Value;
				if (materialFile.ToLower().EndsWith(".mtlgeo"))
				{
					astrMtlList.Add(materialFile.ToLower().Substring(8));
					try
					{
						List<TextureInfo> materialTextureList = FindTexturesInMtlFile(materialFile);
						aobTextureList.AddRange(materialTextureList);
					}
					catch (Exception e)
					{
						throw new Exception("Error occurred reading " + materialFile +" : "+ e.Message);
					}
				}
			}
		}

		/// <summary>
		/// Parses a material file and returns a list of textures in the file.
		/// </summary>
		static List<TextureInfo> FindTexturesInMtlFile(string materialFile)
		{
			List<TextureInfo> textureList = new List<TextureInfo>();

			// Open the material file for reading
			XmlDocument doc = new XmlDocument();
			doc.Load(materialFile);

			// Get all the texture images referenced by this file
			string xpath = "//filename";
			XmlNodeList nodeList = doc.SelectNodes(xpath);

			// Add these textures to the list
			foreach (XmlNode node in nodeList)
			{
				if (node.InnerText != "none")
				{
					TextureInfo newTextureInfo = new TextureInfo();
					newTextureInfo.Path = node.InnerText;
					textureList.Add(newTextureInfo);
				}
			}

			return textureList;
		}

		/// <summary>
		/// Writes a list of textures to an XML file.
		/// </summary>
		static void WriteTextureListToXML(string xmlFile, List<TextureInfo> textureList)
		{
			rageFileUtilities.MakeDir(rageFileUtilities.GetLocationFromPath(xmlFile));

			// Open the XML file for writing
			XmlTextWriter writer = new XmlTextWriter(xmlFile, Encoding.Default);

			try
			{
				// Make the formatting all nice and pretty
				writer.Formatting = Formatting.Indented;

				// Write the texture list to the file
				writer.WriteStartElement("root");
				foreach (TextureInfo textureInfo in textureList)
				{
					writer.WriteStartElement("texture");

					writer.WriteStartElement("path");
					writer.WriteString(textureInfo.Path);
					writer.WriteEndElement();

					writer.WriteEndElement();
				}
				writer.WriteEndElement();

				// We're done writing, close the file
				writer.Close();
			}
			catch (Exception e)
			{
				writer.Close();
				throw new Exception(e.Message);
			}
		}

		/// <summary>
		/// Writes a list of textures to an TXT file.
		/// </summary>
		static void WriteTextureListToTXT(string txtFile, List<TextureInfo> textureList)
		{
			rageFileUtilities.MakeDir(rageFileUtilities.GetLocationFromPath(txtFile));

			// Open the TXT file for writing
			StreamWriter sw = File.CreateText(txtFile);

			try
			{
				// Write the texture list to the file
				foreach (TextureInfo textureInfo in textureList)
				{
					sw.WriteLine(textureInfo.Path);
				}

				// We're done writing, close the file
				sw.Close();
			}
			catch (Exception e)
			{
				sw.Close();
				throw new Exception(e.Message);
			}
		}



		/// <summary>
		/// Writes a list of mtls to an XML file.
		/// </summary>
		static void WriteMtlListToXML(string xmlFile, List<string> mtlList)
		{
			rageFileUtilities.MakeDir(rageFileUtilities.GetLocationFromPath(xmlFile));

			// Open the XML file for writing
			XmlTextWriter writer = new XmlTextWriter(xmlFile, Encoding.Default);

			try
			{
				// Make the formatting all nice and pretty
				writer.Formatting = Formatting.Indented;

				// Write the mtl list to the file
				writer.WriteStartElement("root");
				foreach (string mtlInfo in mtlList)
				{
					writer.WriteStartElement("mtl");

					writer.WriteStartElement("path");
					writer.WriteString(mtlInfo);
					writer.WriteEndElement();

					writer.WriteEndElement();
				}
				writer.WriteEndElement();

				// We're done writing, close the file
				writer.Close();
			}
			catch (Exception e)
			{
				writer.Close();
				throw new Exception(e.Message);
			}
		}

		/// <summary>
		/// Writes a list of mtls to an TXT file.
		/// </summary>
		static void WriteMtlListToTXT(string txtFile, List<string> mtlList)
		{
			rageFileUtilities.MakeDir(rageFileUtilities.GetLocationFromPath(txtFile));

			// Open the TXT file for writing
			StreamWriter sw = File.CreateText(txtFile);

			try
			{
				// Write the mtl list to the file
				foreach (string mtlInfo in mtlList)
				{
					sw.WriteLine(mtlInfo);
				}

				// We're done writing, close the file
				sw.Close();
			}
			catch (Exception e)
			{
				sw.Close();
				throw new Exception(e.Message);
			}
		}
	}
}