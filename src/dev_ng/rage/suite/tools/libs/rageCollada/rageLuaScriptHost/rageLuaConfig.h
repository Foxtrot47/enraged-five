#ifndef __RAGE_LUA_CONFIG_H__
#define __RAGE_LUA_CONFIG_H__

extern "C"
{
    #include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

namespace rage
{
	//////////////////////////////////////////////////////////////////////////

	class rageLuaConfigState
	{
	public:
		template < typename T >
		T Get( const char* key ) const;
	private:	
		rageLuaConfigState( lua_State* pLuaState );
	private:
		lua_State* m_pLuaState;

		friend class rageLuaConfig;
	};

	inline rageLuaConfigState::rageLuaConfigState( lua_State* pLuaState )
		:	m_pLuaState ( pLuaState ) { }

	template < >
	const char* rageLuaConfigState::Get( const char* key ) const;

	template < >
	float rageLuaConfigState::Get( const char* key ) const;

	template < >
	bool rageLuaConfigState::Get( const char* key ) const;

	template < >
	int rageLuaConfigState::Get( const char* key ) const;

	//////////////////////////////////////////////////////////////////////////

	template < typename T >
	class rageLuaConfigData
	{
	public:
		int GetCount() const;
		T* GetData() const;
	public:
		rageLuaConfigData( const char* key );
		~rageLuaConfigData();
	private:
		T* m_pData;
		int m_Size;
		int m_Count;

		const char* m_Key;

		friend class rageLuaConfig;
	};

	template < typename T >
	rageLuaConfigData< T >::rageLuaConfigData( const char* key )
		:	m_pData ( NULL )
		,	m_Size ( 0 )
		,	m_Count ( 0 )
		,	m_Key ( key ) { }

	template < typename T >
	inline rageLuaConfigData< T >::~rageLuaConfigData()
	{
		delete[] m_pData;
	}

	template < typename T >
	inline int rageLuaConfigData< T >::GetCount() const
	{
		return m_Count;
	}

	template < typename T >
	inline T* rageLuaConfigData< T >::GetData() const
	{
		return m_pData;
	}

	//////////////////////////////////////////////////////////////////////////

	class rageLuaConfig 
	{
	public:
		rageLuaConfig( const char* filename );
	public:
		void SetGlobal( const char* key, const char* value );
		void SetGlobal( const char* key, float value );
		void SetGlobal( const char* key, bool value );
		void SetGlobal( const char* key, int value );

		template < typename T >
		bool Get( rageLuaConfigData< T >& data, int size = 8 ) const;

		template < typename T >
		T GetGlobal( const char* key ) const;
	private:
		lua_State* m_pLuaState;
	};

	template < typename T >
	bool rageLuaConfig::Get( rageLuaConfigData< T >& data, int size ) const
	{
		Assert(  0 == data.m_Count && NULL == data.m_pData );
		Assert( 0 != size );

		data.m_Size = size;
		data.m_pData = new T[ data.m_Size ];

		lua_getglobal( m_pLuaState, data.m_Key );
		lua_pushnil( m_pLuaState );
		while ( 0 != lua_next( m_pLuaState, -2 ) )
		{
			int idx = int ( lua_tonumber( m_pLuaState, -2 ) );
			--idx;

			if ( idx >= data.m_Size )
			{
				T* pTemp = data.m_pData;
				data.m_pData = new T[ data.m_Size * 2 ];
				memcpy( data.m_pData, pTemp, sizeof( T ) * data.m_Size );
				delete[] pTemp;

				data.m_Size *= 2;
			}

			rageLuaConfigState state ( m_pLuaState );
			Serialize( state, data.m_pData[ idx ] );
			++data.m_Count;

			lua_pop( m_pLuaState, 1 );
		}

		return true;
	}

	template < >
	const char* rageLuaConfig::GetGlobal( const char* key ) const;

	template < >
	float rageLuaConfig::GetGlobal( const char* key ) const;

	template < >
	bool rageLuaConfig::GetGlobal( const char* key ) const;

	template < >
	int rageLuaConfig::GetGlobal( const char* key ) const;
}

#endif // __RAGE_LUA_CONFIG_H__
