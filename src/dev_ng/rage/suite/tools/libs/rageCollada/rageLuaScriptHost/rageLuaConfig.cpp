#include "rageLuaConfig.h"

namespace rage
{
	template < >
	const char* rageLuaConfigState::Get( const char* key ) const
	{
		if ( lua_istable( m_pLuaState, -1 ) )
		{
			lua_pushstring( m_pLuaState, key );
			lua_gettable( m_pLuaState, -2 );

			if ( lua_isfunction( m_pLuaState, -1 ) )
			{
				lua_call( m_pLuaState, 0, LUA_MULTRET );
			}

			const char* ret = lua_tostring( m_pLuaState, -1 );
			lua_pop( m_pLuaState, 1 );

			return ret;
		}

		return NULL;
	}

	template < >
	float rageLuaConfigState::Get( const char* key ) const
	{
		if ( lua_istable( m_pLuaState, -1 ) )
		{
			lua_pushstring( m_pLuaState, key );
			lua_gettable( m_pLuaState, -2 );

			if ( lua_isfunction( m_pLuaState, -1 ) )
			{
				lua_call( m_pLuaState, 0, LUA_MULTRET );
			}

			float ret = float( lua_tonumber( m_pLuaState, -1 ) );
			lua_pop( m_pLuaState, 1 );

			return ret;
		}

		return 0.f;
	}

	template < >
	bool rageLuaConfigState::Get( const char* key ) const
	{
		if ( lua_istable( m_pLuaState, -1 ) )
		{
			lua_pushstring( m_pLuaState, key );
			lua_gettable( m_pLuaState, -2 );

			if ( lua_isfunction( m_pLuaState, -1 ) )
			{
				lua_call( m_pLuaState, 0, LUA_MULTRET );
			}

			bool ret = ( lua_toboolean( m_pLuaState, -1 ) ? true : false );
			lua_pop( m_pLuaState, 1 );

			return ret;
		}

		return false;
	}

	template < >
	int rageLuaConfigState::Get( const char* key ) const
	{
		if ( lua_istable( m_pLuaState, -1 ) )
		{
			lua_pushstring( m_pLuaState, key );
			lua_gettable( m_pLuaState, -2 );

			if ( lua_isfunction( m_pLuaState, -1 ) )
			{
				lua_call( m_pLuaState, 0, LUA_MULTRET );
			}

			int ret = int( lua_tointeger( m_pLuaState, -1 ) );
			lua_pop( m_pLuaState, 1 );

			return ret;
		}

		return 0;
	}

	rageLuaConfig::rageLuaConfig( const char* filename )
	{
		m_pLuaState = lua_open();
		Assert( NULL != m_pLuaState );

		luaL_openlibs( m_pLuaState );

		if ( luaL_loadfile( m_pLuaState, filename ) || 
			 lua_pcall( m_pLuaState, 0, 0, 0 ) )
		{
			printf( "%s\n", lua_tostring( m_pLuaState, -1 ) );
		}
	}

	void rageLuaConfig::SetGlobal( const char* key, const char* value )
	{
		lua_pushstring( m_pLuaState, value );
		lua_setglobal( m_pLuaState, key );
	}

	void rageLuaConfig::SetGlobal( const char* key, float value )
	{
		lua_pushnumber( m_pLuaState, value );
		lua_setglobal( m_pLuaState, key );
	}

	void rageLuaConfig::SetGlobal( const char* key, bool value )
	{
		lua_pushboolean( m_pLuaState, value );
		lua_setglobal( m_pLuaState, key );
	}

	void rageLuaConfig::SetGlobal( const char* key, int value )
	{
		lua_pushinteger( m_pLuaState, value );
		lua_setglobal( m_pLuaState, key );
	}

	template < >
	const char* rageLuaConfig::GetGlobal( const char* key ) const
	{
		lua_getglobal( m_pLuaState, key );
		return lua_tostring( m_pLuaState, -1 );
	}

	template < >
	float rageLuaConfig::GetGlobal( const char* key ) const
	{
		lua_getglobal( m_pLuaState, key );
		return float( lua_tonumber( m_pLuaState, -1 ) );
	}

	template < >
	bool rageLuaConfig::GetGlobal( const char* key ) const
	{
		lua_getglobal( m_pLuaState, key );
		return lua_toboolean( m_pLuaState, -1 ) ? true : false; 
	}

	template < >
	int rageLuaConfig::GetGlobal( const char* key ) const
	{
		lua_getglobal( m_pLuaState, key );
		return int( lua_tointeger( m_pLuaState, -1 ) );
	}
}
