//rageLuaScriptHost.cpp

#include <algorithm>

#include "rageLuaScriptHost.h"

using namespace std;

namespace rage
{

//-----------------------------------------------------------------------------

static int rageDisplay(lua_State *L)
{
	const char *dispStr = luaL_checkstring(L,1); //Read the string param
	Displayf("%s", dispStr);
	return 1;
}

static int rageWarning(lua_State *L)
{
	const char *dispStr = luaL_checkstring(L,1); //Read the string param
	Warningf("%s", dispStr);
	return 1;
}


static int rageError(lua_State *L)
{
	const char *dispStr = luaL_checkstring(L,1); //Read the string param
	Errorf("%s", dispStr);
	return 1;
}


static const luaL_reg rageOutputLib[] = 
{
	{"Display", rageDisplay},
	{"Warning", rageWarning},
	{"Error", rageError},
	{ NULL, NULL }
};

//-----------------------------------------------------------------------------

rageLuaScriptHost::rageLuaScriptHost()
{
	m_pLuaState = lua_open();
	Assert(m_pLuaState);

	luaL_openlibs(m_pLuaState);

	luaL_openlib(m_pLuaState, "rage", rageOutputLib, 0);
}

//-----------------------------------------------------------------------------

rageLuaScriptHost::~rageLuaScriptHost()
{
	//lua_close(m_pLuaState);

	for(vector<HMODULE>::iterator it = m_extRegistry.begin(); it != m_extRegistry.end(); ++it)
	{
		if(!FreeLibrary( *it ))
		{
			LPVOID lpMsgBuf;
			DWORD dwError = GetLastError();
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
					dwError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );
			Errorf("Failed to unload module extension, error code %d : %s", dwError,lpMsgBuf);
			LocalFree( lpMsgBuf );
		}
	}

	m_pLuaState = NULL;
}

//-----------------------------------------------------------------------------

void rageLuaScriptHost::ReportLuaError()
{
	Assertf(m_pLuaState, "LUA state is currently NULL");
	Errorf("LUA -> %s", lua_tostring(m_pLuaState, -1));
	lua_pop(m_pLuaState, 1);
}

//-----------------------------------------------------------------------------

bool rageLuaScriptHost::LoadAndExecuteScript(const char* szPath)
{
	Assertf(m_pLuaState, "LUA state is currently NULL");

	if( find(m_loadedScripts.begin(), m_loadedScripts.end(), string(szPath)) == m_loadedScripts.end() )
	{
		//The script has not already been loaded, so load it...
		if(luaL_loadfile(m_pLuaState, szPath))
		{
			ReportLuaError();
			return false;
		}
		
		if(lua_pcall(m_pLuaState, 0, 0, 0))
		{
			ReportLuaError();
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rageLuaScriptHost::LoadExtension(const char* szPath, const char* szInitFn, const char* szConvertFn)
{
	Assertf(m_pLuaState, "LUA state is currently NULL");
	Assert(szPath);
	Assert(szInitFn);

	//char cmdBuffer[1024];
	//sprintf_s(cmdBuffer,1024,"package.loadlib(\"%s\",\"%s\")();", szPath, szInitFn);
	//luaL_dostring(m_pLuaState, cmdBuffer);
	//string err = lua_tostring(m_pLuaState, -1);
	//Errorf("%s", err.c_str());

	HMODULE hMod = LoadLibrary(szPath);
	if(!hMod)
	{
		LPVOID lpMsgBuf;
		DWORD dwError = GetLastError();
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
				dwError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );
		Errorf("Failed to load extension file '%s' error code %d : %s",szPath,dwError,lpMsgBuf);
		LocalFree( lpMsgBuf );
		return false;
	}

	lua_CFunction pInitFn = reinterpret_cast<lua_CFunction>(GetProcAddress(hMod, szInitFn));
	if(!pInitFn)
	{
		LPVOID lpMsgBuf;
		DWORD dwError = GetLastError();
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
				dwError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );
		Errorf("Failed to locate initialization function '%s' in file '%s' error code %d : %s",szInitFn,szPath, dwError,lpMsgBuf);
		LocalFree( lpMsgBuf );
		return false;
	}

	m_pConvertFn = NULL;

	if ( NULL != szConvertFn )
	{
		m_pConvertFn = reinterpret_cast< ConvertFunction >( GetProcAddress( hMod, szConvertFn ) );
		if ( !m_pConvertFn )
		{
			LPVOID lpMsgBuf;
			DWORD dwError = GetLastError();
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
				dwError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );
			Errorf("Failed to locate conversion function '%s' in file '%s' error code %d : %s",szConvertFn,szPath, dwError,lpMsgBuf);
			LocalFree( lpMsgBuf );
			return false;
		}
	}

	int iResult = pInitFn(m_pLuaState);

	m_extRegistry.push_back(hMod);

	return true;
}

//-----------------------------------------------------------------------------

}//End namespace rage
