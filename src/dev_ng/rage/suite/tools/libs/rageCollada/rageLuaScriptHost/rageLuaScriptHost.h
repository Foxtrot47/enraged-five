// rageLuaScriptHost.h

#ifndef __RAGE_LUA_SCRIPT_HOST_H__
#define __RAGE_LUA_SCRIPT_HOST_H__

#pragma warning(push)
#pragma warning(disable:4668)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#pragma warning(pop)

#include <vector>
#include <string>
#include <map>

extern "C"
{
    #include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

#include "atl/singleton.h"
#include "atl/functor.h"
#include "diag/output.h"

using namespace rage;
using namespace std;


namespace rage
{

//-----------------------------------------------------------------------------

class rageLuaScriptHost
{
public:
	virtual ~rageLuaScriptHost();

	bool	LoadExtension(const char* szPath, const char* szInitFn, const char* szConvertFn);
	bool	LoadAndExecuteScript(const char* szPath);

	lua_State*	GetState() const { return m_pLuaState; }

	int		Convert( int index, void** ptr ) const { return m_pConvertFn( m_pLuaState, index, ptr ); }
protected:
	rageLuaScriptHost();

	void	ReportLuaError();

protected:
	lua_State*			m_pLuaState;
	vector<HMODULE>		m_extRegistry;
	vector<string>		m_loadedScripts;

	typedef int (*ConvertFunction) (lua_State*, int, void** );
	ConvertFunction		m_pConvertFn;
};

typedef atSingleton<rageLuaScriptHost> rageLuaScriptHostSingleton;

#define LUASCRIPTHOST				::rageLuaScriptHostSingleton::InstanceRef()
#define INIT_LUASCRIPTHOST			::rageLuaScriptHostSingleton::Instantiate()
#define SHUTDOWN_LUASCRIPTHOST		::rageLuaScriptHostSingleton::Destroy()

//-----------------------------------------------------------------------------

template < typename T1, typename T2 >
class rageLshPair
{
};

class rageLshNullT
{
};

class rageLshSwigObjWrap
{
public:
	rageLshSwigObjWrap(const char* szModuleName, const char* szCtorName, void* pInstance)
		: m_moduleName(szModuleName)
		, m_swigCtorName(szCtorName)
		, m_pInstance(pInstance)
	{};

	const char* m_moduleName;
	const char* m_swigCtorName;
	void* m_pInstance;
};

class rageLuaScriptHostCallBase
{
public:
	rageLuaScriptHostCallBase(const char* szFnName)
	{
		Assert(rageLuaScriptHostSingleton::IsInstantiated());
		lua_getglobal(LUASCRIPTHOST.GetState(), szFnName);
	}
protected:
	void	PushParam(const int& value) { lua_pushinteger(LUASCRIPTHOST.GetState(), value); }
	void	PushParam(const float& value) { lua_pushnumber(LUASCRIPTHOST.GetState(), value); }
	void	PushParam(const double& value) { lua_pushnumber(LUASCRIPTHOST.GetState(), value); }
	void	PushParam(const char* value) { lua_pushstring(LUASCRIPTHOST.GetState(), value); }
	void	PushParam(const string& value) { lua_pushstring(LUASCRIPTHOST.GetState(), value.c_str()); }
	
	void PushParam(const rageLshSwigObjWrap& obj) 
	{	
		lua_getglobal(LUASCRIPTHOST.GetState(), obj.m_moduleName);
		lua_pushstring(LUASCRIPTHOST.GetState(), obj.m_swigCtorName);
		lua_gettable(LUASCRIPTHOST.GetState(), -2);
		lua_remove(LUASCRIPTHOST.GetState(), -2);
		lua_pushlightuserdata(LUASCRIPTHOST.GetState(), obj.m_pInstance);
		lua_call(LUASCRIPTHOST.GetState(),1,1);
	}

	void	GetResult(int& value) { value = lua_tointeger(LUASCRIPTHOST.GetState(), -1); }
	void	GetResult(float& value) { value = (float)lua_tonumber(LUASCRIPTHOST.GetState(), -1); }
	void	GetResult(double& value) { value = lua_tonumber(LUASCRIPTHOST.GetState(), -1); }
	void	GetResult(string& value) { value = (char*)lua_tostring(LUASCRIPTHOST.GetState(), -1); }
	void	GetResult(rageLshNullT& value) { };

	template < class T >
	void	GetResult( T*& value ) 
	{ 
		T* arg;
		LUASCRIPTHOST.Convert( -1, (void**)&arg );

		value = arg;
	}
};

//-----------------------------------------------------------------------------

template< typename TR, 
		  typename P1 = rageLshNullT, typename P2 = rageLshNullT, typename P3 = rageLshNullT,  
		  typename P4 = rageLshNullT, typename P5 = rageLshNullT, typename P6 = rageLshNullT >
class rageLuaScriptHostCall
	: public rageLshPair<TR, typename rageLuaScriptHostCall<P1,P2,P3,P4,P5,P6,rageLshNullT> >
	, public rageLuaScriptHostCallBase
{
public:
	rageLuaScriptHostCall(const char* szFnName)
		: rageLuaScriptHostCallBase(szFnName)
	{
	}

	TR call(P1 a1, P2 a2, P3 a3, P4 a4, P5 a5, P6 a6)
	{
		TR returnVal;
		PushParam(a1);
		PushParam(a2);
		PushParam(a3);
		PushParam(a4);
		PushParam(a5);
		PushParam(a6);

		if(lua_pcall(LUASCRIPTHOST.GetState(),6,1,0) != 0)
		{
			Errorf("LUA -> %s", lua_tostring(LUASCRIPTHOST.GetState(), -1));
			Assert(0);
		}
		GetResult(returnVal);
		return returnVal;
	}
};

//-----------------------------------------------------------------------------

template< typename TR, typename P1, typename P2, typename P3, typename P4, typename P5>
class rageLuaScriptHostCall<TR, P1, P2, P3, P4, P5, rageLshNullT>
	: public rageLshPair<TR, P1>
	, public rageLuaScriptHostCallBase
{
public:
	rageLuaScriptHostCall(const char* szFnName)
		: rageLuaScriptHostCallBase(szFnName)
	{
	}

	TR call(P1 a1, P2 a2, P3 a3, P4 a4, P5 a5)
	{
		TR returnVal;
		PushParam(a1);
		PushParam(a2);
		PushParam(a3);
		PushParam(a4);
		PushParam(a5);
	
		if(lua_pcall(LUASCRIPTHOST.GetState(),5,1,0) != 0)
		{
			Errorf("LUA -> %s", lua_tostring(LUASCRIPTHOST.GetState(), -1));
			Assert(0);
		}
		GetResult(returnVal);
		return returnVal;
	}
};

//-----------------------------------------------------------------------------

template< typename TR, typename P1, typename P2, typename P3, typename P4>
class rageLuaScriptHostCall<TR, P1, P2, P3, P4, rageLshNullT>
	: public rageLshPair<TR, P1>
	, public rageLuaScriptHostCallBase
{
public:
	rageLuaScriptHostCall(const char* szFnName)
		: rageLuaScriptHostCallBase(szFnName)
	{
	}

	TR call(P1 a1, P2 a2, P3 a3, P4 a4)
	{
		TR returnVal;
		PushParam(a1);
		PushParam(a2);
		PushParam(a3);
		PushParam(a4);
		
		if(lua_pcall(LUASCRIPTHOST.GetState(),4,1,0) != 0)
		{
			Errorf("LUA -> %s", lua_tostring(LUASCRIPTHOST.GetState(), -1));
			Assert(0);
		}
		GetResult(returnVal);
		return returnVal;
	}
};

//-----------------------------------------------------------------------------

template< typename TR, typename P1, typename P2, typename P3>
class rageLuaScriptHostCall<TR, P1, P2, P3, rageLshNullT>
	: public rageLshPair<TR, P1>
	, public rageLuaScriptHostCallBase
{
public:
	rageLuaScriptHostCall(const char* szFnName)
		: rageLuaScriptHostCallBase(szFnName)
	{
	}

	TR call(P1 a1, P2 a2, P3 a3)
	{
		TR returnVal;
		PushParam(a1);
		PushParam(a2);
		PushParam(a3);
		
		if(lua_pcall(LUASCRIPTHOST.GetState(),3,1,0) != 0)
		{
			Errorf("LUA -> %s", lua_tostring(LUASCRIPTHOST.GetState(), -1));
			Assert(0);
		}
		GetResult(returnVal);
		return returnVal;
	}
};

//-----------------------------------------------------------------------------

template< typename TR, typename P1, typename P2>
class rageLuaScriptHostCall<TR, P1, P2, rageLshNullT>
	: public rageLshPair<TR, P1>
	, public rageLuaScriptHostCallBase
{
public:
	rageLuaScriptHostCall(const char* szFnName)
		: rageLuaScriptHostCallBase(szFnName)
	{
	}

	TR call(P1 a1, P2 a2)
	{
		TR returnVal;
		PushParam(a1);
		PushParam(a2);
		
		if(lua_pcall(LUASCRIPTHOST.GetState(),2,1,0) != 0)
		{
			Errorf("LUA -> %s", lua_tostring(LUASCRIPTHOST.GetState(), -1));
			Assert(0);
		}
		GetResult(returnVal);
		return returnVal;
	}
};

//-----------------------------------------------------------------------------

template< typename TR, typename P1>
class rageLuaScriptHostCall<TR, P1, rageLshNullT >
	: public rageLshPair<TR, P1 >
	, public rageLuaScriptHostCallBase
{
public:
	rageLuaScriptHostCall(const char* szFnName)
		: rageLuaScriptHostCallBase(szFnName)
	{
	}

	TR call(P1 a1)
	{
		TR returnVal;
		PushParam(a1);
		if(lua_pcall(LUASCRIPTHOST.GetState(),1,1,0) != 0)
		{
			Errorf("LUA -> %s", lua_tostring(LUASCRIPTHOST.GetState(), -1));
			Assert(0);
		}
		GetResult(returnVal);
		return returnVal;
	}
};

//-----------------------------------------------------------------------------

template< typename TR >
class rageLuaScriptHostCall<TR, rageLshNullT >
	: public rageLshPair<TR, rageLshNullT >
	, public rageLuaScriptHostCallBase
{
public:
	rageLuaScriptHostCall(const char* szFnName)
		: rageLuaScriptHostCallBase(szFnName)
	{
	}

	TR call(void)
	{
		TR returnVal;

		if(lua_pcall(LUASCRIPTHOST.GetState(),1,1,0) != 0)
		{
			Errorf("LUA -> %s", lua_tostring(LUASCRIPTHOST.GetState(), -1));
			Assert(0);
		}
		GetResult(returnVal);
		return returnVal;
	}
};

//-----------------------------------------------------------------------------

} // end namespace rage

#endif //__RAGE_LUA_SCRIPT_HOST_H__
