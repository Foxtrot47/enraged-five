
-------------------------------------------------------------------------------

function IsNodeGeometryNode( sceneNode )
	
	numInstances = sceneNode:GetInstanceCount()
	if( numInstances > 0 ) then
		i = 0
		while i < numInstances do
			inst = sceneNode:GetInstance(i)
			if ( inst:GetEntityType() == FCollada.FCDEntity_GEOMETRY ) then
				return 1
			end
			i = i + 1
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function HasParentNodeNamedRec ( sceneNode, parentName )

	if( sceneNode:GetName():c_str() == parentName ) then
		return 1
	else
		numParents = sceneNode:GetParentCount()
		if( numParents > 0 ) then
			i = 0
			while i < numParents do
				parentNode = sceneNode:GetParent(i)
				return HasParentNodeNamedRec( parentNode, parentName )
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function HasParentNodeNamed ( sceneNode, parentName )

	numParents = sceneNode:GetParentCount()
	if( numParents > 0 ) then
		i = 0
		while i < numParents do
			parentNode = sceneNode:GetParent(i)
			if( HasParentNodeNamedRec( parentNode, parentName) == 1 ) then
				return 1
			else
				i = i + 1
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function IsNodeBoundingBox( sceneNode )

	technique = sceneNode:GetExtra():GetDefaultType():FindTechnique( "RAGE" )
	if ( technique == nil ) then
		return 0
	end
		
	node = technique:FindParameter( "rageBoundBox" )
	if ( node == nil ) then
		return 0
	end	
		
	return 1	

end

-------------------------------------------------------------------------------

function IsNodeBoundingCapsule( sceneNode )

	technique = sceneNode:GetExtra():GetDefaultType():FindTechnique( "RAGE" )
	if ( technique == nil ) then
		return 0
	end

	node = technique:FindParameter( "rageBoundCapsule" )
	if ( node == nil ) then
		return 0
	end
	
	return 1
	
end

-------------------------------------------------------------------------------

function IsNodeBoundingSphere( sceneNode )

	technique = sceneNode:GetExtra():GetDefaultType():FindTechnique( "RAGE" )
	if ( technique == nil ) then
		return 0
	end

	node = technique:FindParameter( "rageBoundSphere" )
	if ( node == nil ) then
		return 0
	end
	
	return 1

end

-------------------------------------------------------------------------------

function IsBoundTreaty( sceneNode )
	
	rage.Display( "testing bound: " .. sceneNode:GetName():c_str() )
	
	if ( IsNodeBoundingBox( sceneNode ) == 1 ) then
		return 1
	end
	
	if ( IsNodeBoundingCapsule( sceneNode ) == 1 ) then
		return 1
	end
	
	if ( IsNodeBoundingSphere( sceneNode ) == 1 ) then
		return 1
	end	
	
	if ( IsNodeGeometryNode( sceneNode ) == 1 ) then
		if ( string.find( sceneNode:GetName():c_str(), "Bound" ) ~= nil ) then
			return 1
		end
	end
	
--	if( IsNodeGeometryNode( sceneNode ) == 1) then
--		if ( HasParentNodeNamed ( sceneNode, "bound" ) == 1 ) then
--			return 1
--		end
--	end
	
	return 0;
end

-------------------------------------------------------------------------------

function IsMeshTreaty( sceneNode )
	
	if ( IsBoundTreaty( sceneNode ) == 1 ) then
		return 0
	end
	
	if( IsNodeGeometryNode( sceneNode ) == 1 ) then
		return 1
	end
	
	return 0;
end

-------------------------------------------------------------------------------

function IsLodTreaty( sceneNode )

	if( IsNodeGeometryNode( sceneNode ) == 1) then
		if ( HasParentNodeNamed ( sceneNode, "lod" ) == 1 ) then
			return 1
		end
	end
	
	return 0;
end

-------------------------------------------------------------------------------

function IsSkinned( sceneNode )

	numInstances = sceneNode:GetInstanceCount()
	if( numInstances > 0 ) then
		i = 0
		while i < numInstances do
			inst = sceneNode:GetInstance(i)
			if ( inst:GetEntityType() == FCollada.FCDEntity_CONTROLLER ) then
				controller = FCollada.FCDEntityToFCDController( inst:GetEntity() )
				if ( controller:IsSkin() == true ) then
					return 1
				end
			end
			i = i + 1
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function IsOctreeGrid( sceneNode )

	if( IsNodeGeometryNode( sceneNode ) == 1) then
		if ( HasParentNodeNamed ( sceneNode, "OctreeGrid" ) == 1 ) then
			return 1
		end
	end

	return 0
end

-------------------------------------------------------------------------------

function IsOctree( sceneNode )

	if ( IsNodeGeometryNode( sceneNode ) == 1 ) then
		if ( HasParentNodeNamed( sceneNode, "Octree" ) == 1 ) then
			return 1
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function IsBvhGrid( sceneNode )

	if ( IsNodeGeometryNode( sceneNode ) == 1 ) then
		if ( HasParentNodeNamed( sceneNode, "BvhGrid" ) == 1 ) then
			return 1
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function IsBvh( sceneNode )

	if ( IsNodeGeometryNode( sceneNode ) == 1 ) then
		if ( HasParentNodeNamed( sceneNode, "Bvh" ) == 1 ) then
			return 1
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function IsGeometryBound( sceneNode )

	if ( IsNodeGeometryNode( sceneNode ) == 1 ) then
		return 1
	end

	return 0
end

-------------------------------------------------------------------------------

function IsSkeleton( sceneNode )

	if ( sceneNode:IsJoint() == true ) then
		parentNode = sceneNode:GetParent()
		if ( parentNode:GetName():c_str() == "root" ) then
			return 1
		end
	end

	return 0		
end

-------------------------------------------------------------------------------

function IsSkeletonNode( sceneNode )

	if ( sceneNode:GetName():c_str() == "root" ) then
		return 1
	end
	
	if ( sceneNode:IsJoint() == true ) then
		return 1
	end

	return 0

end

-------------------------------------------------------------------------------

function IsSkeletonRoot( sceneNode )

	if ( sceneNode:GetName():c_str() == "root" ) then
		return 1
	end
	
	return 0

end

-------------------------------------------------------------------------------

function HasSkinnedGeometryChildRec( sceneNode )

	if ( sceneNode:IsJoint() == true ) then
		return 0
	end

	if( IsSkinned( sceneNode ) == 1 ) then
		return 1
	else
		numChildren = sceneNode:GetChildrenCount()
		if( numChildren > 0 ) then
			i = 0
			while i < numChildren do
				childNode = sceneNode:GetChild(i)
				return HasSkinnerGeometryChildRec( childNode )
			end
		end
	end
	
	return 0

end

-------------------------------------------------------------------------------

function HasSkinnedGeometryChild( sceneNode )
	numChildren = sceneNode:GetChildrenCount()
	if( numChildren > 0 ) then
		i = 0
		while i < numChildren do
			childNode = sceneNode:GetChild(i)
			if( HasSkinnedGeometryChildRec( childNode ) == 1 ) then
				return 1
			else
				i = i + 1
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function HasBoundChildRec( sceneNode )

	if ( sceneNode:IsJoint() == true ) then
		return 0
	end
	
	if ( IsBoundTreaty( sceneNode ) == 1 ) then
		return 1
	else
		numChildren = sceneNode:GetChildrenCount()
		if ( numChildren > 0 ) then
			i = 0
			while i < numChildren do
				childNode = sceneNode:GetChild(i)
				if ( HasBoundChildRec( childNode ) == 1 ) then
					return 1
				else
					i = i + 1
				end
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

 function HasBoundChild( sceneNode )
 
	numChildren = sceneNode:GetChildrenCount()
	if ( numChildren > 0 ) then 
		
		endCount = numChildren - 1
		for i = 0,endCount do
			childNode = sceneNode:GetChild(i)
			if ( HasBoundChildRec( childNode ) == 1 ) then
				return 1
			end
		end

	end
 
	return 0
 end

-------------------------------------------------------------------------------

function IsFragment( sceneNode )

	name = sceneNode:GetName():c_str()
	
	if ( sceneNode:IsJoint() == true ) then
		if ( HasBoundChild( sceneNode ) == 1 ) then
			if ( string.find( name, "Frag" ) ~= nil ) then	
				return 1
			end
		end		
	end

	return 0

end

-------------------------------------------------------------------------------

function IsFragmentRoot( sceneNode )

	if ( sceneNode:GetName():c_str() == "root" ) then
		return 1
	end
	
	return 0

end

-------------------------------------------------------------------------------

function IsBoundChildRec( sceneNode, boundNode )

	if ( sceneNode:IsJoint() == true ) then
		return 0
	end
	
	if ( sceneNode:GetDaeId():c_str() == boundNode:GetDaeId():c_str() ) then
		return 1
	else
		numChildren = sceneNode:GetChildrenCount()

		if ( numChildren > 0 ) then
			endCount = numChildren - 1
			for i = 0,endCount do
				childNode = sceneNode:GetChild(i)
				if ( IsBoundChildRec( childNode, boundNode ) == 1 ) then
					return 1
				end
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

 function IsBoundChild( parentNode, boundNode )
 
	numChildren = parentNode:GetChildrenCount()
	if ( numChildren > 0 ) then 
		endCount = numChildren - 1
		for i = 0,endCount do
			childNode = parentNode:GetChild(i)
			
			if ( IsBoundChildRec( childNode, boundNode ) == 1 ) then
				return 1
			end
		end

	end
 
	return 0
 end

-------------------------------------------------------------------------------

function IsFragmentBound( fragmentNode, boundNode )

	if ( IsBoundChild( fragmentNode, boundNode ) == 1 ) then
		rage.Display( boundNode:GetName():c_str() .. " bounds " .. fragmentNode:GetName():c_str() )
		return 1
	end
	
	return 0

end

