#include "rageColladaCompile.h"

#include "rageColladaLib/rageColladaConvert.h"
#include "rageColladaLib/rageColladaProperties.h"
#include "rageColladaLib/rageColladaSerialize.h"
#include "rageColladaLib/rageColladaUtil.h"
#include "rageColladaLib/mgc.h"

#include "rageLuaScriptHost/rageLuaScriptHost.h"

#include "rageDiplomat/rageDiplomat.h"
#include "rageDiplomat/rageLuaScriptTreaty.h"

#include "file/asset.h"
#include "file/token.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "parser/manager.h"
#include "vector/geometry.h"

#include <algorithm>

#if __OPTIMIZED
#define FCOLLADALUA_PATH	"FColladaLua.dll"
#else
#define FCOLLADALUA_PATH	"C:\\soft\\rage_head\\rage\\suite\\tools\\rageCollada\\FColladaSWIG\\FColladaLua\\Debug\\FColladaLua.dll"
#endif
#define FCOLLADALUA_INIT_FN	"luaopen_FCollada"

#define TREATY_SCRIPT "C:\\soft\\rage\\suite\\tools\\rageCollada\\rageColladaCompileLib\\rageExportTreaties.lua"

using namespace std;

namespace
{

const FCDSceneNode* FindThresholdParent( const FCDSceneNode* node )
{
	if ( NULL != node )
	{
		if ( rage::IsLodThreshold( node ) )
		{
			return node;
		}

		size_t parentCount = node->GetParentCount();
		for ( size_t parentIndex = 0; parentIndex < parentCount; ++parentIndex )
		{
			const FCDSceneNode* found = FindThresholdParent( node->GetParent( parentIndex ) );
			if ( NULL != found )
			{
				return found;
			}
		}
	}

	return NULL;
}

const FCDSceneNode* FindChild( const FCDSceneNode* parent, const FCDSceneNode* node )
{
	if ( NULL != parent )
	{
		if ( parent == node )
		{
			return node;
		}

		size_t childCount = parent->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			const FCDSceneNode* found = FindChild( parent->GetChild( childIndex ), node );
			if ( NULL != found )
			{
				return found;
			}
		}
	}

	return NULL;
}

float FindThreshold( const FCDSceneNode* node )
{
	// traverse up the scene until we get the node with a threshold

	const FCDSceneNode* threshold = FindThresholdParent( node );
	if ( NULL != threshold )
	{
		rage::RAGEThresholdList thresholds;
		rage::GetLodThreshold( threshold, thresholds );

		size_t childCount = threshold->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			const FCDSceneNode* child = FindChild( threshold->GetChild( childIndex ), node );
			if ( NULL != child )
			{
				if ( childIndex < thresholds.size() )
				{
					return float( thresholds[ childIndex ] );
				}
				else
				{
					return 9999.0f;
				}

			}
		}
	}

	return 9999.0f;
}

class ColladaProcessor
{
public:
	void AddMeshElement( FCDSceneNode* pNode, void* )
	{
		meshes.push_back( pNode );

		const FCDEntityInstanceContainer& instances = pNode->GetInstances();
		for ( FCDEntityInstanceContainer::const_iterator it = instances.begin(); it != instances.end(); ++it )
		{
			const FCDGeometryInstance* geometry = dynamic_cast< const FCDGeometryInstance* >( *it );
			if ( NULL != geometry )
			{
				rage::FindReferencedMaterials( materials, geometry );
			}
		}
	}

	void AddBoundingBoxElement( FCDSceneNode* pNode, void* )
	{
		boundingBoxes.push_back( pNode );
		bounds.push_back( pNode );
	}

	void AddBoundingCapsuleElement( FCDSceneNode* pNode, void* )
	{
		boundingCapsules.push_back( pNode );
		bounds.push_back( pNode );
	}

	void AddBoundingSphereElement( FCDSceneNode* pNode, void* )
	{
		boundingSpheres.push_back( pNode );
		bounds.push_back( pNode );
	}

	void AddLodElement( FCDSceneNode* pNode, void* )
	{
		lods.push_back( pNode );
	}

	void AddSkinElement( FCDSceneNode* pNode, void* )
	{
		skins.push_back( pNode );

		size_t instanceCount = pNode->GetInstanceCount();
		for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
		{
			const FCDControllerInstance* pControllerInstance = 
				dynamic_cast< const FCDControllerInstance* >( pNode->GetInstance( instanceIndex ) );
			if ( NULL != pControllerInstance )
			{
				rage::FindReferencedMaterials( materials, pControllerInstance );
			}
		}	
	}

	void AddOctreeGrid( FCDSceneNode* pNode, void* )
	{
		octreeGrids.push_back( pNode );
		bounds.push_back( pNode );
	}

	void AddOctree( FCDSceneNode* pNode, void* )
	{
		octrees.push_back( pNode );
		bounds.push_back( pNode );
	}

	void AddBvhGrid( FCDSceneNode* pNode, void* )
	{
		bvhGrids.push_back( pNode );
		bounds.push_back( pNode );
	}

	void AddBvh( FCDSceneNode* pNode, void* )
	{
		bvhs.push_back( pNode );
		bounds.push_back( pNode );
	}

	void AddGeometryBound( FCDSceneNode* pNode, void* )
	{
		boundGeometries.push_back( pNode );
		bounds.push_back( pNode );
	}

	void AddSkeleton( FCDSceneNode* pNode, void* )
	{
		skeletons.push_back( pNode );
	}

	void AddSkeletonNode( FCDSceneNode* pNode, void* )
	{
		skeletonNodes.push_back( pNode );
	}

	void AddSkeletonRoot( FCDSceneNode* pNode, void* )
	{
		skeletonRoot = pNode;
	}

	void AddFragment( FCDSceneNode* pNode, void* )
	{
		fragments.push_back( pNode );
	}

	void AddFragmentRoot( FCDSceneNode* pNode, void* )
	{
		fragmentRoot = pNode;
	}

	bool RegisterTreaties( const char* pTreatyScript  )
	{
		#define ACCEPTFTOR( _str )	rageShouldAcceptFtor* _str = new rageShouldAcceptFtor( #_str )

		typedef rageLuaScriptTreaty< rageShouldAcceptFtor > LuaScriptTreaty;
		LuaScriptTreaty* pMeshScriptTreaty = 
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddMeshElement ), NULL );
		ACCEPTFTOR( IsMeshTreaty );
		if ( !pMeshScriptTreaty->InitializeTreatyScript( pTreatyScript, *IsMeshTreaty ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pMeshScriptTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pMeshScriptTreaty );

		LuaScriptTreaty* pBoundingBoxScriptTeaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddBoundingBoxElement ), NULL );
		ACCEPTFTOR( IsNodeBoundingBox );
		if ( !pBoundingBoxScriptTeaty->InitializeTreatyScript( pTreatyScript, *IsNodeBoundingBox ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pBoundingBoxScriptTeaty;

			return false;
		}
		diplomat.RegisterTreaty( pBoundingBoxScriptTeaty );

		LuaScriptTreaty* pBoundingCapsuleScriptTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddBoundingCapsuleElement ), NULL );
		ACCEPTFTOR( IsNodeBoundingCapsule );
		if ( !pBoundingCapsuleScriptTreaty->InitializeTreatyScript( pTreatyScript, *IsNodeBoundingCapsule ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pBoundingCapsuleScriptTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pBoundingCapsuleScriptTreaty );

		LuaScriptTreaty* pBoundingSphereScriptTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddBoundingSphereElement ), NULL );
		ACCEPTFTOR( IsNodeBoundingSphere );
		if ( !pBoundingSphereScriptTreaty->InitializeTreatyScript( pTreatyScript, *IsNodeBoundingSphere ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			delete pBoundingSphereScriptTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pBoundingSphereScriptTreaty );

		LuaScriptTreaty* pLodScriptTeaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddLodElement ), NULL );
		ACCEPTFTOR( IsLodTreaty );
		if ( !pLodScriptTeaty->InitializeTreatyScript( pTreatyScript, *IsLodTreaty ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pLodScriptTeaty;

			return false;
		}
		diplomat.RegisterTreaty( pLodScriptTeaty );

		LuaScriptTreaty* pSkinTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddSkinElement ), NULL );
		ACCEPTFTOR( IsSkinned );
		if ( !pSkinTreaty->InitializeTreatyScript( pTreatyScript, *IsSkinned ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pSkinTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pSkinTreaty );

		LuaScriptTreaty* pOctreeGridTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddOctreeGrid ), NULL );
		ACCEPTFTOR( IsOctreeGrid );
		if ( !pOctreeGridTreaty->InitializeTreatyScript( pTreatyScript, *IsOctreeGrid ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pOctreeGridTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pOctreeGridTreaty );

		LuaScriptTreaty* pOctreeTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddOctree ), NULL );
		ACCEPTFTOR( IsOctree );
		if ( !pOctreeTreaty->InitializeTreatyScript( pTreatyScript, *IsOctree ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pOctreeTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pOctreeTreaty );	

		LuaScriptTreaty* pBvhGridTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddBvhGrid ), NULL );
		ACCEPTFTOR( IsBvhGrid );
		if ( !pBvhGridTreaty->InitializeTreatyScript( pTreatyScript, *IsBvhGrid ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pBvhGridTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pBvhGridTreaty );

		LuaScriptTreaty* pBvhTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddBvh ), NULL );
		ACCEPTFTOR( IsBvh );
		if ( !pBvhTreaty->InitializeTreatyScript( pTreatyScript, *IsBvh ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pBvhTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pBvhTreaty );

		LuaScriptTreaty* pGeometryBoundTreaty =
			new LuaScriptTreaty( MakeFunctor( *this , &ColladaProcessor::AddGeometryBound ), NULL );
		ACCEPTFTOR( IsGeometryBound );
		if ( !pGeometryBoundTreaty->InitializeTreatyScript( pTreatyScript, *IsGeometryBound ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pGeometryBoundTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pGeometryBoundTreaty );

		LuaScriptTreaty* pSkeletonTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddSkeleton ), NULL );
		ACCEPTFTOR( IsSkeleton );
		if ( !pSkeletonTreaty->InitializeTreatyScript( pTreatyScript, *IsSkeleton ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pSkeletonTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pSkeletonTreaty );

		LuaScriptTreaty* pSkeletonNodeTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddSkeletonNode ), NULL );
		ACCEPTFTOR( IsSkeletonNode );
		if ( !pSkeletonNodeTreaty->InitializeTreatyScript( pTreatyScript, *IsSkeletonNode ) ) 
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pSkeletonNodeTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pSkeletonNodeTreaty );

		LuaScriptTreaty* pSkeletonRootTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddSkeletonRoot ), NULL );
		ACCEPTFTOR( IsSkeletonRoot );
		if ( !pSkeletonRootTreaty->InitializeTreatyScript( pTreatyScript, *IsSkeletonRoot ) )
		{
			Errorf( "There was an error registering a scripted treaty." );
			delete pSkeletonRootTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pSkeletonRootTreaty );

		LuaScriptTreaty* pFragmentTreaty =
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddFragment ), NULL );
		ACCEPTFTOR( IsFragment );
		if ( !pFragmentTreaty->InitializeTreatyScript( pTreatyScript, *IsFragment ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			delete pFragmentTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pFragmentTreaty );

		LuaScriptTreaty* pFragmentRootTreaty = 
			new LuaScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddFragmentRoot ), NULL );
		ACCEPTFTOR( IsFragmentRoot );
		if ( !pFragmentRootTreaty->InitializeTreatyScript( pTreatyScript, *IsFragmentRoot ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			delete pFragmentRootTreaty;

			return false;
		}
		diplomat.RegisterTreaty( pFragmentRootTreaty );

		return true;
	}

	void Clear()
	{
		diplomat.ClearTreaties();
	}

	void Process( FCDocument* document )
	{
		meshes.clear();
		lods.clear();

		boundingBoxes.clear();
		boundingCapsules.clear();
		boundingSpheres.clear();

		diplomat.VisitDepthFirst( document->GetVisualSceneInstance(), rageDiplomat::MULTIPLE_SIGNING );
	}

	class MyShouldAcceptFtor
	{
	public:
		MyShouldAcceptFtor( FCDSceneNode* pFragmentNode, const char* szScriptFnName )
			:	m_pFragmentNode ( pFragmentNode )
			,	m_scriptFnName( szScriptFnName ) { }
		MyShouldAcceptFtor() {}
	public:
		bool operator()( FCDSceneNode* pNode )
		{
			int iResult; 
			iResult = rageLuaScriptHostCall< int , rageLshSwigObjWrap, rageLshSwigObjWrap > (
				m_scriptFnName.c_str()).call( rageLshSwigObjWrap( "FCollada", "FCDSceneNode_p_ctor", m_pFragmentNode), rageLshSwigObjWrap("FCollada", "FCDSceneNode_p_ctor", pNode) );
			if(iResult > 0)
				return true;
			else
				return false;
		}
	private:
		FCDSceneNode* m_pFragmentNode;
		std::string m_scriptFnName;

	};

	void AddFragmentBound( FCDSceneNode* pBoundNode, void* pData )
	{
		FCDSceneNode* pFragmentNode = reinterpret_cast< FCDSceneNode* >( pData );
		
		// if we can find the fragment, then add the bound,
		//	otherwise add the fragment to the list and the 
		//	bound as well.

		RAGEEntity::RAGEFragment* pFragment = NULL;

		size_t fragmentCount = fragmentList.size();
		for ( size_t fragmentIndex = 0; fragmentIndex < fragmentCount; ++fragmentIndex )
		{
			if ( fragmentList[ fragmentIndex ].fragment == pFragmentNode )
			{
				pFragment = &fragmentList[ fragmentIndex ];		
				break;
			}
		}

		if ( NULL != pFragment )
		{
			pFragment->bounds.push_back( pBoundNode );
		}
		else
		{
			RAGEEntity::RAGEFragment fragment;
			fragment.fragment = pFragmentNode;
			fragment.bounds.push_back( pBoundNode );
		
			fragmentList.push_back( fragment );
		}
	}

	bool ProcessFragmentBounds( const char* pTreatyScript )
	{
		typedef rageLuaScriptTreaty< MyShouldAcceptFtor > ScriptTreaty;

		size_t fragmentCount = fragments.size();
		for ( size_t fragmentIndex = 0; fragmentIndex < fragmentCount; ++fragmentIndex )
		{
			FCDSceneNode* pFragment = fragments[ fragmentIndex ];

			MyShouldAcceptFtor ftor ( pFragment, "IsFragmentBound" );

			ScriptTreaty* pFragmentBoundTreaty =
				new ScriptTreaty( MakeFunctor( *this, &ColladaProcessor::AddFragmentBound ), pFragment );
			if ( !pFragmentBoundTreaty->InitializeTreatyScript( pTreatyScript, ftor ) )
			{
				Errorf( "There was an error registering a scripted treaty" );
				delete pFragmentBoundTreaty;

				return false;
			}
			diplomat.RegisterTreaty( pFragmentBoundTreaty );

			diplomat.ForEach( &(*bounds.begin()), &(*bounds.end()) );

			diplomat.ClearTreaties();
		}

		return true;
	}

	void SerializeSkinnedMeshes( vector< string >& outputs, const std::string& outpath )
	{
		size_t skinnedMeshCount = skins.size();
		for ( size_t skinnedMeshIndex = 0; skinnedMeshIndex < skinnedMeshCount; ++skinnedMeshIndex )
		{
			const FCDSceneNode* node = skins[ skinnedMeshIndex ];

			size_t instanceCount = node->GetInstanceCount();
			for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
			{
				const FCDControllerInstance* instance = 
					dynamic_cast< const FCDControllerInstance* >( node->GetInstance( instanceIndex ) );
				if ( NULL != instance )
				{
					const FCDController* controller = static_cast< const FCDController* >( instance->GetEntity() );
					if ( controller->IsSkin() )
					{
						const FCDSkinController* skin = controller->GetSkinController();
						const FCDEntity* entity = skin->GetTarget();
						// NOTE: apparently this can point to another controller, just keep this in mind

						if ( FCDEntity::GEOMETRY == entity->GetType() )
						{
							FMMatrix44 matrix = node->ToMatrix();

							rage::mshMesh mesh;
							rage::ConvertMesh( mesh, skin, materials, matrix );					

							const FCDGeometry* geometry = static_cast< const FCDGeometry* >( entity );
							std::string filename = 
								outpath + std::string( "/" ) + geometry->GetDaeId().c_str();
							outputs.push_back( filename + ".mesh" );

							rage::SerializeMesh( filename.c_str(), mesh );
						}
					}
				}
			}
		}
	}

	void SerializeMeshes( vector< string >& outputs, const std::string& outpath )
	{
		size_t meshCount = meshes.size();
		for ( size_t meshIndex = 0; meshIndex < meshCount; ++meshIndex )
		{
			const FCDSceneNode* node = meshes[ meshIndex ];
		
			const FCDEntityInstanceContainer& instances = node->GetInstances();
			for ( FCDEntityInstanceContainer::const_iterator it = instances.begin(); it != instances.end(); ++it )
			{
				const FCDGeometryInstance* instance = dynamic_cast< const FCDGeometryInstance* >( *it );
				if ( NULL != instance )
				{
					const FCDGeometry* geometry = static_cast< const FCDGeometry* >( instance->GetEntity() );

					std::string filename = outpath + std::string( "/" ) + geometry->GetDaeId().c_str();
					outputs.push_back( filename + ".mesh" );

					FMMatrix44 matrix = node->ToMatrix();

					rage::mshMesh mesh;
					rage::ConvertMesh( mesh, geometry, materials, matrix );

					rage::SerializeMesh( filename.c_str(), mesh );
				}
			}
		}
	}

	void SerializeBounds( vector< string >& outputs, const std::string& outpath )
	{
		//std::string filename = /*outpath + std::string( "/" ) +*/ sceneNode->GetName().c_str();

		size_t boxCount = boundingBoxes.size();
		for ( size_t boxIndex = 0; boxIndex < boxCount; ++boxIndex )
		{
			const FCDSceneNode* node = boundingBoxes[ boxIndex ];	

			rage::phBoundBox box;
			rage::ConvertBoundBox( box, node );
			
			std::string filename = outpath + std::string( "/" ) + node->GetDaeId().c_str();
			outputs.push_back( filename + ".bnd" );

			rage::SerializeBound( filename.c_str(), box );
		}

		size_t capsuleCount = boundingCapsules.size();
		for ( size_t capsuleIndex = 0; capsuleIndex < capsuleCount; ++capsuleIndex )
		{
			const FCDSceneNode* node = boundingCapsules[ capsuleIndex ];

			rage::phBoundCapsule capsule;
			rage::ConvertBoundCapsule( capsule, node );
			
			std::string filename = outpath + std::string( "/" ) + node->GetDaeId().c_str();
			outputs.push_back( filename + ".bnd" );

			rage::SerializeBound( filename.c_str(), capsule );
		}

		size_t sphereCount = boundingSpheres.size();
		for ( size_t sphereIndex = 0; sphereIndex < sphereCount; ++sphereIndex )
		{
			const FCDSceneNode* node = boundingSpheres[ sphereIndex ];

			rage::phBoundSphere sphere;
			rage::ConvertBoundSphere( sphere, node );

			std::string filename = outpath + std::string( "/" ) + node->GetDaeId().c_str();
			outputs.push_back( filename + ".bnd" );

			rage::SerializeBound( filename.c_str(), sphere );
		}
/*
		size_t octreeGridCount = octreeGrids.size();
		for ( size_t octreeGridIndex = 0; octreeGridIndex < octreeGridCount; ++octreeGridIndex )
		{
			const FCDSceneNode* node = octreeGrids[ octreeGridIndex ];

			rage::phBoundOTGrid octreeGrid;
			rage::ConvertBoundOctreeGrid( octreeGrid, node );

			std::string filename = outpath + std::string( "/" ) + node->GetDaeId().c_str();
			outputs.push_back( filename + ".bnd" );

			rage::SerializeBound( filename.c_str(), octreeGrid );
		}

		size_t octreeCount = octrees.size();
		for ( size_t octreeIndex = 0; octreeIndex < octreeCount; ++octreeIndex )
		{
			const FCDSceneNode* node = octrees[ octreeIndex ];

			rage::phBoundOctree octree;
			rage::ConvertBoundOctree( octree, node );

			std::string filename = outpath + std::string( "/" ) + node->GetDaeId().c_str();
			outputs.push_back( filename + ".bnd" );

			rage::SerializeBound( filename.c_str(), octree );
		}

		size_t bvhGridCount = bvhGrids.size();
		for ( size_t bvhGridIndex = 0; bvhGridIndex < bvhGridCount; ++bvhGridIndex )
		{
			const FCDSceneNode* node = bvhGrids[ bvhGridIndex ];

			rage::phBoundOTGrid bvhGrid;
			rage::ConvertBoundBvhGrid( bvhGrid, node );

			std::string filename = outpath + std::string( "/" ) + node->GetDaeId().c_str();
			outputs.push_back( filename + ".bnd" );

			rage::SerializeBound( filename.c_str(), bvhGrid ); 
		}
*/
		size_t bvhCount = bvhs.size();
		for ( size_t bvhIndex = 0; bvhIndex < bvhCount; ++bvhIndex )
		{
			const FCDSceneNode* node = bvhs[ bvhIndex ];

			rage::phBoundBVH bvh;
			rage::ConvertBoundBvh( bvh, node );

			std::string filename = outpath + std::string( "/" ) + node->GetDaeId().c_str();
			outputs.push_back( filename + ".bnd" );

			rage::SerializeBound( filename.c_str(), bvh );
		}

		size_t boundGeometryCount = boundGeometries.size();
		for ( size_t boundGeometryIndex = 0; boundGeometryIndex < boundGeometryCount; ++boundGeometryIndex )
		{
			const FCDSceneNode* node = boundGeometries[ boundGeometryIndex ];

			rage::phBoundGeometry geometry;
			rage::ConvertBoundGeometry( geometry, node );

			std::string filename = outpath + std::string( "/" ) + node->GetDaeId().c_str();
			outputs.push_back( filename + ".bnd" );

			rage::SerializeBound( filename.c_str(), geometry );
		}
	}

	void SerializeSkeleton( string& outputs, const std::string& outpath )
	{
		if ( NULL != skeletonRoot )
		{
			std::string filename = outpath + std::string( "/" ) + skeletonRoot->GetDaeId().c_str();
			outputs = filename + ".skel";
			// TODO: REPLACE WITH HOW SKELS ARE NOW SERIALIZED
			//rage::SerializeSkeleton( filename.c_str(), skeletonRoot, skeletonNodes );
			Assert( 0 );
		}

#if 0
		size_t skeletonCount = skeletons.size();
		for ( size_t skeletonIndex = 0; skeletonIndex < skeletonCount; ++skeletonIndex )
		{
			const FCDSceneNode* pNode = skeletons[ skeletonIndex ];

			std::string filename = outpath + std::string( "/" ) + pNode->GetName().c_str();
			outputs.push_back( filename + ".skel" );

			rage::SerializeSkeleton( filename.c_str(), pNode );
		}
#endif
	}

	void SerializeEntity( vector< string >& outputs, const string& skelton, const char* name )
	{
		// build up the lod arrays for the meshes and serialize out the entity
		rage::RAGEEntity::RAGEEntityLodGroupList rageLods;
		size_t meshCount = meshes.size();
		for ( size_t meshIndex = 0; meshIndex < meshCount; ++meshIndex )
		{
			const FCDSceneNode* node = meshes[ meshIndex ];

			float threshold = FindThreshold( node );

			bool found = false;

			size_t rageLodsCount = rageLods.size();
			size_t rageLodsIndex = 0;
			for ( ; rageLodsIndex < rageLodsCount; ++rageLodsIndex )
			{
				if ( threshold == rageLods[ rageLodsIndex ].threshold )
				{
					found = true;
					break;
				}
			}

			const FCDEntityInstanceContainer& instances = node->GetInstances();
			for ( FCDEntityInstanceContainer::const_iterator it = instances.begin(); it != instances.end(); ++it )
			{
				const FCDGeometryInstance* geometry = dynamic_cast< FCDGeometryInstance* >( const_cast< FCDEntityInstance* >( *it ) );
				if ( NULL != geometry )
				{
					if ( true != found )
					{
						rage::RAGEEntity::LOD lod;
						lod.meshes.push_back( dynamic_cast< const FCDGeometry* >( geometry->GetEntity() ) );
						lod.threshold = threshold;

						rageLods.push_back( lod );
					}
					else
					{
						rageLods[ rageLodsIndex ].meshes.push_back( 
							dynamic_cast< const FCDGeometry* >( geometry->GetEntity() ) );
					}

				}
			}
		}
	
		// don't forget to add the skinned meshes
		size_t skinCount = skins.size();
		for ( size_t skinIndex = 0; skinIndex < skinCount; ++skinIndex )
		{
			const FCDSceneNode* pNode = skins[ skinIndex ];

			float threshold = FindThreshold( pNode );

			bool found = false;
	
			size_t rageLodsCount = rageLods.size();
			size_t rageLodsIndex = 0;
			for ( ; rageLodsIndex < rageLodsCount; ++rageLodsIndex )
			{
				if ( threshold == rageLods[ rageLodsIndex ].threshold )
				{
					found = true;
					break;
				}
			}

			const FCDEntityInstanceContainer& instances = pNode->GetInstances();
			for ( FCDEntityInstanceContainer::const_iterator it = instances.begin(); it != instances.end(); ++it )
			{
				const FCDControllerInstance* pControllerInstance = 
					dynamic_cast< FCDControllerInstance* >( const_cast< FCDEntityInstance* >( *it ) );
				if ( NULL != pControllerInstance )
				{
					const FCDController* pController = static_cast< const FCDController* >( pControllerInstance->GetEntity() );
					const FCDGeometry* pGeometry = pController->GetBaseGeometry();

					if ( true != found )
					{
						rage::RAGEEntity::LOD lod;
						lod.meshes.push_back( pGeometry );
						lod.threshold = threshold;

						rageLods.push_back( lod );
					}
					else
					{
						rageLods[ rageLodsIndex ].meshes.push_back( pGeometry );
					}

				}
			}
		}

		rage::RAGEEntity::RAGEMaterialList rageMaterials;
		size_t materialCount = materials.size();
		for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
		{
			const FCDMaterial* material = materials[ materialIndex ].second;
			if ( rage::IsShaderMaterial( material ) )
			{
				std::string materialName = 
					rage::GetShaderMaterial( material );

				rage::RAGEEntity::RAGEMaterialList::iterator found = 
					std::find( rageMaterials.begin(), rageMaterials.end(), materialName );
				if ( found == rageMaterials.end() )
				{
					rageMaterials.push_back( materialName );
				}
			}
		}

		rage::RAGEEntity entity;
		entity.lodGroups = rageLods;
		entity.materials = rageMaterials;
		entity.bounds = bounds;
		entity.fragmentRoot = fragmentRoot;
		entity.fragments = fragmentList;
		entity.skeleton = skelton;

		std::string filename = std::string( name ) + ".type";
		outputs.push_back( filename );

		rage::SerializeEntity( name, entity );
	}
public:
	ColladaProcessor()
		:	skeletonRoot ( NULL )
		,	fragmentRoot ( NULL ) { }
public:
	FCDSceneNodeList meshes;
	FCDSceneNodeList lods;

	FCDSceneNodeList skins;

	FCDSceneNodeList octreeGrids;
	FCDSceneNodeList octrees;
	FCDSceneNodeList bvhGrids;
	FCDSceneNodeList bvhs;
	FCDSceneNodeList boundGeometries;

	FCDSceneNodeList skeletons;

	FCDSceneNodeList skeletonNodes;
	FCDSceneNode* skeletonRoot;

	FCDSceneNodeList fragments;

	FCDSceneNode* fragmentRoot;

	RAGEEntity::RAGEFragmentList fragmentList;

	rage::RAGEEntity::RAGEBoundList bounds;
	FCDSceneNodeList boundingBoxes;
	FCDSceneNodeList boundingCapsules;
	FCDSceneNodeList boundingSpheres;

	rage::FCDMaterialList materials;

	rageDiplomat diplomat;
};

} // namespace

#include "rageColladaLib/rageColladaHelpers.h"

namespace rage
{
	void rageColladaCompile::Compile( const char* filename, const char* outpath )
	{
		INIT_PARSER;
		INIT_LUASCRIPTHOST;

		rageColladaProperties::Initialize();

		FCollada::Initialize();

		if( !LUASCRIPTHOST.LoadExtension( FCOLLADALUA_PATH, FCOLLADALUA_INIT_FN, NULL ) )
		{		
			Errorf( "Failed to load LUA extension : '%s'", FCOLLADALUA_PATH );
			return;
		}

		rageColladaProperties::Register( "Unit", "BaseUnits", "NONE" );
		rageColladaProperties::Register( "Unit", "UnitTypeLinearTo", "NONE" );

		rageColladaProperties::Register( "Bounds", "ExportAsOctree", false );
		rageColladaProperties::Register( "Bounds", "ExportAsOctreeGrid", false );
		rageColladaProperties::Register( "Bounds", "ExportAsBvh", false );
		rageColladaProperties::Register( "Bounds", "ExportAsBvhGrid", false );
		rageColladaProperties::Register( "Bounds", "OctreeGridCellSize", 300.f );
		rageColladaProperties::Register( "Bounds", "UseChildrenOfLevelInstanceNode", false );
		rageColladaProperties::Register( "Bounds", "ZeroPrimitiveCentriods", false );
		rageColladaProperties::Register( "Bounds", "CheckBoundVertCount", false );
		rageColladaProperties::Register( "Bounds", "QuadrifyGeometryBounds", true );
		rageColladaProperties::Register( "Bounds", "ApplyBulletShrinkMargin", false );

		ColladaProcessor cp;
		cp.RegisterTreaties( TREATY_SCRIPT );

		FCDocument* document = FCollada::NewTopDocument();
		bool loaded = FCollada::LoadDocument( document, filename );

		using namespace std;

		string file ( filename );
		string::size_type index = file.rfind( "/" );
		
		file = file.erase( index, file.length() - index );

		cp.Process( document );

		cp.Clear();

		cp.ProcessFragmentBounds( TREATY_SCRIPT );

		std::string outdir = file;

		cp.SerializeSkinnedMeshes( m_Meshes, outdir );
		cp.SerializeMeshes( m_Meshes, outdir );
		cp.SerializeBounds( m_Bounds, outdir );
		cp.SerializeSkeleton( m_Skeleton, outdir );		

		const FCDSceneNode* node = document->GetVisualSceneInstance();

		std::string typefile ( file );
		typefile += std::string( "/" );
		typefile += node->GetName().c_str();
		
		cp.SerializeEntity( m_Entities, m_Skeleton, typefile.c_str() );
	
		document->Release();

		FCollada::Release();

		rageColladaProperties::Shutdown();

		SHUTDOWN_LUASCRIPTHOST;
		SHUTDOWN_PARSER;
	}

	void rageColladaCompile::Reset()
	{
		m_Entities.clear();
		m_Animations.clear();
		m_Bounds.clear();
		m_Meshes.clear();
		m_Skeleton.clear();
	}

	int rageColladaCompile::GetEntityCount() const 
	{ 
		return int( m_Entities.size() ); 
	}

	const char* rageColladaCompile::GetEntity( const int index ) const 
	{ 
		Assert( 0 <= index && index < GetEntityCount() ); 
		return m_Entities[ index ].c_str(); 
	}

	int rageColladaCompile::GetAnimationCount() const 
	{ 
		return int( m_Animations.size() ); 
	}

	const char* rageColladaCompile::GetAnimation( const int index ) const
	{
		Assert( 0 <= index && index < GetAnimationCount() );
		return m_Animations[ index ].c_str();
	}

	int rageColladaCompile::GetBoundCount() const 
	{ 
		return int( m_Bounds.size() ); 
	}

	const char* rageColladaCompile::GetBound( const int index ) const
	{
		Assert( 0 <= index && index < GetBoundCount() );
		return m_Bounds[ index ].c_str();
	}

	int rageColladaCompile::GetMeshCount() const 
	{ 
		return int( m_Meshes.size() ); 
	}

	const char* rageColladaCompile::GetMesh( const int index ) const
	{
		Assert( 0 <= index && index < GetMeshCount() );
		return m_Meshes[ index ].c_str();
	}

	const char* rageColladaCompile::GetSkeleton() const
	{
		return m_Skeleton.c_str();
	}

} // rage
