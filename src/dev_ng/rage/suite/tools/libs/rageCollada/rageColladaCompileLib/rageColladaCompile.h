#ifndef __RAGE_COLLADA_COMPILE__
#define __RAGE_COLLADA_COMPILE__

#include <string>
#include <vector>

namespace rage
{

	class rageColladaCompile
	{
	public:
		void Compile( const char* filename, const char* outpath );

		void Reset();

		int GetEntityCount() const;
		const char* GetEntity( int index ) const;
	
		int GetAnimationCount() const;
		const char* GetAnimation( int index ) const;
	
		int GetBoundCount() const;
		const char* GetBound( int index ) const;
		
		int GetMeshCount() const;
		const char* GetMesh( int index ) const;
		
		const char* GetSkeleton() const;
	private:
		typedef std::vector< std::string > FilenameList;

		FilenameList m_Entities;
		FilenameList m_Animations;
		FilenameList m_Bounds;
		FilenameList m_Meshes;
		std::string m_Skeleton;
	};

} // rage

#endif // __RAGE_COLLADA_COMPILE__
