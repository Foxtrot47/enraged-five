#ifndef __FCDENTITY_CAST_H__
#define __FCDENTITY_CAST_H__

#define CreateFCDEntityCastFunctionsForType(destination_type)	\
inline destination_type* FCDEntityTo##destination_type( FCDEntity* entity )	\
{																			\
	return static_cast< destination_type* >( entity );						\
}																			\
	inline FCDEntity* ##destination_type##ToFCDEntity( destination_type* entity )	\
{																			\
	return static_cast< FCDEntity* >( entity );						\
}


CreateFCDEntityCastFunctionsForType(FCDAnimation)
CreateFCDEntityCastFunctionsForType(FCDAnimationClip)
CreateFCDEntityCastFunctionsForType(FCDController)
CreateFCDEntityCastFunctionsForType(FCDEffect)
CreateFCDEntityCastFunctionsForType(FCDEmitter)
CreateFCDEntityCastFunctionsForType(FCDForceField)
CreateFCDEntityCastFunctionsForType(FCDGeometry)
CreateFCDEntityCastFunctionsForType(FCDImage)
CreateFCDEntityCastFunctionsForType(FCDMaterial)
CreateFCDEntityCastFunctionsForType(FCDPhysicsAnalyticalGeometry)
CreateFCDEntityCastFunctionsForType(FCDPhysicsMaterial)
CreateFCDEntityCastFunctionsForType(FCDPhysicsModel)
CreateFCDEntityCastFunctionsForType(FCDPhysicsRigidBody)
CreateFCDEntityCastFunctionsForType(FCDPhysicsRigidConstraint)
CreateFCDEntityCastFunctionsForType(FCDPhysicsScene)
CreateFCDEntityCastFunctionsForType(FCDSceneNode)
CreateFCDEntityCastFunctionsForType(FCDTargetedEntity)


#define CreateFCDObjectWithIdCastFunctionsForType(destination_type)	\
inline destination_type* FCDObjectWithIdTo##destination_type( FCDObjectWithId* entity )	\
{																			\
	return static_cast< destination_type* >( entity );						\
}																			\
	inline FCDObjectWithId* ##destination_type##ToFCDObjectWithId( destination_type* entity )	\
{																			\
	return static_cast< FCDObjectWithId* >( entity );						\
}


CreateFCDObjectWithIdCastFunctionsForType(FCDEntity)
CreateFCDObjectWithIdCastFunctionsForType(FCDGeometrySource)


#endif // __FCDENTITY_CAST_H__ 
