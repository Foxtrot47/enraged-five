#ifndef __FCDTransform_CAST_H__
#define __FCDTransform_CAST_H__

inline FCDTLookAt* FCDTransformToFCDTLookAt( FCDTransform* Transform )
{
	return static_cast< FCDTLookAt* >( Transform );
}

inline FCDTMatrix* FCDTransformToFCDTMatrix( FCDTransform* Transform )
{
	return static_cast< FCDTMatrix* >( Transform );
}

inline FCDTRotation* FCDTransformToFCDTRotation( FCDTransform* Transform )
{
	return static_cast< FCDTRotation* >( Transform );
}

inline FCDTScale* FCDTransformToFCDTScale( FCDTransform* Transform )
{
	return static_cast< FCDTScale* >( Transform );
}

inline FCDTSkew* FCDTransformToFCDTSkew( FCDTransform* Transform )
{
	return static_cast< FCDTSkew* >( Transform );
}

inline FCDTTranslation* FCDTransformToFCDTTranslation( FCDTransform* Transform )
{
	return static_cast< FCDTTranslation* >( Transform );
}





inline FCDTransform* FCDTLookAtToFCDTransform( FCDTLookAt* Transform )
{
	return static_cast< FCDTransform* >( Transform );
}

inline FCDTransform* FCDTMatrixToFCDTransform( FCDTMatrix* Transform )
{
	return static_cast< FCDTransform* >( Transform );
}

inline FCDTransform* FCDTRotationToFCDTransform( FCDTRotation* Transform )
{
	return static_cast< FCDTransform* >( Transform );
}

inline FCDTransform* FCDTScaleToFCDTransform( FCDTScale* Transform )
{
	return static_cast< FCDTransform* >( Transform );
}

inline FCDTransform* FCDTSkewToFCDTransform( FCDTSkew* Transform )
{
	return static_cast< FCDTransform* >( Transform );
}

inline FCDTransform* FCDTTranslationToFCDTransform( FCDTTranslation* Transform )
{
	return static_cast< FCDTransform* >( Transform );
}




#endif // __FCDTransform_CAST_H__ 
