%define IGNORE_RTTI(class_name)
%ignore class_name##::GetClassType;
%ignore class_name##::GetObjectType;
%enddef


%{

#include "FCollada.h"
#include "FColladaPlugin.h"
#include "FCDocument\FCDAnimated.h"
#include "FCDocument\FCDAnimation.h"
#include "FCDocument\FCDAnimationChannel.h"
#include "FCDocument\FCDAnimationClip.h"
#include "FCDocument\FCDAnimationClipTools.h"
#include "FCDocument\FCDAnimationCurve.h"
#include "FCDocument\FCDAnimationCurveTools.h"
#include "FCDocument\FCDAnimationKey.h"
#include "FCDocument\FCDAnimationMultiCurve.h"
#include "FCDocument\FCDAsset.h"
#include "FCDocument\FCDCamera.h"
#include "FCDocument\FCDController.h"
#include "FCDocument\FCDControllerInstance.h"
#include "FCDocument\FCDControllerTools.h"
#include "FCDocument\FCDEffect.h"
#include "FCDocument\FCDEffectCode.h"
#include "FCDocument\FCDEffectParameter.h"
#include "FCDocument\FCDEffectParameterFactory.h"
#include "FCDocument\FCDEffectParameterSampler.h"
#include "FCDocument\FCDEffectParameterSurface.h"
#include "FCDocument\FCDEffectPass.h"
#include "FCDocument\FCDEffectPassShader.h"
#include "FCDocument\FCDEffectPassState.h"
#include "FCDocument\FCDEffectProfile.h"
#include "FCDocument\FCDEffectProfileFX.h"
#include "FCDocument\FCDEffectStandard.h"
#include "FCDocument\FCDEffectTechnique.h"
#include "FCDocument\FCDEffectTools.h"
#include "FCDocument\FCDEmitter.h"
#include "FCDocument\FCDEmitterInstance.h"
#include "FCDocument\FCDEmitterObject.h"
#include "FCDocument\FCDEmitterParticle.h"
#include "FCDocument\FCDEntity.h"
#include "FCDocument\FCDEntityInstance.h"
#include "FCDocument\FCDEntityReference.h"
#include "FCDocument\FCDExternalReferenceManager.h"
#include "FCDocument\FCDExtra.h"
#include "FCDocument\FCDForceDeflector.h"
#include "FCDocument\FCDForceDrag.h"
#include "FCDocument\FCDForceField.h"
#include "FCDocument\FCDForceGravity.h"
#include "FCDocument\FCDForcePBomb.h"
#include "FCDocument\FCDForceTyped.h"
#include "FCDocument\FCDForceWind.h"
#include "FCDocument\FCDGeometry.h"
#include "FCDocument\FCDGeometryInstance.h"
#include "FCDocument\FCDGeometryMesh.h"
#include "FCDocument\FCDGeometryNURBSSurface.h"
#include "FCDocument\FCDGeometryPolygons.h"
#include "FCDocument\FCDGeometryPolygonsTools.h"
#include "FCDocument\FCDGeometrySource.h"
#include "FCDocument\FCDGeometrySpline.h"
#include "FCDocument\FCDImage.h"
#include "FCDocument\FCDLibrary.h"
#include "FCDocument\FCDLight.h"
#include "FCDocument\FCDMaterial.h"
#include "FCDocument\FCDMaterialInstance.h"
#include "FCDocument\FCDMorphController.h"
#include "FCDocument\FCDObject.h"
#include "FCDocument\FCDocument.h"
#include "FCDocument\FCDocumentTools.h"
#include "FCDocument\FCDParticleEmitter.h"
#include "FCDocument\FCDParticleModifier.h"
#include "FCDocument\FCDPhysicsAnalyticalGeometry.h"
#include "FCDocument\FCDPhysicsForceFieldInstance.h"
#include "FCDocument\FCDPhysicsMaterial.h"
#include "FCDocument\FCDPhysicsModel.h"
#include "FCDocument\FCDPhysicsModelInstance.h"
#include "FCDocument\FCDPhysicsRigidBody.h"
#include "FCDocument\FCDPhysicsRigidBodyInstance.h"
#include "FCDocument\FCDPhysicsRigidBodyParameters.h"
#include "FCDocument\FCDPhysicsRigidConstraint.h"
#include "FCDocument\FCDPhysicsRigidConstraintInstance.h"
#include "FCDocument\FCDPhysicsScene.h"
#include "FCDocument\FCDPhysicsShape.h"
#include "FCDocument\FCDPlaceHolder.h"
#include "FCDocument\FCDSceneNode.h"
#include "FCDocument\FCDSceneNodeIterator.h"
#include "FCDocument\FCDSceneNodeTools.h"
#include "FCDocument\FCDSkinController.h"
#include "FCDocument\FCDTargetedEntity.h"
#include "FCDocument\FCDTexture.h"
#include "FCDocument\FCDTransform.h"
#include "FColladaTest\FCTestExportImport\FCTestExportImport.h"
#include "FMath\FMAllocator.h"
#include "FMath\FMArray.h"
#include "FMath\FMArrayPointer.h"
#include "FMath\FMath.h"
#include "FMath\FMColor.h"
#include "FMath\FMFloat.h"
#include "FMath\FMInteger.h"
#include "FMath\FMInterpolation.h"
#include "FMath\FMMatrix33.h"
#include "FMath\FMMatrix44.h"
#include "FMath\FMQuaternion.h"
#include "FMath\FMRandom.h"
#include "FMath\FMSort.h"
#include "FMath\FMTree.h"
#include "FMath\FMVector2.h"
#include "FMath\FMVector3.h"
#include "FMath\FMVector4.h"
#include "FMath\FMVolume.h"
#include "FUtils\FUAssert.h"
#include "FUtils\FUBase64.h"
#include "FUtils\FUBoundingBox.h"
#include "FUtils\FUBoundingSphere.h"
#include "FUtils\FUCrc32.h"
#include "FUtils\FUDaeEnum.h"
#include "FUtils\FUDaeEnumSyntax.h"
#include "FUtils\FUDaeSyntax.h"
#include "FUtils\FUDateTime.h"
#include "FUtils\FUDebug.h"
#include "FUtils\FUError.h"
#include "FUtils\FUEvent.h"
#include "FUtils\FUFile.h"
#include "FUtils\FUFileManager.h"
#include "FUtils\FUFunctor.h"
#include "FUtils\FULogFile.h"
#include "FUtils\FUObject.h"
#include "FUtils\FUObjectType.h"
#include "FUtils\FUPlugin.h"
#include "FUtils\FUPluginManager.h"
#include "FUtils\FUSingleton.h"
#include "FUtils\FUString.h"
#include "FUtils\FUStringBuilder.h"
#include "FUtils\FUStringConversion.h"
#include "FUtils\FUTestBed.h"
#include "FUtils\FUtils.h"
#include "FUtils\FUUniqueStringMap.h"
#include "FUtils\FUUri.h"
#include "FUtils\FUXmlDocument.h"
#include "FUtils\FUXmlParser.h"
#include "FUtils\FUXmlWriter.h"
#include "FUtils\Platforms.h"


#include "FCDEntityCast.h"
#include "FCDEntityInstanceCast.h"

#pragma warning(disable:4101) //unreferenced local variable
#pragma warning(disable:4102) //unreferenced label
#pragma warning(disable:4800) //forcing int value to bool (performance warning)

// add code here

#ifdef __cplusplus
extern "C" {
#endif

#ifdef SWIGLUA
SWIGEXPORT int lua_GetResult(lua_State* L,int idx,void** ptr)
{
	return SWIG_ConvertPtr( L, idx, ptr, NULL, 0 );
}
#endif 

#ifdef __cplusplus
}
#endif

%}

typedef unsigned int size_t;

#ifdef SWIGLUA || SWIGRUBY

%rename (FUDaeInterpolation_STEP) FUDaeInterpolation::STEP;		
%rename (FUDaeInterpolation_LINEAR) FUDaeInterpolation::LINEAR;
%rename (FUDaeInterpolation_BEZIER) FUDaeInterpolation::BEZIER;
%rename (FUDaeInterpolation_TCB) FUDaeInterpolation::TCB;
%rename (FUDaeInterpolation_UNKNOWN) FUDaeInterpolation::UNKNOWN;
%rename (FUDaeInterpolation_DEFAULT) FUDaeInterpolation::DEFAULT;


%rename (FUDaeSplineType_LINEAR) FUDaeSplineType::LINEAR;
%rename (FUDaeSplineType_BEZIER) FUDaeSplineType::BEZIER;
%rename (FUDaeSplineType_NURBS) FUDaeSplineType::NURBS;
%rename (FUDaeSplineType_UNKNOWN) FUDaeSplineType::UNKNOWN;
%rename (FUDaeSplineType_DEFAULT) FUDaeSplineType::DEFAULT;

%rename (FUDaeSplineType_FromString) FUDaeSplineType::FromString(const fm::string&);
%rename (FUDaeSplineType_ToString) FUDaeSplineType::ToString(Type);

%rename (FUDaeSplineForm_OPEN) FUDaeSplineForm::OPEN;
%rename (FUDaeSplineForm_CLOSED) FUDaeSplineForm::CLOSED;
%rename (FUDaeSplineForm_UNKNOWN) FUDaeSplineForm::UNKNOWN;
%rename (FUDaeSplineForm_DEFAULT) FUDaeSplineForm::DEFAULT;

%rename (FUDaeTextureChannel_AMBIENT) FUDaeTextureChannel::AMBIENT;
%rename (FUDaeTextureChannel_BUMP) FUDaeTextureChannel::BUMP;
%rename (FUDaeTextureChannel_DIFFUSE) FUDaeTextureChannel::DIFFUSE;
%rename (FUDaeTextureChannel_DISPLACEMENT) FUDaeTextureChannel::DISPLACEMENT;
%rename (FUDaeTextureChannel_EMISSION) FUDaeTextureChannel::EMISSION;
%rename (FUDaeTextureChannel_FILTER) FUDaeTextureChannel::FILTER;
%rename (FUDaeTextureChannel_OPACITY) FUDaeTextureChannel::OPACITY;
%rename (FUDaeTextureChannel_REFLECTION) FUDaeTextureChannel::REFLECTION;
%rename (FUDaeTextureChannel_REFRACTION) FUDaeTextureChannel::REFRACTION;
%rename (FUDaeTextureChannel_SHININESS) FUDaeTextureChannel::SHININESS;
%rename (FUDaeTextureChannel_SPECULAR) FUDaeTextureChannel::SPECULAR;
%rename (FUDaeTextureChannel_SPECULAR_LEVEL) FUDaeTextureChannel::SPECULAR_LEVEL;
%rename (FUDaeTextureChannel_TRANSPARENT) FUDaeTextureChannel::TRANSPARENT;
%rename (FUDaeTextureChannel_COUNT) FUDaeTextureChannel::COUNT;
%rename (FUDaeTextureChannel_UNKNOWN) FUDaeTextureChannel::UNKNOWN;
%rename (FUDaeTextureChannel_DEFAULT) FUDaeTextureChannel::DEFAULT;

%rename (FUDaeTextureWrapMode_NONE) FUDaeTextureWrapMode::NONE;
%rename (FUDaeTextureWrapMode_WRAP) FUDaeTextureWrapMode::WRAP;
%rename (FUDaeTextureWrapMode_MIRROR) FUDaeTextureWrapMode::MIRROR;
%rename (FUDaeTextureWrapMode_CLAMP) FUDaeTextureWrapMode::CLAMP;
%rename (FUDaeTextureWrapMode_BORDER) FUDaeTextureWrapMode::BORDER;
%rename (FUDaeTextureWrapMode_UNKNOWN) FUDaeTextureWrapMode::UNKNOWN;
%rename (FUDaeTextureWrapMode_DEFAULT) FUDaeTextureWrapMode::DEFAULT;

%rename (FUDaeTextureFilterFunction_NONE) FUDaeTextureFilterFunction::NONE;
%rename (FUDaeTextureFilterFunction_NEAREST) FUDaeTextureFilterFunction::NEAREST;
%rename (FUDaeTextureFilterFunction_LINEAR) FUDaeTextureFilterFunction::LINEAR;
%rename (FUDaeTextureFilterFunction_NEAREST_MIPMAP_NEAREST) FUDaeTextureFilterFunction::NEAREST_MIPMAP_NEAREST;
%rename (FUDaeTextureFilterFunction_LINEAR_MIPMAP_NEAREST) FUDaeTextureFilterFunction::LINEAR_MIPMAP_NEAREST;
%rename (FUDaeTextureFilterFunction_NEAREST_MIPMAP_LINEAR) FUDaeTextureFilterFunction::NEAREST_MIPMAP_LINEAR;
%rename (FUDaeTextureFilterFunction_LINEAR_MIPMAP_LINEAR) FUDaeTextureFilterFunction::LINEAR_MIPMAP_LINEAR;
%rename (FUDaeTextureFilterFunction_UNKNOWN) FUDaeTextureFilterFunction::UNKNOWN;
%rename (FUDaeTextureFilterFunction_DEFAULT) FUDaeTextureFilterFunction::DEFAULT;

%rename (FUDaeMorphMethod_NORMALIZED) FUDaeMorphMethod::NORMALIZED;
%rename (FUDaeMorphMethod_RELATIVE) FUDaeMorphMethod::RELATIVE;
%rename (FUDaeMorphMethod_UNKNOWN) FUDaeMorphMethod::UNKNOWN;
%rename (FUDaeMorphMethod_DEFAULT) FUDaeMorphMethod::DEFAULT;

%rename (FUDaeInfinity_CONSTANT) FUDaeInfinity::CONSTANT;
%rename (FUDaeInfinity_LINEAR) FUDaeInfinity::LINEAR;
%rename (FUDaeInfinity_CYCLE) FUDaeInfinity::CYCLE;
%rename (FUDaeInfinity_CYCLE_RELATIVE) FUDaeInfinity::CYCLE_RELATIVE;
%rename (FUDaeInfinity_OSCILLATE) FUDaeInfinity::OSCILLATE;
%rename (FUDaeInfinity_UNKNOWN) FUDaeInfinity::UNKNOWN;
%rename (FUDaeInfinity_DEFAULT) FUDaeInfinity::DEFAULT;

%rename (FUDaeBlendMode_NONE) FUDaeBlendMode::NONE;
%rename (FUDaeBlendMode_OVER) FUDaeBlendMode::OVER;
%rename (FUDaeBlendMode_IN) FUDaeBlendMode::IN;
%rename (FUDaeBlendMode_OUT) FUDaeBlendMode::OUT;
%rename (FUDaeBlendMode_ADD) FUDaeBlendMode::ADD;
%rename (FUDaeBlendMode_SUBTRACT) FUDaeBlendMode::SUBTRACT;
%rename (FUDaeBlendMode_MULTIPLY) FUDaeBlendMode::MULTIPLY;
%rename (FUDaeBlendMode_DIFFERENCE) FUDaeBlendMode::DIFFERENCE;
%rename (FUDaeBlendMode_LIGHTEN) FUDaeBlendMode::LIGHTEN;
%rename (FUDaeBlendMode_DARKEN) FUDaeBlendMode::DARKEN;
%rename (FUDaeBlendMode_SATURATE) FUDaeBlendMode::SATURATE;
%rename (FUDaeBlendMode_DESATURATE) FUDaeBlendMode::DESATURATE;
%rename (FUDaeBlendMode_ILLUMINATE) FUDaeBlendMode::ILLUMINATE;
%rename (FUDaeBlendMode_UNKNOWN) FUDaeBlendMode::UNKNOWN;
%rename (FUDaeBlendMode_DEFAULT) FUDaeBlendMode::DEFAULT;

%rename (FUDaeGeometryInput_POSITION) FUDaeGeometryInput::POSITION;
%rename (FUDaeGeometryInput_VERTEX) FUDaeGeometryInput::VERTEX;
%rename (FUDaeGeometryInput_NORMAL) FUDaeGeometryInput::NORMAL;
%rename (FUDaeGeometryInput_GEOTANGENT) FUDaeGeometryInput::GEOTANGENT;
%rename (FUDaeGeometryInput_GEOBINORMAL) FUDaeGeometryInput::GEOBINORMAL;
%rename (FUDaeGeometryInput_TEXCOORD) FUDaeGeometryInput::TEXCOORD;
%rename (FUDaeGeometryInput_TEXTANGENT) FUDaeGeometryInput::TEXTANGENT;
%rename (FUDaeGeometryInput_TEXBINORMAL) FUDaeGeometryInput::TEXBINORMAL;
%rename (FUDaeGeometryInput_UV) FUDaeGeometryInput::UV;
%rename (FUDaeGeometryInput_COLOR) FUDaeGeometryInput::COLOR;
%rename (FUDaeGeometryInput_EXTRA) FUDaeGeometryInput::EXTRA;
%rename (FUDaeGeometryInput_POINT_SIZE) FUDaeGeometryInput::POINT_SIZE;
%rename (FUDaeGeometryInput_POINT_ROTATION) FUDaeGeometryInput::POINT_ROTATION;
%rename (FUDaeGeometryInput_UNKNOWN) FUDaeGeometryInput::UNKNOWN;

%rename (FUDaeProfileType_CG) FUDaeProfileType::CG;
%rename (FUDaeProfileType_HLSL) FUDaeProfileType::HLSL;
%rename (FUDaeProfileType_GLSL) FUDaeProfileType::GLSL;
%rename (FUDaeProfileType_GLES) FUDaeProfileType::GLES;
%rename (FUDaeProfileType_COMMON) FUDaeProfileType::COMMON;
%rename (FUDaeProfileType_UNKNOWN) FUDaeProfileType::UNKNOWN;
 
%rename (FUDaePassStateFunction_NEVER) FUDaePassStateFunction::NEVER;
%rename (FUDaePassStateFunction_LESS) FUDaePassStateFunction::LESS;
%rename (FUDaePassStateFunction_EQUAL) FUDaePassStateFunction::EQUAL;
%rename (FUDaePassStateFunction_LESS_EQUAL) FUDaePassStateFunction::LESS_EQUAL;
%rename (FUDaePassStateFunction_GREATER) FUDaePassStateFunction::GREATER;
%rename (FUDaePassStateFunction_NOT_EQUAL) FUDaePassStateFunction::NOT_EQUAL;
%rename (FUDaePassStateFunction_GREATER_EQUAL) FUDaePassStateFunction::GREATER_EQUAL;
%rename (FUDaePassStateFunction_ALWAYS) FUDaePassStateFunction::ALWAYS;
%rename (FUDaePassStateFunction_INVALID) FUDaePassStateFunction::INVALID;

%rename (FUDaePassStateStencilOperation_KEEP) FUDaePassStateStencilOperation::KEEP;
%rename (FUDaePassStateStencilOperation_ZERO) FUDaePassStateStencilOperation::ZERO;
%rename (FUDaePassStateStencilOperation_REPLACE) FUDaePassStateStencilOperation::REPLACE;
%rename (FUDaePassStateStencilOperation_INCREMENT) FUDaePassStateStencilOperation::INCREMENT;
%rename (FUDaePassStateStencilOperation_DECREMENT) FUDaePassStateStencilOperation::DECREMENT;
%rename (FUDaePassStateStencilOperation_INVERT) FUDaePassStateStencilOperation::INVERT;
%rename (FUDaePassStateStencilOperation_INCREMENT_WRAP) FUDaePassStateStencilOperation::INCREMENT_WRAP;
%rename (FUDaePassStateStencilOperation_DECREMENT_WRAP) FUDaePassStateStencilOperation::DECREMENT_WRAP;
%rename (FUDaePassStateStencilOperation_INVALID) FUDaePassStateStencilOperation::INVALID;

%rename (FUDaePassStateBlendType_ZERO) FUDaePassStateBlendType::ZERO;
%rename (FUDaePassStateBlendType_ONE) FUDaePassStateBlendType::ONE;
%rename (FUDaePassStateBlendType_SOURCE_COLOR) FUDaePassStateBlendType::SOURCE_COLOR;
%rename (FUDaePassStateBlendType_ONE_MINUS_SOURCE_COLOR) FUDaePassStateBlendType::ONE_MINUS_SOURCE_COLOR;
%rename (FUDaePassStateBlendType_DESTINATION_COLOR) FUDaePassStateBlendType::DESTINATION_COLOR;
%rename (FUDaePassStateBlendType_ONE_MINUS_DESTINATION_COLOR) FUDaePassStateBlendType::ONE_MINUS_DESTINATION_COLOR;
%rename (FUDaePassStateBlendType_SOURCE_ALPHA) FUDaePassStateBlendType::SOURCE_ALPHA;
%rename (FUDaePassStateBlendType_ONE_MINUS_SOURCE_ALPHA) FUDaePassStateBlendType::ONE_MINUS_SOURCE_ALPHA;
%rename (FUDaePassStateBlendType_DESTINATION_ALPHA) FUDaePassStateBlendType::DESTINATION_ALPHA;
%rename (FUDaePassStateBlendType_ONE_MINUS_DESTINATION_ALPHA) FUDaePassStateBlendType::ONE_MINUS_DESTINATION_ALPHA;
%rename (FUDaePassStateBlendType_CONSTANT_COLOR) FUDaePassStateBlendType::CONSTANT_COLOR;
%rename (FUDaePassStateBlendType_ONE_MINUS_CONSTANT_COLOR) FUDaePassStateBlendType::ONE_MINUS_CONSTANT_COLOR;
%rename (FUDaePassStateBlendType_CONSTANT_ALPHA) FUDaePassStateBlendType::CONSTANT_ALPHA;
%rename (FUDaePassStateBlendType_ONE_MINUS_CONSTANT_ALPHA) FUDaePassStateBlendType::ONE_MINUS_CONSTANT_ALPHA;
%rename (FUDaePassStateBlendType_SOURCE_ALPHA_SATURATE) FUDaePassStateBlendType::SOURCE_ALPHA_SATURATE;
%rename (FUDaePassStateBlendType_INVALID) FUDaePassStateBlendType::INVALID;

%rename (FUDaePassStateFaceType_FRONT) FUDaePassStateFaceType::FRONT;
%rename (FUDaePassStateFaceType_BACK) FUDaePassStateFaceType::BACK;
%rename (FUDaePassStateFaceType_FRONT_AND_BACK) FUDaePassStateFaceType::FRONT_AND_BACK;
%rename (FUDaePassStateFaceType_INVALID) FUDaePassStateFaceType::INVALID;

%rename (FUDaePassStateBlendEquation_ADD) FUDaePassStateBlendEquation::ADD;
%rename (FUDaePassStateBlendEquation_SUBTRACT) FUDaePassStateBlendEquation::SUBTRACT;
%rename (FUDaePassStateBlendEquation_REVERSE_SUBTRACT) FUDaePassStateBlendEquation::REVERSE_SUBTRACT;
%rename (FUDaePassStateBlendEquation_MIN) FUDaePassStateBlendEquation::MIN;
%rename (FUDaePassStateBlendEquation_MAX) FUDaePassStateBlendEquation::MAX;
%rename (FUDaePassStateBlendEquation_INVALID) FUDaePassStateBlendEquation::INVALID;

%rename (FUDaePassStateMaterialType_EMISSION) FUDaePassStateMaterialType::EMISSION;
%rename (FUDaePassStateMaterialType_AMBIENT) FUDaePassStateMaterialType::AMBIENT;
%rename (FUDaePassStateMaterialType_DIFFUSE) FUDaePassStateMaterialType::DIFFUSE;
%rename (FUDaePassStateMaterialType_SPECULAR) FUDaePassStateMaterialType::SPECULAR;
%rename (FUDaePassStateMaterialType_AMBIENT_AND_DIFFUSE) FUDaePassStateMaterialType::AMBIENT_AND_DIFFUSE;
%rename (FUDaePassStateMaterialType_ZINVALID) FUDaePassStateMaterialType::ZINVALID;

%rename (FUDaePassStateFogType_LINEAR) FUDaePassStateFogType::LINEAR;
%rename (FUDaePassStateFogType_EXP) FUDaePassStateFogType::EXP;
%rename (FUDaePassStateFogType_EXP2) FUDaePassStateFogType::EXP2;
%rename (FUDaePassStateFogType_INVALID) FUDaePassStateFogType::INVALID;

%rename (FUDaePassStateFogCoordinateType_FOG_COORDINATE) FUDaePassStateFogCoordinateType::FOG_COORDINATE;
%rename (FUDaePassStateFogCoordinateType_FRAGMENT_DEPTH) FUDaePassStateFogCoordinateType::FRAGMENT_DEPTH;
%rename (FUDaePassStateFogCoordinateType_INVALID) FUDaePassStateFogCoordinateType::INVALID;

%rename (FUDaePassStateFrontFaceType_CLOCKWISE) FUDaePassStateFrontFaceType::CLOCKWISE;
%rename (FUDaePassStateFrontFaceType_COUNTER_CLOCKWISE) FUDaePassStateFrontFaceType::COUNTER_CLOCKWISE;
%rename (FUDaePassStateFrontFaceType_INVALID) FUDaePassStateFrontFaceType::INVALID;

%rename (FUDaePassStateLogicOperation_CLEAR) FUDaePassStateLogicOperation::CLEAR;
%rename (FUDaePassStateLogicOperation_AND) FUDaePassStateLogicOperation::AND;
%rename (FUDaePassStateLogicOperation_AND_REVERSE) FUDaePassStateLogicOperation::AND_REVERSE;
%rename (FUDaePassStateLogicOperation_COPY) FUDaePassStateLogicOperation::COPY;
%rename (FUDaePassStateLogicOperation_AND_INVERTED) FUDaePassStateLogicOperation::AND_INVERTED;
%rename (FUDaePassStateLogicOperation_NOOP) FUDaePassStateLogicOperation::NOOP; 
%rename (FUDaePassStateLogicOperation_XOR) FUDaePassStateLogicOperation::XOR;
%rename (FUDaePassStateLogicOperation_OR) FUDaePassStateLogicOperation::OR;
%rename (FUDaePassStateLogicOperation_NOR) FUDaePassStateLogicOperation::NOR;
%rename (FUDaePassStateLogicOperation_EQUIV) FUDaePassStateLogicOperation::EQUIV;
%rename (FUDaePassStateLogicOperation_INVERT) FUDaePassStateLogicOperation::INVERT;
%rename (FUDaePassStateLogicOperation_OR_REVERSE) FUDaePassStateLogicOperation::OR_REVERSE;
%rename (FUDaePassStateLogicOperation_COPY_INVERTED) FUDaePassStateLogicOperation::COPY_INVERTED;
%rename (FUDaePassStateLogicOperation_NAND) FUDaePassStateLogicOperation::NAND;
%rename (FUDaePassStateLogicOperation_SET) FUDaePassStateLogicOperation::SET;
%rename (FUDaePassStateLogicOperation_INVALID) FUDaePassStateLogicOperation::INVALID;

%rename (FUDaePassStatePolygonMode_POINT) FUDaePassStatePolygonMode::POINT;
%rename (FUDaePassStatePolygonMode_LINE) FUDaePassStatePolygonMode::LINE;
%rename (FUDaePassStatePolygonMode_FILL) FUDaePassStatePolygonMode::FILL;
%rename (FUDaePassStatePolygonMode_INVALID) FUDaePassStatePolygonMode::INVALID;

%rename (FUDaePassStateShadeModel_FLAT) FUDaePassStateShadeModel::FLAT;
%rename (FUDaePassStateShadeModel_SMOOTH) FUDaePassStateShadeModel::SMOOTH;
%rename (FUDaePassStateShadeModel_INVALID) FUDaePassStateShadeModel::INVALID;

%rename (FUDaePassStateLightModelColorControlType_SINGLE_COLOR) FUDaePassStateLightModelColorControlType::SINGLE_COLOR;
%rename (FUDaePassStateLightModelColorControlType_SEPARATE_SPECULAR_COLOR) FUDaePassStateLightModelColorControlType::SEPARATE_SPECULAR_COLOR;
%rename (FUDaePassStateLightModelColorControlType_INVALID) FUDaePassStateLightModelColorControlType::INVALID;

%rename (FUDaePassState_ALPHA_FUNC) FUDaePassState::ALPHA_FUNC;
%rename (FUDaePassState_BLEND_FUNC) FUDaePassState::BLEND_FUNC;
%rename (FUDaePassState_BLEND_FUNC_SEPARATE) FUDaePassState::BLEND_FUNC_SEPARATE;
%rename (FUDaePassState_BLEND_EQUATION) FUDaePassState::BLEND_EQUATION;
%rename (FUDaePassState_BLEND_EQUATION_SEPARATE) FUDaePassState::BLEND_EQUATION_SEPARATE;
%rename (FUDaePassState_COLOR_MATERIAL) FUDaePassState::COLOR_MATERIAL;
%rename (FUDaePassState_CULL_FACE) FUDaePassState::CULL_FACE;
%rename (FUDaePassState_DEPTH_FUNC) FUDaePassState::DEPTH_FUNC;
%rename (FUDaePassState_FOG_MODE) FUDaePassState::FOG_MODE;
%rename (FUDaePassState_FOG_COORD_SRC) FUDaePassState::FOG_COORD_SRC;
%rename (FUDaePassState_FRONT_FACE) FUDaePassState::FRONT_FACE;
%rename (FUDaePassState_LIGHT_MODEL_COLOR_CONTROL) FUDaePassState::LIGHT_MODEL_COLOR_CONTROL;
%rename (FUDaePassState_LOGIC_OP) FUDaePassState::LOGIC_OP;
%rename (FUDaePassState_POLYGON_MODE) FUDaePassState::POLYGON_MODE;
%rename (FUDaePassState_SHADE_MODEL) FUDaePassState::SHADE_MODEL;
%rename (FUDaePassState_STENCIL_FUNC) FUDaePassState::STENCIL_FUNC;
%rename (FUDaePassState_STENCIL_OP) FUDaePassState::STENCIL_OP;
%rename (FUDaePassState_STENCIL_FUNC_SEPARATE) FUDaePassState::STENCIL_FUNC_SEPARATE;
%rename (FUDaePassState_STENCIL_OP_SEPARATE) FUDaePassState::STENCIL_OP_SEPARATE;
%rename (FUDaePassState_STENCIL_MASK_SEPARATE) FUDaePassState::STENCIL_MASK_SEPARATE;
%rename (FUDaePassState_LIGHT_ENABLE) FUDaePassState::LIGHT_ENABLE;
%rename (FUDaePassState_LIGHT_AMBIENT) FUDaePassState::LIGHT_AMBIENT;
%rename (FUDaePassState_LIGHT_DIFFUSE) FUDaePassState::LIGHT_DIFFUSE;
%rename (FUDaePassState_LIGHT_SPECULAR) FUDaePassState::LIGHT_SPECULAR;
%rename (FUDaePassState_LIGHT_POSITION) FUDaePassState::LIGHT_POSITION;
%rename (FUDaePassState_LIGHT_CONSTANT_ATTENUATION) FUDaePassState::LIGHT_CONSTANT_ATTENUATION;
%rename (FUDaePassState_LIGHT_LINEAR_ATTENUATION) FUDaePassState::LIGHT_LINEAR_ATTENUATION;
%rename (FUDaePassState_LIGHT_QUADRATIC_ATTENUATION) FUDaePassState::LIGHT_QUADRATIC_ATTENUATION;
%rename (FUDaePassState_LIGHT_SPOT_CUTOFF) FUDaePassState::LIGHT_SPOT_CUTOFF;
%rename (FUDaePassState_LIGHT_SPOT_DIRECTION) FUDaePassState::LIGHT_SPOT_DIRECTION;
%rename (FUDaePassState_LIGHT_SPOT_EXPONENT) FUDaePassState::LIGHT_SPOT_EXPONENT;
%rename (FUDaePassState_TEXTURE1D) FUDaePassState::TEXTURE1D;
%rename (FUDaePassState_TEXTURE2D) FUDaePassState::TEXTURE2D;
%rename (FUDaePassState_TEXTURE3D) FUDaePassState::TEXTURE3D;
%rename (FUDaePassState_TEXTURECUBE) FUDaePassState::TEXTURECUBE;
%rename (FUDaePassState_TEXTURERECT) FUDaePassState::TEXTURERECT;
%rename (FUDaePassState_TEXTUREDEPTH) FUDaePassState::TEXTUREDEPTH;
%rename (FUDaePassState_TEXTURE1D_ENABLE) FUDaePassState::TEXTURE1D_ENABLE;
%rename (FUDaePassState_TEXTURE2D_ENABLE) FUDaePassState::TEXTURE2D_ENABLE;
%rename (FUDaePassState_TEXTURE3D_ENABLE) FUDaePassState::TEXTURE3D_ENABLE;
%rename (FUDaePassState_TEXTURECUBE_ENABLE) FUDaePassState::TEXTURECUBE_ENABLE;
%rename (FUDaePassState_TEXTURERECT_ENABLE) FUDaePassState::TEXTURERECT_ENABLE;
%rename (FUDaePassState_TEXTUREDEPTH_ENABLE) FUDaePassState::TEXTUREDEPTH_ENABLE;
%rename (FUDaePassState_TEXTURE_ENV_COLOR) FUDaePassState::TEXTURE_ENV_COLOR;
%rename (FUDaePassState_TEXTURE_ENV_MODE) FUDaePassState::TEXTURE_ENV_MODE;
%rename (FUDaePassState_CLIP_PLANE) FUDaePassState::CLIP_PLANE;
%rename (FUDaePassState_CLIP_PLANE_ENABLE) FUDaePassState::CLIP_PLANE_ENABLE;
%rename (FUDaePassState_BLEND_COLOR) FUDaePassState::BLEND_COLOR;
%rename (FUDaePassState_CLEAR_COLOR) FUDaePassState::CLEAR_COLOR;
%rename (FUDaePassState_CLEAR_STENCIL) FUDaePassState::CLEAR_STENCIL;
%rename (FUDaePassState_CLEAR_DEPTH) FUDaePassState::CLEAR_DEPTH;
%rename (FUDaePassState_COLOR_MASK) FUDaePassState::COLOR_MASK;
%rename (FUDaePassState_DEPTH_BOUNDS) FUDaePassState::DEPTH_BOUNDS;
%rename (FUDaePassState_DEPTH_MASK) FUDaePassState::DEPTH_MASK;
%rename (FUDaePassState_DEPTH_RANGE) FUDaePassState::DEPTH_RANGE;
%rename (FUDaePassState_FOG_DENSITY) FUDaePassState::FOG_DENSITY;
%rename (FUDaePassState_FOG_START) FUDaePassState::FOG_START;
%rename (FUDaePassState_FOG_END) FUDaePassState::FOG_END;
%rename (FUDaePassState_FOG_COLOR) FUDaePassState::FOG_COLOR;
%rename (FUDaePassState_LIGHT_MODEL_AMBIENT) FUDaePassState::LIGHT_MODEL_AMBIENT;
%rename (FUDaePassState_LIGHTING_ENABLE) FUDaePassState::LIGHTING_ENABLE;
%rename (FUDaePassState_LINE_STIPPLE) FUDaePassState::LINE_STIPPLE;
%rename (FUDaePassState_LINE_WIDTH) FUDaePassState::LINE_WIDTH;
%rename (FUDaePassState_MATERIAL_AMBIENT) FUDaePassState::MATERIAL_AMBIENT;
%rename (FUDaePassState_MATERIAL_DIFFUSE) FUDaePassState::MATERIAL_DIFFUSE;
%rename (FUDaePassState_MATERIAL_EMISSION) FUDaePassState::MATERIAL_EMISSION;
%rename (FUDaePassState_MATERIAL_SHININESS) FUDaePassState::MATERIAL_SHININESS;
%rename (FUDaePassState_MATERIAL_SPECULAR) FUDaePassState::MATERIAL_SPECULAR;
%rename (FUDaePassState_MODEL_VIEW_MATRIX) FUDaePassState::MODEL_VIEW_MATRIX;
%rename (FUDaePassState_POINT_DISTANCE_ATTENUATION) FUDaePassState::POINT_DISTANCE_ATTENUATION;
%rename (FUDaePassState_POINT_FADE_THRESHOLD_SIZE) FUDaePassState::POINT_FADE_THRESHOLD_SIZE;
%rename (FUDaePassState_POINT_SIZE) FUDaePassState::POINT_SIZE;
%rename (FUDaePassState_POINT_SIZE_MIN) FUDaePassState::POINT_SIZE_MIN;
%rename (FUDaePassState_POINT_SIZE_MAX) FUDaePassState::POINT_SIZE_MAX;
%rename (FUDaePassState_POLYGON_OFFSET) FUDaePassState::POLYGON_OFFSET;
%rename (FUDaePassState_PROJECTION_MATRIX) FUDaePassState::PROJECTION_MATRIX;
%rename (FUDaePassState_SCISSOR) FUDaePassState::SCISSOR;
%rename (FUDaePassState_STENCIL_MASK) FUDaePassState::STENCIL_MASK;
%rename (FUDaePassState_ALPHA_TEST_ENABLE) FUDaePassState::ALPHA_TEST_ENABLE;
%rename (FUDaePassState_AUTO_NORMAL_ENABLE) FUDaePassState::AUTO_NORMAL_ENABLE;
%rename (FUDaePassState_BLEND_ENABLE) FUDaePassState::BLEND_ENABLE;
%rename (FUDaePassState_COLOR_LOGIC_OP_ENABLE) FUDaePassState::COLOR_LOGIC_OP_ENABLE;
%rename (FUDaePassState_COLOR_MATERIAL_ENABLE) FUDaePassState::COLOR_MATERIAL_ENABLE;
%rename (FUDaePassState_CULL_FACE_ENABLE) FUDaePassState::CULL_FACE_ENABLE;
%rename (FUDaePassState_DEPTH_BOUNDS_ENABLE) FUDaePassState::DEPTH_BOUNDS_ENABLE;
%rename (FUDaePassState_DEPTH_CLAMP_ENABLE) FUDaePassState::DEPTH_CLAMP_ENABLE;
%rename (FUDaePassState_DEPTH_TEST_ENABLE) FUDaePassState::DEPTH_TEST_ENABLE;
%rename (FUDaePassState_DITHER_ENABLE) FUDaePassState::DITHER_ENABLE;
%rename (FUDaePassState_FOG_ENABLE) FUDaePassState::FOG_ENABLE;
%rename (FUDaePassState_LIGHT_MODEL_LOCAL_VIEWER_ENABLE) FUDaePassState::LIGHT_MODEL_LOCAL_VIEWER_ENABLE;
%rename (FUDaePassState_LIGHT_MODEL_TWO_SIDE_ENABLE) FUDaePassState::LIGHT_MODEL_TWO_SIDE_ENABLE;
%rename (FUDaePassState_LINE_SMOOTH_ENABLE) FUDaePassState::LINE_SMOOTH_ENABLE;
%rename (FUDaePassState_LINE_STIPPLE_ENABLE) FUDaePassState::LINE_STIPPLE_ENABLE;
%rename (FUDaePassState_LOGIC_OP_ENABLE) FUDaePassState::LOGIC_OP_ENABLE;
%rename (FUDaePassState_MULTISAMPLE_ENABLE) FUDaePassState::MULTISAMPLE_ENABLE;
%rename (FUDaePassState_NORMALIZE_ENABLE) FUDaePassState::NORMALIZE_ENABLE;
%rename (FUDaePassState_POINT_SMOOTH_ENABLE) FUDaePassState::POINT_SMOOTH_ENABLE;
%rename (FUDaePassState_POLYGON_OFFSET_FILL_ENABLE) FUDaePassState::POLYGON_OFFSET_FILL_ENABLE;
%rename (FUDaePassState_POLYGON_OFFSET_LINE_ENABLE) FUDaePassState::POLYGON_OFFSET_LINE_ENABLE;
%rename (FUDaePassState_POLYGON_OFFSET_POINT_ENABLE) FUDaePassState::POLYGON_OFFSET_POINT_ENABLE;
%rename (FUDaePassState_POLYGON_SMOOTH_ENABLE) FUDaePassState::POLYGON_SMOOTH_ENABLE;
%rename (FUDaePassState_POLYGON_STIPPLE_ENABLE) FUDaePassState::POLYGON_STIPPLE_ENABLE;
%rename (FUDaePassState_RESCALE_NORMAL_ENABLE) FUDaePassState::RESCALE_NORMAL_ENABLE;
%rename (FUDaePassState_SAMPLE_ALPHA_TO_COVERAGE_ENABLE) FUDaePassState::SAMPLE_ALPHA_TO_COVERAGE_ENABLE;
%rename (FUDaePassState_SAMPLE_ALPHA_TO_ONE_ENABLE) FUDaePassState::SAMPLE_ALPHA_TO_ONE_ENABLE;
%rename (FUDaePassState_SAMPLE_COVERAGE_ENABLE) FUDaePassState::SAMPLE_COVERAGE_ENABLE;
%rename (FUDaePassState_SCISSOR_TEST_ENABLE) FUDaePassState::SCISSOR_TEST_ENABLE;
%rename (FUDaePassState_STENCIL_TEST_ENABLE) FUDaePassState::STENCIL_TEST_ENABLE;
%rename (FUDaePassState_COUNT) FUDaePassState::COUNT;
%rename (FUDaePassState_INVALID) FUDaePassState::INVALID;

#endif

%rename (SetDaeId_const) SetDaeId( const fm::string& );
%rename (GetVersion_const) GetVersion() const;
%rename (FindAnimatedValue_const) FindAnimatedValue(const float*) const;
%rename (__eq__) operator== (const fm::string&, const char*);
%rename (__add__) operator+ (const fm::string&, int32);

%rename (__eq__) FUObjectType::operator==(const FUObjectType& otherType) const;
%rename (__neq__) FUObjectType::operator!=(const FUObjectType& otherType) const;

// %ignore fm::vector<T,PRIMITIVE>::operator[](INTEGER index);
// %ignore vector<T,PRIMITIVE>::operator[](INTEGER index) const;
// %ignore vector<*,*>::operator==(const fm::vector<T,PRIMITIVE>& other) const;
// %ignore vector::operator =(const fm::vector<T,PRIMITIVE>& rhs);

%rename (FCDEntityInstance_Type) FCDEntityInstance::Type;
%rename (FCDEffectCode_Type) FCDEffectCode::Type;
%rename (FCDEffectParameter_Type) FCDEffectParameter::Type;
%rename (FCDEntity_Type) FCDEntity::Type;
%rename (FCDTransform_Type) FCDTransform::Type;
%rename (FUDaeSplineType_Type) FUDaeSplineType::Type;
%rename (FUDaeProfileType_Type) FUDaeProfileType::Type;
%rename (FUDaePassStateBlendType_Type) FUDaePassStateBlendType::Type;
%rename (FUDaePassStateFaceType_Type) FUDaePassStateFaceType::Type;
%rename (FUDaePassStateMaterialType_Type) FUDaePassStateMaterialType::Type;
%rename (FUDaePassStateFogType_Type) FUDaePassStateFogType::Type;
%rename (FUDaePassStateFogCoordinateType_Type) FUDaePassStateFogCoordinateType::Type;
%rename (FUDaePassStateLightModelColorControlType_Type) FUDaePassStateLightModelColorControlType::Type;

%rename (FUFile_Mode) FUFile::Mode;
%rename (FUDaeBlendMode_Mode) FUDaeBlendMode::Mode;
%rename (FUDaePassStatePolygonMode_Mode) FUDaePassStatePolygonMode::Mode;

%rename (FUDaePassStateStencilOperation_Operation) FUDaePassStateStencilOperation::Operation;
%rename (FUDaePassStateLogicOperation_Operation) FUDaePassStateLogicOperation::Operation;

%rename (FUDaeInterpolation_FromString) FUDaeInterpolation::FromString(const fm::string&);
%rename (FUDaeInterpolation_ToString) FUDaeInterpolation::ToString(Interpolation);

%rename (FUDaeSplineForm_FromString) FUDaeSplineForm::FromString(const fm::string&);
%rename (FUDaeSplineForm_ToString) FUDaeSplineForm::ToString(Form);

%rename (FUDaeTextureChannel_FromString) FUDaeTextureChannel::FromString(const fm::string&);
%ignore FUDaeTextureWrapMode::FromString(const char*);
%rename (FUDaeTextureWrapMode_FromString) FUDaeTextureWrapMode::FromString(const fm::string&);
%rename (FUDaeTextureWrapMode_ToString) FUDaeTextureWrapMode::ToString(WrapMode);

%ignore FUDaeTextureFilterFunction::FromString(const char*);
%rename (FUDaeTextureFilterFunction_FromString) FUDaeTextureFilterFunction::FromString(const fm::string&);
%rename (FUDaeTextureFilterFunction_ToString) FUDaeTextureFilterFunction::ToString(FilterFunction);

%ignore FUDaeMorphMethod::FromString(const char*);
%rename (FUDaeMorphMethod_FromString) FUDaeMorphMethod::FromString(const fm::string&);
%rename (FUDaeMorphMethod_ToString) FUDaeMorphMethod::ToString(Method);

%ignore FUDaeInfinity::FromString(const char*);
%rename (FUDaeInfinity_FromString) FUDaeInfinity::FromString(const fm::string&);
%rename (FUDaeInfinity_ToString) FUDaeInfinity::ToString(Infinity);

%ignore FUDaeBlendMode::FromString(const char*);
%rename (FUDaeBlendMode_FromString) FUDaeBlendMode::FromString(const fm::string&);
%rename (FUDaeBlendMode_ToString) FUDaeBlendMode::ToString(Mode);

%ignore FUDaeGeometryInput::FromString(const char*);
%rename (FUDaeGeometryInput_FromString) FUDaeGeometryInput::FromString(const fm::string&);
%rename (FUDaeGeometryInput_ToString) FUDaeGeometryInput::ToString(Semantic);

%ignore FUDaeProfileType::FromString(const char*); 
%rename (FUDaeProfileType_FromString) FUDaeProfileType::FromString(const fm::string&);
%rename (FUDaeProfileType_ToString) FUDaeProfileType::ToString(Type);

%ignore FUDaePassStateFunction::FromString(const char*);
%rename (FUDaePassStateFunction_FromString) FUDaePassStateFunction::FromString(const fm::string&);
%rename (FUDaePassStateFunction_ToString) FUDaePassStateFunction::ToString(Function);

%ignore FUDaePassStateStencilOperation::FromString(const char*);
%rename (FUDaePassStateStencilOperation_FromString) FUDaePassStateStencilOperation::FromString(const fm::string&);
%rename (FUDaePassStateStencilOperation_ToString) FUDaePassStateStencilOperation::ToString(Operation);

%ignore FUDaePassStateBlendType::FromString(const char*);
%rename (FUDaePassStateBlendType_FromString) FUDaePassStateBlendType::FromString(const fm::string&);
%rename (FUDaePassStateBlendType_ToString) FUDaePassStateBlendType::ToString(Type);

%ignore FUDaePassStateFaceType::FromString(const char*);
%rename (FUDaePassStateFaceType_FromString) FUDaePassStateFaceType::FromString(const fm::string&);
%rename (FUDaePassStateFaceType_ToString) FUDaePassStateFaceType::ToString(Type);

%ignore FUDaePassStateBlendEquation::FromString(const char*);
%rename (FUDaePassStateBlendEquation_FromString) FUDaePassStateBlendEquation::FromString(const fm::string&);
%rename (FUDaePassStateBlendEquation_ToString) FUDaePassStateBlendEquation::ToString(Equation);

%ignore FUDaePassStateMaterialType::FromString(const char*);
%rename (FUDaePassStateMaterialType_FromString) FUDaePassStateMaterialType::FromString(const fm::string&);
%rename (FUDaePassStateMaterialType_ToString) FUDaePassStateMaterialType::ToString(Type);

%ignore FUDaePassStateFogType::FromString(const char*);
%rename (FUDaePassStateFogType_FromString) FUDaePassStateFogType::FromString(const fm::string&);
%rename (FUDaePassStateFogType_ToString) FUDaePassStateFogType::ToString(Type type);

%ignore FUDaePassStateFogCoordinateType::FromString(const char*);
%rename (FUDaePassStateFogCoordinateType_FromString) FUDaePassStateFogCoordinateType::FromString(const fm::string&);
%rename (FUDaePassStateFogCoordinateType_ToString) FUDaePassStateFogCoordinateType::ToString(Type);

%ignore FUDaePassStateFrontFaceType::FromString(const char*);
%rename (FUDaePassStateFrontFaceType_FromString) FUDaePassStateFrontFaceType::FromString(const fm::string&);
%rename (FUDaePassStateFrontFaceType_ToString) FUDaePassStateFrontFaceType::ToString(Type);

%ignore FUDaePassStateLogicOperation::FromString(const char*);
%rename (FUDaePassStateLogicOperation_FromString) FUDaePassStateLogicOperation::FromString(const fm::string&);
%rename (FUDaePassStateLogicOperation_ToString) FUDaePassStateLogicOperation::ToString(Operation);

%ignore FUDaePassStatePolygonMode::FromString(const char*);
%rename (FUDaePassStatePolygonMode_FromString) FUDaePassStatePolygonMode::FromString(const fm::string&);
%rename (FUDaePassStatePolygonMode_ToString) FUDaePassStatePolygonMode::ToString(Mode);

%ignore FUDaePassStateShadeModel::FromString(const char*);
%rename (FUDaePassStateShadeModel_FromString) FUDaePassStateShadeModel::FromString(const fm::string&);
%rename (FUDaePassStateShadeModel_ToString) FUDaePassStateShadeModel::ToString(Model);

%ignore FUDaePassStateLightModelColorControlType::FromString(const char*);
%rename (FUDaePassStateLightModelColorControlType_FromString) FUDaePassStateLightModelColorControlType::FromString(const fm::string&);
%rename (FUDaePassStateLightModelColorControlType_ToString) FUDaePassStateLightModelColorControlType::ToString(Type);

%ignore FUDaePassState::FromString(const char*);
%rename ( FUDaePassState_FromString) FUDaePassState::FromString(const fm::string&);
%rename ( FUDaePassState_ToString) FUDaePassState::ToString(State);

%ignore FUDaeAccessor::XY;
%ignore FUDaeAccessor::XYZW;
%ignore FUDaeAccessor::RGBA;
%ignore FUDaeAccessor::STPQ;

//The following methods from the FCDocument class have been marked as depricated in the
//FCollada libraries.
%ignore GetVisualSceneRoot();
%ignore GetVisualSceneRoot() const;
%ignore GetPhysicsSceneRoot(size_t index = 0);
%ignore GetPhysicsSceneRoot(size_t index = 0) const;

//Ignore the global empty string variables
%ignore emptyString;
%ignore emptyCharString;
%ignore emptyFCharString;
%ignore emptyFString;

%ignore InsertHandler;
%ignore *::InsertHandler;

IGNORE_RTTI(FUObject);
IGNORE_RTTI(FCDObject);
IGNORE_RTTI(FCDObjectWithId);
IGNORE_RTTI(FCDEntity);
IGNORE_RTTI(FCDExtra);
IGNORE_RTTI(FCDEType);
IGNORE_RTTI(FCDENode);
IGNORE_RTTI(FCDETechnique);
IGNORE_RTTI(FCDEAttribute);
IGNORE_RTTI(FCDController);
IGNORE_RTTI(FCDocument);
IGNORE_RTTI(FCDSceneNode);
IGNORE_RTTI(FCDEntityInstance);
IGNORE_RTTI(FCDGeometryInstance);
IGNORE_RTTI(FCDControllerInstance);
IGNORE_RTTI(FCDMaterialInstance);
IGNORE_RTTI(FCDAnimationChannel);

%ignore FCDEAttribute::FCDEAttribute;

%ignore FCDObjectWithId::MAX_ID_LENGTH;

%ignore FCDGeometrySource::Clone();

%include "FUtils/Platforms.h"
%include "FUtils/FUObject.h"
%include "FUtils/FUObjectType.h"
%include "FUtils/FUString.h"
%include "FUtils/FUUri.h"
%include "FMath/FMArray.h"
%include "FMath/FMSort.h"

%template (charvector)		fm::vector<char, true>;
%template (fstring)			fm::stringT<char>;
%template (charcomparator)	fm::comparator<char>;

// FCDocument::FindAnimationChannels(...) has a parameter of type FCDAnimationChannelList which is a private typedef
// of the class, which causes problems for SWIG's wrappers since they can't instantiate it.
%ignore FindAnimationChannels(const fm::string& pointer, FCDAnimationChannelList& channels);

%ignore *::LinkTarget();
%ignore *::__classType;

%rename (FCollada_Release) FCollada::Release(void* buffer);
%include "FCollada.h"

// *********************************************************************************************
// FMath
// *********************************************************************************************
%rename (fm_Release) fm::Release(void* buffer);
%include "FMath/FMAllocator.h"
%include "FMath/FMArray.h"
%include "FMath/FMArrayPointer.h"
%include "FMath/FMath.h"
%ignore FMColor::operator uint8*();
%ignore operator*(float s, const FMColor& c);
%include "FMath/FMColor.h"
%include "FMath/FMFloat.h"
%include "FMath/FMInteger.h"
%rename (FMInterpolation_Interpolation) FMInterpolation::Interpolation;
%include "FMath/FMInterpolation.h"
// %include "FMath/FMMatrix33.h"
// %include "FMath/FMMatrix44.h"
// %include "FMath/FMQuaternion.h"
%include "FMath/FMRandom.h"
%include "FMath/FMSort.h"
// %include "FMath/FMTree.h"
// %include "FMath/FMVector2.h"
// %include "FMath/FMVector3.h"
// %include "FMath/FMVector4.h"
%include "FMath/FMVolume.h"


// *********************************************************************************************
// FUtils
// *********************************************************************************************
%include "FUtils/FUAssert.h"
%include "FUtils/FUBase64.h"
%ignore FUBoundingBox::Infinity;
%include "FUtils/FUBoundingBox.h"
%include "FUtils/FUBoundingSphere.h"
%ignore FUCrc32::CRC32(const fstring& text);
%ignore FUCrc32::CRC32(const fchar* text);
%ignore FUCrc32::CRC32(const fm::string& text);
%include "FUtils/FUCrc32.h"
%include "FUtils/FUDaeEnum.h"
%include "FUtils/FUDaeEnumSyntax.h"
%include "FUtils/FUDaeSyntax.h"
%include "FUtils/FUDateTime.h"
// The next line causes linker problems with FUDebug::logFile;
// %include "FUtils/FUDebug.h"
// The next line causes linker problems with FUError::fatalLevel;
// %include "FUtils/FUError.h"
%ignore FUEvent0::operator()();
%include "FUtils/FUEvent.h"
%include "FUtils/FUFile.h"
%include "FUtils/FUFileManager.h"
%include "FUtils/FUFunctor.h"
%include "FUtils/FULogFile.h"
%include "FUtils/FUObject.h"
%include "FUtils/FUObjectType.h"
%include "FUtils/FUPlugin.h"
%include "FUtils/FUPluginManager.h"
%include "FUtils/FUSingleton.h"
%include "FUtils/FUString.h"
// The next line causes linker problems
// %include "FUtils/FUStringBuilder.h"
%include "FUtils/FUStringConversion.h"
// The next line causes linker problems
// %include "FUtils/FUTestBed.h"
%include "FUtils/FUtils.h"
%include "FUtils/FUUniqueStringMap.h"
%include "FUtils/FUUri.h"
%include "FUtils/FUXmlDocument.h"
%include "FUtils/FUXmlParser.h"
%include "FUtils/FUXmlWriter.h"
%include "FUtils/Platforms.h"

%rename (FUDaeInterpolation_Interpolation) FUDaeInterpolation::Interpolation;
%include "FUtils/FUDaeEnum.h"
%include "FCDocument/FCDLibrary.h"
%include "FCDocument/FCDObject.h"
%template (FColladaDocumentAnimationLibrary) FCDLibrary<FCDAnimation>;
%template (FColladaDocumentAnimationClipLibrary) FCDLibrary<FCDAnimationClip>;
%template (FColladaDocumentCameraLibrary) FCDLibrary<FCDCamera>;
%template (FColladaDocumentControllerLibrary) FCDLibrary<FCDController>;
%template (FColladaDocumentEffectLibrary) FCDLibrary<FCDEffect>;
%template (FColladaDocumentEmitterLibrary) FCDLibrary<FCDEmitter>;
%template (FColladaDocumentForceFieldLibrary) FCDLibrary<FCDForceField>;
%template (FColladaDocumentGeometryLibrary) FCDLibrary<FCDGeometry>;
%template (FColladaDocumentImageLibrary) FCDLibrary<FCDImage>;
%template (FColladaDocumentLightLibrary) FCDLibrary<FCDLight>;
%template (FColladaDocumentMaterialLibrary) FCDLibrary<FCDMaterial>;
%template (FColladaDocumentVisualSceneNodeLibrary) FCDLibrary<FCDSceneNode>;
%template (FColladaDocumentPhysicsModelLibrary) FCDLibrary<FCDPhysicsModel>;
%template (FColladaDocumentPhysicsMaterialLibrary) FCDLibrary<FCDPhysicsMaterial>;
%template (FColladaDocumentPhysicsSceneLibrary) FCDLibrary<FCDPhysicsScene>;
%include "FCDocument/FCDEntity.h"
%include "FCDocument/FCDExtra.h"
%include "FCDocument/FCDController.h"
%include "FCDocument/FCDocument.h"
%include "FCDocument/FCDObject.h"
%template (VectorOfVoidPtrs) fm::vector<const void*, true>;
// %template (FColladaDocumentSceneNodePVector) fm::pvector<FCDSceneNode>;
// %template (FColladaDocumentEntityInstancePVector) fm::pvector<FCDEntityInstance>;
// %template (FColladaDocumentSceneNodeTrackList) FUObjectList<FCDSceneNode>;
// %template (FColladaDocumentEntityInstanceObjectList) FUObjectList<FCDEntityInstance>;
// %template (FColladaDocumentEntityInstanceContainer) FUObjectContainer<FCDEntityInstance>;
%ignore FUObjectTracker::TracksObject(const FUObject* object) const;
%ignore FUObjectTracker::TracksObject(FUObject* object);

%ignore FUObjectContainer<FCDTransform>::Add();
%ignore fm::pvector<FCDTransform>::pvector(size_t size, const FCDTransform& defaultValue);
%ignore FUObjectList<FCDTransform>::TracksObject(const FUObject* object) const;
%ignore FUObjectList<FCDTransform>::TracksObject(FUObject* object);
%template (FColladaDocumentTransformPVector) fm::pvector<FCDTransform>;
%template (FColladaDocumentTransformObjectList) FUObjectList<FCDTransform>;
%template (FColladaDocumentTransformContainer) FUObjectContainer<FCDTransform>;

%ignore FUObjectContainer<FCDEntityInstance>::Add();
%ignore fm::pvector<FCDEntityInstance>::pvector(size_t size, const FCDEntityInstance& defaultValue);
%ignore FUObjectList<FCDEntityInstance>::TracksObject(const FUObject* object) const;
%ignore FUObjectList<FCDEntityInstance>::TracksObject(FUObject* object);
%template (FColladaDocumentEntityInstancePVector) fm::pvector<FCDEntityInstance>;
%template (FColladaDocumentEntityInstanceObjectList) FUObjectList<FCDEntityInstance>;
%template (FColladaDocumentEntityInstanceContainer) FUObjectContainer<FCDEntityInstance>;

%include "FCDocument/FCDSceneNode.h"
%include "FCDocument/FCDEntityInstance.h"
%include "FCDocument/FCDGeometryInstance.h"
%include "FCDocument/FCDGeometryMesh.h"
%include "FCDocument/FCDGeometrySource.h"
%include "FCDocument/FCDGeometry.h"
%include "FCDocument/FCDControllerInstance.h"
%include "FCDocument/FCDMaterialInstance.h"
%include "FCDocument/FCDAnimationChannel.h"
%include "FCDocument/FCDTargetedEntity.h"

// %include "FCollada.h"
// %include "FColladaPlugin.h"
%ignore FCDAnimatedPoint3::Create;
%include "FCDocument/FCDAnimated.h"
%ignore FCDAnimation::ReleaseChild;
%include "FCDocument/FCDAnimation.h"
%include "FCDocument/FCDAnimationChannel.h"
%include "FCDocument/FCDAnimationClip.h"
%include "FCDocument/FCDAnimationClipTools.h"
// %include "FCDocument/FCDAnimationCurve.h"
%ignore FCDAnimationCurveTools::Average;
%ignore FCDAnimationCurveTools::TakeFirst;
%include "FCDocument/FCDAnimationCurveTools.h"
%ignore FCDAnimationMKey::FCDAnimationMKey;
%ignore FCDAnimationMKeyTCB::FCDAnimationMKeyTCB;
%ignore FCDAnimationMKeyBezier::FCDAnimationMKeyBezier;
%include "FCDocument/FCDAnimationKey.h"
// The next line causes linker problems I can't fix
// %include "FCDocument/FCDAnimationMultiCurve.h"
%include "FCDocument/FCDAsset.h"
%include "FCDocument/FCDCamera.h"
%include "FCDocument/FCDControllerInstance.h"
%include "FCDocument/FCDControllerTools.h"
%include "FCDocument/FCDEffect.h"
%include "FCDocument/FCDEffectCode.h"
%include "FCDocument/FCDEffectParameter.h"
%include "FCDocument/FCDEffectParameterFactory.h"
%include "FCDocument/FCDEffectParameterSampler.h"
%include "FCDocument/FCDEffectParameterSurface.h"
%include "FCDocument/FCDEffectPass.h"
%include "FCDocument/FCDEffectPassShader.h"
%include "FCDocument/FCDEffectPassState.h"
%include "FCDocument/FCDEffectProfile.h"
%include "FCDocument/FCDEffectProfileFX.h"
%ignore FCDEffectStandard::AmbientColorSemantic;
%ignore FCDEffectStandard::DiffuseColorSemantic;
%ignore FCDEffectStandard::EmissionColorSemantic;
%ignore FCDEffectStandard::EmissionFactorSemantic;
%ignore FCDEffectStandard::IndexOfRefractionSemantic;
%ignore FCDEffectStandard::ReflectivityColorSemantic;
%ignore FCDEffectStandard::ReflectivityFactorSemantic;
%ignore FCDEffectStandard::ShininessSemantic;
%ignore FCDEffectStandard::SpecularFactorSemantic;
%ignore FCDEffectStandard::TranslucencyColorSemantic;
%ignore FCDEffectStandard::TranslucencyFactorSemantic;
%ignore FCDEffectStandard::SpecularColorSemantic;
%include "FCDocument/FCDEffectStandard.h"
%include "FCDocument/FCDEffectTechnique.h"
%include "FCDocument/FCDEffectTools.h"
%include "FCDocument/FCDEmitter.h"
%include "FCDocument/FCDEmitterInstance.h"
%include "FCDocument/FCDEmitterObject.h"
%include "FCDocument/FCDEmitterParticle.h"
%include "FCDocument/FCDEntity.h"
%include "FCDocument/FCDEntityInstance.h"
%include "FCDocument/FCDEntityReference.h"
%include "FCDocument/FCDExternalReferenceManager.h"
%include "FCDocument/FCDExtra.h"
%include "FCDocument/FCDForceDeflector.h"
%include "FCDocument/FCDForceDrag.h"
%include "FCDocument/FCDForceField.h"
%include "FCDocument/FCDForceGravity.h"
%include "FCDocument/FCDForcePBomb.h"
%include "FCDocument/FCDForceTyped.h"
%include "FCDocument/FCDForceWind.h"
%include "FCDocument/FCDGeometry.h"
%include "FCDocument/FCDGeometryInstance.h"
%include "FCDocument/FCDGeometryMesh.h"
%include "FCDocument/FCDGeometryNURBSSurface.h"
%include "FCDocument/FCDGeometryPolygons.h"
%include "FCDocument/FCDGeometryPolygonsTools.h"
%include "FCDocument/FCDGeometrySource.h"
%include "FCDocument/FCDGeometrySpline.h"
%include "FCDocument/FCDImage.h"
%include "FCDocument/FCDLight.h"
%include "FCDocument/FCDMaterial.h"
%include "FCDocument/FCDMaterialInstance.h"
%include "FCDocument/FCDMorphController.h"
%include "FCDocument/FCDObject.h"
%include "FCDocument/FCDocument.h"
// The next line causes linker problems with Vector3
// %include "FCDocument/FCDocumentTools.h"
%include "FCDocument/FCDParticleEmitter.h"
%include "FCDocument/FCDParticleModifier.h"
// // %include "FCDocument/FCDPhysicsAnalyticalGeometry.h"
// %include "FCDocument/FCDPhysicsForceFieldInstance.h"
// %include "FCDocument/FCDPhysicsMaterial.h"
// %include "FCDocument/FCDPhysicsModel.h"
// %include "FCDocument/FCDPhysicsModelInstance.h"
// %include "FCDocument/FCDPhysicsRigidBody.h"
// %include "FCDocument/FCDPhysicsRigidBodyInstance.h"
// %include "FCDocument/FCDPhysicsRigidBodyParameters.h"
// %include "FCDocument/FCDPhysicsRigidConstraint.h"
// %include "FCDocument/FCDPhysicsRigidConstraintInstance.h"
// %include "FCDocument/FCDPhysicsScene.h"
// %include "FCDocument/FCDPhysicsShape.h"
%include "FCDocument/FCDPlaceHolder.h"
%include "FCDocument/FCDSceneNode.h"
%include "FCDocument/FCDSceneNodeIterator.h"
%include "FCDocument/FCDSceneNodeTools.h"
// The next line causes linker problems with FMMatrix44::Identity;
// %include "FCDocument/FCDSkinController.h"
%include "FCDocument/FCDTexture.h"
%include "FCDocument/FCDTransform.h"
// %include "FColladaTest/FCTestExportImport/FCTestExportImport.h"
%include "FCDEntityCast.h"
%include "FCDEntityInstanceCast.h"

#ifdef SWIGLUA
ADD_CLASS_P_CTOR (FCDocument);
ADD_CLASS_P_CTOR (FCDSceneNode);
ADD_CLASS_P_CTOR (FCDGeometry); 
#endif

