//FCDEntityInstanceCast.h

#ifndef __FCDENTITYINSTANCE_CAST_H__
#define __FCDENTITYINSTANCE_CAST_H__

inline FCDControllerInstance* FCDEntityInstanceToFCDControllerInstance( FCDEntityInstance* instance )
{
	return static_cast<FCDControllerInstance*>( instance );
}

inline FCDGeometryInstance* FCDEntityInstanceToFCDGeometryInstance( FCDEntityInstance* instance )
{
	return static_cast<FCDGeometryInstance*>( instance );
}

inline FCDMaterialInstance* FCDEntityInstanceToFCDMaterialInstance( FCDEntityInstance* instance )
{
	return static_cast<FCDMaterialInstance*>( instance );
}


inline FCDEntityInstance* FCDControllerInstanceToFCDEntityInstance( FCDControllerInstance* instance )
{
	return static_cast<FCDEntityInstance*>( instance );
}

inline FCDEntityInstance* FCDGeometryInstanceToFCDEntityInstance( FCDGeometryInstance* instance )
{
	return static_cast<FCDEntityInstance*>( instance );
}

inline FCDEntityInstance* FCDMaterialInstanceToFCDEntityInstance( FCDMaterialInstance* instance )
{
	return static_cast<FCDEntityInstance*>( instance );
}





#endif //__FCDINSTANCE_CAST_H__

