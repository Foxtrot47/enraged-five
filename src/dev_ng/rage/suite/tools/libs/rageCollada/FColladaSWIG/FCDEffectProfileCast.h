#ifndef __FCDEffectProfile_CAST_H__
#define __FCDEffectProfile_CAST_H__

#define CreateFCDEffectProfileCastFunctionsForType(destination_type)	\
inline destination_type* FCDEffectProfileTo##destination_type( FCDEffectProfile* EffectProfile )	\
{																			\
	return static_cast< destination_type* >( EffectProfile );						\
}																			\
	inline FCDEffectProfile* ##destination_type##ToFCDEffectProfile( destination_type* EffectProfile )	\
{																			\
	return static_cast< FCDEffectProfile* >( EffectProfile );						\
}


CreateFCDEffectProfileCastFunctionsForType(FCDEffectStandard)
CreateFCDEffectProfileCastFunctionsForType(FCDEffectProfileFX)

#endif // __FCDEffectProfile_CAST_H__ 
