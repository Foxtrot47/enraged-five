
#define REMOVE_DEPRECATED_METHODS 1

#ifdef SWIGRUBY
	%module fcolladaruby
#else
	#ifdef SWIGCSHARP
		%module FColladaCSharp
	#else
		#ifdef SWIGPYTHON
			%module FColladaPython
		#else
			%module FCollada
		#endif
	#endif
#endif

#define WIN32

#ifdef SWIGLUA
%include "FColladaLua/class_p.i"
#endif

%ignore _WIN32_WINNT;

%define IGNORE_RTTI(class_name)
%ignore class_name##::GetClassType;
%ignore class_name##::GetObjectType;
%enddef

//WARNINGS being ignored
//	302 = "Identifier redefined" (occurs due to duplicate tyepdefs in header files)
//	312 = "Nested class not currently supported"
//	319 = "No access specifier given for base class"
//	362 = "operator="
//	365 = "operator+="
//	366 = "operator-="
//  367 = "operator*="
//	368 = "operator/="
//	378 = "operator!="
//	383 = "operator++"
//	389 = "operator[]"
//	503 = "Can't wrap operator unless renamed to a valid identifier"
//	801 = "Wrong constant name"
#pragma SWIG nowarn=302,312,319,362,365,366,367,368,378,383,389,503,801

%{

//The FCollada headers define both vsnprintf and snprintf, so collisions can occur
//with other code that defines these macros (e.g. Ruby headers), so undef them
//before including the FCollada headers
#ifdef vsnprintf
	#undef vsnprintf
#endif

#ifdef snprintf
	#undef snprintf
#endif

#include "FUtils\Platforms.h"
#include "FUtils\FUtils.h"
#include "FUtils\FUAssert.h"		// And references these classes FUStaticFunctor1

#include "FMath\FMAllocator.h"
#include "FMath\FMFloat.h"
#include "FMath\FMSort.h"		// Defines these classes comparator, icomparator, pcomparator
#include "FMath\FMArray.h"		// And references these classes comparator
#include "FMath\FMVector2.h"		// Defines these classes FMVector2
#include "FMath\FMVector3.h"
#include "FMath\FMVector4.h"
#include "FMath\FMAngleAxis.h"		// Defines these classes FMAngleAxis
#include "FMath\FMArrayPointer.h"		// Defines these classes pvector
#include "FMath\FMath.h"
#include "FMath\FMColor.h"		// Defines these classes FMColor
#include "FMath\FMInteger.h"
#include "FMath\FMInterpolation.h"
#include "FMath\FMLookAt.h"		// Defines these classes FMLookAt
#include "FMath\FMMatrix44.h"
#include "FMath\FMQuaternion.h"		// Defines these classes FMQuaternion
#include "FMath\FMRandom.h"
#include "FMath\FMSkew.h"		// Defines these classes FMSkew
#include "FMath\FMTree.h"		// Defines these classes pair, tree, node, iterator, const_iterator
#include "FMath\FMVolume.h"
#include "FMath\FMMatrix33.h"		// Defines these classes FMMatrix33		// And references these classes FMVector2

#include "FUtils\FUBase64.h"
#include "FUtils\FUBoundingBox.h"		// Defines these classes FUBoundingBox
#include "FUtils\FUBoundingSphere.h"		// Defines these classes FUBoundingSphere
#include "FUtils\FUCrc32.h"
#include "FUtils\FUCriticalSection.h"		// Defines these classes FUCriticalSection
#include "FUtils\FUDaeEnumSyntax.h"
#include "FUtils\FUDaeSyntax.h"
#include "FUtils\FUDateTime.h"		// Defines these classes FUDateTime
#include "FUtils\FUDebug.h"		// Defines these classes FUDebug
#include "FUtils\FUErrorLog.h"		// Defines these classes FUErrorLog
#include "FUtils\FUFile.h"		// Defines these classes FUFile
#include "FUtils\FUFunctor.h"		// Defines these classes IFunctor0, FUFunctor0, FUStaticFunctor0, IFunctor1, FUFunctor1, FUStaticFunctor1, IFunctor2, FUFunctor2, FUStaticFunctor2, IFunctor3, FUFunctor3, FUStaticFunctor3
#include "FUtils\FULogFile.h"		// Defines these classes FULogFile
#include "FUtils\FUObject.h"		// Defines these classes FUObject, FUObjectOwner, FUObjectRef, FUObjectContainer
#include "FUtils\FUObjectType.h"		// Defines these classes FUObjectType
#include "FUtils\FUPluginManager.h"		// Defines these classes FUPluginManager		// And references these classes FUObjectContainer
#include "FUtils\FUSemaphore.h"		// Defines these classes FUSemaphore, FUBinarySemaphore
#include "FUtils\FUSingleton.h"
#include "FUtils\FUString.h"		// Defines these classes stringT
#include "FUtils\FUStringBuilder.h"		// Defines these classes FUStringBuilderT
#include "FUtils\FUSynchronizableObject.h"		// Defines these classes FUSynchronizableObject		// And references these classes FUCriticalSection
#include "FUtils\FUThread.h"		// Defines these classes FUThread
#include "FUtils\FUTracker.h"		// Defines these classes FUTrackable, FUTracker, FUTrackedPtr, FUTrackedList		// And references these classes FUObject
#include "FUtils\FUUniqueStringMap.h"		// Defines these classes FUUniqueStringMapT
#include "FUtils\FUUri.h"		// Defines these classes FUUri
#include "FUtils\FUXmlDocument.h"		// Defines these classes FUXmlDocument
#include "FUtils\FUEvent.h"		// Defines these classes FUEvent0, FUEvent1, FUEvent2, FUEvent3		// And references these classes IFunctor0, FUFunctor0, FUStaticFunctor0, IFunctor1, FUStaticFunctor1, IFunctor2, FUFunctor2, FUStaticFunctor2, IFunctor3, FUFunctor3, FUStaticFunctor3
#include "FUtils\FUFileManager.h"		// Defines these classes FUFileManager		// And references these classes IFunctor1, IFunctor2, IFunctor3
#include "FUtils\FUParameter.h"		// Defines these classes FUParameterT, Parameter_##parameterName		// And references these classes FMVector2, FUTrackedPtr, FUObjectRef, FUTrackedList, FUObjectContainer
#include "FUtils\FUParameterizable.h"		// Defines these classes FUParameterizable		// And references these classes FUTrackable
#include "FUtils\FUPlugin.h"		// Defines these classes FUPlugin		// And references these classes FUTrackable
#include "FUtils\FUStringConversion.h"		// Defines these classes FUStringConversion		// And references these classes FMVector2, FUStringBuilderT
#include "FUtils\FUXmlParser.h"		// And references these classes node
#include "FUtils\FUXmlWriter.h"		// And references these classes node
#include "FUtils\FUError.h"		// Defines these classes FUError, FUErrorSimpleHandler		// And references these classes IFunctor3, FUCriticalSection, FUEvent3, FUFunctor3, FUStaticFunctor3
#include "FUtils\FUDaeEnum.h"

#include "FCDocument\FCDAnimationClipTools.h"
#include "FCDocument\FCDAnimationCurveTools.h"
#include "FCDocument\FCDAnimationKey.h"		// Defines these classes FCDAnimationKey, FCDAnimationKeyBezier, FCDAnimationKeyTCB, FCDAnimationMKey, FCDAnimationMKeyBezier, FCDAnimationMKeyTCB		// And references these classes FMVector2
#include "FCDocument\FCDControllerTools.h"
#include "FCDocument\FCDEffectParameterFactory.h"		// Defines these classes FCDEffectParameterFactory
#include "FCDocument\FCDEmitterObject.h"
#include "FCDocument\FCDEmitterParticle.h"
#include "FCDocument\FCDForceDeflector.h"
#include "FCDocument\FCDForceDrag.h"
#include "FCDocument\FCDForceGravity.h"
#include "FCDocument\FCDForcePBomb.h"
#include "FCDocument\FCDForceTyped.h"
#include "FCDocument\FCDForceWind.h"
#include "FCDocument\FCDGeometryNURBSSurface.h"
#include "FCDocument\FCDGeometryPolygonsTools.h"
#include "FCDocument\FCDLightTools.h"
#include "FCDocument\FCDocument.h"		// Defines these classes FCDLayer, FCDocument		// And references these classes FUObjectRef, tree
#include "FCDocument\FCDocumentTools.h"
#include "FCDocument\FCDParameterAnimatable.h"		// Defines these classes FCDParameterAnimatable, FCDParameterAnimatableT, FCDParameterListAnimatable, FCDParameterListAnimatableT, Parameter_##parameterName, Parameter_##parameterName		// And references these classes FUObjectRef, FMVector2, FMAngleAxis, FMLookAt, FMSkew, FUObjectContainer
#include "FCDocument\FCDParticleEmitter.h"
#include "FCDocument\FCDParticleModifier.h"
#include "FCDocument\FCDSceneNodeIterator.h"		// Defines these classes FCDSceneNodeIteratorT
#include "FCDocument\FCDSceneNodeTools.h"
#include "FCDocument\FCDVersion.h"		// Defines these classes FCDVersion
#include "FCollada.h"		// And references these classes IFunctor0
#include "FCDocument\FCDObject.h"		// Defines these classes FCDObject		// And references these classes FUParameterizable
#include "FCDocument\FCDObjectWithId.h"		// Defines these classes FCDObjectWithId		// And references these classes FCDObject
#include "FCDocument\FCDPhysicsRigidBodyParameters.h"		// Defines these classes FCDPhysicsRigidBodyParameters		// And references these classes FCDObject, FUTrackedPtr, FMAngleAxis
#include "FCDocument\FCDPlaceHolder.h"		// Defines these classes FCDPlaceHolder		// And references these classes FCDObject, FUTracker, FUTrackedList
#include "FCDocument\FCDSkinController.h"		// Defines these classes FCDSkinControllerVertex, FCDSkinControllerJoint, FCDSkinController		// And references these classes FCDObject, FUObjectRef
#include "FCDocument\FCDTransform.h"		// Defines these classes FCDTransform, FCDTTranslation, FCDTScale, FCDTRotation, FCDTMatrix, FCDTLookAt, FCDTSkew, FCDTFactory		// And references these classes FCDObject, FMAngleAxis, FMQuaternion, FMLookAt, FMSkew
#include "FColladaPlugin.h"		// Defines these classes FCPExtraTechnique, FColladaPluginManager		// And references these classes FUPlugin, FUObject, FUObjectContainer
#include "FCDocument\FCDAnimated.h"		// Defines these classes FCDAnimated, FCDAnimatedCustom		// And references these classes FUTrackedList, FCDObject, FUTracker, node
#include "FCDocument\FCDAnimationChannel.h"		// Defines these classes FCDAnimationChannel		// And references these classes FCDObject
#include "FCDocument\FCDAnimationCurve.h"		// Defines these classes FCDAnimationCurve, FCDConversionFunctor, FCDConversionScaleFunctor, FCDConversionOffsetFunctor		// And references these classes FCDObject, FUTrackedPtr
#include "FCDocument\FCDAnimationMultiCurve.h"		// Defines these classes FCDAnimationMultiCurve		// And references these classes FCDObject
#include "FCDocument\FCDAsset.h"		// Defines these classes FCDAsset, FCDAssetContributor		// And references these classes FCDObject, FUDateTime
#include "FCDocument\FCDEffectCode.h"		// Defines these classes FCDEffectCode		// And references these classes FCDObject
#include "FCDocument\FCDEffectParameter.h"		// Defines these classes FCDEffectParameter, FCDEffectParameterT, FCDEffectParameterAnimatableT, FCDEffectParameterAnnotation		// And references these classes FCDObject, FCDParameterAnimatableT, FMVector2, FUParameterizable
#include "FCDocument\FCDEffectParameterSampler.h"		// Defines these classes FCDEffectParameterSampler		// And references these classes FCDEffectParameter
#include "FCDocument\FCDEffectParameterSurface.h"		// Defines these classes FCDEffectParameterSurface, FCDEffectParameterSurfaceInitFactory, FCDEffectParameterSurfaceInit, FCDEffectParameterSurfaceInitCube, FCDEffectParameterSurfaceInitVolume, FCDEffectParameterSurfaceInitFrom, FCDEffectParameterSurfaceInitAsNull, FCDEffectParameterSurfaceInitAsTarget, FCDEffectParameterSurfaceInitPlanar		// And references these classes FCDEffectParameter
#include "FCDocument\FCDEffectPass.h"		// Defines these classes FCDEffectPass		// And references these classes FCDObject
#include "FCDocument\FCDEffectPassShader.h"		// Defines these classes FCDEffectPassBind, FCDEffectPassShader		// And references these classes FCDObject
#include "FCDocument\FCDEffectPassState.h"		// Defines these classes FCDEffectPassState		// And references these classes FCDObject
#include "FCDocument\FCDEffectProfile.h"		// Defines these classes FCDEffectProfile		// And references these classes FCDObject
#include "FCDocument\FCDEffectProfileFX.h"		// Defines these classes FCDEffectProfileFX		// And references these classes FCDEffectProfile
#include "FCDocument\FCDEffectStandard.h"		// Defines these classes FCDEffectStandard		// And references these classes FCDEffectProfile
#include "FCDocument\FCDEffectTechnique.h"		// Defines these classes FCDEffectTechnique		// And references these classes FCDObject
#include "FCDocument\FCDEffectTools.h"		// And references these classes FCDEffectParameter
#include "FCDocument\FCDEntity.h"		// Defines these classes FCDEntity		// And references these classes FCDObjectWithId
#include "FCDocument\FCDEntityReference.h"		// Defines these classes FCDEntityReference		// And references these classes FCDObject, FUTracker, FUUri
#include "FCDocument\FCDExternalReferenceManager.h"		// Defines these classes FCDExternalReferenceManager		// And references these classes FCDObject, FUObjectContainer
#include "FCDocument\FCDExtra.h"		// Defines these classes FCDExtra, FCDEType, FCDENode, FCDETechnique, FCDEAttribute		// And references these classes FCDObject, FUTrackable, FUParameterizable
#include "FCDocument\FCDForceField.h"		// Defines these classes FCDForceField		// And references these classes FCDEntity
#include "FCDocument\FCDGeometry.h"		// Defines these classes FCDGeometry		// And references these classes FCDEntity
#include "FCDocument\FCDGeometryMesh.h"		// Defines these classes FCDGeometryMesh		// And references these classes FCDObject
#include "FCDocument\FCDGeometryPolygons.h"		// Defines these classes FCDGeometryPolygons		// And references these classes FCDObject
#include "FCDocument\FCDGeometryPolygonsInput.h"		// Defines these classes FCDGeometryPolygonsInput		// And references these classes FCDObject, FUTracker
#include "FCDocument\FCDGeometrySource.h"		// Defines these classes FCDGeometrySource		// And references these classes FCDObjectWithId, FUObjectContainer, FCDAnimated
#include "FCDocument\FCDGeometrySpline.h"		// Defines these classes FCDSpline, FCDLinearSpline, FCDBezierSpline, FCDNURBSSpline, FCDGeometrySpline		// And references these classes FCDObject, FUObjectContainer
#include "FCDocument\FCDImage.h"		// Defines these classes FCDImage		// And references these classes FCDEntity
#include "FCDocument\FCDLibrary.h"		// Defines these classes FCDLibrary		// And references these classes FCDObject
#include "FCDocument\FCDMaterial.h"		// Defines these classes FCDMaterialTechniqueHint, FCDMaterial		// And references these classes FCDEntity
#include "FCDocument\FCDMorphController.h"		// Defines these classes FCDMorphTarget, FCDMorphController		// And references these classes FCDObject, FCDEntity
#include "FCDocument\FCDPhysicsAnalyticalGeometry.h"		// Defines these classes FCDPhysicsAnalyticalGeometry, FCDPASBox, FCDPASPlane, FCDPASSphere, FCDPASCylinder, FCDPASCapsule, FCDPASTaperedCapsule, FCDPASTaperedCylinder, FCDPASFactory		// And references these classes FCDEntity, FMVector2
#include "FCDocument\FCDPhysicsMaterial.h"		// Defines these classes FCDPhysicsMaterial		// And references these classes FCDEntity
#include "FCDocument\FCDPhysicsModel.h"		// Defines these classes FCDPhysicsModel		// And references these classes FUObjectContainer, FCDEntity
#include "FCDocument\FCDPhysicsRigidBody.h"		// Defines these classes FCDPhysicsRigidBody		// And references these classes FCDEntity
#include "FCDocument\FCDPhysicsRigidConstraint.h"		// Defines these classes FCDPhysicsRigidConstraint		// And references these classes FUObjectContainer, FCDEntity, FUTrackedPtr
#include "FCDocument\FCDPhysicsScene.h"		// Defines these classes FCDPhysicsScene		// And references these classes FUObjectContainer, FCDEntity
#include "FCDocument\FCDPhysicsShape.h"		// Defines these classes FCDPhysicsShape		// And references these classes FUObjectContainer, FCDTransform, FCDObject, FUTrackedPtr, FUObjectRef
#include "FCDocument\FCDSceneNode.h"		// Defines these classes FCDSceneNode		// And references these classes FCDEntity
#include "FCDocument\FCDTargetedEntity.h"		// Defines these classes FCDTargetedEntity		// And references these classes FCDEntity
#include "FCDocument\FCDTexture.h"		// Defines these classes FCDTexture		// And references these classes FCDObject, FCDExtra
#include "FCDocument\FCDAnimation.h"		// Defines these classes FCDAnimation		// And references these classes FCDEntity
#include "FCDocument\FCDAnimationClip.h"		// Defines these classes FCDAnimationClip		// And references these classes FUTrackedList, FCDEntity
#include "FCDocument\FCDCamera.h"		// Defines these classes FCDCamera		// And references these classes FCDTargetedEntity
#include "FCDocument\FCDController.h"		// Defines these classes FCDController		// And references these classes FCDEntity
#include "FCDocument\FCDEffect.h"		// Defines these classes FCDEffect		// And references these classes FCDEntity
#include "FCDocument\FCDEmitter.h"		// Defines these classes FCDEmitter		// And references these classes FCDEntity
#include "FCDocument\FCDEntityInstance.h"		// Defines these classes FCDEntityInstance, FCDEntityInstanceFactory		// And references these classes FCDObject, FUTracker, FCDExtra, node, FCDEntity
#include "FCDocument\FCDGeometryInstance.h"		// Defines these classes FCDGeometryInstance		// And references these classes FCDEntityInstance
#include "FCDocument\FCDLight.h"		// Defines these classes FCDLight		// And references these classes FCDTargetedEntity
#include "FCDocument\FCDMaterialInstance.h"		// Defines these classes FCDMaterialInstanceBind, FCDMaterialInstanceBindVertexInput, FCDMaterialInstance		// And references these classes FUParameterizable, FCDEntityInstance
#include "FCDocument\FCDPhysicsForceFieldInstance.h"		// Defines these classes FCDPhysicsForceFieldInstance		// And references these classes FCDEntityInstance
#include "FCDocument\FCDPhysicsModelInstance.h"		// Defines these classes FCDPhysicsModelInstance		// And references these classes FCDEntityInstance
#include "FCDocument\FCDPhysicsRigidBodyInstance.h"		// Defines these classes FCDPhysicsRigidBodyInstance		// And references these classes FCDEntityInstance
#include "FCDocument\FCDPhysicsRigidConstraintInstance.h"		// Defines these classes FCDPhysicsRigidConstraintInstance		// And references these classes FCDEntityInstance
#include "FCDocument\FCDControllerInstance.h"		// Defines these classes FCDControllerInstance		// And references these classes FUTrackedList, FCDGeometryInstance
#include "FCDocument\FCDEmitterInstance.h"		// Defines these classes FCDEmitterInstance		// And references these classes FCDEntityInstance

#include "FCDEffectProfileCast.h"
#include "FCDEntityCast.h"
#include "FCDEntityDeletion.h"
#include "FCDEntityInstanceCast.h"
#include "FCDGeometryPolygonsInputDeletion.h"
#include "FCDTransformCast.h"

#include "Rtfs_wrap.h"

#pragma warning(disable:4101) //unreferenced local variable
#pragma warning(disable:4102) //unreferenced label
#pragma warning(disable:4800) //forcing int value to bool (performance warning)

// add code here

#ifdef __cplusplus
extern "C" {
#endif

#ifdef SWIGLUA
SWIGEXPORT int lua_GetResult(lua_State* L,int idx,void** ptr)
{
	return SWIG_ConvertPtr( L, idx, ptr, NULL, 0 );
}
#endif 

#ifdef __cplusplus
}
#endif

%}

typedef unsigned int size_t;

#ifdef SWIGLUA
#define RENAME_STUFF_FOR_CLASSLESS_LANGUAGES
#endif
#ifdef SWIGPYTHON
#define RENAME_STUFF_FOR_CLASSLESS_LANGUAGES
#endif
#ifdef SWIGRUBY
#define RENAME_STUFF_FOR_CLASSLESS_LANGUAGES
#endif

#ifdef RENAME_STUFF_FOR_CLASSLESS_LANGUAGES

%rename (FUDaeInterpolation_STEP) FUDaeInterpolation::STEP;		
%rename (FUDaeInterpolation_LINEAR) FUDaeInterpolation::LINEAR;
%rename (FUDaeInterpolation_BEZIER) FUDaeInterpolation::BEZIER;
%rename (FUDaeInterpolation_TCB) FUDaeInterpolation::TCB;
%rename (FUDaeInterpolation_UNKNOWN) FUDaeInterpolation::UNKNOWN;
%rename (FUDaeInterpolation_DEFAULT) FUDaeInterpolation::DEFAULT;


%rename (FUDaeSplineType_LINEAR) FUDaeSplineType::LINEAR;
%rename (FUDaeSplineType_BEZIER) FUDaeSplineType::BEZIER;
%rename (FUDaeSplineType_NURBS) FUDaeSplineType::NURBS;
%rename (FUDaeSplineType_UNKNOWN) FUDaeSplineType::UNKNOWN;
%rename (FUDaeSplineType_DEFAULT) FUDaeSplineType::DEFAULT;

%rename (FUDaeSplineType_FromString) FUDaeSplineType::FromString(const fm::string&);
%rename (FUDaeSplineType_ToString) FUDaeSplineType::ToString(Type);

%rename (FUDaeSplineForm_OPEN) FUDaeSplineForm::OPEN;
%rename (FUDaeSplineForm_CLOSED) FUDaeSplineForm::CLOSED;
%rename (FUDaeSplineForm_UNKNOWN) FUDaeSplineForm::UNKNOWN;
%rename (FUDaeSplineForm_DEFAULT) FUDaeSplineForm::DEFAULT;

%rename (FUDaeTextureChannel_AMBIENT) FUDaeTextureChannel::AMBIENT;
%rename (FUDaeTextureChannel_BUMP) FUDaeTextureChannel::BUMP;
%rename (FUDaeTextureChannel_DIFFUSE) FUDaeTextureChannel::DIFFUSE;
%rename (FUDaeTextureChannel_DISPLACEMENT) FUDaeTextureChannel::DISPLACEMENT;
%rename (FUDaeTextureChannel_EMISSION) FUDaeTextureChannel::EMISSION;
%rename (FUDaeTextureChannel_FILTER) FUDaeTextureChannel::FILTER;
%rename (FUDaeTextureChannel_OPACITY) FUDaeTextureChannel::OPACITY;
%rename (FUDaeTextureChannel_REFLECTION) FUDaeTextureChannel::REFLECTION;
%rename (FUDaeTextureChannel_REFRACTION) FUDaeTextureChannel::REFRACTION;
%rename (FUDaeTextureChannel_SHININESS) FUDaeTextureChannel::SHININESS;
%rename (FUDaeTextureChannel_SPECULAR) FUDaeTextureChannel::SPECULAR;
%rename (FUDaeTextureChannel_SPECULAR_LEVEL) FUDaeTextureChannel::SPECULAR_LEVEL;
%rename (FUDaeTextureChannel_TRANSPARENT) FUDaeTextureChannel::TRANSPARENT;
%rename (FUDaeTextureChannel_COUNT) FUDaeTextureChannel::COUNT;
%rename (FUDaeTextureChannel_UNKNOWN) FUDaeTextureChannel::UNKNOWN;
%rename (FUDaeTextureChannel_DEFAULT) FUDaeTextureChannel::DEFAULT;

%rename (FUDaeTextureWrapMode_NONE) FUDaeTextureWrapMode::NONE;
%rename (FUDaeTextureWrapMode_WRAP) FUDaeTextureWrapMode::WRAP;
%rename (FUDaeTextureWrapMode_MIRROR) FUDaeTextureWrapMode::MIRROR;
%rename (FUDaeTextureWrapMode_CLAMP) FUDaeTextureWrapMode::CLAMP;
%rename (FUDaeTextureWrapMode_BORDER) FUDaeTextureWrapMode::BORDER;
%rename (FUDaeTextureWrapMode_UNKNOWN) FUDaeTextureWrapMode::UNKNOWN;
%rename (FUDaeTextureWrapMode_DEFAULT) FUDaeTextureWrapMode::DEFAULT;

%rename (FUDaeTextureFilterFunction_NONE) FUDaeTextureFilterFunction::NONE;
%rename (FUDaeTextureFilterFunction_NEAREST) FUDaeTextureFilterFunction::NEAREST;
%rename (FUDaeTextureFilterFunction_LINEAR) FUDaeTextureFilterFunction::LINEAR;
%rename (FUDaeTextureFilterFunction_NEAREST_MIPMAP_NEAREST) FUDaeTextureFilterFunction::NEAREST_MIPMAP_NEAREST;
%rename (FUDaeTextureFilterFunction_LINEAR_MIPMAP_NEAREST) FUDaeTextureFilterFunction::LINEAR_MIPMAP_NEAREST;
%rename (FUDaeTextureFilterFunction_NEAREST_MIPMAP_LINEAR) FUDaeTextureFilterFunction::NEAREST_MIPMAP_LINEAR;
%rename (FUDaeTextureFilterFunction_LINEAR_MIPMAP_LINEAR) FUDaeTextureFilterFunction::LINEAR_MIPMAP_LINEAR;
%rename (FUDaeTextureFilterFunction_UNKNOWN) FUDaeTextureFilterFunction::UNKNOWN;
%rename (FUDaeTextureFilterFunction_DEFAULT) FUDaeTextureFilterFunction::DEFAULT;

%rename (FUDaeMorphMethod_NORMALIZED) FUDaeMorphMethod::NORMALIZED;
%rename (FUDaeMorphMethod_RELATIVE) FUDaeMorphMethod::RELATIVE;
%rename (FUDaeMorphMethod_UNKNOWN) FUDaeMorphMethod::UNKNOWN;
%rename (FUDaeMorphMethod_DEFAULT) FUDaeMorphMethod::DEFAULT;

%rename (FUDaeInfinity_CONSTANT) FUDaeInfinity::CONSTANT;
%rename (FUDaeInfinity_LINEAR) FUDaeInfinity::LINEAR;
%rename (FUDaeInfinity_CYCLE) FUDaeInfinity::CYCLE;
%rename (FUDaeInfinity_CYCLE_RELATIVE) FUDaeInfinity::CYCLE_RELATIVE;
%rename (FUDaeInfinity_OSCILLATE) FUDaeInfinity::OSCILLATE;
%rename (FUDaeInfinity_UNKNOWN) FUDaeInfinity::UNKNOWN;
%rename (FUDaeInfinity_DEFAULT) FUDaeInfinity::DEFAULT;

%rename (FUDaeBlendMode_NONE) FUDaeBlendMode::NONE;
%rename (FUDaeBlendMode_OVER) FUDaeBlendMode::OVER;
%rename (FUDaeBlendMode_IN) FUDaeBlendMode::IN;
%rename (FUDaeBlendMode_OUT) FUDaeBlendMode::OUT;
%rename (FUDaeBlendMode_ADD) FUDaeBlendMode::ADD;
%rename (FUDaeBlendMode_SUBTRACT) FUDaeBlendMode::SUBTRACT;
%rename (FUDaeBlendMode_MULTIPLY) FUDaeBlendMode::MULTIPLY;
%rename (FUDaeBlendMode_DIFFERENCE) FUDaeBlendMode::DIFFERENCE;
%rename (FUDaeBlendMode_LIGHTEN) FUDaeBlendMode::LIGHTEN;
%rename (FUDaeBlendMode_DARKEN) FUDaeBlendMode::DARKEN;
%rename (FUDaeBlendMode_SATURATE) FUDaeBlendMode::SATURATE;
%rename (FUDaeBlendMode_DESATURATE) FUDaeBlendMode::DESATURATE;
%rename (FUDaeBlendMode_ILLUMINATE) FUDaeBlendMode::ILLUMINATE;
%rename (FUDaeBlendMode_UNKNOWN) FUDaeBlendMode::UNKNOWN;
%rename (FUDaeBlendMode_DEFAULT) FUDaeBlendMode::DEFAULT;

%rename (FUDaeGeometryInput_POSITION) FUDaeGeometryInput::POSITION;
%rename (FUDaeGeometryInput_VERTEX) FUDaeGeometryInput::VERTEX;
%rename (FUDaeGeometryInput_NORMAL) FUDaeGeometryInput::NORMAL;
%rename (FUDaeGeometryInput_GEOTANGENT) FUDaeGeometryInput::GEOTANGENT;
%rename (FUDaeGeometryInput_GEOBINORMAL) FUDaeGeometryInput::GEOBINORMAL;
%rename (FUDaeGeometryInput_TEXCOORD) FUDaeGeometryInput::TEXCOORD;
%rename (FUDaeGeometryInput_TEXTANGENT) FUDaeGeometryInput::TEXTANGENT;
%rename (FUDaeGeometryInput_TEXBINORMAL) FUDaeGeometryInput::TEXBINORMAL;
%rename (FUDaeGeometryInput_UV) FUDaeGeometryInput::UV;
%rename (FUDaeGeometryInput_COLOR) FUDaeGeometryInput::COLOR;
%rename (FUDaeGeometryInput_EXTRA) FUDaeGeometryInput::EXTRA;
%rename (FUDaeGeometryInput_POINT_SIZE) FUDaeGeometryInput::POINT_SIZE;
%rename (FUDaeGeometryInput_POINT_ROTATION) FUDaeGeometryInput::POINT_ROTATION;
%rename (FUDaeGeometryInput_UNKNOWN) FUDaeGeometryInput::UNKNOWN;

%rename (FUDaeProfileType_CG) FUDaeProfileType::CG;
%rename (FUDaeProfileType_HLSL) FUDaeProfileType::HLSL;
%rename (FUDaeProfileType_GLSL) FUDaeProfileType::GLSL;
%rename (FUDaeProfileType_GLES) FUDaeProfileType::GLES;
%rename (FUDaeProfileType_COMMON) FUDaeProfileType::COMMON;
%rename (FUDaeProfileType_UNKNOWN) FUDaeProfileType::UNKNOWN;
 
%rename (FUDaePassStateFunction_NEVER) FUDaePassStateFunction::NEVER;
%rename (FUDaePassStateFunction_LESS) FUDaePassStateFunction::LESS;
%rename (FUDaePassStateFunction_EQUAL) FUDaePassStateFunction::EQUAL;
%rename (FUDaePassStateFunction_LESS_EQUAL) FUDaePassStateFunction::LESS_EQUAL;
%rename (FUDaePassStateFunction_GREATER) FUDaePassStateFunction::GREATER;
%rename (FUDaePassStateFunction_NOT_EQUAL) FUDaePassStateFunction::NOT_EQUAL;
%rename (FUDaePassStateFunction_GREATER_EQUAL) FUDaePassStateFunction::GREATER_EQUAL;
%rename (FUDaePassStateFunction_ALWAYS) FUDaePassStateFunction::ALWAYS;
%rename (FUDaePassStateFunction_INVALID) FUDaePassStateFunction::INVALID;

%rename (FUDaePassStateStencilOperation_KEEP) FUDaePassStateStencilOperation::KEEP;
%rename (FUDaePassStateStencilOperation_ZERO) FUDaePassStateStencilOperation::ZERO;
%rename (FUDaePassStateStencilOperation_REPLACE) FUDaePassStateStencilOperation::REPLACE;
%rename (FUDaePassStateStencilOperation_INCREMENT) FUDaePassStateStencilOperation::INCREMENT;
%rename (FUDaePassStateStencilOperation_DECREMENT) FUDaePassStateStencilOperation::DECREMENT;
%rename (FUDaePassStateStencilOperation_INVERT) FUDaePassStateStencilOperation::INVERT;
%rename (FUDaePassStateStencilOperation_INCREMENT_WRAP) FUDaePassStateStencilOperation::INCREMENT_WRAP;
%rename (FUDaePassStateStencilOperation_DECREMENT_WRAP) FUDaePassStateStencilOperation::DECREMENT_WRAP;
%rename (FUDaePassStateStencilOperation_INVALID) FUDaePassStateStencilOperation::INVALID;

%rename (FUDaePassStateBlendType_ZERO) FUDaePassStateBlendType::ZERO;
%rename (FUDaePassStateBlendType_ONE) FUDaePassStateBlendType::ONE;
%rename (FUDaePassStateBlendType_SOURCE_COLOR) FUDaePassStateBlendType::SOURCE_COLOR;
%rename (FUDaePassStateBlendType_ONE_MINUS_SOURCE_COLOR) FUDaePassStateBlendType::ONE_MINUS_SOURCE_COLOR;
%rename (FUDaePassStateBlendType_DESTINATION_COLOR) FUDaePassStateBlendType::DESTINATION_COLOR;
%rename (FUDaePassStateBlendType_ONE_MINUS_DESTINATION_COLOR) FUDaePassStateBlendType::ONE_MINUS_DESTINATION_COLOR;
%rename (FUDaePassStateBlendType_SOURCE_ALPHA) FUDaePassStateBlendType::SOURCE_ALPHA;
%rename (FUDaePassStateBlendType_ONE_MINUS_SOURCE_ALPHA) FUDaePassStateBlendType::ONE_MINUS_SOURCE_ALPHA;
%rename (FUDaePassStateBlendType_DESTINATION_ALPHA) FUDaePassStateBlendType::DESTINATION_ALPHA;
%rename (FUDaePassStateBlendType_ONE_MINUS_DESTINATION_ALPHA) FUDaePassStateBlendType::ONE_MINUS_DESTINATION_ALPHA;
%rename (FUDaePassStateBlendType_CONSTANT_COLOR) FUDaePassStateBlendType::CONSTANT_COLOR;
%rename (FUDaePassStateBlendType_ONE_MINUS_CONSTANT_COLOR) FUDaePassStateBlendType::ONE_MINUS_CONSTANT_COLOR;
%rename (FUDaePassStateBlendType_CONSTANT_ALPHA) FUDaePassStateBlendType::CONSTANT_ALPHA;
%rename (FUDaePassStateBlendType_ONE_MINUS_CONSTANT_ALPHA) FUDaePassStateBlendType::ONE_MINUS_CONSTANT_ALPHA;
%rename (FUDaePassStateBlendType_SOURCE_ALPHA_SATURATE) FUDaePassStateBlendType::SOURCE_ALPHA_SATURATE;
%rename (FUDaePassStateBlendType_INVALID) FUDaePassStateBlendType::INVALID;

%rename (FUDaePassStateFaceType_FRONT) FUDaePassStateFaceType::FRONT;
%rename (FUDaePassStateFaceType_BACK) FUDaePassStateFaceType::BACK;
%rename (FUDaePassStateFaceType_FRONT_AND_BACK) FUDaePassStateFaceType::FRONT_AND_BACK;
%rename (FUDaePassStateFaceType_INVALID) FUDaePassStateFaceType::INVALID;

%rename (FUDaePassStateBlendEquation_ADD) FUDaePassStateBlendEquation::ADD;
%rename (FUDaePassStateBlendEquation_SUBTRACT) FUDaePassStateBlendEquation::SUBTRACT;
%rename (FUDaePassStateBlendEquation_REVERSE_SUBTRACT) FUDaePassStateBlendEquation::REVERSE_SUBTRACT;
%rename (FUDaePassStateBlendEquation_MIN) FUDaePassStateBlendEquation::MIN;
%rename (FUDaePassStateBlendEquation_MAX) FUDaePassStateBlendEquation::MAX;
%rename (FUDaePassStateBlendEquation_INVALID) FUDaePassStateBlendEquation::INVALID;

%rename (FUDaePassStateMaterialType_EMISSION) FUDaePassStateMaterialType::EMISSION;
%rename (FUDaePassStateMaterialType_AMBIENT) FUDaePassStateMaterialType::AMBIENT;
%rename (FUDaePassStateMaterialType_DIFFUSE) FUDaePassStateMaterialType::DIFFUSE;
%rename (FUDaePassStateMaterialType_SPECULAR) FUDaePassStateMaterialType::SPECULAR;
%rename (FUDaePassStateMaterialType_AMBIENT_AND_DIFFUSE) FUDaePassStateMaterialType::AMBIENT_AND_DIFFUSE;
%rename (FUDaePassStateMaterialType_ZINVALID) FUDaePassStateMaterialType::ZINVALID;

%rename (FUDaePassStateFogType_LINEAR) FUDaePassStateFogType::LINEAR;
%rename (FUDaePassStateFogType_EXP) FUDaePassStateFogType::EXP;
%rename (FUDaePassStateFogType_EXP2) FUDaePassStateFogType::EXP2;
%rename (FUDaePassStateFogType_INVALID) FUDaePassStateFogType::INVALID;

%rename (FUDaePassStateFogCoordinateType_FOG_COORDINATE) FUDaePassStateFogCoordinateType::FOG_COORDINATE;
%rename (FUDaePassStateFogCoordinateType_FRAGMENT_DEPTH) FUDaePassStateFogCoordinateType::FRAGMENT_DEPTH;
%rename (FUDaePassStateFogCoordinateType_INVALID) FUDaePassStateFogCoordinateType::INVALID;

%rename (FUDaePassStateFrontFaceType_CLOCKWISE) FUDaePassStateFrontFaceType::CLOCKWISE;
%rename (FUDaePassStateFrontFaceType_COUNTER_CLOCKWISE) FUDaePassStateFrontFaceType::COUNTER_CLOCKWISE;
%rename (FUDaePassStateFrontFaceType_INVALID) FUDaePassStateFrontFaceType::INVALID;

%rename (FUDaePassStateLogicOperation_CLEAR) FUDaePassStateLogicOperation::CLEAR;
%rename (FUDaePassStateLogicOperation_AND) FUDaePassStateLogicOperation::AND;
%rename (FUDaePassStateLogicOperation_AND_REVERSE) FUDaePassStateLogicOperation::AND_REVERSE;
%rename (FUDaePassStateLogicOperation_COPY) FUDaePassStateLogicOperation::COPY;
%rename (FUDaePassStateLogicOperation_AND_INVERTED) FUDaePassStateLogicOperation::AND_INVERTED;
%rename (FUDaePassStateLogicOperation_NOOP) FUDaePassStateLogicOperation::NOOP; 
%rename (FUDaePassStateLogicOperation_XOR) FUDaePassStateLogicOperation::XOR;
%rename (FUDaePassStateLogicOperation_OR) FUDaePassStateLogicOperation::OR;
%rename (FUDaePassStateLogicOperation_NOR) FUDaePassStateLogicOperation::NOR;
%rename (FUDaePassStateLogicOperation_EQUIV) FUDaePassStateLogicOperation::EQUIV;
%rename (FUDaePassStateLogicOperation_INVERT) FUDaePassStateLogicOperation::INVERT;
%rename (FUDaePassStateLogicOperation_OR_REVERSE) FUDaePassStateLogicOperation::OR_REVERSE;
%rename (FUDaePassStateLogicOperation_COPY_INVERTED) FUDaePassStateLogicOperation::COPY_INVERTED;
%rename (FUDaePassStateLogicOperation_NAND) FUDaePassStateLogicOperation::NAND;
%rename (FUDaePassStateLogicOperation_SET) FUDaePassStateLogicOperation::SET;
%rename (FUDaePassStateLogicOperation_INVALID) FUDaePassStateLogicOperation::INVALID;

%rename (FUDaePassStatePolygonMode_POINT) FUDaePassStatePolygonMode::POINT;
%rename (FUDaePassStatePolygonMode_LINE) FUDaePassStatePolygonMode::LINE;
%rename (FUDaePassStatePolygonMode_FILL) FUDaePassStatePolygonMode::FILL;
%rename (FUDaePassStatePolygonMode_INVALID) FUDaePassStatePolygonMode::INVALID;

%rename (FUDaePassStateShadeModel_FLAT) FUDaePassStateShadeModel::FLAT;
%rename (FUDaePassStateShadeModel_SMOOTH) FUDaePassStateShadeModel::SMOOTH;
%rename (FUDaePassStateShadeModel_INVALID) FUDaePassStateShadeModel::INVALID;

%rename (FUDaePassStateLightModelColorControlType_SINGLE_COLOR) FUDaePassStateLightModelColorControlType::SINGLE_COLOR;
%rename (FUDaePassStateLightModelColorControlType_SEPARATE_SPECULAR_COLOR) FUDaePassStateLightModelColorControlType::SEPARATE_SPECULAR_COLOR;
%rename (FUDaePassStateLightModelColorControlType_INVALID) FUDaePassStateLightModelColorControlType::INVALID;

%rename (FUDaePassState_ALPHA_FUNC) FUDaePassState::ALPHA_FUNC;
%rename (FUDaePassState_BLEND_FUNC) FUDaePassState::BLEND_FUNC;
%rename (FUDaePassState_BLEND_FUNC_SEPARATE) FUDaePassState::BLEND_FUNC_SEPARATE;
%rename (FUDaePassState_BLEND_EQUATION) FUDaePassState::BLEND_EQUATION;
%rename (FUDaePassState_BLEND_EQUATION_SEPARATE) FUDaePassState::BLEND_EQUATION_SEPARATE;
%rename (FUDaePassState_COLOR_MATERIAL) FUDaePassState::COLOR_MATERIAL;
%rename (FUDaePassState_CULL_FACE) FUDaePassState::CULL_FACE;
%rename (FUDaePassState_DEPTH_FUNC) FUDaePassState::DEPTH_FUNC;
%rename (FUDaePassState_FOG_MODE) FUDaePassState::FOG_MODE;
%rename (FUDaePassState_FOG_COORD_SRC) FUDaePassState::FOG_COORD_SRC;
%rename (FUDaePassState_FRONT_FACE) FUDaePassState::FRONT_FACE;
%rename (FUDaePassState_LIGHT_MODEL_COLOR_CONTROL) FUDaePassState::LIGHT_MODEL_COLOR_CONTROL;
%rename (FUDaePassState_LOGIC_OP) FUDaePassState::LOGIC_OP;
%rename (FUDaePassState_POLYGON_MODE) FUDaePassState::POLYGON_MODE;
%rename (FUDaePassState_SHADE_MODEL) FUDaePassState::SHADE_MODEL;
%rename (FUDaePassState_STENCIL_FUNC) FUDaePassState::STENCIL_FUNC;
%rename (FUDaePassState_STENCIL_OP) FUDaePassState::STENCIL_OP;
%rename (FUDaePassState_STENCIL_FUNC_SEPARATE) FUDaePassState::STENCIL_FUNC_SEPARATE;
%rename (FUDaePassState_STENCIL_OP_SEPARATE) FUDaePassState::STENCIL_OP_SEPARATE;
%rename (FUDaePassState_STENCIL_MASK_SEPARATE) FUDaePassState::STENCIL_MASK_SEPARATE;
%rename (FUDaePassState_LIGHT_ENABLE) FUDaePassState::LIGHT_ENABLE;
%rename (FUDaePassState_LIGHT_AMBIENT) FUDaePassState::LIGHT_AMBIENT;
%rename (FUDaePassState_LIGHT_DIFFUSE) FUDaePassState::LIGHT_DIFFUSE;
%rename (FUDaePassState_LIGHT_SPECULAR) FUDaePassState::LIGHT_SPECULAR;
%rename (FUDaePassState_LIGHT_POSITION) FUDaePassState::LIGHT_POSITION;
%rename (FUDaePassState_LIGHT_CONSTANT_ATTENUATION) FUDaePassState::LIGHT_CONSTANT_ATTENUATION;
%rename (FUDaePassState_LIGHT_LINEAR_ATTENUATION) FUDaePassState::LIGHT_LINEAR_ATTENUATION;
%rename (FUDaePassState_LIGHT_QUADRATIC_ATTENUATION) FUDaePassState::LIGHT_QUADRATIC_ATTENUATION;
%rename (FUDaePassState_LIGHT_SPOT_CUTOFF) FUDaePassState::LIGHT_SPOT_CUTOFF;
%rename (FUDaePassState_LIGHT_SPOT_DIRECTION) FUDaePassState::LIGHT_SPOT_DIRECTION;
%rename (FUDaePassState_LIGHT_SPOT_EXPONENT) FUDaePassState::LIGHT_SPOT_EXPONENT;
%rename (FUDaePassState_TEXTURE1D) FUDaePassState::TEXTURE1D;
%rename (FUDaePassState_TEXTURE2D) FUDaePassState::TEXTURE2D;
%rename (FUDaePassState_TEXTURE3D) FUDaePassState::TEXTURE3D;
%rename (FUDaePassState_TEXTURECUBE) FUDaePassState::TEXTURECUBE;
%rename (FUDaePassState_TEXTURERECT) FUDaePassState::TEXTURERECT;
%rename (FUDaePassState_TEXTUREDEPTH) FUDaePassState::TEXTUREDEPTH;
%rename (FUDaePassState_TEXTURE1D_ENABLE) FUDaePassState::TEXTURE1D_ENABLE;
%rename (FUDaePassState_TEXTURE2D_ENABLE) FUDaePassState::TEXTURE2D_ENABLE;
%rename (FUDaePassState_TEXTURE3D_ENABLE) FUDaePassState::TEXTURE3D_ENABLE;
%rename (FUDaePassState_TEXTURECUBE_ENABLE) FUDaePassState::TEXTURECUBE_ENABLE;
%rename (FUDaePassState_TEXTURERECT_ENABLE) FUDaePassState::TEXTURERECT_ENABLE;
%rename (FUDaePassState_TEXTUREDEPTH_ENABLE) FUDaePassState::TEXTUREDEPTH_ENABLE;
%rename (FUDaePassState_TEXTURE_ENV_COLOR) FUDaePassState::TEXTURE_ENV_COLOR;
%rename (FUDaePassState_TEXTURE_ENV_MODE) FUDaePassState::TEXTURE_ENV_MODE;
%rename (FUDaePassState_CLIP_PLANE) FUDaePassState::CLIP_PLANE;
%rename (FUDaePassState_CLIP_PLANE_ENABLE) FUDaePassState::CLIP_PLANE_ENABLE;
%rename (FUDaePassState_BLEND_COLOR) FUDaePassState::BLEND_COLOR;
%rename (FUDaePassState_CLEAR_COLOR) FUDaePassState::CLEAR_COLOR;
%rename (FUDaePassState_CLEAR_STENCIL) FUDaePassState::CLEAR_STENCIL;
%rename (FUDaePassState_CLEAR_DEPTH) FUDaePassState::CLEAR_DEPTH;
%rename (FUDaePassState_COLOR_MASK) FUDaePassState::COLOR_MASK;
%rename (FUDaePassState_DEPTH_BOUNDS) FUDaePassState::DEPTH_BOUNDS;
%rename (FUDaePassState_DEPTH_MASK) FUDaePassState::DEPTH_MASK;
%rename (FUDaePassState_DEPTH_RANGE) FUDaePassState::DEPTH_RANGE;
%rename (FUDaePassState_FOG_DENSITY) FUDaePassState::FOG_DENSITY;
%rename (FUDaePassState_FOG_START) FUDaePassState::FOG_START;
%rename (FUDaePassState_FOG_END) FUDaePassState::FOG_END;
%rename (FUDaePassState_FOG_COLOR) FUDaePassState::FOG_COLOR;
%rename (FUDaePassState_LIGHT_MODEL_AMBIENT) FUDaePassState::LIGHT_MODEL_AMBIENT;
%rename (FUDaePassState_LIGHTING_ENABLE) FUDaePassState::LIGHTING_ENABLE;
%rename (FUDaePassState_LINE_STIPPLE) FUDaePassState::LINE_STIPPLE;
%rename (FUDaePassState_LINE_WIDTH) FUDaePassState::LINE_WIDTH;
%rename (FUDaePassState_MATERIAL_AMBIENT) FUDaePassState::MATERIAL_AMBIENT;
%rename (FUDaePassState_MATERIAL_DIFFUSE) FUDaePassState::MATERIAL_DIFFUSE;
%rename (FUDaePassState_MATERIAL_EMISSION) FUDaePassState::MATERIAL_EMISSION;
%rename (FUDaePassState_MATERIAL_SHININESS) FUDaePassState::MATERIAL_SHININESS;
%rename (FUDaePassState_MATERIAL_SPECULAR) FUDaePassState::MATERIAL_SPECULAR;
%rename (FUDaePassState_MODEL_VIEW_MATRIX) FUDaePassState::MODEL_VIEW_MATRIX;
%rename (FUDaePassState_POINT_DISTANCE_ATTENUATION) FUDaePassState::POINT_DISTANCE_ATTENUATION;
%rename (FUDaePassState_POINT_FADE_THRESHOLD_SIZE) FUDaePassState::POINT_FADE_THRESHOLD_SIZE;
%rename (FUDaePassState_POINT_SIZE) FUDaePassState::POINT_SIZE;
%rename (FUDaePassState_POINT_SIZE_MIN) FUDaePassState::POINT_SIZE_MIN;
%rename (FUDaePassState_POINT_SIZE_MAX) FUDaePassState::POINT_SIZE_MAX;
%rename (FUDaePassState_POLYGON_OFFSET) FUDaePassState::POLYGON_OFFSET;
%rename (FUDaePassState_PROJECTION_MATRIX) FUDaePassState::PROJECTION_MATRIX;
%rename (FUDaePassState_SCISSOR) FUDaePassState::SCISSOR;
%rename (FUDaePassState_STENCIL_MASK) FUDaePassState::STENCIL_MASK;
%rename (FUDaePassState_ALPHA_TEST_ENABLE) FUDaePassState::ALPHA_TEST_ENABLE;
%rename (FUDaePassState_AUTO_NORMAL_ENABLE) FUDaePassState::AUTO_NORMAL_ENABLE;
%rename (FUDaePassState_BLEND_ENABLE) FUDaePassState::BLEND_ENABLE;
%rename (FUDaePassState_COLOR_LOGIC_OP_ENABLE) FUDaePassState::COLOR_LOGIC_OP_ENABLE;
%rename (FUDaePassState_COLOR_MATERIAL_ENABLE) FUDaePassState::COLOR_MATERIAL_ENABLE;
%rename (FUDaePassState_CULL_FACE_ENABLE) FUDaePassState::CULL_FACE_ENABLE;
%rename (FUDaePassState_DEPTH_BOUNDS_ENABLE) FUDaePassState::DEPTH_BOUNDS_ENABLE;
%rename (FUDaePassState_DEPTH_CLAMP_ENABLE) FUDaePassState::DEPTH_CLAMP_ENABLE;
%rename (FUDaePassState_DEPTH_TEST_ENABLE) FUDaePassState::DEPTH_TEST_ENABLE;
%rename (FUDaePassState_DITHER_ENABLE) FUDaePassState::DITHER_ENABLE;
%rename (FUDaePassState_FOG_ENABLE) FUDaePassState::FOG_ENABLE;
%rename (FUDaePassState_LIGHT_MODEL_LOCAL_VIEWER_ENABLE) FUDaePassState::LIGHT_MODEL_LOCAL_VIEWER_ENABLE;
%rename (FUDaePassState_LIGHT_MODEL_TWO_SIDE_ENABLE) FUDaePassState::LIGHT_MODEL_TWO_SIDE_ENABLE;
%rename (FUDaePassState_LINE_SMOOTH_ENABLE) FUDaePassState::LINE_SMOOTH_ENABLE;
%rename (FUDaePassState_LINE_STIPPLE_ENABLE) FUDaePassState::LINE_STIPPLE_ENABLE;
%rename (FUDaePassState_LOGIC_OP_ENABLE) FUDaePassState::LOGIC_OP_ENABLE;
%rename (FUDaePassState_MULTISAMPLE_ENABLE) FUDaePassState::MULTISAMPLE_ENABLE;
%rename (FUDaePassState_NORMALIZE_ENABLE) FUDaePassState::NORMALIZE_ENABLE;
%rename (FUDaePassState_POINT_SMOOTH_ENABLE) FUDaePassState::POINT_SMOOTH_ENABLE;
%rename (FUDaePassState_POLYGON_OFFSET_FILL_ENABLE) FUDaePassState::POLYGON_OFFSET_FILL_ENABLE;
%rename (FUDaePassState_POLYGON_OFFSET_LINE_ENABLE) FUDaePassState::POLYGON_OFFSET_LINE_ENABLE;
%rename (FUDaePassState_POLYGON_OFFSET_POINT_ENABLE) FUDaePassState::POLYGON_OFFSET_POINT_ENABLE;
%rename (FUDaePassState_POLYGON_SMOOTH_ENABLE) FUDaePassState::POLYGON_SMOOTH_ENABLE;
%rename (FUDaePassState_POLYGON_STIPPLE_ENABLE) FUDaePassState::POLYGON_STIPPLE_ENABLE;
%rename (FUDaePassState_RESCALE_NORMAL_ENABLE) FUDaePassState::RESCALE_NORMAL_ENABLE;
%rename (FUDaePassState_SAMPLE_ALPHA_TO_COVERAGE_ENABLE) FUDaePassState::SAMPLE_ALPHA_TO_COVERAGE_ENABLE;
%rename (FUDaePassState_SAMPLE_ALPHA_TO_ONE_ENABLE) FUDaePassState::SAMPLE_ALPHA_TO_ONE_ENABLE;
%rename (FUDaePassState_SAMPLE_COVERAGE_ENABLE) FUDaePassState::SAMPLE_COVERAGE_ENABLE;
%rename (FUDaePassState_SCISSOR_TEST_ENABLE) FUDaePassState::SCISSOR_TEST_ENABLE;
%rename (FUDaePassState_STENCIL_TEST_ENABLE) FUDaePassState::STENCIL_TEST_ENABLE;
%rename (FUDaePassState_COUNT) FUDaePassState::COUNT;
%rename (FUDaePassState_INVALID) FUDaePassState::INVALID;

#endif

%rename (SetDaeId_const) SetDaeId( const fm::string& );
%rename (GetVersion_const) GetVersion() const;
%rename (FindAnimatedValue_const) FindAnimatedValue(const float*) const;
%rename (__eq__) operator== (const fm::string&, const char*);
%rename (__add__) operator+ (const fm::string&, int32);

%rename (__eq__) FUObjectType::operator==(const FUObjectType& otherType) const;
%rename (__neq__) FUObjectType::operator!=(const FUObjectType& otherType) const;

%rename (__multiply__) operator*(const FMMatrix44& m1, const FMMatrix44& m2);


// %ignore fm::vector<T,PRIMITIVE>::operator[](INTEGER index);
// %ignore vector<T,PRIMITIVE>::operator[](INTEGER index) const;
// %ignore vector<*,*>::operator==(const fm::vector<T,PRIMITIVE>& other) const;
// %ignore vector::operator =(const fm::vector<T,PRIMITIVE>& rhs);

%rename (FCDEntityInstance_Type) FCDEntityInstance::Type;
%rename (FCDEffectCode_Type) FCDEffectCode::Type;
%rename (FCDEffectParameter_Type) FCDEffectParameter::Type;
%rename (FCDEntity_Type) FCDEntity::Type;
%rename (FCDTransform_Type) FCDTransform::Type;
%rename (FUDaeSplineType_Type) FUDaeSplineType::Type;
%rename (FUDaeProfileType_Type) FUDaeProfileType::Type;
%rename (FUDaePassStateBlendType_Type) FUDaePassStateBlendType::Type;
%rename (FUDaePassStateFaceType_Type) FUDaePassStateFaceType::Type;
%rename (FUDaePassStateMaterialType_Type) FUDaePassStateMaterialType::Type;
%rename (FUDaePassStateFogType_Type) FUDaePassStateFogType::Type;
%rename (FUDaePassStateFogCoordinateType_Type) FUDaePassStateFogCoordinateType::Type;
%rename (FUDaePassStateLightModelColorControlType_Type) FUDaePassStateLightModelColorControlType::Type;

%rename (FUFile_Mode) FUFile::Mode;
%rename (FUDaeBlendMode_Mode) FUDaeBlendMode::Mode;
%rename (FUDaePassStatePolygonMode_Mode) FUDaePassStatePolygonMode::Mode;

%rename (FUDaePassStateStencilOperation_Operation) FUDaePassStateStencilOperation::Operation;
%rename (FUDaePassStateLogicOperation_Operation) FUDaePassStateLogicOperation::Operation;

%rename (FUDaeInterpolation_FromString) FUDaeInterpolation::FromString(const fm::string&);
%rename (FUDaeInterpolation_ToString) FUDaeInterpolation::ToString(Interpolation);

%rename (FUDaeSplineForm_FromString) FUDaeSplineForm::FromString(const fm::string&);
%rename (FUDaeSplineForm_ToString) FUDaeSplineForm::ToString(Form);

%rename (FUDaeTextureChannel_FromString) FUDaeTextureChannel::FromString(const fm::string&);
%ignore FUDaeTextureWrapMode::FromString(const char*);
%rename (FUDaeTextureWrapMode_FromString) FUDaeTextureWrapMode::FromString(const fm::string&);
%rename (FUDaeTextureWrapMode_ToString) FUDaeTextureWrapMode::ToString(WrapMode);

%ignore FUDaeTextureFilterFunction::FromString(const char*);
%rename (FUDaeTextureFilterFunction_FromString) FUDaeTextureFilterFunction::FromString(const fm::string&);
%rename (FUDaeTextureFilterFunction_ToString) FUDaeTextureFilterFunction::ToString(FilterFunction);

%ignore FUDaeMorphMethod::FromString(const char*);
%rename (FUDaeMorphMethod_FromString) FUDaeMorphMethod::FromString(const fm::string&);
%rename (FUDaeMorphMethod_ToString) FUDaeMorphMethod::ToString(Method);

%ignore FUDaeInfinity::FromString(const char*);
%rename (FUDaeInfinity_FromString) FUDaeInfinity::FromString(const fm::string&);
%rename (FUDaeInfinity_ToString) FUDaeInfinity::ToString(Infinity);

%ignore FUDaeBlendMode::FromString(const char*);
%rename (FUDaeBlendMode_FromString) FUDaeBlendMode::FromString(const fm::string&);
%rename (FUDaeBlendMode_ToString) FUDaeBlendMode::ToString(Mode);

%ignore FUDaeGeometryInput::FromString(const char*);
%rename (FUDaeGeometryInput_FromString) FUDaeGeometryInput::FromString(const fm::string&);
%rename (FUDaeGeometryInput_ToString) FUDaeGeometryInput::ToString(Semantic);

%ignore FUDaeProfileType::FromString(const char*); 
%rename (FUDaeProfileType_FromString) FUDaeProfileType::FromString(const fm::string&);
%rename (FUDaeProfileType_ToString) FUDaeProfileType::ToString(Type);

%ignore FUDaePassStateFunction::FromString(const char*);
%rename (FUDaePassStateFunction_FromString) FUDaePassStateFunction::FromString(const fm::string&);
%rename (FUDaePassStateFunction_ToString) FUDaePassStateFunction::ToString(Function);

%ignore FUDaePassStateStencilOperation::FromString(const char*);
%rename (FUDaePassStateStencilOperation_FromString) FUDaePassStateStencilOperation::FromString(const fm::string&);
%rename (FUDaePassStateStencilOperation_ToString) FUDaePassStateStencilOperation::ToString(Operation);

%ignore FUDaePassStateBlendType::FromString(const char*);
%rename (FUDaePassStateBlendType_FromString) FUDaePassStateBlendType::FromString(const fm::string&);
%rename (FUDaePassStateBlendType_ToString) FUDaePassStateBlendType::ToString(Type);

%ignore FUDaePassStateFaceType::FromString(const char*);
%rename (FUDaePassStateFaceType_FromString) FUDaePassStateFaceType::FromString(const fm::string&);
%rename (FUDaePassStateFaceType_ToString) FUDaePassStateFaceType::ToString(Type);

%ignore FUDaePassStateBlendEquation::FromString(const char*);
%rename (FUDaePassStateBlendEquation_FromString) FUDaePassStateBlendEquation::FromString(const fm::string&);
%rename (FUDaePassStateBlendEquation_ToString) FUDaePassStateBlendEquation::ToString(Equation);

%ignore FUDaePassStateMaterialType::FromString(const char*);
%rename (FUDaePassStateMaterialType_FromString) FUDaePassStateMaterialType::FromString(const fm::string&);
%rename (FUDaePassStateMaterialType_ToString) FUDaePassStateMaterialType::ToString(Type);

%ignore FUDaePassStateFogType::FromString(const char*);
%rename (FUDaePassStateFogType_FromString) FUDaePassStateFogType::FromString(const fm::string&);
%rename (FUDaePassStateFogType_ToString) FUDaePassStateFogType::ToString(Type type);

%ignore FUDaePassStateFogCoordinateType::FromString(const char*);
%rename (FUDaePassStateFogCoordinateType_FromString) FUDaePassStateFogCoordinateType::FromString(const fm::string&);
%rename (FUDaePassStateFogCoordinateType_ToString) FUDaePassStateFogCoordinateType::ToString(Type);

%ignore FUDaePassStateFrontFaceType::FromString(const char*);
%rename (FUDaePassStateFrontFaceType_FromString) FUDaePassStateFrontFaceType::FromString(const fm::string&);
%rename (FUDaePassStateFrontFaceType_ToString) FUDaePassStateFrontFaceType::ToString(Type);

%ignore FUDaePassStateLogicOperation::FromString(const char*);
%rename (FUDaePassStateLogicOperation_FromString) FUDaePassStateLogicOperation::FromString(const fm::string&);
%rename (FUDaePassStateLogicOperation_ToString) FUDaePassStateLogicOperation::ToString(Operation);

%ignore FUDaePassStatePolygonMode::FromString(const char*);
%rename (FUDaePassStatePolygonMode_FromString) FUDaePassStatePolygonMode::FromString(const fm::string&);
%rename (FUDaePassStatePolygonMode_ToString) FUDaePassStatePolygonMode::ToString(Mode);

%ignore FUDaePassStateShadeModel::FromString(const char*);
%rename (FUDaePassStateShadeModel_FromString) FUDaePassStateShadeModel::FromString(const fm::string&);
%rename (FUDaePassStateShadeModel_ToString) FUDaePassStateShadeModel::ToString(Model);

%ignore FUDaePassStateLightModelColorControlType::FromString(const char*);
%rename (FUDaePassStateLightModelColorControlType_FromString) FUDaePassStateLightModelColorControlType::FromString(const fm::string&);
%rename (FUDaePassStateLightModelColorControlType_ToString) FUDaePassStateLightModelColorControlType::ToString(Type);

%ignore FUDaePassState::FromString(const char*);
%rename ( FUDaePassState_FromString) FUDaePassState::FromString(const fm::string&);
%rename ( FUDaePassState_ToString) FUDaePassState::ToString(State);

%ignore FUDaeAccessor::XY;
%ignore FUDaeAccessor::XYZW;
%ignore FUDaeAccessor::RGBA;
%ignore FUDaeAccessor::STPQ;

//The following methods from the FCDocument class have been marked as depricated in the
//FCollada libraries.
%ignore GetVisualSceneRoot();
%ignore GetVisualSceneRoot() const;
%ignore GetPhysicsSceneRoot(size_t index = 0);
%ignore GetPhysicsSceneRoot(size_t index = 0) const;

//Ignore the global empty string variables
%ignore emptyString;
%ignore emptyCharString;
%ignore emptyFCharString;
%ignore emptyFString;

%ignore InsertHandler;
%ignore FCDMaterialInstanceBind;
%ignore FCDMaterialInstanceBindVertexInput;
%ignore FCDEffectParameterAnnotation;
%ignore FCDEffectPassBind;
%ignore resize;

%ignore FMVector2::XAxis;
%ignore FMVector2::YAxis;
%ignore FMVector2::ZAxis;
%ignore FMVector2::Origin;
%ignore FMVector2::One;
%ignore FMVector2::Zero;
%ignore FMVector2::Normalize;
%ignore FMVector3::XAxis;
%ignore FMVector3::YAxis;
%ignore FMVector3::ZAxis;
%ignore FMVector3::Origin;
%ignore FMVector3::One;
%ignore FMVector3::Zero;
%ignore FMVector4::One;
%ignore FMVector4::Zero;
%ignore FMVector4::AlphaOne;
%ignore FMMatrix44::Identity;
%ignore FMMatrix33::identity;

%ignore FMVector3::Normalize;

%ignore FCDEffectPass::GetDaeId;
%ignore FCDEffectProfile::GetDaeId;
%ignore FCDEffectTechnique::GetDaeId;
%ignore FCDGeometryMesh::Link;
%ignore FCDEffect::Flatten;
%ignore FCDTransform::SetTransformsDirtyFlag;

%ignore FCDAnimatedStandardQualifiers::EMPTY;
%ignore FCDAnimatedStandardQualifiers::XYZW;
%ignore FCDAnimatedStandardQualifiers::RGBA;
%ignore FCDAnimatedStandardQualifiers::ROTATE_AXIS;
%ignore FCDAnimatedStandardQualifiers::SKEW;
%ignore FCDAnimatedStandardQualifiers::MATRIX;
%ignore FCDAnimatedStandardQualifiers::LOOKAT;

%ignore FCDSkinController::AddJoint;
%ignore FCDAnimationCurve::is2DEvaluation;
%ignore FCDAnimationCurve::Set2DCurveEvaluation;
%ignore FCDAnimationCurve::Is2DCurveEvaluation;
%ignore FCDAnimationMultiCurve::is2DEvaluation;
%ignore FCDAnimationMultiCurve::Set2DCurveEvaluation;
%ignore FCDAnimationMultiCurve::Is2DCurveEvaluation;

%ignore StandardizeUpAxisAndLength;

%ignore FUError::GetFatalityLevel;
%ignore FUError::SetFatalityLevel;

IGNORE_RTTI(FUObject);
IGNORE_RTTI(FUParameterizable);
IGNORE_RTTI(FUPlugin);
IGNORE_RTTI(FCDObject);
IGNORE_RTTI(FCDObjectWithId);
IGNORE_RTTI(FCDEntity);
IGNORE_RTTI(FCDExtra);
IGNORE_RTTI(FCDEType);
IGNORE_RTTI(FCDENode);
IGNORE_RTTI(FCDETechnique);
IGNORE_RTTI(FCDEAttribute);
IGNORE_RTTI(FCDController);
IGNORE_RTTI(FCDocument);
IGNORE_RTTI(FCDSceneNode);
IGNORE_RTTI(FCDEntityInstance);
IGNORE_RTTI(FCDGeometryInstance);
IGNORE_RTTI(FCDControllerInstance);
IGNORE_RTTI(FCDMaterialInstance);
IGNORE_RTTI(FCDAnimationChannel);

%ignore FCDEAttribute::FCDEAttribute;

%ignore FCDObjectWithId::MAX_ID_LENGTH;

%ignore FCDGeometrySource::Clone;



// FCDocument::FindAnimationChannels(...) has a parameter of type FCDAnimationChannelList which is a private typedef
// of the class, which causes problems for SWIG's wrappers since they can't instantiate it.
%ignore FindAnimationChannels(const fm::string& pointer, FCDAnimationChannelList& channels);

%ignore *::LinkTarget();
%ignore *::__classType;

%rename (FCollada_Release) FCollada::Release(void* buffer);

// *********************************************************************************************
// FMath
// *********************************************************************************************
%rename (fm_Release) fm::Release(void* buffer);
%ignore FMColor::operator uint8*();
%ignore operator*(float s, const FMColor& c);
%rename (FMInterpolation_Interpolation) FMInterpolation::Interpolation;

#undef ALIGN_STRUCT
#define ALIGN_STRUCT(x)


%ignore FMQuaternion::Identity;
%ignore FMQuaternion::Zero;


// *********************************************************************************************
// FUtils
// *********************************************************************************************
%ignore FUBoundingBox::Infinity;
%ignore FUCrc32::CRC32(const fstring& text);
%ignore FUCrc32::CRC32(const fchar* text);
%ignore FUCrc32::CRC32(const fm::string& text);
%ignore ToString;
%ignore FUEvent0::operator()();

%rename (FUDaeInterpolation_Interpolation) FUDaeInterpolation::Interpolation;

%ignore FUObjectTracker::TracksObject(const FUObject* object) const;
%ignore FUObjectTracker::TracksObject(FUObject* object);

%ignore FUObjectContainer<FCDTransform>::Add();
%ignore fm::pvector<FCDTransform>::pvector(size_t size, const FCDTransform& defaultValue);
%ignore FUObjectList<FCDTransform>::TracksObject(const FUObject* object) const;
%ignore FUObjectList<FCDTransform>::TracksObject(FUObject* object);

%ignore FUObjectContainer<FCDEntityInstance>::Add();
%ignore fm::pvector<FCDEntityInstance>::pvector(size_t size, const FCDEntityInstance& defaultValue);
%ignore FUObjectList<FCDEntityInstance>::TracksObject(const FUObject* object) const;
%ignore FUObjectList<FCDEntityInstance>::TracksObject(FUObject* object);

%ignore FCDAnimatedPoint3::Create;
%ignore FCDAnimation::ReleaseChild;
%ignore FCDAnimationCurveTools::Average;
%ignore FCDAnimationCurveTools::TakeFirst;
%ignore FCDAnimationMKey::FCDAnimationMKey;
%ignore FCDAnimationMKeyTCB::FCDAnimationMKeyTCB;
%ignore FCDAnimationMKeyBezier::FCDAnimationMKeyBezier;
%ignore FCDEffectStandard::AmbientColorSemantic;
%ignore FCDEffectStandard::DiffuseColorSemantic;
%ignore FCDEffectStandard::EmissionColorSemantic;
%ignore FCDEffectStandard::EmissionFactorSemantic;
%ignore FCDEffectStandard::IndexOfRefractionSemantic;
%ignore FCDEffectStandard::ReflectivityColorSemantic;
%ignore FCDEffectStandard::ReflectivityFactorSemantic;
%ignore FCDEffectStandard::ShininessSemantic;
%ignore FCDEffectStandard::SpecularFactorSemantic;
%ignore FCDEffectStandard::TranslucencyColorSemantic;
%ignore FCDEffectStandard::TranslucencyFactorSemantic;
%ignore FCDEffectStandard::SpecularColorSemantic;
%ignore FCDPhysicsRigidBodyInstance::GetCollisionEvent;

#ifdef SWIGLUA
ADD_CLASS_P_CTOR (FCDocument);
ADD_CLASS_P_CTOR (FCDSceneNode);
ADD_CLASS_P_CTOR (FCDGeometry); 
#endif

// *********************************
// ** FCDocument/FCDEffectTools.h **
// *********************************

%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDMaterialInstance const *,char const *);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDGeometryInstance const *,char const *);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDMaterial const *,char const *);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDEffect const *,char const *);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDEffectProfile const *,char const *);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDEffectTechnique const *,char const *);

%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDMaterialInstance const *,char const *, bool);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDMaterial const *,char const *,bool);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDEffect const *,char const *, bool);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDEffectProfile const *,char const *, bool);
%ignore FCDEffectTools::FindEffectParameterBySemantic(FCDEffectTechnique const *,char const *, bool);

%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDEffectProfile const *,char const *,FCDEffectParameterList &);
%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDEffectTechnique const *,char const *, FCDEffectParameterList &);
%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDEffect const *,char const *,FCDEffectParameterList &);
%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDMaterial const *,char const *,FCDEffectParameterList &);
%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDMaterialInstance const *,char const *,FCDEffectParameterList &);

%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDEffectProfile const *,char const *, FCDEffectParameterList &, bool);
%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDEffectTechnique const *,char const *, FCDEffectParameterList &, bool);
%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDEffect const *,char const *, FCDEffectParameterList &, bool);
%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDMaterial const *,char const *, FCDEffectParameterList &, bool);
%ignore FCDEffectTools::FindEffectParametersBySemantic(FCDMaterialInstance const *,char const *,FCDEffectParameterList &, bool);

%ignore FCDEffectTools::FindEffectParameterByReference(FCDMaterialInstance const *,char const *);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDGeometryInstance const *,char const *);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDMaterial const *,char const *);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDEffect const *,char const *);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDEffectProfile const *,char const *);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDEffectTechnique const *,char const *);

%ignore FCDEffectTools::FindEffectParameterByReference(FCDEffect *,char const *,bool);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDMaterialInstance const *,char const *,bool);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDEffectProfile const *,char const *,bool);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDMaterial const *,char const *,bool);
%ignore FCDEffectTools::FindEffectParameterByReference(FCDEffectTechnique const *,char const *,bool);

%ignore FCDEffectTools::FindEffectParametersByReference(FCDMaterialInstance const *,char const *,FCDEffectParameterList &);
%ignore FCDEffectTools::FindEffectParametersByReference(FCDMaterial const *,char const *,FCDEffectParameterList &);
%ignore FCDEffectTools::FindEffectParametersByReference(FCDEffect const *,char const *,FCDEffectParameterList &);
%ignore FCDEffectTools::FindEffectParametersByReference(FCDEffectProfile const *,char const *,FCDEffectParameterList &);
%ignore FCDEffectTools::FindEffectParametersByReference(FCDEffectTechnique const *,char const *,FCDEffectParameterList &);

%ignore FCDEffectTools::FindEffectParametersByReference(FCDMaterialInstance const *,char const *,FCDEffectParameterList &, bool);
%ignore FCDEffectTools::FindEffectParametersByReference(FCDEffect const *,char const *,FCDEffectParameterList &, bool);
%ignore FCDEffectTools::FindEffectParametersByReference(FCDEffectProfile const *,char const *,FCDEffectParameterList &, bool);
%ignore FCDEffectTools::FindEffectParametersByReference(FCDMaterial const *,char const *,FCDEffectParameterList &, bool);
%ignore FCDEffectTools::FindEffectParametersByReference(FCDEffectTechnique const *,char const *,FCDEffectParameterList &,bool);

// ************************************
// ** FCDocument/FCDAnimationCurve.h **
// ************************************
%ignore FCDAnimationCurve::GetDriver(const FCDAnimated*& driver, int32& index) const;


// **********************************************
// ** FCDocument/FCDExternalReferenceManager.h **
// **********************************************
%ignore FCDExternalReferenceManager::FindPlaceHolder(FCDocument const *) const;

// ***************************
// ** FCDocument/FCDExtra.h **
// ***************************
%ignore FCDENode::SetName(fm::string const &);

%ignore FCDENode::AddAttribute(fm::string const &,fchar const *);
%ignore FCDENode::AddAttribute(fm::string const &,fstring const &);


// ***********************************
// ** Methods Deprecated post 3.05A **
// ***********************************

#if REMOVE_DEPRECATED_METHODS

%ignore FUObjectContainer::release(const ObjectClass* value); //Please use '"FUObject::Release()"' instead.

%ignore FCollada::LoadDocument(FCDocument* document, const fchar* filename);
%ignore FCollada::LoadDocument(const fchar* filename);
%ignore FCollada::RegisterPlugin(FUPlugin* plugin);

%ignore FCDPhysicsRigidBodyParameters::GetPhysicsShapeList() const;	//Please use 'GetPhysicsShapeCount and GetPhysicsShape(index)' instead.
%ignore FCDPhysicsRigidBodyParameters::GetOwner();

%ignore FColladaPluginManager::AddPlugin(FCPExtraTechnique* plugin);
%ignore FColladaPluginManager::AddArchivePlugin(FCPArchive* plugin);
%ignore FColladaPluginManager::LoadDocument(FCDocument* document, const fchar* filename); //Please use 'LoadDocumentFromFile' instead.
%ignore FColladaPluginManager::SaveDocument(FCDocument* document, const fchar* filename); //Please use 'SaveDocumentToFile' instead

%ignore FCDAnimationChannel::GetCurves() const; //Please use 'GetCurveCount and GetCurve(index)' instead.
%ignore FCDAsset::HasUpAxis() const; //Please use 'GetHasUpAxisFlag' instead.
%ignore	FCDAsset::HasUnits() const; //Please use 'GetHasUnitsFlag' instead.
%ignore FCDAsset::ResetUpAxis(); //Please use 'ResetHasUpAxisFlag' instead.
%ignore FCDAsset::ResetUnits(); //Please use 'ResetHasUnitsFlag' instead.

%ignore FCDEffectParameter::ReleaseAnnotation(FCDEffectParameterAnnotation* annotation); //Please use 'annotation->Release' instead.
%ignore FCDEffectPass::ReleaseShader(FCDEffectPassShader* shader); //Please use 'shader->Release()' instead.
%ignore FCDEffectPass::GetRenderStates() const; //Please use 'GetRenderStateCount and GetRenderState(index)' instead.
%ignore FCDEffectPassShader::GetBindings() const; //Please use 'GetBindingCount and GetBinding(index)' instead.
%ignore FCDEffectPassShader::ReleaseBinding(FCDEffectPassBind* binding); //Please use 'binding->Release()' instead.
%ignore FCDEffectProfile::Flatten(); //Please use 'not recommended' instead.
%ignore FCDEffectProfileFX::GetTechniqueList() const; //Please use 'GetTechniqueCount and GetTechnique(index)' instead.
%ignore FCDEffectProfileFX::GetCodeList(); //Please use 'GetCodeCount and GetCode(index)' instead.
%ignore FCDEffectProfileFX::Flatten(); //Please use 'not recommended' instead.
%ignore FCDEffectStandard::ReleaseTexture(FCDTexture* texture); //Please use 'texture->Release()' instead.
%ignore FCDEffectStandard::Flatten(); //Please use 'not recommended' instead.
%ignore FCDEffectTechnique::GetPassList(); //Please use 'GetPassCount and GetPass(index)' instead.
%ignore FCDEffectTechnique::GetCodeList(); //Please use 'GetCodeCount and GetCode(index) or FindCode' instead.
%ignore FCDEffectTechnique::Flatten(); //Please use 'not recommended' instead.
%ignore FCDExtra::GetTypes() const; //Please use 'GetTypeCount and GetType(index)' instead.
%ignore FCDEType::GetTechniques() const; //Please use 'GetTechniqueCount and GetTechnique(index)' instead.
%ignore FCDENode::GetChildNodes() const; //Please use 'GetChildNodeCount and GetChildNode(index)' instead.
%ignore FCDENode::GetAttributes() const; //Please use 'GetAttributeCount and GetAttribute(index)' instead.
%ignore FCDGeometryMesh::GetVertexSources() const; //Please use 'GetVertexSourceCount and GetVertexSource(index)' instead.
%ignore FCDGeometryMesh::GetSources() const; //Please use 'GetSourceCount and GetSource(index)' instead.
%ignore FCDGeometryPolygons::IsTriangles() const; //Please use 'TestPolyType' instead.
%ignore FCDGeometryPolygons::GetInputs(); // Please use 'GetInputCount and GetInput(index)' instead.
%ignore FCDMaterial::Flatten(); // Please use 'not recommended' instead.
%ignore FCDMorphTarget::IsAnimated() const; //Please use 'GetWeight().IsAnimated()' instead.
%ignore FCDMorphTarget::GetAnimatedWeight(); //Please use 'GetWeight().GetAnimated()' instead.
%ignore FCDMorphTarget::GetAnimatedWeight() const; //Please use 'GetWeight().GetAnimated()' instead.
%ignore FCDMorphController::GetTargets() const; //Please use 'GetTargetCount and GetTarget(index)' instead.
%ignore FCDPhysicsRigidConstraint::GetAnimatedEnabled(); //Please use 'GetEnabled().GetAnimated' instead.
%ignore FCDPhysicsRigidConstraint::GetAnimatedEnabled() const; //Please use 'GetEnabled().GetAnimated' instead.
%ignore FCDPhysicsRigidConstraint::GetAnimatedInterpenetrate(); //Please use 'GetInterpenetrate().GetAnimated' instead.
%ignore FCDPhysicsRigidConstraint::GetAnimatedInterpenetrate() const; //Please use 'GetInterpenetrate().GetAnimated' instead.
%ignore FCDSceneNode::IsJoint() const; //Please use 'GetJointFlag' instead.
%ignore FCDAnimation::GetChannels() const; //Please use 'GetChannelCount and GetChannel(index)' instead.
%ignore FCDCamera::IsPerspective() const; //Please use 'GetProjectionType() == FCDCamera::PERSPECTIVE' instead.
%ignore FCDCamera::SetPerspective(); //Please use 'SetProjectionType(FCDCamera::PERSPECTIVE)' instead.
%ignore FCDCamera::IsOrthographic() const; //Please use 'GetProjectionType() == FCDCamera::ORTHOGRAPHIC' instead.
%ignore FCDCamera::SetOrthographic(); //Please use 'SetProjectionType(FCDCamera::ORTHOGRAPHIC)' instead.
%ignore FCDEffect::GetProfiles() const; //Please use 'GetProfileCount and GetProfile(index)' instead.
%ignore FCDGeometryInstance::GetMaterialInstances(); //Please use 'GetMaterialInstance' instead.
%ignore FCDGeometryInstance::GetMaterialInstances() const; // Please use 'GetMaterialInstance' instead.
%ignore FCDLight::HasMaxExtras() const; //Please use 'GetExtra()->GetDefaultType()->GetTechniques()' instead.
%ignore FCDLight::HasMayaExtras() const; //Please use 'GetExtra()->GetDefaultType()->GetTechniques()' instead.
%ignore FCDLight::SetHasMaxExtras(bool); //Please use 'nothing' instead.
%ignore FCDLight::SetHasMayaExtras(bool); //Please use 'nothing' instead.
%ignore FCDLight::GetPenumbraAngle(); //Please use 'GetOuterAngle and GetFallOffAngle' instead.
%ignore FCDLight::GetPenumbraAngle() const; //Please use 'GetOuterAngle and GetFallOffAngle' instead.
%ignore FCDLight::SetPenumbraAngle(float angle); //Please use 'SetOuterAngle and SetFallOffAngle' instead.
%ignore FCDMaterialInstance::GetType() const; //Please use 'HasType(FCDMaterialInstance::GetClassType())' instead.
%ignore FCDMaterialInstance::GetBindings() const; //Please use 'GetBindingCount and GetBinding(index)' instead.
%ignore FCDMaterialInstance::GetVertexInputBindings(); //Please use 'GetVertexInputBindingCount and GetVertexInputBinding(index)' instead.
%ignore FCDMaterialInstance::FlattenMaterial(); //Please use 'not recommended' instead.
%ignore FCDPhysicsModelInstance::GetInstances() const; //Please use 'GetInstanceCount and GetInstance(index)' instead.
%ignore FCDPhysicsModelInstance::RemoveInstance(FCDEntityInstance* instance); //Please use 'instance->Release() or SAFE_RELEASE(instance)' instead.

#endif //REMOVE_DEPRECATED_METHODS



// %template (FColladaDocumentSceneNodePVector) fm::pvector<FCDSceneNode>;
// %template (FColladaDocumentEntityInstancePVector) fm::pvector<FCDEntityInstance>;
// %template (FColladaDocumentSceneNodeTrackList) FUObjectList<FCDSceneNode>;
// %template (FColladaDocumentEntityInstanceObjectList) FUObjectList<FCDEntityInstance>;
// %template (FColladaDocumentEntityInstanceContainer) FUObjectContainer<FCDEntityInstance>;


%include "FUtils\Platforms.h"
%include "FUtils\FUBoundingBox.h"		// Defines these classes FUBoundingBox
%include "FUtils\FUBoundingSphere.h"		// Defines these classes FUBoundingSphere
%include "FUtils\FUCrc32.h"
%include "FUtils\FUCriticalSection.h"		// Defines these classes FUCriticalSection
%include "FUtils\FUDaeEnum.h"
%include "FUtils\FUDaeEnumSyntax.h"
%include "FUtils\FUDaeSyntax.h"
%include "FUtils\FUDateTime.h"		// Defines these classes FUDateTime
%include "FUtils\FUDebug.h"		// Defines these classes FUDebug
%include "FUtils\FUErrorLog.h"		// Defines these classes FUErrorLog
%include "FUtils\FUFile.h"		// Defines these classes FUFile
%include "FUtils\FUFunctor.h"		// Defines these classes IFunctor0, FUFunctor0, FUStaticFunctor0, IFunctor1, FUFunctor1, FUStaticFunctor1, IFunctor2, FUFunctor2, FUStaticFunctor2, IFunctor3, FUFunctor3, FUStaticFunctor3
%include "FUtils\FULogFile.h"		// Defines these classes FULogFile
%include "FUtils\FUObject.h"		// Defines these classes FUObject, FUObjectOwner, FUObjectRef, FUObjectContainer

%include "FMath\FMArrayPointer.h"		// Defines these classes pvector

%template (FColladaDocumentTransformPVector) fm::pvector<FCDTransform>;
%template (FColladaDocumentEntityInstancePVector) fm::pvector<FCDEntityInstance>;

%template (FColladaDocumentTransformContainer) FUObjectContainer<FCDTransform>;
%template (FColladaDocumentEntityInstanceContainer) FUObjectContainer<FCDEntityInstance>;

%include "FUtils\FUObjectType.h"		// Defines these classes FUObjectType
%include "FUtils\FUPluginManager.h"		// Defines these classes FUPluginManager		// And references these classes FUObjectContainer
%include "FUtils\FUSemaphore.h"		// Defines these classes FUSemaphore, FUBinarySemaphore
%include "FUtils\FUSingleton.h"
%include "FUtils\FUString.h"		// Defines these classes stringT

%include "FUtils\FUStringBuilder.h"		// Defines these classes FUStringBuilderT
%include "FUtils\FUSynchronizableObject.h"		// Defines these classes FUSynchronizableObject		// And references these classes FUCriticalSection
%include "FUtils\FUThread.h"		// Defines these classes FUThread
%include "FUtils\FUtils.h"
%include "FUtils\FUTracker.h"		// Defines these classes FUTrackable, FUTracker, FUTrackedPtr, FUTrackedList		// And references these classes FUObject
%include "FUtils\FUUniqueStringMap.h"		// Defines these classes FUUniqueStringMapT
%include "FUtils\FUUri.h"		// Defines these classes FUUri
%include "FUtils\FUXmlDocument.h"		// Defines these classes FUXmlDocument
%include "FMath\FMAllocator.h"
%include "FMath\FMAngleAxis.h"		// Defines these classes FMAngleAxis

%include "FMath\FMath.h"
%include "FMath\FMColor.h"		// Defines these classes FMColor
%include "FMath\FMFloat.h"
%include "FMath\FMInteger.h"
%include "FUtils\FUBase64.h"
%include "FMath\FMInterpolation.h"
%include "FMath\FMLookAt.h"		// Defines these classes FMLookAt
%include "FMath\FMMatrix44.h"
%include "FMath\FMQuaternion.h"		// Defines these classes FMQuaternion
%include "FMath\FMRandom.h"
%include "FMath\FMSkew.h"		// Defines these classes FMSkew
%include "FMath\FMSort.h"		// Defines these classes comparator, icomparator, pcomparator
%include "FMath\FMTree.h"		// Defines these classes pair, tree, node, iterator, const_iterator
%include "FMath\FMVector2.h"		// Defines these classes FMVector2
%include "FMath\FMVector3.h"
%include "FMath\FMVector4.h"
%include "FMath\FMVolume.h"
%include "FCDocument\FCDAnimationClipTools.h"
%include "FCDocument\FCDAnimationCurveTools.h"
%include "FCDocument\FCDAnimationKey.h"		// Defines these classes FCDAnimationKey, FCDAnimationKeyBezier, FCDAnimationKeyTCB, FCDAnimationMKey, FCDAnimationMKeyBezier, FCDAnimationMKeyTCB		// And references these classes FMVector2
%include "FCDocument\FCDControllerTools.h"
%include "FCDocument\FCDEffectParameterFactory.h"		// Defines these classes FCDEffectParameterFactory
%include "FCDocument\FCDEmitterObject.h"
%include "FCDocument\FCDEmitterParticle.h"
%include "FCDocument\FCDForceDeflector.h"
%include "FCDocument\FCDForceDrag.h"
%include "FCDocument\FCDForceGravity.h"
%include "FCDocument\FCDForcePBomb.h"
%include "FCDocument\FCDForceTyped.h"
%include "FCDocument\FCDForceWind.h"
%include "FCDocument\FCDGeometryNURBSSurface.h"
%include "FCDocument\FCDGeometryPolygonsTools.h"
%include "FCDocument\FCDLightTools.h"
%include "FCDocument\FCDocumentTools.h"
%include "FCDocument\FCDParameterAnimatable.h"		// Defines these classes FCDParameterAnimatable, FCDParameterAnimatableT, FCDParameterListAnimatable, FCDParameterListAnimatableT, Parameter_##parameterName, Parameter_##parameterName		// And references these classes FUObjectRef, FMVector2, FMAngleAxis, FMLookAt, FMSkew, FUObjectContainer
%include "FCDocument\FCDParticleEmitter.h"
%include "FCDocument\FCDParticleModifier.h"
%include "FCDocument\FCDSceneNodeIterator.h"		// Defines these classes FCDSceneNodeIteratorT
%include "FCDocument\FCDSceneNodeTools.h"
%include "FCDocument\FCDVersion.h"		// Defines these classes FCDVersion
%include "FCollada.h"		// And references these classes IFunctor0
%include "FUtils\FUAssert.h"		// And references these classes FUStaticFunctor1
%include "FUtils\FUEvent.h"		// Defines these classes FUEvent0, FUEvent1, FUEvent2, FUEvent3		// And references these classes IFunctor0, FUFunctor0, FUStaticFunctor0, IFunctor1, FUStaticFunctor1, IFunctor2, FUFunctor2, FUStaticFunctor2, IFunctor3, FUFunctor3, FUStaticFunctor3
%include "FUtils\FUFileManager.h"		// Defines these classes FUFileManager		// And references these classes IFunctor1, IFunctor2, IFunctor3
%include "FUtils\FUParameter.h"		// Defines these classes FUParameterT, Parameter_##parameterName		// And references these classes FMVector2, FUTrackedPtr, FUObjectRef, FUTrackedList, FUObjectContainer
%include "FUtils\FUParameterizable.h"		// Defines these classes FUParameterizable		// And references these classes FUTrackable
%include "FUtils\FUPlugin.h"		// Defines these classes FUPlugin		// And references these classes FUTrackable
%include "FUtils\FUStringConversion.h"		// Defines these classes FUStringConversion		// And references these classes FMVector2, FUStringBuilderT
%include "FUtils\FUXmlParser.h"		// And references these classes node
%include "FUtils\FUXmlWriter.h"		// And references these classes node
%include "FMath\FMArray.h"		// And references these classes comparator

%template (VectorOfVoidPtrs) fm::vector<const void*, true>;

#ifdef SWIGRUBY
	%template (Charvector)		fm::vector<char, true>;
#else
	%template (charvector)		fm::vector<char, true>;
#endif

#ifdef SWIGRUBY
	%template (Fstring)			fm::stringT<char>;
#else
	%template (fstring)			fm::stringT<char>;
#endif


%include "FMath\FMMatrix33.h"		// Defines these classes FMMatrix33		// And references these classes FMVector2
%include "FCDocument\FCDObject.h"		// Defines these classes FCDObject		// And references these classes FUParameterizable
%include "FCDocument\FCDocument.h"		// Defines these classes FCDLayer, FCDocument		// And references these classes FUObjectRef, tree
%include "FCDocument\FCDObjectWithId.h"		// Defines these classes FCDObjectWithId		// And references these classes FCDObject
%include "FCDocument\FCDPhysicsRigidBodyParameters.h"		// Defines these classes FCDPhysicsRigidBodyParameters		// And references these classes FCDObject, FUTrackedPtr, FMAngleAxis
%include "FCDocument\FCDPlaceHolder.h"		// Defines these classes FCDPlaceHolder		// And references these classes FCDObject, FUTracker, FUTrackedList
%include "FCDocument\FCDSkinController.h"		// Defines these classes FCDSkinControllerVertex, FCDSkinControllerJoint, FCDSkinController		// And references these classes FCDObject, FUObjectRef
%include "FCDocument\FCDTransform.h"		// Defines these classes FCDTransform, FCDTTranslation, FCDTScale, FCDTRotation, FCDTMatrix, FCDTLookAt, FCDTSkew, FCDTFactory		// And references these classes FCDObject, FMAngleAxis, FMQuaternion, FMLookAt, FMSkew
%include "FColladaPlugin.h"		// Defines these classes FCPExtraTechnique, FColladaPluginManager		// And references these classes FUPlugin, FUObject, FUObjectContainer
%include "FUtils\FUError.h"		// Defines these classes FUError, FUErrorSimpleHandler		// And references these classes IFunctor3, FUCriticalSection, FUEvent3, FUFunctor3, FUStaticFunctor3
%include "FCDocument\FCDAnimated.h"		// Defines these classes FCDAnimated, FCDAnimatedCustom		// And references these classes FUTrackedList, FCDObject, FUTracker, node
%include "FCDocument\FCDAnimationChannel.h"		// Defines these classes FCDAnimationChannel		// And references these classes FCDObject
%include "FCDocument\FCDAnimationCurve.h"		// Defines these classes FCDAnimationCurve, FCDConversionFunctor, FCDConversionScaleFunctor, FCDConversionOffsetFunctor		// And references these classes FCDObject, FUTrackedPtr
%include "FCDocument\FCDAnimationMultiCurve.h"		// Defines these classes FCDAnimationMultiCurve		// And references these classes FCDObject
%include "FCDocument\FCDAsset.h"		// Defines these classes FCDAsset, FCDAssetContributor		// And references these classes FCDObject, FUDateTime
%include "FCDocument\FCDEffectCode.h"		// Defines these classes FCDEffectCode		// And references these classes FCDObject
%include "FCDocument\FCDEffectParameter.h"		// Defines these classes FCDEffectParameter, FCDEffectParameterT, FCDEffectParameterAnimatableT, FCDEffectParameterAnnotation		// And references these classes FCDObject, FCDParameterAnimatableT, FMVector2, FUParameterizable
%include "FCDocument\FCDEffectParameterSampler.h"		// Defines these classes FCDEffectParameterSampler		// And references these classes FCDEffectParameter
%include "FCDocument\FCDEffectParameterSurface.h"		// Defines these classes FCDEffectParameterSurface, FCDEffectParameterSurfaceInitFactory, FCDEffectParameterSurfaceInit, FCDEffectParameterSurfaceInitCube, FCDEffectParameterSurfaceInitVolume, FCDEffectParameterSurfaceInitFrom, FCDEffectParameterSurfaceInitAsNull, FCDEffectParameterSurfaceInitAsTarget, FCDEffectParameterSurfaceInitPlanar		// And references these classes FCDEffectParameter
%include "FCDocument\FCDEffectPass.h"		// Defines these classes FCDEffectPass		// And references these classes FCDObject
%include "FCDocument\FCDEffectPassShader.h"		// Defines these classes FCDEffectPassBind, FCDEffectPassShader		// And references these classes FCDObject
%include "FCDocument\FCDEffectPassState.h"		// Defines these classes FCDEffectPassState		// And references these classes FCDObject
%include "FCDocument\FCDEffectProfile.h"		// Defines these classes FCDEffectProfile		// And references these classes FCDObject
%include "FCDocument\FCDEffectProfileFX.h"		// Defines these classes FCDEffectProfileFX		// And references these classes FCDEffectProfile
%include "FCDocument\FCDEffectStandard.h"		// Defines these classes FCDEffectStandard		// And references these classes FCDEffectProfile
%include "FCDocument\FCDEffectTechnique.h"		// Defines these classes FCDEffectTechnique		// And references these classes FCDObject
%include "FCDocument\FCDEffectTools.h"		// And references these classes FCDEffectParameter
%include "FCDocument\FCDEntity.h"		// Defines these classes FCDEntity		// And references these classes FCDObjectWithId
%include "FCDocument\FCDEntityReference.h"		// Defines these classes FCDEntityReference		// And references these classes FCDObject, FUTracker, FUUri
%include "FCDocument\FCDExternalReferenceManager.h"		// Defines these classes FCDExternalReferenceManager		// And references these classes FCDObject, FUObjectContainer
%include "FCDocument\FCDExtra.h"		// Defines these classes FCDExtra, FCDEType, FCDENode, FCDETechnique, FCDEAttribute		// And references these classes FCDObject, FUTrackable, FUParameterizable
%include "FCDocument\FCDForceField.h"		// Defines these classes FCDForceField		// And references these classes FCDEntity
%include "FCDocument\FCDGeometry.h"		// Defines these classes FCDGeometry		// And references these classes FCDEntity
%include "FCDocument\FCDGeometryMesh.h"		// Defines these classes FCDGeometryMesh		// And references these classes FCDObject
%include "FCDocument\FCDGeometryPolygons.h"		// Defines these classes FCDGeometryPolygons		// And references these classes FCDObject
%include "FCDocument\FCDGeometryPolygonsInput.h"		// Defines these classes FCDGeometryPolygonsInput		// And references these classes FCDObject, FUTracker
%include "FCDocument\FCDGeometrySource.h"		// Defines these classes FCDGeometrySource		// And references these classes FCDObjectWithId, FUObjectContainer, FCDAnimated
%include "FCDocument\FCDGeometrySpline.h"		// Defines these classes FCDSpline, FCDLinearSpline, FCDBezierSpline, FCDNURBSSpline, FCDGeometrySpline		// And references these classes FCDObject, FUObjectContainer
%include "FCDocument\FCDImage.h"		// Defines these classes FCDImage		// And references these classes FCDEntity
%include "FCDocument\FCDLibrary.h"		// Defines these classes FCDLibrary		// And references these classes FCDObject
%template (FColladaDocumentAnimationLibrary) FCDLibrary<FCDAnimation>;
%template (FColladaDocumentAnimationClipLibrary) FCDLibrary<FCDAnimationClip>;
%template (FColladaDocumentCameraLibrary) FCDLibrary<FCDCamera>;
%template (FColladaDocumentControllerLibrary) FCDLibrary<FCDController>;
%template (FColladaDocumentEffectLibrary) FCDLibrary<FCDEffect>;
%template (FColladaDocumentEmitterLibrary) FCDLibrary<FCDEmitter>;
%template (FColladaDocumentForceFieldLibrary) FCDLibrary<FCDForceField>;
%template (FColladaDocumentGeometryLibrary) FCDLibrary<FCDGeometry>;
%template (FColladaDocumentImageLibrary) FCDLibrary<FCDImage>;
%template (FColladaDocumentLightLibrary) FCDLibrary<FCDLight>;
%template (FColladaDocumentMaterialLibrary) FCDLibrary<FCDMaterial>;
%template (FColladaDocumentVisualSceneNodeLibrary) FCDLibrary<FCDSceneNode>;
%template (FColladaDocumentPhysicsModelLibrary) FCDLibrary<FCDPhysicsModel>;
%template (FColladaDocumentPhysicsMaterialLibrary) FCDLibrary<FCDPhysicsMaterial>;
%template (FColladaDocumentPhysicsSceneLibrary) FCDLibrary<FCDPhysicsScene>;
%include "FCDocument\FCDMaterial.h"		// Defines these classes FCDMaterialTechniqueHint, FCDMaterial		// And references these classes FCDEntity
%include "FCDocument\FCDMorphController.h"		// Defines these classes FCDMorphTarget, FCDMorphController		// And references these classes FCDObject, FCDEntity
%include "FCDocument\FCDPhysicsAnalyticalGeometry.h"		// Defines these classes FCDPhysicsAnalyticalGeometry, FCDPASBox, FCDPASPlane, FCDPASSphere, FCDPASCylinder, FCDPASCapsule, FCDPASTaperedCapsule, FCDPASTaperedCylinder, FCDPASFactory		// And references these classes FCDEntity, FMVector2
%include "FCDocument\FCDPhysicsMaterial.h"		// Defines these classes FCDPhysicsMaterial		// And references these classes FCDEntity
%include "FCDocument\FCDPhysicsModel.h"		// Defines these classes FCDPhysicsModel		// And references these classes FUObjectContainer, FCDEntity
%include "FCDocument\FCDPhysicsRigidBody.h"		// Defines these classes FCDPhysicsRigidBody		// And references these classes FCDEntity
%include "FCDocument\FCDPhysicsRigidConstraint.h"		// Defines these classes FCDPhysicsRigidConstraint		// And references these classes FUObjectContainer, FCDEntity, FUTrackedPtr
%include "FCDocument\FCDPhysicsScene.h"		// Defines these classes FCDPhysicsScene		// And references these classes FUObjectContainer, FCDEntity
%include "FCDocument\FCDPhysicsShape.h"		// Defines these classes FCDPhysicsShape		// And references these classes FUObjectContainer, FCDTransform, FCDObject, FUTrackedPtr, FUObjectRef
%include "FCDocument\FCDSceneNode.h"		// Defines these classes FCDSceneNode		// And references these classes FCDEntity
%include "FCDocument\FCDTargetedEntity.h"		// Defines these classes FCDTargetedEntity		// And references these classes FCDEntity
%include "FCDocument\FCDTexture.h"		// Defines these classes FCDTexture		// And references these classes FCDObject, FCDExtra
%include "FCDocument\FCDAnimation.h"		// Defines these classes FCDAnimation		// And references these classes FCDEntity
%include "FCDocument\FCDAnimationClip.h"		// Defines these classes FCDAnimationClip		// And references these classes FUTrackedList, FCDEntity
%include "FCDocument\FCDCamera.h"		// Defines these classes FCDCamera		// And references these classes FCDTargetedEntity
%include "FCDocument\FCDController.h"		// Defines these classes FCDController		// And references these classes FCDEntity
%include "FCDocument\FCDEffect.h"		// Defines these classes FCDEffect		// And references these classes FCDEntity
%include "FCDocument\FCDEmitter.h"		// Defines these classes FCDEmitter		// And references these classes FCDEntity
%include "FCDocument\FCDEntityInstance.h"		// Defines these classes FCDEntityInstance, FCDEntityInstanceFactory		// And references these classes FCDObject, FUTracker, FCDExtra, node, FCDEntity
%include "FCDocument\FCDGeometryInstance.h"		// Defines these classes FCDGeometryInstance		// And references these classes FCDEntityInstance
%include "FCDocument\FCDLight.h"		// Defines these classes FCDLight		// And references these classes FCDTargetedEntity
%include "FCDocument\FCDMaterialInstance.h"		// Defines these classes FCDMaterialInstanceBind, FCDMaterialInstanceBindVertexInput, FCDMaterialInstance		// And references these classes FUParameterizable, FCDEntityInstance
%include "FCDocument\FCDPhysicsForceFieldInstance.h"		// Defines these classes FCDPhysicsForceFieldInstance		// And references these classes FCDEntityInstance
%include "FCDocument\FCDPhysicsModelInstance.h"		// Defines these classes FCDPhysicsModelInstance		// And references these classes FCDEntityInstance
%include "FCDocument\FCDPhysicsRigidBodyInstance.h"		// Defines these classes FCDPhysicsRigidBodyInstance		// And references these classes FCDEntityInstance
%include "FCDocument\FCDPhysicsRigidConstraintInstance.h"		// Defines these classes FCDPhysicsRigidConstraintInstance		// And references these classes FCDEntityInstance
%include "FCDocument\FCDControllerInstance.h"		// Defines these classes FCDControllerInstance		// And references these classes FUTrackedList, FCDGeometryInstance
%include "FCDocument\FCDEmitterInstance.h"		// Defines these classes FCDEmitterInstance		// And references these classes FCDEntityInstance

%include "FCDEffectProfileCast.h"
%include "FCDEntityCast.h"
%include "FCDEntityDeletion.h"
%include "FCDEntityInstanceCast.h"
%include "FCDGeometryPolygonsInputDeletion.h"
%include "FCDTransformCast.h"
%include "Rtfs_wrap.h"

