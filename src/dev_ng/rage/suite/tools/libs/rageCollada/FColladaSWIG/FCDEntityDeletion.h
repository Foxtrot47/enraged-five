#ifndef __FCDENTITY_DELETEION_H__
#define __FCDENTITY_DELETEION_H__

inline void DeleteEntityAndRemoveFromLists( FCDEntity* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDAnimation* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDAnimationClip* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDController* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDEffect* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDEmitter* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDForceField* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDGeometry* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDImage* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDMaterial* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDPhysicsAnalyticalGeometry* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDPhysicsMaterial* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDPhysicsModel* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDPhysicsRigidBody* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDPhysicsRigidConstraint* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDPhysicsScene* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDSceneNode* pEntity )
{
	delete pEntity;
}

inline void DeleteEntityAndRemoveFromLists( FCDTargetedEntity* pEntity )
{
	delete pEntity;
}
#endif // __FCDENTITY_DELETEION_H__
