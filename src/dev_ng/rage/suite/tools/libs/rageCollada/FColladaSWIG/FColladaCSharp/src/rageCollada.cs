using System;
using System.Collections.Generic;
using System.Text;
using rageUsefulCSharpToolClasses;
using OrganicBit.Zip;

namespace FColladaCS
{
	public class rageCollada
	{
		public static bool LoadDocument(FCDocument document, string filename)
		{
			// Does the filename end with dae?
			if(filename.ToLower().EndsWith(".dae"))
			{
				// Given a dae, so just save normally
				return FColladaCSharp.LoadDocumentFromFile(document, filename);
			}
			else if (filename.ToLower().EndsWith(".zip"))
			{
				// Open the zip file
				ZipReader reader = new ZipReader(filename);

				reader.Reset();
				ZipEntry obColladaFile = null;
				do
				{
					reader.MoveNext();
					obColladaFile = reader.Current;
					if (obColladaFile == null)
					{
						Console.Error.WriteLine("Error occurred locating any dae files in zipfile " + filename);
						return false;
					}
				} while (!obColladaFile.Name.ToLower().EndsWith(".dae"));

				// Got dae, so get data out of it
				int iDataSize = (int)obColladaFile.Length;
				byte[] managedData = new byte[iDataSize];
				reader.Read(managedData, 0, iDataSize);
				reader.Close();

				// Pass data to FCollada
				IntPtr pDataPtr = System.Runtime.InteropServices.Marshal.AllocHGlobal(managedData.Length);
				System.Runtime.InteropServices.Marshal.Copy(managedData, 0, pDataPtr, managedData.Length);
				SWIGTYPE_p_void obDataPtr = new SWIGTYPE_p_void(pDataPtr, true);
				string strFilenameInZip = rageFileUtilities.RemoveFileExtension((rageFileUtilities.GetFilenameFromFilePath(filename))) + ".dae";
				bool bReturnMe = FColladaCSharp.LoadDocumentFromMemory(strFilenameInZip, document, obDataPtr, (uint)iDataSize);
				return bReturnMe;
			}
			else
			{
				Console.Error.WriteLine("Unknown fileextension while loading " + filename);
				return false;
			}
		}

		public static bool SaveDocument(FCDocument document, string filename)
		{
			// Does the filename end with dae?
			if(filename.ToLower().EndsWith(".dae"))
			{
				// Given a dae, so just save normally
				return FColladaCSharp.SaveDocument(document, filename);
			}
			else if(filename.ToLower().EndsWith(".zip"))
			{
                /*
				// Open the zip file
				ZipWriter writer = new ZipWriter(filename);

				// Create an entry for it
				string strFilenameInZip = rageFileUtilities.RemoveFileExtension((rageFileUtilities.GetFilenameFromFilePath(filename))) + ".dae";
				ZipEntry entry = new ZipEntry(strFilenameInZip);
				// entry.ModifiedTime = File.GetLastWriteTime(strPathAndFilename);
				// entry.Comment = "local file comment";
				entry.Level = (int)CompressionLevel.Smallest;
				writer.AddEntry(entry);

				// Get data
				// int iDataSize = 0;
				UInt32 uiDataSize = 10;
				System.Runtime.InteropServices.GCHandle gch = System.Runtime.InteropServices.GCHandle.Alloc(uiDataSize);
				IntPtr pDataSizePtr = System.Runtime.InteropServices.GCHandle.ToIntPtr(gch); 
				SWIGTYPE_p_unsigned_int obDataSize = new SWIGTYPE_p_unsigned_int(pDataSizePtr, true);

				char cData = 'k';
				System.Runtime.InteropServices.GCHandle gch2 = System.Runtime.InteropServices.GCHandle.Alloc(cData);
				IntPtr pDataPtr = System.Runtime.InteropServices.GCHandle.ToIntPtr(gch2);
				System.Runtime.InteropServices.GCHandle gch3 = System.Runtime.InteropServices.GCHandle.Alloc(pDataPtr);
				IntPtr ppDataPtrPtr = System.Runtime.InteropServices.GCHandle.ToIntPtr(gch3);
				SWIGTYPE_p_p_char obData = new SWIGTYPE_p_p_char(ppDataPtrPtr, true);

				// Call into FCollda
                bool bReturnMe = FColladaCSharp.SaveDocumentToMemory(document, rageFileUtilities.RemoveFileExtension(filename) + ".dae", obData, obDataSize);

				// Get data out of unmanaged land
				int iDataSize = System.Runtime.InteropServices.Marshal.ReadInt32(pDataSizePtr);
				pDataPtr = System.Runtime.InteropServices.Marshal.ReadIntPtr(ppDataPtrPtr);
				byte[] managedData = new byte[iDataSize];
				System.Runtime.InteropServices.Marshal.Copy(pDataPtr, managedData, 0, iDataSize);

				// Write data into zip
				writer.Write(managedData, 0, iDataSize);
				writer.Close();

				// return bReturnMe;
				return bReturnMe;
                 */
                return false;
			}
			else
			{
				Console.Error.WriteLine("Unknown fileextension while saving " + filename);
				return false;
			}
		}
	}
}
