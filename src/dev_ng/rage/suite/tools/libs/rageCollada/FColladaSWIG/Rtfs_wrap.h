#ifndef RTFS_WRAP
#define RTFS_WRAP

#include "rtfs.h"

inline int FSInit()
{
	return rtfs::Init();
}

inline int FSDeinit()
{
	return rtfs::Deinit();
}

inline int FSMount( const char* dir, const char* point, int append )
{
	return rtfs::Mount( dir, point, append );
}

inline int FSRemoveFromPath( const char* dir )
{
	return rtfs::RemoveFromPath( dir );
}

inline int FSMkdir( const char* dir )
{
	return rtfs::Mkdir( dir );
}

char** FSEnumerateFiles( const char* dir )
{
	return rtfs::EnumerateFiles( dir );
}

void FSFreeList( void* list )
{
	rtfs::FreeList( list );
}

#endif // RTFS_WRAP 
