#include "rcMeshTools.h"

#include "FCDocument/FCDGeometryMesh.h"
#include "FCDocument/FCDGeometryPolygons.h"

namespace rage
{
	struct HashIndexMapItem { UInt32List allValues; UInt32List newIndex; };
	typedef fm::vector<UInt32List> UInt32ListList;
	typedef fm::pvector<FCDGeometryPolygonsInput> InputList;
	typedef fm::map<uint32, HashIndexMapItem> HashIndexMap;

	void GenerateUniqueIndices(FCDGeometryMesh* mesh, FCDGeometryPolygons* polygonsToProcess, FCDGeometryIndexTranslationMap* translationMap)
	{
		// Prepare a list of unique index buffers.
		size_t polygonsCount = mesh->GetPolygonsCount();
		if (polygonsCount == 0) return;
		UInt32ListList indexBuffers; indexBuffers.resize(polygonsCount);
		size_t totalVertexCount = 0;

		// Fill in the index buffers for each polygons set.
		for (size_t p = 0; p < polygonsCount; ++p)
		{
			UInt32List& indexBuffer = indexBuffers[p];
			FCDGeometryPolygons* polygons = mesh->GetPolygons(p);
			// DO NOT -EVER- TOUCH MY INDICES - (Says Psuedo-FCDGeometryPoints)
			// Way to much code assumes (and carefully guards) the existing sorted structure
			if (polygons->GetPrimitiveType() == FCDGeometryPolygons::POINTS) return;
			if (polygonsToProcess != NULL && polygons != polygonsToProcess) continue;

			// Find all the indices list to determine the hash size.
			InputList idxOwners;
			size_t inputCount = polygons->GetInputCount();
			FCDGeometryPolygonsInput** inputs = polygons->GetInputs();
			for (size_t i = 0; i < inputCount; ++i)
			{
				if (inputs[i]->OwnsIndices())
				{
					// Drop index lists with the wrong number of values and avoid repeats
					FUAssert(idxOwners.empty() || idxOwners.front()->GetIndexCount() == inputs[i]->GetIndexCount(), continue);
					if (idxOwners.find(inputs[i]) == idxOwners.end())
					{
						idxOwners.push_back(inputs[i]);
					}
				}
			}
			size_t listCount = idxOwners.size();
			if (listCount == 0) continue; // no inputs?

			// Set-up a simple hashing function.
			UInt32List hashingFunction;
			uint32 hashSize = (uint32) listCount;
			hashingFunction.reserve(hashSize);
			for (uint32 h = 0; h < hashSize; ++h) hashingFunction.push_back(32 * h / hashSize);

			// Iterate over the index lists, hashing/merging the indices.
			HashIndexMap hashMap;
			size_t originalIndexCount = idxOwners.front()->GetIndexCount();
			indexBuffer.reserve(originalIndexCount);
			for (size_t i = 0; i < originalIndexCount; ++i)
			{
				// Generate the hash value for this vertex-face pair.
				uint32 hashValue = 0;
				for (size_t l = 0; l < listCount; ++l)
				{
					hashValue ^= (idxOwners[l]->GetIndices()[i]) << hashingFunction[l];
				}

				// Look for this value in the already-collected ones.
				HashIndexMap::iterator it = hashMap.find(hashValue);
				HashIndexMapItem* hashItem;
				uint32 newIndex = (uint32) totalVertexCount;
				if (it != hashMap.end())
				{
					hashItem = &((*it).second);
					size_t repeatCount = hashItem->allValues.size() / listCount;
					for (size_t r = 0; r < repeatCount && newIndex == totalVertexCount; ++r)
					{
						size_t l;
						for (l = 0; l < listCount; ++l)
						{
							if (idxOwners[l]->GetIndices()[i] != hashItem->allValues[r * listCount + l]) break;
						}
						if (l == listCount)
						{
							// We have a match: re-use this index.
							newIndex = hashItem->newIndex[r];
						}
					}
				}
				else
				{
					HashIndexMap::iterator k = hashMap.insert(hashValue, HashIndexMapItem());
					hashItem = &k->second;
				}

				if (newIndex == totalVertexCount)
				{
					// Append this new value/index to the hash map item and to the index buffer.
					for (size_t l = 0; l < listCount; ++l)
					{
						hashItem->allValues.push_back(idxOwners[l]->GetIndices()[i]);
					}
					hashItem->newIndex.push_back(newIndex);
					totalVertexCount++;
				}
				indexBuffer.push_back(newIndex);
			}
		}

		// De-reference the source data so that all the vertex data match the new indices.
		size_t meshSourceCount = mesh->GetSourceCount();
		for (size_t d = 0; d < meshSourceCount; ++d)
		{
			FCDGeometrySource* oldSource = mesh->GetSource(d);
			uint32 stride = oldSource->GetStride();
			const float* oldVertexData = oldSource->GetData();
			bool isPositionSource = oldSource->GetType() == FUDaeGeometryInput::POSITION && translationMap != NULL;
			FloatList vertexBuffer;
			vertexBuffer.resize(stride * totalVertexCount, 0.0f);

			// When processing just one polygons set, duplicate the source
			// so that the other polygons set can correctly point to the original source.
			FCDGeometrySource* newSource = (polygonsToProcess != NULL) ? mesh->AddSource(oldSource->GetType()) : oldSource;

			FCDAnimatedList newAnimatedList;
			newAnimatedList.clear();
			for (size_t p = 0; p < polygonsCount; ++p)
			{
				const UInt32List& indexBuffer = indexBuffers[p];
				FCDGeometryPolygons* polygons = mesh->GetPolygons(p);
				if (polygonsToProcess != NULL && polygonsToProcess != polygons) continue;
				FCDGeometryPolygonsInput* oldInput = polygons->FindInput(oldSource);
				if (oldInput == NULL) continue;

				// Retrieve the old list of indices and de-reference the data values.
				uint32* oldIndexList = oldInput->GetIndices();
				size_t oldIndexCount = oldInput->GetIndexCount();
				if (oldIndexList == NULL || oldIndexCount == 0) continue;

				size_t indexCount = min(oldIndexCount, indexBuffer.size());
				for (size_t i = 0; i < indexCount; ++i)
				{
					uint32 newIndex = indexBuffer[i];
					uint32 oldIndex = oldIndexList[i];
					if (oldIndex >= oldSource->GetValueCount()) continue;

					FCDAnimatedList& animatedValues = oldSource->GetAnimatedValues();
					FCDAnimated* oldAnimated = NULL;
					for (size_t j = 0; j < animatedValues.size(); j++)
					{
						FCDAnimated* animated = animatedValues[j];
						if (animated->GetValue(0) == &(oldVertexData[stride * oldIndex]))
						{
							oldAnimated = animated;
							break;
						}
					}
					if (oldAnimated != NULL)
					{
						FCDAnimated* newAnimated = oldAnimated->Clone(oldAnimated->GetDocument());
						newAnimated->SetArrayElement(newIndex);
						newAnimatedList.push_back(newAnimated);
					}

					// [GLaforte - 12-10-2006] Potential performance optimization: this may copy the same data over itself many times.
					for (uint32 s = 0; s < stride; ++s)
					{
						vertexBuffer[stride * newIndex + s] = oldVertexData[stride * oldIndex + s];

						// Add this value to the vertex position translation map.
						if (isPositionSource)
						{
							FCDGeometryIndexTranslationMap::iterator itU = translationMap->find(oldIndex);
							if (itU == translationMap->end()) { itU = translationMap->insert(oldIndex, UInt32List()); }
							UInt32List::iterator itF = itU->second.find(newIndex);
							if (itF == itU->second.end()) itU->second.push_back(newIndex);
						}
					}
				}

				if (polygonsToProcess != NULL)
				{
					// Change the relevant input, if it exists, to point towards the new source.
					uint32 set = oldInput->GetSet();
					SAFE_RELEASE(oldInput);
					FCDGeometryPolygonsInput* newInput = polygons->AddInput(newSource, 0);
					newInput->SetSet(set);
				}
			}

			// Set the compiled data in the source.
			// [ewhittom] In some occasions we have no indices and yet
			// vertices (for example in incomplete meshes). In these cases we do
			// not want to clear the vertices.
			if (!(vertexBuffer.empty() && newSource->GetDataCount() != 0))
			{
				newSource->SetData(vertexBuffer, stride);
			}
			FCDAnimatedList& animatedList = newSource->GetAnimatedValues();
			animatedList.clear();
			for (FCDAnimatedList::iterator it = newAnimatedList.begin();
					it != newAnimatedList.end(); it++)
			{
				animatedList.push_back(*it);
			}
		}

		// find sources with non default Set since they cannot be per-vertex
		FUObjectContainer<FCDGeometrySource> setSources;
		for (size_t p = 0; p < polygonsCount; ++p)
		{
			FCDGeometryPolygons* polygons = mesh->GetPolygons(p);
			if (polygonsToProcess != NULL && polygons != polygonsToProcess) continue;
			size_t inputCount = polygons->GetInputCount();
			for (size_t inputIndex = 0; inputIndex < inputCount; inputIndex++)
			{
				FCDGeometryPolygonsInput* input = polygons->GetInput(inputIndex);
				if (input->GetSet() != -1)
				{
					FCDGeometrySource* source = input->GetSource();
					if (setSources.find(source) == setSources.end())
					{
						setSources.push_back(source);
					}
				}
			}
		}

		if (polygonsToProcess == NULL)
		{
			// Next, make all the sources per-vertex.
			size_t _sourceCount = mesh->GetSourceCount();
			for (size_t s = 0; s < _sourceCount; ++s)
			{
				FCDGeometrySource* it = mesh->GetSource(s);
				if (!mesh->IsVertexSource(it) && (setSources.find(it) == setSources.end()))
				{
					mesh->AddVertexSource(it);
				}
			}
		}

		while (!setSources.empty())
		{
			setSources.pop_back();
		}

		// Enforce the index buffers.
		for (size_t p = 0; p < polygonsCount; ++p)
		{
			const UInt32List& indexBuffer = indexBuffers[p];
			FCDGeometryPolygons* polygons = mesh->GetPolygons(p);
			if (polygonsToProcess != NULL && polygons != polygonsToProcess) continue;

			size_t inputCount = polygons->GetInputCount();
			for (size_t i = 0; i < inputCount; i++)
			{
				FCDGeometryPolygonsInput* anyInput = polygons->GetInput(i);
				if (anyInput->GetSource()->GetDataCount() == 0) continue;
				// [ewhittom] Allow empty index buffers with non-empty vertex buffers
				if (indexBuffer.empty()) continue;

				anyInput->SetIndices(&indexBuffer.front(), indexBuffer.size());
			}
		}

		// Let the world know that we've deindexed this geometry
		for ( size_t p = 0; p < polygonsCount; ++p )
		{
			FCDGeometryPolygons* polygons = mesh->GetPolygons(p);
			if ( NULL != polygonsToProcess && polygons != polygonsToProcess )
			{
				continue;
			}

			FCDExtra* extra = polygons->GetExtra();
			FCDETechnique* technique = 
				extra->GetDefaultType()->AddTechnique( DAE_RAGE_PROFILE );

			technique->AddParameter( "deindexed", true );
		}
	}
}
