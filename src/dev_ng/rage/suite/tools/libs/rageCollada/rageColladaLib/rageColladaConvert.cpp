#include "rageColladaConvert.h"

#include "rageColladaProperties.h"
#include "rageColladaUtil.h"
#include "mgc.h"

#include <algorithm>
#include <list>
#include <map>
#include <stack>

namespace
{
	static const float IDENTITY[] = { 1, 0, 0, 0, 0, 1, 0 ,0 ,0, 0, 1, 0, 0, 0, 0, 1 };
}

static const int INVALID_INDEX = -1;

using namespace std;
using namespace rage;

static mshPrimType GetPrimitiveType( const FCDGeometryPolygons* polygons )
{
	if ( polygons->IsTriangles() )
	{
		return mshTRIANGLES;
	}

	switch ( polygons->GetPrimitiveType() )
	{
	case FCDGeometryPolygons::LINES:
		return mshLINES;

	case FCDGeometryPolygons::LINE_STRIPS:
		Assert( false );
		return mshPOLYGON;

	case FCDGeometryPolygons::TRIANGLE_FANS:
		Assert( false );
		return mshPOLYGON;

	case FCDGeometryPolygons::TRIANGLE_STRIPS:
		return mshTRISTRIP;

	case FCDGeometryPolygons::POINTS:
		return mshPOINTS;

	default:
		Assert( false );
		return mshPOLYGON;
	}
}

template < typename Type >
static void AddUniqueVertices( mshMaterial& material, const FCDGeometryPolygonsInput* input, Type func )
{
	const FCDGeometrySource* source = input->GetSource();

	const uint32* indices = input->GetIndices();
	int indexCount = int( input->GetIndexCount() );

	int stride = int( source->GetStride() );

	typedef std::vector< uint32 > UniqueVertexList;
	UniqueVertexList uniqueVerts;
	uniqueVerts.reserve( indexCount );

	int currIndex = 0;

	for ( int indexIndex = 0; indexIndex < indexCount; ++indexIndex )
	{
		uint32 index = indices[ indexIndex ];

		UniqueVertexList::const_iterator it = std::find( uniqueVerts.begin(), uniqueVerts.end(), index );
		if ( it != uniqueVerts.end() )
		{
			continue;
		}

		uniqueVerts.push_back( index );						

		const float* start = source->GetData() + ( index * stride );

		func( material, start, currIndex++ );
	}
}

class AddPositionsFunctor
{
public:
	AddPositionsFunctor()
		: m_transformMtx( Matrix34::IdentityType )
	{};

	AddPositionsFunctor(Matrix34& transformMtx)
		: m_transformMtx(transformMtx)
	{};

	void operator()( mshMaterial& material, const float* positions, int index )
	{
		Vector3 vPos( positions[ 0 ], positions[ 1 ], positions[ 2 ] );
		m_transformMtx.Transform( vPos );
		material.SetPos( index, vPos );
	}

public:
	Matrix34 m_transformMtx;
};

class AddNormalsFunctor
{
public:
	void operator()( mshMaterial& material, const float* normals, int index )
	{
		material.SetNormal( index, Vector3( normals[ 0 ], normals[ 1 ], normals[ 2 ] ) );
	}
};

class AddTexCoordsFunctor
{
public:
	void operator()( mshMaterial& material, const float* texcoords, int index )
	{
		material.Verts[ index ].TexCount++;

		material.SetTexCoord( index, 0, Vector2( texcoords[ 0 ], texcoords[ 1 ] ) );
	}
};

static void FindMinMaxIndices( int& min, int& max, const FCDGeometryPolygonsInput* input )
{
	int indexCount = int( input->GetIndexCount() );
	const uint32* indices = input->GetIndices();

	min = INT_MAX; 
	max = -INT_MAX;

	for ( int i = 0; i < indexCount; ++i )
	{
		int index = int( indices[ i ] );

		min = rage::Min( min, index );
		max = rage::Max( max, index );
	}
}

static int ComputeVertexCount( const FCDGeometryPolygonsInput* input )
{
	int indexCount = int( input->GetIndexCount() );
	const uint32* indices = input->GetIndices();

	int min = INT_MAX, max = -INT_MAX;

	for ( int i = 0; i < indexCount; ++i )
	{
		int index = int( indices[ i ] );

		min = rage::Min( min, index );
		max = rage::Max( max, index );
	}

	return ( max - min ) + 1;
}

static int ComputeVertexCount( int min, int max )
{
	return ( max - min ) + 1;
}

static int FindMaterialIndex( const rage::FCDMaterialList& materials, const char* symbol )
{
	for ( int index = 0; index < int( materials.size() ); ++index )
	{
		if ( 0 == strcmp( materials[ index ].first.c_str(), symbol ) )
		{
			return index;
		}
	}

	return -1;
}

template < typename Type >
static void SetCentroidOffset( const FCDSceneNode* sceneNode, Type& bound )
{
	size_t transformCount = sceneNode->GetTransformCount();
	for ( size_t transformIndex = 0; transformIndex < transformCount; ++transformIndex )
	{
		const FCDTransform* transform = sceneNode->GetTransform( transformIndex );
		if ( FCDTransform::TRANSLATION == transform->GetType() )
		{
			const FCDTTranslation* translation = 
				dynamic_cast< const FCDTTranslation* >( transform );

			FMVector3 trans = translation->GetTranslation();
			rage::Vector3 offset ( trans.x, trans.y, trans.y );

			bound.SetCentroidOffset( offset );
		}
	}
}

static bool IsConditioned( const FCDGeometryPolygons* polygons )
{
	// Check that we have triangles

	if ( polygons->GetPrimitiveType() == FCDGeometryPolygons::LINE_STRIPS || 
		polygons->GetPrimitiveType() == FCDGeometryPolygons::LINES || 
		polygons->GetPrimitiveType() == FCDGeometryPolygons::POINTS) 
	{
		Warningf( "Geometry has not been triangulated correctly" );
		return false;
	}

	const uint32* faceVertexCounts = polygons->GetFaceVertexCounts();
	size_t faceCount = polygons->GetFaceVertexCountCount();
	for ( size_t faceIndex = 0; faceIndex < faceCount; ++faceIndex )
	{
		if ( 3 != faceVertexCounts[ faceIndex ] )
		{
			Warningf( "Geometry has not been triangulated correctly" );
			return false;
		}
	}

	// Check that the geometry is deindexed?


	return true;
}

namespace rage {

	void ConvertMesh( mshMesh& mesh, const FCDGeometry* pGeometry, const FCDMaterialList& materials, FMMatrix44& transformMtx )
	{
		std::vector< Mgc::Vector3 > points;

		const FCDGeometryMesh* pMesh = pGeometry->GetMesh();

		size_t polygonsCount = pMesh->GetPolygonsCount();
		for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
		{
			const FCDGeometryPolygons* polygons = pMesh->GetPolygons( polygonsIndex );

			mshMaterial& material = mesh.NewMtl();
			int name = FindMaterialIndex( materials, polygons->GetMaterialSemantic().c_str() );
			Assert( INVALID_INDEX != name );

			char buffer[ 64 ];
			sprintf( buffer, "#%d", name );
			material.Name = buffer;

			// assumes the data is de-indexed, would rather use VERTEX, but it's useless
			const FCDGeometryPolygonsInput* input = polygons->FindInput( FUDaeGeometryInput::POSITION );

			int min, max;
			FindMinMaxIndices( min, max, input );

			// we can't just lump all the positions in otherwise we get unwanted data in each prim vert set

			const FCDGeometryPolygonsInput* pPositionsInput = polygons->FindInput( FUDaeGeometryInput::POSITION );
			const FCDGeometrySource* pPositionsSource = pPositionsInput->GetSource();
			const float* pPositions = pPositionsSource->GetData();

			const FCDGeometryPolygonsInput* pNormalsInput = polygons->FindInput( FUDaeGeometryInput::NORMAL );
			const FCDGeometrySource* pNormalsSource = pNormalsInput->GetSource();
			const float* pNormals = pNormalsSource->GetData();

			FCDGeometryPolygonsInputList texcoordInputs;
			FCDGeometryPolygonsInputList cpvInputs;

			size_t inputCount = polygons->GetInputCount();
			for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
			{
				const FCDGeometryPolygonsInput* pInput = polygons->GetInput( inputIndex );

				if ( FUDaeGeometryInput::TEXCOORD == pInput->GetSemantic() )
				{
					texcoordInputs.push_back( pInput );
				}
				else
				if ( FUDaeGeometryInput::COLOR == pInput->GetSemantic() )
				{
					cpvInputs.push_back( pInput );
				}
			}

			const FCDGeometryPolygonsInput* pVertexInput = polygons->FindInput( FUDaeGeometryInput::POSITION );
			Assert( pVertexInput );

			typedef std::vector< uint32 > UniqueIndexList;
			UniqueIndexList uniqueIndices;

			size_t indexCount = pPositionsInput->GetIndexCount();
			const uint32* pIndices = pPositionsInput->GetIndices();

			uniqueIndices.reserve( indexCount );

			for ( size_t indexIndex = 0; indexIndex < indexCount; ++indexIndex )
			{
				uint32 index = pIndices[ indexIndex ];

				uint32 positionIndex = index * pPositionsSource->GetStride();

				rage::Vector3 position ( pPositions[ positionIndex ], pPositions[ positionIndex + 1 ], pPositions[ positionIndex + 2 ] );
				Mgc::Vector3 point( position.x, position.y, position.z );
				points.push_back( point );

				//rageBindShapeTransformMtx.Transform( position );
				//rageParentNodeWorldTransformMtx.UnTransform( position );

				uint32 normalIndex = index * pNormalsSource->GetStride();

				rage::Vector3 normal ( pNormals[ normalIndex ], pNormals[ normalIndex + 1 ], pNormals[ normalIndex + 2 ] );

				size_t texcoordCount = texcoordInputs.size();
				std::vector< rage::Vector2 > texcoords( texcoordCount );
				
				for ( size_t texcoordIndex = 0; texcoordIndex < texcoordCount; ++texcoordIndex )
				{
					const FCDGeometryPolygonsInput* pTexCoordInput = texcoordInputs[ texcoordIndex ];
					const FCDGeometrySource* pTexCoordSource = pTexCoordInput->GetSource();
					const float* pTexCoords = pTexCoordSource->GetData();

					uint32 coordIndex = index * pTexCoordSource->GetStride();

					rage::Vector2 texcoord ( pTexCoords[ coordIndex ], pTexCoords[ coordIndex ] );
					texcoords[ texcoordIndex ] = texcoord;
				}
		
				rage::Vector4 cpv[ 3 ] = { 
					rage::Vector4( 1.f, 1.f, 1.f, 1.f ),
					rage::Vector4( 1.f, 1.f, 1.f, 1.f ),
					rage::Vector4( 1.f, 1.f, 1.f, 1.f ) };

				size_t cpvCount = cpvInputs.size();
				for ( size_t cpvIndex = 0; cpvIndex < cpvCount; ++cpvIndex )
				{
					const FCDGeometryPolygonsInput* pCpvInput = cpvInputs[ cpvIndex ];
					const FCDGeometrySource* pCpvSource = pCpvInput->GetSource();
					const float* pCpvData = pCpvSource->GetData();

					uint32 cpvIdx = index * pCpvSource->GetStride();
					cpv[ cpvIndex ].Set( 
						pCpvData[ cpvIdx ], pCpvData[ cpvIdx + 1 ], pCpvData[ cpvIdx + 2 ], pCpvData[ cpvIdx + 3 ] );
				}

				mshBinding binding;
				binding.Reset();

				UniqueIndexList::iterator found = 
					std::find( uniqueIndices.begin(), uniqueIndices.end(), index );
				if ( found == uniqueIndices.end() )
				{
					// TODO: MULTIPLE UV SETS
					int vertCount = material.AddVertex(
						position, binding, normal, cpv[ 0 ], cpv[ 1 ], cpv[ 2 ], int( texcoordCount ), &texcoords[ 0 ], 0, NULL );

					const FCDGeometryPolygonsInput* pTangentInput = 
						polygons->FindInput( FUDaeGeometryInput::TEXTANGENT );
					const FCDGeometryPolygonsInput* pBiTangentInput = 
						polygons->FindInput( FUDaeGeometryInput::TEXBINORMAL );
					if ( NULL != pTangentInput && NULL != pBiTangentInput )
					{
						mshVertex& v = material.Verts[ vertCount ];
						v.TanBiCount = 1;

						const FCDGeometrySource* pTangentSource = pTangentInput->GetSource();
						const float* pTangentData = pTangentSource->GetData();

						uint32 tangentIndex = index * pTangentSource->GetStride();

						v.TanBi[ 0 ].T.Set( 
							pTangentData[ tangentIndex ], 
							pTangentData[ tangentIndex + 1 ], 
							pTangentData[ tangentIndex + 2 ] );

						const FCDGeometrySource* pBiTangentSource = pBiTangentInput->GetSource();
						const float* pBiTangentData = pBiTangentSource->GetData();

						uint32 biTangentIndex = index * pBiTangentSource->GetStride();

						v.TanBi[ 0 ].B.Set( 
							pBiTangentData[ biTangentIndex ], 
							pBiTangentData[ biTangentIndex + 1 ], 
							pBiTangentData[ biTangentIndex + 2 ] );
					}

					uniqueIndices.push_back( index );
				}
			}

			mshPrimitive& prim = material.Prim.Append();

			prim.Type = GetPrimitiveType( polygons );
			prim.Priority = material.Priority;

			prim.Idx.Reallocate( int( indexCount ) );

			for ( size_t indexIndex = 0; indexIndex < indexCount; ++indexIndex )
			{
				prim.Idx.Append() = pIndices[ indexIndex ] - min;
			}
		}

		mesh.Weld();
		mesh.ComputeTanBi(0);
		mesh.RemoveDegenerates();
	}

	void ConvertMesh( mshMesh& mesh, const FCDSkinController* pSkin, const FCDMaterialList& materials, FMMatrix44& parentNodeWorldTransformMtx )
	{
		//Normalize the skin influences to a maximum of 4 joint influences per vertex
		(const_cast<FCDSkinController*>(pSkin))->ReduceInfluences(4);

		std::vector< Mgc::Vector3 > points;

		std::vector<const FCDSceneNode*>		orderedJointNodes;
		std::map<size_t, size_t>				colladaJointIndexToRageBoneIndexMap;

		GetRageJointOrdering(pSkin, orderedJointNodes, colladaJointIndexToRageBoneIndexMap);

		const FMMatrix44& bindShapeMtx = pSkin->GetBindShapeTransform();
		Matrix34 rageBindShapeTransformMtx;
		ToMatrix34( rageBindShapeTransformMtx, bindShapeMtx);

		Matrix34 rageParentNodeWorldTransformMtx;
		ToMatrix34( rageParentNodeWorldTransformMtx, parentNodeWorldTransformMtx);

		mesh.MakeSkinned();

		const FCDController* pController = pSkin->GetParent();
		AssertMsg( pController, "Could not get skin controller parent." );
		const FCDGeometry* geometry = pController->GetBaseGeometry();
		Assertf( geometry, "Could not get base geometry for controller '%s'", pController->GetName().c_str());
		const FCDGeometryMesh* inputMesh = geometry->GetMesh();
		Assertf( inputMesh, "Could not get mesh for geometry '%s'", geometry->GetName().c_str());

		size_t polygonsCount = inputMesh->GetPolygonsCount();

		for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
		{
			const FCDGeometryPolygons* polygons = inputMesh->GetPolygons( polygonsIndex );
			
			if(!polygons->GetFaceVertexCount())
				continue;

			int name = FindMaterialIndex( materials, polygons->GetMaterialSemantic().c_str() );
			Assert( INVALID_INDEX != name );

			// assumes the data is de-indexed, would rather use VERTEX, but it's useless
			const FCDGeometryPolygonsInput* input = polygons->FindInput( FUDaeGeometryInput::POSITION );

			// we can't just lump all the positions in otherwise we get unwanted data in each prim vert set

			const FCDGeometryPolygonsInput* pPositionsInput = polygons->FindInput( FUDaeGeometryInput::POSITION );
			const FCDGeometrySource* pPositionsSource = pPositionsInput->GetSource();
			const float* pPositions = pPositionsSource->GetData();

			const FCDGeometryPolygonsInput* pNormalsInput = polygons->FindInput( FUDaeGeometryInput::NORMAL );
			const FCDGeometrySource* pNormalsSource = pNormalsInput->GetSource();
			const float* pNormals = pNormalsSource->GetData();

			FCDGeometryPolygonsInputList texcoordInputs;
			FCDGeometryPolygonsInputList cpvInputs;

			size_t inputCount = polygons->GetInputCount();
			for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
			{
				const FCDGeometryPolygonsInput* pInput = polygons->GetInput( inputIndex );

				if ( FUDaeGeometryInput::TEXCOORD == pInput->GetSemantic() )
				{
					texcoordInputs.push_back( pInput );
				}
				else
				if ( FUDaeGeometryInput::COLOR == pInput->GetSemantic() )
				{
					cpvInputs.push_back( pInput );
				}
			}

			size_t influenceCount = pSkin->GetInfluenceCount();
			const FCDSkinControllerVertex* pInfluences = pSkin->GetVertexInfluences();

			const FCDGeometryPolygonsInput* pVertexInput = polygons->FindInput( FUDaeGeometryInput::POSITION );
			Assert( pVertexInput );

			typedef std::vector< uint32 > UniqueIndexList;
			UniqueIndexList uniqueIndices;

			size_t indexCount = pPositionsInput->GetIndexCount();
			const uint32* pIndices = pPositionsInput->GetIndices();

			size_t firstIndexIndex = 0;
			size_t lastIndexIndex = 0;
			typedef std::vector<int> UniqueJointList;
			UniqueJointList uniqueJoints;

			do 
			{
				mshMaterial& material = mesh.NewMtl();

				char buffer[ 64 ];
				sprintf( buffer, "#%d", name );
				material.Name = buffer;

				// Determine the range of indices we can use for this material without
				// going over the max number of matrices per material
				int matrixCount = 0;
				uniqueJoints.clear();
				for ( size_t indexIndex = firstIndexIndex; indexIndex < indexCount; ++indexIndex )
				{
					FCDSkinControllerVertex influence = pInfluences[ pIndices[indexIndex] ];
					
					size_t pairCount = influence.GetPairCount();
					for ( size_t pairIndex = 0; pairIndex < pairCount; ++pairIndex )
					{
						int jointIndex = influence.GetPair(pairIndex)->jointIndex;
						float weight = influence.GetPair(pairIndex)->weight;

						// If this is a unique joint index, increment our matrix count
						UniqueJointList::iterator found = std::find( uniqueJoints.begin(), uniqueJoints.end(), jointIndex );
						if ( weight && found == uniqueJoints.end() )
						{
							++matrixCount;
							uniqueJoints.push_back( jointIndex );
						}
					}

					if ( matrixCount > mshMaxMatricesPerMaterial )
					{
						// There are too many matrices, so stop counting
						break;
					}

					lastIndexIndex = indexIndex;
				}

				size_t materialIndexCount = ( lastIndexIndex - firstIndexIndex ) + 1;

				int min = INT_MAX;
				int max = -INT_MAX;
				for ( size_t indexIndex = firstIndexIndex; indexIndex <= lastIndexIndex; ++indexIndex )
				{
					int index = int( pIndices[indexIndex] );
					min = rage::Min( min, index );
					max = rage::Max( max, index );
				}
				
				uniqueIndices.clear();
				uniqueIndices.reserve( int(materialIndexCount) );

				for ( size_t indexIndex = firstIndexIndex; indexIndex <= lastIndexIndex; ++indexIndex )
				{
					uint32 index = pIndices[ indexIndex ];

					uint32 positionIndex = index * pPositionsSource->GetStride();

					rage::Vector3 position ( pPositions[ positionIndex ], pPositions[ positionIndex + 1 ], pPositions[ positionIndex + 2 ] );
					Mgc::Vector3 point( position.x, position.y, position.z );
					points.push_back( point );

					//rageBindShapeTransformMtx.Transform( position );
					//rageParentNodeWorldTransformMtx.UnTransform( position );

					uint32 normalIndex = index * pNormalsSource->GetStride();

					rage::Vector3 normal ( pNormals[ normalIndex ], pNormals[ normalIndex + 1 ], pNormals[ normalIndex + 2 ] );

					size_t texcoordCount = texcoordInputs.size();
					std::vector< rage::Vector2 > texcoords ( texcoordCount );

					for ( size_t texcoordIndex = 0; texcoordIndex < texcoordCount; ++texcoordIndex )
					{
						const FCDGeometryPolygonsInput* pTexCoordInput = texcoordInputs[ texcoordIndex ];
						const FCDGeometrySource* pTexCoordSource = pTexCoordInput->GetSource();
						const float* pTexCoords = pTexCoordSource->GetData();

						uint32 coordIndex = index * pTexCoordSource->GetStride();

						rage::Vector2 texcoord ( pTexCoords[ coordIndex ], pTexCoords[ coordIndex + 1 ] );
						texcoords[ texcoordIndex ] = texcoord;
					}

					rage::Vector4 cpv[ 3 ] = { 
						rage::Vector4( 1.f, 1.f, 1.f, 1.f ),
						rage::Vector4( 1.f, 1.f, 1.f, 1.f ),
						rage::Vector4( 1.f, 1.f, 1.f, 1.f ) };

					size_t cpvCount = cpvInputs.size();
					for ( size_t cpvIndex = 0; cpvIndex < cpvCount; ++cpvIndex )
					{
						const FCDGeometryPolygonsInput* pCpvInput = cpvInputs[ cpvIndex ];
						const FCDGeometrySource* pCpvSource = pCpvInput->GetSource();
						const float* pCpvData = pCpvSource->GetData();

						uint32 cpvIdx = index * pCpvSource->GetStride();
						cpv[ cpvIndex ].Set( 
							pCpvData[ cpvIdx ], pCpvData[ cpvIdx + 1 ], pCpvData[ cpvIdx + 2 ], pCpvData[ cpvIdx + 3 ] );
					}

					mshBinding binding;
					binding.Reset();

					size_t pairCount = pInfluences[ index ].GetPairCount();
					Assert( pairCount <= mshMaxMatricesPerVertex );
					// TODO: HANDLE ABOVE ERROR CASE PROPERLY
					for ( size_t pairIndex = 0; pairIndex < pairCount; ++pairIndex )
					{
						const FCDJointWeightPair* pair = pInfluences[ index ].GetPair( pairIndex );
						// TODO: FIGURE OUT HOW RAGE HANDLES THE BIND BEING THE JOINT
						Assert( -1 != pair->jointIndex );

						std::map<size_t, size_t>::iterator boneIndexIt = colladaJointIndexToRageBoneIndexMap.find(pair->jointIndex);
						Assertf( boneIndexIt != colladaJointIndexToRageBoneIndexMap.end(), 
							"Collada joint index %d, has no corresponding Rage boneIndex", pair->jointIndex);
						int rageBoneIndex = boneIndexIt->second;
						binding.Mtx[ int( pairIndex ) ] = rageBoneIndex;
						binding.Wgt[ int( pairIndex ) ] = pair->weight;
					}

					UniqueIndexList::iterator found = 
						std::find( uniqueIndices.begin(), uniqueIndices.end(), index );
					if ( found == uniqueIndices.end() )
					{
						int vertCount = material.AddVertex(
							position, binding, normal, cpv[ 0 ], cpv[ 1 ], cpv[ 2 ], int( texcoordCount ), &texcoords[ 0 ], 0, NULL );

						const FCDGeometryPolygonsInput* pTangentInput = 
							polygons->FindInput( FUDaeGeometryInput::TEXTANGENT );
						const FCDGeometryPolygonsInput* pBiTangentInput = 
							polygons->FindInput( FUDaeGeometryInput::TEXBINORMAL );
						if ( NULL != pTangentInput && NULL != pBiTangentInput )
						{
							mshVertex& v = material.Verts[ vertCount ];
							v.TanBiCount = 1;

							const FCDGeometrySource* pTangentSource = pTangentInput->GetSource();
							const float* pTangentData = pTangentSource->GetData();

							uint32 tangentIndex = index * pTangentSource->GetStride();

							v.TanBi[ 0 ].T.Set( 
								pTangentData[ tangentIndex ], 
								pTangentData[ tangentIndex + 1 ], 
								pTangentData[ tangentIndex + 2 ] );

							const FCDGeometrySource* pBiTangentSource = pBiTangentInput->GetSource();
							const float* pBiTangentData = pBiTangentSource->GetData();

							uint32 biTangentIndex = index * pBiTangentSource->GetStride();

							v.TanBi[ 0 ].B.Set( 
								pBiTangentData[ biTangentIndex ], 
								pBiTangentData[ biTangentIndex + 1 ], 
								pBiTangentData[ biTangentIndex + 2 ] );
						}

						uniqueIndices.push_back( index );
					}
				}

				mshPrimitive& prim = material.Prim.Append();

				prim.Type = GetPrimitiveType( polygons );
				prim.Priority = material.Priority;
				prim.Idx.Reallocate( int(materialIndexCount) );

				for ( size_t indexIndex = firstIndexIndex; indexIndex <= lastIndexIndex; ++indexIndex )
				{
					prim.Idx.Append() = pIndices[ indexIndex ] - min;
				}

				firstIndexIndex = lastIndexIndex + 1;

			} while ( firstIndexIndex < indexCount );
		}

		for ( size_t rageBoneIndex = 0; rageBoneIndex < orderedJointNodes.size(); ++rageBoneIndex )
		{
			rage::Vector3 offset = rage::VEC3_ZERO;

			// Determine if this Rage bone index is used by the skin
			for ( std::map<size_t, size_t>::iterator it = colladaJointIndexToRageBoneIndexMap.begin();
					it != colladaJointIndexToRageBoneIndexMap.end(); ++it )
			{
				// If so, calculate this bone's offset
				if ( it->second == rageBoneIndex )
				{
					const FCDSceneNode* pJointNode = orderedJointNodes[rageBoneIndex];

					rage::Matrix34 rageJointWorldMatrix;
					FMMatrix44 jointWorldMatrix = CalculateWorldMatrixNoRotation( pJointNode );
					ToMatrix34(rageJointWorldMatrix, jointWorldMatrix);

					offset = rageJointWorldMatrix.d;

					break;
				}
			}

			mesh.AddOffset( offset );
		}

		Mgc::Sphere sphere = Mgc::MinSphere( int( points.size() ), &points[ 0 ] );
		mesh.SetBoundSphere(
			Vector4( float( sphere.Center().x ), float( sphere.Center().y ), float( sphere.Center().z ), float( sphere.Radius() ) ) );

		Matrix34 bbox;
		Mgc::Box3 box = Mgc::MinBox( int( points.size() ), &points[ 0 ] );
		bbox.a.x = (float)box.Axis( 0 ).x;
		bbox.a.y = (float)box.Axis( 0 ).y;
		bbox.a.z = (float)box.Axis( 0 ).z;
		bbox.b.x = (float)box.Axis( 1 ).x;
		bbox.b.y = (float)box.Axis( 1 ).y;
		bbox.b.z = (float)box.Axis( 1 ).z;
		bbox.c.x = (float)box.Axis( 2 ).x;
		bbox.c.y = (float)box.Axis( 2 ).y;
		bbox.c.z = (float)box.Axis( 2 ).z;
		bbox.d.x = (float)box.Center().x;
		bbox.d.y = (float)box.Center().y;
		bbox.d.z = (float)box.Center().z;
		mesh.SetBoundBox( bbox, Vector3( float( box.Extent( 0 ) ), float( box.Extent( 1 ) ), float( box.Extent( 2 ) ) ) );

		mesh.Weld();
		mesh.ComputeTanBi(0);
		mesh.RemoveDegenerates();
	}

	void ConvertMesh( ConvertMeshResult &outResults, const FCDSceneNode* pNode, const FCDMaterialList& materials, rageColladaSpace space )
	{
		FMMatrix44 toWorldSpaceMtx = pNode->CalculateWorldTransform();
	
		size_t instanceCount = pNode->GetInstanceCount();
		for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
		{
			const FCDEntityInstance* pInstance = pNode->GetInstance( instanceIndex );
			if ( FCDEntityInstance::GEOMETRY == pInstance->GetType() )
			{
				const FCDGeometry* pGeometry = 
					static_cast< const FCDGeometry* >( pInstance->GetEntity() );
				
				mshMesh* pMesh = new mshMesh();

				if( space == kRageColladaSpaceWorld)
				{
					ConvertMesh( *pMesh, pGeometry, materials, toWorldSpaceMtx );
				}
				else if ( space == kRageColladaSpaceLocal )
				{
					FMMatrix44 identity( IDENTITY );
					ConvertMesh( *pMesh, pGeometry, materials, identity );
				}

				outResults.m_mshMeshList.push_back( pMesh );

				if(instanceCount == 1) 
					outResults.m_mshMeshNameList.push_back( string(pNode->GetName().c_str()) );
				else
					outResults.m_mshMeshNameList.push_back( string(pInstance->GetEntity()->GetName().c_str()) );
			}
			else
			if ( FCDEntityInstance::CONTROLLER == pInstance->GetType() )
			{
				const FCDController* pController = 
					static_cast< const FCDController* >( pInstance->GetEntity() );
				if ( const FCDSkinController* pSkinController = pController->GetSkinController() )
				{
					mshMesh* pMesh = new mshMesh();

					FMMatrix44 parentNodeWorldTransformMtx( IDENTITY );
					const FCDSceneNode *pParentNode = pNode->GetParent();
					while(pParentNode && !pParentNode->IsJoint())
					{
						pParentNode = pParentNode->GetParent();
					}
					
					if(pParentNode)
					{
						parentNodeWorldTransformMtx = pParentNode->CalculateWorldTransform();
					}

					ConvertMesh( *pMesh, pSkinController, materials, parentNodeWorldTransformMtx );

					outResults.m_mshMeshList.push_back( pMesh );

					if(instanceCount == 1) 
						outResults.m_mshMeshNameList.push_back( string(pNode->GetName().c_str()) );
					else	
						outResults.m_mshMeshNameList.push_back( string(pInstance->GetEntity()->GetName().c_str()) );
				}

			}
		}
	}
/*
	static void ConvertBoundBvhGridThunk( phBoundOTGrid& grid, float cellSize, int minX, int maxX, int minZ, int maxZ )
	{
		grid.InitGrid< phBoundBVH >( cellSize, minX, maxX, minZ, maxZ );
	}

	static void ConvertBoundOctreeGridThunk( phBoundOTGrid& grid, float cellSize, int minX, int maxX, int minZ, int maxZ )
	{
		grid.InitGrid< phBoundOctree >( cellSize, minX, maxX, minZ, maxZ );
	}

	typedef void ( *BoundGridInitFn )( phBoundOTGrid&, float, int, int, int, int );

	static bool ConvertBoundGrid( phBoundOTGrid& grid, const FCDSceneNode* pNode, BoundGridInitFn pBoundGridInitFn )
	{
		phOctreeCell::SetDefaultMaxPerCell( 22 );

		phBoundGeometry geometry;
		bool result = ConvertBoundGeometry( geometry, pNode );
		if ( true != result )
		{
			return false;
		}

		float octreeGridCellSize;
		rageColladaProperties::GetValue( "Bounds", "OctreeGridCellSize", octreeGridCellSize );

		int minX = int( floorf( geometry.GetBoundingBoxMin().x / octreeGridCellSize ) );
		int maxX = int( floorf( geometry.GetBoundingBoxMax().x / octreeGridCellSize ) );
		int minZ = int( floorf( geometry.GetBoundingBoxMin().z / octreeGridCellSize ) );
		int maxZ = int( floorf( geometry.GetBoundingBoxMax().z / octreeGridCellSize ) );

		geometry.RemoveDegeneratePolys();
		geometry.WeldVertices( 1e-3f );

		pBoundGridInitFn( grid, octreeGridCellSize, minX, maxX, minZ, maxZ );

		grid.AddGeometry( &geometry );
		if ( !grid.InitOctrees() )
		{
			return false;
		}

		int minotx = grid.GetMinFirstAxis();
		int maxotx = grid.GetMaxFirstAxis();
		int minotz = grid.GetMinSecondAxis();
		int maxotz = grid.GetMaxSecondAxis();

		for ( int ot1 = minotx; ot1 <= maxotx; ++ot1 )
		{
			for ( int ot2 = minotz; ot2 <= maxotz; ++ot2 )
			{
				phBoundGeometry* pOctree = 
					static_cast< phBoundGeometry* >( grid.GetOctree( ot1, ot2 ) );
				pOctree->SetVertEdgeMaterial( pOctree->GetNumMaterials() - 1 );
			}
		}

		return true;
	}
*/
	bool ConvertBoundBox( rage::phBoundBox& boundBox, const FCDSceneNode* sceneNode )
	{
		const FCDETechnique* rageTechnique = sceneNode->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
		if ( NULL == rageTechnique )
		{
			return false;
		}

		const FCDENode* node = rageTechnique->FindParameter("rageBoundBox");
		if ( NULL != node )
		{
			const FCDEAttribute* xattr = node->FindAttribute( "x" );
			const FCDEAttribute* yattr = node->FindAttribute( "y" );
			const FCDEAttribute* zattr = node->FindAttribute( "z" );

			rage::Vector3 extents( 
				float( atof( xattr->value.c_str() ) ), 
				float( atof( yattr->value.c_str() ) ),
				float( atof( zattr->value.c_str() ) ) );

			boundBox.SetBoxSize( extents );

			// NOTE: I'm cheating here and just taking the first translation I find

			// TODO: CHECK THIS BECAUSE I'M SURE I NEEDED IT FOR SOMETHING ELSE,
			//	YET IT SCREWS UP THE FRAGMENTS... SIGH
			//SetCentroidOffset( sceneNode, boundBox );

			// TODO: NEEDS TO POTENTIALLY BE ROTATED
			// TODO: MATERIALS

			boundBox.SetMaterial( phMaterialMgr::DEFAULT_MATERIAL_ID );

			return true;
		}

		return false;
	}
	
	bool ConvertBoundBvh( phBoundBVH& bvh, const FCDSceneNode* sceneNode )
	{
		phBoundGeometry geometry;
		bool result = ConvertBoundGeometry( geometry, sceneNode );
		if ( true != result )
		{
			return false;
		}

		bvh.Copy( &geometry );
		bvh.Build();

		return true;
	}
/*
	bool ConvertBoundBvhGrid( phBoundOTGrid& bvhGrid, const FCDSceneNode* sceneNode )
	{
		return ConvertBoundGrid( bvhGrid, sceneNode, &ConvertBoundBvhGridThunk );
	}
*/
	bool ConvertBoundCapsule( phBoundCapsule& boundCapsule, const FCDSceneNode* sceneNode )
	{
		const FCDETechnique* rageTechnique = sceneNode->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
		if ( NULL == rageTechnique )
		{
			return false;
		}

		const FCDENode* node = rageTechnique->FindParameter( "rageBoundCapsule" );
		if ( NULL != node )
		{
			const FCDEAttribute* radius = node->FindAttribute( "radius" );
			const FCDEAttribute* length = node->FindAttribute( "length" );

			boundCapsule.SetCapsuleSize( 
				float( atof( radius->value.c_str() ) ), 
				float( atof( length->value.c_str() ) ) );

			// NOTE: I'm cheating here and just taking the first translation I find

			SetCentroidOffset( sceneNode, boundCapsule );	

			// TODO: NEEDS TO POTENTIALLY BE ROTATED
			// TODO: MATERIALS

			boundCapsule.SetMaterial( phMaterialMgr::DEFAULT_MATERIAL_ID );

			return true;
		}

		return false;
	}

	// NOTE: FOR THIS TO WORK EVERYTHING SHOULD BE CONDITIONED INTO ONE INSTANCE GEOMETRY
	bool ConvertBoundGeometry( phBoundGeometry& geometry, const FCDSceneNode* sceneNode )
	{
		// aquire and check there is only one instance geometry
		const FCDGeometry* pGeometry = NULL;

		size_t instanceCount = sceneNode->GetInstanceCount();
		for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
		{
			const FCDEntityInstance* pEntityInstance = sceneNode->GetInstance( instanceIndex );
			if ( FCDEntityInstance::GEOMETRY == pEntityInstance->GetType() )
			{
				Assert( !pGeometry );
				pGeometry = static_cast< const FCDGeometry* >( pEntityInstance->GetEntity() );
			}
		}

		Assert( NULL != pGeometry );

		size_t polygonCount = 0;
		size_t vertexCount = 0;

		const FCDGeometryMesh* pMesh = pGeometry->GetMesh();

		size_t polygonsCount = pMesh->GetPolygonsCount();
		for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
		{
			const FCDGeometryPolygons* pPolygons = pMesh->GetPolygons( polygonsIndex );
			Assert( FCDGeometryPolygons::POLYGONS == pPolygons->GetPrimitiveType() );

			polygonCount += pPolygons->GetFaceCount();

			const FCDGeometryPolygonsInput* pInput =
				pPolygons->FindInput( FUDaeGeometryInput::POSITION );
			const FCDGeometrySource* pSource = pInput->GetSource();

			vertexCount += pSource->GetValueCount();
		}

		geometry.Init( int( vertexCount ), 1, int( polygonCount ) );

		int vertexIndex = 0;
		for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
		{
			const FCDGeometryPolygons* pPolygons = pMesh->GetPolygons( polygonsIndex );

			const FCDGeometryPolygonsInput* pInput =
				pPolygons->FindInput( FUDaeGeometryInput::POSITION );
			const FCDGeometrySource* pSource = pInput->GetSource();

			const float* pPositions = pSource->GetData();
			size_t positionCount = pSource->GetDataCount();

			for ( size_t positionIndex = 0; positionIndex < positionCount; positionIndex += pSource->GetStride() )
			{
				rage::Vector3 position ( 
					pPositions[ positionIndex ],
					pPositions[ positionIndex + 1 ],
					pPositions[ positionIndex + 2 ] );

				geometry.SetVertex( vertexIndex, position );
				++vertexIndex;
			}
		}

		// TODO: PHYSICS MATERIALS
		geometry.SetMaterialId( 0, phMaterialMgr::DEFAULT_MATERIAL_ID );

		int polygonIndex = 0;
		for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
		{
			const FCDGeometryPolygons* pPolygons = pMesh->GetPolygons( polygonsIndex );

			const FCDGeometryPolygonsInput* pInput =
				pPolygons->FindInput( FUDaeGeometryInput::POSITION );

			phPolygon poly;

			const uint32* indices = pInput->GetIndices();

			size_t indexCount = pInput->GetIndexCount();
			for ( size_t indexIndex = 0; indexIndex < indexCount; indexIndex += 3 )
			{
				uint32 i0 = indices[ indexIndex ];
				uint32 i1 = indices[ indexIndex + 1 ];
				uint32 i2 = indices[ indexIndex + 2 ];

				poly.InitTriangle( i1, i2, i0,
					geometry.GetVertex( i0 ), geometry.GetVertex( i1 ), geometry.GetVertex( i2 ) );
				poly.SetMaterialIndex( 0 );

				rage::Vector3 tmp = poly.GetUnitNormal();
				if ( tmp.Mag2() < 0.9f )
				{
					return false;
				}

				geometry.SetPolygon( polygonIndex, poly );
				++polygonIndex;
			}
		}

		geometry.DecreaseNumPolys( polygonIndex );
		geometry.PostLoadCompute();

		rage::Vector3 cgOffset;
		geometry.CalcCGOffset( cgOffset );
		geometry.SetCGOffset( cgOffset );

		geometry.ComputeNeighbors( NULL );
		geometry.WeldVertices( 1e-3f );

		bool quadrifyGeometryBounds;
		rageColladaProperties::GetValue( "Bounds", "QuadrifyGeometryBounds", quadrifyGeometryBounds );
		if ( quadrifyGeometryBounds )
		{
			geometry.ConvertTrianglesToQuads();
		}

		geometry.RemoveDegeneratePolys();

		return true;
	}
/*
	bool ConvertBoundOctree( phBoundOctree& octree, const FCDSceneNode* sceneNode )
	{
		phOctreeCell::SetDefaultMaxPerCell( 22 );

		phBoundGeometry geometry;
		bool result = ConvertBoundGeometry( geometry, sceneNode );
		if ( true != result )
		{
			return false;
		}

		octree.AddGeometry( &geometry );
		octree.InitOctree( -1 );
		octree.SetVertEdgeMaterial( geometry.GetNumMaterials() - 1 );

		return true;
	}

	// TODO: REMOVE THIS CONSTANT AND MOVE IT INTO A SCRIPT

	bool ConvertBoundOctreeGrid( phBoundOTGrid& octreeGrid, const FCDSceneNode* sceneNode )
	{
		return ConvertBoundGrid( octreeGrid, sceneNode, &ConvertBoundOctreeGridThunk );
	}
*/
	bool ConvertBoundSphere( phBoundSphere& boundSphere, const FCDSceneNode* sceneNode )
	{
		const FCDETechnique* rageTechnique = sceneNode->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
		if ( NULL == rageTechnique )
		{
			return false;
		}

		const FCDENode* node = rageTechnique->FindParameter( "rageBoundSphere" );
		if ( NULL != node )
		{
			const FCDEAttribute* radius = node->FindAttribute( "radius" );

			boundSphere.SetSphereRadius( float( atof( radius->value.c_str() ) ) );

			// NOTE: I'm cheating here and just taking the first translation I find

			SetCentroidOffset( sceneNode, boundSphere );

			// TODO: MATERIALS

			boundSphere.SetMaterial( phMaterialMgr::DEFAULT_MATERIAL_ID );

			return true;
		}

		return false;
	}

}
