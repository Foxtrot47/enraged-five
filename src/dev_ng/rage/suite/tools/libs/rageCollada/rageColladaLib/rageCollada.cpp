// 
// rageColladaLib/rageCollada.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
//
#if 0
#include "rageCollada.h"

#include "rageShaderMaterial/rageShaderMaterial.h"
#include "rageShaderMaterial/rageShaderMaterialGroup.h"

#include "atl/string.h"
#include "file/asset.h"
#include "file/token.h"
#include "mesh/serialize.h"
#include "phcore/materialmgrimpl.h"
#include "vector/quaternion.h"

#include "mgc.h"

#include <algorithm>
#include <map>
#include <vector>

#include <cassert>


using namespace rage;

static const int INVALID_INDEX = -1;

static const size_t MAX_LOD = 4;




// TODO: this should really take the rigid body that the mesh is going to use,
//	trying to get it ourselves could lead to multiple possible solutions.





bool IsBoundBoxNode( const FCDSceneNode* sceneNode )
{
	const FCDETechnique* rageTechnique = sceneNode->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
	if ( NULL == rageTechnique )
	{
		return false;
	}

	const FCDENode* node = rageTechnique->FindParameter( "rageBoundBox" );
	if ( NULL != node )
	{
		return true;
	}

	return false;
}

bool IsBoundCapsuleNode( const FCDSceneNode* sceneNode )
{
	const FCDETechnique* rageTechnique = sceneNode->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
	if ( NULL == rageTechnique )
	{
		return false;
	}

	const FCDENode* node = rageTechnique->FindParameter( "rageBoundCapsule" );
	if ( NULL != node )
	{
		return true;
	}

	return false;
}

bool IsBoundSphereNode( const FCDSceneNode* sceneNode )
{
	const FCDETechnique* rageTechnique = sceneNode->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
	if ( NULL == rageTechnique )
	{
		return false;
	}

	const FCDENode* node = rageTechnique->FindParameter( "rageBoundSphere" );
	if ( NULL != node )
	{
		return true;
	}

	return false;
}



static void Indent( fiStream* s, int indent )
{
	atString st;
	for( int a = 0; a < indent; ++a )
		st += "\t";
	s->Write( st, st.GetLength() );	
}

// Sources are split depending on what material they are referenced by. Makes
// the collada data bigger but we aren't writing this back out to disk again
// so it's not a problem.
#if 0
void rageCollada::SplitSources( FCDGeometryMesh* mesh )
{
	if ( 1 >= mesh->GetPolygonsCount() )
	{
		return;
	}

	FCDocument* document = mesh->GetDocument();
	FCDGeometryLibrary* library = document->GetGeometryLibrary();

	FCDGeometry* geometry = mesh->GetParent();
	
	// create a new geometry for each of the materials

	typedef std::map< std::string, FCDGeometry* > FCDGeometryMap;
	FCDGeometryMap geometryMap;

	int polygonsCount = int( mesh->GetPolygonsCount() );
	for ( int polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
	{
		FCDGeometryPolygons* polygons = mesh->GetPolygons( polygonsIndex );
		
		FCDGeometry* newGeometry = library->AddEntity();	
		newGeometry->SetDaeId( geometry->GetDaeId() + TO_FSTRING( "-" ) + polygons->GetMaterialSemantic() );
		newGeometry->SetName( geometry->GetName() + TO_FSTRING ( "-" ) + polygons->GetMaterialSemantic() );

		FCDGeometryMesh* newMesh = newGeometry->CreateMesh();
		FCDGeometryPolygons* newPolygons = newMesh->AddPolygons();
		newPolygons->SetMaterialSemantic( polygons->GetMaterialSemantic() );

		int faceVertexCountCount = int( polygons->GetFaceVertexCountCount() );
		for ( int i = 0; i < faceVertexCountCount; ++i )
		{
			newPolygons->AddFaceVertexCount( polygons->GetFaceVertexCount( i ) );
		}

		geometryMap.insert( std::make_pair( polygons->GetMaterialSemantic().c_str(), newGeometry ) );
	}

	int sourceCount = int( mesh->GetSourceCount() );
	for ( int sourceIndex = 0; sourceIndex < sourceCount; ++sourceIndex )
	{
		FCDGeometrySource* source = mesh->GetSource( sourceIndex );

		int polygonsCount = int( mesh->GetPolygonsCount() );
		for ( int polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
		{
			FCDGeometryPolygons* polygons = mesh->GetPolygons( polygonsIndex );

			FCDGeometryMap::iterator geoIt = geometryMap.find( polygons->GetMaterialSemantic().c_str() );
			Assert( geoIt != geometryMap.end() );

			FCDGeometry* newGeometry = geoIt->second;
			FCDGeometryMesh* newMesh = newGeometry->GetMesh();
			FCDGeometryPolygons* newPolygons = newMesh->GetPolygons( 0 ); // the idea of this it to make sure there are only one polygons per geometry

			int inputCount = polygons->GetInputCount();
			for ( int inputIndex = 0; inputIndex < inputCount; ++inputIndex )
			{
				FCDGeometryPolygonsInput* input = polygons->GetInput( inputIndex );
				if ( source == input->GetSource() )
				{
					FCDGeometrySource* newSource = NULL;
					if ( mesh->IsVertexSource( input->GetSource() ) )
					{
						newSource = newMesh->AddVertexSource( input->GetSemantic() );
					}
					else
					{
						newSource = newMesh->AddSource( input->GetSemantic() );
					}
			
					FCDGeometryPolygonsInput* newInput = newPolygons->AddInput( newSource, input->GetOffset() );

					newSource->SetDaeId( source->GetDaeId() + TO_FSTRING( "-" ) + polygons->GetMaterialSemantic() );
					
					const FCDGeometrySource* oldSource = input->GetSource();
					
					int indexCount = int( input->GetIndexCount() );
					const uint32* indices = input->GetIndices();

					int stride = int( oldSource->GetStride() );

					FloatList data;

					typedef std::vector< uint32 > UniqueIndices;
					UniqueIndices uniqueIndices;
					uniqueIndices.reserve( indexCount );

					uint32 smallestIndex = INT_MAX;
					for ( int indexIndex = 0; indexIndex < indexCount; ++indexIndex )
					{
						uint32 index = indices[ indexIndex ];
					
						UniqueIndices::iterator found = std::find( uniqueIndices.begin(), uniqueIndices.end(), index );
						if ( found != uniqueIndices.end() )
						{
							continue;
						}

						uniqueIndices.push_back( index );

						const float* start = oldSource->GetData() + ( index * stride );

						for ( int i = 0; i < stride; ++i )
						{
							data.push_back( *( start + i ) );
						}

						smallestIndex = rage::Min( smallestIndex, index );
					}

					newSource->SetData( data, stride );

					for ( int indexIndex = 0; indexIndex < indexCount; ++indexIndex )
					{
						newInput->AddIndex( indices[ indexIndex ] - smallestIndex );
					}

					newMesh->Recalculate();
				}
			}
		}
	}

	// find all the instances that reference this geometry and replace them with the new set
	
	FCDVisualSceneNodeLibrary* visualScenes = document->GetVisualSceneLibrary();

	int visualSceneCount = int( visualScenes->GetEntityCount() );
	for ( int visualSceneIndex = 0; visualSceneIndex < visualSceneCount; ++visualSceneIndex )
	{
		FCDSceneNode* sceneNode = visualScenes->GetEntity( visualSceneIndex );
		
		// only goes one deep currently
		int childCount = int( sceneNode->GetChildrenCount() );
		for ( int childIndex = 0; childIndex < childCount; ++childIndex )
		{
			FCDSceneNode* childNode = sceneNode->GetChild( childIndex );

			int instanceCount = int( childNode->GetInstanceCount() );
			for ( int instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
			{
				FCDEntityInstance* instance = childNode->GetInstance( instanceIndex );
				if ( FCDEntityInstance::GEOMETRY == instance->GetType() )
				{
					FCDGeometryInstance* geometryInstance = reinterpret_cast< FCDGeometryInstance* >( instance );
					FCDGeometry* geo = reinterpret_cast< FCDGeometry* >( instance->GetEntity() );

					if ( geo == geometry )
					{
						int materialInstanceCount = int( geometryInstance->GetMaterialInstanceCount() );
						for ( int materialInstanceIndex = 0; materialInstanceIndex < materialInstanceCount; ++materialInstanceIndex )
						{
							FCDMaterialInstance* materialInstance = 
								geometryInstance->GetMaterialInstance( materialInstanceIndex );

							FCDGeometryMap::iterator it = geometryMap.find( materialInstance->GetSemantic().c_str() );
							Assert( it != geometryMap.end() );

							// we have a match and we need to switch in our seperate geometries
							FCDGeometryInstance* newGeometryInstance = static_cast< FCDGeometryInstance* >( childNode->AddInstance( FCDEntity::GEOMETRY ) );
							newGeometryInstance->SetEntity( it->second );

							FCDMaterialInstance* newMaterialInstance = newGeometryInstance->AddMaterialInstance();
							newMaterialInstance->SetSemantic( materialInstance->GetSemantic() );
							newMaterialInstance->SetEntity( materialInstance->GetEntity() );
						}


					}
				}
			}
		}
		
	}
	
	// do this after the new ones are hooked up
	geometry->Release();
}
#endif 


static void WriteSva( const char* name, const FCDMaterial* material )
{
	fiStream* stream = ASSET.Create( name, "sva" );
	if ( stream )
	{
		char buffer[ 512 ];
		sprintf( buffer, "DiffuseTex {\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );
		const FCDEffect* effect = material->GetEffect();
		const FCDEffectStandard* profile = static_cast< const FCDEffectStandard* >( effect->FindProfile( FUDaeProfileType::COMMON ) );
		Assert( profile );

		sprintf( buffer, "\tgrcTexture " );
		stream->Write( buffer, int( strlen( buffer ) ) );

		// this is just a hack to see if there are no textures
		if ( !profile->GetTextureCount( FUDaeTextureChannel::DIFFUSE ) )
		{
			sprintf( buffer, "gimme_checkers.dds" );
			stream->Write( buffer, int( strlen( buffer ) ) );
		}
		else
		{
			// do something with the texture
		}

		sprintf( buffer, "\r\n}" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		stream->Close();
	}
}

void WriteMaterial( const char* name, const FCDMaterialList& materials )
{
	fiStream* stream = ASSET.Create( name, "shadergroup" );
	if ( stream )
	{
		char buffer[ 512 ];
		sprintf( buffer, "ShadingGroup {\r\n\tCount %d\r\n", 1 );
		stream->Write( buffer, int( strlen( buffer ) ) );

		sprintf( buffer, "\tShaders %d {\r\n\t\tVers: 1\n", materials.size() );
		stream->Write( buffer, int( strlen( buffer ) ) );

		for ( rageCollada::FCDMaterialList::const_iterator it = materials.begin(); it != materials.end(); ++it )
		{
			sprintf( buffer, "\t\t%s ", "default.sps" );
			stream->Write( buffer, int( strlen( buffer ) ) );

			char sva[ 256 ];
			sprintf( sva, "%s_%03d.sva", (*it).second->GetName().c_str(), 0 );
			sprintf( buffer, "1 %s\r\n", sva );
			stream->Write( buffer, int( strlen( buffer ) ) );

			// write out an sva for the material

			WriteSva( sva, ( *it ).second );
		}

		sprintf( buffer, "\t}\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		sprintf( buffer, "}\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		stream->Close();
	}
}

void rageCollada::Triangulate( FCDGeometryLibrary* geometries )
{
	Assert( NULL != geometries );

	int geometryCount = int( geometries->GetEntityCount() );
	for ( int geometryIndex = 0; geometryIndex < geometryCount; ++geometryIndex )
	{
		FCDGeometry* geometry = geometries->GetEntity( geometryIndex );
		FCDGeometryMesh* mesh = geometry->GetMesh();

		rageCollada::Triangulate( mesh );
	}
}

void rageCollada::Triangulate( FCDGeometryMesh* mesh )
{
	FCDGeometryPolygonsTools::Triangulate( mesh );
}

void rageCollada::DeIndex( FCDGeometryLibrary* geometries )
{
	Assert( NULL != geometries );

	int geometryCount = int( geometries->GetEntityCount() );
	for ( int geometryIndex = 0; geometryIndex < geometryCount; ++geometryIndex )
	{
		FCDGeometry* geometry = geometries->GetEntity( geometryIndex );
		FCDGeometryMesh* mesh = geometry->GetMesh();

		rageCollada::DeIndex( mesh );
	}
}

void rageCollada::DeIndex( FCDGeometryMesh* mesh )
{
	FCDGeometryPolygonsTools::GenerateUniqueIndices( mesh );
}

#endif
