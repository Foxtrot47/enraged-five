#ifndef __RAGE_POLYGON_GROUP_ITERATOR_H__
#define __RAGE_POLYGON_GROUP_ITERATOR_H__
#pragma once

#include <vector>

namespace rage
{
	class RcVertex;

	class RcMesh
	{
	public:
		struct PolygonGroup
		{
			FCDMaterialInstance* pMaterial;
			RcVertex* pVertex; // this is a bit wack. RcVertex is sort of a polygon group..
		};
	public:
		// NOTE: Have to be carefui here not to copy skin controller contents more than needed!
		void Append( const FCDGeometryPolygons* pPolygons, const FCDSkinController* pSkinController, const FCDMaterial* pMaterial, const FMMatrix44* pTransform = NULL );
		void Append( const FCDGeometryPolygons* pPolygons, const FCDMaterial* pMaterial, const FMMatrix44* pTransform = NULL );

		void Append( const FCDSkinController* pSkinController );
	public:
		const FCDGeometrySource* GetPositionsSource() const;
		FCDGeometrySource* GetPositionsSource();
		bool HasPositions() const;

		const FCDGeometrySource* GetNormalsSource() const;
		FCDGeometrySource* GetNormalsSource();
		bool HasNormals() const;

		const FCDGeometrySource* GetTangentsSource( size_t index ) const;
		FCDGeometrySource* GetTangentsSource( size_t index );
		size_t GetTangentsSourceCount() const;
		bool HasTangents() const;

		FCDGeometrySource* FindTangentsSourceWithName( const char* name );

		const FCDGeometrySource* GetBiTangentsSource( size_t index ) const;
		FCDGeometrySource* GetBiTangentsSource( size_t index );
		size_t GetBiTangentsSourceCount() const;
		bool HasBiTangents() const;

		FCDGeometrySource* FindBiTangentsSourceWithName( const char* name );

		const FCDGeometrySource* GetTexCoordsSource( size_t index ) const;
		FCDGeometrySource* GetTexCoordsSource( size_t index );
		size_t GetTexCoordsSourceCount() const;
		bool HasTexCoords() const;

		FCDGeometrySource* FindTexCoordsSourceWithName( const char* name );

		const FCDGeometrySource* GetColorsSource( size_t index ) const;
		FCDGeometrySource* GetColorsSource( size_t index );
		size_t GetColorsSourceCount() const;
		bool HasColors() const;

		FCDGeometrySource* FindColorsSourceWithName( const char* name );

		const FCDGeometrySource* GetGeoTangentsSource() const;
		FCDGeometrySource* GetGeoTangentsSource();
		bool HasGeoTangents() const;

		const FCDGeometrySource* GetGeoBiTangentsSource() const;
		FCDGeometrySource* GetGeoBiTangentsSource();
		bool HasGeoBiTangents() const;
	public:
		RcMesh( FCDGeometryMesh* pMesh, FCDControllerInstance* pControllerInstance );
		RcMesh( FCDGeometryMesh* pMesh, FCDGeometryInstance* pInstance );

		~RcMesh();
	private:
		bool SourceExists( const FCDGeometrySourceList& sources ) const;
		bool SourceExists( const FCDGeometrySource* pSource ) const;

		void SetSources( FCDGeometryPolygons* pPolygons );

		void AppendSources( const FCDGeometryPolygons* pPolygons, const FMMatrix44* pTransform );
		template < class Tpolicy >
		void AppendSource( FUDaeGeometryInput::Semantic semantic, const FCDGeometryPolygons* pPolygons, const FMMatrix44* pTransform, FCDGeometrySource* pSource );

		FCDGeometrySource* AddSource( FUDaeGeometryInput::Semantic semantic, const char* name );
	private:
		FCDGeometryMesh* m_pMesh;
		// TODO: MIGHT NOT NEED CONTROLLER INSTANCE SINCE IT'S A GEOMETRY INSTANCE
		FCDControllerInstance* m_pControllerInstance;
		FCDGeometryInstance* m_pGeometryInstance;

		typedef std::vector< PolygonGroup* > PolygonGroupList;
		PolygonGroupList m_PolygonGroupList;

		FCDGeometryPolygons* m_pOwnerPolygons;
		
		FCDGeometrySource* m_pPositionsSource;
		FCDGeometrySource* m_pNormalsSource;
		FCDGeometrySourceList m_TangentsSources;
		FCDGeometrySourceList m_BiTangentSources;
		FCDGeometrySourceList m_TexCoordsSources;
		FCDGeometrySourceList m_ColorsSources;
		FCDGeometrySource* m_pGeoTangentsSource;
		FCDGeometrySource* m_pGeoBiTangentsSource;
	private:
		friend class RcVertex;
	};

	inline const FCDGeometrySource* RcMesh::GetPositionsSource() const
	{
		return m_pPositionsSource;
	}

	inline FCDGeometrySource* RcMesh::GetPositionsSource()
	{
		return m_pPositionsSource;
	}

	inline bool RcMesh::HasPositions() const
	{
		return NULL != m_pPositionsSource;
	}

	inline const FCDGeometrySource* RcMesh::GetNormalsSource() const
	{
		return m_pNormalsSource;
	}

	inline FCDGeometrySource* RcMesh::GetNormalsSource()
	{
		return m_pNormalsSource;
	}

	inline bool RcMesh::HasNormals() const
	{
		return NULL != m_pNormalsSource;
	}

	inline const FCDGeometrySource* RcMesh::GetTangentsSource( const size_t index ) const
	{
		Assert( 0 <= index && index < GetTangentsSourceCount() );

		return m_TangentsSources[ index ];
	}

	inline FCDGeometrySource* RcMesh::GetTangentsSource( const size_t index )
	{
		Assert( 0 <= index && index < GetTangentsSourceCount() );

		return m_TangentsSources[ index ];
	}

	inline size_t RcMesh::GetTangentsSourceCount() const
	{
		return m_TangentsSources.size();
	}

	inline bool RcMesh::HasTangents() const
	{
		return 0 != m_TangentsSources.size();
	}

	inline const FCDGeometrySource* RcMesh::GetBiTangentsSource( const size_t index ) const
	{
		Assert( 0 <= index && index < GetBiTangentsSourceCount() );

		return m_BiTangentSources[ index ];
	}

	inline FCDGeometrySource* RcMesh::GetBiTangentsSource( const size_t index )
	{
		Assert( 0 <= index && index < GetBiTangentsSourceCount() );

		return m_BiTangentSources[ index ];
	}

	inline size_t RcMesh::GetBiTangentsSourceCount() const
	{
		return m_BiTangentSources.size();
	}

	inline bool RcMesh::HasBiTangents() const
	{
		return 0 != m_BiTangentSources.size();
	}

	inline const FCDGeometrySource* RcMesh::GetTexCoordsSource( const size_t index ) const
	{
		Assert( 0 <= index && index < GetTexCoordsSourceCount() );

		return m_TexCoordsSources[ index ];
	}

	inline FCDGeometrySource* RcMesh::GetTexCoordsSource( const size_t index )
	{
		Assert( 0 <= index && index < GetTexCoordsSourceCount() );

		return m_TexCoordsSources[ index ];
	}

	inline size_t RcMesh::GetTexCoordsSourceCount() const
	{
		return m_TexCoordsSources.size();
	}

	inline bool RcMesh::HasTexCoords() const
	{
		return 0 != m_TexCoordsSources.size();
	}

	inline const FCDGeometrySource* RcMesh::GetColorsSource( const size_t index ) const
	{
		Assert( 0 != index && index < GetColorsSourceCount() );

		return m_ColorsSources[ index ];
	}

	inline FCDGeometrySource* RcMesh::GetColorsSource( const size_t index )
	{
		Assert( 0 != index && index < GetColorsSourceCount() );

		return m_ColorsSources[ index ];
	}

	inline size_t RcMesh::GetColorsSourceCount() const
	{
		return m_ColorsSources.size();
	}

	inline bool RcMesh::HasColors() const
	{
		return 0 != m_ColorsSources.size();
	}

	inline const FCDGeometrySource* RcMesh::GetGeoTangentsSource() const
	{
		return m_pGeoTangentsSource;
	}

	inline FCDGeometrySource* RcMesh::GetGeoTangentsSource() 
	{
		return m_pGeoTangentsSource;
	}

	inline bool RcMesh::HasGeoTangents() const
	{
		return NULL != m_pGeoTangentsSource;
	}

	inline const FCDGeometrySource* RcMesh::GetGeoBiTangentsSource() const
	{
		return m_pGeoBiTangentsSource;
	}

	inline FCDGeometrySource* RcMesh::GetGeoBiTangentsSource()
	{
		return m_pGeoBiTangentsSource;
	}

	inline bool RcMesh::HasGeoBiTangents() const
	{
		return NULL != m_pGeoBiTangentsSource;
	}

} // rage

#endif // __RAGE_POLYGON_GROUP_ITERATOR_H__
