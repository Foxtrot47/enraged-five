// /rcSkeletonSerializer.h

#ifndef __RCSKELETON_SERIALIZER_H__
#define __RCSKELETON_SERIALIZER_H__

#include <map>
#include <string>
#include <vector>

#include "atl/functor.h"

using namespace std;

class FCDSceneNode;

namespace rage
{

class RcSkeletonSerializer
{
public:
	typedef Functor2Ret<unsigned short, const char*, int> USER_BONE_ID_FTOR;

public:
	RcSkeletonSerializer() 
		: m_userDefinedBoneIdFtor(NULL)
	{
	};

	~RcSkeletonSerializer() {};

	bool	SerializeSkeleton( const char* szSkelName, const FCDSceneNode* pSkelRootNode );

	void	SetUserDefinedBoneIdCallback( USER_BONE_ID_FTOR userFtor ) { m_userDefinedBoneIdFtor = userFtor; }

private:
	int SerializeSkeletonRec( fiStream* pStream, const FCDSceneNode* pNode, int boneIndex, int indentLevel );
	int GetBoneCount( const FCDSceneNode* pSkeletonRoot) const;

private:
	USER_BONE_ID_FTOR						m_userDefinedBoneIdFtor;
	vector< pair<unsigned short, string> >	m_usedBoneIds;
};


}//End namespace rage

#endif // __RCSKELETON_SERIALIZER_H__

