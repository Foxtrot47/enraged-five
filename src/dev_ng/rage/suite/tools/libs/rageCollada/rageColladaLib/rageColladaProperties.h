#ifndef __RAGE_COLLADA_PROPERTIES__
#define __RAGE_COLLADA_PROPERTIES__

namespace rage {

class rageColladaProperties 
{
public:
	static bool Initialize();
	static void Shutdown();

	static void Register( const char* module, const char* key, bool value );
	static bool SetValue( const char* module, const char* key, bool value );
	static bool GetValue( const char* module, const char* key, bool& value );

	static void Register( const char* module, const char* key, int value );
	static bool SetValue( const char* module, const char* key, int value );
	static bool GetValue( const char* module, const char* key, int& value );

	static void Register( const char* module, const char* key, float value );
	static bool SetValue( const char* module, const char* key, float value );
	static bool GetValue( const char* module, const char* key, float& value );
};

} // rage

#endif // __RAGE_COLLADA_PROPERTIES__
