// /rcSkeletonSerializer.cpp
#include <algorithm>
#include <queue>

#include "cranimation/animtrack.h"
#include "file/asset.h"

#include "rageColladaUtil.h"

#include "rcSkeletonSerializer.h"

using namespace std;
using namespace rage;

//-----------------------------------------------------------------------------

namespace 
{
	inline void Indent( fiStream* s, int numTabs )
	{
		for( int a = 0; a < numTabs; a++ )
			s->Write( "\t", 1 );
	}
}

//-----------------------------------------------------------------------------

namespace rage
{


bool RcSkeletonSerializer::SerializeSkeleton( const char* name, const FCDSceneNode* pRootNode )
{
	m_usedBoneIds.clear();

	int numBones = GetBoneCount(pRootNode);
	
	ASSET.CreateLeadingPath(name);
	fiStream* pStream = ASSET.Create( name, "skel" );
	if( NULL != pStream)
	{
		char buffer[512];

		sprintf( buffer, "Version: 109\r\n" );
		pStream->Write( buffer, (int)strlen( buffer ) );

		sprintf( buffer, "AuthoredOrientation %d\r\n", true );
		pStream->Write( buffer, (int)strlen( buffer ) );
		
		sprintf( buffer, "NumBones %d\r\n", numBones );
		pStream->Write( buffer, (int)strlen( buffer ) );

		if(SerializeSkeletonRec( pStream, pRootNode, 0, 0 ) < 0)
			return false;

		pStream->Close();
	}

	return true;
}

//-----------------------------------------------------------------------------

int RcSkeletonSerializer::SerializeSkeletonRec( fiStream* pStream, const FCDSceneNode* pNode, int boneIndex, int indentLevel )
{
	char buffer[512];
	
	Indent( pStream, indentLevel );
	sprintf( buffer, "bone%d %s {\r\n", boneIndex, pNode->GetName().c_str() );
	pStream->Write( buffer, (int)strlen(buffer) );

	//Generate a hashed boneId for the bone
	unsigned short boneId = (unsigned short)boneIndex;
	if(boneIndex)
	{
		if( m_userDefinedBoneIdFtor )
		{
			boneId = m_userDefinedBoneIdFtor( pNode->GetName().c_str(), boneIndex );
		}
		else
		{
			boneId = crAnimTrack::ConvertBoneNameToId( pNode->GetName().c_str() );
		}
	}

	//Check to see if the hashed boneId conflicts with boneIds for any previously hashed bones
	for(vector< pair<unsigned short, string> >::iterator it = m_usedBoneIds.begin();
		it != m_usedBoneIds.end();
		++it)
	{
		if( it->first == boneId )
		{
			Errorf("Bone Id for bone '%s' clashes when hashed with bone id '%d' from bone '%s'",
				pNode->GetName().c_str(), it->first, it->second.c_str());
			return -1;
		}
	}
	m_usedBoneIds.push_back( pair<unsigned short, string>(boneId, string(pNode->GetName().c_str())) );

	//Write out the boneId
	if(boneId != boneIndex)
	{
		Indent( pStream, indentLevel + 1);
		sprintf( buffer, "boneid %d\r\n", boneId);
		pStream->Write( buffer, (int)strlen(buffer) );
	}

	//Gather up the necessary transform information for this bone.
	FMMatrix44 localNodeMatrix = pNode->ToMatrix();
	Matrix34 rageLocalNodeMatrix;
	ToMatrix34( rageLocalNodeMatrix, localNodeMatrix );

	Vector3 offset(rageLocalNodeMatrix.d);
	
	Vector3	eulers;
	rageLocalNodeMatrix.ToEulersXYZ( eulers );

	Vector3	orient(0.0f,0.0f,0.0f);
	Vector3 sorient(0.0f,0.0f,0.0f);

	const FCDTransformContainer& nodeTransforms = pNode->GetTransforms();
	size_t numTransforms = nodeTransforms.size();

	for(size_t i=0; i < numTransforms; i++)
	{
		const FCDTransform* pTransform = nodeTransforms[i];

		if(pTransform->GetType() == FCDTransform::ROTATION)
		{
			const FCDTRotation *pRotTransform = static_cast<const FCDTRotation*>(pTransform);

			const fstring& sid = pRotTransform->GetSubId();
			if(!strcmp(sid.c_str(),"jointOrientX"))
				orient.x = DtoR * pRotTransform->GetAngle();
			else if(!strcmp(sid.c_str(),"jointOrientY"))
				orient.y = DtoR * pRotTransform->GetAngle();
			else if(!strcmp(sid.c_str(), "jointOrientZ"))
				orient.z = DtoR * pRotTransform->GetAngle();
			else if(!strcmp(sid.c_str(),"rotateAxisX"))
				sorient.x = DtoR * pRotTransform->GetAngle();
			else if(!strcmp(sid.c_str(),"rotateAxisY"))
				sorient.y = DtoR * pRotTransform->GetAngle();
			else if(!strcmp(sid.c_str(),"rotateAxisZ"))
				sorient.z = DtoR * pRotTransform->GetAngle();
			
		}
	}

	Indent( pStream, indentLevel + 1);
	sprintf( buffer, "offset %f %f %f\r\n", offset.x, offset.y, offset.z );
	pStream->Write( buffer, (int)strlen( buffer ) );

	Indent( pStream, indentLevel + 1);
	sprintf( buffer, "euler %f %f %f\r\n", eulers.x, eulers.y, eulers.z );
	pStream->Write( buffer, (int)strlen( buffer ) );

	Indent( pStream, indentLevel + 1);
	sprintf( buffer, "orient %f %f %f\r\n", orient.x, orient.y, orient.z );
	pStream->Write( buffer, (int)strlen( buffer ) );

	Indent( pStream, indentLevel + 1);
	sprintf( buffer, "sorient %f %f %f\r\n", sorient.x, sorient.y, sorient.z );
	pStream->Write( buffer, (int)strlen( buffer ) );

	//HACK
	//TODO : Fix this so it writes the correct channel descriptors here
	if(boneIndex)
	{
		Indent( pStream, indentLevel + 1);
		sprintf( buffer, "rotX rotY rotZ\r\n" );
		pStream->Write( buffer, (int)strlen( buffer ) );
	}
	else
	{
		Indent( pStream, indentLevel + 1);
		sprintf( buffer, "transX transY transZ rotX rotY rotZ\r\n" );
		pStream->Write( buffer, (int)strlen( buffer ) );
	}

	size_t numChildren = pNode->GetChildrenCount();
	
	int ogBoneIndex = boneIndex;

	for( size_t i=0; i < numChildren; i++ )
	{
		const FCDSceneNode *pChildNode = pNode->GetChild(i);
		if(pChildNode->IsJoint() && (strcmpi(pChildNode->GetName().c_str(), "_null") != 0))
		{
			int res = SerializeSkeletonRec( pStream, pChildNode, boneIndex + 1, indentLevel + 1 );
			boneIndex += res;
		}
	}

	Indent( pStream, indentLevel );
	pStream->Write( "}\r\n", 3 );

	return boneIndex - ogBoneIndex + 1;
}

//-----------------------------------------------------------------------------

int RcSkeletonSerializer::GetBoneCount( const FCDSceneNode* pSkeletonRoot ) const
{
	Assert( pSkeletonRoot->IsJoint() );

	int nBones = 0;

	queue< const FCDSceneNode* > boneQueue;
	boneQueue.push(pSkeletonRoot);

	while(!boneQueue.empty())
	{
		const FCDSceneNode* pBone = boneQueue.front();
		boneQueue.pop();

		if ( pBone->IsJoint() && (strcmpi(pBone->GetName().c_str(), "_null") != 0) )
		{
			nBones++;

			size_t nChildren = pBone->GetChildrenCount();
			for(size_t i=0; i < nChildren; i++)
			{
				boneQueue.push(pBone->GetChild(i));
			}
		}
	}

	return nBones;
}

//-----------------------------------------------------------------------------

}//end namespace rage

