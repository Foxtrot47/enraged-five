// /rcAnimationConverter.h

#ifndef __RCANIMATION_CONVERTER_H__
#define __RCANIMATION_CONVERTER_H__

#include <vector>
#include <map>

#include "float.h"

#include "atl/functor.h"
#include "crAnimation/animation.h"
#include "vector/matrix34.h"

#include "animExportCtrl.h"

using namespace std;

class FCDSceneNode;

namespace rage
{

class RcAnimationConverter
{
public:
	typedef Functor2Ret<unsigned short, const char*, int> USER_BONE_ID_FTOR; 

public:
	RcAnimationConverter() {};
	~RcAnimationConverter() {};

	bool ConvertAnimationCompressed(crAnimation& outAnim, FCDSceneNode* pSkelRootNode,
									AnimExportCtrlSpec* pAnimExportCtrl = NULL,
									vector<FCDSceneNode*>* pMoverNodes = NULL);

	bool ConvertAnimationUncompressed(crAnimation& outAnim, FCDSceneNode* pSkelRootNode, 
									  vector<FCDSceneNode*>* pMoverNodes = NULL );

	void SetUserDefinedBoneIdCallback( USER_BONE_ID_FTOR userFtor ) { m_userDefinedBoneIdFtor = userFtor; }

private:
	void SampleNodeTransforms( FCDSceneNode* pNode, 
							  float startTime, float endTime, float framesPerSecond,
							  vector<Matrix34>& frameMatrices );

	void SampleMorphTargetWeights( FCDMorphController* pMorphCtrl,
									float startTime, float endTime, float framesPerSecond,
									atArray< atArray< float > >& targetFrameWeights );

private:
	USER_BONE_ID_FTOR						m_userDefinedBoneIdFtor;
	vector< pair<unsigned short, string> >	m_usedBoneIds;
};


}//end namespace rage


#endif //__RCANIMATION_CONVERTED_H__

