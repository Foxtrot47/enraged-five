#ifndef __RAGE_COLLADA_SERIALIZE__
#define __RAGE_COLLADA_SERIALIZE__

#include "rageColladaUtil.h"

#include "mesh/mesh.h"
#include "phbound/bound.h"

class FCDSceneNode;

namespace rage {

	bool SerializeMesh( const char* name, mshMesh& mesh );

	bool SerializeBound( const char* name, phBound& bound );

	struct RAGEEntity 
	{
		struct LOD 
		{
			FCDGeometryList meshes;
			float threshold;
		};

		typedef std::vector< RAGEEntity::LOD > RAGEEntityLodGroupList;
		RAGEEntityLodGroupList lodGroups;

		typedef std::vector< std::string > RAGEMaterialList;
		RAGEMaterialList materials;

		typedef std::vector< FCDSceneNode* > RAGEBoundList;
		RAGEBoundList bounds;

		const FCDSceneNode* fragmentRoot;

		struct RAGEFragment
		{
			FCDSceneNode* fragment;
			FCDSceneNodeList bounds;
		};

		typedef std::vector< RAGEFragment > RAGEFragmentList;
		RAGEFragmentList fragments;

		std::string skeleton;
	};

	bool SerializeEntity( const char* name, const RAGEEntity& entity );

} // rage

#endif // __RAGE_COLLADA_SERIALIZE__
