#ifndef __RAGE_RC_VERTEX_ITERATOR_H__
#define __RAGE_RC_VERTEX_ITERATOR_H__
#pragma once 

#include "FCDocument/FCDGeometryPolygons.h"

namespace rage
{
	class RcMesh;

	class RcVertex 
	{
	public:
		class Iterator 
		{
		public:
			FMVector3 GetPosition() const;
			FMVector3 GetNormal() const;

			FMVector3 GetTangent( int set = 0 ) const;
			int GetTangentCount() const;
			FMVector3 GetBiTangent( int set = 0 ) const;
			int GetBiTangentCount() const;

			FMVector2 GetTexCoord( int set = 0 ) const;
			int GetTexCoordCount() const;

			FMVector3 GetColor( int set = 0 ) const;
			int GetColorCount() const;

			FMVector3 GetGeoTangent() const;
			FMVector3 GetGeoBiTangent() const;
		private:
			Iterator( class RcVertex* that, size_t start );
		public:
			Iterator& operator= ( const Iterator& rhs );

			void operator++ ();
		private:
			RcVertex& that;
			size_t curr;

			friend class RcVertex;
		};
	public:
		Iterator Begin() { return Iterator( this, 0 ); }
		Iterator End() { return Iterator( this, m_VertexCount ); }
	public:
		bool HasPositions() const;
		bool HasNormals() const;
		bool HasTangents() const;
		bool HasBiTangents() const;
		bool HasTexCoords() const;
		bool HasColors() const;
		bool HasGeoTangents() const;
		bool HasGeoBiTangents() const;

		bool Append( const FCDGeometryPolygons* pPolygons, const FCDSkinController* pSkinController, const FMMatrix44* pTransform, RcMesh* pParentMesh );
		bool Append( const FCDGeometryPolygons* pPolygons, const FMMatrix44* pTransform, RcMesh* pParentMesh );
	public:
		RcVertex( FCDGeometryPolygons* pPolygons );
	private:
		FCDGeometryPolygons* m_pPolygons;
		size_t m_VertexCount;
		int m_InputCount;
	private:
		FCDGeometryPolygonsInput* m_pPositionsInput;
		FCDGeometryPolygonsInput* m_pNormalsInput;
		FCDGeometryPolygonsInputList m_TangentsInputs;
		FCDGeometryPolygonsInputList m_BiTangentsInputs;
		FCDGeometryPolygonsInputList m_TexCoordsInputs;
		FCDGeometryPolygonsInputList m_ColorsInputs;
		FCDGeometryPolygonsInput* m_pGeoTangentsInput;
		FCDGeometryPolygonsInput* m_pGeoBiTangentsInput;
	};

	inline bool RcVertex::HasPositions() const
	{
		return NULL != m_pPositionsInput;
	}

	inline bool RcVertex::HasNormals() const
	{
		return NULL != m_pNormalsInput;
	}

	inline bool RcVertex::HasTangents() const
	{
		return 0 != m_TangentsInputs.size();
	}

	inline bool RcVertex::HasBiTangents() const
	{
		return 0 != m_BiTangentsInputs.size();
	}

	inline bool RcVertex::HasTexCoords() const
	{
		return 0 != m_TexCoordsInputs.size();
	}

	inline bool RcVertex::HasColors() const
	{
		return 0 != m_ColorsInputs.size();
	}

	inline bool RcVertex::HasGeoTangents() const
	{
		return NULL != m_pGeoTangentsInput;
	}

	inline bool RcVertex::HasGeoBiTangents() const
	{
		return NULL != m_pGeoBiTangentsInput;
	}

} // rage

#endif // __RAGE_RC_VERTEX_ITERATOR_H__
