// /rageColladaSerialize.cpp

#include <queue>

#include "rageColladaSerialize.h"

#include "rageShaderMaterial/rageShaderMaterial.h"
#include "rageShaderMaterial/rageShaderMaterialGroup.h"

#include "cranimation/animtrack.h"
#include "file/asset.h"
#include "file/token.h"
#include "math/amath.h"
#include "mesh/serialize.h"
#include "phcore/materialmgrimpl.h"
#include "vector/quaternion.h"

static const size_t MAX_LOD = 4;

using namespace std;
using namespace rage;


inline void Indent( fiStream* s, int numTabs )
{
	for( int a = 0; a < numTabs; a++ )
		s->Write( "\t", 1 );
}

static void SerializeShaderGroupInfo( const rage::RAGEEntity& entity, fiStream* stream )
{
	rageShaderMaterialGroup rageShaders;

	size_t materialCount = entity.materials.size();
	for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
	{
		std::string material = entity.materials[ materialIndex ];

		rageShaderMaterial rageShader;
		bool loaded = rageShader.LoadFromMTL( material.c_str() );
		if ( true != loaded )
		{
			// TODO: ERROR
			return;
		}

		rageShaders.AddMaterial( material.c_str(), rageShader );
	}

	// TODO: Separate shading group file

	fiTokenizer tok ( "entity.type", stream );
	rageShaders.WriteToShaderGroupFile( tok );
}

static void SerializeLodGroupMeshSection( const rage::RAGEEntity& entity, fiStream* stream, std::string& path )
{
	char buffer[ 1024 ];

	size_t lodCount = rage::Min(  entity.lodGroups.size(), MAX_LOD );

	sprintf_s( buffer, "\t%s {\r\n", "mesh" );
	//Indent( buffer, 0 );
	stream->Write( buffer, int( strlen( buffer ) ) );

	static const char* c_LodLevelIDStrings[ MAX_LOD ] = {
		"high",
		"med",
		"low",
		"vlow"
	};

	rage::FCDGeometryList geometryList;

	for ( size_t lodIndex = 0; lodIndex < lodCount; ++lodIndex )
	{
		int totalMeshCount = 0;

		size_t meshCount = entity.lodGroups[ lodIndex ].meshes.size();
		for ( size_t meshIndex = 0; meshIndex < meshCount; ++meshIndex )
		{
			const FCDGeometry* mesh = entity.lodGroups[ lodIndex ].meshes[ meshIndex ];
			if ( NULL != mesh )
			{
				++totalMeshCount;
			}
		}

		if ( totalMeshCount )
		{
			sprintf_s( buffer, "\t\t%s %d\r\n", c_LodLevelIDStrings[ lodIndex ], totalMeshCount );
			//Indent( buffer, 0 );
			stream->Write( buffer, int( strlen( buffer ) ) );

			for ( size_t meshIndex = 0; meshIndex < meshCount; ++meshIndex )
			{
				const FCDGeometry* mesh = entity.lodGroups[ lodIndex ].meshes[ meshIndex ];
				if ( NULL != mesh )
				{
					sprintf_s( buffer, "\t\t\t%s.mesh ", mesh->GetDaeId().c_str() );
					//Indent( buffer, 0 );
					stream->Write( buffer, int( strlen( buffer ) ) );

					// TODO: allow proper indexing like rex

					// HACK: WHERE THE HELL IS THIS INDEX MEANT TO COME FROM!!!!
					sprintf_s( buffer, "%d\r\n", 1 );
					stream->Write( buffer, int( strlen( buffer ) ) );

					geometryList.push_back( mesh );
				}

				//float threshold = entity.lodGroups[ lodIndex ].threshold;

				//sprintf_s( buffer, "\t\t\t%f\r\n", threshold );
				//Indent( buffer, 0 );
				//stream->Write( buffer, int( strlen( buffer ) ) );
			}

			float threshold = entity.lodGroups[ lodIndex ].threshold;

			sprintf_s( buffer, "\t\t\t%f\r\n", threshold );
			//Indent( buffer, 0 );
			stream->Write( buffer, int( strlen( buffer ) ) );
		}
		else
		{
			float threshold = 9999.f;

			sprintf_s( buffer, "\t\t%s none %f \r\n", c_LodLevelIDStrings[ lodIndex ], threshold );
			//Indent( buffer, 0 );
			stream->Write( buffer, int( strlen( buffer ) ) );
		}
	}

	for ( size_t lodIndex = lodCount; lodIndex < MAX_LOD; ++lodIndex )
	{
		float threshold = 9999.f;

		sprintf_s( buffer, "\t\t%s none %f \r\n", c_LodLevelIDStrings[ lodIndex ], threshold );
		//Indent( buffer, 0 );
		stream->Write( buffer, int( strlen( buffer ) ) );
	}

	rage::Vector3 center;
	float radius;
	rage::ComputeBoundingSphere( geometryList, center, radius );

	sprintf_s( buffer, "\t\tcenter %f %f %f\r\n", center.x, center.y, center.z );
	//Indent
	stream->Write( buffer, int( strlen( buffer ) ) );

	sprintf_s( buffer, "\t\tradius %f\r\n", radius );
	// Indent
	stream->Write( buffer, int( strlen( buffer ) ) );

	rage::Vector3 min;
	rage::Vector3 max;
	rage::ComputeBoundingBox( geometryList, min, max );

	sprintf_s( buffer, "\t\tbox %f %f %f %f %f %f\r\n", min.x, min.y, min.z, max.x, max.y, max.z );
	//Indent
	stream->Write( buffer, int( strlen( buffer ) ) );

	sprintf_s( buffer, "\t}\r\n" );
	//Indent
	stream->Write( buffer, int( strlen( buffer ) ) );
}

static void SerializeLodGroups( const rage::RAGEEntity& entity, fiStream* stream, std::string& path )
{
	char buffer[ 1024 ];

	size_t lodCount = rage::Min( entity.lodGroups.size(), MAX_LOD );

	if ( 0 == lodCount )
	{
		sprintf_s( buffer, "lodgroup none\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		return;
	}

	size_t totalMeshCount = 0;
	for ( size_t lodIndex = 0; lodIndex < lodCount; ++lodIndex )
	{
		size_t meshCount = entity.lodGroups[ lodIndex ].meshes.size();
		for ( size_t meshIndex = 0; meshIndex < meshCount; ++meshIndex )
		{
			const FCDGeometry* mesh = entity.lodGroups[ lodIndex ].meshes[ meshIndex ];
			if ( NULL != mesh )
			{
				++totalMeshCount;
			}
		}
	}

	// SplitLodGroupsByFlags

	if ( totalMeshCount > 0 )
	{
		sprintf_s( buffer, "lodgroup { \r\n" );
		//Indent( buffer, 0 );
		stream->Write( buffer, int( strlen( buffer ) ) );

		SerializeLodGroupMeshSection( entity, stream, path );
	}

	sprintf_s( buffer, "}\r\n" );
	//Indent( buffer, 0 );
	stream->Write( buffer, int( strlen( buffer ) ) );
}

static void SerializeBoundDetails( int indent, const FCDSceneNode* bound, fiStream* stream )
{
	char buffer[ 512 ];

	buffer[ 0 ] = '\0';
	strcat( buffer, "bound " );
	//strcat( buffer, path.c_str() );
	//strcat( buffer, "/" );
	strcat( buffer, bound->GetDaeId().c_str() );
	strcat( buffer, ".bnd" );

	//stream->Write( buffer, strlen( buffer ) );

	// TODO: positions/rotation/ situations

	// HACK: just pull out the bits needed. Should really build a 
	//	matrix out of the transform stack, but we don't want the
	//	scale here since that'll wack everything.

	// craptastic... FCollada's FMMatrix44::Identity doesn't link!
	static float identity[] = { 1, 0, 0, 0, 0, 1, 0 ,0 ,0, 0, 1, 0, 0, 0, 0, 1 };
	
	//FMMatrix44 translation ( identity );

	FMMatrix44 translation = bound->CalculateWorldTransform();

	size_t transformCount = bound->GetTransformCount();
	/*for ( size_t transformIndex = 0; transformIndex < transformCount; ++transformIndex )
	{
		const FCDTransform* transform = bound->GetTransform( transformIndex );
		if ( FCDTransform::TRANSLATION == transform->GetType() )
		{
			const FCDTTranslation* trans = dynamic_cast< const FCDTTranslation* >( transform );
			FMMatrix44 mat = trans->ToMatrix();

			translation *= mat;
		}
	}
	*/
	FMVector3 trans = translation.GetTranslation();

	char val[ 64 ];
	sprintf_s( val, " < %f, %f, %f >", 
		trans.x, trans.y, trans.z );
	strcat( buffer, val );

	FMMatrix44 rotation ( identity );

	for ( size_t transformIndex = 0; transformIndex < transformCount; ++transformIndex )
	{
		const FCDTransform* transform = bound->GetTransform( transformIndex );
		if ( FCDTransform::ROTATION == transform->GetType() )
		{
			const FCDTRotation* rot = dynamic_cast< const FCDTRotation* >( transform );
			FMMatrix44 mat = rot->ToMatrix();

			rotation *= mat;

		}
	}

	rage::Matrix34 m; 
	rage::ToMatrix34( m, rotation );

	rage::Quaternion quat;
	m.ToQuaternion( quat );

	sprintf_s( val, " < %f, %f, %f, %f >", quat.x, quat.y, quat.z, quat.w );
	strcat( buffer, val );

	strcat( buffer, "\r\n" );
	Indent( stream, indent );
	stream->Write( buffer, int( strlen( buffer ) ) );
}

static void SerializeBoundList( const rage::RAGEEntity& entity, fiStream* stream, std::string& path )
{
	size_t boundCount = entity.bounds.size();
	for ( size_t boundIndex = 0; boundIndex < boundCount; ++boundIndex )
	{
		SerializeBoundDetails( 1, entity.bounds[ boundIndex ], stream );	
	}
}

static void SerializeBoundInfo( const rage::RAGEEntity& entity, fiStream* stream, std::string& path )
{
	if ( 0 != entity.bounds.size() )
	{
		char buffer[ 64 ];
		sprintf_s( buffer, "bound {\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		SerializeBoundList( entity, stream, path );

		sprintf_s( buffer, "}\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );
	}
}

static const RAGEEntity::RAGEFragment* FindFragment( const RAGEEntity::RAGEFragmentList& fragments, const FCDSceneNode* pSceneNode )
{
	size_t fragmentCount = fragments.size();
	for ( size_t fragmentIndex = 0; fragmentIndex < fragmentCount; ++fragmentIndex )
	{
		const RAGEEntity::RAGEFragment* pFrag = &fragments[ fragmentIndex ];
		if ( pSceneNode == pFrag->fragment )
		{
			return pFrag;
		}
	}

	return NULL;
}

static int SerializeFragmentGroup( int indent, const FCDSceneNode* pSceneNode, const RAGEEntity::RAGEFragmentList& fragments, fiStream* stream, int boneIndex )
{
	if ( const RAGEEntity::RAGEFragment* pFragment = FindFragment( fragments, pSceneNode ) )
	{
		const FCDSceneNode* pFragmentNode = pFragment->fragment;
		const FCDSceneNodeList bounds = pFragment->bounds;

		char buffer[ 64 ];

		Indent( stream, indent );
		sprintf_s( buffer, "group %s {\r\n", pFragmentNode->GetName().c_str() );
		stream->Write( buffer, int( strlen( buffer ) ) );

		Indent( stream, indent + 1 );
		sprintf_s( buffer, "child {\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		Indent( stream, indent + 2 );
		sprintf_s( buffer, "bound {\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		size_t boundCount = bounds.size();
		for ( size_t boundIndex = 0; boundIndex < boundCount; ++boundIndex )
		{
			const FCDSceneNode* pBoundNode = bounds[ boundIndex ];
			SerializeBoundDetails( indent + 3, pBoundNode, stream );
		}

		Indent( stream, indent + 2 );
		sprintf_s( buffer, "}\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		Indent( stream, indent + 2 );
		sprintf_s( buffer, "parentBoneIndex bone %d\r\n", boneIndex );
		stream->Write( buffer, int( strlen( buffer ) ) );

		Indent( stream, indent + 1 );
		sprintf_s( buffer, "}\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		int ogBoneIndex = boneIndex;

		size_t childCount = pSceneNode->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			int ret = SerializeFragmentGroup( indent + 1, pSceneNode->GetChild( childIndex ), fragments, stream, boneIndex + 1 );
			boneIndex += ret;
		}

		Indent( stream, indent );
		sprintf_s( buffer, "}\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );

		return boneIndex - ogBoneIndex + 1;
	}

	return 0;
}

static void SerializeFragmentInfo( const rage::RAGEEntity& entity, fiStream* stream, std::string& path )
{
	if ( 0 != entity.fragments.size() && NULL != entity.fragmentRoot )
	{
		char buffer[ 64 ];
		sprintf_s( buffer, "fragments {\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );
	
		// TODO: Check that the root is not actually in the fragment list.

		int indent = 1;

		int boneIndex = 1;

		const RAGEEntity::RAGEFragmentList& fragments = entity.fragments;
		const FCDSceneNode* pFragmentRoot = entity.fragmentRoot;
		size_t childCount = pFragmentRoot->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			int ret = SerializeFragmentGroup( indent, pFragmentRoot->GetChild( childIndex ), fragments, stream, boneIndex );
			boneIndex += ret;
		}
		
		sprintf_s( buffer, "}\r\n" );
		stream->Write( buffer, int( strlen( buffer ) ) );
	}
}

namespace rage {

	bool SerializeMesh( const char* name, mshMesh& mesh )
	{
		ASSET.CreateLeadingPath( name );

		fiStream* stream = ASSET.Create( name, "mesh" );
		if ( stream )
		{
			// write out in ascii
			fiAsciiTokenizer tok;
			tok.Init( name, stream );
			mshSerializer serializer( tok, true );
			mesh.Serialize( serializer );

			stream->Close();

			return true;
		}

		return false;
	}

	bool SerializeBound( const char* name, phBound& bound )
	{
		fiStream* stream = ASSET.Create( name, "bnd" );
		if ( NULL != stream )
		{
			fiAsciiTokenizer tok;
			tok.Init( name, stream );

			phMaterialMgrImpl< phMaterial >::Create();

			phBound::Save( tok, &bound );

			phMaterialMgr::GetInstance().Destroy();

			stream->Close();

			return true;
		}

		return false;
	}

	bool SerializeEntity( const char* name, const RAGEEntity& entity )
	{
		fiStream* stream = ASSET.Create( name, "type" );
		if ( NULL != stream )
		{
			char buffer[ 512 ];

			sprintf( buffer, "Version: 103\r\n" );
			stream->Write( buffer, int( strlen( buffer ) ) );

			// Skeleton
			if ( entity.skeleton != "" )
			{
				sprintf( buffer, "skel {\r\n" );
				stream->Write( buffer, int( strlen( buffer ) ) );

				string::size_type pos = entity.skeleton.find_last_of( "/" );
				string skeleton ( entity.skeleton, pos + 1 );

				sprintf( buffer, "\tskel %s\r\n", skeleton.c_str() );
				stream->Write( buffer, int( strlen( buffer ) ) );

				sprintf( buffer, "}\r\n" );
				stream->Write( buffer, int( strlen( buffer ) ) );
			}

			// Write shader groups

			size_t materialCount = entity.materials.size();

			if ( materialCount )
			{
				/*
				if ( writeSeperateShaderGroupFile )
				{

				}
				else
				*/
				{
					sprintf_s( buffer, "shadinggroup {\r\n" );
					stream->Write( buffer, int( strlen( buffer ) ) );

					SerializeShaderGroupInfo( entity, stream );

					sprintf_s( buffer, "}\r\n" );
					stream->Write( buffer, int( strlen( buffer ) ) );
				}
			}
			else
			{
				sprintf_s( buffer, "shadinggroup none\r\n" );
				stream->Write( buffer, int( strlen( buffer ) ) );
			}

			std::string path ( name );
			path = path.replace( path.find_last_of( "/" ), path.length(), "" );

			SerializeLodGroups( entity, stream, path );

			SerializeBoundInfo( entity, stream, path );

			SerializeFragmentInfo( entity, stream, path );

			stream->Close();

			return true;
		}

		return false;
	}

} // rage
