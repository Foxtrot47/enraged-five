// 
// rexBase/animExportCtrl.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "crAnimation/animTrack.h"

#include "animExportCtrl.h"
#include "animExportCtrl_parser.h"

using namespace std;

//-----------------------------------------------------------------------------


namespace rage 
{

bool AnimExportCtrlSpec::sm_ParsableClassesRegistered = false;

bool AnimExportCtrlSpec::LoadFromXML(const char* filePath)
{
	bool bResult = true;

	fiStream * streamCtrl = fiStream::Open(filePath);
	if (streamCtrl)
	{
		Reset();
		
		if(!PARSER.LoadObject(streamCtrl, *this))
			bResult = false;
	
		streamCtrl->Close();
	}
	else
		bResult = false;

	return bResult;
}

//-----------------------------------------------------------------------------

bool AnimExportCtrlSpec::SaveToXML(const char* filePath) const
{
	bool bResult = false;
	fiStream * streamCtrl = fiStream::Create(filePath);
	if (streamCtrl)
	{
		bResult = PARSER.SaveObject(streamCtrl, this);
		streamCtrl->Close();
	}
	return bResult;
}

//-----------------------------------------------------------------------------

const AnimExportCtrlTrackSpec* AnimExportCtrlSpec::FindTrackSpec(const char* searchName) const
{
	int trackSpecCount = m_TrackSpecs.GetCount();
	for(int i=0; i<trackSpecCount; i++)
	{
		if(StrWildCmp(m_TrackSpecs[i].GetNameExpr(), searchName))
			return &m_TrackSpecs[i];
	}
	return NULL;
}

//-----------------------------------------------------------------------------

int AnimExportCtrlSpec::StrWildCmp(const char *wild, const char *string) const
{
	const char *cp = NULL, *mp = NULL;

	while ((*string) && (*wild != '*')) 
	{
		if ((*wild != *string) && (*wild != '?')) 
		{
		return 0;
		}
		wild++;
		string++;
	}

	while (*string) 
	{
		if (*wild == '*') 
		{
		if (!*++wild) 
		{
			return 1;
		}
		mp = wild;
		cp = string+1;
		} 
		else if ((*wild == *string) || (*wild == '?')) 
		{
		wild++;
		string++;
		} 
		else 
		{
		wild = mp;
		string = cp++;
		}
	}

	while (*wild == '*') 
	{
		wild++;
	}
	return !*wild;
}

//-----------------------------------------------------------------------------

void AnimExportCtrlTrack::BuildComponentOutputName(int cIdx, atString& compOutputName) const
{
	compOutputName = (const char*)m_outputName; 
	compOutputName += (const char*)m_splitComponents[cIdx];
}
	
//-----------------------------------------------------------------------------

void AnimExportCtrlTrack::PostLoad()
{
	SplitComponentString();
}

//-----------------------------------------------------------------------------

void AnimExportCtrlTrack::SplitComponentString()
{
	m_splitComponents.clear();

	unsigned int cmpStrLen = strlen((const char*)m_components);
	if(!cmpStrLen)
		return;

	atString cmpName;
	for(unsigned int i=0; i<cmpStrLen; i++)
	{
		if(m_components[i] == ' ')
			continue;
		else if(m_components[i] == ',') 
		{
			m_splitComponents.PushAndGrow((const char*)cmpName);
			cmpName.Reset();
		}
		else
		{
			cmpName += m_components[i];
		}
	}

	m_splitComponents.PushAndGrow((const char*)cmpName);
}

//-----------------------------------------------------------------------------

int AnimExportCtrlTrack::GetTrackId() const
{
	if(m_trackId)
	{
		return AnimExportCtrlSpec::ConvertTrackIdStrToTrackId(m_trackId);
	}
	else
	{
		return -1;
	}
}

//-----------------------------------------------------------------------------

int AnimExportCtrlSpec::ConvertTrackIdStrToTrackId(const char* trackIdStr)
{
	if(!_strcmpi("TRACK_BONE_TRANSLATION", trackIdStr))
		return kTrackBoneTranslation;
	else if(!_strcmpi("TRACK_BONE_ROTATION", trackIdStr))
		return kTrackBoneRotation;
	else if(!_strcmpi("TRACK_BONE_SCALE", trackIdStr))
		return kTrackBoneScale;
	else if(!_strcmpi("TRACK_BONE_CONSTRAINT", trackIdStr))
		return kTrackBoneConstraint;
	else if(!_strcmpi("TRACK_VISIBILITY", trackIdStr))
		return kTrackVisibility;
	else if(!_strcmpi("TRACK_MOVER_TRANSLATION", trackIdStr))
		return kTrackMoverTranslation;
	else if(!_strcmpi("TRACK_MOVER_ROTATION", trackIdStr))
		return kTrackMoverRotation;
	else if(!_strcmpi("TRACK_CAMERA_TRANSLATION", trackIdStr))
		return kTrackCameraTranslation;
	else if(!_strcmpi("TRACK_CAMERA_ROTATION", trackIdStr))
		return kTrackCameraRotation;
	else if(!_strcmpi("TRACK_CAMERA_SCALE", trackIdStr))
		return kTrackCameraScale;
	else if(!_strcmpi("TRACK_CAMERA_FOCAL_LENGTH", trackIdStr))
		return kTrackCameraFocalLength;
	else if(!_strcmpi("TRACK_CAMERA_HORIZONTAL_FILM_APERTURE", trackIdStr))
		return kTrackCameraHorizontalFilmAperture;
	else if(!_strcmpi("TRACK_CAMERA_APERTURE", trackIdStr))
		return kTrackCameraAperture;
	else if(!_strcmpi("TRACK_CAMERA_FOCAL_POINT", trackIdStr))
		return kTrackCameraFocalPoint;
	else if(!_strcmpi("TRACK_CAMERA_F_STOP", trackIdStr))
		return kTrackCameraFStop;
	else if(!_strcmpi("TRACK_CAMERA_FOCUS_DISTANCE", trackIdStr))
		return kTrackCameraFocusDistance;
	else if(!_strcmpi("TRACK_SHADER_FRAME_INDEX", trackIdStr))
		return kTrackShaderFrameIndex;
	else if(!_strcmpi("TRACK_SHADER_SLIDE_U", trackIdStr))
		return kTrackShaderSlideU;
	else if(!_strcmpi("TRACK_SHADER_SLIDE_V", trackIdStr))
		return kTrackShaderSlideV;
	else if(!_strcmpi("TRACK_SHADER_ROTATE_UV", trackIdStr))
		return kTrackShaderRotateUV;
	else if(!_strcmpi("TRACK_MOVER_SCALE", trackIdStr))
		return kTrackMoverScale;
	else if(!_strcmpi("TRACK_BLEND_SHAPE", trackIdStr))
		return kTrackBlendShape;
	else if(!_strcmpi("TRACK_VISEMES", trackIdStr))
		return kTrackVisemes;

	return -1;
}

//-----------------------------------------------------------------------------

}//End namespace rage

