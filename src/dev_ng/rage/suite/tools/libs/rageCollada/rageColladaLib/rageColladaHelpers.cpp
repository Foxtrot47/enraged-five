#include "rageColladaHelpers.h"
#include "rcMeshTools.h"
#include "mesh/mesh.h"

#include "rcPolygonGroupIterator.h"
#include "rcVertexIterator.h"

#include <algorithm>
#include <stack>

using namespace std;

namespace rage {

	static const int INVALID_INDEX = -1;

	#define IS_INVALID_INDEX( _idx ) ( _idx == -1 )

	static const float IDENTITY[] = { 1, 0, 0, 0, 0, 1, 0 ,0 ,0, 0, 1, 0, 0, 0, 0, 1 };	

	void Triangulate( FCDSceneNode* pSceneNode )
	{
		Assert( NULL != pSceneNode );

		size_t instanceCount = pSceneNode->GetInstanceCount();
		for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
		{	
			FCDGeometryInstance* pGeometryInstance = 
				dynamic_cast< FCDGeometryInstance* >( pSceneNode->GetInstance( instanceIndex ) );
			if ( NULL != pGeometryInstance && NULL != pGeometryInstance->GetEntity() )
			{
				Triangulate( static_cast< FCDGeometry* >( pGeometryInstance->GetEntity() ) );
			}

			FCDControllerInstance* pControllerInstance = 
				dynamic_cast< FCDControllerInstance* >( pSceneNode->GetInstance( instanceIndex ) );
			if ( NULL != pControllerInstance )
			{
				FCDController* pController = static_cast< FCDController* >( pControllerInstance->GetEntity() );

				if ( pController->IsSkin() )
				{
					Triangulate( pController->GetBaseGeometry() );
				}
			}
		}

		size_t childCount = pSceneNode->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			Triangulate( pSceneNode->GetChild( childIndex ) );
		}
	}

	void Triangulate( FCDGeometry* PGeometry )
	{
		FCDGeometryPolygonsTools::Triangulate( PGeometry->GetMesh() );
	}

	void Deindex( FCDSceneNode* pSceneNode )
	{
		Assert( NULL != pSceneNode );

		size_t instanceCount = pSceneNode->GetInstanceCount();
		for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
		{
			FCDEntityInstance* pInstance = pSceneNode->GetInstance( instanceIndex );
			
			if ( FCDEntityInstance::CONTROLLER == pInstance->GetType() )
			{
				FCDController* pController = 
					static_cast< FCDController* >( pInstance->GetEntity() );

				if ( pController->IsSkin() )
				{
					Deindex( pController->GetSkinController() );
				}
			}
			else
			if ( FCDEntityInstance::GEOMETRY == pInstance->GetType() )
			{
				FCDGeometry* pGeometry =
					static_cast< FCDGeometry* >( pInstance->GetEntity() );

				if ( NULL != pGeometry )
				{
					Deindex( pGeometry );
				}
			}
		}
		
		size_t childCount = pSceneNode->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			Deindex( pSceneNode->GetChild( childIndex ) );
		}
	}

	void Deindex( FCDGeometry* pGeometry )
	{
		FCDGeometryMesh* pMesh = pGeometry->GetMesh();
		if ( pMesh != NULL )
		{
			FCDGeometryPolygonsTools::GenerateUniqueIndices( pMesh );
		}
	}

	void Deindex( FCDSkinController* pSkinController )
	{
		FCDController* pController = pSkinController->GetParent();
		AssertMsg( pController, "Could not get skin controller parent." );
		FCDGeometry* pGeometry = pController->GetBaseGeometry();
		Assertf( pGeometry, "Could not get base geometry for controller '%s'", pController->GetName().c_str());

		FCDGeometryIndexTranslationMap tmap;
		FCDGeometryPolygonsTools::GenerateUniqueIndices( pGeometry->GetMesh(), NULL, &tmap );

		FCDControllerTools::ApplyTranslationMap( pSkinController, tmap );
	}

	void CalcTextureTangentBasis( FCDGeometry* pGeometry, FCDGeometrySource* pTexcoordSource )
	{
		FCDGeometryPolygonsTools::GenerateTextureTangentBasis( pGeometry->GetMesh(), pTexcoordSource );

		FCDGeometryPolygonsInput* pTangentTest = pGeometry->GetMesh()->GetPolygons( 0 )->FindInput( FUDaeGeometryInput::TEXTANGENT );
	}

	typedef std::vector< FCDGeometryMesh* > FCDGeometryMeshList;

	static void FindMeshes( FCDSceneNode* pSceneNode, FCDGeometryMeshList& meshes )
	{
		size_t instanceCount = pSceneNode->GetInstanceCount();
		for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
		{
			FCDEntityInstance* pInstance = pSceneNode->GetInstance( instanceIndex );
			if ( FCDEntity::GEOMETRY == pInstance->GetType() )
			{
				FCDGeometry* pGeometry = static_cast< FCDGeometry* >( pInstance->GetEntity() );
				FCDGeometryMesh* pMesh = pGeometry->GetMesh();

				FCDGeometryMeshList::iterator found = std::find( meshes.begin(), meshes.end(), pMesh );
				if ( found == meshes.end() )
				{
					meshes.push_back( pMesh );
				}
			}
		}

		size_t childCount = pSceneNode->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			FindMeshes( pSceneNode->GetChild( childIndex ), meshes );
		}
	}

	typedef std::pair< const FCDMaterial*, FCDGeometryPolygons* > FCDMaterialGeometryPolygonsPair;
	typedef fm::vector< FCDMaterialGeometryPolygonsPair > FCDMaterialGeometryPolygonsPairList;

	//struct GeometryElement { const FCDGeometrySource* pSource; const FCDGeometryPolygonsInput* pInput }; 

	struct GeometryElement
	{
		GeometryElement( const FCDGeometrySource* pSource, const FCDGeometryPolygonsInput* pInput )
			:	pGeometryPolygonsInput ( pInput )
			,	pGeometrySource ( pSource ) { }

		GeometryElement()
			:	pGeometryPolygonsInput ( NULL )
			,	pGeometrySource ( NULL ) { }

		const FCDGeometryPolygonsInput* pGeometryPolygonsInput;
		const FCDGeometrySource* pGeometrySource;
	};
	typedef fm::vector< GeometryElement > GeometryElementList;
		
	//typedef std::pair< const FCDGeometrySource*, const FCDGeometryPolygonsInput* > GeometryElement;
	//typedef std::vector< GeometryElement > GeometryElementList;

	struct SemanticElement
	{
		SemanticElement( FUDaeGeometryInput::Semantic semantic, unsigned int index = 0 )
			:	Semantic ( semantic )
			,	Index ( index ) { }

		FUDaeGeometryInput::Semantic Semantic;
		unsigned int Index;

		GeometryElementList GeometryElements;
	};
	typedef fm::vector< SemanticElement > SemanticElementList;

	//typedef std::pair< FUDaeGeometryInput::Semantic, GeometryElementList > SemanticElement;
	//typedef std::vector< SemanticElement > SemanticElementList;

	static FCDGeometryPolygons* FindNewPolygonsForMaterial( FCDMaterialGeometryPolygonsPairList& polygons, const fstring& semantic, const FCDEntityInstance* pEntityInstance )
	{
		const FCDMaterialInstance** pMaterialInstances = NULL;
		size_t materialCount = 0;

		if ( FCDEntityInstance::GEOMETRY == pEntityInstance->GetType() )
		{
			const FCDGeometryInstance* pGeometryInstance =
				static_cast< const FCDGeometryInstance* >( pEntityInstance );

			pMaterialInstances = pGeometryInstance->GetMaterialInstances();
			materialCount = pGeometryInstance->GetMaterialInstanceCount();
		}
		else
		if ( FCDEntityInstance::CONTROLLER == pEntityInstance->GetType() )
		{
			const FCDControllerInstance* pControllerInstance =
				static_cast< const FCDControllerInstance* >( pEntityInstance );

			pMaterialInstances = pControllerInstance->GetMaterialInstances();
			materialCount = pControllerInstance->GetMaterialInstanceCount();
		}

		for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
		{
			const FCDMaterialInstance* pMaterialInstance = pMaterialInstances[ materialIndex ];
			if ( pMaterialInstance->GetSemantic() == semantic )
			{
				const FCDMaterial* pMaterial = pMaterialInstance->GetMaterial();

				// find me...
				size_t polygonsCount = polygons.size();
				for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
				{
					if ( polygons[ polygonsIndex ].first == pMaterial )
					{
						return polygons[ polygonsIndex ].second;
					}
				}
			}
		}

		return NULL;
	}

	// if semantic is the same then we add it to a bucket that we can use to merge data
	// this needs to special case textures somehow
	static void AddPolygons( FCDMaterialGeometryPolygonsPairList& polygons, SemanticElementList& elements, const FCDEntityInstance* pEntityInstance, FCDGeometry* pGeometry, const FMMatrix44* transform = NULL )
	{
		FCDGeometryMesh* pMesh = pGeometry->GetMesh();

		size_t polygonsCount = pMesh->GetPolygonsCount();
		for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
		{
			FCDGeometryPolygons* pPolygons = pMesh->GetPolygons( polygonsIndex );

			FCDGeometryPolygons* pNewPolygons =
				FindNewPolygonsForMaterial( polygons, pPolygons->GetMaterialSemantic(), pEntityInstance );

			pNewPolygons->SetMaterialSemantic( pPolygons->GetMaterialSemantic() );

			size_t inputCount = pPolygons->GetInputCount();
			for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
			{
				FCDGeometryPolygonsInput* pInput = pPolygons->GetInput( inputIndex );
				FCDGeometrySource* pSource = pInput->GetSource();
				FUDaeGeometryInput::Semantic semantic = pSource->GetType();

				// transform any positions into world space
				if ( FUDaeGeometryInput::POSITION == semantic )
				{
					if ( NULL != transform )
					{
						size_t positionsCount = pSource->GetDataCount();
						float* positions = pSource->GetData();
						for ( size_t positionsIndex = 0; positionsIndex < positionsCount; positionsIndex += 3 )
						{
							FMVector3 position ( positions );
							position = transform->TransformCoordinate( position );	

							positions[ 0 ] = position.x;
							positions[ 1 ] = position.y;
							positions[ 2 ] = position.z;

							positions += pSource->GetStride();
						}
					}
				}

				// What the hell was i thinking... i can't do it like that!
				SemanticElementList::iterator it = elements.begin();
				for ( ; it != elements.end(); ++it )
				{
					if ( semantic == it->Semantic )
					{
						break;
					}
				}

				if ( it != elements.end() )
				{
					it->GeometryElements.push_back( GeometryElement ( pSource, pInput ) );
				}
				else
				{
					GeometryElementList gel;

					gel.push_back( GeometryElement( pSource, pInput ) );

					SemanticElement se ( semantic );
					se.GeometryElements = gel;

					elements.push_back( se );
				}
			}

			size_t vertexCountCount = pPolygons->GetFaceVertexCountCount();
			for ( size_t vertexCountIndex = 0; vertexCountIndex < vertexCountCount; ++vertexCountIndex )
			{
				pNewPolygons->AddFaceVertexCount( 
					uint32( pPolygons->GetFaceVertexCount( vertexCountIndex ) ) );
			}
		}
	}

	static bool HasJointWithId( const fm::vector< FCDSkinControllerJoint* >& joints, fstring id )
	{
		fm::vector< FCDSkinControllerJoint* >::const_iterator it = joints.begin();
		while ( it != joints.end() )
		{
			if ( (*it)->GetId() == id )
			{
				return true;
			}

			++it;
		}

		return false;
	}

	static int FindNewJointIndex( size_t jointIndex , const FCDSkinController* pSkin, const FCDSkinController* pNewSkin )
	{
		const FCDSkinControllerJoint* pJoint = pSkin->GetJoint( jointIndex );
		fstring id = pJoint->GetId();

		int jointCount = int( pNewSkin->GetJointCount() );
		for ( int jointIndex = 0; jointIndex < jointCount; ++jointIndex )
		{
			const FCDSkinControllerJoint* pNewJoint = pNewSkin->GetJoint( jointIndex );
			if ( id == pNewJoint->GetId() )
			{
				return jointIndex;
			}
		}

		return INVALID_INDEX;
	}

	static void JointGeometrySources( FCDGeometryMesh* pMesh, const FCDMaterialGeometryPolygonsPairList& polygons, const SemanticElementList& elements )
	{
		size_t semanticCount = elements.size();
		for ( size_t semanticIndex = 0; semanticIndex < semanticCount; ++semanticIndex )
		{
			const SemanticElement& element = elements[ semanticIndex ];
			const GeometryElementList& sources = elements[ semanticIndex ].GeometryElements;

			FloatList floats;
			uint32 stride = 0;

			uint32 indexOffset = 0;
			UInt32List indices;

			size_t sourceCount = sources.size();
			for ( size_t sourceIndex = 0; sourceIndex < sourceCount; ++sourceIndex )
			{
				const FCDGeometrySource* pSource = sources[ sourceIndex ].pGeometrySource;
				stride = pSource->GetStride();

				uint32 indexMax = 0;

				const float* pData = pSource->GetData();
				size_t dataCount = pSource->GetDataCount();
				for ( size_t dataIndex = 0; dataIndex < dataCount; ++dataIndex )
				{
					floats.push_back( pData[ dataIndex ] );
				}

				const FCDGeometryPolygonsInput* pInput = sources[ sourceIndex ].pGeometryPolygonsInput;
				const uint32* pIndices = pInput->GetIndices();
				size_t indexCount = pInput->GetIndexCount();
				for ( size_t indexIndex = 0; indexIndex < indexCount; ++indexIndex )
				{
					uint32 index = pIndices[ indexIndex ] + indexOffset;
					indices.push_back( index );

					indexMax = rage::Max( indexMax, index );
				}

				indexOffset = ( indexMax + 1 );
			}

			FCDGeometrySource* pNewSource =  pMesh->AddSource( element.Semantic );
			pNewSource->SetDaeId( FUDaeGeometryInput::ToString( element.Semantic ) );
			pNewSource->SetData( floats, stride );			

			if ( elements[ semanticIndex ].Semantic == FUDaeGeometryInput::POSITION )
			{
				pMesh->AddVertexSource( pNewSource );
			}

			size_t polygonsCount = polygons.size();
			for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
			{
				FCDGeometryPolygons* pPolygons = polygons[ polygonsIndex ].second;

				FCDGeometryPolygonsInput* pNewInput = 
					pPolygons->AddInput( pNewSource, uint32( semanticIndex ) );
				pNewInput->AddIndices( indices );
			}
		}
	}

	void MapMaterials( RCUPolygonMaterialPairList& polymats, const FCDControllerInstance* pControllerInstance )
	{
		const FCDController* pController =
			static_cast< const FCDController* >( pControllerInstance->GetEntity() );
		// TODO: SUPPORT FOR MORPH CONTROLLERS
		const FCDGeometry* pGeometry = pController->GetBaseGeometry();
		const FCDGeometryMesh* pMesh = pGeometry->GetMesh();

		size_t polygonsCount = pMesh->GetPolygonsCount();

		size_t materialCount = pControllerInstance->GetMaterialInstanceCount();
		for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
		{
			const FCDMaterialInstance* pMaterial =
				pControllerInstance->GetMaterialInstance( materialIndex );

			for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
			{
				const FCDGeometryPolygons* pPolygons = pMesh->GetPolygons( polygonsIndex );
				if ( pPolygons->GetMaterialSemantic() == pMaterial->GetSemantic() )
				{
					polymats.push_back( make_pair( pPolygons, pMaterial ) );

					break;
				}
			}
		}
	}

	void MapMaterials( RCUPolygonMaterialPairList& polymats, const FCDGeometryInstance* pGeometryInstance )
	{
		const FCDGeometry* pGeometry = 
			static_cast< const FCDGeometry* >( pGeometryInstance->GetEntity() );
		const FCDGeometryMesh* pMesh = pGeometry->GetMesh();

		size_t polygonsCount = pMesh->GetPolygonsCount();

		size_t materialCount = pGeometryInstance->GetMaterialInstanceCount();
		for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
		{
			const FCDMaterialInstance* pMaterial =
				pGeometryInstance->GetMaterialInstance( materialIndex );

			for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
			{
				const FCDGeometryPolygons* pPolygons = pMesh->GetPolygons( polygonsIndex );
				if ( pPolygons->GetMaterialSemantic() == pMaterial->GetSemantic() )
				{
					polymats.push_back( make_pair( pPolygons, pMaterial ) );

					break;
				}
			}
		}
	}

	void RemoveNodes( const FCDSceneNodeList& nodes )
	{
		size_t nodeCount = nodes.size();
		for ( size_t nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex )
		{
			FCDSceneNode* pNode = const_cast< FCDSceneNode* >( nodes[ nodeIndex ] );
			
			size_t instanceCount = pNode->GetInstanceCount();
			for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
			{
				FCDEntityInstance* pEntityInstance = pNode->GetInstance( instanceIndex );
				if ( FCDEntityInstance::CONTROLLER == pEntityInstance->GetType() )
				{
					FCDController* pController = 
						static_cast< FCDController* >( pEntityInstance->GetEntity() );
					FCDGeometry* pGeometry = pController->GetBaseGeometry();

					delete pGeometry;

					delete pController;
				}
				else
				if ( FCDEntityInstance::GEOMETRY == pEntityInstance->GetType() )
				{
					FCDGeometry* pGeometry =
						static_cast< FCDGeometry* >( pEntityInstance->GetEntity() );
					delete pGeometry;
				}
			}

			delete pNode;
		}
	}

	struct ControllerMapPolicy
	{
		static void Do( RCUPolygonMaterialPairList& polymats, const FCDEntityInstance* pInstance )
		{
			if ( FCDEntityInstance::CONTROLLER == pInstance->GetType() )
			{
				const FCDControllerInstance* pControllerInstance = 
					static_cast< const FCDControllerInstance* >( pInstance );
				MapMaterials( polymats, pControllerInstance );
			}
		}
	};

	struct GeometryMapPolicy
	{
		static void Do( RCUPolygonMaterialPairList& polymats, const FCDEntityInstance* pInstance )
		{
			if ( FCDEntityInstance::GEOMETRY == pInstance->GetType() )
			{
				const FCDGeometryInstance* pGeometryInstance =
					static_cast< const FCDGeometryInstance* >( pInstance );
				MapMaterials( polymats, pGeometryInstance );
			}
		}
	};

	template < class TPolicy >
	static void MapNodeMaterials( RCUPolygonMaterialPairList& polymats, const FCDSceneNodeList& nodes )
	{
		size_t nodeCount = nodes.size();
		for ( size_t nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex )
		{
			const FCDSceneNode* pNode = nodes[ nodeIndex ];

			size_t instanceCount = pNode->GetInstanceCount();
			for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
			{
				TPolicy::Do( polymats, pNode->GetInstance( instanceIndex ) );
			}
		}
	}

	struct AppendSkinControllers
	{
		AppendSkinControllers( RcMesh& mesh )
			:	mesh ( mesh ) { }

		void operator() ( const FCDSceneNode* pSceneNode )
		{
			const FCDEntityInstanceContainer& instances = pSceneNode->GetInstances();
			for_each( instances.begin(), instances.end(), AppendSkinControllers( mesh ) );
		}

		void operator() ( const FCDEntityInstance* pEntityInstance )
		{
			if ( FCDEntityInstance::CONTROLLER == pEntityInstance->GetType() )
			{
				const FCDController* pController =
					static_cast< const FCDController* >( pEntityInstance->GetEntity() );
				mesh.Append( pController->GetSkinController() );
			}
		}

		void operator=( const AppendSkinControllers& ) { }

		RcMesh& mesh;
	};

	// TODO: SUPPORT MORPH TARGETS
	void MergeControllers( FCDSceneNode* pRootNode, const FCDSceneNodeList& nodes )
	{
		RCUPolygonMaterialPairList polymats;
		MapNodeMaterials< ControllerMapPolicy >( polymats, nodes );

		FCDocument* pDocument = pRootNode->GetDocument();
		FCDGeometry* pGeometry = 
			pDocument->GetGeometryLibrary()->AddEntity();
		FCDController* pController = 
			pDocument->GetControllerLibrary()->AddEntity();
		FCDSkinController* pSkinController = pController->CreateSkinController();

		pSkinController->SetTarget( pGeometry );

		FCDControllerInstance* pControllerInstance = 
			static_cast< FCDControllerInstance* >( pRootNode->AddInstance( FCDEntity::CONTROLLER ) );
		pControllerInstance->SetEntity( pController );

		FCDGeometryMesh* pMesh = pGeometry->CreateMesh();
		RcMesh mesh ( pMesh, pControllerInstance );

		for_each( nodes.begin(), nodes.end(), AppendSkinControllers( mesh ) );

		size_t polymatCount = polymats.size();
		for ( size_t polymatIndex = 0; polymatIndex < polymatCount; ++polymatIndex )
		{
			RCUPolygonMaterialPair& polymat = polymats[ polymatIndex ];
			const FCDMaterialInstance* pMaterialInstance = polymat.second;
			const FCDGeometryPolygons* pPolygons = polymat.first;

			const FCDEntityInstance* pEntityInstance = pMaterialInstance->GetParent();
			Assert( FCDEntityInstance::CONTROLLER == pEntityInstance->GetType() );

			const FCDSceneNode* pParentNode =
				pEntityInstance->GetParent();
			FMMatrix44 transform = pParentNode->CalculateWorldTransform();

			const FCDSkinController* pSourceSkinController = 
				static_cast< const FCDController* >( pEntityInstance->GetEntity() )->GetSkinController();

			transform *= pSourceSkinController->GetBindShapeTransform();
			
			mesh.Append( pPolygons, pSourceSkinController, pMaterialInstance->GetMaterial(), &transform );
		}

		RemoveNodes( nodes );
	}

	void MergeGeometries( FCDSceneNode* pRootNode, const FCDSceneNodeList& nodes )
	{
		RCUPolygonMaterialPairList polymats;
		MapNodeMaterials< GeometryMapPolicy >( polymats, nodes );

		FCDocument* pDocument = pRootNode->GetDocument();
		FCDGeometry* pNewGeometry = 
			pDocument->GetGeometryLibrary()->AddEntity();

		FCDGeometryInstance* pNewGeometryInstance = 
			static_cast< FCDGeometryInstance* >( pRootNode->AddInstance( FCDEntity::GEOMETRY ) );
		pNewGeometryInstance->SetEntity( pNewGeometry );

		FCDGeometryMesh* pNewMesh = pNewGeometry->CreateMesh();
		RcMesh mesh ( pNewMesh, pNewGeometryInstance );

		size_t polymatCount = polymats.size();
		for ( size_t polymatIndex = 0; polymatIndex < polymatCount; ++polymatIndex )
		{
			RCUPolygonMaterialPair& polymat = polymats[ polymatIndex ];
			const FCDMaterialInstance* pMaterialInstance = polymat.second;
			const FCDGeometryPolygons* pPolygons = polymat.first;

			const FCDSceneNode* pParentNode = 
				pMaterialInstance->GetParent()->GetParent();
			FMMatrix44 transform = pParentNode->CalculateWorldTransform();

			mesh.Append( pPolygons, pMaterialInstance->GetMaterial(), &transform );
		}

		RemoveNodes( nodes );
	}

	// We can merge any geometries together, as well as any controllers 
	//	with the same rooted skeleton.
#if 0
	void MeshMergeOld( FCDSceneNode* pRootNode, FCDMeshMergeList& meshes )
	{
		// list of geometries that are going to be merged
		typedef std::pair< FCDGeometryInstance*, FMMatrix44 > FCDGeometryInstancePair;
		typedef std::vector< FCDGeometryInstancePair > FCDGeometryInstanceList;
		FCDGeometryInstanceList geometries;

		// list of controllers that are going to be merged
		typedef std::vector< FCDControllerInstance* > FCDControllerInstanceList;
		FCDControllerInstanceList controllers;

		// how many controllers and geometries are we going to have to merge
		size_t mergeCount = meshes.size();
		for ( size_t mergeIndex = 0; mergeIndex < mergeCount; ++mergeIndex )
		{
			FCDSceneNode* pSceneNode = meshes[ mergeIndex ];
			FMMatrix44 transform = pSceneNode->CalculateWorldTransform();
			size_t instanceCount = pSceneNode->GetInstanceCount();
			for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
			{
				FCDEntityInstance* pEntityInstance = pSceneNode->GetInstance( instanceIndex );
				if ( FCDEntityInstance::GEOMETRY == pEntityInstance->GetType() )
				{
					FCDGeometryInstance* pGeometryInstance = 
						static_cast< FCDGeometryInstance* >( pEntityInstance );

					geometries.push_back( std::make_pair( pGeometryInstance, transform ) );
				}
				else
				if ( FCDEntityInstance::CONTROLLER == pEntityInstance->GetType() )
				{
					FCDControllerInstance* pControllerInstance = 
						static_cast< FCDControllerInstance* >( pEntityInstance );

					controllers.push_back( pControllerInstance );
				}
			}
		}

		FCDGeometryMesh* pNewMesh = NULL;

		size_t geometryCount = geometries.size();
		if ( 0 != geometryCount )
		{
			FCDGeometry* pNewGeometry = 
				pRootNode->GetDocument()->GetGeometryLibrary()->AddEntity();
			pNewMesh = pNewGeometry->CreateMesh();

			FCDGeometryInstance* pNewGeometryInstance =
				static_cast< FCDGeometryInstance* >( pRootNode->AddInstance( FCDEntity::GEOMETRY ) );
			pNewGeometryInstance->SetEntity( pNewGeometry );

			// need info about instances and the semantics they use
			typedef fm::vector< const FCDMaterial* > FCDMaterialList;
			FCDMaterialList materials;

			for ( size_t geometryIndex = 0; geometryIndex < geometryCount; ++geometryIndex )
			{
				const FCDGeometryInstance* pGeometryInstance = geometries[ geometryIndex ].first;
				size_t materialCount = pGeometryInstance->GetMaterialInstanceCount();
				for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
				{
					const FCDMaterialInstance* pMaterialInstance = pGeometryInstance->GetMaterialInstance( materialIndex );
					const FCDMaterial* pMaterial = pMaterialInstance->GetMaterial();

					if ( !materials.contains( pMaterial ) )
					{
						materials.push_back( pMaterial );
					}
				}
			}

			FCDMaterialGeometryPolygonsPairList polygons;

			size_t materialCount = materials.size();
			for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
			{
				const FCDMaterial* pMaterial = materials[ materialIndex ];

				FCDGeometryPolygons* pNewPolygons = pNewMesh->AddPolygons();
				polygons.push_back( std::make_pair( pMaterial, pNewPolygons ) );
			}
			
			SemanticElementList elements;	

			for ( size_t geometryIndex = 0; geometryIndex < geometryCount; ++geometryIndex )
			{
				FCDGeometryInstance* pGeometryInstance = geometries[ geometryIndex ].first;
				FCDGeometry* pGeometry = 
					static_cast< FCDGeometry* >( pGeometryInstance->GetEntity() );

				FMMatrix44 transform = geometries[ geometryIndex ].second;

				AddPolygons( polygons, elements, pGeometryInstance, pGeometry, &transform );
			}

			JointGeometrySources( pNewMesh, polygons, elements );
		}

		size_t controllerCount = controllers.size();
		if ( 0 != controllerCount )
		{
			FCDGeometry* pNewGeometry =
				pRootNode->GetDocument()->GetGeometryLibrary()->AddEntity();
			pNewMesh = pNewGeometry->CreateMesh();

			FCDController* pNewController = 
				pRootNode->GetDocument()->GetControllerLibrary()->AddEntity();
			FCDSkinController* pNewSkin = pNewController->CreateSkinController();
			pNewSkin->SetTarget( pNewGeometry );

			FCDControllerInstance* pNewControllerInstance = 
				static_cast< FCDControllerInstance* >( pRootNode->AddInstance( FCDEntity::CONTROLLER ) );
			pNewControllerInstance->SetEntity( pNewController );

			// chances are that all the material semantics will match, but i don't want to risk it
			typedef fm::vector< const FCDMaterial* > FCDMaterialList;
			FCDMaterialList materials;

			for ( size_t controllerIndex = 0; controllerIndex < controllerCount; ++controllerIndex )
			{
				const FCDControllerInstance* pControllerInstance = controllers[ controllerIndex ];
				size_t materialCount = pControllerInstance->GetMaterialInstanceCount();
				for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
				{
					const FCDMaterialInstance* pMaterialInstance = 
						pControllerInstance->GetMaterialInstance( materialIndex );

					const FCDMaterial* pMaterial = pMaterialInstance->GetMaterial();

					if ( !materials.contains( pMaterial ) )
					{
						materials.push_back( pMaterial );
					}
				}
			}

			FCDMaterialGeometryPolygonsPairList polygons;
			{
				size_t materialCount = materials.size();
				for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
				{
					const FCDMaterial* pMaterial = materials[ materialIndex ];

					FCDGeometryPolygons* pNewPolygons = pNewMesh->AddPolygons();
					polygons.push_back( std::make_pair( pMaterial, pNewPolygons ) );
				}
			}

			typedef fm::vector< FCDSkinControllerJoint* > FCDSkinControllerJointList;
			FCDSkinControllerJointList joints;

			SemanticElementList elements;

			for ( size_t controllerIndex = 0; controllerIndex < controllerCount; ++controllerIndex )
			{
				FCDControllerInstance* pControllerInstance = controllers[ controllerIndex ];
				FCDController* pController = 
					static_cast< FCDController* >( pControllerInstance->GetEntity() );

				FCDSkinController* pSkin = pController->GetSkinController();
				Assert( NULL != pSkin );

				FCDGeometry* pGeometry = pController->GetBaseGeometry();
				if ( NULL != pGeometry )
				{
					size_t jointCount = pSkin->GetJointCount();
					for ( size_t jointIndex = 0; jointIndex < jointCount; ++jointIndex )
					{
						FCDSkinControllerJoint* pJoint = pSkin->GetJoint( jointIndex );
						if ( true != HasJointWithId( joints, pJoint->GetId() ) )
						{
							joints.push_back( pJoint );
						}
					}

					AddPolygons( polygons, elements, pControllerInstance, pGeometry );
				}
			}

			size_t jointCount = joints.size();
			for ( size_t jointIndex = 0; jointIndex < jointCount; ++jointIndex )
			{
				const FCDSkinControllerJoint* pJoint = joints[ jointIndex ];
				pNewSkin->AddJoint( pJoint->GetId(), pJoint->GetBindPoseInverse() );
			}

			typedef fm::vector<FCDSkinControllerVertex> InfluenceList;
			InfluenceList& influences = pNewSkin->GetInfluenceList();

			for ( size_t controllerIndex = 0; controllerIndex < controllerCount; ++controllerIndex )
			{
				FCDControllerInstance* pControllerInstance = controllers[ controllerIndex ];
				FCDController* pController =
					static_cast< FCDController* >( pControllerInstance->GetEntity() );
				FCDSkinController* pSkin = pController->GetSkinController();

				FCDSkinControllerVertex* pInfluences = pSkin->GetVertexInfluences();
				size_t influenceCount = pSkin->GetInfluenceCount();
				for ( size_t influenceIndex = 0; influenceIndex < influenceCount; ++influenceIndex )
				{
					FCDSkinControllerVertex scv;

					size_t pairCount = pInfluences[ influenceIndex ].GetPairCount();
					Assert( pairCount < mshMaxMatricesPerVertex );

					const FCDSkinControllerVertex* pInfluence = &pInfluences[ influenceIndex ];

					for ( size_t pairIndex = 0; pairIndex < pairCount; ++pairIndex )
					{
						const FCDJointWeightPair* pJointWeightPair = pInfluence->GetPair( pairIndex );
						int32 jointIndex = FindNewJointIndex( pJointWeightPair->jointIndex, pSkin, pNewSkin );
						if ( IS_INVALID_INDEX( jointIndex ) )
						{
							Errorf( "failed to find joint with index %d", pJointWeightPair->jointIndex );
						}

						scv.AddPair( jointIndex, pJointWeightPair->weight );
					}

					influences.push_back( scv );
				}
			}

			// join them all

			JointGeometrySources( pNewMesh, polygons, elements );

			FUUriList& roots = controllers[ 0 ]->GetSkeletonRoots();
			size_t rootCount = roots.size();

			FUUriList& newRoots = 
				pNewControllerInstance->GetSkeletonRoots();
			newRoots.resize( rootCount );

			for ( size_t rootIndex = 0; rootIndex < rootCount; ++rootIndex )
			{
				newRoots[ rootIndex ] = roots[ rootIndex ];
			}

			size_t materialCount = controllers[ 0 ]->GetMaterialInstanceCount();
			for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
			{
				FCDMaterialInstance* pMaterialInstance = 
					controllers[ 0 ]->GetMaterialInstance( materialIndex );
				pNewControllerInstance->AddMaterialInstance( 
					pMaterialInstance->GetMaterial(), pMaterialInstance->GetSemantic() );
			}
		}

		for ( size_t mergeIndex = 0; mergeIndex < mergeCount; ++mergeIndex )
		{
			FCDSceneNode* pNode = meshes[ mergeIndex ];
			
			size_t instanceCount = pNode->GetInstanceCount();
			for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
			{
				FCDEntityInstance* pEntityInstance = pNode->GetInstance( instanceIndex );
				if ( FCDEntityInstance::CONTROLLER == pEntityInstance->GetType() )
				{
					FCDController* pController = 
						static_cast< FCDController* >( pEntityInstance->GetEntity() );
					FCDGeometry* pGeometry = pController->GetBaseGeometry();

					delete pGeometry;

					delete pController;
				}
				else
				if ( FCDEntityInstance::GEOMETRY == pEntityInstance->GetType() )
				{
					FCDGeometry* pGeometry =
						static_cast< FCDGeometry* >( pEntityInstance->GetEntity() );
					delete pGeometry;
				}
			}

			delete pNode;
		}

		meshes.clear();
	}
#endif 

	static FMMatrix44 TranslationMatrix( const FCDSceneNode* pSceneNode )
	{
		FMMatrix44 matrix ( IDENTITY );
		size_t transformCount = pSceneNode->GetTransformCount();
		for ( size_t transformIndex = 0; transformIndex < transformCount; ++transformIndex )
		{
			const FCDTransform* pTransform = pSceneNode->GetTransform( transformIndex );
			if ( FCDTransform::TRANSLATION == pTransform->GetType() )
			{
				const FCDTTranslation* pTranslation = static_cast< const FCDTTranslation* >( pTransform );
				
				FMMatrix44 translation =
					FMMatrix44::TranslationMatrix( pTranslation->GetTranslation() );

				matrix = matrix * translation;
			}
		}

		return matrix;
	}

	static FMMatrix44 CalcTranslation( const FCDSceneNode* pSceneNode )
	{
		const FCDSceneNode* pParent = pSceneNode->GetParent();
		if ( NULL != pParent )
		{
			return CalcTranslation( pParent ) * TranslationMatrix( pSceneNode );
		}
		else
		{
			return TranslationMatrix( pSceneNode );
		}
	}

	static FMMatrix44 RotationMatrix( const FCDSceneNode* pSceneNode )
	{
		FMMatrix44 matrix ( IDENTITY );
		size_t transformCount = pSceneNode->GetTransformCount();
		for ( size_t transformIndex = 0; transformIndex < transformCount; ++transformIndex )
		{
			const FCDTransform* pTransform = pSceneNode->GetTransform( transformIndex );
			if ( FCDTransform::ROTATION == pTransform->GetType() )
			{
				const FCDTRotation* pRotation = static_cast< const FCDTRotation* >( pTransform );

				FMMatrix44 rotation = 
					FMMatrix44::AxisRotationMatrix( pRotation->GetAxis(), pRotation->GetAngle() );

				matrix = matrix * rotation;
			}
		}

		return matrix;
	}

	static FMMatrix44 CalcRotation( const FCDSceneNode* pSceneNode )
	{
		const FCDSceneNode* pParent = pSceneNode->GetParent();
		if ( NULL != pParent )
		{
			return  RotationMatrix( pSceneNode ) * CalcRotation( pParent );
		}
		else
		{
			return RotationMatrix( pSceneNode );
		}
	}

	static void PrintMatrix( const FMMatrix44& matrix )
	{
		Printf( "\t%f\t%f\t%f\t%f\n\t%f\t%f\t%f\t%f\n\t%f\t%f\t%f\t%f\n\t%f\t%f\t%f\t%f\n",
			matrix[ 0 ][ 0 ], matrix[ 1 ][ 0 ], matrix[ 2 ][ 0 ], matrix[ 3 ][ 0 ], 
			matrix[ 0 ][ 1 ], matrix[ 1 ][ 1 ], matrix[ 2 ][ 1 ], matrix[ 3 ][ 1 ],
			matrix[ 0 ][ 2 ], matrix[ 1 ][ 2 ], matrix[ 2 ][ 2 ], matrix[ 3 ][ 2 ],
			matrix[ 0 ][ 3 ], matrix[ 1 ][ 3 ], matrix[ 2 ][ 3 ], matrix[ 3 ][ 3 ] );
	}

	static void AddParentJoints( FCDSceneNodeList& joints, FCDSceneNode* pRootNode )
	{
		std::stack< FCDSceneNode* > nodes;
		nodes.push( pRootNode );
		while ( 0 != nodes.size() )
		{
			FCDSceneNode* pNode = nodes.top();
			nodes.pop();

			if ( pNode->IsJoint() && !joints.contains( pNode ) )
			{
				joints.push_back( pNode );
			}

			int childCount = int( pNode->GetChildrenCount() );
			for ( int childIndex = childCount - 1; 0 <= childIndex; --childIndex )
			{
				nodes.push( pNode->GetChild( childIndex ) );
			}
		}
	}

	// set bone id's in the same order as everything else...
	static int SetBoneIds( FCDSceneNode* pNode, FCDSceneNodeList& list, int boneIndex )
	{
		if ( list.end() != std::find( list.begin(), list.end(), pNode ) )
		{
			char sid[ 256 ];
			sprintf_s( sid, "bone%d", boneIndex );

			pNode->SetSubId( sid );

			int oldBoneIndex = boneIndex;

			size_t childCount = pNode->GetChildrenCount();
			for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
			{
				int ret = SetBoneIds( pNode->GetChild( childIndex ), list, boneIndex + 1 );
				boneIndex += ret;
			}

			return  boneIndex - oldBoneIndex + 1;
		}

		return 0;
	}

	static int32 FindInfluenceBoneIndex( FCDSceneNode* pSceneNode, FCDSceneNodeList& joints )
	{
		FCDSceneNode* pCurr = pSceneNode;
		while ( !pCurr->IsJoint() )
		{
			pCurr = pCurr->GetParent();
		}

		Assert( pCurr );

		size_t jointCount = joints.size();
		for ( size_t jointIndex = 0; jointIndex < jointCount; ++jointIndex )
		{
			FCDSceneNode* pJoint = joints[ jointIndex ];
			if ( pJoint == pCurr )
			{
				return int32( jointIndex );
			}
		}

		Assert( false );
		return -1;
	}

	static FCDSceneNode* FindParentJoint( FCDSceneNode* pSceneNode )
	{
		Assert( NULL != pSceneNode );

		FCDSceneNode* pParent = pSceneNode->GetParent();
		while ( NULL != pParent )
		{
			if ( pParent->IsJoint() )
			{
				return pParent;
			}

			pParent = pParent->GetParent();
		}

		return NULL;
	}

	void RigidSkin( FCDSceneNode* pRootNode, FCDMeshRigidSkinningList& list )
	{
		size_t listCount = list.size();

		FCDSceneNodeList joints;
		AddParentJoints( joints, pRootNode );
				
		int boneIndex = 1;
		size_t childCount = pRootNode->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			int ret = SetBoneIds( pRootNode->GetChild( childIndex ), joints, boneIndex );
			boneIndex += ret;
		}
		
		for ( size_t listIndex = 0; listIndex < listCount; ++listIndex )
		{
			typedef std::vector< FCDEntityInstance* > FCDEntityInstanceList;
			FCDEntityInstanceList entities;

			FCDSceneNode* pSceneNode = list[ listIndex ];
			size_t instanceCount = pSceneNode->GetInstanceCount();
			for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
			{
				FCDEntityInstance* pInstance = pSceneNode->GetInstance( instanceIndex );
				// this shouldn't already be skinned. guess we can just throw out the old
				//	skinning and do it again if need be. TODO: that
				//Assert( pInstance->GetType() != FCDEntityInstance::CONTROLLER );

				entities.push_back( pInstance );
			}

			// get all the joints and give them a bone id.

			// get as many parent bones as we can		
	
			FCDSceneNodeList bones;
			
			FCDSceneNode* pParent = pSceneNode->GetParent();
			while ( NULL != pParent )
			{
				if ( pParent->IsJoint() )
				{
					bones.push_back( pParent );
				}
				
				pParent = pParent->GetParent();
			}

			if ( !bones.size() )
			{
				Warningf( "Unable to rigid skin node: %s", pSceneNode->GetDaeId().c_str() );
				continue;
			}

			size_t entityCount = entities.size();
			for ( size_t entityIndex = 0; entityIndex < entityCount; ++entityIndex )
			{
				FCDEntityInstance* pInstance = entities[ entityIndex ];

				if ( pInstance->GetType() == FCDEntityInstance::GEOMETRY )
				{
					FCDGeometryInstance* pGeometryInstance = 
						static_cast< FCDGeometryInstance* >( pInstance );

					FCDGeometry* pGeometry = 
						static_cast< FCDGeometry* >( pInstance->GetEntity() );
					FCDocument* pDocument = pGeometry->GetDocument();
					FCDControllerLibrary* pControllers = pDocument->GetControllerLibrary();

					FCDController* pNewController = pControllers->AddEntity();
					pNewController->SetDaeId( pGeometry->GetDaeId() + "-skin" );
					pNewController->SetName( pGeometry->GetName() );

					FCDSkinController* pSkin = pNewController->CreateSkinController();
					pSkin->SetTarget( pGeometry );

					FCDSceneNode* pParentNode = pGeometryInstance->GetParent();

					pSkin->SetBindShapeTransform( pParentNode->CalculateWorldTransform() );

					FCDSceneNode* pParent = pInstance->GetParent();
					FCDControllerInstance* pControllerInstance = 
						static_cast< FCDControllerInstance* >( pParent->AddInstance( pNewController ) );

					FUUriList& skeletonRoots = pControllerInstance->GetSkeletonRoots();
					skeletonRoots.push_back( FUUri( "", bones.back()->GetDaeId() ) );

					size_t materialInstanceCount = pGeometryInstance->GetMaterialInstanceCount();
					for ( size_t materialInstanceIndex = 0; 
						materialInstanceIndex < materialInstanceCount; ++materialInstanceIndex )
					{
						FCDMaterialInstance* pGeometryMaterialInstance = 
							pGeometryInstance->GetMaterialInstance( materialInstanceIndex );
						FCDMaterial* pMaterial = pGeometryMaterialInstance->GetMaterial();
						const fstring& semantic = pGeometryMaterialInstance->GetSemantic();

						FCDMaterialInstance* pMaterialInstance = 
							pControllerInstance->AddMaterialInstance( pMaterial, semantic );
					}

					// Add all the bones to controller. this will make the fragment crap a bit cleaner

					int jointCount = int( joints.size() );
					for ( int jointIndex = 0; jointIndex < jointCount; ++jointIndex )
					{
						FCDSceneNode* pBone = joints[ jointIndex ];

						pSkin->AddJoint( pBone->GetSubId(), pBone->CalculateWorldTransform().Inverted() );
					}

					int influenceBoneIndex = FindInfluenceBoneIndex( pParent, joints );

					typedef fm::vector<FCDSkinControllerVertex> InfluenceList;
					InfluenceList& influences = pSkin->GetInfluenceList();
					size_t influenceCount = influences.size();
					for ( size_t influenceIndex = 0; influenceIndex < influenceCount; ++influenceIndex )
					{
						FCDSkinControllerVertex& scv = influences[ influenceIndex ];

						scv.AddPair( influenceBoneIndex, 1.0f );
					}

					// make sure the node has the correct translation
					FMMatrix44 parentMatrix = pParent->ToMatrix();

					FCDSceneNode* pJoint = FindParentJoint( pParent->GetParent() );
					Assert( NULL != pJoint );

					FMMatrix44 jointInverse = pJoint->CalculateWorldTransform().Inverted();
					
					FMMatrix44 matrix = jointInverse;/* * parentMatrix;*/

					// destroy all the existing transforms and just create new ones
					size_t transformCount = pParent->GetTransformCount();
					for ( size_t transformIndex = 0; transformIndex < transformCount; ++transformIndex )
					{
						delete pParent->GetTransform( 0 );
					}

					FCDTTranslation* pTranslation = 
						static_cast< FCDTTranslation* >( pParent->AddTransform( FCDTransform::TRANSLATION ) );
					pTranslation->SetSubId( "translate" );
					pTranslation->SetTranslation( matrix.GetTranslation() );

					FCDTRotation* pRotationZ =
						static_cast< FCDTRotation* >( pParent->AddTransform( FCDTransform::ROTATION ) );
					pRotationZ->SetSubId( "rotateZ" );
					pRotationZ->SetRotation( FMVector3( 0.f, 0.f, 1.f ), FMath::RadToDeg( rage::Atan2f( -matrix[ 1 ][ 2 ], matrix[ 1 ][ 1 ] ) ) );

					FCDTRotation* pRotationY =
						static_cast< FCDTRotation* >( pParent->AddTransform( FCDTransform::ROTATION ) );
					pRotationY->SetSubId( "rotateY" );
					pRotationY->SetRotation( FMVector3( 0.f, 1.f, 0.f ), FMath::RadToDeg( rage::Atan2f( matrix[ 2 ][ 0 ], matrix[ 0 ][ 0 ] ) ) );

					FCDTRotation* pRotationX =
						static_cast< FCDTRotation* >( pParent->AddTransform( FCDTransform::ROTATION ) );
					pRotationX->SetSubId( "rotateX" );
					pRotationX->SetRotation( FMVector3( 1.f, 0.f, 0.f ), FMath::RadToDeg( rage::Asinf( matrix[ 1 ][ 0 ] ) ) );

					delete pGeometryInstance;
				}
			}
		}
	}

	void ReRootSkeleton( FCDSceneNode* pRootNode, FCDSceneNode* pSkeletonNode )
	{
		pRootNode->SetJointFlag( true );

		// find all the joints in the skeleton and find a suitable id

		FCDSceneNodeList joints;

		std::stack< FCDSceneNode* > traverse;
		traverse.push( pSkeletonNode );
		while ( 0 != traverse.size() )
		{
			FCDSceneNode* pSceneNode = traverse.top();
			traverse.pop();

			if ( pSceneNode->IsJoint() )
			{
				joints.push_back( pSceneNode );
			}

			size_t childCount = pSceneNode->GetChildrenCount();
			for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
			{
				traverse.push( pSceneNode->GetChild( childIndex ) );
			}
		}

		int index = 0;
		bool done = false;
		while ( !done )
		{
			char sid[ 32 ];
			sprintf_s( sid, "bone%d", index );
		
			bool found = false;

			for ( FCDSceneNodeList::iterator it = joints.begin(); it != joints.end(); ++it )
			{
				if ( 0 == strcmpi( (*it)->GetSubId().c_str(), sid ) )
				{
					found = true;
					break;
				}
			}

			if ( true != found )
			{
				pRootNode->SetSubId( sid );
				break;
			}

			++index;
		}

		// need to root the skeleton from this root now...

		traverse.push( pSkeletonNode );
		while ( 0 != traverse.size() )
		{
			FCDSceneNode* pSceneNode = traverse.top();
			traverse.pop();

			size_t instanceCount = pSceneNode->GetInstanceCount();
			for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
			{
				FCDEntityInstance* pEntityInstance = pSceneNode->GetInstance( instanceIndex );
				if ( FCDEntityInstance::CONTROLLER == pEntityInstance->GetType() )
				{
					FCDControllerInstance* pControllerInstance = 
						static_cast< FCDControllerInstance* >( pEntityInstance );
					FUUriList& root = pControllerInstance->GetSkeletonRoots();
					root.clear();
					root.push_back( FUUri( "", pRootNode->GetDaeId() ) );

					FCDController* pController = 
						static_cast< FCDController* >( pEntityInstance->GetEntity() );
					FCDSkinController* pSkinController = pController->GetSkinController();
					Assert( NULL != pSkinController );

					std::vector< FCDSkinControllerJoint > controllerJoints;

					size_t jointCount = pSkinController->GetJointCount();
					for ( size_t jointIndex = 0; jointIndex < jointCount; ++jointIndex )
					{
						FCDSkinControllerJoint* pJoint = pSkinController->GetJoint( jointIndex );
						controllerJoints.push_back( *pJoint );
					}

					pSkinController->GetJointsList().clear();

					pSkinController->AddJoint( 
						pRootNode->GetSubId(), pRootNode->CalculateWorldTransform().Inverted() );

					size_t controllerJointCount = controllerJoints.size();
					for ( size_t jointIndex = 0; jointIndex < controllerJointCount; ++jointIndex )
					{
						FCDSkinControllerJoint& pJoint = controllerJoints[ jointIndex ];
						pSkinController->AddJoint( pJoint.GetId() ,pJoint.GetBindPoseInverse() );
					}

					size_t influenceCount = pSkinController->GetInfluenceCount();
					for ( size_t influenceIndex = 0; influenceIndex < influenceCount; ++influenceIndex )
					{
						FCDSkinControllerVertex* pInfluence =
							pSkinController->GetVertexInfluence( influenceIndex );

						size_t pairCount = pInfluence->GetPairCount();
						for ( size_t pairIndex = 0; pairIndex < pairCount; ++pairIndex )
						{
							FCDJointWeightPair* pPair = pInfluence->GetPair( pairIndex );

							FCDSkinControllerJoint& pJoint = controllerJoints[ pPair->jointIndex ]; 
							
							size_t newJointCount = pSkinController->GetJointCount();
							size_t newJointIndex = 0;
							for ( ; newJointIndex < jointCount; ++newJointIndex )
							{
								if ( pSkinController->GetJoint( newJointIndex )->GetId() == pJoint.GetId() )
								{
									break;
								}
							}

							pPair->jointIndex = int32( newJointIndex );
						}	
					}
				}
			}


			size_t childCount = pSceneNode->GetChildrenCount();
			for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
			{
				traverse.push( pSceneNode->GetChild( childIndex ) );
			}
		}
	}

	//-----------------------------------------------------------------------------
	//PURPOSE
	//  Function that will search for all skinned elements within the specified
	//	pSceneNode, setting each element's source mesh to the zero-rotation bind pose.
	//PARAMS
	//  pSceneNode - Pointer to the scene node which will be searched for skinned elements.
	void SetZeroRotationBindPose( FCDSceneNode* pSceneNode )
	{
		AssertMsg( pSceneNode, "Do not pass NULL scene node to SetRageBindPose()." );

		// Look through this node's instances
		size_t instanceCount = pSceneNode->GetInstanceCount();
		for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
		{
			// If we come across a controller instance
			FCDEntityInstance* pInstance = pSceneNode->GetInstance( instanceIndex );
			if ( pInstance->GetType() == FCDEntityInstance::CONTROLLER )
			{
				// If the controller is a skin controller, it's time to go to work!
				FCDController* pController = static_cast<FCDController*>( pInstance->GetEntity() );
				if ( pController->IsSkin() )
				{
					SetZeroRotationBindPose( pController->GetSkinController() );
				}
			}
		}

		// Recurse through the child nodes
		size_t childCount = pSceneNode->GetChildrenCount();
		for ( size_t childIndex = 0; childIndex < childCount; ++childIndex )
		{
			SetZeroRotationBindPose( pSceneNode->GetChild(childIndex) );
		}
	}

	//-----------------------------------------------------------------------------
	//PURPOSE
	//  Function that takes a skin controller and applies its influences to its target
	//	mesh, ignoring all joint rotations in order to set the zero-rotation bind pose.
	//PARAMS
	//  pSkinController - Pointer to the skin controller to be used.
	void SetZeroRotationBindPose( FCDSkinController* pSkinController )
	{
		AssertMsg( pSkinController, "Do not pass NULL skin controller pointer to SetRageBindPose()." );

		// Get the skinning information from the skin controller
		const FMMatrix44& bindShapeTransform = pSkinController->GetBindShapeTransform();
		fm::vector<FCDSkinControllerJoint>& joints = pSkinController->GetJointsList();
		fm::vector<FCDSkinControllerVertex>& influences = pSkinController->GetInfluenceList();

		// Get the vertex position data for the mesh influenced by this controller
		FCDController* pController = pSkinController->GetParent();
		AssertMsg( pController, "Could not get parent for skin controller." );
		FCDGeometry* pGeometry = pController->GetBaseGeometry();
		Assertf( pGeometry, "Could not get base geometry for controller '%s'.", pController->GetName().c_str());
		FCDGeometryMesh* pMesh = pGeometry->GetMesh();
		Assertf( pMesh, "Could not get mesh for geometry '%s'.", pGeometry->GetName().c_str());
		FCDGeometrySource* pPositionSource = pMesh->FindSourceByType( FUDaeGeometryInput::POSITION );
		AssertMsg( pPositionSource, ("Could not find vertex position source for geometry '%s'.", pGeometry->GetName().c_str()) );
		size_t positionCount = pPositionSource->GetDataCount();
		uint32 positionStride = pPositionSource->GetStride();
		float* pPositionData = pPositionSource->GetData();

		// Map each joint index to that joint's world transform (ignoring rotations)
		std::map<size_t, const FCDSceneNode*> jointIndexToJointNodeMap;
		GetJointIndexToJointNodeMap( pSkinController, jointIndexToJointNodeMap );
		std::map<size_t, FMMatrix44> jointIndexToWorldTransformMap;
		for ( size_t jointIndex = 0; jointIndex < jointIndexToJointNodeMap.size(); ++jointIndex )
		{
			const FCDSceneNode* pJointNode = jointIndexToJointNodeMap[jointIndex];
			jointIndexToWorldTransformMap[jointIndex] = CalculateWorldMatrixNoRotation( pJointNode );
		}

		// For each influence
		size_t influenceCount = pSkinController->GetInfluenceCount();
		for ( size_t influenceIndex = 0; influenceIndex < influenceCount; ++influenceIndex )
		{
			// Get the vertex affected by this influence
			int positionIndex = int(influenceIndex) * positionStride;
			FMVector4 position( pPositionData[positionIndex], pPositionData[positionIndex+1], pPositionData[positionIndex+2], 1.f );
			FMVector4 newPosition( 0.0f, 0.0f, 0.0f, 1.f );

			// For each joint-weight pair affecting this vertex
			FCDSkinControllerVertex influence = influences[influenceIndex];
			for ( size_t pairIndex = 0; pairIndex < influence.GetPairCount(); ++pairIndex )
			{
				int jointIndex = influence.GetPair(pairIndex)->jointIndex;
				float weight = influence.GetPair(pairIndex)->weight;

				// Get the influencing joint's inverse bind pose
				FMMatrix44 bindPoseInverse = joints[jointIndex].GetBindPoseInverse();

				// Get the influencing joint's world transform (ignoring rotations)
				FMMatrix44 jointWorldTransform = jointIndexToWorldTransformMap[jointIndex];

				// Apply this joint-weight pair's influence to the vertex position
				newPosition += ( (jointWorldTransform * bindPoseInverse * bindShapeTransform * position) * weight );
			}

			// Update the vertex position
			pPositionData[positionIndex] = newPosition.x;
			pPositionData[positionIndex+1] = newPosition.y;
			pPositionData[positionIndex+2] = newPosition.z;
		}
	}

} // rage
