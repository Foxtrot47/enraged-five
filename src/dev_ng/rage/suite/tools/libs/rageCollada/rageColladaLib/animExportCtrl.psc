<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<structdef onPostLoad="PostLoad" type="::rage::AnimExportCtrlTrack">
	<string name="m_inputName" noInit="true" type="ConstString"/>
	<string name="m_outputName" noInit="true" type="ConstString"/>
	<string name="m_components" noInit="true" type="ConstString"/>
	<float init="-1.0" name="m_compressionTol" noInit="false" type="float"/>
	<string name="m_trackId" noInit="true" type="ConstString"/>
</structdef>

<structdef type="::rage::AnimExportCtrlTrackSpec">
	<string name="m_nameExpr" noInit="true" type="ConstString"/>
	<bool name="m_bIncludeChildren" noInit="true"/>
	<array name="m_Tracks" type="atArray">
		<struct type="::rage::AnimExportCtrlTrack"/>
	</array>
</structdef>

<structdef type="::rage::AnimExportCtrlSpec">
	<array name="m_TrackSpecs" type="atArray">
		<struct type="::rage::AnimExportCtrlTrackSpec"/>
	</array>
</structdef>

</ParserSchema>