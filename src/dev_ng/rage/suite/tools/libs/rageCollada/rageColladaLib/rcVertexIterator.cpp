#include "rcVertexIterator.h"

#include "math/amath.h"

#include "rcPolygonGroupIterator.h"

#include <algorithm>

using namespace std;

namespace rage
{
	RcVertex::RcVertex( FCDGeometryPolygons* pPolygons )
		:	m_pPolygons ( pPolygons ) 
		,	m_VertexCount ( m_pPolygons->GetFaceCount() )
		,	m_InputCount ( 0 )
		,	m_pPositionsInput ( NULL )
		,	m_pNormalsInput ( NULL )
		,	m_pGeoTangentsInput ( NULL )
		,	m_pGeoBiTangentsInput ( NULL )
	{ 
		/*
		m_pPositionsInput = m_pPolygons->FindInput( FUDaeGeometryInput::POSITION );
		m_pNormalsInput = m_pPolygons->FindInput( FUDaeGeometryInput::NORMAL );

		m_pPolygons->FindInputs( FUDaeGeometryInput::TEXTANGENT, m_TangentsInputs );
		m_pPolygons->FindInputs( FUDaeGeometryInput::TEXBINORMAL, m_BiTangentsInputs );
		m_pPolygons->FindInputs( FUDaeGeometryInput::TEXCOORD, m_TexCoordsInputs );
		m_pPolygons->FindInputs( FUDaeGeometryInput::COLOR, m_ColorsInputs );

		m_pGeoTangentsInput = m_pPolygons->FindInput( FUDaeGeometryInput::GEOTANGENT );
		m_pGeoTangentsInput = m_pPolygons->FindInput( FUDaeGeometryInput::GEOBINORMAL );
		*/
	}

	struct Transform3dCoordPolicy
	{
		static void Transform( float* pTransformedData, const float* pData, const FMMatrix44* pTransform )
		{
			FMVector3 coord ( pData[ 0 ], pData[ 1 ], pData[ 2 ] );
			FMVector3 result = pTransform->TransformCoordinate( coord );

			pTransformedData[ 0 ] = result.x;
			pTransformedData[ 1 ] = result.y;
			pTransformedData[ 2 ] = result.z;
		}
	};

	struct NoTrans4dPolicy
	{
		static void Transform( float* pTransformedData, const float* pData, const FMMatrix44* )
		{
			pTransformedData[ 0 ] = pData[ 0 ];
			pTransformedData[ 1 ] = pData[ 1 ];
			pTransformedData[ 2 ] = pData[ 2 ];
			pTransformedData[ 3 ] = pData[ 3 ];
		}
	};

	struct NoTrans3dPolicy
	{
		static void Transform( float* pTransformedData, const float* pData, const FMMatrix44* )
		{
			pTransformedData[ 0 ] = pData[ 0 ];
			pTransformedData[ 1 ] = pData[ 1 ];
			pTransformedData[ 2 ] = pData[ 2 ];
		}
	};

	struct NoTrans2dPolicy
	{
		static void Transform( float* pTransformedData, const float* pData, const FMMatrix44* )
		{
			pTransformedData[ 0 ] = pData[ 0 ];
			pTransformedData[ 1 ] = pData[ 1 ];
		}
	};

	template < class TTransformPolicy >
	void AppendSource( FCDGeometryPolygonsInput* pDestInput, const FCDGeometryPolygonsInput* pSourceInput, const FMMatrix44* pTransform )
	{
		// for each index in the source, try find the data in the destination,
		//	if it exists then use an index to that, if not then add it
		//	and use that index.
		// yes this is slow, but it's the only way to guarenteed that only
		//	the correct data ends up in the sources and that there is
		//	no duplication where there shouldn't be...

		FCDGeometrySource* pDestSource = pDestInput->GetSource();
		FloatList& destData = pDestSource->GetSourceData();
		uint32 destStride = pDestSource->GetStride();

		const FCDGeometrySource* pSourceSource = pSourceInput->GetSource();
		const float* pSourceData = pSourceSource->GetData();
		uint32 sourceStride = pSourceSource->GetStride();

		Assert( destStride == sourceStride ); // Learn to code, toffee

		size_t sourceIndexCount = pSourceInput->GetIndexCount();
		const uint32* pSourceIndices = pSourceInput->GetIndices();
		for ( size_t sourceIndexIndex = 0; sourceIndexIndex < sourceIndexCount; ++sourceIndexIndex )
		{
			uint32 index = pSourceIndices[ sourceIndexIndex ];
			const float* pData = pSourceData + ( index * sourceStride );

			float transformed[ 64 ];
			TTransformPolicy::Transform( transformed, pData, pTransform );

			// can we find this data in the buffer and if so where...
			
			uint32 foundIndex = 0;
			bool found = false;
			size_t destDataCount = destData.size();
			for ( size_t destDataIndex = 0; destDataIndex < destDataCount; destDataIndex += destStride )
			{
				bool match = true;
				for ( uint32 i = 0; i < destStride; ++i )
				{
					if ( transformed[ i ] != destData[ i + destDataIndex ] )
					{
						match = false;
						break;
					}
				}

				if ( true == match )
				{
					// found what we're after, use that index
					foundIndex = destDataIndex / destStride;
					found = true;
					break;
				}
			}

			if ( found )
			{
				pDestInput->AddIndex( foundIndex );
			}
			else
			{
				uint32 index = destData.size() / destStride;
				pDestInput->AddIndex( index );

				for ( uint32 i = 0; i < sourceStride; ++i )
				{
					destData.push_back( transformed[ i ] );
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//
	template < class TTransformPolicy >
	void AppendSource( 
		FCDGeometryPolygonsInput* pDestInput, 
		const FCDGeometryPolygonsInput* pSourceInput, 
		FCDSkinController* pDestSkin, 
		const FCDSkinController* pSourceSkin, 
		const FMMatrix44* pTransform )
	{
		// for each index in the source, try find the data in the destination,
		//	if it exists then use an index to that, if not then add it
		//	and use that index.
		// yes this is slow, but it's the only way to guarenteed that only
		//	the correct data ends up in the sources and that there is
		//	no duplication where there shouldn't be...

		struct FindByJointId
		{
			FindByJointId( const char* id )
				:	id ( id ) { }
			
			bool operator() ( const FCDSkinControllerJoint& joint )
			{
				return joint.GetId() == id;
			}

			fstring id;
		};

		typedef fm::vector< FCDSkinControllerJoint > FCDSkinControllerJointList;
		FCDSkinControllerJointList& destJoints = pDestSkin->GetJointsList();
		typedef fm::vector< FCDSkinControllerVertex > FCDSkinControllerVertexList;
		FCDSkinControllerVertexList& destInfluences = pDestSkin->GetInfluenceList();

		const FCDSkinControllerJoint* pSourceJoints = pSourceSkin->GetJoints();
		size_t sourceJointCount = pSourceSkin->GetJointCount();
		const FCDSkinControllerVertex* pSourceInfluences = pSourceSkin->GetVertexInfluences();
		size_t sourceInfluenceCount = pSourceSkin->GetInfluenceCount();

		FCDGeometrySource* pDestSource = pDestInput->GetSource();
		FloatList& destData = pDestSource->GetSourceData();
		uint32 destStride = pDestSource->GetStride();

		const FCDGeometrySource* pSourceSource = pSourceInput->GetSource();
		const float* pSourceData = pSourceSource->GetData();
		uint32 sourceStride = pSourceSource->GetStride();

		Assert( destStride == sourceStride ); // Learn to code, toffee

		size_t sourceIndexCount = pSourceInput->GetIndexCount();
		const uint32* pSourceIndices = pSourceInput->GetIndices();
		for ( size_t sourceIndexIndex = 0; sourceIndexIndex < sourceIndexCount; ++sourceIndexIndex )
		{
			uint32 index = pSourceIndices[ sourceIndexIndex ];
			const float* pData = pSourceData + ( index * sourceStride );

			float transformed[ 64 ];
			TTransformPolicy::Transform( transformed, pData, pTransform );

			// can we find this data in the buffer and if so where...
			
			uint32 foundIndex = 0;
			bool found = false;
			size_t destDataCount = destData.size();
			for ( size_t destDataIndex = 0; destDataIndex < destDataCount; destDataIndex += destStride )
			{
				bool match = true;
				for ( uint32 i = 0; i < destStride; ++i )
				{
					if ( transformed[ i ] != destData[ i + destDataIndex ] )
					{
						match = false;
						break;
					}
				}

				if ( true == match )
				{
					// the actaul source data matches

					if ( pSourceInput->GetSemantic() == FUDaeGeometryInput::POSITION )
					{
						// check that the skinning data matches as well...
						const FCDSkinControllerVertex& sourceInfluence = pSourceInfluences[ index ];
						FCDSkinControllerVertex& destInfluence = destInfluences[ destDataIndex / destStride ];
						if ( sourceInfluence.GetPairCount() != destInfluence.GetPairCount() )
						{
							// they don't eqaul, add a new index for the positions data
							break;
						}

						// else

						bool skinMatch = true;
						size_t pairCount = sourceInfluence.GetPairCount();
						for ( size_t s = 0; s < pairCount; ++s )
						{
							const FCDJointWeightPair* pSourcePair = sourceInfluence.GetPair( s );
							FCDJointWeightPair* pDestPair = destInfluence.GetPair( s );

							// make sure they use the same joint...
							if ( pSourceJoints[ pSourcePair->jointIndex ].GetId() != 
								 destJoints[ pDestPair->jointIndex ].GetId() )
							{
								skinMatch = false;
								break;
							}
							else
							if ( pSourcePair->weight != pDestPair->weight )
							{
								skinMatch = false;
								break;
							}
						}

						if ( true == skinMatch )
						{
							foundIndex = destDataIndex / destStride;
							found = true;
							break;
						}
					}
					else
					{
						foundIndex = destDataIndex / destStride;
						found = true;
						break;
					}
				}
			}

			if ( found )
			{
				pDestInput->AddIndex( foundIndex );
			}
			else
			{
				uint32 destIndex = destData.size() / destStride;
				pDestInput->AddIndex( destIndex );

				for ( uint32 i = 0; i < sourceStride; ++i )
				{
					destData.push_back( transformed[ i ] );
				}
				

				destInfluences.push_back( pSourceInfluences[ index ] );
			}
		}
	}

	static FCDGeometryPolygonsInput* FindBySourceName( FCDGeometryPolygonsInputList& inputs, const char* name )
	{
		struct FindBySourceName
		{
			FindBySourceName( const char* name )
				:	name ( name ) { }

			bool operator() ( const FCDGeometryPolygonsInput* pInput )
			{
				return pInput->GetSource()->GetName() == name;
			}

			fstring name;
		};

		FCDGeometryPolygonsInputList::iterator it = 
			find_if( inputs.begin(), inputs.end(), FindBySourceName( name ) );
		if ( inputs.end() != it )
		{
			return *it;
		}

		return NULL;
	}

	bool RcVertex::Append( const FCDGeometryPolygons* pPolygons, const FCDSkinController* pSkinController, const FMMatrix44* pTransform, RcMesh* pParentMesh )
	{
		size_t inputCount = pPolygons->GetInputCount();
		for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
		{
			const FCDGeometryPolygonsInput* pSourceInput = pPolygons->GetInput( inputIndex );
			const FCDGeometrySource* pSourceSource = pSourceInput->GetSource();
			switch ( pSourceInput->GetSemantic() )
			{
			case FUDaeGeometryInput::POSITION:
				{
					if ( true != pParentMesh->HasPositions() )
					{
						FCDGeometrySource* pSource = pParentMesh->AddSource( FUDaeGeometryInput::POSITION, "position" );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					// ADD SKIN CONTROLLER?

					if ( true != HasPositions() )
					{
						m_pPositionsInput = m_pPolygons->AddInput( 
							pParentMesh->GetPositionsSource(), m_InputCount );
						++m_InputCount;
					}

					FCDControllerInstance* pControllerInstance = pParentMesh->m_pControllerInstance;
					Assert( NULL != pControllerInstance );
					FCDController* pController = 
						static_cast< FCDController* >( pControllerInstance->GetEntity() );

					AppendSource< Transform3dCoordPolicy >( 
						m_pPositionsInput, pSourceInput, pController->GetSkinController(), pSkinController, pTransform );
				}
				break;
			case FUDaeGeometryInput::NORMAL:
				{
					if ( true != pParentMesh->HasNormals() )
					{
						FCDGeometrySource* pSource = pParentMesh->AddSource( FUDaeGeometryInput::NORMAL, "normal" );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					if ( true != HasNormals() )
					{
						m_pNormalsInput = m_pPolygons->AddInput( 
							pParentMesh->GetNormalsSource(), m_InputCount );
						++m_InputCount;
					}

					AppendSource< NoTrans3dPolicy >( m_pNormalsInput, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::TEXTANGENT:
				{
					const char* name = pSourceInput->GetSource()->GetName().c_str();

					FCDGeometrySource* pSource = pParentMesh->FindTangentsSourceWithName( name );
					if ( !pSource )
					{
						pSource = pParentMesh->AddSource( FUDaeGeometryInput::TEXTANGENT, name );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					FCDGeometryPolygonsInput* pDestination =
						FindBySourceName( m_TangentsInputs, name );
					if ( !pDestination )
					{
						pDestination = m_pPolygons->AddInput( pSource, m_InputCount );
						++m_InputCount;
						m_TangentsInputs.push_back( pDestination );
					}

					AppendSource< NoTrans3dPolicy >( pDestination, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::TEXBINORMAL:
				{
					const char* name = pSourceInput->GetSource()->GetName().c_str();

					FCDGeometrySource* pSource = pParentMesh->FindBiTangentsSourceWithName( name );
					if ( !pSource )
					{
						pSource = pParentMesh->AddSource( FUDaeGeometryInput::TEXBINORMAL, name );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					FCDGeometryPolygonsInput* pDestination =
						FindBySourceName( m_BiTangentsInputs, name );
					if ( !pDestination )
					{
						pDestination = m_pPolygons->AddInput( pSource, m_InputCount );
						++m_InputCount;
						m_BiTangentsInputs.push_back( pDestination );
					}

					AppendSource< NoTrans3dPolicy >( pDestination, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::TEXCOORD:
				{
					const char* name = pSourceInput->GetSource()->GetName().c_str();

					FCDGeometrySource* pSource = pParentMesh->FindTexCoordsSourceWithName( name );
					if ( !pSource )
					{
						pSource = pParentMesh->AddSource( FUDaeGeometryInput::TEXCOORD, name );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					FCDGeometryPolygonsInput* pDestination =
						FindBySourceName( m_TexCoordsInputs, name );
					if ( !pDestination )
					{
						pDestination = m_pPolygons->AddInput( pSource, m_InputCount );
						++m_InputCount;
						m_TexCoordsInputs.push_back( pDestination );
					}

					AppendSource< NoTrans2dPolicy >( pDestination, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::COLOR:
				{
					const char* name = pSourceInput->GetSource()->GetName().c_str();

					FCDGeometrySource* pSource = pParentMesh->FindColorsSourceWithName( name );
					if ( !pSource )
					{
						pSource = pParentMesh->AddSource( FUDaeGeometryInput::COLOR, name );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					FCDGeometryPolygonsInput* pDestination =
						FindBySourceName( m_ColorsInputs, name );
					if ( !pDestination )
					{
						pDestination = m_pPolygons->AddInput( pSource, m_InputCount );
						++m_InputCount;
						m_ColorsInputs.push_back( pDestination );
					}

					AppendSource< NoTrans4dPolicy >( pDestination, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::GEOTANGENT:
				{
					if ( true != pParentMesh->HasGeoTangents() )
					{
						FCDGeometrySource* pSource = pParentMesh->AddSource( FUDaeGeometryInput::GEOTANGENT, "geotangent" );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					if ( true != HasGeoTangents() )
					{
						m_pGeoTangentsInput = m_pPolygons->AddInput( 
							pParentMesh->GetGeoTangentsSource(), m_InputCount );
						++m_InputCount;
					}

					AppendSource< NoTrans3dPolicy >( m_pGeoTangentsInput, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::GEOBINORMAL:
				{
					if ( true != pParentMesh->HasGeoBiTangents() )
					{
						FCDGeometrySource* pSource = pParentMesh->AddSource( FUDaeGeometryInput::GEOBINORMAL, "geobinormal" );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					if ( true != HasGeoBiTangents() )
					{
						m_pGeoBiTangentsInput = m_pPolygons->AddInput( 
							pParentMesh->GetGeoBiTangentsSource(), m_InputCount );
						++m_InputCount;
					}

					AppendSource< NoTrans3dPolicy >( m_pGeoBiTangentsInput, pSourceInput, pTransform );
				}
				break;
			default:
				Assert( false );
				break;
			}
		}

		size_t faceVertCountCount = pPolygons->GetFaceVertexCountCount();
		for ( size_t f = 0; f < faceVertCountCount; ++f )
		{
			m_pPolygons->AddFaceVertexCount( 
				uint32 ( pPolygons->GetFaceVertexCount( f ) ) );
		}

		return true;
	}

	bool RcVertex::Append( const FCDGeometryPolygons* pPolygons, const FMMatrix44* pTransform, RcMesh* pParentMesh )
	{
		// if the source doesn't exist then create it...
		size_t inputCount = pPolygons->GetInputCount();
		for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
		{
			const FCDGeometryPolygonsInput* pSourceInput = pPolygons->GetInput( inputIndex );
			const FCDGeometrySource* pSourceSource = pSourceInput->GetSource();
			switch ( pSourceInput->GetSemantic() )
			{
			case FUDaeGeometryInput::POSITION:
				{
					if ( true != pParentMesh->HasPositions() )
					{
						FCDGeometrySource* pSource = pParentMesh->AddSource( FUDaeGeometryInput::POSITION, "position" );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					if ( true != HasPositions() )
					{
						m_pPositionsInput = m_pPolygons->AddInput( 
							pParentMesh->GetPositionsSource(), m_InputCount );
						++m_InputCount;
					}

					AppendSource< Transform3dCoordPolicy >( m_pPositionsInput, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::NORMAL:
				{
					if ( true != pParentMesh->HasNormals() )
					{
						FCDGeometrySource* pSource = pParentMesh->AddSource( FUDaeGeometryInput::NORMAL, "normal" );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					if ( true != HasNormals() )
					{
						m_pNormalsInput = m_pPolygons->AddInput( 
							pParentMesh->GetNormalsSource(), m_InputCount );
						++m_InputCount;
					}

					AppendSource< NoTrans3dPolicy >( m_pNormalsInput, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::TEXTANGENT:
				{
					const char* name = pSourceInput->GetSource()->GetName().c_str();

					FCDGeometrySource* pSource = pParentMesh->FindTangentsSourceWithName( name );
					if ( !pSource )
					{
						pSource = pParentMesh->AddSource( FUDaeGeometryInput::TEXTANGENT, name );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					FCDGeometryPolygonsInput* pDestination =
						FindBySourceName( m_TangentsInputs, name );
					if ( !pDestination )
					{
						pDestination = m_pPolygons->AddInput( pSource, m_InputCount );
						++m_InputCount;
						m_TangentsInputs.push_back( pDestination );
					}

					AppendSource< NoTrans3dPolicy >( pDestination, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::TEXBINORMAL:
				{
					const char* name = pSourceInput->GetSource()->GetName().c_str();

					FCDGeometrySource* pSource = pParentMesh->FindBiTangentsSourceWithName( name );
					if ( !pSource )
					{
						pSource = pParentMesh->AddSource( FUDaeGeometryInput::TEXBINORMAL, name );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					FCDGeometryPolygonsInput* pDestination =
						FindBySourceName( m_BiTangentsInputs, name );
					if ( !pDestination )
					{
						pDestination = m_pPolygons->AddInput( pSource, m_InputCount );
						++m_InputCount;
						m_BiTangentsInputs.push_back( pDestination );
					}

					AppendSource< NoTrans3dPolicy >( pDestination, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::TEXCOORD:
				{
					const char* name = pSourceInput->GetSource()->GetName().c_str();

					FCDGeometrySource* pSource = pParentMesh->FindTexCoordsSourceWithName( name );
					if ( !pSource )
					{
						pSource = pParentMesh->AddSource( FUDaeGeometryInput::TEXCOORD, name );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					FCDGeometryPolygonsInput* pDestination =
						FindBySourceName( m_TexCoordsInputs, name );
					if ( !pDestination )
					{
						pDestination = m_pPolygons->AddInput( pSource, m_InputCount );
						++m_InputCount;
						m_TexCoordsInputs.push_back( pDestination );
					}

					AppendSource< NoTrans2dPolicy >( pDestination, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::COLOR:
				{
					const char* name = pSourceInput->GetSource()->GetName().c_str();

					FCDGeometrySource* pSource = pParentMesh->FindColorsSourceWithName( name );
					if ( !pSource )
					{
						pSource = pParentMesh->AddSource( FUDaeGeometryInput::COLOR, name );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					FCDGeometryPolygonsInput* pDestination =
						FindBySourceName( m_ColorsInputs, name );
					if ( !pDestination )
					{
						pDestination = m_pPolygons->AddInput( pSource, m_InputCount );
						++m_InputCount;
						m_ColorsInputs.push_back( pDestination );
					}

					AppendSource< NoTrans4dPolicy >( pDestination, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::GEOTANGENT:
				{
					if ( true != pParentMesh->HasGeoTangents() )
					{
						FCDGeometrySource* pSource = pParentMesh->AddSource( FUDaeGeometryInput::GEOTANGENT, "geotangent" );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					if ( true != HasGeoTangents() )
					{
						m_pGeoTangentsInput = m_pPolygons->AddInput( 
							pParentMesh->GetGeoTangentsSource(), m_InputCount );
						++m_InputCount;
					}

					AppendSource< NoTrans3dPolicy >( m_pGeoTangentsInput, pSourceInput, pTransform );
				}
				break;
			case FUDaeGeometryInput::GEOBINORMAL:
				{
					if ( true != pParentMesh->HasGeoBiTangents() )
					{
						FCDGeometrySource* pSource = pParentMesh->AddSource( FUDaeGeometryInput::GEOBINORMAL, "geobinormal" );
						pSource->SetStride( pSourceSource->GetStride() );
					}

					if ( true != HasGeoBiTangents() )
					{
						m_pGeoBiTangentsInput = m_pPolygons->AddInput( 
							pParentMesh->GetGeoBiTangentsSource(), m_InputCount );
						++m_InputCount;
					}

					AppendSource< NoTrans3dPolicy >( m_pGeoBiTangentsInput, pSourceInput, pTransform );
				}
				break;
			default:
				Assert( false );
				break;
			}
		}

		/*
		if ( HasPositions() )
		{
			const FCDGeometryPolygonsInput* pInput = pPolygons->FindInput( FUDaeGeometryInput::POSITION );
			Assert( NULL != pInput );

			AppendSource< Transform3dCoordPolicy >( m_pPositionsInput, pInput, pTransform );
		}
		if ( HasNormals() )
		{
			const FCDGeometryPolygonsInput* pInput = pPolygons->FindInput( FUDaeGeometryInput::NORMAL );
			Assert( NULL != pInput );

			AppendSource< NoTrans3dPolicy >( m_pNormalsInput, pInput, pTransform );
		}
		if ( HasTangents() )
		{
			FCDGeometryPolygonsInputConstList inputs;
			pPolygons->FindInputs( FUDaeGeometryInput::TEXTANGENT, inputs );
			Assert( inputs.size() == m_TangentsInputs.size() );

			size_t inputCount = inputs.size();
			for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
			{
				AppendSource< NoTrans3dPolicy >( m_TangentsInputs[ inputIndex ], inputs[ inputIndex ], pTransform );
			}
		}
		if ( HasBiTangents() )
		{
			FCDGeometryPolygonsInputConstList inputs;
			pPolygons->FindInputs( FUDaeGeometryInput::TEXBINORMAL, inputs );
			Assert( inputs.size() == m_BiTangentsInputs.size() );

			size_t inputCount = inputs.size();
			for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
			{
				AppendSource< NoTrans3dPolicy >( m_BiTangentsInputs[ inputIndex ], inputs[ inputIndex ], pTransform );
			}
		}
		if ( HasTexCoords() )
		{
			FCDGeometryPolygonsInputConstList inputs;
			pPolygons->FindInputs( FUDaeGeometryInput::TEXCOORD, inputs );
			Assert( inputs.size() == m_TexCoordsInputs.size() );
			
			size_t inputCount = inputs.size();
			for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
			{
				AppendSource< NoTrans2dPolicy >( m_TexCoordsInputs[ inputIndex ], inputs[ inputIndex ], pTransform );
			}
		}
		if ( HasColors() )
		{
			FCDGeometryPolygonsInputConstList inputs;
			pPolygons->FindInputs( FUDaeGeometryInput::COLOR, inputs );
			Assert( inputs.size() == m_ColorsInputs.size() );

			size_t inputCount = inputs.size();
			for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
			{
				AppendSource< NoTrans3dPolicy >( m_ColorsInputs[ inputIndex ], inputs[ inputIndex ], pTransform );
			}
		}
		if ( HasGeoTangents() )
		{
			const FCDGeometryPolygonsInput* pInput = pPolygons->FindInput( FUDaeGeometryInput::GEOTANGENT );
			Assert( NULL != pInput );

			AppendSource< NoTrans3dPolicy >( m_pGeoTangentsInput, pInput, pTransform );
		}
		if ( HasGeoBiTangents() )
		{
			const FCDGeometryPolygonsInput* pInput = pPolygons->FindInput( FUDaeGeometryInput::GEOBINORMAL );
			Assert( NULL != pInput );

			AppendSource< NoTrans3dPolicy >( m_pGeoBiTangentsInput, pInput, pTransform );
		}
		*/

		size_t faceVertCountCount = pPolygons->GetFaceVertexCountCount();
		for ( size_t f = 0; f < faceVertCountCount; ++f )
		{
			m_pPolygons->AddFaceVertexCount( 
				uint32 ( pPolygons->GetFaceVertexCount( f ) ) );
		}

		return true;
	}

	static float* GetData( FCDGeometryPolygonsInput* pInput, uint32 index )
	{
		uint32 i = pInput->GetIndices()[ index ];
		FCDGeometrySource* pSource = pInput->GetSource();
		i *= pSource->GetStride();
		
		Assert( i < pSource->GetDataCount() );

		float* pData = pSource->GetData();
		
		return pData + index;
	}

	FMVector3 RcVertex::Iterator::GetPosition() const
	{
		Assert( NULL != that.m_pPositionsInput );

		float* pData = GetData( that.m_pPositionsInput, curr );

		return FMVector3 ( pData[ 0 ], pData[ 1 ], pData[ 2 ] );
	}

	FMVector3 RcVertex::Iterator::GetNormal() const
	{
		Assert( NULL != that.m_pNormalsInput );

		float* pData = GetData( that.m_pNormalsInput, curr );

		return FMVector3 ( pData[ 0 ], pData[ 1 ], pData[ 2 ] );
	}

	FMVector3 RcVertex::Iterator::GetTangent( int set ) const
	{
		Assert( 0 <= set && set < GetTangentCount() );

		float* pData = GetData( that.m_TangentsInputs[ set ], curr );

		return FMVector3 ( pData[ 0 ], pData[ 1 ], pData[ 2 ] );
	}

	int RcVertex::Iterator::GetTangentCount() const
	{
		return int ( that.m_TangentsInputs.size() );
	}

	FMVector3 RcVertex::Iterator::GetBiTangent( int set ) const
	{
		Assert( 0 <= set && set < GetBiTangentCount() );

		float* pData = GetData( that.m_BiTangentsInputs[ set ], curr );

		return FMVector3 ( pData[ 0 ], pData[ 1 ], pData[ 2 ] );
	}

	int RcVertex::Iterator::GetBiTangentCount() const
	{
		return int ( that.m_BiTangentsInputs.size() );
	}

	FMVector2 RcVertex::Iterator::GetTexCoord( int set ) const
	{
		Assert( 0 <= set && set < GetTexCoordCount() );

		float* pData = GetData( that.m_TexCoordsInputs[ set ], curr );

		return FMVector2( pData[ 0 ], pData[ 1 ] );
	}

	int RcVertex::Iterator::GetTexCoordCount() const
	{
		return int ( that.m_TexCoordsInputs.size() );
	}

	FMVector3 RcVertex::Iterator::GetColor( int set ) const
	{
		Assert( 0 <= set && set < GetColorCount() );

		float* pData = GetData( that.m_ColorsInputs[ set ], curr );

		return FMVector3( pData[ 0 ], pData[ 1 ], pData[ 2 ] );
	}

	int RcVertex::Iterator::GetColorCount() const
	{
		return int ( that.m_ColorsInputs.size() );
	}

	FMVector3 RcVertex::Iterator::GetGeoTangent() const
	{
		Assert( NULL != that.m_pGeoTangentsInput );

		float* pData = GetData( that.m_pGeoTangentsInput, curr );

		return FMVector3( pData[ 0 ], pData[ 1 ], pData[ 2 ] );
	}

	FMVector3 RcVertex::Iterator::GetGeoBiTangent() const
	{
		Assert( NULL != that.m_pGeoBiTangentsInput );

		float* pData = GetData( that.m_pGeoBiTangentsInput, curr );

		return FMVector3 ( pData[ 0 ], pData[ 1 ], pData[ 2 ] );
	}

	RcVertex::Iterator::Iterator( RcVertex* pThat, size_t start )
		:	that ( *pThat )
		,	curr ( start )
	{
	}

	RcVertex::Iterator& RcVertex::Iterator::operator= ( const RcVertex::Iterator& rhs )
	{
		that = rhs.that;
		curr = rhs.curr;

		return *this;
	}

	void RcVertex::Iterator::operator++()
	{
		++curr;

	}

} // rage
