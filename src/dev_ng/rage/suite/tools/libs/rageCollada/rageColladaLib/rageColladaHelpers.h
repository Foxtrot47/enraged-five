#ifndef __RAGE_COLLADA_HELPERS__
#define __RAGE_COLLADA_HELPERS__

#include "rageColladaUtil.h"

class FCDGeometry;
class FCDGeometrySource;
class FCDSceneNode;
class FCDSkinController;

namespace rage {

	// Triangulate the from a scene node
	void Triangulate( FCDSceneNode* pSceneNode );

	// Triangulate only one geometry element
	void Triangulate( FCDGeometry* pGeometry );

	// Deindex the from a scene node
	void Deindex( FCDSceneNode* pSceneNode );

	// Deindex only one geometry element
	void Deindex( FCDGeometry* pGeometry );

	// Deindex only one skinned element
	void Deindex( FCDSkinController* pSkinController );

	// Calculate texture tangents & bi-tangents
	void CalcTextureTangentBasis( FCDGeometry* pGeometry, FCDGeometrySource* pTexcoordSource );

	typedef std::pair< const FCDGeometryPolygons*, const FCDMaterialInstance* > RCUPolygonMaterialPair;
	void MapMaterials( RCUPolygonMaterialPair& polymat, const FCDControllerInstance* pControllerInstance );
	void MapMaterials( RCUPolygonMaterialPair& polymat, const FCDGeometryInstance* pGeometryInstance );

	typedef fm::vector< RCUPolygonMaterialPair > RCUPolygonMaterialPairList;

	void MergeControllers( FCDSceneNode* pRootNode, const FCDSceneNodeList& nodes );
	void MergeGeometries( FCDSceneNode* pRootNode, const FCDSceneNodeList& nodes );

	// Take the root node in here so we can calculate correct indexes
	typedef std::vector< FCDSceneNode* > FCDMeshRigidSkinningList;
	void RigidSkin( FCDSceneNode* pRootNode, FCDMeshRigidSkinningList& list );

	void ReRootSkeleton( FCDSceneNode* pRootNode, FCDSceneNode* pSkeletonNode );

	// Set skinned elements to the zero-rotation bind pose
	void SetZeroRotationBindPose( FCDSceneNode* sceneNode );
	void SetZeroRotationBindPose( FCDSkinController* skinController );

} // rage 

#endif // __RAGE_COLLADA_HELPERS__
