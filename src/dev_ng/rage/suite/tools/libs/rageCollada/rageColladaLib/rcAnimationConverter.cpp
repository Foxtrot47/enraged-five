// /rcAnimationConverter.cpp
#include <queue>

#include "rageColladaUtil.h"
#include "rcAnimationConverter.h"

using namespace std;

namespace rage
{

//-----------------------------------------------------------------------------

void RcAnimationConverter::SampleNodeTransforms( FCDSceneNode* pNode, 
							  float startTime, float endTime, float framesPerSecond,
							  vector<Matrix34>& frameMatrices )
{
	float step = 1.0f / framesPerSecond;
	int frameCount = (int)ceil( (endTime - startTime) * framesPerSecond);
	
	Matrix34 idMat;
	idMat.Identity();
	frameMatrices.clear();
	frameMatrices.assign(frameCount, idMat);

	FCDAnimatedList	animateds;
	
	const FCDTransformContainer& nodeTransforms = pNode->GetTransforms();
	for (FCDTransformContainer::const_iterator it = nodeTransforms.begin(); it != nodeTransforms.end(); ++it)
	{
		const FCDAnimated* animated = (*it)->GetAnimated();
		if (animated != NULL && animated->HasCurve()) animateds.push_back(animated);
	}

	if (!animateds.empty())
	{
		float sampleTime = startTime;
		for(int i=0; i<frameCount; i++)
		{
			for (FCDAnimatedList::iterator it = animateds.begin(); it != animateds.end(); ++it)
			{
				// Sample each animated, which changes the transform values directly
				(*it)->Evaluate(sampleTime);
			}

			FMMatrix44 sampleMatrix = pNode->ToMatrix();
			ToMatrix34( frameMatrices[i], sampleMatrix );

			sampleTime += step;
		}
	}
}

//-----------------------------------------------------------------------------

void RcAnimationConverter::SampleMorphTargetWeights( FCDMorphController* pMorphCtrl,
										float startTime, float endTime, float framesPerSecond,
										atArray< atArray< float > >& targetFrameWeights )
{
	float step = 1.0f / framesPerSecond;
	int frameCount = (int)ceil( (endTime - startTime) * framesPerSecond);

	FCDEntity* pBaseTargetEntity = pMorphCtrl->GetBaseTarget();
	const fstring& baseTargetName = pBaseTargetEntity->GetName();
	size_t targetCount = pMorphCtrl->GetTargetCount();

	//Initialize the output array
	targetFrameWeights.Reset();
	targetFrameWeights.Reserve(targetCount);
	
	for(size_t i=0; i<targetCount; i++)
	{
		atArray<float>& frameWeights = targetFrameWeights.Grow();
		frameWeights.Reset();
		frameWeights.Reserve(frameCount);
		for(int j=0; j<frameCount; j++)
		{
			frameWeights.Grow() = 0.0f;
		}
	}

	for(size_t targetIdx = 0; targetIdx < targetCount; targetIdx++)
	{
		atArray<float>& frameWeights = targetFrameWeights[targetIdx];
		
		FCDMorphTarget* pTarget = pMorphCtrl->GetTarget(targetIdx);
		Assert(pTarget);

		if(pTarget->IsAnimated())
		{
			FCDAnimated* pTargetAnim = pTarget->GetAnimatedWeight();
			Assert(pTargetAnim);

			float sampleTime = startTime;
			for(int i=0; i<frameCount; i++)
			{
				pTargetAnim->Evaluate(sampleTime);
				frameWeights[frameCount] = pTarget->GetWeight();
				sampleTime += step;
			}
		}
		else
		{
			float staticTargetWeight = pTarget->GetWeight();
			for(int i=0; i<frameCount; i++)
				frameWeights[frameCount] = staticTargetWeight;
		}
	}
}

//-----------------------------------------------------------------------------

class RcSampledNodeCacheElement
{
public:
	RcSampledNodeCacheElement()
		: m_identifier(0)
	{};

	virtual ~RcSampledNodeCacheElement() {};

	RcSampledNodeCacheElement( const char* srcNodeName, unsigned short id, unsigned char trackType)
		: m_srcNodeName(srcNodeName)
		, m_identifier(id)
		, m_trackType(trackType)
	{};

public:
	string				m_srcNodeName;
	unsigned short		m_identifier;
	unsigned char		m_trackType;

	virtual crAnimTrack* CreateAnimTrack( const crAnimTolerance& compressionTolerance, u16 framesPerChunk ) = 0;
};

//-----------------------------------------------------------------------------

class RcSampledNodeVector3CacheElement : public RcSampledNodeCacheElement
{
public:
	RcSampledNodeVector3CacheElement( const char* srcNodeName, unsigned short id, unsigned char trackType)
		: RcSampledNodeCacheElement(srcNodeName, id, trackType)
	{};

	virtual crAnimTrack* CreateAnimTrack( const crAnimTolerance& compressionTolerance, u16 framesPerChunk )
	{
		crAnimTrack* pVector3Track = new crAnimTrack(m_trackType, m_identifier);
		pVector3Track->CreateVector3(m_sampledVec3, compressionTolerance, framesPerChunk);
		return pVector3Track;
	}

public:
	atArray<Vector3> m_sampledVec3;
};

//-----------------------------------------------------------------------------

class RcSampledNodeQuaternionCacheElement : public RcSampledNodeCacheElement
{
public:
	RcSampledNodeQuaternionCacheElement( const char* srcNodeName, unsigned short id, unsigned char trackType)
		: RcSampledNodeCacheElement(srcNodeName, id, trackType)
	{};

	virtual crAnimTrack* CreateAnimTrack( const crAnimTolerance& compressionTolerance, u16 framesPerChunk )
	{
		crAnimTrack* pQuatTrack = new crAnimTrack(m_trackType, m_identifier);
		pQuatTrack->CreateQuaternion(m_sampledQuat, compressionTolerance, framesPerChunk);
		return pQuatTrack;
	}

public:
	atArray<Quaternion> m_sampledQuat;
};

//-----------------------------------------------------------------------------

class RcSampledNodeFloatCacheElement : public RcSampledNodeCacheElement
{
public:
	RcSampledNodeFloatCacheElement( const char* srcNodeName, unsigned short id, unsigned char trackType)
		: RcSampledNodeCacheElement(srcNodeName, id, trackType)
	{};

	virtual crAnimTrack* CreateAnimTrack( const crAnimTolerance& compressionTolerance, u16 framesPerChunk )
	{
		crAnimTrack* pFloatTrack = new crAnimTrack;
		pFloatTrack->CreateFloat(m_trackType, m_identifier, m_sampledFloat, compressionTolerance, framesPerChunk);
		return pFloatTrack;
	}
public:
	atArray< float > m_sampledFloat;
};

//-----------------------------------------------------------------------------

const AnimExportCtrlTrackSpec* FindAnimCtrlFileTrackSpec(AnimExportCtrlSpec* pAnimExportCtrl, FCDSceneNode* pJoint, bool bIsRoot)
{
	Assert(pAnimExportCtrl);
	Assert(pJoint);

	const AnimExportCtrlTrackSpec* pTrackSpec = NULL;

	if(bIsRoot)
	{
		//Root joints are special cased in the control file, so look for the "root" spec
		pTrackSpec = pAnimExportCtrl->FindTrackSpec("root");
		if(!pTrackSpec)
		{
			//The "root" spec wasn't defined in the control spec, so try to get the spec based on the joint name
			pTrackSpec = pAnimExportCtrl->FindTrackSpec( pJoint->GetName().c_str() );
		}
	}
	else
	{
		//The joint isn't the root, so start the search for the track specification using the joint's name
		pTrackSpec = pAnimExportCtrl->FindTrackSpec( pJoint->GetName().c_str() );
		if( !pTrackSpec || (pTrackSpec && (strcmp(pTrackSpec->GetNameExpr(),"*") == 0)) )
		{
			//Either the track spec wasn't found, or the spec that was found includes all joints, so try
			//to search up the hierarchy looking for a track specification that matches and is marked
			//to include its children
			FCDSceneNode* pParentJoint = pJoint->GetParent();
			while(pParentJoint)
			{
				const AnimExportCtrlTrackSpec* pParentTrackSpec = pAnimExportCtrl->FindTrackSpec( pParentJoint->GetName().c_str() );
				if (pParentTrackSpec) 
				{
					if(pParentTrackSpec->IncludesChildren() && (strcmp(pParentTrackSpec->GetNameExpr(), "*") != 0))
					{
						//The parent track specification isn't the default fallthrough of "*", and is marked
						//as including its children, so use this track specification
						pTrackSpec = pParentTrackSpec;
						break;
					}
				}
				pParentJoint = pParentJoint->GetParent();
			}
		}
	}

	return pTrackSpec;
}

//-----------------------------------------------------------------------------

typedef std::vector< RcSampledNodeCacheElement* > SAMPLED_NODE_CACHE;

bool RcAnimationConverter::ConvertAnimationCompressed(crAnimation& outAnim, FCDSceneNode* pSkelRootNode, 
													  AnimExportCtrlSpec* pAnimExportCtrl,
													  vector<FCDSceneNode*>* pMoverNodes )
{
	const bool	bBoneIdsUsed = true;
	const bool	bAuthoredOrientation = true;
	const float framesPerSecond = 30.0f;

	FCDocument* pDoc = pSkelRootNode->GetDocument();
	Assert(pDoc);

	float startTime, endTime;
	startTime = pDoc->GetStartTime();
	endTime = pDoc->GetEndTime();

	float timeDuration = endTime - startTime;
	int numFrames = (int) ceil( (timeDuration * framesPerSecond) );
	
	SAMPLED_NODE_CACHE smpNodeCache;

	//Sample the nodes, and store the results into the sampled node cache
	queue<FCDSceneNode*> nodeQueue;
	nodeQueue.push( pSkelRootNode );
	int boneIndex = -1;

	while(!nodeQueue.empty())
	{
		FCDSceneNode* pNode = nodeQueue.front();
		nodeQueue.pop();

		boneIndex++;
		const fstring& nodeName = pNode->GetName();

		if(pNode->IsJoint() && (strcmpi(pNode->GetName().c_str(), "_null") != 0))
		{
			vector<Matrix34> frameMatrices;

			//Sample the transforms for this node across the specified time range
			SampleNodeTransforms(pNode, startTime, endTime, framesPerSecond, frameMatrices);

			//Generate a bone id
			unsigned short boneId;
			if( pNode->GetParent() && !pNode->GetParent()->IsJoint())
				boneId = 0;
			else
			{
				const char* boneName = nodeName.c_str();
				if( m_userDefinedBoneIdFtor )
				{
					boneId = m_userDefinedBoneIdFtor( boneName, boneIndex );
				}
				else
					boneId = crAnimTrack::ConvertBoneNameToId(boneName);
			}

			bool bSampleTranslation = true;
			bool bSampleRotation = true;

			//If an animation control file has been specified, use it to determine what tracks should be sampled (and exported)
			//TODO : Need to pick up the compression specifications from the control file as well
			if(pAnimExportCtrl)
			{
				const AnimExportCtrlTrackSpec* pTrackSpec = FindAnimCtrlFileTrackSpec(pAnimExportCtrl, pNode, (pNode == pSkelRootNode));
				if(pTrackSpec)
				{
					const AnimExportCtrlTrack* pTransCtrlTrack = pTrackSpec->GetTrackByInputName("translate");
					if(!pTransCtrlTrack)
					{
						bSampleTranslation = false;
					}

					const AnimExportCtrlTrack* pRotCtrlTrack = pTrackSpec->GetTrackByInputName("rotate");
					if(!pRotCtrlTrack)
					{
						bSampleRotation = false;
					}
				}
			}
			else
			{
				//Default to writing rotation and translation for the root, and only rotation for the other joints
				if(pNode == pSkelRootNode)
					bSampleTranslation = false;
			}

			if(bSampleTranslation)
			{
				RcSampledNodeVector3CacheElement *pTransElement = new RcSampledNodeVector3CacheElement( pNode->GetName().c_str(), boneId, kTrackBoneTranslation );
				pTransElement->m_sampledVec3.Reserve(numFrames);
				for(int frameIdx=0; frameIdx < numFrames; frameIdx++)
				{
					pTransElement->m_sampledVec3.Grow() = frameMatrices[frameIdx].d;
				}
				smpNodeCache.push_back(pTransElement);
			}

			if(bSampleRotation)
			{
				RcSampledNodeQuaternionCacheElement *pRotElement = new RcSampledNodeQuaternionCacheElement( pNode->GetName().c_str(), boneId, kTrackBoneRotation );
				pRotElement->m_sampledQuat.Reserve(numFrames);
				for(int frameIdx=0; frameIdx<numFrames; frameIdx++)
				{
					frameMatrices[frameIdx].ToQuaternion(pRotElement->m_sampledQuat.Grow());
				}
				smpNodeCache.push_back(pRotElement);
			}
		}

		//Add the children to the queue of nodes
		size_t childCount = pNode->GetChildrenCount();
		for(size_t childIdx = 0; childIdx < childCount; childIdx++)
		{
			nodeQueue.push(pNode->GetChild(childIdx));
		}
	}

	//Export any tracks associated with the movers
	if(pMoverNodes)
	{
		for( vector<FCDSceneNode*>::iterator it = pMoverNodes->begin();
			it != pMoverNodes->end();
			++it)
		{
			FCDSceneNode *pNode = (*it);

			vector<Matrix34> frameMatrices;
			SampleNodeTransforms(pNode, startTime, endTime, framesPerSecond, frameMatrices);

			//The mover id is set to zero, unless multiple movers have been specified, in which case it needs
			//a unique mover id generated for it
			u16 moverId = 0;
			if(pMoverNodes->size() > 1)
			{
				moverId = crAnimTrack::ConvertMoverNameToId( pNode->GetName().c_str() );
			}

			//Sample the mover translation track
			RcSampledNodeVector3CacheElement *pTransElement = new RcSampledNodeVector3CacheElement( pNode->GetName().c_str(), moverId, kTrackMoverTranslation );
			pTransElement->m_sampledVec3.Reserve(numFrames);
			for(int frameIdx=0; frameIdx < numFrames; frameIdx++)
			{
				pTransElement->m_sampledVec3.Grow() = frameMatrices[frameIdx].d;
			}
			smpNodeCache.push_back(pTransElement);
			

			//Sample the mover rotation track
			RcSampledNodeQuaternionCacheElement *pRotElement = new RcSampledNodeQuaternionCacheElement( pNode->GetName().c_str(), moverId, kTrackMoverRotation );
			pRotElement->m_sampledQuat.Reserve(numFrames);
			for(int frameIdx=0; frameIdx < numFrames; frameIdx++)
			{
				frameMatrices[frameIdx].ToQuaternion(pRotElement->m_sampledQuat.Grow());
			}
			smpNodeCache.push_back(pRotElement);
		}
	}//End if(pMoverNodes)

	//Export any animation associated with the document controllers
	FCDControllerLibrary *pCtrlLib = pDoc->GetControllerLibrary();
	if(pCtrlLib)
	{
		size_t ctrlCount = pCtrlLib->GetEntityCount();
		for(size_t ctrlIdx = 0; ctrlIdx < ctrlCount; ctrlIdx++)
		{
			FCDController *pCtrl = pCtrlLib->GetEntity(ctrlIdx);
			Assert(pCtrl);

			//Export Morpher controller animation
			if(pCtrl->IsMorph())
			{
				FCDMorphController* pMorphCtrl = pCtrl->GetMorphController();
				Assert(pMorphCtrl);
				
				atArray< atArray< float > > targetFrameWeights;
				SampleMorphTargetWeights( pMorphCtrl, startTime, endTime, framesPerSecond, targetFrameWeights);

				size_t targetCount = pMorphCtrl->GetTargetCount();
				for(size_t targetIdx = 0; targetIdx < targetCount; targetIdx++)
				{
					const FCDMorphTarget* pTarget = pMorphCtrl->GetTarget(targetIdx);
					Assert(pTarget);
					const FCDGeometry* pTargetGeom = pTarget->GetGeometry();
					const fstring& targetName = pTargetGeom->GetName();

					unsigned short trackId = crAnimTrack::ConvertBoneNameToId(targetName.c_str());

					RcSampledNodeFloatCacheElement *pFloatElement = new RcSampledNodeFloatCacheElement( targetName.c_str(), trackId, kTrackBlendShape );
					pFloatElement->m_sampledFloat.Reserve(numFrames);
					for(int frameIdx=0; frameIdx < numFrames; ++frameIdx)
					{
						pFloatElement->m_sampledFloat.Grow() = targetFrameWeights[targetIdx][frameIdx];
					}
					smpNodeCache.push_back(pFloatElement);
				}
			}//End if(pCtrl->IsMorph()...
		}//End for(size_t ctrlIdx = 0...
	}//End if(pCtrlLib)...

	
	bool bLooping = false;				//TODO : Need to allow this to be specified by the user
	bool bNormalizeRoot = false;		//TODO : Need to allow this to be specified by the user
	unsigned int maxBlockSize = 65536;	//TODO : Need to allow this to be specified by the user

	

	unsigned short framesPerChunk = (maxBlockSize>0) ? u16(((numFrames+16)&(~15))-1) : crAnimation::sm_DefaultFramesPerChunk;
	unsigned short numBlocks = 1;

	bool bHasMoverTracks = (pMoverNodes && pMoverNodes->size());

	crAnimToleranceSimple compressionTolerance;
	compressionTolerance.Init(
			crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError,
			crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError,
			crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError,
			crAnimTolerance::eDecompressionCost(crAnimTolerance::kDecompressionCostDefault), 
			crAnimTolerance::eCompressionCost(crAnimTolerance::kCompressionCostDefault));

	do
	{
		crAnimation tmpAnim;

		tmpAnim.Create(u16(numFrames), 
						timeDuration, 
						bLooping, 
						bHasMoverTracks, 
						(bHasMoverTracks && bNormalizeRoot), 
						bBoneIdsUsed, 
						bAuthoredOrientation, 
						framesPerChunk);


		for(SAMPLED_NODE_CACHE::iterator it = smpNodeCache.begin(); it != smpNodeCache.end(); ++it)
		{
			RcSampledNodeCacheElement* pElement = (*it);
			tmpAnim.CreateTrack(pElement->CreateAnimTrack( compressionTolerance, framesPerChunk ));
		}

		if(maxBlockSize)
		{
			tmpAnim.Pack();
		}

		if(!maxBlockSize || tmpAnim.GetMaxBlockSize() <= maxBlockSize)
		{
			//Stop the block size search...
			outAnim = tmpAnim;
			break;
		}
		else
		{
			// calculate the current number of blocks, then pick a new frames per chunk value that will add one more block
			unsigned short newFramesPerChunk = unsigned short((numFrames-2)/++numBlocks);

			const u16 minFramesPerChunk = 16;
			if(newFramesPerChunk > minFramesPerChunk)
			{
				framesPerChunk = ((newFramesPerChunk+16)&(~15))-1;
			}
			else
			{
				Errorf("Max block size driven frames per chunk search failed.");
				break;
			}
		}
	}
	while(1);
	
	//Clean up the sampled node cache
	for(SAMPLED_NODE_CACHE::iterator it = smpNodeCache.begin(); it != smpNodeCache.end(); ++it)
	{
		delete (*it);
	}

	return true;
}

bool RcAnimationConverter::ConvertAnimationUncompressed( crAnimation& outAnim, FCDSceneNode* pSkelRootNode, vector<FCDSceneNode*>* pMoverNodes )
{
	const bool	bBoneIdsUsed = true;
	const bool	bAuthoredOrientation = true;
	const float framesPerSecond = 30.0f;

	FCDocument* pDoc = pSkelRootNode->GetDocument();
	Assert(pDoc);

	float startTime, endTime;
	startTime = pDoc->GetStartTime();
	endTime = pDoc->GetEndTime();

	float timeDuration = endTime - startTime;
	int numFrames = (int) ceil( (timeDuration * framesPerSecond) );
	
	bool bLooping = false;				//TODO : Need to allow this to be specified by the user
	bool bNormalizeRoot = false;		//TODO : Need to allow this to be specified by the user
	
	bool bHasMoverTracks = (pMoverNodes && pMoverNodes->size());

	crAnimToleranceSimple compressionTolerance;
	compressionTolerance.Init(
			crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError,
			crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError,
			crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError,
			crAnimTolerance::eDecompressionCost(crAnimTolerance::kDecompressionCostDefault), 
			crAnimTolerance::eCompressionCost(crAnimTolerance::kCompressionCostDefault));
	
	outAnim.Create(u16(numFrames), 
					timeDuration, 
					bLooping, 
					bHasMoverTracks, 
					(bHasMoverTracks && bNormalizeRoot), 
					bBoneIdsUsed, 
					bAuthoredOrientation);

	queue<FCDSceneNode*> nodeQueue;
	nodeQueue.push( pSkelRootNode );
	int boneIndex = -1;

	while(!nodeQueue.empty())
	{
		FCDSceneNode* pNode = nodeQueue.front();
		nodeQueue.pop();

		boneIndex++;
		const fstring& nodeName = pNode->GetName();

		if(pNode->IsJoint() && (strcmpi(pNode->GetName().c_str(), "_null") != 0))
		{
			vector<Matrix34> frameMatrices;

			//Sample the transforms for this node across the specified time range
			SampleNodeTransforms(pNode, startTime, endTime, framesPerSecond, frameMatrices);

			//Generate a bone id
			unsigned short boneId;
			if( pNode->GetParent() && !pNode->GetParent()->IsJoint())
				boneId = 0;
			else
			{
				const char* boneName = nodeName.c_str();
				if( m_userDefinedBoneIdFtor )
				{
					boneId = m_userDefinedBoneIdFtor( boneName, boneIndex );
				}
				else
					boneId = crAnimTrack::ConvertBoneNameToId(boneName);
			}

			//Export the translation track
			atArray<Vector3>	av;
			av.Reserve(numFrames);
			for(int frameIdx=0; frameIdx < numFrames; frameIdx++)
			{
				av.Grow() = frameMatrices[frameIdx].d;
			}

			crAnimTrack* pTranslateTrack = new crAnimTrack(kTrackBoneTranslation, boneId);
			//TODO : Need to support compression tolerance specifications here
			pTranslateTrack->CreateVector3(av);
			outAnim.CreateTrack(pTranslateTrack);

			//Export the rotation track
			atArray<Quaternion> aq;
			aq.Reserve(numFrames);
			for(int frameIdx=0; frameIdx<numFrames; frameIdx++)
			{
				frameMatrices[frameIdx].ToQuaternion(aq.Grow());
			}

			crAnimTrack* pRotateTrack = new crAnimTrack(kTrackBoneRotation, boneId);
			pRotateTrack->CreateQuaternion(aq);
			outAnim.CreateTrack(pRotateTrack);
		}

		//Add the children to the queue of nodes
		size_t childCount = pNode->GetChildrenCount();
		for(size_t childIdx = 0; childIdx < childCount; childIdx++)
		{
			nodeQueue.push(pNode->GetChild(childIdx));
		}
	}

	if(pMoverNodes)
	{
		//Export any tracks associated with the movers
		for( vector<FCDSceneNode*>::iterator it = pMoverNodes->begin();
			it != pMoverNodes->end();
			++it)
		{
			FCDSceneNode *pNode = (*it);

			vector<Matrix34> frameMatrices;
			SampleNodeTransforms(pNode, startTime, endTime, framesPerSecond, frameMatrices);

			//The mover id is set to zero, unless multiple movers have been specified, in which case it needs
			//a unique mover id generated for it
			u16 moverId = 0;
			if(pMoverNodes->size() > 1)
			{
				moverId = crAnimTrack::ConvertMoverNameToId( pNode->GetName().c_str() );
			}

			//Export the mover translation track
			atArray<Vector3>	av;
			av.Reserve(numFrames);
			for(int frameIdx=0; frameIdx < numFrames; frameIdx++)
			{
				av.Grow() = frameMatrices[frameIdx].d;
			}
			
			crAnimTrack* pMoverTranslateTrack = new crAnimTrack(kTrackMoverTranslation, moverId);
			//TODO  : Need to support compression tolerance specifications here
			pMoverTranslateTrack->CreateVector3(av);
			outAnim.CreateTrack(pMoverTranslateTrack);

			//Export the mover rotation track
			atArray<Quaternion> aq;
			aq.Reserve(numFrames);
			for(int frameIdx=0; frameIdx < numFrames; frameIdx++)
			{
				frameMatrices[frameIdx].ToQuaternion(aq.Grow());
			}

			crAnimTrack* pMoverRotateTrack = new crAnimTrack(kTrackMoverRotation, moverId);
			//TODO : Need to support compression tolerance specifications here
			pMoverRotateTrack->CreateQuaternion(aq);
			outAnim.CreateTrack(pMoverRotateTrack);
		}
	}//End if(pMoverNodes)

	//Export any animation associated with the document controllers
	FCDControllerLibrary *pCtrlLib = pDoc->GetControllerLibrary();
	if(pCtrlLib)
	{
		size_t ctrlCount = pCtrlLib->GetEntityCount();
		for(size_t ctrlIdx = 0; ctrlIdx < ctrlCount; ctrlIdx++)
		{
			FCDController *pCtrl = pCtrlLib->GetEntity(ctrlIdx);
			Assert(pCtrl);

			//Export Morpher controller animation
			if(pCtrl->IsMorph())
			{
				FCDMorphController* pMorphCtrl = pCtrl->GetMorphController();
				Assert(pMorphCtrl);
				
				atArray< atArray< float > > targetFrameWeights;
				SampleMorphTargetWeights( pMorphCtrl, startTime, endTime, framesPerSecond, targetFrameWeights);

				size_t targetCount = pMorphCtrl->GetTargetCount();
				for(size_t targetIdx = 0; targetIdx < targetCount; targetIdx++)
				{
					const FCDMorphTarget* pTarget = pMorphCtrl->GetTarget(targetIdx);
					Assert(pTarget);
					const FCDGeometry* pTargetGeom = pTarget->GetGeometry();
					const fstring& targetName = pTargetGeom->GetName();

					unsigned short trackId = crAnimTrack::ConvertBoneNameToId(targetName.c_str());

					crAnimTrack* pAnimMorphTrack = new crAnimTrack;
					
					pAnimMorphTrack->CreateFloat(kTrackBlendShape, trackId, targetFrameWeights[targetIdx]);
					outAnim.CreateTrack(pAnimMorphTrack);
				}
			}//End if(pCtrl->IsMorph()...
		}//End for(size_t ctrlIdx = 0...
	}//End if(pCtrlLib)...

	return true;
}

//-----------------------------------------------------------------------------


}//end namespace rage

