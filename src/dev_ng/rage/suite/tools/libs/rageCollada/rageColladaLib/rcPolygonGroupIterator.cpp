#include "rcPolygonGroupIterator.h"

#include "rcVertexIterator.h"

#include "mesh/mesh.h"

#include <algorithm>

using namespace std;

namespace {

	static void CreatePolygonSources( FCDGeometryPolygons* pDest, const FCDGeometryPolygons* pSource )
	{
		FCDGeometryMesh* pMesh = pDest->GetParent();

		size_t inputCount = pSource->GetInputCount();
		for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
		{
			const FCDGeometryPolygonsInput* pInput = pSource->GetInput( inputIndex );

			FCDGeometrySource* pGeometrySource = NULL;
			if ( FUDaeGeometryInput::POSITION == pInput->GetSemantic() )
			{
				pGeometrySource = pMesh->AddVertexSource( FUDaeGeometryInput::POSITION );
			}
			else
			{
				pGeometrySource = pMesh->AddSource( pInput->GetSemantic() );
			}

			pGeometrySource->SetStride( pInput->GetSource()->GetStride() );

			pDest->AddInput( pGeometrySource, inputIndex );
			
		}
	}

	static void CopyPolygonSources( FCDGeometryPolygons* pDest, FCDGeometryPolygons* pSource )
	{
		size_t offset = 0;

		bool position = false;

		size_t inputCount = pSource->GetInputCount();
		for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
		{
			FCDGeometryPolygonsInput* pInput = pSource->GetInput( inputIndex );

			if ( true != position && FUDaeGeometryInput::POSITION == pInput->GetSemantic() )
			{
				position = true;

				pDest->AddInput( pSource->GetInput( inputIndex )->GetSource(), offset );
				offset++;
			}

			if ( FUDaeGeometryInput::POSITION != pInput->GetSemantic() )
			{
				pDest->AddInput( pSource->GetInput( inputIndex )->GetSource(), offset );
				offset++;
			}
		}
	}

	struct Transform3DCoordPolicy
	{
	public:
		static void Transform( const FMMatrix44* transform, FloatList& floats, size_t start, size_t end )
		{
			for ( size_t i = start; i < end; i += 3 )
			{
				FMVector3 coord ( floats[ i ], floats[ i + 1 ], floats[ i + 2 ] );
				FMVector3 result = transform->TransformCoordinate( coord );

				floats[ i ] = result.x;
				floats[ i + 1 ] = result.y;
				floats[ i + 2 ] = result.z;
			}
		}
	};

	struct NullPolicy
	{
	public:
		static void Transform( const FMMatrix44*, FloatList&, size_t, size_t ) { }
	};

	template < typename TPolicy >
	static void AppendSourceData( FCDGeometrySource* pDestinationSource, const FCDGeometryPolygonsInput* pSourceInput, const FMMatrix44* pTransform )
	{
		const FCDGeometrySource* pSourceSource = pSourceInput->GetSource();

		const float* pSourceData = pSourceSource->GetData();
		FloatList& dest = pDestinationSource->GetSourceData();

		// Use the input to add only the correct data

		size_t start = dest.size();

		size_t sourceCount = pSourceSource->GetDataCount();
		for ( size_t f = 0; f < sourceCount; ++f )
		{
			dest.push_back( pSourceData[ f ] );
		}

		size_t end = dest.size();
		TPolicy::Transform( pTransform, dest, start, end );
	}

	template < typename TPolicy >
	static void AppendSourceData( FCDGeometrySourceList& dest, const FCDGeometryPolygonsInputConstList& source, const FMMatrix44* pTransform )
	{
		size_t sourceCount = dest.size();
		for ( size_t sourceIndex = 0; sourceIndex < sourceCount; ++sourceIndex )
		{
			const FCDGeometryPolygonsInput* pInput = source[ sourceIndex ];
			AppendSourceData< TPolicy >( dest[ sourceIndex ], pInput, pTransform );
		}
	}
}

namespace rage 
{
	RcMesh::RcMesh( FCDGeometryMesh* pMesh, FCDControllerInstance* pControllerInstance )
		:	m_pMesh ( pMesh )
		,	m_pControllerInstance ( pControllerInstance )
		,	m_pGeometryInstance ( NULL )
		,	m_pOwnerPolygons ( NULL )
		,	m_pPositionsSource ( NULL )
		,	m_pNormalsSource ( NULL )
		,	m_pGeoTangentsSource ( NULL )
		,	m_pGeoBiTangentsSource ( NULL )
	{
	}

	RcMesh::RcMesh( FCDGeometryMesh* pMesh, FCDGeometryInstance* pGeometryInstance )
		:	m_pMesh ( pMesh )
		,	m_pControllerInstance( NULL )
		,	m_pGeometryInstance ( pGeometryInstance )
		,	m_pOwnerPolygons ( NULL )
		,	m_pPositionsSource ( NULL )
		,	m_pNormalsSource ( NULL )
		,	m_pGeoTangentsSource ( NULL )
		,	m_pGeoBiTangentsSource ( NULL )
	{
	}

	RcMesh::~RcMesh()
	{
		for ( PolygonGroupList::iterator it = m_PolygonGroupList.begin();
			it != m_PolygonGroupList.end(); ++it )
		{
			PolygonGroup* pGroup = (*it);
			delete pGroup->pVertex;

			delete pGroup;
		}
	}

	bool RcMesh::SourceExists( const FCDGeometrySourceList& sources ) const
	{
		return ( 0 != sources.size() ) || ( 0 == m_PolygonGroupList.size() );			
	}

	// If a source is NULL and there are no polygon groups then the source
	//	can still potentially exist.
	bool RcMesh::SourceExists( const FCDGeometrySource* pSource ) const
	{
		return ( NULL != pSource ) || ( 0 == m_PolygonGroupList.size() );
	}

	void RcMesh::SetSources( FCDGeometryPolygons* pPolygons )
	{
		size_t inputCount = pPolygons->GetInputCount();
		for ( size_t inputIndex = 0; inputIndex < inputCount; ++inputIndex )
		{
			FCDGeometryPolygonsInput* pInput = pPolygons->GetInput( inputIndex );
			FCDGeometrySource* pSource = pInput->GetSource();

			switch ( pInput->GetSemantic() )
			{
			case FUDaeGeometryInput::POSITION:
				m_pPositionsSource = pSource;
				break;
			case FUDaeGeometryInput::NORMAL:
				m_pNormalsSource = pSource;
				break;
			case FUDaeGeometryInput::TEXTANGENT:
				m_TangentsSources.push_back( pSource );
				break;
			case FUDaeGeometryInput::TEXBINORMAL:
				m_BiTangentSources.push_back( pSource );
				break;
			case FUDaeGeometryInput::TEXCOORD:
				m_TexCoordsSources.push_back( pSource );
				break;
			case FUDaeGeometryInput::COLOR:
				m_ColorsSources.push_back( pSource );
				break;
			case FUDaeGeometryInput::GEOTANGENT:
				m_pGeoTangentsSource = pSource;
				break;
			case FUDaeGeometryInput::GEOBINORMAL:
				m_pGeoBiTangentsSource = pSource;
				break;
			default:
				Assert( false );
				break;
			}
		}
	}
	
	void RcMesh::AppendSources( const FCDGeometryPolygons* pPolygons, const FMMatrix44* pTransform )
	{
		if ( SourceExists( m_pPositionsSource ) )
		{
			const FCDGeometryPolygonsInput* pInput = pPolygons->FindInput( FUDaeGeometryInput::POSITION );
			if ( NULL != pInput )
			{
				AppendSourceData< Transform3DCoordPolicy >( m_pPositionsSource, pInput, pTransform );
			}
		}
		if ( SourceExists( m_pNormalsSource ) )
		{
			const FCDGeometryPolygonsInput* pInput = pPolygons->FindInput( FUDaeGeometryInput::NORMAL );
			if ( NULL != pInput )
			{
				AppendSourceData< Transform3DCoordPolicy >( m_pNormalsSource, pInput, pTransform );
			}
		}
		if ( SourceExists( m_TangentsSources ) )
		{
			FCDGeometryPolygonsInputConstList inputs;
			pPolygons->FindInputs( FUDaeGeometryInput::TEXTANGENT, inputs );
			if ( 0 != inputs.size() )
			{
				AppendSourceData< NullPolicy >( m_TangentsSources, inputs, pTransform );
			}
		}
		if ( SourceExists( m_BiTangentSources ) )
		{
			FCDGeometryPolygonsInputConstList inputs;
			pPolygons->FindInputs( FUDaeGeometryInput::TEXBINORMAL, inputs );
			if ( 0 != inputs.size() )
			{
				AppendSourceData< NullPolicy >( m_BiTangentSources, inputs, pTransform );
			}
		}
		if ( SourceExists( m_TexCoordsSources ) )
		{
			FCDGeometryPolygonsInputConstList inputs;
			pPolygons->FindInputs( FUDaeGeometryInput::TEXCOORD, inputs );
			if ( 0 != inputs.size() )
			{
				AppendSourceData< NullPolicy >( m_TexCoordsSources, inputs, pTransform );
			}
		}
		if ( SourceExists( m_ColorsSources ) )
		{
			FCDGeometryPolygonsInputConstList inputs;
			pPolygons->FindInputs( FUDaeGeometryInput::COLOR, inputs );
			if ( 0 != inputs.size() )
			{
				AppendSourceData< NullPolicy >( m_ColorsSources, inputs, pTransform );
			}
		}
		if ( SourceExists( m_pGeoTangentsSource ) )
		{
			const FCDGeometryPolygonsInput* pInput = pPolygons->FindInput( FUDaeGeometryInput::GEOTANGENT );
			if ( NULL != pInput )
			{
				AppendSourceData< NullPolicy >( m_pGeoTangentsSource, pInput, pTransform );
			}
		}
		if ( SourceExists( m_pGeoBiTangentsSource ) )
		{
			const FCDGeometryPolygonsInput* pInput = pPolygons->FindInput( FUDaeGeometryInput::GEOBINORMAL );
			if ( NULL != pInput )
			{
				AppendSourceData< NullPolicy >( m_pGeoBiTangentsSource, pInput, pTransform );
			}
		}
	}

	FCDGeometrySource* RcMesh::AddSource( FUDaeGeometryInput::Semantic semantic, const char* name )
	{
		struct FindByName
		{
			FindByName( const char* name )
				:	name ( name ) { }

			bool operator() ( const FCDGeometrySource* pSource )
			{
				return pSource->GetName() == name;
			}

			fstring name;
		};

		FCDGeometrySource* pSource = NULL;

		switch ( semantic )
		{
			case FUDaeGeometryInput::POSITION:
				{
					if ( !m_pPositionsSource )
					{
						pSource = m_pMesh->AddVertexSource( semantic );
						m_pPositionsSource = pSource;
					}
				}
				break;
			case FUDaeGeometryInput::NORMAL:
				{
					if ( !m_pNormalsSource )
					{
						pSource = m_pMesh->AddSource( semantic );
						m_pNormalsSource = pSource;
					}
				}
				break;
			case FUDaeGeometryInput::TEXTANGENT:
				{
					if ( m_TangentsSources.end() ==
						find_if( m_TangentsSources.begin(), m_TangentsSources.end(), FindByName( name ) ) )
					{
						pSource = m_pMesh->AddSource( semantic );
						m_TangentsSources.push_back( pSource );
					}
				}
				break;
			case FUDaeGeometryInput::TEXBINORMAL:
				{
					if ( m_BiTangentSources.end() ==
						find_if( m_BiTangentSources.begin(), m_BiTangentSources.end(), FindByName( name ) ) )
					{
						pSource = m_pMesh->AddSource( semantic );
						m_BiTangentSources.push_back( pSource );
					}
				}
				break;
			case FUDaeGeometryInput::TEXCOORD:
				{
					if ( m_TexCoordsSources.end() ==
						find_if( m_TexCoordsSources.begin(), m_TexCoordsSources.end(), FindByName( name ) ) )
					{
						pSource = m_pMesh->AddSource( semantic );
						m_TexCoordsSources.push_back( pSource );
					}
				}
				break;
			case FUDaeGeometryInput::COLOR:
				{
					if ( m_ColorsSources.end() ==
						find_if( m_ColorsSources.begin(), m_ColorsSources.end(), FindByName( name ) ) )
					{
						pSource = m_pMesh->AddSource( semantic );
						m_ColorsSources.push_back( pSource );
					}
				}
				break;
			case FUDaeGeometryInput::GEOTANGENT:
				{
					if ( !m_pGeoTangentsSource )
					{
						pSource = m_pMesh->AddSource( semantic );
						m_pGeoTangentsSource = pSource;
					}
				}
				break;
			case FUDaeGeometryInput::GEOBINORMAL:
				{
					if ( !m_pGeoBiTangentsSource )
					{
						pSource = m_pMesh->AddSource( semantic );
						m_pGeoBiTangentsSource = pSource;
					}
				}
				break;
			default:
				Assert( false );
				break;
		}

		if ( NULL != pSource )
		{
			pSource->SetName( name );
		}

		return pSource;
	}

	struct FindMaterialFunctor
	{
		FindMaterialFunctor( const FCDMaterial* pMaterial )
			:	pMaterial ( pMaterial ) { }

		bool operator() ( const RcMesh::PolygonGroup* cmp )
		{
			if ( cmp->pMaterial->GetMaterial() == pMaterial )
			{
				return true;
			}

			return false;
		}

		const FCDMaterial* pMaterial;
	};

	void RcMesh::Append( const FCDGeometryPolygons* pPolygons, const FCDSkinController* pSkinController, const FCDMaterial* pMaterial, const FMMatrix44* pTransform )
	{
		FindMaterialFunctor findFtor ( pMaterial );
		PolygonGroupList::iterator found =
			find_if( m_PolygonGroupList.begin(), m_PolygonGroupList.end(), findFtor );

		if ( m_PolygonGroupList.end() != found )
		{
			RcVertex* pVertex = ( *found )->pVertex;
			pVertex->Append( pPolygons, pSkinController, pTransform, this );
		}
		else
		{
			PolygonGroup* pPolyGroup = new PolygonGroup;
			m_PolygonGroupList.push_back( pPolyGroup );

			pPolyGroup->pMaterial = 
				m_pControllerInstance->AddMaterialInstance( const_cast< FCDMaterial* >( pMaterial ), pPolygons->GetMaterialSemantic() );

			FCDGeometryPolygons* pNewPolygons = m_pMesh->AddPolygons();
			pNewPolygons->SetMaterialSemantic( pPolygons->GetMaterialSemantic() );

			pPolyGroup->pVertex = new RcVertex( pNewPolygons );
			pPolyGroup->pVertex->Append( pPolygons, pSkinController, pTransform, this );
		}

	}

	void RcMesh::Append( const FCDGeometryPolygons* pPolygons, const FCDMaterial* pMaterial, const FMMatrix44* pTransform )
	{
		// if there is already a polygon group with this material, we're sweet
		//	otherwise we create another polygon group and keep rollin'
		FindMaterialFunctor findFtor ( pMaterial );
		PolygonGroupList::iterator found = 
			find_if( m_PolygonGroupList.begin(), m_PolygonGroupList.end(), findFtor );

		if ( m_PolygonGroupList.end() != found )
		{
			RcVertex* pVertex = ( *found )->pVertex;
			pVertex->Append( pPolygons, pTransform, this );
		}
		else
		{
			PolygonGroup* pPolyGroup = new PolygonGroup;
			m_PolygonGroupList.push_back( pPolyGroup );
				
			pPolyGroup->pMaterial = 
				m_pGeometryInstance->AddMaterialInstance( const_cast< FCDMaterial* >( pMaterial ), pPolygons->GetMaterialSemantic() );
			
			FCDGeometryPolygons* pNewPolygons = m_pMesh->AddPolygons();
			pNewPolygons->SetMaterialSemantic( pPolygons->GetMaterialSemantic() );
			/*
			if ( !m_pOwnerPolygons )
			{
				CreatePolygonSources( pNewPolygons, pPolygons );
				m_pOwnerPolygons = pNewPolygons;
				SetSources( m_pOwnerPolygons );
			}
			else
			{
				CopyPolygonSources( pNewPolygons, m_pOwnerPolygons );
			}
			*/
			
			pPolyGroup->pVertex = new RcVertex( pNewPolygons );
			pPolyGroup->pVertex->Append( pPolygons, pTransform, this );
		}
	}

	struct FindByGeometrySourceName
	{
		FindByGeometrySourceName( const char* name )
			:	name ( name ) { }

		bool operator() ( const FCDGeometrySource* pSource )
		{
			return pSource->GetName() == name;
		}

		fstring name;
	};

	FCDGeometrySource* RcMesh::FindTangentsSourceWithName( const char* name )
	{
		FCDGeometrySourceList::iterator it = 
			find_if ( m_TangentsSources.begin(), m_TangentsSources.end(), FindByGeometrySourceName( name ) );
		if ( it != m_TangentsSources.end() )
		{
			return (*it );
		}

		return NULL;
	}

	FCDGeometrySource* RcMesh::FindBiTangentsSourceWithName( const char* name )
	{
		FCDGeometrySourceList::iterator it =
			find_if( m_BiTangentSources.begin(), m_BiTangentSources.end(), FindByGeometrySourceName( name ) );
		if ( it != m_BiTangentSources.end() )
		{
			return ( *it );
		}

		return NULL;
	}

	FCDGeometrySource* RcMesh::FindTexCoordsSourceWithName( const char* name )
	{
		FCDGeometrySourceList::iterator it =
			find_if( m_TexCoordsSources.begin(), m_TexCoordsSources.end(), FindByGeometrySourceName( name ) );
		if ( it != m_TexCoordsSources.end() )
		{
			return ( *it );
		}

		return NULL;
	}

	FCDGeometrySource* RcMesh::FindColorsSourceWithName( const char* name )
	{
		FCDGeometrySourceList::iterator it = 
			find_if( m_ColorsSources.begin(), m_ColorsSources.end(), FindByGeometrySourceName( name ) );
		if ( it != m_ColorsSources.end() )
		{
			return ( *it );
		}

		return NULL;
	}

	typedef fm::vector< FCDSkinControllerJoint > FCDSkinControllerJointList;

	struct FindByJointId
	{
		FindByJointId( const fm::string id )
			: id ( id ) { }

		bool operator() ( const FCDSkinControllerJoint& joint )
		{
			return joint.GetId() == id;
		}

		fm::string id;
	};

	struct AddJointFunctor
	{
		AddJointFunctor( FCDSkinController* pSkinController )
			:	pSkinController ( pSkinController ) { }

		void operator() ( const FCDSkinControllerJoint& append )
		{
			FCDSkinControllerJointList& joints = pSkinController->GetJointsList();
			FCDSkinControllerJointList::iterator it = 
				find_if( joints.begin(), joints.end(), FindByJointId( append.GetId() ) );
			if ( joints.end() == it )
			{
				pSkinController->AddJoint( append.GetId(), append.GetBindPoseInverse() );
			}
		}

		FCDSkinController* pSkinController;
	};

	typedef fm::vector< FCDSkinControllerVertex > FCDSkinControllerVertexList;

	#define IS_INVALID_INDEX( _idx ) ( _idx == -1 )
	static const int INVALID_INDEX = -1;

	static int FindNewJointIndex( size_t jointIndex , const FCDSkinController* pOldSkin, const FCDSkinController* pNewSkin )
	{
		const FCDSkinControllerJoint* pJoint = pOldSkin->GetJoint( jointIndex );
		fstring id = pJoint->GetId();

		int jointCount = int( pNewSkin->GetJointCount() );
		for ( int jointIndex = 0; jointIndex < jointCount; ++jointIndex )
		{
			const FCDSkinControllerJoint* pNewJoint = pNewSkin->GetJoint( jointIndex );
			if ( id == pNewJoint->GetId() )
			{
				return jointIndex;
			}
		}

		return INVALID_INDEX;
	}

	struct AddInfluenceFunctor
	{
		AddInfluenceFunctor( FCDSkinController* pSkinController, const FCDSkinController* pOldSkinController )
			:	pOldSkinController ( pOldSkinController )	
			,	pSkinController ( pSkinController ) { } 


		void operator() ( const FCDSkinControllerVertex& vertex )
		{
			FCDSkinControllerVertex scv;

			size_t pairCount = vertex.GetPairCount();
			Assert( pairCount < mshMaxMatricesPerVertex );

			for ( size_t pairIndex = 0; pairIndex < pairCount; ++pairIndex )
			{
				const FCDJointWeightPair* pJointWeightPair = vertex.GetPair( pairIndex );
				int32 jointIndex = FindNewJointIndex( pJointWeightPair->jointIndex, pOldSkinController, pSkinController );
				if ( IS_INVALID_INDEX( jointIndex ) )
				{
					Errorf( "failed to find joint with index %d", pJointWeightPair->jointIndex );
				}

				scv.AddPair( jointIndex, pJointWeightPair->weight );
			}

			pSkinController->GetInfluenceList().push_back( scv );
		}

		const FCDSkinController* pOldSkinController;
		FCDSkinController* pSkinController;
	};

	void RcMesh::Append( const FCDSkinController* pSkinController )
	{
		FCDController* pController = 
			static_cast< FCDController* >( m_pControllerInstance->GetEntity() );

		FCDSkinControllerJointList& joints = 
			const_cast< FCDSkinController* >( pSkinController )->GetJointsList();

		FCDSkinController* pSkin = pController->GetSkinController();

		AddJointFunctor addJoints ( pSkin );
		for_each( joints.begin(), joints.end(), addJoints ); 

	}


} // rage

