#ifndef __RC_MESH_TOOLS_H__
#define __RC_MESH_TOOLS_H__
#pragma once

#include "FCDocument/FCDGeometryPolygonsTools.h"

class FCDGeometryMesh;
class FCDGeometryPolygons;

namespace rage
{
	// NOTE: this is a copy from FCollada, but we add some extra data
	//	so we can identify that the geometry has been processed.
	void GenerateUniqueIndices( FCDGeometryMesh* mesh, FCDGeometryPolygons* polygonsToProcess = NULL, FCDGeometryIndexTranslationMap* translationMap = NULL );

}; // rage

#endif // __RC_MESH_TOOLS_H__
