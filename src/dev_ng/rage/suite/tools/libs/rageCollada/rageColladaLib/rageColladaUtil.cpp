#include "rageColladaUtil.h"

#include "..\..\..\..\3rdParty\MiniZip\zip.h"
#include "..\..\..\..\3rdParty\MiniZip\unzip.h"
#include "..\..\..\..\3rdParty\MiniZip\iowin32.h"

#include "mgc.h"

#include <list>
#include <stack>

namespace
{
	static const float IDENTITY[] = { 1, 0, 0, 0, 0, 1, 0 ,0 ,0, 0, 1, 0, 0, 0, 0, 1 };
}

namespace rage {
	//-----------------------------------------------------------------------------

	void ToMatrix44( Matrix44& out, const FMMatrix44& in )
	{
		out.a[ 0 ] = in[ 0 ][ 0 ];
		out.a[ 1 ] = in[ 0 ][ 1 ];
		out.a[ 2 ] = in[ 0 ][ 2 ];
		out.a[ 3 ] = in[ 0 ][ 3 ];

		out.b[ 0 ] = in[ 1 ][ 0 ];
		out.b[ 1 ] = in[ 1 ][ 1 ];
		out.b[ 2 ] = in[ 1 ][ 2 ];
		out.b[ 3 ] = in[ 1 ][ 3 ];

		out.c[ 0 ] = in[ 2 ][ 0 ];
		out.c[ 1 ] = in[ 2 ][ 1 ];
		out.c[ 2 ] = in[ 2 ][ 2 ];
		out.c[ 3 ] = in[ 2 ][ 3 ];

		out.d[ 0 ] = in[ 3 ][ 0 ];
		out.d[ 1 ] = in[ 3 ][ 1 ];
		out.d[ 2 ] = in[ 3 ][ 2 ];
		out.d[ 3 ] = in[ 3 ][ 3 ];
	}

	//-----------------------------------------------------------------------------

	void ToMatrix34( Matrix34& out, const FMMatrix44& in )
	{
		out.a[ 0 ] = in[ 0 ][ 0 ];
		out.a[ 1 ] = in[ 0 ][ 1 ];
		out.a[ 2 ] = in[ 0 ][ 2 ];

		out.b[ 0 ] = in[ 1 ][ 0 ];
		out.b[ 1 ] = in[ 1 ][ 1 ];
		out.b[ 2 ] = in[ 1 ][ 2 ];

		out.c[ 0 ] = in[ 2 ][ 0 ];
		out.c[ 1 ] = in[ 2 ][ 1 ];
		out.c[ 2 ] = in[ 2 ][ 2 ];

		out.d[ 0 ] = in[ 3 ][ 0 ];
		out.d[ 1 ] = in[ 3 ][ 1 ];
		out.d[ 2 ] = in[ 3 ][ 2 ];
	}

	//-----------------------------------------------------------------------------

	void ComputeBoundingSphere( const FCDGeometryList& geometryList, Vector3& center, float& radius )
	{
		center.Set( 0.0f, 0.0f, 0.0f );
		radius = 0.0f; 

		std::vector< Mgc::Vector3 > points;

		size_t geometryCount = geometryList.size();
		for ( size_t geometryIndex = 0; geometryIndex < geometryCount; ++geometryIndex )
		{
			const FCDGeometry* geometry = geometryList[ geometryIndex ];
			const FCDGeometryMesh* mesh = geometry->GetMesh();

			size_t polygonsCount = mesh->GetPolygonsCount();
			for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
			{
				const FCDGeometryPolygons* polygons = mesh->GetPolygons( polygonsIndex );
				const FCDGeometryPolygonsInput* input = 
					polygons->FindInput( FUDaeGeometryInput::POSITION );
				const FCDGeometrySource* source = input->GetSource();

				const float* positions = source->GetData();
				const size_t positionsCount = source->GetDataCount();

				for ( size_t positionsIndex = 0; positionsIndex < positionsCount; positionsIndex += 3 )
				{
					Mgc::Vector3 p ( 
						positions[ positionsIndex ],
						positions[ positionsIndex + 1 ],
						positions[ positionsIndex + 2 ] );

					points.push_back( p );
				}
			}
		}

		Mgc::Sphere sphere = Mgc::MinSphere( int( points.size() ), &points[ 0 ] );

		center.Set( 
			float( sphere.Center().x ),
			float( sphere.Center().y ),
			float( sphere.Center().z ) );
		radius = float( sphere.Radius() );
	}

	//-----------------------------------------------------------------------------

	void ComputeBoundingBox( const FCDGeometryList& geometryList, Vector3& min, Vector3& max )
	{
		min.Set(  LARGE_FLOAT,  LARGE_FLOAT,  LARGE_FLOAT );
		max.Set( -LARGE_FLOAT, -LARGE_FLOAT, -LARGE_FLOAT );

		size_t geometryCount = geometryList.size();
		for ( size_t geometryIndex = 0; geometryIndex < geometryCount; ++geometryIndex )
		{
			const FCDGeometry* geometry = geometryList[ geometryIndex ];
			const FCDGeometryMesh* mesh = geometry->GetMesh();

			size_t polygonsCount = mesh->GetPolygonsCount();
			for ( size_t polygonsIndex = 0; polygonsIndex < polygonsCount; ++polygonsIndex )
			{
				const FCDGeometryPolygons* polygons = mesh->GetPolygons( polygonsIndex );
				const FCDGeometryPolygonsInput* input = 
					polygons->FindInput( FUDaeGeometryInput::POSITION );
				const FCDGeometrySource* source = input->GetSource();

				const float* positions = source->GetData();
				const size_t positionsCount = source->GetDataCount();
				for ( size_t positionsIndex = 0; positionsIndex < positionsCount; positionsIndex += 3 )
				{
					min.x = rage::Min( min.x, positions[ positionsIndex ] );
					max.x = rage::Max( max.x, positions[ positionsIndex ] );
					min.y = rage::Min( min.y, positions[ positionsIndex + 1 ] );
					max.y = rage::Max( max.y, positions[ positionsIndex + 1 ] );
					min.z = rage::Min( min.z, positions[ positionsIndex + 2 ] );
					max.z = rage::Max( max.z, positions[ positionsIndex + 2 ] );
				}
			}
		}
	}

	//-----------------------------------------------------------------------------

	std::string GetShaderMaterial( const FCDMaterial* material )
	{
		const FCDEffect* effect = material->GetEffect();

		const FCDETechnique* rageTechnique = effect->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
		if ( NULL != rageTechnique )
		{
			const FCDENode* node = rageTechnique->FindParameter("rageShaderMaterial");
			if ( NULL != node )
			{
				const FCDEAttribute* path = node->FindAttribute( "path" );

				std::string filename ( path->value.c_str() );
				filename = filename.replace( 0, strlen( "file:///" ), "" );

				return filename;
			}
		}

		return NULL;
	}

	//-----------------------------------------------------------------------------

	bool IsShaderMaterial( const FCDMaterial* material )
	{
		const FCDEffect* effect = material->GetEffect();

		const FCDETechnique* rageTechnique = effect->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
		if ( NULL == rageTechnique )
		{
			return false;
		}

		const FCDENode* node = rageTechnique->FindParameter("rageShaderMaterial");
		if ( NULL == node )
		{
			return false;
		}

		return true;
	}

	//-----------------------------------------------------------------------------

	void GetLodThreshold( const FCDSceneNode* sceneNode, rage::RAGEThresholdList& thresholds )
	{
		const FCDETechnique* rageTechnique = sceneNode->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
		if ( NULL != rageTechnique )
		{
			size_t nodeCount = rageTechnique->GetChildNodeCount();
			for ( size_t nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex )
			{
				const FCDENode* node = rageTechnique->GetChildNode( nodeIndex );
				const char* name = node->GetName();
				Assert( 0 == strcmp( name, "rageLod" ) );

				const FCDEAttribute* threshold = node->FindAttribute( "threshold" );
				Assert( NULL != threshold );

				thresholds.push_back( float( atof( threshold->value.c_str() ) ) );
			}
		}
	}

	//-----------------------------------------------------------------------------

	bool IsLodThreshold( const FCDSceneNode* sceneNode )
	{
		const FCDETechnique* rageTechnique = sceneNode->GetExtra()->GetDefaultType()->FindTechnique( "RAGE" );
		if ( NULL == rageTechnique )
		{
			return false;
		}

		const FCDENode* node = rageTechnique->FindParameter( "rageLod" );
		if ( NULL == node )
		{
			return false;
		}

		return true;
	}

	//-----------------------------------------------------------------------------

	void FindReferencedMaterials( FCDMaterialList& materials, const FCDGeometryInstance *pInstance )
	{
		size_t materialCount = pInstance->GetMaterialInstanceCount();
		for ( size_t materialIndex = 0; materialIndex < materialCount; ++materialIndex )
		{
			const FCDMaterialInstance* material = pInstance->GetMaterialInstance( materialIndex );
			const FCDMaterial* target = material->GetMaterial();

			materials.push_back( std::make_pair( material->GetSemantic().c_str(), target ) );
		}
	}

	//-----------------------------------------------------------------------------

	void FindReferencedMaterials( FCDMaterialList& materials, const FCDSceneNode *pNode )
	{
		size_t instanceCount = pNode->GetInstanceCount();
		for ( size_t instanceIndex = 0; instanceIndex < instanceCount; ++instanceIndex )
		{
			const FCDGeometryInstance* pGeomInstance = dynamic_cast<const FCDGeometryInstance*>(pNode->GetInstance(instanceIndex));
			if(pGeomInstance)
			{
				FindReferencedMaterials( materials, pGeomInstance );
			}
		}
	}

	//-----------------------------------------------------------------------------
	// PURPOSE
	//	Function to compute the world transform for a node, ignoring any rotations.
	// PARAMS
	//	pSceneNode - Pointer to input node to compute transform for.
	// RETURN
	//	Returns the world transform, minus any rotations, for pSceneNode.
	FMMatrix44 CalculateWorldMatrixNoRotation( const FCDSceneNode* pSceneNode )
	{
		FMMatrix44 localTransform ( IDENTITY );

		// Calculate the local transform for this node (ignoring rotations)
		const FCDTransformContainer& transforms = pSceneNode->GetTransforms();
		for ( FCDTransformContainer::const_iterator it = transforms.begin(); it != transforms.end(); ++it )
		{
			if ( (*it)->GetType() != FCDTransform::ROTATION )
			{
				localTransform = localTransform * (*it)->ToMatrix();
			}
		}

		// Recursively calculate the world transform (ignoring rotations) for this
		// node's parent, multiplying it with the node's local transform
		const FCDSceneNode* pParentNode = pSceneNode->GetParent();
		if ( pParentNode )
		{
			return ( CalculateWorldMatrixNoRotation(pParentNode) * localTransform );
		}
		else
		{
			return localTransform;
		}
	}

	//-----------------------------------------------------------------------------
	// PURPOSE
	//	Function to compute the world transform for a joint
	// PARAMS
	//	pJointNode - Pointer to input joint to compute transform for
	//  outWorldMatrix - Output reference to resulting world transform matrix
	void GetJointWorldMatrix( const FCDSceneNode* pJointNode, rage::Matrix34& outWorldMatrix, bool bIncludeJointOrients)
	{
		outWorldMatrix.Identity();
		
		//Get a list of all the local-space matrices for the joint, without scale orients or joint orients
		std::list< FMMatrix44 > localMats;
		
		const FCDSceneNode* pCurrentJointNode = pJointNode;
		while(pCurrentJointNode)
		{
			const FCDTransformContainer& nodeTransforms = pCurrentJointNode->GetTransforms();
			
			FMMatrix44 localMat(IDENTITY);
			
			FMMatrix44 localScaleMtx(IDENTITY);
			FMMatrix44 localRotationMtx(IDENTITY);
			//FMMatrix44 localInverseParentScaleMtx(IDENTITY);
			FMMatrix44 localTransMtx(IDENTITY);

			for (FCDTransformContainer::const_iterator it = nodeTransforms.begin(); it != nodeTransforms.end(); ++it)
			{
				const FCDTransform* pTransform = (*it);
				if(pTransform->GetType() == FCDTransform::SCALE)
				{
					localScaleMtx = pTransform->ToMatrix();
					localMat = localMat * localScaleMtx;
				}
				else if(pTransform->GetType() == FCDTransform::ROTATION)
				{
					const fm::string& rotSubId = pTransform->GetSubId();
					if( bIncludeJointOrients || rotSubId[0] == 'r')
					{
						localRotationMtx = localRotationMtx * pTransform->ToMatrix();
					}
					localMat = localMat * localRotationMtx;
				}
				else if(pTransform->GetType() == FCDTransform::TRANSLATION)
				{
					localTransMtx = pTransform->ToMatrix();
					localMat = localMat * localTransMtx;
				}
			}

			//Get the inverse parent scale matrix
			/*const FCDSceneNode* pParentJointNode = pCurrentJointNode->GetParent();
			if(pParentJointNode)
			{
				const FCDTransformContainer& parentNodeTransforms = pParentJointNode->GetTransforms();
				for(FCDTransformContainer::const_iterator it2 = parentNodeTransforms.begin(); it2 != parentNodeTransforms.end(); ++it2)
				{
					if((*it2)->GetType() == FCDTransform::SCALE)
					{
						localInverseParentScaleMtx = (*it2)->ToMatrix();
						break;
					}
				}
				localInverseParentScaleMtx.Inverted();
			}*/

			localMats.push_front(localMat);

			pCurrentJointNode = pCurrentJointNode->GetParent();
		}

		FMMatrix44 worldMtx(IDENTITY);

		if(localMats.size() == 1)
		{
			worldMtx = localMats.front();
		}
		else
		{
			FMMatrix44 A,B;
			A = localMats.front();
			localMats.pop_front();
			B = localMats.front();
			localMats.pop_front();

			worldMtx = B * A;
			while(localMats.size())
			{
				A = localMats.front();
				localMats.pop_front();
				worldMtx = A * worldMtx;
			}
		}

		ToMatrix34(outWorldMatrix, worldMtx);
	}

	//-----------------------------------------------------------------------------
	//PURPOSE
	//  Function that will return a list of the skin joint influencers in the order they are arranged in the scene graph
	//  if it was walked using a depth first traversal.  
	//PARAMS
	//  pSkin - Pointer to source FCollada skin controller
	//  outOrderedJointNodeArray - Output reference to a vector which when the function completes will be filled with FCDSceneNode pointers
	//							   listed in the order they are discovered when the scene graph is traversed using a depth first traversal
	//  outJointIndexMap - Output reference to a map which maps the FCollada scene joint index to the rage bone index
	void GetRageJointOrdering(const FCDSkinController* pSkin, 
									std::vector<const FCDSceneNode*>& outOrderedJointNodeArray,
									std::map<size_t, size_t>& outJointIndexMap)
	{
		const FCDocument* pDoc = pSkin->GetDocument();
		const FCDSceneNode* pVsRoot = pDoc->GetVisualSceneInstance();

		outOrderedJointNodeArray.clear();
		outJointIndexMap.clear();

		std::vector< const FCDSceneNode* > skinJointNodeArray;
		
		size_t numJoints = pSkin->GetJointCount();
		
		skinJointNodeArray.reserve(numJoints);
		outOrderedJointNodeArray.reserve(numJoints);

		for(size_t jointIdx = 0; jointIdx < numJoints; ++jointIdx)
		{
			const fm::string& jointSubId = pSkin->GetJoint(jointIdx)->GetId();
			const FCDEntity* pJointEntity = pVsRoot->FindSubId(jointSubId);
			Assertf(pJointEntity, "Joint with subId of '%s' is referenced by a skin controller, but could not be found in the scene", jointSubId.c_str());
			const FCDSceneNode* pJointNode = dynamic_cast<const FCDSceneNode*>(pJointEntity);
			Assertf(pJointNode, "Dynamic case of FCDEntity '%s' to FCDSceneNode failed", pJointEntity->GetName().c_str());
			skinJointNodeArray.push_back(pJointNode);
		}

		int rageBoneIndex = 0;

		std::stack<const FCDSceneNode*> dftNodeStack;
		dftNodeStack.push(pVsRoot);
		while(!dftNodeStack.empty())
		{
			const FCDSceneNode *pNode = dftNodeStack.top();
			dftNodeStack.pop();

			if ( pNode->IsJoint() && (strcmpi(pNode->GetName().c_str(), "_null") != 0) )
			{
				for(size_t jointIdx=0; jointIdx < skinJointNodeArray.size(); ++jointIdx)
				{
					if( skinJointNodeArray[jointIdx] == pNode )
					{
						Assertf( outJointIndexMap.find(jointIdx) == outJointIndexMap.end(), "Joint '%s', with an index of %d, is already in the bone index map",
							pNode->GetName().c_str(), jointIdx);
						outJointIndexMap[jointIdx] = rageBoneIndex;
					}
				}

				++rageBoneIndex;
				outOrderedJointNodeArray.push_back(pNode);
			}
			
			size_t numChildren = pNode->GetChildrenCount();
			for(size_t childIdx = numChildren; childIdx > 0; childIdx--)
			{
				dftNodeStack.push(pNode->GetChild(childIdx-1));
			}
		}
	}

	//-----------------------------------------------------------------------------
	//PURPOSE
	//  Function that will return a map of each of the skin's joint indices to its
	//	scene node.
	//PARAMS
	//  pSkinController- Pointer to source FCollada skin controller.
	//  outJointIndexToJointNodeMap - Reference to the output map.
	void GetJointIndexToJointNodeMap(	const FCDSkinController* pSkinController,
										std::map<size_t, const FCDSceneNode*>& outJointIndexToJointNodeMap )
	{
		AssertMsg( pSkinController, "Do not pass a NULL skin controller pointer to GetJointIndexToJointNodeMap()." );

		const FCDocument* pDocument = pSkinController->GetDocument();
		AssertMsg( pDocument, "Could not get document from skin controller." );
		const FCDSceneNode* pVisualScene = pDocument->GetVisualSceneInstance();
		AssertMsg( pVisualScene, "Could not get visual scene instance from document." );

		size_t jointCount = pSkinController->GetJointCount();
		for ( size_t jointIndex = 0; jointIndex < jointCount; ++jointIndex )
		{
			const FCDSkinControllerJoint* pJoint = pSkinController->GetJoint( jointIndex );
			const FCDEntity* pJointEntity = pVisualScene->FindSubId( pJoint->GetId() );
			Assertf( pJointEntity, "Could not find matching node for joint index '%d'.", jointIndex);
			Assertf( pJointEntity->GetType() == FCDEntity::SCENE_NODE, "The entity that matches the subId for joint index '%d' is not a scene node.", jointIndex);
			const FCDSceneNode* pJointNode = static_cast<const FCDSceneNode*>( pJointEntity );

			outJointIndexToJointNodeMap[jointIndex] = pJointNode;
		}
	}

	//-----------------------------------------------------------------------------
	//PURPOSE
	//  Function that will return true if this visual scene has been tagged with
	//	the specified conditioner name, false otherwise.
	//PARAMS
	//  pVsNode - Visual scene instance node to check.
	//  name - The name of the conditioner tag we're looking for.
	//RETURN
	//	True if the visual scene instance has been tagged with the specified
	//	conditioner name, false otherwise.
	//NOTES
	//	This function should probably return the tagged conditioner information.
	bool GetConditionerTag( const FCDSceneNode* pVsNode, const char* name )
	{
		AssertMsg( pVsNode, "Do not pass NULL scene node pointer to GetConditionerTag()." );

		const FCDExtra* pExtra = pVsNode->GetExtra();
		if ( pExtra )
		{
			const FCDETechnique* pRageTechnique = pExtra->GetDefaultType()->FindTechnique( DAE_RAGE_PROFILE );
			if ( pRageTechnique )
			{
				const FCDENode* pConditioningNode = pRageTechnique->FindChildNode( DAE_RAGE_CONDITIONER_TAGS_ELEMENT );
				if ( pConditioningNode )
				{
					FCDENodeList conditionerNodes;
					pConditioningNode->FindChildrenNodes( DAE_RAGE_CONDITIONER_ELEMENT, conditionerNodes );

					// See if a conditioner node exists by matching the name attribute
					const FCDENode* pConditionerNode = NULL;
					for ( size_t i = 0; i < conditionerNodes.size(); ++i )
					{
						const FCDEAttribute* pNameAttribute = conditionerNodes[i]->FindAttribute( DAE_RAGE_CONDITIONER_NAME_ATTRIBUTE );
						if ( pNameAttribute && (stricmp(pNameAttribute->value.c_str(), name) == 0) )
						{
							pConditionerNode = conditionerNodes[i];
							break;
						}
					}

					if ( pConditionerNode )
					{
						// TODO: This should probably return the conditioner's attributes
						return true;
					}
				}
			}
		}

		return false;
	}

	//-----------------------------------------------------------------------------
	//PURPOSE
	//  Function that tag a visual scene instance with the specified conditioner
	//	information.
	//PARAMS
	//  pVsNode - Visual scene instance node to tag.
	//  name - The name of the conditioner.
	//	version - The version of the conditioner.
	void SetConditionerTag( FCDSceneNode* pVsNode, const char* name, const char* version )
	{
		AssertMsg( pVsNode, "Do not pass NULL scene node pointer to SetConditionerTag()." );

		FCDExtra* pExtra = pVsNode->GetExtra();
		AssertMsg( pExtra, ("Could not get extra node for scene node '%s'", pVsNode->GetName().c_str()) );

		FCDETechnique* pRageTechnique = pExtra->GetDefaultType()->FindTechnique( DAE_RAGE_PROFILE );
		if ( pRageTechnique == NULL )
		{
			pRageTechnique = pExtra->GetDefaultType()->AddTechnique( DAE_RAGE_PROFILE );
			AssertMsg( pRageTechnique, "Failed to add 'RAGE' technique to extra node." );
		}

		FCDENode* pConditioningNode = pRageTechnique->FindChildNode( DAE_RAGE_CONDITIONER_TAGS_ELEMENT );
		if ( pConditioningNode == NULL )
		{
			pConditioningNode = pRageTechnique->AddChildNode( DAE_RAGE_CONDITIONER_TAGS_ELEMENT );
			AssertMsg( pConditioningNode, "Failed to add 'conditioning' node to technique node." );
		}

		FCDENodeList conditionerNodes;
		pConditioningNode->FindChildrenNodes( DAE_RAGE_CONDITIONER_ELEMENT, conditionerNodes );

		// See if a conditioner node already exists by matching the name attribute
		FCDENode* pConditionerNode = NULL;
		for ( size_t i = 0; i < conditionerNodes.size(); ++i )
		{
			FCDEAttribute* pNameAttribute = conditionerNodes[i]->FindAttribute( DAE_RAGE_CONDITIONER_NAME_ATTRIBUTE );
			if ( pNameAttribute && (stricmp(pNameAttribute->value.c_str(), name) == 0) )
			{
				pConditionerNode = conditionerNodes[i];
				break;
			}
		}

		// If no matching conditioner node was found, create one
		if ( pConditionerNode == NULL )
		{
			pConditionerNode = pConditioningNode->AddChildNode( DAE_RAGE_CONDITIONER_ELEMENT );
			AssertMsg( pConditionerNode, "Failed to add conditioner node to conditioning node." );
		}

		// Set the conditioner node's attributes
		pConditionerNode->AddAttribute( DAE_RAGE_CONDITIONER_NAME_ATTRIBUTE, name );
		pConditionerNode->AddAttribute( DAE_RAGE_CONDITIONER_VERSION_ATTRIBUTE, version );
	}


	bool RageCollada::LoadDocument(FCDocument* pDocument, const char* filename)
	{
		// Does the filename end with dae?
		const char* pcFileExt = strrchr(filename, '.');
		if(stricmp(pcFileExt, ".dae") == 0)
		{
			// Given a dae, so just load normally
			return FCollada::LoadDocument( pDocument, filename );
		}
		else if(stricmp(pcFileExt, ".zip") == 0)
		{
			// Given a zip file, so do some magic first
			// Open the zip file
			int err = 0;
			zlib_filefunc_def ffunc;
			fill_win32_filefunc(&ffunc);
			unzFile pobZipFile = unzOpen(filename);
			if(!pobZipFile)
			{
				Errorf("Error occurred opening %s", filename);
				return false;
			}

			// Find the first dae in the file
			err = unzGoToFirstFile(pobZipFile);
			if (err != ZIP_OK)
			{
				Errorf("Error occurred locating any files in zipfile %s\n", filename);
				return false;
			}

			// Get file info
			unz_file_info pfile_info;
			char pcFilenameInZip[512];
			err = unzGetCurrentFileInfo(pobZipFile, 
									 &pfile_info,
									 pcFilenameInZip, 512,
									 NULL, 0,
									 NULL, 0);
			if (err != ZIP_OK)
			{
				Errorf("Error occurred getting info about a file in zipfile %s\n",filename);
				return false;
			}

			while(stricmp(strrchr(pcFilenameInZip, '.'), ".dae") != 0)
			{
				// Not a dae, so...
				unzCloseCurrentFile(pobZipFile);

				// ...try next file
				err = unzGoToNextFile(pobZipFile);
				if (err != ZIP_OK)
				{
					Errorf("Error occurred locating any dae files in zipfile %s\n", filename);
					return false;
				}

				// Get file info
				unz_file_info pfile_info;
				char pcFilenameInZip[512];
				err = unzGetCurrentFileInfo(pobZipFile, 
										 &pfile_info,
										 pcFilenameInZip, 512,
										 NULL, 0,
										 NULL, 0);
				if (err != ZIP_OK)
				{
					Errorf("Error occurred getting info about a file in zipfile %s\n",filename);
					return false;
				}
			}

			// Read data from file
			err = unzOpenCurrentFile(pobZipFile);
			if (err != ZIP_OK)
			{
				Errorf("Error occurred opening %s in zipfile %s\n",pcFilenameInZip, filename);
				return false;
			}

			char* pcData = new char[pfile_info.uncompressed_size];
			int iBytesCopied = unzReadCurrentFile(pobZipFile, pcData, pfile_info.uncompressed_size);
			if(iBytesCopied < 0)
			{
				Errorf("Error occurred reading %s in zipfile %s\n",pcFilenameInZip, filename);
				return false;
			}

			unzCloseCurrentFile(pobZipFile);
			unzClose(pobZipFile);

			// Covert data to collada doc
			bool bReturnMe = FCollada::LoadDocumentFromMemory(pcFilenameInZip, pDocument, pcData, pfile_info.uncompressed_size );
			delete[] pcData;
			return bReturnMe;
		}
		else
		{
			Errorf("Unknown fileextension %s while loading %s", pcFileExt, filename);
			return false;
		}
	}

	bool RageCollada::SaveDocument(FCDocument* pDocument, const char* filename)
	{
		// Does the filename end with dae?
		const char* pcFileExt = strrchr(filename, '.');
		if(stricmp(pcFileExt, ".dae") == 0)
		{
			// Given a dae, so just save normally
			return FCollada::SaveDocument( pDocument, filename );
		}
		else if(stricmp(pcFileExt, ".zip") == 0)
		{
			// Open the zip file
			int err = 0;
			zlib_filefunc_def ffunc;
			fill_win32_filefunc(&ffunc);
			zipFile pobZipFile = zipOpen(filename,false);
			if(!pobZipFile)
			{
				Errorf("Error occurred saving to %s", filename);
				return false;
			}

			// Generate the file to go in the zip
			const char* pcStartOfFilename = filename;
			if(strrchr(pcStartOfFilename, '\\') != NULL)
			{
				pcStartOfFilename = (strrchr(pcStartOfFilename, '\\') + 1);
			}
			if(strrchr(pcStartOfFilename, '/') != NULL)
			{
				pcStartOfFilename = (strrchr(pcStartOfFilename, '/') + 1);
			}

			char pcFilenameInZip[512];
			strcpy(pcFilenameInZip, pcStartOfFilename);
			char* pcFileExtOfFileInZip = strrchr(pcFilenameInZip, '.');
			strcpy(pcFileExtOfFileInZip, ".dae");

			zip_fileinfo pcFileInfoForFileInZip;
			pcFileInfoForFileInZip.tmz_date.tm_sec = pcFileInfoForFileInZip.tmz_date.tm_min = pcFileInfoForFileInZip.tmz_date.tm_hour =	pcFileInfoForFileInZip.tmz_date.tm_mday = pcFileInfoForFileInZip.tmz_date.tm_mon = pcFileInfoForFileInZip.tmz_date.tm_year = 0;
			pcFileInfoForFileInZip.dosDate = 0;
			pcFileInfoForFileInZip.internal_fa = 0;
			pcFileInfoForFileInZip.external_fa = 0;

			// Set file time for file in the zip
			SYSTEMTIME obSystemTime;
			GetSystemTime(&obSystemTime);
			pcFileInfoForFileInZip.dosDate = 0;
			pcFileInfoForFileInZip.tmz_date.tm_year = obSystemTime.wYear;
			pcFileInfoForFileInZip.tmz_date.tm_mon = obSystemTime.wMonth;
			pcFileInfoForFileInZip.tmz_date.tm_mday = obSystemTime.wDay;
			pcFileInfoForFileInZip.tmz_date.tm_hour = obSystemTime.wHour;
			pcFileInfoForFileInZip.tmz_date.tm_min = obSystemTime.wMinute;
			pcFileInfoForFileInZip.tmz_date.tm_sec = obSystemTime.wSecond;

			err = zipOpenNewFileInZip3(pobZipFile,pcFilenameInZip,&pcFileInfoForFileInZip,
											 NULL,0,NULL,0,NULL,
											 Z_DEFLATED,
											 Z_BEST_COMPRESSION,0,
											 -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY,
											 NULL,0);
			if (err != ZIP_OK)
			{
				Errorf("Error occurred creating %s in zipfile\n",pcFilenameInZip);
				return false;
			}

			// Get data to write
			char* pcData  = NULL;
			size_t iDataSize = 0;
			FCollada::SaveDocumentToMemory( pDocument, pcFilenameInZip, pcData, iDataSize );

			// Write it into zip
			err = zipWriteInFileInZip (pobZipFile,pcData,iDataSize);
			if (err<0)
			{
				Errorf("Error occurred writing %s in zipfile\n",pcFilenameInZip);
				return false;
			}

			err = zipCloseFileInZip(pobZipFile);
			if (err!=ZIP_OK)
			{
				Errorf("Error occurred closing %s in zipfile\n",pcFilenameInZip);
				return false;
			}

			err = zipClose(pobZipFile,NULL);
			if (err != ZIP_OK)
			{
				Errorf("Error occurred closing %s in zipfile\n",filename);
				return false;
			}
			delete[] pcData;

			return true;
		}
		else
		{
			Errorf("Unknown fileextension %s while saving %s", pcFileExt, filename);
			return false;
		}
	}
}
