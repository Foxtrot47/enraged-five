#include "rageColladaProperties.h"

#include "atl/binmap.h"

#define INVALID_INDEX( idx ) ( -1 == idx )

namespace rage {

	typedef atStringMap< bool > BoolValueMap;
	typedef atStringMap< int > IntValueMap;
	typedef atStringMap< float > FloatValueMap;

	struct Module 
	{
		BoolValueMap boolValues;
		IntValueMap intValues;
		FloatValueMap floatValues;
	};

	typedef atStringMap< Module* > ModuleMap;

	static ModuleMap s_Modules;

	static bool FindKeyValue( const char* module, const char* key, bool** value )
	{
		int moduleIndex = s_Modules.IndexOf( module );
		if ( INVALID_INDEX( moduleIndex ) )
		{
			return false;
		}

		Module** pModule = s_Modules.GetItem( moduleIndex );
		Assert( NULL != *pModule );

		int keyIndex = ( *pModule )->boolValues.IndexOf( key ) ;
		if ( INVALID_INDEX( keyIndex ) )
		{
			return false;
		}

		*value = ( *pModule )->boolValues.GetItem( keyIndex );

		return true;
	}

	static bool FindKeyValue( const char* module, const char* key, int** value )
	{
		int moduleIndex = s_Modules.IndexOf( module );
		if ( INVALID_INDEX( moduleIndex ) )
		{
			return false;
		}

		Module** pModule = s_Modules.GetItem( moduleIndex );
		Assert( NULL != *pModule );

		int keyIndex = ( *pModule )->intValues.IndexOf( key );
		if ( INVALID_INDEX( keyIndex ) )
		{
			return false;
		}

		*value = ( *pModule )->intValues.GetItem( keyIndex );

		return true;
	}

	static bool FindKeyValue( const char* module, const char* key, float** value )
	{
		int moduleIndex = s_Modules.IndexOf( module );
		if ( INVALID_INDEX( moduleIndex ) )
		{
			return false;
		}

		Module** pModule = s_Modules.GetItem( moduleIndex );
		Assert( NULL != *pModule );

		int keyIndex = ( *pModule )->floatValues.IndexOf( key );
		if ( INVALID_INDEX( keyIndex ) )
		{
			return false;
		}

		*value = ( *pModule )->floatValues.GetItem( keyIndex );

		return true;
	}

	bool rageColladaProperties::Initialize()
	{
		s_Modules.Reset();

		return true;
	}

	void rageColladaProperties::Shutdown()
	{
		for ( ModuleMap::Iterator it = s_Modules.Begin(); it != s_Modules.End(); ++it )
		{
			delete (*it);
		}

		s_Modules.Reset();
	}

	void rageColladaProperties::Register( const char* module, const char* key, const bool value )
	{
		int index = s_Modules.IndexOf( module );

		if ( -1 != index )
		{
			Module** pModule = s_Modules.GetItem( index );
			( *pModule )->boolValues.Insert( key, value );
		}
		else
		{
			Module* pModule = new Module;
			pModule->boolValues.Insert( key, value );

			s_Modules.Insert( module, pModule );
		}
	}

	bool rageColladaProperties::SetValue( const char* module, const char* key, const bool value )
	{
		bool* pValue;
		if ( FindKeyValue( module, key, &pValue ) )
		{
			*pValue = value;

			return true;
		}

		return false;
	}

	bool rageColladaProperties::GetValue( const char* module, const char* key, bool& value )
	{
		bool* pValue;
		if ( FindKeyValue( module, key, &pValue ) )
		{
			value = *pValue;

			return true;
		}

		return false;
	}

	void rageColladaProperties::Register( const char* module, const char* key, const int value )
	{
		int index = s_Modules.IndexOf( module );

		if ( -1 != index )
		{
			Module** pModule = s_Modules.GetItem( index );
			( *pModule )->intValues.Insert( key, value );
		}
		else
		{
			Module* pModule = new Module;
			pModule->intValues.Insert( key, value );

			s_Modules.Insert( module, pModule );
		}
	}

	bool rageColladaProperties::SetValue( const char* module, const char* key, const int value )
	{
		int* pValue;
		if ( FindKeyValue( module, key, &pValue ) )
		{
			*pValue = value;

			return true;
		}

		return false;
	}

	bool rageColladaProperties::GetValue( const char* module, const char* key, int& value )
	{
		int* pValue;
		if ( FindKeyValue( module, key, &pValue ) )
		{
			value = *pValue;

			return true;
		}

		return false;
	}

	void rageColladaProperties::Register( const char* module, const char* key, const float value )
	{
		int index = s_Modules.IndexOf( module );

		if ( -1 != index )
		{
			Module** pModule = s_Modules.GetItem( index );
			( *pModule )->floatValues.Insert( key, value );
		}
		else
		{
			Module* pModule = new Module;
			pModule->floatValues.Insert( key, value );

			s_Modules.Insert( module, pModule );
		}
	}

	bool rageColladaProperties::SetValue( const char* module, const char* key, const float value )
	{
		float* pValue;
		if ( FindKeyValue( module, key, &pValue ) )
		{
			*pValue = value;

			return true;
		}

		return false;
	}

	bool rageColladaProperties::GetValue( const char* module, const char* key, float& value )
	{
		float* pValue;
		if ( FindKeyValue( module, key, &pValue ) )
		{
			value = *pValue;

			return true;
		}

		return false;
	}

} // rage 
