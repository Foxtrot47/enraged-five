#ifndef __RAGE_COLLADA_CONVERT__
#define __RAGE_COLLADA_CONVERT__

#include <string>

#include "rageColladaUtil.h"

#include "mesh/mesh.h"
#include "phbound/boundbox.h"
#include "phbound/boundbvh.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundgeom.h"
//#include "phbound/boundoctree.h"
//#include "phbound/boundotgrid.h"
#include "phbound/boundsphere.h"

class FCDGeometryMesh;
class FCDSceneNode;

namespace rage {

	enum rageColladaSpace
	{
		kRageColladaSpaceWorld,
		kRageColladaSpaceLocal,
	};

	bool ConvertBoundBox( phBoundBox& box, const FCDSceneNode* pNode );
	bool ConvertBoundBvh( phBoundBVH& bvh, const FCDSceneNode* pNode );
	//bool ConvertBoundBvhGrid( phBoundOTGrid& bvhGrid, const FCDSceneNode* pNode );
	bool ConvertBoundCapsule( phBoundCapsule& capsule, const FCDSceneNode* pNode );
	bool ConvertBoundGeometry( phBoundGeometry& geometry, const FCDSceneNode* pNode );
	//bool ConvertBoundOctree( phBoundOctree& octree ,const FCDSceneNode* pNode );
	//bool ConvertBoundOctreeGrid( phBoundOTGrid& octreeGrid, const FCDSceneNode* pNode );
	bool ConvertBoundSphere( phBoundSphere& sphere, const FCDSceneNode* pNode );
	
	void ConvertMesh( mshMesh& mesh, const FCDGeometry* pGeometry, const FCDMaterialList& materials, FMMatrix44& transformMtx  );

	void ConvertMesh( mshMesh& mesh, const FCDSkinController* pSkin, const FCDMaterialList& materials, FMMatrix44& parentNodeWorldTransformMtx );


	struct ConvertMeshResult
	{
		typedef std::vector< mshMesh* > mshMeshList;
		mshMeshList	m_mshMeshList;

		std::vector< std::string > m_mshMeshNameList;

		size_t			GetNumMeshes() const { return m_mshMeshList.size(); }
		mshMesh*		GetMesh(size_t idx) const { Assert(idx<m_mshMeshList.size()); return m_mshMeshList[idx]; }
		const char*		GetMeshName(size_t idx) const { Assert(idx<m_mshMeshNameList.size()); return m_mshMeshNameList[idx].c_str(); }
	};
	void ConvertMesh( ConvertMeshResult &outResults, const FCDSceneNode* pNode, const FCDMaterialList& materials, rageColladaSpace space );

} // rage

#endif // __RAGE_COLLADA_CONVERT__
