#ifndef __RAGE_COLLADA_UTIL__
#define __RAGE_COLLADA_UTIL__

#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector3.h"

#include <string>
#include <vector>
#include <map>

class FCDGeometry;
class FCDGeometryInstance;
class FCDMaterial;
class FCDSceneNode;

namespace rage {

	typedef std::vector< std::pair< std::string, const FCDMaterial* > > FCDMaterialList;
	typedef std::vector< const FCDGeometry* > FCDGeometryList;

	void ToMatrix44( Matrix44& out, const FMMatrix44& in );
	void ToMatrix34( Matrix34& out, const FMMatrix44& in );

	FMMatrix44 CalculateWorldMatrixNoRotation( const FCDSceneNode* pSceneNode );

	void GetJointWorldMatrix( const FCDSceneNode* pJointNode, rage::Matrix34& outWorldMatrix, bool bIncludeJointOrients = false );
	void GetRageJointOrdering(const FCDSkinController* pSkin, 
									std::vector<const FCDSceneNode*>& outOrderedJointNodeArray,
									std::map<size_t, size_t>& outJointIndexMap);
	void GetJointIndexToJointNodeMap(	const FCDSkinController* pSkinController,
										std::map<size_t, const FCDSceneNode*>& outJointIndexToJointNodeMap );

	void ComputeBoundingSphere( const FCDGeometryList& geometryList, Vector3& center, float& radius );
	void ComputeBoundingBox( const FCDGeometryList& geometryList, Vector3& min, Vector3& max  );

	std::string GetShaderMaterial( const FCDMaterial* material );
	bool IsShaderMaterial( const FCDMaterial* material );

	typedef std::vector< double > RAGEThresholdList;

	void GetLodThreshold( const FCDSceneNode* sceneNode, RAGEThresholdList& thresholds );
	bool IsLodThreshold( const FCDSceneNode* sceneNode );

	void FindReferencedMaterials( FCDMaterialList& materials, const FCDGeometryInstance *pInstance );
	void FindReferencedMaterials( FCDMaterialList& materials, const FCDSceneNode *pNode );

	// Functions for getting/setting the conditioner tags of a visual scene instance
	bool GetConditionerTag( const FCDSceneNode* pVsNode, const char* name );
	void SetConditionerTag( FCDSceneNode* pVsNode, const char* name, const char* version );

	class RageCollada
	{
	public:
		static bool LoadDocument(FCDocument* pDocument, const char* filename);
		static bool SaveDocument(FCDocument* pDocument, const char* filename);
	};
	
} // rage

#endif // __RAGE_COLLADA_UTIL__
