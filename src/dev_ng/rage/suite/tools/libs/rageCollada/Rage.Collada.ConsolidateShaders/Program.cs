using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Rage.Collada.ConsolidateShaders
{
	class Program
	{
		static void ReplaceAllReferencesToOneEffectNodeWithAnother(FCDEffect obOldEffect, FCDEffect obNewEffect)
		{
			// Effects are referenced in materials, so go through all materials looking for references to the old effect
			using (FColladaDocumentMaterialLibrary obInputMaterialLibraryNode = obOldEffect.GetDocument().GetMaterialLibrary())
			{
				for (uint iMaterialNo = 0; iMaterialNo < obInputMaterialLibraryNode.GetEntityCount(); iMaterialNo++)
				{
					// Get Material
					using (FCDMaterial obMaterial = obInputMaterialLibraryNode.GetEntity(iMaterialNo))
					{
						if (FColladaCSharp.IsEquivalent(obMaterial.GetEffect().GetDaeId(), obOldEffect.GetDaeId()))
						{
							// Repoint it
							obMaterial.SetEffect(obNewEffect);
						}
					}
				}
			}
		}

		static void ReplaceAllReferencesToOneMaterialNodeWithAnother(FCDMaterial obOldMaterial, FCDMaterial obNewMaterial, FCDMaterialInstance obInstance)
		{
			// Geometries have instance materials that need repointing
			using (FCDMaterial obMaterial = obInstance.GetMaterial())
			{
				if (obMaterial != null)
				{
					if (FColladaCSharp.IsEquivalent(obMaterial.GetDaeId(), obOldMaterial.GetDaeId()))
					{
						obInstance.SetMaterial(obNewMaterial);
					}
				}
			}
		}

		static void ReplaceAllReferencesToOneMaterialNodeWithAnother(FCDMaterial obOldMaterial, FCDMaterial obNewMaterial, FCDGeometryInstance obInstance)
		{
			// Geometries have instance materials that need repointing
			for (uint iInstanceNo = 0; iInstanceNo < obInstance.GetMaterialInstanceCount(); iInstanceNo++)
			{
				ReplaceAllReferencesToOneMaterialNodeWithAnother(obOldMaterial, obNewMaterial, obInstance.GetMaterialInstance(iInstanceNo));
			}
		}

		static void ReplaceAllReferencesToOneMaterialNodeWithAnother(FCDMaterial obOldMaterial, FCDMaterial obNewMaterial, FCDEntityInstance obInstance)
		{
			using (FCDEntity obOldEntity = obInstance.GetEntity())
			{
				if (obOldEntity != null)
				{
					if (FColladaCSharp.IsEquivalent(obOldEntity.GetDaeId(), obOldMaterial.GetDaeId()))
					{
						obInstance.SetEntity(obNewMaterial);
					}
				}
			}

			switch (obInstance.GetType())
			{
				case FCDEntityInstance.FCDEntityInstance_Type.SIMPLE:
					{
						// Nothing special needed for simple instances
						break;
					}
				case FCDEntityInstance.FCDEntityInstance_Type.GEOMETRY:
					{
						// Geometries have instance materials that need repointing
						ReplaceAllReferencesToOneMaterialNodeWithAnother(obOldMaterial, obNewMaterial, FColladaCSharp.FCDEntityInstanceToFCDGeometryInstance(obInstance));
						break;
					}
				default:
					{
						Console.Error.WriteLine("Unable to handle instance of type " + obInstance.GetType());
						break;
					}
			}
		}

		static void ReplaceAllReferencesToOneMaterialNodeWithAnother(FCDMaterial obOldMaterial, FCDMaterial obNewMaterial, FCDSceneNode obSceneNode)
		{
			// Look at myself
			for (uint iInstanceNo = 0; iInstanceNo < obSceneNode.GetInstanceCount(); iInstanceNo++)
			{
				ReplaceAllReferencesToOneMaterialNodeWithAnother(obOldMaterial, obNewMaterial, obSceneNode.GetInstance(iInstanceNo));
			}

			// Recurse
			for (uint iChild = 0; iChild < obSceneNode.GetChildrenCount(); iChild++)
			{
				FCDSceneNode obChild = obSceneNode.GetChild(iChild);
				ReplaceAllReferencesToOneMaterialNodeWithAnother(obOldMaterial, obNewMaterial, obChild);
			}
		}

		static void ReplaceAllReferencesToOneMaterialNodeWithAnother(FCDMaterial obOldMaterial, FCDMaterial obNewMaterial)
		{
			// Materials are referenced in materials, so go through all materials looking for references to the old Material
			using (FColladaDocumentVisualSceneNodeLibrary obSourceVisualSceneLibraryNode = obOldMaterial.GetDocument().GetVisualSceneLibrary())
			{
				for (uint iSceneNodeNo = 0; iSceneNodeNo < obSourceVisualSceneLibraryNode.GetEntityCount(); iSceneNodeNo++)
				{
					// Get SceneNode
					using (FCDSceneNode obSceneNode = obSourceVisualSceneLibraryNode.GetEntity(iSceneNodeNo))
					{
						ReplaceAllReferencesToOneMaterialNodeWithAnother(obOldMaterial, obNewMaterial, obSceneNode);
					}
				}
			}
		}

		static string GetMltPathAndFilenameFromEffectNode(FCDEffect obInputEffect)
		{
			// Does it have an "Extra" section?
			FCDExtra obInputEffectExtra = obInputEffect.GetExtra();

			if (obInputEffectExtra != null)
			{
				// Got extra, so find out if it is Rage extra
				for (uint iExtraType = 0; iExtraType < obInputEffectExtra.GetTypeCount(); iExtraType++)
				{
					FCDEType obType = obInputEffectExtra.GetType(iExtraType);
					// Console.WriteLine("obType = " + obType.GetName().c_str());
					for (uint iTechnique = 0; iTechnique < obType.GetTechniqueCount(); iTechnique++)
					{
						FCDETechnique obTechnique = obType.GetTechnique(iTechnique);
						// Console.WriteLine("obTechnique = " + obTechnique.GetProfile());
						if (obTechnique.GetProfile() == "RAGE")
						{
							// Bingo!  Found a RAGE shader, get the mtlgeo and see if I can find a match
							for (uint iChild = 0; iChild < obTechnique.GetChildNodeCount(); iChild++)
							{
								FCDENode obChild = obTechnique.GetChildNode(iChild);
								// Console.WriteLine("obChild = " + obChild.GetName());
								if (obChild.GetName() == "rageShaderMaterial")
								{
									// Bingo!  Found a RAGE shader, get the mtlgeo and see if I can find a match
									for (uint iAttribute = 0; iAttribute < obChild.GetAttributeCount(); iAttribute++)
									{
										FCDEAttribute obAttribute = obChild.GetAttribute(iAttribute);
										// Console.WriteLine("obAttribute = " + obAttribute.name.c_str());etDaeId())
										if (FColladaCSharp.IsEquivalent(obAttribute.GetName(), "path"))
										{
											// Bingo!  Found a RAGE shader, get the mtlgeo and see if I can find a match
											// Console.WriteLine(obAttribute.value.c_str());
											return obAttribute.GetValue().c_str().ToLower().Replace('\\', '/');
										}
									}
								}
							}
						}
					}
				}
			}
			return null;
		}

		static int Main(string[] args)
		{
			// T:\mc4\assets\city\la_Pipeline5\SectorColladaFiles\be_smb_01.dae T:\fish.dae
			// T:\mc4\assets\city\la_Pipeline5\SectorColladaFiles\be_smc_highway.dae T:\fish.dae
			// T:\mc4\assets\city\la_Pipeline5\SectorColladaFiles\hw_ss_08.dae T:\fish.dae
			if (args.Length != 2)
			{
				Console.WriteLine("Usage:");
				Console.WriteLine("Rage.Collada.ConsolidateShaders <input file> <output file>");
				return -1;
			}

			FColladaCSharp.FSInit();
			FColladaCSharp.SetDereferenceFlag(false);

			// Get args
			string strInputFile = args[0];
            string inputfile = Path.GetFileName(strInputFile);
            string inputmnt = null;
            if (Path.GetExtension(inputfile) == ".zip")
            {
                inputmnt = strInputFile.Replace('\\', '/');
                inputfile = inputfile.Replace(".zip", ".dae");
            }
            else
            {
                inputmnt = Path.GetDirectoryName(strInputFile);
            }

			string strOutputFile = args[1];
            string outputfile = Path.GetFileName(strOutputFile);
            string outputmnt = null;
            if (Path.GetExtension(outputfile) == ".zip")
            {
                outputmnt = strOutputFile.Replace('\\', '/');
                outputfile = outputfile.Replace(".zip", ".dae");
            }
            else
            {
                outputmnt = Path.GetDirectoryName(strOutputFile);
            }

			// Init FCollada
			FColladaCSharp.Initialize();
			FColladaCSharp.Initialize();
			FColladaCSharp.Initialize();

			// Load the document
			FCDocument obInputColladaDoc = new FCDocument();
			Console.WriteLine("Loading : " + strInputFile);

            FColladaCSharp.FSMount(inputmnt, null, 0);
			bool bResult = FColladaCSharp.LoadDocumentFromFile(obInputColladaDoc, inputfile);
            FColladaCSharp.FSRemoveFromPath(inputmnt);

			//Console.WriteLine("bResult = " + bResult);

			// Go through effects and consolidate them
			Console.WriteLine("Consolidating Effects");
			using (FColladaDocumentEffectLibrary obInputEffectLibraryNode = obInputColladaDoc.GetEffectLibrary())
			{
				uint iNoOfEffectsBefore = obInputEffectLibraryNode.GetEntityCount();
				for (uint iEffectNo = 0; iEffectNo < obInputEffectLibraryNode.GetEntityCount(); iEffectNo++)
				{
					// Get Effect
					using (FCDEffect obEffect = obInputEffectLibraryNode.GetEntity(iEffectNo))
					{

						// Get mtl
						string strMltPathAndFilename = GetMltPathAndFilenameFromEffectNode(obEffect);

						if (strMltPathAndFilename != null)
						{
							// Got an mtl path and filename, see if it matches any existing ones
							for (uint iOtherEffectNo = (iEffectNo + 1); iOtherEffectNo < obInputEffectLibraryNode.GetEntityCount(); iOtherEffectNo++)
							{
								// Get Effect
								using (FCDEffect obOtherEffect = obInputEffectLibraryNode.GetEntity(iOtherEffectNo))
								{

									// Get mtl
									string strOtherMltPathAndFilename = GetMltPathAndFilenameFromEffectNode(obOtherEffect);

									if (strMltPathAndFilename == strOtherMltPathAndFilename)
									{
										// Bingo!  Got a match, so consolidate
										// Console.WriteLine(obEffect.GetDaeId().c_str() + " and " + obOtherEffect.GetDaeId().c_str() + " both use the mtl " + strMltPathAndFilename);
										ReplaceAllReferencesToOneEffectNodeWithAnother(obOtherEffect, obEffect);

										// Delete the other effect
										FColladaCSharp.DeleteEntityAndRemoveFromLists(obOtherEffect);
										iOtherEffectNo--;
									}
								}
							}
						}
					}
				}
				uint iNoOfEffectsAfter = obInputEffectLibraryNode.GetEntityCount();
				Console.WriteLine("There was " + iNoOfEffectsBefore + " effects, there are now " + iNoOfEffectsAfter + " effects (" + (iNoOfEffectsBefore - iNoOfEffectsAfter) + " removed)");
			}


			// Go through Materials and consolidate them
			Console.WriteLine("Consolidating Materials");
			using (FColladaDocumentMaterialLibrary obInputMaterialLibraryNode = obInputColladaDoc.GetMaterialLibrary())
			{
				uint iNoOfMaterialsBefore = obInputMaterialLibraryNode.GetEntityCount();
				for (uint iMaterialNo = 0; iMaterialNo < obInputMaterialLibraryNode.GetEntityCount(); iMaterialNo++)
				{
					// Get Material
					using(FCDMaterial obMaterial = obInputMaterialLibraryNode.GetEntity(iMaterialNo))
					{
						if (obMaterial.GetEffect() != null)
						{
							// Get mtl
							fstring fstrMaterialsEffect = obMaterial.GetEffect().GetDaeId();
							if (fstrMaterialsEffect.c_str() != null)
							{
								// Got an mtl path and filename, see if it matches any existing ones
								for (uint iOtherMaterialNo = (iMaterialNo + 1); iOtherMaterialNo < obInputMaterialLibraryNode.GetEntityCount(); iOtherMaterialNo++)
								{
									// Get Material
									using (FCDMaterial obOtherMaterial = obInputMaterialLibraryNode.GetEntity(iOtherMaterialNo))
									{

										if (obOtherMaterial.GetEffect() != null)
										{
											// Get mtl
											if (FColladaCSharp.IsEquivalent(fstrMaterialsEffect, obOtherMaterial.GetEffect().GetDaeId()))
											{
												// Bingo!  Got a match, so consolidate
												// Console.WriteLine(obMaterial.GetDaeId().c_str() + " and " + obOtherMaterial.GetDaeId().c_str() + " both use the effect " + strOtherMaterialsEffect);
												ReplaceAllReferencesToOneMaterialNodeWithAnother(obOtherMaterial, obMaterial);

												// Delete the other Material
												FColladaCSharp.DeleteEntityAndRemoveFromLists(obOtherMaterial);
												iOtherMaterialNo--;
											}
										}
									}
								}
							}
						}
					}
				}
				uint iNoOfMaterialsAfter = obInputMaterialLibraryNode.GetEntityCount();
				Console.WriteLine("There was " + iNoOfMaterialsBefore + " materials, there are now " + iNoOfMaterialsAfter + " materials (" + (iNoOfMaterialsBefore - iNoOfMaterialsAfter) + " removed)");
			}

			// Save the document
			Console.WriteLine("Saving : " + strOutputFile);
            FColladaCSharp.FSMount(outputmnt, null, 0);
			try
			{
				FColladaCSharp.SaveDocument(obInputColladaDoc, outputfile);
			}
			catch (Exception e)
			{
				// Something bad happened
				Console.Error.WriteLine("Failed to save to \"" + strOutputFile + "\"");
				Console.Error.WriteLine(e.ToString());
				if (File.Exists(strOutputFile))
				{
					FileInfo obFile = new FileInfo(strOutputFile);
					obFile.Attributes = 0;
					File.Delete(strOutputFile);
				}
				return -1;
			}
			FColladaCSharp.FSRemoveFromPath(outputmnt);

            FColladaCSharp.FSDeinit();
			return 0;
		}
	}
}
