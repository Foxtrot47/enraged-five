-------------------------------------------------------------------------------

function IsNodeGeometryNode( sceneNode )
	
	numInstances = sceneNode:GetInstanceCount()
	if( numInstances > 0 ) then
		i = 0
		while i < numInstances do
			inst = sceneNode:GetInstance(i)
			if ( inst:GetEntityType() == FCollada.FCDEntity_GEOMETRY ) then
				return 1
			end
			i = i + 1
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function IsNodeControllerNode( sceneNode )

	numInstances = sceneNode:GetInstanceCount()
	if ( numInstances > 0 ) then
		i = 0
		while i < numInstances do
			inst = sceneNode:GetInstance(i)
			if ( inst:GetEntityType() == FCollada.FCDEntity_CONTROLLER ) then
				rage.Display( "Controller node: " .. sceneNode:GetName():c_str() )
				return 1
			end
			i = i + 1
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function HasParentJointNamedRec ( sceneNode, parentName )

	if( sceneNode:GetName():c_str() == parentName ) then
		if ( sceneNode:IsJoint() == true ) then
			return 1
		end
		return 0	
	else
		numParents = sceneNode:GetParentCount()
		if( numParents > 0 ) then
			i = 0
			while i < numParents do
				parentNode = sceneNode:GetParent(i)
				return HasParentJointNamedRec( parentNode, parentName )
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function HasParentJointNamed ( sceneNode, parentName )

	numParents = sceneNode:GetParentCount()
	if( numParents > 0 ) then
		i = 0
		while i < numParents do
			parentNode = sceneNode:GetParent(i)
			if( HasParentJointNamedRec( parentNode, parentName) == 1 ) then
				return 1
			else
				i = i + 1
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function HasParentNodeNamedRec ( sceneNode, parentName )

	if ( sceneNode:GetName():c_str() == parentName ) then
		return 1
	else
		numParents = sceneNode:GetParentCount()
		if ( numParents > 0 ) then
			i = 0
			while i < numParents do
				parentNode = sceneNode:GetParent( i )
				return HasParentNodeNamedRec( parentNode, parentName )
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function HasParentNodeNamed( sceneNode, parentName )

	numParents = sceneNode:GetParentCount()
	if ( numParents > 0 ) then
		i = 0
		while i < numParents do
			parentNode = sceneNode:GetParent( i )
			if ( HasParentNodeNamedRec( parentNode, parentName ) == 1 ) then
				return 1
			else
				i = i + 1
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function DoMergeNode( sceneNode )

	rage.Display( "MERGE TEST: " .. sceneNode:GetName():c_str() )
	if ( string.find( sceneNode:GetName():c_str(), "Bound" ) ~= nil ) then
		rage.Display( "DON'T MERGE" )
		return 0
	end

	if ( IsNodeControllerNode( sceneNode ) == 1 ) then
		if ( HasParentNodeNamed( sceneNode, "root" ) == 1 ) then
			rage.Display( "MERINGING: " .. sceneNode:GetName():c_str() )
			return 1
		end	
	end

	return 0
	
end

-------------------------------------------------------------------------------

function IsMeshMergeRoot( sceneNode )

	if ( sceneNode:GetName():c_str() == "root" ) then
		return 1
	end	

	return 0
end


