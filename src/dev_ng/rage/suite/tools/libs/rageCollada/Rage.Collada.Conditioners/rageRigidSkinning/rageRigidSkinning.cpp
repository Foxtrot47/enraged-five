#include "rageRigidSkinning.h"

#include "rageColladaHelpers.h"

#include "rageLuaScriptHost/rageLuaScriptHost.h"
#include "rageDiplomat/rageDiplomat.h"
#include "rageDiplomat/rageLuaScriptTreaty.h"

#if __OPTIMIZED
#define FCOLLADALUA_PATH	"FColladaLua.dll"
#else
#define FCOLLADALUA_PATH	"C:\\soft\\rage_head\\rage\\suite\\tools\\rageCollada\\FColladaSWIG\\FColladaLua\\Debug\\FColladaLua.dll"
#endif
#define FCOLLADALUA_INIT_FN	"luaopen_FCollada"

#define TREATY_SCRIPT "C:\\soft\\rage\\suite\\tools\\rageCollada\\Rage.Collada.Conditioners\\rageRigidSkinning\\rageRigidSkinning.lua"

namespace {
	
	struct RigidSkinningUserData
	{
		FCDMeshRigidSkinningList meshes;
		FCDSceneNode* pRoot;
	};

	void AddMeshRigidSkinningElement( FCDSceneNode* pNode, void* pData )
	{
		RigidSkinningUserData* pUserData = 
			reinterpret_cast< RigidSkinningUserData* >( pData );
		pUserData->meshes.push_back( pNode );
	}

	void AddMeshRigidSkinningRootElement( FCDSceneNode* pNode, void* pData )
	{
		RigidSkinningUserData* pUserData =
			reinterpret_cast< RigidSkinningUserData* >( pData );
		pUserData->pRoot = pNode;
	}

}

namespace rage {

	bool rageRigidSkinning::Init()
	{
		return true;
	}

	int rageRigidSkinning::Execute()
	{
		INIT_LUASCRIPTHOST;

		if( !LUASCRIPTHOST.LoadExtension( FCOLLADALUA_PATH, FCOLLADALUA_INIT_FN, NULL ) )
		{		
			Errorf( "Failed to load LUA extension : '%s'", FCOLLADALUA_PATH );
			return 1;
		}

		typedef rageLuaScriptTreaty< rageShouldAcceptFtor > ScriptTreaty;

		rageShouldAcceptFtor doRigidSkinning ( "DoRigidSkinning" );

		RigidSkinningUserData data;
		ScriptTreaty* pRigidSkinningScriptTreaty = 
			new ScriptTreaty( MakeFunctor( &AddMeshRigidSkinningElement ), &data );
		if ( !pRigidSkinningScriptTreaty->InitializeTreatyScript( TREATY_SCRIPT, doRigidSkinning ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			return 1;
		}

		rageShouldAcceptFtor isRigidSkinningRoot ( "IsRigidSkinningRoot" );

		ScriptTreaty* pRigidSkinningRootScriptTreaty = 
			new ScriptTreaty( MakeFunctor( &AddMeshRigidSkinningRootElement ), &data );
		if ( !pRigidSkinningRootScriptTreaty->InitializeTreatyScript( TREATY_SCRIPT, isRigidSkinningRoot ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			return 1;
		}

		rageDiplomat diplomat;
		diplomat.RegisterTreaty( pRigidSkinningScriptTreaty );
		diplomat.RegisterTreaty( pRigidSkinningRootScriptTreaty );

		diplomat.VisitDepthFirst( m_document->GetVisualSceneInstance(), rageDiplomat::MULTIPLE_SIGNING );

		RigidSkin( data.pRoot, data.meshes );

		SHUTDOWN_LUASCRIPTHOST;

		return 0;
	}

	rageIConditioner::Register< rageRigidSkinningProxy > g_RigidSkinningProxy;

}
