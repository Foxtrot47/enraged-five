-------------------------------------------------------------------------------

function IsFragmentRoot( sceneNode )

	if ( sceneNode:GetName():c_str() == "root" ) then
		return 1
	end
	
	return 0

end

-------------------------------------------------------------------------------

function HasParentJointRec ( sceneNode )

	if( sceneNode:IsJoint() == true ) then
		return 1
	else
		numParents = sceneNode:GetParentCount()
		if( numParents > 0 ) then
			count = numParents - 1
			for i = 0, count do
				parentNode = sceneNode:GetParent(i)
				return HasParentJointRec( parentNode )
			end
		end
	end
	
	return 0
end

-------------------------------------------------------------------------------

function HasParentJoint ( sceneNode )

	numParents = sceneNode:GetParentCount()
	if( numParents > 0 ) then
		count = numParents - 1
		for i = 0, count do
			parentNode = sceneNode:GetParent(i)
			if( HasParentJointRec( parentNode ) == 1 ) then
				return 1
			end
		end
	end
	
	return 0
end


-------------------------------------------------------------------------------

function IsSkeletonRoot( sceneNode )

	if ( sceneNode:IsJoint() == true ) then
		if ( HasParentJoint( sceneNode ) ~= 1 ) then
			return 1
		end
	end	
	
	return 0

end

-------------------------------------------------------------------------------

