#ifndef __RAGE_FRAGMENT_H__
#define __RAGE_FRAGMENT_H__
#pragma once

#include "rageColladaConditioner.h"
#include "rageColladaExport.h"

namespace rage {

	class rageFragment : public rageConditioner
	{
	public:
		DLL_EXPORT bool Init();
		DLL_EXPORT int Execute();

		DLL_EXPORT std::string GetBaseName() const { return "rageFragment"; }
		DLL_EXPORT std::string GetDescription() const { return "rageFragment"; }
	};

	class rageFragmentProxy : public rageConditionerCreator
	{
	public:
		DLL_EXPORT std::string GetBaseName() const { return "rageFragment"; }
		DLL_EXPORT std::string GetDescription() const { return "rageFragment"; };

		DLL_EXPORT rageIConditioner* Create() const { return new rageFragment(); };
	};

} // rage

#endif // __RAGE_FRAGMENT_H__
