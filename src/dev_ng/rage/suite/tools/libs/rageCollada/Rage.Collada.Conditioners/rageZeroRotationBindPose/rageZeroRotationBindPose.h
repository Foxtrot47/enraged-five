#ifndef __RAGE_ZEROROTATIONBINDPOSE__
#define __RAGE_ZEROROTATIONBINDPOSE__
#pragma once

#include "rageColladaConditioner.h"
#include "rageColladaExport.h"

namespace rage 
{

	class rageZeroRotationBindPose : public rageConditioner
	{
	public:
		DLL_EXPORT bool Init();
		DLL_EXPORT int Execute();

		DLL_EXPORT std::string GetBaseName() const { return "rageZeroRotationBindPose"; }
		DLL_EXPORT std::string GetDescription() const { return "Puts skinned meshes into a zero-rotation bind pose."; }
	};

	class rageZeroRotationBindPoseProxy : public rageConditionerCreator
	{
		DLL_EXPORT std::string GetBaseName() const { return "rageZeroRotationBindPose"; }
		DLL_EXPORT std::string GetDescription() const { return "Puts skinned meshes into a zero-rotation bind pose."; }

		DLL_EXPORT rageIConditioner* Create() const { return new rageZeroRotationBindPose(); }
	};

} // rage

#endif // __RAGE_ZEROROTATIONBINDPOSE__
