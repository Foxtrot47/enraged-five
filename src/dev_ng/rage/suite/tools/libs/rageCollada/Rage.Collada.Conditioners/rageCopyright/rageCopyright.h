#ifndef __RAGE_COPYRIGHT__
#define __RAGE_COPYRIGHT__
#pragma once

#include "rageColladaConditioner.h"
#include "rageColladaExport.h"

namespace rage
{

class rageCopyright : public rageConditioner
{
public:
	DLL_EXPORT bool Init();
	DLL_EXPORT int Execute();

	DLL_EXPORT std::string GetBaseName() const { return "rageCopyright"; }
	DLL_EXPORT std::string GetDescription() const { return "rageCopyright"; }
};

class rageCopyrightProxy : public rageConditionerCreator
{
	DLL_EXPORT std::string GetBaseName() const { return "rageCopyright"; }
	DLL_EXPORT std::string GetDescription() const { return "rageCopyright"; }

	DLL_EXPORT rageIConditioner* Create() const { return new rageCopyright(); }
};

} // rage

#endif // __RAGE_COPYRIGHT__
