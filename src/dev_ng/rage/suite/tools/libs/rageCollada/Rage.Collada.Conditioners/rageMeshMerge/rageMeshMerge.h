#ifndef __RAGE_MESH_MERGE__
#define __RAGE_MESH_MERGE__
#pragma once 

#include "rageColladaConditioner.h"
#include "rageColladaExport.h"

namespace rage {

	class rageMeshMerge : public rageConditioner
	{
	public:
		DLL_EXPORT bool Init();
		DLL_EXPORT int Execute();

		DLL_EXPORT std::string GetBaseName() const { return "rageMeshMerge"; }
		DLL_EXPORT std::string GetDescription() const { return "rageMeshMerge"; }
	};

	class rageMeshMergeProxy : public rageConditionerCreator
	{
	public:
		DLL_EXPORT std::string GetBaseName() const { return "rageMeshMerge"; }
		DLL_EXPORT std::string GetDescription() const { return "rageMeshMerge"; }

		DLL_EXPORT rageIConditioner* Create() const { return new rageMeshMerge(); }
	};

} // rage

#endif // __RAGE_MESH_MERGE__
