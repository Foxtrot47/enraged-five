#include "rageTriangulate.h"

#include "rageColladaHelpers.h"

namespace rage {

	bool rageTriangulate::Init()
	{
		printf( "Triangulate::Init()\n" );

		return true;
	}

	int rageTriangulate::Execute()
	{
		printf( "Triangulate::Execute()\n" );

		FCDSceneNode* pobVisualSceneNode = m_document->GetVisualSceneInstance();
		if(!pobVisualSceneNode)
		{
			Errorf("No visual scene node found in scene, nothing to triangulate");
			return -1;
		}

		rage::Triangulate( pobVisualSceneNode );

		return 0;
	}

	rageIConditioner::Register< rageTriangulateProxy > g_TriangulateProxy;

} // rage
