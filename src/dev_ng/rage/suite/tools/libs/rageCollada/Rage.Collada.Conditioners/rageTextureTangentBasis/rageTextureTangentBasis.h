#ifndef __RAGE_TEXTURE_TANGENT_BASIS_H__
#define __RAGE_TEXTURE_TANGENT_BASIS_H__
#pragma once 

#include "rageColladaConditioner.h"
#include "rageColladaExport.h"

namespace rage 
{
	
class rageTextureTangentBasis : public rageConditioner
{
public:
	DLL_EXPORT bool Init();
	DLL_EXPORT int Execute();

	DLL_EXPORT std::string GetBaseName() const { return "rageTextureTangentBasis"; }
	DLL_EXPORT std::string GetDescription() const { return "rageTextureTangentBasis"; }
};

class rageTextureTangentBasisProxy : public rageConditionerCreator
{
	DLL_EXPORT std::string GetBaseName() const { return "rageTextureTangentBasis"; }
	DLL_EXPORT std::string GetDescription() const { return "rageTextureTangentBasis"; }

	DLL_EXPORT rageIConditioner* Create() const { return new rageTextureTangentBasis(); }
};

}

#endif // __RAGE_TEXTURE_TANGENT_BASIS_H__
