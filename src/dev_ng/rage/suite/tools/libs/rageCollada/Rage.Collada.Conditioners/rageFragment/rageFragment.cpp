#include "rageFragment.h"

#include "rageColladaHelpers.h"

#include "rageLuaScriptHost/rageLuaScriptHost.h"
#include "rageDiplomat/rageDiplomat.h"
#include "rageDiplomat/rageLuaScriptTreaty.h"

#if __OPTIMIZED
#define FCOLLADALUA_PATH	"FColladaLua.dll"
#else
#define FCOLLADALUA_PATH	"C:\\soft\\rage_head\\rage\\suite\\tools\\rageCollada\\FColladaSWIG\\FColladaLua\\Debug\\FColladaLua.dll"
#endif
#define FCOLLADALUA_INIT_FN	"luaopen_FCollada"

#define TREATY_SCRIPT "C:\\soft\\rage\\suite\\tools\\rageCollada\\Rage.Collada.Conditioners\\rageFragment\\rageFragment.lua"

namespace {
	
	static void AddRootFragmentElement( FCDSceneNode* pNode, void* pData )
	{
		FCDSceneNode** pRootFragmentNode = 
			reinterpret_cast< FCDSceneNode** >( pData );

		Assert( !*pRootFragmentNode );
		*pRootFragmentNode = pNode;
	}

	static void AddRootSkeletonElement( FCDSceneNode* pNode, void* pData )
	{
		FCDSceneNode** pRootSkeletonNode = 
			reinterpret_cast< FCDSceneNode** >( pData );

		Assert( !*pRootSkeletonNode );
		*pRootSkeletonNode = pNode;
	}

}

namespace rage {

	bool rageFragment::Init()
	{
		return true;
	}

	int rageFragment::Execute()
	{
		// the skinning is already handles by the rigid skinning conditioner
		// the mesh merging will be handled by the mesh merge conditioner
		// all that need to be done is a hack to make the root node a
		// joint and include that in the hierarchy. any other fragment hacks
		// should go in here too.

		INIT_LUASCRIPTHOST;

		if( !LUASCRIPTHOST.LoadExtension( FCOLLADALUA_PATH, FCOLLADALUA_INIT_FN, NULL ) )
		{		
			Errorf( "Failed to load LUA extension : '%s'", FCOLLADALUA_PATH );
			return 1;
		}

		typedef rageLuaScriptTreaty< rageShouldAcceptFtor > ScriptTreaty;

		rageShouldAcceptFtor isFragmentRoot ( "IsFragmentRoot" );

		rageDiplomat diplomat;

		FCDSceneNode* pRootFragmentNode = NULL;
		ScriptTreaty* pRootFragmentNodeTreaty = 
			new ScriptTreaty( MakeFunctor( &AddRootFragmentElement ), &pRootFragmentNode );
		if ( !pRootFragmentNodeTreaty->InitializeTreatyScript( TREATY_SCRIPT, isFragmentRoot ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			return 1;
		}
		diplomat.RegisterTreaty( pRootFragmentNodeTreaty );

		rageShouldAcceptFtor isSkeletonRoot ( "IsSkeletonRoot" );

		FCDSceneNode* pRootSkeletonNode = NULL;
		ScriptTreaty* pRootSkeletonNodeTreaty = 
			new ScriptTreaty( MakeFunctor( &AddRootSkeletonElement ), &pRootSkeletonNode );
		if ( !pRootSkeletonNodeTreaty->InitializeTreatyScript( TREATY_SCRIPT, isSkeletonRoot ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			return 1;
		}
		diplomat.RegisterTreaty( pRootSkeletonNodeTreaty );

		diplomat.VisitBreadthFirst( m_document->GetVisualSceneInstance(), rageDiplomat::MULTIPLE_SIGNING );

		Assert( pRootFragmentNode && pRootSkeletonNode );

		ReRootSkeleton( pRootFragmentNode, pRootSkeletonNode );

		SHUTDOWN_LUASCRIPTHOST;

		return 0;
	}

	rageIConditioner::Register< rageFragmentProxy > g_FragmentProxy;
}
