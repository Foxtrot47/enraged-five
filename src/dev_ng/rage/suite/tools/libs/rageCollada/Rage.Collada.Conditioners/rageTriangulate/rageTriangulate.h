#ifndef __RAGE_TRIANGULATE__
#define __RAGE_TRIANGULATE__
#pragma once 

#include "rageColladaConditioner.h"
#include "rageColladaExport.h"

namespace rage
{

	class rageTriangulate : public rageConditioner
	{
	public:
		DLL_EXPORT bool Init();
		DLL_EXPORT int Execute();

		DLL_EXPORT std::string GetBaseName() const { return "rageTriangulate"; }
		DLL_EXPORT std::string GetDescription() const { return "rageTriangulate"; }
	};

	class rageTriangulateProxy : public rageConditionerCreator
	{
		DLL_EXPORT std::string GetBaseName() const { return "rageTriangulate"; }
		DLL_EXPORT std::string GetDescription() const { return "rageTriangulate"; }

		DLL_EXPORT rageIConditioner* Create() const { return new rageTriangulate(); }
	};

} // rage

#endif // __RAGE_TRIANGULATE__
