-------------------------------------------------------------------------------

function CalcTexBasisForNode( sceneNode )

	-- TODO: CHECK THAT IT'S NOT A GEOMETRY BOUND

	rage.Display( sceneNode:GetName():c_str() )

	instanceCount = sceneNode:GetInstanceCount()
	
	rage.Display( instanceCount )
	
	if ( instanceCount > 0 ) then
	
		for i = 0, instanceCount - 1 do
			
			instance = sceneNode:GetInstance( i )
			rage.Display( "instance type " .. instance:GetType() )
			if ( instance:GetType() == FCollada.FCDEntityInstance_GEOMETRY ) then
				return 1
			end
			
		end
		
	end	
	
	return 0

end

-------------------------------------------------------------------------------

function FindTexCoordSource( geometry )

	mesh = geometry:GetMesh()
	
	sourceCount = mesh:GetSourceCount()
	for i = 0, sourceCount - 1 do
		source = mesh:GetSource( i )
		if ( source:GetType() == FCollada.FUDaeGeometryInput_TEXCOORD ) then
			return source;
		end
	end
	
	return nil
	
end

-------------------------------------------------------------------------------



