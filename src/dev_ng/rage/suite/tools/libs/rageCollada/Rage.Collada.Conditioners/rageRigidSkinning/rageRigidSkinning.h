#ifndef __RAGE_RIGID_SKINNING_H__
#define __RAGE_RIGID_SKINNING_H__
#pragma once

#include "rageColladaConditioner.h"
#include "rageColladaExport.h"

namespace rage 
{
	class rageRigidSkinning : public rageConditioner
	{
	public:
		DLL_EXPORT bool Init();
		DLL_EXPORT int Execute();

		DLL_EXPORT std::string GetBaseName() const { return "rageRigidSkinning"; }
		DLL_EXPORT std::string GetDescription() const { return "rageRigidSkinning"; }
	};

	class rageRigidSkinningProxy : public rageConditionerCreator
	{
	public:
		DLL_EXPORT std::string GetBaseName() const { return "rageRigidSkinning"; }
		DLL_EXPORT std::string GetDescription() const { return "rageRigidSkinning"; }

		DLL_EXPORT rageIConditioner* Create() const { return new rageRigidSkinning(); }
	};
} // rage

#endif // __RAGE_RIGID_SKINNING_H__
