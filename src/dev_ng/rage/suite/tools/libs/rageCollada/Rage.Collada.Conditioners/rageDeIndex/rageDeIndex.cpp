#include "rageDeIndex.h"

#include "rageColladaHelpers.h"

namespace rage 
{
	bool rageDeIndex::Init()
	{
		printf( "Deindex::Init()\n" );

		return true;
	}

	int rageDeIndex::Execute()
	{
		printf( "Deindex::Execute()\n" );

		FCDSceneNode* pobVisualSceneNode = m_document->GetVisualSceneInstance();
		if(!pobVisualSceneNode)
		{
			Errorf("No visual scene node found in scene, nothing to deindex");
			return -1;
		}

		rage::Deindex( pobVisualSceneNode );

		FCollada::SaveDocument( m_document, "deindexed.dae" );

		return 0;
	}

	rageIConditioner::Register< rageDeIndexProxy > g_DeindexProxy;
}

