#include "rageZeroRotationBindPose.h"

#include "rageColladaHelpers.h"

namespace rage 
{
	bool rageZeroRotationBindPose::Init()
	{
		printf( "rageZeroRotationBindPose::Init()\n" );

		return true;
	}

	int rageZeroRotationBindPose::Execute()
	{
		printf( "rageZeroRotationBindPose::Execute()\n" );

		rage::SetZeroRotationBindPose( m_document->GetVisualSceneInstance() );

		FCollada::SaveDocument( m_document, "zero-rotation-bind-posed.dae" );

		return 0;
	}

	rageIConditioner::Register< rageZeroRotationBindPoseProxy > g_zeroRotationBindPoseProxy;
}
