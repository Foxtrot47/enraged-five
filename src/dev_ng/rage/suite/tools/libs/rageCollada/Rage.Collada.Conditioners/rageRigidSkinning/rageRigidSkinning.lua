
-------------------------------------------------------------------------------

function DoRigidSkinning( sceneNode )

	if ( string.find( sceneNode:GetName():c_str(), "Bound" ) ~= nil ) then
		return 0
	end
	
	return 1
	
end

-------------------------------------------------------------------------------

function IsRigidSkinningRoot( sceneNode )

	rage.Display( "testing " .. sceneNode:GetName():c_str() )

	if ( sceneNode:GetName():c_str() == "root" ) then
		return 1
	end
	
	return 0

end

