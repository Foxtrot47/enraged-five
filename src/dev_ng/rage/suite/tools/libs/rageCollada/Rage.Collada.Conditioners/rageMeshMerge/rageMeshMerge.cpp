#include "rageMeshMerge.h"

#include "rageColladaHelpers.h"

#include "rageLuaScriptHost/rageLuaScriptHost.h"
#include "rageDiplomat/rageDiplomat.h"
#include "rageDiplomat/rageLuaScriptTreaty.h"

#if __OPTIMIZED
#define FCOLLADALUA_PATH	"FColladaLua.dll"
#else
#define FCOLLADALUA_PATH	"C:\\soft\\rage_head\\rage\\suite\\tools\\rageCollada\\FColladaSWIG\\FColladaLua\\Debug\\FColladaLua.dll"
#endif
#define FCOLLADALUA_INIT_FN	"luaopen_FCollada"

#define TREATY_SCRIPT "C:\\soft\\rage\\suite\\tools\\rageCollada\\Rage.Collada.Conditioners\\rageMeshMerge\\rageMeshMerge.lua"

namespace {
	
	void AddMeshMergeElement( FCDSceneNode* pNode, void* pData )
	{
		FCDSceneNodeList* pMeshMergeList = reinterpret_cast< FCDSceneNodeList* >( pData );
		pMeshMergeList->push_back( pNode );
	}

	void AddMeshMergeRoot( FCDSceneNode* pNode, void* pData )
	{
		FCDSceneNode** pRootNode = reinterpret_cast< FCDSceneNode** >( pData );
		*pRootNode = pNode;
	}
}

namespace rage 
{

	bool rageMeshMerge::Init()
	{
		return true;
	}

	int rageMeshMerge::Execute()
	{
		INIT_LUASCRIPTHOST;

		if( !LUASCRIPTHOST.LoadExtension( FCOLLADALUA_PATH, FCOLLADALUA_INIT_FN, NULL ) )
		{		
			Errorf( "Failed to load LUA extension : '%s'", FCOLLADALUA_PATH );
			return 1;
		}

		typedef rageLuaScriptTreaty< rageShouldAcceptFtor > ScriptTreaty;

		rageShouldAcceptFtor doMergeNode ( "DoMergeNode" );

		FCDSceneNodeList mergeMeshes;
		ScriptTreaty* pMeshMergeScriptTreaty = 
			new ScriptTreaty( MakeFunctor( &AddMeshMergeElement ), &mergeMeshes );
		if ( !pMeshMergeScriptTreaty->InitializeTreatyScript( TREATY_SCRIPT, doMergeNode ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			return 1;
		}

		rageDiplomat diplomat;
		diplomat.RegisterTreaty( pMeshMergeScriptTreaty );

		rageShouldAcceptFtor isMeshMergeRoot ( "IsMeshMergeRoot" );

		FCDSceneNode* pRootNode = NULL;
		ScriptTreaty* pMeshMergeRootTreaty = 
			new ScriptTreaty( MakeFunctor( &AddMeshMergeRoot ), &pRootNode );
		if ( !pMeshMergeRootTreaty->InitializeTreatyScript( TREATY_SCRIPT, isMeshMergeRoot ) )
		{
			Errorf( "There was an error registering a scripted treaty" );
			return 1;
		}

		diplomat.RegisterTreaty( pMeshMergeRootTreaty );

		FCDVisualSceneNodeLibrary* pSceneNodeLibrary = m_document->GetVisualSceneLibrary();
		size_t sceneCount = pSceneNodeLibrary->GetEntityCount();
		for ( size_t sceneIndex = 0; sceneIndex < sceneCount; ++sceneIndex )
		{
			FCDSceneNode* pSceneRoot = pSceneNodeLibrary->GetEntity( sceneIndex );
			diplomat.VisitDepthFirst( pSceneRoot );
		}
		
		// find a common parent and use it for the merge.

		FCDSceneNode* pParent = pRootNode->GetParent();
		Assert( NULL != pParent );
		FCDSceneNode* pNewSceneNode = pParent->AddChildNode();
		pNewSceneNode->SetDaeId( "merged_node" );
		pNewSceneNode->SetName( "merged_node" );

		MergeControllers( pNewSceneNode, mergeMeshes );

		FCollada::SaveDocument( m_document, "merged-meshes.dae" );

		SHUTDOWN_LUASCRIPTHOST;

		return 0;
	}

	rageIConditioner::Register< rageMeshMergeProxy > g_MeshMergeProxy;

} // rage
