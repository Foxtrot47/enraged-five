#include "rageTextureTangentBasis.h"

#include "rageColladaHelpers.h"

#include "rageLuaScriptHost/rageLuaScriptHost.h"
#include "rageDiplomat/rageDiplomat.h"
#include "rageDiplomat/rageLuaScriptTreaty.h"

#if __OPTIMIZED
#define FCOLLADALUA_PATH	"FColladaLua.dll"
#else
#define FCOLLADALUA_PATH	"C:\\soft\\rage_head\\rage\\suite\\tools\\rageCollada\\FColladaSWIG\\FColladaLua\\Debug\\FColladaLua.dll"
#endif
#define FCOLLADALUA_CONVERT_FN	"lua_GetResult"
#define FCOLLADALUA_INIT_FN	"luaopen_FCollada"

#define TREATY_SCRIPT "C:\\soft\\rage\\suite\\tools\\rageCollada\\Rage.Collada.Conditioners\\rageTextureTangentBasis\\rageTextureTangentBasis.lua"

namespace 
{
	static void CallbackThunk( FCDSceneNode*, void* ) { }

	struct CalcTexTangentData
	{
		FCDGeometry* pGeometry;					// The to be processed
		FCDGeometrySource* pGeometrySource;		// UVs to use in calculation
	};
	typedef std::vector< CalcTexTangentData > CalcTexTangentDataList;

	class AcceptFtor
	{
	public:
		AcceptFtor( const char* szAcceptNodeFnName, const char* szFindSourceFnName )
			:	m_nodeFnName ( szAcceptNodeFnName )
			,	m_sourceFnName ( szFindSourceFnName ) { }
		AcceptFtor() {}
	public:
		const CalcTexTangentDataList& GetTexTangentDataList() const
		{
			return m_data;
		}

		bool operator()( FCDSceneNode* pNode )
		{
			int accept = 
				rageLuaScriptHostCall< int , rageLshSwigObjWrap > (
					m_nodeFnName.c_str() ).call( 
						rageLshSwigObjWrap( "FCollada", "FCDSceneNode_p_ctor", pNode ) 
					);

			if ( accept )
			{
				FCDEntityInstanceContainer& instances = pNode->GetInstances();
				for ( FCDEntityInstanceContainer::iterator it = instances.begin(); it != instances.end(); ++it )
				{
					FCDEntityInstance* pEntityInstance = (*it);
					
					if ( FCDEntityInstance::GEOMETRY == pEntityInstance->GetType() )
					{
						FCDGeometry* pGeometry = 
							reinterpret_cast< FCDGeometry* >( pEntityInstance->GetEntity() );

						FCDGeometrySource* pSource = 
							rageLuaScriptHostCall< FCDGeometrySource* , rageLshSwigObjWrap > (
								m_sourceFnName.c_str()).call( 
									rageLshSwigObjWrap( "FCollada", "FCDGeometry_p_ctor", pGeometry ) 
								);

						Assert( FUDaeGeometryInput::TEXCOORD == pSource->GetType() );

						if ( NULL != pSource )
						{
							CalcTexTangentData data = { pGeometry, pSource };
							m_data.push_back( data );
						}
					}
				}
			}

			// always return false
			return false;
		}
	private:
		CalcTexTangentDataList m_data;

		std::string m_nodeFnName;		// Script function to determine if we should process this node
		std::string m_sourceFnName;		// Script function to determine if which source to use for geometry
	};
}

namespace rage {

bool rageTextureTangentBasis::Init()
{
	return true;
}

int rageTextureTangentBasis::Execute()
{
	INIT_LUASCRIPTHOST;
	
	if( !LUASCRIPTHOST.LoadExtension( FCOLLADALUA_PATH, FCOLLADALUA_INIT_FN, FCOLLADALUA_CONVERT_FN ) )
	{		
		Errorf( "Failed to load LUA extension : '%s'", FCOLLADALUA_PATH );
		return 1;
	}

	AcceptFtor ftor ( "CalcTexBasisForNode", "FindTexCoordSource" );

	typedef rageLuaScriptTreaty< AcceptFtor > ScriptTreaty;
	ScriptTreaty* pTreaty =
		new ScriptTreaty( MakeFunctor( &CallbackThunk ), NULL );
	if ( !pTreaty->InitializeTreatyScript( TREATY_SCRIPT, ftor ) )
	{
		Errorf( "There was an error registering a scripted treaty" );
		return 1;
	}

	rageDiplomat diplomat;
	diplomat.RegisterTreaty( pTreaty );

	diplomat.VisitDepthFirst( m_document->GetVisualSceneInstance() );

	const CalcTexTangentDataList& data = ftor.GetTexTangentDataList();
	for ( CalcTexTangentDataList::const_iterator it = data.begin(); it != data.end(); ++it )
	{
		rage::CalcTextureTangentBasis( it->pGeometry, it->pGeometrySource );
	}
	
	SHUTDOWN_LUASCRIPTHOST;

	FCollada::SaveDocument( m_document, "tangent-basis.dae" );

	return 0;
}

rageIConditioner::Register< rageTextureTangentBasisProxy > g_TextureTangentBasisProxy;

} // rage
