#include "rageCopyright.h"

#pragma warning ( push )
#pragma warning ( disable : 4702 ) // unreachable code

#include "FCDocument/FCDAsset.h"

#pragma warning ( pop )

namespace rage {

bool rageCopyright::Init()
{
	printf( "Copyright::Init()\n" );

	return true;
}

int rageCopyright::Execute()
{
	printf( "Copyright::Execute()\n" );

	FCDAsset* asset = m_document->GetAsset();

	if ( 0 == asset->GetContributorCount() )
	{
		FCDAssetContributor* contributer = asset->AddContributor();
		contributer->SetCopyright( TO_FSTRING( "(C) Copyright Rockstar Games" ) );
	}
	else
	{
		for ( size_t i = 0; i < asset->GetContributorCount(); ++i )
		{
			FCDAssetContributor* contributer = asset->GetContributor( i );
			contributer->SetCopyright(  TO_FSTRING( "(C) Copyright Rockstar Games" ) );
		}
	}

	return 0;
}

rageIConditioner::Register< rageCopyrightProxy > g_CopyrightProxy;

} // rage


