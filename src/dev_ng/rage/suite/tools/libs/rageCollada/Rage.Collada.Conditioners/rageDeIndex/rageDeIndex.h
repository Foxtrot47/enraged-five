#ifndef __RAGE_DEINDEX__
#define __RAGE_DEINDEX__
#pragma once

#include "rageColladaConditioner.h"
#include "rageColladaExport.h"

namespace rage 
{

class rageDeIndex : public rageConditioner
{
public:
	DLL_EXPORT bool Init();
	DLL_EXPORT int Execute();

	DLL_EXPORT std::string GetBaseName() const { return "rageDeIndex"; }
	DLL_EXPORT std::string GetDescription() const { return "rageDeIndex"; }
};

class rageDeIndexProxy : public rageConditionerCreator
{
	DLL_EXPORT std::string GetBaseName() const { return "rageDeIndex"; }
	DLL_EXPORT std::string GetDescription() const { return "rageDeIndex"; }

	DLL_EXPORT rageIConditioner* Create() const { return new rageDeIndex(); }
};

} // rage

#endif // __RAGE_DEINDEX__
