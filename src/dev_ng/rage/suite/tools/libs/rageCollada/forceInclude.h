// /forceInclude.h

#ifndef __RAGECOLLADA_FORCEINCLUDE_H__
#define __RAGECOLLADA_FORCEINCLUDE_H__

#define _WIN32_WINNT 0x0500
#pragma pack( push )
#pragma warning(disable: 4996)
#include <windows.h>
#pragma pack( pop )

#include "FCollada.h"

#include "FUtils/FUAssert.h"
#include "FUtils/FUBase64.h"
#include "FUtils/FUBoundingBox.h"
#include "FUtils/FUBoundingSphere.h"
#include "FUtils/FUCrc32.h"
#include "FUtils/FUDaeEnum.h"
#include "FUtils/FUDaeEnumSyntax.h"
#include "FUtils/FUDaeSyntax.h"
#include "FUtils/FUDateTime.h"
#include "FUtils/FUDebug.h"
#include "FUtils/FUError.h"
#include "FUtils/FUEvent.h"
#include "FUtils/FUFile.h"
#include "FUtils/FUFileManager.h"
#include "FUtils/FUFunctor.h"
#include "FUtils/FULogFile.h"
#include "FUtils/FUObject.h"
#include "FUtils/FUObjectType.h"
#include "FUtils/FUPlugin.h"
#include "FUtils/FUPluginManager.h"
#include "FUtils/FUSingleton.h"
#include "FUtils/FUString.h"
#include "FUtils/FUStringBuilder.h"
#include "FUtils/FUStringConversion.h"
#include "FUtils/FUTestBed.h"
#include "FUtils/FUtils.h"
#include "FUtils/FUUniqueStringMap.h"
#include "FUtils/FUUri.h"
#include "FUtils/FUXmlDocument.h"
#include "FUtils/FUXmlParser.h"
#include "FUtils/FUXmlWriter.h"
#include "FUtils/Platforms.h"

#include "FCDocument/FCDAnimated.h"
#include "FCDocument/FCDAnimation.h"
#include "FCDocument/FCDAnimationChannel.h"
#include "FCDocument/FCDAnimationClip.h"
#include "FCDocument/FCDAnimationClipTools.h"
#include "FCDocument/FCDAnimationCurve.h"
#include "FCDocument/FCDAnimationCurveTools.h"
#include "FCDocument/FCDAnimationKey.h"
#include "FCDocument/FCDAnimationMultiCurve.h"
#include "FCDocument/FCDAsset.h"
#include "FCDocument/FCDCamera.h"
#include "FCDocument/FCDController.h"
#include "FCDocument/FCDControllerInstance.h"
#include "FCDocument/FCDControllerTools.h"
#include "FCDocument/FCDEffect.h"
#include "FCDocument/FCDEffectCode.h"
#include "FCDocument/FCDEffectParameter.h"
#include "FCDocument/FCDEffectParameterFactory.h"
#include "FCDocument/FCDEffectParameterSampler.h"
#include "FCDocument/FCDEffectParameterSurface.h"
#include "FCDocument/FCDEffectPass.h"
#include "FCDocument/FCDEffectPassShader.h"
#include "FCDocument/FCDEffectPassState.h"
#include "FCDocument/FCDEffectProfile.h"
#include "FCDocument/FCDEffectProfileFX.h"
#include "FCDocument/FCDEffectStandard.h"
#include "FCDocument/FCDEffectTechnique.h"
#include "FCDocument/FCDEffectTools.h"
#include "FCDocument/FCDEmitter.h"
#include "FCDocument/FCDEmitterInstance.h"
#include "FCDocument/FCDEmitterObject.h"
#include "FCDocument/FCDEmitterParticle.h"
#include "FCDocument/FCDEntity.h"
#include "FCDocument/FCDEntityInstance.h"
#include "FCDocument/FCDEntityReference.h"
#include "FCDocument/FCDExternalReferenceManager.h"
#include "FCDocument/FCDExtra.h"
#include "FCDocument/FCDForceDeflector.h"
#include "FCDocument/FCDForceDrag.h"
#include "FCDocument/FCDForceField.h"
#include "FCDocument/FCDForceGravity.h"
#include "FCDocument/FCDForcePBomb.h"
#include "FCDocument/FCDForceTyped.h"
#include "FCDocument/FCDForceWind.h"
#include "FCDocument/FCDGeometry.h"
#include "FCDocument/FCDGeometryInstance.h"
#include "FCDocument/FCDGeometryMesh.h"
#include "FCDocument/FCDGeometryNURBSSurface.h"
#include "FCDocument/FCDGeometryPolygons.h"
#include "FCDocument/FCDGeometryPolygonsInput.h"
#include "FCDocument/FCDGeometryPolygonsTools.h"
#include "FCDocument/FCDGeometrySource.h"
#include "FCDocument/FCDGeometrySpline.h"
#include "FCDocument/FCDImage.h"
#include "FCDocument/FCDLibrary.h"
#include "FCDocument/FCDLight.h"
#include "FCDocument/FCDLightTools.h"
#include "FCDocument/FCDMaterial.h"
#include "FCDocument/FCDMaterialInstance.h"
#include "FCDocument/FCDMorphController.h"
#include "FCDocument/FCDObject.h"
#include "FCDocument/FCDObjectWithId.h"
#include "FCDocument/FCDocument.h"
#include "FCDocument/FCDocumentTools.h"
#include "FCDocument/FCDParameterAnimatable.h"
#include "FCDocument/FCDParticleEmitter.h"
#include "FCDocument/FCDParticleModifier.h"
#include "FCDocument/FCDPhysicsAnalyticalGeometry.h"
#include "FCDocument/FCDPhysicsForceFieldInstance.h"
#include "FCDocument/FCDPhysicsMaterial.h"
#include "FCDocument/FCDPhysicsModel.h"
#include "FCDocument/FCDPhysicsModelInstance.h"
#include "FCDocument/FCDPhysicsRigidBody.h"
#include "FCDocument/FCDPhysicsRigidBodyInstance.h"
#include "FCDocument/FCDPhysicsRigidBodyParameters.h"
#include "FCDocument/FCDPhysicsRigidConstraint.h"
#include "FCDocument/FCDPhysicsRigidConstraintInstance.h"
#include "FCDocument/FCDPhysicsScene.h"
#include "FCDocument/FCDPhysicsShape.h"
#include "FCDocument/FCDPlaceHolder.h"
#include "FCDocument/FCDSceneNode.h"
#include "FCDocument/FCDSceneNodeIterator.h"
#include "FCDocument/FCDSceneNodeTools.h"
#include "FCDocument/FCDSkinController.h"
#include "FCDocument/FCDTargetedEntity.h"
#include "FCDocument/FCDTexture.h"
#include "FCDocument/FCDTransform.h"
#include "FCDocument/FCDVersion.h"


#include "FMath/FMAllocator.h"
#include "FMath/FMArray.h"
#include "FMath/FMArrayPointer.h"
#include "FMath/FMath.h"
#include "FMath/FMColor.h"
#include "FMath/FMFloat.h"
#include "FMath/FMInteger.h"
#include "FMath/FMInterpolation.h"
#include "FMath/FMMatrix33.h"
#include "FMath/FMMatrix44.h"
#include "FMath/FMQuaternion.h"
#include "FMath/FMRandom.h"
#include "FMath/FMSort.h"
#include "FMath/FMTree.h"
#include "FMath/FMVector2.h"
#include "FMath/FMVector3.h"
#include "FMath/FMVector4.h"
#include "FMath/FMVolume.h"

#ifdef DEPRECATED
#undef DEPRECATED
#endif

#ifdef _CRT_SECURE_NO_DEPRECATE
#undef _CRT_SECURE_NO_DEPRECATE
#endif

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#if defined(_DEBUG)
#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG)
#include "forceinclude/win32_toolrelease.h"
#else
#include "forceinclude/win32_toolbeta.h"
#endif

#endif //__RAGECOLLADA_FORCEINCLUDE_H__

