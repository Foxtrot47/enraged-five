// Examples of usage:
//
// Rage.Collada.MergeFiles.exe t:\mergedColladaDoc.dae t:\Sphere.dae t:\Cube.dae t:\Cubes.dae
// Rage.Collada.MergeFiles.exe t:\mergedColladaDoc.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hi_va_rd_navmesh_01x.dae 
// Rage.Collada.MergeFiles.exe t:\mergedColladaDoc.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hi_va_rd_navmesh_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_01x.dae 
// Rage.Collada.MergeFiles.exe t:\mergedColladaDoc.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hi_va_rd_navmesh_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_03x.dae 
// Rage.Collada.MergeFiles.exe t:\mergedColladaDoc.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hi_va_rd_navmesh_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_03x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_04x.dae 
// Rage.Collada.MergeFiles.exe t:\mergedColladaDoc.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hi_va_rd_navmesh_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_03x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_04x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hw_oh_blk_04a.dae 
// Rage.Collada.MergeFiles.exe t:\mergedColladaDoc.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hi_va_rd_navmesh_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_01x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_03x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_04x.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hw_oh_blk_04a.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hw_ww_blk_03b.dae 
// Rage.Collada.MergeFiles.exe T:\mc4\assets\city\la_Pipeline5\SectorColladaFiles\be_smb_02_north.dae T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hi_va_rd_000x.dae;T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hi_va_rd_navmesh_01x.dae;T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_01x.dae;T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_03x.dae;T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hl_va_blk_04x.dae;T:\mc4\assets\city\la_Pipeline5\MayaFilesAsCollada\be_smb_02_north\la_hw_oh_blk_04a.dae

// #define CAPTURE_EXCEPTIONS

using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Reflection;
using rageUsefulCSharpToolClasses;

namespace Rage.Collada.MergeFiles
{
	class Program
	{
		[Conditional("DEBUG")]
		public static void DebugPrintTrace()
		{
			StackTrace st = new StackTrace(true);
			StackFrame sf = st.GetFrame(1);
			Console.WriteLine("Trace "
				+ sf.GetFileName() + " "
				+ sf.GetMethod().Name + ":"
				+ sf.GetFileLineNumber() + "\n");
		}

		/*
		static void MergePhysicsSceneLibrary(FColladaDocumentPhysicsSceneLibrary obSourcePhysicsSceneLibraryNode, FColladaDocumentPhysicsSceneLibrary obDestinationPhysicsSceneLibraryNode, ref Dictionary<string, string> obOldIdToNewIdDictionary)
		{
			for (uint iSourceEntityNo = 0; iSourceEntityNo < obSourcePhysicsSceneLibraryNode.GetEntityCount(); iSourceEntityNo++)
			{
				// Get PhysicsScene
				FCDPhysicsScene obSourcePhysicsScene = obSourcePhysicsSceneLibraryNode.GetEntity(iSourceEntityNo);

				// Do I already have a PhysicsScene with the same name?
				FCDPhysicsScene obDestinationPhysicsScene = null;
				for (uint iDestinationEntityNo = 0; iDestinationEntityNo < obDestinationPhysicsSceneLibraryNode.GetEntityCount(); iDestinationEntityNo++)
				{
					// Get PhysicsScene
					if (
						(obDestinationPhysicsSceneLibraryNode.GetEntity(iDestinationEntityNo).GetName().c_str() == obSourcePhysicsScene.GetName().c_str())
						||
						(obDestinationPhysicsSceneLibraryNode.GetEntity(iDestinationEntityNo).GetDaeId().c_str() == obSourcePhysicsScene.GetDaeId().c_str())
						)
					{
						// Bingo!
						obDestinationPhysicsScene = obDestinationPhysicsSceneLibraryNode.GetEntity(iDestinationEntityNo);
						break;
					}
				}

				if (obDestinationPhysicsScene != null)
				{
					// I already have a PhysicsScene with this name, oh dear
					// Ideally I would combine them if they are the same here, but for now just rename it
					string strInputColladaFile = obSourcePhysicsScene.GetDocument().GetFileUrl().c_str();
					string strOldName = obSourcePhysicsScene.GetName().c_str();
					string strNewName = strOldName + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					string strOldId = obSourcePhysicsScene.GetDaeId().c_str();
					string strNewId = strOldId + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					obSourcePhysicsScene.SetDaeId(new fstring(strNewId));
					obSourcePhysicsScene.SetName(new fstring(strNewName));
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldId))
					{
						obOldIdToNewIdDictionary.Add(strOldId, strNewId);
					}
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldName))
					{
						obOldIdToNewIdDictionary.Add(strOldName, strNewName);
					}
				}
				obDestinationPhysicsSceneLibraryNode.AddEntity(obSourcePhysicsScene);
			}
		}
		*/

		static void MergeEffectLibrary(FColladaDocumentEffectLibrary obSourceEffectLibraryNode, FColladaDocumentEffectLibrary obDestinationEffectLibraryNode, ref Dictionary<string, string> obOldIdToNewIdDictionary)
		{
			for (uint iSourceEntityNo = 0; iSourceEntityNo < obSourceEffectLibraryNode.GetEntityCount(); iSourceEntityNo++)
			{
				// Get Effect
				FCDEffect obSourceEffect = obSourceEffectLibraryNode.GetEntity(iSourceEntityNo);

				// Do I already have a Effect with the same name?
				FCDEntity obExistingEntityWithTheSameId = obDestinationEffectLibraryNode.GetDocument().FindEntity(FColladaCSharp.FCDEntityToFCDObjectWithId(obSourceEffect).GetDaeId());
				if (obExistingEntityWithTheSameId != null)
				{
					// I already have a Effect with this name, oh dear
					// Ideally I would combine them if they are the same here, but for now just rename it
					string strInputColladaFile = obSourceEffect.GetDocument().GetFileUrl().c_str();
					string strOldName = obSourceEffect.GetName().c_str();
					string strNewName = strOldName + "_" + Guid.NewGuid().ToString(); // Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					string strOldId = obSourceEffect.GetDaeId().c_str();
					string strNewId = strOldId + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					obSourceEffect.SetDaeId(new fstring(strNewId));
					obSourceEffect.SetName(new fstring(strNewName));
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldId))
					{
						obOldIdToNewIdDictionary.Add(strOldId, strNewId);
					}
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldName))
					{
						obOldIdToNewIdDictionary.Add(strOldName, strNewName);
					}
				}
				FCDEffect obDestinationEffect = obDestinationEffectLibraryNode.AddEntity();
				obSourceEffect.Clone(obDestinationEffect, true);
			}
		}

		static void MergeCameraLibrary(FColladaDocumentCameraLibrary obSourceCameraLibraryNode, FColladaDocumentCameraLibrary obDestinationCameraLibraryNode, ref Dictionary<string, string> obOldIdToNewIdDictionary)
		{
			for (uint iSourceEntityNo = 0; iSourceEntityNo < obSourceCameraLibraryNode.GetEntityCount(); iSourceEntityNo++)
			{
				// Get Camera
				FCDCamera obSourceCamera = obSourceCameraLibraryNode.GetEntity(iSourceEntityNo);

				// Do I already have a Camera with the same name?
				FCDEntity obExistingEntityWithTheSameId = obDestinationCameraLibraryNode.GetDocument().FindEntity(obSourceCamera.GetDaeId());
				if (obExistingEntityWithTheSameId != null)
				{
					// I already have a Camera with this name, oh dear
					// Ideally I would combine them if they are the same here, but for now just rename it
					string strInputColladaFile = obSourceCamera.GetDocument().GetFileUrl().c_str();
					string strOldName = obSourceCamera.GetName().c_str();
					string strNewName = strOldName + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					string strOldId = obSourceCamera.GetDaeId().c_str();
					string strNewId = strOldId + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					obSourceCamera.SetDaeId(new fstring(strNewId));
					obSourceCamera.SetName(new fstring(strNewName));
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldId))
					{
						obOldIdToNewIdDictionary.Add(strOldId, strNewId);
					}
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldName))
					{
						obOldIdToNewIdDictionary.Add(strOldName, strNewName);
					}
				}
				FCDCamera obDestinationCamera = obDestinationCameraLibraryNode.AddEntity();
				obSourceCamera.Clone(obDestinationCamera, true);
			}
		}

		static void MergeLightLibrary(FColladaDocumentLightLibrary obSourceLightLibraryNode, FColladaDocumentLightLibrary obDestinationLightLibraryNode, ref Dictionary<string, string> obOldIdToNewIdDictionary)
		{
			for (uint iSourceEntityNo = 0; iSourceEntityNo < obSourceLightLibraryNode.GetEntityCount(); iSourceEntityNo++)
			{
				// Get Light
				FCDLight obSourceLight = obSourceLightLibraryNode.GetEntity(iSourceEntityNo);

				// Do I already have a Light with the same name?
				FCDEntity obExistingEntityWithTheSameId = obDestinationLightLibraryNode.GetDocument().FindEntity(obSourceLight.GetDaeId());
				if (obExistingEntityWithTheSameId != null)
				{
					// I already have a Light with this name, oh dear
					// Ideally I would combine them if they are the same here, but for now just rename it
					string strInputColladaFile = obSourceLight.GetDocument().GetFileUrl().c_str();
					string strOldName = obSourceLight.GetName().c_str();
					string strNewName = strOldName + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					string strOldId = obSourceLight.GetDaeId().c_str();
					string strNewId = strOldId + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					obSourceLight.SetDaeId(new fstring(strNewId));
					obSourceLight.SetName(new fstring(strNewName));
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldId))
					{
						obOldIdToNewIdDictionary.Add(strOldId, strNewId);
					}
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldName))
					{
						obOldIdToNewIdDictionary.Add(strOldName, strNewName);
					}
				}
				FCDLight obDestinationLight = obDestinationLightLibraryNode.AddEntity();
				obSourceLight.Clone(obDestinationLight, true);
			}
		}

		static void MergeMaterialLibrary(FColladaDocumentMaterialLibrary obSourceMaterialLibraryNode, FColladaDocumentMaterialLibrary obDestinationMaterialLibraryNode, ref Dictionary<string, string> obOldIdToNewIdDictionary)
		{
			for (uint iSourceEntityNo = 0; iSourceEntityNo < obSourceMaterialLibraryNode.GetEntityCount(); iSourceEntityNo++)
			{
				// Get material
				FCDMaterial obSourceMaterial = obSourceMaterialLibraryNode.GetEntity(iSourceEntityNo);

				// Do I already have a material with the same name?
				FCDEntity obExistingEntityWithTheSameId = obDestinationMaterialLibraryNode.GetDocument().FindEntity(obSourceMaterial.GetDaeId());
				if (obExistingEntityWithTheSameId != null)
				{
					// I already have a material with this name, oh dear
					// Ideally I would combine them if they are the same here, but for now just rename it
					string strInputColladaFile = obSourceMaterial.GetDocument().GetFileUrl().c_str();
					string strOldName = obSourceMaterial.GetName().c_str();
					string strNewName = strOldName + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					string strOldId = obSourceMaterial.GetDaeId().c_str();
					string strNewId = strOldId + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					obSourceMaterial.SetDaeId(new fstring(strNewId));
					obSourceMaterial.SetName(new fstring(strNewName));
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldId))
					{
						obOldIdToNewIdDictionary.Add(strOldId, strNewId);
					}
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldName))
					{
						obOldIdToNewIdDictionary.Add(strOldName, strNewName);
					}
				}
				FCDMaterial obDestinationMaterial = obDestinationMaterialLibraryNode.AddEntity();
				// Console.WriteLine("Cloning " + obSourceMaterial.GetDaeId().c_str());
				obSourceMaterial.Clone(obDestinationMaterial, true);

				// Get source effect
				FCDEffect obSourceEffect = obSourceMaterial.GetEffect();

				// Find destination effect
				FCDEffect obDestinationEffect = obDestinationMaterial.GetDocument().FindEffect(obSourceEffect.GetDaeId());
				//Console.WriteLine("obSourceEffect = " + obSourceEffect.GetDaeId().c_str());
				//Console.WriteLine("obDestinationEffect = " + obDestinationEffect.GetDaeId().c_str());
				obDestinationMaterial.SetEffect(obDestinationEffect);
			}
		}

		static void RepointIds(FCDGeometrySource obNode, string strOldName, string strNewName, string strOldId, string strNewId)
		{
			// Set myself up
			if (strOldId != "")
			{
				obNode.SetDaeId(new fstring(obNode.GetDaeId().c_str().Replace(strOldId, strNewId)));
			}
			if (strOldName != "")
			{
				obNode.SetName(new fstring(obNode.GetName().c_str().Replace(strOldName, strNewName)));
			}
		}

		static void RepointIds(FCDGeometryMesh obNode, string strOldName, string strNewName, string strOldId, string strNewId)
		{
			// Set myself up
			//			obNode.SetDaeId(new fstring(obNode.GetDaeId().c_str().Replace(strOldId, strNewId)));
			//			obNode.SetName(new fstring(obNode.GetName().c_str().Replace(strOldName, strNewName)));

			// Do my children
			for (uint i = 0; i < obNode.GetVertexSourceCount(); i++)
			{
				RepointIds(obNode.GetVertexSource(i), strOldName, strNewName, strOldId, strNewId);
			}
			for (uint i = 0; i < obNode.GetSourceCount(); i++)
			{
				RepointIds(obNode.GetSource(i), strOldName, strNewName, strOldId, strNewId);
			}
		}

		static void RenameGeometryNode(FCDGeometry obGeometryNode, string strOldName, string strNewName, string strOldId, string strNewId)
		{
			// Set myself up
			if (strOldId != "")
			{
				obGeometryNode.SetDaeId(new fstring(obGeometryNode.GetDaeId().c_str().Replace(strOldId, strNewId)));
			}
			if (strOldName != "")
			{
				obGeometryNode.SetName(new fstring(obGeometryNode.GetName().c_str().Replace(strOldName, strNewName)));
			}

			// Do my children
			if (obGeometryNode.GetMesh() != null)
			{
				RepointIds(obGeometryNode.GetMesh(), strOldName, strNewName, strOldId, strNewId);
			}
		}

		static void MergeGeometryLibrary(FColladaDocumentGeometryLibrary obSourceGeometryLibraryNode, FColladaDocumentGeometryLibrary obDestinationGeometryLibraryNode, ref Dictionary<string, string> obOldIdToNewIdDictionary)
		{
			for (uint iSourceEntityNo = 0; iSourceEntityNo < obSourceGeometryLibraryNode.GetEntityCount(); iSourceEntityNo++)
			{
				// Get Geometry
				FCDGeometry obSourceGeometry = obSourceGeometryLibraryNode.GetEntity(iSourceEntityNo);

				// Do I already have a Geometry with the same name?
				FCDEntity obExistingEntityWithTheSameId = obDestinationGeometryLibraryNode.GetDocument().FindEntity(obSourceGeometry.GetDaeId());
				if (obExistingEntityWithTheSameId != null)
				{
					// I already have a Geometry with this name, oh dear
					// Ideally I would combine them if they are the same here, but for now just rename it
					string strInputColladaFile = obSourceGeometry.GetDocument().GetFileUrl().c_str();
					string strOldName = obSourceGeometry.GetName().c_str();
					string strNewName = strOldName + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					string strOldId = obSourceGeometry.GetDaeId().c_str();
					string strNewId = strOldId + "_" + Guid.NewGuid().ToString(); // Regex.Replace(rageFileUtilities.RemoveFileExtension(strInputColladaFile), @"[^\w\.@-]", "");
					RenameGeometryNode(obSourceGeometry, strOldName, strNewName, strOldId, strNewId);
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldId))
					{
						obOldIdToNewIdDictionary.Add(strOldId, strNewId);
					}
					if (!obOldIdToNewIdDictionary.ContainsKey(strOldName))
					{
						obOldIdToNewIdDictionary.Add(strOldName, strNewName);
					}
				}
				FCDGeometry obDestinationGeometry = obDestinationGeometryLibraryNode.AddEntity();
				obSourceGeometry.Clone(obDestinationGeometry, true);
			}
		}

		static void ForceInstanceToBeLocal(FCDMaterialInstance obInstance)
		{
			// Geometries have instance materials that need repointing
			FCDMaterial obOldMaterial = obInstance.GetMaterial();
			FCDMaterial obNewMaterial = obInstance.GetDocument().FindMaterial(obOldMaterial.GetDaeId());
			// Console.WriteLine("obOldMaterial = " + obOldMaterial.GetDaeId().c_str());
			// Console.WriteLine("obNewMaterial = " + obNewMaterial.GetDaeId().c_str());
			obInstance.SetMaterial(obNewMaterial);
		}

		static void ForceInstanceToBeLocal(FCDGeometryInstance obInstance)
		{
			// Geometries have instance materials that need repointing
			for (uint iInstanceNo = 0; iInstanceNo < obInstance.GetMaterialInstanceCount(); iInstanceNo++)
			{
				ForceInstanceToBeLocal(obInstance.GetMaterialInstance(iInstanceNo));
			}
		}

		static void ForceInstanceToBeLocal(FCDEntityInstance obInstance)
		{
			//Console.WriteLine("obInstance.GetName() = " + obInstance.GetName().c_str());
			//Console.WriteLine("obInstance.GetAbsolutePath() = " + obInstance.GetEntityUri().GetAbsolutePath().c_str());
			//Console.WriteLine("obInstance.GetAbsoluteUri() = " + obInstance.GetEntityUri().GetAbsoluteUri().c_str());
			//Console.WriteLine("obInstance.GetAuthority() = " + obInstance.GetEntityUri().GetAuthority().c_str());
			//Console.WriteLine("obInstance.GetFragment() = " + obInstance.GetEntityUri().GetFragment().c_str());
			//Console.WriteLine("obInstance.GetHashCode() = " + obInstance.GetEntityUri().GetHashCode());
			//Console.WriteLine("obInstance.GetHostname() = " + obInstance.GetEntityUri().GetHostname().c_str());
			//Console.WriteLine("obInstance.GetPath() = " + obInstance.GetEntityUri().GetPath().c_str());
			//Console.WriteLine("obInstance.GetScheme() = " + obInstance.GetEntityUri().GetScheme());
			//Console.WriteLine("obInstance.GetType() = " + obInstance.GetEntityUri().GetType());
			string strFragment = obInstance.GetEntityUri().GetFragment().c_str();
			if ((strFragment == null) || (strFragment == ""))
			{
				// I can't help here, it is a lost cause, so bail
				return;
			}
			FCDEntity obOldEntity = obInstance.GetEntity();
			if (obOldEntity != null)
			{
				FCDEntity obNewEntity = obInstance.GetDocument().FindEntity(obOldEntity.GetDaeId());
				//Console.WriteLine("obOldEntity = " + obOldEntity.GetDaeId().c_str());
				//Console.WriteLine("obNewEntity = " + obNewEntity.GetDaeId().c_str());
				obInstance.SetEntity(obNewEntity);

				switch (obInstance.GetType())
				{
					case FCDEntityInstance.FCDEntityInstance_Type.SIMPLE:
						{
							// Nothing special needed for simple instances
							break;
						}
					case FCDEntityInstance.FCDEntityInstance_Type.GEOMETRY:
						{
							// Geometries have instance materials that need repointing
							ForceInstanceToBeLocal(FColladaCSharp.FCDEntityInstanceToFCDGeometryInstance(obInstance));
							break;
						}
					default:
						{
							Console.Error.WriteLine("Unable to handle instance of type " + obInstance.GetType());
							break;
						}
				}
			}
		}

		static void CopySceneNode(FCDSceneNode obSourceNode, FCDSceneNode obDestinationNode)
		{
			// Console.WriteLine("CopySceneNode(FCDSceneNode obSourceNode, FCDSceneNode obDestinationNode)");
			// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace(); 
			//if (obDestinationNode.GetTransformCount() > 0)
			//{
			//    obDestinationNode.GetTransforms().clear();
			//}
			// Ideally I could clone obSourceNode and all its children by calling: obSourceNode.Clone(obDestinationNode, true);
			// Unfortunately this breaks if one of the children has an instance that references an external file
			// So have to do it manuall and check for that
			// Check if my instances are worth cloning
			bool bDangerousToClone = false;
			for (uint iInstCount = 0; iInstCount < obSourceNode.GetInstanceCount(); iInstCount++)
			{
				FCDEntityInstance obSourceInstance = obSourceNode.GetInstance(iInstCount);
				// Console.WriteLine("Instance : " + obSourceInstance.GetEntityUri().GetPath().c_str() + " " + obSourceInstance.IsExternalReference());
				// Console.WriteLine("Instance type : " + obSourceInstance.GetEntityType());
				if (obSourceInstance.IsExternalReference())
				{
					bDangerousToClone = true;
					break;
				}

				// obSourceInstance doesn't have an external reference, but does its materials
				if (obSourceInstance.GetEntityType() == FCDEntity.FCDEntity_Type.GEOMETRY)
				{
					FCDGeometryInstance obSourceGeometryInstance = FColladaCSharp.FCDEntityInstanceToFCDGeometryInstance(obSourceInstance);
					for (uint iMaterialCount = 0; iMaterialCount < obSourceGeometryInstance.GetMaterialInstanceCount(); iMaterialCount++)
					{
						FCDMaterialInstance obMaterialInstance = obSourceGeometryInstance.GetMaterialInstance(iMaterialCount);
						if (obMaterialInstance.IsExternalReference())
						{
							bDangerousToClone = true;
							break;
						}
					}
				}
			}
			// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();
			if (!bDangerousToClone)
			{
				// Console.WriteLine("Cloning : " + obSourceNode.GetDaeId().c_str() + " " + obSourceNode.GetType());
				obSourceNode.Clone(obDestinationNode);
			}
			else
			{
				// I have troublesome external instances
				obDestinationNode.SetDaeId(obSourceNode.GetDaeId());
				obDestinationNode.SetName(obSourceNode.GetName());
				for (uint iInstCount = 0; iInstCount < obSourceNode.GetInstanceCount(); iInstCount++)
				{
					FCDEntityInstance obSourceInstance = obSourceNode.GetInstance(iInstCount);
					FCDEntityInstance obDestinationInstance = obDestinationNode.AddInstance(obSourceInstance.GetEntityType());
					// Console.WriteLine("obSourceInstance : " + obSourceInstance.GetEntityUri().GetPath().c_str());
					// Console.WriteLine("obDestinationInstance : " + obDestinationInstance.GetEntityUri().GetPath().c_str());
					obDestinationInstance.SetEntityUri(obSourceInstance.GetEntityUri());
					// Console.WriteLine("obSourceInstance : " + obSourceInstance.GetEntityUri().GetPath().c_str());
					// Console.WriteLine("obDestinationInstance : " + obDestinationInstance.GetEntityUri().GetPath().c_str());
				}
			}
			// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();

			for (uint iChildNo = 0; iChildNo < obSourceNode.GetChildrenCount(); iChildNo++)
			{
				FCDSceneNode obDestinationChildNode = obDestinationNode.AddChildNode();
				CopySceneNode(obSourceNode.GetChild(iChildNo), obDestinationChildNode);
			}
			// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();

			// Console.WriteLine(obSourceNode.GetDaeId().c_str() + " has " + obSourceNode.GetChildrenCount() + " children");
			// Console.WriteLine(obDestinationNode.GetDaeId().c_str() + " has " + obDestinationNode.GetChildrenCount() + " children");
			// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();

			// Repoint references
			for (uint iInstanceNo = 0; iInstanceNo < obDestinationNode.GetInstanceCount(); iInstanceNo++)
			{
				// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();
				// if (!obSourceNode.GetInstance(iInstanceNo).IsExternalReference())
				FCDEntityInstance obInstance = obDestinationNode.GetInstance(iInstanceNo);
				// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();
				// Console.WriteLine("obInstance : " + obInstance.GetEntityUri().GetPath().c_str());
				if (obInstance.IsExternalReference())
				{
					// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();
					//Console.WriteLine("obInstance.GetName() = " + obInstance.GetName().c_str());
					//Console.WriteLine("obInstance.GetAbsolutePath() = " + obInstance.GetEntityUri().GetAbsolutePath().c_str());
					//Console.WriteLine("obInstance.GetAbsoluteUri() = " + obInstance.GetEntityUri().GetAbsoluteUri().c_str());
					//Console.WriteLine("obInstance.GetAuthority() = " + obInstance.GetEntityUri().GetAuthority().c_str());
					//Console.WriteLine("obInstance.GetFragment() = " + obInstance.GetEntityUri().GetFragment().c_str());
					//Console.WriteLine("obInstance.GetHashCode() = " + obInstance.GetEntityUri().GetHashCode());
					//Console.WriteLine("obInstance.GetHostname() = " + obInstance.GetEntityUri().GetHostname().c_str());
					//Console.WriteLine("obInstance.GetPath() = " + obInstance.GetEntityUri().GetPath().c_str());
					//Console.WriteLine("obInstance.GetScheme() = " + obInstance.GetEntityUri().GetScheme());
					//Console.WriteLine("obInstance.GetType() = " + obInstance.GetEntityUri().GetType());
					// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();
					// ForceInstanceToBeLocal(obInstance);
					// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();
					//Console.WriteLine("obInstance.GetName() = " + obInstance.GetName().c_str());
					//Console.WriteLine("obInstance.GetAbsolutePath() = " + obInstance.GetEntityUri().GetAbsolutePath().c_str());
					//Console.WriteLine("obInstance.GetAbsoluteUri() = " + obInstance.GetEntityUri().GetAbsoluteUri().c_str());
					//Console.WriteLine("obInstance.GetAuthority() = " + obInstance.GetEntityUri().GetAuthority().c_str());
					//Console.WriteLine("obInstance.GetFragment() = " + obInstance.GetEntityUri().GetFragment().c_str());
					//Console.WriteLine("obInstance.GetHashCode() = " + obInstance.GetEntityUri().GetHashCode());
					//Console.WriteLine("obInstance.GetHostname() = " + obInstance.GetEntityUri().GetHostname().c_str());
					//Console.WriteLine("obInstance.GetPath() = " + obInstance.GetEntityUri().GetPath().c_str());
					//Console.WriteLine("obInstance.GetScheme() = " + obInstance.GetEntityUri().GetScheme());
					//Console.WriteLine("obInstance.GetType() = " + obInstance.GetEntityUri().GetType());
				}
			}
			// Console.WriteLine("obSourceNode.GetChildrenCount() = " + obSourceNode.GetChildrenCount()); DebugPrintTrace();
		}

		static void CopySceneNodeNameToID_Recursive(FCDSceneNode obSceneNode)
		{
			obSceneNode.SetName(new fstring(obSceneNode.GetName().c_str() + Guid.NewGuid().ToString()));

			// Recurse
			for (uint iChildNo = 0; iChildNo < obSceneNode.GetChildrenCount(); iChildNo++)
			{
				CopySceneNodeNameToID_Recursive(obSceneNode.GetChild(iChildNo));
			}

		}

		static void MergeVisualSceneLibrary(FColladaDocumentVisualSceneNodeLibrary obSourceVisualSceneLibraryNode, FColladaDocumentVisualSceneNodeLibrary obDestinationVisualSceneLibraryNode, ref Dictionary<string, string> obOldIdToNewIdDictionary)
		{
			// There should really only be one visual scene node, so just add the contents of all the source nodes to the one destination node
			FCDSceneNode obDestinationVisualScene = null;
			if (obDestinationVisualSceneLibraryNode.GetEntityCount() == 0)
			{
				obDestinationVisualScene = obDestinationVisualSceneLibraryNode.AddEntity();
			}
			else
			{
				obDestinationVisualScene = obDestinationVisualSceneLibraryNode.GetEntity(0);
			}

			// Console.WriteLine("obSourceVisualSceneLibraryNode.GetEntityCount() = " + obSourceVisualSceneLibraryNode.GetEntityCount());

			for (uint iSourceEntityNo = 0; iSourceEntityNo < obSourceVisualSceneLibraryNode.GetEntityCount(); iSourceEntityNo++)
			{
				// Get VisualScene
				FCDSceneNode obSourceVisualScene = obSourceVisualSceneLibraryNode.GetEntity(iSourceEntityNo);
				// Console.WriteLine("obSourceVisualScene.GetChildrenCount() = " + obSourceVisualScene.GetChildrenCount());
				for (uint iSourceSceneNode = 0; iSourceSceneNode < obSourceVisualScene.GetChildrenCount(); iSourceSceneNode++)
				{
					// Console.WriteLine("obSourceVisualScene.GetChildrenCount() = " + obSourceVisualScene.GetChildrenCount());
					// Console.WriteLine("iSourceSceneNode = " + iSourceSceneNode);
					// Console.WriteLine("obSourceVisualScene.GetChildrenCount() = " + obSourceVisualScene.GetChildrenCount());
					FCDSceneNode obSourceNodeInTheSourceScene = obSourceVisualScene.GetChild(iSourceSceneNode);
					// Console.WriteLine("obSourceVisualScene.GetChildrenCount() = " + obSourceVisualScene.GetChildrenCount());
					// Console.WriteLine("obSourceNodeInTheSourceScene.GetName() = " + obSourceNodeInTheSourceScene.GetName().c_str());
					// Console.WriteLine("obSourceVisualScene.GetChildrenCount() = " + obSourceVisualScene.GetChildrenCount());

					// Do I already have a Node with the same name?
					FCDSceneNode obDestinationNodeInTheDestinationScene = null;
					for (uint iDestinationSceneNode = 0; iDestinationSceneNode < obDestinationVisualScene.GetChildrenCount(); iDestinationSceneNode++)
					{
						// Get VisualScene
						if (
							(FColladaCSharp.IsEquivalent(obDestinationVisualScene.GetChild(iDestinationSceneNode).GetName(), obSourceNodeInTheSourceScene.GetName()))
							||
							(FColladaCSharp.IsEquivalent(obDestinationVisualScene.GetChild(iDestinationSceneNode).GetDaeId(), obSourceNodeInTheSourceScene.GetDaeId()))
							)
						{
							// Bingo!
							obDestinationNodeInTheDestinationScene = obDestinationVisualScene.GetChild(iDestinationSceneNode);
							break;
						}
					}

					// Copy scene node across
					if (obDestinationNodeInTheDestinationScene == null)
					{
						obDestinationNodeInTheDestinationScene = obDestinationVisualScene.AddChildNode();
					}
					// Console.WriteLine("obSourceVisualScene.GetChildrenCount() = " + obSourceVisualScene.GetChildrenCount());
					CopySceneNode(obSourceNodeInTheSourceScene, obDestinationNodeInTheDestinationScene);
					// Console.WriteLine("obSourceVisualScene.GetChildrenCount() = " + obSourceVisualScene.GetChildrenCount());
				}
			}

			// Now everything has been merged, BUT that can result in multiple SceneNodes at the same level with the same name 
			// this is fine in Collada, but causes problems if the files are ever loaded back into Maya, so fix that up
			// CopySceneNodeNameToID_Recursive(obDestinationVisualScene);
		}

		// Display a formatted string indented by the specified amount.
		static void Display(Int32 indent, string strText)
		{
			Console.Write(new string(' ', indent * 2));
			Console.WriteLine(strText);
		}

		static void PrintAssemblyDebugInfo(int iIndent, Assembly obAssembly)
		{
			Display(iIndent, "-------------------------------------------------------------");
			Display(iIndent, "Assembly Name : " + obAssembly);
			Display(iIndent, "Assembly FullName : " + obAssembly.FullName);
			Display(iIndent, "Assembly CodeBase : " + obAssembly.CodeBase);
			Display(iIndent, "Assembly EscapedCodeBase : " + obAssembly.EscapedCodeBase);
			Display(iIndent, "Assembly Location : " + obAssembly.Location);
			foreach (Module m in obAssembly.GetModules())
			{
				Display(iIndent, "Module Name : " + m);
				//strErrorMessage += "Assembly FullName : " + a.FullName + "\n";
				//strErrorMessage += "Assembly CodeBase : " + a.CodeBase + "\n";
				//strErrorMessage += "Assembly EscapedCodeBase : " + a.EscapedCodeBase + "\n";
				//strErrorMessage += "Assembly Version : " + a.Version + "\n";
				//strErrorMessage += "Assembly VersionCompatibility : " + a.VersionCompatibility + "\n";
			}

		}

		static void PrintAssemblyDebugInfo()
		{
			Console.WriteLine("CurrentDirectory = " + Environment.CurrentDirectory);
			PrintAssemblyDebugInfo(0, Assembly.GetExecutingAssembly());

			// Display information about each assembly loading into this AppDomain.
			foreach (Assembly b in AppDomain.CurrentDomain.GetAssemblies())
			{
				PrintAssemblyDebugInfo(1, b);
			}
		}


		static string Which(string strThingToLookUp)
		{
			rageStatus obStatus;
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.EchoToConsole = true;
			obCommand.Command = "which";
			obCommand.Arguments = strThingToLookUp;
			Console.WriteLine("which " + strThingToLookUp);
			obCommand.Execute(out obStatus);
			return "";
		}

		static int Main(string[] args)
		{
#if CAPTURE_EXCEPTIONS
			try
#endif
			{
				Console.WriteLine("C# version of Rage.Collada.MergeFiles");
				FColladaCSharp.FSInit();
				FColladaCSharp.SetDereferenceFlag(false);

				//Which("Rage.Collada.MergeFiles.exe");
				//Which("FCollada.dll");
				//Which("FColladaCS.dll");
				//Which("FColladaCSharp.dll");
				//Random obRandomNumberGenerator = new Random();
				//if (obRandomNumberGenerator.Next(2) == 0)
				//{
				//		args = new string[] { "T:/mc4/assets/city/la_Pipeline5/SectorColladaFiles/be_smb_01.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_beach_00x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_beach_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_blk_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_blk_02x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_blk_04x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_blk_hill_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_blk_hill_02x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_ocean_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_rd_000x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_be_smb_rd_navmesh_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_ext_blk_north_01a.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_01/la_occluders.dae" };
				//}
				//else
				//{
				//    args = new string[] { "T:/mc4/assets/city/la_Pipeline5/SectorColladaFiles/be_smb_02_north.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_be_smb_blk_02x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_be_smb_blk_04x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_be_smb_blk_hill_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_be_smb_blk_hill_02x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_be_smb_rd_000x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_be_smb_rd_navmesh_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_be_smc_getty_museum_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_be_smc_holiday_inn_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_ext_blk_north_01a.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_fwy_pasadena_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_fwy_sandiego_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_fwy_sandiego_navmesh_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_fwy_ventura_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_fwy_ventura_03x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_fwy_ventura_navmesh_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_hi_va_rd_000x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_hi_va_rd_navmesh_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_hl_mh_blk_03x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_hl_mh_blk_11x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_hl_mh_rd_navmesh_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_hl_va_blk_01x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_hl_va_blk_03x.dae", "T:/mc4/assets/city/la_Pipeline5/MayaFilesAsCollada/be_smb_02_north/la_hl_va_blk_11x.dae" };
				//}
				if (args.Length < 2)
				{
					Console.WriteLine("Usage:");
					Console.WriteLine("Rage.Collada.MergeFiles <merged output file> <input file 1> [input file 2] [input file 3] [input file 4]...");
					Console.WriteLine("\t\tif an input file end with the extension .dae it is treated as a collada file");
					Console.WriteLine("\t\tif an input file end with the extension .txt it is treated as a plain text file containing a list of collada files");
					return -1;
				}

				// Get args
				string strOutputFile = args[0];
				List<string> astrInputColladaFiles = new List<string>();
				for (int i = 1; i < args.Length; i++)
				{
					astrInputColladaFiles.AddRange(args[i].Split(';'));
				}
				for (int i = 0; i < astrInputColladaFiles.Count; i++)
				{
					if (astrInputColladaFiles[i].EndsWith(".txt"))
					{
						// List of Collada files
						string[] astrMoreInputFiles = File.ReadAllLines(astrInputColladaFiles[i]);
						astrInputColladaFiles.AddRange(astrMoreInputFiles);
						astrInputColladaFiles.RemoveAt(i);
						i--;
					}
				}

				// find the output mount dir and the file
				string outputfile = Path.GetFileName(strOutputFile);
				string outputmnt = null;
				if (Path.GetExtension(outputfile) == ".zip")
				{
					outputmnt = strOutputFile.Replace('\\', '/');
					outputfile = outputfile.Replace(".zip", ".dae");
				}
				else
				{
					outputmnt = Path.GetDirectoryName(strOutputFile);
				}

				// Init FCollada
				// FColladaCSharp.PrintTest();
				uint iVersionNo = FColladaCSharp.GetVersion();
				Console.WriteLine("iVersionNo = " + iVersionNo);
				//				PrintAssemblyDebugInfo();
				//				return 0;
				FColladaCSharp.Initialize();
				FColladaCSharp.Initialize();
				FColladaCSharp.Initialize();

				// Setup merged doc
				rageStatus obStatus;


				// Files to merge, so merge them
				using (FCDocument obMergedColladaDoc = new FCDocument())
				{
					rageFileUtilities.DeleteLocalFile(strOutputFile, out obStatus);

					if (astrInputColladaFiles.Count == 1)
					{
						// Only one input file!  So no point merging, just pass through
						string strInputColladaFile = astrInputColladaFiles[0];
						Console.WriteLine("Loading [1/1] : " + strInputColladaFile);
						string inputfile = Path.GetFileName(strInputColladaFile);
						string inputmnt = Path.GetDirectoryName(strInputColladaFile);
						FColladaCSharp.FSMount(inputmnt, null, 0);
						bool bResult = FColladaCSharp.LoadDocumentFromFile(obMergedColladaDoc, inputfile);
						FColladaCSharp.FSRemoveFromPath(inputmnt);
					}
					else
					{

						//                  FColladaCSharp.FSMount(outputmnt, null, 0);
						//					FColladaCSharp.SaveDocument(obMergedColladaDoc, outputfile);
						//                  FColladaCSharp.FSRemoveFromPath(outputmnt);
						//					rageFileUtilities.DeleteLocalFile(strOutputFile, out obStatus);
						// Fill out the asset block
						FCDAsset obAssetBlock = obMergedColladaDoc.GetAsset();
						obAssetBlock.SetUnitName(new fstring("centimeter"));
						obAssetBlock.SetUnitConversionFactor(0.01f);

						// Add the <scene> block
						obMergedColladaDoc.AddVisualScene();

						// Merge everything
						int iColladaFileCount = 0;
						foreach (string strInputColladaFile in astrInputColladaFiles)
						{
							using (FCDocument obSourceColladaDoc = new FCDocument())
							{
								Console.WriteLine("Loading [" + (++iColladaFileCount) + "/" + astrInputColladaFiles.Count + "] : " + strInputColladaFile);

								string inputfile = Path.GetFileName(strInputColladaFile);
								string inputmnt = Path.GetDirectoryName(strInputColladaFile);
								FColladaCSharp.FSMount(inputmnt, null, 0);
								bool bResult = FColladaCSharp.LoadDocumentFromFile(obSourceColladaDoc, inputfile);
								FColladaCSharp.FSRemoveFromPath(inputmnt);

								//Console.WriteLine("bResult = " + bResult);
								//Console.WriteLine("obSourceColladaDoc.GetFileUrl() = " + obSourceColladaDoc.GetFileUrl().c_str());
								//FCDSceneNode obSceneNode = obSourceColladaDoc.GetVisualSceneInstance();
								//Console.WriteLine("obSceneNode.GetName() = " + obSceneNode.GetName().c_str());
								//Console.WriteLine("Done");

								// Merge Libraries
								Dictionary<string, string> obOldIdToNewIdDictionary = new Dictionary<string, string>(); ;
								// MergePhysicsSceneLibrary(obSourceColladaDoc.GetPhysicsSceneLibrary(), obMergedColladaDoc.GetPhysicsSceneLibrary(), ref obOldIdToNewIdDictionary);
								MergeCameraLibrary(obSourceColladaDoc.GetCameraLibrary(), obMergedColladaDoc.GetCameraLibrary(), ref obOldIdToNewIdDictionary);
								MergeLightLibrary(obSourceColladaDoc.GetLightLibrary(), obMergedColladaDoc.GetLightLibrary(), ref obOldIdToNewIdDictionary);
								MergeEffectLibrary(obSourceColladaDoc.GetEffectLibrary(), obMergedColladaDoc.GetEffectLibrary(), ref obOldIdToNewIdDictionary);
								MergeMaterialLibrary(obSourceColladaDoc.GetMaterialLibrary(), obMergedColladaDoc.GetMaterialLibrary(), ref obOldIdToNewIdDictionary);
								MergeGeometryLibrary(obSourceColladaDoc.GetGeometryLibrary(), obMergedColladaDoc.GetGeometryLibrary(), ref obOldIdToNewIdDictionary);
								MergeVisualSceneLibrary(obSourceColladaDoc.GetVisualSceneLibrary(), obMergedColladaDoc.GetVisualSceneLibrary(), ref obOldIdToNewIdDictionary);

								// obSourceColladaDoc.Release();

								// Save progress so far
								// Console.WriteLine("Saving");
								// FColladaCSharp.SaveDocument(obMergedColladaDoc, strOutputFile);
								// Console.WriteLine("Saved");
								// rageFileUtilities.DeleteLocalFile(strOutputFile, out obStatus);
							}
						}
					}
					FColladaCSharp.FSMount(outputmnt, null, 0);
					FColladaCSharp.SaveDocument(obMergedColladaDoc, outputfile);
					FColladaCSharp.FSRemoveFromPath(outputmnt);
					//FColladaCSharp.SaveDocument(obMergedColladaDoc, strOutputFile);
					Console.WriteLine("Saved, if I die now, who cares");
					// obMergedColladaDoc.Release();
				}
				Console.WriteLine("Files merged successfully");

				FColladaCSharp.FSDeinit();

				return 0;
			}
#if CAPTURE_EXCEPTIONS
			catch (Exception ex)
			{
				// Something bad happened somewhere
				String strErrorMessage = "";
				for (int i = 0; i < 80; i++) strErrorMessage += "*";
				strErrorMessage += "\nSomething bad happened during collada combining :\n\n";
				strErrorMessage += "---------------------- CommandLine ----------------------\n";
				strErrorMessage += Environment.CommandLine + "\n";

				// Outer exception
				strErrorMessage += "-------------------- Outer Exception --------------------\n";
				strErrorMessage += "Outer exception Type : " + ex.GetType() + "\n";
				strErrorMessage += "Outer exception Message : " + ex.Message + "\n";
				strErrorMessage += "Outer exception StackTrace : " + ex.StackTrace + "\n";
				strErrorMessage += "Outer exception String : " + ex.ToString() + "\n";

				// Inner exceptions
				ex = ex.InnerException;
				while (ex != null)
				{
					strErrorMessage += "-------------------- Inner Exception --------------------\n";
					strErrorMessage += "Inner exception Type : " + ex.GetType() + "\n";
					strErrorMessage += "Inner exception Message : " + ex.Message + "\n";
					strErrorMessage += "Inner exception StackTrace : " + ex.StackTrace + "\n";
					strErrorMessage += "Inner exception String : " + ex.ToString() + "\n";
					ex = ex.InnerException;
				}


				// Log error
				Console.WriteLine(strErrorMessage);
				PrintAssemblyDebugInfo();
				return -1;
			}
#endif
		}
	}
}

