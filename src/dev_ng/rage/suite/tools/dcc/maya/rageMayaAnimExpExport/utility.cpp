// /utility.cpp

#pragma warning(push)
#pragma warning(disable : 4668)

#include <maya/MGlobal.h>
#include <maya/MFnDagNode.h>
#include <maya/MPlug.h>
#include <maya/MDagPath.h>

#pragma warning(pop)

#include <string>

#include "utility.h"

using namespace std;

namespace exprExportUtility
{

//-----------------------------------------------------------------------------

bool GetStringFromAttribute(MObject mobj, const char* attrName, string &outString)
{
	MStatus status;

	MFnDagNode fnDag(mobj, &status);
	if(status.error())
		return false;
	
	MPlug plug = fnDag.findPlug(attrName, status);
	if(status.error())
		return false;
	
	MString s;
	status = plug.getValue(s);
	if(status.error())
		return false;
	
	outString = s.asChar();

	return true;
}


//-----------------------------------------------------------------------------

MStatus GetNormalizedNodeName(MDagPath& nodeDp, string& outName, bool bRemoveNamespace)
{
	MStatus status(MS::kSuccess);
	string nodeName;

	if(nodeDp.node().apiType() == MFn::kMesh)
	{
		//Always name meshes based on their parent transform name
		MDagPath transformDp;
		status = MDagPath::getAPathTo(nodeDp.transform(), transformDp);
		if(status != MS::kSuccess)
			return status;

		nodeName = transformDp.partialPathName().asChar();
	}

	if(bRemoveNamespace)
	{
		size_t colonPos = nodeName.find_last_of(':');
		if(colonPos != string::npos)
		{
			nodeName = nodeName.substr(colonPos+1);
		}
	}

	size_t nameLen = nodeName.size();
	for(size_t i=0; i<nameLen; i++)
	{
		if( (nodeName[i] == ':') ||
			(nodeName[i] == '|') ||
			(nodeName[i] == ' ') )
		{
			nodeName[i] = '_';
		}
	}

	outName = nodeName;

	return status;

}

//-----------------------------------------------------------------------------

}// end namespace exprExportUtility

