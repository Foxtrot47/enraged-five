// /utility.h

#ifndef _ANIMEXPREXPORT_UTILITY_H_
#define _ANIMEXPREXPORT_UTILITY_H_

#pragma warning(push)
#pragma warning(disable : 4668)

#include <maya/MGlobal.h>
#include <maya/MDagPath.h>

#pragma warning(pop)

#include <string>

using namespace std;

class MObject;

namespace exprExportUtility
{

	bool		GetStringFromAttribute(MObject mobj, const char* attrName, string &outString);
	MStatus		GetNormalizedNodeName(MDagPath& nodeDp, string& outName, bool bRemoveNamespace = true);

}//end namespace exprExortUtility

#endif

