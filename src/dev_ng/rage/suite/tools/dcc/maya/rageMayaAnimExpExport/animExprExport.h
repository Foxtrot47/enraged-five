
// 
// /animExprExport.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef _ANIM_EXPR_EXPORT_H_
#define _ANIM_EXPR_EXPORT_H_

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <maya/MFnDependencyNode.h>
#pragma warning(pop)

#include "animExprData/animExprData.h"
#include "animExprExportCommon/trackIdMgr.h"

using namespace rage;

//-----------------------------------------------------------------------------

class rageMayaAnimExprExport : public MPxCommand
{
public:
	rageMayaAnimExprExport();
	virtual ~rageMayaAnimExprExport();

	//MPxCommand Interface implementation
	static  void*	creator();
	static	MSyntax	newSyntax();

	virtual MStatus doIt ( const MArgList & args );
	virtual bool	isUndoable () const {return false;}

private:
	enum ePlugType
	{	
		kRotate,
		kTranslate,
		kOther
	};

private:
	//PURPOSE: Export expressions driving any of the supplied jonits
	bool			ExportJointExpressions(AeFile& theFile, vector< MDagPath >& jointArray );

	//PURPOSE: Export expressions driving aspects of the supplied meshes
	bool			ExportMeshExpressions(AeFile& theFile, vector< MDagPath >& meshArray );

	//PURPOSE: Export expressions driving any blendshapes of the supplied mesh
	bool			ExportMeshBlendShapeExpressions(AeFile& theFile, AeMesh* pMesh, MDagPath& meshDagPath);

	//PURPOSE: Export expressions driving any animated normal map shaders assigned to the supplied mesh
	bool			ExportAnimatedNormalMapExpressions(AeFile& theFile, AeMesh* pMesh, MDagPath& meshDagPath);

	//PURPOSE: Top level method for creating controllers, routes to creation of specific controller based on
	//an analysis of the DG.
	AeController*	CreateController(AeFile& theFile, MPlug& plug, bool bAllowTimeBasedCurves = false);

	//PURPOSE: Creates a controller to represent a unitless curve in Maya, typically used for set-driven-key expressions
	AeController*	CreateFloatCurveController(AeFile& theFile, MPlug dstPlug, MPlug srcPlug);

	//PURPOSE: Creates a controller to represent a time based curve in Maya, typically used as part of a blend to achieve additive effects with expressions
	AeController*	CreateDirectController(AeFile& theFile, MPlug dstPlug, MPlug srcPlug);

	//PURPOSE: Creates a controller to represent a weighted sum of input values, represented by a blendWeighted node in Maya 
	AeController*	CreateBlendController(AeFile& theFile, MPlug dstPlug, MPlug srcPlug);

	//PURPOSE: Creates a controller to represent a scripted expression node in Maya.
	AeController*	CreateExpressionController(AeFile& theFile, MPlug dstPlug, MPlug srcPlug);

	//PURPOSE: Traverses the inputs to a particular plug and generates driver and driver components to represent those inputs
	MStatus			CreateComponentizedDriver(AeFile& theFile, AeDriver** pOutDriver, AeDriverComponent** pOutDriverComponent, MPlug& driverPlug);

	//PURPOSE: Utility method to collect information about a specific driver.
	MStatus			CollectDriverComponentAttributes(MPlug& driverPlug, eComponentType& type, eComponentRotationOrder& rotOrder, eComponentAxis& axis);

	//PURPOSE: Traverses the DG looking for any blendshape deformers feeding intp the supplied node
	void			FindAttachedBlenshapeNodes(const MDagPath& dp, vector< MObject >& retBlendShapeNodes);

	//PURPOSE: Traverses the network of shaders assigned to the supplied node, looking for the IM animated normal map shader nodes
	void			FindAttachedAnimatedNormalMapShaders(const MDagPath& dp, vector< MObject >& retAnimNrmMapNodes);

	//PURPOSE: Given a block of scripted expressions, locates the expression which modifies the supplied destination plug
	bool			FindExpressionForPlug(MPlug dstPlug, const string& inExpressions, string& retExpression);

	//PURPOSE: Traverses the DG to determine what the angular output conversion factor should be for the supplied dependency node
	MStatus			GetOutputConversionFactor(MFnDependencyNode& fnDep, float& outConvFactor);

	bool			IsParallelBlender(MObject obj);
	bool			IsTRSAttribute(const char* szName);

private:
	trackIdMgr		m_trackIdMgr;

	float			m_outputLinearScaleFactor;
	MString			m_strOuputLinearUnits;

	bool			m_bExportBlendShapeExpressions;
	bool			m_bExportAnimNrmExpressions;
};

//-----------------------------------------------------------------------------

#endif //_ANIM_EXPR_EXPORT_H_

