// 
// /pluginMain.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
// Example useage:
//
// select body arm_l arm_r hip_l hip_r
// rageAnimExprExport -tid "T:/rdr2/TOOLS/expressions/trackIdMap01.xml" -includeChildren 0 -outputPath "c:/temp/staging/rawRex/ex_horse01_animnormalmaps_expr_arm_l.xml"
//
// -command "file -open \"N:/RSGSAN/RDR2/usr/khansen/for Dan/Art/Character/expressions/animal/horse01_animnormalmaps_expr.mb\"; select body arm_l arm_r hip_l hip_r; rageAnimExprExport -tid \"T:/rdr2/TOOLS/expressions/trackIdMap01.xml\" -includeChildren 0 -outputPath \"c:/temp/staging/rawRex/ex_horse01_animnormalmaps_expr_arm_l.xml\""
//

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#pragma warning(pop)

#include "system/param.h"
#include "parser/manager.h"

#include "animExprExport.h"

using namespace rage;

//-----------------------------------------------------------------------------

MStatus __declspec(dllexport) initializePlugin( MObject obj )
{
	MFnPlugin plugin( obj, "Alias|Wavefront", "6.5", "Any");

	rage::sysParam::Init(0,NULL);
	INIT_PARSER;

	plugin.registerCommand("rageAnimExprExport", rageMayaAnimExprExport::creator, rageMayaAnimExprExport::newSyntax);
	
	
	return( MS::kSuccess );
}

//-----------------------------------------------------------------------------

MStatus __declspec(dllexport) uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );

	SHUTDOWN_PARSER;

	plugin.deregisterCommand("rageAnimExprExport");
	
	return( MS::kSuccess );
}

//-----------------------------------------------------------------------------

