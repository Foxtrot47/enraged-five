// 
// /animExprExport.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#pragma warning(push)
#pragma warning(disable : 4668)

#include <maya/MGlobal.h>
#include <maya/MArgDatabase.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnTransform.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MItDag.h>
#include <maya/MObject.h>
#include <maya/MObjectArray.h>
#include <maya/MPlug.h>
#include <maya/MSelectionList.h>
#include <maya/MStatus.h>
#include <maya/MStringArray.h>
#include <maya/MPlugArray.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MFnMesh.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MFnBlendShapeDeformer.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnExpression.h>
#include <maya/MFnIkJoint.h>
#include <maya/MFnSet.h>
#pragma warning(pop)

#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <algorithm>
#include <list>
#include <cctype>
#include <sstream>

#include "crAnimation/animtrack.h"

#include "animExprData/animExprData.h"
#include "animExprScriptParser/exprParser.h"
#include "animExprExportCommon/unitConvert.h"

#include "animExprExport.h"
#include "utility.h"

using namespace std;
using namespace rage;

#define MAX_ANIMNRM_MULTS	60

//-----------------------------------------------------------------------------

rageMayaAnimExprExport::rageMayaAnimExprExport()
	: m_outputLinearScaleFactor(1.0f)
	, m_strOuputLinearUnits("none")
	, m_bExportBlendShapeExpressions(true)
	, m_bExportAnimNrmExpressions(true)
{
}

//-----------------------------------------------------------------------------

rageMayaAnimExprExport::~rageMayaAnimExprExport()
{
}

//-----------------------------------------------------------------------------

void* rageMayaAnimExprExport::creator()
{
	return new rageMayaAnimExprExport();
}

//-----------------------------------------------------------------------------

MSyntax rageMayaAnimExprExport::newSyntax()
{
	MSyntax syntax;
    
	syntax.addFlag("op", "outputPath", MSyntax::kString);
	
	syntax.addFlag("tid", "trackIdFile", MSyntax::kString);
	
	syntax.addFlag("ic", "includeChildren", MSyntax::kBoolean);
	syntax.addFlag("ibs", "includeBlendShapes", MSyntax::kBoolean);
	syntax.addFlag("ian", "includeAnimNrmMaps", MSyntax::kBoolean);

	syntax.addFlag("olu", "outputLinearUnits", MSyntax::kString);

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	syntax.useSelectionAsDefault(true);
	syntax.setObjectType(MSyntax::kSelectionList, 1);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus rageMayaAnimExprExport::doIt ( const MArgList & args )
{
	/*
	FILE* krDebug = fopen("//sanw-krose/share/deleteme/animExportDebug.txt", "a");
	fprintf(krDebug, "-=-=--=-=-=-=--=-=-=-=--=-=-=-=--=-=-=-=--=-=-=-=--=-=-=-=--=-=-\n");
	for(unsigned i=0; i<args.length(); i++)
	{
		fprintf(krDebug, "args[%d] = %s\n", i, args.asString(i));
	}
	MSelectionList activeSelectionList;
	MGlobal::getActiveSelectionList(activeSelectionList);
	MStringArray astrSelectionList;
	activeSelectionList.getSelectionStrings(astrSelectionList);
	MString strSelection = "";
	for(unsigned i=0; i<astrSelectionList.length(); i++)
	{
		fprintf(krDebug, "astrSelectionList[%d] = %s\n", i, astrSelectionList[i]);
		strSelection += " ";
		strSelection += astrSelectionList[i];
	}
	fprintf(krDebug, strSelection.asChar());
	fclose(krDebug);
	*/

	MStatus status;

	MSelectionList	inputList;


	//Parse the command arguments
	MArgDatabase argData(syntax(), args);
	
	//Get the path of the output expression file
	MString outputPath;
	status = argData.getFlagArgument("outputPath", 0, outputPath);
	if(status != MS::kSuccess)
	{	
		setResult(0);
		return MS::kInvalidParameter;
	}

	//Get the path for the track id file
	MString trackIdFilePath;
	status = argData.getFlagArgument("trackIdFile", 0, trackIdFilePath);
	if(status == MS::kSuccess && trackIdFilePath.length() != 0)
	{
		if(!m_trackIdMgr.LoadIdFile(trackIdFilePath.asChar()))
		{
			MGlobal::displayError("Failed to load track id file");
			setResult(0);
			return MS::kFailure;
		}
	}

	if(argData.isFlagSet("outputLinearUnits"))
	{
		MString mstrOuputLinearUnits;
		status = argData.getFlagArgument("outputLinearUnits", 0, mstrOuputLinearUnits);
		if(!status.error())
		{
			UnitConverter::EnumUnitTypeLinear outputLinearType;
			if(UnitConverter::StringNameToUnitTypeLinear(mstrOuputLinearUnits.asChar(), outputLinearType))
			{
				m_strOuputLinearUnits = mstrOuputLinearUnits;

				//By default Maya returns all units in centimeters
				m_outputLinearScaleFactor = UnitConverter::GetUnitTypeLinearConversionFactor(UnitConverter::CENTIMETERS, outputLinearType);
			}
			else
			{
				char errMsgBuf[1024];
				sprintf_s(errMsgBuf, 1024, "Unknown linear unit type '%s'", mstrOuputLinearUnits.asChar());
				MGlobal::displayError(errMsgBuf);
				setResult(0);
				return MS::kInvalidParameter;
			}
		}
		else
		{
			MGlobal::displayError("Invalid parameter for argument -olu/-outputLinearUnits");
			setResult(0);
			return MS::kInvalidParameter;
		}
	}

	//Determine if we should include children of the supplied nodes
	bool bIncludeChildren;
	if(!argData.isFlagSet("includeChildren", &status))
	{
		//If the flag isn't set, default to including child bones
		bIncludeChildren = true;
	}
	else
	{
		//If the flag is set, then get the value for it
		status = argData.getFlagArgument("includeChildren", 0, bIncludeChildren);
	}

	//Determine if we should export expressions that are driving blend shapes.
	if(!argData.isFlagSet("includeBlendShapes", &status))
	{
		m_bExportBlendShapeExpressions = true;
	}
	else
	{
		status = argData.getFlagArgument("includeBlendShapes", 0, m_bExportBlendShapeExpressions);
	}

	//Determine if we should export expressions that are driving animated normal maps.
	if(!argData.isFlagSet("includeAnimNrmMaps", &status))
	{
		m_bExportAnimNrmExpressions = true;
	}
	else
	{
		status = argData.getFlagArgument("includeAnimNrmMaps", 0, m_bExportAnimNrmExpressions);
	}

	//Get the bones to export expression data for, by first checking to see if any objects were 
	//explicitly passed in on the command line
	argData.getObjects(inputList);
	if(!inputList.length())
	{
		//No objects were supplied, so use the active selection list
		MGlobal::getActiveSelectionList(inputList);
	}

	if(!inputList.length())
	{
		MGlobal::displayWarning("No input object specfied to export expression data for.");
		setResult(0);
		return MS::kSuccess;
	}

	vector< MDagPath > jointArray;
	vector< MDagPath > meshArray;

	unsigned int numInputs = inputList.length();

	for(unsigned int i = 0; i < numInputs; i++)
	{
		MDagPath curDagPath;

		inputList.getDagPath(i, curDagPath);

		if(curDagPath.node().apiType() == MFn::kJoint)
		{
			if(bIncludeChildren)
			{
				//Recurse through all the children of the selected joint and add them into
				//the list of nodes that could potentially have expressions exported
				MItDag dagIterator;
				dagIterator.reset(curDagPath, MItDag::kDepthFirst, MFn::kJoint);
				while( !dagIterator.isDone() )
				{
					MDagPath jointDagPath;
					dagIterator.getPath(jointDagPath);
					jointArray.push_back(jointDagPath);
					dagIterator.next();
				}
			}
			else
			{
				//Just add the input node
				jointArray.push_back(curDagPath);
			}
		}
		else if(curDagPath.node().apiType() == MFn::kTransform)
		{
			if(curDagPath.extendToShape() == MS::kSuccess)
			{
				if(curDagPath.node().apiType() == MFn::kMesh)
				{
					meshArray.push_back(curDagPath);
				}
			}
		}
	}


	AeFile theFile;

	theFile.SetGlobalTranslateScaleFactor( (float)m_outputLinearScaleFactor);
	
	bool bRes = true;

	if(jointArray.size())
		bRes = ExportJointExpressions(theFile, jointArray);

	if(meshArray.size())
		bRes = ExportMeshExpressions(theFile, meshArray);

	if(bRes)
	{
		ASSET.CreateLeadingPath(outputPath.asChar());
	
		if(PARSER.SaveObject(outputPath.asChar(), "", &theFile))
		{
			setResult(1);
			return MS::kSuccess;
		}
		else
		{
			char errMsgBuf[1024];
			sprintf_s(errMsgBuf, 1024, "Failed to write file '%s'", outputPath.asChar());
			MGlobal::displayError(errMsgBuf);
			setResult(0);
			return MS::kFailure;
		}
	}
	else
	{
		setResult(0);
		return MS::kFailure;
	}
}

//-----------------------------------------------------------------------------

bool IsPlugExpressionDriven(MPlug& plug, MStatus *status = NULL)
{
	if(status)
		(*status) = MS::kSuccess;

	if(plug.isConnected())
	{
		MPlugArray connectedPlugs;
		plug.connectedTo(connectedPlugs, true, false, status);
		if(connectedPlugs.length())
		{
			MFnDependencyNode srcPlugDepNode(connectedPlugs[0].node());
			
			MFn::Type nodeType = srcPlugDepNode.object().apiType();

			if( (nodeType == MFn::kAnimCurveUnitlessToAngular)  ||
				(nodeType == MFn::kAnimCurveUnitlessToDistance) ||
				(nodeType == MFn::kAnimCurveUnitlessToUnitless) ||
				(nodeType == MFn::kExpression)					||
				(nodeType == MFn::kBlendWeighted) )
			{
				return true;
			}
			else if (nodeType == MFn::kUnitConversion)
			{
				//Look one more connection upstream
				MPlug inputPlug = srcPlugDepNode.findPlug("input", status);
				return IsPlugExpressionDriven(inputPlug, status);
			}
		}
	}
	return false;
}

//-----------------------------------------------------------------------------

bool rageMayaAnimExprExport::IsParallelBlender(MObject obj)
{
	MStatus status;
	MFnBlendShapeDeformer fnBlendShapeDef(obj, &status);
	Assert(status == MS::kSuccess);
	
	bool bIsParallelBlender = false;	
	MPlug isParallelPlug = fnBlendShapeDef.findPlug("parallelBlender", &status);
	if(status == MS::kSuccess)
	{
		bool bTmp;
		status = isParallelPlug.getValue(bTmp);
		if(status == MS::kSuccess)
			bIsParallelBlender = bTmp;
	}
	return bIsParallelBlender;
}

//-----------------------------------------------------------------------------

void rageMayaAnimExprExport::FindAttachedBlenshapeNodes(const MDagPath& dp, vector< MObject >& retBlendShapeNodes)
{
	MStatus status;
	MFnMesh fnDp(dp, &status);
	if(status != MS::kSuccess)
		return;

	MPlug inMeshPlug = fnDp.findPlug("inMesh", &status);
	Assertf( (status == MS::kSuccess), "Internal Maya failure, failed to locate 'inMesh' plug for the mesh (%s)", fnDp.name().asChar());

	MItDependencyGraph itDG(inMeshPlug,  MFn::kBlendShape, MItDependencyGraph::kUpstream, MItDependencyGraph::kBreadthFirst);
	while( !itDG.isDone() )
	{
		if( itDG.thisNode().apiType() == MFn::kBlendShape && !IsParallelBlender(itDG.thisNode()) )
		{
			bool exists = false;
			for( unsigned int i = 0; i < retBlendShapeNodes.size(); i++ )
			{
				if( itDG.thisNode() == retBlendShapeNodes[i] )
				{
					exists = true;
					break;
				}
			}

			if( !exists )
			{
				retBlendShapeNodes.push_back(itDG.thisNode());
			}
		}
		itDG.next();
	}
}

//-----------------------------------------------------------------------------

void rageMayaAnimExprExport::FindAttachedAnimatedNormalMapShaders(const MDagPath& dp, vector< MObject >& retAnimNrmMapNodes)
{
	MStatus status;
	MFnMesh fnMesh(dp, &status);
	if(status != MS::kSuccess)
		return;

	MObjectArray	shaderObjArray;
	MIntArray		faceIdxArray;

	status = fnMesh.getConnectedShaders(0, shaderObjArray, faceIdxArray);
	int nShaders = shaderObjArray.length();
	for(int shaderIdx = 0; shaderIdx < nShaders; shaderIdx++ )
	{
		MFnSet shaderSet(shaderObjArray[shaderIdx], &status);

		//Find the surface shader plug
		MPlug surfaceShaderPlug = shaderSet.findPlug("surfaceShader", &status);
		if(status.error())
			continue;
	
		//Get all of the incoming connections to the surface shader plug
		MPlugArray surfaceShaderConnections;
		surfaceShaderPlug.connectedTo( surfaceShaderConnections, true, false, &status);
		if(status.error())
			continue;

		if(surfaceShaderConnections.length() != 1)
		{
			if(surfaceShaderConnections.length() > 0)
			{
				Displayf("Too many connections (%d) to surfaceShader on shader set '%s'", surfaceShaderConnections.length(), shaderSet.name().asChar());
				continue;
			}
			else
			{
				//Nothing connected to the surface shader plug, so skip this set...
				continue;
			}
		}

		MObject shaderMObj = surfaceShaderConnections[0].node();
		MFnDependencyNode shaderFnDep(shaderMObj, &status);
		if( (shaderFnDep.object().apiType() != MFn::kLambert) &&
			(shaderFnDep.object().apiType() != MFn::kBlinn) )
		{
			continue;	//Only looking for lambert or blinn materials 
		}

		MPlug normalCameraPlug = shaderFnDep.findPlug("normalCamera", &status);
		if(status.error())
			continue;	

		MPlugArray normalCameraConnections;
		normalCameraPlug.connectedTo( normalCameraConnections, true, false, &status);
		if( status.error() || !normalCameraConnections.length() )
			continue;	//Nothing connected to the bump map input, so skip this shader...

		MFnDependencyNode normalCameraSourceFnDep(normalCameraConnections[0].node());
		if(normalCameraSourceFnDep.typeName() == "AnimNormalMap")
		{
			retAnimNrmMapNodes.push_back(normalCameraSourceFnDep.object());
		}

	}//End for(int shaderIdx...
}

//-----------------------------------------------------------------------------

bool rageMayaAnimExprExport::ExportMeshExpressions(AeFile& theFile, vector< MDagPath >& meshArray )
{
	//Export any expressions associated with the blend shapes
	for(vector<MDagPath>::iterator it = meshArray.begin(); 
		it != meshArray.end();
		++it)
	{
		MDagPath curDagPath = (*it);

		AeMesh* pMesh = new AeMesh();

		exprExportUtility::GetNormalizedNodeName(curDagPath, pMesh->m_id, true);

		if(m_bExportBlendShapeExpressions)
			ExportMeshBlendShapeExpressions(theFile, pMesh, (*it));

		if(m_bExportAnimNrmExpressions)
			ExportAnimatedNormalMapExpressions(theFile, pMesh, (*it));

		//TODO : Export general driven shader parameters
	
		if((pMesh->m_blendTargets.size()) || (pMesh->m_animNrmMults.size()))
		{
			theFile.m_meshLibrary.push_back(pMesh);
		}
		else
		{
			delete pMesh;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rageMayaAnimExprExport::ExportAnimatedNormalMapExpressions(AeFile& theFile, AeMesh* pMesh, MDagPath& meshDagPath)
{
	MStatus status;

	vector< MObject > animNrmMapMObjs;

	FindAttachedAnimatedNormalMapShaders(meshDagPath, animNrmMapMObjs);
	if(!animNrmMapMObjs.size())
		return true;

	for(vector<MObject>::iterator it = animNrmMapMObjs.begin();
		it != animNrmMapMObjs.end();
		++it)
	{
		MFnDependencyNode animNrmFnDep((*it), &status);

		//Get a list of all the multipliers that have been enabled on the animated normal map node,
		//and the user defined names for the multipliers
		MPlugArray		multiplierPlugArray;
		MStringArray	multiplierNameArray;

		char plugBuf[32];
		memset(plugBuf, 0, 32);

		for(int multIdx=1; multIdx<=MAX_ANIMNRM_MULTS; multIdx++)
		{
			sprintf(plugBuf, "Enable%d", multIdx);

			MPlug multEnablePlug = animNrmFnDep.findPlug(plugBuf, &status);
			if(!multEnablePlug.isNull())
			{
				bool bEnable = false;
				status = multEnablePlug.getValue( bEnable );

				if( !status.error() && bEnable)
				{
					//Get the multiplier plug
					sprintf(plugBuf, "mu%d", multIdx);
					MPlug multPlug = animNrmFnDep.findPlug(plugBuf, &status);

					//Check to see if it is expression driven
					bool bExpressionDriven = IsPlugExpressionDriven(multPlug, &status);

					if( bExpressionDriven )
					{
						//Get the multiplier name
						MString multName;

						sprintf(plugBuf, "ExprName%d", multIdx);
						MPlug multNamePlug = animNrmFnDep.findPlug(plugBuf, &status);
						if( !multNamePlug.isNull() )
						{
							status = multNamePlug.getValue( multName );
						}
						
						if( !multPlug.isNull() && (multName.length() > 0) )
						{
							multiplierPlugArray.append(multPlug);
							multiplierNameArray.append(multName);
						}
					}
				}
			}
		}//End for(int multIdx=1

		int nEnabledMultipliers = multiplierPlugArray.length();
		for(int multIdx = 0; multIdx < nEnabledMultipliers; multIdx++)
		{
			MPlug multPlug = multiplierPlugArray[multIdx];

			AeMeshAnimNormalMapMult* pAnimNrm = new AeMeshAnimNormalMapMult();
			pAnimNrm->m_id = multiplierNameArray[multIdx].asChar();
			pAnimNrm->m_hashId = crAnimTrack::ConvertBoneNameToId(pAnimNrm->m_id.c_str());

			AeController* pController = CreateController(theFile, multPlug);
			if(pController)
			{
				pAnimNrm->m_pController = pController;
				pAnimNrm->m_controllerId = pController->m_id;

				theFile.m_controllerLibrary.push_back(pController);
			}

			pMesh->m_animNrmMults.push_back(pAnimNrm);
		}

	}//End for(vector<MObject>::iterator it = animNrmMapMObjs.begin()...

	return true;
}

//-----------------------------------------------------------------------------

bool rageMayaAnimExprExport::ExportMeshBlendShapeExpressions(AeFile& theFile, AeMesh* pMesh, MDagPath& meshDagPath)
{
	MStatus status;

	vector< MObject > blendShapeMObjs;

	FindAttachedBlenshapeNodes(meshDagPath, blendShapeMObjs);
	if(!blendShapeMObjs.size())
	{
		Displayf("%s has no blend shapes", meshDagPath.fullPathName().asChar());
		return true;
	}

	for(vector<MObject>::iterator it = blendShapeMObjs.begin();
		it != blendShapeMObjs.end();
		++it)
	{
		MFnBlendShapeDeformer fnBlend((*it), &status);

		MStringArray aliasStringArr;
		fnBlend.getAliasList(aliasStringArr, &status);
		unsigned int numAliasItems = aliasStringArr.length();

		MIntArray indexArray;
		status = fnBlend.weightIndexList(indexArray);
		unsigned int numWeightIndicies = indexArray.length();

		MPlug weightsPlug = fnBlend.findPlug("weight");

		bool bIsArray = weightsPlug.isArray();
		unsigned int numWeights = weightsPlug.numElements();

		Assert( (numWeights * 2) == numAliasItems);

		for(unsigned int weightIdx=0; weightIdx < numWeights; weightIdx++)
		{
			int logicalIdx = indexArray[weightIdx];
			MPlug curWeightPlug = weightsPlug.elementByLogicalIndex( logicalIdx, &status);
			
			if(curWeightPlug.isConnected() && IsPlugExpressionDriven(curWeightPlug, &status))
			{
				AeMeshBlendTarget *pMeshTarget = new AeMeshBlendTarget();

				MString aliasName;
				if(fnBlend.getPlugsAlias(curWeightPlug, aliasName, &status))
				{
					const char* targetName = aliasName.asChar();
					pMeshTarget->m_id = targetName;
					pMeshTarget->m_hashId = crAnimTrack::ConvertBoneNameToId(targetName);
				}

				AeController* pController = CreateController(theFile, curWeightPlug);
				if(pController)
				{
					pMeshTarget->m_pController = pController;
					pMeshTarget->m_controllerId = pController->m_id;

					theFile.m_controllerLibrary.push_back(pController);
				}

				pMesh->m_blendTargets.push_back(pMeshTarget);
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rageMayaAnimExprExport::ExportJointExpressions(AeFile& theFile, vector< MDagPath >& jointArray )
{
	MStatus status;

	int tmpId = 0;

	for( vector< MDagPath >::iterator it = jointArray.begin();
		 it != jointArray.end();
		 ++it)
	{
		MDagPath& curDagPath = (*it);

		MString ppName = curDagPath.partialPathName();
		const char* dbgName = ppName.asChar();
		string boneName = dbgName;					//TODO : Fixup path names as appropriate
		int boneId = tmpId++;						//TODO : Get the corrrect bone id

		MFnDependencyNode fnDep(curDagPath.node(), &status);
		if(status != MS::kSuccess)
			return false;

		MPlugArray			exprPlugArr;
		vector<ePlugType>	exprPlugTypeArr;

		MPlug translateXPlug = fnDep.findPlug("translateX", &status);
		if( (status == MS::kSuccess) && IsPlugExpressionDriven(translateXPlug))
		{
			exprPlugArr.append(translateXPlug);
			exprPlugTypeArr.push_back(kTranslate);
		}
		
		MPlug translateYPlug = fnDep.findPlug("translateY", &status);
		if( (status == MS::kSuccess) && IsPlugExpressionDriven(translateYPlug))
		{
			exprPlugArr.append(translateYPlug);
			exprPlugTypeArr.push_back(kTranslate);
		}
		
		MPlug translateZPlug = fnDep.findPlug("translateZ", &status);
		if( (status == MS::kSuccess) && IsPlugExpressionDriven(translateZPlug))
		{
			exprPlugArr.append(translateZPlug);
			exprPlugTypeArr.push_back(kTranslate);
		}

		MPlug rotateXPlug = fnDep.findPlug("rotateX", &status);
		if( (status == MS::kSuccess) && IsPlugExpressionDriven(rotateXPlug))
		{
			exprPlugArr.append(rotateXPlug);
			exprPlugTypeArr.push_back(kRotate);
		}

		MPlug rotateYPlug = fnDep.findPlug("rotateY", &status);
		if( (status == MS::kSuccess) && IsPlugExpressionDriven(rotateYPlug))
		{
			exprPlugArr.append(rotateYPlug);
			exprPlugTypeArr.push_back(kRotate);
		}

		MPlug rotateZPlug = fnDep.findPlug("rotateZ", &status);
		if( (status == MS::kSuccess) && IsPlugExpressionDriven(rotateZPlug))
		{
			exprPlugArr.append(rotateZPlug);
			exprPlugTypeArr.push_back(kRotate);
		}
		
		if(!exprPlugArr.length())
		{
			//None of the transforms for this joint are being driven by expressions
			continue;
		}

		AeBone *pAeBone = new AeBone();
		
		pAeBone->m_id = boneName;

		//Generate the runtime hashed id for the bone
		MFnDagNode curFnDag(curDagPath);

		int parentCount = curFnDag.parentCount();
		Assertf(parentCount, "Found a joint that has no parent nodes (not even the world root), scene may be corrupt.");

		MObject parentMObj = curFnDag.parent(0);
		MFn::Type parentFnType = parentMObj.apiType();
		if(parentFnType == MFn::kWorld)
		{
			pAeBone->m_hashId = 0;
		}
		else
		{
			string userBoneId;
			if(exprExportUtility::GetStringFromAttribute(curFnDag.object(), "boneID", userBoneId))
			{
				pAeBone->m_hashId = crAnimTrack::ConvertBoneNameToId(userBoneId.c_str());
			}
			else
			{
				pAeBone->m_hashId = crAnimTrack::ConvertBoneNameToId(boneName.c_str());
			}
		}
		

		int numExpPlugs = exprPlugArr.length();
		for(int i=0; i<numExpPlugs; i++)
		{
			MPlug curPlug = exprPlugArr[i];

			AeBoneTransformComponent* pComp = new AeBoneTransformComponent();
			
			string compId = curPlug.name().asChar();
			size_t attrStart = compId.find_last_of('.');
			pComp->m_id = compId.substr(attrStart+1);

			AeController* pController = CreateController(theFile, curPlug);
			if(pController)
			{
				//Setup the controller identifiers
				pComp->m_controllerId = pController->m_id;
				pComp->m_pController = pController;
				
				if(exprPlugTypeArr[i] == kRotate)
				{
					//For rotation based controllers, the unit conversion will be set
					//at the controller level
					pComp->m_unitConversionFactor = 1.0f;
				}
				else if(exprPlugTypeArr[i] == kTranslate)
				{
					pComp->m_unitConversionFactor = m_outputLinearScaleFactor;
				}

				//Add the transform component to the bones list of driven components
				pAeBone->m_transform.push_back(pComp);

				//Add the controller to the files controller library
				theFile.m_controllerLibrary.push_back(pController);
			}
		}

		theFile.m_boneLibrary.push_back(pAeBone);
	}

	return true;
}

//-----------------------------------------------------------------------------

MStatus rageMayaAnimExprExport::GetOutputConversionFactor(MFnDependencyNode& fnDep, float& outConvFactor)
{
	MStatus status = MS::kSuccess;

	double unitConvFactor = 1.0f;

	MPlug outputPlug;
	outputPlug = fnDep.findPlug("output", &status); 
	if(!status.error() && !outputPlug.isNull())
	{
		if(outputPlug.isArray())
		{
			//The output plug is an array based plug, so we need to look through all the outgoing connections
			//to see if a unit conversion node is connected
			int nOutputElements = outputPlug.numElements();
			bool bFoundConvNode = false;
			for(int elemIdx = 0; elemIdx < nOutputElements; elemIdx++)
			{
				MPlug elementPlug = outputPlug.elementByPhysicalIndex(elemIdx);

				MPlugArray elementPlugConnections;
				elementPlug.connectedTo(elementPlugConnections, false, true, &status);
				int nOutputConnections = elementPlugConnections.length();
			
				for(int conIdx = 0; conIdx < nOutputConnections; conIdx++)
				{
					MPlug elementPlugDestPlug = elementPlugConnections[conIdx];
					MFnDependencyNode destPlugDepNode(elementPlugDestPlug.node());
					if( destPlugDepNode.object().apiType() == MFn::kUnitConversion )
					{
						MPlug convFactPlug = destPlugDepNode.findPlug("conversionFactor", &status);
						status = convFactPlug.getValue(unitConvFactor);
						bFoundConvNode = true;
						break;
					}
				}

				if(bFoundConvNode)
					break;
			}//end for(int elemIdx = 0...
		}
		else
		{
			MPlugArray outputPlugConnections;
			outputPlug.connectedTo(outputPlugConnections, false, true, &status);
			int nOutputPlugConnections = outputPlugConnections.length();
			for( int conIdx = 0; conIdx < nOutputPlugConnections; conIdx++ )
			{
				MPlug outputDstPlug = outputPlugConnections[conIdx];
				MFnDependencyNode outputDstPlugDepNode(outputDstPlug.node());
				if( outputDstPlugDepNode.object().apiType() == MFn::kUnitConversion )
				{
					MPlug convFactPlug = outputDstPlugDepNode.findPlug("conversionFactor", &status);
					status = convFactPlug.getValue(unitConvFactor);
					break;
				}
			}
		}
	}//End if(!status.error() && !outputPlug.isNull())

	outConvFactor = (float)unitConvFactor;

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

AeController* rageMayaAnimExprExport::CreateController(AeFile& theFile, MPlug& plug, bool bAllowTimeBasedCurves)
{
	MStatus status;

	if(!plug.isConnected())
		return NULL;
	
	MPlugArray connectedPlugs;
	plug.connectedTo(connectedPlugs, true, false, &status);
	int nConnectedPlugs = connectedPlugs.length();
	if(!nConnectedPlugs)
		return NULL;

	MFnDependencyNode srcPlugDepNode(connectedPlugs[0].node());
	const char* srcPlugDepNodeName = srcPlugDepNode.name().asChar();
	
	MFn::Type nodeType = srcPlugDepNode.object().apiType();

	AeController* pRetController = NULL;

	if( (nodeType == MFn::kAnimCurveUnitlessToAngular)  ||
		(nodeType == MFn::kAnimCurveUnitlessToDistance) ||
		(nodeType == MFn::kAnimCurveUnitlessToUnitless) )
	{
		pRetController = CreateFloatCurveController(theFile, plug, connectedPlugs[0]);
	}
	else if( (nodeType == MFn::kAnimCurveTimeToAngular) ||
			 (nodeType == MFn::kAnimCurveTimeToDistance) ||
			 (nodeType == MFn::kAnimCurveTimeToUnitless) )
	{
		if(!bAllowTimeBasedCurves)
		{
			//For time to * curves we need to export them as direct "pass through" controllers. This
			//allows for additive animations in the expression data
			pRetController = CreateDirectController(theFile, plug, connectedPlugs[0]);
		}
		else
		{
			MFnAnimCurve fnCurve( srcPlugDepNode.object() );

			//So there is an overloaded meaning time based curves can have for the expression system.  In some cases
			//they are used to represent constant values when connected to a blend weighted node, but in other cases
			//they are used to represent additive expressions that require input data from an animation.  The way
			//we'll choose which type to use is by checking the number of keyframes on the curve, is there is exactly
			//one key at time 0, then we'll treat it as a constant (exported as a float curve with one key), otherwise 
			//it's a direct controller
			unsigned int nKeys = fnCurve.numKeys();
			unsigned int index;
			if( (nKeys == 1) && fnCurve.find(MTime(0.0,MTime::kSeconds), index, &status) )
			{
				pRetController = CreateFloatCurveController(theFile, plug, connectedPlugs[0]);
			}
			else
			{
				pRetController = CreateDirectController(theFile, plug, connectedPlugs[0]);
			}
		}
	}
	else if( nodeType == MFn::kBlendWeighted )
	{
		pRetController = CreateBlendController(theFile, plug, connectedPlugs[0]);
	}
	else if( nodeType == MFn::kExpression )
	{
		pRetController = CreateExpressionController(theFile, plug, connectedPlugs[0]);
	}
	else if( nodeType == MFn::kUnitConversion )
	{
		MPlug inputPlug = srcPlugDepNode.findPlug("input");
		pRetController = CreateController(theFile, inputPlug, bAllowTimeBasedCurves);
	}
	else if( nodeType == MFn::kMultiplyDivide )
	{
		MPlug inputPlug = srcPlugDepNode.findPlug("input1X");
		if(inputPlug.isConnected())
		{
			pRetController = CreateController(theFile, inputPlug, bAllowTimeBasedCurves);
		}
		else
		{
			inputPlug = srcPlugDepNode.findPlug("input2X");
			if(inputPlug.isConnected())
			{
				pRetController = CreateController(theFile, inputPlug, bAllowTimeBasedCurves);
			}
		}
	}

	return pRetController;
}

//-----------------------------------------------------------------------------

AeController* rageMayaAnimExprExport::CreateDirectController(AeFile& theFile, MPlug dstPlug, MPlug srcPlug)
{
	MStatus status;

	AeDirectController* pDirectController = new AeDirectController();

	//The direct controller is a special case since the driver plug is actually the driven plug.  In order
	//to find the correct driver node we need to search upstream by way of the DG nodes output connections
	//until we come to a plug on a joint node which is our source
	MFnDependencyNode curNode(srcPlug.node());
	pDirectController->m_id = curNode.name().asChar();

	//We allow for a "multiplyDivide" node to be connected to the output of the animation curve as a represenation
	//of a scale on an animation.  So check for this setup in the DG
	MPlug curveOutputPlug = curNode.findPlug("output", false, &status);
	Assertf(!curveOutputPlug.isNull(), "Failed to locate output plug on node %s", curNode.name().asChar());
	
	MPlugArray curveOutputPlugConPlugs;
	curveOutputPlug.connectedTo(curveOutputPlugConPlugs, false, true, &status);
	Assertf(curveOutputPlugConPlugs.length() == 1, "Too many connections to output plug on node %s", curNode.name().asChar());
	
	MPlug outputDestPlug = curveOutputPlugConPlugs[0];
	MFnDependencyNode outputDestNode(outputDestPlug.node());

	//Skip over any unit conversion nodes
	if( outputDestNode.object().apiType() == MFn::kUnitConversion )
	{
		MPlug unitConvOutputPlug = outputDestNode.findPlug("output");
		MPlugArray unitConvOutputPlugConn;
		unitConvOutputPlug.connectedTo(unitConvOutputPlugConn, false, true, &status);
		Assertf(unitConvOutputPlugConn.length() == 1, "Too many connections to output plug on node %s", outputDestNode.name().asChar());
		outputDestPlug = unitConvOutputPlugConn[0];
		outputDestNode.setObject( outputDestPlug.node() );
	}

	if( outputDestNode.object().apiType() == MFn::kMultiplyDivide )
	{
		MPlug input1XPlug = outputDestNode.findPlug("input1X", false, &status);
		MPlug input2XPlug = outputDestNode.findPlug("input2X", false, &status);

		if(outputDestPlug == input1XPlug)
		{
			//The output of the animation is feeding into the input1X attribute, so the multipler constant
			//must be stored in the input2X attribute
			if(!input2XPlug.isConnected())
			{
				double scaleValue;
				input2XPlug.getValue(scaleValue);
				pDirectController->m_scale = (float)scaleValue;
			}
			else
			{
				Errorf("mutliplyDivide node '%s' has connections on both input1X and input2X, can't export constant scale value from this node", outputDestNode.name().asChar());
			}
		}
		else
		{
			//The output of the animation is feeding into the input2X attribute, so the multiplier constant 
			//must be stored in the input1X attributes
			if(!input1XPlug.isConnected())
			{
				double scaleValue;
				input1XPlug.getValue(scaleValue);
				pDirectController->m_scale = (float)scaleValue;
			}
			else
			{
				Errorf("mutliplyDivide node '%s' has connections on both input1X and input2X, can't export constant scale value from this node", outputDestNode.name().asChar());
			}
		}
	}

	//Search back up through the DG to locate the source transform attribute for the direct controller
	MPlug curInputPlug = dstPlug;

	bool bFound = false;
	while(1)
	{
		if(curNode.object().apiType() == MFn::kJoint)
		{
			bFound = true;
			break;
		}
		else
		{
			MPlug outputPlug;
			MPlugArray connectedPlugs;
			MString curNodeName = curNode.name();

			if(curNode.object().apiType() == MFn::kMultiplyDivide)
			{
				outputPlug = curNode.findPlug("outputX", false, &status);
			}
			else
			{
				outputPlug = curNode.findPlug("output", false, &status);
			}

			if(status == MS::kSuccess)
			{
				outputPlug.connectedTo(connectedPlugs, false, true, &status);
				Assertf(connectedPlugs.length() == 1, "Incorrect number of connections to output plug (%d), on '%s'", connectedPlugs.length(), curNodeName.asChar());

				curInputPlug = connectedPlugs[0];
				curNode.setObject(curInputPlug.node());
			}
			else
				break;
		}
	}

	//If we found the source joint and component plug, then create a driver from them..
	if(bFound)
	{
		AeDriver* pDriver = NULL;
		AeDriverComponent* pDriverComponent = NULL;

		CreateComponentizedDriver(theFile, &pDriver, &pDriverComponent, curInputPlug);
		
		Assert(pDriver);
		Assert(pDriverComponent);

		pDirectController->AddDriver(pDriver, pDriverComponent, 1.0f);

		return pDirectController;
	}
	else
	{
		delete pDirectController;
		return NULL;
	}
}

//-----------------------------------------------------------------------------

AeController* rageMayaAnimExprExport::CreateFloatCurveController(AeFile &theFile, MPlug dstPlug, MPlug srcPlug)
{
	MStatus status;

	AeFloatCurveController* pCurveController = new AeFloatCurveController();
	
	MFnAnimCurve srcAnimCurve(srcPlug.node());

	pCurveController->m_id = srcAnimCurve.name().asChar();

	int numKeys = srcAnimCurve.numKeys();

	for(int keyIdx=0; keyIdx < numKeys; keyIdx++)
	{
		AeCurveKey key;
		key.m_in = (float)srcAnimCurve.unitlessInput(keyIdx, &status);
		key.m_out = (float)srcAnimCurve.value(keyIdx, &status);

		pCurveController->m_keys.push_back(key);
	}

	MPlug inputPlug = srcAnimCurve.findPlug("input", &status);
	if(!inputPlug.isConnected())
	{
		//It's possible that the unitless curve in Maya isn't connected to anything through its input 
		//attribute.  This typically occurs when a unitless curve is being used as a constant which
		//feeds into a blend weighted controller
		return pCurveController;
	}

	MPlug driverPlug;

	MPlugArray inputConnections;
	inputPlug.connectedTo(inputConnections, true, false, &status);

	MFnDependencyNode inSourceNode(inputConnections[0].node(), &status);
	
	double unitConvFactor = 1.0f;

	//Check for a unit conversion node
	MFn::Type inSourceNodeType = inSourceNode.object().apiType();
	if( inSourceNodeType == MFn::kUnitConversion)
	{
		//Get the conversion factor
		MPlug convFactPlug = inSourceNode.findPlug("conversionFactor", &status);
		status = convFactPlug.getValue(unitConvFactor);
		
		//Bake this conversion factor into the keyframe data
		/*for(vector<AeCurveKey>::iterator it = pCurveController->m_keys.begin();
			it != pCurveController->m_keys.end();
			++it)
		{
			(*it).m_out *= (float)convFact;	
		}*/

		//Get the input of the unit conversion node
		MPlug convFactInputPlug = inSourceNode.findPlug("input");
		MPlugArray convFactInputPlugConn;
		convFactInputPlug.connectedTo(convFactInputPlugConn, true, false, &status);
		Assert(convFactInputPlugConn.length());
		driverPlug = convFactInputPlugConn[0];
	}
	else
	{
		driverPlug = inputConnections[0];
	}

	AeDriver* pDriver = NULL;
	AeDriverComponent* pDriverComponent = NULL;

	CreateComponentizedDriver(theFile, &pDriver, &pDriverComponent, driverPlug);
	
	Assert(pDriver);
	Assert(pDriverComponent);

	pCurveController->AddDriver(pDriver, pDriverComponent, (float)unitConvFactor);

	//Get the controllers output conversion factor
	float outputConvFactor = 1.0f;
	GetOutputConversionFactor(srcAnimCurve, outputConvFactor);
	pCurveController->m_outputUnitConversionFactor = outputConvFactor;

	return pCurveController;
}

//-----------------------------------------------------------------------------

AeController* rageMayaAnimExprExport::CreateBlendController(AeFile& theFile, MPlug dstPlug, MPlug srcPlug)
{
	MStatus status;

	MFnDependencyNode blendDepNode(srcPlug.node(), &status);
	if(status != MS::kSuccess)
		return NULL;

	AeBlendController* pBlendCtrl = new AeBlendController();
	pBlendCtrl->m_id = blendDepNode.name().asChar();
	pBlendCtrl->m_type = kFloat; //TODO : This needs to be set correctly based on the data types being blended...

	MPlug inputPlug = blendDepNode.findPlug("input", &status);
	bool bIsArray = inputPlug.isArray();
	unsigned int numElements = inputPlug.numElements();

	for(unsigned int i=0; i<numElements; i++)
	{
		MPlug curPlug = inputPlug.elementByPhysicalIndex(i, &status);

		MPlugArray connectedPlugs;
		curPlug.connectedTo(connectedPlugs, true, false, &status);
		if(connectedPlugs.length())
		{
			Assertf(connectedPlugs.length() == 1, "Found plug '%s', on node '%s' that is the destination plug for more than one connection",
				curPlug.name().asChar(), blendDepNode.name().asChar());

			AeController* pController = CreateController(theFile, curPlug, true);
			if(pController)
			{
				AeBlendControllerItem* pItem = new AeBlendControllerItem();
				pItem->m_pController = pController;
				pBlendCtrl->m_inputs.push_back(pItem);
			}
		}
	}

	//Get the controllers output conversion factor
	float outputConvFactor = 1.0f;
	GetOutputConversionFactor(blendDepNode, outputConvFactor);
	pBlendCtrl->m_outputUnitConversionFactor = outputConvFactor;

	return pBlendCtrl;
}

//-----------------------------------------------------------------------------

bool rageMayaAnimExprExport::FindExpressionForPlug(MPlug dstPlug, const string& inExpressions, string& retExpression)
{
	MStatus status;

	list< string > exprByPlugList;

	size_t startPos = 0;
	size_t semiPos = 0;
	size_t inExpressionsLength = inExpressions.length();

	if(!inExpressions.size())
		return false;

	//The incoming expression string may contain multiple lines of expressions, for different node/attribute pairs. 
	//So split up the complete expression string into a list of indivdual lines (one for each expression).
	while(startPos < inExpressionsLength )
	{
		size_t semiPos = inExpressions.find_first_of(";", startPos);
		if(semiPos != string::npos)
		{
			string plugExpr = inExpressions.substr(startPos, ((semiPos+1) - startPos));
			
			//Trim off the leading whitespace
			size_t nonWsStart = plugExpr.find_first_not_of(" \t\r\n");
			plugExpr = plugExpr.substr(nonWsStart);
	
			exprByPlugList.push_back(plugExpr);
			startPos = semiPos + 1;
		}
		else
		{
			//There isn't a semi colon, so there must only be one expression in the script text passed in
			exprByPlugList.push_back(inExpressions);
		}
	}

	if( exprByPlugList.size() == 1 )
	{
		//There is only one expression in the list, so it has to be the one we are looking for
		//since we found the plug by traversing the DG.
		retExpression = *exprByPlugList.begin();
	}
	else
	{
		//The supplied destination plug may be a unit conversion node, in which case
		//we need to step over that item in the graph to find the real desitination plug
		//for the expression
		MFn::Type dstPlugType = dstPlug.node().apiType();
		if(dstPlugType == MFn::kUnitConversion)
		{
			MFnDependencyNode fnUnitDep(dstPlug.node());
			MPlug unitOutputPlug = fnUnitDep.findPlug("output");

			MPlugArray unitOutputPlugConnections;
			unitOutputPlug.connectedTo(unitOutputPlugConnections, false, true);
			Assert(unitOutputPlugConnections.length() == 1);

			dstPlug = unitOutputPlugConnections[0];
		}

		//Get the full (name).(attribute) name of the plug
		MString dstPlugName = dstPlug.name();
		const char* szDstPlugName = dstPlugName.asChar();
		int dstPlugNameLen = dstPlugName.length();
	
		//Look for an expression in the list that matches the destination plug.  Note that
		//all of the expression in the list will take the form of (name).(attr) = ( expression )
		//so we just need to look at the start of the expression string.
		for(list<string>::iterator it = exprByPlugList.begin();
			it != exprByPlugList.end();
			++it)
		{
			if( (*it).compare(0, dstPlugNameLen, szDstPlugName) == 0)
			{
				retExpression = (*it);
				break;
			}
		}
	}

	return (retExpression.size() != 0 ? true : false );
}

//-----------------------------------------------------------------------------

AeController* rageMayaAnimExprExport::CreateExpressionController(AeFile& theFile, MPlug dstPlug, MPlug srcPlug)
{
	MStatus status;

	MFnExpression srcExpression(srcPlug.node());

	AeExpressionController* pExpressionController = new AeExpressionController();

	//Generate the id of the expression controller by combining the name of the expression node in maya
	//with the name of the plug being driven by it.. so first get the name of the plug being driven
	MPlug drivenPlug = dstPlug;
	MFn::Type dstDrivenPlugType = drivenPlug.node().apiType();
	if(dstDrivenPlugType == MFn::kUnitConversion)
	{
		MFnDependencyNode fnUnitDep(dstPlug.node());
		MPlug unitOutputPlug = fnUnitDep.findPlug("output");

		MPlugArray unitOutputPlugConnections;
		unitOutputPlug.connectedTo(unitOutputPlugConnections, false, true);
		Assert(unitOutputPlugConnections.length() == 1);

		drivenPlug = unitOutputPlugConnections[0];
	}

	//Build up the rest of the expression controller id...
	MString mstrDrivenPlugName = drivenPlug.name();
	string strDrivenPlugName(mstrDrivenPlugName.asChar());
	size_t dotPos = strDrivenPlugName.find('.');
	Assert(dotPos != string::npos);
	strDrivenPlugName[dotPos] = '_';

	stringstream exprCtrlNameStream;
	exprCtrlNameStream << srcExpression.name().asChar() << "_" << strDrivenPlugName;

	pExpressionController->m_id = exprCtrlNameStream.str();

	//Convert the MEL script expression into a generic expression tree
	MString expression;
	status = srcExpression.getExpression(expression);

	string plugExpression;
	bool bFindRes = FindExpressionForPlug(dstPlug, string(expression.asChar()), plugExpression);
	Assertf( bFindRes, "Failed to find expression for driven plug in expression node");

	ExprParser theParser;
	ExprTree* pExpTree = theParser.ParseMelExpression(plugExpression.c_str());
	if(!pExpTree)
	{
		//The parser failed...
		delete pExpressionController;
		return NULL;
	}
	
	pExpressionController->m_pExprTree = pExpTree;

	//Each of the input connections represents a driver for the expression
	//so iterate through them and create the associated drivers
	MPlug inputPlug = srcExpression.findPlug("input", &status);
	unsigned int numElements = inputPlug.numElements();

	for(unsigned int i=0; i<numElements; i++)
	{	
		MPlug inputElementPlug = inputPlug.elementByPhysicalIndex(i, &status);
		
		MPlug driverPlug;

		MPlugArray inputElementPlugConnections;
		inputElementPlug.connectedTo(inputElementPlugConnections, true, false, &status);
		
		if(inputElementPlugConnections.length())
		{
			Assertf(inputElementPlugConnections.length() == 1, "Found plug input[%d] on node '%s' that is the destination plug for more than one connection",
				i, srcExpression.name().asChar());

			MPlug driverPlug = inputElementPlugConnections[0];
			
			double unitConvFactor = 1.0f;

			//Check to see if a unit conversion node is in the DG
			if(driverPlug.node().apiType() == MFn::kUnitConversion)
			{
				//Get the conversion factor
				MFnDependencyNode unitConvDepNode(driverPlug.node());

				MPlug convFactPlug = unitConvDepNode.findPlug("conversionFactor", &status);
				status = convFactPlug.getValue(unitConvFactor);

				//Move on to the input of the unit conversion node
				MPlug unitConvInputPlug = unitConvDepNode.findPlug("input");
				
				MPlugArray unitConvInputPlugConnections;
				unitConvInputPlug.connectedTo(unitConvInputPlugConnections, true, false, &status);
				
				Assertf(unitConvInputPlugConnections.length() == 1, "More than one connection to input plug of node '%s'", unitConvDepNode.name().asChar());

				driverPlug = unitConvInputPlugConnections[0];
			}
		
			//Get the name of the driver attribute
			MString driverFullPlugName = driverPlug.name();
			string stdDriverFullPlugName = driverFullPlugName.asChar();
			size_t dotPos = stdDriverFullPlugName.find_last_of('.');
			Assert(dotPos != string::npos);
			string stdDriverPlugName = stdDriverFullPlugName.substr(dotPos+1);

			//Get the name of the driver
			MFnDependencyNode driverPlugNode(driverPlug.node());
			MString driverPlugNodeName = driverPlugNode.name();
			const char* dbgDriverPlugNodeName = driverPlugNodeName.asChar();

			AeDriver* pDriver = NULL;
			AeDriverComponent* pDriverComponent = NULL;

			CreateComponentizedDriver(theFile, &pDriver, &pDriverComponent, driverPlug);
	
			Assert(pDriver);
			Assert(pDriverComponent);

			pExpressionController->AddDriver(pDriver, pDriverComponent, (float)unitConvFactor);

			if( (strcmpi(pDriver->m_id.c_str(), driverPlugNodeName.asChar()) != 0) ||
				(strcmpi(pDriverComponent->m_id.c_str(), stdDriverPlugName.c_str()) != 0) )
			{
				//The resulting driver name doesn't match up with the node names in Maya.  This can happen for 
				//a variety of reasons (splitting out of driver components into unique drivers etc..) so we
				//need to fix up references to this driver in the expression tree so the expression name/attr pair
				//nodes reference the correct drivers
				queue<ExprNodeBase*> exprNodeQueue;
				exprNodeQueue.push(pExpressionController->m_pExprTree->m_pRoot);

				while(!exprNodeQueue.empty())
				{
					ExprNodeBase* pExprNode = exprNodeQueue.front();
					exprNodeQueue.pop();

					if( (pExprNode->GetSuperType() == ExprNodeBase::kOperand) &&
						( ((ExprNodeOperand*)pExprNode)->GetOperandType() == ExprNodeOperand::kNameAttrPair) )
					{
						ExprNodeNameAttrPair* pNameAttrPairNode = static_cast<ExprNodeNameAttrPair*>(pExprNode);
						if( (strcmpi(pNameAttrPairNode->m_name.c_str(), driverPlugNodeName.asChar()) == 0) &&
							(strcmpi(pNameAttrPairNode->m_attr.c_str(), stdDriverPlugName.c_str()) == 0) )
						{
							//This name/attr pair node is the one we are looking for that has the driver renamed
							//when it was created
							pNameAttrPairNode->m_name = pDriver->m_id;
							pNameAttrPairNode->m_attr = pDriverComponent->m_id;
						}
					}

					for(unsigned int i=0; i<pExprNode->GetChildCount(); i++)
						exprNodeQueue.push(pExprNode->GetChild(i));
				}
			}
		}
	}

	//Get the controllers output conversion factor
	float outputConvFactor = 1.0f;
	GetOutputConversionFactor(srcExpression, outputConvFactor);
	pExpressionController->m_outputUnitConversionFactor = outputConvFactor;

	return pExpressionController;
}

//-----------------------------------------------------------------------------

bool rageMayaAnimExprExport::IsTRSAttribute(const char* szName)
{
	const char* tSearch = strstr(szName, "translate");
	if(tSearch == szName)
		return true;

	const char* rSearch = strstr(szName, "rotate");
	if(rSearch == szName)
		return true;

	const char* sSearch = strstr(szName, "scale");
	if(sSearch == szName)
		return true;

	return false;
}

//-----------------------------------------------------------------------------

MStatus rageMayaAnimExprExport::CollectDriverComponentAttributes(MPlug& driverPlug, 
															  eComponentType& type, eComponentRotationOrder& rotOrder, eComponentAxis& axis)
{
	MStatus status(MS::kSuccess);

	string fullPlugName = driverPlug.name().asChar();
	
	size_t attrStart = fullPlugName.find_last_of('.');
	string attribName = fullPlugName.substr(attrStart + 1);
	size_t attribNameLen = attribName.size();

	transform(attribName.begin(), attribName.end(), attribName.begin(), (int(*)(int))tolower);
	
	//Set the defaults
	rotOrder = kXYZ;

	if( attribName == "rotatex" )
	{
		type = kQuat;
		axis = kXAxis;
	}
	else if( attribName == "rotatey" )
	{
		type = kQuat;
		axis = kYAxis;
	}
	else if( attribName == "rotatez" )
	{
		type = kQuat;
		axis = kZAxis;
	}
	else if( attribName == "rotate" )
	{
		type = kQuat;
		axis = kAxisAll;

		MFnDependencyNode fnDep(driverPlug.node(), &status);
		if(fnDep.object().apiType() == MFn::kJoint)
		{
			MFnIkJoint fnJoint(fnDep.object(), &status);

			double rot[3];
			MTransformationMatrix::RotationOrder order;
			fnJoint.getOrientation(rot, order);
			switch(order)
			{
			case MTransformationMatrix::kXYZ:
				rotOrder = kXYZ;
				break;
			case MTransformationMatrix::kYZX:
				rotOrder = kYZX;
				break;
			case MTransformationMatrix::kZXY:
				rotOrder = kZXY;
				break;
			case MTransformationMatrix::kXZY:
				rotOrder = kXZY;
				break;
			case MTransformationMatrix::kYXZ:
				rotOrder = kYXZ;
				break;
			case MTransformationMatrix::kZYX:
				rotOrder = kZYX;
				break;
			default:
				Assertf(0, "Unknown rotation order found on joint '%s'", fnJoint.name().asChar());
				break;
			}
		}
	}
	else if( attribName == "translatex" )
	{
		type = kVector;
		axis = kXAxis;
	}
	else if( attribName == "translatey" )
	{
		type = kVector;
		axis = kYAxis;
	}
	else if( attribName == "translatez" )
	{
		type = kVector;
		axis = kZAxis;
	}
	else if( attribName == "translate" )
	{
		type = kVector;
		axis = kAxisAll;
	}
	else
	{
		type = kFloat;
		axis = kAxisNone;
	}

	return status;
}

//-----------------------------------------------------------------------------

MStatus rageMayaAnimExprExport::CreateComponentizedDriver(AeFile& theFile, AeDriver** pOutDriver, AeDriverComponent** pOutDriverComponent, MPlug& driverPlug)
{
	MFnDependencyNode driverNode(driverPlug.node());
	MDagPath driverDagPath;
	MDagPath::getAPathTo(driverPlug.node(), driverDagPath);

	MString msDriverPath = driverDagPath.partialPathName();
	string driverId = msDriverPath.asChar();

	string componentId = driverPlug.name().asChar();
	size_t attrStart = componentId.find_last_of('.');
	componentId = componentId.substr(attrStart + 1);

	bool isTrsAttr = IsTRSAttribute(componentId.c_str());
	if(!isTrsAttr)
	{
		//If the driver attribute isn't referencing a particular transform component (i.e. the X portion of the translation)
		//then it needs to be split out into it's own driver and driver component
		driverId.append("_");
		driverId.append(componentId);
	}
	
	AeDriver* pDriver = NULL;

	//Check to see if this driver has already been added to the library...
	for(vector< AeDriver* >::iterator it = theFile.m_driverLibrary.begin();
		it != theFile.m_driverLibrary.end();
		++it)
	{
		AeDriver *pCurDriver = (*it);
		if(pCurDriver->m_id == driverId)
		{
			pDriver = pCurDriver;
			(*pOutDriver) = pDriver;
			break;
		}
	}

	if(!pDriver)
	{
		//The driver doesn't already exist, so create it
		pDriver = new AeDriver();
		pDriver->m_id = driverId;

		//Generate a runtime hashed id for the driver.  To do this we need to check if the driver is a joint or not.  
		//If it is a joint, then it's bone id is 0 if its the root, otherwise its the hashed name.  If the driver
		//isn't a joint, then just has the driver id to generate the runtime hash id.
		MFnDagNode driverFnDag(driverDagPath);
		if(driverFnDag.object().apiType() == MFn::kJoint)
		{
			int parentCount = driverFnDag.parentCount();
			Assertf(parentCount, "Found a joint that has no parent nodes (not even the world root), scene may be corrupt.");

			MObject driverParent = driverFnDag.parent(0);
			MFn::Type driverParentType = driverParent.apiType();
			if(driverParentType == MFn::kWorld)
			{
				//The joint is the root joint of the joint hierarchy, so it's boneId will always be 0
				pDriver->m_boneId = 0;
			}
			else
			{
				string userBoneId;
				if(exprExportUtility::GetStringFromAttribute(driverFnDag.object(), "boneID", userBoneId))
				{
					//A user defined boneId was set on the joint, so hash it instead of hashing the joint name
					pDriver->m_boneId = crAnimTrack::ConvertBoneNameToId(userBoneId.c_str());
				}
				else
				{
					//Hash the joint name
					pDriver->m_boneId = crAnimTrack::ConvertBoneNameToId(driverId.c_str());
				}
			}
		}
		else
		{
			//Anything other than a joint always needs to have its named hashed to generate the bone id
			pDriver->m_boneId = crAnimTrack::ConvertBoneNameToId(driverId.c_str());
		}
			
		//Add the driver to the driver library
		theFile.m_driverLibrary.push_back(pDriver);
		(*pOutDriver) = pDriver;
	}

	//Check to see if the driver component exists already under the driver
	bool bComponentExists = false;
	for(vector< AeDriverComponent >::iterator it = pDriver->m_components.begin();
		it != pDriver->m_components.end();
		++it)
	{
		if((*it).m_id == componentId)
		{
			(*pOutDriverComponent) = &(*it);
			bComponentExists = true;
			break;
		}
	}

	if(!bComponentExists)
	{
		//The component doesn't exist, so add it
		AeDriverComponent cmp;
		cmp.m_id = componentId;

		CollectDriverComponentAttributes(driverPlug, cmp.m_type, cmp.m_rotOrder, cmp.m_axis);

		//If the driver node is a joint, and this is a transform based accessor then we need to assign the standard
		//bone animation track id's
		if( isTrsAttr && (driverNode.object().apiType() == MFn::kJoint))
		{
			Assertf( (cmp.m_type == kVector) || (cmp.m_type == kQuat), 
				"Found a joint driver component that isn't a quat or vector type. %.%", driverId.c_str(), componentId.c_str());

			if(cmp.m_type == kVector)
				cmp.m_trackId = kTrackBoneTranslation;
			else if(cmp.m_type == kQuat)
				cmp.m_trackId = kTrackBoneRotation;
		}
		else
		{
			if(!m_trackIdMgr.Lookup(driverId.c_str(), componentId.c_str(), (unsigned int&)cmp.m_trackId))
			{
				Errorf("Failed to generate valid track id for '%s'", componentId.c_str());
			}
		}

		pDriver->m_components.push_back(cmp);		
		(*pOutDriverComponent) = &pDriver->m_components[pDriver->m_components.size() - 1];
	}

	return MS::kSuccess;
}


//-----------------------------------------------------------------------------

