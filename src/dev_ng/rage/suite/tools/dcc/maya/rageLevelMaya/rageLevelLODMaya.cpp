// 
// rageLevelMaya/rageLevelLODMaya.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//

#pragma warning(disable: 4668)
#include <maya/MGlobal.h>
#include <maya/MFileIO.h>
#include <maya/MItDag.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshEdge.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MFnSet.h>
#include <maya/MPlug.h>
#include <maya/MCommonSystemUtils.h>
#pragma warning(error: 4668)

#include <d3d9.h>
#include <D3d9types.h>
#include <D3dx9math.h>

#include "rageLevelLODMaya.h"

#include "atl/string.h"
#include "system/param.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "demesh/demesh.h"
#include "rageLevel/rageLevel.h"
#include "rageShaderMaterial/shaderMaterialGeoParamValueTexture.h"
#include "rageShaderMaterial/rageShaderMaterialTemplate.h"
#include "grcore/state.h"
#include "grcore/device.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "grcore/light.h"
#include "grcore/texture.h"
#include "grcore/image.h"
#include "rageShaderBucket/rageShaderBucketManager.h"
#include "grmodel/setup.h"
#include "grmodel/shader.h"
#include "grmodel/shaderfx.h"
#include "grmodel/shadergroup.h"
#include "init/modulesettings.h"

using namespace rage;

#define RAGE_LEVEL_LOD_MODEL_DATA_CACHE_SIZE 25

namespace rage {
	XPARAM(width);
	XPARAM(height);
	XPARAM(hidewindow);
	XPARAM(noquits);
}

//////////////////////////////////////////////////////////////

rageLevelLODMaya::rageLevelLODMaya()
:
m_szTexturePrefix(NULL)
{

	m_bCalcGroundPlane = true;

	m_RenderTechnique = RAGE_LEVEL_LOD_RENDER_SIDE_SQUARE;

	m_vColor0.Set(1,1,1,1);
	m_vColor1.Set(1,1,1,1);
	m_vColor2.Set(1,1,1,1);
	
	m_pSetup				= NULL;
	m_pShaderBucketManager	= NULL;
	m_pViewport				= NULL;
	m_pOutputTextureStream  = NULL;

	for (int i=0; i<RAGE_LEVEL_LOD_RENDER_TECHNIQUE_COUNT; i++)
	{
		m_apSaveTargetRT[i]		= NULL;
		m_apSaveDepthTargetRT[i]= NULL;
		m_apSaveTargetSurface[i]= NULL;
	}
}

//////////////////////////////////////////////////////////////

rageLevelLODMaya::~rageLevelLODMaya()
{
	Shutdown();
}

//////////////////////////////////////////////////////////////

MStatus rageLevelLODMaya::doIt( const MArgList& args ) 
{
	if (args.length() < 3)
		return MS::kFailure;

	MString sInputLevelFileName;
	MString sInputResourceFolder;
	MString sInputSectorName;
	MString sInputTemplateFileName;
	MString sInputLODFolderName;
	MStringArray sInputShaderBucketFileName;
	MStringArray sTexturePrefix;
	Vector3 vMin(-6000,0,-6000);
	Vector3 vMax(6000,0,6000);
	int		nSectorSize = 800;
	bool	bOnlyCustomLODs = false;
	bool	bSkipMayaMeshCreate = false;
	bool	bSkipTopDown = false;

	for (u32 i=0; i<args.length(); i++)
	{
		if (args.asString(i)=="-level" && args.length() > i+1)
		{
			sInputLevelFileName = args.asString(i+1);
			i++;
		}
		else if (args.asString(i)=="-template" && args.length() > i+1)
		{
			sInputTemplateFileName = args.asString(i+1);
			i++;
		}
		else if (args.asString(i)=="-inputfolder" && args.length() > i+1)
		{
			sInputLODFolderName = args.asString(i+1);
			i++;
		}
		else if (args.asString(i)=="-inputsectorfile" && args.length() > i+1)
		{
			rageSectorData SectorData;
			if (!SectorData.Load(args.asString(i+1).asChar()))
			{
				SectorData.LoadDefault();
			}

			vMin = SectorData.GetExtentsMin();
			vMax = SectorData.GetExtentsMax();
			nSectorSize = SectorData.GetSectorSize();
			i++;
		}
		else if (args.asString(i)=="-pass" && args.length() > i+2)
		{
			sInputShaderBucketFileName.append(args.asString(i+1));
			i++;
			sTexturePrefix.append(args.asString(i+1));
			i++;
		}
		else if (args.asString(i)=="-color0" && args.length() > i+4)
		{
			m_vColor0.x = (float)args.asDouble(i+1);
			m_vColor0.y = (float)args.asDouble(i+2);
			m_vColor0.z = (float)args.asDouble(i+3);
			m_vColor0.w = (float)args.asDouble(i+4);
			i+=4;
		}
		else if (args.asString(i)=="-color1" && args.length() > i+4)
		{
			m_vColor1.x = (float)args.asDouble(i+1);
			m_vColor1.y = (float)args.asDouble(i+2);
			m_vColor1.z = (float)args.asDouble(i+3);
			m_vColor1.w = (float)args.asDouble(i+4);
			i+=4;
		}
		else if (args.asString(i)=="-color2" && args.length() > i+4)
		{
			m_vColor2.x = (float)args.asDouble(i+1);
			m_vColor2.y = (float)args.asDouble(i+2);
			m_vColor2.z = (float)args.asDouble(i+3);
			m_vColor2.w = (float)args.asDouble(i+4);
			i+=4;
		}
		else if (args.asString(i)=="-topdown" && args.length() > i+5)
		{
			vMin.x = (float)args.asDouble(i+1);
			vMin.z = (float)args.asDouble(i+2);
			vMax.x = (float)args.asDouble(i+3);
			vMax.z = (float)args.asDouble(i+4);
			nSectorSize = args.asInt(i+5);
			i+=5;
		}
		// Optional parameter to only build lods for sectors with custom geometry
		else if (args.asString(i)=="-inputResourceFolder")
		{
			//sInputResourceFolder = args.asString(i+1); We can not support resources right now because tool builds can not load resources
			i++;
		}
		// Optional parameter to only build lods for sectors with custom geometry
		else if (args.asString(i)=="-onlycustom")
		{
			bOnlyCustomLODs = true;
		}
		// Optional parameter to skip the creation of the maya mesh should only be used with -onlycustom
		else if (args.asString(i)=="-skipcreate")
		{
			bSkipMayaMeshCreate = true;
		}
		// Optional parameter to skip the creation of the maya mesh should only be used with -onlycustom
		else if (args.asString(i)=="-skiptopdown")
		{
			bSkipTopDown = true;
		}
		// Optional parameter to just do a single sector
		else if (args.asString(i)=="-sector" && args.length() > i+1)
		{
			sInputSectorName = args.asString(i+1);
			i++;
		}
	}

	// Set the input resource folder name (NULL if does not exist)
	const char * szInputResourceFolder = (sInputResourceFolder.length() > 0) ? sInputResourceFolder.asChar() : NULL;
	
	rageLevel * pLevel = new rageLevel;
	if (!pLevel->Load(sInputLevelFileName.asChar()))
	{
		return MS::kFailure;
	}

	// Generate the output folder
	char szOutputLODTextureFileName[512];
	pLevel->GetOutputLODTextureFileName(szOutputLODTextureFileName, sizeof(szOutputLODTextureFileName));
	ASSET.CreateLeadingPath(szOutputLODTextureFileName);

	// Open the output texture file
	fiStream * pStream = fiStream::Create(szOutputLODTextureFileName);
	if (!pStream)
	{
		Errorf("Could not open texture file %s\n", szOutputLODTextureFileName);
		return MS::kFailure;
	}

	int nInputSectorIndex = -1;
	if (sInputSectorName.length()>0)
	{
		for (int i=0; i<pLevel->GetSectorCount(); i++)
		{
			if (stricmp(pLevel->GetSector(i)->GetName(), sInputSectorName.asChar())==0)
			{
				nInputSectorIndex = i;
				break;
			}
		}
	}

	if (!Init())
	{
		pStream->Close();
		pStream = NULL;
		return MS::kFailure;
	}

	// Make sure we have a bucket file name
	if (!sInputShaderBucketFileName.length())
	{
		atString sShaderBucketFileName;
		initModuleSettings::GetModuleSetting(("LODShaderBucketFileName"), sShaderBucketFileName);
		if (!sShaderBucketFileName.GetLength())
			initModuleSettings::GetModuleSetting(("ShaderBucketFileName"), sShaderBucketFileName);
		sInputShaderBucketFileName.append(sShaderBucketFileName.c_str());
	}

	// Load all the data and export the LODs
	pLevel->LoadData(szInputResourceFolder);

	// Run through all the passes
	for (u32 iPass=0; iPass < sInputShaderBucketFileName.length(); iPass++)
	{
		// Set up the texture prefix
		if (sTexturePrefix.length()>iPass)
			m_szTexturePrefix = sTexturePrefix[iPass].asChar();
		else
			m_szTexturePrefix = NULL;

		// Create all the output texture files
		GetOutputLODTextureFileName(pLevel, szOutputLODTextureFileName, sizeof(szOutputLODTextureFileName));
		ASSET.CreateLeadingPath(szOutputLODTextureFileName);

		// Read in the material group file name if we are doing an incremental of a single sector.
		char szMaterialGroupFileName[512];
		GetOutputLODMaterialGroupFileName(pLevel, szMaterialGroupFileName, sizeof(szMaterialGroupFileName));
		if (nInputSectorIndex >= 0)
		{
			m_RageMaterialGroup.LoadFromXML(szMaterialGroupFileName);
		}

		m_pOutputTextureStream = fiStream::Create(szOutputLODTextureFileName);
		if (!m_pOutputTextureStream)
		{
			Errorf("Could not open texture file %s\n", szOutputLODTextureFileName);
			continue;
		}

		// Right it to the parent texture file
		fprintf(pStream, "%s\n", szOutputLODTextureFileName);

		// Load the correct bucket manager
		m_pShaderBucketManager->LoadFromXML(sInputShaderBucketFileName[iPass].asChar());
		m_pShaderBucketManager->Validate();

		// Add a force state to fix the alpha issue
		rageShaderBucketState state;
		state.Init((int)grcsColorWrite, (u32)grccwRGBA);
		m_pShaderBucketManager->AddForceState(state);

		if (!GenerateTopDownTextures(vMin, vMax, nSectorSize, pLevel, sInputTemplateFileName.asChar(), szInputResourceFolder, bSkipTopDown))
		{
			Errorf("GenerateTopDownTextures() failed.\n");
			continue;
		}

		for (int i=0; i<pLevel->GetSectorCount(); i++)
		{
			if ((nInputSectorIndex < 0 || nInputSectorIndex == i))
			{
				const rageLevelSector * pSector = pLevel->GetSector(i);

				Displayf("Generating Sector LOD for %s (%d / %d)\n", pSector->GetName(), i, pLevel->GetSectorCount());

				rageLevelLODMaya::ReturnStatus obReturnStatus = GenerateSectorLOD(pLevel, i, sInputTemplateFileName.asChar(), sInputLODFolderName.asChar(), szInputResourceFolder, (iPass==0), bOnlyCustomLODs, bSkipMayaMeshCreate);
				if (obReturnStatus == rageLevelLODMaya::Failure)
				{
					Errorf("GenerateSectorLOD failed.\n");
				}

				// Lets get everything back to square one if we did not load a custom lod file or if we are just generating a texture for the other passes
				if (!bOnlyCustomLODs && iPass==0)
				{
					MFileIO::newFile(true);
				}
			}
		}

		// Write out the shadergroup file only when we have written out the models this will make sure the indices are the same
		if (iPass==0)
		{
			char szShaderGroupFileName[512];
			pLevel->GetOutputLODShaderGroupFileName(szShaderGroupFileName, sizeof(szShaderGroupFileName));
			m_RageMaterialGroup.WriteToShaderGroupFile(szShaderGroupFileName);
			m_RageMaterialGroup.SaveToXML(szMaterialGroupFileName);
		}

		m_RageMaterialGroup.Reset();

		// Reset the cache
		m_ModelDataCache.Reset();

		// Reset the bucket manager
		m_pShaderBucketManager->Reset();

		// Make sure we unload all that is in our cache
		for (int i=0; i<pLevel->GetSectorCount(); i++)
		{
			pLevel->UnloadSectorData(i);
		}
	}

	pStream->Close();
	pStream = NULL;
	
	pLevel->UnloadData();

	Shutdown();

	delete pLevel;
	pLevel = NULL;

	return MS::kSuccess;
}

const char * rageLevelLODMaya::GetOutputLODTextureFileName(const rageLevel * pLevel, char * szBuffer, const int nSizeOfBuffer) const
{
	memset(szBuffer, 0, sizeof(nSizeOfBuffer));
	if (m_szTexturePrefix)
		formatf(szBuffer, nSizeOfBuffer, "%s%s/textures.txt", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer), m_szTexturePrefix);
	else
		formatf(szBuffer, nSizeOfBuffer, "%stextures.txt", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer));
	return szBuffer;
}

const char * rageLevelLODMaya::GetOutputLODMaterialGroupFileName(const rageLevel * pLevel, char * szBuffer, const int nSizeOfBuffer) const
{
	memset(szBuffer, 0, sizeof(nSizeOfBuffer));
	if (m_szTexturePrefix)
		formatf(szBuffer, nSizeOfBuffer, "%s%s/lod.mtlgroup", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer), m_szTexturePrefix);
	else
		formatf(szBuffer, nSizeOfBuffer, "%slod.mtlgroup", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer));
	return szBuffer;
}

const char * rageLevelLODMaya::GetSectorLODTextureFileName(const rageLevel * pLevel, const char * szName, char * szBuffer, const int nSizeOfBuffer) const
{
	memset(szBuffer, 0, sizeof(nSizeOfBuffer));
	if (m_szTexturePrefix)
		formatf(szBuffer, nSizeOfBuffer, "%s%s/%s.dds", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer), m_szTexturePrefix, szName);
	else
		formatf(szBuffer, nSizeOfBuffer, "%s%s.dds", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer), szName);
	return szBuffer;
}

const char * rageLevelLODMaya::GetSectorLODMaterialFileName(const rageLevel * pLevel, const char * szName, char * szBuffer, const int nSizeOfBuffer) const
{
	memset(szBuffer, 0, sizeof(nSizeOfBuffer));
	if (m_szTexturePrefix)
		formatf(szBuffer, nSizeOfBuffer, "%s%s/%s.mtlgeo", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer), m_szTexturePrefix, szName);
	else
		formatf(szBuffer, nSizeOfBuffer, "%s%s.mtlgeo", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer), szName);
	return szBuffer;
}

const char * rageLevelLODMaya::GetSectorLODMeshFileName(const rageLevel * pLevel, const char * szName, char * szBuffer, const int nSizeOfBuffer) const
{
	memset(szBuffer, 0, sizeof(nSizeOfBuffer));
	formatf(szBuffer, nSizeOfBuffer, "%s%s.mesh", pLevel->GetOutputLODFolder(szBuffer, nSizeOfBuffer), szName);
	return szBuffer;
}

bool rageLevelLODMaya::GenerateTopDownTextures(const Vector3 & vMin, const Vector3 & vMax, const int nSectorSize, rageLevel * pLevel, const char * szTemplateFileName, const char * szInputResourceFolder, const bool bSkipTopDown)
{
	try 
	{

	m_SectorData.Reset();
	m_SectorData.GenerateUniformSectors(vMin, vMax, nSectorSize);

	int nMaxLoadedCount = 0;

	for (int iSector=0; iSector<m_SectorData.GetUniformSectorCount(); iSector++)
	{
		const rageSector & RageSector = m_SectorData.GetUniformSector(iSector);

		// Set up the new view port
		const Vector2 & vExtentsMin = RageSector.GetExtentsMin();
		const Vector2 & vExtentsMax = RageSector.GetExtentsMax();
		const Vector2 & vCenter		= RageSector.GetCenter();
		const float &	fRadius		= RageSector.GetRadius();

		Displayf("Generating top down texture %s, (%f, %f) (%f, %f)", RageSector.GetName(), vExtentsMin.x, vExtentsMin.y, vExtentsMax.x, vExtentsMax.y);

		m_vPlanes[4][0].Set(vExtentsMin.x, 0.0f, vExtentsMin.y);
		m_vPlanes[4][1].Set(vExtentsMin.x, 0.0f, vExtentsMax.y);
		m_vPlanes[4][2].Set(vExtentsMax.x, 0.0f, vExtentsMax.y);
		m_vPlanes[4][3].Set(vExtentsMax.x, 0.0f, vExtentsMin.y);

		float fX = (m_vPlanes[4][2].x - m_vPlanes[4][0].x) / 2.0f;
		float fY = (m_vPlanes[4][2].z - m_vPlanes[4][0].z) / 2.0f;

		// Get the direction of the camera
		Vector3 v1 = m_vPlanes[4][1] - m_vPlanes[4][0];
		Vector3 v2 = m_vPlanes[4][2] - m_vPlanes[4][0];
		v1.Cross(v2);
		v1.Normalize();

		m_vPlaneDirection[4] = v1;

		// Set the world matrix for the camera
		Vector3 vTo(vCenter.x, 0.0f, vCenter.y);
		Vector3 vFrom = vTo;
		vFrom += v1 * fRadius;

		Matrix34 m34;
		m34.Identity();
		m34.d.Set(vFrom);
		m34.LookAt(vTo, ZAXIS);
		m_pViewport->SetCameraMtx(m34);

		m_pViewport->Ortho(-fX, fX, -fY, fY, 0.0f, fRadius * 2.0f);

		// Flag to see if we have data to render
		bool bIsValid = false;

		int nLoadedCount = 0;
		// Load the sectors needed to be rendered
		for (int i=0; i<pLevel->GetSectorCount(); i++)
		{
			const rageLevelSector * pSector = pLevel->GetSector(i);
			Vector4 vBoundingSphereFlat;
			pSector->GetBoundingSphereFlat(vBoundingSphereFlat);

			if (m_pViewport->IsModelSphereVisible(vBoundingSphereFlat))
			{
				// Something is in here so it is valid
				bIsValid = true;

				// Check to see if we are skipping the render part this is done after the validation flag is set
				if (bSkipTopDown)
					break;

				// Increment the count
				nLoadedCount++;

				// Make sure it is not already loaded
				if (!pSector->IsDataLoaded())
				{
					// Load the graphics objects
					if (!pLevel->LoadSectorData(i, szInputResourceFolder))
					{
						Errorf("\tLoadSectorData failed for sector %s.", pSector->GetName());
						return false;
					}

					// Add it to the cache
					m_ModelDataCache.Add(i);
				}

				// Add it to the bucket manager
				AddSectorToBucketManager(pSector);
			}
		}

		if (!bIsValid)
		{
			Displayf("\tNothing visible in uniform sector '%s', skipping top-down rendering for this sector", RageSector.GetName() );
			continue;
		}
		else
		{
			Displayf("\t%d non-uniform sector(s) visible from uniform sector '%s'", nLoadedCount, RageSector.GetName() );
			if(nLoadedCount > nMaxLoadedCount)
				nMaxLoadedCount = nLoadedCount;
		}

		// Set the composite matrix
		m_amUniformSectorCompositeMatrices.Grow() = m_pViewport->GetFullCompositeMtx();
		m_apUniformSectors.Grow() = &m_SectorData.GetSector(iSector);

		// Get the output names
		char szSectorLODTextureFileName[512];
		char szSectorLODMaterialFileName[512];
		GetSectorLODMaterialFileName(pLevel, m_SectorData.GetSector(iSector).GetName(), szSectorLODMaterialFileName, sizeof(szSectorLODMaterialFileName));
		GetSectorLODTextureFileName(pLevel, m_SectorData.GetSector(iSector).GetName(), szSectorLODTextureFileName, sizeof(szSectorLODTextureFileName));

		// Check to see if we want to render the top down part
		if (!bSkipTopDown)
		{
			// Create the render function functor
			atFunctor2< bool, const void * , float * > RenderFunction;
			RenderFunction.Reset< rageLevelLODMaya, &rageLevelLODMaya::RenderTopDownProjection > ( this  );

			// Next step in generating the LOD is to render the sector into a texture
			if (!RenderSectorToTexture(RenderFunction, RAGE_LEVEL_LOD_TOP_DOWN_TEXTURE_WIDTH, RAGE_LEVEL_LOD_TOP_DOWN_TEXTURE_HEIGHT, szSectorLODTextureFileName, szSectorLODMaterialFileName, szTemplateFileName, &RageSector))
			{
				Errorf("\tCould not render uniform sector into texture");
			}

			// Release all from the bucket manager
			m_pShaderBucketManager->DeleteAllData();

			// Free up some memory if we need to
			FreeModelData(pLevel);
		}

		// Add this to the material group
		m_RageMaterialGroup.AddMaterial(szSectorLODMaterialFileName);

		// Write this name to the output texture file
		fprintf(m_pOutputTextureStream, "%s\n", szSectorLODTextureFileName);

		Displayf("\tFinished Generating top down texture %s", RageSector.GetName());
	}

	Displayf("Maximum number of non-uniform sectors visible from a single uniform sector was : %d", nMaxLoadedCount);
	}
	catch (std::exception) {
		Displayf("Bug info : %d", 2);
	}
	return true;
}

// You pass in which pass.  Only textures need to be generated on each of the passes.  Models will be generated on the first.
rageLevelLODMaya::ReturnStatus rageLevelLODMaya::GenerateSectorLOD(rageLevel * pLevel, const int nSectorIndex, const char * szTemplateFileName, const char * szInputLODFolderName, const char * szInputResourceFolder, const bool bFirstPass, const bool bOnlyCustomLODs, const bool bSkipMayaMeshCreate)
{
	const rageLevelSector * pSector = pLevel->GetSector(nSectorIndex);

	if (!pSector->IsUniformSector())
	{
		// Set the technique
		const Vector3 & vMin = pSector->GetBoundingBox().GetMinPosition();
		const Vector3 & vMax = pSector->GetBoundingBox().GetMaxPosition();

		float fDiffX = vMax.x - vMin.x;
		float fDiffY = vMax.y - vMin.y;
		float fDiffZ = vMax.z - vMin.z;

		// If the height is greater then both the width and the depth use the vertical
		if (fDiffY > fDiffX && fDiffY > fDiffZ)
		{
			m_RenderTechnique = RAGE_LEVEL_LOD_RENDER_SIDE_VERTICAL;
			Displayf ("Rendering with the side vertical technique\n");
		}
		else if ((fDiffX * 0.5f) > fDiffY && (fDiffZ * 0.5f) > fDiffY)
		{
			m_RenderTechnique = RAGE_LEVEL_LOD_RENDER_SIDE_HORIZONTAL;
			Displayf ("Rendering with the side horizontal technique\n");
		}
		else
		{
			m_RenderTechnique = RAGE_LEVEL_LOD_RENDER_SIDE_SQUARE;
			Displayf ("Rendering with the size square technique\n");
		}
	}
	else
	{
		m_RenderTechnique = RAGE_LEVEL_LOD_RENDER_TOP_DOWN;
		Displayf ("Rendering with the top down technique\n");
	}
	
	// First step is to combine all the meshes into one Maya mesh
	MString outputMeshName;
	if (bFirstPass)
	{
		rageLevelLODMaya::ReturnStatus obReturnStatus = CombineSectorToMayaMesh(pLevel, nSectorIndex, outputMeshName, szInputLODFolderName, bOnlyCustomLODs, bSkipMayaMeshCreate);
		if (obReturnStatus == rageLevelLODMaya::Success_But_Do_Not_Process_Further)
		{
			return rageLevelLODMaya::Success;
		}
		if (obReturnStatus == rageLevelLODMaya::Failure)
		{
			Errorf("CombineSectorToMayaMesh failed.");
			return rageLevelLODMaya::Failure;
		}
	}

	// Make sure we are not a uniform sector
	if (!pSector->IsUniformSector())
	{
		// Load the model data if we are uniform load the surrounding area
		if (!LoadSectorModelData(pLevel, nSectorIndex, szInputResourceFolder))
		{
			Errorf("Could not load the model data.");
			return rageLevelLODMaya::Failure;
		}

		// Create the render function functor
		atFunctor2< bool, const void * , float * > RenderFunction;
		RenderFunction.Reset< rageLevelLODMaya, &rageLevelLODMaya::RenderSideProjection >( this  );

		// Get the output names
		char szSectorLODTextureFileName[512];
		char szSectorLODMaterialFileName[512];
		GetSectorLODMaterialFileName(pLevel, pSector->GetName(), szSectorLODMaterialFileName, sizeof(szSectorLODMaterialFileName));
		GetSectorLODTextureFileName(pLevel, pSector->GetName(), szSectorLODTextureFileName, sizeof(szSectorLODTextureFileName));

		// Next step in generating the LOD is to render the sector into a texture
		if (!RenderSectorToTexture(RenderFunction, RAGE_LEVEL_LOD_SIDE_TEXTURE_WIDTH, RAGE_LEVEL_LOD_SIDE_TEXTURE_HEIGHT, szSectorLODTextureFileName, szSectorLODMaterialFileName, szTemplateFileName, pSector))
		{
			return rageLevelLODMaya::Failure;
		}

		// Remove it from the bucket manager
		m_pShaderBucketManager->DeleteAllData();

		// Free up some memory if we need to
		FreeModelData(pLevel);

		// Add this to the material group
		m_RageMaterialGroup.AddMaterial(szSectorLODMaterialFileName);

		// Write this name to the output texture file
		fprintf(m_pOutputTextureStream, "%s\n", szSectorLODTextureFileName);
	}

	if (bFirstPass)
	{
		// Export out the mesh file
		if (!ExportSectorMesh(pLevel, nSectorIndex, outputMeshName.asChar()))
		{
			Errorf("ExportSectorMesh failed.");
			return rageLevelLODMaya::Failure;
		}
	}

	return rageLevelLODMaya::Success;
}

#define RENDER_PROJECTION_RENDER_FORMAT	 grctfA32B32G32R32F
#define RENDER_PROJECTION_TEX_FORMAT	D3DFMT_A32B32G32R32F

bool rageLevelLODMaya::Init()
{
	// Initialize the system parameters
	// sysParam::Init(0,0);

	// Set the flag to not crash
	PARAM_noquits.Set("");

	// Set the flag for no window
	PARAM_hidewindow.Set("");

	// Set the window width and height from the material
	char szWidth[16];
	char szHeight[16];
	formatf(szWidth, sizeof(szWidth), "%d", RAGE_LEVEL_LOD_SCREEN_WIDTH);
	formatf(szHeight, sizeof(szHeight), "%d", RAGE_LEVEL_LOD_SCREEN_HEIGHT);
	PARAM_width.Set(szWidth);
	PARAM_height.Set(szHeight);

	INIT_PARSER;
	rageShaderMaterialTemplate::InitShaderMaterialLibrary();

	atString sDefaultAssetDirectory;
	atString sDefaultShaderDirectory;
	atString sDefaultGlobalTextureDirectory;

	initModuleSettings::GetModuleSetting(("PathToAssets"), sDefaultAssetDirectory);
	initModuleSettings::GetModuleSetting(("PathToCompiledShaders"), sDefaultShaderDirectory);
	initModuleSettings::GetModuleSetting(("PathToGlobalTextures"), sDefaultGlobalTextureDirectory);

	m_pSetup = new grmSetup;
	m_pSetup->Init((const char *)sDefaultAssetDirectory, "RAGE Level Viewer");

	m_pSetup->BeginGfx(true);
	m_pSetup->CreateDefaultFactories();

	GRCDEVICE.SetBlockOnLostFocus(false);

	grcEffect::SetDefaultPath((const char *)sDefaultShaderDirectory);

	grcTextureFactory::GetInstance().PreloadGlobalTextures((const char *)sDefaultGlobalTextureDirectory);

	grmShaderFactory::GetInstance().PreloadShaders((const char *)sDefaultShaderDirectory);

	// Initialize the off screen targets
	grcTextureFactory::CreateParams params;
	params.Multisample = 0;
	params.HasParent = false;
	params.Parent = NULL; 
	params.UseFloat = true;
	params.Lockable = true;
	//params.Format = grctfA16B16G16R16F;
	params.Format = RENDER_PROJECTION_RENDER_FORMAT;

	// Create the render targets for both the side planes and the top down
	for (int i=0; i<RAGE_LEVEL_LOD_RENDER_TECHNIQUE_COUNT; i++)
	{
		m_apSaveTargetRT[i]			= grcTextureFactory::GetInstance().CreateRenderTarget("rageLevelLOD RT", grcrtPermanent, s_RageLevelLODRenderDemensions[i][0], s_RageLevelLODRenderDemensions[i][1], 32, &params);
		m_apSaveDepthTargetRT[i]	= grcTextureFactory::GetInstance().CreateRenderTarget("rageLevelLOD RDT", grcrtDepthBuffer, s_RageLevelLODRenderDemensions[i][0], s_RageLevelLODRenderDemensions[i][1], 32, NULL);

		AssertVerify( SUCCEEDED( GRCDEVICE.GetCurrent()->CreateOffscreenPlainSurface(s_RageLevelLODRenderDemensions[i][0], s_RageLevelLODRenderDemensions[i][1], RENDER_PROJECTION_TEX_FORMAT, D3DPOOL_SYSTEMMEM, &m_apSaveTargetSurface[i],NULL) ) );
	}

	// Initialize the bucket manager
	rageShaderBucketManager::InitShaderBucketLibrary();
	m_pShaderBucketManager = new rageShaderBucketManager();

	// Initialize the light
	m_pLights = new grcLightGroup;
	m_pLights->SetActiveCount(1);
	m_pLights->SetAmbient(1.0f, 1.0f, 1.0f, 1.0f);
	m_pLights->SetLightType(0, grcLightGroup::LTTYPE_DIR);
	m_pLights->SetColor(0, 0.0f, 0.0f, 0.0f);

	// Initialize the viewport
	m_pViewport = new grcViewport;
	m_pViewport->SetWorldMtx(M34_IDENTITY);

	// Set the max texture size this saves a lot of memory
	grcImage::SetSizeRatio((1.0f / 32.0f));

	return true;
}

bool rageLevelLODMaya::Shutdown()
{
	m_szTexturePrefix = NULL;

	if (m_pOutputTextureStream)
	{
		m_pOutputTextureStream->Close();
		m_pOutputTextureStream = NULL;
	}

	m_amUniformSectorCompositeMatrices.Reset();
	m_apUniformSectors.Reset();
	m_ModelDataCache.Reset();

	delete m_pViewport;
	m_pViewport = NULL;

	delete m_pShaderBucketManager;
	m_pShaderBucketManager = NULL;

	for (int i=RAGE_LEVEL_LOD_RENDER_TECHNIQUE_COUNT-1; i>=0; i--)
	{
		if(m_apSaveTargetSurface[i])
		{
			m_apSaveTargetSurface[i]->Release();
			m_apSaveTargetSurface[i] = NULL;
		}

		if(m_apSaveDepthTargetRT[i])
		{
			m_apSaveDepthTargetRT[i]->Release();
			m_apSaveDepthTargetRT[i] = NULL;
		}

		if(m_apSaveTargetRT[i])
		{
			m_apSaveTargetRT[i]->Release();
			m_apSaveTargetRT[i] = NULL;
		}
	}

	if(grcTextureFactory::HasInstance())
	{
		grcTextureFactory::GetInstance().UnloadGlobalTextures();	
	}

	if (m_pSetup)
	{
		m_pSetup->DestroyFactories();
		m_pSetup->EndGfx();
		m_pSetup->Shutdown();

		delete m_pSetup;
		m_pSetup = NULL;
	}

	rageShaderMaterialTemplate::ShutdownShaderMaterialLibrary();

	if(::rage::parManagerSingleton::IsInstantiated())
	{
		SHUTDOWN_PARSER;
	}

	return true;
}

void rageLevelLODMaya::FillGaps(const int x, const int y, const int index, const int nWidth, const int nHeight, const float * pBits, float * pOutputImage)
{
	// Get the surrounding pixels
	int p0 = -1;
	int p1 = -1;
	int p2 = -1;
	int p3 = -1;
	int p4 = -1;
	int p5 = -1;
	int p6 = -1;
	int p7 = -1;
	if (y>0)
	{
		if (x>0)
			p0 = (((y-1) * nWidth) + (x-1)) * 4;

		p1 = (((y-1) * nWidth) + x) * 4;

		if (x<nWidth)
			p2 = (((y-1) * nWidth) + (x+1)) * 4;
	}

	if (x>0)
		p3 = ((y * nWidth) + (x-1)) * 4;

	if (x<nWidth-1)
		p4 = ((y * nWidth) + (x+1)) * 4;

	if (y<nHeight-1)
	{
		if (x>0)
			p5 = (((y+1) * nWidth) + (x-1)) * 4;

		p6 = (((y+1) * nWidth) + x) * 4;

		if (x<nWidth)
			p7 = (((y+1) * nWidth) + (x+1)) * 4;
	}

	float nColorR = 0.0f;
	float nColorG = 0.0f;
	float nColorB = 0.0f;
	float nColorCount = 0;
	if (p0 >= 0 && (pBits[p0+3]!=0.0f))
	{
		nColorR += pBits[p0+0];
		nColorG += pBits[p0+1];
		nColorB += pBits[p0+2];
		nColorCount++;
	}

	if (p1 >= 0 && (pBits[p1+3]!=0.0f))
	{
		nColorR += pBits[p1+0];
		nColorG += pBits[p1+1];
		nColorB += pBits[p1+2];
		nColorCount++;
	}

	if (p2 >= 0 && (pBits[p2+3]!=0.0f))
	{
		nColorR += pBits[p2+0];
		nColorG += pBits[p2+1];
		nColorB += pBits[p2+2];
		nColorCount++;
	}

	if (p3 >= 0 && (pBits[p3+3]!=0.0f))
	{
		nColorR += pBits[p3+0];
		nColorG += pBits[p3+1];
		nColorB += pBits[p3+2];
		nColorCount++;
	}

	if (p4 >= 0 && (pBits[p4+3]!=0.0f))
	{
		nColorR += pBits[p4+0];
		nColorG += pBits[p4+1];
		nColorB += pBits[p4+2];
		nColorCount++;
	}

	if (p5 >= 0 && (pBits[p5+3]!=0.0f))
	{
		nColorR += pBits[p5+0];
		nColorG += pBits[p5+1];
		nColorB += pBits[p5+2];
		nColorCount++;
	}

	if (p6 >= 0 && (pBits[p6+3]!=0.0f))
	{
		nColorR += pBits[p6+0];
		nColorG += pBits[p6+1];
		nColorB += pBits[p6+2];
		nColorCount++;
	}

	if (p7 >= 0 && (pBits[p7+3]!=0.0f))
	{
		nColorR += pBits[p7+0];
		nColorG += pBits[p7+1];
		nColorB += pBits[p7+2];
		nColorCount++;
	}

	if (nColorCount > 1)
	{
		nColorR /= nColorCount;
		nColorG /= nColorCount;
		nColorB /= nColorCount;

		pOutputImage[index+0] = (float)nColorR;
		pOutputImage[index+1] = (float)nColorG;
		pOutputImage[index+2] = (float)nColorB;
		pOutputImage[index+3] = 1.0f;
	}
}

bool rageLevelLODMaya::SaveDDS(const int nWidth, const int nHeight, const char * szSectorLODTextureFileName, LPDIRECT3DTEXTURE9 pOriginalTexture)
{

	HRESULT hr;
	LPD3DXBUFFER pMemBuffer=NULL;

	hr = D3DXSaveTextureToFileInMemory(&pMemBuffer, D3DXIFF_DDS, pOriginalTexture, NULL);
	if (pMemBuffer==NULL)
	{
		Errorf("D3DXSaveTextureToFileInMemory failed with return value '%x'", hr);
		return false;
	}

	LPDIRECT3DTEXTURE9 pNewTexture = NULL;
	hr = D3DXCreateTextureFromFileInMemoryEx(GRCDEVICE.GetCurrent(), 
		pMemBuffer->GetBufferPointer(),
		pMemBuffer->GetBufferSize(),
		nWidth,
		nHeight,
		1,
		0,
		D3DFMT_A32B32G32R32F,
		D3DPOOL_SYSTEMMEM,
		D3DX_DEFAULT,
		D3DX_FILTER_BOX | D3DX_FILTER_SRGB_IN | D3DX_FILTER_SRGB_OUT,
		0,
		NULL,
		NULL,
		&pNewTexture);

	pMemBuffer->Release();
	pMemBuffer=NULL;
	
	if ((pNewTexture==NULL) || (hr!=0))
	{
		Errorf("D3DXCreateTextureFromFileInMemoryEx failed with return value '%x'", hr);
		return false;
	}

	char acHighestResolutionSectorLODTextureFileName[255];
	strcpy(acHighestResolutionSectorLODTextureFileName, szSectorLODTextureFileName);
	strcpy(strrchr(acHighestResolutionSectorLODTextureFileName, '.'), "_High.dds");
	hr = D3DXSaveTextureToFile( acHighestResolutionSectorLODTextureFileName, D3DXIFF_DDS, pNewTexture, NULL);
	if(FAILED(hr))
	{
		Errorf("D3DXSaveTextureToFile failed with return value '%x'", hr);
	}

	pNewTexture->Release();
	pNewTexture = NULL;

	// Create a bat file to convert it to the lowres version
	char acBatFileToConvertTextureFileName[255];
	strcpy(acBatFileToConvertTextureFileName, szSectorLODTextureFileName);
	strcpy(strrchr(acBatFileToConvertTextureFileName, '.'), ".bat");
	FILE* obBat = fopen(acBatFileToConvertTextureFileName, "w");
	if(!obBat)
	{
		Errorf("Failed to create file %s", acBatFileToConvertTextureFileName);
		return false;
	}
	fprintf(obBat, "rageTextureConvert -srgbgamma -hdrc -autoresize -maxwidth %d -maxheight %d -in %s -out %s", (nWidth / RAGE_LEVEL_LOD_TEXTURE_SCALE_FACTOR), (nHeight / RAGE_LEVEL_LOD_TEXTURE_SCALE_FACTOR), acHighestResolutionSectorLODTextureFileName, szSectorLODTextureFileName);
	fclose(obBat);

	// Escape the filename
	MString strTexturesResult;
	MString strTextureGenerationBatFile = acBatFileToConvertTextureFileName;
	MString strTextureGenerationBatFileWithEscapedBackSlashes;
	for(unsigned u=0; u<strTextureGenerationBatFile.length(); u++)
	{
		if(strTextureGenerationBatFile.asChar()[u] == '\\')
		{
			strTextureGenerationBatFileWithEscapedBackSlashes += "\\";
		}
		strTextureGenerationBatFileWithEscapedBackSlashes += strTextureGenerationBatFile.substring(u, u);
	}

	// Execute the bat file to process textures
	Displayf("=====================================================================================");
	Displayf(strTextureGenerationBatFile.asChar());
	Displayf("-------------------------------------------------------------------------------------");
	MGlobal::executeCommand("system(\""+ strTextureGenerationBatFileWithEscapedBackSlashes +"\")", strTexturesResult);
	Displayf(strTexturesResult.asChar());
	Displayf("=====================================================================================");

	return (hr==0);
}

// This function loads the model data to be rendered
bool rageLevelLODMaya::LoadSectorModelData(rageLevel * pLevel, const int nSectorIndex, const char * szInputResourceFolder)
{
	const rageLevelSector * pSector = pLevel->GetSector(nSectorIndex);

	if (!pSector)
		return false;

	// Load the graphics objects
	if (!pSector->IsDataLoaded())
	{
		if (!pLevel->LoadSectorData(nSectorIndex, szInputResourceFolder))
		{
			Errorf("LoadSectorData failed for sector %s.", pSector->GetName());
			return false;
		}

		// Add it to the cache
		m_ModelDataCache.Add(nSectorIndex);
	}

	// Add it to the bucket manager
	AddSectorToBucketManager(pSector);

	// Return
	return true;
}

bool rageLevelLODMaya::FreeModelData(rageLevel * pLevel)
{
	while (m_ModelDataCache.GetModelDataCount() > RAGE_LEVEL_LOD_MODEL_DATA_CACHE_SIZE)
	{
		int nSectorIndex = m_ModelDataCache.GetTailSectorIndex();

		const rageLevelSector * pSector = pLevel->GetSector(nSectorIndex);

		if (pSector && pSector->IsDataLoaded())
		{
			// Unload the graphics objects
			pLevel->UnloadSectorData(nSectorIndex);
		}

		m_ModelDataCache.Remove(nSectorIndex);
	}

	// Return
	return true;	
}

void rageLevelLODMaya::AddSectorToBucketManager(const rageLevelSector * pSector)
{
	// Add this sector to the bucket manager
	for (int iModel=0; iModel<pSector->GetModelCount(); iModel++)
	{
		const grmModel * pModel = pSector->GetModel(iModel)->GetModel();
		if (pModel)
		{
			for (int iGeom=0; iGeom<pModel->GetGeometryCount(); iGeom++)
			{
				const grmShaderFx * pShader = (grmShaderFx *)&pSector->GetShaderGroup()->GetShader(pModel->GetShaderIndex(iGeom));
				m_pShaderBucketManager->UpdateData(pShader->GetDrawBucket(), (u32)pShader + (u32)pModel, pShader, pModel, &M34_IDENTITY);
			}
		}
	}
}

bool rageLevelLODMaya::InitViewport(const rageLevelSector * pSector, const int nPlane)
{
	// Setup the viewport to match the sector and plane
	const rageLevelBoundingBox & BoundingBox = pSector->GetBoundingBox();

	Vector4 vBoundingSphere;
	BoundingBox.GetBoundingSphere(vBoundingSphere);

	const Vector3 & vMin = BoundingBox.GetMinPosition();
	const Vector3 & vMax = BoundingBox.GetMaxPosition();

	float fX = 1.0f;
	float fY = 1.0f;
	if (nPlane == 0)
	{
		// Plane A
		m_vPlanes[0][0].Set(vMin.x, vMin.y, vMin.z);
		m_vPlanes[0][1].Set(vMin.x, vMax.y, vMin.z);
		m_vPlanes[0][2].Set(vMax.x, vMax.y, vMin.z);
		m_vPlanes[0][3].Set(vMax.x, vMin.y, vMin.z);

		fX = (m_vPlanes[0][2].x - m_vPlanes[0][0].x) / 2.0f;
		fY = (m_vPlanes[0][2].y - m_vPlanes[0][0].y) / 2.0f;
	}
	else if (nPlane == 1)
	{
		// Plane B
		m_vPlanes[1][0].Set(vMin.x, vMin.y, vMax.z);
		m_vPlanes[1][1].Set(vMin.x, vMax.y, vMax.z);
		m_vPlanes[1][2].Set(vMin.x, vMax.y, vMin.z);
		m_vPlanes[1][3].Set(vMin.x, vMin.y, vMin.z);

		fX = (m_vPlanes[1][0].z - m_vPlanes[1][2].z) / 2.0f;
		fY = (m_vPlanes[1][2].y - m_vPlanes[1][0].y) / 2.0f;
	}
	else if (nPlane == 2)
	{
		// Plane C
		m_vPlanes[2][0].Set(vMax.x, vMin.y, vMax.z);
		m_vPlanes[2][1].Set(vMax.x, vMax.y, vMax.z);
		m_vPlanes[2][2].Set(vMin.x, vMax.y, vMax.z);
		m_vPlanes[2][3].Set(vMin.x, vMin.y, vMax.z);

		fX = (m_vPlanes[2][0].x - m_vPlanes[2][2].x) / 2.0f;
		fY = (m_vPlanes[2][2].y - m_vPlanes[2][0].y) / 2.0f;
	}
	else if (nPlane == 3)
	{
		// Plane D
		m_vPlanes[3][0].Set(vMax.x, vMin.y, vMin.z);
		m_vPlanes[3][1].Set(vMax.x, vMax.y, vMin.z);
		m_vPlanes[3][2].Set(vMax.x, vMax.y, vMax.z);
		m_vPlanes[3][3].Set(vMax.x, vMin.y, vMax.z);

		fX = (m_vPlanes[3][2].z - m_vPlanes[3][0].z) / 2.0f;
		fY = (m_vPlanes[3][2].y - m_vPlanes[3][0].y) / 2.0f;
	}
	else if (nPlane == 4)
	{
		m_vPlanes[4][0].Set(vMin.x, 0.0f, vMin.y);
		m_vPlanes[4][1].Set(vMin.x, 0.0f, vMax.y);
		m_vPlanes[4][2].Set(vMax.x, 0.0f, vMax.y);
		m_vPlanes[4][3].Set(vMax.x, 0.0f, vMin.y);

		fX = (m_vPlanes[4][2].x - m_vPlanes[4][0].x) / 2.0f;
		fY = (m_vPlanes[4][2].z - m_vPlanes[4][0].z) / 2.0f;
	}
	
	// Get the direction of the camera
	Vector3 v1 = m_vPlanes[nPlane][1] - m_vPlanes[nPlane][0];
	Vector3 v2 = m_vPlanes[nPlane][2] - m_vPlanes[nPlane][0];
	v1.Cross(v2);
	v1.Normalize();

	m_vPlaneDirection[nPlane] = v1;

	// Set the world matrix for the camera
	Vector3 vTo;
	vBoundingSphere.GetVector3(vTo);
	Vector3 vFrom = vTo;
	vFrom += v1 * vBoundingSphere.w;

	Matrix34 m34;
	m34.Identity();
	m34.d.Set(vFrom);
	if (nPlane!=4)
		m34.LookAt(vTo, YAXIS);
	else
		m34.LookAt(vTo, ZAXIS);
	m_pViewport->SetCameraMtx(m34);

	m_pViewport->Ortho(-fX, fX, -fY, fY, 0.0f, vBoundingSphere.w * 2.0f);

	// Set the composite matrix
	m_mPlaneFullCompositeMatrix[nPlane] = m_pViewport->GetFullCompositeMtx();

	return true;
}

bool rageLevelLODMaya::RenderSideProjection(const void * pSector, float * pOutputImage)
{	
	// Side projection
	//	   1	         2
	//     \xxxxxxxxxxxxx\
	//		x			x
	//		x			x
	//	    x			x
	//		x			x
	//		x			x
	//	   \xxxxxxxxxxxxx\
	//	   0	         3

	bool bIsValid = false;

	// Draw the sector from this side
	for (int iPlane=0; iPlane<4; iPlane++)
	{
		// Start updating
		m_pSetup->BeginUpdate();

		// End Updating
		m_pSetup->EndUpdate();

		// Start rendering
		m_pSetup->BeginDraw();

		// Initialize the viewport
		InitViewport((const rageLevelSector *)pSector, iPlane);

		// Set the current viewport
		grcViewport::SetCurrent(m_pViewport);
		grcState::SetCullMode(grccmBack);

		grcViewport::SetCurrentWorldMtx(M34_IDENTITY);

		grcTextureFactory::GetInstance().LockRenderTarget(0, m_apSaveTargetRT[m_RenderTechnique], m_apSaveDepthTargetRT[m_RenderTechnique]);

		GRCDEVICE.Clear(true, Color32(0.375f,0.375f,0.375f,1.0f), true, 1.0f, false, 0);

		// Setup the lighting
		m_pLights->SetDirection(0, m_vPlaneDirection[iPlane]);
		grcState::SetLightingGroup(*m_pLights);
		grcLightState::SetEnabled(true);

		m_pShaderBucketManager->Draw();

		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		// Try to recover from shaderfx rendering
		grcState::Default();	
		

		m_pSetup->EndDraw();

		IDirect3DSurface9 *pRTSurface;
		static_cast<IDirect3DTexture9 *>(m_apSaveTargetRT[m_RenderTechnique]->GetTexturePtr())->GetSurfaceLevel(0, &pRTSurface);
		D3DLOCKED_RECT rect;
		GRCDEVICE.GetCurrent()->GetRenderTargetData(pRTSurface, m_apSaveTargetSurface[m_RenderTechnique]);
		m_apSaveTargetSurface[m_RenderTechnique]->LockRect(&rect, NULL,0);
		float * pBits = (float *)rect.pBits;


		// Remember to add gamma processing
		float fOneOverGamma = 1.0f / GAMMA_FOR_LOD_TEXTURES;

		// Copy pixel data
		for (int y=0; y<s_RageLevelLODRenderDemensions[m_RenderTechnique][1]; y++)
		{
			for (int x=0; x<s_RageLevelLODRenderDemensions[m_RenderTechnique][0]; x++)
			{
				int index0 = ((((s_RageLevelLODRenderDemensions[m_RenderTechnique][1]-1) - y) * s_RageLevelLODRenderDemensions[m_RenderTechnique][0]) + x) * 4;
				int index1 = 0;
				if (m_RenderTechnique==RAGE_LEVEL_LOD_RENDER_SIDE_SQUARE)
					index1 =   ((((((((3-iPlane)/2)+1)*s_RageLevelLODRenderDemensions[m_RenderTechnique][1])-1) - y) * RAGE_LEVEL_LOD_SIDE_TEXTURE_WIDTH) + (((iPlane%2))*s_RageLevelLODRenderDemensions[m_RenderTechnique][0]) + x) * 4;
				else if (m_RenderTechnique==RAGE_LEVEL_LOD_RENDER_SIDE_HORIZONTAL)
					index1 = ((((((iPlane+1)*s_RageLevelLODRenderDemensions[m_RenderTechnique][1])-1) - y) * RAGE_LEVEL_LOD_SIDE_TEXTURE_WIDTH) + x) * 4;
				else if (m_RenderTechnique==RAGE_LEVEL_LOD_RENDER_SIDE_VERTICAL)
					index1 = ((((s_RageLevelLODRenderDemensions[m_RenderTechnique][1]-1) - y) * RAGE_LEVEL_LOD_SIDE_TEXTURE_WIDTH) + (iPlane * s_RageLevelLODRenderDemensions[m_RenderTechnique][0]) + x) * 4;

				pOutputImage[index1+0] = pow( pBits[index1+0], fOneOverGamma );
				pOutputImage[index1+1] = pow( pBits[index1+1], fOneOverGamma );
				pOutputImage[index1+2] = pow( pBits[index1+2], fOneOverGamma );
				pOutputImage[index1+3] = (pBits[index0+3]==0.0f) ? 0.0f : 1.0f; // Make alpha either full or nothing

				if (pOutputImage[index1+3]==1.0f)
					bIsValid = true;
			}
		}

		m_apSaveTargetSurface[m_RenderTechnique]->UnlockRect();

		pRTSurface->Release();
		pRTSurface = NULL;
	}

	// Make sure we initalize the top down plane for this sector
	InitViewport((const rageLevelSector *)pSector, 4);

	return bIsValid;
}

bool rageLevelLODMaya::RenderTopDownProjection(const void * , float * pOutputImage)
{
	// Top down projection
	//			 C
	//     \xxxxxxxxxxxxx\
	//		x			x
	//		x			x
	//	 B  x			x  D
	//		x			x
	//		x			x
	//	   \xxxxxxxxxxxxx\
	//			 A



	u32 index = 0;
	bool bIsValid = false;

	try
	{

	
	// Start updating
	m_pSetup->BeginUpdate();

	// End Updating
	m_pSetup->EndUpdate();

	// Start rendering
	m_pSetup->BeginDraw();

	// Set the current viewport
	grcViewport::SetCurrent(m_pViewport);
	grcState::SetCullMode(grccmBack);

	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);

	grcTextureFactory::GetInstance().LockRenderTarget(0, m_apSaveTargetRT[RAGE_LEVEL_LOD_RENDER_TOP_DOWN], m_apSaveDepthTargetRT[RAGE_LEVEL_LOD_RENDER_TOP_DOWN]);

	GRCDEVICE.Clear(true, Color32(0.375f,0.375f,0.375f,1.0f), true, 1.0f, false, 0);

	// Setup the lighting
	m_pLights->SetDirection(0, m_vPlaneDirection[4]);
	grcState::SetLightingGroup(*m_pLights);
	grcLightState::SetEnabled(true);

	m_pShaderBucketManager->Draw();

	grcTextureFactory::GetInstance().UnlockRenderTarget(0);

	// Try to recover from shaderfx rendering
	grcState::Default();	
	

	m_pSetup->EndDraw();

	IDirect3DSurface9 *pRTSurface;

	IDirect3DTexture9 * tex = static_cast<IDirect3DTexture9 *>(m_apSaveTargetRT[RAGE_LEVEL_LOD_RENDER_TOP_DOWN]->GetTexturePtr());
	AssertVerify( SUCCEEDED( tex->GetSurfaceLevel(0, &pRTSurface) ));
	// TODO
	//grcTexelIterator<Vector4> itor( tex );

	//while ( itor.next())
	//{
	//	*itor.w = (pBits[index+3]==0.0f) ? 0.0f : 1.0f;
	//}


	D3DSURFACE_DESC	 surf;
	AssertVerify( SUCCEEDED( tex->GetLevelDesc(0, &surf) ));

	u32 w = s_RageLevelLODRenderDemensions[RAGE_LEVEL_LOD_RENDER_TOP_DOWN][0];;
	u32 h = s_RageLevelLODRenderDemensions[RAGE_LEVEL_LOD_RENDER_TOP_DOWN][1];

	Assert( surf.Width == w);
	Assert( surf.Height == h);
	Assert( surf.Format == RENDER_PROJECTION_TEX_FORMAT);

	D3DLOCKED_RECT rect;
	AssertVerify( SUCCEEDED(GRCDEVICE.GetCurrent()->GetRenderTargetData(pRTSurface, m_apSaveTargetSurface[RAGE_LEVEL_LOD_RENDER_TOP_DOWN]) ));
	AssertVerify( SUCCEEDED(m_apSaveTargetSurface[RAGE_LEVEL_LOD_RENDER_TOP_DOWN]->LockRect(&rect, NULL,0) ));
	float * pBits = (float *)rect.pBits;

	// Remember to add gamma processing
	float fOneOverGamma = 1.0f / GAMMA_FOR_LOD_TEXTURES;

	// Copy pixel data
	for (u32 y=0; y < h ; y++)
	{
		for (u32 x=0; x< w ; x++)
		{
			index = ((((h-1) - y) * w) + x) * 4;

			Assert( index >= 0 && index < (w * h * 4) );

			pOutputImage[index+0] = pow( pBits[index+0], fOneOverGamma );
			pOutputImage[index+1] = pow( pBits[index+1], fOneOverGamma );
			pOutputImage[index+2] = pow( pBits[index+2], fOneOverGamma );
			pOutputImage[index+3] = (pBits[index+3]==0.0f) ? 0.0f : 1.0f; // Make alpha either full or nothing

			if (pOutputImage[index+3]==1.0f)
			{
				bIsValid = true;
			}
		}
	}
		
		AssertVerify( SUCCEEDED( m_apSaveTargetSurface[RAGE_LEVEL_LOD_RENDER_TOP_DOWN]->UnlockRect() ));

		AssertVerify( SUCCEEDED(pRTSurface->Release() ) );
		pRTSurface = NULL;
	}
	catch(std::exception exp)
	{
		Displayf("%s %d %s index = %d", __FILE__, __LINE__, __FUNCTION__, index);
		throw(exp);
	}

	return bIsValid;
}

bool rageLevelLODMaya::RenderSectorToTexture(atFunctor2< bool, const void *, float * > & RenderFunction,
											 const int nWidth, 
											 const int nHeight, 
											 const char * szSectorLODTextureFileName,
											 const char * szSectorLODMaterialFileName,
											 const char * szTemplateFileName,
											 const void * pSector)
{
	

	bool bStatus = false;
	LPDIRECT3DTEXTURE9 pOutputTexture = NULL;

	// Initialize the output mtl
	rageShaderMaterial outputMtl;
	outputMtl.Init(szTemplateFileName);

	// Create the output image data
	D3DXCreateTexture(GRCDEVICE.GetCurrent(), nWidth, nHeight, 1, D3DUSAGE_DYNAMIC, D3DFMT_A32B32G32R32F, D3DPOOL_SYSTEMMEM , &pOutputTexture);
	if (!pOutputTexture)
	{
		Errorf("Could not create output image data");
		goto END;
	}

	D3DLOCKED_RECT lockedRectNew;
	if(!SUCCEEDED(pOutputTexture->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
	{
		goto END;
	}  

	float* pOutputImage = (float*)lockedRectNew.pBits;

	if (!RenderFunction(pSector, pOutputImage))
	{
		goto END;
	}

	float * pTempBuffer = new float[nWidth * nHeight * 4];
	memcpy(pTempBuffer, pOutputImage, nWidth * nHeight * 4);

	for (int i=0; i<5; i++)
	{
		for (int y=0; y<nHeight; y++)
		{
			for (int x=0; x<nWidth; x++)
			{
				int index = (y * nWidth + x) * 4;

				// Lets put in a gap
				if (pTempBuffer[index+3] == 0)
				{
					FillGaps(x, y, index, nWidth, nHeight, pTempBuffer, pOutputImage);
				}
			}
		}

		memcpy(pTempBuffer, pOutputImage, nWidth * nHeight * 4);
	}

	delete [] pTempBuffer;
	pTempBuffer = NULL;

	// Clean up the DirectX
	pOutputTexture->UnlockRect(0);
	pOutputImage = NULL;

	// Save out the dds file
	if (!SaveDDS(nWidth, nHeight, szSectorLODTextureFileName, pOutputTexture))
	{
		Errorf("Could not save out dds file %s", szSectorLODTextureFileName);
		goto END;
	}

	// Save out the mtl file
	for (int i=0; i<outputMtl.GetParamCount(); i++)
	{
		shaderMaterialGeoParamValue * pParam = outputMtl.GetParam(i);
		if (pParam->GetType() == grcEffect::VT_TEXTURE)
		{
			((shaderMaterialGeoParamValueTexture *)pParam)->SetData(szSectorLODTextureFileName);
			break;
		}
	}

	// Save to the material out to file
	if (!outputMtl.SaveTo(szSectorLODMaterialFileName))
	{
		Errorf("Could not save out mtl file %s", szSectorLODMaterialFileName);
		goto END;
	}

	bStatus = true;

END:

	// Release the image
	if (pOutputTexture)
	{
		pOutputTexture->Release();
		pOutputTexture = NULL;
	}

	return bStatus;
}

void rageLevelLODMaya::GetBestUvs(const Vector3 * vInputPositions, const Vector3 & vPolyNormal, Vector2 * vOutputUvs, const void * & pOutputSector, const bool bTopDownOnly) const
{
	// Pick the best plane to use for this poly
	int nPlane = 0;

	if (!bTopDownOnly)
	{
		float fMinAngle = fabs(vPolyNormal.FastAngle(m_vPlaneDirection[0]));
		for (int iPlane=1; iPlane<5; iPlane++)
		{
			float fAngle = fabs(vPolyNormal.FastAngle(m_vPlaneDirection[iPlane]));
			if (fAngle < fMinAngle)
			{
				nPlane = iPlane;
				fMinAngle = fAngle;
			}
		}
	}
	else
	{
		nPlane = 4;
	}

	// Check to see if we want to use the top down projection
	if (nPlane == 4)
	{
		for (int i=0; i<m_amUniformSectorCompositeMatrices.GetCount(); i++)
		{
			pOutputSector = m_apUniformSectors[i];

			for (int iVertex=0; iVertex<3; iVertex++)
			{
				// This is in screen space
				Vector3 vPlaneUv = m_amUniformSectorCompositeMatrices[i].FullTransform(vInputPositions[iVertex]);
				vOutputUvs[iVertex].x = (vPlaneUv.x / RAGE_LEVEL_LOD_SCREEN_WIDTH);
				vOutputUvs[iVertex].y = 1.0f - (vPlaneUv.y / RAGE_LEVEL_LOD_SCREEN_HEIGHT);

				if (vOutputUvs[iVertex].x < -0.001f || vOutputUvs[iVertex].x > 1.001f ||
					vOutputUvs[iVertex].y < -0.001f || vOutputUvs[iVertex].y > 1.001f)
				{
					pOutputSector = NULL;
					break;
				}
			}

			if (pOutputSector != NULL)
			{
				// We filled the array in and found the one we want so finish
				return;
			}			
		}

		// THIS means we need to handle the case where not all the vertices fit in a single mshMaterial
		Errorf("Could not find this uv texture coordinates anywhere");
		return;
	}

	// Transform all positions into screen space
	for (int iVertex=0; iVertex<3; iVertex++)
	{
		// This is in screen space
		Vector3 vPlaneUv = m_mPlaneFullCompositeMatrix[nPlane].FullTransform(vInputPositions[iVertex]);

		// Convert to normalized space
		if (m_RenderTechnique == RAGE_LEVEL_LOD_RENDER_SIDE_SQUARE)
		{
			vOutputUvs[iVertex].x = ((       (vPlaneUv.x / RAGE_LEVEL_LOD_SCREEN_WIDTH )) * 0.5f) + (0.5f * (nPlane%2));
			vOutputUvs[iVertex].y = ((1.0f - (vPlaneUv.y / RAGE_LEVEL_LOD_SCREEN_HEIGHT)) * 0.5f) + (0.5f * (nPlane/2));
		}
		else if (m_RenderTechnique == RAGE_LEVEL_LOD_RENDER_SIDE_HORIZONTAL)
		{
			vOutputUvs[iVertex].x = (vPlaneUv.x / RAGE_LEVEL_LOD_SCREEN_WIDTH);
			vOutputUvs[iVertex].y = ((1.0f - (vPlaneUv.y / RAGE_LEVEL_LOD_SCREEN_HEIGHT)) * 0.25f) + (0.25f * (3-nPlane));
		}
		else if (m_RenderTechnique == RAGE_LEVEL_LOD_RENDER_SIDE_VERTICAL)
		{
			vOutputUvs[iVertex].x = ((vPlaneUv.x / RAGE_LEVEL_LOD_SCREEN_WIDTH) * 0.25f) + (0.25f * nPlane);
			vOutputUvs[iVertex].y = (1.0f - (vPlaneUv.y / RAGE_LEVEL_LOD_SCREEN_HEIGHT));
		}
	}
}

void rageLevelLODMaya::SetGroundPlane(const char * szGroundPlaneMeshName)
{
	MObject groundMeshObject;
	GetMeshObject(szGroundPlaneMeshName, groundMeshObject);
	MFnMesh groundMesh(groundMeshObject);

	if (groundMesh.numPolygons() != 1)
		Errorf("Invalid number of polygons for ground plane", groundMesh.numPolygons());

	MIntArray groundVertexList;
	groundMesh.getPolygonVertices(0,groundVertexList);

	// Get the first vertex position
	Vector3 avGroundPlanePositions[3];
	for (int iVertex=0; iVertex<3; iVertex++)
	{
		MPoint mayaPos;
		groundMesh.getPoint(groundVertexList[iVertex], mayaPos, MSpace::kWorld);
		avGroundPlanePositions[iVertex].Set((float)mayaPos.x, (float)mayaPos.y, (float)mayaPos.z);
	}

	// Compute the ground plane
	m_vGroundPlane.ComputePlane(avGroundPlanePositions[0], avGroundPlanePositions[1], avGroundPlanePositions[2]);

	// Set the flag so we don't do it again
	m_bCalcGroundPlane = false;
}

void rageLevelLODMaya::SetGroundPlane(const Vector3 * pvGroundPlanePositions)
{
	// Get the first vertex position
	Vector4 vPlane0;
	Vector4 vPlane1;
	Vector4 vPlane2;

	vPlane0.ComputePlane(pvGroundPlanePositions[0], pvGroundPlanePositions[1], pvGroundPlanePositions[2]);
	vPlane1.ComputePlane(pvGroundPlanePositions[1], pvGroundPlanePositions[2], pvGroundPlanePositions[3]);
	vPlane2.ComputePlane(pvGroundPlanePositions[2], pvGroundPlanePositions[3], pvGroundPlanePositions[0]);

	Vector3 vNormal0(vPlane0.x, vPlane0.y, vPlane0.z);
	Vector3 vNormal1(vPlane1.x, vPlane1.y, vPlane1.z);
	Vector3 vNormal2(vPlane2.x, vPlane2.y, vPlane2.z);
	Vector3 vAverageNormal;
	vAverageNormal.Average(vNormal0, vNormal1);
	vAverageNormal.Average(vNormal2);

	// Set the ground plane to be the average normal and the absolute greatest distance from any plane
	m_vGroundPlane.Set(vAverageNormal.x, vAverageNormal.y, vAverageNormal.z, (vPlane0.w + vPlane1.w + vPlane2.w)/3.0f);

	// Set the flag so we don't do it again
	m_bCalcGroundPlane = false;
}

rageLevelLODMaya::ReturnStatus rageLevelLODMaya::CombineSectorToMayaMesh(rageLevel * pLevel, const int nSectorIndex, MString & outputMeshName, const char * szInputLODFolderName, const bool bOnlyCustomLODs, const bool bSkipMayaMeshCreate)
{
	const rageLevelSector * pSector = pLevel->GetSector(nSectorIndex);

	if (!pSector)
	{
		return rageLevelLODMaya::Failure;
	}

	// First we need to generate the Maya mesh node
	MFloatPointArray vertexArray;
	MIntArray polygonCounts;
	MIntArray polygonConnects;
	MIntArray polygonShader;
	int vertexIndex = 0;

	// Initialize the ground plane flag
	m_bCalcGroundPlane = true;

	// First lets look for a custom made model
	bool bUseCustomMesh = false;
	char szInputMayaFile[256];

	Displayf("Attempting to load custom LOD Maya file : %s%s_LOD", szInputLODFolderName, pSector->GetName());

	//Try to load the .mb version of the file
	formatf(szInputMayaFile, sizeof(szInputMayaFile), "%s%s_LOD.mb", szInputLODFolderName, pSector->GetName());
	MStatus status = MFileIO::open(szInputMayaFile, NULL, true, MFileIO::kLoadNoReferences);
	if(status != MStatus::kSuccess)
	{
		//Fallback to trying to load the .ma version of the file
		formatf(szInputMayaFile, sizeof(szInputMayaFile), "%s%s_LOD.ma", szInputLODFolderName, pSector->GetName());
		status = MFileIO::open(szInputMayaFile, NULL,true, MFileIO::kLoadNoReferences);
	}

	if (status == MStatus::kSuccess)
	{
		Displayf("Loaded custom LOD Maya file");

		// Get the textures exported
		ExportArtistProvidedTexturesFromLowLodMayaFile(pLevel);

		// Get on with the rest of it
		MStatus status;
		MItDag dagIterator( MItDag::kBreadthFirst, MFn::kInvalid, &status);
		for ( ; !dagIterator.isDone(); dagIterator.next() )
		{
			MDagPath dagPath;
			status = dagIterator.getPath(dagPath);

			if (!status) 
			{
				break;
			}

			// skip over intermediate objects
			MFnDagNode dagNode( dagPath, &status );
			if ( dagNode.isIntermediateObject() ||
				 (dagNode.name() == "original_mesh") || 
				 (dagNode.name() == "ground_mesh") )
			{
				continue;
			}
			
			// skip over intermediate objects
			MFnDagNode dagGroundNode( dagPath, &status );
			if (dagGroundNode.name() == "ground_plane") 
			{
				SetGroundPlane(dagGroundNode.name().asChar());
				continue;
			}

			MObject dagNodeObject = dagNode.object();
			if (!bUseCustomMesh && dagNodeObject.hasFn(MFn::kMesh))
			{
				outputMeshName = dagNode.name();
				bUseCustomMesh = true;
				continue;
			}
		}

		// Cleanup the geometry (Triangulate)
		if(outputMeshName != "")
		{
			char szCommand[256];
			formatf(szCommand, sizeof(szCommand), "delete -all -constructionHistory;\npolyMergeVertex  -d 0.2 -ch 0 -tx 0 %s;\npolyTriangulate -ch 0 %s;", outputMeshName.asChar(), outputMeshName.asChar());
			MGlobal::executeCommand(szCommand, true);
		}
		else
		{
			// No output mesh found, so there isn't any low lod model for this sector
			Displayf("No mesh found for sector %s in Maya file %s", pSector->GetName(), szInputMayaFile);
		}
	}
	else
	{
		//There wasn't a custom LOD file available, so create a new one...
		MFileIO::newFile(true);
	}

	// Set the path to the models
	ConstString sAssetPath = ASSET.GetPath();
	ASSET.SetPath(pLevel->GetInputModelFolder());
	ASSET.PushFolder("temp");
	ASSET.PushFolder(pSector->GetName());

	if (!bSkipMayaMeshCreate)
	{
		for (int i=0; i<pSector->GetModelCount(); i++)
		{
			const rageLevelModel * pModel = pSector->GetModel(i);

			for (int j=0; j<pModel->GetMaterialCount(); j++)
			{
				mshMesh inputMesh;
				const rageLevelMaterial * pMaterial = pModel->GetMaterial(j);
				if (!SerializeFromFile(pMaterial->GetMeshFileName(), inputMesh, "mesh"))
				{
					Errorf("Could not load mesh file %s", pMaterial->GetMeshFileName());
					continue;
				}

				// Make sure we are in triangles
				inputMesh.CleanModel();

				for (int iMtl=0; iMtl < inputMesh.GetMtlCount(); iMtl++)
				{
					mshMaterial & inputMtl = inputMesh.GetMtl(iMtl);
					for (int iPrim=0; iPrim<inputMtl.Prim.GetCount(); iPrim++)
					{
						mshPrimitive & inputPrim = inputMtl.Prim[iPrim];

						if (inputPrim.Type != mshTRIANGLES)
						{
							Errorf("Unsupported primitive type");
							continue;
						}
						
						for (int iIndex=0; iIndex<inputPrim.Idx.GetCount()-2; iIndex+=3)
						{
							// Get each of the verts for the triangle
							mshVertex & inputVert0 = inputMtl.Verts[inputPrim.Idx[iIndex+0]];
							mshVertex & inputVert1 = inputMtl.Verts[inputPrim.Idx[iIndex+1]];
							mshVertex & inputVert2 = inputMtl.Verts[inputPrim.Idx[iIndex+2]];

							// Lets set the world positions
							vertexArray.append(inputVert0.Pos.x, inputVert0.Pos.y, inputVert0.Pos.z);
							vertexArray.append(inputVert1.Pos.x, inputVert1.Pos.y, inputVert1.Pos.z);
							vertexArray.append(inputVert2.Pos.x, inputVert2.Pos.y, inputVert2.Pos.z);

							Vector3 vInputPositions[3];
							vInputPositions[0] = inputVert0.Pos;
							vInputPositions[1] = inputVert1.Pos;
							vInputPositions[2] = inputVert2.Pos;

							polygonConnects.append(vertexIndex++);
							polygonConnects.append(vertexIndex++);
							polygonConnects.append(vertexIndex++);

							polygonCounts.append(3);
						}
					}
				}
			}
		}

		// Finally create the Maya mesh
		MFnMesh outputMesh;
		outputMesh.create(vertexArray.length(), 
						  polygonCounts.length(), 
						  vertexArray, 
						  polygonCounts,
						  polygonConnects,
						  MObject::kNullObj,
						  &status);
		if (status!=MStatus::kSuccess)
		{
			Errorf("Could not create the Maya mesh object.");

			// Set the path back before we return
			ASSET.PopFolder();
			ASSET.PopFolder();
			ASSET.SetPath((const char *)sAssetPath);

			return rageLevelLODMaya::Failure;
		}

		// Store the name
		outputMesh.setName("original_mesh");

		// Cleanup the geometry (Merge all the vertices that are within 0.2 distance)
		char szCommand[512];
		formatf(szCommand, sizeof(szCommand), "polyMergeVertex  -d 0.2 -ch 0 -tx 0 %s;", outputMesh.name().asChar());
		MGlobal::executeCommand(szCommand, true);

		// Save out the reference maya scene
		char szFileName[128];
		formatf(szFileName, sizeof(szFileName), "%s_LOD", pSector->GetName());
		ASSET.FullPath(szInputMayaFile, sizeof(szInputMayaFile), szFileName, "mb");
		Displayf("Saving sector %s as %s", pSector->GetName(), szInputMayaFile);
		MFileIO::saveAs(szInputMayaFile, NULL, true);
		Displayf("Saved sector");
	}

	// Check to see if we are using a custom mesh.
	if (!bUseCustomMesh)
	{
		// Make sure we want to use a pregenerated mesh other wise return here after we generate the one the artist need to use to build with
		if (bOnlyCustomLODs)
		{
			// Set the path back before we return
			ASSET.PopFolder();
			ASSET.PopFolder();
			ASSET.SetPath((const char *)sAssetPath);

			// We return false so we don't continue the process
			return rageLevelLODMaya::Success_But_Do_Not_Process_Further;
		}

		// Next step is to bring this mesh into Maya and remove some of its polys
		if (!OptimizeSectorMesh("original_mesh", outputMeshName))
		{
			Errorf("OptimizeSectorMesh failed.");

			// Set the path back before we return
			ASSET.PopFolder();
			ASSET.PopFolder();
			ASSET.SetPath((const char *)sAssetPath);

			return rageLevelLODMaya::Failure;
		}
	}

	// Set the path back
	ASSET.PopFolder();
	ASSET.PopFolder();
	ASSET.SetPath((const char *)sAssetPath);
	
	return rageLevelLODMaya::Success;
}

void rageLevelLODMaya::GetMeshObject(const char * szInputMeshNodeName, MObject & inputMeshObject) const
{
	MStatus status;
	MItDag dagIterator( MItDag::kBreadthFirst, MFn::kInvalid, &status);
	for ( ; !dagIterator.isDone(); dagIterator.next() )
	{
		MDagPath dagPath;
		status = dagIterator.getPath(dagPath);

		if (!status) 
		{
			return;
		}

		// skip over intermediate objects
		MFnDagNode dagNode( dagPath, &status );
		if (dagNode.isIntermediateObject()) 
		{
			continue;
		}

		if (stricmp(dagNode.name().asChar(), szInputMeshNodeName) == 0)
		{
			inputMeshObject = dagNode.object();
			break;
		}
	}
}

bool rageLevelLODMaya::MergeEdges(const char * szInputMeshNodeName) const
{
	// Get this mesh object
	MObject inputMeshObject;
	GetMeshObject(szInputMeshNodeName, inputMeshObject);
	MFnDagNode inputMeshDagNode(inputMeshObject);
	MDagPath inputMeshPath;
	inputMeshDagNode.getPath(inputMeshPath);

	// Modify all the vertices
	bool bMergedVertices = false;

	MGlobal::clearSelectionList();
	for(MItMeshEdge inputEdge(inputMeshObject);	!inputEdge.isDone(); inputEdge.next() )
	{
		double dLength = 0.0;
		inputEdge.getLength(dLength);
		if (dLength < 0.75)
		{
			MObject inputEdgeObject = inputEdge.edge();
			MGlobal::select(inputMeshPath, inputEdgeObject);
			bMergedVertices = true;
		}
	}

	// Run the mel scripts to merge the vertices
	if (bMergedVertices)
	{
		char szCommand[512];
		formatf(szCommand, sizeof(szCommand), "polyCollapseEdge  -ch 0;");
		MGlobal::executeCommand(szCommand);

		formatf(szCommand, sizeof(szCommand), "polyMergeVertex  -d 0.2 -ch 0 -tx 0 %s;", szInputMeshNodeName);
		MGlobal::executeCommand(szCommand);
	}

	return bMergedVertices;
}

bool rageLevelLODMaya::CloseEdges(const char * szInputMeshNodeName) const
{

	// Initialize the Maya object
	MObject inputMeshObject;
	GetMeshObject(szInputMeshNodeName, inputMeshObject);
	MFnMesh inputMesh(inputMeshObject);

	MDagPath inputMeshPath;
	inputMesh.getPath(inputMeshPath);

	// For every face in the mesh
	int nEdgeCount = 0;
	MGlobal::clearSelectionList();
	for(MItMeshEdge inputEdge(inputMeshObject);	!inputEdge.isDone(); inputEdge.next() )
	{
		MIntArray outputPolyList;
		inputEdge.getConnectedFaces(outputPolyList);

		// Get the normal
		atArray <Vector3> polyNormals;
		polyNormals.Resize(outputPolyList.length());

		for (int i=0; i<polyNormals.GetCount(); i++)
		{
			MIntArray vertexList;
			inputMesh.getPolygonVertices(outputPolyList[i], vertexList);

			if (vertexList.length() != 3)
			{
				Errorf("Only triangles are supported when passed into DeleteEdges");
				continue;
			}

			MPoint mayaPoint;
			Vector3 inputMeshVertices[3];
			for (int j=0; j<3; j++)
			{	
				inputMesh.getPoint(vertexList[j], mayaPoint, MSpace::kWorld);
				inputMeshVertices[j].Set((float)mayaPoint.x, (float)mayaPoint.y, (float)mayaPoint.z);
			}

			Vector3 v0 = inputMeshVertices[0] - inputMeshVertices[1];
			Vector3 v1  = inputMeshVertices[2] - inputMeshVertices[1];
			v0.Cross(v1);
			polyNormals[i].Normalize(v0);
		}

		bool bMaintainEdge = false;
		for (int i=0; i<polyNormals.GetCount(); i++)
		{
			for (int j=i+1; j<polyNormals.GetCount(); j++)
			{
				if (fabs(polyNormals[i].FastAngle(polyNormals[j])) > 0.02f)
				{
					bMaintainEdge = true;
				}
			}
		}

		if (!bMaintainEdge)
		{
			MObject edgeObject = inputEdge.edge();
			MGlobal::select(inputMeshPath, edgeObject);
			nEdgeCount++;
		}
	}

	if (nEdgeCount>0)
	{
		// Cleanup the geometry (Merge all the vertices that are within 0.01 distance)
		char szCommand[512];
		formatf(szCommand, sizeof(szCommand), "polyCloseBorder -ch 0;");
		MGlobal::executeCommand(szCommand);
	}

	return nEdgeCount>0;
}

bool rageLevelLODMaya::DeleteEdges(const char * szInputMeshNodeName) const
{
	
	// Initialize the Maya object
	MObject inputMeshObject;
	GetMeshObject(szInputMeshNodeName, inputMeshObject);
	MFnMesh inputMesh(inputMeshObject);

	MDagPath inputMeshPath;
	inputMesh.getPath(inputMeshPath);

	// For every face in the mesh
	int nEdgeCount = 0;
	MGlobal::clearSelectionList();
	for(MItMeshEdge inputEdge(inputMeshObject);	!inputEdge.isDone(); inputEdge.next() )
	{
		if (!inputEdge.onBoundary())
		{
			MIntArray outputPolyList;
			inputEdge.getConnectedFaces(outputPolyList);

			// Get the normal
			atArray <Vector3> polyNormals;
			polyNormals.Resize(outputPolyList.length());

			for (int i=0; i<polyNormals.GetCount(); i++)
			{
				MIntArray vertexList;
				inputMesh.getPolygonVertices(outputPolyList[i], vertexList);

				if (vertexList.length() != 3)
				{
					Errorf("Only triangles are supported when passed into DeleteEdges");
					continue;
				}

				MPoint mayaPoint;
				Vector3 inputMeshVertices[3];
				for (int j=0; j<3; j++)
				{	
					inputMesh.getPoint(vertexList[j], mayaPoint, MSpace::kWorld);
					inputMeshVertices[j].Set((float)mayaPoint.x, (float)mayaPoint.y, (float)mayaPoint.z);
				}

				Vector3 v0 = inputMeshVertices[0] - inputMeshVertices[1];
				Vector3 v1  = inputMeshVertices[2] - inputMeshVertices[1];
				v0.Cross(v1);
				polyNormals[i].Normalize(v0);
			}

			bool bMaintainEdge = false;
			for (int i=0; i<polyNormals.GetCount(); i++)
			{
				for (int j=i+1; j<polyNormals.GetCount(); j++)
				{
					if (fabs(polyNormals[i].FastAngle(polyNormals[j])) > 0.02f)
					{
						bMaintainEdge = true;
					}
				}
			}

			if (!bMaintainEdge)
			{
				MObject edgeObject = inputEdge.edge();
				MGlobal::select(inputMeshPath, edgeObject);
				nEdgeCount++;
			}
		}
	}

	if (nEdgeCount>0)
	{
		// Cleanup the geometry (Merge all the vertices that are within 0.01 distance)
		char szCommand[512];
		formatf(szCommand, sizeof(szCommand), "polyDelEdge -cv 1 -ch 0;");
		MGlobal::executeCommand(szCommand);

		// Clean up using the clean function in Maya
		formatf(szCommand, sizeof(szCommand), "select %s;\npolyCleanupArgList 3 { \"0\",\"1\",\"0\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1e-005\",\"0\",\"1e-005\",\"0\",\"1\",\"1\" };", szInputMeshNodeName);
		MGlobal::executeCommand(szCommand);
		

		// Cleanup the geometry (Triangulate)
		formatf(szCommand, sizeof(szCommand), "polyTriangulate -ch 0 %s;", szInputMeshNodeName);
		MGlobal::executeCommand(szCommand);
	}

	return nEdgeCount>0;
}

bool rageLevelLODMaya::DeleteFaces(const char * szInputMeshNodeName) const
{	
	// Initialize the Maya object
	MObject inputMeshObject;
	GetMeshObject(szInputMeshNodeName, inputMeshObject);
	MFnMesh inputMesh(inputMeshObject);

	MDagPath inputMeshPath;
	inputMesh.getPath(inputMeshPath);

	// For every face in the mesh
	int nPolygonCount = 0;
	MGlobal::clearSelectionList();
	for(MItMeshPolygon inputPoly(inputMeshObject);	!inputPoly.isDone(); inputPoly.next() )
	{
		MIntArray vertexList;
		inputMesh.getPolygonVertices(inputPoly.index(), vertexList);

		if (vertexList.length() != 3)
		{
			Errorf("Triangles are only supported for DeleteFaces()");
			continue;
		}

		Vector3 vertexPositions0[3];
		for (int iVertex=0; iVertex<3; iVertex++)
		{
			MPoint point;
			inputMesh.getPoint(vertexList[iVertex], point, MSpace::kWorld);
			vertexPositions0[iVertex].Set((float)point.x, (float)point.y, (float)point.z);
		}

		Vector3 vPolyNormal0 = vertexPositions0[1] - vertexPositions0[0];
		Vector3 v0  = vertexPositions0[2] - vertexPositions0[0];
		vPolyNormal0.Cross(v0);
		vPolyNormal0.Normalize();

		float fX = fabs(vPolyNormal0.x);
		float fY = fabs(vPolyNormal0.y);
		float fZ = fabs(vPolyNormal0.z);
		int nPlane0 = (fX >= fY && fX >=fZ) ? 0 : ((fY >= fX && fY >= fZ) ? 1 : ((fZ >= fX && fZ >= fY) ? 2 : -1));

		if (nPlane0 < 0)
			continue;

		bool isContained[3];
		isContained[0] = isContained[1] = isContained[2] = 0;

		for(MItMeshPolygon otherPoly(inputMeshObject);	!otherPoly.isDone(); otherPoly.next() )
		{
			if (otherPoly.index()!=inputPoly.index())
			{
				inputMesh.getPolygonVertices(otherPoly.index(), vertexList);

				if (vertexList.length() != 3)
				{
					Errorf("Triangles are only supported for DeleteFaces()");
					continue;
				}

				Vector3 vertexPositions1[3];
				for (int iVertex=0; iVertex<3; iVertex++)
				{
					MPoint point;
					inputMesh.getPoint(vertexList[iVertex], point, MSpace::kWorld);
					vertexPositions1[iVertex].Set((float)point.x, (float)point.y, (float)point.z);
				}

				Vector3 vPolyNormal1 = vertexPositions1[1] - vertexPositions1[0];
				Vector3 v0  = vertexPositions1[2] - vertexPositions1[0];
				vPolyNormal1.Cross(v0);
				vPolyNormal1.Normalize();

				float fX = fabs(vPolyNormal1.x);
				float fY = fabs(vPolyNormal1.y);
				float fZ = fabs(vPolyNormal1.z);
				int nPlane1 = (fX >= fY && fX >=fZ) ? 0 : ((fY >= fX && fY >= fZ) ? 1 : ((fZ >= fX && fZ >= fY) ? 2 : -1));

				if (nPlane0 != nPlane1)
					continue;
				
				float tri1A = 0.0f;
				float tri1B = 0.0f;
				float tri2A = 0.0f;
				float tri2B = 0.0f;
				float tri3A = 0.0f;
				float tri3B = 0.0f;

				switch (nPlane0)
				{
					case 0:
					{	
						tri1A = vertexPositions1[0].z;
						tri1B = vertexPositions1[0].y;
						tri2A = vertexPositions1[1].z;
						tri2B = vertexPositions1[1].y;
						tri3A = vertexPositions1[2].z;
						tri3B = vertexPositions1[2].y;
						break;
					}

					case 1:
					{
						tri1A = vertexPositions1[0].x;
						tri1B = vertexPositions1[0].z;
						tri2A = vertexPositions1[1].x;
						tri2B = vertexPositions1[1].z;
						tri3A = vertexPositions1[2].x;
						tri3B = vertexPositions1[2].z;
						break;
					}

					case 2:
					{
						tri1A = vertexPositions1[0].x;
						tri1B = vertexPositions1[0].y;
						tri2A = vertexPositions1[1].x;
						tri2B = vertexPositions1[1].y;
						tri3A = vertexPositions1[2].x;
						tri3B = vertexPositions1[2].y;
						break;
					}
				}

				float pointA = 0.0f;
				float pointB = 0.0f;

				for (int iVertex=0; iVertex<3; iVertex++)
				{
					switch (nPlane0)
					{
						case 0:
						{
							pointA = vertexPositions0[iVertex].z;
							pointB = vertexPositions0[iVertex].y;
							break;
						}
						case 1:
						{
							pointA = vertexPositions0[iVertex].x;
							pointB = vertexPositions0[iVertex].z;
							break;
						}
						case 2:
						{
							pointA = vertexPositions0[iVertex].x;
							pointB = vertexPositions0[iVertex].y;
							break;
						}
					}

					if (geom2D::Test2DPointVsTri(pointA, pointB, tri1A, tri1B, tri2A, tri2B, tri3A, tri3B))
					{
						isContained[iVertex] = true;
					}
				}

				// All points are contained lets break out
				if (isContained[0] && isContained[1] && isContained[2])
				{
					MObject polyObject = inputPoly.polygon();
					MGlobal::select(inputMeshPath, polyObject);
					nPolygonCount++;
					break;
				}
			}
		}
	}

	if (nPolygonCount>0)
	{
		// Cleanup the geometry (Delete all unnecessary faces)
		char szCommand[512];
		formatf(szCommand, sizeof(szCommand), "polyDelFacet -ch 0;");
		MGlobal::executeCommand(szCommand);
	}

	return nPolygonCount>0;
}

bool rageLevelLODMaya::OptimizeSectorMesh(const char * szInputMeshNodeName, MString & outputMeshName)
{
	MergeEdges(szInputMeshNodeName);

	for (int i=0; i<5; i++)
	{
		CloseEdges(szInputMeshNodeName);
		DeleteEdges(szInputMeshNodeName);
	}

	//DeleteFaces(szInputMeshNodeName); (Needs to be optimized)

	MObject inputMeshObject;
	GetMeshObject(szInputMeshNodeName, inputMeshObject);
	MFnMesh inputMesh(inputMeshObject);

	// Set the new name
	outputMeshName = "optimized_mesh";
	inputMesh.setName(outputMeshName);

	return true;
}

bool rageLevelLODMaya::ExportSectorMesh(rageLevel * pLevel, const int nSectorIndex, const char * szInputMeshNodeName)
{
	const rageLevelSector * pSector = pLevel->GetSector(nSectorIndex);

	if (!pSector)
		return false;

	// Create the LOD data object
	rageSectorLODData LODData;

	// Set the face normals to be soft
	char szCommand[512];
	formatf(szCommand, sizeof(szCommand), "select %s; polySetToFaceNormal;", szInputMeshNodeName);
	MGlobal::executeCommand(szCommand);
	
	formatf(szCommand, sizeof(szCommand), "select %s; polySoftEdge -a 30 -ws 1 -ch 0;", szInputMeshNodeName);
	MGlobal::executeCommand(szCommand);

	// Initialize the Maya object
	MObject inputMeshObject;
	GetMeshObject(szInputMeshNodeName, inputMeshObject);
	MFnMesh inputMesh(inputMeshObject);

	const Vector3 & vExtentsMin = pSector->GetBoundingBox().GetMinPosition();
	const Vector3 & vExtentsMax = pSector->GetBoundingBox().GetMaxPosition();
	
	Vector4 vBoundingSphere;
	pSector->GetBoundingSphereFlat(vBoundingSphere);

	Vector2 vCenter(vBoundingSphere.x, vBoundingSphere.z);
	float fRadius2 = vBoundingSphere.w * vBoundingSphere.w;

	Vector3 avTestGroundPlanePositions[4];
	Vector3 avGroundPlanePositions[4];
	avTestGroundPlanePositions[0].Set(FLT_MAX, FLT_MAX, FLT_MAX);
	avTestGroundPlanePositions[1].Set(FLT_MAX, FLT_MAX, -FLT_MAX);
	avTestGroundPlanePositions[2].Set(-FLT_MAX, FLT_MAX, -FLT_MAX);
	avTestGroundPlanePositions[3].Set(-FLT_MAX, FLT_MAX, FLT_MAX);
	avGroundPlanePositions[0].Set(vExtentsMin.x, FLT_MAX, vExtentsMin.z);
	avGroundPlanePositions[1].Set(vExtentsMin.x, FLT_MAX, vExtentsMax.z);
	avGroundPlanePositions[2].Set(vExtentsMax.x, FLT_MAX, vExtentsMax.z);
	avGroundPlanePositions[3].Set(vExtentsMax.x, FLT_MAX, vExtentsMin.z);

	// Allocate the outputMesh
	mshMesh outputDemeshMesh;

	// Create the default sector material
	mshMaterial & defaultDemeshMeshMaterial = outputDemeshMesh.NewMtl();
	defaultDemeshMeshMaterial.Name = "#0";

	// Get a list of all the shaders that are connected to the mesh.  At this point only custom shaders 
	// that should be used to override the basic sector material should be assigned to parts of the mesh.
	MObjectArray	arrConnectedShaders;
	MIntArray		arrFaceToShaderIndicies;
	inputMesh.getConnectedShaders(0, arrConnectedShaders, arrFaceToShaderIndicies);

	// Create materials in the demesh for any custom shaders that have been applied to the LOD model.
	// While doing this, build a lookup table of the MTL filenames that we can use later when we need to
	// add references to those MTLs in the LOD shadergroup file.
	atArray< atString > customMtlLookupTable;
	customMtlLookupTable.PushAndGrow("#0");

	char tmpNameBuf[8];

	unsigned int nMayaMtls = arrConnectedShaders.length();
	for(unsigned int i=0; i<nMayaMtls; i++)
	{
		//Add a place holder material to the demeshMesh
		mshMaterial &newDemeshMtl = outputDemeshMesh.NewMtl();
		sprintf(tmpNameBuf, "#%d", i+1);
		newDemeshMtl.Name = tmpNameBuf;

		//Attempt to retrieve the rage shader material filename, if the custom material assigned to the geometry
		//isn't a rageShaderMaterial (or something goes wrong in the process of trying to gather this name)
		//then the material filename should be kept at "none", so we can check against this later.
		atString rageMtlFileName("none");

		MStatus status;
		MFnSet shaderFnSet(arrConnectedShaders[i], &status);
		if(status != MS::kSuccess)
		{
			Errorf("An error occured while collecting shaders for the mesh in sector '%s'", pSector->GetName());	
		}
		else
		{
			MPlug surfaceShader = shaderFnSet.findPlug("surfaceShader",&status);

			MPlugArray plugs;
			surfaceShader.connectedTo(plugs, true, false, &status);
			if( (status == MS::kSuccess) && 
				(plugs.length() == 1) )
			{
				MObject obj = plugs[0].node();
				MFnDependencyNode matDN(obj, &status);

				//Get the MTL Filename associated with the shader
				MPlug mtlFileNamePlug = matDN.findPlug("MtlFilename");
				if(!mtlFileNamePlug.isNull())
				{
					MString mtlFileName = "none";
					mtlFileNamePlug.getValue(mtlFileName);
					rageMtlFileName = mtlFileName.asChar();
				}
				else
				{
					//A material other than a RSM was applied to this mesh (likely its lambert1), so the faces using this material need to be assigned
					//the default sector material.  This can be accomplished by updating the face to shader index array assigning -1 to those faces 
					//which is the same as marking them as having no material (which in turn causes those faces to be assigned the sector material)
					unsigned int numIndicies = arrFaceToShaderIndicies.length();
					for(unsigned int j=0; j<numIndicies; j++)
					{
						if( arrFaceToShaderIndicies[j] == (int)i)
							arrFaceToShaderIndicies[j] = -1;
					}
				}
			}
			else
			{
				Errorf("An error occured while collecting shaders for the mesh in sector '%s'", pSector->GetName());
			}
		}

		customMtlLookupTable.PushAndGrow( rageMtlFileName );
	}

	// Create the demesh to be used to split the polys
	for (int iPoly=0; iPoly<inputMesh.numPolygons(); iPoly++) 
	{
		//If there isn't a material assigned to the poly (shader index will be -1), then the material for that 
		//poly will be the default sector material (material index 0 in the mesh).
		int polyMtlIdx = arrFaceToShaderIndicies[iPoly];
		mshMaterial &demeshMaterial = ( polyMtlIdx == -1) ? outputDemeshMesh.GetMtl(0) : outputDemeshMesh.GetMtl(polyMtlIdx+1);

		MIntArray outputVertexList;
		inputMesh.getPolygonVertices(iPoly,outputVertexList);

		if (outputVertexList.length() != 3)
		{
			Errorf("Unsupported polygon type.  Triangles only supported.");
			continue;
		}

		Vector3						vInputPositions[3];
		Vector3						vInputNormals[3];
		atArray< atArray<Vector2> > vInputUVs;
		
		MPoint		mayaPos;
		MVector		mayaNormal;
		float		mayaUVal, mayaVVal;

		// Get the first vertex position
		for (int iVertex=0; iVertex<3; iVertex++)
		{
			inputMesh.getPoint(outputVertexList[iVertex], mayaPos, MSpace::kWorld);
			vInputPositions[iVertex].Set((float)mayaPos.x, (float)mayaPos.y, (float)mayaPos.z);

			inputMesh.getFaceVertexNormal(iPoly, outputVertexList[iVertex], mayaNormal, MSpace::kWorld);
			vInputNormals[iVertex].Set((float)mayaNormal.x, (float)mayaNormal.y, (float)mayaNormal.z);

			atArray<Vector2>& vertexUVs = vInputUVs.Grow();
			inputMesh.getPolygonUV(iPoly, iVertex, mayaUVal, mayaVVal);
			vertexUVs.Grow().Set( mayaUVal, mayaVVal );

			//Look for a 2nd UV set named "window" (special case )
			MString windowUVSetName("window");
			MStatus status = inputMesh.getPolygonUV(iPoly, iVertex, mayaUVal, mayaVVal, &windowUVSetName);
			if(status == MS::kSuccess)
			{
				vertexUVs.Grow().Set( mayaUVal, mayaVVal );
			}

			if (m_bCalcGroundPlane)
			{
				if (vInputPositions[iVertex].y < avGroundPlanePositions[0].y)
				{
					if (avTestGroundPlanePositions[0].FlatDist2(avGroundPlanePositions[0]) >= vInputPositions[iVertex].FlatDist2(avGroundPlanePositions[0]))
					{
						avGroundPlanePositions[0].y		= vInputPositions[iVertex].y;
						avTestGroundPlanePositions[0]	= vInputPositions[iVertex];
					}
				}

				if (vInputPositions[iVertex].y < avGroundPlanePositions[1].y)
				{
					if (avTestGroundPlanePositions[1].FlatDist2(avGroundPlanePositions[1]) >= vInputPositions[iVertex].FlatDist2(avGroundPlanePositions[1]))
					{
						avGroundPlanePositions[1].y		= vInputPositions[iVertex].y;
						avTestGroundPlanePositions[1]	= vInputPositions[iVertex];
					}
				}

				if (vInputPositions[iVertex].y < avGroundPlanePositions[2].y)
				{
					if (avTestGroundPlanePositions[2].FlatDist2(avGroundPlanePositions[2]) >= vInputPositions[iVertex].FlatDist2(avGroundPlanePositions[2]))
					{
						avGroundPlanePositions[2].y		= vInputPositions[iVertex].y;
						avTestGroundPlanePositions[2]	= vInputPositions[iVertex];
					}
				}

				if (vInputPositions[iVertex].y < avGroundPlanePositions[3].y)
				{
					if (avTestGroundPlanePositions[3].FlatDist2(avGroundPlanePositions[3]) >= vInputPositions[iVertex].FlatDist2(avGroundPlanePositions[3]))
					{
						avGroundPlanePositions[3].y		= vInputPositions[iVertex].y;
						avTestGroundPlanePositions[3]	= vInputPositions[iVertex];
					}
				}
			}
		}

		mshBinding bind;
		bind.Reset();

		mshPrimitive & outputPrimitive = demeshMaterial.Prim.Append();
		outputPrimitive.Type = mshPOLYGON;
		outputPrimitive.Priority = demeshMaterial.Priority;

		for (int iVertex=0; iVertex<3; iVertex++)
		{
			mshIndex vertexIndex = demeshMaterial.AddVertex(vInputPositions[iVertex], bind, vInputNormals[iVertex], 
															Vector4(1,1,1,1), Vector4(1,1,1,1), Vector4(1,1,1,1), 
															vInputUVs[iVertex].GetCount(), &vInputUVs[iVertex][0]);
			outputPrimitive.Idx.Append() = vertexIndex;
		}
	}

	//This will weld, triangulate, and remove degenerate polys from the mesh...
	outputDemeshMesh.CleanModel();

	// Create the demesh and cut it by the surrounding sector planes
	deMesh * outputDemesh = deMesh::CreateFromMesh(&outputDemeshMesh);

	for (int iSector=0; iSector<m_apUniformSectors.GetCount(); iSector++)
	{
		float fOtherRadius2 = m_apUniformSectors[iSector]->GetRadius() * m_apUniformSectors[iSector]->GetRadius();
		if (m_apUniformSectors[iSector]->GetCenter().Dist2(vCenter) < fRadius2 + fOtherRadius2)
		{
			Vector4 avPlanes[4];
			m_apUniformSectors[iSector]->GetSectorPlanes(avPlanes);

			for (int i=0; i<4; i++)
			{
				outputDemesh->CutWithPlane(avPlanes[i]);
			}
		}
	}

	// Convert back to the mshMesh class
	outputDemeshMesh.Reset();
	outputDemesh->GetMshMesh(&outputDemeshMesh);

	// Clean up the demesh object since it's no longer needed...
	delete outputDemesh;
	outputDemesh = NULL;

	// Init the output rage mesh
	mshMesh outputRageMesh;

	// Add the geometry to the output mesh that is using the sector materials...
	atMap < const void*, int > sectorToMtlIndexMap;
	
	mshMaterial& inputMeshMaterial = outputDemeshMesh.GetMtl(0);

	int nInputMeshPrimCount = inputMeshMaterial.Prim.GetCount();
	for(int primIdx=0; primIdx < nInputMeshPrimCount; primIdx++)
	{
		mshPrimitive& inputPrim = inputMeshMaterial.Prim[primIdx];

		//Step through each triangle in the prim and 
		int nVertexCount = inputPrim.Idx.GetCount();
		for(int vIndex=0; vIndex < nVertexCount; vIndex += 3)
		{
			//Get the vertex positions and normals for this triangle
			Vector3 vInputPositions[3];
			Vector3 vInputNormals[3];

			vInputPositions[0] = inputMeshMaterial.GetVertex( inputPrim.Idx[ vIndex + 0 ] ).Pos;
			vInputNormals[0] = inputMeshMaterial.GetVertex( inputPrim.Idx[ vIndex + 0] ).Nrm;

			vInputPositions[1] = inputMeshMaterial.GetVertex( inputPrim.Idx[ vIndex + 1 ] ).Pos;
			vInputNormals[1] = inputMeshMaterial.GetVertex( inputPrim.Idx[ vIndex + 1] ).Nrm;

			vInputPositions[2] = inputMeshMaterial.GetVertex( inputPrim.Idx[ vIndex + 2 ] ).Pos;
			vInputNormals[2] = inputMeshMaterial.GetVertex( inputPrim.Idx[ vIndex + 2] ).Nrm;

			//Calculate the face normal for this triangle
			Vector3 vPolyNormal = vInputPositions[1] - vInputPositions[0];
			Vector3 v0  = vInputPositions[2] - vInputPositions[0];
			vPolyNormal.Cross(v0);
			vPolyNormal.Normalize();

			//Get the UVs for the poly.  Note that pOutput may end up pointing to a different sector material if the 
			//poly should be using a top-down projection
			Vector2 uvs[3];
			const void* pOutput = pSector;
			GetBestUvs(&vInputPositions[0], vPolyNormal, &uvs[0], pOutput, pSector->IsUniformSector());

			mshBinding bind;
			bind.Reset();

			if (!pOutput)
				pOutput = pSector;
			
			int nMtlIndex = -1;

			//Find the sector material
			int* lookupIdx = sectorToMtlIndexMap.Access( pOutput );
			if(lookupIdx)
				nMtlIndex = *lookupIdx;

			if(nMtlIndex < 0)
			{
				//There isn't a sector material already setup, so we'll need to add an entry into the shadergroup file for the current 
				//sector material
				char szSectorLODTextureFileName[512];
				char szSectorLODMaterialFileName[512];
				if (pOutput == pSector)
				{
					GetSectorLODTextureFileName(pLevel, pSector->GetName(), szSectorLODTextureFileName, sizeof(szSectorLODTextureFileName));
					GetSectorLODMaterialFileName(pLevel, pSector->GetName(), szSectorLODMaterialFileName, sizeof(szSectorLODMaterialFileName));
				}
				else
				{
					const rageSector * pOutputSector = (const rageSector *)pOutput;
					GetSectorLODTextureFileName(pLevel, pOutputSector->GetName(), szSectorLODTextureFileName, sizeof(szSectorLODTextureFileName));
					GetSectorLODMaterialFileName(pLevel, pOutputSector->GetName(), szSectorLODMaterialFileName, sizeof(szSectorLODMaterialFileName));
				}

				char szMeshMtlIndexName[32];
				int nShaderIndex = m_RageMaterialGroup.GetMaterialIndexByName(szSectorLODMaterialFileName);
				if (nShaderIndex < 0)
				{
					Errorf("Invalid shader index could not find %s", szSectorLODMaterialFileName);
					continue;
				}

				LODData.AddData(szSectorLODTextureFileName, szSectorLODMaterialFileName);

				nMtlIndex = outputRageMesh.GetMtlCount();
				mshMaterial & outputMaterial = outputRageMesh.NewMtl();

				rageShaderMaterialGroup::SetMaterialIndex(szMeshMtlIndexName, nShaderIndex);
				outputMaterial.Name = szMeshMtlIndexName;

				sectorToMtlIndexMap.Insert( pOutput, nMtlIndex );
			}

			//Add a new primitive to the mesh and assign the vertices...
			mshPrimitive & outputPrimitive = outputRageMesh.GetMtl(nMtlIndex).Prim.Append();
			outputPrimitive.Type = mshPOLYGON;
			outputPrimitive.Priority = outputRageMesh.GetMtl(nMtlIndex).Priority;

			for (int iVertex=0; iVertex<3; iVertex++)
			{
				mshIndex vertexIndex = outputRageMesh.GetMtl(nMtlIndex).AddVertex(vInputPositions[iVertex], bind, vInputNormals[iVertex], m_vColor0, m_vColor1, m_vColor2, 1, &uvs[iVertex]);
				outputPrimitive.Idx.Append() = vertexIndex;
			}

		}//End for(int vIndex=0;...
	}//End for(int primIdx=0;...

	//Now add any geometry that is using a custom shaders (which will be materials in the mesh after the 0 index material which
	//got the sector materials applied in the code above..
	for(int mtlIdx=1; mtlIdx < outputDemeshMesh.GetMtlCount(); mtlIdx++)
	{
		inputMeshMaterial = outputDemeshMesh.GetMtl(mtlIdx);

		atString& rageShaderMaterialFileName = customMtlLookupTable[mtlIdx];

		if(strcmp((const char*)rageShaderMaterialFileName, "none") == 0)
		{
			//Something went wrong earlier while collecting the materials, which generated the necessary error message
			//so we can just skip over that geometry here..
			continue;
		}

		//Get the index of the material in the shader group, and if there isn't an entry, then
		//create one
		int nShaderIndex = m_RageMaterialGroup.GetMaterialIndexByName((const char*)rageShaderMaterialFileName);
		if(nShaderIndex < 0)
		{
			//Add the material to the LOD shader group
			nShaderIndex = m_RageMaterialGroup.AddMaterial((const char*)rageShaderMaterialFileName);
			if(nShaderIndex < 0)
			{
				Errorf("Failed to load material '%s', during LOD Mesh export, geometry using that material will not be present in the exported mesh.",
					(const char*)rageShaderMaterialFileName);
				continue;
			}

			//Load up the material and add the necessary texture and material references to the LOD Data, and texture list files
			char szCustomLODTextureFileName[MAX_PATH];
			const rageShaderMaterial& rsm = m_RageMaterialGroup.GetMaterial(nShaderIndex);
			int nRsmParams = rsm.GetShaderParamCount();
			for(int i=0; i<nRsmParams; i++)
			{
				shaderMaterialGeoParamValue *pParm = rsm.GetShaderParam(i);
				if(pParm->GetType() == grcEffect::VT_TEXTURE)
				{
					shaderMaterialGeoParamValueTexture *pTextureParam = static_cast<shaderMaterialGeoParamValueTexture*>(pParm);
					const char* pSourceTexturePath = pTextureParam->GetData();
					if(pSourceTexturePath && strlen(pSourceTexturePath))
					{
						//Build up a path to where the exported DDS file for the this texture will live in the asset folder for the city
						// i.e. T:/mc4/assets/city/la/lod/customAll/SomeTexture.dds
						const char* fileName = ASSET.FileName(pSourceTexturePath);
						char baseName[MAX_PATH];
						ASSET.BaseName(baseName, MAX_PATH, fileName);
						formatf(szCustomLODTextureFileName, MAX_PATH,"%sCustomAll/%s.dds", pLevel->GetOutputLODFolder(szCustomLODTextureFileName, MAX_PATH), baseName);
						
						//Add a reference to the texture to the LOD Data 
						LODData.AddTextureFileName(szCustomLODTextureFileName);

						//Add a reference to the texture to the texture list file so it will be included in the LOD texture dictionary when
						//resources are generated
						fprintf(m_pOutputTextureStream, "%s\n", szCustomLODTextureFileName);
					}
				}
			}

			//Add a reference to the material to the LOD data
			LODData.AddMaterialFileName((const char*)rageShaderMaterialFileName);
		}

		//Add the new material to the output mesh and set its name to the proper index into the shadergroup
		char szMeshMtlIndexName[32];
		rageShaderMaterialGroup::SetMaterialIndex(szMeshMtlIndexName, nShaderIndex);

		mshMaterial& outputMaterial = outputRageMesh.NewMtl();
		outputMaterial.Name = szMeshMtlIndexName;
		
		//Copy all the primitives from the input mesh to the output mesh
		nInputMeshPrimCount = inputMeshMaterial.Prim.GetCount();
		for(int primIdx=0; primIdx < nInputMeshPrimCount; primIdx++)
		{
			mshPrimitive& inputPrim = inputMeshMaterial.Prim[primIdx];
			
			int nVertexCount = inputPrim.Idx.GetCount();
			for(int vIndex=0; vIndex < nVertexCount; vIndex += 3)
			{
				mshPrimitive & outputPrim = outputMaterial.Prim.Append();
				outputPrim.Type = mshPOLYGON;
				outputPrim.Priority = outputMaterial.Priority;

				for(int i=0; i < 3; i++)
				{
					mshIndex vertexIndex = outputMaterial.AddVertex( inputMeshMaterial.Verts[ inputPrim.Idx[ vIndex + i ] ] );
					outputPrim.Idx.Append() = vertexIndex;
				}
			}
		}
	}//End for(int mtlIdx=1...

	// Initialize the ground plane
	if (m_bCalcGroundPlane)
	{
		SetGroundPlane(avGroundPlanePositions);
	}
	LODData.SetGroundPlane(m_vGroundPlane);

	//Try to inject any ground-height data into the mesh
	InjectVertexToGroundHeight(outputRageMesh, pSector);

	outputRageMesh.CleanModel();
	outputRageMesh.ReorderVertices();

	char szSectorLODMeshFileName[256];
	if (!SerializeToBinaryFile(GetSectorLODMeshFileName(pLevel, pSector->GetName(), szSectorLODMeshFileName, sizeof(szSectorLODMeshFileName)), outputRageMesh))
	{
		Errorf("Could not save out mesh file %s", szSectorLODMeshFileName);
		return false;
	}

	LODData.SetMeshFileName(szSectorLODMeshFileName);

	char szSectorLODDataFileName[256];
	LODData.Save(pSector->GetSectorLODDataFileName(szSectorLODDataFileName, sizeof(szSectorLODDataFileName)));
/*
	// Save out a copy of the scene as a Maya file
	char szSectorDestPathAndFilename[256];
	strcpy(szSectorDestPathAndFilename, szSectorLODMeshFileName);
	strcat(szSectorDestPathAndFilename, ".mb");
	MFileIO::saveAs(szSectorDestPathAndFilename, "mayaBinary", true);

	strcpy(szSectorDestPathAndFilename, szSectorLODMeshFileName);
	strcat(szSectorDestPathAndFilename, ".ma");
	MFileIO::saveAs(szSectorDestPathAndFilename, "mayaAscii", true);

	strcpy(szSectorDestPathAndFilename, szSectorLODMeshFileName);
	strcat(szSectorDestPathAndFilename, ".dae");
	MFileIO::saveAs(szSectorDestPathAndFilename, "COLLADA exporter", true);
*/
	return true;
}

bool rageLevelLODMaya::InjectVertexToGroundHeight(mshMesh& outputMesh, const rageLevelSector* pSector)
{
	MStatus status;

	//Look for a mesh named "ground_mesh" in the scene...
	MSelectionList selList;
	if(selList.add("ground_mesh") == MS::kSuccess)
	{
		bool bMissedHitTest = false;

		//There is a ground mesh in the scene, so for each vertex in the outputmesh calculate its 
		//height above the ground plane.
		MDagPath gmDagPath;
		status = selList.getDagPath(0, gmDagPath);

		MFnMesh gmFnMesh(gmDagPath, &status);
		
		MMeshIsectAccelParams accParams = gmFnMesh.autoUniformGridParams();
		MFloatVector rayDirection(0.0f, -1.0f, 0.0f);

		int nMtls = outputMesh.GetMtlCount();
		for(int mtlIdx=0; mtlIdx<nMtls; mtlIdx++)
		{
			mshMaterial &mtl = outputMesh.GetMtl(mtlIdx);

			int nVerts = mtl.GetVertexCount();
			for(int vIdx=0; vIdx<nVerts; vIdx++)
			{
				mshVertex &vtx = mtl.Verts[vIdx];
				Vector3 vtxPos = vtx.Pos;

				//Cast a ray from the vertex down the -Y axis and test for an intersection
				//with the ground mesh
				MFloatPoint raySource(vtxPos.x, vtxPos.y, vtxPos.z);

				float distToGround;
				MFloatPoint hitPoint;
				bool bRes = gmFnMesh.closestIntersection( raySource,
														  rayDirection,
														  NULL, NULL, 
														  false,
														  MSpace::kWorld,
														  10000.0f,
														  false,
														  &accParams,
														  hitPoint,
														  NULL, NULL, NULL, NULL, NULL,
														  0.001f,
														  &status);
				if(bRes)
				{
					//There was an intersection with the ground mesh
					Vector3 vc3HitPoint(hitPoint.x, hitPoint.y, hitPoint.z);
					distToGround = vtxPos.Dist(vc3HitPoint);
				}
				else
				{
					//There was no interesection with the ground mesh, so use the distance
					//from the ground plane instead to keep from writing junk data into the mesh
					distToGround = m_vGroundPlane.DistanceToPlane( vtxPos );
					bMissedHitTest = true;
				}

				float intPart; 
				float fracPart = modf( distToGround, &intPart );
				vtx.Cpv.x = min(intPart, 255.0f) / 255.0f;
				vtx.Cpv.y = fracPart;
			}
		}

		if(bMissedHitTest)
		{
			Warningf("Failed to calculate the distance above the ground mesh for one or more vertices in the sector '%s', the height above the calculated ground plane was used for these vertices.", pSector->GetName());
		}

		gmFnMesh.freeCachedIntersectionAccelerator();
	}
	else
	{
		//There is no ground mesh in the scene, so to make sure that junk data doesn't get read in at runtime
		//set the vertex heights to their height above the calculated ground plane.
		int nMtls = outputMesh.GetMtlCount();
		for(int mtlIdx=0; mtlIdx<nMtls; mtlIdx++)
		{
			mshMaterial &mtl = outputMesh.GetMtl(mtlIdx);

			int nVerts = mtl.GetVertexCount();
			for(int vIdx=0; vIdx<nVerts; vIdx++)
			{
				mshVertex &vtx = mtl.Verts[vIdx];
				
				float distToGround = m_vGroundPlane.DistanceToPlane( vtx.Pos );
				
				float intPart; 
				float fracPart = modf( distToGround, &intPart );
				vtx.Cpv.x = min(intPart, 255.0f) / 255.0f;
				vtx.Cpv.y = fracPart;
			}
		}
	}
	
	return true;
}

void rageLevelLODMaya::ExportArtistProvidedTexturesFromLowLodMayaFile(const rageLevel * pLevel)
{
	// Generate textures
	MStatus obStatus;
	MString strTempPath;
	MString strTemp("TEMP");
	MCommonSystemUtils::getEnv(strTemp, strTempPath);
	MString strTextureGenerationBatFile = strTempPath +"\\rageGenerateTextures.bat";
	MString strTextureGenerationBatFileWithEscapedBackSlashes;
	for(unsigned u=0; u<strTextureGenerationBatFile.length(); u++)
	{
		if(strTextureGenerationBatFile.asChar()[u] == '\\')
		{
			strTextureGenerationBatFileWithEscapedBackSlashes += "\\";
		}
		strTextureGenerationBatFileWithEscapedBackSlashes += strTextureGenerationBatFile.substring(u, u);
	}

	// Get all the MTLs referenced in this scene
	MItDependencyNodes dgIterator;
	for ( ; !dgIterator.isDone(); dgIterator.next() )
	{
		// Get MFnNode
		MFnDependencyNode obFnNode(dgIterator.thisNode());

		if(obFnNode.typeName() == "rageShaderMaya")
		{
			// Get the mtl attribute
			MPlug obMtlPlug = obFnNode.findPlug("MtlFilename");

			if(!obMtlPlug.isNull())
			{
				// Get the mtl filename
				MString strMtlPathAndFilename;
				obMtlPlug.getValue(strMtlPathAndFilename);

				// Generate a bat file to process textures
				MString strCmd = "rageLaunchC#AppInThisFolder.bat rageMtlsToTextures.exe -mtlfile \\\""+ strMtlPathAndFilename +"\\\" -batchout \\\""+ strTextureGenerationBatFileWithEscapedBackSlashes +"\\\" -texout \\\""+ MString(pLevel->GetOutputFolder()) +"LOD/CustomAll/\\\"";
				Displayf("=====================================================================================");
				Displayf(strCmd.asChar());
				Displayf("-------------------------------------------------------------------------------------");
				MString strRageMtlsToTexturesResult;
				MGlobal::executeCommand("system(\""+ strCmd +"\")", strRageMtlsToTexturesResult);
				Displayf(strRageMtlsToTexturesResult.asChar());
				Displayf("=====================================================================================");

				// Execute the bat file to process textures
				Displayf("=====================================================================================");
				Displayf(strTextureGenerationBatFile.asChar());
				Displayf("-------------------------------------------------------------------------------------");
				MGlobal::executeCommand("system(\""+ strTextureGenerationBatFileWithEscapedBackSlashes +"\")", strRageMtlsToTexturesResult);
				Displayf(strRageMtlsToTexturesResult.asChar());
				Displayf("=====================================================================================");
			}
		}
	}
}


