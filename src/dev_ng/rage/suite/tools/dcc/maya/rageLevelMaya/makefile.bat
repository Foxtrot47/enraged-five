call ..\..\..\..\rage\base\tools\dcc\libs\rex\setmayapath.bat
set XDEFINE=NT_PLUGIN

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\tools %RAGE_DIR%\suite\src %RAGE_DIR%\suite\tools

set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% rageLevel init rageShaderBucket spatialdata demesh rageUsefulCPlusPlusToolClasses

set TESTER_BASE=maya_plugin
set RESOURCES=rageLevelSectorMaya.cpp rageLevelSectorMaya.h
set TESTERS=rageLevelMaya
