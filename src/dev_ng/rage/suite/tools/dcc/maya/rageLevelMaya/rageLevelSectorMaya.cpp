// 
// rageLevelMaya/rageLevelSectorMaya.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//

#pragma warning(disable: 4668)
#include <maya/MItDag.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#pragma warning(error: 4668)

#include "rageLevelSectorMaya.h"

using namespace rage;

//////////////////////////////////////////////////////////////

MStatus rageLevelSectorMaya::doIt( const MArgList& args ) 
{
	if (args.length() < 6)
	{
		return MS::kFailure;
	}

	bool bGenerateUniformSectors = false;
	for (unsigned i=0; i<args.length(); i++)
	{
		if(args.asString(i) == "-generateUniformSectors")
		{
			bGenerateUniformSectors = true;
			break;
		}
	}

	MStatus status;

	MItDag dagIterator( MItDag::kBreadthFirst, MFn::kInvalid, &status);

	if ( MS::kSuccess != status) 
	{
		return MS::kFailure;
	}

	for ( ; !dagIterator.isDone(); dagIterator.next() )
	{
		MDagPath dagPath;
		status = dagIterator.getPath(dagPath);

		if (!status) 
		{
			return MS::kFailure;
		}

		// skip over intermediate objects
		//
		MFnDagNode dagNode( dagPath, &status );
		if (dagNode.isIntermediateObject()) 
		{
			continue;
		}

		// Now output the mesh information
		const char* dagName = dagNode.name().asChar();

		if (stricmp(dagName, "sectors") == 0)
		{
			status = dagTraverseSectors(dagPath);
			if (status == MS::kFailure)
				return MS::kFailure;
		}
	}


	// Generate the uniformed sectors
	if(bGenerateUniformSectors || (m_SectorData.GetSectorCount() == 0))
	{
		Vector3 vMin;
		Vector3 vMax;
		int nSectorSize;

		vMin.x = (float)args.asDouble(1);
		vMin.y = 0.0f;
		vMin.z = (float)args.asDouble(2);

		vMax.x = (float)args.asDouble(3);
		vMax.y = 0.0f;
		vMax.z = (float)args.asDouble(4);

		nSectorSize = args.asInt(5);

		m_SectorData.GenerateUniformSectors(vMin, vMax, nSectorSize);
	}

	// Write out the data
	MString outputFile = args.asString(0);
	m_SectorData.Save(outputFile.asChar());

	return MS::kSuccess;
}

MStatus rageLevelSectorMaya::dagTraverseSectors(const MDagPath & dagpath) 
{

	MStatus status;

	// Find the mesh
	for (unsigned iChild=0; iChild<dagpath.childCount(); iChild++) {

		MObject dagobj = dagpath.child(iChild);
		MFnDagNode childnode(dagobj);

		MDagPath dagpathChild;
		childnode.getPath(dagpathChild);

		char szSectorName[128];
		sprintf(szSectorName, "%s", childnode.name().asChar());

		status = dagTraverseSector(dagpathChild, szSectorName);
		if (status == MS::kFailure)
			return MS::kFailure;

	}

	return MS::kSuccess;

}

MStatus rageLevelSectorMaya::dagTraverseSector(const MDagPath & dagpath, const char * szSectorName) 
{

	MStatus status = MS::kSuccess;

	if (dagpath.hasFn(MFn::kMesh)) {

		newSector(dagpath, szSectorName);

		return status;
	}

	// Find the mesh
	for (unsigned iChild=0; iChild<dagpath.childCount(); iChild++) {

		MObject dagobj = dagpath.child(iChild);
		MFnDagNode childnode(dagobj);

		MDagPath dagpathChild;
		childnode.getPath(dagpathChild);

		dagTraverseSector(dagpathChild, szSectorName);
	}

	return MS::kSuccess;

}

int rageLevelSectorMaya::newSector(const MDagPath & dagpath, const char * szSectorName)
{
	MStatus status;
	MFnMesh mesh(dagpath, &status);
	if (status != MStatus::kSuccess)
		return -1;

	rageSector & Sector = m_SectorData.AddSector();

	Sector.SetName(szSectorName);

	MFloatPointArray vtxArray;
	mesh.getPoints(vtxArray, MSpace::kWorld);

	atBucket <Vector2> aVertices;
	aVertices.Resize(vtxArray.length());
	for (int v=0; v<aVertices.GetCount(); v++) {
		aVertices[v].x = vtxArray[v].x;
		aVertices[v].y = vtxArray[v].z;
	}

	Sector.AddVertices(aVertices);

	MIntArray idxVertices;
	for (int p=0; p<mesh.numPolygons(); p++) {

		mesh.getPolygonVertices (p, idxVertices);

		atBucket <u32> aIndices;
		aIndices.Resize(idxVertices.length());
		for (int i=0; i<(int)idxVertices.length(); i++) {
			aIndices[i] = (u32)idxVertices[i];
		}

		Sector.AddPolygon(aIndices);
	}

	return 0;
}
