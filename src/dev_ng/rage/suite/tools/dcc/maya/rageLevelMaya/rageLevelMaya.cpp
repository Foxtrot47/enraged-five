// 
// /rageLevelMaya.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
//	Example command lines
//
//	-command "rageLevelLODMaya \"-level\" \"T:/mc4/assets/city/test_medians/city.level\" \"-template\" \"T:/mc4/assets/templates/city/CityLOD.template\" \"-inputfolder\" \"T:/mc4/art/city/LOD/test_medians/\" \"-pass\" \"T:/mc4/tools/tune/LODshaderBucketsDay.xml\" \"Day\" \"-pass\" \"T:/mc4/tools/tune/LODshaderBucketsNight.xml\" \"Night\" \"-color0\" \"0\" \"0\" \"0\" \"1\" \"-onlycustom\" "
//	-command "rageLevelLODMaya \"-level\" \"T:/mc4/assets/city/la/city.level\" \"-template\" \"T:/mc4/assets/templates/city/CityLOD.template\" \"-inputfolder\" \"T:/mc4/art/city/LOD/la/\" \"-pass\" \"T:/mc4/tools/tune/LODshaderBucketsDay.xml\" \"Day\" \"-pass\" \"T:/mc4/tools/tune/LODshaderBucketsNight.xml\" \"Night\" \"-color0\" \"0\" \"0\" \"0\" \"1\" \"-onlycustom\" "
//	-command "rageLevelLODMaya \"-sector\" \"be_smb_012\" \"-level\" \"T:/mc4/assets/city/la/city.level\" \"-template\" \"T:/mc4/assets/templates/city/CityLOD.template\" \"-inputfolder\" \"T:/mc4/art/city/LOD/la/\" \"-pass\" \"T:/mc4/tools/tune/LODshaderBucketsDay.xml\" \"Day\" \"-pass\" \"T:/mc4/tools/tune/LODshaderBucketsNight.xml\" \"Night\" \"-color0\" \"0\" \"0\" \"0\" \"1\" \"-onlycustom\" "
//	-file T:\mc4\art\railnet\la.mb -command "rageLevelSectorMaya \"T:\\mc4\\assets\\city\\la_temp\\city.sector\" -6000.000000 -6000.000000 6000.000000 6000.000000 400.000000;"
//
//
#pragma warning(disable: 4668)
#pragma warning(push, 3)
#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <crtdbg.h>
#pragma warning(pop)
#pragma warning(error: 4668)

#include "system/param.h"
#include "rageLevelSectorMaya.h"

using namespace rage;

namespace rage
{
	XPARAM(emailasserts);
	XPARAM(nopopups);
//	XPARAM(breakonalloc);
}


//////////////////////////////////////////////////////////////

void* rageLevelSectorMaya::creator()
{
	return new rageLevelSectorMaya();
}

//////////////////////////////////////////////////////////////

MStatus initializePlugin( MObject obj )
{
	// Evil hack, but fake the command line for rage
	rage::sysParam::Init(0,NULL);

#if __ASSERT
	rage::PARAM_emailasserts.Set("krose@rockstarsandiego.com");
#endif //__ASSERT

	// Make it so asserts do not kill Maya
	static const char* gTrueVal = {"true"};
	int bSilentRunning; 
	MGlobal::executeCommand("rageSilentRunning()", bSilentRunning); 
	if(bSilentRunning)
	{
		// rage::PARAM_nopopups.Set(gTrueVal);
	}
	// rage::PARAM_breakonalloc.Set(0);
	// sysMemAllocator::GetCurrent().SetBreakOnAlloc(0);
	// _crtBreakAlloc = 0;

	MFnPlugin plugin( obj, "Alias", "7.0", "Any");

	// Register the commands with the system
	plugin.registerCommand("rageLevelSectorMaya", rageLevelSectorMaya::creator);

	return MStatus::kSuccess;

}
//////////////////////////////////////////////////////////////

MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );
	plugin.deregisterCommand( "rageLevelSectorMaya" );

	return MStatus::kSuccess;
}
