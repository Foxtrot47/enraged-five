// 
// rageLevelMaya/rageLevelSectorMaya.h 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 
#ifndef RAGE_LEVEL_SECTOR_MAYA_H
#define RAGE_LEVEL_SECTOR_MAYA_H

#pragma warning(disable: 4668)
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MPxCommand.h>
#pragma warning(error: 4668)

#include "rageLevel/rageSectorData.h"

namespace rage {

//////////////////////////////////////////////////////////////
class rageLevelSectorMaya : public MPxCommand {
public:
	rageLevelSectorMaya () {}
	virtual         ~rageLevelSectorMaya () {};
	static void*    creator();

	MStatus   	doIt( const MArgList& args );

	bool		isUndoable() const {return false;}
	bool		hasSyntax() const {return false;}

private:

	MStatus	dagTraverseSectors(const MDagPath & dagpath);
	MStatus dagTraverseSector(const MDagPath & dagpath, const char * szSectorName);
	int		newSector(const MDagPath & dagpath, const char * szSectorName);

	rageSectorData		m_SectorData;

};

//////////////////////////////////////////////////////////////

} // end of namespace rage

#endif
