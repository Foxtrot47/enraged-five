#include "rageMayaMarkupWriter.h"

#pragma warning ( push )
#pragma warning ( disable : 4668 )
#pragma warning ( disable : 4100 )
#include <maya/MFnDagNode.h>
#include <maya/MGlobal.h>
#include <maya/MItDag.h>
#include <maya/MMatrix.h>
#include <maya/MPlugArray.h>
#include <maya/MTransformationMatrix.h>
#pragma warning ( pop )

#include "rageMayaAttribute.h"

#include "tinyxml.h"

#include <queue>

using namespace rage;

void rageMayaMarkupWriter::WriteMarkup( const char* filename )
{
	// Create the XML document
	TiXmlDocument doc;
	TiXmlDeclaration* pDeclaration = new TiXmlDeclaration( "1.0", "", "" );
	doc.LinkEndChild( pDeclaration );

	TiXmlComment* pComment = new TiXmlComment( "THIS IS AN AUTOMATICALLY GENERATED FILE. DO NOT EDIT" );
	doc.LinkEndChild( pComment );

	TiXmlElement* pMarkupElement = new TiXmlElement( "Markup" );
	doc.LinkEndChild( pMarkupElement );

	// Write each export element in the scene to the markup element
	WriteScene( pMarkupElement );

	// Write the file
	doc.SaveFile( filename );
	doc.Clear();
}

void rageMayaMarkupWriter::WriteScene( TiXmlElement* pParentElement )
{
	static const char* exportRootName = "|_ExData_:RexExportData";

	// Find the export root node
	MDagPath exportRootDp;
	if ( !FindMayaNode(exportRootDp, exportRootName) )
	{
		// TODO: Error, could not find export root node
		return;
	}

	// Traverse the everything beneath the export root node
	std::queue<MDagPath> queue;
	queue.push( exportRootDp );
	while ( !queue.empty() )
	{
		MDagPath dp = queue.front();
		queue.pop();

		MObject obj = dp.node();
		MFnDagNode fn( obj );
		const char* name = fn.name().asChar();

		// Identify each node by its name
		if ( strstr(name, "Entity") != NULL )					{ WriteEntity(dp, pParentElement); }
		else if ( strstr(name, "Mesh") != NULL )				{ WriteMesh(dp, pParentElement); }
		else if ( strstr(name, "Bound") != NULL )				{ WriteBound(dp, pParentElement); }
		else if ( strstr(name, "Skeleton") != NULL )			{ WriteSkeleton(dp, pParentElement); }
		else if ( strstr(name, "AnimationSegment") != NULL )	{ WriteAnimationSegment(dp, pParentElement); }
		else if ( strstr(name, "Fragment") != NULL )			{ WriteFragment(dp, pParentElement); }

		int childCount = dp.childCount();
		for ( int i = 0; i < childCount; ++i )
		{
			MDagPath childDp = MDagPath::getAPathTo( dp.child(i) );
			queue.push( childDp );
		}
	}
}

void rageMayaMarkupWriter::WriteEntity( const MDagPath& dp, TiXmlElement* pParentElement )
{
	MDagPath childDp = MDagPath::getAPathTo( dp.child(0) );
	MObject childObj = childDp.node();

	MPlug plug;
	if ( GetAttributePlug(plug, childObj, "Nodes") )
	{
		MPlugArray connections;
		plug.connectedTo( connections, false, true );

		int connectionCount = connections.length();
		for ( int i = 0; i < connectionCount; ++i )
		{
			MPlug connection = connections[i];
			MObject nodeObj = connection.node();
			MDagPath nodeDp = MDagPath::getAPathTo( nodeObj );
			MObject node = nodeDp.node();
			MFnDagNode nodeFn( node );
			char pathname[512];
			strcpy_s(pathname, sizeof(pathname), nodeFn.fullPathName().asChar());
			char* c = pathname;
			while (*c != '\0')
			{
				if (*c == '|')
					*c = '/';

				++c;
			}
			
			TiXmlElement* pNodeElement = FindNodeElement( pathname, pParentElement );
			if ( pNodeElement == NULL )
			{
				pNodeElement = AddNodeElement( pathname, pParentElement );
			}

			AddDataElement( "Drawable", pNodeElement );
		}
	}
}

void rageMayaMarkupWriter::WriteMesh( const MDagPath& dp, TiXmlElement* pParentElement )
{
	MObject obj = dp.node();

	MPlug plug;
	if ( GetAttributePlug(plug, obj, "Nodes") )
	{
		MPlugArray connections;
		plug.connectedTo( connections, false, true );

		int connectionCount = connections.length(); 
		for ( int i = 0; i < connectionCount; ++i )
		{
			MPlug connection = connections[i];
			MObject nodeObj = connection.node();
			MDagPath nodeDp = MDagPath::getAPathTo( nodeObj );
			MObject node = nodeDp.node();
			MFnDagNode nodeFn( node );
			char pathname[512];
			strcpy_s(pathname, sizeof(pathname), nodeFn.fullPathName().asChar());
			char* c = pathname;
			while (*c != '\0')
			{
				if (*c == '|')
					*c = '/';

				++c;
			}

			TiXmlElement* pNodeElement = FindNodeElement( pathname, pParentElement );
			if ( pNodeElement == NULL )
			{
				pNodeElement = AddNodeElement( pathname, pParentElement );
			}

			AddDataElement( "Mesh", pNodeElement );
		}
	}
}

void rageMayaMarkupWriter::AddDataElement(MFnDagNode& dn, TiXmlElement* pParentElement)
{
	static const MTypeId RAGE_BOUND_BOX_NODE_ID		= 0x7010;
	static const MTypeId RAGE_BOUND_CAPSULE_NODE_ID	= 0x7011;
	static const MTypeId RAGE_BOUND_SPHERE_NODE_ID	= 0x7012;

	char pathname[512];
	strcpy_s(pathname, sizeof(pathname), dn.fullPathName().asChar());
	char* c = pathname;
	while (*c != '\0')
	{
		if (*c == '|')
			*c = '/';

		++c;
	}
	//const char* name = dn.name().asChar();
	TiXmlElement* pNodeElement = FindNodeElement( pathname, pParentElement );

	MDagPath dp = MDagPath::getAPathTo(dn.object());
	MTransformationMatrix transform( dp.inclusiveMatrix() );
	double scale[3];
	transform.getScale( scale, MSpace::kWorld );

	MStatus status;
	MObject child = dn.child(0, &status);
	if (status == MS::kSuccess)
	{
		MFnDagNode childdn (child);

		char scaleX[32], scaleY[32], scaleZ[32];
		sprintf_s( scaleX, "%f", scale[0] );
		sprintf_s( scaleY, "%f", scale[1] );
		sprintf_s( scaleZ, "%f", scale[2] );

		if ( childdn.typeId() == RAGE_BOUND_BOX_NODE_ID )
		{
			if (pNodeElement == NULL)
				pNodeElement = AddNodeElement(pathname, pParentElement);

			TiXmlElement* pDataElement = AddDataElement( "BoundBox", pNodeElement );
			AddAttributeElement( "X", scaleX, pDataElement );
			AddAttributeElement( "Y", scaleY, pDataElement );
			AddAttributeElement( "Z", scaleZ, pDataElement );
		}
		else if ( childdn.typeId() == RAGE_BOUND_CAPSULE_NODE_ID )
		{
			if (pNodeElement == NULL)
				pNodeElement = AddNodeElement(pathname, pParentElement);

			TiXmlElement* pDataElement = AddDataElement( "BoundCapsule", pNodeElement );
			AddAttributeElement( "Radius", scaleX, pDataElement );
			AddAttributeElement( "Length", scaleZ, pDataElement );
		}
		else if ( childdn.typeId() == RAGE_BOUND_SPHERE_NODE_ID )
		{
			if (pNodeElement == NULL)
				pNodeElement = AddNodeElement(pathname, pParentElement);

			TiXmlElement* pDataElement = AddDataElement( "BoundSphere", pNodeElement );
			AddAttributeElement( "Radius", scaleX, pDataElement );
		}
		else if (strcmp(childdn.typeName().asChar(), "mesh") == 0)
		{
			if (pNodeElement == NULL)
				pNodeElement = AddNodeElement(pathname, pParentElement);

			AddDataElement( "BoundMesh", pNodeElement );
		}
	}

	int childCount = dn.childCount();
	for (int childIndex = 0; childIndex < childCount; ++childIndex)
	{
		MObject child = dn.child(childIndex);
		MFnDagNode dagnode (child);
		AddDataElement(dagnode, pParentElement);
	}
}

void rageMayaMarkupWriter::WriteBound( const MDagPath& dp, TiXmlElement* pParentElement )
{
	MObject obj = dp.node();

	MPlug plug;
	if ( GetAttributePlug(plug, obj, "Nodes") )
	{
		MPlugArray connections;
		plug.connectedTo( connections, false, true );

		int connectionCount = connections.length(); 
		for ( int i = 0; i < connectionCount; ++i )
		{
			MPlug connection = connections[i];
			MObject nodeObj = connection.node();
			MDagPath nodeDp = MDagPath::getAPathTo( nodeObj );
			MObject node = nodeDp.node();
			MFnDagNode nodeFn( node );

			MFnDagNode nodefn( nodeObj );
			AddDataElement(nodefn, pParentElement);
		}
	}
}

void rageMayaMarkupWriter::WriteSkeleton( const MDagPath& /*dp*/, TiXmlElement* /*pParentElement*/ )
{
	// TODO: Not exactly sure how to get the root node name from the skeleton
}

void rageMayaMarkupWriter::WriteAnimationSegment( const MDagPath& dp, TiXmlElement* pParentElement )
{
	int segmentCount = dp.childCount();
	for ( int i = 0; i < segmentCount; ++i )
	{
		MDagPath segmentDp = MDagPath::getAPathTo( dp.child(i) );
		MObject segmentObj = segmentDp.node();
		MFnDagNode segmentFn( segmentDp );
		MString segmentName = segmentFn.name();

		MStringArray parts;
		segmentName.split( ':', parts );
		segmentName = parts[ parts.length() - 1 ];

		const char* rootName = NULL;
		MPlug plug;
		if ( GetAttributePlug(plug, segmentObj, "Nodes") )
		{
			MPlugArray connections;
			plug.connectedTo( connections, false, true );
			if ( connections.length() > 0 )
			{
				MDagPath rootDp = MDagPath::getAPathTo( connections[0].node() );
				MFnDagNode rootFn( rootDp.node() );
				rootName = rootFn.name().asChar();
			}
		}

		char buffer[256];
		MString startFrame = GetAttributeValue(segmentObj, "StartFrame").ToString( buffer, sizeof(buffer) );
		MString endFrame = GetAttributeValue(segmentObj, "EndFrame").ToString( buffer, sizeof(buffer) );
		MString looping = GetAttributeValue(segmentObj, "Looping").ToString( buffer, sizeof(buffer) );

		TiXmlElement* pNodeElement = FindNodeElement( rootName, pParentElement );
		if ( pNodeElement == NULL )
		{
			pNodeElement = AddNodeElement( rootName, pParentElement );
		}

		TiXmlElement* pAnimationElement = FindDataElement( "Animation", pNodeElement );
		if ( pAnimationElement == NULL )
		{
			pAnimationElement = AddDataElement( "Animation", pNodeElement );
		}

		TiXmlElement* pSegmentElement = AddDataElement( "Segment", pAnimationElement );
		AddAttributeElement( "Name", segmentName.asChar(), pSegmentElement );
		AddAttributeElement( "StartFrame", startFrame.asChar(), pSegmentElement );
		AddAttributeElement( "EndFrame", endFrame.asChar(), pSegmentElement );
		AddAttributeElement( "Looping", looping.asChar(), pSegmentElement );
	}
}

void rageMayaMarkupWriter::WriteFragment( const MDagPath& dp, TiXmlElement* pParentElement )
{
	MObject obj = dp.node();

	MPlug plug;
	if ( GetAttributePlug(plug, obj, "Nodes") )
	{
		MPlugArray connections;
		plug.connectedTo( connections, false, true );

		int connectionCount = connections.length(); 
		for ( int i = 0; i < connectionCount; ++i )
		{
			MPlug connection = connections[i];
			MObject nodeObj = connection.node();
			MDagPath nodeDp = MDagPath::getAPathTo( nodeObj );
			MObject node = nodeDp.node();
			MFnDagNode nodeFn( node );
			char pathname[512];
			strcpy_s(pathname, sizeof(pathname), nodeFn.fullPathName().asChar());
			char* c = pathname;
			while (*c != '\0')
			{
				if (*c == '|')
					*c = '/';

				++c;
			}

			TiXmlElement* pNodeElement = FindNodeElement( pathname, pParentElement );
			if ( pNodeElement == NULL )
			{
				pNodeElement = AddNodeElement( pathname, pParentElement );
			}

			AddDataElement( "Fragment", pNodeElement );
		}
	}
}

bool rageMayaMarkupWriter::FindMayaNode( MDagPath& outDp, const char* nodeName )
{
	MItDag it( MItDag::kBreadthFirst, MFn::kInvalid );
	while ( !it.isDone() )
	{
		MDagPath dp;
		it.getPath( dp );

		if ( strcmp(dp.partialPathName().asChar(), nodeName) == 0 ||
			 strcmp(dp.fullPathName().asChar(), nodeName) == 0 )
		{
			outDp = dp;
			return true;
		}

		it.next();
	}

	return false;
}

TiXmlElement* rageMayaMarkupWriter::FindNodeElement( const char* name, TiXmlElement* pParentElement )
{
	Assert( pParentElement );

	TiXmlElement* pElement = pParentElement->FirstChildElement( "Node" );
	while ( pElement != NULL )
	{
		if ( strcmp(pElement->Attribute("Id"), name) == 0 )
		{
			return pElement;
		}
		pElement = pElement->NextSiblingElement( "Node" );
	}

	return NULL;
}

TiXmlElement* rageMayaMarkupWriter::FindDataElement( const char* name, TiXmlElement* pParentElement )
{
	Assert( pParentElement );

	TiXmlElement* pElement = pParentElement->FirstChildElement( "Data" );
	while ( pElement != NULL )
	{
		if ( strcmp(pElement->Attribute("Name"), name) == 0 )
		{
			return pElement;
		}
		pElement = pElement->NextSiblingElement( "Data" );
	}

	return NULL;
}

TiXmlElement* rageMayaMarkupWriter::AddNodeElement( const char* name, TiXmlElement* pParentElement )
{
	Assert( pParentElement );

	TiXmlElement* pNodeElement = new TiXmlElement( "Node" );
	pNodeElement->SetAttribute( "Id", name );
	pNodeElement->SetAttribute( "Technique", "RAGE" );
	pParentElement->LinkEndChild( pNodeElement );

	return pNodeElement;
}

TiXmlElement* rageMayaMarkupWriter::AddDataElement( const char* name, TiXmlElement* pParentElement )
{
	Assert( pParentElement );

	TiXmlElement* pDataElement = new TiXmlElement( "Data" );
	pDataElement->SetAttribute( "Name", name );
	pParentElement->LinkEndChild( pDataElement );

	return pDataElement;
}

TiXmlElement* rageMayaMarkupWriter::AddAttributeElement( const char* name, const char* value, TiXmlElement* pParentElement )
{
	Assert( pParentElement );

	TiXmlElement* pAttributeElement = new TiXmlElement( "Attribute" );
	pAttributeElement->SetAttribute( "Name", name );
	pAttributeElement->SetAttribute( "Value", value );
	pParentElement->LinkEndChild( pAttributeElement );

	return pAttributeElement;
}
