#pragma warning ( push )
#pragma warning ( disable : 4668 )
#pragma warning ( disable : 4100 )
#include <maya/MFileIO.h>
#include <maya/MGlobal.h>
#include <maya/MFnPlugin.h >
#include <maya/MSceneMessage.h>
#pragma warning ( pop )

#include "rageMayaMarkupWriter.h"

static MCallbackId g_BeforeExportCallbackId;

static void BeforeExportCallback( void* )
{
	MString filename = MFileIO::beforeExportFilename();

	MStringArray parts;
	filename.split( '.', parts );

	MString extension = parts[parts.length() - 1].toLowerCase();
	if ( extension == "dae" )
	{
		MString xmlFilename;
		for ( unsigned i = 0; i < parts.length() - 1; ++i )
		{
			xmlFilename += parts[i];
		}
		xmlFilename += ".markup.xml";

		rageMayaMarkupWriter::WriteMarkup( xmlFilename.asChar() );
	}
}

MStatus initializePlugin( MObject obj )
{
	MStatus status;

	g_BeforeExportCallbackId = MSceneMessage::addCallback( MSceneMessage::kBeforeExport, &BeforeExportCallback, NULL, &status );
	if ( MS::kSuccess != status )
	{
		MGlobal::displayError( "An error occurred registering callback: " + status.errorString() );
	}

	return status;
}

MStatus uninitializePlugin( MObject obj )
{
	MStatus status;

	MMessage::removeCallback( g_BeforeExportCallbackId );

	return status;
}
