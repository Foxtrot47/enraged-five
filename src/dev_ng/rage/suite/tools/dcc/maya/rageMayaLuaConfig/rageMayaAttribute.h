#ifndef __RAGE_MAYA_ATTRIBUTE_H__
#define __RAGE_MAYA_ATTRIBUTE_H__

#pragma warning ( push )
#pragma warning ( disable : 4668 )
#pragma warning ( disable : 4100 )
#include <maya/MPlug.h>
#include <maya/MString.h>
#pragma warning ( pop )

namespace rage
{
	class Value
	{
	public:
		enum Type
		{
			TYPE_UNKNOWN,
			TYPE_NULL,
			TYPE_BOOLEAN,
			TYPE_BYTE,
			TYPE_SHORT,
			TYPE_INTEGER,
			TYPE_FLOAT,
			TYPE_DOUBLE,
			TYPE_STRING,
			TYPE_CONSTSTRING,
			TYPE_MSTRING,
		};
	public:
		Value() : m_Type ( TYPE_UNKNOWN ) { }
		Value( bool value ) { SetValue( value ); }
		Value( char value ) { SetValue( value ); }
		Value( short value ) { SetValue( value ); }
		Value( int value ) { SetValue( value ); }
		Value( float value ) { SetValue( value ); }
		Value( double value ) { SetValue( value ); }
		Value( char* value ) { SetValue( value ); }
		Value( const char* value ) { SetValue( value ); }
		Value( const MString& value ) { SetValue( value ); }
		Value( const Value& rhs ) { SetValue( rhs ); }
		Value( Type type )
			:	m_Type ( type )
		{
			switch ( type )
			{
			case TYPE_UNKNOWN:
			case TYPE_NULL:
				break;
			case TYPE_BOOLEAN:
				Boolean = false;
				break;
			case TYPE_BYTE:
				Byte = 0;
				break;
			case TYPE_SHORT:
				Short = 0;
				break;
			case TYPE_INTEGER:
				Int = 0;
				break;
			case TYPE_FLOAT:
				Float = 0.f;
				break;
			case TYPE_DOUBLE:
				Double = 0.0;
				break;
			case TYPE_STRING:
				CharPtr = NULL;
				break;
			case TYPE_CONSTSTRING:
				ConstCharPtr = NULL;
				break;
			case TYPE_MSTRING:
				m_String = "";
				break;
			default:
				m_Type = TYPE_UNKNOWN;
			};
		}

		~Value() { }
	public:
		void SetValue( bool value ) { m_Type = TYPE_BOOLEAN; Boolean = value; }
		void SetValue( char value ) { m_Type = TYPE_BYTE; Byte = value; }
		void SetValue( short value ) { m_Type = TYPE_SHORT; Short = value; }
		void SetValue( int value ) { m_Type = TYPE_INTEGER; Int = value; }
		void SetValue( float value ) { m_Type = TYPE_FLOAT; Float = value; }
		void SetValue( double value ) { m_Type = TYPE_DOUBLE; Double = value; }
		void SetValue( char* value ) { m_Type = TYPE_STRING; CharPtr = value; }
		void SetValue( const char* value ) { m_Type = TYPE_CONSTSTRING; ConstCharPtr = value; }
		void SetValue( const MString& value ) { m_Type = TYPE_MSTRING; m_String = value; }
		void SetValue( const Value& rhs )
		{
			m_Type = rhs.m_Type;
			switch ( m_Type )
			{
			case TYPE_UNKNOWN:
			case TYPE_NULL:
				break;
			case TYPE_BOOLEAN:
				Boolean = rhs.Boolean;
				break;
			case TYPE_BYTE:
				Byte = rhs.Byte;
				break;
			case TYPE_SHORT:
				Short = rhs.Short;
				break;
			case TYPE_INTEGER:
				Int = rhs.Int;
				break;
			case TYPE_FLOAT:
				Float = rhs.Float;
				break;
			case TYPE_DOUBLE:
				Double = rhs.Double;
				break;
			case TYPE_STRING:
				CharPtr = rhs.CharPtr;
				break;
			case TYPE_CONSTSTRING:
				ConstCharPtr = rhs.ConstCharPtr;
				break;
			case TYPE_MSTRING:
				m_String = rhs.m_String;
				break;
			default:
				m_Type = TYPE_UNKNOWN;
				break;
			}
		}

		Type GetType() const { return m_Type; }

		const MString* GetMString() const { if ( TYPE_MSTRING == GetType() ) return &m_String; else return NULL; }

		bool IsUnknown() const { return TYPE_UNKNOWN == m_Type; }
		bool IsNull() const { return TYPE_NULL == m_Type; }
		bool IsIntegral() const { return ( TYPE_INTEGER == m_Type || TYPE_BYTE == m_Type || TYPE_SHORT == m_Type || TYPE_BOOLEAN == m_Type ); }
		bool IsFloat() const { return ( TYPE_FLOAT == m_Type || TYPE_DOUBLE == m_Type ); }
		bool IsNumeric() const { return ( IsIntegral() || IsFloat() ); }
		bool IsString() const { return ( TYPE_STRING == m_Type || TYPE_CONSTSTRING == m_Type || TYPE_MSTRING == m_Type ); }

		const char* ToString( char* buffer, int length ) const;
		int ToInt( int default = 0 ) const;
		float ToFloat( float default = 0.f ) const;
		bool ToBool( bool default = false ) const;
	public:
		static const char* TypeToString( Type type );
	public:
		Value& operator= ( const Value& rhs ) { SetValue( rhs ); return *this; }

		friend bool operator!= ( const Value& lhs, const Value& rhs );
		friend bool operator== ( const Value& lhs, const Value& rhs );
	public:
		Type m_Type;

		union
		{
			bool Boolean;
			char Byte;
			short Short;
			int Int;
			float Float;
			double Double;
			char* CharPtr;
			const char* ConstCharPtr;
		};
		MString m_String;
	};

	bool AttributeExists( const MObject& obj, const MString& attributeName );
	bool GetAttributePlug( MPlug& outPlug, const MObject& obj, const MString& attributeName );
	Value GetAttributeValue( const MObject& obj, const MString& name );

} // rage 
#endif // __RAGE_MAYA_ATTRIBUTE_H__
