#ifndef __RAGE_MAYA_MARKUP_WRITER_H__
#define __RAGE_MAYA_MARKUP_WRITER_H__

#pragma warning ( push )
#pragma warning ( disable : 4668 )
#pragma warning ( disable : 4100 )
#include <maya/MDagPath.h>
#include <Maya/MFnDagNode.h>
#pragma warning ( pop )

class TiXmlElement;

class rageMayaMarkupWriter
{
public:
	static void WriteMarkup( const char* filename );
private:
	static void WriteScene( TiXmlElement* pParentElement );
private:
	static void WriteEntity( const MDagPath& dp, TiXmlElement* pParentElement );
	static void WriteMesh( const MDagPath& dp, TiXmlElement* pParentElement );
	static void WriteBound( const MDagPath& dp, TiXmlElement* pParentElement );
	static void WriteSkeleton( const MDagPath& dp, TiXmlElement* pParentElement );
	static void WriteAnimationSegment( const MDagPath& dp, TiXmlElement* pParentElement );
	static void WriteFragment( const MDagPath& dp, TiXmlElement* pParentElement );
private:
	static bool FindMayaNode( MDagPath& outDp, const char* nodeName );
	static TiXmlElement* FindNodeElement( const char* name, TiXmlElement* pParentElement );
	static TiXmlElement* FindDataElement( const char* name, TiXmlElement* pParentElement );
private:
	static TiXmlElement* AddNodeElement( const char* name, TiXmlElement* pParentElement );
	static void AddDataElement(MFnDagNode& dn, TiXmlElement* pParentElement);
	static TiXmlElement* AddDataElement( const char* name, TiXmlElement* pParentElement );
	static TiXmlElement* AddAttributeElement( const char* name, const char* value, TiXmlElement* pParentElement );
};

#endif // __RAGE_MAYA_MARKUP_WRITER_H__
