#include "rageMayaAttribute.h"

#pragma warning ( push )
#pragma warning ( disable : 4668 )
#pragma warning ( disable : 4100 )
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MDistance.h>
#pragma warning ( pop )

#include <cstdio>

namespace rage {

	const char* Value::TypeToString( Value::Type type )
	{
		switch( type )
		{
		case TYPE_UNKNOWN:
			return "UnKnown";
		case TYPE_NULL:
			return "Null";
		case TYPE_BOOLEAN:
			return "Boolean";
		case TYPE_BYTE:
			return "Byte";
		case TYPE_SHORT:
			return "Short";
		case TYPE_INTEGER:
			return "Integer";
		case TYPE_FLOAT:
			return "Float";
		case TYPE_DOUBLE:
			return "Double";
		case TYPE_STRING:
			return "String";
		case TYPE_CONSTSTRING:
			return "ConstString";
		case TYPE_MSTRING:
			return "MString";
		default:
			return "type not supported";
		}
	}

	bool operator != ( const Value& lhs, const Value& rhs )
	{
		return !( lhs == rhs );
	}

	bool operator == ( const Value& lhs, const Value& rhs )
	{
		if ( lhs.GetType() != rhs.GetType() )
		{
			return false;
		}

		bool result;

		switch ( lhs.GetType() )
		{
		case Value::TYPE_UNKNOWN:
		case Value::TYPE_NULL:
			result = false;
			break;
		case Value::TYPE_BOOLEAN:
			result = lhs.Boolean == rhs.Boolean;
			break;
		case Value::TYPE_BYTE:
			result = lhs.Byte == rhs.Byte;
			break;
		case Value::TYPE_SHORT:
			result = lhs.Short == rhs.Short;
			break;
		case Value::TYPE_INTEGER:
			result = lhs.Int == rhs.Int;
			break;
		case Value::TYPE_FLOAT:
			result = lhs.Float == rhs.Float;
			break;
		case Value::TYPE_DOUBLE:
			result = lhs.Double == rhs.Double;
			break;
		case Value::TYPE_STRING:
			result = lhs.CharPtr == rhs.CharPtr;
			break;
		case Value::TYPE_CONSTSTRING:
			result = lhs.ConstCharPtr == rhs.ConstCharPtr;
			break;
		case Value::TYPE_MSTRING:
			result = lhs.m_String == rhs.m_String;
			break;
		default:
			result = false;
		}

		return result;
	}

	const char* Value::ToString( char* buffer, int length ) const
	{
		switch ( GetType() )
		{
		case TYPE_BOOLEAN:
			{
				sprintf_s( buffer, length, Boolean ? "True" : "False" );
				return buffer;
			}
			break;
		case TYPE_BYTE:
			{
				sprintf_s( buffer, length, "%c", char( Byte ) );
				return buffer;
			}
			break;
		case TYPE_SHORT:
			{
				sprintf_s( buffer, length, "%d", Short );
				return buffer;
			}
			break;
		case TYPE_INTEGER:
			{
				sprintf_s( buffer, length, "%d", Int );
				return buffer;
			}
			break;
		case TYPE_FLOAT:
			{
				sprintf_s( buffer, length, "%f", Float );
				return buffer;
			}
			break;
		case TYPE_DOUBLE:
			{
				sprintf_s( buffer, length, "%f", Double );
				return buffer;
			}
		case TYPE_STRING:
				return CharPtr;
		case TYPE_CONSTSTRING:
			return ConstCharPtr;
		case TYPE_MSTRING:
			return m_String.asChar();
		default:
			return NULL;
		}
	}
			
	bool Value::ToBool( bool default ) const 
	{
		bool res = default;

		switch ( GetType() )
		{
		case TYPE_UNKNOWN:
		case TYPE_NULL:
			break;
		case TYPE_BOOLEAN:
			res = Boolean;
			break;
		case TYPE_BYTE:
			res = 0 != Byte;
			break;
		case TYPE_SHORT:
			res = 0 != Short;
			break;
		case TYPE_INTEGER:
			res = 0 != Int;
			break;
		case TYPE_FLOAT:
			res = 0 != Float;
			break;
		case TYPE_DOUBLE:
			res = 0 != Double;
			break;
		case TYPE_STRING:
			if ( CharPtr[ 0 ] == 'T' || CharPtr[ 0 ] == 't' || CharPtr[ 0 ] == '1' )
			{
				res = true;
			}
			else
			if ( CharPtr[ 0 ] == 'F' || CharPtr[ 0 ] == 'f' || CharPtr[ 0 ] == '0' )
			{
				res = false;
			}
			break;
		case TYPE_CONSTSTRING:
			if ( ConstCharPtr[ 0 ] == 'T' || ConstCharPtr[ 0 ] == 't' || ConstCharPtr[ 0 ] == '1' )
			{
				res = true;
			}
			else
			if ( ConstCharPtr[ 0 ] == 'F' || ConstCharPtr[ 0 ] == 'f' || ConstCharPtr[ 0 ] == '0' )
			{
				res = false;
			}
			break;
		case TYPE_MSTRING:
			if ( !m_String.length() )
			{
				res = default;
			}

			if ( m_String == "TRUE" || m_String == "true" || m_String == "1" )
			{
				res = true;
			}
			else
			if ( m_String == "FALSE" || m_String == "false" || m_String == "0" )
			{
				res = false;
			}
			break;
		default:
			break;
		}

		return res;
	}

	int Value::ToInt( int default ) const
	{
		int res = default;

		switch ( GetType() )
		{
		case TYPE_UNKNOWN:
		case TYPE_NULL:
			break;
		case TYPE_BOOLEAN:
			res = int ( Boolean );
			break;
		case TYPE_BYTE:
			res = int ( Byte );
			break;
		case TYPE_SHORT:
			res = int ( Short );
			break;
		case TYPE_INTEGER:
			res = int ( Int );
			break;
		case TYPE_FLOAT:
			res = int ( Float );
			break;
		case TYPE_DOUBLE:
			res = int ( Double );
			break;
		case TYPE_STRING:
			res = atoi( CharPtr );
			break;
		case TYPE_CONSTSTRING:
			res = atoi( ConstCharPtr );
			break;
		case TYPE_MSTRING:
			res = atoi( m_String.asChar() );
			break;
		default:
			break;
		}

		return res;
	}

	bool AttributeExists( const MObject& obj, const MString& attributeName )
	{
		if ( !obj.isNull() ) 
		{
			MStatus status = MS::kSuccess;

			MFnDependencyNode dn( obj );
			dn.attribute( attributeName, &status );

			return ( status == MS::kSuccess );
		}

		return false;
	}

	bool GetAttributePlug( MPlug& outPlug, const MObject& obj, const MString& attributeName )
	{
		if ( AttributeExists(obj, attributeName) )
		{
			MStatus status = MS::kSuccess;

			MFnDependencyNode dn( obj );
			MObject attributeObj  = dn.attribute( attributeName );
			outPlug = dn.findPlug( attributeObj, &status );

			return ( status == MS::kSuccess );
		}

		return false;
	}

	Value GetAttributeValue( const MObject& obj, const MString& name )
	{
		Value val;

		if ( AttributeExists( obj, name ) )
		{
			MStatus status = MS::kSuccess;
			MFnDependencyNode dn ( obj );
			MObject attributeObj;
			MPlug plug;

			attributeObj = dn.attribute( MString ( name.asChar(), name.length() ) );
			plug = dn.findPlug( attributeObj, &status );

			MFn::Type t = attributeObj.apiType();
			switch ( t )
			{
			case MFn::kNumericAttribute:
				{
					MFnNumericAttribute numericAttribute ( attributeObj );

					switch ( numericAttribute.unitType() )
					{
					case MFnNumericData::kBoolean:
						{
							bool value;
							plug.getValue( value );
							val.SetValue( value );
						}
						break;
					case MFnNumericData::kByte:
						{
							char value;
							plug.getValue( value );
							val.SetValue( value );
						}
						break;
					case MFnNumericData::kShort:
						{
							short value;
							plug.getValue( value );
							val.SetValue( value );
						}
						break;
					case MFnNumericData::kLong:
						{
							int value;
							plug.getValue( value );
							val.SetValue( value );
						}
						break;
					case MFnNumericData::kFloat:
						{
							float value;
							plug.getValue( value );
							val.SetValue( value );
						}
						break;
					case MFnNumericData::kDouble:
						{
							double value;
							plug.getValue( value );
							val.SetValue( value );
						}
						break;
					default:
						break;
					}
				}
				break;
			case MFn::kDoubleLinearAttribute:
				{
					MFnUnitAttribute unitAttr ( attributeObj );

					switch ( unitAttr.unitType() )
					{
					case MFnUnitAttribute::kAngle:
						break;
					case MFnUnitAttribute::kDistance:
						{
							MDistance distance;
							MStatus status;
							status = plug.getValue( distance );
							if ( status )
							{
								double value = distance.asCentimeters();
								val.SetValue( value );
							}
						}
						break;
					case MFnUnitAttribute::kTime:
						break;
					default:
						break;
					}
				}
				break;
			case MFn::kTypedAttribute:
				break;
			default:
				break;
			}

			if ( Value::TYPE_UNKNOWN == val.GetType() )
			{
				MString str;
				plug.getValue( str );
				val.SetValue( str );
			}
		}

		return val;
	}

} // rage
