// 
// metadataEditor.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __AUTOSAVE_UTILITY_H__
#define __AUTOSAVE_UTILITY_H__

//--- SDK include
#include "rexMBAnimExportCommon\utility.h"
#include <fbsdk/fbsdk.h>

#include "atl/string.h"
#include "parser\manager.h"


//--- Registration define
#define ORTOOLNOTE__CLASSNAME	    AutosaveUtility
#define ORTOOLNOTE__CLASSSTR		"AutosaveUtility"

// Forward Declarations
namespace rage
{
	class sysTimer;
}

class AutosaveOptions
{
public:
	AutosaveOptions() 
	{
		m_Enabled = true;
		m_NumberOfFiles = 3;
		m_TimeInterval = 5.0f;
		m_AutosaveFileName = "Autosave";
	}
	virtual ~AutosaveOptions() {}

	bool			m_Enabled;
	int				m_NumberOfFiles;
	float			m_TimeInterval;
	rage::atString	m_AutosaveFileName;

	PAR_PARSABLE;
};

class AutosaveUtility : public FBTool
{
	//--- FiLMBOX Tool declaration.
	FBToolDeclare( AutosaveUtility, FBTool );

public:
	//--- FiLMBOX Construction/Destruction,
	virtual bool FBCreate           ();		//!< FiLMBOX Creation function.
	virtual void FBDestroy          ();		//!< FiLMBOX Destruction function.

	void EventToolIdle( HISender pSender, HKEvent pEvent );
	void EventTimerStop( HISender pSender, HKEvent pEvent );
	void EventTimerReset( HISender pSender, HKEvent pEvent );

	void EventEnableChange( HISender pSender, HKEvent pEvent );
	void EventNumberOfFilesChange( HISender pSender, HKEvent pEvent );
	void EventTimeIntervalChange( HISender pSender, HKEvent pEvent );
	void EventAutosaveFileNameChange( HISender pSender, HKEvent pEvent );

private:
	//--- UI Management
	void    UICreate	            ();
	void	UISetDefaults			();

	void			Save();
	bool			LoadOptions();
	bool			SaveOptions();

	std::string		GetAutosavePath();
	const char*		GetSpecialFolderPath( const DWORD csidl );

	FBSystem	    m_System;
	FBApplication	m_Application;
	FBPlayerControl m_PlayerControl;
	HFBScene		m_Scene;

	FBLabel			m_EnableLabel;
	FBButton		m_EnableCheckbox;

	FBLabel			m_NumberOfFilesLabel;
	FBEdit			m_NumberOfFilesEdit;

	FBLabel			m_IntervalTimeLabel;
	FBEdit			m_IntervalTimeEdit;

	FBLabel			m_AutosaveFileNameLabel;
	FBEdit			m_AutosaveFileNameEdit;

	FBLabel			m_AutosaveLocationLabel;
	FBLabel			m_AutosaveLocationPath;

	AutosaveOptions m_Options;
	int				m_CurrentAutosaveIndex;
	std::string		m_AutosaveLocation;
	rage::sysTimer* m_Timer;

};

#endif // __AUTOSAVE_UTILITY_H__
