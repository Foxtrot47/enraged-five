// 
// metadataEditor.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "AutosaveUtility.h"
#include "AutosaveUtility_parser.h"
#include "rexMBAnimExportCommon\utility.h"
#include "system\timer.h"
#include "file\asset.h"
#include "file\limits.h"
#include "parser\manager.h"
#include <fbsdk\fbsdk.h>

//#include "shlobj.h"

#define AUTOSAVE_OPTIONS_FILE_NAME "options.xml"

using namespace rage;

//#############################################################################

//--- Registration defines
#define ORTOOLNOTE__CLASS	ORTOOLNOTE__CLASSNAME
#define ORTOOLNOTE__LABEL	"Autosave Utility"		//This corresponds to the name in the menu.
#define ORTOOLNOTE__DESC	"Autosave Utility"

#pragma warning(push)
#pragma warning(disable:4100)

//--- FiLMBOX implementation and registration
FBToolImplementation(	ORTOOLNOTE__CLASS	);
FBRegisterTool		(	ORTOOLNOTE__CLASS,
					 ORTOOLNOTE__LABEL,
					 ORTOOLNOTE__DESC,
					 FB_DEFAULT_SDK_ICON			);	// Icon filename (default=Open Reality icon)

#pragma warning(pop)

/************************************************
*	Tool creation function.
************************************************/
bool AutosaveUtility::FBCreate()
{	
	LoadOptions();
	UICreate();
	UISetDefaults();
	return true;
}

/************************************************
*	Tool destruction function.
************************************************/
void AutosaveUtility::FBDestroy()
{

}

/************************************************
*	Create the UI (Assign regions & controls).
************************************************/
void AutosaveUtility::UICreate()
{
	m_Scene = m_System.Scene;

	int iAttachTopY = rexMBAnimExportCommon::c_iMargin;
	int iLabelWidth = rexMBAnimExportCommon::c_iButtonWidth * 2 + 5;
	int iButtonWidth = rexMBAnimExportCommon::c_iButtonWidth * 2;

	int iHeight = (6 * (rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin));
	int iWidth = (iLabelWidth + iButtonWidth + rexMBAnimExportCommon::c_iMargin);

	StartSize[0] = iWidth;
	StartSize[1] = iHeight;

	MinSize[0] = iWidth;
	MinSize[1] = iHeight;


	AddRegion	( "EnableLabel",	"EnableLabel",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachLeft,		"",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		iLabelWidth,						kFBAttachNone,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "EnableLabel",	m_EnableLabel );
	m_EnableLabel.Caption = "Enable";

	AddRegion	( "EnableCheckbox",	"EnableCheckbox",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"EnableLabel",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "EnableCheckbox",	m_EnableCheckbox );
	m_EnableCheckbox.Style = kFBCheckbox;

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	AddRegion	( "NumberOfFilesLabel",	"NumberOfFilesLabel",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachLeft,		"",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		iLabelWidth,						kFBAttachNone,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "NumberOfFilesLabel",	m_NumberOfFilesLabel		);
	m_NumberOfFilesLabel.Caption = "Number of Files:";

	AddRegion	( "NumberOfFilesEdit",	"NumberOfFilesEdit",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"NumberOfFilesLabel",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "NumberOfFilesEdit",	m_NumberOfFilesEdit		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	AddRegion	( "IntervalTimeLabel",	"IntervalTimeLabel",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachLeft,		"",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		iLabelWidth,						kFBAttachNone,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "IntervalTimeLabel",	m_IntervalTimeLabel		);
	m_IntervalTimeLabel.Caption = "Backup Interval (minutes):";

	AddRegion	( "IntervalTimeEdit",	"IntervalTimeEdit",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"IntervalTimeLabel",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "IntervalTimeEdit",	m_IntervalTimeEdit		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	AddRegion	( "AutosaveFileNameLabel",	"AutosaveFileNameLabel",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachLeft,		"",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		iLabelWidth,												kFBAttachNone,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "AutosaveFileNameLabel",	m_AutosaveFileNameLabel		);
	m_AutosaveFileNameLabel.Caption = "Autosave File Name:";

	AddRegion	( "AutosaveFileNameEdit",	"AutosaveFileNameEdit",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"AutosaveFileNameLabel",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "AutosaveFileNameEdit",	m_AutosaveFileNameEdit		);


	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	AddRegion	( "AutosaveLocationLabel",	"AutosaveLocationLabel",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachLeft,		"",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		iLabelWidth,												kFBAttachNone,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,						kFBAttachNone,		"",	1.0 );
	SetControl	( "AutosaveLocationLabel",	m_AutosaveLocationLabel		);
	m_AutosaveLocationLabel.Caption = "Autosave Location:";

	AddRegion	( "AutosaveLocationPath",	"AutosaveLocationPath",		
		rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"AutosaveLocationLabel",	1.0,
		iAttachTopY,												kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,							kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight * 2,					kFBAttachNone,		"",	1.0 );
	SetControl	( "AutosaveLocationPath",	m_AutosaveLocationPath );

	/*OnIdle.Add(this, (FBCallback) &AutosaveUtility::EventToolIdle);
	m_Application.OnFileExit.Add(this, (FBCallback) &AutosaveUtility::EventTimerStop);
	m_Application.OnFileNew.Add(this, (FBCallback) &AutosaveUtility::EventTimerStop);
	m_Application.OnFileOpen.Add(this, (FBCallback) &AutosaveUtility::EventTimerStop);
	m_Application.OnFileSave.Add(this, (FBCallback) &AutosaveUtility::EventTimerStop);
	m_Application.OnFileNewCompleted.Add(this, (FBCallback) &AutosaveUtility::EventTimerReset);
	m_Application.OnFileOpenCompleted.Add(this, (FBCallback) &AutosaveUtility::EventTimerReset);
	m_Application.OnFileSaveCompleted.Add(this, (FBCallback) &AutosaveUtility::EventTimerReset);

	//Bind the events when these text fields change.
	//
	m_EnableCheckbox.OnClick.Add(this, (FBCallback) &AutosaveUtility::EventEnableChange);
	m_NumberOfFilesEdit.OnChange.Add(this, (FBCallback) &AutosaveUtility::EventNumberOfFilesChange);
	m_IntervalTimeEdit.OnChange.Add(this, (FBCallback) &AutosaveUtility::EventTimeIntervalChange);
	m_AutosaveFileNameEdit.OnChange.Add(this, (FBCallback) &AutosaveUtility::EventAutosaveFileNameChange);*/
}

void AutosaveUtility::UISetDefaults()
{
	m_EnableCheckbox.State = (m_Options.m_Enabled == true) ? CHECKBOX_CHECKED : CHECKBOX_UNCHECKED;

	char captionText[RAGE_MAX_PATH];
	sprintf_s(captionText, RAGE_MAX_PATH, "%d", m_Options.m_NumberOfFiles);
	m_NumberOfFilesEdit.Text = captionText;

	sprintf_s(captionText, RAGE_MAX_PATH, "%f", m_Options.m_TimeInterval);
	m_IntervalTimeEdit.Text = "5.0";

	sprintf_s(captionText, RAGE_MAX_PATH, "%f", m_Options.m_TimeInterval);
	m_AutosaveFileNameEdit.Text = (char*) m_Options.m_AutosaveFileName.c_str();

	m_AutosaveLocationPath.Caption = (char*) GetAutosavePath().c_str();
	m_AutosaveLocationPath.WordWrap = true;

	m_CurrentAutosaveIndex = 0;

	m_Timer = new rage::sysTimer();
}

void AutosaveUtility::EventEnableChange( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if( m_EnableCheckbox.State == CHECKBOX_CHECKED )
	{
		m_Options.m_Enabled = true;
	}
	else
	{
		m_Options.m_Enabled = false;
	}

	SaveOptions();
}

void AutosaveUtility::EventNumberOfFilesChange( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	char* editText = m_NumberOfFilesEdit.Text.AsString();
	sscanf(editText, "%d", &(m_Options.m_NumberOfFiles));

	SaveOptions();
}

void AutosaveUtility::EventTimeIntervalChange( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	char* editText = m_IntervalTimeEdit.Text.AsString();
	sscanf(editText, "%f", &(m_Options.m_TimeInterval));

	EventTimerReset(NULL, NULL); //Reset the timer since the user has adjusted the interval.

	SaveOptions();
}

void AutosaveUtility::EventAutosaveFileNameChange( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_Options.m_AutosaveFileName = m_AutosaveFileNameEdit.Text.AsString();
	SaveOptions();
}

void AutosaveUtility::EventTimerStop( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_Timer != NULL)
	{
		delete m_Timer;
	}
}

void AutosaveUtility::EventTimerReset( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_Timer != NULL)
	{
		m_Timer->Reset();
	}
	else
	{
		m_Timer = new sysTimer();
	}
}

void AutosaveUtility::EventToolIdle( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	//The time interval is specified by the user in minutes.  GetTime()
	//returns in units of seconds.
	float timeIntervalSeconds = m_Options.m_TimeInterval * 60.0f;
	if(m_Timer->GetTime() >= timeIntervalSeconds)
	{
		Save();
		m_Timer->Reset();
	}
}

void AutosaveUtility::Save()
{
	if(m_Options.m_Enabled == true)
	{
		std::string autosavePath = GetAutosavePath();

		std::string autosaveFileName(autosavePath);
		autosaveFileName.append(m_Options.m_AutosaveFileName);

		char autosaveSuffix[RAGE_MAX_PATH];
		sprintf_s(autosaveSuffix, RAGE_MAX_PATH, "%d", m_CurrentAutosaveIndex);
		autosaveFileName.append(autosaveSuffix);
		autosaveFileName.append(".fbx");

		ASSET.CreateLeadingPath(autosaveFileName.c_str());

		m_Application.FileSave( (char*) autosaveFileName.c_str());

		m_CurrentAutosaveIndex++;
	}
}

bool  AutosaveUtility::SaveOptions()
{
	std::string autosavePath = GetAutosavePath();
	autosavePath.append(AUTOSAVE_OPTIONS_FILE_NAME);

	ASSET.CreateLeadingPath(autosavePath.c_str());

	const char* filepath = autosavePath.c_str();
	if( PARSER.SaveObject(filepath, "", &m_Options) ) 
		return true;

	return false;
}

bool AutosaveUtility::LoadOptions()
{
	std::string autosavePath = GetAutosavePath();
	autosavePath.append(AUTOSAVE_OPTIONS_FILE_NAME);

	if( ASSET.Exists(AUTOSAVE_OPTIONS_FILE_NAME, "") == false)
		return true;

	if( PARSER.LoadObject(autosavePath.c_str(), "", m_Options) ) 
		return true;

	return false;
}

std::string AutosaveUtility::GetAutosavePath()
{
/*	char path[512];
	const char* MyDocuments = GetSpecialFolderPath( CSIDL_PERSONAL ); 
#define MOTION_BUILDER_AUTOSAVE_PATH "\\motionbuilder\\autosave\\"

	sprintf(path, "%s%s", MyDocuments, MOTION_BUILDER_AUTOSAVE_PATH);
	return std::string(path);
*/
	return std::string("");
}

const char* AutosaveUtility::GetSpecialFolderPath( const DWORD /*csidl*/ )  
{  
/*
	HANDLE ProcToken = NULL;  
	OpenProcessToken( GetCurrentProcess(), TOKEN_READ, &ProcToken );  

	TCHAR szBuffer[MAX_PATH*2] = { 0 };  

	SHGetFolderPath( NULL, csidl, ProcToken, SHGFP_TYPE_CURRENT, szBuffer );  

	const char* SpecialFolderPath = szBuffer;  
	CloseHandle( ProcToken );  
	return SpecialFolderPath;  
*/
	return NULL;
}  