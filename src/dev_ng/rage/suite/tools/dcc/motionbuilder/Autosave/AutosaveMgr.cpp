// /AutosaveCmdMgr.cpp

#include "AutosaveMgr.h"

//--- Registration defines
#define AUTOSAVECUSTOMMGR__CLASS AUTOSAVECUSTOMMGR__CLASSNAME
#define AUTOSAVECUSTOMMGR__NAME  AUTOSAVECUSTOMMGR__CLASSSTR

//--- FiLMBOX implementation and registration
FBCustomManagerImplementation( AUTOSAVECUSTOMMGR__CLASS  );  // Manager class name.
FBRegisterCustomManager( AUTOSAVECUSTOMMGR__CLASS );         // Manager class name.

//-----------------------------------------------------------------------------

bool AutosaveCustomManager::FBCreate()
{
	return true;
}

//-----------------------------------------------------------------------------

void AutosaveCustomManager::FBDestroy()
{
}

//-----------------------------------------------------------------------------

bool AutosaveCustomManager::Init()
{
	return true;
}

//-----------------------------------------------------------------------------

bool AutosaveCustomManager::Open()
{
	FBPopNormalTool("Autosave Utility", false);
	return true;
}

//-----------------------------------------------------------------------------

bool AutosaveCustomManager::Clear()
{
	return true;
}

//-----------------------------------------------------------------------------

bool AutosaveCustomManager::Close()
{
	return true;
}

//-----------------------------------------------------------------------------
