// /AutosaveMgr.h

#ifndef __AUTOSAVE_CUSTOM_MANAGER_H__
#define __AUTOSAVE_CUSTOM_MANAGER_H__

#include "rexMBAnimExportCommon/utility.h"

#define AUTOSAVECUSTOMMGR__CLASSNAME AutosaveCustomManager
#define AUTOSAVECUSTOMMGR__CLASSSTR  "AutosaveCustomManager"

//-----------------------------------------------------------------------------

//This whole class exists only to call FBPopNormalTool on the animation exporter
//which allows for the tool to load data from files before the user has opened the
//animation export tool for the first time... a waste of code, but it works.

class AutosaveCustomManager : public FBCustomManager
{
	//--- FiLMBOX box declaration.
	FBCustomManagerDeclare( AutosaveCustomManager );

public:
	virtual bool FBCreate();        //!< FiLMBOX creation function.
	virtual void FBDestroy();       //!< FiLMBOX destruction function.

	virtual bool Init();
	virtual bool Open();
	virtual bool Clear();
	virtual bool Close();

private:

};

//-----------------------------------------------------------------------------

#endif //__AUTOSAVE_CUSTOM_MANAGER_H__

