#include <windows.h>
#include <commctrl.h>

#include "AutosaveUtility.h"
#include "parser/manager.h"
#include "system/param.h"
#include "rexMBAnimExportCommon/utility.h"

using namespace rage;

#pragma warning(push)
#pragma warning(disable:4100)

FBLibraryDeclare( AutosaveUtility )
{
	FBLibraryRegister( AutosaveUtility );
	FBLibraryRegister( AutosaveCustomManager );
}

FBLibraryDeclareEnd;

#pragma warning(pop)

bool FBLibrary::LibInit()	{ 
	
	char appName[] = "AutosaveUtility.dll";
	char* p_Argv[1];

	p_Argv[0] = appName;

	sysParam::Init(1,p_Argv);

	INIT_PARSER;

	return true; 
}

bool FBLibrary::LibOpen()	{ return true; }
bool FBLibrary::LibReady()	{ return true; }
bool FBLibrary::LibClose()	{ return true; }

bool FBLibrary::LibRelease()
{ 
	SHUTDOWN_PARSER;
	return true; 
}

