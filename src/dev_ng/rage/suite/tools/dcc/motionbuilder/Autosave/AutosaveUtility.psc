<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<structdef type="::AutosaveOptions">
	<bool init="true" name="m_Enabled"/>
	<int init="3" name="m_NumberOfFiles"/>
	<float init="5.0" name="m_TimeInterval"/>
	<string name="m_AutosaveFileName" noInit="true" type="atString"/>
</structdef>

</ParserSchema>