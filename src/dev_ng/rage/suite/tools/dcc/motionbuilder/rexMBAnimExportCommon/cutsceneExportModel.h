// 
// rexMBAnimExportCommon/cutsceneExportModel.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REXMBRAGE_CUTSCENE_EXPORT_MODEL_H__
#define __REXMBRAGE_CUTSCENE_EXPORT_MODEL_H__

#include "atl/array.h"
#include "atl/string.h"
#include "rexBase/logAndErrorGenerator.h"
#include "rexGeneric/objectAnimation.h"
#include "animExport.h"
#include "clipxml/clipxml.h"

namespace rage
{

//#############################################################################

class crClipAnimation;
class RexMbCutsceneExportData;
class rexSerializerRAGEAnimation;

//##############################################################################

struct SToyboxEntry
{
public:
	SToyboxEntry()
		: strName("")
	{
		bPreviouslyDropped=false;
		bPreviouslyLoaded=false;
		bValid = false;
	}

	void Reset()
	{
		frameList.Reset();
		loadListFrames.Reset();
		dropListFrames.Reset();
		bPreviouslyLoaded = false;
		bPreviouslyDropped = false;
		bValid = false;
		strName = "";
	}

	atArray<int> frameList; //An ordered-by-time array of loadListFrames and dropListFrames.
	atArray<int> loadListFrames;
	atArray<int> dropListFrames;
	bool bPreviouslyLoaded;
	bool bPreviouslyDropped;
	atString strName;
	bool bValid;
};

//--------------------------------------------------------------------------------
struct SCameraCutData
{
    SCameraCutData()
        : fTime(0.0f)
    {
        cName[0] = 0;
        vPosition.Zero();
        vRotationQuaternion.Zero();
    }

    float fTime;
    char cName[32];
    Vector3 vPosition;
    Vector4 vRotationQuaternion;

	bool operator==(const SCameraCutData &a) const
	{
		if( this->fTime == a.fTime &&
			this->vPosition == a.vPosition &&
			this->vRotationQuaternion == a.vRotationQuaternion &&
			(strcmpi(this->cName, a.cName) == 0))
			return true;

		return false;
	}
};

//##############################################################################

class RexMbCutsceneExportModel
{
public:
    RexMbCutsceneExportModel( HFBModel pModel, SToyboxEntry* pToyboxEntry, const char *pFilenameWithoutExtension );
    virtual ~RexMbCutsceneExportModel() = 0;

    // PURPOSE: Sets the functor to be called when the caption of a progress indicator should be changed
    // PARAMS:
    //    ftor - the callback function
    //static void SetProgressCaptionCallback( rexMBAnimExportCommon::ProgressCaptionFtor ftor );

    // PURPOSE: Sets the functor to be called when error, warning, or informational text should be displayed.
    // PARAMS:
    //    ftor - the callback function
    //static void SetProgressMessageCallback( rexMBAnimExportCommon::ProgressMessageFtor ftor );

    // PURPOSE: Invokes the progress caption update functor
    // PARAMS:
    //    caption - new caption value
    static void SetProgressCaption( const char* pCaption );

    // PURPOSE: Invokes the progress message update functor
    // PARAMS:
    //    pMessage - the message to display
    //static void SetProgressMessage( const char* pMessage );

    // PURPOSE: Step 1.  Initialize the RexMbCutsceneExportData and track information arrays.
    // PARAMS:
    //    nFrames - the number of frames we are going to collect data for
    // RETURNS: true on success, otherwise false
    virtual bool Initialize( int nFrames ) = 0;

    // PURPOSE: Resets all of the RexMbCutsceneExportData
    virtual bool Reset();

    // PURPOSE: Step 2a.  Sample the data for the current frame.
    void CollectAnimData();

    // PURPOSE: Static version of Step 2a.  Sample the data for the current frame.
    // PARAMS:
    //    animDataList - the list of structres to collect the data into
    static void CollectAnimData( atArray<RexMbCutsceneExportData *> &animDataList );

    // PURPOSE: Step 2b.  Read all of the keyframe data between start and end.
    // PARAMS:
    //    fStartTime - the start time
    //    fEndTime - the end time
    //    nFrames - the number of frames we are going to collect data for
    void CollectAnimData( float fStartTime, float fEndTime, int nFrames );

    // PURPOSE: Static version of Step 2b.  Read all of the keyframe data between start and end.
    // PARAMS:
    //    animDataList - the list of structres to collect the data into
    //    fStartTime - the start time
    //    fEndTime - the end time
    //    nFrames - the number of frames we are going to collect data for
    static void CollectAnimData( atArray<RexMbCutsceneExportData *> &animDataList, float fStartTime, float fEndTime, int nFrames );

    // PURPOSE: Step 3.  Save the animation.
    // PARAMS:
    //    fStartTime - the start time
    //    fEndTime - the end time
    //    nFrames - the number of frames we are going to collect data for
    // RETURNS: true on success, otherwise false
    bool SerializeAnimation( float fStartTime, float fEndTime, int nFrames );
    
    // PURPOSE: Step 4.  Save the clip.
    //    fStartTime - the start time
    //    fEndTime - the end time
    // RETURNS: true on success, otherwise false
    bool SerializeClip( float fStartTime, float fEndTime, RexAnimMarkupData* pAnimData=NULL );

    HFBModel GetModel();
	const SToyboxEntry* GetToyboxData() const;
    const char* GetFilenameWithoutExtension() const;
    const atArray<RexMbCutsceneExportData *>& GetExportDataList() const;
    const atArray<RexMbCutsceneExportData *>& GetTrackExportDataList() const;
    const atArray<atString>& GetTrackNameList() const;
    const atArray<int>& GetTrackIDList() const;

protected:
    // PURPOSE: Perform any custom processing to the data that was collected.  Called by SerializeAnimation().
    // PARAMS:
    //    nFrames - the number of frames we collected data for
    // RETURNS: true on success, otherwise false
    virtual bool ProcessCollectedAnimData( int nFrames );

    // PURPOSE: Create a custom serializer.  Call by SerializeAnimation().
    // RETURNS: The initialized serializer
    virtual rexSerializerRAGEAnimation* CreateAnimationSerializer() = 0;

    // PURPOSE: Process the crClipAnimation by adding custom properties, tags, etc.  Called by SerializeClip().
    // PARAMS:
    //    pClipAnim - the clip to process
    // RETURNS: true on success, otherwise false
    virtual bool ProcessClip( ClipXML* clip );
	virtual bool ProcessClip( crClipAnimation *pClipAnim );

    HFBModel m_pModel;
    char m_cFilenameWithoutExtension[RAGE_MAX_PATH];
	SToyboxEntry* m_toyboxData;
    atArray<RexMbCutsceneExportData *> m_exportDataList;
    atArray<RexMbCutsceneExportData *> m_trackExportDataList;
    atArray<atString> m_trackNameList;
    atArray<int> m_trackIDList;
	atArray<atString> m_trackVarNameList;

private:
    // PURPOSE: Creates and initializes the rexObjectGenericAnimationSegment and rexObjectGenericAnimation objects
    // PARAMS:
    //    fStartTime - the start time
    //    fEndTime - the end time
    //    pAnimationSegment - reference to the segment to create and initialize
    //    pAnimation - reference to the animation to create and initialize
    // RETURNS: true on success, otherwise false
    bool InitRexAnimation( float fStartTime, float fEndTime, rexObjectGenericAnimationSegment* &pAnimationSegment, rexObjectGenericAnimation* &pAnimation );

    // PURPOSE: Exports the rexObjectGenericAnimation using the provided rexSerializerRAGEAnimation.
    // PARAMS:
    //    pRexSer - the serializer
    //    pAnimation - the animation to export
    // RETURNS: true on success, otherwise false
    bool ExportRexAnimation( rexSerializerRAGEAnimation *pRexSer, rexObjectGenericAnimation* pAnimation );

    // PURPOSE: Create a list of tracks with with the names and ids provided in m_trackNameList and m_trackIDList.
    // PARAMS:
    //    iStartFrame - the start frame
    //    iEndFrame - the end frame
    //    nFrames - the number of frames we are going to collect
    //    trackList - out parameter for the created tracks
    void InitTracksInfo( float fStartTime, float fEndTime, int nFrames, atArray<rexObjectGenericAnimation::TrackInfo*>& trackList );    

    // PURPOSE: Initializes a generic animation track
    // PARAMS:
    //    fStartTime - the start time
    //    fEndTime - the end time
    //    nFrames - the number of frames we are going to collect
    //    track - generic animation track to be initialized
    //RETURNS: true for success, false for failure
    bool InitTrackInfo( float fStartTime, float fEndTime, int nFrames, rexObjectGenericAnimation::TrackInfo& track );

    // PURPOSE: Transfers the data from m_trackExportDataList to the array of TrackInfo.
    // PARAMS:
    //    nFrames - the number of frames we are going to collect
    //    trackList - the destination data
    void AddAnimDataToTracks( int nFrames, atArray<rexObjectGenericAnimation::TrackInfo*> &trackList );

	// PURPOSE: Removes all crTagBlocks to the given clip 
	// PARAMS:
	//    pClip - the clip to edit
	void RemoveBlockTags(ClipXML* clip);
	void RemoveBlockTags(crClipAnimation *pClipAnim);

    // PURPOSE: Adds the crTagBlocks to the given clip for the camera cuts
    // PARAMS:
    //    fStartTime - the start time
    //    fEndTime - the end time
    //    pClip - the clip to edit
    void AddCameraCutTagBlocks( float fStartTime, float fEndTime, ClipXML* clip );
	void AddCameraCutTagBlocks( float fStartTime, float fEndTime, crClipAnimation *pClipAnim );

	// PURPOSE: Adds the crTagBlocks to the given clip based on the toybox information.
	// PARAMS:
	//    fStartTime - the start time
	//    fEndTime - the end time
	//    pClip - the clip to edit
	void AddToyboxTagBlocks( float fStartTime, float fEndTime, ClipXML* clip );
	void AddToyboxTagBlocks( float fStartTime, float fEndTime, crClipAnimation *pClipAnim );

};

inline void RexMbCutsceneExportModel::SetProgressCaption( const char* pCaption )
{
    ULOGGER_MOBO.SetProgressCaption(pCaption);
}

inline HFBModel RexMbCutsceneExportModel::GetModel()
{
    return m_pModel;
}

inline const char* RexMbCutsceneExportModel::GetFilenameWithoutExtension() const
{
    return m_cFilenameWithoutExtension;
}

inline const SToyboxEntry* RexMbCutsceneExportModel::GetToyboxData() const
{
	return m_toyboxData;
}

inline const atArray<RexMbCutsceneExportData *>& RexMbCutsceneExportModel::GetExportDataList() const
{
    return m_exportDataList;
}

inline const atArray<RexMbCutsceneExportData *>& RexMbCutsceneExportModel::GetTrackExportDataList() const
{
    return m_trackExportDataList;
}

inline const atArray<atString>& RexMbCutsceneExportModel::GetTrackNameList() const
{
    return m_trackNameList;
}

inline const atArray<int>& RexMbCutsceneExportModel::GetTrackIDList() const
{
    return m_trackIDList;
}

//#############################################################################

class RexMbCutsceneExportGenericModel : public RexMbCutsceneExportModel
{
public:
    RexMbCutsceneExportGenericModel( HFBModel pModel, SToyboxEntry* pToyboxEntry, const char *pFilenameWithoutExtension );
    ~RexMbCutsceneExportGenericModel();

    // PURPOSE: Step 1.  Initialize the RexMbCutsceneExportData and track information arrays.
    // PARAMS:
    //    nFrames - the number of frames we are going to collect data for
    // RETURNS: true on success, otherwise false
    virtual bool Initialize( int nFrames );

protected:
    // PURPOSE: Create a custom serializer.  Call by SerializeAnimation().
    // RETURNS: The initialized serializer
    virtual rexSerializerRAGEAnimation* CreateAnimationSerializer();
};

//#############################################################################

class RexMbCutsceneExportCameraModel : public RexMbCutsceneExportModel
{
public:
    RexMbCutsceneExportCameraModel( HFBModel pModel, SToyboxEntry* pToyboxEntry, const char *pFilenameWithoutExtension );
    ~RexMbCutsceneExportCameraModel();

    // PURPOSE: Step 1.  Initialize the RexMbCutsceneExportData and track information arrays.
    // PARAMS:
    //    nFrames - the number of frames we are going to collect data for
    // RETURNS: true on success, otherwise false
    virtual bool Initialize( int nFrames );

    void GetCameraCutData( atArray<SCameraCutData> &cameraCutDataList );

	const atArray<RexMbCutsceneExportData *>& GetTrackExportData() { return m_trackExportDataList; }

protected:
    // PURPOSE: Step 3.  Perform any custom processing to the data that was collected.
    // PARAMS:
    //    nFrames - the number of frames we collected data for
    // RETURNS: true on success, otherwise false
    virtual bool ProcessCollectedAnimData( int nFrames );
    
    // PURPOSE: Create a custom serializer.  Call by SerializeAnimation().
    // RETURNS: The initialized serializer
    virtual rexSerializerRAGEAnimation* CreateAnimationSerializer();
};

//#############################################################################

class RexMbCutsceneExportLightModel : public RexMbCutsceneExportModel
{
public:
    RexMbCutsceneExportLightModel( HFBModel pModel, SToyboxEntry* pToyboxInfo, const char *pFilenameWithoutExtension );
    ~RexMbCutsceneExportLightModel();

    // PURPOSE: Step 1.  Initialize the RexMbCutsceneExportData and track information arrays.
    // PARAMS:
    //    nFrames - the number of frames we are going to collect data for
    // RETURNS: true on success, otherwise false
    virtual bool Initialize( int nFrames );

protected:
	// PURPOSE: Step 3.  Perform any custom processing to the data that was collected.
	// PARAMS:
	//    nFrames - the number of frames we collected data for
	// RETURNS: true on success, otherwise false
	virtual bool ProcessCollectedAnimData( int nFrames );
    // PURPOSE: Create a custom serializer.  Call by SerializeAnimation().
    // RETURNS: The initialized serializer
    virtual rexSerializerRAGEAnimation* CreateAnimationSerializer();
};

//#############################################################################

class RexMbCutsceneExportParticleEffectModel : public RexMbCutsceneExportModel
{
public:
    RexMbCutsceneExportParticleEffectModel( HFBModel pModel, SToyboxEntry* pToyboxEntry, const char *pFilenameWithoutExtension );
    ~RexMbCutsceneExportParticleEffectModel();

    // PURPOSE: Step 1.  Initialize the RexMbCutsceneExportData and track information arrays.
    // PARAMS:
    //    nFrames - the number of frames we are going to collect data for
    // RETURNS: true on success, otherwise false
    virtual bool Initialize( int nFrames );

    virtual bool Reset();

protected:
    // PURPOSE: Perform any custom processing to the data that was collected.  Called by SerializeAnimation().
    // PARAMS:
    //    nFrames - the number of frames we collected data for
    // RETURNS: true on success, otherwise false
    virtual bool ProcessCollectedAnimData( int nFrames );

    // PURPOSE: Create a custom serializer.  Call by SerializeAnimation().
    // RETURNS: The initialized serializer
    virtual rexSerializerRAGEAnimation* CreateAnimationSerializer();

    // PURPOSE: Process the crClipAnimation by adding custom properties, tags, etc.  Called by SerializeClip().
    // PARAMS:
    //    pClipAnim - the clip to process
    // RETURNS: true on success, otherwise false
	virtual bool ProcessClip( ClipXML* clip );
	virtual bool ProcessClip( crClipAnimation *pClipAnim );

private:
    // PURPOSE: Retrieves the list of evolution parameters that are available for the given particle effect model.
    // PARAMS:
    //    evoParamNameList - out parameter for the collected names
    // RETURNS: true if some were found, otherwise false
    bool GetEvoParamNames( atArray<ConstString> &evoParamNameList );

    bool m_bIsConstrained;
    int m_iNumEvoParams;
};

//##############################################################################

class RexMbCutsceneExportData
{
public:
    RexMbCutsceneExportData();
    virtual ~RexMbCutsceneExportData() = 0;

    virtual void ReadFrameData() = 0;

    virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames ) = 0;

    virtual void WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo ) = 0;
};

//##############################################################################

class RexMbCutsceneExportMatrixData : public RexMbCutsceneExportData
{
public:
    RexMbCutsceneExportMatrixData( int nFrames );
    virtual ~RexMbCutsceneExportMatrixData();

    const atArray<Matrix34>& GetFrameData() const;

protected:
    atArray<Matrix34> m_frameMatrices;
};

inline const atArray<Matrix34>& RexMbCutsceneExportMatrixData::GetFrameData() const
{
    return m_frameMatrices;
}

//##############################################################################

class RexMbCutsceneExportVectorData : public RexMbCutsceneExportData
{
public:
    RexMbCutsceneExportVectorData( int nFrames );
    virtual ~RexMbCutsceneExportVectorData();

    const atArray<Vector3>& GetFrameData() const;

protected:
    atArray<Vector3> m_frameVectors;
};

inline const atArray<Vector3>& RexMbCutsceneExportVectorData::GetFrameData() const
{
    return m_frameVectors;
}

//##############################################################################

class RexMbCutsceneExportVector4Data : public RexMbCutsceneExportData
{
public:
	RexMbCutsceneExportVector4Data( int nFrames );
	virtual ~RexMbCutsceneExportVector4Data();

	const atArray<Vector4>& GetFrameData() const;

protected:
	atArray<Vector4> m_frameVectors;
};

inline const atArray<Vector4>& RexMbCutsceneExportVector4Data::GetFrameData() const
{
	return m_frameVectors;
}

//##############################################################################

class RexMbCutsceneExportModelMatrixData : public RexMbCutsceneExportMatrixData
{
public:
	RexMbCutsceneExportModelMatrixData( HFBModel pModel, int nFrames, float fTranslateScaleFactor=1.0f, bool bForceZUp=false, bool bUseGlobal=false );

    virtual void ReadFrameData();

    virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

    virtual void WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo );

    HFBModel GetModel() const;
    float GetTranslateScaleFactor() const;
	bool GetForceZUp() { return m_ForceZUp; }

protected:
    HFBModel m_pModel;
    float m_fTranslateScaleFactor;
	bool m_ForceZUp;
	bool m_UseGlobal;
};

inline HFBModel RexMbCutsceneExportModelMatrixData::GetModel() const
{
    return m_pModel;
}

inline float RexMbCutsceneExportModelMatrixData::GetTranslateScaleFactor() const
{
    return m_fTranslateScaleFactor;
}

//##############################################################################

class RexMbCutsceneExportBoxVectorData : public RexMbCutsceneExportVectorData
{
public:
    RexMbCutsceneExportBoxVectorData( HFBBox pBox, int nFrames );

    HFBBox GetBox() const;

protected:
    HFBBox m_pBox;
};

inline HFBBox RexMbCutsceneExportBoxVectorData::GetBox() const
{
    return m_pBox;
}

//##############################################################################

class RexMbCutsceneExportDepthOfFieldData : public RexMbCutsceneExportVectorData
{
public:
    RexMbCutsceneExportDepthOfFieldData( HFBModel pModel, int nFrames, float fTranslateScaleFactor=1.0f );
    virtual ~RexMbCutsceneExportDepthOfFieldData();

    virtual void ReadFrameData();

    virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

    virtual void WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo );

    HFBModel GetModel() const;
    HFBModel GetNearPlaneModel() const;
    HFBModel GetFarPlaneModel() const;
    float GetTranslateScaleFactor() const;

protected:
    HFBModel m_pModel;
    HFBModel m_pNearPlaneModel;
    HFBModel m_pFarPlaneModel;
    float m_fTranslateScaleFactor;
};

inline HFBModel RexMbCutsceneExportDepthOfFieldData::GetModel() const
{
    return m_pModel;
}

inline HFBModel RexMbCutsceneExportDepthOfFieldData::GetNearPlaneModel() const
{
    return m_pNearPlaneModel;
}

inline HFBModel RexMbCutsceneExportDepthOfFieldData::GetFarPlaneModel() const
{
    return m_pFarPlaneModel;
}

inline float RexMbCutsceneExportDepthOfFieldData::GetTranslateScaleFactor() const
{
    return m_fTranslateScaleFactor;
}

//##############################################################################

class RexMbCutsceneExportDepthOfField4PlanesData : public RexMbCutsceneExportVector4Data
{
public:
	RexMbCutsceneExportDepthOfField4PlanesData( HFBModel pModel, int nFrames, float fTranslateScaleFactor=1.0f );
	virtual ~RexMbCutsceneExportDepthOfField4PlanesData();

	virtual void ReadFrameData();

	virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

	virtual void WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo );

	HFBModel GetModel() const;
	HFBModel GetNearPlaneModel() const;
	HFBModel GetFarPlaneModel() const;
	HFBModel GetNearDOFStrengthPlaneModel() const;
	HFBModel GetFarDOFStrengthPlaneModel() const;
	float GetTranslateScaleFactor() const;

protected:
	HFBModel m_pModel;
	HFBModel m_pNearPlaneModel;
	HFBModel m_pFarPlaneModel;
	HFBModel m_pNearDOFStrengthPlaneModel;
	HFBModel m_pFarDOFStrengthPlaneModel;
	float m_fTranslateScaleFactor;
};

inline HFBModel RexMbCutsceneExportDepthOfField4PlanesData::GetModel() const
{
	return m_pModel;
}

inline HFBModel RexMbCutsceneExportDepthOfField4PlanesData::GetNearPlaneModel() const
{
	return m_pNearPlaneModel;
}

inline HFBModel RexMbCutsceneExportDepthOfField4PlanesData::GetFarPlaneModel() const
{
	return m_pFarPlaneModel;
}

inline HFBModel RexMbCutsceneExportDepthOfField4PlanesData::GetNearDOFStrengthPlaneModel() const
{
	return m_pNearDOFStrengthPlaneModel;
}

inline HFBModel RexMbCutsceneExportDepthOfField4PlanesData::GetFarDOFStrengthPlaneModel() const
{
	return m_pFarDOFStrengthPlaneModel;
}

inline float RexMbCutsceneExportDepthOfField4PlanesData::GetTranslateScaleFactor() const
{
	return m_fTranslateScaleFactor;
}

//##############################################################################

class RexMbCutsceneExportPropertyData : public RexMbCutsceneExportBoxVectorData
{
public:
    RexMbCutsceneExportPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames );

    const atString& GetPropertyName() const;

    HFBProperty GetProperty() const;

protected:
    atString m_propertyName;
    HFBProperty m_pProperty;
};

inline const atString& RexMbCutsceneExportPropertyData::GetPropertyName() const
{
    return m_propertyName;
}

inline HFBProperty RexMbCutsceneExportPropertyData::GetProperty() const
{
    return m_pProperty;
}

//##############################################################################

class RexMbCutsceneExportBoolPropertyData : public RexMbCutsceneExportPropertyData
{
public:
    RexMbCutsceneExportBoolPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames );

    virtual void ReadFrameData();

    virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

    virtual void WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo );

protected:    
    bool m_bDefaultPropertyValue;
};

//##############################################################################

class RexMbCutsceneExportFloatPropertyData : public RexMbCutsceneExportPropertyData
{
public:
    RexMbCutsceneExportFloatPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames, bool bWholeNumber = false );

    virtual void ReadFrameData();

    virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

    virtual void WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo );

protected:
    float m_fDefaultPropertyValue;
	bool m_bWholeNumber;
};

//##############################################################################

class RexMbCutsceneExportIntegerPropertyData : public RexMbCutsceneExportPropertyData
{
public:
	RexMbCutsceneExportIntegerPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames );

	virtual void ReadFrameData();

	virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

	virtual void WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo );

protected:
	int m_iDefaultPropertyValue;
};

//##############################################################################

class RexMbCutsceneExportVectorPropertyData : public RexMbCutsceneExportPropertyData
{
public:
    RexMbCutsceneExportVectorPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames );

    virtual void ReadFrameData();

    virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

    virtual void WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo );

protected:
    FBVector3d m_vDefaultPropertyValue;
};

//##############################################################################

} // namespace rage

#endif //__REXMBRAGE_CUTSCENE_EXPORT_MODEL_H__
