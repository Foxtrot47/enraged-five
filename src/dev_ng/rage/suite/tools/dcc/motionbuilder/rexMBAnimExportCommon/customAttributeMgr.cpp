#include "customAttributeMgr.h"

#include "configParser/configGameView.h"
#include "configParser/configExportView.h"
using namespace configParser;

static ConfigGameView gs_GameView;

void CustomAttributeMgr::LoadAttributeFile(atString filename)
{
	if(filename == "") return;

	char czAssetDir[MAX_PATH];
	gs_GameView.GetAssetsDir(czAssetDir, MAX_PATH);
	atString strExportDir(czAssetDir);
	strExportDir.Lowercase();
	strExportDir.Replace("/", "\\");

	filename.Replace("$(assets)", czAssetDir);

	std::fstream file_op(filename.c_str(), std::ios::in);
	if(file_op.is_open())
	{
		while(!file_op.eof()) 
		{
			char str[MAX_PATH*2];
			file_op.getline(str,MAX_PATH*2);

			std::vector<atString> vec;
			SplitString(str, vec);

			if(vec.size() != ATTRIBUTE_LINE_ENTRIES) continue;

			m_mapAttributes[vec[0]] = vec[1];
		}         

		file_op.close();
	}
}

bool CustomAttributeMgr::FindAttribute(atString find, atString& second)
{
	std::map<atString, atString>::iterator it = m_mapAttributes.find(find);
	if(it == m_mapAttributes.end())
	{
		return false;
	}

	second = it->second;
	return true;
}

void CustomAttributeMgr::SplitString(const char* str, std::vector<atString>& vec)
{
	char* pch = strtok ((char*)str,",");
	while (pch != NULL)
	{
		atString p(pch);
		vec.push_back(p);
		pch = strtok (NULL, ",");
	}
}

