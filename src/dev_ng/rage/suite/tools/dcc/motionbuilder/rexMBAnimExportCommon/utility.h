#ifndef __REXMBANIMEXPORTCOMMON_UTILITY_H__
#define __REXMBANIMEXPORTCOMMON_UTILITY_H__

#include "atl/functor.h"
#include "atl/string.h"

#include <map>
#include <string>
#include "vector/matrix34.h"

#pragma warning(push)
#pragma warning(disable:4263)
#pragma warning(disable:4265)

#include "cutfile/cutfile2.h"
#include "UserObjects/OpenRealitySDK.h"
#include "file/device.h"

#include "cutfile/cutfeventdef.h"

#pragma warning(pop)

#pragma warning(disable:4481)

using namespace rage;
using namespace std;

class RexAnimMarkupData;

#define PROFILE_ANIM_EXPORT	0
#define PROFILE_CUTSCENE_EXPORT 0
#define USE_NEW_ANIM_PIPELINE 1
#define ASSET_PIPELINE_3 1
#define CUTSCENE_ASSET_PIPELINE_3 1

#define USE_NEW_BLOCK_TAGS 1

//NOTE:  Between 3D Studio Max and MotionBuilder, the scale differs by 100 (meters, centimeters, respective).
#define MOTIONBUILDER_UNIT_SCALE 1.0f / 100.0f

#define BLOCK_TAG_PAD 0.001f

static const char* DefaultSkelFile = "(no skel file)";

enum ESectioningMethod
{
	NO_SECTIONING_METHOD,
	TIME_SLICE_SECTIONING_METHOD,
	CAMERA_CUT_SECTIONING_METHOD,
	SECTION_SPLIT_PROPERTY_SECTIONING_METHOD,
	NUM_SECTIONING_METHODS
};

struct SFaceDefaults
{
public:
	atString suffix;
	atString attributes;
	atString spec;
};

static const float TIME_TO_FRAME_TOLERANCE = 0.001f;
static const int CUTSCENE_EXPORTER = 0;
static const int INGAME_EXPORTER = 1;

int ExecuteCommand(const char* commandline, const char* currentdir, atString& strOutput, const char* pipeinput=NULL);
HFBModel RAGEFindModelByName(const char* pModelName);
bool RecurseFindBone(HFBModel bone, atString bonename);
void CB_FindFirstFileAll(const rage::fiFindData &data, void *userArg);

//##############################################################################

// PURPOSE: An FBSpread class with a method to refresh itself
class RageFBSpread : public FBSpread
{
public:
	void Refresh( bool pNow=false );
};

//##############################################################################


enum CheckboxState
{
	CHECKBOX_UNCHECKED = 0,
	CHECKBOX_CHECKED,
};

struct SCameraCutKeyData
{
	float fTime;
	int iCameraIndex;
	char cName[32];
};

struct BoneEntry
{
    char* c_boneName;
    char* c_boneID;
};

struct OverlayEntry
{
	int iStart;
	int iEnd;
	bool bStart;
};

struct ScreenFadeEntry
{
	float fStart;
	float fEnd;
	bool bFadeOut;
	Color32 color;
};

struct BoneOffsetEntry
{
	atString boneName;
	float fOffet;
};

struct Version
{
	Version(int major, int minor, int revision)
	{
		Major = major;
		Minor = minor;
		Revision = revision;
	}
	int Major;
	int Minor;
	int Revision;
};

class AetAdditionalModel
{
public:
	AetAdditionalModel() { }

	string m_modelName;
	string m_trackType;
	string m_attributeName;

	bool operator==(const AetAdditionalModel &b)
	{
		if(m_modelName == b.m_modelName &&
			m_trackType == b.m_trackType &&
			m_attributeName == b.m_attributeName)
			return true;

		return false;
	}
};

static const Version s_cutscene_exporter_version = Version(2, 2, 54);
static const Version s_ingame_exporter_version = Version(1, 0, 62);

namespace rage
{
    class cutfCutsceneFile2;
    class cutfParticleEffectObject;
    class fiStream; 
	struct SCameraCutData;
}

namespace rexMBAnimExportCommon
{
    // PURPOSE: The standard space around the edges of the popup and between controls
    //    on the same line.
    const int c_iMargin = 5;

    // PURPOSE: The standard width of a label, when it is attached to the left side
    //    and precedes an editable field (TextBox, ComboBox, etc.)
    const int c_iLabelWidth = 75;

    // PURPOSE: The standard width of a short label
    const int c_iShortLabelWidth = 50;

	// PURPOSE: The standard width of a medium label
	const int c_iMediumLabelWidth = 85;

    // PURPOSE: The standard width of a long label
    const int c_iLongLabelWidth = 100;

    // PURPOSE: The standard width of a button.
    const int c_iButtonWidth = 60;

	// PURPOSE: The standard width of a button.
	const int c_iCheckBoxWidth = 65;

    // PURPOSE: The standard width of a wide button.
    const int c_iWideButtonWidth = 125;

	// PURPOSE: The standard width of a wide button.
	const int c_iSpecialButtonWidth = 225;

    // PURPOSE: The standard width of the FBColorEdit control
    const int c_iColorWidth = 200;

    const int c_iNumericUpDownWidth = 50;

    // PURPOSE: The height of TextBoxes, ComboBoxes, Labels, etc.
    const int c_iFieldHeight = 16;

    // PURPOSE: The height of a row
    const int c_iRowHeight = 20;

	void		CreateObjectInterchange();

	void		KillObjectInterchange();

	//PURPOSE : Retrieves the start time of the current take.
	float		GetAnimStartTime();

	//PURPOSE : Retrieves the end time of the current take.
	float		GetAnimEndTime();

	//PURPOSE : Retrieves the start time of the current shot track.
	float		GetSceneStartTime();

	//PURPOSE : Retrieves the end time of the current shot track.
	float		GetSceneEndTime();

	//PURPOSE : Retrieves the FPS value (currently hardcoded at 30FPS)
	float		GetFPS();

	//PURPOSE : Converts the supplied FBTime object to a seconds-based value
	float		GetSeconds(FBTime timeIn);

	bool		CheckFPS();

	char*		CreateLogFileName(const char* pLogFile, const char* pSceneName);

	void		ClearLogPathContents(const char *pLogDir );

	atString	GetDefaultLogDir();

	void		DeselectAllModels();

    // PURPOSE: Converts the supplied FBTime object to a frame-based value.
    int         GetFrame( FBTime timeIn );

	// PURPOSE: Finds and returns the model that goes with the given object.
	// PARAMS:
	//    pObject - the object to find the model for
	// RETURNS: NULL, if it isn't found
	HFBModel	FindModelForObject( const cutfObject *pObject );

	// PURPOSE: Updates data in the object from items, attributes, and/or models
	//    found in the scene.
	// PARAMS:
	//    pObject - the object to update
	void		LoadObjectData( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Updates the items, attributes, and/or models found in the scene from
	//    the object.
	// PARAMS:
	//    pObject - the object to update
	void		SaveObjectData( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Retrieves the list of camera cuts data from the Camera Switcher.
	// PARAMS:
	//    cameraCutDataList - out parameter for the cut data.
	void		GetCameraCutData( atArray<SCameraCutData> &cameraCutDataList, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Retrieves the key times of the model's SectionSplit property.
	// PARAMS:
	//    pCamera - the model with the SectionSplit property
	//    sectionSplitList - out parameter for the split times
	void		GetSectionSplitTimes( HFBModel pCamera, atArray<float> &sectionSplitList );

	// PURPOSE: Retrieves the names of animation nodes starting with "Overlay" for the given model.
	// PARAMS:
	//    pCameraModel - the model containing the overlays
	//    overlayNameList - out parameter for the names that were found
	// RETURNS: true if one or more were found, otherwise false
	// NOTES:
	bool		GetOverlayNames( HFBModel pCameraModel, atArray<ConstString> &overlayNameList );

	// PURPOSE: Retrieves the list of OverlayEntries for the given model.
	// PARAMS:
	//    pCameraModel - the model containing the overlays
	//    pOverlayName - the name of the overlay (animation node name)
	//    overlayFrames - out parameter for the OverlayEntry structs.
	// RETURNS: true if one or more were found, otherwise false
	bool		GetOverlayFrames( HFBModel pCameraModel, const char* pOverlayName, atArray<OverlayEntry>& overlayFrames, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Retrieves the list of ScreenFadeEntries for the given model.
	// PARAMS:
	//    pCameraModel - the model containing the screen fades
	//    screenFadeFrames - out parameter for the ScreenFadeEntry structs.
	// RETURNS: true if one or more were found, otherwise false
	bool		GetScreenFadeFrames( HFBModel pModel, atArray<ScreenFadeEntry>& screenFadeFrames, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Acquire the start frame of the animation.
	int			GetStartFrame();

	// PURPOSE: Acquire the end frame of the animation.
	int			GetEndFrame();

    // PURPOSE: Converts the supplied seconds into an FBTime object.
    // PARAMS:
    //    seconds - the time to convert
    // RETURNS: FBTime object
    FBTime GetTime( float seconds );

	//PURPOSE : Retrieves the animation export markup file path for the currently loaded file.
	//Returns an empty string if no file is currently loaded into MotionBuilder
	std::string		GetAnimExportMarkupFilePath();

    //PURPOSE : Retrieves the cut scene export markup file path for the currently loaded file.
    //Returns an empty string if no file is currently loaded into MotionBuilder
	std::string      GetCutsceneExportMarkupFilePath();

	bool		GetAssetPath(atString& assetPath);

	bool		GetArtPath(atString& artPath);

    // PURPOSE: Modifies the text by replacing all whitespace characters with underscores.
    // PARAMS:
    //    pText = the string to modify
    // RETURNS: The string that was passed in
    char* ReplaceWhitespaceCharacters( char *pText );
	atString ReplaceWhitespaceCharacters( atString& atText );

    // PURPOSE: Retrieves the hashed bone id for the given model node.
    // PARAMS:
    //    pModel - the model node
    //    bTranslate - true to translate using the spec file, otherwise false
    // RETURNS: The hashed bone id
    atString GetBoneID( HFBModel pModel, bool bTranslate );

	std::map<std::string, std::string>& GetAdditionalModelTrackTypes();

	atString GetNameSpaceFromModel( HFBModel pModel );

	atArray<SFaceDefaults>& GetCutsceneFaceDefaults();

	atArray<SFaceDefaults>& GetAnimFaceDefaults();

	atString GetBoneNameFromModel( HFBModel pModel );

    // PURPOSE: Reads one line of text from the file stream, up to the length of the buffer.
    // PARAMS:
    //    pStream - the file stream to read from
    //    pDest - the destination buffer
    //    iDestLen - the length of the buffer
    // RETURNS: false if nothing was saved to the buffer, otherwise true
    bool ReadLine( fiStream* pStream, char* pDest, s32 iDestLen );

    // PURPOSE: Writes one line to the file stream, appending the newline character for you.
    // PARAMS:
    //    pStream - the file stream to write to
    //    pText - the text to write
    // RETURNS:
    // NOTES:
    bool WriteLine( fiStream* pStream, const char* pText );

    // PURPOSE: Loads the default xml bone translation file for the given type.  The default xml
    //    file is given by "MotionBuilderConfigFilePath" in the module settings file.
    // PARAMS:
    //    pExportType - The export type.  Valid types are "Ped", "Vehicle", "Prop", and "All".
    // RETURNS: true on success, otherwise false
    bool LoadBoneTranslationConfig( const char* pExportType );

    // PURPOSE: Loads the xml bone translation file for the given type.
    // PARAMS:
    //    pExportType - The export type.  Valid types are "Ped", "Vehicle", "Prop", and "All".
    // RETURNS: true on success, otherwise false
    // NOTES: This doesn't currently deal with duplicate entries.
    bool LoadBoneTranslationXmlConfigFile( const char* pFilename, const char* pExportType );

	// PURPOSE: Loads the xml bone file for the given skeleton.
	// PARAMS:
	//    pSkeleton - The skeleton name - i.e. "player.skel".
	//	  atBones - Array containing the bones listed for the specified skeleton.
	// RETURNS: true on success, otherwise false
	bool LoadBoneValidationConfigFile( const char* pFilename, const char* pSkeleton, atArray<atString>& atBones, atArray<bool>& atEnforce );

	// PURPOSE: Loads the xml file with all the unused compression templates.
	// PARAMS:
	//	  atInvalidTemplates - Array containing all the compression templates that we shouldn't be exporting with.
	// RETURNS: true on success, otherwise false
	bool LoadCompressionTemplateValidationConfigFile( const char* pFilename, atArray<atString>& atUnusedTemplates);

	// PURPOSE: Loads the xml bone offset file.
	// RETURNS: true on success, otherwise false
	bool LoadBoneOffsetValidationConfigFile( );

	// PURPOSE: Loads the xml additional model track type file.
	// RETURNS: true on success, otherwise false
	bool LoadAdditionalModelConfigFile();

	// PURPOSE: Loads the xml face defaults file.
	// RETURNS: true on success, otherwise false
	bool LoadFaceSettingsConfigFile();

	// PURPOSE: Validates if specified bones exist in the exported characters.
	// PARAMS:
	//    pExportData - The export data.  Contains all the export data for the characters to validate against.
	// RETURNS: true on success, otherwise false
	// NOTES: This doesn't currently deal with duplicate entries.
	bool ValidateBones(RexAnimMarkupData* pExportData);

	// PURPOSE: Validates if specified bones offset is out of bounds of validation.
	// PARAMS:
	//    pBone - The bone to check against the config
	//	  translation - The bones translation vector to check versus the offset validation.
	// RETURNS: true on success, otherwise false
	bool ValidateBoneOffset( const char* pBone, Vector3 translation );

	// PURPOSE: Validates if all the data has valid compression templates applied.
	// PARAMS:
	//    pExportData - The export data.  Contains all the export data for the characters to validate against.
	// RETURNS: true on success, otherwise false
	bool ValidateCompressionTemplates(RexAnimMarkupData* pExportData);

	bool ValidateIKRoot(RexAnimMarkupData* pExportData);

    // PURPOSE: Deletes all files in the given directory
    // PARAMS:
    //    pPath - the directory path
    // RETURNS: The number of files deleted.
    int DeleteFiles( const char* pPath, bool bForceReadOnlyDelete=false );

    // PURPOSE: Converts the given FBMatrix matrix into a Matrix34.
    // PARAMS:
    //    mbMatrix - the motion builder matrix
    //    rageMatrix - the rage matrix
    void ConvertToRageMaxtrixFromMotionBuilder( FBMatrix& mbMatrix, Matrix34& rageMatrix );

    // PURPOSE: Locates the model that the given model is constrained to.
    // PARAMS:
    //    pModel - the model to find the constraint for
    // RETURNS: the constraining model
    HFBModel GetConstraintObject( HFBModel pModel );

    // PURPOSE: Splits the give string into pieces separated by the delimiter.
    // PARAMS:
    //    pString - the string to tokenize
    //    pDelimiter - the delimiter
    //    tokens - the list of string parts
    // NOTES: This operation is destructive to the original string.
    void TokenizeString( char* pString, char* pDelimiter, atArray<char*>& tokens );

	atString ArrayToString( const atArray<atString>& atObj, atString strSep);

    // PURPOSE: Creates a shader of the type and with the name specified.
    // PARAMS:
    //    pShaderTypeName - the type name
    //    pShaderName - the name
    // RETURNS: A motion builder shader object.  NULL if one could not be created.
    HFBShader CreateShader( char* pShaderTypeName, char* pShaderName );

    // PURPOSE: Locates the animation node with the given name by examining the node
    //    passed in and its children.
    // PARAMS:
    //    pFindNode - the node to start looking at
    //    pName - the name to look for
    // RETURNS: The animation node with the given name.  NULL if not found.
    HFBAnimationNode FindAnimByName( HFBAnimationNode pFindNode, const char* pName );

    // PURPOSE: Locates the animation node whose name starts with the specified string. Examining the node
    //    passed in and its children.
    // PARAMS:
    //    pFindNode - the node to start looking at
    //    pStartsWith - the name to look for
    //    animList - out parameter for the list of animation nodes that were found
    void FindAnimsBeginningWith( HFBAnimationNode pFindNode, const char* pStartsWith, atArray<HFBAnimationNode> &animList );

	// PURPOSE: Locates the animation node whose name ends with the specified string. Examining the node
	//    passed in and its children.
	// PARAMS:
	//    pFindNode - the node to start looking at
	//    pEndsWith - the name to look for
	//    animList - out parameter for the list of animation nodes that were found
	void FindAnimsEndingWith( HFBAnimationNode pFindNode, const char* pEndsWith, atArray<HFBAnimationNode> &animList );

    // PURPOSE: Determines the "MotionBuilderRmptfxPath" setting specified in the module settings file.
    //    If the value could not be retrieved, the default is "%RAGE_ASSET_ROOT%\tune\rmptfx".
    // PARAMS:
    //    rmptfxPath - out parameter for the path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderRmptfxPathSetting( atString &rmptfxPath, bool subst);

	// PURPOSE: Determines the "MotionBuilderPostRenderScript" setting specified in the module settings file.
	// PARAMS:
	//    postRenderScript - out parameter for the path
	// RETURNS: true if the path could be determined, otherwise false.
	bool GetAnimationPostRenderScript( atString &postRenderScript );

    // PURPOSE: Determines the "MotionBuilderProjectConfigs" setting specified in the module settings file.
    //    If the value could not be retrieved, the default is "RAGE".
    // PARAMS:
    //    configs - out parameter for the list of config names
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderProjectConfigsSetting( atArray<atString> &configs );

	// PURPOSE: Determines the stream folder (ie streamGTA5).
	// PARAMS:
	//    networkPath - out parameter for the network stream folder
	// RETURNS: true if the path could be determined, otherwise false.
	bool GetMotionBuilderNetworkStreamSetting( atString &networkPath );

    // PURPOSE: Determines the "MotionBuilderCutsceneExportRoot" setting specified in the module settings file.
    //    If the value could not be retrieved, the default is "T:/$(config)/assets/cuts".
    // PARAMS:
    //    exportPath - out parameter for the export path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderCutsceneExportRoot( atString &exportPath );

    // PURPOSE: Determines the "MotionBuilderCutsceneFaceExportPath" setting specified in the module settings file.
    //    If the value could not be retrieved, the default is "T:/$(config)/assets/cuts/$(scene)/faces".
    // PARAMS:
    //    faceExportPath - out parameter for the face export path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderCutsceneFaceExportPath( atString &faceExportPath );

    // PURPOSE: Determines the "MotionBuilderCutsceneBackupPath" setting specified in the module settings file.
    //    If the value could not be retrieved, the default is "MotionBuilderCutsceneExportRoot".
    // PARAMS:
    //    backupPath - out parameter for the backup path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderCutsceneBackupPath( atString &backupPath );

    // PURPOSE: Determines the "MotionBuilderAnimCtrlFilePath" setting specified in the module settings file.
    //    If the value could not be retrieved, the default is "%RAGE_ROOT_FOLDER%\tools\motionbuilder\config".
    // PARAMS:
    //    animCtrlFilePath - out parameter for the path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderAnimCtrlFilePathSetting( atString &animCtrlFilePath );

	// PURPOSE: Determines the "GetMotionBuilderAnimCompressionFilePathSetting" setting specified in the module settings file.
	//    If the value could not be retrieved, the default is "%RAGE_ROOT_FOLDER%\tools\motionbuilder\config".
	// PARAMS:
	//    animCompressionFilePath - out parameter for the path
	// RETURNS: true if the path could be determined, otherwise false.
	bool GetMotionBuilderAnimCompressionFilePathSetting( atString &animCompressionFilePath );

    // PURPOSE: Determines the "MotionBuilderEventDefsPath" setting specified in the module settings file.
    //    If the value could not be retrieved, the default is "%RAGE_ROOT_FOLDER%\tools\motionbuilder\config\eventDefs.xml"
    // PARAMS:
    //    eventDefsPath - out parameter for the event defs file path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderEventDefsPathSetting( atString &eventDefsPath );

    // PURPOSE: Determines the "MotionBuilderFxListCutsceneSectionName" setting used to identify the which section of particle effects in
    //    the .fxlist file should be available to be placed in the scene.  If the value cannot be retrieved, the default is "CUTSCENES".
    //    It will be a case-insensitive comparison.  End-of-file will also end the section, obviously.  Set this to "all" to include
    //    particles from all sections.
    // PARAMS:
    //    cutsceneSectionName - out parameter for the token
    // RETURNS: true if the token could be determined, otherwise false.
    bool GetMotionBuilderFxListCutsceneSectionName( atString &cutsceneSectionName );

	// PURPOSE: Determines the "MotionBuilderSkelFilePath" setting specified in the module settings file.
    // PARAMS:
    //    skelFilePath - out parameter for the path
    // RETURNS: true if the path could be determined, otherwise false.
	bool GetMotionBuilderSkelFilePathSetting( atString &skelFilePath );

	// PURPOSE: Determines the "MotionBuilderStreamDirectory" setting specified in the module settings file.
    // PARAMS:
    //    streamPath - out parameter for the path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderStreamPathSetting( atString &streamPath );

	// PURPOSE: Determines the "MotionBuilderRootAssetPath" setting specified in the module settings file.
    // PARAMS:
    //    rootAssetPath - out parameter for the path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderRootAssetPathSetting( atArray<atString> &rootAssetPaths );

	// PURPOSE: Determines the "MotionBuilderRootCompressedAssetPath" setting specified in the module settings file.
    // PARAMS:
    //    rootCompressedAssetPath - out parameter for the path
    // RETURNS: true if the path could be determined, otherwise false.
    bool GetMotionBuilderRootCompressedAssetPathSetting( atString &rootCompressedAssetPath );

	// PURPOSE: Determines the "MotionBuilderContentRootPath" setting specified in the module settings file.
	// PARAMS:
	//    contentRootPath - out parameter for the path
	// RETURNS: true if the path could be determined, otherwise false.
	bool GetMotionBuilderContentRootPathSetting( atString &contentRootPath );

    // PURPOSE: Creates a new particle effect placeholder model
    // PARAMS:
    //    pName - the name of the effect
    // RETURNS: A pointer to the newly created model, NULL on failure.
    HFBModelNull CreateParticleEffect( const char* pName );

	// PURPOSE: Creates a new decal effect placeholder model
	// PARAMS:
	//    pName - the name of the decal effect
	// RETURNS: A pointer to the newly created model, NULL on failure.
	HFBModel CreateDecal( const char* pName );

    // PURPOSE: Adds the init and evolution properties to the particle effect model
    // PARAMS:
    //    particleEffectModel - the model to add the properties to
    //    pRmptfxPath - the path to the rmptfx folder, underneath which lives the effectrules
    //    pName - the name of the effect
    // RETURNS: true if the evolution parameters were loaded, otherwise false.
    bool AddParticleEffectProperties( HFBModel particleEffectModel, const char* pRmptfxPath, const char* pName, const char* pListName );

	// PURPOSE: Adds the init properties to the decal effect model
	// PARAMS:
	//    decalModel - the model to add the properties to
	void AddDecalProperties( HFBModel decalModel );

    // PURPOSE: Retrieves the names of the effect's evolution parameters, if any
    // PARAMS:
    //    pEffectRuleFilename - the name of the effectrule file (doesn't need the extension)
    //    evoParamNameList - the list of names to fill
    // RETURNS: returns false if the file could not be loaded, otherwise true
    bool GetEvoParamNames( const char *pEffectRuleFilename, atArray<ConstString> &evoParamNameList );

	// PURPOSE: Retrieves the state of the effect loop property
	// PARAMS:
	//    pEffectRuleFilename - the name of the effectrule file (doesn't need the extension)
	// RETURNS: returns false if the file could not be loaded or if the effect is not looped, otherwise true
	bool IsEffectLooped( const char *pEffectRuleFilename );

    // PURPOSE: Gets a list of anim control specification ilenames
    // PARAMS:
    //    specFilenames - the spec list to fill
    // RETURNS: true if GetMotionBuilderAnimCtrlFilePathSetting was able to return a path, otherwise false.
    bool FindAnimCtrlFilePathFiles( atArray<atString> &specFilenames );

	// PURPOSE: Gets a list of anim compression filenames
	// PARAMS:
	//    compressfilenames - the compress list to fill
	// RETURNS: true if FindAnimCompressionFilePathFiles was able to return a path, otherwise false.
	bool FindAnimCompressionFilePathFiles( atArray<atString> &compressFilenames );

	// PURPOSE: Gets a list of anim control specification and anim compress filenames
    // PARAMS:
    //    skelFilenames - the skel list to fill
    //
    // RETURNS: true if GetMotionBuilderAnimSkelFilePathSetting was able to return a path, otherwise false.
	bool FindSkelFilePathFiles( atArray<atString> &skelFilenames );

	// PURPOSE: Turns an array of strings into a ~ delimited string for populating a combo box which
	// is embedded in a spread
     // PARAMS:
    //    filenames - list of strings that will be sue to populate the spread combobox
    //    comboBoxString - target concatented delimited string
	void BuildSpreadSheetComboBoxStringFromArray( atArray<atString> &filenames, atString &comboBoxString );

	atString ResolveComboValueFromDelimitedString( int idx, atString &comboBoxString );

	int ResolveComboIndexFromString( atString val, atString &comboBoxString );

	// PURPOSE:	Get the TOOLSPROJECT env variable
	//
	atString GetProjectName();

	// PURPOSE:	Get the PROJROOT env variable
	//
	atString GetProjectRootDir();

	// PURPOSE:	Gateway to the content system for building the independent clip dictionaries
	//
	//
	bool BuildAnimationDictionariesUsingContentSystem( const char* pRootSrcDir, const char* pAnimSrcDir, const char* pSceneName, const char* pRelativeOutputPath, const char* pStreamSubDir, bool bCutscene=false, bool bPreview=false );

	// PURPOSE:	Gateway to the content system for building the clip image
	//
	//
	bool BuildImageUsingContentSystem( bool bRebuild, const char* pSrcDir, const char* pImageName, const char* pExportDir, bool bCutscene );

	// PURPOSE: Segments the animations for the given scene and builds one more
	//    animation dictionaries in the given destination directory.
	// PARAMS:
	//    pSrcDir - the directory where the animations live
	//    pDestDir - the directory to place the animation dictionaries in.
	//    iSectioningMethod - the sectioning method to use
	//    fSectionByTimeSliceDuration - the time slice duration
	// RETURNS: true on success, otherwise false
	bool BuildIndependantData( const char* pSceneName, const char* pSrcDir, const char* pDestDir=NULL, s32 iSectioningMethod=-1, float fSectionByTimeSliceDuration=0.0f, bool bBuildCutOnly=false );

	// PURPOSE:	Gateway to ruby pipeline for building a clip rpf
	//
	//
	bool BuildRPF( bool bRebuild, const char* pDictName, const char* pRelativeDir );

	// PURPOSE:	Gateway to ruby pipeline for building a clip rpf
	//
	//
	bool BuildAllRPF( bool bRebuild, const char* pSrcDir );

	// PURPOSE:	Gateway to ruby pipeline for building a dictionary
	//
	//
	bool BuildDictionary( const char* pSrcDir, const char* pRootDir, const char* pDictName );

	// PURPOSE:	Build in game zips for the target RPF
	//
	//
	bool BuildIngameZips( const char* pRootdir );

	// PURPOSE:	Build in game zips for all target RPFs
	//
	//
	bool BuildAllIngameZips( );

	// PURPOSE:	Gateway to ruby pipeline for previewing a dictionary
	//
	//
	bool PreviewDictionary( const char* pDictName, const char* pRootDir );

	// PURPOSE:	Check label status
	//
	//
	bool CheckToolsLabelStatus( );

	// PURPOSE:	When in cutscene story mode, there may be sections of frames which need to be discarded
	//
	//
	void CalculateDiscardedFrames(bool bStoryCheck = true);

	// PURPOSE:	Check if a frame number for the scene needs to be discarded
	//
	// RETURNS: true if discarded, false if valid
	bool DiscardFrame(int iFrame);

	// PURPOSE:	Get the number of frames in total which are discarded
	//
	// RETURNS: the number of frames discarded
	int GetNumberOfDiscardedFrames();

	bool ValidateAndCreateStoryTrack(cutfCutsceneFile2* pCutFile, bool bCreate, int iStartFrame, int iEndFrame, const char* pSceneName);

	int GetFrameAfterImport(int iFrame, const atArray<int>& atDiscard);

	int GetFrameAfterDiscardedFrames(const int iCurrentFrame);

	atArray<int>& GetDiscardedFramesArray();

	void SetStoryMode(bool bStoryMode);

	float GetStoryTrackStartTime();

	float GetStoryTrackEndTime();

	int GetStoryTrackRealRangeEnd();

	// PURPOSE:	Get the status of story mode
	//
	// RETURNS: boolean status if story mode is on
	bool GetStoryMode();

	void SetActivePart(const atString& atActivePart);

	const char* GetActivePart();

	void SetUsePerforceIntegration(bool bPerforceIntegration);
	bool GetPerforceIntegration();

	void SetUseClipXml(bool bClip);
	bool GetUseClipXml();

	void SetUseWarnIfCheckedOut(bool bWarning);
	bool GetUseWarnIfCheckedOut();

	void SetExporterMode(int iMode);
	int GetExporterMode();

	void GetDlcProjects(atMap<atString, atString>& atProjects);
	atString GetProjectOrDlcAssetsDir(atString strSource);
	atString GetProjectOrDlcExportDir(atString strSource);
	atString GetProjectOrDlcArtDir(atString strSource);
	atString GetProjectOrDlcRootDir(atString strSource);

	atString GetProjectOrDlcIngameExportDir(atString strSource);
	atString GetProjectOrDlcCutsceneExportDir(atString strSource);

	void GetProjectOrDlcIngameSourceExportDir(atArray<atString>& rootAssetPaths, atString strSource);
	
	bool IsDlc(atString strSource);

	const char* GetFBXFileName();

	// PURPOSE:	Check if the file is a cutscene light file - loads the data.lightxml and validates
	//
	// RETURNS: false if discarded, true is valid
	bool IsLightFile(const char* pExportDir, const char* pFileName);

	bool GetNewOrigin(atString& atOrigin);

	void SetNewOrigin(bool bOrigin, atString atOrigin);

	// PURPOSE:	Get the default selection set templates
	//
	// RETURNS: the value, empty string if unsuccessful.
	bool GetDefaultControlFile( atString& ctrlFile);
	bool GetDefaultSkeletonFile( atString& skelFile );
	bool GetDefaultCompressionFile( atString& compressionFile );

	void LookAt(const Vector3 &to, Matrix34 &mat, const Vector3& up = YAXIS);

	// PURPOSE: Resolve skeleton type fromt he skeleton filename.
	int ResolveSkeletonType( const char* pSkelFile );

	// PURPOSE: Get local computer name.  no shit.
	atString GetLocalComputerName();

	// PURPOSE: Get user name
	atString GetLocalUserName();

	// PURPOSE: Acquires an environment variable.  Warns if it does not exist.
	bool GetEnvironmentVariable(const char* envVar, char* envValue, int size);

    // PURPOSE: Divides the input string into chunks of the given length.
    // PARAMS:
    //    pInput - the input string
    //    size - the maximum size of each string (including the null-terminator)
    //    output - the output list of strings
    void SplitStringByLength( const char *pInput, int size, atArray<atString> &output );

    // PURPOSE: Merges the given list of strings into one long string
    // PARAMS:
    //    input - the input list of strings
    // RETURNS: The merged string or NULL on error.  Delete this manually when finished.
    const char* ConcatenateStrings( const atArray<atString> &input, const char* pDelimiter=NULL );

	// PURPOSE: Scans anim markup for a character of type MODEL_CAMERA_TYPE
	// RETURNS: true if the scene has a camera in it
	bool HasCamera( const RexAnimMarkupData* pAnimData );

	// PURPOSE: Retrieves the list of camera cuts times from the Camera Switcher.
	// PARAMS:
	//    cameraCutList - out parameter for the cut times.
	void GetCameraCutTimes( atArray<float> &cameraCutList );
	void GetCameraCutKeys( atArray<SCameraCutKeyData> &cameraCutKeyData );
	bool GetCameraCutShots( atArray<SCameraCutKeyData> &cameraCutKeyDataList );

	// PURPOSE: Set the BaseLayer on every take, this is to fix any possible issues of artists leaving a new layer selected
	void SetBaseLayerOnAllTakes();

	void GetDiscardFrameArrayAsString( atString& str );

	int GetFrameFromTime(float fTime);

	bool IsRagEvent(int iEventId);

	bool IsFileUnderProjectRoot(const char* pPath);
	bool IsFileUnderArtRoot(const char* pPath);

	bool CheckWritableRPF();
	void ReadTargets(atArray<atString>& atTargets);

	float GetValueFromFCurve(HFBAnimationNode pNode);

	// PURPOSE: Sets the face animation data on the specified object
	// PARAMS:
	//    pPedModelObj - ped model object to set data on
	//	  czNode - face node name
	void SetFaceAnimationNode(cutfPedModelObject *pPedModelObj, const char* czNode);

	// PURPOSE: Find face node from defaults 
	// PARAMS:
	//    pObject - object associated with face node
	//	  faceNodeName - out face node name if found
	// RETURNS: True if the face node is found, false if not
	bool FindFaceNode(cutfObject* pObject, atString& faceNodeName);

	// PURPOSE: Adds the face node (if found) to the object properties
	// PARAMS:
	//    pObject - object associated with face node
	void FindAndAddFaceNode(cutfObject* pObject);

	enum MessageBoxResult
	{
		RESULT_NONE = 0,
		RESULT_YES,
		RESULT_NO
	};

	// PURPOSE: An easier way to prompt a standard "Yes/No" message box.
	// PARAMS:
	//	char * title
	//	char * message
	// RETURN:	MessageBoxResult
	MessageBoxResult ShowYesNoMessageBox(char* title, char* message);

	//PURPOSE : This method will take in a string name of a take, and locate
	//the actual structure for that take in the current scene.
	bool FindTake(const char* szTakeName, HFBTake& retTake);

	//PURPOSE : Determines if the specified take still exists in the scene.
	bool TakeExists(const HFBTake take);

	// These enums come from the game (Jimmy/GTA) and
	// should really be moved
	//PURPOSE : For indexing events
	enum EClipEventType
	{
		AT_EVENT = 128,
		// animated mover track event
		AT_CAMERA_EVENT,
		// ambient anim flags
		AT_AMBIENT_FLAGS,
		// audio hash keys
		AT_AUDIO,
		// gesture speaker anim hash keys
		AT_GESTURE_SPEAKER,
		// gesture listener anim hash keys
		AT_GESTURE_LISTENER,
		// facial speaker anim hash keys
		AT_FACIAL_SPEAKER,
		// facial listener anim hash keys
		AT_FACIAL_LISTENER

	};

	// We need a permanent solution for this

	// Project specific flags used in crAnimation
	// Note that FLAGS_RESERVED_FOR_RAGE_USE_ONLY = 0x0000ffff
	// and that FLAGS_AVAILABLE_FOR_PROJECT_USE = 0xffff0000
	// These will be shifted 1<<16 when stored in crAnimation
	enum eInternalAnimFlag
	{
		// Tracks that extract angular velocity need their translation track to be
		// unwound so that the rotation is removed.
		// Set this flag after unwinding the rotation from the translation.
		ANIM_FLAG_ROTATION_UNWOUND			= (1<<0),

		// Set if the track has an event track
		ANIM_FLAG_EVENT_TRACK				= (1<<1),

		// Set if the track has an independent mover track
		ANIM_FLAG_INDEPENDENT_MOVER_TRACK	= (1<<2),

		// Set if the track has audio hash key track
		ANIM_FLAG_AUDIO_TRACK				= (1<<3),

		// Set if the track has a gesture speaker track
		ANIM_FLAG_GESTURE_SPEAKER_TRACK		= (1<<4),

		// Set if the track has a gesture listener track
		ANIM_FLAG_GESTURE_LISTENER_TRACK	= (1<<5),

		// Set if the track has a facial speaker track
		ANIM_FLAG_FACIAL_SPEAKER_TRACK		= (1<<6),

		// Set if the track has a facial listener track
		ANIM_FLAG_FACIAL_LISTENER_TRACK		= (1<<7),

		// Set if the track has a ambient flags track
		ANIM_FLAG_AMBIENT_FLAGS_TRACK		= (1<<8),

		ANIM_FLAG_MAX						= (1<<15)
	};


    typedef rage::Functor1<const char*>	ProgressCaptionFtor;
    typedef rage::Functor1<float>		ProgressPercentFtor;
    typedef rage::Functor1<const char*>	ProgressMessageFtor;

    inline void NullCaptionFunctor(const char* /*c*/) {};
    inline void NullPercentFunctor(float /*f*/) {};
    inline void NullMessageFunctor(const char* /*c*/) {};

}

#endif //__REXMBANIMEXPORTCOMMON_UTILITY_H__

