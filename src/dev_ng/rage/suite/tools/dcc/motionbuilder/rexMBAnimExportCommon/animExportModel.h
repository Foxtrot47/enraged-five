// /animBatchExport.h

#ifndef __ANIM_BATCH_EXPORT_H__
#define __ANIM_BATCH_EXPORT_H__

#include <string>
#include <vector>

#include "animExport.h"

#include "atl/functor.h"
#include "parser/manager.h"
#include "rexGeneric/objectAnimation.h"
#include "customAttributeMgr.h"

#include "rexMBAnimExportCommon/Logger.h"

using namespace std;
using namespace rage;

#define MODEL_TYPE_BODY "Body"
#define MODEL_TYPE_FACE "Face"
#define MODEL_TYPE_CAMERA "Camera"

//#############################################################################

#define REXMB_ANIM_DATA_VERSION	1

//#############################################################################

class RexMbAnimData;
class RexMbAnimExportData;
class RexMbAnimExportDataBase;
class RexMbAnimSegmentData;

//#############################################################################

//PURPOSE : Internal structure to be used for caching properties about various models in the scene.
//This caching system is needed since retrieving various model properties from Motion Builder is a 
//very slow operation
struct ModelPropertyCacheElement
{
    string		m_rotOrder;
    Vector3		m_preRotation;
    Vector3		m_postRotation;
};

//#############################################################################

class RexMbAnimExportModel
{
public:
    typedef vector < HFBModel > MODEL_ARRAY;

public:
    RexMbAnimExportModel( const RexAnimMarkupData *pAnimData );
    ~RexMbAnimExportModel();
  
    // PURPOSE: Sets the functor to be called when the caption of a progress indicator should be changed
    // PARAMS:
    //    ftor - the callback function
    //static void SetProgressCaptionCallback( rexMBAnimExportCommon::ProgressCaptionFtor ftor );

    // PURPOSE: Sets the functor to be called when error, warning, or informational text should be displayed.
    // PARAMS:
    //    ftor - the callback function
    //static void SetProgressMessageCallback( rexMBAnimExportCommon::ProgressMessageFtor ftor );

    // PURPOSE: Invokes the progress caption update functor
    // PARAMS:
    //    caption - new caption value
    static void SetProgressCaption( const char* pCaption );

    // PURPOSE: Invokes the progress message update functor
    // PARAMS:
    //    pMessage - the message to display
    //static void SetProgressMessage( const char* pMessage );

    //PURPOSE: Step 1 - Initializes gnrcAnimation for extracting the animation data out of motion builder, and initializes 
    //  our list of RexMbAnimExportData objects.
    //PARAMS: gnrcAnimation - resulting object to contain the results of the animation extraction	
    //        fStartTime - the time to start with
    //        fEndTime - the time to end with
    //        nFrames - the number of frame to export
    //RETURNS: true for success, false for failure
    bool Initialize( rexObjectGenericAnimation& gnrcAnimation, float fStartTime, float fEndTime, int nFrames );

    // PURPOSE: Step 2a.  Sample the data for the current frame.
    void CollectAnimData();

    // PURPOSE: Step 2b.  Read all of the keyframe data between start and end.
    // PARAMS:
    //    fStartTime - the start time
    //    fEndTime - the end time
    //    nFrames - the number of frames we are going to collect data for
    void CollectAnimData( float fStartTime, float fEndTime, int nFrames );

    //PURPOSE: Step 3 - Writes a RAGE .anim file
    //PARAMS: gnrcAnimation - resulting object to contain the results of the animation extraction	
    //RETURNS: true for success, false for failure
    bool SerializeAnimation( rexObjectGenericAnimation& gnrcAnimation, const char* outputPath);

    void Clear();

    const RexAnimMarkupData* GetAnimData() const;

	const vector<RexMbAnimExportDataBase *> GetExportDataList() { return m_exportDataList; } 

private:
    //PURPOSE : Utility function which inspects the Motion Builder scene to build up a list of models
    //that have been marked to be excluded from having animation data exported
    //PARAMS : pMbAnimData - structure containing information about what has been marked by the user for export
    void CreateNoExportModelList();

    //PURPOSE : Generates the generic animation chunk hierarchy based on the model hierarchy in Motion Builder
    //PARAMS: hModel - Motion Builder model
    //		  pParentChunk - Pointer to the current parent chunk
    //		  mbModelArray - output array which will be filled with references to all models that have been added to the
    //						 chunk hierarchy
    //		  gnrcChunkArray - Output array of generic chunks.
    //        fStartTime - the time to start with
    //        fEndTime - the time to end with
    //        nFrames - the number of frame to export
    void BuildChunkHierarchy(HFBModel hModel, rexObjectGenericAnimation::ChunkInfo* pParentChunk, vector<HFBModel>& mbModelArray,
        vector<rexObjectGenericAnimation::ChunkInfo*>& gnrcChunkArray, float fStartTime, float fEndTime, int nFrames, atString modelType,
		atArray< RexAdditionalAnimData* >& additionalModels);

    //PURPOSE : Utility function which indicates if a model should have animation data exported for it or not
    //PARAMS : hModel - Motion Builder model
    //RETURNS : true if the model should be exported, false if not
    bool IsModelOkForExport( HFBModel hModel );

    //static rexMBAnimExportCommon::ProgressCaptionFtor sm_progressCaptionFtor;
    //static rexMBAnimExportCommon::ProgressCaptionFtor sm_progressMessageFtor;

    FBSystem m_system;
    const RexAnimMarkupData* m_pAnimData;
    MODEL_ARRAY m_noExportModelArray;
    vector<RexMbAnimExportDataBase *> m_exportDataList;
	CustomAttributeMgr m_attributeMgr;

};

inline void RexMbAnimExportModel::SetProgressCaption( const char* pCaption )
{
    ULOGGER_MOBO.SetProgressCaption(pCaption);
}

//inline void RexMbAnimExportModel::SetProgressMessage( const char* pMessage )
//{
//    sm_progressMessageFtor( pMessage ); 
//}

inline const RexAnimMarkupData* RexMbAnimExportModel::GetAnimData() const
{
    return m_pAnimData;
}

class RexMbAnimExportDataBase
{
public:
	RexMbAnimExportDataBase(){};
	//RexMbAnimExportDataBase( HFBModel pModel, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
		//float fStartTime, float fEndTime, int nFrames, float fTranslateScale=1.0f ){};
	virtual ~RexMbAnimExportDataBase() {};

	virtual void ReadFrameData() = 0;

	virtual void ReadKeyData( float fStartTime, float fEndTime, int nFrames ) = 0;

	virtual HFBModel GetModel();
	virtual const ModelPropertyCacheElement& GetPropertyCache() const;
	virtual const atArray<rexObjectGenericAnimation::TrackInfo *>& GetTracks() const;

protected:
	//PURPOSE : Initializes generic animation tracks (array based input)
	//PARAMS: iStartFrame - the frame to start on
	//		  iEndFrame - the frame to end on
	//		  trackNames - array of string track types
	//		  trackIds - array of integer track types
	virtual void InitTracksInfo( float fStartTime, float fEndTime, int nFrames, const vector<string>& trackNames, const vector<int>& trackIds );

	//PURPOSE : Initializes a generic animation track
	//PARAMS : iStartFrame - the frame to start on
	//		   iEndFrame - the frame to end on
	//		   track - generic animation track to be initialized
	//RETURNS: true for success, false for failure
	virtual bool InitTrackInfo( float fStartTime, float fEndTime, int nFrames, rexObjectGenericAnimation::TrackInfo& track );

	//PURPOSE : Performs the actual sampling of animation data
	//PARAMS: track - generic track structure to fill
	//        m - the matrix to fill the track with
	virtual void GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 &m ) = 0;

	//PURPOSE: Gather properties from a model and add them to the cache
	virtual void AddModelPropertiesToCache();

	HFBModel m_pModel;
	float m_fTranslateScale;
	atArray<rexObjectGenericAnimation::TrackInfo *> /*&*/m_tracks;
	ModelPropertyCacheElement m_propertyCache;
};

inline HFBModel RexMbAnimExportDataBase::GetModel()
{
	return m_pModel;
}

inline const ModelPropertyCacheElement& RexMbAnimExportDataBase::GetPropertyCache() const
{
	return m_propertyCache;
}

inline const atArray<rexObjectGenericAnimation::TrackInfo *>& RexMbAnimExportDataBase::GetTracks() const
{
	return m_tracks;
}


//#############################################################################

class RexMbAnimExportData : public RexMbAnimExportDataBase
{
public:
    RexMbAnimExportData( HFBModel pModel, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
        float fStartTime, float fEndTime, int nFrames, atArray<RexAdditionalAnimData*>& additionalModels, float fTranslateScale=1.0f );
    ~RexMbAnimExportData();

    void ReadFrameData();

    void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

private:
    //PURPOSE : Performs the actual sampling of animation data
    //PARAMS: track - generic track structure to fill
    //        m - the matrix to fill the track with
    void GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 &m );

	atArray<RexAdditionalAnimData*> m_additionalModels;
};

//////

class RexMbAnimExportDataCamera : public RexMbAnimExportDataBase
{
public:
	RexMbAnimExportDataCamera( HFBModel pModel, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
		float fStartTime, float fEndTime, int nFrames, bool bEnableDOF, float fTranslateScale=1.0f );
	~RexMbAnimExportDataCamera();

	void ReadFrameData();

	void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

private:
	//PURPOSE : Performs the actual sampling of animation data
	//PARAMS: track - generic track structure to fill
	//        m - the matrix to fill the track with
	void GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 &m );

	HFBModel m_pNearPlaneModel;
	HFBModel m_pFarPlaneModel;
	HFBModel m_pNearDOFStrengthPlaneModel;
	HFBModel m_pFarDOFStrengthPlaneModel;
};

//////

class RexMbAnimExportDataFacial : public RexMbAnimExportDataBase
{
public:
	RexMbAnimExportDataFacial( HFBModel pModel, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
		float fStartTime, float fEndTime, int nFrames, float fTranslateScale=1.0f );
	~RexMbAnimExportDataFacial();

	void ReadFrameData();

	void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

private:
	//PURPOSE : Performs the actual sampling of animation data
	//PARAMS: track - generic track structure to fill
	//        m - the matrix to fill the track with
	void GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 &m );

	HFBProperty m_pPropertyViseme;
	HFBModel m_ModelTint;
};

//////

class RexMbAnimExportDataFacialCustomAttribute : public RexMbAnimExportDataBase
{
public:
	RexMbAnimExportDataFacialCustomAttribute( HFBModel pModel, atString attributeName, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
		float fStartTime, float fEndTime, int nFrames, float fTranslateScale=1.0f );
	~RexMbAnimExportDataFacialCustomAttribute();

	void ReadFrameData();

	void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

private:
	//PURPOSE : Performs the actual sampling of animation data
	//PARAMS: track - generic track structure to fill
	//        m - the matrix to fill the track with
	void GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 &m );

	atString	m_customAttributeName;

};

///////

class RexMbAnimExportDataAdditional : public RexMbAnimExportDataBase
{
public:
	RexMbAnimExportDataAdditional( HFBModel pModel, atString trackName, atString attributeName, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
		float fStartTime, float fEndTime, int nFrames, float fTranslateScale=1.0f );
	~RexMbAnimExportDataAdditional();

	void ReadFrameData();

	void ReadKeyData( float fStartTime, float fEndTime, int nFrames );

private:
	//PURPOSE : Performs the actual sampling of animation data
	//PARAMS: track - generic track structure to fill
	//        m - the matrix to fill the track with
	void GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 &m );

	atString	m_customAttributeName;
};

//#############################################################################

#endif // __ANIM_BATCH_EXPORT_H__
