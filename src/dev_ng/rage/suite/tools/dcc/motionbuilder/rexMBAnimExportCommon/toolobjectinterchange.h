// 
// rexMBRage/toolobjectinterchange.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REXMBRAGE_TOOLOBJECTINTERCHANGE_H__ 
#define __REXMBRAGE_TOOLOBJECTINTERCHANGE_H__ 

#include "atl/array.h"
#include "cutfile/cutfevent.h"
#include "string/string.h"

#include "utility.h"
#include "UserObjects/OpenRealitySDK.h"

namespace rage {

class cutfCutsceneFile2;
class cutfEvent;
class cutfObject;
class cutfSetAnimToPlayEventArgs;
class RexRageCutsceneExport;

//##############################################################################

// PURPOSE: An interface class for interchanging the data from the Motion 
//  Builder scene (via RexRageCutsceneExport) and a cutfObject.
class IToolObjectInterchange
{
public:
    IToolObjectInterchange();
    virtual ~IToolObjectInterchange() = 0;

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

protected:
    // PURPOSE: Looks through the Event List for cutfObjectIdEvents that are dispatched to the given object.
    //    Matches the event ids that are dispatched and puts their index into one of the index lists.
    // PARAMS:
    //    pCutfile - the cutfile containing the event list to search
    //    pObject - the object to search for
    //    objectEventList - the list that will contain all of the events for the object, and what the indexes will be relative to
    //    iEventId1 - the first event id to search for
    //    iEventId2 - the second event id to search for
    //    eventIndexList1 - the index list to the put the results of the first event id
    //    eventIndexList2 - the index list to the put the results of the second event id
    void FilterEventsForObject( const cutfCutsceneFile2 *pCutfile, const cutfObject *pObject, atArray<cutfEvent *> &objectEventList,
        s32 iEventId1, s32 iEventId2, atArray<int> &eventIndexList1, atArray<int> &eventIndexList2 );

    // PURPOSE: Goes through the Event List and removes the indicated events (and Event Args with 0 references)
    // PARAMS:
    //    pCutfile - cutfile containing the Event List and Event Args List to modify
    //    objectEventList - the list of events that the indexing is relative to
    //    iStartIndex1 - the index in indexList1 to start at
    //    iStartIndex2 - the index in indexList2 to start at
    //    indexList1 - an array of indexes into objectEventList
    //    indexList2 - an array of indexes into objectEventList
    void RemoveExtraEvents( cutfCutsceneFile2 *pCutfile, atArray<cutfEvent *> &objectEventList, 
        int iStartIndex1, int iStartIndex2, atArray<s32> &indexList1, atArray<s32> &indexList2 );

    // PURPOSE: Adds/Removes the specified events based on the object's model's Visibility property
    // PARAMS:
    //    pCutsceneExport - the cut scene exporter class
    //    pObject - the object to search for
    //    iObjectId - id of the object to dispatch the events to.  can be different from the pObject's object id.
    //    objectEventList - the list of events that the indexing is relative to
    //    iVisibleEventId - the event to add when the object becomes visible
    //    iInvisibleEventId - the event to add when the object becomes invisible
    //    visibleEventIndexList - an array of indexes into objectEventList for the iVisibleEventId events
    //    invisibleEventIndexList - an array of indexes into objectEventList for the iInvisibleEventId events
    //    pEventArgsToClone - the event args to clone for each event pair
    void CreateVisibilityEvents( cutfCutsceneFile2* pCutFile, cutfObject *pObject, s32 iObjectId, atArray<cutfEvent *> &objectEventList, 
        s32 iVisibleEventId, s32 iInvisibleEventId, atArray<int> &visibleEventIndexList, atArray<int> &invisibleEventIndexList,
        cutfEventArgs *pEventArgsToClone );

private:
    void RemoveExtraEvents( cutfCutsceneFile2 *pCutfile, atArray<cutfEvent *> &objectEventList, int iStartIndex, atArray<s32> &indexList );
protected:
	void AddTypeFile( cutfObject *pObject );
};

inline void IToolObjectInterchange::LoadObjectDataFromTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport*/ )
{
    // do nothing
}

inline void IToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport*/ )
{
    // do nothing
}

//##############################################################################

class AssetManagerToolObjectInterchange : public IToolObjectInterchange
{
public:
    AssetManagerToolObjectInterchange();
    virtual ~AssetManagerToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

protected:
    // PURPOSE: Creates a cutfObjectIdEvent with event args of type cutfObjectIdListEventArgs.
    // PARAMS:
    //    iEventType - the event type id
    //    iObjectType - the object type id.  these object ids will be added to the cutfObjectIdListEventArgs.
    //    eventList - the event list to add or modify
    //    pObject - the object to send the event ot
    //    pCutsceneExport - the tool that holds, or can access, the data
    // RETURNS:
    // NOTES:
    void CreateObjectIdListEvent( s32 iEventType, s32 iObjectType, atArray<cutfEvent *> &eventList, cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateLoadSceneEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateAddBlockingBoundsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateAddRemovalBoundsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateLoadOverlaysEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateLoadSubtitlesEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateHideObjectsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateFixupObjectsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateLoadModelsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    void CreateLoadParticleEffectsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class AnimationManagerToolObjectInterchange : public IToolObjectInterchange
{
public:
    AnimationManagerToolObjectInterchange();
    virtual ~AnimationManagerToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class OverlayToolObjectInterchange : public IToolObjectInterchange
{
public:
    OverlayToolObjectInterchange();
    virtual ~OverlayToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class DecalToolObjectInterchange : public IToolObjectInterchange
{
public:
	DecalToolObjectInterchange();
	virtual ~DecalToolObjectInterchange();

	// PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
	//    by the RexRageCutsceneExport.
	// PARAMS:
	//    pObject - the object to copy the data to
	//    pCutsceneExport - the tool to that holds, or can access, the data
	virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
	//    the cutfObject and its cutfEvents.
	// PARAMS:
	//    pObject - the object that holds the data
	//    pCutsceneExport - the tool to copy the data to
	virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class ScreenFadeToolObjectInterchange : public IToolObjectInterchange
{
public:
    ScreenFadeToolObjectInterchange();
    virtual ~ScreenFadeToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class AnimatedObjectToolObjectInterchange : public IToolObjectInterchange
{
public:
    AnimatedObjectToolObjectInterchange();
    virtual ~AnimatedObjectToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

protected:
    // PURPOSE: Creates the set and clear events, removes extras from the cutfCutsceneFile2.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    //    pAnimName - the name of the animation to set and clear
    void CreateAnimEvents( cutfObject *pObject, cutfCutsceneFile2* pCutFile, const char* pAnimName );
};

//##############################################################################

class AnimatedParticleEffectObjectToolObjectInterchange : public AnimatedObjectToolObjectInterchange
{
public:
	AnimatedParticleEffectObjectToolObjectInterchange();
	virtual ~AnimatedParticleEffectObjectToolObjectInterchange();

	// PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
	//    by the RexRageCutsceneExport.
	// PARAMS:
	//    pObject - the object to copy the data to
	//    pCutsceneExport - the tool to that holds, or can access, the data
	virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
	//    the cutfObject and its cutfEvents.
	// PARAMS:
	//    pObject - the object that holds the data
	//    pCutsceneExport - the tool to copy the data to
	virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

protected:
	// PURPOSE: Creates the set and clear events, removes extras from the cutfCutsceneFile2.
	// PARAMS:
	//    pObject - the object that holds the data
	//    pCutsceneExport - the tool to copy the data to
	//    pAnimName - the name of the animation to set and clear
	void CreateAnimEvents( cutfObject *pObject, cutfCutsceneFile2* pCutFile, const char* pAnimName );
};

//##############################################################################

class PedModelObjectToolObjectInterchange : public AnimatedObjectToolObjectInterchange
{
public:
    PedModelObjectToolObjectInterchange();
    virtual ~PedModelObjectToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class CameraToolObjectInterchange : public AnimatedObjectToolObjectInterchange
{
public:
    CameraToolObjectInterchange();
    virtual ~CameraToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class VehicleModelToolObjectInterchange : public AnimatedObjectToolObjectInterchange
{
public:
    VehicleModelToolObjectInterchange();
    virtual ~VehicleModelToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

protected:
    void GenerateVehicleRemovalInfo( HFBModel hVehBone, atArray<atString> &vehicleRemovalList, bool bTranslateBoneId );
};

//##############################################################################

class LightToolObjectInterchange : public AnimatedObjectToolObjectInterchange
{
public:
    LightToolObjectInterchange();
    virtual ~LightToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class BlockingBoundsToolObjectInterchange : public IToolObjectInterchange
{
public:
    BlockingBoundsToolObjectInterchange();
    virtual ~BlockingBoundsToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

protected:
    void LoadBoundsDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class RemovalBoundsToolObjectInterchange : public BlockingBoundsToolObjectInterchange
{
public:
    RemovalBoundsToolObjectInterchange();
    virtual ~RemovalBoundsToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class AudioToolObjectInterchange : public IToolObjectInterchange
{
public:
    AudioToolObjectInterchange();
    virtual ~AudioToolObjectInterchange();

    // PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
    //    by the RexRageCutsceneExport.
    // PARAMS:
    //    pObject - the object to copy the data to
    //    pCutsceneExport - the tool to that holds, or can access, the data
    virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

    // PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
    //    the cutfObject and its cutfEvents.
    // PARAMS:
    //    pObject - the object that holds the data
    //    pCutsceneExport - the tool to copy the data to
    virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class RayfireToolObjectInterchange : public IToolObjectInterchange
{
public:
	RayfireToolObjectInterchange();
	virtual ~RayfireToolObjectInterchange();

	// PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
	//    by the RexRageCutsceneExport.
	// PARAMS:
	//    pObject - the object to copy the data to
	//    pCutsceneExport - the tool to that holds, or can access, the data
	virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
	//    the cutfObject and its cutfEvents.
	// PARAMS:
	//    pObject - the object that holds the data
	//    pCutsceneExport - the tool to copy the data to
	virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

class WeaponModelToolObjectInterchange : public AnimatedObjectToolObjectInterchange
{
public:
	WeaponModelToolObjectInterchange();
	virtual ~WeaponModelToolObjectInterchange();

	// PURPOSE: Sets the data fields of the cutfObject and creates its cutfEvents from the data provided
	//    by the RexRageCutsceneExport.
	// PARAMS:
	//    pObject - the object to copy the data to
	//    pCutsceneExport - the tool to that holds, or can access, the data
	virtual void LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );

	// PURPOSE: Sets the data fields of the RexRageCutsceneExport from the data in
	//    the cutfObject and its cutfEvents.
	// PARAMS:
	//    pObject - the object that holds the data
	//    pCutsceneExport - the tool to copy the data to
	virtual void SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile );
};

//##############################################################################

} // namespace rage

#endif // __REXMBRAGE_TOOLOBJECTINTERCHANGE_H__ 
