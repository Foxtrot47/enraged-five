// /animExport.h

#ifndef __ANIM_EXPORT_H__
#define __ANIM_EXPORT_H__

#include <map>
#include <string>
#include <vector>

#include "utility.h"
#include "parser/manager.h"
#include "rexGeneric/objectAnimation.h"
#include "UserObjects/AnimMarkup.h"

#include "animExportModel.h"

#include "crclip/clipanimation.h"
#include "clipxml/clipxml.h"

#include "Logger.h"

using namespace std;
using namespace rage;

//#############################################################################

#define MOVER_PROPERTY_NAME "Mover_DO_NOT_RESOURCE"
#define ADDITIVE_PROPERTY_NAME "Additive_DO_NOT_RESOURCE"
#define ADDITIVE_BASE_ANIM_PROPERTY_NAME "Additive_Base_Anim_DO_NOT_RESOURCE"
#define LINEAR_COMPRESSION_PROPERTY_NAME "LinearCompression_DO_NOT_RESOURCE"
#define COMPRESSION_FILE_PROPERTY_NAME "Compressionfile_DO_NOT_RESOURCE"
#define CONTROL_FILE_PROPERTY_NAME "ControlFile_DO_NOT_RESOURCE"
#define LOOPING_PROPERTY_NAME "Looping_DO_NOT_RESOURCE"
#define SKELETON_FILE_PROPERTY_NAME "Skelfile_DO_NOT_RESOURCE"
#define INIT_OFFSET_POS_PROPERTY_NAME "InitialOffsetPosition"
#define INIT_OFFSET_ROT_PROPERTY_NAME "InitialOffsetRotation"
#define USED_IN_NMBLEND_PROPERTY_NAME "UsedInNMBlend_DO_NOT_RESOURCE"
#define SOURCE_USER_NAME "SourceUserName_DO_NOT_RESOURCE"
#define SOURCE_HASH_USER_NAME "SourceUserName"
#define EXPORTER_VERSION_PROPERTY_NAME "ExporterVersion_DO_NOT_RESOURCE"
#define FBXFILE_PROPERTY_NAME "FBXFile_DO_NOT_RESOURCE"
#define ABSOLUTE_FBXFILE_PROPERTY_NAME "AbsoluteFBXFile_DO_NOT_RESOURCE"
#define EXPORTER_TYPE_PROPERTY_NAME "ExporterType"
#define PERFORCE_REVISION_PROPERTY_NAME "P4Version_DO_NOT_RESOURCE"
#define CAMERA_APPROVED_PROPERTY_NAME "CameraApproved"
#define RANGE_START_PROPERTY_NAME "RangeStart_DO_NOT_RESOURCE"
#define RANGE_END_PROPERTY_NAME "RangeEnd_DO_NOT_RESOURCE"
#define DISCARDED_FRAMES_PROPERTY_NAME "DiscardedFrames_DO_NOT_RESOURCE"
#define SCENE_DIRTY_PROPERTY_NAME "SceneDirty_DO_NOT_RESOURCE"

//#############################################################################

class RexMbAnimExportModel;

namespace rage
{
class crProperties;
}


//#############################################################################

class RexRageAnimExport
{
public:
	//PURPOSE: Default constructor
	RexRageAnimExport() 
		: m_bInteractiveMode(false)
        , m_iCurrentExportProgressItem(0)
        , m_iNumExportProgressItems(0)
	{
	};
	
	//PURPOSE: Default destructor
	~RexRageAnimExport() {};
	
	bool Initialize();
	
	//PURPOSE: Enables/Disables the display of message boxes for errors and warnings that 
	//occur during export.
	void SetInteractiveMode(bool bOnOff) { m_bInteractiveMode = bOnOff; }

	//PURPOSE: Sets the functor to be called when the caption of a progress indicator should be changed
    //void SetProgressCaptionCallback( rexMBAnimExportCommon::ProgressCaptionFtor ftor);

	//PURPOSE: Sets the functor to be called when the percent complete of a progress indicator should be changed
    //void SetProgressPercentCallback( rexMBAnimExportCommon::ProgressPercentFtor ftor);

    //PURPOSE: Sets the functor to be called when a message should be added to the output
   // void SetProgressMessageCallback( rexMBAnimExportCommon::ProgressMessageFtor ftor);

    //PURPOSE: Invokes the progress caption update functor
    //PARAMS : caption - new caption value
    void SetProgressCaption(const char* caption);

    //PURPOSE: Invokes the progress percent update functor
    //PARAMS : percent - new percent value
    void SetProgressPercent(float percent);

    //PURPOSE: Invokes the progress message update functor
    //PARAMS : message - the message to add
    void SetProgressMessage(const char* message);

    //PURPOSE: Main function to perform export of animations
    //PARAMS: pAnimData - structure containing information about how to export animation data
    //        bSample - sample the data vs grabbing keyframes
    //RETURNS: true for success, false for failure
    bool DoExport( const RexAnimMarkupData* pAnimData, bool bSample=true );

    //PURPOSE: Main function to perform export of animations
    //PARAMS: animDataList - list of structures containing information about how to export animation data
    //        bSample - sample the data vs grabbing keyframes
    //RETURNS: true for success, false for failure
    bool DoExport( const atArray<RexAnimMarkupData *> &animDataList, bool bSample=true );

	//PURPOSE: Merge body and face animations together
	//PARAMS: pAnimData - structure containing information about how to export animation data
	//RETURNS: true for success, false for failure
	bool ProcessFaceMerge( const RexAnimMarkupData* pAnimData );

	//PURPOSE: Process the base animation additive animations
	//PARAMS: pAnimData - structure containing information about how to export animation data
	//RETURNS: true for success, false for failure
	bool ProcessBaseAnimationAdditive( const RexAnimMarkupData* pAnimData );

	//PURPOSE: Execute render script post export
	//RETURNS: true for success, false for failure
	bool ExecutePostRenderScript(bool bRunScript);

	//PURPOSE: Acquires the name of a clip associated with a take and animation data.
	//PARAMS: pAnimData - the animation data to be exported
	//RETURNS: the full file path to the .clip file.
	char* GetClipFilePath(const RexAnimMarkupData* pAnimData, const RexCharAnimData* pCharData, const RexMbAnimSegmentData* pSegmentData, const int idxOutputPath);

	//PURPOSE: Acquires the path of an exported anim file associated with a take and animation data.
	//PARAMS: pAnimData - the animation data to be exported
	//RETURNS: the full file path to the .anim file.
	char* GetAnimFilePath(const RexAnimMarkupData* pAnimData, const RexCharAnimData* pCharData, const RexMbAnimSegmentData* pSegmentData, const int idxOutputPath);

	// PURPOSE:	Create a mover track from a series of .trk style string
	//			This is only really for the move from MAX and I may just port it 
	//			to the batch conversion process
	bool CreateMoverFromString( const char* pMoverStr, HFBModel hRootModel );

	//PURPOSE: Save clip data for animations in the scene
	bool SaveClipDataAllTakes( const RexAnimMarkupData* pAnimData );

	//PURPOSE: Load clip data for a given take
	bool SaveClipData( const RexAnimMarkupData* pAnimData, const RexCharAnimData* pCharData, const RexMbAnimSegmentData* pSegData, const vector<RexMbAnimExportDataBase*>& pExportDataList, float fStartTime, float fEndTime, const int idxOutputPath, bool bHasCamera );

	//PURPOSE: Load clip data for animations in the scene
	bool LoadClipDataAllTakes( RexAnimMarkupData* pAnimData );

	//PURPOSE: Load clip data for a given take
	// REMOVED CONST DUE TO NEEDING TO MOD THE DATA FOR CLIP LOADING - 22/03/10 MW
	bool LoadClipData( /*const*/ RexAnimMarkupData* pAnimData,  const RexCharAnimData* pCharData, RexMbAnimSegmentData* pSegData );

	//PURPOSE: Removes all properties and user objects attached to a Model.
	void ClearModelProperties(const HFBModel hModel);
		
	//PURPOSE: Removes all properties and user objects associated with a take.
	void ClearTakeProperties(const HFBTake hTake);

private:

    bool DoExportInternal( const vector<RexMbAnimExportModel *> &exportModelList, float fStartTime, float fEndTime, bool bSample );

    //PURPOSE : Utility function which takes in a single RexMbAnimData structure which may contain segments from
    //multiple Takes, and splits the data up into multiple RexMbAnimData structures where each resulting structure
    //contains segments for only one take
    //PARAMS : pMbAnimData - pointer to animation export markup object to split
    //		   outSplitAnimData - output parameter which contains the split up markup objects
    void SplitAnimDataByTakes(const RexAnimMarkupData* pMbAnimData, vector< RexAnimMarkupData* >& outSplitAnimData) const;

    //PURPOSE : This method takes in an animation markup object that may have
    //multiple root-mover model pairs specified for export, and splits it up
    //into separate markup objects where each object contains only a reference
    //to a single root-mover model pair.
    //PARAMS : pMbAnimData - pointer to animation export markup object to split
    //		   outSplitAnimData - output parameter which contains the split up markup objects
    void SplitAnimDataByModels(const RexAnimMarkupData* pMbAnimData, vector< RexAnimMarkupData* >& outSplitAnimData) const;

	void AddCameraCutTagBlocks( float fStartTime, float fEndTime, ClipXML* clip );
	void AddCameraCutTagBlocks( float fStartTime, float fEndTime, crClipAnimation *pClipAnim );

	// PURPOSE: Utility function for deleting vectors containing pointers.
	template<typename T>
	void DeleteVector(atArray<vector<T *>> &inArray);

	bool IsPowerOf2(int iNum);

	bool ValidateFacialAttributeFile(const RexAnimMarkupData* pAnimData);

	atString GetP4RevisionInfo(const char* pModelName);

	//PURPOSE : Validate the animation translation/rotation data to check for any INF/Nan errors.
	//PARAMS : pExportModel - pointer to animation export model data
	//RETURNS: true if successful, false on error
	bool ValidateAnimationData( RexMbAnimExportModel *pExportModel );

private:

	FBSystem m_system;
	bool m_bInteractiveMode;

    int m_iCurrentExportProgressItem;
    int m_iNumExportProgressItems;

	std::string m_ErrorMessage;
};


#endif //__ANIM_EXPORT_H__

