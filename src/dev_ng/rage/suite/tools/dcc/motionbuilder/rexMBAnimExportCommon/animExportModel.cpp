// /animBatchExport.cpp

#pragma warning(push)
#pragma warning(disable:4668)
#include <windows.h>
#pragma warning(pop)

#include <algorithm>
#include <list>
#include <queue>
#include <stack>
#include <sstream>
#include <vector>

#include "animExport.h"
#include "animExportModel.h"
#include "keylist.h"
#include "timelist.h"
#include "timerLog.h"
#include "utility.h"

#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "crmetadata/properties.h"

#include "atl/string.h"
#include "cranimation/animtrack.h"
#include "file/device.h"
#include "file/asset.h"
#include "rexRage/serializerAnimation.h"
#include "vector/vector3.h"
#include "vector/matrix44.h"
#include "vector/matrix34.h"
#include "vector/quaternion.h"

#include "rexMBRage/shared.h"

using namespace std;
using namespace rage;

//#############################################################################

string NormalizeModelName(const char* szName)
{
    string stdName(szName);

    //Remove the namespace (if any)
    size_t colonPos = stdName.find_last_of(':');
    if(colonPos != string::npos)
        stdName = stdName.substr(colonPos + 1);

    //Replace all the spaces with underscores
    size_t len = stdName.size();
    for(size_t i=0; i < len; i++)
    {
        if(isspace(stdName[i]))
        {
            stdName[i] = '_';
        }
    }

    return stdName;
}

string GetNormalizedModelNamespace(const char* szName)
{
    string stdName(szName);

    //Grab everything up to the first colon.  If a colon isn't present
    //then the model doesn't have a namespace
    size_t colonPos = stdName.find_first_of(':');
    if(colonPos == string::npos)
        return string("");

    string stdNameSpace = stdName.substr(0, colonPos);

    //Replace all the spaces with underscores
    size_t len = stdNameSpace.size();
    for(size_t i=0; i < len; i++)
    {
        if(isspace(stdNameSpace[i]))
        {
            stdNameSpace[i] = '_';
        }
    }

    return stdNameSpace;
}

#pragma warning(disable:4244)

//#############################################################################

//rexMBAnimExportCommon::ProgressCaptionFtor RexMbAnimExportModel::sm_progressCaptionFtor = MakeFunctor(&rexMBAnimExportCommon::NullCaptionFunctor);
//rexMBAnimExportCommon::ProgressCaptionFtor RexMbAnimExportModel::sm_progressMessageFtor = MakeFunctor(&rexMBAnimExportCommon::NullMessageFunctor);

RexMbAnimExportModel::RexMbAnimExportModel( const RexAnimMarkupData *pAnimData )
: m_pAnimData(pAnimData)
{
	
}

RexMbAnimExportModel::~RexMbAnimExportModel()
{
    delete m_pAnimData;

    Clear();
}

bool RexMbAnimExportModel::Initialize( rexObjectGenericAnimation& gnrcAnimation, float fStartTime, float fEndTime, int nFrames )
{
#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::StartTimer("RexMbAnimExportModel::Initialize");
    }
#endif //PROFILE_ANIM_EXPORT

    CreateNoExportModelList();

    //Initialize the generic animation properties
    gnrcAnimation.m_StartTime = fStartTime;
    gnrcAnimation.m_EndTime = fEndTime;
    gnrcAnimation.m_FramesPerSecond = rexMBAnimExportCommon::GetFPS();		//Always returns 30.0
	gnrcAnimation.m_ParentMatrix.Identity();	

    //Add and configure the generic segments
    int segmentCount = m_pAnimData->GetNumSegments();
    for(int segIdx = 0; segIdx < segmentCount; segIdx++)
    {
		RexCharAnimData* pCharData = m_pAnimData->GetCharacter(0);
        RexMbAnimSegmentData* pMbSegData = m_pAnimData->GetSegment(segIdx);
        Assert(pMbSegData);

        rexObjectGenericAnimationSegment* pGnrcSegment = new rexObjectGenericAnimationSegment;

		//Apply the output animation prefix and/or suffix.
		//NOTE: It would be nice if we can use RexAnimExport::GetAnimFilePath but we don't need an extension
		//for this particular string.
		// Make sure the starting default string is the take name override if one is set.

		string modifiedSegmentName;
		const char* overrideName = pMbSegData->GetTakeNameOverride();
		if ( overrideName == NULL || strlen(overrideName) == 0)
		{	
			modifiedSegmentName = pMbSegData->m_segmentName;
		}
		else
		{
			modifiedSegmentName = pMbSegData->m_takeNameOverride;
		}
		
		modifiedSegmentName.insert(0, pCharData->GetAnimPrefix() );
		modifiedSegmentName.append( pCharData->GetAnimSuffix() );
       
		pGnrcSegment->SetName(modifiedSegmentName.c_str());

        pGnrcSegment->m_StartTime = ( pMbSegData->m_iStartFrame < 0 ? -1 : (pMbSegData->m_iStartFrame / gnrcAnimation.m_FramesPerSecond));
        pGnrcSegment->m_EndTime = ( pMbSegData->m_iEndFrame < 0 ? -1 : (pMbSegData->m_iEndFrame / gnrcAnimation.m_FramesPerSecond));
        pGnrcSegment->m_Autoplay = pMbSegData->m_bAutoplay;
        pGnrcSegment->m_Looping = pMbSegData->m_bLooping;
        pGnrcSegment->m_IgnoreChildBones = false;

        int nSegInputModels = pMbSegData->GetNumInputModels();
        for(int modelIdx = 0; modelIdx < nSegInputModels; modelIdx++)
        {
            const char* inputModelName = pMbSegData->GetInputModelName(modelIdx);
            pGnrcSegment->m_ChunkNames.PushAndGrow( atString( NormalizeModelName(inputModelName).c_str() ) );
        }

        gnrcAnimation.m_ContainedObjects.PushAndGrow(pGnrcSegment,1);
    }

    //Convert the mover data
	RexCharAnimData* pCharacter = m_pAnimData->GetCharacter(0);

	//Determine which attributes file to use for each character.
	if ( pCharacter->GetFacialAttribute() == NULL || strlen(pCharacter->GetFacialAttribute()) == 0 )
	{
		m_attributeMgr.LoadAttributeFile(atString(m_pAnimData->m_attributeFilePath.c_str()));
	}
	else
	{
		m_attributeMgr.LoadAttributeFile((atString)pCharacter->GetFacialAttribute());
	}

    int nMoverModels = pCharacter->GetNumMoverModels();
    if(nMoverModels)
    {
        const char* rootModelName = pCharacter->GetInputModelName(0);
        HFBModel hRootModel = RAGEFindModelByName(const_cast<char*>(rootModelName));
        if(hRootModel == NULL)
		{
			ULOGGER_MOBO.SetProgressMessage("While converting mover data, failed to find root model '%s' in the current scene.", rootModelName);
			return false;
		}

        if(nMoverModels > 1)
        {
            //TODO : Need to support multiple movers
        }
        else
        {
            gnrcAnimation.m_MultiMoverMode = false;
        }

        for ( int moverIdx = 0; moverIdx < nMoverModels; ++moverIdx )
        {
            const char* moverModelName = pCharacter->GetMoverModelName(moverIdx);
            if(!strlen(moverModelName))
            {
                continue;
            }

            HFBModel hMoverModel = RAGEFindModelByName(const_cast<char*>(moverModelName));
            Assertf(hMoverModel, "While converting mover data, failed to find mover model '%s' in the current scene.", moverModelName);

            // Get the scale of the model's parent, which we will use to scale the translation tracks later
            float fTranslateScale = 1.0f;
            if ( m_pAnimData->m_useParentScale )
            {
                HFBModel hParentModel = hMoverModel->Parent;
                if ( hParentModel != NULL )
                {
                    FBVector3d scale;
                    hParentModel->GetVector( scale, kModelScaling );
                    
                    // In RDR2, character is scaled by 100 when brought into MotionBuilder, so scale down
                    fTranslateScale = scale[0] * 0.01f;
                }
            }

            rexObjectGenericAnimation::MoverChunkInfo* pMoverChunkInfo = new rexObjectGenericAnimation::MoverChunkInfo();
            
			atArray<RexAdditionalAnimData*> additionalModels;
            RexMbAnimExportData *pMoverExportData = rage_new RexMbAnimExportData( hMoverModel, pMoverChunkInfo->m_MoverTracks, 
                gnrcAnimation.m_StartTime, gnrcAnimation.m_EndTime, nFrames, additionalModels, fTranslateScale );
            m_exportDataList.push_back( pMoverExportData );

            //TODO : Maya allows for setting the mover id from a moverId attribute on the mover transform node
            //	we may need to support something like this...

            pMoverChunkInfo->m_MoverName = NormalizeModelName(moverModelName).c_str();

            //Append the mover chunk to the generic animation
            gnrcAnimation.m_MoverChunks.PushAndGrow(pMoverChunkInfo);
        }
    }//End if(nMoverModels)

    //Setup the generic bone animation chunk hierarchy...
    vector<HFBModel>								mbModelArray;
    vector<rexObjectGenericAnimation::ChunkInfo*>	gnrcChunkArray;

    int nInputModels = pCharacter->GetNumInputModels();
    for ( int modelIdx = 0; modelIdx < nInputModels; ++modelIdx )
    {
        const char* modelName = pCharacter->GetInputModelName(modelIdx);
        HFBModel pModel = RAGEFindModelByName(const_cast<char*>(modelName));
        Assertf(pModel, "Failed to find model '%s' in the current scene.", modelName);

        // Get the scale of the model's parent, which we will use to scale the translation tracks later
        float fTranslateScale = 1.0f;
        if ( m_pAnimData->m_useParentScale )
        {
            HFBModel hParentModel = pModel->Parent;
            if ( hParentModel != NULL )
            {
                FBVector3d scale;
                hParentModel->GetVector( scale, kModelScaling );

                // in RDR2, character is scaled by 100 when brought into MotionBuilder, so scale down
                fTranslateScale = scale[0] * 0.01f;
            }
        }

        //Create the root chunk
        rexObjectGenericAnimation::ChunkInfo* pChunkInfo = new rexObjectGenericAnimation::ChunkInfo();

		if(strcmp(pCharacter->GetModelType(),MODEL_TYPE_CAMERA) == 0)
		{
			RexMbAnimExportDataCamera *pCameraExportData = rage_new RexMbAnimExportDataCamera( pModel, pChunkInfo->m_Tracks, 
				gnrcAnimation.m_StartTime, gnrcAnimation.m_EndTime, nFrames, m_pAnimData->m_bEnableCameraDOF, fTranslateScale );
			m_exportDataList.push_back( pCameraExportData );
		}
		else if(strcmp(pCharacter->GetModelType(),MODEL_TYPE_FACE) == 0)
		{
			RexMbAnimExportDataFacial *pFacialExportData = rage_new RexMbAnimExportDataFacial( pModel, pChunkInfo->m_Tracks, 
				gnrcAnimation.m_StartTime, gnrcAnimation.m_EndTime, nFrames, fTranslateScale );
			m_exportDataList.push_back( pFacialExportData );
		}
		else
		{
			atArray<RexAdditionalAnimData*> additionalModels;
			RexMbAnimExportData *pMoverExportData = rage_new RexMbAnimExportData( pModel, pChunkInfo->m_Tracks, 
				gnrcAnimation.m_StartTime, gnrcAnimation.m_EndTime, nFrames, pCharacter->m_additionalModels, fTranslateScale );
			m_exportDataList.push_back( pMoverExportData );
		}

        pChunkInfo->m_Parent = NULL;
        pChunkInfo->m_BoneMatrix.Identity();
        pChunkInfo->m_BoneIndex = 0;
        pChunkInfo->m_BoneID = 0; //rexMBAnimExportCommon::GetBoneID( m_pModel, false );
        pChunkInfo->m_IsJoint = pModel->Is(FBModelSkeleton::TypeInfo);
        pChunkInfo->m_Name = NormalizeModelName(modelName).c_str();

		HFBProperty pProperty = pModel->PropertyList.Find("SUPPRESS_WARNINGS"); // boolean property
		if(pProperty)
		{
			bool v=false;
			pProperty->GetData( &v, sizeof(v) );

			if(v)
			{
				pChunkInfo->m_bSuppressWarnings = true;
			}
		}

        gnrcAnimation.m_RootChunks.PushAndGrow(pChunkInfo);

        mbModelArray.push_back(pModel);
        gnrcChunkArray.push_back(pChunkInfo);

        BuildChunkHierarchy(pModel, pChunkInfo, mbModelArray, gnrcChunkArray, gnrcAnimation.m_StartTime, gnrcAnimation.m_EndTime, nFrames, atString(pCharacter->GetModelType()), pCharacter->m_additionalModels );

		for(int i=0; i < pCharacter->m_additionalModels.GetCount(); ++i)
		{
			RexAdditionalAnimData* pAdditionalModelData = pCharacter->m_additionalModels[i];

			// Skip models which are already within the hierarchy as they get handled internally of that structure. This handles additional models.
			if(RecurseFindBone(pModel, pAdditionalModelData->m_modelName))
				continue;

			HFBModel pAdditionalModel = RAGEFindModelByName(const_cast<char*>(pAdditionalModelData->m_modelName.c_str()));
			if (pAdditionalModel == nullptr)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Failed to find model '%s' in the current scene.", pAdditionalModelData->m_modelName.c_str());
				return false;
			}

			rexObjectGenericAnimation::ChunkInfo* pAdditionalModelChunkInfo = new rexObjectGenericAnimation::ChunkInfo();

			RexMbAnimExportDataAdditional *pAdditionalExportData = rage_new RexMbAnimExportDataAdditional( pAdditionalModel, pAdditionalModelData->m_trackId, pAdditionalModelData->m_attributeName, pAdditionalModelChunkInfo->m_Tracks, 
				gnrcAnimation.m_StartTime, gnrcAnimation.m_EndTime, nFrames, fTranslateScale );
			m_exportDataList.push_back( pAdditionalExportData );

			pAdditionalModelChunkInfo->m_Parent = pChunkInfo;
			pAdditionalModelChunkInfo->m_BoneMatrix.Identity();
			pAdditionalModelChunkInfo->m_BoneIndex = (int)mbModelArray.size();
			pAdditionalModelChunkInfo->m_BoneID = pAdditionalModelData->m_attributeName;
			pAdditionalModelChunkInfo->m_IsJoint = pAdditionalModel->Is(FBModelSkeleton::TypeInfo);
			pAdditionalModelChunkInfo->m_Name = NormalizeModelName(pAdditionalModelData->m_attributeName.c_str()).c_str();

			mbModelArray.push_back(pAdditionalModel);
			gnrcChunkArray.push_back(pAdditionalModelChunkInfo);
			pChunkInfo->m_Children.PushAndGrow(pAdditionalModelChunkInfo);
		}
	}

#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::EndTimer("RexMbAnimExportModel::Initialize");
    }
#endif //PROFILE_ANIM_EXPORT

    return m_exportDataList.size() > 0;
}

void RexMbAnimExportModel::CollectAnimData()
{
    for ( vector<RexMbAnimExportDataBase *>::iterator it = m_exportDataList.begin(); it != m_exportDataList.end(); ++it )
    {
        (*it)->ReadFrameData();
    }
}

void RexMbAnimExportModel::CollectAnimData( float fStartTime, float fEndTime, int nFrames )
{
#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::StartTimer("RexMbAnimExportModel::CollectAnimData( keyframes )");
    }
#endif //PROFILE_ANIM_EXPORT

    for ( vector<RexMbAnimExportDataBase *>::iterator it = m_exportDataList.begin(); it != m_exportDataList.end(); ++it )
    {
        (*it)->ReadKeyData( fStartTime, fEndTime, nFrames );
    }

#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::EndTimer("RexMbAnimExportModel::CollectAnimData( keyframes )");
    }
#endif //PROFILE_ANIM_EXPORT
}

bool RexMbAnimExportModel::SerializeAnimation( rexObjectGenericAnimation& gnrcAnimation, const char* outputPath)
{
#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::StartTimer("RexMbAnimExportModel::SerializeAnimation");
    }
#endif //PROFILE_ANIM_EXPORT

    rexSerializerRAGEAnimation rexSer;

    //Initialize the output path
	rexSer.SetOutputPath(atString(outputPath));

    if(m_pAnimData->m_ctrlFilePath.GetLength() == 0)
    {
        //No control file has been specified, so export the default setup of the root bone having translation and rotation 
        //tracks, and all other bones having only rotation tracks. 
        rexSer.AddChannelToWrite(atString("translateX"),atString("translateX"),rexObjectGenericAnimation::TRACK_TRANSLATE,true);
        rexSer.AddChannelToWrite(atString("translateY"),atString("translateY"),rexObjectGenericAnimation::TRACK_TRANSLATE,true);
        rexSer.AddChannelToWrite(atString("translateZ"),atString("translateZ"),rexObjectGenericAnimation::TRACK_TRANSLATE,true);
        rexSer.AddChannelToWrite(atString("rotateX"),atString("rotateX"),rexObjectGenericAnimation::TRACK_ROTATE,false);
        rexSer.AddChannelToWrite(atString("rotateY"),atString("rotateY"),rexObjectGenericAnimation::TRACK_ROTATE,false);
        rexSer.AddChannelToWrite(atString("rotateZ"),atString("rotateZ"),rexObjectGenericAnimation::TRACK_ROTATE,false);
    }
    else
    {
		atString controlFilePath;
		rexMBAnimExportCommon::GetMotionBuilderAnimCtrlFilePathSetting( controlFilePath );
		controlFilePath = atString( controlFilePath, m_pAnimData->m_ctrlFilePath.c_str());
        //An animation control file has been specified so tell the serializer to use it.		
        if (rexSer.LoadAnimExportCtrlFile(controlFilePath.c_str()))
		{
            ULOGGER_MOBO.SetProgressMessage("Loaded animation control file '%s'", m_pAnimData->m_ctrlFilePath.c_str() );

            rexSer.SetUseAnimCtrlExportFile(true);
        }
        else
        {
            ULOGGER_MOBO.SetProgressMessage("ERROR: Failed to load animation control file '%s'", m_pAnimData->m_ctrlFilePath.c_str());

#if PROFILE_ANIM_EXPORT
            if(rexMBTimerLog::IsTimerLogEnabled())
            {
                rexMBTimerLog::EndTimer("RexMbAnimExportModel::SerializeAnimation");
            }
#endif //PROFILE_ANIM_EXPORT
            return false;
        }
    }

	// Check for ctrl file override at the take level
	if( m_pAnimData->GetSegmetFromTakes() )
	{
		RexMbAnimSegmentData* pMbAnimSegment = m_pAnimData->GetSegment(0);
		RexCharAnimData* pMbCharData = m_pAnimData->GetCharacter(0);
		bool ctrlFileOverride = false;
		atString animCtrlFilePath;

		if(pMbAnimSegment->m_ctrlFilePath != "(no spec file)")
		{
			//An animation control file has been specified so tell the serializer to use it.
			animCtrlFilePath;
			rexMBAnimExportCommon::GetMotionBuilderAnimCtrlFilePathSetting( animCtrlFilePath );
			animCtrlFilePath += "\\";
			animCtrlFilePath += pMbAnimSegment->m_ctrlFilePath.c_str();
			ctrlFileOverride = true;
		}
		else if(strcmp(pMbCharData->GetCtrlFile(), "(no spec file)") != 0)
		{
			animCtrlFilePath;
			rexMBAnimExportCommon::GetMotionBuilderAnimCtrlFilePathSetting( animCtrlFilePath );
			animCtrlFilePath += "\\";
			animCtrlFilePath += pMbCharData->GetCtrlFile();
			ctrlFileOverride = true;
		}

		if(ctrlFileOverride)
		{
			if (rexSer.LoadAnimExportCtrlFile(animCtrlFilePath))
			{
				Displayf("Loaded animation control file '%s'", animCtrlFilePath);
				rexSer.SetUseAnimCtrlExportFile(true);
			}
			else
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Failed to load animation control file '%s'", animCtrlFilePath);

				return false;
			}
		}
	}

    rexSer.RegisterProgressBarTextCallback( &RexMbAnimExportModel::SetProgressCaption );

    rexObjectGenericEntityComponent *pEntityComponent = dynamic_cast<rexObjectGenericEntityComponent *>( gnrcAnimation.m_ContainedObjects[0] );
    ULOGGER_MOBO.SetProgressMessage("Saving animation '%s.anim'.", pEntityComponent->GetName().c_str() );

    //Serialize the actual animation files...
    rexResult res = rexResult::PERFECT;
    res.Combine(rexSer.Serialize( gnrcAnimation ));

    int msgCount = res.GetMessageCount();
    if ( msgCount && (res.Errors() || res.Warnings()) )
    {
        ULOGGER_MOBO.SetProgressMessage("The following %s occurred while writing the animation:\n", res.Errors() ? "error(s)" : "warning(s)" );
        
        for(int msgIdx = 0; msgIdx < msgCount; msgIdx++)
        {
            atString msgString = res.Errors() ? atString("ERROR: ") : atString("WARNING: ");
            msgString += res.GetMessageByIndex(msgIdx);

            ULOGGER_MOBO.SetProgressMessage( msgString.c_str() );
        }
    }

#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::EndTimer("RexMbAnimExportModel::SerializeAnimation");
    }
#endif //PROFILE_ANIM_EXPORT

    return !res.Errors();
}

void RexMbAnimExportModel::Clear()
{    
    m_noExportModelArray.clear();

    for ( vector<RexMbAnimExportDataBase *>::iterator it = m_exportDataList.begin(); it != m_exportDataList.end(); ++it )
    {
        delete (*it);
    }

    m_exportDataList.clear();
}

void RexMbAnimExportModel::CreateNoExportModelList()
{
    //Locate the 'NoExport' set
    HFBScene hScene = m_system.Scene;
    HFBSet	 hNoExportSet = NULL;

    int nSets = hScene->Sets.GetCount();
    for(int setIdx = 0; setIdx < nSets; setIdx++)
    {
        HFBSet hCurSet = hScene->Sets[setIdx];
        string setName = hCurSet->Name.AsString();
        if(strcmpi(setName.c_str(), "NoExport") == 0)
        {
            hNoExportSet = hCurSet;
            break;
        }
    }

    //Add all the models in the NoExport set to the list of models to exclude from
    //having animation data exported
    if(hNoExportSet)
    {
        int nSetItems = hNoExportSet->Items.GetCount();
        for(int itemIdx = 0; itemIdx < nSetItems; itemIdx++)
        {
            HFBComponent hComp = hNoExportSet->Items[itemIdx];
            if(hComp->Is(FBModel::TypeInfo))
            {
                HFBModel pModel = (HFBModel)hComp;
                MODEL_ARRAY::iterator findIt = find(m_noExportModelArray.begin(), m_noExportModelArray.end(), pModel);
                if(findIt == m_noExportModelArray.end())
                    m_noExportModelArray.push_back(pModel);
            }
        }
    }

	RexCharAnimData* pCharacter = m_pAnimData->GetCharacter(0);
    //Add all the models in the scene that have been assigned a naming convention that 
    //should prevent them from being exported
    if(pCharacter->GetNumInputModels())
    {
        const char* szRootModelName = pCharacter->GetInputModelName(0);
        HFBModel hRootModel = RAGEFindModelByName(const_cast<char*>(szRootModelName));
        Assertf(hRootModel, "Root model '%s' not found in scene", szRootModelName);

        queue<HFBModel> modelQueue;
        modelQueue.push(hRootModel);
        while(!modelQueue.empty())
        {
            HFBModel pModel = modelQueue.front();
            modelQueue.pop();

            string normalizedModelName = NormalizeModelName(pModel->LongName.AsString());

            if( (normalizedModelName.find("_null") != string::npos) ||
                (normalizedModelName.find("_NULL") != string::npos) ||
                (normalizedModelName.find("_HIDE") != string::npos) ||
                (normalizedModelName.find("_hide") != string::npos) )
            {
                MODEL_ARRAY::iterator findIt = find(m_noExportModelArray.begin(), m_noExportModelArray.end(), pModel);
                if(findIt == m_noExportModelArray.end())
                    m_noExportModelArray.push_back(pModel);
            }

            int nChildren = pModel->Children.GetCount();
            for(int childIdx = 0; childIdx < nChildren; childIdx++)
            {
                modelQueue.push(pModel->Children[childIdx]);
            }
        }
    }
}

void RexMbAnimExportModel::BuildChunkHierarchy( HFBModel hModel, 
                                            rexObjectGenericAnimation::ChunkInfo* pParentChunk,
                                            vector<HFBModel>& mbModelArray,
                                            vector<rexObjectGenericAnimation::ChunkInfo*>& gnrcChunkArray, 
                                            float fStartTime, float fEndTime, int nFrames, atString modelType, 
											atArray< RexAdditionalAnimData* >& additionalModels  )
{
	if(strcmp(modelType,MODEL_TYPE_CAMERA) == 0)
		return;
    int nChildren = hModel->Children.GetCount();
    for(int childIdx=0; childIdx < nChildren; childIdx++)
    {
        HFBModel childModel = hModel->Children[childIdx];

        //Filter out models that shouldn't have animation exported (e.g. null bones)
        if(!IsModelOkForExport(childModel))
        {
            Displayf("Skipping chunk for model : %s", childModel->LongName.AsString());
            continue;
        }

        //Create a chunk for the child model
        rexObjectGenericAnimation::ChunkInfo* pChunkInfo = new rexObjectGenericAnimation::ChunkInfo();
        pChunkInfo->m_Parent = pParentChunk;
        pChunkInfo->m_BoneMatrix.Identity(); 
        pChunkInfo->m_BoneIndex = (int)mbModelArray.size();
#if !HACK_MP3
		pChunkInfo->m_BoneID = rexMBAnimExportCommon::GetBoneID( childModel, true ).c_str(); 
#else
        pChunkInfo->m_BoneID = NormalizeModelName(childModel->LongName.AsString()).c_str(); 
#endif // HACK_GTA4
        pChunkInfo->m_IsJoint = childModel->Is(FBModelSkeleton::TypeInfo);
        pChunkInfo->m_Name = NormalizeModelName(childModel->LongName.AsString()).c_str();

		HFBProperty pProperty = childModel->PropertyList.Find("SUPPRESS_WARNINGS"); // boolean property
		if(pProperty)
		{
			bool v=false;
			pProperty->GetData( &v, sizeof(v) );

			if(v)
			{
				pChunkInfo->m_bSuppressWarnings = true;
			}
		}

#ifdef _DEBUG
        // TEMP Displayf( "%s hashes to %d", (const char*)pChunkInfo->m_BoneID, crAnimTrack::ConvertBoneNameToId( pChunkInfo->m_BoneID ) );
#endif //_DEBUG
		if(strcmp(modelType,MODEL_TYPE_FACE) == 0)
		{
			RexMbAnimExportDataFacial *pChildExportData = rage_new RexMbAnimExportDataFacial( childModel, pChunkInfo->m_Tracks, fStartTime, fEndTime, nFrames );
			m_exportDataList.push_back( pChildExportData );

			// Iterate through the custom attributes on this model
			for(int i=0; i < childModel->PropertyList.GetCount(); ++i)
			{
				atString attributeName(childModel->PropertyList[i]->GetName());

				// check if the attribute is within our block
				atString attributeFullName;
				if(!m_attributeMgr.FindAttribute(attributeName, attributeFullName))
				{
					// max 2012 fix. attributes come through as just "scream_CA" not full attribute file naming so we fix them up here.
					// max 20111 works.
					char cAttributeName[MAX_PATH];
					sprintf(cAttributeName,"%s.%s_A.%s", childModel->Name.AsString(), childModel->Name.AsString(), attributeName.c_str());
					attributeName = cAttributeName;
					if(!m_attributeMgr.FindAttribute(attributeName, attributeFullName)) 
					{
						ULOGGER_MOBO.SetProgressMessage("WARNING: Face custom property '%s' was not found. Attribute file tag '%s'", childModel->PropertyList[i]->GetName(), attributeName.c_str());
						continue;
					}
				}

				//Create a chunk for the child model
				rexObjectGenericAnimation::ChunkInfo* pAttributeChunkInfo = new rexObjectGenericAnimation::ChunkInfo();
				pAttributeChunkInfo->m_Parent = pParentChunk;
				pAttributeChunkInfo->m_BoneMatrix.Identity(); 
				pAttributeChunkInfo->m_BoneIndex = (int)mbModelArray.size();
				pAttributeChunkInfo->m_BoneID = attributeFullName.c_str();
				pAttributeChunkInfo->m_IsJoint = childModel->Is(FBModelSkeleton::TypeInfo);
				pAttributeChunkInfo->m_Name = NormalizeModelName(attributeName.c_str()).c_str();

				RexMbAnimExportDataFacialCustomAttribute* pChildAttributeExportData = rage_new RexMbAnimExportDataFacialCustomAttribute( childModel, atString(childModel->PropertyList[i]->GetName())/*attributeName*/, pAttributeChunkInfo->m_Tracks, fStartTime, fEndTime, nFrames );
				m_exportDataList.push_back( pChildAttributeExportData );

				gnrcChunkArray.push_back(pAttributeChunkInfo);
				pParentChunk->m_Children.PushAndGrow(pAttributeChunkInfo);
			}
		}
		else
		{
			
			RexMbAnimExportData *pChildExportData = rage_new RexMbAnimExportData( childModel, pChunkInfo->m_Tracks, fStartTime, fEndTime, nFrames, additionalModels );
			m_exportDataList.push_back( pChildExportData );
		}

        //Add the model reference and the associated chunk to the storage arrays that will be
        //used later for collecting the animation sample data
        mbModelArray.push_back(childModel);
        gnrcChunkArray.push_back(pChunkInfo);

        pParentChunk->m_Children.PushAndGrow(pChunkInfo);

        //Recurse for this nodes children
        BuildChunkHierarchy( childModel, pChunkInfo, mbModelArray, gnrcChunkArray, fStartTime, fEndTime, nFrames, modelType, additionalModels );
    }
}

bool RexMbAnimExportModel::IsModelOkForExport( HFBModel pModel )
{
    MODEL_ARRAY::iterator findIt = find(m_noExportModelArray.begin(), m_noExportModelArray.end(), pModel);
    if(findIt == m_noExportModelArray.end())
        return true;
    else
    {
        Displayf("Model '%s' will not have animation exported", pModel->LongName.AsString());
        return false;
    }
}


void RexMbAnimExportDataBase::InitTracksInfo( float fStartTime, float fEndTime, int nFrames, const vector<string>& trackNames, const vector<int>& trackIds )
{
	int trackCount = (int)trackNames.size();
	m_tracks.Reset();
	m_tracks.Reserve(trackCount);

	for(int trackIdx = 0; trackIdx < trackCount; trackIdx++)
	{
		rexObjectGenericAnimation::TrackInfo* pTrack = NULL;
		switch(trackIds[trackIdx])
		{
		case rexObjectGenericAnimation::TRACK_TRANSLATE:
		case rexObjectGenericAnimation::TRACK_SCALE:
			pTrack = new rexObjectGenericAnimation::TrackInfoVector3();
			break;

		case rexObjectGenericAnimation::TRACK_ROTATE:
			pTrack = new rexObjectGenericAnimation::TrackInfoQuaternion();
			break;

		case rexObjectGenericAnimation::TRACK_CAMERA_FOV:
		case rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN:
		case rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT:
		case rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN:
		case rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT:
			pTrack = new rexObjectGenericAnimation::TrackInfoFloat();
			break;

		default:
			pTrack = new rexObjectGenericAnimation::TrackInfoFloat();
			break;
		}

		pTrack->m_Name = trackNames[trackIdx].c_str();
		pTrack->m_TrackID = trackIds[trackIdx];

		if ( InitTrackInfo( fStartTime, fEndTime, nFrames, *pTrack ) )
		{
			m_tracks.Append() = pTrack;
		}
		else
		{
			//Couldn't find the appropriate data for the track, so don't add it
			//TODO : Produce some sort of useful warning/error here...
			delete pTrack;
		}
	}
}

bool RexMbAnimExportDataBase::InitTrackInfo( float fStartTime, float fEndTime, int nFrames, rexObjectGenericAnimation::TrackInfo& track )
{
	track.m_StartTime = fStartTime;
	track.m_EndTime = fEndTime;

	//TODO : Maya exporter supports determining if a track is locked by the following logic
	// track.m_IsLocked = (plug.isLocked() || !plug.isKeyable() || (track.m_EndTime <= track.m_StartTime));
	// we probably should look to support some similar functionality here.
	track.m_IsLocked = false;

	track.ResetKeys( nFrames );

	return true;
}

void RexMbAnimExportDataBase::AddModelPropertiesToCache()
{
	//Rotation Order
	if( m_pModel->Is(FBModelSkeleton::TypeInfo) )
	{
		bool enableRotationDofPropValue = false;

		HFBProperty enableRotationDofProp = m_pModel->PropertyList.Find("Enable Rotation DOF");
		if(enableRotationDofProp)
		{
			enableRotationDofProp->GetData(&enableRotationDofPropValue, sizeof(bool));
		}

		if(enableRotationDofPropValue)
		{
			HFBProperty rotOrderProp = m_pModel->PropertyList.Find("RotationOrder");
			if(rotOrderProp)
			{
				int rotOrderPropValue;
				rotOrderProp->GetData(&rotOrderPropValue, sizeof(int));
				switch(rotOrderPropValue)
				{
				case 0: // XYZ
					m_propertyCache.m_rotOrder = "xyz";
					break;
				case 1: // XZY
					m_propertyCache.m_rotOrder = "xzy";
					break;
				case 2: // YZX
					m_propertyCache.m_rotOrder = "yzx";
					break;
				case 3: // YXZ
					m_propertyCache.m_rotOrder = "yxz";
					break;
				case 4: // ZXY
					m_propertyCache.m_rotOrder = "zxy";
					break;
				case 5: // ZYX
					m_propertyCache.m_rotOrder = "zyx";
					break;
				default:
					Assertf(0, "Unknown rotation order enum value of '%d'", rotOrderPropValue);
				}
			}
		}
		else
		{
			m_propertyCache.m_rotOrder = "xyz";
		}
	}
	else
	{
		m_propertyCache.m_rotOrder = "xyz";
	}

	//TODO : May need to get these propertoes from MB, since aparently incoming joint orients will sometimes be
	//converted into the pre and post rotations on the nodes in MB, just haven't seen a case of this happening yet
	m_propertyCache.m_preRotation.Set(0.0f, 0.0f, 0.0f);		
	m_propertyCache.m_postRotation.Set(0.0f, 0.0f, 0.0f);	
}



//#############################################################################

RexMbAnimExportData::RexMbAnimExportData( HFBModel pModel, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
                                         float fStartTime, float fEndTime, int nFrames, atArray<RexAdditionalAnimData*>& additionalModels,
										 float fTranslateScale )
{
	m_pModel = pModel;
	m_tracks = tracks;
	m_fTranslateScale = fTranslateScale;
	m_additionalModels = additionalModels;

    vector< string > trackNames;
    vector< int > trackIDs;

	for(int i=0; i < additionalModels.GetCount(); ++i)
	{
		if(stricmp(additionalModels[i]->m_modelName.c_str(), pModel->Name.AsString()) == 0)
		{
			trackNames.push_back( string(additionalModels[i]->m_trackId.c_str()) );
			trackIDs.push_back( rexObjectGenericAnimation::TRACK_UNKNOWN );
		}
	}

    trackNames.push_back( string("translate") );
    trackIDs.push_back( rexObjectGenericAnimation::TRACK_TRANSLATE );

    trackNames.push_back( string("rotate") );
    trackIDs.push_back( rexObjectGenericAnimation::TRACK_ROTATE );

	trackNames.push_back( string("scale") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_SCALE );

	//////////////////////////////////////////////////////////////////////////
	// Add these because we want the Camera First Person pivot models to have camera translation and rotation, we strip these in the spec file

	trackNames.push_back( string("cameraTranslation") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_TRANSLATE );

	trackNames.push_back( string("cameraRotation") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_ROTATE );

	//////////////////////////////////////////////////////////////////////////

    InitTracksInfo( fStartTime, fEndTime, nFrames, trackNames, trackIDs );

    AddModelPropertiesToCache();

	tracks = m_tracks;
}

RexMbAnimExportData::~RexMbAnimExportData()
{

}

void RexMbAnimExportData::ReadFrameData()
{
    FBMatrix mbLocalTransform;
    m_pModel->GetMatrix(mbLocalTransform, kModelTransformation, false);

    Matrix34 rageLocalTransform;
    FBMatrixToRageMatrix34(mbLocalTransform, rageLocalTransform);

    int nTracks = m_tracks.GetCount();
    for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
    {
        GetTrackFrameData( *(m_tracks[trackIdx]), rageLocalTransform );
    }
}

void RexMbAnimExportData::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
    HFBAnimationNode pAnimNode = m_pModel->AnimationNodeInGet();

    TimeList tlUniqueTimes;

    // Translate
    HFBAnimationNode pTransNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Translation" );
    FBVector3d trans = m_pModel->Translation;

    HFBAnimationNode pTransXNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "X" );
    int iKeyCountX = 0;
    if ( pTransXNode->FCurve )
    {
        iKeyCountX = pTransXNode->FCurve->Keys.GetCount();
    }

    KeyList klTranX;
    if ( iKeyCountX == 0 )
    {
        klTranX.AddKey( 0, trans[0] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountX; ++i )
        {
            FBTime time = pTransXNode->FCurve->Keys[i].Time;
            klTranX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransXNode->FCurve->Keys[i].Value );
        }
    }

    HFBAnimationNode pTransYNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Y" );
    int iKeyCountY = 0;
    if( pTransYNode->FCurve )
    {
        iKeyCountY = pTransYNode->FCurve->Keys.GetCount();
    }

    KeyList klTranY;
    if ( iKeyCountY == 0 )
    {
        klTranY.AddKey( 0, trans[1] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountY; ++i )
        {
            FBTime time = pTransYNode->FCurve->Keys[i].Time;
            klTranY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransYNode->FCurve->Keys[i].Value );
        }
    }

    HFBAnimationNode pTransZNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Z" );
    int iKeyCountZ = 0;
    if ( pTransYNode->FCurve )
    {
        iKeyCountZ = pTransZNode->FCurve->Keys.GetCount();
    }

    KeyList klTranZ;
    if ( iKeyCountZ == 0 )
    {
        klTranZ.AddKey( 0.0f, trans[2] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountZ; ++i )
        {
            FBTime time = pTransZNode->FCurve->Keys[i].Time;
            klTranZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransZNode->FCurve->Keys[i].Value );
        }
    }

    // Rotate
    HFBAnimationNode pRotNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Rotation" );
    FBVector3d rot = m_pModel->Rotation;


    HFBAnimationNode pRotXNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "X" );
    int iKeyCountRX = 0;
    if ( pRotXNode->FCurve )
    {
        iKeyCountRX = pRotXNode->FCurve->Keys.GetCount();
    }

    KeyList klRotX;
    if ( iKeyCountRX == 0 )
    {
        klRotX.AddKey( 0, rot[0] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountRX; ++i )
        {
            FBTime time = pRotXNode->FCurve->Keys[i].Time;
            klRotX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotXNode->FCurve->Keys[i].Value );
        }
    }

    HFBAnimationNode pRotYNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Y" );
    int iKeyCountRY = 0;
    if ( pRotYNode->FCurve )
    {
        iKeyCountRY = pRotYNode->FCurve->Keys.GetCount();
    }

    KeyList klRotY;
    if ( iKeyCountRY == 0 )
    {
        klRotY.AddKey( 0, rot[1] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountRY; ++i )
        {
            FBTime time = pRotYNode->FCurve->Keys[i].Time;
            klRotY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotYNode->FCurve->Keys[i].Value );
        }
    }

    HFBAnimationNode pRotZNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Z" );
    int iKeyCountRZ = 0;
    if ( pRotZNode->FCurve )
    {
        iKeyCountRZ = pRotZNode->FCurve->Keys.GetCount();
    }

    KeyList klRotZ;  
    if ( iKeyCountRZ == 0 )
    {
        klRotZ.AddKey( 0, rot[2] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountRZ; ++i )
        {
            FBTime time = pRotZNode->FCurve->Keys[i].Time;
            klRotZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotZNode->FCurve->Keys[i].Value );
        }
    }

    int iStartFrame = (int)(fStartTime * rexMBAnimExportCommon::GetFPS());
    for ( int i = 0; i < nFrames; ++i )
    {
        FBQuaternion quatOut;
        FBRVector vecIn;

        vecIn = FBRVector( klRotX.GetValue(iStartFrame + i), klRotY.GetValue(iStartFrame + i), klRotZ.GetValue(iStartFrame + i) );

        Matrix34 mat;
        Quaternion quat;
        Vector3 vec;

        FBRotationToQuaternion( quatOut, vecIn );
        quat.x = quatOut[0];
        quat.y = quatOut[1];
        quat.z = quatOut[2];
        quat.w = quatOut[3];		

        vec.x = klTranX.GetValue(iStartFrame + i);
        vec.y = klTranY.GetValue(iStartFrame + i);
        vec.z = klTranZ.GetValue(iStartFrame + i);

        Matrix34 m;
        m.Identity();
        m.FromQuaternion( quat );
        m.d = vec;

        int nTracks = m_tracks.GetCount();
        for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
        {
            GetTrackFrameData( *(m_tracks[trackIdx]), m );
        }
    }
}

void RexMbAnimExportData::GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 & /*m*/ )
{
    switch(track.m_TrackID)
    {
    case rexObjectGenericAnimation::TRACK_TRANSLATE:
        {
			FBVector3d mbTransVector;
            m_pModel->GetVector(mbTransVector, kModelTranslation, false);

            Vector3 translate( mbTransVector[0], mbTransVector[1], mbTransVector[2] );
            if ( m_fTranslateScale != 1.0f )
            {
                translate *= m_fTranslateScale;
            }
		
            track.AppendVector3Key( translate );
        }
        break;
    case rexObjectGenericAnimation::TRACK_ROTATE:
        {
            FBVector3d mbRotationVector;
            m_pModel->GetVector(mbRotationVector, kModelRotation, false);

            //MotionBuilder seems to sometimes report rotation vectors of (1.#QUAN0,1.#QUAN0,1.#QUAN0) which
            //if exported as quaternions will cause a the animation runtime to crash due to feeding in garbage
            //data... So the best we can do in this case is set the rotation vector to (0,0,0), which seems to 
            //be what MB does when displaying the values in the property GUI for those cases.
            if( (mbRotationVector[0] != mbRotationVector[0]) &&
                (mbRotationVector[1] != mbRotationVector[1]) &&
                (mbRotationVector[2] != mbRotationVector[2]) )
            {
                mbRotationVector[0] = 0.0f;
                mbRotationVector[1] = 0.0f;
                mbRotationVector[2] = 0.0f;
            }

            Quaternion q;
            q.FromEulers( Vector3(mbRotationVector[0] * DtoR, mbRotationVector[1] * DtoR, mbRotationVector[2] * DtoR), m_propertyCache.m_rotOrder.c_str() );

            track.AppendQuaternionKey(q);	
        }
        break;
    case rexObjectGenericAnimation::TRACK_SCALE:
        {
            FBVector3d mbScaleVector;
            m_pModel->GetVector( mbScaleVector, kModelScaling, false);
            track.AppendVector3Key( Vector3( mbScaleVector[0], mbScaleVector[1], mbScaleVector[2] ) );
        }
        break;
	case rexObjectGenericAnimation::TRACK_UNKNOWN:
		{
			for(int i=0; i < m_additionalModels.GetCount(); ++i)
			{
				if(stricmp(m_additionalModels[i]->m_trackId.c_str(), track.m_Name.c_str()) == 0)
				{
					double data=0;
					FBPropertyDouble* prop = (FBPropertyDouble*)m_pModel->PropertyList.Find(m_additionalModels[i]->m_attributeName.c_str());
					if(prop != NULL)
					{
						prop->GetData(&data, sizeof(data));
					}

					track.AppendFloatKey( (float)data );
				}
			}	
		}
		break;
    }
}

//#############################################################################

//#############################################################################

RexMbAnimExportDataCamera::RexMbAnimExportDataCamera( HFBModel pModel, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
										 float fStartTime, float fEndTime, int nFrames, bool bEnableDOF, float fTranslateScale )
										 //: m_pModel(pModel)
										 //, m_tracks(tracks)
										 //, m_fTranslateScale(fTranslateScale)
{
	m_pModel = pModel;
	m_tracks = tracks;
	m_fTranslateScale = fTranslateScale;

	vector< string > trackNames;
	vector< int > trackIDs;

	trackNames.push_back( string("cameraTranslation") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_TRANSLATE );

	trackNames.push_back( string("cameraRotation") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_ROTATE );

	trackNames.push_back( string("cameraFOV") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_UNKNOWN );

	if(bEnableDOF)
	{
		trackNames.push_back( string("cameraDepthOfFieldNearOutOfFocusPlane") );
		trackIDs.push_back( rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT );

		trackNames.push_back( string("cameraDepthOfFieldNearInFocusPlane") );
		trackIDs.push_back( rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN );

		trackNames.push_back( string("cameraDepthOfFieldFarOutOfFocusPlane") );
		trackIDs.push_back( rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT );

		trackNames.push_back( string("cameraDepthOfFieldFarInFocusPlane") );
		trackIDs.push_back( rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN );
	}

	InitTracksInfo( fStartTime, fEndTime, nFrames, trackNames, trackIDs );

	AddModelPropertiesToCache();

	char cName[512];
	safecpy( cName, pModel->Name.AsString() );

	char cNearPlaneName[1024];
	sprintf( cNearPlaneName, "%s NearPlane", cName );

	char cFarPlaneName[1024];
	sprintf( cFarPlaneName, "%s FarPlane", cName );

	char cNearDOFStrengthPlaneName[1024];
	sprintf( cNearDOFStrengthPlaneName, "%s DOF_Strength_NearPlane", cName );

	char cFarDOFStrengthPlaneName[1024];
	sprintf( cFarDOFStrengthPlaneName, "%s DOF_Strength_FarPlane", cName );

	m_pNearPlaneModel = RAGEFindModelByName( cNearPlaneName );
	m_pFarPlaneModel = RAGEFindModelByName( cFarPlaneName );
	m_pNearDOFStrengthPlaneModel = RAGEFindModelByName( cNearDOFStrengthPlaneName );
	m_pFarDOFStrengthPlaneModel = RAGEFindModelByName( cFarDOFStrengthPlaneName );

	tracks = m_tracks;
}

RexMbAnimExportDataCamera::~RexMbAnimExportDataCamera()
{

}

void RexMbAnimExportDataCamera::ReadFrameData()
{
	FBMatrix mbLocalTransform;
	m_pModel->GetMatrix(mbLocalTransform, kModelTransformation, false);

	Matrix34 rageLocalTransform;
	FBMatrixToRageMatrix34(mbLocalTransform, rageLocalTransform);

	//////////////////////////////////////////////////////////////////////////
	FBVector3d mbRotationVector;
	m_pModel->GetVector(mbRotationVector, kModelRotation, false);

	FBVector3d mbTranslationVector;
	m_pModel->GetVector(mbTranslationVector, kModelTranslation, false);

	Quaternion q;
	q.FromEulers( Vector3(mbRotationVector[0] * DtoR, mbRotationVector[1] * DtoR, mbRotationVector[2] * DtoR), m_propertyCache.m_rotOrder.c_str() );

	rageLocalTransform.FromQuaternion(q);
	rageLocalTransform.d = Vector3(mbTranslationVector[0], mbTranslationVector[1], mbTranslationVector[2]);
	//////////////////////////////////////////////////////////////////////////

	int nTracks = m_tracks.GetCount();
	for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
	{
		GetTrackFrameData( *(m_tracks[trackIdx]), rageLocalTransform );
	}
}

void RexMbAnimExportDataCamera::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
	HFBAnimationNode pAnimNode = m_pModel->AnimationNodeInGet();

	TimeList tlUniqueTimes;

	// Translate
	HFBAnimationNode pTransNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Translation" );
	FBVector3d trans = m_pModel->Translation;

	HFBAnimationNode pTransXNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "X" );
	int iKeyCountX = 0;
	if ( pTransXNode->FCurve )
	{
		iKeyCountX = pTransXNode->FCurve->Keys.GetCount();
	}

	KeyList klTranX;
	if ( iKeyCountX == 0 )
	{
		klTranX.AddKey( 0, trans[0] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountX; ++i )
		{
			FBTime time = pTransXNode->FCurve->Keys[i].Time;
			klTranX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransXNode->FCurve->Keys[i].Value );
		}
	}

	HFBAnimationNode pTransYNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Y" );
	int iKeyCountY = 0;
	if( pTransYNode->FCurve )
	{
		iKeyCountY = pTransYNode->FCurve->Keys.GetCount();
	}

	KeyList klTranY;
	if ( iKeyCountY == 0 )
	{
		klTranY.AddKey( 0, trans[1] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountY; ++i )
		{
			FBTime time = pTransYNode->FCurve->Keys[i].Time;
			klTranY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransYNode->FCurve->Keys[i].Value );
		}
	}

	HFBAnimationNode pTransZNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Z" );
	int iKeyCountZ = 0;
	if ( pTransYNode->FCurve )
	{
		iKeyCountZ = pTransZNode->FCurve->Keys.GetCount();
	}

	KeyList klTranZ;
	if ( iKeyCountZ == 0 )
	{
		klTranZ.AddKey( 0.0f, trans[2] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountZ; ++i )
		{
			FBTime time = pTransZNode->FCurve->Keys[i].Time;
			klTranZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransZNode->FCurve->Keys[i].Value );
		}
	}

	// Rotate
	HFBAnimationNode pRotNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Rotation" );
	FBVector3d rot = m_pModel->Rotation;


	HFBAnimationNode pRotXNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "X" );
	int iKeyCountRX = 0;
	if ( pRotXNode->FCurve )
	{
		iKeyCountRX = pRotXNode->FCurve->Keys.GetCount();
	}

	KeyList klRotX;
	if ( iKeyCountRX == 0 )
	{
		klRotX.AddKey( 0, rot[0] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountRX; ++i )
		{
			FBTime time = pRotXNode->FCurve->Keys[i].Time;
			klRotX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotXNode->FCurve->Keys[i].Value );
		}
	}

	HFBAnimationNode pRotYNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Y" );
	int iKeyCountRY = 0;
	if ( pRotYNode->FCurve )
	{
		iKeyCountRY = pRotYNode->FCurve->Keys.GetCount();
	}

	KeyList klRotY;
	if ( iKeyCountRY == 0 )
	{
		klRotY.AddKey( 0, rot[1] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountRY; ++i )
		{
			FBTime time = pRotYNode->FCurve->Keys[i].Time;
			klRotY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotYNode->FCurve->Keys[i].Value );
		}
	}

	HFBAnimationNode pRotZNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Z" );
	int iKeyCountRZ = 0;
	if ( pRotZNode->FCurve )
	{
		iKeyCountRZ = pRotZNode->FCurve->Keys.GetCount();
	}

	KeyList klRotZ;  
	if ( iKeyCountRZ == 0 )
	{
		klRotZ.AddKey( 0, rot[2] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountRZ; ++i )
		{
			FBTime time = pRotZNode->FCurve->Keys[i].Time;
			klRotZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotZNode->FCurve->Keys[i].Value );
		}
	}

	int iStartFrame = (int)(fStartTime * rexMBAnimExportCommon::GetFPS());
	for ( int i = 0; i < nFrames; ++i )
	{
		FBQuaternion quatOut;
		FBRVector vecIn;

		vecIn = FBRVector( klRotX.GetValue(iStartFrame + i), klRotY.GetValue(iStartFrame + i), klRotZ.GetValue(iStartFrame + i) );

		Matrix34 mat;
		Quaternion quat;
		Vector3 vec;

		FBRotationToQuaternion( quatOut, vecIn );
		quat.x = quatOut[0];
		quat.y = quatOut[1];
		quat.z = quatOut[2];
		quat.w = quatOut[3];		

		vec.x = klTranX.GetValue(iStartFrame + i);
		vec.y = klTranY.GetValue(iStartFrame + i);
		vec.z = klTranZ.GetValue(iStartFrame + i);

		Matrix34 m;
		m.Identity();
		m.FromQuaternion( quat );
		m.d = vec;

		int nTracks = m_tracks.GetCount();
		for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
		{
			GetTrackFrameData( *(m_tracks[trackIdx]), m );
		}
	}
}


void RexMbAnimExportDataCamera::GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 &m )
{
	Matrix34 mCam = m;

	mCam.d *= MOTIONBUILDER_UNIT_SCALE;

	Vector3 upVector = Vector3( 0.0f, 1.0f, 0.0f );
	Vector3 upCameraVector;

	// transform up vector into camera space
	mCam.Transform( upVector, upCameraVector );
	upCameraVector = upCameraVector - mCam.d;
	upCameraVector.Normalize();

	switch(track.m_TrackID)
	{
	case rexObjectGenericAnimation::TRACK_TRANSLATE:
		{
			HFBCamera camera = (HFBCamera)m_pModel;

			if(camera->Interest)
			{
				FBMatrix mbLocalTransform;
				camera->Interest->GetMatrix(mbLocalTransform, kModelTransformation, false);
				
				Matrix34 rageLocalTransform;
				FBMatrixToRageMatrix34(mbLocalTransform, rageLocalTransform);
				rageLocalTransform.d *= MOTIONBUILDER_UNIT_SCALE;
				
				mCam.Identity3x3();
#if HACK_GTA4
				rexMBAnimExportCommon::LookAt( rageLocalTransform.d, mCam, upCameraVector );
#else
				mCam.LookAt( rageLocalTransform.d, upCameraVector );
#endif // HACK_GTA4
			}
			else
			{
				// Create a fake interest, our camera needs some kind of point of interest for the direction
				Vector3 forwardVector(1.0f,0.0f,0.0f);
				Vector3 forwardCameraVector;
				Vector3 forwardWorldVector;

				Matrix34 camMatrix = mCam;
				camMatrix.Transform(forwardVector, forwardCameraVector); // transfer into camera space
				Matrix34 worldMatrix;
				worldMatrix.Identity();
				worldMatrix.UnTransform(forwardCameraVector, forwardWorldVector); // transfer into world space

				rexMBAnimExportCommon::LookAt( forwardWorldVector, mCam );
			}

#if HACK_GTA4
			mCam.RotateFullY(PI);

			float y = mCam.d.y;
			mCam.d.x = -mCam.d.x;
			mCam.d.y = mCam.d.z;
			mCam.d.z = y;

			atString atNewOrigin;
			if(rexMBAnimExportCommon::GetNewOrigin(atNewOrigin))
			{
				HFBModel originModel = RAGEFindModelByName((char*)atNewOrigin.c_str());
				if(originModel != NULL)
				{
					// Since Eulers are a cunt we cant just remove the parents rotation via matrices, so 
					// I have to add this hack to remove the rotation from the model then add it again later
					// this then drops down to the parent so I can retrieve values without the dummy rotation
					//////////////////////////////////////////////////////////////////////////
					std::string sOriginDummyName = atNewOrigin.c_str();
					int colonPos = (int)sOriginDummyName.find_last_of(":");
					sOriginDummyName = sOriginDummyName.substr(0,colonPos);
					sOriginDummyName = sOriginDummyName.append(":Dummy01");
					
					FBSystem system;
					FBVector3d originalVector;
					HFBModel originModelParent = RAGEFindModelByName(sOriginDummyName.c_str());
					if(originModelParent != NULL)
					{
						originModelParent->GetVector(originalVector,kModelRotation,false);
						originModelParent->SetVector(FBVector3d(0,0,0),kModelRotation,false);
						system.Scene->Evaluate();
					}
					//////////////////////////////////////////////////////////////////////////

					FBMatrix originMatrixFB;
					originModel->GetMatrix(originMatrixFB, kModelTransformation, true);

					//////////////////////////////////////////////////////////////////////////
					if(originModelParent != NULL)
					{
						originModelParent->SetVector(originalVector,kModelRotation,false);
						system.Scene->Evaluate();
					}
					//////////////////////////////////////////////////////////////////////////

					Matrix34 originMatrixRage;
					originMatrixRage.Identity();
					FBMatrixToRageMatrix34(originMatrixFB, originMatrixRage);
					originMatrixRage.Normalize();
					originMatrixRage.d *= MOTIONBUILDER_UNIT_SCALE;

					mCam.DotTranspose(originMatrixRage);
				}
			}

			Vector3 translate = mCam.d;
#else
			Vector3 translate = mCam.d;
#endif

			track.AppendVector3Key( translate );
		}
		break;
	case rexObjectGenericAnimation::TRACK_ROTATE:
		{
			HFBCamera camera = (HFBCamera)m_pModel;

			if(camera->Interest)
			{
				FBMatrix mbLocalTransform;
				camera->Interest->GetMatrix(mbLocalTransform, kModelTransformation, false);

				Matrix34 rageLocalTransform;
				FBMatrixToRageMatrix34(mbLocalTransform, rageLocalTransform);
				rageLocalTransform.d *= MOTIONBUILDER_UNIT_SCALE;

				// transformation matrix
				mCam.Identity3x3();
#if HACK_GTA4
				rexMBAnimExportCommon::LookAt( rageLocalTransform.d, mCam, upCameraVector );
#else
				mCam.LookAt( rageLocalTransform.d );
#endif // HACK_GTA4
			}
			else
			{
				// Create a fake interest, our camera needs some kind of point of interest for the direction
				Vector3 forwardVector(1.0f,0.0f,0.0f);
				Vector3 forwardCameraVector;
				Vector3 forwardWorldVector;

				Matrix34 camMatrix = mCam;
				camMatrix.Transform(forwardVector, forwardCameraVector); // transfer into camera space
				Matrix34 worldMatrix;
				worldMatrix.Identity();
				worldMatrix.UnTransform(forwardCameraVector, forwardWorldVector); // transfer into world space

				rexMBAnimExportCommon::LookAt( forwardWorldVector, mCam, upCameraVector );
			}

#if HACK_GTA4

			// Add roll
			mCam.RotateLocalZ( camera->Roll * DtoR );

			// Fix up the cameras otherwise they are 180 the wrong way in-game (previously fixed up at runtime by script)
			mCam.RotateFullY(PI);

			Quaternion quatYUp;
			Quaternion quatZUp;
			mCam.ToQuaternion(quatYUp);
			quatZUp.x = -quatYUp.x;
			quatZUp.y = quatYUp.z;
			quatZUp.z = quatYUp.y;
			quatZUp.w = quatYUp.w;
			mCam.FromQuaternion(quatZUp);

			atString atNewOrigin;
			if(rexMBAnimExportCommon::GetNewOrigin(atNewOrigin))
			{
				HFBModel originModel = RAGEFindModelByName((char*)atNewOrigin.c_str());
				if(originModel != NULL)
				{
					// Since Eulers are a cunt we cant just remove the parents rotation via matrices, so 
					// I have to add this hack to remove the rotation from the model then add it again later
					// this then drops down to the parent so I can retrieve values without the dummy rotation
					//////////////////////////////////////////////////////////////////////////
					std::string sOriginDummyName = atNewOrigin.c_str();
					int colonPos = (int)sOriginDummyName.find_last_of(":");
					sOriginDummyName = sOriginDummyName.substr(0,colonPos);
					sOriginDummyName = sOriginDummyName.append(":Dummy01");

					FBSystem system;
					FBVector3d originalVector;
					HFBModel originModelParent = RAGEFindModelByName(sOriginDummyName.c_str());
					if(originModelParent != NULL)
					{
						originModelParent->GetVector(originalVector,kModelRotation,false);
						originModelParent->SetVector(FBVector3d(0,0,0),kModelRotation,false);
						system.Scene->Evaluate();
					}
					//////////////////////////////////////////////////////////////////////////

					FBMatrix originMatrixFB;
					originModel->GetMatrix(originMatrixFB, kModelTransformation, true);

					//////////////////////////////////////////////////////////////////////////
					if(originModelParent != NULL)
					{
						originModelParent->SetVector(originalVector,kModelRotation,false);
						system.Scene->Evaluate();
					}
					//////////////////////////////////////////////////////////////////////////

					Matrix34 originMatrixRage;
					originMatrixRage.Identity();
					FBMatrixToRageMatrix34(originMatrixFB, originMatrixRage);
					originMatrixRage.d *= MOTIONBUILDER_UNIT_SCALE;

					mCam.DotTranspose(originMatrixRage);
				}
			}

			Quaternion q;
			mCam.ToQuaternion( q );
#else
			Quaternion q;
			mCam.ToQuaternion( q );
#endif

			q.Normalize();
			track.AppendQuaternionKey(q);	
		}
		break;
	case rexObjectGenericAnimation::TRACK_UNKNOWN:
		{
			HFBCamera camera = (HFBCamera)m_pModel;

			track.AppendFloatKey((float)camera->FieldOfView);
		}
		break;
	case rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT:
		{
			float fFarPlaneDistance = 175.0f;
			if(m_pFarPlaneModel)
			{
				FBVector3d trans = m_pFarDOFStrengthPlaneModel->Translation;
				fFarPlaneDistance = trans[0] * MOTIONBUILDER_UNIT_SCALE;
			}

			track.AppendFloatKey(fFarPlaneDistance);
		}
		break;
	case rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN:
		{
			float fFarOutPlaneDistance = 150.0f;
			if(m_pFarDOFStrengthPlaneModel)
			{
				FBVector3d trans = m_pFarPlaneModel->Translation;
				fFarOutPlaneDistance = trans[0] * MOTIONBUILDER_UNIT_SCALE;
			}

			track.AppendFloatKey(fFarOutPlaneDistance);
		}
		break;
	case rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN:
		{
			float fNearPlaneDistance = 0.1f;
			if(m_pNearPlaneModel)
			{
				FBVector3d trans = m_pNearPlaneModel->Translation;
				fNearPlaneDistance = trans[0] * MOTIONBUILDER_UNIT_SCALE;
			}

			track.AppendFloatKey(fNearPlaneDistance);
		}
		break;
	case rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT:
		{
			float fNearOutPlaneDistance = 0.0f;
			if(m_pNearDOFStrengthPlaneModel)
			{
				FBVector3d trans = m_pNearDOFStrengthPlaneModel->Translation;
				fNearOutPlaneDistance = trans[0] * MOTIONBUILDER_UNIT_SCALE;
			}

			track.AppendFloatKey(fNearOutPlaneDistance);
		}
		break;
	}
}

RexMbAnimExportDataFacial::RexMbAnimExportDataFacial( HFBModel pModel, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
													 float fStartTime, float fEndTime, int nFrames, float fTranslateScale )
													 : m_ModelTint(NULL)
													 //: m_pModel(pModel)
													 //, m_tracks(tracks)
													 //, m_fTranslateScale(fTranslateScale)
{
	m_pModel = pModel;
	m_tracks = tracks;
	m_fTranslateScale = fTranslateScale;

	vector< string > trackNames;
	vector< int > trackIDs;

	trackNames.push_back( string("facialTranslation") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_TRANSLATE );

	trackNames.push_back( string("facialRotation") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_ROTATE );

	trackNames.push_back( string("facialScale") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_SCALE );

	m_pPropertyViseme = m_pModel->PropertyList.Find("viseme");
	if(m_pPropertyViseme)
	{
		trackNames.push_back( string("viseme") );
		trackIDs.push_back( rexObjectGenericAnimation::TRACK_UNKNOWN );
	}

	// Currently only used for cutscenes, this grabs the joystick model and then pulls out the Y axis value later.

	char cModel[RAGE_MAX_PATH];
	sprintf(cModel, "%s:cutsceneBlush_OFF", rexMBAnimExportCommon::GetNameSpaceFromModel(m_pModel).c_str());

	m_ModelTint = RAGEFindModelByName(cModel);
	if(m_ModelTint)
	{
		trackNames.push_back( string("facialTinting") );
		trackIDs.push_back( rexObjectGenericAnimation::TRACK_UNKNOWN );
	}

	InitTracksInfo( fStartTime, fEndTime, nFrames, trackNames, trackIDs );

	AddModelPropertiesToCache();

	tracks = m_tracks;
}

RexMbAnimExportDataFacial::~RexMbAnimExportDataFacial()
{

}

void RexMbAnimExportDataFacial::ReadFrameData()
{
	FBMatrix mbLocalTransform;
	m_pModel->GetMatrix(mbLocalTransform, kModelTransformation, false);

	Matrix34 rageLocalTransform;
	FBMatrixToRageMatrix34(mbLocalTransform, rageLocalTransform);

	int nTracks = m_tracks.GetCount();
	for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
	{
		GetTrackFrameData( *(m_tracks[trackIdx]), rageLocalTransform );
	}
}

void RexMbAnimExportDataFacial::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
	HFBAnimationNode pAnimNode = m_pModel->AnimationNodeInGet();

	TimeList tlUniqueTimes;

	// Translate
	HFBAnimationNode pTransNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Translation" );
	FBVector3d trans = m_pModel->Translation;

	HFBAnimationNode pTransXNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "X" );
	int iKeyCountX = 0;
	if ( pTransXNode->FCurve )
	{
		iKeyCountX = pTransXNode->FCurve->Keys.GetCount();
	}

	KeyList klTranX;
	if ( iKeyCountX == 0 )
	{
		klTranX.AddKey( 0, trans[0] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountX; ++i )
		{
			FBTime time = pTransXNode->FCurve->Keys[i].Time;
			klTranX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransXNode->FCurve->Keys[i].Value );
		}
	}

	HFBAnimationNode pTransYNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Y" );
	int iKeyCountY = 0;
	if( pTransYNode->FCurve )
	{
		iKeyCountY = pTransYNode->FCurve->Keys.GetCount();
	}

	KeyList klTranY;
	if ( iKeyCountY == 0 )
	{
		klTranY.AddKey( 0, trans[1] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountY; ++i )
		{
			FBTime time = pTransYNode->FCurve->Keys[i].Time;
			klTranY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransYNode->FCurve->Keys[i].Value );
		}
	}

	HFBAnimationNode pTransZNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Z" );
	int iKeyCountZ = 0;
	if ( pTransYNode->FCurve )
	{
		iKeyCountZ = pTransZNode->FCurve->Keys.GetCount();
	}

	KeyList klTranZ;
	if ( iKeyCountZ == 0 )
	{
		klTranZ.AddKey( 0.0f, trans[2] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountZ; ++i )
		{
			FBTime time = pTransZNode->FCurve->Keys[i].Time;
			klTranZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransZNode->FCurve->Keys[i].Value );
		}
	}

	// Rotate
	HFBAnimationNode pRotNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Rotation" );
	FBVector3d rot = m_pModel->Rotation;


	HFBAnimationNode pRotXNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "X" );
	int iKeyCountRX = 0;
	if ( pRotXNode->FCurve )
	{
		iKeyCountRX = pRotXNode->FCurve->Keys.GetCount();
	}

	KeyList klRotX;
	if ( iKeyCountRX == 0 )
	{
		klRotX.AddKey( 0, rot[0] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountRX; ++i )
		{
			FBTime time = pRotXNode->FCurve->Keys[i].Time;
			klRotX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotXNode->FCurve->Keys[i].Value );
		}
	}

	HFBAnimationNode pRotYNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Y" );
	int iKeyCountRY = 0;
	if ( pRotYNode->FCurve )
	{
		iKeyCountRY = pRotYNode->FCurve->Keys.GetCount();
	}

	KeyList klRotY;
	if ( iKeyCountRY == 0 )
	{
		klRotY.AddKey( 0, rot[1] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountRY; ++i )
		{
			FBTime time = pRotYNode->FCurve->Keys[i].Time;
			klRotY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotYNode->FCurve->Keys[i].Value );
		}
	}

	HFBAnimationNode pRotZNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Z" );
	int iKeyCountRZ = 0;
	if ( pRotZNode->FCurve )
	{
		iKeyCountRZ = pRotZNode->FCurve->Keys.GetCount();
	}

	KeyList klRotZ;  
	if ( iKeyCountRZ == 0 )
	{
		klRotZ.AddKey( 0, rot[2] );
	}
	else
	{
		for ( int i = 0; i < iKeyCountRZ; ++i )
		{
			FBTime time = pRotZNode->FCurve->Keys[i].Time;
			klRotZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotZNode->FCurve->Keys[i].Value );
		}
	}

	int iStartFrame = (int)(fStartTime * rexMBAnimExportCommon::GetFPS());
	for ( int i = 0; i < nFrames; ++i )
	{
		FBQuaternion quatOut;
		FBRVector vecIn;

		vecIn = FBRVector( klRotX.GetValue(iStartFrame + i), klRotY.GetValue(iStartFrame + i), klRotZ.GetValue(iStartFrame + i) );

		Matrix34 mat;
		Quaternion quat;
		Vector3 vec;

		FBRotationToQuaternion( quatOut, vecIn );
		quat.x = quatOut[0];
		quat.y = quatOut[1];
		quat.z = quatOut[2];
		quat.w = quatOut[3];		

		vec.x = klTranX.GetValue(iStartFrame + i);
		vec.y = klTranY.GetValue(iStartFrame + i);
		vec.z = klTranZ.GetValue(iStartFrame + i);

		Matrix34 m;
		m.Identity();
		m.FromQuaternion( quat );
		m.d = vec;

		int nTracks = m_tracks.GetCount();
		for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
		{
			GetTrackFrameData( *(m_tracks[trackIdx]), m );
		}
	}
}


void RexMbAnimExportDataFacial::GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 & /*m*/ )
{
	switch(track.m_TrackID)
	{
	case rexObjectGenericAnimation::TRACK_TRANSLATE:
		{
			FBVector3d mbTransVector;
			m_pModel->GetVector(mbTransVector, kModelTranslation, false);

			Vector3 translate( mbTransVector[0], mbTransVector[1], mbTransVector[2] );
			if ( m_fTranslateScale != 1.0f )
			{
				translate *= m_fTranslateScale;
			}

			track.AppendVector3Key( translate );
		}
		break;
	case rexObjectGenericAnimation::TRACK_ROTATE:
		{
			FBVector3d mbRotationVector;
			m_pModel->GetVector(mbRotationVector, kModelRotation, false);

			//MotionBuilder seems to sometimes report rotation vectors of (1.#QUAN0,1.#QUAN0,1.#QUAN0) which
			//if exported as quaternions will cause a the animation runtime to crash due to feeding in garbage
			//data... So the best we can do in this case is set the rotation vector to (0,0,0), which seems to 
			//be what MB does when displaying the values in the property GUI for those cases.
			if( (mbRotationVector[0] != mbRotationVector[0]) &&
				(mbRotationVector[1] != mbRotationVector[1]) &&
				(mbRotationVector[2] != mbRotationVector[2]) )
			{
				mbRotationVector[0] = 0.0f;
				mbRotationVector[1] = 0.0f;
				mbRotationVector[2] = 0.0f;
			}

			Quaternion q;
			q.FromEulers( Vector3(mbRotationVector[0] * DtoR, mbRotationVector[1] * DtoR, mbRotationVector[2] * DtoR), m_propertyCache.m_rotOrder.c_str() );

			track.AppendQuaternionKey(q);	
		}
		break;
	case rexObjectGenericAnimation::TRACK_SCALE:
		{
			FBVector3d mbScaleVector;
			m_pModel->GetVector( mbScaleVector, kModelScaling, false);
			track.AppendVector3Key( Vector3( mbScaleVector[0], mbScaleVector[1], mbScaleVector[2] ) );
		}
		break;
	case rexObjectGenericAnimation::TRACK_UNKNOWN:
		{
			if(m_pPropertyViseme != NULL && (stricmp(track.m_Name.c_str(), "viseme") == 0))
			{
				double data=0;
				m_pPropertyViseme->GetData(&data, sizeof(data));
				track.AppendFloatKey( (float)data );
			}

			if(m_ModelTint != NULL && (stricmp(track.m_Name.c_str(), "facialtinting") == 0))
			{
				FBVector3d mbTransVector;
				m_ModelTint->GetVector(mbTransVector, kModelTranslation, false);

				float data = mbTransVector[1]; // Pull the Y from the vector/joystick
				//data /= 100;

				track.AppendFloatKey( (float)data );
			}
		}
	}
}

////

RexMbAnimExportDataFacialCustomAttribute::RexMbAnimExportDataFacialCustomAttribute( HFBModel pModel, atString attributeName, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
													 float fStartTime, float fEndTime, int nFrames, float fTranslateScale )
													 //: m_pModel(pModel)
													 //, m_tracks(tracks)
													 //, m_fTranslateScale(fTranslateScale)
{
	m_pModel = pModel;
	m_tracks = tracks;
	m_fTranslateScale = fTranslateScale;
	m_customAttributeName = attributeName;

	vector< string > trackNames;
	vector< int > trackIDs;

	trackNames.push_back( string("facialControl") );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_UNKNOWN );

	InitTracksInfo( fStartTime, fEndTime, nFrames, trackNames, trackIDs );

	AddModelPropertiesToCache();

	tracks = m_tracks;
}

RexMbAnimExportDataFacialCustomAttribute::~RexMbAnimExportDataFacialCustomAttribute()
{

}

void RexMbAnimExportDataFacialCustomAttribute::ReadFrameData()
{
	FBMatrix mbLocalTransform;
	m_pModel->GetMatrix(mbLocalTransform, kModelTransformation, false);

	Matrix34 rageLocalTransform;
	FBMatrixToRageMatrix34(mbLocalTransform, rageLocalTransform);

	int nTracks = m_tracks.GetCount();
	for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
	{
		GetTrackFrameData( *(m_tracks[trackIdx]), rageLocalTransform );
	}
}

void RexMbAnimExportDataFacialCustomAttribute::ReadKeyData( float /*fStartTime*/, float /*fEndTime*/, int nFrames )
{
	Matrix34 m;
	for ( int i = 0; i < nFrames; ++i )
	{
		int nTracks = m_tracks.GetCount();
		for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
		{
			GetTrackFrameData( *(m_tracks[trackIdx]), m );
		}
	}
}

void RexMbAnimExportDataFacialCustomAttribute::GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 & /*m*/ )
{
	double data=0;
	
	switch(track.m_TrackID)
	{
	case rexObjectGenericAnimation::TRACK_UNKNOWN:
		{
			FBPropertyDouble* prop = (FBPropertyDouble*)m_pModel->PropertyList.Find(m_customAttributeName.c_str());
			if(prop != NULL)
			{
				data=0;
				prop->GetData(&data, sizeof(data));
				track.AppendFloatKey( (float)data );
			}
		}
		break;
	}
}

////

RexMbAnimExportDataAdditional::RexMbAnimExportDataAdditional( HFBModel pModel, atString trackName, atString attributeName, atArray<rexObjectGenericAnimation::TrackInfo *> &tracks, 
																				   float fStartTime, float fEndTime, int nFrames, float fTranslateScale )
																				   //: m_pModel(pModel)
																				   //, m_tracks(tracks)
																				   //, m_fTranslateScale(fTranslateScale)
{
	m_pModel = pModel;
	m_tracks = tracks;
	m_fTranslateScale = fTranslateScale;
	m_customAttributeName = attributeName;

	// depending on what our data says we add the specific tracks

	vector< string > trackNames;
	vector< int > trackIDs;

	trackNames.push_back( string(trackName.c_str()) );
	trackIDs.push_back( rexObjectGenericAnimation::TRACK_UNKNOWN );

	InitTracksInfo( fStartTime, fEndTime, nFrames, trackNames, trackIDs );

	AddModelPropertiesToCache();

	tracks = m_tracks;
}

RexMbAnimExportDataAdditional::~RexMbAnimExportDataAdditional()
{

}

void RexMbAnimExportDataAdditional::GetTrackFrameData( rexObjectGenericAnimation::TrackInfo& track, const Matrix34 & /*m*/ )
{
	switch(track.m_TrackID)
	{
	case rexObjectGenericAnimation::TRACK_TRANSLATE:
		{
			FBVector3d mbTransVector;
			m_pModel->GetVector(mbTransVector, kModelTranslation, false);

			Vector3 translate( mbTransVector[0], mbTransVector[1], mbTransVector[2] );
			if ( m_fTranslateScale != 1.0f )
			{
				translate *= m_fTranslateScale;
			}

			track.AppendVector3Key( translate );
		}
		break;
	case rexObjectGenericAnimation::TRACK_ROTATE:
		{
			FBVector3d mbRotationVector;
			m_pModel->GetVector(mbRotationVector, kModelRotation, false);

			//MotionBuilder seems to sometimes report rotation vectors of (1.#QUAN0,1.#QUAN0,1.#QUAN0) which
			//if exported as quaternions will cause a the animation runtime to crash due to feeding in garbage
			//data... So the best we can do in this case is set the rotation vector to (0,0,0), which seems to 
			//be what MB does when displaying the values in the property GUI for those cases.
			if( (mbRotationVector[0] != mbRotationVector[0]) &&
				(mbRotationVector[1] != mbRotationVector[1]) &&
				(mbRotationVector[2] != mbRotationVector[2]) )
			{
				mbRotationVector[0] = 0.0f;
				mbRotationVector[1] = 0.0f;
				mbRotationVector[2] = 0.0f;
			}

			Quaternion q;
			q.FromEulers( Vector3(mbRotationVector[0] * DtoR, mbRotationVector[1] * DtoR, mbRotationVector[2] * DtoR), m_propertyCache.m_rotOrder.c_str() );

			track.AppendQuaternionKey(q);	
		}
		break;
	case rexObjectGenericAnimation::TRACK_SCALE:
		{
			FBVector3d mbScaleVector;
			m_pModel->GetVector( mbScaleVector, kModelScaling, false);
			track.AppendVector3Key( Vector3( mbScaleVector[0], mbScaleVector[1], mbScaleVector[2] ) );
		}
		break;
	case rexObjectGenericAnimation::TRACK_UNKNOWN:
		{
			double data=0;
			FBPropertyDouble* prop = (FBPropertyDouble*)m_pModel->PropertyList.Find(m_customAttributeName.c_str());
			if(prop != NULL)
			{
				prop->GetData(&data, sizeof(data));
			}
			
			track.AppendFloatKey( (float)data );
		}
	}
}

void RexMbAnimExportDataAdditional::ReadFrameData()
{
	FBMatrix mbLocalTransform;
	m_pModel->GetMatrix(mbLocalTransform, kModelTransformation, false);

	Matrix34 rageLocalTransform;
	FBMatrixToRageMatrix34(mbLocalTransform, rageLocalTransform);

	int nTracks = m_tracks.GetCount();
	for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
	{
		GetTrackFrameData( *(m_tracks[trackIdx]), rageLocalTransform );
	}
}

void RexMbAnimExportDataAdditional::ReadKeyData( float /*fStartTime*/, float /*fEndTime*/, int nFrames )
{
	Matrix34 m;
	for ( int i = 0; i < nFrames; ++i )
	{
		int nTracks = m_tracks.GetCount();
		for( int trackIdx = 0; trackIdx < nTracks; trackIdx++)
		{
			GetTrackFrameData( *(m_tracks[trackIdx]), m );
		}
	}
}
