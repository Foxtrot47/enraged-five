<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<structdef autoregister="true" type="RexRageCutscenePart">
	<string name="m_name" noInit="true" type="atString"/>

	<string name="m_filename" noInit="true" type="atString"/>
</structdef>

<structdef autoregister="true" type="RexRageCutscenePartFile">
<array name="m_parts" type="atArray">
	<pointer policy="owner" type="RexRageCutscenePart"/>
</array>
<string name="m_path" noInit="true" type="atString"/>
<string name="m_audio" noInit="true" type="atString"/>
<s32 init="0" name="m_sectionMethodIndex"/>
<s32 init="0" name="m_sectionTimeDuration"/>
</structdef>

</ParserSchema>