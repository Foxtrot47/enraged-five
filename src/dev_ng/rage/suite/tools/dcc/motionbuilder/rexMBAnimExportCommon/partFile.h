#pragma once

using namespace rage;

#include "parser/manager.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfevent.h"
#include "rexMBAnimExportCommon/utility.h"

namespace rage {

class RexRageCutscenePart
{
public:
	RexRageCutscenePart();

	RexRageCutscenePart(cutfCutsceneFile2* pMaster, const char* name ):
		m_export(true)
	{
		m_pMasterCutfile = pMaster;
		m_name = name;
		m_cutfile.SetRangeStart(rexMBAnimExportCommon::GetAnimStartTime()*rexMBAnimExportCommon::GetFPS());
		m_cutfile.SetRangeEnd(rexMBAnimExportCommon::GetAnimEndTime()*rexMBAnimExportCommon::GetFPS());
	}

	virtual ~RexRageCutscenePart();

	bool IsExportable();
	void UpdateObjects();
	void SetMaster(cutfCutsceneFile2* pMaster);
	const char* GetName();
	cutfCutsceneFile2* GetCutsceneFile();
	void SetCutsceneFileName(const char* pFilename);
	const char* GetCutsceneFileName();
	bool SaveFile(const char* pFilename);
	void SetCanExport(bool bExport);
	RexRageCutscenePart* Clone();
	bool ValidateDecalFrames();

	PAR_PARSABLE;

private:

	atString m_name;
	atString m_filename;
	cutfCutsceneFile2* m_pMasterCutfile;
	cutfCutsceneFile2 m_cutfile;
	bool m_export;
};

inline void RexRageCutscenePart::SetCanExport(bool bExport)
{
	m_export = bExport;
}

inline const char* RexRageCutscenePart::GetCutsceneFileName()
{
	return m_filename.c_str();
}

inline void RexRageCutscenePart::SetCutsceneFileName(const char* pFilename)
{
	m_filename = pFilename;
}

inline cutfCutsceneFile2* RexRageCutscenePart::GetCutsceneFile()
{
	return &m_cutfile;
}

inline const char* RexRageCutscenePart::GetName()
{
	return m_name.c_str();
}

inline void RexRageCutscenePart::SetMaster(cutfCutsceneFile2* pMaster)
{
	m_pMasterCutfile = pMaster;
}

inline bool RexRageCutscenePart::IsExportable()
{
	return m_export;
}

class RexRageCutscenePartFile
{
public:
	RexRageCutscenePartFile();
	virtual ~RexRageCutscenePartFile(){ Clear(); };

	void AddPart(RexRageCutscenePart* pItem);
	void DeletePart(s32 iIndex);

	RexRageCutscenePart* GetPart(s32 iIndex);
	s32 GetNumParts();

	void Clear();
	void CopyFrom(const RexRageCutscenePartFile& partFile);

	bool LoadFile(const char* pFilename, bool bValidate=true);
	bool SaveFile(const char* pFilename);

	void SetFolder(const char* pFilename);
	void SetAudio(const char* pAudio);

	void SetSectionMethodIndex(int iSectionMethodIndex);
	void SetSectionMethodDuration(int iSectionMethodDuration);

	const char* GetFolder();
	const char* GetAudio();

	int GetSectionMethodIndex();
	int GetSectionMethodDuration();

	bool MoveUp(s32 index);
	bool MoveDown(s32 index);

	PAR_PARSABLE;

protected:

	atArray<RexRageCutscenePart *> m_parts;
	atString m_path;
	atString m_audio;
	int m_sectionMethodIndex;
	int m_sectionTimeDuration;
};

inline const char* RexRageCutscenePartFile::GetAudio()
{
	return m_audio.c_str();
}

inline const char* RexRageCutscenePartFile::GetFolder()
{
	return m_path.c_str();
}

inline int RexRageCutscenePartFile::GetSectionMethodIndex()
{
	return m_sectionMethodIndex;
}

inline int RexRageCutscenePartFile::GetSectionMethodDuration()
{
	return m_sectionTimeDuration;
}

inline bool RexRageCutscenePartFile::MoveUp(s32 index)
{
	if(index > 0 && index < m_parts.GetCount())
	{
		RexRageCutscenePart* pTemp = m_parts[index];
		m_parts[index] = m_parts[index-1];
		m_parts[index-1] = pTemp;
		return true;
	}
	return false;
}

inline bool RexRageCutscenePartFile::MoveDown(s32 index)
{
	if(index >= 0 && index < m_parts.GetCount()-1)
	{
		RexRageCutscenePart* pTemp = m_parts[index+1];
		m_parts[index+1] = m_parts[index];
		m_parts[index] = pTemp;
		return true;
	}
	return false;
}

inline void RexRageCutscenePartFile::SetFolder(const char* pFilename)
{
	m_path = pFilename;
}

inline void RexRageCutscenePartFile::SetAudio(const char* pAudio)
{
	m_audio = pAudio;
}

inline void RexRageCutscenePartFile::SetSectionMethodIndex(int iSectionMethodIndex)
{
	m_sectionMethodIndex = iSectionMethodIndex;
}

inline void RexRageCutscenePartFile::SetSectionMethodDuration(int iSectionMethodDuration)
{
	m_sectionTimeDuration = iSectionMethodDuration;
}

inline s32 RexRageCutscenePartFile::GetNumParts()
{
	return m_parts.GetCount();
}

inline RexRageCutscenePart* RexRageCutscenePartFile::GetPart(s32 iIndex)
{
	return m_parts[iIndex];
}

inline void RexRageCutscenePartFile::Clear()
{
	for(int i=0; i < m_parts.GetCount(); ++i)
	{
		if(m_parts[i])
			delete m_parts[i];
	}

	m_parts.clear();
}

} // namespace rage