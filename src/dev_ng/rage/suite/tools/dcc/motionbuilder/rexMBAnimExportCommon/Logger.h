#ifndef __REXMBRAGE_LOGGER_H__
#define __REXMBRAGE_LOGGER_H__

#include "Utility.h"
#include "RsULog/ULogger.h"

#define ULOGGER_MOBO CLogger::GetInstance()

// Derived ULOGGER, contains motionbuilder specific functionality for progress bar.
class CLogger : public UniversalLogHelper
{	
private:
	CLogger();

	static CLogger *_instance;

	FBProgress *m_pProgressBar;
	atString m_strLogFilename;

	// Used for success messagebox
	atString m_strTitle;
	atString m_strMessage;

public:
	virtual ~CLogger(){};

	// PURPOSE: Returns the static singleton instance.
	static CLogger& GetInstance();

	// PURPOSE: Readies our progress meter and log file stream before export.
	// PARAMS:
	//    pLogFilename - the file name of the log to create, if any.
	void PreExport( const char *pLogFilename, const char* pContext );

	// PURPOSE: Frees our progress meter and closes the log file stream after export.
	// PARAMS:
	//    bShowDialog - If true display the ulog viewer, else don't.
	void PostExport( bool bShowDialog=true, bool bResult=false, const char* ptrTitle=NULL, const char* ptrMessage=NULL );

	void PostExport( const char* pLogDir, bool bShowDialog=true, bool bResult=false, const char* ptrTitle=NULL, const char* ptrMessage=NULL );

	// PURPOSE: Sets a log message.
	// PARAMS:
	//    pMessage - log message containing variable arguments (if any).
	//	  ... - variable arguments, values corresponding to pMessage %d %s etc
	void SetProgressMessage(const char * pMessage, ...);

	// PURPOSE: Progress caption update callback for RexRageCutsceneExport.
	// PARAMS:
	//   pCaption - the caption text
	//	  ... - variable arguments, values corresponding to pMessage %d %s etc
	void SetProgressCaption( const char *pCaption, ... );

	// PURPOSE: Reset progress bar to 0
	void ResetProgress();

	// PURPOSE: Progress percent update callback for RexRageCutsceneExport.
	// PARAMS:
	//   fPercent - the percent complete (0 - 100)
	void SetProgressPercent( float fPercent );

	// PURPOSE: Get the current log filename, can resume a previous log.
	const char* GetLogFilename();

	// PURPOSE: Get the current MoBu process log directory in tools/logs
	const char* GetProcessLogDirectory();

	// PURPOSE: Clear out the process log direcory
	void ClearProcessLogDirectory();
};

#endif