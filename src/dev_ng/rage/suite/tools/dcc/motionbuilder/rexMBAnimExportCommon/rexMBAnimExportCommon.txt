Project rexMBAnimExportCommon
Template motionbuilder_configurations.xml

ConfigurationType library

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\suite\src
IncludePath $(RAGE_DIR)\base\tools\dcc\libs
IncludePath $(RAGE_DIR)\base\tools\libs
IncludePath $(RAGE_DIR)\suite\tools\dcc\motionbuilder
IncludePath $(RAGE_DIR)\framework\tools\src\dcc\libs
IncludePath $(RAGE_DIR)\framework\src
IncludePath BOOST
IncludePath LIBXML

ForceInclude ForceInclude.h

Define _USRDLL
Define USER_OBJECT_EXPORTS
Define _CRT_SECURE_NO_WARNINGS

Files {
	AnimExport.cpp
	AnimExport.h
	AnimExportModel.cpp
	AnimExportModel.h
	CutsceneExport.cpp
	CutsceneExport.h
	CutsceneExportModel.cpp
	CutsceneExportModel.h
	Keylist.cpp
	Keylist.h
	Logger.cpp
	Logger.h
	PartFile.cpp
	PartFile.h
	Perforce.cpp
	Perforce.h	 
	pipeline.cpp
	pipeline.h
	SyncedScene.cpp
	SyncedScene.h
	Timelist.cpp
	Timelist.h
	Timerlog.cpp
	TimerLog.h
	ToolObjectInterchange.cpp
	ToolObjectInterchange.h
	UserObjects.cpp
	UserObjects.h
	Utility.cpp
	Utility.h
}

Parse {
	CutsceneExport
	PartFile
	SyncedScene
}