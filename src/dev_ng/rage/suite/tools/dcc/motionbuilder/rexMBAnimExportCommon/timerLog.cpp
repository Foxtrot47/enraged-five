// /timerLog.cpp

#include "timerLog.h"

#include "UserObjects/OpenRealitySDK.h"

using namespace rage;

atString rexTimerLog::GetSceneName()
{
	FBApplication fbApp;

	return atString(fbApp.FBXFileName.AsString());
}
