#include <list>

using namespace rage;

#include "timelist.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
TimeList::TimeList()
{
	m_current = ml_KeyItems.end();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool TimeList::checkTime(float fTime,bool bDoneOnce)
{
	std::list<float>::iterator end = ml_KeyItems.end();
	std::list<float>::iterator next;

	if(fTime < 0.0f)
	{
		return true;
	}

	if(m_current == end)
	{
		m_current = ml_KeyItems.begin();

		//we have no entries in the list
		if(m_current == end)
		{
			ml_KeyItems.push_back(fTime);
			return true;
		}

		//if the time is before the beginning
		//of the list add it to the start

		if(*m_current > fTime)
		{
			ml_KeyItems.insert(m_current,fTime);
			return true;
		}
	}

	while(m_current != end)
	{
		if(*m_current == fTime)
		{
			return true;
		}

		next = m_current;
		next++;

		//if we off the end of the list then
		//add this time to the end

		if(next == end)
		{
			if(bDoneOnce)
			{
				ml_KeyItems.push_back(fTime);
				return true;
			}
			else
			{
				m_current = end;
				return checkTime(fTime,true);
			}
		}
		else
		{
			if((*next > fTime) && (*m_current < fTime))
			{
				//should interpolate the value at this stage
				ml_KeyItems.insert(next,fTime);

				return true;
			}
		}

		m_current = next;
	}

	if(!bDoneOnce)
	{
		return checkTime(fTime,true);
	}

	return false;
}

