// 
// rexMBRage/cutsceneExport.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REXMBRAGE_CUTSCENE_EXPORT_H__
#define __REXMBRAGE_CUTSCENE_EXPORT_H__

#include "animExport.h"
#include "cutsceneExportModel.h"
#include "utility.h"

#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "init/ini.h"
#include "parser/manager.h"
#include "rexBase/logAndErrorGenerator.h"
#include "rexGeneric/objectAnimation.h"
#include "string/string.h"
#include "vector/color32.h"
#include "vector/vector3.h"

#include "rexMBAnimExportCommon/partFile.h"
#include "UserObjects/OpenRealitySDK.h"

namespace rage
{

class crClip;
class crSkeletonData;
class crSkeleton;
class cutfObject;
class IToolObjectInterchange;
class rexObjectGenericAnimation;
class rexObjectGenericAnimationSegment;
class RexMbCutsceneExportData;
class RexMbCutsceneExportModel;
class rexSerializerRAGEAnimation;

//##############################################################################

class RexMbCutsceneData
{
public:
    RexMbCutsceneData();
    virtual ~RexMbCutsceneData();

    void Clear();

    const char* GetConfig() const;
    void SetConfig( const char *pConfig );

    const char* GetSceneName() const;
    void SetSceneName( const char *pSceneName );

    void GetExportPath( char* pDest, int destLen ) const;

    void GetFaceExportPath( char* pDest, int destLen ) const;

    cutfCutsceneFile2& GetCutsceneFile();

    // PURPOSE: Saves the class out to an XML file
    // PARAMS:
    //    pFilename - full path of the file to save the structure to
    // RETURNS: true on success, otherwise false.
    bool SaveToFile( const char *pFilename );

    // PURPOSE: Loads the class from an XML file
    // PARAMS:
    //    pFilename - full path of the file to load the structure from
    // RETURNS: true on success, otherwise false.
    bool LoadFromFile( const char *pFilename );

    // PURPOSE: Allocates the buffer and saves the data to it
    // PARAMS:
    //    pOutput - pointer to the buffer to create
    // RETURNS: true on success, otherwise false.
    bool SaveToString( char** pOutput );

    // PURPOSE: Loads the class from the buffer
    // PARAMS:
    //   pInput - the buffer
    // RETURNS: true on success, otherwise false.
    bool LoadFromString( const char* pInput );

    // PURPOSE: Validates that the supplied markup data is ok for use in export
    bool ValidateCutsceneExportData( );

    PAR_PARSABLE;

private:
    void UpdateExportPaths();

    atString m_config;
    atString m_sceneName;
    
    // Keep these around for backwards compatibility
    bool m_overrideExportRoot;
    bool m_overrideFaceDir;
    mutable atString m_exportRoot;
    mutable atString m_faceDir;
    mutable atString m_backupDir;
    
    cutfCutsceneFile2 m_cutsceneFile;
};

inline cutfCutsceneFile2& RexMbCutsceneData::GetCutsceneFile()
{
    return m_cutsceneFile;
}

//##############################################################################

struct SModelObject
{
    SModelObject()
        : pModel(NULL)
        , pObject(NULL)
    { 
		pToyboxEntry = rage_new SToyboxEntry();
	}

	void Cleanup()
	{ 
		if ( pToyboxEntry != NULL )
		{
			delete pToyboxEntry;
			pToyboxEntry = NULL;
		}
	}

    HFBModel pModel;
    cutfObject *pObject;
	SToyboxEntry *pToyboxEntry;
};

//##############################################################################

class RexRageCutsceneExport
{
public:
    RexRageCutsceneExport();
    ~RexRageCutsceneExport();

    // PURPOSE: Initializes the static members of this class.
    static void InitClass();

    // PURPOSE: Shuts down the static members of this class.
    static void ShutdownClass();

    // PURPOSE: Enables/Disables the display of message boxes for errors and warnings that occur during export.
    void SetInteractiveMode( bool bOnOff );

    // PURPOSE: Get the start frame for all exporting animations
    // RETURNS: The global start frame
    float GetStartFrame() const;

    // PURPOSE: Set the start frame for the all exporting animation
    // PARAMS:
    //    fFrame - the global start frame
    void SetStartFrame( float fFrame );

    // PURPOSE: Get the end frame for all exporting animations
    // RETURNS: The global end frame
    float GetEndFrame() const;

    // PURPOSE: Set the end frame for the all exporting animation
    // PARAMS:
    //    fFrame - the global end frame
    void SetEndFrame( float fFrame );

    // PURPOSE: Get the name of the scene to export.  This is be used with the ExportRoot to determine the animation export path.
    // RETURNS: The scene name.
    const char* GetSceneName() const;

    // PURPOSE: Set the name of the scene to export.  This is be used with the ExportRoot to determine the animation export path.
    // PARAMS:
    //    pSceneName - the scene name
    void SetSceneName( const char *pSceneName );

    // PURPOSE: Get the export path
    // PARAMS:
    //    pDest - the dest buffer
    //    destLen - the length of the buffer
    void GetExportPath( char *pDest, int destLen ) const;

    // PURPOSE: Get the face export path
    // PARAMS:
    //    pDest - the dest buffer
    //    destLen - the length of the buffer
    void GetFaceExportPath( char *pDest, int destLen ) const;

    // PURPOSE: Retrieves the cut scene file data
    // RETURNS: The cut file
    cutfCutsceneFile2& GetCutsceneFile();

	// PURPOSE: Retrieves the part file data
	// RETURNS: The part file
	RexRageCutscenePartFile& GetCutscenePartFile();

    // PURPOSE: Retrieves the custom event definitions data
    // RETURNS: The event defs
    const cutfEventDefs& GetEventDefs() const;

    // PURPOSE: Loads the cut file
    // PARAMS:
    //    pFullPath - the full filename to load
    // RETURNS: true on success, otherwise false
    bool OpenCutFile( const char *pFullPath );

	// PURPOSE: Loads the part file
	// PARAMS:
	//    pFullPath - the full filename to load
	// RETURNS: true on success, otherwise false
	bool OpenPartFile( const char *pFullPath );

    // PURPOSE: Saves the cut file
    // RETURNS: true if successful.
    bool SaveCutFile();

	// PURPOSE: Saves the part file
	// RETURNS: true if successful.
	bool SaveItemFile();

	// PURPOSE: Build te icd.zip file for AP3
	// RETURNS: true if successful.
	bool BuildZip();
	bool BuildDictionary(const char* pSrcDir);

	// PURPOSE: Joins the part scenes, concatting the anims/clips and cutxml
	// RETURNS: true if successful.
	bool JoinScenes(bool bCutFileOnly=false);

    // PURPOSE: Saves the RexMBCutsceneData class out to an XML file
    // PARAMS:
    //    pFilename - full path of the file to save the structure to
    // RETURNS: true on success, otherwise false.
    bool SaveCutsceneDataToFile( const char *pFilename );

    // PURPOSE: Loads the RexMBCutsceneData class from an XML file
    // PARAMS:
    //    pFilename - full path of the file to load the structure from
    // RETURNS: true on success, otherwise false.
    bool LoadCutsceneDataFromFile( const char *pFilename );

    // PURPOSE: Allocates the buffer and saves the RexMBCutsceneData class to it
    // PARAMS:
    //    pOutput - pointer to the buffer to create
    // RETURNS: true on success, otherwise false.
    bool SaveCutsceneDataToString( char** pOutput );

    // PURPOSE: Loads the RexMBCutsceneData class from the buffer
    // PARAMS:
    //   pInput - the buffer
    // RETURNS: true on success, otherwise false.
    bool LoadCutsceneDataFromString( const char* pInput );

    // PURPOSE: Validates that the supplied markup data is ok for use in export
    bool ValidateCutsceneExportData();

	// PURPOSE: Validate all part offsets are the same
	bool ValidatePartOffsets();

	// PURPOSE: Validate any models that don't have the handle property set
	bool ValidateHandles();

	// PURPOSE: Validate for any overlapping ranges within the parts
	bool ValidateOverlappingRanges();

	// PURPOSE: Validate all parts section methods are the same
	bool ValidateSectionMethods();

	// PURPOSE: Validate all parts are the greater than the minimum length
	bool ValidateSceneLengths();

	// PURPOSE: Validate a weapon/prop model, TEMP
	bool ValidateWeaponProps();

    // PURPOSE: Prepares the cutfile for animation export by setting the IsSectioned flag to false
    //    and setting each Ped Model's FaceAndBodyAreMerged flag to false.
    void PrepareCutfileForAnimationExport();

public:
	bool Export( atArray<SModelObject> &modelObjects, atArray<SModelObject> &otherModelObjects, bool bSampleCamera, bool bSample=true, bool bShowValidation=true, bool bJustFace=false );

	// PURPOSE: Build animation dictionaries using the icd.zip file
	// PARAMS:
	//   bPreview - if the dictionaries are to be built in preview mode.
	bool BuildAnimationDictionaries(bool bPreview = false);

	// PURPOSE: Generates a modelObject list. This is used by phython when instantiating an instance of this class without any UI.
	// PARAMS:
	//   modelObjects - modelObjects array to fill by ref.
	void GenerateModelObjectList(atArray<SModelObject>& modelObjects);	

	// PURPOSE: Indicates whether the object can have an animation or not.
	// PARAMS:
	//    pObject - the object to check
	// RETURNS: true if it can have an animation, otherwise false.
	bool IsObjectAnimatable( cutfObject *pObject ) const;

	// PURPOSE: Builds a list of the exported animation(s) and clip for the given object that exist on disc.
	// PARAMS:
	//    pObject - the object for export
	//    exportFilenameList - the list of files that were exported
	//    bIncludePedModelFace - for Ped Model Object, whether or not to include its face animation, if it exists.
	void GetExportFilenames( const char* pExportPath, const cutfObject *pObject, atArray<atString> &exportFilenameList, bool bIncludePedModelFace );

	// PURPOSE: Load the event defs file given by the module settings MotionBuilderEventDefsPath, if any.
	void LoadEventDefsFile();

	void CleanupRedundantExportFiles(const char* pExportPath, atArray<SModelObject> &modelObjects, atArray<SModelObject> &otherModelObjects);

	// PURPOSE: remove objects we don't need in motionbuilder on cutxml import, static lights and subtitle objects
	// PARAMS:
	void RemoveObjects();

	// PURPOSE: Remove all events bar any that are edited in rag, we don't need the rest
	// PARAMS:
	void RemoveEvents();

	// PURPOSE: Push all rag edited events back into motionbuilder space
	// PARAMS:
	void ChangeStreamProcessedEvents();

	void CheckoutData(const char* pExportPath, const char* pFaceExportPath, atArray<SModelObject> &modelObjects);

	void SetPartData();

private:

	// PURPOSE: Exports the animations for all models in the list, using one pass for the non-skeletal models 
	//   (and eventually one pass for all of the skeletal models).
	// PARAMS:
	//    modelObjects - the list of models and their associated cutfObject
	//    bSampleCamera - sample the camera data vs grabbing its keyframes
	//    bSample - sample the data for everything else vs grabbing their keyframes
	// RETURNS: true on success, otherwise false
	bool DoExport( atArray<SModelObject> &modelObjects, bool bSampleCamera, bool bSample=true, bool bJustFace=false );

	// PURPOSE: Enabled/Disable constraints for the toybox in a specific folder ToyBoxFolder
	// PARAMS:
	//    bEnabled - status if to enable or disable
	void SetConstraints(bool bEnabled);

	void SetVehicleConstraints(bool bEnabled);

	// PURPOSE: Validates that if any model dupes exist, ^ models then the parent must also exist.
	// PARAMS:
	//    modelObjects - list of models
	// RETURNS: true on success, otherwise false
	bool ValidateModelDupes( const atArray<SModelObject>& modelObjects );

	// PURPOSE: Validates that the animation and asset managers are in their reserved slots
	// RETURNS: true on success, otherwise false
	bool ValidateAnimationAndAssetManager();

	// PURPOSE: Validates that the camera fov and dof values are valid.
	// RETURNS: true on success, otherwise false
	bool ValidateFOVAndDOF( RexMbCutsceneExportModel *pExportModel );

	// PURPOSE: Processes the data for a skeletal object export - this is used for body and face exporting
	// PARAMS:
	//    pObject - object associated
	//    czLongName - full name of the object
	//    czAnimFilename - name of the animation
	//    czAnimCtrlFilePath - control file file path
	//    czFaceAttributesFilename - filename of the attributes file for exporting custom attributes on faces
	//    czModelType - model type string
	//    skeletalModelList - skeletal list of models to process
	void ProcessSkeletalData(cutfObject *pObject, SToyboxEntry* pToyboxEntry, const char* czLongName, const char* czAnimFilename, const char* czAnimCtrlFilePath, const char* czFaceAttributesFilename, const char* czModelType, atArray<RexAnimMarkupData *>& skeletalModelList, atArray<SToyboxEntry*>& toyboxEntries);

	// PURPOSE: Writes the memory stats for the toybox, if a model is in the toybox it is deemed out of memory.
	// PARAMS:
	//    modelObjects - list of models.
	// RETURNS: true if the toybox exists, otherwise false.
	bool WriteToybox(atArray<SModelObject>& modelObjects);

	// PURPOSE: Calculates the bounding box for a specific model, this takes into account translation and scaling.
	// PARAMS:
	//    model - the model which you want the bounding box of.
	//	  min - min bounds of the bounding box.
	//	  max - max bounds of the bounding box.
	void CalculateBoundingBox(const HFBModel& model, FBVector3d& min, FBVector3d& max);

	// PURPOSE: Checks if a bounding box collision happens between the bounding box provided and the object position provided.
	// PARAMS:
	//	  min - min bounds of the bounding box.
	//	  max - max bounds of the bounding box.
	//    objectPosition - position of the object you want to check again the bounds.
	// RETURNS: true if a collision occurs, otherwise false.
	bool BoundingBoxCollision(const FBVector3d& min, const FBVector3d& max, const FBVector3d& objectPosition);

	// PURPOSE: Progress percent update callback for RexRageCutsceneExport.
	// PARAMS:
	//   fPercent - the percent complete (0 - 100)
	void SetProgressPercent( float fPercent );

    // PURPOSE: checks if the model exists for all Model Objects.
    // RETURNS: True on success, otherwise, false
    bool ValidateModelsExist();

    enum EAnimExportType
    {
        OBJECT_PED,
        OBJECT_VEHICLE,
        OBJECT_PROP,
        OBJECT_FX,
        OBJECT_LIGHT,
        OBJECT_CAMERA
    };

    // PURPOSE: Looking at SModelObject, returns the type id to use for the animation export. 
    // PARAMS:
    //    modelObject - the model to get the anim export type of
    // RETURNS: The anim export type id
    s32 GetAnimExportType( const SModelObject &modelObject );
   
    // PURPOSE: Exports the animation and clip for list of skeletal models.
    // PARAMS:
    //    animSegmentDataList - the list of data structures describing what RexRageAnimExport should export.
    //    bSample - sample the data vs grabbing keyframes
    // RETURNS: true on success, otherwise false
    bool DoExportInternal( atArray<RexAnimMarkupData *> &animSegmentDataList, atArray<SToyboxEntry*> &toyboxEntries, bool bSample );
     
    // PURPOSE: Exports the animation and clip for the list of non-skeletal models.
    // PARAMS:
    //    exportModelList - the list of model instances to export
    //    bSample - sample the data vs grabbing keyframes
    // RETURNS: true on success, otherwise false
    bool DoExportInternal( atArray<RexMbCutsceneExportModel *> &exportModelList, bool bSample );

    bool m_bInteractiveMode;

    int m_iCurrentExportProgressItem;
    int m_iNumExportProgressItems;

    float m_iStartFrame;
    float m_iEndFrame;

    RexMbCutsceneData m_cutsceneData;
    cutfEventDefs m_eventDefs;
	RexRageCutscenePartFile m_cutscenePartData;
};

inline void RexRageCutsceneExport::SetInteractiveMode( bool bOnOff )
{
    m_bInteractiveMode = bOnOff;
}

inline float RexRageCutsceneExport::GetStartFrame() const
{
    return m_iStartFrame;
}

inline void RexRageCutsceneExport::SetStartFrame( float fFrame )
{
    m_iStartFrame = fFrame;
}

inline float RexRageCutsceneExport::GetEndFrame() const
{
    return m_iEndFrame;
}

inline void RexRageCutsceneExport::SetEndFrame( float fFrame )
{
    m_iEndFrame = fFrame;
}

inline const char* RexRageCutsceneExport::GetSceneName() const
{
    return m_cutsceneData.GetSceneName();
}

inline void RexRageCutsceneExport::SetSceneName( const char *pSceneName )
{
    m_cutsceneData.SetSceneName( pSceneName );
}

inline void RexRageCutsceneExport::GetExportPath( char *pDest, int destLen ) const
{
    m_cutsceneData.GetExportPath( pDest, destLen );
}

inline void RexRageCutsceneExport::GetFaceExportPath( char *pDest, int destLen ) const
{
    m_cutsceneData.GetFaceExportPath( pDest, destLen );
}

inline cutfCutsceneFile2& RexRageCutsceneExport::GetCutsceneFile()
{
    return m_cutsceneData.GetCutsceneFile();
}

inline RexRageCutscenePartFile& RexRageCutsceneExport::GetCutscenePartFile()
{
	return m_cutscenePartData;
}

inline const cutfEventDefs& RexRageCutsceneExport::GetEventDefs() const
{
    return m_eventDefs;
}

inline bool RexRageCutsceneExport::SaveCutsceneDataToFile( const char *pFilename )
{
    return m_cutsceneData.SaveToFile( pFilename );
}

inline bool RexRageCutsceneExport::SaveCutsceneDataToString( char** pOutput )
{
    return m_cutsceneData.SaveToString( pOutput );
}

inline bool RexRageCutsceneExport::ValidateCutsceneExportData(  )
{
    return m_cutsceneData.ValidateCutsceneExportData( );
}

//##############################################################################

} // namespace rage

#endif //__REXMBRAGE_CUTSCENE_EXPORT_H__

