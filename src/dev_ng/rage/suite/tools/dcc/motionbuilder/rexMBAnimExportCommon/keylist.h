#ifndef __KEYLIST_TOOL_H__
#define __KEYLIST_TOOL_H__

//#############################################################################

struct KeyList
{
    struct KeyItem
    {
        int iFrame;
        float fValue;
    };

    KeyList();

    void AddKey( int iFrame, float fValue );
    float GetValue( int iFrame );

    std::vector<KeyItem> m_keyItemList;
};

//#############################################################################

#endif //__KEYLIST_TOOL_H__

