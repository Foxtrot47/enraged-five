// 
// rexMBAnimExportCommon/cutsceneExportModel.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "cutsceneExportModel.h"

#include "UserObjects/OpenRealitySDK.h"
#include <list>
#include "keylist.h"
#include "timelist.h"
#include "timerLog.h"
#include "utility.h"

#include "cranimation/animation.h"
#include "crclip/clipanimation.h"
#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "crmetadata/properties.h"
#include "crmetadata/propertyattributes.h"
#include "rexRAGE/serializerAnimation.h"
#include "fwanimation/animdefines.h"
#include "rexMBRage/shared.h"
#include "perforce.h"

#pragma warning (disable:4239)

using namespace rage;

namespace rage
{

RexMbCutsceneExportModel::RexMbCutsceneExportModel( HFBModel pModel, SToyboxEntry* pToyboxEntry, const char *pFilenameWithoutExtension )
: m_pModel(pModel),
m_toyboxData(pToyboxEntry)
{
    if ( pFilenameWithoutExtension != NULL )
    {
        safecpy( m_cFilenameWithoutExtension, pFilenameWithoutExtension, sizeof(m_cFilenameWithoutExtension) );
    }
}

RexMbCutsceneExportModel::~RexMbCutsceneExportModel()
{
    Reset();
}

bool RexMbCutsceneExportModel::Reset()
{
    for ( int i = 0; i < m_exportDataList.GetCount(); ++i )
    {
        delete m_exportDataList[i];
    }

    m_exportDataList.Reset();
    m_trackExportDataList.Reset();
    m_trackNameList.Reset();
    m_trackIDList.Reset();

    return true;
}

void RexMbCutsceneExportModel::CollectAnimData()
{
    RexMbCutsceneExportModel::CollectAnimData( m_exportDataList );
}

void RexMbCutsceneExportModel::CollectAnimData( atArray<RexMbCutsceneExportData *> &animDataList )
{
    // Go through each of our data collectors and pull out the data for the current frame
    for ( int i = 0; i < animDataList.GetCount(); ++i )
    {
        animDataList[i]->ReadFrameData();
    }
}

void RexMbCutsceneExportModel::CollectAnimData( float fStartTime, float fEndTime, int nFrames )
{
    RexMbCutsceneExportModel::CollectAnimData( m_exportDataList, fStartTime, fEndTime, nFrames );
}

void RexMbCutsceneExportModel::CollectAnimData( atArray<RexMbCutsceneExportData *> &animDataList, float fStartTime, float fEndTime, int nFrames )
{
#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::StartTimer( "RexMbCutsceneExportModel::CollectAnimData( keyframes )" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    // Go through each of our data collectors and pull out the key frame data for the whole scene
    for ( int i = 0; i < animDataList.GetCount(); ++i )
    {
        animDataList[i]->ReadKeyData( fStartTime, fEndTime, nFrames );
    }

#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::CollectAnimData( keyframes )" );
    }
#endif //PROFILE_CUTSCENE_EXPORT
}

bool RexMbCutsceneExportModel::SerializeAnimation( float fStartTime, float fEndTime, int nFrames )
{
#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::StartTimer( "RexMbCutsceneExportModel::SerializeAnimation" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    if ( !ProcessCollectedAnimData( nFrames ) )
    {
#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::SerializeAnimation" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        return false;
    }

    rexObjectGenericAnimation::ChunkInfo* pChunk = rage_new rexObjectGenericAnimation::ChunkInfo;
    pChunk->m_Name = "root";
    pChunk->m_IsJoint = false;
    pChunk->m_BoneIndex = 0;
    pChunk->m_Scale = Vector3(1.0f,1.0f,1.0f);
    pChunk->m_BoneID = "root";

    rexObjectGenericAnimationSegment* pAnimationSegment = NULL;
    rexObjectGenericAnimation* pAnimation = NULL;    

    if ( !InitRexAnimation( fStartTime, fEndTime, pAnimationSegment, pAnimation ) )
    {
        if ( pAnimation != NULL )
        {
            delete pAnimation;
        }

        if ( pAnimationSegment != NULL )
        {
            delete pAnimationSegment;
        }

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::SerializeAnimation" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        return false;
    }

    InitTracksInfo( fStartTime, fEndTime, nFrames, pChunk->m_Tracks );

    AddAnimDataToTracks( nFrames, pChunk->m_Tracks );

    pAnimationSegment->m_ChunkNames.PushAndGrow( pChunk->m_Name, 1 );
    pAnimation->m_RootChunks.PushAndGrow( pChunk, 1 );

    rexSerializerRAGEAnimation* pRexSer = CreateAnimationSerializer();
    if ( pRexSer == NULL )
    {
        delete pAnimation;
        delete pAnimationSegment;

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::SerializeAnimation" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        return false;
    }

    bool result = ExportRexAnimation( pRexSer, pAnimation );
    
    delete pRexSer;
    delete pAnimation;
    
#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::SerializeAnimation" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    return result;
}

bool RexMbCutsceneExportModel::SerializeClip( float fStartTime, float fEndTime, RexAnimMarkupData* pAnimData )
{
#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::StartTimer( "RexMbCutsceneExportModel::SerializeClip" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    char cAnimFilename[RAGE_MAX_PATH];
    sprintf( cAnimFilename, "%s.anim", m_cFilenameWithoutExtension );

	if(rexMBAnimExportCommon::GetUseClipXml())
	{
		char cClipFilename[RAGE_MAX_PATH];
		sprintf( cClipFilename, "%s.clipxml", m_cFilenameWithoutExtension );

		ULOGGER_MOBO.SetProgressMessage("Serializing clip '%s'.", cClipFilename );

		crAnimation* pAnim = crAnimation::AllocateAndLoad( cAnimFilename );

		fStartTime;
		fEndTime;

		ClipXML* clip = rage_new ClipXML();
		clip->Load(cClipFilename);

		clip->SetAnimationName(atString(ASSET.FileName(cAnimFilename)));

		clip->SetStart(0);
		clip->SetEnd(pAnim->GetDuration());

		RemoveBlockTags(clip);
		AddCameraCutTagBlocks( fStartTime, fEndTime, clip );
		AddToyboxTagBlocks( fStartTime, fEndTime, clip );

		atString compFilePath("Cutscene_Default_Compress.txt");
	
		if(pAnimData)
		{
			compFilePath = atString( pAnimData->GetSegment(0)->m_compressionFilePath.c_str());	
		}

		PropertyAttributeStringXML* compressionAttribute = rage_new PropertyAttributeStringXML();
		compressionAttribute->SetName(COMPRESSION_FILE_PROPERTY_NAME);
		atString& attributeString = compressionAttribute->GetString();
		attributeString = compFilePath;

		PropertyXML* pCompressionFile = rage_new PropertyXML();
		pCompressionFile->SetName(COMPRESSION_FILE_PROPERTY_NAME);
		pCompressionFile->AddAttribute( compressionAttribute );
		clip->AddProperty( pCompressionFile );

		PropertyAttributeStringXML* pVersionAttribute = rage_new PropertyAttributeStringXML();
		pVersionAttribute->SetName( EXPORTER_VERSION_PROPERTY_NAME );
		atString& versionValue = pVersionAttribute->GetString();
		char cVersion[RAGE_MAX_PATH];
		sprintf(cVersion, "%d.%d.%d", s_cutscene_exporter_version.Major, s_cutscene_exporter_version.Minor, s_cutscene_exporter_version.Revision);
		versionValue = cVersion;

		PropertyXML* pVersionProperty = rage_new PropertyXML();
		pVersionProperty->SetName( EXPORTER_VERSION_PROPERTY_NAME );
		pVersionProperty->AddAttribute( pVersionAttribute );
		clip->AddProperty( pVersionProperty );

		PropertyAttributeStringXML* pAbsoluteFBXFileAttribute = rage_new PropertyAttributeStringXML();
		pAbsoluteFBXFileAttribute->SetName( ABSOLUTE_FBXFILE_PROPERTY_NAME );
		atString& absoluteFbxFileValue = pAbsoluteFBXFileAttribute->GetString();
		char cAbsoluteFileName[RAGE_MAX_PATH];
		FBApplication application;
		sprintf(cAbsoluteFileName, "%s", application.FBXFileName.AsString());
		absoluteFbxFileValue = cAbsoluteFileName;

		PropertyXML* pAbsoluteFBXFileProperty = rage_new PropertyXML();
		pAbsoluteFBXFileProperty->SetName( ABSOLUTE_FBXFILE_PROPERTY_NAME );
		pAbsoluteFBXFileProperty->AddAttribute( pAbsoluteFBXFileAttribute );
		clip->AddProperty( pAbsoluteFBXFileProperty );

		PropertyAttributeStringXML* pFBXFileAttribute = rage_new PropertyAttributeStringXML();
		pFBXFileAttribute->SetName( FBXFILE_PROPERTY_NAME );
		atString& fbxFileValue = pFBXFileAttribute->GetString();
		char cFileName[RAGE_MAX_PATH];
		//FBApplication application;
		sprintf(cFileName, "%s", application.FBXFileName.AsString());

		// Replace the art path with $(art)
		atString artPath = rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString(""));
		artPath.Lowercase();
		artPath.Replace("/","\\");

		fbxFileValue = cFileName;
		fbxFileValue.Lowercase();
		fbxFileValue.Replace("/","\\");

		fbxFileValue.Replace(artPath.c_str(), "$(art)");

		PropertyXML* pFBXFileProperty = rage_new PropertyXML();
		pFBXFileProperty->SetName( FBXFILE_PROPERTY_NAME );
		pFBXFileProperty->AddAttribute( pFBXFileAttribute );
		clip->AddProperty( pFBXFileProperty );

		PropertyAttributeIntXML* pExporterTypeAttribute = rage_new PropertyAttributeIntXML();
		pExporterTypeAttribute->SetName( EXPORTER_TYPE_PROPERTY_NAME );
		int& exporterTypeValue = pExporterTypeAttribute->GetInt();
		exporterTypeValue = ET_MB_CUTSCENE;

		PropertyXML* pExporterTypeProperty = rage_new PropertyXML();
		pExporterTypeProperty->SetName( EXPORTER_TYPE_PROPERTY_NAME );
		pExporterTypeProperty->AddAttribute( pExporterTypeAttribute );
		clip->AddProperty( pExporterTypeProperty );

		if(pAnimData && pAnimData->GetCharacter(0)->GetNumMoverModels() > 0) // Only models which have a mover have animdata.  (Face models do not have movers.)
		{
			atString sMover(pAnimData->GetCharacter(0)->GetMoverModelName(0));

			HFBModel moverModel = RAGEFindModelByName((char*)sMover.c_str());
			if(moverModel)
			{
				FBSystem system;
				FBPlayerControl controller;   

				FBTime preSampleTime = system.LocalTime;

				double t = fStartTime;
				FBTime fbt;
				fbt.SetSecondDouble( t );
				controller.Goto( fbt );

				system.Scene->Evaluate();

				FBMatrix moverTransform;
				moverTransform.Identity();
				moverModel->GetMatrix(moverTransform, kModelTransformation, false);

				Matrix34 moverTransformRage;
				FBMatrixToRageMatrix34(moverTransform, moverTransformRage);

				Quaternion moverRotation;
				moverTransformRage.ToQuaternion(moverRotation);
				moverRotation.Normalize();

				PropertyAttributeQuaternionXML* pPropInitOffsetRot = rage_new PropertyAttributeQuaternionXML();
				pPropInitOffsetRot->SetName( INIT_OFFSET_ROT_PROPERTY_NAME );
				pPropInitOffsetRot->FromQuaternion(QUATERNION_TO_QUATV(moverRotation));

				PropertyXML* pInitOffsetRotationProperty = rage_new PropertyXML();
				pInitOffsetRotationProperty->SetName(INIT_OFFSET_ROT_PROPERTY_NAME);	
				pInitOffsetRotationProperty->AddAttribute(pPropInitOffsetRot);
				clip->AddProperty( pInitOffsetRotationProperty );

				PropertyAttributeVector3XML* pPropInitOffsetPos = rage_new PropertyAttributeVector3XML();
				pPropInitOffsetPos->SetName( INIT_OFFSET_POS_PROPERTY_NAME );
				Vec3V_Ref offsetPositionValue = pPropInitOffsetPos->GetVector3();
				offsetPositionValue = VECTOR3_TO_VEC3V(moverTransformRage.d);

				PropertyXML* pInitOffsetPositionProperty = rage_new PropertyXML();
				pInitOffsetPositionProperty->SetName(INIT_OFFSET_POS_PROPERTY_NAME);	
				pInitOffsetPositionProperty->AddAttribute(pPropInitOffsetPos);
				clip->AddProperty( pInitOffsetPositionProperty );

				controller.Goto( preSampleTime );
			}
		}

		if ( !ProcessClip( clip ) )
		{
			//delete pClipAnim;

	#if PROFILE_CUTSCENE_EXPORT
			if ( rexMBTimerLog::IsTimerLogEnabled() )
			{
				rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::SerializeClip" );
			}
	#endif //PROFILE_CUTSCENE_EXPORT

			return false;
		}

		ULOGGER_MOBO.SetProgressMessage("Saving clip '%s'.", cClipFilename );

		const char *pExtension = ASSET.FindExtensionInPath( cClipFilename );
		if ( pExtension != NULL )
		{
			*(const_cast<char *>( pExtension )) = 0;
		}

		clip->Save(cClipFilename);

		delete clip;
		delete pAnim;
	}
	else
	{
		char cClipFilename[RAGE_MAX_PATH];
		sprintf( cClipFilename, "%s.clip", m_cFilenameWithoutExtension );

		ULOGGER_MOBO.SetProgressMessage("Serializing clip '%s'.", cClipFilename );

		crClipAnimation* pClipAnim = static_cast<crClipAnimation*>(crClipAnimation::AllocateAndLoad(cClipFilename));
		if(!pClipAnim)
		{
			crAnimation* pAnim = crAnimation::AllocateAndLoad( cAnimFilename );
			if( pAnim )
			{
				pClipAnim = crClipAnimation::AllocateAndCreate( *pAnim );
			}
			else
			{
				pClipAnim = crClipAnimation::Allocate( );
			}
		}

		pClipAnim->SetStartEndTimes(0.0f, pClipAnim->GetAnimation()->GetDuration());

		RemoveBlockTags(pClipAnim);
		AddCameraCutTagBlocks( fStartTime, fEndTime, pClipAnim );
		AddToyboxTagBlocks( fStartTime, fEndTime, pClipAnim );

		atString compFilePath("Cutscene_Default_Compress.txt");

		if(pAnimData)
		{
			compFilePath = atString( pAnimData->GetSegment(0)->m_compressionFilePath.c_str());	
		}

		crProperties* pProperties = pClipAnim->GetProperties();

		crPropertyAttributeString compressionAttribute;
		compressionAttribute.SetName(COMPRESSION_FILE_PROPERTY_NAME);
		atString& attributeString = compressionAttribute.GetString();
		attributeString = compFilePath;

		crProperty pCompressionFile;
		pCompressionFile.SetName(COMPRESSION_FILE_PROPERTY_NAME);
		pCompressionFile.AddAttribute(compressionAttribute);
		pProperties->AddProperty( pCompressionFile );

		crPropertyAttributeString pVersionAttribute;
		pVersionAttribute.SetName( EXPORTER_VERSION_PROPERTY_NAME );
		atString& versionValue = pVersionAttribute.GetString();
		char cVersion[RAGE_MAX_PATH];
		sprintf(cVersion, "%d.%d.%d", s_cutscene_exporter_version.Major, s_cutscene_exporter_version.Minor, s_cutscene_exporter_version.Revision);
		versionValue = cVersion;

		crProperty pVersionProperty;
		pVersionProperty.SetName( EXPORTER_VERSION_PROPERTY_NAME );
		pVersionProperty.AddAttribute( pVersionAttribute );
		pProperties->AddProperty( pVersionProperty );

		crPropertyAttributeString pAbsoluteFBXFileAttribute;
		pAbsoluteFBXFileAttribute.SetName( ABSOLUTE_FBXFILE_PROPERTY_NAME );
		atString& absoluteFbxFileValue = pAbsoluteFBXFileAttribute.GetString();
		char cAbsoluteFileName[RAGE_MAX_PATH];
		FBApplication application;
		sprintf(cAbsoluteFileName, "%s", application.FBXFileName.AsString());
		absoluteFbxFileValue = cAbsoluteFileName;

		crProperty pAbsoluteFBXFileProperty;
		pAbsoluteFBXFileProperty.SetName( ABSOLUTE_FBXFILE_PROPERTY_NAME );
		pAbsoluteFBXFileProperty.AddAttribute( pAbsoluteFBXFileAttribute );
		pProperties->AddProperty( pAbsoluteFBXFileProperty );

		crPropertyAttributeString pFBXFileAttribute;
		pFBXFileAttribute.SetName( FBXFILE_PROPERTY_NAME );
		atString& fbxFileValue = pFBXFileAttribute.GetString();
		char cFileName[RAGE_MAX_PATH];
		//FBApplication application;
		sprintf(cFileName, "%s", application.FBXFileName.AsString());

		// Replace the art path with $(art)
		atString artPath = rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString(""));
		artPath.Lowercase();
		artPath.Replace("/","\\");

		fbxFileValue = cFileName;
		fbxFileValue.Lowercase();
		fbxFileValue.Replace("/","\\");

		fbxFileValue.Replace(artPath.c_str(), "$(art)");

		crProperty pFBXFileProperty;
		pFBXFileProperty.SetName( FBXFILE_PROPERTY_NAME );
		pFBXFileProperty.AddAttribute( pFBXFileAttribute );
		pProperties->AddProperty( pFBXFileProperty );

		crPropertyAttributeInt pPropRevision;
		pPropRevision.SetName( PERFORCE_REVISION_PROPERTY_NAME );
		int& revisionValue = pPropRevision.GetInt();
		int rev = -1;
		perforce::GetRevision(cClipFilename, rev, false);
		revisionValue = rev != -1 ? rev + 1 : 0;

		crProperty pRevisionProperty;
		pRevisionProperty.SetName( PERFORCE_REVISION_PROPERTY_NAME );
		pRevisionProperty.AddAttribute( pPropRevision );
		pProperties->AddProperty( pRevisionProperty );


		crPropertyAttributeInt pExporterTypeAttribute;
		pExporterTypeAttribute.SetName( EXPORTER_TYPE_PROPERTY_NAME );
		int& exporterTypeValue = pExporterTypeAttribute.GetInt();
		exporterTypeValue = ET_MB_CUTSCENE;

		crProperty pExporterTypeProperty;
		pExporterTypeProperty.SetName( EXPORTER_TYPE_PROPERTY_NAME );
		pExporterTypeProperty.AddAttribute( pExporterTypeAttribute );
		pProperties->AddProperty( pExporterTypeProperty );

		if(pAnimData && pAnimData->GetCharacter(0)->GetNumMoverModels() > 0) // Only models which have a mover have animdata.  (Face models do not have movers.)
		{
			atString sMover(pAnimData->GetCharacter(0)->GetMoverModelName(0));

			HFBModel moverModel = RAGEFindModelByName((char*)sMover.c_str());
			if(moverModel)
			{
				FBSystem system;
				FBPlayerControl controller;   

				FBTime preSampleTime = system.LocalTime;

				double t = fStartTime;
				FBTime fbt;
				fbt.SetSecondDouble( t );
				controller.Goto( fbt );

				system.Scene->Evaluate();

				FBMatrix moverTransform;
				moverTransform.Identity();
				moverModel->GetMatrix(moverTransform, kModelTransformation, false);

				Matrix34 moverTransformRage;
				FBMatrixToRageMatrix34(moverTransform, moverTransformRage);

				Quaternion moverRotation;
				moverTransformRage.ToQuaternion(moverRotation);
				moverRotation.Normalize();

				crPropertyAttributeQuaternion pPropInitOffsetRot;
				pPropInitOffsetRot.SetName( INIT_OFFSET_ROT_PROPERTY_NAME );
				QuatV_Ref offsetValue = pPropInitOffsetRot.GetQuaternion();
				offsetValue = QUATERNION_TO_QUATV(moverRotation);

				crProperty pInitOffsetRotationProperty;
				pInitOffsetRotationProperty.SetName(INIT_OFFSET_ROT_PROPERTY_NAME);	
				pInitOffsetRotationProperty.AddAttribute(pPropInitOffsetRot);
				pProperties->AddProperty( pInitOffsetRotationProperty );

				crPropertyAttributeVector3 pPropInitOffsetPos;
				pPropInitOffsetPos.SetName( INIT_OFFSET_POS_PROPERTY_NAME );
				Vec3V_Ref offsetPositionValue = pPropInitOffsetPos.GetVector3();
				offsetPositionValue = VECTOR3_TO_VEC3V(moverTransformRage.d);

				crProperty pInitOffsetPositionProperty;
				pInitOffsetPositionProperty.SetName(INIT_OFFSET_POS_PROPERTY_NAME);	
				pInitOffsetPositionProperty.AddAttribute(pPropInitOffsetPos);
				pProperties->AddProperty( pInitOffsetPositionProperty );

				controller.Goto( preSampleTime );
			}
		}

		if ( !ProcessClip( pClipAnim ) )
		{
			delete pClipAnim;

		#if PROFILE_CUTSCENE_EXPORT
			if ( rexMBTimerLog::IsTimerLogEnabled() )
			{
				rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::SerializeClip" );
			}
		#endif //PROFILE_CUTSCENE_EXPORT

			return false;
		}

		ULOGGER_MOBO.SetProgressMessage("Saving clip '%s'.", cClipFilename );

		const char *pExtension = ASSET.FindExtensionInPath( cClipFilename );
		if ( pExtension != NULL )
		{
			*(const_cast<char *>( pExtension )) = 0;
		}

		pClipAnim->Save( cClipFilename );

		delete pClipAnim;
	}

#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::SerializeClip" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    return true;
}

bool RexMbCutsceneExportModel::ProcessCollectedAnimData( int nFrames )
{
	RexMbCutsceneExportModelMatrixData *pModelAnimData = dynamic_cast<RexMbCutsceneExportModelMatrixData *>( m_exportDataList[0] );
	atArray<Matrix34> &modelFrameMatrices = const_cast<atArray<Matrix34> &>( pModelAnimData->GetFrameData() );

	for ( int i = 0; i < nFrames; ++i )
	{
		Matrix34 mModel = modelFrameMatrices[i];

		Quaternion quatYUp;
		Quaternion quatZUp;
		mModel.ToQuaternion(quatYUp);
		quatZUp.x = -quatYUp.x;
		quatZUp.y = quatYUp.z;
		quatZUp.z = quatYUp.y;
		quatZUp.w = quatYUp.w;
		mModel.FromQuaternion(quatZUp);

		float y = mModel.d.y;
		mModel.d.x = -mModel.d.x;
		mModel.d.y = mModel.d.z;
		mModel.d.z = y;
		modelFrameMatrices[i] = mModel;

	}
	return true;
}


bool RexMbCutsceneExportModel::ProcessClip( ClipXML* /*clip*/ )
{
	return true;
}

bool RexMbCutsceneExportModel::ProcessClip( crClipAnimation* /*pClipAnim*/ )
{
    return true;
}

bool RexMbCutsceneExportModel::InitRexAnimation( float fStartTime, float fEndTime,
    rexObjectGenericAnimationSegment* &pAnimationSegment, rexObjectGenericAnimation* &pAnimation )
{
    char cAnimFilename[RAGE_MAX_PATH];
    sprintf( cAnimFilename, "%s.anim", m_cFilenameWithoutExtension );

    // make sure the directory exists.
    ASSET.CreateLeadingPath( cAnimFilename );

    pAnimationSegment = rage_new rexObjectGenericAnimationSegment;
    pAnimationSegment->SetName( cAnimFilename );

    pAnimationSegment->m_StartTime = fStartTime;
    pAnimationSegment->m_EndTime = fEndTime;
    pAnimationSegment->m_Autoplay = false;
    pAnimationSegment->m_Looping = false;
    pAnimationSegment->m_IgnoreChildBones = false;

    pAnimation = rage_new rexObjectGenericAnimation;
    pAnimation->m_StartTime = pAnimationSegment->m_StartTime;
    pAnimation->m_EndTime = pAnimationSegment->m_EndTime;
    pAnimation->m_FramesPerSecond = rexMBAnimExportCommon::GetFPS();
    pAnimation->m_ParentMatrix.Identity();
    pAnimation->m_ContainedObjects.PushAndGrow( pAnimationSegment, 1 );
    pAnimation->m_AuthoredOrientation = true;

    return true;
}

bool RexMbCutsceneExportModel::ExportRexAnimation( rexSerializerRAGEAnimation *pRexSer, rexObjectGenericAnimation* pAnimation )
{
    pRexSer->RegisterProgressBarTextCallback( &RexMbCutsceneExportModel::SetProgressCaption );

    ULOGGER_MOBO.SetProgressMessage("Saving animation '%s.anim'.", m_cFilenameWithoutExtension );

    rexResult res = pRexSer->Serialize( *pAnimation );

    int msgCount = res.GetMessageCount();
    if ( msgCount && (res.Errors() || res.Warnings()) )
    {
        ULOGGER_MOBO.SetProgressMessage("The following %s occurred while writing the animation:\n", res.Errors() ? "error(s)" : "warning(s)" );
        
        for(int msgIdx = 0; msgIdx < msgCount; msgIdx++)
        {
            atString msgString = res.Errors() ? atString("ERROR: ") : atString("WARNING: ");
            msgString += res.GetMessageByIndex(msgIdx);

            ULOGGER_MOBO.SetProgressMessage( msgString.c_str() );
        }
    }

    return !res.Errors();
}

void RexMbCutsceneExportModel::InitTracksInfo( float fStartTime, float fEndTime, int nFrames, atArray<rexObjectGenericAnimation::TrackInfo*>& trackList )
{
    int iTrackCount = m_trackNameList.GetCount();
    trackList.Reset();
    trackList.Reserve( iTrackCount );

    for ( int i = 0; i < iTrackCount; ++i )
    {
        rexObjectGenericAnimation::TrackInfo* pTrack = NULL;

        switch( m_trackIDList[i] )
        {
        case rexObjectGenericAnimation::TRACK_TRANSLATE:
        case rexObjectGenericAnimation::TRACK_SCALE:
        case rexObjectGenericAnimation::TRACK_CAMERA_DOF:
        case rexObjectGenericAnimation::TRACK_COLOUR:
		case rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION:
            pTrack = rage_new rexObjectGenericAnimation::TrackInfoVector3();
            break;
        case rexObjectGenericAnimation::TRACK_ROTATE:
            pTrack = rage_new rexObjectGenericAnimation::TrackInfoQuaternion();
            break;
        case rexObjectGenericAnimation::TRACK_VISEMES:
        case rexObjectGenericAnimation::TRACK_UNKNOWN:
		case rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN:
		case rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT:
		case rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN:
		case rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT:
		case rexObjectGenericAnimation::TRACK_PARTICLE:
            pTrack = rage_new rexObjectGenericAnimation::TrackInfoFloat();
            break;
        }

        if ( !pTrack )
        {
            ULOGGER_MOBO.SetProgressMessage("WARNING: Unknown track id %d for a track named '%s'.", m_trackIDList[i], m_trackNameList[i].c_str() );

            continue;
        }

        pTrack->m_Name = m_trackNameList[i];

		if(m_trackVarNameList.GetCount() == m_trackNameList.GetCount()) // Only specific models will have m_trackVarNameList set.
		{
			pTrack->m_VarName = m_trackVarNameList[i];
		}
		
        pTrack->m_TrackID = m_trackIDList[i];

        if ( InitTrackInfo( fStartTime, fEndTime, nFrames, *pTrack ) )
        {
            trackList.Append() = pTrack;
        }
        else
        {
            //Couldn't find the appropriate data for the track, so don't add it
            //TODO : Produce some sort of useful warning/error here...
            delete pTrack;
        }
    }
}

bool RexMbCutsceneExportModel::InitTrackInfo( float fStartTime, float fEndTime, int nFrames, rexObjectGenericAnimation::TrackInfo& track )
{
    track.m_StartTime = fStartTime;
    track.m_EndTime = fEndTime;

    //TODO : Maya exporter supports determining if a track is locked by the following logic
    // track.m_IsLocked = (plug.isLocked() || !plug.isKeyable() || (track.m_EndTime <= track.m_StartTime));
    // we probably should look to support some similar functionality here.
    track.m_IsLocked = false;

    track.ResetKeys( nFrames );

    return true;
}

void RexMbCutsceneExportModel::AddAnimDataToTracks( int nFrames, atArray<rexObjectGenericAnimation::TrackInfo*> &trackList )
{
    // Go through each of our data collectors and send the data to the track
    for ( int i = 0; i < m_trackExportDataList.GetCount(); ++i )
    {
        // add the data for each frame to the track
        for ( int j = 0; j < nFrames; ++j )
        {
            m_trackExportDataList[i]->WriteFrameData( j, trackList[i] );
        }
    }
}

void RexMbCutsceneExportModel::RemoveBlockTags(ClipXML* clip)
{
	const char* blockTagName = "Block";

	ULOGGER_MOBO.SetProgressMessage("Wiping clip blocking tags.");

	// Wipe any block tags in the clip before we add any new ones, since the clip is appended it will have old cut blocking tags in.
	for( int i = 0; i < clip->GetTags().GetCount(); ++i)
	{
		TagXML* blockTag = clip->GetTags()[i];
		if(stricmp(blockTag->GetProperty().GetName(),blockTagName) == 0)
		{
			clip->GetTags().Delete(i);
			--i;
		}
	}
}

void RexMbCutsceneExportModel::RemoveBlockTags(crClipAnimation *pClipAnim)
{
	const char* blockTagName = "Block";

	ULOGGER_MOBO.SetProgressMessage("Wiping '%d' clip blocking tags.", pClipAnim->GetTags()->GetNumTags("Block"));

	// Wipe any block tags in the clip before we add any new ones, since the clip is appended it will have old cut blocking tags in.
	for( int i = 0; i < pClipAnim->GetTags()->GetNumTags(); ++i)
	{
		crTag* blockTag = const_cast<crTag*>(pClipAnim->GetTags()->GetTag(i));
		if(stricmp(blockTag->GetName(),blockTagName) == 0)
		{
			pClipAnim->GetTags()->RemoveTag(*blockTag);
			--i;
		}
	}
}

void RexMbCutsceneExportModel::AddCameraCutTagBlocks( float fStartTime, float fEndTime, ClipXML* clip )
{
	atArray<float> cameraCutTimeList;
	rexMBAnimExportCommon::GetCameraCutTimes( cameraCutTimeList );

	ULOGGER_MOBO.SetProgressMessage("Clip now contains blocking tags after wipe.");

	for ( int j = 0; j < cameraCutTimeList.GetCount(); ++j )
	{
		if ( (cameraCutTimeList[j] < fStartTime) || (cameraCutTimeList[j] == 0) )
		{
			continue;
		}

		if ( cameraCutTimeList[j] > fEndTime )
		{
			break;
		}

#if USE_NEW_BLOCK_TAGS
		if( (cameraCutTimeList[j] - fStartTime) <= 0 )
		{
			continue;
		}

		// The time from the cut is on the end of the block so we pull it back half a frame to make the time be the mid. Then we calc by -/+
		// half a frame either side for the start/end.
		float halfFrame = (0.5f * (1.0f / CUTSCENE_FPS));
		// MPW - Move the cut back half a frame. The incoming cut times need to be brought back in future and this can be removed. The cut times are
		// currently pulled back in the build process to avoid disruption.
		float fTime = cameraCutTimeList[j] - halfFrame;
		float time = ((fTime - fStartTime) - halfFrame);
		float startTime = clip->ConvertTimeToPhase( ( time > 0 ) ? (time - BLOCK_TAG_PAD) : 0 );
		float endTime = clip->ConvertTimeToPhase( (fTime - fStartTime) + halfFrame + BLOCK_TAG_PAD ) ;

		TagXML* tag = rage_new TagXML();
		tag->GetProperty().SetName("Block");
		tag->SetStart( startTime );
		tag->SetEnd( endTime );
		clip->GetTags().PushAndGrow(tag);
#else
		// add a block tag for each camera cut
		float thirtiethSecond = 1.0f / rexMBAnimExportCommon::GetFPS();
		// animations come out from start-end not 0-end now, so we need to pull the blocking tags back as they would previously of being fixed on build cropping
		float time = (cameraCutTimeList[j] - fStartTime) - thirtiethSecond;
		float startTime = pClipAnim->ConvertTimeToPhase( ( time > 0 ) ? time : 0 );
		float endTime = pClipAnim->ConvertTimeToPhase( (cameraCutTimeList[j] - fStartTime) + thirtiethSecond ) ;

		crTag pTag;
		pTag.GetProperty().SetName("Block");
		pTag.SetStart( startTime );
		pTag.SetEnd( endTime );
		pClipAnim->GetTags()->AddUniqueTag( pTag );	
#endif
	}
}

void RexMbCutsceneExportModel::AddCameraCutTagBlocks( float fStartTime, float fEndTime, crClipAnimation *pClipAnim )
{
    atArray<float> cameraCutTimeList;
    rexMBAnimExportCommon::GetCameraCutTimes( cameraCutTimeList );

	ULOGGER_MOBO.SetProgressMessage("Clip now contains blocking tags after wipe.");

	for ( int j = 0; j < cameraCutTimeList.GetCount(); ++j )
    {
        if ( (cameraCutTimeList[j] < fStartTime) || (cameraCutTimeList[j] == 0) )
        {
            continue;
        }

        if ( cameraCutTimeList[j] > fEndTime )
        {
            break;
        }

#if USE_NEW_BLOCK_TAGS
		if( (cameraCutTimeList[j] - fStartTime) <= 0 )
		{
			continue;
		}

		// The time from the cut is on the end of the block so we pull it back half a frame to make the time be the mid. Then we calc by -/+
		// half a frame either side for the start/end.
		float halfFrame = (0.5f * (1.0f / CUTSCENE_FPS));
		// MPW - Move the cut back half a frame. The incoming cut times need to be brought back in future and this can be removed. The cut times are
		// currently pulled back in the build process to avoid disruption.
		float fTime = cameraCutTimeList[j] - halfFrame;
		float time = ((fTime - fStartTime) - halfFrame);
		float startTime = pClipAnim->ConvertTimeToPhase( ( time > 0 ) ? (time - BLOCK_TAG_PAD) : 0 );
		float endTime = pClipAnim->ConvertTimeToPhase( (fTime - fStartTime) + halfFrame + BLOCK_TAG_PAD ) ;

		crTag pTag;
		pTag.GetProperty().SetName("Block");
		pTag.SetStart( startTime );
		pTag.SetEnd( endTime );
		pClipAnim->GetTags()->AddUniqueTag( pTag );
#else
		// add a block tag for each camera cut
		float thirtiethSecond = 1.0f / rexMBAnimExportCommon::GetFPS();
		// animations come out from start-end not 0-end now, so we need to pull the blocking tags back as they would previously of being fixed on build cropping
		float time = (cameraCutTimeList[j] - fStartTime) - thirtiethSecond;
		float startTime = pClipAnim->ConvertTimeToPhase( ( time > 0 ) ? time : 0 );
		float endTime = pClipAnim->ConvertTimeToPhase( (cameraCutTimeList[j] - fStartTime) + thirtiethSecond ) ;

		crTag pTag;
		pTag.GetProperty().SetName("Block");
		pTag.SetStart( startTime );
		pTag.SetEnd( endTime );
		pClipAnim->GetTags()->AddUniqueTag( pTag );	
#endif
    }
}

#define TOYBOX_BLOCK_PROPERTY_NAME "Toybox_Block_DO_NOT_RESOURCE"


void RexMbCutsceneExportModel::AddToyboxTagBlocks( float fStartTime, float fEndTime, ClipXML* clip )
{
	const char* blockTagName = "Block";

	//ULOGGER_MOBO.SetProgressMessage("Creating blocking tags for '%d'.", pClipAnim->GetTags()->GetNumTags("Block"));

	for (int frameIndex = 0; frameIndex < m_toyboxData->frameList.GetCount(); ++frameIndex)
	{
		float frameTime = m_toyboxData->frameList[frameIndex] / rexMBAnimExportCommon::GetFPS();

		if (frameTime < fStartTime || frameTime == 0)
			continue;

		if (frameTime > fEndTime)
			continue;

		float halfFrame = (0.5f * (1.0f / CUTSCENE_FPS));
		float fTime = frameTime - halfFrame;
		float time = ((fTime - fStartTime) - halfFrame);
		float startTime = clip->ConvertTimeToPhase( ( time > 0 ) ? (time - BLOCK_TAG_PAD) : 0 );
		float endTime = clip->ConvertTimeToPhase( (fTime - fStartTime) + halfFrame + BLOCK_TAG_PAD ) ;

		TagXML* tag = rage_new TagXML();
		tag->GetProperty().SetName(blockTagName);
		tag->SetStart( startTime );
		tag->SetEnd( endTime );

		PropertyAttributeStringXML* toyboxAttribute = rage_new PropertyAttributeStringXML();
		toyboxAttribute->SetName( TOYBOX_BLOCK_PROPERTY_NAME );
		toyboxAttribute->GetString() = "Toybox Block Tag";
		tag->GetProperty().AddAttribute(toyboxAttribute);

		clip->GetTags().PushAndGrow(tag);
	}
}

void RexMbCutsceneExportModel::AddToyboxTagBlocks( float fStartTime, float fEndTime, crClipAnimation *pClipAnim )
{
	const char* blockTagName = "Block";

	ULOGGER_MOBO.SetProgressMessage("Creating blocking tags for '%d'.", pClipAnim->GetTags()->GetNumTags("Block"));

	for (int frameIndex = 0; frameIndex < m_toyboxData->frameList.GetCount(); ++frameIndex)
	{
		float frameTime = m_toyboxData->frameList[frameIndex] / rexMBAnimExportCommon::GetFPS();

		if (frameTime < fStartTime || frameTime == 0)
			continue;

		if (frameTime > fEndTime)
			continue;

		float halfFrame = (0.5f * (1.0f / CUTSCENE_FPS));
		float fTime = frameTime - halfFrame;
		float time = ((fTime - fStartTime) - halfFrame);
		float startTime = pClipAnim->ConvertTimeToPhase( ( time > 0 ) ? (time - BLOCK_TAG_PAD) : 0 );
		float endTime = pClipAnim->ConvertTimeToPhase( (fTime - fStartTime) + halfFrame + BLOCK_TAG_PAD ) ;

		crTag pTag;
		pTag.GetProperty().SetName(blockTagName);

		crPropertyAttributeString toyboxAttribute;
		toyboxAttribute.SetName( TOYBOX_BLOCK_PROPERTY_NAME );
		toyboxAttribute.GetString() = "Toybox Block Tag";
		pTag.GetProperty().AddAttribute(toyboxAttribute);
		pTag.SetStart( startTime );
		pTag.SetEnd( endTime );
		pClipAnim->GetTags()->AddUniqueTag( pTag );
	}
}

//#############################################################################

RexMbCutsceneExportGenericModel::RexMbCutsceneExportGenericModel( HFBModel pModel, SToyboxEntry* pToyboxEntry, const char *pFilenameWithoutExtension )
: RexMbCutsceneExportModel( pModel, pToyboxEntry, pFilenameWithoutExtension )
{

}

RexMbCutsceneExportGenericModel::~RexMbCutsceneExportGenericModel()
{
    
}

bool RexMbCutsceneExportGenericModel::Initialize( int nFrames )
{
    // Translate and Rotate
    RexMbCutsceneExportModelMatrixData *pAnimData = rage_new RexMbCutsceneExportModelMatrixData( m_pModel, nFrames, 1.0f );
    m_exportDataList.PushAndGrow( pAnimData );

    m_trackExportDataList.PushAndGrow( pAnimData );
    m_trackNameList.PushAndGrow( atString("translate") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_TRANSLATE );

    m_trackExportDataList.PushAndGrow( pAnimData );
    m_trackNameList.PushAndGrow( atString("rotate") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_ROTATE );

    return true;
}

rexSerializerRAGEAnimation* RexMbCutsceneExportGenericModel::CreateAnimationSerializer()
{
    rexSerializerRAGEAnimation *pRexSer = rage_new rexSerializerRAGEAnimation;

    pRexSer->AddChannelToWrite( atString("translateX"), atString("translateX"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("translateY"), atString("translateY"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("translateZ"), atString("translateZ"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("rotateX"), atString("rotateX"), rexObjectGenericAnimation::TRACK_ROTATE, false );
    pRexSer->AddChannelToWrite( atString("rotateY"), atString("rotateY"), rexObjectGenericAnimation::TRACK_ROTATE, false );
    pRexSer->AddChannelToWrite( atString("rotateZ"), atString("rotateZ"), rexObjectGenericAnimation::TRACK_ROTATE, false );

    return pRexSer;
}

//#############################################################################
RexMbCutsceneExportCameraModel::RexMbCutsceneExportCameraModel( HFBModel pModel, SToyboxEntry* pToyboxEntry, const char *pFilenameWithoutExtension )
: RexMbCutsceneExportModel( pModel, pToyboxEntry, pFilenameWithoutExtension )
{

}

RexMbCutsceneExportCameraModel::~RexMbCutsceneExportCameraModel()
{

}

bool RexMbCutsceneExportCameraModel::Initialize( int nFrames )
{
    HFBCamera pCameraModel = dynamic_cast<HFBCamera>( m_pModel );
    /*if ( !pCameraModel->Interest )
    {
        RexMbCutsceneExportModel::SetProgressMessage( "ERROR: No interest object for camera." );
        return false;
    }*/

    // Translate and Rotate

	RexMbCutsceneExportModelMatrixData *pCameraAnimData = rage_new RexMbCutsceneExportModelMatrixData( m_pModel, nFrames, 0.01f, true );
    m_exportDataList.PushAndGrow( pCameraAnimData );

    m_trackExportDataList.PushAndGrow( pCameraAnimData );
    m_trackNameList.PushAndGrow( atString("cameraTranslation") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_TRANSLATE );

    m_trackExportDataList.PushAndGrow( pCameraAnimData );
    m_trackNameList.PushAndGrow( atString("cameraRotation") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_ROTATE );

    // Interest
	if(pCameraModel->Interest)
	{
		RexMbCutsceneExportModelMatrixData *pCameraInterestAnimData = rage_new RexMbCutsceneExportModelMatrixData( pCameraModel->Interest, nFrames, 0.01f );
		m_exportDataList.PushAndGrow( pCameraInterestAnimData );
	}

    // Field of View
    RexMbCutsceneExportFloatPropertyData *pFOVAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "FieldOfView", m_pModel, nFrames );
    m_exportDataList.PushAndGrow( pFOVAnimData );
    m_trackExportDataList.PushAndGrow( pFOVAnimData );
    m_trackNameList.PushAndGrow( atString("cameraFOV") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	// Depth of Field Strength
	RexMbCutsceneExportFloatPropertyData *pDOFStrengthAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "DOF Strength", m_pModel, nFrames );
	m_exportDataList.PushAndGrow( pDOFStrengthAnimData );
	m_trackExportDataList.PushAndGrow( pDOFStrengthAnimData );
	m_trackNameList.PushAndGrow( atString("cameraDOFStrength") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	// Circle of Confusion
	RexMbCutsceneExportIntegerPropertyData *pCircleOfConfusionAnimData = rage_new RexMbCutsceneExportIntegerPropertyData( "CoC", m_pModel, nFrames );
	m_exportDataList.PushAndGrow( pCircleOfConfusionAnimData );
	m_trackExportDataList.PushAndGrow( pCircleOfConfusionAnimData );
	m_trackNameList.PushAndGrow( atString("cameraCoC") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	// Circle of Confusion Night
	RexMbCutsceneExportIntegerPropertyData *pCircleOfConfusionNightAnimData = rage_new RexMbCutsceneExportIntegerPropertyData( "CoC Night", m_pModel, nFrames );
	m_exportDataList.PushAndGrow( pCircleOfConfusionNightAnimData );
	m_trackExportDataList.PushAndGrow( pCircleOfConfusionNightAnimData );
	m_trackNameList.PushAndGrow( atString("cameraNightCoC") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

    // Depth of Field
    RexMbCutsceneExportDepthOfFieldData *pDOFAnimData = rage_new RexMbCutsceneExportDepthOfFieldData( m_pModel, nFrames, 0.01f );
    if ( (pDOFAnimData->GetNearPlaneModel() == NULL) || (pDOFAnimData->GetFarPlaneModel() == NULL) )
    {
        ULOGGER_MOBO.SetProgressMessage( "WARNING: Missing one or both of the Near/Far Plane objects.  Going with defaults." );
    }

    m_exportDataList.PushAndGrow( pDOFAnimData );
    m_trackExportDataList.PushAndGrow( pDOFAnimData );
    m_trackNameList.PushAndGrow( atString("cameraDOF") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_CAMERA_DOF );

	// Depth of Field / 4 Planes
	RexMbCutsceneExportDepthOfField4PlanesData *pDOF4AnimData = rage_new RexMbCutsceneExportDepthOfField4PlanesData( m_pModel, nFrames, 0.01f );
	if ( (pDOF4AnimData->GetNearPlaneModel() == NULL) || (pDOF4AnimData->GetFarPlaneModel() == NULL) )
	{
		ULOGGER_MOBO.SetProgressMessage( "WARNING: Missing one or both of the Near/Far Plane objects.  Going with defaults." );
	}
	m_exportDataList.PushAndGrow( pDOF4AnimData );

	m_trackExportDataList.PushAndGrow( pDOF4AnimData );
	m_trackNameList.PushAndGrow( atString("cameraDepthOfFieldNearOutOfFocusPlane") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT );

	m_trackExportDataList.PushAndGrow( pDOF4AnimData );
	m_trackNameList.PushAndGrow( atString("cameraDepthOfFieldNearInFocusPlane") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN );

	m_trackExportDataList.PushAndGrow( pDOF4AnimData );
	m_trackNameList.PushAndGrow( atString("cameraDepthOfFieldFarOutOfFocusPlane") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT );

	m_trackExportDataList.PushAndGrow( pDOF4AnimData );
	m_trackNameList.PushAndGrow( atString("cameraDepthOfFieldFarInFocusPlane") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN );

    // Roll
    RexMbCutsceneExportFloatPropertyData *pRollAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Roll", m_pModel, nFrames );
    m_exportDataList.PushAndGrow( pRollAnimData );

	// Shallow Depth of Field
	//RexMbCutsceneExportBoolPropertyData *pShallowDOFAnimData = rage_new RexMbCutsceneExportBoolPropertyData( "Shallow_DOF", m_pModel, nFrames );
	//m_exportDataList.PushAndGrow( pShallowDOFAnimData );
	//m_trackExportDataList.PushAndGrow( pShallowDOFAnimData );
	//m_trackNameList.PushAndGrow( atString("cameraShallowDOF") );
	//m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	// Motion Blur
	RexMbCutsceneExportFloatPropertyData *pMotionBlueAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Motion_Blur", m_pModel, nFrames );
	m_exportDataList.PushAndGrow( pMotionBlueAnimData );
	m_trackExportDataList.PushAndGrow( pMotionBlueAnimData );
	m_trackNameList.PushAndGrow( atString("cameraMotionBlur") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	// Simple Depth of Field
	//RexMbCutsceneExportBoolPropertyData *pSimpleDOFAnimData = rage_new RexMbCutsceneExportBoolPropertyData( "Simple_DOF", m_pModel, nFrames );
	//m_exportDataList.PushAndGrow( pSimpleDOFAnimData );
	//m_trackExportDataList.PushAndGrow( pSimpleDOFAnimData );
	//m_trackNameList.PushAndGrow( atString("cameraSimpleDOF") );
	//m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	RexMbCutsceneExportFloatPropertyData *pFocusAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "FOCUS (cm)", m_pModel, nFrames );
	m_exportDataList.PushAndGrow( pFocusAnimData );
	m_trackExportDataList.PushAndGrow( pFocusAnimData );
	m_trackNameList.PushAndGrow( atString("cameraFocus") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

    return true;
}

void RexMbCutsceneExportCameraModel::GetCameraCutData( atArray<SCameraCutData> &cameraCutDataList )
{
    HFBCamera pCameraModel = dynamic_cast<HFBCamera>( m_pModel );

    atArray<SCameraCutKeyData> cameraCutKeyDataList;
	if(!rexMBAnimExportCommon::GetCameraCutShots( cameraCutKeyDataList ))
	{
		rexMBAnimExportCommon::GetCameraCutKeys( cameraCutKeyDataList );
	}

    int nFrames = cameraCutKeyDataList.GetCount();

    cameraCutDataList.Reset();
    cameraCutDataList.Reserve( nFrames );

    RexMbCutsceneExportModelMatrixData cameraAnimData( m_pModel, nFrames, 0.01f );
    RexMbCutsceneExportModelMatrixData cameraInterestAnimData( pCameraModel->Interest, nFrames, 0.01f );

    FBSystem system;
    FBPlayerControl controller;

    FBTime preSampleTime = system.LocalTime;

    // Collect the translation and rotation data
    for ( int i = 0; i < nFrames; ++i )
    {
        FBTime fbt;
        fbt.SetSecondDouble( (double)cameraCutKeyDataList[i].fTime );
        controller.Goto( fbt );

        system.Scene->Evaluate();

        cameraAnimData.ReadFrameData();

		if(pCameraModel->Interest)
		{
			cameraInterestAnimData.ReadFrameData();
		}
    }

    controller.Goto( preSampleTime );

    // Build the SCameraCutData structs
    const atArray<Matrix34> &cameraFrameMatrices = cameraAnimData.GetFrameData();
    const atArray<Matrix34> &cameraInterestFrameMatrices = cameraInterestAnimData.GetFrameData();

    for ( int i = 0; i < nFrames; ++i )
    {
        SCameraCutData cameraCutData;

        // time
        cameraCutData.fTime = cameraCutKeyDataList[i].fTime;

        // camera name
        //HFBCamera pCamera = system.Scene->Cameras[cameraCutKeyDataList[i].iCameraIndex + 6];    // add 6 to skip past the built-in cameras
        //safecpy( cameraCutData.cName, pCamera->Name.AsString(), sizeof(cameraCutData.cName) );
		safecpy( cameraCutData.cName, cameraCutKeyDataList[i].cName, sizeof(cameraCutData.cName) );

        Matrix34 mCam = cameraFrameMatrices[i];

        // transformation matrix
        mCam.Identity3x3();

		if(pCameraModel->Interest)
		{
			mCam.LookAt( cameraInterestFrameMatrices[i].d );
		}
		else
		{
			// Create a fake interest, our camera needs some kind of point of interest for the direction
			Vector3 forwardVector(1.0f,0.0f,0.0f);
			Vector3 forwardCameraVector;
			Vector3 forwardWorldVector;

			Matrix34 camMatrix = cameraFrameMatrices[i];
			camMatrix.Transform(forwardVector, forwardCameraVector); // transfer into camera space
			Matrix34 worldMatrix;
			worldMatrix.Identity();
			worldMatrix.UnTransform(forwardCameraVector, forwardWorldVector); // transfer into world space

			rexMBAnimExportCommon::LookAt( forwardWorldVector, mCam );
		}

        cameraCutData.vPosition = mCam.d;

        Quaternion q;
        mCam.ToQuaternion( q );
        cameraCutData.vRotationQuaternion.Set( q.x, q.y, q.z, q.w );

        cameraCutDataList.Append() = cameraCutData;
    }
}

bool RexMbCutsceneExportCameraModel::ProcessCollectedAnimData( int nFrames )
{
	RexMbCutsceneExportModelMatrixData *pCameraAnimData = dynamic_cast<RexMbCutsceneExportModelMatrixData *>( m_exportDataList[0] );
	atArray<Matrix34> &cameraFrameMatrices = const_cast<atArray<Matrix34> &>( pCameraAnimData->GetFrameData() );

	// dummy refs, annoying but ...
	atArray<Matrix34> dummy1;
	atArray<Vector3> dummy2;
	atArray<Matrix34> &cameraInterestFrameMatrices = dummy1 ;
	atArray<Vector3> &rollFrameVectors = dummy2 ;
	
	HFBCamera pCameraModel = dynamic_cast<HFBCamera>( m_pModel );
	if ( pCameraModel->Interest )
	{
		RexMbCutsceneExportModelMatrixData *pCameraInterestAnimData = dynamic_cast<RexMbCutsceneExportModelMatrixData *>( m_exportDataList[1] );
		cameraInterestFrameMatrices = pCameraInterestAnimData->GetFrameData();

		RexMbCutsceneExportFloatPropertyData *pRollAnimData = dynamic_cast<RexMbCutsceneExportFloatPropertyData *>( m_exportDataList[8] );
		rollFrameVectors = pRollAnimData->GetFrameData();
	}
	else
	{
		RexMbCutsceneExportFloatPropertyData *pRollAnimData = dynamic_cast<RexMbCutsceneExportFloatPropertyData *>( m_exportDataList[7] );
		rollFrameVectors = pRollAnimData->GetFrameData();
	}

    for ( int i = 0; i < nFrames; ++i )
    {
        Matrix34 mCam = cameraFrameMatrices[i];

		Vector3 upVector = Vector3( 0.0f, 1.0f, 0.0f );
		Vector3 upCameraVector;

		// transform up vector into camera space
		mCam.Transform( upVector, upCameraVector );
		upCameraVector = upCameraVector - mCam.d;
		upCameraVector.Normalize();

        // transformation matrix
        mCam.Identity3x3();

		if ( pCameraModel->Interest )
		{
#if HACK_GTA4
			rexMBAnimExportCommon::LookAt( cameraInterestFrameMatrices[i].d, mCam, upCameraVector );
#else
			mCam.LookAt( cameraInterestFrameMatrices[i].d );
#endif // HACK_GTA4
		}
		else
		{
			// Create a fake interest, our camera needs some kind of point of interest for the direction
			Vector3 forwardVector(1.0f,0.0f,0.0f);
			Vector3 forwardCameraVector;
			Vector3 forwardWorldVector;

			Matrix34 camMatrix = cameraFrameMatrices[i];
			camMatrix.Transform(forwardVector, forwardCameraVector); // transfer into camera space
			Matrix34 worldMatrix;
			worldMatrix.Identity();
			worldMatrix.UnTransform(forwardCameraVector, forwardWorldVector); // transfer into world space

			rexMBAnimExportCommon::LookAt( forwardWorldVector, mCam, upCameraVector );
		}

		// Add roll
        mCam.RotateLocalZ( rollFrameVectors[i].GetX() * DtoR );

		// Fix up the cameras otherwise they are 180 the wrong way in-game (previously fixed up at runtime by tom)
		mCam.RotateFullY(PI);

		Quaternion quatYUp;
		Quaternion quatZUp;
		mCam.ToQuaternion(quatYUp);
		quatZUp.x = -quatYUp.x;
		quatZUp.y = quatYUp.z;
		quatZUp.z = quatYUp.y;
		quatZUp.w = quatYUp.w;
		mCam.FromQuaternion(quatZUp);

		float y = mCam.d.y;
		mCam.d.x = -mCam.d.x;
		mCam.d.y = mCam.d.z;
		mCam.d.z = y;        
		
        cameraFrameMatrices[i] = mCam;
    }

    return true;
}

rexSerializerRAGEAnimation* RexMbCutsceneExportCameraModel::CreateAnimationSerializer()
{
    rexSerializerRAGEAnimation *pRexSer = rage_new rexSerializerRAGEAnimation;

    pRexSer->AddChannelToWrite( atString("cameraTranslation"), atString("cameraTranslation"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    //pRexSer->AddChannelToWrite( atString("cameraTranslationY"), atString("cameraTranslationY"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    //pRexSer->AddChannelToWrite( atString("cameraTranslationZ"), atString("cameraTranslationZ"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("cameraRotation"), atString("cameraRotation"), rexObjectGenericAnimation::TRACK_ROTATE, false );
    //pRexSer->AddChannelToWrite( atString("cameraRotationY"), atString("cameraRotationY"), rexObjectGenericAnimation::TRACK_ROTATE, false );
    //pRexSer->AddChannelToWrite( atString("cameraRotationZ"), atString("cameraRotationZ"), rexObjectGenericAnimation::TRACK_ROTATE, false );
    pRexSer->AddChannelToWrite( atString("cameraFOV"), atString("cameraFOV"), rexObjectGenericAnimation::TRACK_UNKNOWN, false );
    pRexSer->AddChannelToWrite( atString("cameraDOF"), atString("cameraDOF"), rexObjectGenericAnimation::TRACK_CAMERA_DOF, false );
	pRexSer->AddChannelToWrite( atString("cameraDOFStrength"), atString("cameraDOFStrength"), rexObjectGenericAnimation::TRACK_UNKNOWN, false );
	pRexSer->AddChannelToWrite( atString("cameraCoC"), atString("cameraCoC"), rexObjectGenericAnimation::TRACK_UNKNOWN, false );
	pRexSer->AddChannelToWrite( atString("cameraNightCoC"), atString("cameraNightCoC"), rexObjectGenericAnimation::TRACK_UNKNOWN, false );
	//pRexSer->AddChannelToWrite( atString("cameraShallowDOF"), atString("cameraShallowDOF"), rexObjectGenericAnimation::TRACK_UNKNOWN, false );
	//pRexSer->AddChannelToWrite( atString("cameraSimpleDOF"), atString("cameraSimpleDOF"), rexObjectGenericAnimation::TRACK_UNKNOWN, false );
	pRexSer->AddChannelToWrite( atString("cameraMotionBlur"), atString("cameraMotionBlur"), rexObjectGenericAnimation::TRACK_UNKNOWN, false );
	pRexSer->AddChannelToWrite( atString("cameraFocus"), atString("cameraFocus"), rexObjectGenericAnimation::TRACK_UNKNOWN, false );
	pRexSer->AddChannelToWrite( atString("cameraDepthOfFieldNearOutOfFocusPlane"), atString("cameraDepthOfFieldNearOutOfFocusPlane"), rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT, false );
	pRexSer->AddChannelToWrite( atString("cameraDepthOfFieldNearInFocusPlane"), atString("cameraDepthOfFieldNearInFocusPlane"), rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN, false );
	pRexSer->AddChannelToWrite( atString("cameraDepthOfFieldFarOutOfFocusPlane"), atString("cameraDepthOfFieldFarOutOfFocusPlane"), rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT, false );
	pRexSer->AddChannelToWrite( atString("cameraDepthOfFieldFarInFocusPlane"), atString("cameraDepthOfFieldFarInFocusPlane"), rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN, false );

    return pRexSer;
}

//#############################################################################

RexMbCutsceneExportLightModel::RexMbCutsceneExportLightModel( HFBModel pModel, SToyboxEntry* pToyboxEntry, const char *pFilenameWithoutExtension )
: RexMbCutsceneExportModel( pModel, pToyboxEntry, pFilenameWithoutExtension )
{

}

RexMbCutsceneExportLightModel::~RexMbCutsceneExportLightModel()
{

}

bool RexMbCutsceneExportLightModel::Initialize( int nFrames )
{
    char LightName[1024];
	safecpy( LightName, (const char*)m_pModel->LongName, sizeof(LightName) );

    HFBLight pLightModel = (HFBLight)m_pModel;
    if ( pLightModel->LightType != kFBLightTypeInfinite )
    {
        // Fall Off attribute is required
        HFBProperty FallOffNode = m_pModel->PropertyList.Find( "Fall Off" );
        if ( !FallOffNode )
        {
            ULOGGER_MOBO.SetProgressMessage("WARNING: The %s light '%s' does not have the custom attribute 'Fall Off'.  The default value of 0 was used in its place.", 
                pLightModel->LightType == kFBLightTypeSpot ? "spot" : "point", LightName );
        }

		// Exp Fall Off attribute is required
		HFBProperty ExpFallOffNode = m_pModel->PropertyList.Find( "Exp Fall Off" );
		if ( !ExpFallOffNode )
		{
			ULOGGER_MOBO.SetProgressMessage("WARNING: The %s light '%s' does not have the custom attribute 'Exp Fall Off'.  The default value of 0 was used in its place.", 
				pLightModel->LightType == kFBLightTypeSpot ? "spot" : "point", LightName );
		}
    }

    // Translate and Rotate
    RexMbCutsceneExportModelMatrixData *pLightAnimData = rage_new RexMbCutsceneExportModelMatrixData( m_pModel, nFrames, 0.01f );
    m_exportDataList.PushAndGrow( pLightAnimData );

    m_trackExportDataList.PushAndGrow( pLightAnimData );
    m_trackNameList.PushAndGrow( atString("translate") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_TRANSLATE );

    m_trackExportDataList.PushAndGrow( pLightAnimData );
    m_trackNameList.PushAndGrow( atString("lightDirection") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION );

    // Light Type-specific data
    switch ( pLightModel->LightType )
    {
    case kFBLightTypePoint:
        {
            RexMbCutsceneExportVectorPropertyData *pColorAnimData = rage_new RexMbCutsceneExportVectorPropertyData( "Color", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pColorAnimData );

            m_trackExportDataList.PushAndGrow( pColorAnimData );
            m_trackNameList.PushAndGrow( atString("color") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_COLOUR );

            RexMbCutsceneExportFloatPropertyData *pIntensityAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Intensity", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pIntensityAnimData );

            m_trackExportDataList.PushAndGrow( pIntensityAnimData );
            m_trackNameList.PushAndGrow( atString("lightIntensity") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

            RexMbCutsceneExportFloatPropertyData *pFallOffAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Fall Off", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pFallOffAnimData );

            m_trackExportDataList.PushAndGrow( pFallOffAnimData );
            m_trackNameList.PushAndGrow( atString("lightFallOff") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

			RexMbCutsceneExportFloatPropertyData *pExpFallOffAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Exp Fall Off", m_pModel, nFrames );
			m_exportDataList.PushAndGrow( pExpFallOffAnimData );

			m_trackExportDataList.PushAndGrow( pExpFallOffAnimData );
			m_trackNameList.PushAndGrow( atString("lightExpFallOff") );
			m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );
        }
        break;
    case kFBLightTypeSpot:
        {
            RexMbCutsceneExportVectorPropertyData *pColorAnimData = rage_new RexMbCutsceneExportVectorPropertyData( "Color", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pColorAnimData );

            m_trackExportDataList.PushAndGrow( pColorAnimData );
            m_trackNameList.PushAndGrow( atString("color") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_COLOUR );

            RexMbCutsceneExportFloatPropertyData *pIntensityAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Intensity", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pIntensityAnimData );

            m_trackExportDataList.PushAndGrow( pIntensityAnimData );
            m_trackNameList.PushAndGrow( atString("lightIntensity") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

            RexMbCutsceneExportFloatPropertyData *pFallOffAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Fall Off", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pFallOffAnimData );

            m_trackExportDataList.PushAndGrow( pFallOffAnimData );
            m_trackNameList.PushAndGrow( atString("lightFallOff") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

            RexMbCutsceneExportFloatPropertyData *pConeAngleAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Cone angle", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pConeAngleAnimData );

            m_trackExportDataList.PushAndGrow( pConeAngleAnimData );
            m_trackNameList.PushAndGrow( atString("lightConeAngle") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

			RexMbCutsceneExportFloatPropertyData *pExpFallOffAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Exp Fall Off", m_pModel, nFrames );
			m_exportDataList.PushAndGrow( pExpFallOffAnimData );

			m_trackExportDataList.PushAndGrow( pExpFallOffAnimData );
			m_trackNameList.PushAndGrow( atString("lightExpFallOff") );
			m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );
        }
        break;
    case kFBLightTypeInfinite:
    default:
        {
            RexMbCutsceneExportVectorPropertyData *pColorAnimData = rage_new RexMbCutsceneExportVectorPropertyData( "Color", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pColorAnimData );

            m_trackExportDataList.PushAndGrow( pColorAnimData );
            m_trackNameList.PushAndGrow( atString("color") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_COLOUR );

            RexMbCutsceneExportFloatPropertyData *pIntensityAnimData = rage_new RexMbCutsceneExportFloatPropertyData( "Intensity", m_pModel, nFrames );
            m_exportDataList.PushAndGrow( pIntensityAnimData );

            m_trackExportDataList.PushAndGrow( pIntensityAnimData );
            m_trackNameList.PushAndGrow( atString("lightIntensity") );
            m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );
        }
        break;
    }

    return true;
}

bool RexMbCutsceneExportLightModel::ProcessCollectedAnimData( int nFrames )
{
	RexMbCutsceneExportModelMatrixData *pLightAnimData = dynamic_cast<RexMbCutsceneExportModelMatrixData *>( m_exportDataList[0] );
	atArray<Matrix34> &lightsFrameMatrices = const_cast<atArray<Matrix34> &>( pLightAnimData->GetFrameData() );

	for ( int i = 0; i < nFrames; ++i )
	{
		// Fix up the particles otherwise they are 180 the wrong way in-game (previously fixed up at runtime by tom)
		lightsFrameMatrices[i].RotateFullY(PI);

		Quaternion quatYUp;
		Quaternion quatZUp;
		lightsFrameMatrices[i].ToQuaternion(quatYUp);
		quatZUp.x = -quatYUp.x;
		quatZUp.y = quatYUp.z;
		quatZUp.z = quatYUp.y;
		quatZUp.w = quatYUp.w;
		lightsFrameMatrices[i].FromQuaternion(quatZUp);

		float y = lightsFrameMatrices[i].d.y;
		lightsFrameMatrices[i].d.x = -lightsFrameMatrices[i].d.x;
		lightsFrameMatrices[i].d.y = lightsFrameMatrices[i].d.z;
		lightsFrameMatrices[i].d.z = y;    
	}

	return true;
}

rexSerializerRAGEAnimation* RexMbCutsceneExportLightModel::CreateAnimationSerializer()
{
    rexSerializerRAGEAnimation *pRexSer = rage_new rexSerializerRAGEAnimation;

    pRexSer->AddChannelToWrite( atString("translateX"), atString("translateX"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("translateY"), atString("translateY"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("translateZ"), atString("translateZ"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("lightDirection"), atString("lightDirection"), rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION, true );
    pRexSer->AddChannelToWrite( atString("color"), atString("color"), rexObjectGenericAnimation::TRACK_COLOUR, true );
    pRexSer->AddChannelToWrite( atString("lightIntensity"), atString("lightIntensity"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );

    HFBLight pLightModel = (HFBLight)m_pModel;
    switch ( pLightModel->LightType )
    {
    case kFBLightTypePoint:
        {
            pRexSer->AddChannelToWrite( atString("lightFallOff"), atString("lightFallOff"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );
			pRexSer->AddChannelToWrite( atString("lightExpFallOff"), atString("lightExpFallOff"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );
        }
        break;
    case kFBLightTypeSpot:
        {
            pRexSer->AddChannelToWrite( atString("lightFallOff"), atString("lightFallOff"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );
            pRexSer->AddChannelToWrite( atString("lightConeAngle"), atString("lightConeAngle"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );
			pRexSer->AddChannelToWrite( atString("lightExpFallOff"), atString("lightExpFallOff"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );
        }
        break;
    case kFBLightTypeInfinite:
    default:
        {
            // nothing more to add
        }
        break;
    }

    return pRexSer;
}

//#############################################################################

RexMbCutsceneExportParticleEffectModel::RexMbCutsceneExportParticleEffectModel( HFBModel pModel, SToyboxEntry* pToyboxInfo, const char *pFilenameWithoutExtension )
: RexMbCutsceneExportModel( pModel, pToyboxInfo, pFilenameWithoutExtension )
, m_bIsConstrained(false)
, m_iNumEvoParams(0)
{

}

RexMbCutsceneExportParticleEffectModel::~RexMbCutsceneExportParticleEffectModel()
{

}

bool RexMbCutsceneExportParticleEffectModel::Initialize( int nFrames )
{
    // Translate and Rotate
    RexMbCutsceneExportModelMatrixData *pParticleEffectAnimData = rage_new RexMbCutsceneExportModelMatrixData( m_pModel, nFrames, 0.01f );
    m_exportDataList.Grow() = pParticleEffectAnimData;

    m_trackExportDataList.PushAndGrow( pParticleEffectAnimData );
    m_trackNameList.PushAndGrow( atString("translate") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_TRANSLATE );
	m_trackVarNameList.PushAndGrow( atString(""));

    m_trackExportDataList.PushAndGrow( pParticleEffectAnimData );
    m_trackNameList.PushAndGrow( atString("rotate") );
    m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_ROTATE );
	m_trackVarNameList.PushAndGrow( atString(""));

    // See if there's a parent constraint
    HFBModel pConstraintParentObj = rexMBAnimExportCommon::GetConstraintObject( m_pModel );
    if ( pConstraintParentObj )
    {        
        RexMbCutsceneExportModelMatrixData *pConstraintAnimData = rage_new RexMbCutsceneExportModelMatrixData( pConstraintParentObj, nFrames, 0.01f, false, true );
        m_exportDataList.PushAndGrow( pConstraintAnimData );
        
        m_bIsConstrained = true;
    }
    
    // get the names of the evo parameters we should have
    atArray<ConstString> evoParamNameList;
    GetEvoParamNames( evoParamNameList );

    m_iNumEvoParams = evoParamNameList.GetCount();
    for ( int i = 0; i < m_iNumEvoParams; ++i )
    {
        RexMbCutsceneExportFloatPropertyData *pFloatPropertyAnimData = rage_new RexMbCutsceneExportFloatPropertyData( evoParamNameList[i].c_str(), m_pModel, nFrames );

        m_exportDataList.PushAndGrow( pFloatPropertyAnimData );
        m_trackExportDataList.PushAndGrow( pFloatPropertyAnimData );

        char cTrackName[16];
        sprintf( cTrackName, "particleData" );
        m_trackNameList.PushAndGrow( atString(cTrackName) );
		m_trackVarNameList.PushAndGrow( atString(evoParamNameList[i].c_str()) );
		m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_PARTICLE );
    }

    return true;
}

bool RexMbCutsceneExportParticleEffectModel::Reset()
{    
    m_bIsConstrained = false;
    m_iNumEvoParams = 0;

    return RexMbCutsceneExportModel::Reset();
}

bool RexMbCutsceneExportParticleEffectModel::ProcessCollectedAnimData( int nFrames )
{
    RexMbCutsceneExportModelMatrixData *pParticleEffectAnimData = dynamic_cast<RexMbCutsceneExportModelMatrixData *>( m_exportDataList[0] );
    atArray<Matrix34> &particleEffectFrameMatrices = const_cast<atArray<Matrix34> &>( pParticleEffectAnimData->GetFrameData() );

    if ( m_bIsConstrained )
    {
        RexMbCutsceneExportModelMatrixData *pInterestOrConstraintAnimData 
            = dynamic_cast<RexMbCutsceneExportModelMatrixData *>( m_exportDataList[1] );
        const atArray<Matrix34> &constraintFrameMatrices = pInterestOrConstraintAnimData->GetFrameData();

        // If the effect is constrained to something, factor in the parent's transformation matrix.
        for ( int i = 0; i < nFrames; ++i )
        {
            particleEffectFrameMatrices[i].DotTranspose( constraintFrameMatrices[i] );
        }
    }
	else
	{
#if HACK_GTA4
		for ( int i = 0; i < nFrames; ++i )
		{
			// Fix up the particles otherwise they are 180 the wrong way in-game (previously fixed up at runtime by tom)
			particleEffectFrameMatrices[i].RotateFullY(PI);

			Quaternion quatYUp;
			Quaternion quatZUp;
			particleEffectFrameMatrices[i].ToQuaternion(quatYUp);
			quatZUp.x = -quatYUp.x;
			quatZUp.y = quatYUp.z;
			quatZUp.z = quatYUp.y;
			quatZUp.w = quatYUp.w;
			particleEffectFrameMatrices[i].FromQuaternion(quatZUp);

			float y = particleEffectFrameMatrices[i].d.y;
			particleEffectFrameMatrices[i].d.x = -particleEffectFrameMatrices[i].d.x;
			particleEffectFrameMatrices[i].d.y = particleEffectFrameMatrices[i].d.z;
			particleEffectFrameMatrices[i].d.z = y;
		}
#endif
	}

    return true;
}

rexSerializerRAGEAnimation* RexMbCutsceneExportParticleEffectModel::CreateAnimationSerializer()
{
    rexSerializerRAGEAnimation *pRexSer = rage_new rexSerializerRAGEAnimation;
    
    pRexSer->AddChannelToWrite( atString("translateX"), atString("translateX"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("translateY"), atString("translateY"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("translateZ"), atString("translateZ"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
    pRexSer->AddChannelToWrite( atString("rotateX"), atString("rotateX"), rexObjectGenericAnimation::TRACK_ROTATE, false );
    pRexSer->AddChannelToWrite( atString("rotateY"), atString("rotateY"), rexObjectGenericAnimation::TRACK_ROTATE, false );
    pRexSer->AddChannelToWrite( atString("rotateZ"), atString("rotateZ"), rexObjectGenericAnimation::TRACK_ROTATE, false );

	pRexSer->AddChannelToWrite( atString("particleData"), atString("particleData"), rexObjectGenericAnimation::TRACK_PARTICLE, false ); 

    return pRexSer;
}

bool RexMbCutsceneExportParticleEffectModel::ProcessClip( ClipXML* clip )
{
    // Add the initializer properties
    HFBProperty pProperty = m_pModel->PropertyList.Find( "fxinit_CanBeCulled" );
    if ( pProperty )
    {
        FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;

        bool b;
        pPropertyBool->GetData( &b, sizeof(b) );

		PropertyAttributeIntXML* culledAttribute = rage_new PropertyAttributeIntXML();
		culledAttribute->SetName("CanBeCulled");
		int& value = culledAttribute->GetInt();
		value = b;

		PropertyXML* culledProperty = rage_new PropertyXML();
		culledProperty->SetName("CanBeCulled");
		culledProperty->AddAttribute( culledAttribute );

		clip->AddProperty(culledProperty);
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_ColorAlphaTint" );
    if ( pProperty )
    {
        FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;

        double d;
        pPropertyDouble->GetData( &d, sizeof(d) );

		PropertyAttributeFloatXML* colorAlphaTintAttribute = rage_new PropertyAttributeFloatXML();
		colorAlphaTintAttribute->SetName( "ColorAlphaTint" );
		float& value = colorAlphaTintAttribute->GetFloat();

		value = (float) d;

		if ( value < 0.0 )
		{
			value = 0.0f;
		}
		else if ( value > 1.0 )
		{
			value = 1.0f;
		}

		PropertyXML* colorAlphaTintProperty = rage_new PropertyXML();
		colorAlphaTintProperty->SetName( "ColorAlphaTint" );
		colorAlphaTintProperty->AddAttribute(colorAlphaTintAttribute);

		clip->AddProperty( colorAlphaTintProperty );
    }
/*
    pProperty = m_pModel->PropertyList.Find( "fxinit_ColorTint" );
    if ( pProperty )
    {
        FBPropertyColorAndAlpha *pPropertyColor = (FBPropertyColorAndAlpha *)pProperty;

        FBColorAndAlpha color;
        pPropertyColor->GetData( &color, sizeof(color) );

        crPropertyVector4 p( Vector4( (float)(color[0]), (float)(color[1]), (float)(color[2]), (float)(color[3]) ) );
        pClipAnim->GetProperties()->AddProperty( "ColorTint", p );
    }
*/
    HFBProperty pInvertXAxisProperty = m_pModel->PropertyList.Find( "fxinit_InvertXAxis" );
    HFBProperty pInvertYAxisProperty = m_pModel->PropertyList.Find( "fxinit_InvertYAxis" );
    HFBProperty pInvertZAxisProperty = m_pModel->PropertyList.Find( "fxinit_InvertZAxis" );

    float fInvertXAxis = 0.0f;
    if ( pInvertXAxisProperty )
    {
        FBPropertyBool *pPropertyBool = (FBPropertyBool *)pInvertXAxisProperty;

        bool b;
        pPropertyBool->GetData( &b, sizeof(b) );

        fInvertXAxis = b ? 1.0f : 0.0f;
    }

    float fInvertYAxis = 0.0f;
    if ( pInvertYAxisProperty )
    {
        FBPropertyBool *pPropertyBool = (FBPropertyBool *)pInvertYAxisProperty;

        bool b;
        pPropertyBool->GetData( &b, sizeof(b) );

        fInvertYAxis = b ? 1.0f : 0.0f;
    }

    float fInvertZAxis = 0.0f;
    if ( pInvertZAxisProperty )
    {
        FBPropertyBool *pPropertyBool = (FBPropertyBool *)pInvertZAxisProperty;

        bool b;
        pPropertyBool->GetData( &b, sizeof(b) );

        fInvertZAxis = b ? 1.0f : 0.0f;
    }

	PropertyAttributeVector3XML* invertAxisAttribute = rage_new PropertyAttributeVector3XML();
	invertAxisAttribute->SetName("InvertAxis");
	Vec3V_Ref value = invertAxisAttribute->GetVector3();

	value.SetX( fInvertXAxis );
	value.SetY( fInvertYAxis );
	value.SetZ( fInvertZAxis );

	PropertyXML* invertAxisProperty = rage_new PropertyXML();
	invertAxisProperty->SetName("InvertAxis");
	invertAxisProperty->AddAttribute( invertAxisAttribute );
	clip->AddProperty( invertAxisProperty );

    pProperty = m_pModel->PropertyList.Find( "fxinit_LODScale" );
    if ( pProperty )
    {
        FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;

        double d;
        pPropertyDouble->GetData( &d, sizeof(d) );

		PropertyAttributeFloatXML* lodScaleAttribute = rage_new PropertyAttributeFloatXML();
		lodScaleAttribute->SetName( "LODScale" );
		float& value = lodScaleAttribute->GetFloat();

		value = (float) d;

		PropertyXML* lodScaleProperty = rage_new PropertyXML();
		lodScaleProperty->SetName( "LODScale" );
		lodScaleProperty->AddAttribute(lodScaleAttribute);
		clip->AddProperty( lodScaleProperty );
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_OffsetRotation" );
    if ( pProperty )
    {
        FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;

        FBVector3d v;
        pPropertyVector3d->GetData( &v, sizeof(v) );

		PropertyAttributeVector3XML* offsetRotationAttribute = rage_new PropertyAttributeVector3XML();
		offsetRotationAttribute->SetName("OffsetRotation");
		Vec3V& value = offsetRotationAttribute->GetVector3();
		value.SetX( (float) v[0] );
		value.SetY( (float) v[1] );
		value.SetZ( (float) v[2] );

		PropertyXML* offsetRotationProperty = rage_new PropertyXML();
		offsetRotationProperty->SetName("OffsetRotation");
		offsetRotationProperty->AddAttribute( offsetRotationAttribute );
		clip->AddProperty( offsetRotationProperty );
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_OffsetTranslation" );
    if ( pProperty )
    {
        FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;

        FBVector3d v;
        pPropertyVector3d->GetData( &v, sizeof(v) );

		PropertyAttributeVector3XML* offsetTranslationAttribute = rage_new PropertyAttributeVector3XML();
		offsetTranslationAttribute->SetName("OffsetTranslation");
		Vec3V& value = offsetTranslationAttribute->GetVector3();
		value.SetX( (float) v[0] );
		value.SetY( (float) v[1] );
		value.SetZ( (float) v[2] );

		PropertyXML* offsetTranslationProperty = rage_new PropertyXML();
		offsetTranslationProperty->SetName("OffsetTranslation");
		offsetTranslationProperty->AddAttribute(offsetTranslationAttribute);
		clip->AddProperty( offsetTranslationProperty );
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_OverrideSpawnDistance" );
    if ( pProperty )
    {
        FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;

        double d;
        pPropertyDouble->GetData( &d, sizeof(d) );

		PropertyAttributeFloatXML* overrideSpawnDistanceAttribute = rage_new PropertyAttributeFloatXML();
		overrideSpawnDistanceAttribute->SetName( "OverrideSpawnDistance" );
		float& value = overrideSpawnDistanceAttribute->GetFloat();
		value = (float) d;
		if ( value  < -1.0f )
		{
			value = -1.0f ;
		}

		PropertyXML* overrideSpawnDistanceProperty = rage_new PropertyXML();
		overrideSpawnDistanceProperty->SetName( "OverrideSpawnDistance" );
		overrideSpawnDistanceProperty->AddAttribute( overrideSpawnDistanceAttribute );
		clip->AddProperty( overrideSpawnDistanceProperty );
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_UserZoomScale" );
    if ( pProperty == NULL )
    {
        // backwards compatibility
        pProperty = m_pModel->PropertyList.Find( "fxinit_UseZoomScale" );    // stupid typo
    }

    if ( pProperty )
    {
        FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;

        double d;
        pPropertyDouble->GetData( &d, sizeof(d) );

		PropertyAttributeFloatXML* userZoomScaleAttribute = rage_new PropertyAttributeFloatXML();
		userZoomScaleAttribute->SetName( "UserZoomScale" );
		float& value = userZoomScaleAttribute->GetFloat();
		value = (float) d;
		if ( value  < 0.0f )
		{
			value = 0.0f;
		}

		PropertyXML* userZoomScaleProperty = rage_new PropertyXML();
		userZoomScaleProperty->SetName( "UserZoomScale" );
		userZoomScaleProperty->AddAttribute( userZoomScaleAttribute );
		clip->AddProperty( userZoomScaleProperty);
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_Velocity" );
    if ( pProperty )
    {
        FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;

        FBVector3d v;
        pPropertyVector3d->GetData( &v, sizeof(v) );

		PropertyAttributeVector3XML* velocityAttribute = rage_new PropertyAttributeVector3XML();
		velocityAttribute->SetName( "Velocity" );
		Vec3V_Ref value = velocityAttribute->GetVector3();
		value.SetX( (float) v[0] );
		value.SetY( (float) v[1] );
		value.SetZ( (float) v[2] );

		PropertyXML* velocityProperty = rage_new PropertyXML();
		velocityProperty->SetName( "Velocity" );
		velocityProperty->AddAttribute( velocityAttribute );
		clip->AddProperty( velocityProperty );
    }

    return true;
}

bool RexMbCutsceneExportParticleEffectModel::ProcessClip( crClipAnimation *pClipAnim )
{
    // Add the initializer properties
    HFBProperty pProperty = m_pModel->PropertyList.Find( "fxinit_CanBeCulled" );
    if ( pProperty )
    {
        FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;

        bool b;
        pPropertyBool->GetData( &b, sizeof(b) );

		crPropertyAttributeInt culledAttribute;
		culledAttribute.SetName("CanBeCulled");
		int& value = culledAttribute.GetInt();
		value = b;

		crProperty culledProperty;
		culledProperty.SetName("CanBeCulled");
		culledProperty.AddAttribute( culledAttribute );

		pClipAnim->GetProperties()->AddProperty( culledProperty );
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_ColorAlphaTint" );
    if ( pProperty )
    {
        FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;

        double d;
        pPropertyDouble->GetData( &d, sizeof(d) );

		crPropertyAttributeFloat colorAlphaTintAttribute;
		colorAlphaTintAttribute.SetName( "ColorAlphaTint" );
		float& value = colorAlphaTintAttribute.GetFloat();

		value = (float) d;

		if ( value < 0.0 )
		{
			value = 0.0f;
		}
		else if ( value > 1.0 )
		{
			value = 1.0f;
		}

		crProperty colorAlphaTintProperty;
		colorAlphaTintProperty.SetName( "ColorAlphaTint" );
		colorAlphaTintProperty.AddAttribute(colorAlphaTintAttribute);

		pClipAnim->GetProperties()->AddProperty( colorAlphaTintProperty );
    }
/*
    pProperty = m_pModel->PropertyList.Find( "fxinit_ColorTint" );
    if ( pProperty )
    {
        FBPropertyColorAndAlpha *pPropertyColor = (FBPropertyColorAndAlpha *)pProperty;

        FBColorAndAlpha color;
        pPropertyColor->GetData( &color, sizeof(color) );

        crPropertyVector4 p( Vector4( (float)(color[0]), (float)(color[1]), (float)(color[2]), (float)(color[3]) ) );
        pClipAnim->GetProperties()->AddProperty( "ColorTint", p );
    }
*/
    HFBProperty pInvertXAxisProperty = m_pModel->PropertyList.Find( "fxinit_InvertXAxis" );
    HFBProperty pInvertYAxisProperty = m_pModel->PropertyList.Find( "fxinit_InvertYAxis" );
    HFBProperty pInvertZAxisProperty = m_pModel->PropertyList.Find( "fxinit_InvertZAxis" );

    float fInvertXAxis = 0.0f;
    if ( pInvertXAxisProperty )
    {
        FBPropertyBool *pPropertyBool = (FBPropertyBool *)pInvertXAxisProperty;

        bool b;
        pPropertyBool->GetData( &b, sizeof(b) );

        fInvertXAxis = b ? 1.0f : 0.0f;
    }

    float fInvertYAxis = 0.0f;
    if ( pInvertYAxisProperty )
    {
        FBPropertyBool *pPropertyBool = (FBPropertyBool *)pInvertYAxisProperty;

        bool b;
        pPropertyBool->GetData( &b, sizeof(b) );

        fInvertYAxis = b ? 1.0f : 0.0f;
    }

    float fInvertZAxis = 0.0f;
    if ( pInvertZAxisProperty )
    {
        FBPropertyBool *pPropertyBool = (FBPropertyBool *)pInvertZAxisProperty;

        bool b;
        pPropertyBool->GetData( &b, sizeof(b) );

        fInvertZAxis = b ? 1.0f : 0.0f;
    }

	crPropertyAttributeVector3 invertAxisAttribute;
	invertAxisAttribute.SetName("InvertAxis");
	Vec3V_Ref value = invertAxisAttribute.GetVector3();

	value.SetX( fInvertXAxis );
	value.SetY( fInvertYAxis );
	value.SetZ( fInvertZAxis );

	crProperty invertAxisProperty;
	invertAxisProperty.SetName("InvertAxis");
	invertAxisProperty.AddAttribute( invertAxisAttribute );
	pClipAnim->GetProperties()->AddProperty( invertAxisProperty );

    pProperty = m_pModel->PropertyList.Find( "fxinit_LODScale" );
    if ( pProperty )
    {
        FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;

        double d;
        pPropertyDouble->GetData( &d, sizeof(d) );

		crPropertyAttributeFloat lodScaleAttribute;
		lodScaleAttribute.SetName( "LODScale" );
		float& value = lodScaleAttribute.GetFloat();

		value = (float) d;

		crProperty lodScaleProperty;
		lodScaleProperty.SetName( "LODScale" );
		lodScaleProperty.AddAttribute(lodScaleAttribute);
		pClipAnim->GetProperties()->AddProperty( lodScaleProperty );

    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_OffsetRotation" );
    if ( pProperty )
    {
        FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;

        FBVector3d v;
        pPropertyVector3d->GetData( &v, sizeof(v) );

		crPropertyAttributeVector3 offsetRotationAttribute;
		offsetRotationAttribute.SetName("OffsetRotation");
		Vec3V& value = offsetRotationAttribute.GetVector3();
		value.SetX( (float) v[0] );
		value.SetY( (float) v[1] );
		value.SetZ( (float) v[2] );

		crProperty offsetRotationProperty;
		offsetRotationProperty.SetName("OffsetRotation");
		offsetRotationProperty.AddAttribute( offsetRotationAttribute );
		pClipAnim->GetProperties()->AddProperty( offsetRotationProperty );
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_OffsetTranslation" );
    if ( pProperty )
    {
        FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;

        FBVector3d v;
        pPropertyVector3d->GetData( &v, sizeof(v) );

		crPropertyAttributeVector3 offsetTranslationAttribute;
		offsetTranslationAttribute.SetName("OffsetTranslation");
		Vec3V& value = offsetTranslationAttribute.GetVector3();
		value.SetX( (float) v[0] );
		value.SetY( (float) v[1] );
		value.SetZ( (float) v[2] );

		crProperty offsetTranslationProperty;
		offsetTranslationProperty.SetName("OffsetTranslation");
		offsetTranslationProperty.AddAttribute(offsetTranslationAttribute);
		pClipAnim->GetProperties()->AddProperty( offsetTranslationProperty );
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_OverrideSpawnDistance" );
    if ( pProperty )
    {
        FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;

        double d;
        pPropertyDouble->GetData( &d, sizeof(d) );

		crPropertyAttributeFloat overrideSpawnDistanceAttribute;
		overrideSpawnDistanceAttribute.SetName( "OverrideSpawnDistance" );
		float& value = overrideSpawnDistanceAttribute.GetFloat();
		value = (float) d;
		if ( value  < -1.0f )
		{
			value = -1.0f ;
		}

		crProperty overrideSpawnDistanceProperty;
		overrideSpawnDistanceProperty.SetName( "OverrideSpawnDistance" );
		overrideSpawnDistanceProperty.AddAttribute( overrideSpawnDistanceAttribute );
		pClipAnim->GetProperties()->AddProperty( overrideSpawnDistanceProperty );
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_UserZoomScale" );
    if ( pProperty == NULL )
    {
        // backwards compatibility
        pProperty = m_pModel->PropertyList.Find( "fxinit_UseZoomScale" );    // stupid typo
    }

    if ( pProperty )
    {
        FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;

        double d;
        pPropertyDouble->GetData( &d, sizeof(d) );

		crPropertyAttributeFloat userZoomScaleAttribute;
		userZoomScaleAttribute.SetName( "UserZoomScale" );
		float& value = userZoomScaleAttribute.GetFloat();
		value = (float) d;
		if ( value  < 0.0f )
		{
			value = 0.0f;
		}

		crProperty userZoomScaleProperty;
		userZoomScaleProperty.SetName( "UserZoomScale" );
		userZoomScaleProperty.AddAttribute( userZoomScaleAttribute );
		pClipAnim->GetProperties()->AddProperty( userZoomScaleProperty);
    }

    pProperty = m_pModel->PropertyList.Find( "fxinit_Velocity" );
    if ( pProperty )
    {
        FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;

        FBVector3d v;
        pPropertyVector3d->GetData( &v, sizeof(v) );

		crPropertyAttributeVector3 velocityAttribute;
		velocityAttribute.SetName( "Velocity" );
		Vec3V_Ref value = velocityAttribute.GetVector3();
		value.SetX( (float) v[0] );
		value.SetY( (float) v[1] );
		value.SetZ( (float) v[2] );

		crProperty velocityProperty;
		velocityProperty.SetName( "Velocity" );
		velocityProperty.AddAttribute( velocityAttribute );
		pClipAnim->GetProperties()->AddProperty( velocityProperty );
    }

    return true;
}

bool RexMbCutsceneExportParticleEffectModel::GetEvoParamNames( atArray<ConstString> &evoParamNameList )
{
    atString rmptxPath;
    if ( !rexMBAnimExportCommon::GetMotionBuilderRmptfxPathSetting( rmptxPath, false ) )
    {
        ULOGGER_MOBO.SetProgressMessage( "ERROR: Unable to determine the 'rmptfx' directory.  Evolution Parameters will not be generated for Particle Effects." );        
        return false;
    }

	atString dlcOrProjectArtDir = rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString(""));
	rmptxPath.Lowercase();
	rmptxPath.Replace("$(art)", dlcOrProjectArtDir.c_str());

	atString effectName(m_pModel->LongName.AsString());
	int index = effectName.IndexOf("^");
	if(index != -1)
	{
		effectName.Set(effectName, 0, index);
	}

    atArray<char *> tokens;
    rexMBAnimExportCommon::TokenizeString( (char*)effectName.c_str(), ":", tokens );
    char *p_cToken = NULL;
    if ( tokens.GetCount() > 0 )
    {
        p_cToken = tokens[0];
    }
    else
    {
        p_cToken = (char*)effectName.c_str();
    }

    // Chop off the space and everything after
    atString filename( p_cToken );
    int indexOf = filename.LastIndexOf( ' ' );
    if ( indexOf != -1 )
    {
        p_cToken[indexOf] = 0;
    }

    char cEffectRuleFilename[RAGE_MAX_PATH];
    sprintf( cEffectRuleFilename, "%s\\effectrules\\%s", rmptxPath.c_str(), p_cToken );

    evoParamNameList.Reset();
    if ( !rexMBAnimExportCommon::GetEvoParamNames( cEffectRuleFilename, evoParamNameList ) )
    {
        ULOGGER_MOBO.SetProgressMessage("WARNING: Unable to open '%s' to retrieve the Evolution Parameters for the effect.", cEffectRuleFilename );
    }

    HFBAnimationNode animNode = m_pModel->AnimationNodeInGet();

    for ( int i = 0; i < evoParamNameList.GetCount(); ++i )
    {
        // see if it exists
        if ( animNode )
        {            
            HFBAnimationNode evoParamAnimNode = rexMBAnimExportCommon::FindAnimByName( animNode, evoParamNameList[i].c_str() );
            if ( evoParamAnimNode == NULL )
            {
                ULOGGER_MOBO.SetProgressMessage("WARNING: The Particle Effect '%s' does not have an evolution parameter named '%s'.", 
					(const char*)m_pModel->LongName, evoParamNameList[i].c_str() );
            }
        }
    }

    return evoParamNameList.GetCount() > 0;
}


//##############################################################################

RexMbCutsceneExportData::RexMbCutsceneExportData()
{

}

RexMbCutsceneExportData::~RexMbCutsceneExportData()
{

}

//##############################################################################

RexMbCutsceneExportMatrixData::RexMbCutsceneExportMatrixData( int nFrames )
{
    m_frameMatrices.Reserve( nFrames );
}

RexMbCutsceneExportMatrixData::~RexMbCutsceneExportMatrixData()
{
    m_frameMatrices.Reset();
}

//##############################################################################

RexMbCutsceneExportVectorData::RexMbCutsceneExportVectorData( int nFrames )
{
    m_frameVectors.Reserve( nFrames );
}

RexMbCutsceneExportVectorData::~RexMbCutsceneExportVectorData()
{
    m_frameVectors.Reset();
}

//##############################################################################

RexMbCutsceneExportVector4Data::RexMbCutsceneExportVector4Data( int nFrames )
{
	m_frameVectors.Reserve( nFrames );
}

RexMbCutsceneExportVector4Data::~RexMbCutsceneExportVector4Data()
{
	m_frameVectors.Reset();
}

//##############################################################################
RexMbCutsceneExportModelMatrixData::RexMbCutsceneExportModelMatrixData( HFBModel pModel, int nFrames, float fTranslateScaleFactor, bool bForceZUp, bool bUseGlobal )
: RexMbCutsceneExportMatrixData(nFrames)
, m_pModel(pModel)
, m_fTranslateScaleFactor(fTranslateScaleFactor)
, m_ForceZUp(bForceZUp)
, m_UseGlobal(bUseGlobal)
{

}

#pragma warning(disable: 4244)	

void RexMbCutsceneExportModelMatrixData::ReadFrameData()
{
    FBQuaternion quatOut;
    FBRVector vecIn;

    FBVector3d rot = m_pModel->Rotation;

	if(m_UseGlobal)
	{
		// THIS IS DIRTY HACK. NEEDS TO BE FIXED CORRECTLY LATER
		// MPW - THIS IS BECAUSE OF THE WAY PARTICLES ARE ATTACHED, SINCE THE DUMMY CAN POTENTIALLY HAVE -90 ROTATION
		// IT FUCKS THE PARTICLE RELATIVE POSITION SINCE THE PARTICLE HAS NO DUMMY.

		char cDummyNode[RAGE_MAX_PATH];
		sprintf(cDummyNode, "%s:Dummy01", rexMBAnimExportCommon::GetNameSpaceFromModel(m_pModel).c_str());

		HFBModel pDummyModel = RAGEFindModelByName(cDummyNode);
		if(pDummyModel)
		{
			FBVector3d oldRotation;
			pDummyModel->GetVector(oldRotation, kModelRotation, true);
			pDummyModel->SetVector(FBVector3d(0,0,0), kModelRotation, true);

			m_pModel->GetVector(rot, kModelRotation, true);

			pDummyModel->SetVector(oldRotation, kModelRotation, true);
		}
		else
		{
			m_pModel->GetVector(rot, kModelRotation, true);
		}

		// THIS IS DIRTY HACK. NEEDS TO BE FIXED CORRECTLY LATER
	}

    vecIn = FBRVector( rot[0], rot[1], rot[2] );

    Quaternion quat;
    Vector3 vec;

    FBRotationToQuaternion( quatOut, vecIn );
    quat.x = quatOut[0];
    quat.y = quatOut[1];
    quat.z = quatOut[2];
    quat.w = quatOut[3];		

    FBVector3d trans = m_pModel->Translation;

	if(m_UseGlobal)
	{
		m_pModel->GetVector(trans, kModelTranslation, true);
	}

    vec.x = trans[0] * m_fTranslateScaleFactor;
    vec.y = trans[1] * m_fTranslateScaleFactor;
    vec.z = trans[2] * m_fTranslateScaleFactor;

    Matrix34 &m = m_frameMatrices.Append();
    m.Identity();
    m.FromQuaternion( quat );
    m.d = vec;
}

void RexMbCutsceneExportModelMatrixData::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
    HFBAnimationNode pAnimNode = m_pModel->AnimationNodeInGet();

    // Translate
    HFBAnimationNode pTransNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Translation" );
    FBVector3d trans = m_pModel->Translation;

    HFBAnimationNode pTransXNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "X" );
    int iKeyCountX = 0;
    if ( pTransXNode->FCurve )
    {
        iKeyCountX = pTransXNode->FCurve->Keys.GetCount();
    }

    KeyList klTranX;
    if ( iKeyCountX == 0 )
    {
        klTranX.AddKey( 0, trans[0] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountX; ++i )
        {		
            FBTime time = pTransXNode->FCurve->Keys[i].Time;
            klTranX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransXNode->FCurve->Keys[i].Value );
        }
    }

    HFBAnimationNode pTransYNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Y" );
    int iKeyCountY = 0;
    if( pTransYNode->FCurve )
    {
        iKeyCountY = pTransYNode->FCurve->Keys.GetCount();
    }

    KeyList klTranY;
    if ( iKeyCountY == 0 )
    {
        klTranY.AddKey( 0, trans[1] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountY; ++i )
        {
            FBTime time = pTransYNode->FCurve->Keys[i].Time;
            klTranY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransYNode->FCurve->Keys[i].Value );
        }
    }

    HFBAnimationNode pTransZNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Z" );
    int iKeyCountZ = 0;
    if ( pTransYNode->FCurve )
    {
        iKeyCountZ = pTransZNode->FCurve->Keys.GetCount();
    }

    KeyList klTranZ;
    if ( iKeyCountZ == 0 )
    {
        klTranZ.AddKey( 0, trans[2] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountZ; ++i )
        {
            FBTime time = pTransZNode->FCurve->Keys[i].Time;
            klTranZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pTransZNode->FCurve->Keys[i].Value );
        }
    }

    // Rotate
    HFBAnimationNode pRotNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Rotation" );
    FBVector3d rot = m_pModel->Rotation;


    HFBAnimationNode pRotXNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "X" );
    int iKeyCountRX = 0;
    if ( pRotXNode->FCurve )
    {
        iKeyCountRX = pRotXNode->FCurve->Keys.GetCount();
    }

    KeyList klRotX;
    if ( iKeyCountRX == 0 )
    {
        klRotX.AddKey( 0, rot[0] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountRX; ++i )
        {
            FBTime time = pRotXNode->FCurve->Keys[i].Time;
            klRotX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotXNode->FCurve->Keys[i].Value );
        }
    }

    HFBAnimationNode pRotYNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Y" );
    int iKeyCountRY = 0;
    if ( pRotYNode->FCurve )
    {
        iKeyCountRY = pRotYNode->FCurve->Keys.GetCount();
    }

    KeyList klRotY;
    if ( iKeyCountRY == 0 )
    {
        klRotY.AddKey( 0, rot[1] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountRY; ++i )
        {
            FBTime time = pRotYNode->FCurve->Keys[i].Time;
            klRotY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotYNode->FCurve->Keys[i].Value );
        }
    }

    HFBAnimationNode pRotZNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Z" );
    int iKeyCountRZ = 0;
    if ( pRotZNode->FCurve )
    {
        iKeyCountRZ = pRotZNode->FCurve->Keys.GetCount();
    }

    KeyList klRotZ;  
    if ( iKeyCountRZ == 0 )
    {
        klRotZ.AddKey( 0, rot[2] );
    }
    else
    {
        for ( int i = 0; i < iKeyCountRZ; ++i )
        {
            FBTime time = pRotZNode->FCurve->Keys[i].Time;
            klRotZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pRotZNode->FCurve->Keys[i].Value );
        }
    }

    int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);
    for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); ++i )
    {
		if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

        FBQuaternion quatOut;
        FBRVector vecIn;

        vecIn = FBRVector( klRotX.GetValue( iStartFrame + i ), klRotY.GetValue( iStartFrame + i ), klRotZ.GetValue( iStartFrame + i ) );

        Matrix34 mat;
        Quaternion quat;
        Vector3 vec;

        FBRotationToQuaternion( quatOut, vecIn );
        quat.x = quatOut[0];
        quat.y = quatOut[1];
        quat.z = quatOut[2];
        quat.w = quatOut[3];		

        vec.x = klTranX.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;
        vec.y = klTranY.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;
        vec.z = klTranZ.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;

        Matrix34 &m = m_frameMatrices.Append();
        m.Identity();
        m.FromQuaternion( quat );
        m.d = vec;
    }
}

void RexMbCutsceneExportModelMatrixData::WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo )
{
    switch( pTrackInfo->m_TrackID )
    {
    case rexObjectGenericAnimation::TRACK_TRANSLATE:
        {
            Vector3 v = m_frameMatrices[iFrame].GetVector(3);
            pTrackInfo->AppendVector3Key( v );
        }
        break;
    case rexObjectGenericAnimation::TRACK_SCALE:
        {
            // No scale support
            pTrackInfo->AppendVector3Key( Vector3( 0.0f, 0.0f, 0.0f ) );
        }
        break;
    case rexObjectGenericAnimation::TRACK_ROTATE:
        {
            Quaternion q;
            m_frameMatrices[iFrame].ToQuaternion( q );
            q.Normalize();
            pTrackInfo->AppendQuaternionKey( q );
        }
		break;
	case rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION:
		{
			Vector3 v(VEC3_ZERO);
			m_frameMatrices[iFrame].Transform3x3(Vector3(0,0,-1), v);

			pTrackInfo->AppendVector3Key( v );
		}
        break;
    default:
        break;
    }
}

//##############################################################################

RexMbCutsceneExportBoxVectorData::RexMbCutsceneExportBoxVectorData( HFBBox pBox, int nFrames )
: RexMbCutsceneExportVectorData(nFrames)
, m_pBox(pBox)
{

}

//##############################################################################

RexMbCutsceneExportDepthOfFieldData::RexMbCutsceneExportDepthOfFieldData( HFBModel pModel, int nFrames, float fTranslateScaleFactor )
: RexMbCutsceneExportVectorData(nFrames)
, m_pModel(pModel)
, m_fTranslateScaleFactor(fTranslateScaleFactor)
{
    char cName[512];
    safecpy( cName, pModel->Name.AsString() );

    char cNearPlaneName[1024];
    sprintf( cNearPlaneName, "%s NearPlane", cName );

    char cFarPlaneName[1024];
    sprintf( cFarPlaneName, "%s FarPlane", cName );

    m_pNearPlaneModel = RAGEFindModelByName( cNearPlaneName );
    m_pFarPlaneModel = RAGEFindModelByName( cFarPlaneName );

    // backwards compatibility
    if ( (m_pNearPlaneModel == NULL) && (m_pFarPlaneModel == NULL) )
    {
        m_pNearPlaneModel = RAGEFindModelByName( "NearPlane" );
        m_pFarPlaneModel = RAGEFindModelByName( "FarPlane" );
    }
}

RexMbCutsceneExportDepthOfFieldData::~RexMbCutsceneExportDepthOfFieldData()
{

}

void RexMbCutsceneExportDepthOfFieldData::ReadFrameData()
{
    double fNearPlaneDistance = 50.0f;
    double fFarPlaneDistance = 1500.0f;
    if ( m_pNearPlaneModel && m_pFarPlaneModel )
    {            
        FBVector3d nearTrans = m_pNearPlaneModel->Translation;
        fNearPlaneDistance = nearTrans[0];

        FBVector3d farTrans = m_pFarPlaneModel->Translation;
        fFarPlaneDistance = farTrans[0];
    }

    Vector3 &v = m_frameVectors.Append();
    v.Zero();
    v.x = fNearPlaneDistance * m_fTranslateScaleFactor;
    v.y = fFarPlaneDistance * m_fTranslateScaleFactor;
}

void RexMbCutsceneExportDepthOfFieldData::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
    KeyList klNear;
    KeyList klFar;

    if ( m_pNearPlaneModel && m_pFarPlaneModel )
    {
        // Near Plane Translate X
        HFBAnimationNode pNearAnimNode = m_pNearPlaneModel->AnimationNodeInGet();
        HFBAnimationNode pNearTransNode = rexMBAnimExportCommon::FindAnimByName( pNearAnimNode, "Lcl Translation" );
        HFBAnimationNode pNearTransXNode = rexMBAnimExportCommon::FindAnimByName( pNearTransNode, "X" );

        int iNearKeyCount = 0;
        if ( pNearTransXNode->FCurve )
        {
            iNearKeyCount = pNearTransXNode->FCurve->Keys.GetCount();
        }

        if ( iNearKeyCount == 0 )
        {
            FBVector3d trans = m_pNearPlaneModel->Translation;
            klNear.AddKey( 0, trans[0] );
        }
        else
        {
            for ( int i = 0; i < iNearKeyCount; ++i )
            {		
                FBTime time = pNearTransXNode->FCurve->Keys[i].Time;
                klNear.AddKey( rexMBAnimExportCommon::GetFrame( time ), pNearTransXNode->FCurve->Keys[i].Value );
            }
        }

        // Far Plane Translate X
        HFBAnimationNode pFarAnimNode = m_pFarPlaneModel->AnimationNodeInGet();
        HFBAnimationNode pFarTransNode = rexMBAnimExportCommon::FindAnimByName( pFarAnimNode, "Lcl Translation" );
        HFBAnimationNode pFarTransXNode = rexMBAnimExportCommon::FindAnimByName( pFarTransNode, "X" );

        int iFarKeyCount = 0;
        if ( pFarTransXNode->FCurve )
        {
            iFarKeyCount = pFarTransXNode->FCurve->Keys.GetCount();
        }

        if ( iFarKeyCount == 0 )
        {
            FBVector3d trans = m_pFarPlaneModel->Translation;
            klFar.AddKey( 0, trans[0] );
        }
        else
        {
            for ( int i = 0; i < iFarKeyCount; ++i )
            {
                FBTime time = pFarTransXNode->FCurve->Keys[i].Time;
                klFar.AddKey( rexMBAnimExportCommon::GetFrame( time ), pFarTransXNode->FCurve->Keys[i].Value );
            }
        }
    }
    else
    {
        klNear.AddKey( 0, 50.0f );
        klFar.AddKey( 0, 1500.0f );
    }

    int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);
    for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); ++i )
    {
		if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

        Vector3 &v = m_frameVectors.Append();
        v.Zero();
        v.x = klNear.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;
        v.y = klFar.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;
    }
}

void RexMbCutsceneExportDepthOfFieldData::WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo )
{
    pTrackInfo->AppendVector3Key( m_frameVectors[iFrame] );
}

//##############################################################################

RexMbCutsceneExportDepthOfField4PlanesData::RexMbCutsceneExportDepthOfField4PlanesData( HFBModel pModel, int nFrames, float fTranslateScaleFactor )
: RexMbCutsceneExportVector4Data(nFrames)
, m_pModel(pModel)
, m_fTranslateScaleFactor(fTranslateScaleFactor)
{
	char cName[512];
	safecpy( cName, pModel->Name.AsString() );

	char cNearPlaneName[1024];
	sprintf( cNearPlaneName, "%s NearPlane", cName );

	char cFarPlaneName[1024];
	sprintf( cFarPlaneName, "%s FarPlane", cName );

	char cNearDOFStrengthPlaneName[1024];
	sprintf( cNearDOFStrengthPlaneName, "%s DOF_Strength_NearPlane", cName );

	char cFarDOFStrengthPlaneName[1024];
	sprintf( cFarDOFStrengthPlaneName, "%s DOF_Strength_FarPlane", cName );

	m_pNearPlaneModel = RAGEFindModelByName( cNearPlaneName );
	m_pFarPlaneModel = RAGEFindModelByName( cFarPlaneName );
	m_pNearDOFStrengthPlaneModel = RAGEFindModelByName( cNearDOFStrengthPlaneName );
	m_pFarDOFStrengthPlaneModel = RAGEFindModelByName( cFarDOFStrengthPlaneName );

	// backwards compatibility
	if ( (m_pNearPlaneModel == NULL) && (m_pFarPlaneModel == NULL) )
	{
		m_pNearPlaneModel = RAGEFindModelByName( "NearPlane" );
		m_pFarPlaneModel = RAGEFindModelByName( "FarPlane" );
	}
}

RexMbCutsceneExportDepthOfField4PlanesData::~RexMbCutsceneExportDepthOfField4PlanesData()
{

}

void RexMbCutsceneExportDepthOfField4PlanesData::ReadFrameData()
{
	double fNearPlaneDistance = 50.0f;
	double fFarPlaneDistance = 1500.0f;
	double fNearDOFStrengthPlaneDistance = 50.0f;
	double fFarDOFStrengthPlaneDistance = 1500.0f;

	if ( m_pNearPlaneModel && m_pFarPlaneModel )
	{            
		FBVector3d nearTrans = m_pNearPlaneModel->Translation;
		fNearPlaneDistance = nearTrans[0];

		FBVector3d farTrans = m_pFarPlaneModel->Translation;
		fFarPlaneDistance = farTrans[0];
	}

	if(m_pNearDOFStrengthPlaneModel && m_pFarDOFStrengthPlaneModel)
	{
		FBVector3d nearTrans = m_pNearDOFStrengthPlaneModel->Translation;
		fNearDOFStrengthPlaneDistance = nearTrans[0];

		FBVector3d farTrans = m_pFarDOFStrengthPlaneModel->Translation;
		fFarDOFStrengthPlaneDistance = farTrans[0];
	}

	Vector4 &v = m_frameVectors.Append();
	v.Zero();
	v.x = fNearDOFStrengthPlaneDistance * m_fTranslateScaleFactor;
	v.y = fNearPlaneDistance * m_fTranslateScaleFactor;
	v.z = fFarPlaneDistance * m_fTranslateScaleFactor;
	v.w = fFarDOFStrengthPlaneDistance * m_fTranslateScaleFactor;
}

void RexMbCutsceneExportDepthOfField4PlanesData::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
	KeyList klNear;
	KeyList klFar;
	KeyList klNearDOF;
	KeyList klFarDOF;

	if ( m_pNearPlaneModel && m_pFarPlaneModel && m_pNearDOFStrengthPlaneModel && m_pFarDOFStrengthPlaneModel )
	{
		// Near Plane Translate X
		HFBAnimationNode pNearAnimNode = m_pNearPlaneModel->AnimationNodeInGet();
		HFBAnimationNode pNearTransNode = (pNearAnimNode) ? rexMBAnimExportCommon::FindAnimByName( pNearAnimNode, "Lcl Translation" ) : NULL;
		HFBAnimationNode pNearTransXNode = (pNearTransNode) ? rexMBAnimExportCommon::FindAnimByName( pNearTransNode, "X" ) : NULL;

		int iNearKeyCount = 0;
		if ( pNearTransXNode && pNearTransXNode->FCurve )
		{
			iNearKeyCount = pNearTransXNode->FCurve->Keys.GetCount();
		}
		else if(pNearTransXNode == NULL)
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' is corrupt.", m_pNearPlaneModel->LongName.AsString());
		}

		if ( iNearKeyCount == 0 )
		{
			FBVector3d trans = m_pNearPlaneModel->Translation;
			klNear.AddKey( 0, trans[0] );
		}
		else
		{
			for ( int i = 0; i < iNearKeyCount; ++i )
			{		
				FBTime time = pNearTransXNode->FCurve->Keys[i].Time;
				klNear.AddKey( rexMBAnimExportCommon::GetFrame( time ), pNearTransXNode->FCurve->Keys[i].Value );
			}
		}

		// Far Plane Translate X
		HFBAnimationNode pFarAnimNode = m_pFarPlaneModel->AnimationNodeInGet();
		HFBAnimationNode pFarTransNode = (pFarAnimNode) ? rexMBAnimExportCommon::FindAnimByName( pFarAnimNode, "Lcl Translation" ) : NULL;
		HFBAnimationNode pFarTransXNode = (pFarTransNode) ? rexMBAnimExportCommon::FindAnimByName( pFarTransNode, "X" ) : NULL;

		int iFarKeyCount = 0;
		if ( pFarTransXNode && pFarTransXNode->FCurve )
		{
			iFarKeyCount = pFarTransXNode->FCurve->Keys.GetCount();
		}
		else if(pFarTransXNode == NULL)
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' is corrupt.", m_pFarPlaneModel->LongName.AsString());
		}

		if ( iFarKeyCount == 0 )
		{
			FBVector3d trans = m_pFarPlaneModel->Translation;
			klFar.AddKey( 0, trans[0] );
		}
		else
		{
			for ( int i = 0; i < iFarKeyCount; ++i )
			{
				FBTime time = pFarTransXNode->FCurve->Keys[i].Time;
				klFar.AddKey( rexMBAnimExportCommon::GetFrame( time ), pFarTransXNode->FCurve->Keys[i].Value );
			}
		}

		// Near DOF Plane Translate X
		HFBAnimationNode pNearDOFAnimNode = m_pNearDOFStrengthPlaneModel->AnimationNodeInGet();
		HFBAnimationNode pNearDOFTransNode = (pNearDOFAnimNode) ? rexMBAnimExportCommon::FindAnimByName( pNearDOFAnimNode, "Lcl Translation" ) : NULL;
		HFBAnimationNode pNearDOFTransXNode = (pNearDOFTransNode) ? rexMBAnimExportCommon::FindAnimByName( pNearDOFTransNode, "X" ) : NULL;

		int iNearDOFKeyCount = 0;
		if ( pNearDOFTransXNode && pNearDOFTransXNode->FCurve )
		{
			iNearDOFKeyCount = pNearDOFTransXNode->FCurve->Keys.GetCount();
		}
		else if(pNearDOFTransXNode == NULL)
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' is corrupt.", m_pNearDOFStrengthPlaneModel->LongName.AsString());
		}

		if ( iNearDOFKeyCount == 0 )
		{
			FBVector3d trans = m_pNearDOFStrengthPlaneModel->Translation;
			klNearDOF.AddKey( 0, trans[0] );
		}
		else
		{
			for ( int i = 0; i < iNearDOFKeyCount; ++i )
			{		
				FBTime time = pNearDOFTransXNode->FCurve->Keys[i].Time;
				klNearDOF.AddKey( rexMBAnimExportCommon::GetFrame( time ), pNearDOFTransXNode->FCurve->Keys[i].Value );
			}
		}

		// Far DOF Plane Translate X
		HFBAnimationNode pFarDOFAnimNode = m_pFarDOFStrengthPlaneModel->AnimationNodeInGet();
		HFBAnimationNode pFarDOFTransNode = (pFarDOFAnimNode) ? rexMBAnimExportCommon::FindAnimByName( pFarDOFAnimNode, "Lcl Translation" ) : NULL;
		HFBAnimationNode pFarDOFTransXNode = (pFarDOFTransNode) ? rexMBAnimExportCommon::FindAnimByName( pFarDOFTransNode, "X" ) : NULL;

		int iFarDOFKeyCount = 0;
		if ( pFarDOFTransXNode && pFarDOFTransXNode->FCurve )
		{
			iFarDOFKeyCount = pFarDOFTransXNode->FCurve->Keys.GetCount();
		}
		else if(pFarDOFTransXNode == NULL)
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' is corrupt.", m_pFarDOFStrengthPlaneModel->LongName.AsString());
		}

		if ( iFarDOFKeyCount == 0 )
		{
			FBVector3d trans = m_pFarDOFStrengthPlaneModel->Translation;
			klFarDOF.AddKey( 0, trans[0] );
		}
		else
		{
			for ( int i = 0; i < iFarDOFKeyCount; ++i )
			{
				FBTime time = pFarDOFTransXNode->FCurve->Keys[i].Time;
				klFarDOF.AddKey( rexMBAnimExportCommon::GetFrame( time ), pFarDOFTransXNode->FCurve->Keys[i].Value );
			}
		}
	}
	else
	{
		klNear.AddKey( 0, 50.0f );
		klFar.AddKey( 0, 1500.0f );
		klNearDOF.AddKey( 0, 50.0f );
		klFarDOF.AddKey( 0, 1500.0f );
	}

	int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);
	for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); ++i )
	{
		if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

		Vector4 &v = m_frameVectors.Append();
		v.Zero();
		v.x = klNearDOF.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;
		v.y = klNear.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;
		v.z = klFar.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;
		v.w = klFarDOF.GetValue( iStartFrame + i ) * m_fTranslateScaleFactor;
	}
}

void RexMbCutsceneExportDepthOfField4PlanesData::WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo )
{
	switch( pTrackInfo->m_TrackID )
	{
	case rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT:
		{
			pTrackInfo->AppendFloatKey( m_frameVectors[iFrame].x );
		}
		break;
	case rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN:
		{
			pTrackInfo->AppendFloatKey( m_frameVectors[iFrame].y );
		}
		break;
	case rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN:
		{
			pTrackInfo->AppendFloatKey( m_frameVectors[iFrame].z );
		}
		break;	
	case rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT:
		{
			pTrackInfo->AppendFloatKey( m_frameVectors[iFrame].w );
		}
		break;
	default:
		break;
	}
}

//##############################################################################

RexMbCutsceneExportPropertyData::RexMbCutsceneExportPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames )
: RexMbCutsceneExportBoxVectorData(pBox,nFrames)
, m_propertyName(pPropertyName)
{
    m_pProperty = m_pBox->PropertyList.Find(  m_propertyName.c_str() );
}

//##############################################################################

RexMbCutsceneExportBoolPropertyData::RexMbCutsceneExportBoolPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames )
: RexMbCutsceneExportPropertyData(pPropertyName,pBox,nFrames)
, m_bDefaultPropertyValue(false)
{   
    if ( m_pProperty != NULL )
    {
        bool bValue;
        m_pProperty->GetData( &bValue, sizeof(bValue) );

        m_bDefaultPropertyValue = bValue;
    }
}

void RexMbCutsceneExportBoolPropertyData::ReadFrameData()
{
    bool bPropertyValue = true;
    if ( m_pProperty != NULL )
    {
        m_pProperty->GetData( &bPropertyValue, sizeof(bPropertyValue) );
    }

    Vector3 &v = m_frameVectors.Append();
    v.Zero();
    v.x = bPropertyValue ? 1.0f : 0.0f;
}

void RexMbCutsceneExportBoolPropertyData::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{  
    KeyList klProperty;

    HFBAnimationNode pAnimNode = m_pBox->AnimationNodeInGet();
    HFBAnimationNode pPropertyNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, m_propertyName.c_str() );

    if ( pPropertyNode )
    {
        //If there are no keyframes then set each frame to the current value
        int iKeyCount = 0;
        if ( pPropertyNode->FCurve )
        {
            iKeyCount = pPropertyNode->FCurve->Keys.GetCount();
        }

        if ( iKeyCount == 0 )
        {
            klProperty.AddKey( 0, m_bDefaultPropertyValue );
        }
        else
        {
            for ( int i = 0; i < iKeyCount; ++i )
            {		
                FBTime time = pPropertyNode->FCurve->Keys[i].Time;
                klProperty.AddKey( rexMBAnimExportCommon::GetFrame( time ), pPropertyNode->FCurve->Keys[i].Value );
            }
        }
    }
    else
    {
        klProperty.AddKey( 0, m_bDefaultPropertyValue );
    }

    int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);
    for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); ++i )
    {
		if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

        bool bPropertyValue = (klProperty.GetValue( iStartFrame + i ) != 0.0f);

        Vector3 &v = m_frameVectors.Append();
        v.Zero();
        v.x = bPropertyValue ? 1.0f : 0.0f;
    }
}

void RexMbCutsceneExportBoolPropertyData::WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo )
{
    pTrackInfo->AppendFloatKey( m_frameVectors[iFrame].x );
}

//##############################################################################

RexMbCutsceneExportFloatPropertyData::RexMbCutsceneExportFloatPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames, bool bWholeNumber )
: RexMbCutsceneExportPropertyData(pPropertyName,pBox,nFrames)
, m_fDefaultPropertyValue(0.0f)
, m_bWholeNumber(bWholeNumber)
{
    if ( m_pProperty != NULL )
    {
        double fValue;
        m_pProperty->GetData( &fValue, sizeof(fValue) );

		m_fDefaultPropertyValue = m_bWholeNumber ? (float)((int)fValue) : fValue;
    }
}

void RexMbCutsceneExportFloatPropertyData::ReadFrameData()
{
    double fPropertyValue = 0.0f;
    if ( m_pProperty != NULL )
    {
        m_pProperty->GetData( &fPropertyValue, sizeof(fPropertyValue) );
    }

    Vector3 &v = m_frameVectors.Append();
    v.Zero();
    v.x = m_bWholeNumber ? (float)((int)fPropertyValue) : fPropertyValue;
}

void RexMbCutsceneExportFloatPropertyData::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
    KeyList klProperty;

    HFBAnimationNode pAnimNode = m_pBox->AnimationNodeInGet();
    HFBAnimationNode pPropertyNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, m_propertyName.c_str() );

    if ( pPropertyNode )
    {
        //If there are no keyframes then set each frame to the current value
        int iKeyCount = 0;
        if ( pPropertyNode->FCurve )
        {
            iKeyCount = pPropertyNode->FCurve->Keys.GetCount();
        }

        if ( iKeyCount == 0 )
        {
            klProperty.AddKey( 0, m_fDefaultPropertyValue );
        }
        else
        {
            for ( int i = 0; i < iKeyCount; ++i )
            {		
                FBTime time = pPropertyNode->FCurve->Keys[i].Time;
                klProperty.AddKey( rexMBAnimExportCommon::GetFrame( time ), pPropertyNode->FCurve->Keys[i].Value );
            }
        }
    }
    else
    {
        klProperty.AddKey( 0, m_fDefaultPropertyValue );
    }

    int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);
    for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); ++i )
    {
		if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

        Vector3 &v = m_frameVectors.Append();
        v.Zero();
		v.x = m_bWholeNumber ? (float)((int)klProperty.GetValue( iStartFrame + i )) : klProperty.GetValue( iStartFrame + i );
    }
}

void RexMbCutsceneExportFloatPropertyData::WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo )
{
    pTrackInfo->AppendFloatKey( m_frameVectors[iFrame].x );
}

//##############################################################################

RexMbCutsceneExportIntegerPropertyData::RexMbCutsceneExportIntegerPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames )
	: RexMbCutsceneExportPropertyData(pPropertyName,pBox,nFrames)
	, m_iDefaultPropertyValue(0.0f)
{
	if ( m_pProperty != NULL )
	{
		int iValue;
		m_pProperty->GetData( &iValue, sizeof(iValue) );

		m_iDefaultPropertyValue = iValue;
	}
}

void RexMbCutsceneExportIntegerPropertyData::ReadFrameData()
{
	int iPropertyValue = 0.0f;
	if ( m_pProperty != NULL )
	{
		m_pProperty->GetData( &iPropertyValue, sizeof(iPropertyValue) );
	}

	Vector3 &v = m_frameVectors.Append();
	v.Zero();
	v.x = (float)iPropertyValue;
}

void RexMbCutsceneExportIntegerPropertyData::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
	KeyList klProperty;

	HFBAnimationNode pAnimNode = m_pBox->AnimationNodeInGet();
	HFBAnimationNode pPropertyNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, m_propertyName.c_str() );

	if ( pPropertyNode )
	{
		//If there are no keyframes then set each frame to the current value
		int iKeyCount = 0;
		if ( pPropertyNode->FCurve )
		{
			iKeyCount = pPropertyNode->FCurve->Keys.GetCount();
		}

		if ( iKeyCount == 0 )
		{
			klProperty.AddKey( 0, m_iDefaultPropertyValue );
		}
		else
		{
			for ( int i = 0; i < iKeyCount; ++i )
			{		
				FBTime time = pPropertyNode->FCurve->Keys[i].Time;
				klProperty.AddKey( rexMBAnimExportCommon::GetFrame( time ), pPropertyNode->FCurve->Keys[i].Value );
			}
		}
	}
	else
	{
		klProperty.AddKey( 0, m_iDefaultPropertyValue );
	}

	int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);
	for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); ++i )
	{
		if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

		Vector3 &v = m_frameVectors.Append();
		v.Zero();
		v.x = klProperty.GetValue( iStartFrame + i );
	}
}

void RexMbCutsceneExportIntegerPropertyData::WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo )
{
	pTrackInfo->AppendFloatKey( m_frameVectors[iFrame].x );
}

//##############################################################################

RexMbCutsceneExportVectorPropertyData::RexMbCutsceneExportVectorPropertyData( const char* pPropertyName, HFBBox pBox, int nFrames )
: RexMbCutsceneExportPropertyData(pPropertyName,pBox,nFrames)
, m_vDefaultPropertyValue(0.0, 0.0, 0.0)
{
    if ( m_pProperty != NULL )
    {
        FBVector3d vValue;
        m_pProperty->GetData( &vValue, sizeof(vValue) );

        m_vDefaultPropertyValue = vValue;
    }
}

void RexMbCutsceneExportVectorPropertyData::ReadFrameData()
{
    FBVector3d vPropertyValue = FBVector3d( 0.0, 0.0, 0.0f );
    if ( m_pProperty != NULL )
    {
        m_pProperty->GetData( &vPropertyValue, sizeof(vPropertyValue) );
    }

    Vector3 &v = m_frameVectors.Append();
    v.x = vPropertyValue[0];
    v.y = vPropertyValue[1];
    v.z = vPropertyValue[2];
}

void RexMbCutsceneExportVectorPropertyData::ReadKeyData( float fStartTime, float /*fEndTime*/, int nFrames )
{
    KeyList klPropertyX;
    KeyList klPropertyY;
    KeyList klPropertyZ;

    HFBAnimationNode pAnimNode = m_pBox->AnimationNodeInGet();
    HFBAnimationNode pPropertyNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, m_propertyName.c_str() );

    if ( pPropertyNode )
    {
        // X component
        HFBAnimationNode pPropertyXNode = rexMBAnimExportCommon::FindAnimByName( pPropertyNode, "X" );
        int iKeyXCount = 0;
        if ( pPropertyXNode->FCurve )
        {
            iKeyXCount = pPropertyXNode->FCurve->Keys.GetCount();
        }

        if ( iKeyXCount == 0 )
        {
            klPropertyX.AddKey( 0, m_vDefaultPropertyValue[0] );
        }
        else
        {
            for ( int i = 0; i < iKeyXCount; ++i )
            {		
                FBTime time = pPropertyXNode->FCurve->Keys[i].Time;
                klPropertyX.AddKey( rexMBAnimExportCommon::GetFrame( time ), pPropertyXNode->FCurve->Keys[i].Value );
            }
        }

        // Y component
        HFBAnimationNode pPropertyYNode = rexMBAnimExportCommon::FindAnimByName( pPropertyNode, "Y" );
        int iKeyYCount = 0;
        if ( pPropertyYNode->FCurve )
        {
            iKeyYCount = pPropertyYNode->FCurve->Keys.GetCount();
        }

        if ( iKeyYCount == 0 )
        {
            klPropertyY.AddKey( 0, m_vDefaultPropertyValue[1] );
        }
        else
        {
            for ( int i = 0; i < iKeyYCount; ++i )
            {		
                FBTime time = pPropertyYNode->FCurve->Keys[i].Time;
                klPropertyY.AddKey( rexMBAnimExportCommon::GetFrame( time ), pPropertyYNode->FCurve->Keys[i].Value );
            }
        }

        // Z component
        HFBAnimationNode pPropertyZNode = rexMBAnimExportCommon::FindAnimByName( pPropertyNode, "Z" );
        int iKeyZCount = 0;
        if ( pPropertyZNode->FCurve )
        {
            iKeyZCount = pPropertyZNode->FCurve->Keys.GetCount();
        }

        if ( iKeyZCount == 0 )
        {
            klPropertyZ.AddKey( 0, m_vDefaultPropertyValue[2] );
        }
        else
        {
            for ( int i = 0; i < iKeyZCount; ++i )
            {		
                FBTime time = pPropertyZNode->FCurve->Keys[i].Time;
                klPropertyZ.AddKey( rexMBAnimExportCommon::GetFrame( time ), pPropertyZNode->FCurve->Keys[i].Value );
            }
        }
    }
    else
    {
        klPropertyX.AddKey( 0, m_vDefaultPropertyValue[0] );
        klPropertyY.AddKey( 0, m_vDefaultPropertyValue[1] );
        klPropertyZ.AddKey( 0, m_vDefaultPropertyValue[2] );
    }

    int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);
    for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); ++i )
    {
		if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

        Vector3 &v = m_frameVectors.Append();
        v.x = klPropertyX.GetValue( iStartFrame + i );
        v.y = klPropertyY.GetValue( iStartFrame + i );
        v.z = klPropertyZ.GetValue( iStartFrame + i );
    }
}

void RexMbCutsceneExportVectorPropertyData::WriteFrameData( int iFrame, rexObjectGenericAnimation::TrackInfo *pTrackInfo )
{
    pTrackInfo->AppendVector3Key( m_frameVectors[iFrame] );
}

//#############################################################################

}