<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<structdef autoregister="true" type="RexMbCutsceneData">
    <string name="m_config" type="atString"/>
    <string name="m_sceneName" type="atString"/>
    <string name="m_exportRoot" type="atString"/>
    <string name="m_faceDir" type="atString"/>
    <string name="m_backupDir" type="atString"/>
    <struct name="m_cutsceneFile" type="cutfCutsceneFile2"/>
</structdef>

</ParserSchema>