// /utility.cpp

#include "utility.h"

#include <list>
#include <vector>
#include <map>
#include <string>
#include <stdio.h>
#include "atl/array.h"
#include "cranimation/animtrack.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/stream.h"
#include "file/device.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "parsercore/stream.h"
#include "system/exec.h"

#include "configParser/configGameView.h"
#include "configParser/configExportView.h"
using namespace configParser;

#include "Logger.h"
#include "vectormath/classfreefuncsv.h"

#include "cutsceneExportModel.h"
#include "toolobjectinterchange.h"
#include "UserObjects/AnimMarkup.h"
#include "pipeline.h"
using namespace rage;

static ConfigGameView gs_GameView;
static ConfigCoreView gs_CoreView;
static ConfigExportView gs_ExportView;

//-----------------------------------------------------------------------------

static std::map<std::string, std::string> s_additionalModelTypes;
static std::list<BoneEntry> s_bones;
static atArray<int> s_discardedFrameRanges;
static bool s_useStoryMode;
static bool s_useNewOrigin;
static bool s_usePerforceIntegration = true;
static bool s_useWarnIfCheckedOut = true;
static bool s_useClipXml = true;
static atString s_originObject;
static atMap<ConstString, IToolObjectInterchange *> s_pObjectTypeToToolObjectInterchangeMap;
static atString s_activePart;
static int s_exporterMode;
static atArray<BoneOffsetEntry> s_boneoffsets;
static atArray<SFaceDefaults> s_cutsceneFaceDefaults;
static atArray<SFaceDefaults> s_animFaceDefaults;

// Create our interchange objects, used to create events for our objects when saving the metadata
void rexMBAnimExportCommon::CreateObjectInterchange()
{
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfAssetManagerObject::GetStaticTypeName() ), rage_new AssetManagerToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfAnimationManagerObject::GetStaticTypeName() ), rage_new AnimationManagerToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfOverlayObject::GetStaticTypeName() ), rage_new OverlayToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfScreenFadeObject::GetStaticTypeName() ), rage_new ScreenFadeToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfPedModelObject::GetStaticTypeName() ), rage_new PedModelObjectToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfVehicleModelObject::GetStaticTypeName() ), rage_new VehicleModelToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfPropModelObject::GetStaticTypeName() ), rage_new AnimatedObjectToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfBlockingBoundsObject::GetStaticTypeName() ), rage_new BlockingBoundsToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfRemovalBoundsObject::GetStaticTypeName() ), rage_new RemovalBoundsToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfLightObject::GetStaticTypeName() ), rage_new LightToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfCameraObject::GetStaticTypeName() ), rage_new CameraToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfAnimatedParticleEffectObject::GetStaticTypeName() ), rage_new AnimatedParticleEffectObjectToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfAudioObject::GetStaticTypeName() ), rage_new AudioToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfRayfireObject::GetStaticTypeName() ), rage_new RayfireToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfWeaponModelObject::GetStaticTypeName() ), rage_new WeaponModelToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfParticleEffectObject::GetStaticTypeName() ), rage_new AnimatedParticleEffectObjectToolObjectInterchange );
	s_pObjectTypeToToolObjectInterchangeMap.Insert( ConstString( cutfDecalObject::GetStaticTypeName() ), rage_new DecalToolObjectInterchange );
}

// Kill the interchange objects, delete the memory and KILL IT, KILL IT NOW!
void rexMBAnimExportCommon::KillObjectInterchange()
{
	atMap<ConstString, IToolObjectInterchange *>::Iterator entry = s_pObjectTypeToToolObjectInterchangeMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
	    delete entry.GetData();
	}

	s_pObjectTypeToToolObjectInterchangeMap.Kill();
}

bool RecurseFindBone(HFBModel bone, atString bonename)
{
	if(stricmp(bone->Name, bonename) == 0 || stricmp(bone->LongName, bonename) == 0) 
	{
		return true;
	}

	for(int i=0; i < bone->Children.GetCount(); ++i)
	{
		if(RecurseFindBone(bone->Children[i], bonename))
		{
			return true;
		}
	}

	return false;
}

void WriteToPipe(const char* pipeinput, HANDLE hWriteInPipe )
{
	const DWORD BUFSIZE = 256 * 1024;

	HANDLE g_hInputFile = CreateFile(
		pipeinput,
		GENERIC_READ,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_READONLY,
		NULL);

	if(g_hInputFile != INVALID_HANDLE_VALUE)
	{
		wprintf(L"CreateFile() - %s was successfully opened!\n", pipeinput);
	}
		
	DWORD dwRead, dwWritten;
	CHAR chBuf[BUFSIZE];
	BOOL bSuccess = FALSE;

	for(;;)
	{
		bSuccess = ReadFile(g_hInputFile, chBuf, BUFSIZE, &dwRead, NULL);
		if (! bSuccess || dwRead == 0)
		{
			// Cannot use ErrorExit() lol
			wprintf(L"\nReadFile() - Failed to read file! Error %u\n", GetLastError());
			break;
		}
		else
			wprintf(L"\nReadFile() -  Reading from a file...\n");

		bSuccess = WriteFile(hWriteInPipe, chBuf, dwRead, &dwWritten, NULL);
		if (!bSuccess)
		{
			wprintf(L"\nWriteFile() - Failed to write to pipe for child\'s STDIN! Error %u\n", GetLastError());
			break;
		}
		else
			wprintf(L"\nWriteFile() - writing to the pipe for the child\'s STDIN...\n");
	}

	CloseHandle(g_hInputFile);

	// Close the pipe handle so the child process stops reading
	if (!CloseHandle(hWriteInPipe))
	{
	}
	//ErrorExit(L"CloseHandle()");
	else
		wprintf(L"Closing the pipe handle...\n");
}

int ExecuteCommand(const char* commandline, const char* currentdir, atString& strOutput, const char* pipeinput)
{
	DWORD dwWaitResult = 0;

	HANDLE hReadOutPipe_Read = NULL;
	HANDLE hReadErrorPipe_Read = NULL;

	HANDLE hReadOutPipe = NULL;
	HANDLE hReadErrorPipe = NULL;

	HANDLE hReadInPipe = NULL;
	HANDLE hWriteInPipe = NULL;

	PROCESS_INFORMATION piProcInfo;
	STARTUPINFO siStartInfo;
	SECURITY_ATTRIBUTES saAttr;

	const DWORD BUFSIZE = 256 * 1024;
	BYTE buff[BUFSIZE];

	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	if (!CreatePipe(&hReadErrorPipe_Read, &hReadErrorPipe, &saAttr, 0))
		return 1;
	if (!SetHandleInformation(hReadErrorPipe_Read, HANDLE_FLAG_INHERIT, 0))
		return 1;

	if (!CreatePipe(&hReadOutPipe_Read, &hReadOutPipe, &saAttr, 0))
		return 1;
	if (!SetHandleInformation(hReadOutPipe_Read, HANDLE_FLAG_INHERIT, 0))
		return 1;

	if (!CreatePipe(&hReadInPipe, &hWriteInPipe, &saAttr, 0))
		return 1;
	if (!SetHandleInformation(hWriteInPipe, HANDLE_FLAG_INHERIT, 0))
		return 1;

	ZeroMemory(&piProcInfo, sizeof(piProcInfo));
	ZeroMemory(&siStartInfo, sizeof(siStartInfo));

	siStartInfo.cb = sizeof(STARTUPINFO);
	siStartInfo.hStdError = hReadErrorPipe;
	siStartInfo.hStdOutput = hReadOutPipe;
	siStartInfo.hStdInput = hReadInPipe;
	siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

	BOOL bSuccess = CreateProcessA(	NULL,
		(LPSTR)commandline,
		NULL,
		NULL,
		TRUE,
		CREATE_NO_WINDOW,
		NULL,
		currentdir,
		&siStartInfo,
		&piProcInfo);

	if (!bSuccess)
	{
		fprintf(stderr, "Could not start process: %s", commandline);
		return 1;
	}

	HANDLE WaitHandles[] = {
		piProcInfo.hProcess, hReadOutPipe, hReadErrorPipe
	};

	if(pipeinput)
	{
		WriteToPipe(pipeinput, hWriteInPipe);
	}

	const int ciMaxAttempts = 8;
	int iAttempts = 0;
	while (iAttempts < ciMaxAttempts)
	{
		DWORD dwBytesRead, dwBytesAvailable;

		dwWaitResult = WaitForMultipleObjects(3, WaitHandles, false, 8000);

		while( PeekNamedPipe(hReadOutPipe_Read, NULL, 0, NULL, &dwBytesAvailable, NULL) && dwBytesAvailable )
		{
			ReadFile(hReadOutPipe_Read, buff, BUFSIZE-1, &dwBytesRead, 0);
			buff[dwBytesRead] = '\0';

			strOutput += (char*)buff;
			printf("%s", buff);
		}

		while( PeekNamedPipe(hReadErrorPipe_Read, NULL, 0, NULL, &dwBytesAvailable, NULL) && dwBytesAvailable )
		{
			ReadFile(hReadErrorPipe_Read, buff, BUFSIZE-1, &dwBytesRead, 0);
			buff[dwBytesRead] = '\0';

			strOutput += (char*)buff;
			printf("%s", buff);
		}

		if(dwWaitResult == WAIT_OBJECT_0)
			break;

		if (dwWaitResult == WAIT_TIMEOUT)
			iAttempts++;
	}

	if (dwWaitResult == WAIT_TIMEOUT)
	{
		printf("Warning - Process timed out.\n");
		return 1;
	}

	DWORD dwExitCode;

	GetExitCodeProcess(piProcInfo.hProcess, &dwExitCode);
	if (dwExitCode != 0)
	{
		printf("Error - Exit Code %d\n", dwExitCode);
	}

	return dwExitCode;
}

//##############################################################################

void RageFBSpread::Refresh( bool pNow )
{
	FBSpread::Refresh( pNow );
}

// Implemented to replace FBFindModelByName which is deprecated in MB2012. This function acts like FBFindModelByName by using
// FBFindObjectByName, this does two checks as RAGEFindModelByName will find the model with or without namespace.
HFBModel RAGEFindModelByName(const char* pModelName)
{
	HFBModel pModel = NULL;

	FBComponentList pList;
	FBFindObjectsByName(pModelName, pList, true, true);

	if(pList.GetCount() == 0)
	{
		FBFindObjectsByName(pModelName, pList, false, true);
		if(pList.GetCount() != 0)
		{
			pModel = (HFBModel)pList.GetAt(0);
		}
	}
	else
	{
		pModel = (HFBModel)pList.GetAt(0);
	}

	return pModel;
}

//-----------------------------------------------------------------------------

char* rexMBAnimExportCommon::CreateLogFileName(const char* pLogFile, const char* pSceneName)
{
	char cToolsRoot[RAGE_MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsRoot, RAGE_MAX_PATH);

	char* pLogFilename= new char[RAGE_MAX_PATH];
	if(pSceneName)
	{
		sprintf( pLogFilename, "%s\\logs\\%s_%s.ulog", cToolsRoot, pLogFile, pSceneName );
	}
	else
	{
		sprintf( pLogFilename, "%s\\logs\\%s.ulog", cToolsRoot, pLogFile );
	}

	char cFolder[RAGE_MAX_PATH];
	ASSET.RemoveNameFromPath(cFolder, RAGE_MAX_PATH, pLogFilename);

	CreateDirectory(cFolder, NULL);

	return pLogFilename;
}

//-----------------------------------------------------------------------------

atString rexMBAnimExportCommon::GetDefaultLogDir()
{
	char cToolsRoot[RAGE_MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsRoot, RAGE_MAX_PATH);

	char cLogDir[RAGE_MAX_PATH];
	sprintf( cLogDir, "%s\\logs", cToolsRoot);

	return atString(cLogDir);
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::DeselectAllModels()
{
	FBModelList selectedModels;
	FBGetSelectedModels( selectedModels, NULL, true );

	for(int modelIdx = 0; modelIdx < selectedModels.GetCount(); modelIdx++)
	{
		selectedModels.GetAt( modelIdx )->Selected = false;
	}
}

//-----------------------------------------------------------------------------

void CB_FindFirstFileAll(const fiFindData &data, void *userArg)
{
    atArray<atString>& FileList = *((atArray<atString>*)userArg);
    FileList.PushAndGrow( atString(data.m_Name) );
}

void CB_FindSpecFilesCallback( const fiFindData &data, void *userArg )
{
    atArray<atString>& filenames = *((atArray<atString>*)userArg);

    atString lowercaseFilename(data.m_Name);
    lowercaseFilename.Lowercase();

    if ( lowercaseFilename.EndsWith( ".xml" ) )
    {
        filenames.PushAndGrow( atString(data.m_Name) );
    }
}

void CB_FindCompressionCommandFilesCallback( const fiFindData &data, void *userArg )
{
    atArray<atString>& filenames = *((atArray<atString>*)userArg);

    atString lowercaseFilename(data.m_Name);
    lowercaseFilename.Lowercase();

    if ( lowercaseFilename.EndsWith( ".txt" ) )
    {
        filenames.PushAndGrow( atString(data.m_Name) );
    }
}

void CB_FindSkelFilesCallback( const fiFindData &data, void *userArg )
{
    atArray<atString>& filenames = *((atArray<atString>*)userArg);

    atString lowercaseFilename(data.m_Name);
    lowercaseFilename.Lowercase();

    if ( lowercaseFilename.EndsWith( ".skel" ) )
    {
        filenames.PushAndGrow( atString(data.m_Name) );
    }
}

void rexMBAnimExportCommon::LoadObjectData( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
	IToolObjectInterchange **ppInterchange = s_pObjectTypeToToolObjectInterchangeMap.Access( pObject->GetTypeName() );
	if ( ppInterchange != NULL )
	{
		(*ppInterchange)->LoadObjectDataFromTool( pObject, pCutFile );
	}
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::SaveObjectData( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
	IToolObjectInterchange **ppInterchange = s_pObjectTypeToToolObjectInterchangeMap.Access( pObject->GetTypeName() );
	if ( ppInterchange != NULL )
	{
		(*ppInterchange)->SaveObjectDataToTool( pObject, pCutFile );
	}
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::CheckFPS()
{
	FBPlayerControl playerControl;
	if(playerControl.GetTransportFps() != kFBTimeMode30Frames)
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Scene must be set to 30fps");
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------

float rexMBAnimExportCommon::GetSeconds(FBTime timeIn)
{
	kLongLong temp = timeIn.GetMilliSeconds();
	double tempFloat = (double)temp;
	tempFloat /= 1000.0f;

	return ((float)tempFloat);
}

//-----------------------------------------------------------------------------

int rexMBAnimExportCommon::GetFrame( FBTime timeIn )
{
    atString timeString( (char *)timeIn.GetTimeString() );
    if ( timeString.EndsWith( "*" ) )
    {
        timeString.Truncate( timeString.GetLength() - 1 );
    }

    return atoi( timeString.c_str() );
}

//-----------------------------------------------------------------------------

int rexMBAnimExportCommon::GetStartFrame(  )
{
	FBSystem mSystem;
	FBTimeSpan mTimeSpan = mSystem.CurrentTake->LocalTimeSpan;
	return (int) mTimeSpan.GetStart().GetFrame();
}

//-----------------------------------------------------------------------------

int rexMBAnimExportCommon::GetEndFrame( )
{
	FBSystem mSystem;
	FBTimeSpan mTimeSpan = mSystem.CurrentTake->LocalTimeSpan;
	return (int) mTimeSpan.GetStop().GetFrame();
}

//-----------------------------------------------------------------------------

FBTime rexMBAnimExportCommon::GetTime( float seconds )
{
    FBTime timeOut;
    double tempDouble = (double)seconds * 1000.0;
    timeOut.SetMilliSeconds( (kLongLong) tempDouble );

    return timeOut;
}

//-----------------------------------------------------------------------------

float rexMBAnimExportCommon::GetAnimStartTime()
{
	FBSystem mSystem;
	FBTimeSpan mTimeSpan = mSystem.CurrentTake->LocalTimeSpan;
	FBTime OutTime = mTimeSpan.GetStart();
	float fStartTime = GetFrame(OutTime) / rexMBAnimExportCommon::GetFPS();
	return fStartTime;
}

//-----------------------------------------------------------------------------

float rexMBAnimExportCommon::GetAnimEndTime()
{
	FBSystem mSystem;
	FBTimeSpan mTimeSpan = mSystem.CurrentTake->LocalTimeSpan;
	FBTime OutTime = mTimeSpan.GetStop();
	float fStopTime = GetFrame(OutTime) / rexMBAnimExportCommon::GetFPS();
	return fStopTime;
}

//-----------------------------------------------------------------------------

float rexMBAnimExportCommon::GetSceneStartTime()
{
	return (rexMBAnimExportCommon::GetStoryMode() == true) ? rexMBAnimExportCommon::GetStoryTrackStartTime() : rexMBAnimExportCommon::GetAnimStartTime();
}

//-----------------------------------------------------------------------------

float rexMBAnimExportCommon::GetSceneEndTime()
{
	return (rexMBAnimExportCommon::GetStoryMode() == true) ? rexMBAnimExportCommon::GetStoryTrackEndTime() : rexMBAnimExportCommon::GetAnimEndTime();
}

//-----------------------------------------------------------------------------

float rexMBAnimExportCommon::GetFPS()
{
	return 30.0f;
}

//-----------------------------------------------------------------------------

std::string rexMBAnimExportCommon::GetAnimExportMarkupFilePath()
{
    string retMarkupFilePath;

    FBApplication fbApp;

    char cFbxFilename[RAGE_MAX_PATH];
    safecpy( cFbxFilename, fbApp.FBXFileName.AsString(), sizeof(cFbxFilename) );
    if ( (int)strlen(cFbxFilename) > 0 )
    {
        char cTempFilename[RAGE_MAX_PATH];

        char cTempDir[RAGE_MAX_PATH];
        if ( sysGetEnv( "TEMP", cTempDir, sizeof(cTempDir) ) )
        {
            sprintf( cTempFilename, "%s\\%s", cTempDir, ASSET.FileName( cFbxFilename ) );
        }
        else
        {
            sprintf( cTempFilename, "c:\\%s", ASSET.FileName( cFbxFilename ) );
        }

        ASSET.RemoveExtensionFromPath( cTempFilename, sizeof(cTempFilename), cTempFilename );
        safecat( cTempFilename, ".rexmb", sizeof(cTempFilename) );

        retMarkupFilePath = cTempFilename;
    }

    return retMarkupFilePath;
}

bool rexMBAnimExportCommon::GetAssetPath(atString& assetDir)
{
	TCHAR _assetDir[_MAX_PATH]  = {0};
	if( gs_GameView.GetAssetsDir(_assetDir, _MAX_PATH) == false)
		return false;

	assetDir = _assetDir;
	return true;
}

bool rexMBAnimExportCommon::GetArtPath(atString& artDir)
{
	TCHAR _artDir[_MAX_PATH]  = {0};
	if( gs_GameView.GetArtDir(_artDir, _MAX_PATH) == false)
		return false;

	artDir = _artDir;
	return true;
}

//-----------------------------------------------------------------------------

string rexMBAnimExportCommon::GetCutsceneExportMarkupFilePath()
{
    string retMarkupFilePath;

    FBApplication fbApp;

    char cFbxFilename[RAGE_MAX_PATH];
    safecpy( cFbxFilename, fbApp.FBXFileName.AsString(), sizeof(cFbxFilename) );
    if ( (int)strlen(cFbxFilename) > 0 )
    {
        char cTempFilename[RAGE_MAX_PATH];

        char cTempDir[RAGE_MAX_PATH];
        if ( sysGetEnv( "TEMP", cTempDir, sizeof(cTempDir) ) )
        {
            sprintf( cTempFilename, "%s\\%s", cTempDir, ASSET.FileName( cFbxFilename ) );
        }
        else
        {
            sprintf( cTempFilename, "c:\\%s", ASSET.FileName( cFbxFilename ) );
        }

        ASSET.RemoveExtensionFromPath( cTempFilename, sizeof(cTempFilename), cTempFilename );
        safecat( cTempFilename, ".rexcs", sizeof(cTempFilename) );

        retMarkupFilePath = cTempFilename;
    }

    return retMarkupFilePath;
}

//-----------------------------------------------------------------------------

char* rexMBAnimExportCommon::ReplaceWhitespaceCharacters( char *pText )
{
    int len = (int)strlen( pText );
    for( int i = 0; i < len; ++i )
    {
        if ( isspace( pText[i] ) )
        {
            pText[i] = '_';
        }
    }

    return pText;
}

atString rexMBAnimExportCommon::ReplaceWhitespaceCharacters( atString& atText )
{
	int len = atText.GetLength();
	for( int i = 0; i < len; ++i )
	{
		if ( isspace( atText[i] ) )
		{
			atText[i] = '_';
		}
	}

	return atText;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::ReadLine( fiStream* pStream, char* pDest, s32 iDestLen )
{
    bool bStarted = false;

    s32 index = 0;
    while ( pStream->ReadByte( &pDest[index], 1 ) )
    {
        if ( !bStarted && (pDest[index] != '\n') && (pDest[index] != '\r') )
        {
            bStarted = true;
        }

        if ( bStarted )
        {
            if ( (pDest[index] == '\n') || (pDest[index] == '\r') )
            {
                break;
            }

            index++;

            if ( index == iDestLen )
            {
                break;
            }
        }
    }

    if ( index == 0 )
    {
        return false;
    }

    pDest[index] = '\0';

    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::WriteLine( fiStream* pStream, const char* pText )
{
    pStream->Write( pText, (int)strlen(pText) );	
    pStream->WriteByte( "\r\n", 2 );

    return true;
}

//-----------------------------------------------------------------------------

atString rexMBAnimExportCommon::GetNameSpaceFromModel( HFBModel pModel )
{
	atArray<char*> atTokens;
	TokenizeString((char*)pModel->LongName.AsString(), ":", atTokens);

	return atString(atTokens[0]);
}

//-----------------------------------------------------------------------------

atString rexMBAnimExportCommon::GetBoneNameFromModel( HFBModel pModel )
{
	atArray<char*> atTokens;
	TokenizeString((char*)pModel->LongName.AsString(), ":", atTokens);

	if(atTokens.GetCount() > 1)
	{
		return atString(atTokens[1]);
	}

	return atString(atTokens[0]);
}

//-----------------------------------------------------------------------------

std::map<std::string, std::string>& rexMBAnimExportCommon::GetAdditionalModelTrackTypes()
{
	return s_additionalModelTypes;
}

//----------------------------------------------------------------------------

atArray<SFaceDefaults>& rexMBAnimExportCommon::GetCutsceneFaceDefaults()
{
	return s_cutsceneFaceDefaults;
}

//----------------------------------------------------------------------------

atArray<SFaceDefaults>& rexMBAnimExportCommon::GetAnimFaceDefaults()
{
	return s_animFaceDefaults;
}

//-----------------------------------------------------------------------------

atString rexMBAnimExportCommon::GetBoneID( HFBModel pModel, bool bTranslate )
{	
    char name[1024];
	safecpy( name, (const char*)pModel->LongName, sizeof(name) );

    atArray<char *> tokens;
    rexMBAnimExportCommon::TokenizeString( name, ":", tokens );
    char *pToken = tokens[tokens.GetCount() - 1];    

    if ( pToken )
    {
        if ( !bTranslate )        
        {
            return atString( pToken );
        }

        for ( std::list<BoneEntry>::const_iterator iter = s_bones.begin(); iter != s_bones.end(); ++iter )
        {			
            BoneEntry thisbone = (*iter);

            if ( stricmp( pToken, thisbone.c_boneName ) == 0 )
            {
                return atString( thisbone.c_boneID );
            }
        }
    }

    if ( stricmp( pToken, "char" ) == 0 )
    {
        return atString( "0" );
    }

#if HACK_GTA4
	if( pToken )
		return atString(pToken);
	else
		return atString( pModel->LongName );
#else
    return atString( pModel->LongName );
#endif // HACK_GTA4
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::LoadBoneTranslationConfig( const char* pExportType )
{
	TCHAR boneTagsPath[_MAX_PATH] = {0};
	if ( gs_ExportView.GetBoneTagsFile(boneTagsPath, _MAX_PATH) == false )
		return false;

	return LoadBoneTranslationXmlConfigFile( boneTagsPath, pExportType );
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::LoadBoneTranslationXmlConfigFile( const char* pFilename, const char* pExportType )
{
    parTree* pTree = PARSER.LoadTree( pFilename, "" );
    if ( !pTree )
    {
        return false;
    }

    parTreeNode* pRootNode = pTree->GetRoot();

    std::vector< parTreeNode* > boneIdEntries;
    boneIdEntries.reserve(256);

    if( (strcmpi(pExportType, "Ped") == 0) ||
        (strcmpi(pExportType, "All") == 0) )
    {
        parTreeNode* pPedsNode = pRootNode->FindChildWithName("peds");
        if(!pPedsNode)
        {
            delete pTree;
        }
        else
        {
            parTreeNode *pBoneIdsNode = pPedsNode->FindChildWithName("boneids");
            Assert(pBoneIdsNode); //This must be there or the file isn't formatted correctly...

            for(parTreeNode::ChildNodeIterator cIter = pBoneIdsNode->BeginChildren();
                cIter != pBoneIdsNode->EndChildren(); 
                ++cIter) 
            {
                parTreeNode* pChildNode = (*cIter);
                boneIdEntries.push_back(pChildNode);
            }
        }
    }

    if( (strcmpi(pExportType, "Vehicle") == 0) ||
        (strcmpi(pExportType, "All") == 0) )
    {
        parTreeNode* pVehsNode = pRootNode->FindChildWithName("vehicles");
        if(!pVehsNode)
        {
            delete pTree;
        }
        else
        {
            parTreeNode *pBoneIdsNode = pVehsNode->FindChildWithName("boneids");
            Assert(pBoneIdsNode); //This must be there or the file isn't formatted correctly...

            for(parTreeNode::ChildNodeIterator cIter = pBoneIdsNode->BeginChildren();
                cIter != pBoneIdsNode->EndChildren(); 
                ++cIter) 
            {
                parTreeNode* pChildNode = (*cIter);
                boneIdEntries.push_back(pChildNode);
            }
        }
    }

	if( (strcmpi(pExportType, "Weapon") == 0) ||
		(strcmpi(pExportType, "All") == 0) )
	{
		parTreeNode* pWeapsNode = pRootNode->FindChildWithName("weapons");
		if(!pWeapsNode)
		{
			delete pTree;
		}
		else
		{
			parTreeNode *pBoneIdsNode = pWeapsNode->FindChildWithName("boneids");
			Assert(pBoneIdsNode); //This must be there or the file isn't formatted correctly...

			for(parTreeNode::ChildNodeIterator cIter = pBoneIdsNode->BeginChildren();
				cIter != pBoneIdsNode->EndChildren(); 
				++cIter) 
			{
				parTreeNode* pChildNode = (*cIter);
				boneIdEntries.push_back(pChildNode);
			}
		}
	}

    if( (strcmpi(pExportType, "Prop") == 0) ||
        (strcmpi(pExportType, "All") == 0) )
    {
        parTreeNode* pPropsNode = pRootNode->FindChildWithName("props");
        if(!pPropsNode)
        {
            delete pTree;
        }
        else
        {
            parTreeNode *pBoneIdsNode = pPropsNode->FindChildWithName("boneids");
            Assert(pBoneIdsNode); //This must be there or the file isn't formatted correctly...

            for(parTreeNode::ChildNodeIterator cIter = pBoneIdsNode->BeginChildren();
                cIter != pBoneIdsNode->EndChildren(); 
                ++cIter) 
            {
                parTreeNode* pChildNode = (*cIter);
                boneIdEntries.push_back(pChildNode);
            }
        }
    }

    for(std::vector< parTreeNode* >::iterator it = boneIdEntries.begin();
        it != boneIdEntries.end();
        ++it)
    {
        parTreeNode *pNode = (*it);
        BoneEntry thisbone;
        thisbone.c_boneID = new char[1024];
        thisbone.c_boneID[0] = 0;
        thisbone.c_boneName = new char[1024];
        thisbone.c_boneName[0] = 0;

        parAttribute* pIdAttr = pNode->GetElement().FindAttribute("id");
        Assert(pIdAttr);
        strcpy(thisbone.c_boneID, pIdAttr->GetStringValue());

        parAttribute* pNameAttr = pNode->GetElement().FindAttribute("name");
        Assert(pNameAttr);
        strcpy(thisbone.c_boneName, pNameAttr->GetStringValue());

        s_bones.push_back(thisbone);
    }

    delete pTree;
    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::LoadAdditionalModelConfigFile()
{
	char cValue[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cValue, RAGE_MAX_PATH);

	char cConfigFile[RAGE_MAX_PATH];
	sprintf(cConfigFile, "%s\\etc\\config\\anim\\additional_models.xml", cValue);

	parTree* pTree = PARSER.LoadTree( cConfigFile, "" );
	if ( !pTree )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to open %s", cConfigFile);
		return false;
	}

	parTreeNode* pRootNode = pTree->GetRoot();

	for(parTreeNode::ChildNodeIterator cIter = pRootNode->BeginChildren();
		cIter != pRootNode->EndChildren(); 
		++cIter) 
	{
		parTreeNode* pChildNode = (*cIter);

		parAttribute* pNameAttr = pChildNode->GetElement().FindAttribute("name");
		parAttribute* pIdAttr = pChildNode->GetElement().FindAttribute("id");

		s_additionalModelTypes[pNameAttr->GetStringValue()] = pIdAttr->GetStringValue();
	}

	delete pTree;
	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::LoadFaceSettingsConfigFile()
{
	char cValue[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariableA("RS_TOOLSROOT", cValue, RAGE_MAX_PATH);
	atString sValue = atString(cValue);

	atMap<atString,atArray<SFaceDefaults>*> faceDefaultMap;
	faceDefaultMap[atString(sValue,"\\etc\\config\\anim\\cutscene_face_settings.xml")] = &s_cutsceneFaceDefaults;
	faceDefaultMap[atString(sValue,"\\etc\\config\\anim\\anim_face_settings.xml")] = &s_animFaceDefaults;

	for (int i=0; i<faceDefaultMap.GetNumSlots(); ++i)
	{
		if (faceDefaultMap.GetEntry(i))
		{
			parTree* pTree = PARSER.LoadTree( faceDefaultMap.GetEntry(i)->key, "" );
			if ( !pTree )
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to open %s", faceDefaultMap.GetEntry(i)->key);
				return false;
			}

			parTreeNode* pRootNode = pTree->GetRoot();

			for(parTreeNode::ChildNodeIterator cIter = pRootNode->BeginChildren();
				cIter != pRootNode->EndChildren(); 
				++cIter) 
			{
				parTreeNode* pChildNode = (*cIter);

				if(stricmp(pChildNode->GetElement().GetName(), "defaults") == 0)
				{
					parAttribute* pSuffixAttr = pChildNode->GetElement().FindAttribute("suffix");
					parAttribute* pAttributeAttr = pChildNode->GetElement().FindAttribute("attribute");
					parAttribute* pSpecAttr = pChildNode->GetElement().FindAttribute("spec");

					SFaceDefaults faceDefaults;
					faceDefaults.suffix = pSuffixAttr->GetStringValue();
					faceDefaults.attributes = pAttributeAttr->GetStringValue();
					faceDefaults.spec = pSpecAttr->GetStringValue();

					faceDefaultMap.GetEntry(i)->data->PushAndGrow(faceDefaults);
				}
			}

			delete pTree;
		}
	}
	faceDefaultMap.Kill();
	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::LoadBoneValidationConfigFile( const char* pFilename, const char* pSkeleton, atArray<atString>& atBones, atArray<bool>& atEnforce )
{
	parTree* pTree = PARSER.LoadTree( pFilename, "" );
	if ( !pTree )
	{
		return false;
	}

	parTreeNode* pRootNode = pTree->GetRoot();

	for(parTreeNode::ChildNodeIterator cIter = pRootNode->BeginChildren();
		cIter != pRootNode->EndChildren(); 
		++cIter) 
	{
		parTreeNode* pChildNode = (*cIter);

		parAttribute* pNameAttr = pChildNode->GetElement().FindAttribute("name");
		Assert(pNameAttr);
		if(stricmp(pSkeleton, pNameAttr->GetStringValue()) == 0)
		{
			for(parTreeNode::ChildNodeIterator cIter2 = pChildNode->BeginChildren();
				cIter2 != pRootNode->EndChildren(); 
				++cIter2) 
			{
				parTreeNode* pBoneNode = (*cIter2);

				parAttribute* pEnforceAttr = pBoneNode->GetElement().FindAttribute("enforceAsGenericModel");
				atEnforce.PushAndGrow( (pEnforceAttr) ? pEnforceAttr->GetBoolValue() : false );

				atBones.PushAndGrow(atString(pBoneNode->GetData()));
			}
		}
	}

	delete pTree;
	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::LoadCompressionTemplateValidationConfigFile( const char* pFilename, atArray<atString>& atUnusedTemplates)
{
	parTree* pTree = PARSER.LoadTree( pFilename, "" );
	if ( !pTree )
	{
		return false;
	}

	parTreeNode* pRootNode = pTree->GetRoot();

	for(parTreeNode::ChildNodeIterator cIter = pRootNode->BeginChildren();
		cIter != pRootNode->EndChildren(); 
		++cIter) 
	{
		parTreeNode* pChildNode = (*cIter);

		parAttribute* pNameAttr = pChildNode->GetElement().FindAttribute("name");
		Assert(pNameAttr);
		
		const char* templateName = pNameAttr->GetStringValue();

		atUnusedTemplates.PushAndGrow(atString(templateName));
	}

	delete pTree;
	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::LoadBoneOffsetValidationConfigFile()
{
	char cValue[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cValue, RAGE_MAX_PATH);

	char cConfigFile[RAGE_MAX_PATH];
	sprintf(cConfigFile, "%s\\etc\\config\\anim\\offset_validation.xml", cValue);

	parTree* pTree = PARSER.LoadTree( cConfigFile, "" );
	if ( !pTree )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to open %s", cConfigFile);
		return false;
	}

	parTreeNode* pRootNode = pTree->GetRoot();

	for(parTreeNode::ChildNodeIterator cIter = pRootNode->BeginChildren();
		cIter != pRootNode->EndChildren(); 
		++cIter) 
	{
		parTreeNode* pChildNode = (*cIter);

		BoneOffsetEntry boneOffset;

		parAttribute* pNameAttr = pChildNode->GetElement().FindAttribute("name");
		Assert(pNameAttr);

		boneOffset.boneName = pNameAttr->GetStringValue();

		for(parTreeNode::ChildNodeIterator cIter2 = pChildNode->BeginChildren();
			cIter2 != pRootNode->EndChildren(); 
			++cIter2) 
		{
			parTreeNode* pBoneNode = (*cIter2);

			boneOffset.fOffet = atof(pBoneNode->GetData());

			s_boneoffsets.PushAndGrow(boneOffset);
		}
	}

	delete pTree;
	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::ValidateBoneOffset( const char* pBone, Vector3 translation )
{
	for(int i=0; i < s_boneoffsets.GetCount(); ++i)
	{
		if(strcmpi(pBone, s_boneoffsets[i].boneName.c_str()) == 0)
		{
			bool bPositive = s_boneoffsets[i].fOffet > 0;

			if(bPositive)
			{
				if(translation.x > s_boneoffsets[i].fOffet || translation.y > s_boneoffsets[i].fOffet || translation.z > s_boneoffsets[i].fOffet)
				{
					ULOGGER_MOBO.SetProgressMessage("WARNING: %s has an offet > %f. - (%f,%f,%f)", pBone, s_boneoffsets[i].fOffet, translation.x, translation.y, translation.z );
					return false;
				}

				if(translation.x < (s_boneoffsets[i].fOffet*-1) || translation.y < (s_boneoffsets[i].fOffet*-1) || translation.z < (s_boneoffsets[i].fOffet*-1))
				{
					ULOGGER_MOBO.SetProgressMessage("WARNING: %s has an offet < %f. - (%f,%f,%f)", pBone, (s_boneoffsets[i].fOffet*-1), translation.x, translation.y, translation.z );
					return false;
				}
			}
			else
			{
				if(translation.x < s_boneoffsets[i].fOffet || translation.y < s_boneoffsets[i].fOffet || translation.z < s_boneoffsets[i].fOffet)
				{
					ULOGGER_MOBO.SetProgressMessage("WARNING: %s has an offet < %f on take %s. - (%f,%f,%f)", pBone, s_boneoffsets[i].fOffet, translation.x, translation.y, translation.z );
					return false;
				}

				if(translation.x > (s_boneoffsets[i].fOffet*-1) || translation.y > (s_boneoffsets[i].fOffet*-1) || translation.z > (s_boneoffsets[i].fOffet*-1))
				{
					ULOGGER_MOBO.SetProgressMessage("WARNING: %s has an offet > %f. - (%f,%f,%f)", pBone, (s_boneoffsets[i].fOffet*-1), translation.x, translation.y, translation.z );
					return false;
				}
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::ValidateCompressionTemplates(RexAnimMarkupData* pExportData)
{
	char cValue[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cValue, RAGE_MAX_PATH);

	char cConfigFile[RAGE_MAX_PATH];
	sprintf(cConfigFile, "%s\\etc\\config\\anim\\unused_compression_templates.xml", cValue);

	atArray<atString> atUnusedTemplates;
	if(!rexMBAnimExportCommon::LoadCompressionTemplateValidationConfigFile(cConfigFile, atUnusedTemplates))
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Failed to load unused compression template config file: '%s'", cConfigFile);
		return false;
	}

	int nSegments = pExportData->GetNumSegments();

	for(int segIdx = 0; segIdx < nSegments; segIdx++)
	{
		RexMbAnimSegmentData* pSegData = pExportData->GetSegment(segIdx);

		if ( pSegData && pSegData->IsMarkedForExport() )
		{
			if(pSegData->m_bLinearCompression)
			{
				char cMsg[512];
				sprintf_s(cMsg, 512, "Segment '%s' is using Linear Compression, Continue?", pSegData->GetSegmentName());

				int iResult = FBMessageBox("Warning", cMsg, "Yes", "No");
				if(iResult == 2)
				{
					return false;
				}
			}

			const char* compressionFile = pSegData->GetCompressionFile();

			if (compressionFile && strlen(compressionFile) > 0)
			{
				const char* compressionFilename = ASSET.FileName(compressionFile);

				if (compressionFilename)
				{
					for(int i=0; i < atUnusedTemplates.GetCount(); ++i)
					{
						if (compressionFilename && stricmp(compressionFilename, atUnusedTemplates[i].c_str()) == 0)
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: Segment '%s' has an invalid compression template '%s' applied to it.", pSegData->GetSegmentName(), atUnusedTemplates[i].c_str());
							return false;
						}
					}
				}
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::ValidateIKRoot(RexAnimMarkupData* pExportData)
{
	FBSystem m_system;
	HFBTake currentTake = m_system.CurrentTake;

	int takeCount = m_system.Scene->Takes.GetCount();
	for(int takeIndex = 0; takeIndex < takeCount; ++takeIndex)
	{
		HFBTake takeComponent = (HFBTake) m_system.Scene->Takes.GetAt(takeIndex);
		m_system.CurrentTake = takeComponent;

		for(int characterIndex = 0; characterIndex < pExportData->GetNumCharacters(); ++characterIndex)
		{
			RexCharAnimData* pCharData = pExportData->GetCharacter(characterIndex);

			if(pCharData->m_bExport == false)
				continue;

			if(pCharData->m_modelType == "Body")
			{
				HFBModel pModel = RAGEFindModelByName(pCharData->GetInputModelName(0));
				if(pModel)
				{
					char cHelper[RAGE_MAX_PATH];
					sprintf(cHelper, "%s:%s", rexMBAnimExportCommon::GetNameSpaceFromModel(pModel).c_str(), "IK_FacingDirection" );

					HFBModel pHelperModel = RAGEFindModelByName(cHelper);
					// If we have the helper then we are valid regardless
					if(!pHelperModel)
					{
						// If we have no helper then we check the IK_Root, this is only valid if the keys are 0 in value.
						char cIkRoot[RAGE_MAX_PATH];
						sprintf(cIkRoot, "%s:%s", rexMBAnimExportCommon::GetNameSpaceFromModel(pModel).c_str(), "IK_Root");

						HFBModel pIKModel = RAGEFindModelByName(cIkRoot);
						if(pIKModel)
						{
							HFBAnimationNode pAnimNode = pIKModel->AnimationNodeInGet();
							HFBAnimationNode pTransNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Translation" );
							if(pTransNode)
							{
								HFBAnimationNode pTransXNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "X" );
								HFBAnimationNode pTransYNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Y" );
								HFBAnimationNode pTransZNode = rexMBAnimExportCommon::FindAnimByName( pTransNode, "Z" );

								if ( pTransXNode->FCurve )
								{
									for(int i=0; i < pTransXNode->FCurve->Keys.GetCount(); ++i)
									{
										if(pTransXNode->FCurve->Keys[i].Value != 0)
										{
											ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' does not exist and '%s' has keys with value of non-zero (Translation) - Take '%s'.", cHelper, cIkRoot, m_system.CurrentTake->Name.AsString());
											return false;
										}
									}
								}

								if ( pTransYNode->FCurve )
								{
									for(int i=0; i < pTransYNode->FCurve->Keys.GetCount(); ++i)
									{
										if(pTransYNode->FCurve->Keys[i].Value != 0)
										{
											ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' does not exist and '%s' has keys with value of non-zero (Translation) - Take '%s'.", cHelper, cIkRoot, m_system.CurrentTake->Name.AsString());
											return false;
										}
									}
								}

								if ( pTransZNode->FCurve )
								{
									for(int i=0; i < pTransZNode->FCurve->Keys.GetCount(); ++i)
									{
										if(pTransZNode->FCurve->Keys[i].Value != 0)
										{
											ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' does not exist and '%s' has keys with value of non-zero (Translation) - Take '%s'.", cHelper, cIkRoot, m_system.CurrentTake->Name.AsString());
											return false;
										}
									}
								}
							}

							HFBAnimationNode pRotNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Lcl Rotation" );
							if(pRotNode)
							{
								HFBAnimationNode pRotXNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "X" );
								HFBAnimationNode pRotYNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Y" );
								HFBAnimationNode pRotZNode = rexMBAnimExportCommon::FindAnimByName( pRotNode, "Z" );

								if ( pRotXNode->FCurve )
								{
									for(int i=0; i < pRotXNode->FCurve->Keys.GetCount(); ++i)
									{
										if(pRotXNode->FCurve->Keys[i].Value != 0)
										{
											ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' does not exist and '%s' has keys with value of non-zero (Rotation) - Take '%s'.", cHelper, cIkRoot, m_system.CurrentTake->Name.AsString());
											return false;
										}
									}
								}

								if ( pRotYNode->FCurve )
								{
									for(int i=0; i < pRotYNode->FCurve->Keys.GetCount(); ++i)
									{
										if(pRotYNode->FCurve->Keys[i].Value != 0)
										{
											ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' does not exist and '%s' has keys with value of non-zero (Rotation) - Take '%s'.", cHelper, cIkRoot, m_system.CurrentTake->Name.AsString());
											return false;
										}
									}
								}

								if ( pRotZNode->FCurve )
								{
									for(int i=0; i < pRotZNode->FCurve->Keys.GetCount(); ++i)
									{
										if(pRotZNode->FCurve->Keys[i].Value != 0)
										{
											ULOGGER_MOBO.SetProgressMessage("ERROR: Node '%s' does not exist and '%s' has keys with value of non-zero (Rotation) - Take '%s'.", cHelper, cIkRoot, m_system.CurrentTake->Name.AsString());
											return false;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	m_system.CurrentTake = currentTake;

	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::ValidateBones(RexAnimMarkupData* pExportData)
{
	char cValue[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cValue, RAGE_MAX_PATH);

	char cConfigFile[RAGE_MAX_PATH];
	sprintf(cConfigFile, "%s\\etc\\config\\anim\\bone_validation.xml", cValue);

	for(int characterIndex = 0; characterIndex < pExportData->GetNumCharacters(); ++characterIndex)
	{
		RexCharAnimData* pCharData = pExportData->GetCharacter(characterIndex);

		if(pCharData->m_bExport == false)
			continue;

		atArray<atString> atBones;
		atArray<bool> atEnforce;
		if(!rexMBAnimExportCommon::LoadBoneValidationConfigFile(cConfigFile, pCharData->GetSkeletonFile(), atBones, atEnforce))
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Failed to load skeleton bone config file: '%s'", cConfigFile);
			return false;
		}

		if(pCharData->m_modelType == "Body")
		{
			for(int i=0; i < atBones.GetCount(); ++i)
			{
				if(atEnforce[i])
				{
					bool bFound=false;
					for(int j=0; j < pCharData->m_additionalModels.GetCount(); ++j)
					{
						HFBModel pModel = RAGEFindModelByName(pCharData->m_additionalModels[j]->m_modelName.c_str());
						if(pModel)
						{
							atString boneName = GetBoneNameFromModel(pModel);
							if(boneName == atBones[i])
								bFound = true;
						}
					}

					if(!bFound)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Required bone not found as additional model: '%s' on model '%s'", atBones[i].c_str(), pCharData->GetInputModelName(0));
						return false;
					}
				}
			}

			HFBModel root = RAGEFindModelByName(pCharData->GetMoverModelName(0));
			if(!root)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Could not find mover bone for: '%s' when attempting to validate bones", pCharData->GetMoverModelName(0));
				return false;
			}
			else
			{
				for(int i=0; i < atBones.GetCount(); ++i)
				{
					bool result = RecurseFindBone(root, atBones[i]);
					if(!result)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Required bone not found: '%s' on model '%s'", atBones[i].c_str(), root->LongName.AsString());
						return false;
					}
				}
			}
			
		}
	}

	return true;
};

//-----------------------------------------------------------------------------

int rexMBAnimExportCommon::DeleteFiles( const char* pPath, bool bForceReadOnlyDelete )
{
    int count = 0;
    atArray<atString> FileList;

    int iHandle = ASSET.EnumFiles( pPath, CB_FindFirstFileAll, &FileList );	
    if ( iHandle )
    {
        for ( int i = 0; i < FileList.size(); ++i )
        {
            char cDelPath[RAGE_MAX_PATH];
            sprintf( cDelPath, "%s\\%s", pPath, FileList[i].c_str() );

			if(bForceReadOnlyDelete){ fiDeviceLocal::GetInstance().SetAttributes( cDelPath, fiDeviceLocal::GetInstance().GetAttributes(cDelPath) & ~FILE_ATTRIBUTE_READONLY ); };
            if ( fiDeviceLocal::GetInstance().Delete( cDelPath ) )
            {
                ++count;
            }
        }
    }

    return count;
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::ConvertToRageMaxtrixFromMotionBuilder(FBMatrix& mbMatrix, rage::Matrix34& rageMatrix)
{
    rageMatrix.a = Vector3( (float) mbMatrix[0], (float) mbMatrix[1], (float) mbMatrix[2]);
    rageMatrix.b = Vector3( (float) mbMatrix[4], (float) mbMatrix[5], (float) mbMatrix[6]);
    rageMatrix.c = Vector3( (float) mbMatrix[8], (float) mbMatrix[9], (float) mbMatrix[10]);
    rageMatrix.d = Vector3((float)  mbMatrix[12]/100.0f, (float) mbMatrix[13]/100.0f, (float) mbMatrix[14]/100.0f);
}

//-----------------------------------------------------------------------------

HFBModel rexMBAnimExportCommon::GetConstraintObject(HFBModel pModel)
{
	if(pModel->IsConstrained)
	{
		FBSystem system;
		int nConstraints = system.Scene->Constraints.GetCount();
		for(int constraintId = 0; constraintId < nConstraints; constraintId++)
		{
			HFBConstraint hConstraint = system.Scene->Constraints[constraintId];

			HFBFolder fFolder = hConstraint->Folder;
			if(fFolder != NULL)
			{
				char* pFolderName = (char*)FBComponent::FBComponentGetName(fFolder);
				if(pFolderName != NULL)
				{
					if(strcmpi(pFolderName, "ParticleFolder") == 0)
					{
						if(hConstraint->ReferenceGetCount(0) == 1 && hConstraint->ReferenceGetCount(1) == 1)
						{
							if(hConstraint->ReferenceGet(0,0) == pModel)
							{
								return hConstraint->ReferenceGet(1, 0);
							}
						}
					}
				}
			}
		}
	}

    return NULL;	
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::TokenizeString( char* pString, char* pDelimiter, atArray<char*>& tokens )
{	
    char* pToken = strtok( pString, pDelimiter );	
    while ( pToken )
    {
        tokens.PushAndGrow( pToken );
        pToken = strtok( NULL, pDelimiter );
    }
}

atString rexMBAnimExportCommon::ArrayToString( const atArray<atString>& atObj, atString strSep)
{
	atString strOutput;
	for(int i=0; i < atObj.GetCount(); ++i)
	{
		strOutput += atObj[i];

		if((atObj.GetCount()-1) != i)
		{
			strOutput += strSep;
		}
	}

	return strOutput;
}

//-----------------------------------------------------------------------------

HFBShader rexMBAnimExportCommon::CreateShader( char* pShaderTypeName, char* pShaderName )
{
    // Shader creation.
    HFBShader hShader = NULL;
    FBShaderManager hShaderManager;

    if ( hShaderManager.ShaderTypeNames.Find( pShaderTypeName ) != -1 ||
        hShaderManager.ShaderTypeNamesLocalized.Find( pShaderTypeName ) != -1 )
    {
        hShader = hShaderManager.CreateShader( pShaderTypeName );

        // Change its name, as the default name will be the type name.
        if( hShader )
        {
            hShader->Name = pShaderName;
            return hShader;

        }
        else
        {
            // Warn about creation failure on a correctly registered
            // shader type.
        }
    }
    else
    {
        // Warn about an unknown shader type.
    }

    //
    // Do some work with the shader...
    // 

    return NULL;
}

//-----------------------------------------------------------------------------

HFBAnimationNode rexMBAnimExportCommon::FindAnimByName( HFBAnimationNode pFindNode, const char* pName )
{
    char name[1024];
	safecpy( name, (const char*)pFindNode->Name, sizeof(name) );

    if ( stricmp( name, pName ) == 0 )
    {
        return pFindNode;
    }

    int count = pFindNode->Nodes.GetCount();
    for ( int i = 0; i < count; ++i )
    {
        HFBAnimationNode node = FindAnimByName( pFindNode->Nodes[i], pName );
        if ( node )
        {
            return node;
        }
    }

    return NULL;
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::FindAnimsBeginningWith( HFBAnimationNode pFindNode, const char* pStartsWith, atArray<HFBAnimationNode> &animList )
{
    char name[256];
	safecpy( name, (const char*)pFindNode->Name, sizeof(name) );

    char *pStr = strstr( name, pStartsWith );
    if ( (pStr != NULL) && (pStr == name) )
    {
        animList.Grow() = pFindNode;
    }

    int count = pFindNode->Nodes.GetCount();
    for ( int i = 0; i < count; ++i )
    {
        FindAnimsBeginningWith( pFindNode->Nodes[i], pStartsWith, animList );
    }
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::FindAnimsEndingWith( HFBAnimationNode pFindNode, const char* pEndsWith, atArray<HFBAnimationNode> &animList )
{
	char name[256];
	safecpy( name, (const char*)pFindNode->Name, sizeof(name) );

	atString strName(name);

	if(strName.EndsWith(pEndsWith))
	{
		animList.Grow() = pFindNode;
	}

	int count = pFindNode->Nodes.GetCount();
	for ( int i = 0; i < count; ++i )
	{
		FindAnimsEndingWith( pFindNode->Nodes[i], pEndsWith, animList );
	}
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderRmptfxPathSetting( atString &rmptfxPath, bool subst )
{
	TCHAR rmptfxPathSetting[_MAX_PATH] = {0};
	if( gs_ExportView.GetRmptfxPath(rmptfxPathSetting, _MAX_PATH, subst) == false)
		return false;

	rmptfxPath = rmptfxPathSetting;
    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetAnimationPostRenderScript( atString &postRenderScript )
{
	TCHAR _postRenderScript[_MAX_PATH] = {0};
	if( gs_ExportView.GetAnimationPostRenderScript(_postRenderScript, _MAX_PATH) == false)
		return false;

	postRenderScript = _postRenderScript;
	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderProjectConfigsSetting( atArray<atString> &configs )
{
	//NOTE: Default only to the currently install project.  
	//This is a deprecated function which used to support multiple projects for switching
	//within the same instance of MotionBuilder.  
	//
	TCHAR projectName[_MAX_PATH]  = {0};
	if( gs_GameView.GetProjectName(projectName, _MAX_PATH) == false)
		return false;
	
	atString projectConfigs(projectName);
	configs.PushAndGrow(projectConfigs);
    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderNetworkStreamSetting( atString &networkPath )
{
	TCHAR networkStream[_MAX_PATH]  = {0};
	if( gs_GameView.GetNetworkStreamDir(networkStream, _MAX_PATH) == false)
		return false;

	networkPath = networkStream;
	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderCutsceneExportRoot( atString &exportPath )
{
#if HACK_GTA4
		atString assetDir;
		GetAssetPath(assetDir);
		exportPath = assetDir;
		exportPath += "\\cuts";
#else
        exportPath = "T:\\$(config)\\assets\\cuts";
#endif // HACK_GTA4

    exportPath.Replace( "/", "\\" );
    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderCutsceneFaceExportPath( atString &faceExportPath )
{
#if HACK_GTA4
        faceExportPath = "X:\\$(config)\\assets\\cuts\\$(scene)\\faces";
#else
		faceExportPath = "T:\\$(config)\\assets\\cuts\\$(scene)\\faces";
#endif // HACK_GTA4

    faceExportPath.Replace( "/", "\\" );
    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderCutsceneBackupPath( atString &backupPath )
{
	//TODO:  Originally an option for its own path, but the fallback was the GetMotionBuilderCutsceneExportRoot.
	//Restore this option in a configuration file.
    return GetMotionBuilderCutsceneExportRoot( backupPath );
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderAnimCtrlFilePathSetting( atString &animCtrlFilePath )
{
	TCHAR animCtrlPathSetting[_MAX_PATH]= {0};
	if( gs_ExportView.GetControlFilePath(animCtrlPathSetting, _MAX_PATH) == false)
		return false;

	animCtrlFilePath = animCtrlPathSetting;

    for ( int i = 0; i < animCtrlFilePath.GetLength(); ++i )
    {
        if ( animCtrlFilePath[i] == '/' )
        {
            animCtrlFilePath[i] = '\\';
        }
    }

	if ( animCtrlFilePath.EndsWith( "\\" ) == false )
		animCtrlFilePath += "\\";

    return true;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::GetMotionBuilderAnimCompressionFilePathSetting( atString &animCompressionFilePath )
{
	TCHAR compressFilePathSetting[_MAX_PATH] = {0};
	if( gs_ExportView.GetCompressionFilePath(compressFilePathSetting, _MAX_PATH) == false)
		return false;

	animCompressionFilePath = compressFilePathSetting;

	for ( int i = 0; i < animCompressionFilePath.GetLength(); ++i )
	{
		if ( animCompressionFilePath[i] == '/' )
		{
			animCompressionFilePath[i] = '\\';
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderEventDefsPathSetting( atString &eventDefsPath )
{
	//NOTE:  This function does not appear to be actively used.
	TCHAR eventDefsPathSetting[_MAX_PATH] = {0};
	if( gs_ExportView.GetEventDefsPath(eventDefsPathSetting, _MAX_PATH) == false)
		return false;

	eventDefsPath = eventDefsPathSetting;
    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderFxListCutsceneSectionName( atString &cutsceneSectionName )
{
	TCHAR sectionName[_MAX_PATH] = {0};
	if( gs_ExportView.GetEffectsCutsceneSectionName(sectionName, _MAX_PATH) == false)
		return false;

	cutsceneSectionName = sectionName;
	return true;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::GetMotionBuilderSkelFilePathSetting( atString &skelFilePath )
{
	TCHAR skelFilePathSetting[_MAX_PATH] = {0};
	if( gs_ExportView.GetSkeletonFilePath(skelFilePathSetting, _MAX_PATH) == false)
		return false;

	skelFilePath = skelFilePathSetting;

    for ( int i = 0; i < skelFilePath.GetLength(); ++i )
    {
        if ( skelFilePath[i] == '/' )
        {
            skelFilePath[i] = '\\';
        }
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetMotionBuilderStreamPathSetting( atString &streamPath )
{
	TCHAR streamPathSetting[_MAX_PATH] = {0};
	if( gs_GameView.GetNetworkStreamDir(streamPathSetting, _MAX_PATH) == false)
		return false;

	streamPath = streamPathSetting;
    for ( int i = 0; i < streamPath.GetLength(); ++i )
    {
        if ( streamPath[i] == '/' )
        {
            streamPath[i] = '\\';
        }
    }

    return true;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::GetMotionBuilderRootAssetPathSetting( atArray<atString> &rootAssetPaths )
{
	TCHAR rootAssetPathSetting[_MAX_PATH] = {0};
	if( gs_ExportView.GetAnimExportPath(rootAssetPathSetting, _MAX_PATH) == false)
		return false;

	atString rootAssetPath(rootAssetPathSetting);
	rootAssetPath.Split(rootAssetPaths, ",", true);

	for(int i=0; i < rootAssetPaths.GetCount(); ++i)
	{
		rootAssetPaths[i].Replace("/", "\\");
	}

    return true;
}


//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::GetMotionBuilderRootCompressedAssetPathSetting( atString &rootCompressedAssetPath )
{
	TCHAR rootAssetPathSetting[_MAX_PATH] = {0};
	if( gs_ExportView.GetAnimCompressedExportPath(rootAssetPathSetting, _MAX_PATH) == false)
		return false;

	rootCompressedAssetPath = rootAssetPathSetting;

	for ( int i = 0; i < rootCompressedAssetPath.GetLength(); ++i )
	{
		if ( rootCompressedAssetPath[i] == '/' )
		{
			rootCompressedAssetPath[i] = '\\';
		}
	}

    return true;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::GetMotionBuilderContentRootPathSetting( atString &contentRootPath )
{
	TCHAR toolsRootPath[_MAX_PATH] = {0};

	if ( gs_GameView.GetToolsRootDir(toolsRootPath, _MAX_PATH) == false ) 
		return false;

	contentRootPath = toolsRootPath;
	return true;
}

//-----------------------------------------------------------------------------

HFBModelNull rexMBAnimExportCommon::CreateParticleEffect( const char* pName )
{
    // Find a unique name
    char cNamespace[256];
    safecpy( cNamespace, pName );
    
    char cModelName[64];
    safecpy( cModelName, "effect" );
       
    char cFullName[512];
    sprintf( cFullName, "%s:%s", cNamespace, cModelName );

    int i = 1;
    
    HFBModel pModel = RAGEFindModelByName( cFullName );
    while ( pModel != NULL )
    {
        sprintf( cNamespace, "%s^%d", pName, i );
        //sprintf( cModelName, "effect%d", i );
        sprintf( cFullName, "%s:%s", cNamespace, cModelName );

        pModel = RAGEFindModelByName( cFullName );

        ++i;
    }

    // create the model
    HFBModelNull particleEffectModel = new FBModelNull( cModelName );
    particleEffectModel->Size = 200.0;
    particleEffectModel->SetVector(FBVector3d(0.0f, 10.0f, 0.0f), kModelTranslation);
    particleEffectModel->Show = true;    

    particleEffectModel->ProcessObjectNamespace( kFBConcatNamespace, cNamespace );

    return particleEffectModel;
}

//-----------------------------------------------------------------------------

HFBModel rexMBAnimExportCommon::CreateDecal( const char* pName )
{
	// Find a unique name
	char cNamespace[256];
	safecpy( cNamespace, pName );

	char cModelName[64];
	safecpy( cModelName, "decal" );

	char cFullName[512];
	sprintf( cFullName, "%s:%s", cNamespace, cModelName );

	int i = 1;

	HFBModel pModel = RAGEFindModelByName( cFullName );
	while ( pModel != NULL )
	{
		sprintf( cNamespace, "%s^%d", pName, i );
		//sprintf( cModelName, "effect%d", i );
		sprintf( cFullName, "%s:%s", cNamespace, cModelName );

		pModel = RAGEFindModelByName( cFullName );

		++i;
	}

	// create the model
	HFBModel  decalPlaneModel = new FBModelPlane( cModelName );
	HFBModelNull decalNullModel = new FBModelNull( "null" );

	decalPlaneModel->SetVector(FBVector3d(0.10f, 0.10f, 0.10f), kModelScaling);
	decalPlaneModel->SetVector(FBVector3d(0.0f, 0.0f, 0.0f), kModelTranslation);
	decalNullModel->SetVector(FBVector3d(0.0f, 30.0f, 0.0f), kModelTranslation); // Put the NULL 30cm away, game can only project 30cm
	decalNullModel->SetVector(FBVector3d(90.0f, 0.0f, 0.0f), kModelRotation);
	decalPlaneModel->Show = true; 
	decalNullModel->Show = true;

	decalPlaneModel->ProcessObjectNamespace( kFBConcatNamespace, cNamespace );
	decalNullModel->ProcessObjectNamespace( kFBConcatNamespace, cNamespace );

	decalNullModel->Parent = decalPlaneModel;

	return decalPlaneModel;
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::AddDecalProperties( HFBModel decalModel )
{
	// add custom properties
	HFBProperty pProperty = decalModel->PropertyList.Find( "dclinit_Height" );
	if ( pProperty == NULL )
	{
		pProperty = decalModel->PropertyCreate( "dclinit_Height", kFBPT_double, ANIMATIONNODE_TYPE_NUMBER, false, true );
		if ( pProperty )
		{
			pProperty->SetMinMax( 0.0, 1000.0 );

			double d = 1.0;
			pProperty->SetData( &d );
		}
	}

	pProperty = decalModel->PropertyList.Find( "dclinit_Width" );
	if ( pProperty == NULL )
	{
		pProperty = decalModel->PropertyCreate( "dclinit_Width", kFBPT_double, ANIMATIONNODE_TYPE_NUMBER, false, true );
		if ( pProperty )
		{
			pProperty->SetMinMax( 0.0, 1000.0 );

			double d = 1.0;
			pProperty->SetData( &d );
		}
	}

	pProperty = decalModel->PropertyList.Find( "dclinit_Color" );
	if ( pProperty == NULL )
	{
		pProperty = decalModel->PropertyCreate( "dclinit_Color", kFBPT_ColorRGBA, ANIMATIONNODE_TYPE_COLOR_RGBA, false, true );
		if ( pProperty )
		{
			FBColorAndAlpha clr( 1.0, 1.0, 1.0, 1.0 );
			pProperty->SetData( &clr );
		}
	}

	pProperty = decalModel->PropertyList.Find( "FX_Active" );
	if ( pProperty == NULL )
	{
		pProperty = decalModel->PropertyCreate( "FX_Active", kFBPT_bool, ANIMATIONNODE_TYPE_BOOL, true, true );
		if ( pProperty )
		{
			FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;
			bool b = false;
			pPropertyBool->SetData( &b );
		}
	}
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::AddParticleEffectProperties( HFBModel particleEffectModel, const char* pRmptfxPath, const char* pName, const char* pListName )
{
    // add custom properties
    HFBProperty pProperty = particleEffectModel->PropertyList.Find( "fxinit_CanBeCulled" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_CanBeCulled", kFBPT_bool, ANIMATIONNODE_TYPE_BOOL, false, true );
        if ( pProperty )
        {
            FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;
            bool b = true;
            pPropertyBool->SetData( &b );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_ColorAlphaTint" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_ColorAlphaTint", kFBPT_double, ANIMATIONNODE_TYPE_NUMBER, false, true );
        if ( pProperty )
        {
            pProperty->SetMinMax( 0.0, 1.0 );

            double d = 1.0;
            pProperty->SetData( &d );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_ColorTint" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_ColorTint", kFBPT_ColorRGBA, ANIMATIONNODE_TYPE_COLOR_RGBA, false, true );
        if ( pProperty )
        {
            FBColor clr( 1.0, 1.0, 1.0 );
            pProperty->SetData( &clr );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_InvertXAxis" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_InvertXAxis", kFBPT_bool, ANIMATIONNODE_TYPE_BOOL, false, true );
        if ( pProperty )
        {
            FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;
            bool b = false;
            pPropertyBool->SetData( &b );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_InvertYAxis" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_InvertYAxis", kFBPT_bool, ANIMATIONNODE_TYPE_BOOL, false, true );
        if ( pProperty )
        {
            FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;
            bool b = false;
            pPropertyBool->SetData( &b );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_InvertZAxis" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_InvertZAxis", kFBPT_bool, ANIMATIONNODE_TYPE_BOOL, false, true );
        if ( pProperty )
        {
            FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;
            bool b = false;
            pPropertyBool->SetData( &b );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_LODScale" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_LODScale", kFBPT_double, ANIMATIONNODE_TYPE_NUMBER, false, true );
        if ( pProperty )
        {
            pProperty->SetMinMax( 0.0, 1000.0 );

            FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;
            double d = 1.0;
            pPropertyDouble->SetData( &d );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_OffsetRotation" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_OffsetRotation", kFBPT_Vector3D, ANIMATIONNODE_TYPE_VECTOR, false, true );
        if ( pProperty )
        {
            pProperty->SetMinMax( -10000.0, 10000.0 );

            FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;
            FBVector3d v( 0.0, 0.0, 0.0 );
            pPropertyVector3d->SetData( &v );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_OffsetTranslation" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_OffsetTranslation", kFBPT_Vector3D, ANIMATIONNODE_TYPE_VECTOR, false, true );
        if ( pProperty )
        {
            pProperty->SetMinMax( -10000.0, 10000.0 );

            FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;
            FBVector3d v( 0.0, 0.0, 0.0 );
            pPropertyVector3d->SetData( &v );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_OverrideSpawnDistance" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_OverrideSpawnDistance", kFBPT_double, ANIMATIONNODE_TYPE_NUMBER, false, true );
        if ( pProperty )
        {
            pProperty->SetMinMax( -1.0, 10000.0 );

            FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;
            double d = -1.0;
            pPropertyDouble->SetData( &d );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_UserZoomScale" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_UserZoomScale", kFBPT_double, ANIMATIONNODE_TYPE_NUMBER, false, true );
        if ( pProperty )
        {
            pProperty->SetMinMax( 0.0, 1000.0 );

            FBPropertyDouble *pPropertyDouble = (FBPropertyDouble *)pProperty;
            double d = 1.0;
            pPropertyDouble->SetData( &d );
        }
    }

    pProperty = particleEffectModel->PropertyList.Find( "fxinit_Velocity" );
    if ( pProperty == NULL )
    {
        pProperty = particleEffectModel->PropertyCreate( "fxinit_Velocity", kFBPT_Vector3D, ANIMATIONNODE_TYPE_VECTOR, false, true );
        if ( pProperty )
        {
            pProperty->SetMinMax( 0.0, 1000.0 );

            FBPropertyVector3d *pPropertyVector3d = (FBPropertyVector3d *)pProperty;
            FBVector3d v( 0.0, 0.0, 0.0 );
            pPropertyVector3d->SetData( &v );
        }
    }

	pProperty = particleEffectModel->PropertyList.Find( "FX_List" );
	if ( pProperty == NULL )
	{
		pProperty = particleEffectModel->PropertyCreate( "FX_List", kFBPT_charptr, ANIMATIONNODE_TYPE_STRING, false, true );
		if ( pProperty )
		{
			FBPropertyString *pPropertyString = (FBPropertyString *)pProperty;

			char* pListFullPath = rage_new char[RAGE_MAX_PATH];
			sprintf(pListFullPath, "%s\\fxlists\\%s", pRmptfxPath, pListName);
			pPropertyString->SetPropertyValue( pListFullPath );
		}
	}
   
    char cEffectRuleFilename[RAGE_MAX_PATH];
    if ( strstr( pName, ":" ) != NULL )
    {
        safecpy( cEffectRuleFilename, pName, sizeof(cEffectRuleFilename) );
    }
    else
    {
        sprintf( cEffectRuleFilename, "%s\\effectrules\\%s", pRmptfxPath, pName );
    }

	if(!ASSET.Exists(cEffectRuleFilename, "effectrule"))
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg, "Unable to open file '%s.effectrule'", cEffectRuleFilename);
		FBMessageBox( "Create Effect Warning", cMsg, "OK" );
	}

	pProperty = particleEffectModel->PropertyList.Find( "FX_RuleFile" );
	if ( pProperty == NULL )
	{
		pProperty = particleEffectModel->PropertyCreate( "FX_RuleFile", kFBPT_charptr, ANIMATIONNODE_TYPE_STRING, false, true );
		if ( pProperty )
		{
			FBPropertyString *pPropertyString = (FBPropertyString *)pProperty;

			char* pRuleFullPath = rage_new char[RAGE_MAX_PATH];
			strcpy(pRuleFullPath, cEffectRuleFilename);
			pPropertyString->SetPropertyValue( pRuleFullPath );
		}
	}

	if(rexMBAnimExportCommon::IsEffectLooped( cEffectRuleFilename ))
	{
		pProperty = particleEffectModel->PropertyList.Find( "FX_Active" );
		if ( pProperty == NULL )
		{
			pProperty = particleEffectModel->PropertyCreate( "FX_Active", kFBPT_bool, ANIMATIONNODE_TYPE_BOOL, true, true );
			if ( pProperty )
			{
				FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;
				bool b = false;
				pPropertyBool->SetData( &b );
			}
		}
	}
	else
	{
		pProperty = particleEffectModel->PropertyList.Find( "FX_Trigger" );
		if ( pProperty == NULL )
		{
			pProperty = particleEffectModel->PropertyCreate( "FX_Trigger", kFBPT_bool, ANIMATIONNODE_TYPE_BOOL, true, true );
			if ( pProperty )
			{
				FBPropertyBool *pPropertyBool = (FBPropertyBool *)pProperty;
				bool b = false;
				pPropertyBool->SetData( &b );
			}
		}
	}

    atArray<ConstString> evoParamNameList;
    if ( !rexMBAnimExportCommon::GetEvoParamNames( cEffectRuleFilename, evoParamNameList ) )
    {
        return false;
    }

    for ( int i = 0; i < evoParamNameList.GetCount(); ++i )
    {
        pProperty = particleEffectModel->PropertyList.Find( evoParamNameList[i].c_str() );
        if ( pProperty == NULL )
        {
            pProperty = particleEffectModel->PropertyCreate( (char*)evoParamNameList[i].c_str(), kFBPT_double, ANIMATIONNODE_TYPE_NUMBER, true, true );
            if ( pProperty != NULL )
            {
                pProperty->SetMinMax( 0.0, 1.0 );

                FBPropertyAnimatable* pPropertyAnimatable = (FBPropertyAnimatable*)pProperty;
                pPropertyAnimatable->SetAnimated( true );
            }
        }
    }

    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::IsEffectLooped( const char *pEffectRuleFilename )
{
	parTree* pTree = PARSER.LoadTree( pEffectRuleFilename, "effectrule" );
	if ( (pTree == NULL) || (pTree->GetRoot() == NULL) || (pTree->GetRoot()->GetChild() == NULL) )
	{
		return false;
	}

	parTreeNode *pLoopsNode = pTree->GetRoot()->FindChildWithName( "numLoops" );
	if ( pLoopsNode )
	{
		parAttribute* pLoopAttr = pLoopsNode->GetElement().FindAttribute("value");
		if(pLoopAttr)
		{
			if(-1 == pLoopAttr->GetIntValue()) // -1 is looped, another else is not
			{
				return true;
			}
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetEvoParamNames( const char *pEffectRuleFilename, atArray<ConstString> &evoParamNameList )
{
    parTree* pTree = PARSER.LoadTree( pEffectRuleFilename, "effectrule" );
    if ( (pTree == NULL) || (pTree->GetRoot() == NULL) || (pTree->GetRoot()->GetChild() == NULL) )
    {
		return false;
    }

    parTreeNode *pPtxEvoGroupNode = pTree->GetRoot()->FindChildWithName( "pEvolutionList" );
    if ( (pPtxEvoGroupNode == NULL) || (pPtxEvoGroupNode->GetChild() == NULL) )
    {
        return true;
    }

    parTreeNode *pEvoListNode = pPtxEvoGroupNode->FindChildWithName( "evolutions" );
    if ( (pEvoListNode == NULL) || (pEvoListNode->GetChild() == NULL) )
    {
        return true;
    }

    // Iterate over all the children
    for ( parTreeNode::ChildNodeIterator node = pEvoListNode->BeginChildren(); node != pEvoListNode->EndChildren(); ++node ) 
    {
        if ( strcmp( (*node)->GetElement().GetName(), "Item" ) == 0 )
        {
            parTreeNode *pEvoNameNode = (*node)->FindChildWithName( "name" );
            if ( pEvoNameNode == NULL )
            {
                continue;
            }

            char *pName = pEvoNameNode->GetData();
            if ( pName != NULL )
            {
                evoParamNameList.PushAndGrow( ConstString(pName) );
            }
        }
    }

    delete pTree;
    return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::FindAnimCtrlFilePathFiles( atArray<atString> &specFilenames )
{
	atString animCtrlPath;
	if ( GetMotionBuilderAnimCtrlFilePathSetting(animCtrlPath) == false )
		return false;

	if ( animCtrlPath.EndsWith( "\\" ) )
	{
		animCtrlPath.Set( animCtrlPath, 0, animCtrlPath.GetLength() - 1 );
	}

	ASSET.EnumFiles( animCtrlPath, &CB_FindSpecFilesCallback, &specFilenames );

	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::FindAnimCompressionFilePathFiles( atArray<atString> &compressFilenames )
{
	atString filePath;
	if ( GetMotionBuilderAnimCompressionFilePathSetting(filePath) == false )
		return false;

	if ( filePath.EndsWith( "\\" ) )
	{
		filePath.Set( filePath, 0, filePath.GetLength() - 1 );
	}

	ASSET.EnumFiles( filePath.c_str(), &CB_FindCompressionCommandFilesCallback, &compressFilenames );
	return true;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::FindSkelFilePathFiles( atArray<atString> &skelFilenames )
{
	atString filePath;
	GetMotionBuilderSkelFilePathSetting(filePath);
	if ( filePath.EndsWith( "\\" ) )
	{
		filePath.Set( filePath, 0, filePath.GetLength() - 1 );
	}

	ASSET.EnumFiles( filePath.c_str(), &CB_FindSkelFilesCallback, &skelFilenames );

	if( skelFilenames.GetCount() == 0 )
	{
		skelFilenames.PushAndGrow( atString("(no skeleton file)") );
	}

	return true;
}

//-----------------------------------------------------------------------------
void rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( atArray<atString> &filenames, atString &comboBoxString )
{
	
	for( int i=0; i<filenames.GetCount(); i++ )
	{
		comboBoxString += filenames[i];
		if( i != filenames.GetCount() - 1)
			comboBoxString += "~";
	}
}

//-----------------------------------------------------------------------------
atString rexMBAnimExportCommon::ResolveComboValueFromDelimitedString( int idx, atString &comboBoxString )
{
	atArray<atString> comboBoxStrTokens;
	comboBoxString.Split( comboBoxStrTokens, "~" );

	return comboBoxStrTokens[idx];
}

//-----------------------------------------------------------------------------
int rexMBAnimExportCommon::ResolveComboIndexFromString( atString val, atString &comboBoxString )
{
	atArray<atString> comboBoxStrTokens;
	int retVal = -1;
	comboBoxString.Split( comboBoxStrTokens, "~" );
	for( int i=0; i<comboBoxStrTokens.GetCount(); i++ )
	{
		if( comboBoxStrTokens[i] == val )
		{
			retVal = i;
			break;
		}
	}

	return retVal;
}

//-----------------------------------------------------------------------------
atString rexMBAnimExportCommon::GetProjectName()
{
	TCHAR toolsProjectName[_MAX_PATH] = {0};

	if ( gs_GameView.GetProjectName(toolsProjectName, _MAX_PATH) == false )
		return atString("");

	return atString(toolsProjectName);
}
//-----------------------------------------------------------------------------
atString rexMBAnimExportCommon::GetProjectRootDir()
{
	TCHAR toolsProjectRoot[_MAX_PATH] = {0};

	if ( gs_GameView.GetProjectRootDir(toolsProjectRoot, _MAX_PATH) == false )
		return atString("");

	return atString(toolsProjectRoot);
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::SplitStringByLength( const char *pInput, int size, atArray<atString> &output )
{
    int iTotalLen = (int)strlen( pInput );
    if ( iTotalLen < size )
    {
        output.Grow() = pInput;
        return;
    }

    char *pBuffer = rage_new char[size];

    char *pPtr = const_cast<char *>( pInput );
    int iLen = iTotalLen;
    while ( iLen > 0 )
    {
        if ( iLen >= size )
        {
            iLen = size - 1; 
        }

        safecpy( pBuffer, pPtr, iLen + 1 );
        output.Grow() = pBuffer;

        pPtr += iLen;
        iLen = (int)strlen( pPtr );
    }

    delete [] pBuffer;
}

//-----------------------------------------------------------------------------

const char* rexMBAnimExportCommon::ConcatenateStrings( const atArray<atString> &input, const char* pDelimiter )
{
    int iTotalLen = 1;  // for the null-terminator
    for ( int i = 0; i < input.GetCount(); ++i )
    {
        iTotalLen += input[i].GetLength();
    }

    char *pOutput = rage_new char[iTotalLen];
    pOutput[0] = 0;

    for ( int i = 0; i < input.GetCount(); ++i )
    {
		strcat( pOutput, input[i].c_str() );
		if(pDelimiter && i < input.GetCount()-1)
		{
			safecat( pOutput, pDelimiter, sizeof( pOutput ) );
		}
    }

    return pOutput;
}

bool rexMBAnimExportCommon::HasCamera( const RexAnimMarkupData* pAnimData )
{	
	for( int j = 0; j < pAnimData->GetNumCharacters(); j++ )
	{
		if( strcmp( pAnimData->GetCharacter(j)->GetModelType(), MODEL_TYPE_CAMERA ) == 0 )
		{
			return true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------

//PURPOSE : This method will take in a string name of a take, and locate
//the actual structure for that take in the current scene.
bool rexMBAnimExportCommon::FindTake(const char* szTakeName, HFBTake& retTake)
{
	FBSystem m_system;
	HFBScene hScene = m_system.Scene;

	int nTakes = hScene->Takes.GetCount();
	for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
	{
		HFBTake hTake = hScene->Takes[takeIdx];
		const char* szCurTakeName = hTake->Name.AsString();

		if(strcmp(szTakeName, szCurTakeName) == 0)
		{
			retTake = hTake;
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

//PURPOSE : This method will take in a string name of a take, and locate
//the actual structure for that take in the current scene.
bool rexMBAnimExportCommon::TakeExists(const HFBTake take)
{
	FBSystem m_system;
	HFBScene hScene = m_system.Scene;

	int nTakes = hScene->Takes.GetCount();
	for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
	{
		HFBTake hSceneTake = hScene->Takes[takeIdx];

		if(take == hSceneTake)
		{
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

#define YES_MESSAGE_BOX_BUTTON_TEXT "Yes"
#define NO_MESSAGE_BOX_BUTTON_TEXT "No"

rexMBAnimExportCommon::MessageBoxResult rexMBAnimExportCommon::ShowYesNoMessageBox(char* title, char* message)
{
	int result = FBMessageBox(title, message, YES_MESSAGE_BOX_BUTTON_TEXT, NO_MESSAGE_BOX_BUTTON_TEXT);
	if(result == 1)
		return RESULT_YES;
	else
		return RESULT_NO;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::BuildAnimationDictionariesUsingContentSystem( const char* pRootSrcDir, const char* pAnimSrcDir, const char* pSceneName, const char* pRelativeOutputPath, const char* pStreamSubDir, bool bCutscene, bool bPreview )
{
	atString rootCompressedAssetPath;

	atString rubyFilename;
	atString contentRootPath;
	rexMBAnimExportCommon::GetMotionBuilderContentRootPathSetting( contentRootPath );
	rubyFilename = atString( contentRootPath, "\\lib\\pipeline\\resourcing\\gateways\\pack_file.rb" );
	rexMBAnimExportCommon::GetMotionBuilderRootCompressedAssetPathSetting( rootCompressedAssetPath );

	char cToolsBin[RAGE_MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsBin, RAGE_MAX_PATH);

	char cCmdLine[RAGE_MAX_PATH * 2];
	memset(cCmdLine, 0, RAGE_MAX_PATH * 2);
	safecat( cCmdLine, cToolsBin, sizeof(cCmdLine));
	safecat( cCmdLine, "\\bin\\ruby\\bin\\ruby.exe ", sizeof(cCmdLine) );
	safecat( cCmdLine, rubyFilename.c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " --project=", sizeof(cCmdLine) );
	// TODO: Determine project name.  Really need to work out a solution for dynamically changing projects
	// since module settings relys on an env variable.  
	safecat( cCmdLine, rexMBAnimExportCommon::GetProjectName().c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " --rootsrcdir=", sizeof(cCmdLine) );
	safecat( cCmdLine, pRootSrcDir, sizeof(cCmdLine) );
	safecat( cCmdLine, " --animsrcdir=", sizeof(cCmdLine) );
	safecat( cCmdLine, pAnimSrcDir, sizeof(cCmdLine) );
	safecat( cCmdLine, " --scenename=", sizeof(cCmdLine) );
	safecat( cCmdLine, pSceneName, sizeof(cCmdLine) );
	safecat( cCmdLine, " --destdir=", sizeof(cCmdLine) );
	safecat( cCmdLine, rootCompressedAssetPath.c_str(), sizeof(cCmdLine) );

	safecat( cCmdLine, pRelativeOutputPath,  sizeof(cCmdLine) );
	safecat( cCmdLine, " --outputdir=", sizeof(cCmdLine) );
	safecat( cCmdLine, pStreamSubDir, sizeof(cCmdLine) );
	safecat( cCmdLine, "/", sizeof(cCmdLine) );
	if( bCutscene )
		safecat( cCmdLine, " --sectioned" , sizeof(cCmdLine) );
	if( bPreview )
		safecat( cCmdLine, " --preview" , sizeof(cCmdLine) );

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);

	int result = system( cCmdLine );
	if ( result != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::BuildIndependantData( const char* pSceneName, const char* pSrcDir, const char* pDestDir,
												 int iSectioningMethod, float fSectionByTimeSliceDuration, bool bBuildCutOnly )
{
	char cSectionOptions[64];
	if ( iSectioningMethod == TIME_SLICE_SECTIONING_METHOD )
	{
		sprintf( cSectionOptions, " --sectionMethod=\"%d\" --sectionDuration=\"%f\"", iSectioningMethod, fSectionByTimeSliceDuration );
	}
	else
	{
		sprintf( cSectionOptions, " --sectionMethod=\"%d\"", iSectioningMethod );
	}

	char cSrcDir[RAGE_MAX_PATH];
	safecpy( cSrcDir, pSrcDir, sizeof(cSrcDir) );
	int len = 0;
	for ( int i = 0; i < len; ++i )
	{
		if ( cSrcDir[i] == '/' )
		{
			cSrcDir[i] = '\\';
		}
	}

	char cCommandLine[RAGE_MAX_PATH * 4];
	cCommandLine[0] = 0;

	char cToolsBin[RAGE_MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsBin, RAGE_MAX_PATH);

	safecat( cCommandLine, cToolsBin, sizeof(cCommandLine));
	safecat( cCommandLine, "\\bin\\ruby\\bin\\ruby.exe ", sizeof(cCommandLine) );

	char cRageToolsRoot[RAGE_MAX_PATH];
	if(!sysGetEnv( "RS_TOOLSROOT", cRageToolsRoot, sizeof(cRageToolsRoot) ))
	{
		ULOGGER_MOBO.SetProgressMessage( "ERROR: RS_TOOLSROOT is not set" );
		return false;
	}

	safecat( cCommandLine, "\"", sizeof(cCommandLine) );
	safecat( cCommandLine, cRageToolsRoot, sizeof(cCommandLine) );
	safecat( cCommandLine, "\\lib\\util\\motionbuilder\\cutscene.rb\" ", sizeof(cCommandLine) );

	if ( (pDestDir != NULL) && strlen( pDestDir ) > 0 )
	{
		safecat( cCommandLine, "--patch ", sizeof(cCommandLine) );
	}
	else
	{
		safecat( cCommandLine, "--build ", sizeof(cCommandLine) );
	}

	safecat( cCommandLine, "--inputDir=\"", sizeof(cCommandLine) );
	safecat( cCommandLine, cSrcDir, sizeof(cCommandLine) );
	safecat( cCommandLine, "\"", sizeof(cCommandLine) );

	if ( (pDestDir != NULL) && strlen( pDestDir ) > 0 )
	{
		char cDestDir[RAGE_MAX_PATH];
		safecpy( cDestDir, pDestDir, sizeof(cDestDir) );
		int len = 0;
		for ( int i = 0; i < len; ++i )
		{
			if ( cDestDir[i] == '/' )
			{
				cDestDir[i] = '\\';
			}
		}

		safecat( cCommandLine, " --patchDir=\"", sizeof(cCommandLine) );
		safecat( cCommandLine, cDestDir, sizeof(cCommandLine) );
		safecat( cCommandLine, "\"", sizeof(cCommandLine) );
	}

	safecat( cCommandLine, cSectionOptions, sizeof(cCommandLine) );

	safecat( cCommandLine, " --sceneName=\"", sizeof(cCommandLine) );
	safecat( cCommandLine, pSceneName, sizeof(cCommandLine) );
	safecat( cCommandLine, "\"", sizeof(cCommandLine) );

	if(bBuildCutOnly)
	{
		safecat( cCommandLine, " --rebuildCutOnly", sizeof(cCommandLine) );
	}

	if(rexMBAnimExportCommon::GetPerforceIntegration())
	{
		safecat( cCommandLine, " --perforce", sizeof(cCommandLine) );
	}

	// close the log so the new process can access it
	ULOGGER_MOBO.SetProgressMessage( cCommandLine );

	// execute the command line
	int result = system( cCommandLine );
	if ( result != 0 )
	{
		char msg[1024];
		sprintf_s( msg, 1024, "ERROR: cmd '%s' returned %d.", cCommandLine, result );
		ULOGGER_MOBO.SetProgressMessage( msg );
	}

	return result == 0;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::CheckToolsLabelStatus( )
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return true;

	atString rubyFilename;
	atString contentRootPath;
	rexMBAnimExportCommon::GetMotionBuilderContentRootPathSetting( contentRootPath );
	rubyFilename = atString( contentRootPath, "\\lib\\util\\data_is_labelled.rb" );

	char cToolsBin[RAGE_MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsBin, RAGE_MAX_PATH);

	char cCmdLine[RAGE_MAX_PATH * 4];
	cCmdLine[0] = 0;
	safecat( cCmdLine, cToolsBin, sizeof(cCmdLine));
	safecat( cCmdLine, "\\bin\\ruby\\bin\\ruby.exe ", sizeof(cCmdLine) );
	safecat( cCmdLine, rubyFilename.c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " --tools", sizeof(cCmdLine) );
	
	ULOGGER_MOBO.SetProgressMessage(cCmdLine);

	atString strOutput;
	int result = ExecuteCommand( cCmdLine, cToolsBin, strOutput );
	if ( result != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("WARNING: NOT ON LABELLED TOOLS" );
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::GetDefaultControlFile( atString& ctrlFile )
{
	TCHAR defaultCtrlFile[_MAX_PATH] = {0};
	if ( gs_ExportView.GetDefaultControlFile(defaultCtrlFile, _MAX_PATH) == false )
		return false;

	ctrlFile = atString(defaultCtrlFile);
	return true;
}
//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::GetDefaultSkeletonFile( atString& skelFile )
{
	TCHAR defaultSkeletonFile[_MAX_PATH] = {0};
	if ( gs_ExportView.GetDefaultSkeletonFile(defaultSkeletonFile, _MAX_PATH) == false )
		return false;

	skelFile = atString(defaultSkeletonFile);
	return true;
}
//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::GetDefaultCompressionFile( atString& compressionFile )
{
	TCHAR defaultCompressionFile[_MAX_PATH] = {0};
	if ( gs_ExportView.GetDefaultCompressionFile(defaultCompressionFile, _MAX_PATH) == false )
		return false;

	compressionFile = atString(defaultCompressionFile);
	return true; 
}
//-----------------------------------------------------------------------------
void rexMBAnimExportCommon::LookAt(const Vector3 &to, Matrix34 &mat, const Vector3& up)
{
	// North LookAt function
	Vector3 dir;
	dir.Subtract(to, mat.d);

	Vec3V dirNew = RCC_VEC3V(dir);
	Vec3V upNew = RCC_VEC3V(up);
	Mat34V matNew = RCC_MAT34V(mat);
	LookDown(matNew,dirNew, upNew);

	mat = RCC_MATRIX34(matNew);

	//mat.LookDown(dir, up);
}

bool rexMBAnimExportCommon::GetScreenFadeFrames( HFBModel pModel, atArray<ScreenFadeEntry>& screenFadeFrames, cutfCutsceneFile2* pCutFile )
{
	HFBMaterial pShaderMaterial = NULL;

#if K_KERNEL_VERSION <= 10000
	HFBGeometry pGeometry = pModel->Geometry;

	int count = pGeometry->Materials.GetCount();
	for ( int i = 0; i < count; ++i )
	{
		HFBMaterial pMaterial = static_cast<HFBMaterial>(pGeometry->Materials.GetAt( i ));
		if ( strstr( (char*)pMaterial->Name, "ScreenFadeMat" ) )
		{
			pShaderMaterial = pMaterial;
			break;
		}
	}
#else
	int count = pModel->Materials.GetCount();
	for ( int i = 0; i < count; ++i )
	{
		HFBMaterial pMaterial = static_cast<HFBMaterial>(pModel->Materials.GetAt( i ));
		if ( strstr( (const char*)pMaterial->Name, "ScreenFadeMat" ) )
		{
			pShaderMaterial = pMaterial;
			break;
		}
	}
#endif

	if ( pShaderMaterial == NULL )
	{
		return false;
	}

	HFBAnimationNode pAnimNode = pShaderMaterial->AnimationNodeInGet();
	if ( pAnimNode )
	{    
		// Diffuse
		atArray<RexMbCutsceneExportData *> animDataList;

		float fStartTime = pCutFile->GetRangeStart() / rexMBAnimExportCommon::GetFPS();
		float fEndTime = pCutFile->GetRangeEnd() / rexMBAnimExportCommon::GetFPS();
		int nFrames = int((fEndTime - fStartTime + TIME_TO_FRAME_TOLERANCE) * rexMBAnimExportCommon::GetFPS()) + 1;

		RexMbCutsceneExportVectorPropertyData *pDiffuseAnimData = rage_new RexMbCutsceneExportVectorPropertyData( "Diffuse", pShaderMaterial, nFrames );
		animDataList.PushAndGrow( pDiffuseAnimData );
		RexMbCutsceneExportModel::CollectAnimData( animDataList, fStartTime, fEndTime, nFrames );

		const atArray<Vector3>& diffuseFrameVectors = pDiffuseAnimData->GetFrameData();

#if K_KERNEL_VERSION <= 10000
		HFBAnimationNode pOpacityNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Opacity" );
#else
		HFBAnimationNode pOpacityNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "TransparentColor" );
		if(!pOpacityNode)
		{
			ULOGGER_MOBO.SetProgressMessage("WARNING: Screen fade is invalid. Transparent channel not set.");
		}
#endif
		int i = 1;
		if(pOpacityNode)
		{
#if K_KERNEL_VERSION > 1000
			pOpacityNode = rexMBAnimExportCommon::FindAnimByName( pOpacityNode, "X" );
#endif
			int keyCount = pOpacityNode->FCurve->Keys.GetCount();
			
			for ( i = 1; i < keyCount; ++i )
			{
				int keyValue1 = (int)pOpacityNode->FCurve->Keys[i-1].Value;
				int keyValue2 = (int)pOpacityNode->FCurve->Keys[i].Value;

				if ( ((keyValue1 == 0) && (keyValue2 == 1)) || ((keyValue1 == 1) && (keyValue2 == 0)) )
				{
					ScreenFadeEntry screenFade;
					screenFade.fStart = rexMBAnimExportCommon::GetSeconds( pOpacityNode->FCurve->Keys[i - 1].Time );
					screenFade.fEnd = rexMBAnimExportCommon::GetSeconds( pOpacityNode->FCurve->Keys[i].Time );
					screenFade.bFadeOut = (keyValue1 == 1) && (keyValue2 == 0);

					int iColorFrame = (int) ( rexMBAnimExportCommon::GetFrameFromTime( (screenFade.bFadeOut ? screenFade.fEnd : screenFade.fStart)) - pCutFile->GetRangeStart());
					if ( (iColorFrame >= 0) && (iColorFrame < diffuseFrameVectors.GetCount()) )
					{
						screenFade.color = Color32( diffuseFrameVectors[iColorFrame].GetX(), 
							diffuseFrameVectors[iColorFrame].GetY(), diffuseFrameVectors[iColorFrame].GetZ(), 1.0f );
					}
					else
					{
						screenFade.color = Color32( 0.0f, 0.0f, 0.0f, 1.0f );
					}

					screenFadeFrames.PushAndGrow( screenFade );
				}
				else
				{
					ULOGGER_MOBO.SetProgressMessage( "WARNING: Invalid screen fade key setup, %d/%d is not valid.", keyValue1, keyValue2);
				}
			}
		}

		// Remove items that conflict with the screen fade settings
		for ( int i = 0; i < screenFadeFrames.GetCount(); )
		{
			if ( pCutFile->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_IN_FLAG ) 
				&& (screenFadeFrames[i].fStart <= fStartTime + pCutFile->GetFadeInCutsceneAtBeginningDuration()) )
			{
				ULOGGER_MOBO.SetProgressMessage( "WARNING: Found a screen fade starting at frame %d that conflicts with the Fade Out Game (at beginning) setting.  It will be ignored.",
				rexMBAnimExportCommon::GetFrameFromTime(screenFadeFrames[i].fStart) );

				screenFadeFrames.Delete( i );
			}
			else if ( pCutFile->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_OUT_FLAG ) 
				&& (screenFadeFrames[i].fEnd >= fEndTime - pCutFile->GetFadeOutCutsceneAtEndDuration()) )
			{
				ULOGGER_MOBO.SetProgressMessage( "WARNING: Found a screen fade ending at frame %d that conflicts with the Fade In Game (at end) setting.  It will be ignored.",
				rexMBAnimExportCommon::GetFrameFromTime(screenFadeFrames[i].fEnd) );

				screenFadeFrames.Delete( i );
			}
			else
			{
				++i;
			}
		}
	}

	return screenFadeFrames.GetCount() > 0;
}

void rexMBAnimExportCommon::GetCameraCutData( atArray<SCameraCutData> &cameraCutDataList, cutfCutsceneFile2* pCutFile )
{
	atArray<cutfObject *> cameraObjectList;
	pCutFile->FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );
	if ( cameraObjectList.GetCount() > 0 )
	{
		HFBModel pModel = rexMBAnimExportCommon::FindModelForObject( cameraObjectList[0] );
		if ( pModel != NULL )
		{
			SToyboxEntry toyboxEntryDummy;
			RexMbCutsceneExportCameraModel cameraModel( pModel, &toyboxEntryDummy, NULL );
			cameraModel.GetCameraCutData( cameraCutDataList );
		}
	}
}

void rexMBAnimExportCommon::GetSectionSplitTimes( HFBModel pCamera, atArray<float> &sectionSplitList )
{
	sectionSplitList.Reset();

	HFBAnimationNode pAnimNode = pCamera->AnimationNodeInGet();
	HFBAnimationNode pSectionSplitAnimNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "SectionSplit" );

	if ( pSectionSplitAnimNode )
	{
		for ( int i = 0; i < pSectionSplitAnimNode->FCurve->Keys.GetCount(); ++i )
		{
			FBTime time = (FBTime)pSectionSplitAnimNode->FCurve->Keys[i].Time;
			float fSeconds = (float)time.GetSecondDouble();

			sectionSplitList.PushAndGrow( fSeconds );
		}
	}
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::GetOverlayNames( HFBModel pCameraModel, atArray<ConstString> &overlayNameList )
{
	HFBAnimationNode animNode = pCameraModel->AnimationNodeInGet();
	if ( animNode )
	{
		atArray<HFBAnimationNode> nodeList;
		rexMBAnimExportCommon::FindAnimsEndingWith( animNode, ":Overlay", nodeList );

		overlayNameList.Reserve( nodeList.GetCount() );
		for ( int i = 0; i < nodeList.GetCount(); ++i )
		{
			overlayNameList.Append() = ConstString( (const char*)nodeList[i]->Name );
		}
	}

	return overlayNameList.GetCount() > 0;
}

bool rexMBAnimExportCommon::GetOverlayFrames( HFBModel pCameraModel, const char* pOverlayName, atArray<OverlayEntry> &overlayFrames, cutfCutsceneFile2* /*pCutFile*/ )
{
	if ( pCameraModel != NULL )
	{
		// Get camcorder period in milliseconds (start and end)
		HFBAnimationNode animNode = pCameraModel->AnimationNodeInGet();
		if ( animNode != NULL )
		{
			HFBAnimationNode overlayNode = rexMBAnimExportCommon::FindAnimByName( animNode, pOverlayName );
			if ( overlayNode != NULL )
			{
				int keyFrame = -1;
				int keyCount = overlayNode->FCurve->Keys.GetCount();
				for( int i = 0; i < keyCount; ++i )
				{
					float keyTime = rexMBAnimExportCommon::GetSeconds( overlayNode->FCurve->Keys[i].Time );
					keyFrame = rexMBAnimExportCommon::GetFrameFromTime(keyTime);

					OverlayEntry overlay;
					
					if(overlayNode->FCurve->Keys[i].Value == 1)
					{
						overlay.bStart = true;
						overlay.iStart = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames(keyFrame);
						overlayFrames.PushAndGrow( overlay );
					}
					else if(overlayNode->FCurve->Keys[i].Value == 0)
					{
						overlay.bStart = false;
						overlay.iEnd = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames(keyFrame);
						overlayFrames.PushAndGrow( overlay );
					}
				}	
			}
		}
	}

	return overlayFrames.GetCount() > 0;
}

HFBModel rexMBAnimExportCommon::FindModelForObject( const cutfObject *pObject )
{
	if(dynamic_cast<const cutfBlockingBoundsObject *>( pObject )) 
	{
		// Blocking bound objects have no model associated.
		return NULL;
	}

	const cutfNamedObject *pNamedObject = dynamic_cast<const cutfNamedObject *>( pObject );
	if ( pNamedObject != NULL )
	{
		HFBModel pModel = RAGEFindModelByName( const_cast<char *>( pNamedObject->GetName().GetCStr() ) );
		if ( pModel == NULL )
		{
			char msg[256];
			sprintf( msg, "ERROR:  No model was found with the name '%s'.", pNamedObject->GetName().GetCStr() );
			ULOGGER_MOBO.SetProgressMessage( msg );
		}

		const cutfPedModelObject *pPedObject = dynamic_cast<const cutfPedModelObject *>( pObject );
		if ( pPedObject != NULL )
		{
			if(pPedObject->GetFaceAnimationNodeName().GetCStr() != NULL && (strcmpi(pPedObject->GetFaceAnimationNodeName().GetCStr(),"") != 0))
			{
				HFBModel pFaceModel = RAGEFindModelByName( const_cast<char *>( pPedObject->GetFaceAnimationNodeName().GetCStr() ) );
				if ( pFaceModel == NULL )
				{
					char msg[256];
					sprintf( msg, "ERROR:  No model was found with the name '%s'.", pPedObject->GetFaceAnimationNodeName().GetCStr() );
					ULOGGER_MOBO.SetProgressMessage( msg );
				}

				return pModel;
			}	
		}

		return pModel;
	}

	return NULL;
}

bool GenDynamicResourcingScript( const char *ruby, const char *projname, const char * imagename, const char * subdir, bool rebuild = false )
{
	atString project(projname);
	project.Lowercase();
	char msg[1024];
	fiStream *rubyfile = ASSET.Create( ruby, "" );

	rexMBAnimExportCommon::WriteLine(rubyfile, "require 'pipeline/config/projects'\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "require 'pipeline/resourcing/convert'\n" );

	sprintf(msg, "project = Pipeline::Config.instance.projects[\"%s\"]\n", project.c_str());
	rexMBAnimExportCommon::WriteLine(rubyfile, msg );
	rexMBAnimExportCommon::WriteLine(rubyfile, "project.load_content(nil, Pipeline::Project::ContentType::OUTPUT | Pipeline::Project::ContentType::PLATFORM)\n" );
	sprintf(msg, "image_name = \"%s\"\n", imagename);
	rexMBAnimExportCommon::WriteLine(rubyfile, msg );

	rexMBAnimExportCommon::WriteLine(rubyfile, "ind_output_node = nil\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "plat_output_list = []\n" );

	rexMBAnimExportCommon::WriteLine(rubyfile, "img_output_list = project.content.find_by_script(\"content.name == '\" + image_name + \"' and content.xml_type == 'image'\" )\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "img_output_list.each do | img_output_item |\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "\tif img_output_item.target == project.ind_target\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "\t\tind_output_node = img_output_item\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "\telse\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "\t\tplat_output_list << img_output_item\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "\tend\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "end\n\n" );


	rexMBAnimExportCommon::WriteLine(rubyfile, "stream_dir = project.netstream\n" );
	sprintf(msg, "stream_dir = Pipeline::OS::Path.combine(stream_dir, \"%s\")\n", subdir);
	rexMBAnimExportCommon::WriteLine(rubyfile, msg );

	rexMBAnimExportCommon::WriteLine(rubyfile, "pack_file_list = Pipeline::OS::FindEx.find_files(stream_dir + \"/*.i*\", false)\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "pack_file_list.each do | pack_file |\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "\tind_output_node.add_input(Pipeline::Content::RPF.new( Pipeline::OS::Path.get_basename( pack_file ), Pipeline::OS::Path.get_directory( pack_file ), Pipeline::OS::Path.get_extension( pack_file ), project.ind_target ))\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "end\n\n" );

	rexMBAnimExportCommon::WriteLine(rubyfile, "convert = Pipeline::ConvertSystem.instance()\n" );
	sprintf(msg, "convert.setup( project, nil, false, false, false)\n");
	if( rebuild )
		sprintf(msg, "convert.setup( project, nil, false, true, false)\n");
	rexMBAnimExportCommon::WriteLine(rubyfile, msg );
	rexMBAnimExportCommon::WriteLine(rubyfile, "convert.build(ind_output_node)\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "plat_output_list.each do | map_output_item |\n" );
	rexMBAnimExportCommon::WriteLine(rubyfile, "\tconvert.build(map_output_item)\n\n" );

	rexMBAnimExportCommon::WriteLine(rubyfile, "end\n" );

	rubyfile->Close();

	int result = system( ruby );
	if ( result != 0 )
	{
		char msg[RAGE_MAX_PATH];
		sprintf( msg, "ERROR: cmd '%s' returned %d.", ruby, result );
		return false;
	}
	return true;
}

//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::BuildImageUsingContentSystem( bool bRebuild, const char* pSrcDir, const char* pImageName, const char* pExportDir, bool bCutscene )
{
	atString rubyFilename;
	atString contentRootPath;
	rexMBAnimExportCommon::GetMotionBuilderContentRootPathSetting( contentRootPath );
	rubyFilename = atString( contentRootPath, "\\lib\\pipeline\\resourcing\\gateways\\image_file.rb" );

	char cToolsBin[RAGE_MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsBin, RAGE_MAX_PATH);

	char cCmdLine[RAGE_MAX_PATH * 2];
	memset(cCmdLine, 0, RAGE_MAX_PATH * 2);
	safecat( cCmdLine, cToolsBin, sizeof(cCmdLine));
	safecat( cCmdLine, "\\bin\\ruby\\bin\\ruby.exe ", sizeof(cCmdLine) );
	safecat( cCmdLine, rubyFilename.c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " --project=", sizeof(cCmdLine) );
	safecat( cCmdLine, rexMBAnimExportCommon::GetProjectName().c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " --srcdir=", sizeof(cCmdLine) );
	safecat( cCmdLine, pSrcDir, sizeof(cCmdLine) );
	safecat( cCmdLine, " --imagename=", sizeof(cCmdLine) );
	safecat( cCmdLine, pImageName, sizeof(cCmdLine) );
	safecat( cCmdLine, " --exportsrcdir=", sizeof(cCmdLine) );
	safecat( cCmdLine, pExportDir, sizeof(cCmdLine) );
	if( bRebuild )
		safecat( cCmdLine, " --rebuild" , sizeof(cCmdLine) );
	if( bCutscene )
		safecat( cCmdLine, " --cutscene" , sizeof(cCmdLine) );

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);

	int result = system( cCmdLine );
	if ( result != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
		return false;
	}
	return true;

}

bool rexMBAnimExportCommon::BuildRPF( bool bRebuild, const char* pDictName, const char* pRelativeDir )
{
	atString rubyFilename;
	atString contentRootPath;
	rexMBAnimExportCommon::GetMotionBuilderContentRootPathSetting( contentRootPath );
	rubyFilename = atString( contentRootPath, "\\lib\\pipeline\\resourcing\\gateways\\rpf_from_content.rb" );

	char cToolsBin[_MAX_PATH];
	gs_GameView.GetToolsBinDir( cToolsBin, _MAX_PATH);

	char cExportDir[_MAX_PATH];
	char cSrcFilename[_MAX_PATH];
	gs_GameView.GetExportDir( cExportDir, _MAX_PATH );
	sprintf( cSrcFilename, "%s\\anim\\ingame\\%s\\%s.icd.zip", cExportDir, pRelativeDir, pDictName );

	char cCmdLine[RAGE_MAX_PATH * 4];
	cCmdLine[0] = 0;
	safecat( cCmdLine, cToolsBin, sizeof(cCmdLine));
	safecat( cCmdLine, "\\ruby\\bin\\ruby.exe ", sizeof(cCmdLine) );
	safecat( cCmdLine, rubyFilename.c_str(), sizeof(cCmdLine) );
	if( bRebuild )
		safecat( cCmdLine, " --rebuild" , sizeof(cCmdLine) );
	safecat( cCmdLine, " ", sizeof(cCmdLine) );
	safecat( cCmdLine, cSrcFilename, sizeof(cCmdLine) );

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);

	int result = system( cCmdLine );
	if ( result != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
		return false;
	}
	return true;
}

bool rexMBAnimExportCommon::BuildAllRPF( bool bRebuild, const char* pSrcDir )
{
	atString rubyFilename;
	atString contentRootPath;
	rexMBAnimExportCommon::GetMotionBuilderContentRootPathSetting( contentRootPath );
	rubyFilename = atString( contentRootPath, "\\lib\\pipeline\\resourcing\\gateways\\all_rpf_from_content.rb" );

	char cToolsBin[_MAX_PATH];
	gs_GameView.GetToolsBinDir( cToolsBin, _MAX_PATH);

	char cExportDir[_MAX_PATH];
	char cSourceDir[_MAX_PATH];
	gs_GameView.GetExportDir( cExportDir, _MAX_PATH );
	sprintf( cSourceDir, "%s\\anim\\%s", cExportDir, pSrcDir );

	char cCmdLine[RAGE_MAX_PATH * 4];
	cCmdLine[0] = 0;
	safecat( cCmdLine, cToolsBin, sizeof(cCmdLine));
	safecat( cCmdLine, "\\ruby\\bin\\ruby.exe ", sizeof(cCmdLine) );
	safecat( cCmdLine, rubyFilename.c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " --src=", sizeof(cCmdLine) );
	safecat( cCmdLine, cSourceDir, sizeof(cCmdLine) );
	if( bRebuild )
		safecat( cCmdLine, " --rebuild" , sizeof(cCmdLine) );

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);

	int result = system( cCmdLine );
	if ( result != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
		return false;
	}
	return true;
}

// PURPOSE:	Gateway to ruby pipeline for building a zip
//			
//
bool rexMBAnimExportCommon::BuildDictionary( const char* pSrcDir, const char* pRootDir, const char* pDictName )
{
	atString animExportDir = rexMBAnimExportCommon::GetProjectOrDlcIngameExportDir(atString(""));
	animExportDir.Replace("/", "\\");

	char czZip[RAGE_MAX_PATH];
	sprintf( czZip, "%s\\%s\\%s.icd.zip", animExportDir.c_str(), pRootDir, pDictName );

	xmlDocPtr mDocument = xmlNewDoc(BAD_CAST "1.0");
	xmlNodePtr root_node = xmlNewNode(NULL, BAD_CAST "Assets");
	xmlDocSetRootElement(mDocument, root_node);

	xmlNodePtr elemZip = xmlNewChild(root_node, NULL, BAD_CAST "ZipArchive", NULL);
	xmlNewProp(elemZip, BAD_CAST "path", BAD_CAST czZip);

	atArray<atString> Files;
	ASSET.EnumFiles( pSrcDir, &CB_FindFirstFileAll, &Files );

	for(int i=0; i < Files.GetCount(); ++i)
	{
		if(stricmp(ASSET.FindExtensionInPath(Files[i].c_str()), ".anim") != 0 &&
			stricmp(ASSET.FindExtensionInPath(Files[i].c_str()), ".clip") != 0 &&
			stricmp(ASSET.FindExtensionInPath(Files[i].c_str()), ".clipxml") != 0)
			continue;

		char cFullFilePath[_MAX_PATH];
		sprintf(cFullFilePath, "%s\\%s", pSrcDir, Files[i].c_str());

		xmlNodePtr elemDir = xmlNewChild(elemZip, NULL, BAD_CAST "File", NULL);
		xmlNewProp(elemDir, BAD_CAST "path", BAD_CAST cFullFilePath);
	}

	char czXmlFile[RAGE_MAX_PATH];
	sprintf(czXmlFile, "%s\\master_icd_list.xml", animExportDir.c_str());
	
	ASSET.CreateLeadingPath(czXmlFile);

	if(ASSET.Exists(czXmlFile, ""))
		fiDeviceLocal::GetInstance().SetAttributes( czXmlFile, fiDeviceLocal::GetInstance().GetAttributes(czXmlFile) & ~FILE_ATTRIBUTE_READONLY );

	xmlSaveFormatFileEnc(czXmlFile, mDocument, "UTF-8", 1);

	atArray<atString> contentArray;
	contentArray.PushAndGrow(atString(czXmlFile));
	return rexMBAnimExportCommon::ProcessIngameZips(contentArray, true, false, true);
}

bool rexMBAnimExportCommon::BuildIngameZips( const char* pRootdir )
{
	atString rubyFilename;
	atString contentRootPath;
	rexMBAnimExportCommon::GetMotionBuilderContentRootPathSetting( contentRootPath );
	rubyFilename = atString( contentRootPath, "\\lib\\pipeline\\resourcing\\gateways\\zips_from_dir_leaves.rb" );

	char cArtDir[_MAX_PATH];
	char cSourceDir[_MAX_PATH];
	gs_GameView.GetArtDir( cArtDir, _MAX_PATH );
	sprintf(cSourceDir, "%s/anim/export_mb/%s/", cArtDir, pRootdir );

	char cExportDir[_MAX_PATH];
	char cOutputDirectory[_MAX_PATH];
	gs_GameView.GetExportDir( cExportDir, _MAX_PATH );

	sprintf( cOutputDirectory, "%s\\anim\\ingame\\%s\\", cExportDir, pRootdir );

	char cToolsBin[_MAX_PATH];
	gs_GameView.GetToolsBinDir( cToolsBin, _MAX_PATH);

	char cCmdLine[RAGE_MAX_PATH * 4];
	cCmdLine[0] = 0;
	safecat( cCmdLine, cToolsBin, sizeof(cCmdLine));
	safecat( cCmdLine, "\\ruby\\bin\\ruby.exe ", sizeof(cCmdLine) );
	safecat( cCmdLine, rubyFilename.c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " --srcdir=", sizeof(cCmdLine) );
	safecat( cCmdLine, cSourceDir, sizeof(cCmdLine) );
	safecat( cCmdLine, " --dstdir=", sizeof(cCmdLine) );
	safecat( cCmdLine, cOutputDirectory, sizeof(cCmdLine) );
	safecat( cCmdLine, " --ext=icd.zip", sizeof(cCmdLine) );

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);

	int result = system( cCmdLine );
	if ( result != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
		return false;
	}
	return true;
}

bool rexMBAnimExportCommon::BuildAllIngameZips( )
{
	atString rubyFilename;
	atString contentRootPath;
	rexMBAnimExportCommon::GetMotionBuilderContentRootPathSetting( contentRootPath );
	rubyFilename = atString( contentRootPath, "\\lib\\pipeline\\resourcing\\gateways\\all_zips_from_dir_leaves.rb" );

	char cArtDir[_MAX_PATH];
	char cSourceDir[_MAX_PATH];
	gs_GameView.GetArtDir( cArtDir, _MAX_PATH );
	sprintf(cSourceDir, "%s/anim/export_mb/", cArtDir );

	char cExportDir[_MAX_PATH];
	char cOutputDirectory[_MAX_PATH];
	gs_GameView.GetExportDir( cExportDir, _MAX_PATH );
	sprintf( cOutputDirectory, "%s\\anim\\ingame\\", cExportDir );

	char cToolsBin[_MAX_PATH];
	gs_GameView.GetToolsBinDir( cToolsBin, _MAX_PATH);

	char cCmdLine[RAGE_MAX_PATH * 4];
	cCmdLine[0] = 0;
	safecat( cCmdLine, cToolsBin, sizeof(cCmdLine));
	safecat( cCmdLine, "\\ruby\\bin\\ruby.exe ", sizeof(cCmdLine) );
	safecat( cCmdLine, rubyFilename.c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " --src=", sizeof(cCmdLine) );
	safecat( cCmdLine, cSourceDir, sizeof(cCmdLine) );
	safecat( cCmdLine, " --dst=", sizeof(cCmdLine) );
	safecat( cCmdLine, cOutputDirectory, sizeof(cCmdLine) );
	safecat( cCmdLine, " --ext=icd.zip", sizeof(cCmdLine) );

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);

	int result = system( cCmdLine );
	if ( result != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
		return false;
	}
	return true;
}

bool rexMBAnimExportCommon::PreviewDictionary( const char* pDictName, const char* pRootDir )
{
	atString convertExe;
	char cToolsRootPath[RAGE_MAX_PATH];

	gs_GameView.GetToolsRootDir( cToolsRootPath, sizeof(cToolsRootPath) );

	convertExe = atString( atString(cToolsRootPath), "\\ironlib\\lib\\RSG.Pipeline.Convert.exe" );

	char exportDir[_MAX_PATH];
	char inputFilename[_MAX_PATH];
	char masterPath[_MAX_PATH];
	gs_GameView.GetExportDir( exportDir, _MAX_PATH );
	sprintf( masterPath, "%s\\anim\\ingame\\", exportDir );
	sprintf( inputFilename, "%s\\%s\\%s.icd.zip", masterPath, pRootDir, pDictName );

	char cCmdLine[RAGE_MAX_PATH * 4];
	cCmdLine[0] = 0;
	safecat( cCmdLine, convertExe.c_str(), sizeof(cCmdLine));
	safecat( cCmdLine, " --branch=dev ", sizeof(cCmdLine));
	safecat( cCmdLine, inputFilename, sizeof(cCmdLine));

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);
	int result = system( cCmdLine );
	if ( result != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
		return false;
	}
	return true;
}

float rexMBAnimExportCommon::GetStoryTrackStartTime()
{	
	rexMBAnimExportCommon::SetBaseLayerOnAllTakes();

	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			char cName[RAGE_MAX_PATH];
			sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", rexMBAnimExportCommon::GetActivePart());

			if(strcmpi(pTrackName, cName) == 0)
			{
				if(lTrack->Clips.GetCount())
				{
					HFBStoryClip lStartClip = lTrack->Clips[0];
					return (float)atoi(lStartClip->Start.AsString()) / rexMBAnimExportCommon::GetFPS();
				}
			}
		}
	}

	return -1;
}

float rexMBAnimExportCommon::GetStoryTrackEndTime()
{	
	rexMBAnimExportCommon::SetBaseLayerOnAllTakes();

	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			char cName[RAGE_MAX_PATH];
			sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", rexMBAnimExportCommon::GetActivePart());

			if(strcmpi(pTrackName, cName) == 0)
			{
				if(lTrack->Clips.GetCount())
				{
					HFBStoryClip lEndClip = lTrack->Clips[lTrack->Clips.GetCount()-1];
					return (float)atoi(lEndClip->Stop.AsString()) / rexMBAnimExportCommon::GetFPS();
				}
			}
		}
	}

	return -1;
}

int rexMBAnimExportCommon::GetStoryTrackRealRangeEnd()
{
	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			char cName[RAGE_MAX_PATH];
			sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", rexMBAnimExportCommon::GetActivePart());

			if(strcmpi(pTrackName, cName) != 0) continue; // We only care about the items EST

			if(lTrack->Clips.GetCount() > 0)
			{
				HFBStoryClip lClip = lTrack->Clips[lTrack->Clips.GetCount()-1];
				return atoi(lClip->PropertyList.Find("LastLoopMarkOut")->AsString());
			}
		}
	}

	return -1;
}

void rexMBAnimExportCommon::CalculateDiscardedFrames(bool bStoryCheck)
{
	s_discardedFrameRanges.Reset();

	if( bStoryCheck && !rexMBAnimExportCommon::GetStoryMode() ) return;

	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			char cName[RAGE_MAX_PATH];
			sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", rexMBAnimExportCommon::GetActivePart());

			if(strcmpi(pTrackName, cName) != 0) continue; // We only care about the items EST

			//if(lTrack->Mute == true) continue;

			ULOGGER_MOBO.SetProgressMessage("Story Mode Enabled.");

			int iPreviousFirstLoop = -1;
			int iPreviousLastLoop = -1;

			for (int j = 0; j < lTrack->Clips.GetCount(); ++j)
			{
				HFBStoryClip lClip = lTrack->Clips[j];
				
				int iFirstLoop = atoi(lClip->PropertyList.Find("FirstLoopMarkIn")->AsString());
				int iLastLoop = atoi(lClip->PropertyList.Find("LastLoopMarkOut")->AsString());

				if(iFirstLoop != iPreviousLastLoop && iPreviousLastLoop != -1) // store frame ranges to remove
				{
					if(iPreviousLastLoop > iFirstLoop) 
					{
						continue;
					}

					s_discardedFrameRanges.PushAndGrow( iPreviousLastLoop );
					s_discardedFrameRanges.PushAndGrow( iFirstLoop);

					ULOGGER_MOBO.SetProgressMessage("WARNING: Discarded animation range %d - %d.", iPreviousLastLoop, iFirstLoop);
				}

				iPreviousFirstLoop = iFirstLoop;
				iPreviousLastLoop =  iLastLoop;
			}
		}
	}
}

bool rexMBAnimExportCommon::DiscardFrame(int iFrame)
{
	for(int i=0; i < s_discardedFrameRanges.GetCount(); i+=2)
	{
		if(iFrame >= s_discardedFrameRanges[i] && iFrame < s_discardedFrameRanges[i+1]) // discard if frame is within a discard range
		{
			return true;
		}
	}

	return false;
}

int rexMBAnimExportCommon::GetFrameAfterImport(int iFrame, const atArray<int>& atDiscard)
{
	for(int i=0; i < atDiscard.GetCount(); i+=2)
	{
		if(iFrame > atDiscard[i])
		{
			iFrame += atDiscard[i+1]-atDiscard[i];
		}
	}

	return iFrame;
}

atArray<int>& rexMBAnimExportCommon::GetDiscardedFramesArray()
{
	return s_discardedFrameRanges;
}

int rexMBAnimExportCommon::GetNumberOfDiscardedFrames()
{
	int iDiscardedFrames = 0;

	for(int i=0; i < s_discardedFrameRanges.GetCount(); i+=2)
	{
		iDiscardedFrames += (s_discardedFrameRanges[i+1] - s_discardedFrameRanges[i]);
	}

	return iDiscardedFrames;
}

int rexMBAnimExportCommon::GetFrameAfterDiscardedFrames(const int iCurrentFrame)
{
	int iNewFrame = iCurrentFrame;

	for(int i=s_discardedFrameRanges.GetCount()-1; i != -1 ; i-=2)
	{
		if(iCurrentFrame >= s_discardedFrameRanges[i])
		{
			iNewFrame -= (s_discardedFrameRanges[i]-s_discardedFrameRanges[i-1]);
		}
	}

	return iNewFrame;
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::SetBaseLayerOnAllTakes()
{
	// any anim plotted on layers is auto collapsed to the base layer, so we auto select this layer when exporting

	FBSystem system;
	int nTakes = system.Scene->Takes.GetCount();
	for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
	{
		HFBTake hTake = system.Scene->Takes[takeIdx];
		if(hTake->GetLayerCount() > 0)
		{
			hTake->SetCurrentLayer(0);
		}
	}
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::GetDiscardFrameArrayAsString( atString& strDiscardedFrames )
{
	strDiscardedFrames.Reset();

	for(int i=0; i < s_discardedFrameRanges.GetCount(); i+=2)
	{
		char czEntry[RAGE_MAX_PATH];
		sprintf(czEntry, "%d,%d", s_discardedFrameRanges[i],s_discardedFrameRanges[i+1]);
		strDiscardedFrames += czEntry;
		
		if(i < s_discardedFrameRanges.GetCount()-2)
		{
			strDiscardedFrames += ",";
		}
	}
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::SetStoryMode(bool bStoryMode)
{
	s_useStoryMode = bStoryMode;
}

bool rexMBAnimExportCommon::GetStoryMode()
{
	return s_useStoryMode;
}

void rexMBAnimExportCommon::SetUsePerforceIntegration(bool bP4)
{
	s_usePerforceIntegration = bP4;
}

bool rexMBAnimExportCommon::GetPerforceIntegration()
{
	return s_usePerforceIntegration;
}

void rexMBAnimExportCommon::SetUseClipXml(bool bClip)
{
	s_useClipXml = bClip;
}

bool rexMBAnimExportCommon::GetUseClipXml()
{
	return s_useClipXml;
}

void rexMBAnimExportCommon::SetUseWarnIfCheckedOut(bool bWarn)
{
	s_useWarnIfCheckedOut = bWarn;
}

bool rexMBAnimExportCommon::GetUseWarnIfCheckedOut()
{
	return s_useWarnIfCheckedOut;
}

int rexMBAnimExportCommon::GetExporterMode()
{
	return s_exporterMode;
}

void rexMBAnimExportCommon::SetExporterMode(int iMode)
{
	s_exporterMode = iMode;
}

const char* rexMBAnimExportCommon::GetFBXFileName()
{
	FBApplication app;
	const char* fbxFileName = app.FBXFileName.AsString();

	char* pDest = rage_new char[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(pDest, RAGE_MAX_PATH, ASSET.FileName(fbxFileName));

	return pDest;
}

bool rexMBAnimExportCommon::IsLightFile(const char* pExportDir, const char* pFileName)
{
	char cLightFile[MAX_PATH];
	sprintf( cLightFile, "%s/%s", pExportDir, "data.lightxml" );

	cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();

	if (pCutfile->LoadFile(cLightFile) )
	{
		const atArray<cutfObject *>& objectList = pCutfile->GetObjectList();

		for(int i=0; i < objectList.GetCount(); ++i)
		{
			cutfLightObject* pLightObject = dynamic_cast<cutfLightObject*>(objectList[i]);
			if(pLightObject != NULL)
			{
				if(stricmp(pLightObject->GetName().GetCStr(), pFileName) == 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	return false;
}

void rexMBAnimExportCommon::SetNewOrigin(bool bOrigin, atString atOrigin)
{
	s_useNewOrigin = false;

	if(stricmp(atOrigin.c_str(), "") != 0)
	{
		s_originObject = atOrigin;
		s_useNewOrigin = bOrigin;
	}
}

bool rexMBAnimExportCommon::GetNewOrigin(atString& atOrigin)
{
	atOrigin = s_originObject;
	return s_useNewOrigin;
}

void rexMBAnimExportCommon::SetActivePart(const atString& atActivePart)
{
	s_activePart = atActivePart;
}

const char* rexMBAnimExportCommon::GetActivePart()
{
	return s_activePart.c_str();
}

bool rexMBAnimExportCommon::IsRagEvent(int iEventId)
{
	if(iEventId == CUTSCENE_SET_VARIATION_EVENT ||
#if HACK_GTA4
		iEventId == CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE ||
		iEventId == CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE+1 ||
		iEventId == CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE+2 ||
		iEventId == CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE+3 ||
		iEventId == CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE+4 ||
		iEventId == CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE+5 ||
		iEventId == CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE+6 ||
		iEventId == CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE+7 ||
#endif
		iEventId == CUTSCENE_HIDE_OBJECTS_EVENT ||
		iEventId == CUTSCENE_FIXUP_OBJECTS_EVENT ||
		iEventId == CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT ||
		iEventId == CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT ||
		iEventId == CUTSCENE_SHOW_OBJECTS_EVENT /*||
		
		iEventId == CUTSCENE_ENABLE_CASCADE_SHADOW_BOUNDS_EVENT ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER || 
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_UPDATE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_AIRCRAFT_MODE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_FLY_CAMERA_MODE || 
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_MINMAX ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_MINMAX ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_DEPTH_BIAS ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_SLOPE_BIAS ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE ||
		iEventId == CUTSCENE_CASCADE_SHADOWS_RESET_CASCADE_SHADOWS*/)
	{
		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::IsFileUnderProjectRoot(const char* pPath)
{
	atString atProjectRoot = GetProjectOrDlcRootDir(atString(pPath));
	atProjectRoot.Replace("/","\\");
	atProjectRoot.Lowercase();

	atString atPath(pPath);
	atPath.Replace("/", "\\");
	atPath.Lowercase();

	return atPath.StartsWith(atProjectRoot.c_str());
}
//-----------------------------------------------------------------------------
bool rexMBAnimExportCommon::IsFileUnderArtRoot(const char* pPath)
{
	atString atArtRoot = GetProjectOrDlcArtDir(atString(pPath));
	atArtRoot.Replace("/","\\");
	atArtRoot.Lowercase();

	atString atPath(pPath);
	atPath.Replace("/", "\\");
	atPath.Lowercase();

	bool bResult = atPath.StartsWith(atArtRoot.c_str());
	if(!bResult)
		ULOGGER_MOBO.SetProgressMessage("ERROR: Path '%s' is not under art root '%s'. If DLC, have you synced root xml files?", pPath, atArtRoot.c_str());

	return bResult;
}
//-----------------------------------------------------------------------------
enum eSkelType {
	SKEL_TYPE_MALE,
	SKEL_TYPE_FEMALE,
	SKEL_TYPE_PLAYER
};

int rexMBAnimExportCommon::ResolveSkeletonType( const char* pSkelFile )
{
	// This will eventually be data driven
	if (strcmp(pSkelFile, "player.skel") == 0)
		return SKEL_TYPE_PLAYER;
	else if (strcmp(pSkelFile, "female.skel") == 0)
		return SKEL_TYPE_FEMALE;
	else if (strcmp(pSkelFile, "male.skel") == 0)
		return SKEL_TYPE_MALE;
	else 
		return -1;
}

atString rexMBAnimExportCommon::GetLocalComputerName() 
{  
	TCHAR chrComputerName[MAX_COMPUTERNAME_LENGTH + 1];  
	string strRetVal;  
	DWORD dwBufferSize = MAX_COMPUTERNAME_LENGTH + 1;  

	if(GetComputerName(chrComputerName,&dwBufferSize)) {  
		// We got the name, set the return value.  
		strRetVal = chrComputerName;  
	} else {  
		// Failed to get the name, call GetLastError here to get  
		// the error code.  
		strRetVal = "";  
	}  

	return(atString(strRetVal.c_str()));  
}

atString rexMBAnimExportCommon::GetLocalUserName()
{
	TCHAR chrUserName[RAGE_MAX_PATH];
	string strRetVal; 
	if(GetEnvironmentVariable("USERNAME", chrUserName, RAGE_MAX_PATH))
	{
		strRetVal = chrUserName;
	}
	else
	{
		strRetVal = "";
	}

	return atString(chrUserName);
}

bool rexMBAnimExportCommon::GetEnvironmentVariable(const char* envVar, char* envValue, int size)
{
	if ( sysGetEnv( envVar, envValue, size ) == false )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to find %s environment variable.", envVar);
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------

int rexMBAnimExportCommon::GetFrameFromTime(float fTime)
{
	return (int)floor((fTime * rexMBAnimExportCommon::GetFPS()) + 0.5);
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::ClearLogPathContents(const char *pLogSubDir )
{
	char cToolsRoot[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT",cToolsRoot, RAGE_MAX_PATH);

	char cLogPath[RAGE_MAX_PATH];
	sprintf(cLogPath, "%s\\logs\\%s", cToolsRoot, pLogSubDir);

	atArray<atString> logFiles;
	ASSET.EnumFiles( cLogPath, &CB_FindFirstFileAll, &logFiles );

	for(int i=0; i < logFiles.GetCount(); ++i)
	{
		char cFullLogPath[RAGE_MAX_PATH];
		sprintf(cFullLogPath, "%s\\%s", cLogPath, logFiles[i].c_str());
		DeleteFile(cFullLogPath);
	}
}

//-----------------------------------------------------------------------------

bool rexMBAnimExportCommon::ValidateAndCreateStoryTrack(cutfCutsceneFile2* pCutFile, bool /*bCreate*/, int iStartFrame, int iEndFrame, const char* pSceneName)
{
	if(!rexMBAnimExportCommon::GetStoryMode()) return true;

	rexMBAnimExportCommon::SetBaseLayerOnAllTakes();

	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;

	bool bCreateTrack=true;
	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			char cName[RAGE_MAX_PATH];
			sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", rexMBAnimExportCommon::GetActivePart());

			if(strcmpi(pTrackName, cName) == 0)
			{
				bCreateTrack=false;

				if(lTrack->Clips.GetCount())
				{
					HFBStoryClip lStartClip = lTrack->Clips[0];
					int iStart = atoi(lStartClip->Start.AsString());

					if(iStart != iStartFrame)
					{
						if(rexMBAnimExportCommon::GetExporterMode() == CUTSCENE_EXPORTER)
						{
							ULOGGER_MOBO.SetProgressMessage("WARNING: Story track range updated to match %s '%d to %d'.", cName, pCutFile->GetRangeStart(), iStart );
							pCutFile->SetRangeStart(iStart);
						}
						else
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: Story track / range mismatch on first clip '%d/%d'.", iStart, (int)iStartFrame );
							return false;
						}
					}

					HFBStoryClip lEndClip = lTrack->Clips[lTrack->Clips.GetCount()-1];
					int iEnd = atoi(lEndClip->Stop.AsString());

					if(iEnd != iEndFrame)
					{
						if(rexMBAnimExportCommon::GetExporterMode() == CUTSCENE_EXPORTER)
						{
							ULOGGER_MOBO.SetProgressMessage("WARNING: Story track range updated to match %s '%d to %d'.", cName, pCutFile->GetRangeEnd(), iEnd-1 );
							pCutFile->SetRangeEnd(iEnd-1);
						}
						else
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: Story track / range incorrect on last clip '%d/%d'. Range end should be '%d'.", iEnd, (int)iEndFrame-1, iEnd-1 );
							return false;
						}
					}

					int iShotStart = atoi(lStartClip->PropertyList.Find("FirstLoopMarkIn")->AsString());

					if(iStart != iShotStart)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Story clip start points are not equal '%d/%d'. They must be equal for correct animation and discarded frame exporting.", iStart, iShotStart);
						return false;
					}
				}

				// Validate the clips
				for (int j = 0; j < lTrack->Clips.GetCount(); j++)
				{
					HFBStoryClip lClip = lTrack->Clips[j];

					atString strClipName(lClip->Name.AsString());

					if(lClip->ShotCamera == NULL)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Story track clip '%s', camera is NULL.", strClipName.c_str() );
						return false;
					}

					if(strClipName[0] == 'C' &&
						strClipName[1] == 'l' &&
						strClipName[2] == 'i' &&
						strClipName[3] == 'p' &&
						strClipName[4] == ' ')
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Story track clips cannot be named default. For example 'Clip #'");
						return false;
					}
				}

				// This is just to rename the clips, once we have good data we can remove this
				//-----------------------------------------------------------------------------------
				for (int j = 0; j < lTrack->Clips.GetCount(); j++)
				{
					HFBStoryClip lClip = lTrack->Clips[j];

					atString strClipName(lClip->Name.AsString());

					if(strClipName[0] == 'A' &&
						strClipName[1] == 'u' &&
						strClipName[2] == 't' &&
						strClipName[3] == 'o' &&
						strClipName[4] == 'C' &&
						strClipName[5] == 'l' &&
						strClipName[6] == 'i' &&
						strClipName[7] == 'p')
					{
						int iIndex = -1;
						iIndex = strClipName.IndexOf("_");
						if(iIndex != -1)
						{
							strClipName[iIndex] = 0;
							char cClipName[MAX_PATH];
							sprintf( cClipName, "%s_%s", strClipName.c_str(), pSceneName );

							lClip->Name = cClipName;
							continue;
						}

						char cClipName[MAX_PATH];
						sprintf( cClipName, "%s_%s", strClipName.c_str(), pSceneName );
						lClip->Name = cClipName;
					}
				}
				//-----------------------------------------------------------------------------------

				// Check the last clip, we check its shot vs local stop times. This tells us if we have any discarded frames. This needs to
				// match our calculation or we will get a crash
				/*HFBStoryClip lClip = lTrack->Clips[lTrack->Clips.GetCount()-1];
				if(lClip)
				{
					int iDiscardedFrames = GetCutsceneFile().GetAltRangeEnd() - GetCutsceneFile().GetRangeEnd();

					char* pActionStop = lClip->ShotActionStop.AsString();
					int iActionStop = atoi(pActionStop);

					char* pStop = lClip->Stop.AsString();
					int iStop = atoi(pStop);

					if((iActionStop - iStop) && (iActionStop - iStop) != iDiscardedFrames)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Discarded frame numbers don't match, %d (calculated) vs %d (motionbuilder)", iDiscardedFrames, (iActionStop - iStop));
						return false;
					}
				}*/

				return true;
			}
		}
	}

	ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to find shot track '%s_EST'", rexMBAnimExportCommon::GetActivePart());
	return false;

	// No longer used
	//if(bCreateTrack && bCreate)
	//{
	//	HFBStoryTrack pTrack = new FBStoryTrack(kFBStoryTrackShot, lStory.RootEditFolder);
	//	if(pTrack)
	//	{
	//		char cName[RAGE_MAX_PATH];
	//		sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", rexMBAnimExportCommon::GetActivePart());

	//		pTrack->Name = cName;

	//		ULOGGER_MOBO.SetProgressMessage("Edited Shot Track created");

	//		FBPlayerControl().GotoEnd(); 
	//		FBTime fbEndTime = (FBTime)FBSystem().LocalTime;
	//		FBPlayerControl().GotoStart();

	//		// Create track clips from switcher
	//		HFBModel pCameraSwitcher = RAGEFindModelByName( "Camera Switcher" );
	//		if ( pCameraSwitcher )
	//		{
	//			HFBAnimationNode pSwitcherAnimNode = pCameraSwitcher->AnimationNodeInGet();
	//			if ( pSwitcherAnimNode )
	//			{
	//				HFBAnimationNode pAnimNode = rexMBAnimExportCommon::FindAnimByName( pSwitcherAnimNode, "Camera Index" );
	//				if ( pAnimNode )
	//				{
	//					for ( int i = 0; i < pAnimNode->FCurve->Keys.GetCount(); ++i )
	//					{
	//						HFBCamera pCamera = FBSystem().Scene->Cameras[(int)pAnimNode->FCurve->Keys[i].Value + 6];
	//						if(pCamera)
	//						{
	//							FBTime fbTimeCheck = (FBTime)pAnimNode->FCurve->Keys[i].Time;
	//							if(fbTimeCheck > fbEndTime) continue;

	//							HFBStoryClip pClip = new FBStoryClip(pCamera, pTrack, (FBTime)pAnimNode->FCurve->Keys[i].Time);
	//							if(pClip)
	//							{
	//								char cClipName[MAX_PATH];
	//								sprintf( cClipName, "AutoClip%d_%s", i+1, pSceneName );

	//								pClip->Name = cClipName;
	//								
	//								if(i != pAnimNode->FCurve->Keys.GetCount()-1)
	//								{
	//									pClip->ShotActionStop = (FBTime)pAnimNode->FCurve->Keys[i+1].Time;
	//									pClip->Stop = (FBTime)pAnimNode->FCurve->Keys[i+1].Time;
	//								}
	//								else
	//								{
	//									pClip->ShotActionStop = fbEndTime;
	//									pClip->Stop = fbEndTime;
	//								}
	//							}
	//						}
	//					}
	//				}
	//			}
	//		}
	//		
	//		return ValidateAndCreateStoryTrack(false, iStartFrame, iEndFrame, pSceneName);
	//	}
	//}

	//return true;
}

void rexMBAnimExportCommon::GetCameraCutTimes( atArray<float> &cameraCutList )
{
	atArray<SCameraCutKeyData> cameraCutKeyData;

	if(!GetCameraCutShots( cameraCutKeyData ))
	{
		GetCameraCutKeys( cameraCutKeyData );
	}

	cameraCutList.Reset();
	cameraCutList.Reserve( cameraCutKeyData.GetCount() );

	for ( int i = 0; i < cameraCutKeyData.GetCount(); ++i )
	{
		cameraCutList.Append() = cameraCutKeyData[i].fTime;
	}
}

void rexMBAnimExportCommon::GetCameraCutKeys( atArray<SCameraCutKeyData> &cameraCutKeyDataList )
{
	cameraCutKeyDataList.Reset();

	HFBModel pCameraSwitcher = RAGEFindModelByName( "Camera Switcher" );
	if ( pCameraSwitcher )
	{
		HFBAnimationNode pSwitcherAnimNode = pCameraSwitcher->AnimationNodeInGet();
		if ( pSwitcherAnimNode )
		{
			HFBAnimationNode pAnimNode = rexMBAnimExportCommon::FindAnimByName( pSwitcherAnimNode, "Camera Index" );
			if ( pAnimNode )
			{
				for ( int i = 0; i < pAnimNode->FCurve->Keys.GetCount(); ++i )
				{
					SCameraCutKeyData cameraCutKeyData;

					// Don't use the the utility function, we want something more precise.
					// float fSeconds = rexMBAnimExportCommon::GetSeconds( pAnimNode->FCurve->Keys[i].Time );
					FBTime time = (FBTime)pAnimNode->FCurve->Keys[i].Time;
					cameraCutKeyData.fTime = (float)time.GetSecondDouble();                    
					cameraCutKeyData.iCameraIndex = (int)pAnimNode->FCurve->Keys[i].Value;

					FBSystem system;
					HFBCamera pCamera = system.Scene->Cameras[cameraCutKeyData.iCameraIndex + 6];    // add 6 to skip past the built-in cameras
					safecpy( cameraCutKeyData.cName, pCamera->Name.AsString(), sizeof(cameraCutKeyData.cName) );

					cameraCutKeyDataList.PushAndGrow( cameraCutKeyData );
				}
			}
		}
	}

	// Validation
	int iLastZero = -1;
	for ( int i = 0; i < cameraCutKeyDataList.GetCount(); ++i )
	{
		if ( cameraCutKeyDataList[i].fTime < 0.0f )
		{
			// If we're less than zero, set to 0.
			cameraCutKeyDataList[i].fTime = 0.0f;
		}

		if ( cameraCutKeyDataList[i].fTime == 0.0f )
		{
			iLastZero = i;
		}
	}

	// Clean up so we have only 1 key at time 0.
	--iLastZero;
	while ( iLastZero >= 0 )
	{
		cameraCutKeyDataList.Delete( 0 );
		--iLastZero;
	}
}

bool rexMBAnimExportCommon::GetCameraCutShots( atArray<SCameraCutKeyData> &cameraCutKeyDataList )
{
	if(!rexMBAnimExportCommon::GetStoryMode()) return false;

	cameraCutKeyDataList.Reset();

	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;
	bool bResult = false;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			char cName[RAGE_MAX_PATH];
			sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", rexMBAnimExportCommon::GetActivePart());

			if(strcmpi(pTrackName, cName) != 0) continue; // We only care about the Edited Shot Track

			bResult = true;

			for (int j = 0; j < lTrack->Clips.GetCount(); j++)
			{
				HFBStoryClip lClip = lTrack->Clips[j];

				char* pStart = (char*)lClip->Start.AsString();
				int iFrame = atoi(pStart);

				SCameraCutKeyData cameraCutKeyData;

				cameraCutKeyData.fTime = (float)iFrame/30.f;                    

				FBCamera* pShotCamera = lClip->ShotCamera;
				if(pShotCamera)
				{
					FBSystem system;
					for(int index = 0; index < system.Scene->Cameras.GetCount(); index++ )
					{
						FBCamera* pCamera = system.Scene->Cameras[index];
						if(stricmp(pCamera->Name.AsString(),pShotCamera->Name.AsString()) == 0)
						{
							cameraCutKeyData.iCameraIndex = index-6; // remove inbuilt cameras
							safecpy( cameraCutKeyData.cName, lClip->Name.AsString(), sizeof(cameraCutKeyData.cName) );
						}
					}

					cameraCutKeyDataList.PushAndGrow( cameraCutKeyData );
				}
			}
		}
	}

	// Validation
	int iLastZero = -1;
	for ( int i = 0; i < cameraCutKeyDataList.GetCount(); ++i )
	{
		if ( cameraCutKeyDataList[i].fTime < 0.0f )
		{
			// If we're less than zero, set to 0.
			cameraCutKeyDataList[i].fTime = 0.0f;
		}

		if ( cameraCutKeyDataList[i].fTime == 0.0f )
		{
			iLastZero = i;
		}
	}

	// Clean up so we have only 1 key at time 0.
	--iLastZero;
	while ( iLastZero >= 0 )
	{
		cameraCutKeyDataList.Delete( 0 );
		--iLastZero;
	}

	return bResult;
}

void rexMBAnimExportCommon::GetDlcProjects(atMap<atString, atString>& atProjects)
{
	atProjects.Reset();

	for(int i=0; i < gs_GameView.GetNumDlcProjects(); ++i)
	{
		char cDlcName[RAGE_MAX_PATH];
		gs_GameView.GetDlcProjectName(i, cDlcName, RAGE_MAX_PATH);

		char cDlcRoot[RAGE_MAX_PATH];
		gs_GameView.GetDlcProjectRoot(i, cDlcRoot, RAGE_MAX_PATH);

		atProjects.Insert(atString(cDlcName), atString(cDlcRoot));
	}
}

atString rexMBAnimExportCommon::GetProjectOrDlcRootDir(atString strSource)
{
	if(strSource == "")
	{
		FBApplication application;
		const char* pFullPath = (const char*)application.FBXFileName;

		strSource = pFullPath;
		strSource.Replace("/", "\\");
	}

	atMap<atString, atString> atDlcProjects;
	GetDlcProjects(atDlcProjects);

	atArray<atString> atFbxPathEntries;
	strSource.Replace("/", "\\");
	strSource.Split(atFbxPathEntries, "\\");

	atMap<atString, atString>::Iterator entry = atDlcProjects.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		for(int j=0; j < atFbxPathEntries.GetCount(); ++j)
		{
			if(stricmp(entry.GetKey().c_str(), atFbxPathEntries[j].c_str()) == 0)
			{
				atString s = entry.GetData();
				s.Replace("$(name)", entry.GetKey());
				return s;
			}
		}
	}

	TCHAR projectRoot[_MAX_PATH]  = {0};
	gs_GameView.GetProjectRootDir(projectRoot, _MAX_PATH);

	return atString(projectRoot);
}

atString rexMBAnimExportCommon::GetProjectOrDlcCutsceneExportDir(atString strSource)
{
	return atVarString("%s/anim/cutscene/", GetProjectOrDlcExportDir(strSource).c_str());
}

atString rexMBAnimExportCommon::GetProjectOrDlcIngameExportDir(atString strSource)
{
	return atVarString("%s/anim/ingame/", GetProjectOrDlcExportDir(strSource).c_str());
}

bool rexMBAnimExportCommon::IsDlc(atString strSource)
{
	if(strSource == "")
	{
		FBApplication application;
		const char* pFullPath = (const char*)application.FBXFileName;

		strSource = pFullPath;
		strSource.Replace("/", "\\");
	}

	atMap<atString, atString> atDlcProjects;
	GetDlcProjects(atDlcProjects);

	atArray<atString> atFbxPathEntries;
	strSource.Replace("/", "\\");
	strSource.Split(atFbxPathEntries, "\\");

	atMap<atString, atString>::Iterator entry = atDlcProjects.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		for(int j=0; j < atFbxPathEntries.GetCount(); ++j)
		{
			if(stricmp(entry.GetKey().c_str(), atFbxPathEntries[j].c_str()) == 0)
			{
				return true;
			}
		}
	}

	return false;
}

atString rexMBAnimExportCommon::GetProjectOrDlcExportDir(atString strSource)
{
	if(strSource == "")
	{
		FBApplication application;
		const char* pFullPath = (const char*)application.FBXFileName;

		strSource = pFullPath;
		strSource.Replace("/", "\\");
	}

	atMap<atString, atString> atDlcProjects;
	GetDlcProjects(atDlcProjects);

	atArray<atString> atFbxPathEntries;
	strSource.Replace("/", "\\");
	strSource.Split(atFbxPathEntries, "\\");

	atMap<atString, atString>::Iterator entry = atDlcProjects.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		for(int j=0; j < atFbxPathEntries.GetCount(); ++j)
		{
			if(stricmp(entry.GetKey().c_str(), atFbxPathEntries[j].c_str()) == 0)
			{
				atString strDlcName(entry.GetKey());
				strDlcName.Lowercase();

				TCHAR exportPath[MAX_PATH];
				if(gs_GameView.GetDlcProjectExport(strDlcName.c_str(), exportPath, MAX_PATH))
				{
					atString atExport(exportPath);
					atExport.Replace("$(root)", entry.GetData());
					atExport.Replace("$(name)",entry.GetKey() );
					atExport.Replace("$(assets)",GetProjectOrDlcAssetsDir(strSource).c_str());
					return atExport;
				}
			}
		}
	}

	return rexMBAnimExportCommon::GetExportDir();
}

atString rexMBAnimExportCommon::GetProjectOrDlcAssetsDir(atString strSource)
{
	if(strSource == "")
	{
		FBApplication application;
		const char* pFullPath = (const char*)application.FBXFileName;

		strSource = pFullPath;
		strSource.Replace("/", "\\");
	}

	atMap<atString, atString> atDlcProjects;
	GetDlcProjects(atDlcProjects);

	atArray<atString> atFbxPathEntries;
	strSource.Replace("/", "\\");
	strSource.Split(atFbxPathEntries, "\\");

	atMap<atString, atString>::Iterator entry = atDlcProjects.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		for(int j=0; j < atFbxPathEntries.GetCount(); ++j)
		{
			if(stricmp(entry.GetKey().c_str(), atFbxPathEntries[j].c_str()) == 0)
			{
				atString strDlcName(entry.GetKey());
				strDlcName.Lowercase();

				TCHAR assets[MAX_PATH];
				if(gs_GameView.GetDlcProjectAssets(strDlcName.c_str(), assets, MAX_PATH))
				{
					atString atAssets(assets);
					atAssets.Replace("$(root)", entry.GetData());
					atAssets.Replace("$(name)",entry.GetKey() );
					return atAssets;
				}
			}
		}
	}

	return rexMBAnimExportCommon::GetAssetsDir();
}

atString rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString strSource)
{
	if(strSource == "")
	{
		FBApplication application;
		const char* pFullPath = (const char*)application.FBXFileName;

		strSource = pFullPath;
		strSource.Replace("/", "\\");
	}

	atMap<atString, atString> atDlcProjects;
	GetDlcProjects(atDlcProjects);

	atArray<atString> atFbxPathEntries;
	strSource.Replace("/", "\\");
	strSource.Split(atFbxPathEntries, "\\");

	atMap<atString, atString>::Iterator entry = atDlcProjects.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		for(int j=0; j < atFbxPathEntries.GetCount(); ++j)
		{
			if(stricmp(entry.GetKey().c_str(), atFbxPathEntries[j].c_str()) == 0)
			{
				atString strDlcName(entry.GetKey());
				strDlcName.Lowercase();

				TCHAR art[MAX_PATH];
				if(gs_GameView.GetDlcProjectArt(strDlcName.c_str(), art, MAX_PATH))
				{
					atString atArt(art);
					atArt.Replace("$(root)", entry.GetData());
					atArt.Replace("$(name)",entry.GetKey() );
					return atArt;
				}
			}
		}
	}

	return rexMBAnimExportCommon::GetArtDir();
}

void rexMBAnimExportCommon::GetProjectOrDlcIngameSourceExportDir(atArray<atString>& rootAssetPaths, atString strSource)
{
	if(IsDlc(strSource))
	{
		rootAssetPaths.PushAndGrow(atVarString("%s/anim/export_mb/", GetProjectOrDlcArtDir(strSource).c_str()));
	}
	else
	{
		rexMBAnimExportCommon::GetMotionBuilderRootAssetPathSetting( rootAssetPaths );
	}
}

bool rexMBAnimExportCommon::CheckWritableRPF()
{
	// Get the targets from the local.xml
	atArray<atString> atTargets;
	rexMBAnimExportCommon::ReadTargets(atTargets);

	FBApplication application;
	char* pFullPath = (char*)application.FBXFileName.AsString();

	atString outputPath(pFullPath);
	outputPath.Replace("/", "\\");
	atArray<atString> outputPathTokens;
	outputPath.Split( outputPathTokens, "\\", true );

	atString strProjectRoot = rexMBAnimExportCommon::GetProjectOrDlcRootDir(outputPath);
	strProjectRoot.Replace("/", "\\");

	char cScenePath[RAGE_MAX_PATH];
	sprintf(cScenePath, "%s\\art\\animation\\cutscene\\%s", strProjectRoot.c_str(), "!!scenes");

	atArray<atString> assetPathTokens;
	atString strScenePath(cScenePath);
	strScenePath.Split( assetPathTokens, "\\", true );	

	char cValue[256];
	sysGetEnv( "RS_BUILDBRANCH", cValue, sizeof(cValue) );

	// Check the platform files
	for(int i=0; i < atTargets.GetCount(); ++i)
	{
		char cPlatform[RAGE_MAX_PATH * 2];
		cPlatform[0] = 0;
		safecat( cPlatform, cValue, sizeof(cPlatform) );
		safecat( cPlatform, "\\", sizeof(cPlatform));
		safecat( cPlatform, atTargets[i].c_str(), sizeof(cPlatform));
		safecat( cPlatform, "\\anim\\cutscene\\cuts_", sizeof(cPlatform) );
		safecat( cPlatform, outputPathTokens[assetPathTokens.GetCount()].c_str(), sizeof(cPlatform) );
		safecat( cPlatform, ".rpf", sizeof(cPlatform) );

		DWORD fAttr = GetFileAttributes( cPlatform ); // Check file exists
		if( (fAttr != INVALID_FILE_ATTRIBUTES))
		{
			// Do a dirty little trick - this checks if the file is being used by another process. ie. game.
			if( 0 != rename(cPlatform, cPlatform) ) 
			{
				char msgBuffer[1024];
				snprintf(msgBuffer, 1024, "The file '%s' is in use.", cPlatform );

				FBMessageBox( "Error", msgBuffer, "OK");
				return false;
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

void rexMBAnimExportCommon::ReadTargets(atArray<atString>& atTargets)
{
	INIT_PARSER;

	char cValue[256];
	sysGetEnv( "RS_TOOLSROOT", cValue, sizeof(cValue) );

	char cFile[RAGE_MAX_PATH * 2];
	cFile[0] = 0;
	safecat( cFile, cValue, sizeof(cFile) );
	safecat( cFile, "\\etc\\local", sizeof(cFile) );

	rage::parTree* pXMLTree = PARSER.LoadTree(cFile, "xml");

	if(pXMLTree == NULL)
		return;

	for(parTreeNode::ChildNodeIterator nodeBranches = pXMLTree->GetRoot()->BeginChildren(); nodeBranches != pXMLTree->GetRoot()->EndChildren(); ++nodeBranches) // Branches
	{
		parTreeNode* pBranches = *(nodeBranches);
		parElement& elementBranches = (*nodeBranches)->GetElement();
		if(stricmp(elementBranches.GetName(), "branches" ) == 0)
		{
			atArray<parAttribute>& attributes = elementBranches.GetAttributes();

			for(int i=0; i < attributes.GetCount(); ++i)
			{
				if(stricmp(attributes[i].GetName(), "default" ) == 0)
				{
					atString strBranch(attributes[i].GetStringValue());

					for(parTreeNode::ChildNodeIterator nodeBranch = pBranches->BeginChildren(); nodeBranch != pBranches->EndChildren(); ++nodeBranch) // Branch
					{
						parTreeNode* pBranch = *(nodeBranch);
						parElement& elementBranch = (*nodeBranch)->GetElement();

						atString strBranchName;
						if(elementBranch.GetAttributes().GetCount())
						{
							strBranchName = elementBranch.GetAttributes()[0].GetStringValue();
						}

						if(stricmp(strBranchName.c_str(), strBranch.c_str() ) == 0)
						{
							for(parTreeNode::ChildNodeIterator targets = pBranch->BeginChildren(); targets != pBranch->EndChildren(); ++targets) // Targets
							{
								parTreeNode* pTargets = *(targets);

								for(parTreeNode::ChildNodeIterator target = pTargets->BeginChildren(); target != pTargets->EndChildren(); ++target) // Target
								{
									parElement& elementTarget = (*target)->GetElement();

									atArray<parAttribute>& targetAttributes = elementTarget.GetAttributes();

									for(int j=0; j < targetAttributes.GetCount(); ++j)
									{
										if(stricmp(targetAttributes[j].GetName(), "enabled" ) == 0)
										{
											for(int k=0; k < targetAttributes.GetCount(); ++k)
											{
												if(stricmp(targetAttributes[k].GetName(), "platform" ) == 0)
												{
													atTargets.Grow() = atString(targetAttributes[k].GetStringValue());
												}
											}
										}
									}
								}
							}	
						}
					}
				}
			}
		}
	}		
}

//-----------------------------------------------------------------------------

float rexMBAnimExportCommon::GetValueFromFCurve(HFBAnimationNode pNode)
{
	int iCount = pNode->FCurve->Keys.GetCount();

	FBSystem system;
	int iFrame = rexMBAnimExportCommon::GetFrame(system.LocalTime);

	if ( (iFrame < 0) || (iCount == 0) )
	{
		return 0.0f;
	}

	if ( iFrame <= rexMBAnimExportCommon::GetFrame(pNode->FCurve->Keys[0].Time) )
	{
		return pNode->FCurve->Keys[0].Value;
	}

	if ( iFrame >= rexMBAnimExportCommon::GetFrame(pNode->FCurve->Keys[iCount - 1].Time) )
	{
		return pNode->FCurve->Keys[iCount - 1].Value;
	}

	//float fValue = pNode->FCurve->Keys[0].Value;
	for ( int i = 1; i < iCount; ++i )
	{
		if ( iFrame == rexMBAnimExportCommon::GetFrame(pNode->FCurve->Keys[i - 1].Time) )
		{
			return pNode->FCurve->Keys[i - 1].Value;
		}

		/*if(iFrame > rexMBAnimExportCommon::GetFrame(pNode->FCurve->Keys[i].Time) )
		{
		fValue = pNode->FCurve->Keys[i].Value;
		}*/

		if ( iFrame < rexMBAnimExportCommon::GetFrame(pNode->FCurve->Keys[i].Time) )
		{
			float fValue1 = pNode->FCurve->Keys[i - 1].Value;
			int iFrame1 = rexMBAnimExportCommon::GetFrame(pNode->FCurve->Keys[i - 1].Time);

			float fValue2 =pNode->FCurve->Keys[i].Value;
			int iFrame2 = rexMBAnimExportCommon::GetFrame(pNode->FCurve->Keys[i].Time);

			// linear interpolation
			float fValue = fValue1 + ((fValue2 - fValue1) * ((float)(iFrame - iFrame1) / (float)(iFrame2 - iFrame1)));
			return fValue;
		}
	}

	return 0.0f;
}

void rexMBAnimExportCommon::SetFaceAnimationNode(cutfPedModelObject *pPedModelObj, const char* czNode)
{
	if(!pPedModelObj || !czNode) return;

	pPedModelObj->SetFaceAnimationNodeName((char*)czNode);

	atString faceName(czNode);
	faceName.Lowercase();

	for(int i=0; i < rexMBAnimExportCommon::GetCutsceneFaceDefaults().GetCount(); ++i)
	{
		SFaceDefaults def = rexMBAnimExportCommon::GetCutsceneFaceDefaults()[i];
		def.suffix.Lowercase();

		if(faceName.EndsWith(def.suffix))
		{
			pPedModelObj->SetFaceAttributesFilename(def.attributes.c_str());
			pPedModelObj->SetFaceExportCtrlSpecFile(def.spec.c_str());
		}
	}		
}

bool rexMBAnimExportCommon::FindFaceNode(cutfObject* pObject, atString& faceNodeName)
{
	cutfPedModelObject* pPedObject = dynamic_cast<cutfPedModelObject*>(pObject);

	if(pPedObject)
	{
		for(int i=0; i < rexMBAnimExportCommon::GetCutsceneFaceDefaults().GetCount(); ++i)
		{
			SFaceDefaults def = rexMBAnimExportCommon::GetCutsceneFaceDefaults()[i];

			char cFace[RAGE_MAX_PATH];
			sprintf(cFace, "%s:%s", pObject->GetDisplayName().c_str(), def.suffix.c_str());

			HFBModel pFace = RAGEFindModelByName(cFace);
			if(pFace)
			{
				faceNodeName = cFace;
				return true;
			}
		}
	}

	return false;
}

void rexMBAnimExportCommon::FindAndAddFaceNode(cutfObject* pObject)
{
	atString faceNodeName;
	if(rexMBAnimExportCommon::FindFaceNode(pObject, faceNodeName))
	{
		cutfPedModelObject* pPedObject = dynamic_cast<cutfPedModelObject*>(pObject);

		rexMBAnimExportCommon::SetFaceAnimationNode(pPedObject, faceNodeName.c_str());
	}
}

//#endif // HACK_GTA4


//-----------------------------------------------------------------------------

