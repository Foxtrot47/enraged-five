#include "perforce.h"

#include "UserObjects/OpenRealitySDK.h"

#include "Logger.h"

#include "limits.h"
#include "system/exec.h"
#include "file/asset.h"

#include <iostream>
#include <fstream>
#include <vector>

using namespace rage;

static int sCLNumber = -1;
atArray<atString> perforce::filesQueuedForSync;
atArray<atString> perforce::filesQueuedForAdd;
atArray<atString> perforce::filesQueuedForCheckout;
atArray<atString> perforce::filesQueuedForDelete;
atArray<atString> perforce::filesQueuedForRevert;

#define USE_NEW_PERFORCE_INTEGRATION 1

bool perforce::IsSessionValid(const atString& strOutput, bool bInteractive)
{
	if(strOutput.IndexOf("P4PASSWD") != -1 || strOutput.IndexOf("session has expired") != -1)
	{
		if(bInteractive)
		{
			char cProject[RAGE_MAX_PATH];
			rexMBAnimExportCommon::GetEnvironmentVariable(cProject,"RS_PROJECT",RAGE_MAX_PATH);
			char cCmdLine[RAGE_MAX_PATH];
			sprintf(cCmdLine, "x: & cd gta5 & echo perforce login & p4 login");
			system(cCmdLine);
		}

		return true;
	}

	return false;
}

void perforce::GetRevision(const char* pDir, int& revision, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

	std::string atFile(pDir);
	ConvertSpecialCharacters(atFile);

	char cP4CommandLine[MAX_PATH];
	sprintf(cP4CommandLine, "p4 fstat %s", atFile.c_str());

	revision = -1;

	atString strOutput("");

	char cProjectRoot[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_PROJROOT",cProjectRoot,RAGE_MAX_PATH);

	int iResult = ExecuteCommand(cP4CommandLine, cProjectRoot, strOutput);

	if(iResult == 0)
	{
		ParseRevisionInfo(revision, strOutput);
	}
	else if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		GetRevision(pDir, revision, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("getrevision", strOutput, bInteractive);
	}
}

void perforce::ParseRevisionInfo(int& revision, atString strOutput)
{
	atArray<atString> lines;
	strOutput.Split(lines, "\r\n");

	for(int i=0; i < lines.GetCount(); ++i)
	{
		int index = lines[i].IndexOf("headRev");
		if(index != -1)
		{
			lines[i].Set(lines[i], index + strlen("headRev"), lines[i].GetLength());
			lines[i].Trim();

			revision = atoi(lines[i].c_str());
		}
	}
}

void perforce::GetInfo(const char* pDir, atString& strP4Info, bool bInteractive)
{
	char cP4CommandLine[MAX_PATH];
	sprintf(cP4CommandLine, "p4 info");

	int iResult = ExecuteCommand(cP4CommandLine, pDir, strP4Info);

	if(iResult != 0 && IsSessionValid(strP4Info, bInteractive))
	{
		GetInfo(pDir, strP4Info, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("info", strP4Info, bInteractive);
	}
}

void perforce::CreateChangelistDescription(const char* pDescription, atString& atDescriptionFile, bool bInteractive)
{
	atString strOutput("");
	char cToolsRoot[MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cToolsRoot, MAX_PATH);

	GetInfo(cToolsRoot, strOutput, bInteractive);

	if(strOutput.GetLength())
	{
		atArray<atString> atLines;
		strOutput.Split(atLines, "\n");

		atLines[1].Replace("Client name: ","");
		atLines[0].Replace("User name: ", "");

		char cTemp[MAX_PATH];
		rexMBAnimExportCommon::GetEnvironmentVariable("TEMP", cTemp, MAX_PATH);
		char cTempFile[MAX_PATH];
		sprintf(cTempFile, "%s\\cl.txt", cTemp);

		std::ofstream tbFile (cTempFile);
		if (tbFile.is_open())
		{
			tbFile << "Change: new \nClient: ";
			tbFile << atLines[1].c_str();
			tbFile << " \nUser: ";
			tbFile << atLines[0].c_str();
			tbFile << " \n";
			tbFile << "Status: new \nDescription: ";
			tbFile << pDescription;
		}

		tbFile.close();

		atDescriptionFile = cTempFile;
	}
}

void perforce::ShowErrorMessage(const char* command, const atString& strOutput, bool bInteractive)
{
	if(strOutput.IndexOf("chmod") == -1)
	{
		if(bInteractive)
		{
			if(stricmp(strOutput.c_str(),"") == 0) return; // Ignore errors with no message

			char cTitle[MAX_PATH];
			sprintf(cTitle, "Perforce Integration Error - p4 %s", command);

			FBMessageBox(cTitle, (char*)strOutput.c_str(), "OK");
		}
	}
}

void perforce::Sync(const char* pFullPath, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;


#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;
	atString fullPath(pFullPath);
	filesQueuedForSync.Grow() = fullPath;
#else
	char cDir[MAX_PATH];
	ASSET.RemoveNameFromPath(cDir, MAX_PATH, pFullPath);

	const char* pFilename = ASSET.FileName(pFullPath);


	Sync(cDir, pFilename, false, bInteractive);
#endif
}

void perforce::Sync(const char* pDir, const char* pFile, bool bForce, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

#if USE_NEW_PERFORCE_INTEGRATION
	bForce;
	bInteractive;

	atString fullPath(pDir);
	fullPath += "\\";
	fullPath += pFile;
	filesQueuedForSync.Grow() = fullPath;
#else
	atString strOutput("");
	char cP4CommandLine[MAX_PATH];
	sprintf(cP4CommandLine, "p4 sync %s %s", (bForce) ? "-f" : "",pFile);

	int iResult = ExecuteCommand(cP4CommandLine, pDir, strOutput);

	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		Sync(pDir, pFile, bForce, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("sync", strOutput, bInteractive);
	}
#endif
}

void perforce::Revert(const char* pDir, const char* pFile, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

#if USE_NEW_PERFORCE_INTEGRATION
	pDir;
	bInteractive;

	atString fullPath(pDir);
	fullPath += "\\";
	fullPath += pFile;
	filesQueuedForRevert.Grow() = fullPath;
#else
	atString strOutput("");
	char cP4CommandLine[MAX_PATH];
	sprintf(cP4CommandLine, "p4 revert %s", pFile);

	int iResult = ExecuteCommand(cP4CommandLine, pDir, strOutput);

	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		Revert(pDir, pFile, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("revert", strOutput, bInteractive);
	}
#endif
}

void perforce::CreateChangelist(const char* pDescription, const char* pDir, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

	// Create changelist file
	atString atDescriptionFile;
	CreateChangelistDescription(pDescription, atDescriptionFile, bInteractive);

	atString strOutput("");
	char cP4CommandLine[MAX_PATH];
	sprintf(cP4CommandLine, "p4 change -i ");

	int iResult = ExecuteCommand(cP4CommandLine, pDir, strOutput, atDescriptionFile.c_str());

	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		DeleteFile(atDescriptionFile.c_str());
		return CreateChangelist(pDescription, pDir, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("changelist", strOutput, bInteractive);
	}

	atArray<atString> atSplit;
	strOutput.Split(atSplit," ");

	sCLNumber = atoi(atSplit[1]);

	DeleteFile(atDescriptionFile.c_str());
}

void perforce::Checkout(const char* pFullPath, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;

	atString fullPath(pFullPath);
	filesQueuedForCheckout.Grow() = fullPath;
#else
	char cDir[MAX_PATH];
	ASSET.RemoveNameFromPath(cDir, MAX_PATH, pFullPath);

	const char* pFilename = ASSET.FileName(pFullPath);

	Checkout(cDir, pFilename, bInteractive);
#endif
}

void perforce::Checkout(const char* pDir, const char* pFile, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;
	pDir;

	atString fullPath(pFile);
	filesQueuedForCheckout.Grow() = fullPath;
#else
	atString strOutput("");
	char cP4CommandLine[MAX_PATH];
	//sprintf(cP4CommandLine, "p4 edit -c %d %s", sCLNumber, pFile);
	sprintf(cP4CommandLine, "p4 edit %s", pFile);

	int iResult = ExecuteCommand(cP4CommandLine, pDir, strOutput);

	//FBMessageBox("t", (char*)strOutput.c_str(), "OK");

	if(iResult == 0 && strOutput.IndexOf("not on client") != -1)
	{
		MarkForAdd(pDir, pFile, bInteractive);
	}
	else if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		Checkout(pDir, pFile, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("edit", strOutput, bInteractive);
	}
#endif
}

void perforce::MarkForAdd(const char* pDir, const char* pFile, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;

	atString fullPath(pDir);
	fullPath += "\\";
	fullPath += atString(pFile);
	filesQueuedForAdd.Grow() = fullPath;
#else
	atString atFile(pFile);
	atString atTypeMapping("binary+l");

	if(rexMBAnimExportCommon::GetExporterMode() == INGAME_EXPORTER) // Ingame doesnt want exclusive checkout like cutscenes
		atTypeMapping = "binary";

	if(atFile.IndexOf(".cutxml") != -1 || atFile.IndexOf(".txt") != -1 || atFile.IndexOf(".cutlist") != -1 || atFile.IndexOf(".cutpart") != -1)
	{
		atTypeMapping = "text";
	}

	atString strOutput("");
	char cP4CommandLine[MAX_PATH];
	sprintf(cP4CommandLine, "p4 add -f -t %s %s", atTypeMapping.c_str(), pFile);

	int iResult = ExecuteCommand(cP4CommandLine, pDir, strOutput);
	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		MarkForAdd(pDir, pFile, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("add", strOutput, bInteractive);
	}
#endif
}

void perforce::MarkForDelete(const char* pDir, const char* pFile, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()){return;}

#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;

	atString fullPath(pDir);
	fullPath += "\\";
	fullPath += atString(pFile);
	filesQueuedForDelete.Grow() = fullPath;
#else
	atString strOutput("");
	char cP4CommandLine[MAX_PATH];
	sprintf(cP4CommandLine, "p4 delete %s", pFile);

	int iResult = ExecuteCommand(cP4CommandLine, pDir, strOutput);

	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		MarkForDelete(pDir, pFile, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("delete", strOutput, bInteractive);
	}
#endif
}

void perforce::SyncAndMarkForDelete(const char* pFullPath, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) 
	{
		DeleteFile(pFullPath);
		return;
	}

#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;

	filesQueuedForSync.Grow() = pFullPath;
	filesQueuedForDelete.Grow() = pFullPath;
#else
	char cDir[MAX_PATH];
	ASSET.RemoveNameFromPath(cDir, MAX_PATH, pFullPath);

	const char* pFilename = ASSET.FileName(pFullPath);

	Sync(cDir, pFilename, false, bInteractive);
	MarkForDelete(cDir, pFilename, bInteractive);
#endif
}

void perforce::SyncAndMarkForDelete(const char* pDir, const char* pFile, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) 
	{
		char cFile[RAGE_MAX_PATH];
		sprintf(cFile, "%s\\%s", pDir, pFile);

		DeleteFile(cFile);
		return;
	}

#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;

	atString fullPath(pDir);
	fullPath += "\\";
	fullPath += atString(pFile);
	filesQueuedForSync.Grow() = fullPath;
	filesQueuedForDelete.Grow() = fullPath;
#else
	Sync(pDir, pFile, false, bInteractive);
	MarkForDelete(pDir, pFile, bInteractive);
#endif
}

void perforce::SyncAndCheckout(const char* pFullPath, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;

	atString fullPath(pFullPath);
	filesQueuedForSync.Grow() = fullPath;
	filesQueuedForCheckout.Grow() = fullPath;
#else
	char cDir[MAX_PATH];
	ASSET.RemoveNameFromPath(cDir, MAX_PATH, pFullPath);

	const char* pFilename = ASSET.FileName(pFullPath);

	SyncAndCheckout(cDir, pFilename, bInteractive);
#endif
}

void perforce::SyncAndCheckout(const char* pDir, const char* pFile, bool bInteractive)
{
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

#if USE_NEW_PERFORCE_INTEGRATION
	bInteractive;

	atString fullPath(pDir);
	fullPath += "\\";
	fullPath += atString(pFile);
	filesQueuedForSync.Grow() = fullPath;
	filesQueuedForCheckout.Grow() = fullPath;
#else
	Sync(pDir, pFile, false, bInteractive);
	Checkout(pDir, pFile, bInteractive);
#endif
}


void perforce::SyncInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive)
{
#if USE_NEW_PERFORCE_INTEGRATION
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

	atString strOutput("");
	std::string cP4CommandLine;
	cP4CommandLine = "p4 sync ";
	cP4CommandLine += pFiles;

	int iResult = ExecuteCommand(cP4CommandLine.c_str(), cProjectRoot, strOutput);

	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		SyncInternal(cProjectRoot, pFiles, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("sync", strOutput, bInteractive);
	}
#endif
}

void perforce::AddInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive)
{
#if USE_NEW_PERFORCE_INTEGRATION
	atString atFile(pFiles);
	atString atTypeMapping("binary+l");

	if(rexMBAnimExportCommon::GetExporterMode() == INGAME_EXPORTER) // Ingame doesnt want exclusive checkout like cutscenes
		atTypeMapping = "binary";

	if(atFile.IndexOf(".cutxml") != -1 || atFile.IndexOf(".txt") != -1 || atFile.IndexOf(".cutlist") != -1 || atFile.IndexOf(".cutpart") != -1)
	{
		atTypeMapping = "text";
	}

	//Based on the Perforce API, the 'add' command is the only command that expects a literal string for a file path,
	//meaning we have to undo the special character conversion.  Note that AddInternal can be called from CheckoutInternal
	//which hamstrings us from just not converting the paths in the first place.  
	//The conversion takes place at submit-time instead.  
	std::string filesStr(pFiles);
	ReplaceSpecialCharacters(filesStr);

	atString strOutput("");
	std::string cP4CommandLine;
	cP4CommandLine = "p4 add -f -t ";
	cP4CommandLine += atTypeMapping.c_str();
	cP4CommandLine += " ";
	cP4CommandLine += filesStr.c_str();
	
	int iResult = ExecuteCommand(cP4CommandLine.c_str(), cProjectRoot, strOutput);
	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		AddInternal(cProjectRoot, pFiles, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("add", strOutput, bInteractive);
	}
#endif
}

void perforce::DeleteInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive)
{
#if USE_NEW_PERFORCE_INTEGRATION
	if(!rexMBAnimExportCommon::GetPerforceIntegration()){return;}

	atString strOutput("");
	std::string cP4CommandLine;
	cP4CommandLine = "p4 delete ";
	cP4CommandLine += pFiles;

	int iResult = ExecuteCommand(cP4CommandLine.c_str(), cProjectRoot, strOutput);

	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		MarkForDelete(cProjectRoot, pFiles, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("delete", strOutput, bInteractive);
	}
#endif
}

void perforce::CheckoutInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive)
{
#if USE_NEW_PERFORCE_INTEGRATION

	atString strOutput("");
	std::string cP4CommandLine;
	cP4CommandLine = "p4 edit ";
	cP4CommandLine.append(pFiles);

	int iResult = ExecuteCommand(cP4CommandLine.c_str(), cProjectRoot, strOutput);

	if(iResult == 0 && strOutput.IndexOf("not on client") != -1)
	{
		AddInternal(cProjectRoot, pFiles, bInteractive);
	}
	else if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		Checkout(cProjectRoot, pFiles, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("edit", strOutput, bInteractive);
	}
#endif
}


void perforce::RevertInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive)
{
#if USE_NEW_PERFORCE_INTEGRATION
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

	atString strOutput("");
	std::string cP4CommandLine;
	cP4CommandLine = "p4 revert ";
	cP4CommandLine += pFiles;

	int iResult = ExecuteCommand(cP4CommandLine.c_str(), cProjectRoot, strOutput);

	if(iResult != 0 && IsSessionValid(strOutput, bInteractive))
	{
		Revert(cProjectRoot, pFiles, bInteractive);
	}
	else if(iResult != 0)
	{
		ShowErrorMessage("revert", strOutput, bInteractive);
	}
#endif
}

void perforce::WarnIfCheckedOut(const char* cProjectRoot, const char* pFiles, bool bInteractive)
{
#if USE_NEW_PERFORCE_INTEGRATION
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

	atString strOutput("");
	std::string cP4CommandLine;
	cP4CommandLine = "p4 fstat ";
	cP4CommandLine += pFiles;

	int iResult = ExecuteCommand(cP4CommandLine.c_str(), cProjectRoot, strOutput);

	if(iResult == 0 && bInteractive && rexMBAnimExportCommon::GetUseWarnIfCheckedOut())
	{
		atArray<atString> atSplitFiles;
		strOutput.Split(atSplitFiles, "\r\n\r\n", true);

		for(int i=0; i < atSplitFiles.GetCount(); ++i)
		{
			atArray<atString> atLines;
			atSplitFiles[i].Split(atLines, "\r\n", true);

			atString strDepotFile;
			atArray<atString> atOthers;

			for(int j=0; j < atLines.GetCount(); ++j)
			{
				if(atLines[j].StartsWith("... depotFile"))
				{
					atLines[j].Replace("... depotFile","");
					atLines[j].Trim();
					strDepotFile = atLines[j];
				}
				
				if(atLines[j].StartsWith("... ... otherOpen"))
				{
					atArray<atString> atOtherOpen;
					atLines[j].Split(atOtherOpen, " ", true);

					if(atOtherOpen.GetCount() == 4 && atOtherOpen[3].IndexOf("@") != -1)
					{
						atOthers.PushAndGrow(atOtherOpen[3]);
					}
				}
			}

			if(atOthers.GetCount() && rexMBAnimExportCommon::GetUseWarnIfCheckedOut())
			{
				char cMsg[RAGE_MAX_PATH];
				sprintf(cMsg, "Warning: File '%s' already checked out by '%s'", strDepotFile.c_str(), rexMBAnimExportCommon::ArrayToString(atOthers, atString(" , ")).c_str());
				if(FBMessageBox("Perforce Integration Warning", cMsg, "OK", "Ignore") == 2)
				{
					rexMBAnimExportCommon::SetUseWarnIfCheckedOut(false);
				}
			}
		}
	}

#endif
}

//Special Characters as dictated by Perforce: http://www.perforce.com/perforce/r12.2/manuals/cmdref/o.fspecs.html.
void perforce::ConvertSpecialCharacters(std::string& inString)
{
	//Process the percentages first.
	std::string currentSymbol = "%";
	size_t found = inString.find_first_of(currentSymbol);
	while (found!=string::npos)
	{
		std::string subString = inString.substr(found, 3);
		if ( subString != "%40" && subString != "%23" && subString != "%2A")
		{
			inString.replace(found, 1, "%");
			inString.insert(found+1, "25");
		}
		
		found = inString.find_first_of(currentSymbol, found+1);
	}

	currentSymbol = "@";
	found = inString.find_first_of(currentSymbol);
	while (found != string::npos)
	{
		inString.replace(found, 1, "%");
		inString.insert(found+1, "40");
		found = inString.find_first_of(currentSymbol);
	}

	currentSymbol = "#";
	found = inString.find_first_of(currentSymbol);
	while (found!=string::npos)
	{
		inString.replace(found, 1, "%");
		inString.insert(found+1, "23");
		found = inString.find_first_of(currentSymbol);
	}

	currentSymbol = "*";
	found = inString.find_first_of(currentSymbol);
	while (found!=string::npos)
	{
		inString.replace(found, 1, "%");
		inString.insert(found+1, "2A");
		found = inString.find_first_of(currentSymbol);
	}
}


//Special Characters as dictated by Perforce: http://www.perforce.com/perforce/r12.2/manuals/cmdref/o.fspecs.html.
void perforce::ReplaceSpecialCharacters(std::string& inString)
{
	//Process the percentages first.
	std::string currentSymbol = "%25";
	size_t found = inString.find(currentSymbol);
	while (found!=string::npos)
	{
		inString.replace(found, currentSymbol.length(), "%");
		found = inString.find(currentSymbol, found+1);
	}

	currentSymbol = "%40";
	found = inString.find(currentSymbol);
	while (found != string::npos)
	{
		inString.replace(found, currentSymbol.length(), "@");
		found = inString.find(currentSymbol);
	}

	currentSymbol = "%23";
	found = inString.find(currentSymbol);
	while (found!=string::npos)
	{
		inString.replace(found, currentSymbol.length(), "#");
		found = inString.find(currentSymbol);
	}

	currentSymbol = "%2A";
	found = inString.find(currentSymbol);
	while (found!=string::npos)
	{
		inString.replace(found, currentSymbol.length(), "#");
		found = inString.find(currentSymbol);
	}
}

void perforce::ExecuteQueuedOperations(bool bInteractive)
{
#if USE_NEW_PERFORCE_INTEGRATION
	if(!rexMBAnimExportCommon::GetPerforceIntegration()) return;

	char cProjectRoot[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_PROJROOT",cProjectRoot,RAGE_MAX_PATH);

	//Sync all files.
	std::string syncList("");
	for(int fileIndex = 0; fileIndex < filesQueuedForSync.GetCount(); ++fileIndex)
	{
		if (fileIndex > 0)
			syncList.append(" ");

		syncList.append("\"");
		syncList.append(filesQueuedForSync[fileIndex]);		
		syncList.append("\"");
	}

	if (syncList.length() > 0)
	{
		ConvertSpecialCharacters(syncList);
		SyncInternal(cProjectRoot, syncList.c_str(), bInteractive);
	}

	//Add all files.
	std::string addList("");
	for(int fileIndex = 0; fileIndex < filesQueuedForAdd.GetCount(); ++fileIndex)
	{
		if (fileIndex > 0)
			addList.append(" ");

		addList.append("\"");
		addList.append(filesQueuedForAdd[fileIndex]);
		addList.append("\"");
	}

	if (addList.length() > 0)
	{
		std::string revertList("");
		revertList.append(addList.c_str());
		ConvertSpecialCharacters(revertList);
		RevertInternal(cProjectRoot, revertList.c_str(), bInteractive);

		//No need to convert special characters for add commands.  The command expects a literal string.
		//http://www.perforce.com/perforce/r12.2/manuals/cmdref/o.fspecs.html#1040647
		//To add a file such as status@june.txt, force a literal interpretation of special characters by using:
		//		p4 add -f //depot/path/status@june.txt
		AddInternal(cProjectRoot, addList.c_str(), bInteractive);
	}
		
	//Checkout all files
	std::string checkoutList("");
	for(int fileIndex = 0; fileIndex < filesQueuedForCheckout.GetCount(); ++fileIndex)
	{
		if (fileIndex > 0)
			checkoutList.append(" ");

		checkoutList.append("\"");
		checkoutList.append(filesQueuedForCheckout[fileIndex]);
		checkoutList.append("\"");
	}

	if (checkoutList.length() > 0)
	{
		ConvertSpecialCharacters(checkoutList);
		//RevertInternal(cProjectRoot, checkoutList.c_str(), bInteractive); // This is reverting MoVE edited clips.
		CheckoutInternal(cProjectRoot, checkoutList.c_str(), bInteractive);
		WarnIfCheckedOut(cProjectRoot, checkoutList.c_str(), bInteractive);
	}

	//Delete all files
	std::string deleteList("");
	for(int fileIndex = 0; fileIndex < filesQueuedForDelete.GetCount(); ++fileIndex)
	{
		if (fileIndex > 0)
			deleteList.append(" ");

		deleteList.append("\"");
		deleteList.append(filesQueuedForDelete[fileIndex]);
		deleteList.append("\"");
	}

	if (deleteList.length() > 0)
	{
		ConvertSpecialCharacters(deleteList);
		RevertInternal(cProjectRoot, deleteList.c_str(), bInteractive);
		DeleteInternal(cProjectRoot, deleteList.c_str(), bInteractive);
	}

	//Revert all files
	std::string revertList("");
	for(int fileIndex = 0; fileIndex < filesQueuedForRevert.GetCount(); ++fileIndex)
	{
		if (fileIndex > 0)
			revertList.append(" ");

		revertList.append("\"");
		revertList.append(filesQueuedForRevert[fileIndex]);
		revertList.append("\"");
	}

	if (revertList.length() > 0)
	{
		ConvertSpecialCharacters(revertList);
		RevertInternal(cProjectRoot, revertList.c_str(), bInteractive);
	}

	//Clear all of the lists.
	filesQueuedForSync.clear();
	filesQueuedForAdd.clear();
	filesQueuedForCheckout.clear();
	filesQueuedForDelete.clear();
	filesQueuedForSync.clear();
	filesQueuedForRevert.clear();
#endif
}