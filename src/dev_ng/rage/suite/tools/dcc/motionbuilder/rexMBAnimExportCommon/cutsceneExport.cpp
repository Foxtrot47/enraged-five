
#pragma warning(push)
#pragma warning(disable:4668)
#include <windows.h>
#pragma warning(pop)

#include "atl/creator.h"

#include "cutsceneExport.h"
#include "cutsceneExportModel.h"
#include "cutsceneExport_parser.h"

#include <string>

#include "keylist.h"
#include "timelist.h"
#include "timerLog.h"
#include "toolobjectinterchange.h"
#include "utility.h"
#include "atl/array.h"
#include "cranimation/animation.h"
#include "cranimation/animTrack.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/device.h"
#include "parser/macros.h"
#include "rexGeneric/objectAnimation.h"
#include "rexRage/serializerAnimation.h"
#include "system/exec.h"
#include "utility.h"
#include "cutfile/cutfevent.h"

#include "perforce.h"
#include "pipeline.h"
#include "Logger.h"

#pragma warning(disable:4482)

static const char* strCutscenePath = "!!scenes";

#define RDR_ANIM_CODE 0

namespace rage
{
//-------------------------------------------------
RexMbCutsceneData::RexMbCutsceneData()
: m_overrideExportRoot(false)
, m_overrideFaceDir(false)
{
    Clear();
}

RexMbCutsceneData::~RexMbCutsceneData()
{
    Clear();
}
//-------------------------------------------------
void RexMbCutsceneData::Clear()
{
    SetConfig( NULL );
    SetSceneName( NULL );

    m_overrideExportRoot = false;
    m_overrideFaceDir = false;

    m_exportRoot.Clear();
    m_faceDir.Clear();
    m_backupDir.Clear();
    
    m_cutsceneFile.Clear();
}
//-------------------------------------------------
const char* RexMbCutsceneData::GetConfig() const
{
    return m_config.c_str();
}
//-------------------------------------------------
void RexMbCutsceneData::SetConfig( const char *pConfig )
{
    if ( pConfig != NULL )
    {
        m_config = pConfig;
    }
    else
    {
        m_config = "";             
    }

    UpdateExportPaths();
}
//-------------------------------------------------
const char* RexMbCutsceneData::GetSceneName() const
{
    return m_sceneName;
}
//-------------------------------------------------
void RexMbCutsceneData::SetSceneName( const char *pSceneName )
{
    if ( pSceneName != NULL )
    {
        m_sceneName = pSceneName;
    }
    else
    {
        m_sceneName = "";             
    }

    UpdateExportPaths();
}
//-------------------------------------------------
void RexMbCutsceneData::GetExportPath( char* pDest, int destLen ) const
{
	atString assets = rexMBAnimExportCommon::GetProjectOrDlcAssetsDir(atString(""));
	formatf( pDest, destLen, "%s/cuts/%s", assets.c_str(), m_sceneName.c_str() );
}
//-------------------------------------------------
void RexMbCutsceneData::GetFaceExportPath( char* pDest, int destLen ) const
{
	char cDest[RAGE_MAX_PATH];
	GetExportPath(cDest, RAGE_MAX_PATH);

	formatf( pDest, destLen, "%s/faces", cDest );
}
//-------------------------------------------------
bool RexMbCutsceneData::SaveToFile( const char* pFilename )
{
    return PARSER.SaveObject( pFilename, "", this );
}
//-------------------------------------------------
bool RexMbCutsceneData::LoadFromFile( const char *pFilename )
{
    Clear();

    return PARSER.LoadObject<RexMbCutsceneData>( pFilename, "", *this );
}
//-------------------------------------------------
bool RexMbCutsceneData::SaveToString( char** pOutput )
{
    int bufSize = 150 * 1024;
    char *pBuffer = rage_new char[bufSize];
    char memFileName[256];

    // Save the object into a memory mapped file buffer
    memset( pBuffer, 0, bufSize );

    fiDevice::MakeMemoryFileName( memFileName, 256, pBuffer, bufSize, false, "RexMbCutsceneData");
    fiStream* pMemFile = ASSET.Open( memFileName, "" );

	m_cutsceneFile.ConvertArgPointersToIndices();

    PARSER.SaveObject( pMemFile, this );

    pMemFile->Close();

    // Copy the data from the memory mapped file buffer into the null terminated output string
    int outSize = (int)strlen( pBuffer );
    Assert( outSize < bufSize );

    (*pOutput) = rage_new char[outSize + 1];
    memset( *pOutput, 0, outSize + 1 );
    strcpy( *pOutput, pBuffer );

    delete [] pBuffer;

    return true;
}
//-------------------------------------------------
bool RexMbCutsceneData::LoadFromString( const char* pInput )
{
    if ( (pInput == NULL) || (strlen(pInput) == 0) )
    {
        return false;
    }

    char memFileName[RAGE_MAX_PATH];
    int  inputSize = (int)strlen( pInput );
    fiDevice::MakeMemoryFileName( memFileName, RAGE_MAX_PATH, pInput, inputSize, false, "RexMbCutsceneData" );
    fiStream* pMemFile = ASSET.Open( memFileName, "" );

    Clear();

	PARSER.Settings().SetFlag(parSettings::NEVER_SORT_ATTR_LIST, true);

    if ( !PARSER.LoadObject<RexMbCutsceneData>( pMemFile, *this ) )
    {
        pMemFile->Close();
        return false;
    }
    else
    {
        pMemFile->Close();

		m_cutsceneFile.ConvertArgIndicesToPointers();

        // Minor upgrade
        m_cutsceneFile.UpgradeDrawDistances();

        return true;
    }
}
//-------------------------------------------------
bool RexMbCutsceneData::ValidateCutsceneExportData( )
{
	// set config to be the project
	char cProjectName[256];
	if ( sysGetEnv( "RS_PROJECT", cProjectName, sizeof(cProjectName)) )
	{
		SetConfig(cProjectName);
	}
	else
	{
		SetConfig("RAGE");
	}

    // initialize the scene filename
    if ( (GetSceneName() == NULL) || (strlen( GetSceneName() ) == 0) )
    {
        FBApplication app;

        char cFile[RAGE_MAX_PATH];
		ASSET.RemoveExtensionFromPath( cFile, sizeof(cFile), (const char*)app.FBXFileName );

        SetSceneName( ASSET.FileName( cFile ) );
    }

	// Make sure we have a camera
	atArray<cutfObject *> cameraObjectList;
	m_cutsceneFile.FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );

	if(cameraObjectList.GetCount() == 0)
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Scene does not contain a camera.");
		return false;
	}

	// Validate dof planes
	char cNearDOFStrengthPlaneName[1024];
	sprintf( cNearDOFStrengthPlaneName, "%s DOF_Strength_NearPlane", cameraObjectList[0]->GetDisplayName().c_str() );

	char cFarDOFStrengthPlaneName[1024];
	sprintf( cFarDOFStrengthPlaneName, "%s DOF_Strength_FarPlane", cameraObjectList[0]->GetDisplayName().c_str() );

	HFBModel pNearDOFStrengthPlaneModel = RAGEFindModelByName( cNearDOFStrengthPlaneName );
	HFBModel pFarDOFStrengthPlaneModel = RAGEFindModelByName( cFarDOFStrengthPlaneName );

	if(pNearDOFStrengthPlaneModel == NULL || pFarDOFStrengthPlaneModel == NULL)
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Missing one or both of the Dof Strength Near/Far Plane objects.");
		return false;
	}

    return true;
}
//-------------------------------------------------
void RexMbCutsceneData::UpdateExportPaths()
{
    if ( !m_overrideExportRoot && rexMBAnimExportCommon::GetMotionBuilderCutsceneExportRoot( m_exportRoot ) )
    {
        m_exportRoot.Lowercase();
        m_exportRoot.Replace( "$(config)", GetConfig() );
        m_exportRoot.Replace( "$(scene)", GetSceneName() );
    }

    if ( !m_overrideFaceDir && rexMBAnimExportCommon::GetMotionBuilderCutsceneFaceExportPath( m_faceDir ) )
    {
        m_faceDir.Lowercase();
        m_faceDir.Replace( "$(config)", GetConfig() );
        m_faceDir.Replace( "$(scene)", GetSceneName() );
    }

    if ( rexMBAnimExportCommon::GetMotionBuilderCutsceneBackupPath( m_backupDir ) )
    {
        m_backupDir.Lowercase();
        m_backupDir.Replace( "$(config)", GetConfig() );
        m_backupDir.Replace( "$(scene)", GetSceneName() );
    }
}
//-------------------------------------------------
RexRageCutsceneExport::RexRageCutsceneExport() 
: m_bInteractiveMode(false)
, m_iCurrentExportProgressItem(0)
, m_iNumExportProgressItems(0)
{
	rexMBAnimExportCommon::CreateObjectInterchange();

    cutfEvent::SetEventIdDisplayNameFunc( MakeFunctorRet( m_eventDefs, &cutfEventDefs::GetEventDisplayName ) );
    cutfEvent::SetEventIdRankFunc( MakeFunctorRet( m_eventDefs, &cutfEventDefs::GetEventIdRank ) );
    cutfEvent::SetOppositeEventIdFunc( MakeFunctorRet( m_eventDefs, &cutfEventDefs::GetOppositeEventId ) );
    cutfEventArgs::SetEventArgsTypeNameFunc( MakeFunctorRet( m_eventDefs, &cutfEventDefs::GetEventArgsTypeName ) );
}
//-------------------------------------------------
RexRageCutsceneExport::~RexRageCutsceneExport()
{
	rexMBAnimExportCommon::KillObjectInterchange();
}
//-------------------------------------------------
void RexRageCutsceneExport::InitClass()
{
    crAnimation::InitClass();
    crClip::InitClass();
    cutfEvent::InitClass();
}
//-------------------------------------------------
void RexRageCutsceneExport::ShutdownClass()
{
    crClip::ShutdownClass();
    crAnimation::ShutdownClass();
    cutfEvent::ShutdownClass();
}
//-------------------------------------------------
bool RexRageCutsceneExport::OpenPartFile( const char *pFullPath )
{
	ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/opencutlist", GetSceneName()),"rexmbrage");

	bool bResult = GetCutscenePartFile().LoadFile( pFullPath );

	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		GetCutscenePartFile().GetPart(i)->SetMaster(&GetCutsceneFile());
	}

	ULOGGER_MOBO.PostExport(m_bInteractiveMode);

	return bResult;
}
//-------------------------------------------------
bool RexRageCutsceneExport::OpenCutFile( const char *pFullPath )
{
	// Open up cutfile temp and check if its stream processed, if so then refuse it
	cutfCutsceneFile2* pCutfile = rage_new cutfCutsceneFile2();

	ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/opencutfile", GetSceneName()),"rexmbrage");

	if (pCutfile->LoadFile(pFullPath) )
	{
		if(pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_PART_FLAG) ||
			pCutfile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_EXTERNAL_CONCAT_FLAG))
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to load part or external concat cutxml");
			ULOGGER_MOBO.PostExport(m_bInteractiveMode);
			return false;  
		}

		delete pCutfile;
	}

    if ( !GetCutsceneFile().LoadFile( pFullPath ) )
    {
		ULOGGER_MOBO.PostExport(m_bInteractiveMode);
        return false;    
    }

	SetSceneName( GetCutsceneFile().GetRealSceneName().c_str() );
	
	ChangeStreamProcessedEvents();

	atBitSet &cutsceneFlags = const_cast<atBitSet &>( GetCutsceneFile().GetCutsceneFlags() );
	cutsceneFlags.Clear(cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG);
	cutsceneFlags.Clear(cutfCutsceneFile2::CUTSCENE_STREAM_PROCESSED);
	cutsceneFlags.Clear(cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_FLAG);
	cutsceneFlags.Clear(cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_END_FLAG);
	cutsceneFlags.Clear(cutfCutsceneFile2::CUTSCENE_INTERNAL_CONCAT_FLAG);
	cutsceneFlags.Clear(cutfCutsceneFile2::CUTSCENE_USE_ONE_AUDIO_FLAG);

    LoadEventDefsFile();

	RemoveEvents();
	RemoveObjects();

	GetCutsceneFile().CleanupUnusedEventArgs();

	ULOGGER_MOBO.PostExport(m_bInteractiveMode);

    return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateHandles()
{
	bool bResult = true;
	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		RexRageCutscenePart* pPart =  GetCutscenePartFile().GetPart(i);

		const atArray<cutfObject *>& objectList = pPart->GetCutsceneFile()->GetObjectList();

		for(int j=0; j < objectList.GetCount(); ++j)
		{
			const cutfModelObject* pModelObject = dynamic_cast<const cutfModelObject*>(objectList[j]);
			if(pModelObject)
			{
				//const char* pHandle = pModelObject->GetHandle().GetCStr();
				if(pModelObject->GetHandle().GetCStr() == NULL || (stricmp(pModelObject->GetHandle().GetCStr(),"")==0 || stricmp(pModelObject->GetHandle().GetCStr(),"(no handle)")==0))
				{ 
					if(rexMBAnimExportCommon::IsDlc(atString("")) || RDR_ANIM_CODE == 1)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: %s HAS NO HANDLE SET", pModelObject->GetName().GetCStr());
						bResult = false;
					}
					else
					{
						ULOGGER_MOBO.SetProgressMessage("WARNING: %s HAS NO HANDLE SET", pModelObject->GetName().GetCStr());
					}
				}
			}
		}
	}

	return bResult;
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidatePartOffsets()
{
	if(GetCutscenePartFile().GetNumParts())
	{
		Vector3 vOffset = GetCutscenePartFile().GetPart(0)->GetCutsceneFile()->GetOffset();
		float fRotation = GetCutscenePartFile().GetPart(0)->GetCutsceneFile()->GetRotation();
		for(int i=1; i < GetCutscenePartFile().GetNumParts(); ++i)
		{
			RexRageCutscenePart* pPart =  GetCutscenePartFile().GetPart(i);

			Vector3 vOffsetCompare = pPart->GetCutsceneFile()->GetOffset();
			float fRotationCompare = pPart->GetCutsceneFile()->GetRotation();

			// We cant do straight comparisons on floats in a vector
			if(!vOffset.IsClose(vOffsetCompare,0.005f) || fRotation != fRotationCompare)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: All internal parts scene offset/rotation must match.");
				return false;
			}

			vOffset = pPart->GetCutsceneFile()->GetOffset();
			fRotation = pPart->GetCutsceneFile()->GetRotation();
		}
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::JoinScenes(bool bCutFileOnly)
{
	ULOGGER_MOBO.SetProgressMessage("Joining scenes...");

	char cExportPath[RAGE_MAX_PATH];
	GetExportPath(cExportPath, RAGE_MAX_PATH);

	char cValue[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cValue, RAGE_MAX_PATH);

	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		RexRageCutscenePart* pPart =  GetCutscenePartFile().GetPart(i);

		if( !pPart->IsExportable() ) continue;

		char cPartPath[RAGE_MAX_PATH];
		sprintf(cPartPath, "%s\\%s", cExportPath, pPart->GetName());

		perforce::SyncAndCheckout(cPartPath, "data_stream.cutxml", m_bInteractiveMode);
	}

	atString strOutput("");

	char cCmdLine[RAGE_MAX_PATH];
	sprintf_s(cCmdLine, RAGE_MAX_PATH, "%s\\bin\\anim\\cutscene\\cutfJoinSplit.exe --joiner --assetdir %s --cutpart %s\\data.cutpart", cValue, rexMBAnimExportCommon::GetProjectOrDlcAssetsDir(atString("")).c_str(), cExportPath);

	if(bCutFileOnly)
		safecat(cCmdLine, " --buildcutonly");

	perforce::SyncAndCheckout(cExportPath, "data.cutxml", m_bInteractiveMode);
	perforce::ExecuteQueuedOperations(m_bInteractiveMode);

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);
	bool bResult = (ExecuteCommand(cCmdLine, NULL, strOutput) > 0 ? 0 : 1);
	if(!bResult)
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: %s", cCmdLine );
		return false;
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::SaveItemFile()
{
	char cExportPath[RAGE_MAX_PATH];
	GetExportPath( cExportPath, sizeof(cExportPath) );

	char cDataFilename[RAGE_MAX_PATH];
	sprintf( cDataFilename, "%s\\%s.cutpart", cExportPath, "data" );

	ASSET.CreateLeadingPath( cDataFilename );

	atArray<cutfObject*> atObjectList;
	m_cutsceneData.GetCutsceneFile().FindObjectsOfType(CUTSCENE_AUDIO_OBJECT_TYPE, atObjectList);

	if(atObjectList.GetCount() > 0)
	{
		char cAudioName[RAGE_MAX_PATH];
		sprintf(cAudioName, "%s.wav", atObjectList[0]->GetDisplayName().c_str());
		m_cutscenePartData.SetAudio(cAudioName);
	}

	m_cutscenePartData.SetFolder(cExportPath);

	perforce::SyncAndCheckout(cDataFilename, m_bInteractiveMode);
	perforce::ExecuteQueuedOperations(m_bInteractiveMode);

	return m_cutscenePartData.SaveFile(cDataFilename);
}
//-------------------------------------------------
bool RexRageCutsceneExport::BuildZip()
{
	char czExportPath[RAGE_MAX_PATH];
	GetExportPath(czExportPath, RAGE_MAX_PATH);

	return BuildDictionary(czExportPath);
}
//-------------------------------------------------
bool RexRageCutsceneExport::SaveCutFile()
{
	rexMBAnimExportCommon::SetUseClipXml(false);

	// Check all the models exist within the scene
	if ( !ValidateModelsExist() )
    {
        return false;
    }

	if ( !ValidateSectionMethods() )
	{
		return false;
	}

	if( !ValidateSceneLengths() )
	{
		return false;
	}

	if( !ValidateWeaponProps() )
	{
		return false;
	}

	char cExportPath[RAGE_MAX_PATH];
	GetExportPath( cExportPath, sizeof(cExportPath) );

	perforce::SyncAndCheckout(cExportPath, "data_master.cutxml", m_bInteractiveMode);
	perforce::ExecuteQueuedOperations(m_bInteractiveMode);

	SetPartData();

	char cCutFilePath[RAGE_MAX_PATH];
	sprintf(cCutFilePath, "%s\\%s", cExportPath, "data_master.cutxml");
	if(!GetCutsceneFile().SaveFile(cCutFilePath))
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to save file: %s", cCutFilePath);
		return false;
	}

	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		RexRageCutscenePart* pPart =  GetCutscenePartFile().GetPart(i);
		if(!pPart->IsExportable()) continue;

		rexMBAnimExportCommon::SetStoryMode(pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_STORY_MODE_FLAG ));
		rexMBAnimExportCommon::SetActivePart(atString(pPart->GetName()));
		rexMBAnimExportCommon::CalculateDiscardedFrames();

		SetStartFrame(pPart->GetCutsceneFile()->GetRangeStart());
		SetEndFrame(pPart->GetCutsceneFile()->GetRangeEnd() + 1);

		// Each item will have its own story track, validate this is correct
		if( !rexMBAnimExportCommon::ValidateAndCreateStoryTrack(pPart->GetCutsceneFile(), true, m_iStartFrame, m_iEndFrame, GetSceneName()) )
		{
			return false;
		}

		if( !ValidatePartOffsets())
		{
			return false;
		}
		
		char cItemExportPath[RAGE_MAX_PATH];
		sprintf(cItemExportPath, "%s\\%s", cExportPath, pPart->GetName());
		CreateDirectory(cItemExportPath, NULL);

		atArray<SModelObject> modelObjects;
		atArray<SModelObject> otherModelObjects;
		GenerateModelObjectList(modelObjects);
		CleanupRedundantExportFiles(cItemExportPath, modelObjects, otherModelObjects);

		char cItemFilePath[RAGE_MAX_PATH];
		sprintf(cItemFilePath, "%s\\%s", cItemExportPath, "data.cutxml");

		char cItemFilePathMain[RAGE_MAX_PATH];
		sprintf(cItemFilePathMain, "%s\\%s.cutxml", cExportPath, pPart->GetName());

		char cItemFacePath[RAGE_MAX_PATH];
		sprintf(cItemFacePath, "%s\\%s\\faces", cExportPath, pPart->GetName());

		pPart->SetCutsceneFileName(cItemFilePathMain);
		pPart->GetCutsceneFile()->SetSceneName(pPart->GetName());
		pPart->GetCutsceneFile()->SetFaceDirectory(cItemFacePath);
		
		perforce::SyncAndCheckout(cItemFilePath, m_bInteractiveMode);
		perforce::ExecuteQueuedOperations(m_bInteractiveMode);

		if(!pPart->SaveFile(cItemFilePath))
		{
			return false;
		}

		perforce::SyncAndCheckout(cItemFilePathMain, m_bInteractiveMode);
		perforce::ExecuteQueuedOperations(m_bInteractiveMode);

		// Copy the cutfile from the sub item folder to the main scene folder
		CopyFile(cItemFilePath, cItemFilePathMain, false);

		if(!ValidateHandles())
		{
			return false;
		}
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::LoadCutsceneDataFromFile( const char *pFilename )
{
    if ( !m_cutsceneData.LoadFromFile( pFilename ) )
    {
        return false;
    }

    LoadEventDefsFile();
    return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::LoadCutsceneDataFromString( const char* pInput )
{
    if ( !m_cutsceneData.LoadFromString( pInput ) )
    {
        return false;
    }

    LoadEventDefsFile();
    return true;
}
//-------------------------------------------------
void RexRageCutsceneExport::PrepareCutfileForAnimationExport()
{
    // must turn the sectioned flag off when exporting animations
    atBitSet &cutsceneFlags = const_cast<atBitSet &>( GetCutsceneFile().GetCutsceneFlags() );
    cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG, false );

    // must set the "dual flag" off when exporting ped models
    atArray<cutfObject *> modelObjectList;
    GetCutsceneFile().FindObjectsOfType( CUTSCENE_MODEL_OBJECT_TYPE, modelObjectList );
    for ( int i = 0; i < modelObjectList.GetCount(); ++i )
    {
        cutfModelObject *pModelObject = dynamic_cast<cutfModelObject *>( modelObjectList[i] );
        if ( pModelObject->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
        {
            cutfPedModelObject *pPedModelObject = dynamic_cast<cutfPedModelObject *>( pModelObject );
            pPedModelObject->SetFaceAndBodyAreMerged( false );
        }
    }
}
//-------------------------------------------------
void RexRageCutsceneExport::CalculateBoundingBox(const HFBModel& model, FBVector3d& min, FBVector3d& max)
{
	// Getting the bounding box is not enough, its the wrong side of useless. We need to factor in the translation
	// and scaling to get the correct size/position.

	FBVector3d scaling, translation;
	model->GetBoundingBox(min,max);
	model->GetVector(scaling,kModelScaling);
	model->GetVector(translation,kModelTranslation);

	min[0] = min[0] * scaling[0];
	min[1] = min[1] * scaling[1];
	min[2] = min[2] * scaling[2];

	max[0] = max[0] * scaling[0];
	max[1] = max[1] * scaling[1];
	max[2] = max[2] * scaling[2];

	min[0] = min[0] + translation[0];
	min[1] = min[1] + translation[1];
	min[2] = min[2] + translation[2];

	max[0] = max[0] + translation[0];
	max[1] = max[1] + translation[1];
	max[2] = max[2] + translation[2];	
}
//-------------------------------------------------
bool RexRageCutsceneExport::BoundingBoxCollision(const FBVector3d& min, const FBVector3d& max, const FBVector3d& objectPosition)
{
	// Check the bounding box vs an object to see if the object is within the box or not

	return objectPosition[0] <= max[0] && objectPosition[0] >= min[0] &&
		objectPosition[1] <= max[1] && objectPosition[1] >= min[1] &&
		objectPosition[2] <= max[2] && objectPosition[2] >= min[2];
}
//-------------------------------------------------
void RexRageCutsceneExport::GenerateModelObjectList(atArray<SModelObject>& modelObjects)
{
	const atArray<cutfObject *>& objectList = GetCutsceneFile().GetObjectList();

	for(int i=0 ; i < objectList.GetCount(); ++i)
	{
		if(!IsObjectAnimatable(objectList[i]))
		{
			continue;
		}
		
		SModelObject modelObject;
		modelObject.pModel = rexMBAnimExportCommon::FindModelForObject( objectList[i] );
		modelObject.pObject = objectList[i];

		modelObjects.Grow() = modelObject;
	}
}
//-------------------------------------------------
bool RexRageCutsceneExport::WriteToybox(atArray<SModelObject>& modelObjects)
{
	ULOGGER_MOBO.SetProgressCaption( "Calculating toybox memory logs for animation(s) in one pass." );

	HFBModel toyBoxModel = RAGEFindModelByName("ToyBox");
	if(toyBoxModel != NULL)
	{
		FBVector3d min,max;
		CalculateBoundingBox(toyBoxModel, min, max);

		for ( int i = 0; i < modelObjects.GetCount(); ++i )
		{
			modelObjects[i].pToyboxEntry->Reset();

			if ( modelObjects[i].pModel != NULL )
			{
				cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( modelObjects[i].pObject );

				// Ignore any models which don't sub from model object. ie. HiddenModelObject/FixedObjects. 
				cutfModelObject* pModelObject = dynamic_cast<cutfModelObject *>( modelObjects[i].pObject );
				if(pModelObject == NULL)
				{
					continue;
				}

				std::string strModelName(pNamedObject->GetName().GetCStr());

				int colonPos = (int)strModelName.find_last_of(":");
				if(colonPos != -1)
				{
					strModelName.replace( colonPos+1, strModelName.length()-colonPos, "mover");
					
					atString strName(pNamedObject->GetName().GetCStr());
					strName[colonPos] = 0;
					
					HFBModel moverModel = RAGEFindModelByName((char*)strModelName.c_str());
					if(moverModel != NULL)
					{
						modelObjects[i].pModel = moverModel; // set the mover model, this is what we want to check
						modelObjects[i].pToyboxEntry->strName = strName;
						modelObjects[i].pToyboxEntry->bValid = true;
					}
					else
					{
						modelObjects[i].pToyboxEntry->bValid = false;

						bool bError=true;

						// Need to void any objects which dont actually have movers
						cutfParticleEffectObject *pParticleEffect = dynamic_cast<cutfParticleEffectObject *>( modelObjects[i].pObject );
						cutfAnimatedParticleEffectObject *pAnimatedParticleEffect = dynamic_cast<cutfAnimatedParticleEffectObject *>( modelObjects[i].pObject );
						cutfLightObject *pLight = dynamic_cast<cutfLightObject *>( modelObjects[i].pObject );
						cutfCameraObject *pCamera = dynamic_cast<cutfCameraObject *>( modelObjects[i].pObject );
						cutfRayfireObject *pRayfire = dynamic_cast<cutfRayfireObject *>( modelObjects[i].pObject );
						
						if(pAnimatedParticleEffect != NULL || pParticleEffect != NULL || pLight != NULL || pCamera != NULL || pRayfire != NULL)
						{
							bError = false;
						}

						if(bError)
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: Toybox - Unable to find the mover for model '%s', named '%s'.", pNamedObject->GetName(), strModelName.c_str() );
							return false;
						}
					}
				}
			}
		}

		// Now every frame query the position of the mover
		float fStartTime = m_iStartFrame / rexMBAnimExportCommon::GetFPS();
		float fEndTime = m_iEndFrame / rexMBAnimExportCommon::GetFPS();
		int nFrames = int((fEndTime - fStartTime + TIME_TO_FRAME_TOLERANCE) * rexMBAnimExportCommon::GetFPS()) + 1;
		
		double deltaTime = 1.0 / (double)rexMBAnimExportCommon::GetFPS();

		FBSystem system;
		FBPlayerControl controller;   
		FBTime preSampleTime = system.LocalTime;

		for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); ++i )
		{
			//Set the Motion Builder time and evaluate the scene
			double t = fStartTime + (deltaTime * (double)i);
			FBTime fbt;
			fbt.SetSecondDouble( t );
			controller.Goto( fbt );

			int iFrame = fStartTime*30.0f;
			
			if(rexMBAnimExportCommon::DiscardFrame(i + iFrame))
				continue;

			FBTime lFrameTime = FBTime(0,0,0,i + iFrame);

			for( int j=0; j < modelObjects.GetCount(); ++j)
			{
				SToyboxEntry& toybox = *(modelObjects[j].pToyboxEntry);
				FBEvaluateInfo eval;
				modelObjects[j].pModel->EvaluateAnimationNodes(&eval);

				FBVector3d position;
				modelObjects[j].pModel->GetVector(position, kModelTranslation);

				bool bInTheBox = BoundingBoxCollision(min, max, position);

				if(bInTheBox)
				{
					if(!toybox.bPreviouslyDropped)
					{
						toybox.frameList.Grow() = i + ((i != 0) ? iFrame : 0);
						toybox.dropListFrames.Grow() = i + ((i != 0) ? iFrame : 0); // Push into mobo space
						toybox.bPreviouslyLoaded=false;
						toybox.bPreviouslyDropped=true;
					}
				}
				else
				{
					if(!toybox.bPreviouslyLoaded)
					{
						toybox.frameList.Grow() = i + ((i != 0) ? iFrame : 0);
						toybox.loadListFrames.Grow() = i + ((i != 0) ? iFrame : 0); // Push into mobo space
						toybox.bPreviouslyDropped=false;
						toybox.bPreviouslyLoaded=true;
					}
				}
			}
		}

		controller.Goto(preSampleTime);

		// Re-jig these to remove and shift any discarded frame entries
		if(rexMBAnimExportCommon::GetStoryMode())
		{
			for( int j=0; j < modelObjects.GetCount(); ++j)
			{
				SToyboxEntry& toyboxEntry = *(modelObjects[j].pToyboxEntry);

				for(int i=0; i < toyboxEntry.dropListFrames.GetCount(); ++i)
				{
					toyboxEntry.dropListFrames[i] = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames(toyboxEntry.dropListFrames[i]);
				}

				for(int i=0; i < toyboxEntry.loadListFrames.GetCount(); ++i)
				{
					toyboxEntry.loadListFrames[i] = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames(toyboxEntry.loadListFrames[i]);
				}
			}
		}

		// Write out the stats

		char cExportPath[RAGE_MAX_PATH];
		GetExportPath( cExportPath, sizeof(cExportPath) );

		char cToyboxPath[RAGE_MAX_PATH];
		sprintf( cToyboxPath, "%s\\toybox", cExportPath );

		rexMBAnimExportCommon::DeleteFiles(cToyboxPath);
		CreateDirectory(cToyboxPath, NULL);

		for( int j=0; j < modelObjects.GetCount(); ++j)
		{
			SToyboxEntry& toyboxEntry = *(modelObjects[j].pToyboxEntry);
			if (toyboxEntry.bValid == false)
				continue;

			char cPath[128];
			sprintf( cPath, "%s/%s.txt", cToyboxPath, toyboxEntry.strName.c_str() );

			char cFile[128];
			sprintf( cFile, "%s.txt", toyboxEntry.strName.c_str());

			perforce::SyncAndCheckout(cToyboxPath, cFile, m_bInteractiveMode );
		}

		perforce::ExecuteQueuedOperations(m_bInteractiveMode);

		for( int j=0; j < modelObjects.GetCount(); ++j)
		{
			SToyboxEntry& toyboxEntry = *(modelObjects[j].pToyboxEntry);
			if (toyboxEntry.bValid == false)
				continue;

			char cPath[128];
			sprintf( cPath, "%s/%s.txt", cToyboxPath, toyboxEntry.strName.c_str() );

			char cFile[128];
			sprintf( cFile, "%s.txt", toyboxEntry.strName.c_str());

			for( int k=0; k < toyboxEntry.frameList.GetCount(); ++k)
			{
				toyboxEntry.frameList[k] = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames(toyboxEntry.frameList[k]);
			}

			std::ofstream tbFile (cPath);
			if (tbFile.is_open())
			{
				tbFile << "[Loaded]\n";
				for( int k=0; k < toyboxEntry.loadListFrames.GetCount(); ++k)
				{
					tbFile << toyboxEntry.loadListFrames[k] << "\n";
				}

				tbFile << "\n[Dropped]\n";
				for( int k=0; k < toyboxEntry.dropListFrames.GetCount(); ++k)
				{
					tbFile << toyboxEntry.dropListFrames[k] << "\n";
				}

				tbFile.close();
			}
		}

		return true;	
	}

	ULOGGER_MOBO.SetProgressMessage( "ERROR: Scene does not contain a 'ToyBox' (case sensitive)." );

	return false;
}
//-------------------------------------------------
void RexRageCutsceneExport::SetConstraints(bool bEnabled)
{
	// enable the constraints related to the toybox, turn them on when we write out the files but
	// turn off when we are exporting models so we dont interfere with the animation

	FBSystem system;
	int nConstraints = system.Scene->Constraints.GetCount();
	for(int constraintId = 0; constraintId < nConstraints; constraintId++)
	{
		HFBConstraint hConstraint = system.Scene->Constraints[constraintId];

		HFBFolder fFolder = hConstraint->Folder;
		if(fFolder != NULL)
		{
			char* pFolderName = (char*)FBComponent::FBComponentGetName(fFolder);
			if(pFolderName != NULL)
			{
				if(strcmpi(pFolderName, "ToyBoxFolder") == 0)
				{
					hConstraint->Active = bEnabled;
				}
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExport::SetVehicleConstraints(bool bEnabled)
{
	FBSystem system;
	int nConstraints = system.Scene->Constraints.GetCount();
	for(int constraintId = 0; constraintId < nConstraints; constraintId++)
	{
		HFBConstraint hConstraint = system.Scene->Constraints[constraintId];

		// constraint_connect is the contraint used for vehicle rigs, we disable this so the exported data is correct - url:bugstar:1912861
		if(stricmp(hConstraint->Name.AsString(), "connect_relation") == 0)
		{
			hConstraint->Active = bEnabled;
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExport::CleanupRedundantExportFiles(const char* pExportPath, atArray<SModelObject> &modelObjects, atArray<SModelObject> &otherModelObjects)
{
	perforce::Sync(pExportPath, "...", false, m_bInteractiveMode);

	atArray<atString> animFilenameList;
	for ( int i=0; i < modelObjects.GetCount(); ++i )
	{
		GetExportFilenames( pExportPath, modelObjects[i].pObject, animFilenameList, true );
	}

	for ( int i=0; i < otherModelObjects.GetCount(); ++i )
	{
		GetExportFilenames( pExportPath, otherModelObjects[i].pObject, animFilenameList, true );
	}

	char czDataFile[RAGE_MAX_PATH];
	sprintf(czDataFile, "%s\\%s",pExportPath,"data.cutxml");
	animFilenameList.Grow() = czDataFile;

	char czDataCutFile[RAGE_MAX_PATH];
	sprintf(czDataCutFile, "%s\\%s",pExportPath,"data.cutpart");
	animFilenameList.Grow() = czDataCutFile;

	char czDataStreamFile[RAGE_MAX_PATH];
	sprintf(czDataStreamFile, "%s\\%s",pExportPath,"data_stream.cutxml");
	animFilenameList.Grow() = czDataStreamFile;

	char czDataMasterFile[RAGE_MAX_PATH];
	sprintf(czDataMasterFile, "%s\\%s",pExportPath,"data_master.cutxml");
	animFilenameList.Grow() = czDataMasterFile;

	char czDataLightFile[RAGE_MAX_PATH];
	sprintf(czDataLightFile, "%s\\%s",pExportPath,"data.lightxml");
	animFilenameList.Grow() = czDataLightFile;

	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		char czPartFile[RAGE_MAX_PATH];
		sprintf(czPartFile, "%s\\%s.cutxml", pExportPath, GetCutscenePartFile().GetPart(i)->GetName());
		animFilenameList.Grow() = czPartFile;
	}

	atArray<atString> exportFilenames;
	ASSET.EnumFiles( pExportPath, &CB_FindFirstFileAll, &exportFilenames );

	for(int i=0; i < exportFilenames.GetCount(); ++i)
	{
		char czFullPath[RAGE_MAX_PATH];
		sprintf(czFullPath, "%s\\%s", pExportPath, exportFilenames[i].c_str());
		exportFilenames[i] = atString(czFullPath);
	}

	char czFaceDir[RAGE_MAX_PATH];
	sprintf(czFaceDir, "%s\\%s", pExportPath, "faces");

	atArray<atString> faceFilenames;
	ASSET.EnumFiles( czFaceDir, &CB_FindFirstFileAll, &faceFilenames );

	for(int i=0; i < faceFilenames.GetCount(); ++i)
	{
		char fullPath[RAGE_MAX_PATH];
		sprintf(fullPath, "%s\\%s", czFaceDir, faceFilenames[i].c_str());
		exportFilenames.Grow() = atString(fullPath);
	}

	for(int i=0; i < exportFilenames.GetCount(); ++i)
	{
		atString atFile(exportFilenames[i].c_str());

		bool bFound=false;
		for(int j=0; j < animFilenameList.GetCount(); ++j)
		{
			atString atFileCompare(animFilenameList[j].c_str());

			if(stricmp(atFile.c_str(),atFileCompare.c_str()) == 0)
			{
				bFound=true;
			}
		}

		if(!bFound)
		{
			char cFileName[MAX_PATH];
			ASSET.RemoveExtensionFromPath(cFileName, MAX_PATH, (char*)ASSET.FileName(atFile.c_str()));

			if(rexMBAnimExportCommon::IsLightFile(pExportPath,cFileName)) continue;

			// Marking for delete will delete the file from disk
			perforce::SyncAndMarkForDelete(atFile.c_str(), m_bInteractiveMode);

			char czToyboxFile[RAGE_MAX_PATH];
			sprintf(czToyboxFile, "%s\\toybox\\%s.txt", pExportPath, cFileName);

			perforce::SyncAndMarkForDelete(czToyboxFile, m_bInteractiveMode);
		}
	}

	perforce::ExecuteQueuedOperations(m_bInteractiveMode);
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateOverlappingRanges()
{
	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		RexRageCutscenePart* pPart = GetCutscenePartFile().GetPart(i);

		for(int j=0; j < GetCutscenePartFile().GetNumParts(); ++j)
		{
			if(i == j) continue;

			RexRageCutscenePart* pPartCompare = GetCutscenePartFile().GetPart(j);

			if(pPartCompare->GetCutsceneFile()->GetRangeStart() > pPart->GetCutsceneFile()->GetRangeStart() &&
				pPartCompare->GetCutsceneFile()->GetRangeStart() < pPart->GetCutsceneFile()->GetRangeEnd())
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Parts '%s' and '%s' have overlapping ranges.", pPartCompare->GetName(), pPart->GetName());
				return false;
			}

			if(pPartCompare->GetCutsceneFile()->GetRangeEnd() > pPart->GetCutsceneFile()->GetRangeStart() &&
				pPartCompare->GetCutsceneFile()->GetRangeEnd() < pPart->GetCutsceneFile()->GetRangeEnd())
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Parts '%s' and '%s' have overlapping ranges.", pPartCompare->GetName(), pPart->GetName());
				return false;
			}

			if(pPartCompare->GetCutsceneFile()->GetRangeStart() > pPart->GetCutsceneFile()->GetRangeStart() &&
				pPartCompare->GetCutsceneFile()->GetRangeEnd() < pPart->GetCutsceneFile()->GetRangeEnd())
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Parts '%s' and '%s' have overlapping ranges.", pPartCompare->GetName(), pPart->GetName());
				return false;
			}
		}
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateSectionMethods()
{
	int iSectionMethod = -1;
	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		RexRageCutscenePart* pPart = GetCutscenePartFile().GetPart(i);

		if(iSectionMethod != -1 && iSectionMethod != 0 && pPart->GetCutsceneFile()->GetSectionMethod() != 0 && pPart->GetCutsceneFile()->GetSectionMethod() != iSectionMethod)
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Parts must all use the same section method.");
			return false;
		}

		iSectionMethod = pPart->GetCutsceneFile()->GetSectionMethod();
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateSceneLengths()
{
	int iFrames = 0;

	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		RexRageCutscenePart* pPart = GetCutscenePartFile().GetPart(i);

		iFrames += (pPart->GetCutsceneFile()->GetRangeEnd() - pPart->GetCutsceneFile()->GetRangeStart());
	}

	if(iFrames < CUTSCENE_MINIMUM_LENGTH_FRAMES)
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Full scene length must be larger than the minimum scene length of %d frames.", CUTSCENE_MINIMUM_LENGTH_FRAMES);
		return false;
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateWeaponProps()
{
	for(int i=0; i <GetCutsceneFile().GetObjectList().GetCount(); ++i)
	{
		cutfPropModelObject* pPropObject = dynamic_cast<cutfPropModelObject*>(GetCutsceneFile().GetObjectList()[i]);
		if(pPropObject)
		{
			atString strPropName(pPropObject->GetName().GetCStr());
			strPropName.Lowercase();

			if(strPropName.StartsWith("w_"))
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Model '%s' must be added to the exporter as a 'Weapon Model' not a 'Prop Model'", pPropObject->GetName());
				return false;
			}
		}
	}

	return true;
}
//-------------------------------------------------
void RexRageCutsceneExport::CheckoutData(const char* pExportPath, const char* pFaceExportPath, atArray<SModelObject> &modelObjects)
{
	for(int i=0; i < modelObjects.GetCount(); ++i)
	{
		if( !rexMBAnimExportCommon::GetPerforceIntegration() ) break;

		SModelObject m = modelObjects[i];

		char czAnimPath[RAGE_MAX_PATH];
		sprintf_s(czAnimPath, "%s.anim", m.pObject->GetDisplayName().c_str(), sizeof(czAnimPath));

		char czClipPath[RAGE_MAX_PATH];
		sprintf_s(czClipPath, "%s.clip", m.pObject->GetDisplayName().c_str(), sizeof(czAnimPath));

		perforce::SyncAndCheckout(pExportPath, czAnimPath, m_bInteractiveMode);
		perforce::SyncAndCheckout(pExportPath, czClipPath, m_bInteractiveMode);

		cutfPedModelObject* pPedModel = dynamic_cast<cutfPedModelObject*>(m.pObject);
		if(pPedModel)
		{
			if(pPedModel->GetFaceAnimationNodeName().GetCStr() != NULL && (stricmp(pPedModel->GetFaceAnimationNodeName().GetCStr(),"") != 0))
			{
				CreateDirectory(pFaceExportPath,NULL);

				atString atFace(pPedModel->GetFaceAnimationFilename(pFaceExportPath).GetCStr());

				char czFaceWithoutExtension[MAX_PATH];
				ASSET.RemoveExtensionFromPath(czFaceWithoutExtension,MAX_PATH,atFace.c_str());

				char czFaceAnim[MAX_PATH];
				char czFaceClip[MAX_PATH];
				sprintf( czFaceAnim, "%s.anim", czFaceWithoutExtension);
				sprintf( czFaceClip, "%s.clip", czFaceWithoutExtension);

				perforce::SyncAndCheckout(czFaceAnim, m_bInteractiveMode);
				perforce::SyncAndCheckout(czFaceClip, m_bInteractiveMode);
			}
		}
	}

	perforce::ExecuteQueuedOperations(m_bInteractiveMode);
}
//-------------------------------------------------
void GetSubDirs(const char* pDir, atArray<atString>& atDirs)
{
	// Dir/*
	WIN32_FIND_DATA dirData;
	HANDLE dir = FindFirstFileEx( pDir, FindExInfoStandard, &dirData, 
		FindExSearchLimitToDirectories, NULL, 0 );

	while ( FindNextFile( dir, &dirData ) != 0 )
	{
		if(dirData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			atDirs.PushAndGrow(atString(dirData.cFileName));
		}
	}
}
//-------------------------------------------------
bool RexRageCutsceneExport::BuildDictionary(const char* pSrcDir)
{
	FBApplication application;
	const char* pFullPath = (const char*)application.FBXFileName;

	atString outputPath(pFullPath);
	outputPath.Replace("/", "\\");

	atString strProjectRoot = rexMBAnimExportCommon::GetProjectOrDlcRootDir(outputPath);
	strProjectRoot.Replace("/", "\\");

	if(!rexMBAnimExportCommon::IsFileUnderProjectRoot(outputPath.c_str()))
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Path '%s' is not under project root.", outputPath.c_str());
		return false;
	}

	if(!rexMBAnimExportCommon::IsFileUnderProjectRoot(pSrcDir))
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Path '%s' is not under project root.", pSrcDir);
		return false;
	}

	atString atArtPath = rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString(""));

	char cScenePath[RAGE_MAX_PATH];
	sprintf(cScenePath, "%s\\animation\\cutscene\\%s", atArtPath.c_str(), strCutscenePath);

	atArray<atString> assetPathTokens;
	atString strScenePath(cScenePath);
	strScenePath.Lowercase();
	strScenePath.Replace("/", "\\");

	atString exportAssetsPath = rexMBAnimExportCommon::GetProjectOrDlcCutsceneExportDir(atString(""));

	atString missionPath(outputPath);
	missionPath.Lowercase();

	if(missionPath.StartsWith(strScenePath))
	{
		missionPath.Replace(strScenePath.c_str(), "");
	}
	else
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Output Path '%s' is not under expected path of '%s'", missionPath.c_str(), strScenePath.c_str());
		return false;
	}

	atArray<atString> missionPathTokens;
	missionPath.Split( missionPathTokens, "\\", true );

	char czZip[RAGE_MAX_PATH];
	sprintf(czZip, "%s\\%s\\%s.icd.zip", exportAssetsPath.c_str(), missionPathTokens[0].c_str(), GetSceneName());

	xmlDocPtr mDocument = xmlNewDoc(BAD_CAST "1.0");
	xmlNodePtr root_node = xmlNewNode(NULL, BAD_CAST "Assets");
	xmlDocSetRootElement(mDocument, root_node);

	xmlNodePtr elemZip = xmlNewChild(root_node, NULL, BAD_CAST "ZipArchive", NULL);
	xmlNewProp(elemZip, BAD_CAST "path", BAD_CAST czZip);

	xmlNodePtr elemDir = xmlNewChild(elemZip, NULL, BAD_CAST "Directory", NULL);
	xmlNewProp(elemDir, BAD_CAST "path", BAD_CAST pSrcDir);

	atString strPattern(atString(pSrcDir), "\\*");

	atArray<atString> subDirs;
	GetSubDirs(strPattern.c_str(), subDirs);

	for(int i=0; i < subDirs.GetCount(); ++i)
	{
		if(stricmp(subDirs[i].c_str(), "..") == 0 ||
		stricmp(subDirs[i].c_str(), "toybox") == 0 ||
		stricmp(subDirs[i].c_str(), "obj") == 0)
			continue;

		char czAbsolute[RAGE_MAX_PATH];
		sprintf(czAbsolute, "%s\\%s", pSrcDir, subDirs[i].c_str());
		atString strRelative(atString("\\"), (char*)subDirs[i].c_str());

		xmlNodePtr elemFaceDir = xmlNewChild(elemZip, NULL, BAD_CAST "Directory", NULL);
		xmlNewProp(elemFaceDir, BAD_CAST "path", BAD_CAST czAbsolute);
		xmlNewProp(elemFaceDir, BAD_CAST "destination", BAD_CAST strRelative.c_str());

		char czSubPattern[RAGE_MAX_PATH];
		sprintf(czSubPattern, "%s\\%s\\*", pSrcDir, subDirs[i].c_str());

		atArray<atString> subsubDirs;
		GetSubDirs(czSubPattern, subsubDirs);

		for(int j=0; j < subsubDirs.GetCount(); ++j)
		{
			if(stricmp(subsubDirs[j].c_str(), "..") == 0 ||
				stricmp(subsubDirs[j].c_str(), "toybox") == 0)
				continue;

			char czAbsolute[RAGE_MAX_PATH];
			sprintf(czAbsolute, "%s\\%s\\%s", pSrcDir, subDirs[i].c_str(), subsubDirs[j].c_str());
			char czRelative[RAGE_MAX_PATH];
			sprintf(czRelative, "\\%s\\%s", (char*)subDirs[i].c_str(), (char*)subsubDirs[j].c_str());

			xmlNodePtr elemFaceDir = xmlNewChild(elemZip, NULL, BAD_CAST "Directory", NULL);
			xmlNewProp(elemFaceDir, BAD_CAST "path", BAD_CAST czAbsolute);
			xmlNewProp(elemFaceDir, BAD_CAST "destination", BAD_CAST czRelative);
		}
	}

	char czXmlFile[RAGE_MAX_PATH];
	sprintf(czXmlFile, "%s\\master_icd_list.xml", exportAssetsPath.c_str());

	ASSET.CreateLeadingPath(czXmlFile);

	if(ASSET.Exists(czXmlFile, ""))
		fiDeviceLocal::GetInstance().SetAttributes( czXmlFile, fiDeviceLocal::GetInstance().GetAttributes(czXmlFile) & ~FILE_ATTRIBUTE_READONLY );

	xmlSaveFormatFileEnc(czXmlFile, mDocument, "UTF-8", 1);

	atArray<atString> contentArray;
	contentArray.PushAndGrow(atString(czXmlFile));
	return rexMBAnimExportCommon::ProcessIngameZips(contentArray, true, false, true);
}
//-------------------------------------------------
bool RexRageCutsceneExport::Export( atArray<SModelObject> &modelObjects, atArray<SModelObject> &otherModelObjects, bool bSampleCamera, bool bSample, bool /*bShowValidation*/, bool bJustFace )
{
	rexMBAnimExportCommon::ClearLogPathContents("cutscene\\process");

	ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/process/export", GetSceneName()),"rexmbrage");

	bool bResult = true;

	rexMBAnimExportCommon::SetUseClipXml(false);

	FBApplication application;

	if(!rexMBAnimExportCommon::IsFileUnderArtRoot(application.FBXFileName.AsString()))
	{
		bResult = false;
	}

	bResult = bResult && rexMBAnimExportCommon::CheckFPS();

	//Save metadata files, the exporter may referenced the internal structure.
	//Also used by external processes like the join-split program.
	bResult = bResult && SaveCutFile();
	bResult = bResult && SaveItemFile();

	char czExportPath[RAGE_MAX_PATH];
	GetExportPath(czExportPath, RAGE_MAX_PATH);

	// Toybox constraints (if any)
	SetConstraints(true);
	// Vehicle constraints (if any)
	SetVehicleConstraints(false);

	char cSceneName[RAGE_MAX_PATH];
	safecpy(cSceneName, GetSceneName(), RAGE_MAX_PATH);

	bResult = bResult && ValidateOverlappingRanges();

	bResult = bResult && ValidateModelsExist();

	//MPW - This is temp, to delete any root toybox files, these are no longer generated
	perforce::SyncAndMarkForDelete(czExportPath, "toybox\\...", m_bInteractiveMode);
	perforce::Revert(czExportPath, "....anim", m_bInteractiveMode);
	perforce::Revert(czExportPath, "....clip", m_bInteractiveMode);
	perforce::ExecuteQueuedOperations(m_bInteractiveMode);

	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		RexRageCutscenePart* pPart = GetCutscenePartFile().GetPart(i);

		if(!pPart->IsExportable()) continue;

		char cPath[RAGE_MAX_PATH];
		sprintf(cPath, "%s\\%s", GetCutscenePartFile().GetFolder(), pPart->GetName());

		CreateDirectory(cPath, NULL);

		CleanupRedundantExportFiles(cPath, modelObjects, otherModelObjects);

		char cFacePath[RAGE_MAX_PATH];
		sprintf(cFacePath, "%s\\faces", cPath);

		CheckoutData(cPath, cFacePath, modelObjects);

		rexMBAnimExportCommon::SetStoryMode(pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_STORY_MODE_FLAG ));
		rexMBAnimExportCommon::SetActivePart(atString(pPart->GetName()));
		rexMBAnimExportCommon::CalculateDiscardedFrames();

		char cItemSceneName[RAGE_MAX_PATH];
		sprintf(cItemSceneName, "%s\\%s", cSceneName, pPart->GetName());

		// Update our scenes and ranges for each item
		SetSceneName(cItemSceneName);
		SetStartFrame(pPart->GetCutsceneFile()->GetRangeStart());
		SetEndFrame(pPart->GetCutsceneFile()->GetRangeEnd() + 1);

		// If we export selected objects we need to pass the full list of objects to the toybox otherwise we get missing objects in the load list
		atArray<SModelObject> toyboxModelObjects;
		toyboxModelObjects.insert( toyboxModelObjects.end(), modelObjects.begin(), modelObjects.end() );
		if(otherModelObjects.GetCount() > 0)
			toyboxModelObjects.insert( toyboxModelObjects.end(), otherModelObjects.begin(), otherModelObjects.end() );

		rexMBAnimExportCommon::SetActivePart(atString(pPart->GetName()));

		bResult = bResult && WriteToybox( toyboxModelObjects );

		bResult = bResult && DoExport( modelObjects, bSampleCamera, bSample, bJustFace );
	}

	SetSceneName(cSceneName);
	
	SetConstraints(false);
	SetVehicleConstraints(true);

	CleanupRedundantExportFiles(czExportPath, modelObjects, otherModelObjects);

	char czFaceExportPath[RAGE_MAX_PATH];
	GetFaceExportPath(czFaceExportPath, RAGE_MAX_PATH);

	CheckoutData(czExportPath, czFaceExportPath, modelObjects);
	CheckoutData(czExportPath, czFaceExportPath, otherModelObjects);

	perforce::SyncAndCheckout(czExportPath, "data.cutxml", m_bInteractiveMode);
	perforce::ExecuteQueuedOperations(m_bInteractiveMode);

	bResult = bResult && JoinScenes();

	char cLogDir[RAGE_MAX_PATH];
	sprintf(cLogDir, "%s\\cutscene\\process", rexMBAnimExportCommon::GetDefaultLogDir().c_str());

	char cMsg[RAGE_MAX_PATH];
	sprintf(cMsg, "Success! '%s'", cLogDir);
	ULOGGER_MOBO.PostExport( cLogDir, m_bInteractiveMode, bResult, "Export Animation(s)", cMsg );

	for ( int i = 0; i < modelObjects.GetCount(); ++i )
	{
		modelObjects[i].Cleanup();
	}

	for ( int i = 0; i < otherModelObjects.GetCount(); ++i )
	{
		otherModelObjects[i].Cleanup();
	}

	return bResult;
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateModelDupes( const atArray<SModelObject>& modelObjects )
{
	// look for a dupe (^ in name), then check if the original model exists in the exporter also

	for ( int i = 0; i < modelObjects.GetCount(); ++i )
	{
		if ( modelObjects[i].pModel != NULL )
		{
			cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( modelObjects[i].pObject );

			std::string strModelName(pNamedObject->GetName().GetCStr());

			bool bFound=false;

			int index = (int)strModelName.find("^");
			if(index != -1)
			{
				std::string strModelOriginal(strModelName.c_str());
				strModelOriginal[index] = ':';
				strModelOriginal[index+1] = 0;

				for ( int j = 0; j < modelObjects.GetCount(); ++j )
				{
					cutfNamedObject *pNamedObjectCompare = dynamic_cast<cutfNamedObject *>( modelObjects[j].pObject );

					std::string strModelNameCompare(pNamedObjectCompare->GetName().GetCStr());

					if(strModelNameCompare.find(strModelOriginal.c_str()) != -1)
					{
						bFound = true;
						break;
					}
				}

				if(!bFound)
				{
					ULOGGER_MOBO.SetProgressMessage("ERROR: Model dupe found, but '%s' was not found.", strModelOriginal.c_str());
					return false;
				}
			}
		}
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateAnimationAndAssetManager()
{
	const atArray<cutfObject *>& objectList = GetCutsceneFile().GetObjectList();

	for(int i=0; i < objectList.GetCount(); ++i)
	{
		cutfAnimationManagerObject* pAnimationManager = dynamic_cast<cutfAnimationManagerObject*>(objectList[i]);
		if(pAnimationManager)
		{
			if(pAnimationManager->GetObjectId() != 1)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: AnimationManager is not set to slot 1");
				return false;
			}
		}

		cutfAssetManagerObject* pAssetManager = dynamic_cast<cutfAssetManagerObject*>(objectList[i]);
		if(pAssetManager)
		{
			if(pAssetManager->GetObjectId() != 0)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: AssetManager is not set to slot 0");
				return false;
			}
		}
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::DoExport( atArray<SModelObject> &modelObjects, bool bSampleCamera, bool bSample, bool bJustFace )
{
#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::StartTimer( "RexRageCutsceneExport::DoExport" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

	rexMBAnimExportCommon::SetBaseLayerOnAllTakes();

	if ( GetEndFrame() == 0 )
	{
		SetEndFrame( (float) s32(rexMBAnimExportCommon::GetAnimEndTime() * rexMBAnimExportCommon::GetFPS()) );
	}

	if(!ValidateModelDupes( modelObjects ))
	{
		return false;
	}

	if(!ValidateAnimationAndAssetManager())
	{
		return false;
	}

	if(!m_cutsceneData.ValidateCutsceneExportData( ))
	{
		return false;
	}

    atString animCtrlFilePath;
    if ( rexMBAnimExportCommon::GetMotionBuilderAnimCtrlFilePathSetting( animCtrlFilePath ) && !animCtrlFilePath.EndsWith( "\\" ) )
    {
        animCtrlFilePath += "\\";
    }

    char cDestDir[RAGE_MAX_PATH];
    GetExportPath( cDestDir, sizeof(cDestDir) );

    atArray<RexMbCutsceneExportModel *> sampleModelList;
    atArray<RexMbCutsceneExportModel *> keyframeModelList;
    atArray<RexAnimMarkupData *> skeletalModelList;
	atArray<SToyboxEntry*> toyboxEntries;

    // first, categorize our models and create the data structures that contain the export instructions
    for ( int i = 0; i < modelObjects.GetCount(); ++i )
    {
        if ( modelObjects[i].pModel == NULL )
        {
            cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( modelObjects[i].pObject );

            ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to find the model named '%s'.", pNamedObject->GetName() );

#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExport" );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            return false;
        }

        char cName[CUTSCENE_OBJNAMELEN];
        safecpy( cName, modelObjects[i].pObject->GetDisplayName().c_str(), sizeof(cName) );

        char cAnimFilename[RAGE_MAX_PATH];
        sprintf( cAnimFilename, "%s\\%s", cDestDir, cName );
        rexMBAnimExportCommon::ReplaceWhitespaceCharacters( cAnimFilename );

        s32 iExportType = GetAnimExportType( modelObjects[i] );
        if ( iExportType == -1 )
        {         
            ULOGGER_MOBO.SetProgressMessage("WARNING: Unable to determine the export type for object id=%d type='%s' name='%s'.", modelObjects[i].pObject->GetObjectId(), 
                modelObjects[i].pObject->GetTypeName(), modelObjects[i].pObject->GetDisplayName().c_str() );

            SetProgressPercent( 100.0f );
            ++m_iCurrentExportProgressItem;
            continue;
        }

        switch ( iExportType )
        {
        case OBJECT_FX:
            {
                RexMbCutsceneExportParticleEffectModel *pExportModel 
                    = rage_new RexMbCutsceneExportParticleEffectModel( modelObjects[i].pModel, modelObjects[i].pToyboxEntry, cAnimFilename );
                if ( bSample )
                {
                    sampleModelList.PushAndGrow( pExportModel );
                }
                else
                {
                    keyframeModelList.PushAndGrow( pExportModel );
                }
            }
            break;
        case OBJECT_LIGHT:
            {
                RexMbCutsceneExportLightModel *pExportModel = rage_new RexMbCutsceneExportLightModel( modelObjects[i].pModel, modelObjects[i].pToyboxEntry, cAnimFilename );
                if ( bSample )
                {
                    sampleModelList.PushAndGrow( pExportModel );
                }
                else
                {
                    keyframeModelList.PushAndGrow( pExportModel );
                }
            }
            break;
        case OBJECT_CAMERA:
            {
                if ( bSampleCamera )
                {
                    sampleModelList.PushAndGrow( rage_new RexMbCutsceneExportCameraModel( modelObjects[i].pModel, modelObjects[i].pToyboxEntry, cAnimFilename ) );
                }
                else
                {
                    keyframeModelList.PushAndGrow( rage_new RexMbCutsceneExportCameraModel( modelObjects[i].pModel, modelObjects[i].pToyboxEntry, cAnimFilename ) );
                }
            }
            break;
        default:
            {
				cutfPedModelObject* pPedObject = dynamic_cast<cutfPedModelObject*>(modelObjects[i].pObject);
				if(pPedObject != NULL)
				{
					if(pPedObject->GetFaceAnimationNodeName() != NULL)
					{
						if(strcmpi(pPedObject->GetFaceAnimationNodeName().GetCStr(),"") != 0)
						{
							char cNewAnimFilename[RAGE_MAX_PATH];
							char cFaceExportPath[RAGE_MAX_PATH];
							GetFaceExportPath(cFaceExportPath, sizeof(cFaceExportPath));
							rexMBAnimExportCommon::ReplaceWhitespaceCharacters( cFaceExportPath );

							sprintf( cNewAnimFilename, "%s\\%s_face", cFaceExportPath, cName);
							rexMBAnimExportCommon::ReplaceWhitespaceCharacters( cNewAnimFilename );

							CreateDirectory(cFaceExportPath, NULL);

							ProcessSkeletalData(modelObjects[i].pObject, modelObjects[i].pToyboxEntry, pPedObject->GetFaceAnimationNodeName().GetCStr(), cNewAnimFilename, animCtrlFilePath.c_str(), pPedObject->GetFaceAttributesFilename().GetCStr(), MODEL_TYPE_FACE, skeletalModelList, toyboxEntries);
						}
					}
				}

				if(!bJustFace)
					ProcessSkeletalData(modelObjects[i].pObject, modelObjects[i].pToyboxEntry, (const char*)modelObjects[i].pModel->LongName, cAnimFilename, animCtrlFilePath.c_str(), "", "", skeletalModelList, toyboxEntries);
            }
            break;
        }
    }

    m_iNumExportProgressItems = 0;
    if ( keyframeModelList.GetCount() > 0 )
    {
        ++m_iNumExportProgressItems;
    }

    if ( sampleModelList.GetCount() > 0 )
    {
        ++m_iNumExportProgressItems;
    }

    if ( skeletalModelList.GetCount() > 0 )
    {
        ++m_iNumExportProgressItems;
    }

    m_iCurrentExportProgressItem = 0;

    // second, export the keyframed non-skeletal animations
    if ( keyframeModelList.GetCount() > 0 )
    {
        bool bResult = DoExportInternal( keyframeModelList, false );

        for ( int i = 0; i < keyframeModelList.GetCount(); ++i )
        {
            delete keyframeModelList[i];
        }

        if ( !bResult )
        {
            for ( int i = 0; i < sampleModelList.GetCount(); ++i )
            {
                delete sampleModelList[i];
            }

            for ( int i = 0; i < skeletalModelList.GetCount(); ++i )
            {
                delete skeletalModelList[i];
            }

#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExport" );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            return false;
        }
    }

    // third, export the rest of the non-skeletal animations
    if ( sampleModelList.GetCount() > 0 )
    {
        bool bResult = DoExportInternal( sampleModelList, true );

        for ( int i = 0; i < sampleModelList.GetCount(); ++i )
        {
            delete sampleModelList[i];
        }

        if ( !bResult )
        {
            for ( int i = 0; i < skeletalModelList.GetCount(); ++i )
            {
                delete skeletalModelList[i];
            }

#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExport" );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            return false;
        }
    }

    // fourth, export the skeletal animations
    if ( skeletalModelList.GetCount() > 0 )
    {
        bool bResult = DoExportInternal( skeletalModelList, toyboxEntries, bSample );

        for ( int i = 0; i < skeletalModelList.GetCount(); ++i )
        {
            delete skeletalModelList[i];
        }

        if ( !bResult )
        {
#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExport" );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            return false;
        }
    }

#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExport" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    return true;
}
//-------------------------------------------------
void RexRageCutsceneExport::ProcessSkeletalData(cutfObject *pObject, SToyboxEntry* pToyboxEntry, const char* czLongName, const char* czAnimFilename, const char* czAnimCtrlFilePath, const char* czFaceAttributesFilename, const char* czModelType, atArray<RexAnimMarkupData *>& skeletalModelList, atArray<SToyboxEntry*>& toyboxEntries)
{
    char cAnimExportCtrlSpec[RAGE_MAX_PATH];
    cAnimExportCtrlSpec[0] = 0;

    cutfModelObject *pModelObject = dynamic_cast<cutfModelObject *>( pObject );
    if ( pModelObject != NULL )
    {
        ConstString specFilename;
		if(strcmpi(czModelType, MODEL_TYPE_FACE) == 0)
		{
			specFilename = pModelObject->GetFaceExportCtrlSpecFile().GetCStr() ? pModelObject->GetFaceExportCtrlSpecFile().GetCStr() : "";
		}
		else
		{
			specFilename = pModelObject->GetAnimExportCtrlSpecFile().GetCStr() ? pModelObject->GetAnimExportCtrlSpecFile().GetCStr() : "";
		}

        if ( strlen( specFilename.c_str() ) > 0 )
        {
            sprintf( cAnimExportCtrlSpec, "%s%s", czAnimCtrlFilePath, specFilename.c_str() );
        }
    }

    RexAnimMarkupData *pAnimData = rage_new RexAnimMarkupData();
	RexCharAnimData* pCharacterData = rage_new RexCharAnimData();
    pCharacterData->m_inputModelNames.Grow() = (char *)czLongName;

    pCharacterData->m_animPrefix = "";		//Setup for no animation prefixes
    pCharacterData->m_animSuffix = "";		//Setup for no animation suffixes
#if HACK_GTA4   
	// For the time being, look for a mover model called "mover"
	// We will add a way to pick the mover object per model at a later date
	std::string sRootBoneName = std::string((char *)czLongName);

	size_t colonPos = sRootBoneName.find_last_of(':');
	if(colonPos != string::npos)
		sRootBoneName = sRootBoneName.substr(0, colonPos);
	atString sMoverModelName = atString(atString(sRootBoneName.c_str()), atString(":mover"));
	
	HFBModel mover = RAGEFindModelByName(const_cast<char*>(sMoverModelName.c_str()));
	if( mover )
	{
		const char* pMoverName = (const char*)mover->LongName;
		pCharacterData->m_moverModelNames.Grow() = pMoverName;
	}
	else if(strcmpi(czModelType,MODEL_TYPE_FACE) != 0)
		pCharacterData->m_moverModelNames.Grow() = (char *)czLongName;
#else
	pCharacterData->m_moverModelNames.Grow() = "";	//Setup for no mover model export
#endif // HACK_GTA4

	if(strcmpi(czModelType,MODEL_TYPE_FACE) == 0)
	{
		pCharacterData->m_modelType = czModelType;
		if(czFaceAttributesFilename == NULL)
		{
			pAnimData->m_attributeFilePath = "";
		}
		else
		{
			pAnimData->m_attributeFilePath = czFaceAttributesFilename;
		}
	}

	pAnimData->m_characters.Grow() = pCharacterData;

    pAnimData->m_ctrlFilePath = ASSET.FileName(cAnimExportCtrlSpec);

    RexMbAnimSegmentData *pSegmentData = rage_new RexMbAnimSegmentData;
    pSegmentData->m_iStartFrame = (int) m_iStartFrame;
    pSegmentData->m_iEndFrame = (int) m_iEndFrame;
    pSegmentData->m_bAutoplay = false;
    pSegmentData->m_bLooping = false;
    pSegmentData->m_inputModelNames.Grow() = (char *)czLongName;

    pSegmentData->m_segmentName = czAnimFilename;

	pSegmentData->m_compressionFilePath = pModelObject->GetAnimCompressionFile().GetCStr();

    pAnimData->m_segments.Grow() = pSegmentData;

    pAnimData->m_useParentScale = GetCutsceneFile().GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_PARENT_SCALE_FLAG );

    skeletalModelList.PushAndGrow( pAnimData );
	toyboxEntries.PushAndGrow( pToyboxEntry );
}
//-------------------------------------------------
bool RexRageCutsceneExport::BuildAnimationDictionaries(bool bPreview)
{
	FBApplication application;
	const char* pFullPath = (const char*)application.FBXFileName;

	atString outputPath(pFullPath);
	outputPath.Replace("/", "\\");
	atArray<atString> outputPathTokens;
	outputPath.Split( outputPathTokens, "\\", true );

	atString strProjectRoot = rexMBAnimExportCommon::GetProjectOrDlcRootDir(outputPath);
	strProjectRoot.Replace("/", "\\");

	atString atArtPath = rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString(""));

	char cScenePath[RAGE_MAX_PATH];
	sprintf(cScenePath, "%s\\animation\\cutscene\\%s", atArtPath.c_str(), strCutscenePath);

	atArray<atString> assetPathTokens;
	atString strScenePath(cScenePath);
	strScenePath.Lowercase();
	strScenePath.Replace("/", "\\");
	strScenePath.Split( assetPathTokens, "\\", true );	

	atString exportAssetsPath = rexMBAnimExportCommon::GetProjectOrDlcCutsceneExportDir(atString(""));

	atString missionPath(outputPath);
	missionPath.Lowercase();
	missionPath.Replace(strScenePath.c_str(), "");

	atArray<atString> missionPathTokens;
	missionPath.Split( missionPathTokens, "\\", true );

	char czZip[RAGE_MAX_PATH];
	sprintf(czZip, "%s\\%s\\%s.icd.zip", exportAssetsPath.c_str(), missionPathTokens[0].c_str(), GetSceneName());

	atArray<atString> contentArray;
	contentArray.PushAndGrow(atString(czZip));
	return rexMBAnimExportCommon::ProcessIngameZips(contentArray, false, bPreview, !m_bInteractiveMode);
}
//-------------------------------------------------
void RexRageCutsceneExport::LoadEventDefsFile()
{
    atString eventDefsPath;
    if ( rexMBAnimExportCommon::GetMotionBuilderEventDefsPathSetting( eventDefsPath ) && ASSET.Exists( eventDefsPath.c_str(), NULL ) )
    {
        m_eventDefs.LoadFile( eventDefsPath.c_str() );
    }
}
//-------------------------------------------------
bool RexRageCutsceneExport::IsObjectAnimatable( cutfObject *pObject ) const
{
    return (pObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE)
        || (pObject->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE)
        || (pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
        || (pObject->GetType() == CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE);
}
//-------------------------------------------------
void RexRageCutsceneExport::GetExportFilenames( const char* pExportPath, const cutfObject *pObject, atArray<atString> &exportFilenameList, bool bIncludePedModelFace )
{
    char cAnimFilename[RAGE_MAX_PATH];
    sprintf( cAnimFilename, "%s\\%s.anim", pExportPath, pObject->GetDisplayName().c_str() );
    rexMBAnimExportCommon::ReplaceWhitespaceCharacters( cAnimFilename );
    if ( ASSET.Exists( cAnimFilename, NULL ) )
    {
        exportFilenameList.Grow() = cAnimFilename;

        char cClipFilename[RAGE_MAX_PATH];
if(rexMBAnimExportCommon::GetUseClipXml())
        sprintf( cClipFilename, "%s\\%s.clipxml", pExportPath, pObject->GetDisplayName().c_str() );
else
		sprintf( cClipFilename, "%s\\%s.clip", pExportPath, pObject->GetDisplayName().c_str() );

        rexMBAnimExportCommon::ReplaceWhitespaceCharacters( cClipFilename );

        if ( ASSET.Exists( cClipFilename, NULL ) )
        {
            exportFilenameList.Grow() = cClipFilename;
        }
    }

    if ( bIncludePedModelFace && (pObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE) )
    {
        const cutfModelObject *pModelObject = dynamic_cast<const cutfModelObject *>( pObject );
        if ( pModelObject->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
        {
            char cFaceDirectory[RAGE_MAX_PATH];
            m_cutsceneData.GetFaceExportPath( cFaceDirectory, sizeof(cFaceDirectory) );

            const cutfPedModelObject *pPedModelObject = dynamic_cast<const cutfPedModelObject *>( pModelObject );
            char cFaceAnimFilename[RAGE_MAX_PATH];
            safecpy( cFaceAnimFilename, pPedModelObject->GetFaceAnimationFilename( cFaceDirectory ).GetCStr(), sizeof(cFaceAnimFilename) );
            if ( (strlen( cFaceAnimFilename ) > 0) && ASSET.Exists( cFaceAnimFilename, NULL ) && (pPedModelObject->GetFaceAnimationNodeName().GetCStr() != NULL) && (strlen(pPedModelObject->GetFaceAnimationNodeName().GetCStr()) > 0) )
            {
				char cFileWithoutExtension[RAGE_MAX_PATH];
				ASSET.RemoveExtensionFromPath(cFileWithoutExtension,RAGE_MAX_PATH, ASSET.FileName(cFaceAnimFilename));

				char cAnimationFile[RAGE_MAX_PATH];
				sprintf( cAnimationFile, "%s\\faces\\%s.%s", pExportPath, cFileWithoutExtension, "anim");

				exportFilenameList.Grow() = cAnimationFile;

				char cClipFile[RAGE_MAX_PATH];
if(rexMBAnimExportCommon::GetUseClipXml())
				sprintf( cClipFile, "%s\\faces\\%s.%s", pExportPath, cFileWithoutExtension, "clipxml");
else
				sprintf( cClipFile, "%s\\faces\\%s.%s", pExportPath, cFileWithoutExtension, "clip");

				exportFilenameList.Grow() = cClipFile;
            }
        }
    }
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateModelsExist()
{
    atArray<cutfObject *> modelObjList;
    GetCutsceneFile().FindObjectsOfType( CUTSCENE_MODEL_OBJECT_TYPE, modelObjList );
	GetCutsceneFile().FindObjectsOfType( CUTSCENE_PARTICLE_EFFECT_OBJECT_TYPE, modelObjList );
	GetCutsceneFile().FindObjectsOfType( CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE, modelObjList );
	GetCutsceneFile().FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, modelObjList );
	GetCutsceneFile().FindObjectsOfType( CUTSCENE_DECAL_OBJECT_TYPE, modelObjList );
	GetCutsceneFile().FindObjectsOfType( CUTSCENE_LIGHT_OBJECT_TYPE, modelObjList );

	bool bResult = true;

    for ( int i = 0; i < modelObjList.GetCount(); ++i )
    {
        HFBModel pModel = rexMBAnimExportCommon::FindModelForObject( modelObjList[i] );
        if ( pModel == NULL )
        {
            ULOGGER_MOBO.SetProgressMessage("ERROR: Could not locate model or face model for '%s'.", modelObjList[i]->GetDisplayName().c_str() );
            bResult = false;
        }

		const cutfPedModelObject *pPedObject = dynamic_cast<const cutfPedModelObject *>( modelObjList[i] );
		if ( pPedObject != NULL )
		{
			if(pPedObject->GetFaceAnimationNodeName().GetCStr() != NULL && (strcmpi(pPedObject->GetFaceAnimationNodeName().GetCStr(),"") != 0))
			{
				HFBModel pFaceModel = RAGEFindModelByName( const_cast<char *>( pPedObject->GetFaceAnimationNodeName().GetCStr() ) );
				if ( pFaceModel == NULL )
				{
					ULOGGER_MOBO.SetProgressMessage( "ERROR:  No model was found with the name '%s'.", pPedObject->GetFaceAnimationNodeName().GetCStr() );
					bResult = false;
				}
			}
		}
    }

	return bResult;
}
//-------------------------------------------------
s32 RexRageCutsceneExport::GetAnimExportType( const SModelObject &modelObject )
{
    switch ( modelObject.pObject->GetType() )
    {
    case CUTSCENE_MODEL_OBJECT_TYPE:
        {
            const cutfModelObject *pModelObject = dynamic_cast<const cutfModelObject *>( modelObject.pObject );
            switch ( pModelObject->GetModelType() )
            {
            case CUTSCENE_PED_MODEL_TYPE:
                return OBJECT_PED;

            case CUTSCENE_VEHICLE_MODEL_TYPE:
                return OBJECT_VEHICLE;

            case CUTSCENE_PROP_MODEL_TYPE:
			case CUTSCENE_WEAPON_MODEL_TYPE:
                return OBJECT_PROP;

            default:
                return -1;
            }
        }
        break;
    case CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE:
        return OBJECT_FX;

    case CUTSCENE_LIGHT_OBJECT_TYPE:
        return OBJECT_LIGHT;

    case CUTSCENE_CAMERA_OBJECT_TYPE:
        return OBJECT_CAMERA;

    default:
        return -1;
    }
}
//-------------------------------------------------
bool RexRageCutsceneExport::DoExportInternal( atArray<RexAnimMarkupData *> &animDataList, atArray<SToyboxEntry*>& toyboxEntries, bool bSample )
{
#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::StartTimer( "RexRageCutsceneExport::DoExportInternal( skel )" );
    }
#endif //PROFILE_CUTSCENE_EXPORT    

    RexRageAnimExport animExporter;    
    animExporter.SetInteractiveMode( m_bInteractiveMode );

    // export the animation
    bool bResult = animExporter.DoExport( animDataList, bSample );

    SetProgressPercent( 90.0f );

    if ( !bResult )
    {
#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExportInternal( skel )" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        return false;
    }

    int iNumItems = animDataList.GetCount();
    int iCurrent = 1;

    float fStartTime = m_iStartFrame / rexMBAnimExportCommon::GetFPS();
    float fEndTime = m_iEndFrame / rexMBAnimExportCommon::GetFPS();

    ULOGGER_MOBO.SetProgressCaption("Serializing clip(s) for %d skeletal animation(s).", animDataList.GetCount() );

    for ( int i = 0; i < animDataList.GetCount(); ++i )
    {
#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::StartTimer( animDataList[i]->GetInputModelName( 0 ) );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        // build the clip on top of it
        RexMbCutsceneExportGenericModel exportGenericModel( NULL, toyboxEntries[i], animDataList[i]->GetSegment( 0 )->GetSegmentName());
        bResult = exportGenericModel.SerializeClip( fStartTime, fEndTime, animDataList[i] );

        SetProgressPercent( (((float)iCurrent / (float)iNumItems) * 10.0f) + 90.0f );
        ++iCurrent;

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( animDataList[i]->GetInputModelName( 0 ) );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        if ( !bResult )
        {
            break;
        }
    }

    SetProgressPercent( 100.0f );
    ++m_iCurrentExportProgressItem;

#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExportInternal( skel )" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    return bResult;
}
//-------------------------------------------------
bool RexRageCutsceneExport::ValidateFOVAndDOF( RexMbCutsceneExportModel *pExportModel )
{
	RexMbCutsceneExportCameraModel* pCameraModel = dynamic_cast<RexMbCutsceneExportCameraModel*>(pExportModel);
	if(pCameraModel)
	{
		const atArray<RexMbCutsceneExportData *>& trackExportData = pCameraModel->GetTrackExportData();

		for(int j = 0; j < trackExportData.GetCount(); ++j)
		{
			RexMbCutsceneExportFloatPropertyData* pFloatData = dynamic_cast<RexMbCutsceneExportFloatPropertyData*>(trackExportData[j]);
			if(pFloatData)
			{
				if(stricmp(pFloatData->GetPropertyName(),"FieldOfView") == 0)
				{
					const atArray<Vector3>& frameData = pFloatData->GetFrameData();
					for(int k=0; k < frameData.GetCount(); ++k)
					{
						float fov = frameData[k].x;
						if(fov < 1.f || fov > 130.f)
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: Camera FOV value '%f' is invalid on frame '%d'. Valid values are '%f' to '%f'", fov, k, 1.f, 130.f);
							return false;
						}
					}
				}
			}

			RexMbCutsceneExportDepthOfFieldData* pDOFData = dynamic_cast<RexMbCutsceneExportDepthOfFieldData*>(trackExportData[j]);
			if(pDOFData)
			{
				const atArray<Vector3>& frameData = pDOFData->GetFrameData();
				for(int k=0; k < frameData.GetCount(); ++k)
				{
					float dofNear = frameData[k].x;
					float dofFar = frameData[k].y;
					if(dofNear < 0.f)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Camera Near DOF value '%f' is invalid on frame '%d'. Must be greater than 0.", dofNear, k);
						return false;
					}
					else if(dofFar < dofNear)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Camera Far DOF value '%f' must be greater than the Near DOF value '%f' on frame '%d'.", dofFar, dofNear, k);
						return false;
					}
				}
			}
		}
	}

	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExport::DoExportInternal( atArray<RexMbCutsceneExportModel *> &exportModelList, bool bSample )
{
#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::StartTimer( "RexRageCutsceneExport::DoExportInternal( nonskel )" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    float fStartTime = m_iStartFrame / rexMBAnimExportCommon::GetFPS();
    float fEndTime = (m_iEndFrame) / rexMBAnimExportCommon::GetFPS();
    int nFrames = int((fEndTime - fStartTime + TIME_TO_FRAME_TOLERANCE) * rexMBAnimExportCommon::GetFPS()) + 1;

    // Create the collectors
    for ( int i = 0; i < exportModelList.GetCount(); ++i )
    {
        char cName[1024];
        safecpy( cName, (const char*)exportModelList[i]->GetModel()->LongName, sizeof(cName) );

        ULOGGER_MOBO.SetProgressCaption("Initializing animation for model '%s'.", cName );

        if ( !exportModelList[i]->Initialize( nFrames ) )
        {
#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExportInternal( nonskel )" );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            return false;
        }
    }
    
    int iCurrentStep = 1;
    bool bResult = true;

    // collection the data
    if ( bSample )
    {
        int iNumSteps = exportModelList.GetCount();

        ULOGGER_MOBO.SetProgressCaption("Converting %d non-skeletal animation(s) in one pass.", iNumSteps );

        double deltaTime = 1.0 / (double)rexMBAnimExportCommon::GetFPS();

        FBSystem system;
        FBPlayerControl controller;       

        // We should only update the progress percent every few frames
        int iProgressIncrement = (int)((float)nFrames / 10.0f);
        int iProgressStep = iProgressIncrement;

        FBTime preSampleTime = system.LocalTime;

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::StartTimer( "RexMbCutsceneExportModel::CollectAnimData( sample )" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

		int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);

        for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); i++ )
        {
			if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

            //Set the Motion Builder time and evaluate the scene
            double t = fStartTime + (deltaTime * (double)i);
            FBTime fbt;
            fbt.SetSecondDouble( t );
            controller.Goto( fbt );

            system.Scene->Evaluate();

            for ( int j = 0; j < exportModelList.GetCount(); ++j )
            {
                exportModelList[j]->CollectAnimData();
            }            

            if ( i == iProgressStep )
            {
                SetProgressPercent( ((float)i / (float)nFrames) * 90.0f );
                iProgressStep += iProgressIncrement;
            }
        }

        controller.Goto( preSampleTime );

		////Validate the FOV and DOF
		for ( int i = 0; i < exportModelList.GetCount(); ++i )
		{
			if(!ValidateFOVAndDOF( exportModelList[i] ))
			{
				return false;
			}
		}

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( "RexMbCutsceneExportModel::CollectAnimData( sample )" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        ULOGGER_MOBO.SetProgressCaption("Serializing anim(s) and clip(s) for %d non-skeletal animation(s).", exportModelList.GetCount() );

        iNumSteps = exportModelList.GetCount() * 2;
        iCurrentStep = 1;

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::StartTimer( "Serialize Animations and Clips" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        // save the anims and clips
        for ( int i = 0; i < exportModelList.GetCount(); ++i )
        {
            char cName[1024];
            safecpy( cName, (const char*)exportModelList[i]->GetModel()->LongName, sizeof(cName) );

#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::StartTimer( cName );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            bResult = exportModelList[i]->SerializeAnimation( fStartTime, fEndTime, nFrames );

            SetProgressPercent( (((float)iCurrentStep / (float)iNumSteps) * 10.0f) + 90.0f );
            ++iCurrentStep;

            if ( bResult )
            {
                bResult = exportModelList[i]->SerializeClip( fStartTime, fEndTime );
            }

            SetProgressPercent( (((float)iCurrentStep / (float)iNumSteps) * 10.0f) + 90.0f );
            ++iCurrentStep;

#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( cName );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            if ( !bResult )
            {
                break;
            }
        }

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( "Serialize Animations and Clips" );
        }
#endif //PROFILE_CUTSCENE_EXPORT
    }
    else
    {
        int iNumSteps = exportModelList.GetCount() * 3;

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::StartTimer( "Convert and Serialize Animations and Clips" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        for ( int i = 0; i < exportModelList.GetCount(); ++i )
        {
            char cName[1024];
            safecpy( cName, (const char*)exportModelList[i]->GetModel()->LongName, sizeof(cName) );
            
#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::StartTimer( cName );
            }
#endif //PROFILE_CUTSCENE_EXPORT
            
            ULOGGER_MOBO.SetProgressCaption("Converting animation for model '%s'", cName );

            exportModelList[i]->CollectAnimData( fStartTime, fEndTime, nFrames );

			//Validate the FOV and DOF
			if(!ValidateFOVAndDOF( exportModelList[i] ))
			{
				return false;
			}

            SetProgressPercent( ((float)iCurrentStep / (float)iNumSteps) * 100.0f );
            ++iCurrentStep;

            bResult = exportModelList[i]->SerializeAnimation( fStartTime, fEndTime, nFrames );

            SetProgressPercent( ((float)iCurrentStep / (float)iNumSteps) * 100.0f );
            ++iCurrentStep;

            if ( bResult )
            {
                bResult = exportModelList[i]->SerializeClip( fStartTime, fEndTime );
            }

            SetProgressPercent( ((float)iCurrentStep / (float)iNumSteps) * 100.0f );
            ++iCurrentStep;

#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( cName );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            if ( !bResult )
            {
                break;
            }
        }

#if PROFILE_CUTSCENE_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( "Convert and Serialize Animations and Clips" );
        }
#endif //PROFILE_CUTSCENE_EXPORT
    }

    SetProgressPercent( 100.0f );
    ++m_iCurrentExportProgressItem;

#if PROFILE_CUTSCENE_EXPORT
    if ( rexMBTimerLog::IsTimerLogEnabled() )
    {
        rexMBTimerLog::EndTimer( "RexRageCutsceneExport::DoExportInternal( nonskel )" );
    }
#endif //PROFILE_CUTSCENE_EXPORT

    return bResult;
}
//-------------------------------------------------
void RexRageCutsceneExport::SetProgressPercent( float fPercent ) 
{
    float fItemRange = 100.0f / (float)m_iNumExportProgressItems;
    float fBatchProgress = (fPercent / 100.0f) * fItemRange;
    if ( m_iCurrentExportProgressItem > 0 )
    {
        fBatchProgress += (m_iCurrentExportProgressItem * fItemRange);
    }

    if ( fBatchProgress < 0.0f )
    {
        fBatchProgress = 0.0f;
    }
    else if ( fBatchProgress > 100.0f )
    {
        fBatchProgress = 100.0f;
    }

	ULOGGER_MOBO.SetProgressPercent( fBatchProgress );
}
//-------------------------------------------------
void RexRageCutsceneExport::RemoveObjects()
{
	atArray<cutfObject*> &objectList = const_cast<atArray<cutfObject*> &>(GetCutsceneFile().GetObjectList() );
	
	for(int i=0 ; i < objectList.GetCount(); ++i)
	{
		cutfObject* pObject = objectList[i];

		if(pObject->GetType() == CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE ||
			pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
		{
			GetCutsceneFile().RemoveObject(pObject);
			--i;
		}
		else if(pObject->GetType() == CUTSCENE_SUBTITLE_OBJECT_TYPE)
		{
			GetCutsceneFile().RemoveObject(pObject);
			--i;
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExport::ChangeStreamProcessedEvents()
{
	// any rag edited events will be in zero space, we need to move these back into motionbuilder space. To do this we have to work out which
	// part of our internal concat they are within and use the stored start range in the concatdatalist entry to push it forward. 
	// we also need to take into account any discarded frames within this part.

	Assert(GetCutsceneFile().GetConcatDataList().GetCount() == GetCutsceneFile().GetDiscardedFrameList().GetCount());

	// data.cutxml from new exporter is pretty much a stream as it contains the streams of the shots
	//if(GetCutsceneFile().GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_STREAM_PROCESSED)) 
	{
		atArray<cutfEvent*>& eventList = const_cast<atArray<cutfEvent*> &>(GetCutsceneFile().GetEventList() );

		for(int i=0; i < eventList.GetCount(); ++i)
		{
			cutfEvent* pEvent = eventList[i];

			if(rexMBAnimExportCommon::IsRagEvent(pEvent->GetEventId()))
			{
				if(GetCutsceneFile().GetConcatDataList().GetCount() > 0)
				{
					float fDurationSum = 0;

					for(int j=0; j < GetCutsceneFile().GetConcatDataList().GetCount(); ++j)
					{
						cutfCutsceneFile2::SConcatData entry = GetCutsceneFile().GetConcatDataList()[j];

						float fDuration = (((entry.iRangeEnd - entry.iRangeStart) +1 ) / rexMBAnimExportCommon::GetFPS()) + fDurationSum;

						if(pEvent->GetTime() < (fDuration + 0.001))
						{
							int iFrame = float(int(pEvent->GetTime()*rexMBAnimExportCommon::GetFPS() + 0.5f));//pEvent->GetTime() * rexMBAnimExportCommon::GetFPS();
							// add the start range to push the event time into motionbuilder space, then subtract the 
							// duration of previous scenes from the time as we need the event in its own scene space.
							iFrame += entry.iRangeStart - (fDurationSum * rexMBAnimExportCommon::GetFPS());

							// check discard frames and move forward if within
							cutfCutsceneFile2::SDiscardedFrameData e = GetCutsceneFile().GetDiscardedFrameList()[j];
							iFrame = rexMBAnimExportCommon::GetFrameAfterImport(iFrame, e.frames);

							pEvent->SetTime(iFrame / rexMBAnimExportCommon::GetFPS());
							break;
						}

						fDurationSum += (((entry.iRangeEnd - entry.iRangeStart) +1 ) / rexMBAnimExportCommon::GetFPS());
					}
				}
				else
				{
					pEvent->SetTime(pEvent->GetTime() + (GetCutsceneFile().GetRangeStart() / rexMBAnimExportCommon::GetFPS())); // Events are 0 based from RAG or in stream
					if(GetCutsceneFile().GetDiscardedFrameList().GetCount() > 0)
					{
						pEvent->SetTime(rexMBAnimExportCommon::GetFrameAfterImport((int)(pEvent->GetTime() * rexMBAnimExportCommon::GetFPS()), GetCutsceneFile().GetDiscardedFrameList()[0].frames) / rexMBAnimExportCommon::GetFPS());
					}
				}
			}
		}	
	}
}
//-------------------------------------------------
void RexRageCutsceneExport::RemoveEvents()
{
	atArray<cutfEvent *> &loadEventList = const_cast<atArray<cutfEvent *> &>( GetCutsceneFile().GetLoadEventList() );
	atArray<cutfEvent *> &eventList = const_cast<atArray<cutfEvent *> &>( GetCutsceneFile().GetEventList() );

	for(int i=0; i < loadEventList.GetCount(); ++i)
	{
		GetCutsceneFile().RemoveLoadEvent(loadEventList[i]);
		i--;
	}

	for(int i=0; i < eventList.GetCount(); ++i)
	{
		cutfEvent* pEvent = eventList[i];

		if( !rexMBAnimExportCommon::IsRagEvent(pEvent->GetEventId()) &&
			pEvent->GetEventId() != CUTSCENE_CAMERA_CUT_EVENT) // keep cuts as they have draw distances from game
		{
			GetCutsceneFile().RemoveEvent(pEvent);
			i--;
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExport::SetPartData()
{
	atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &concatDataList 
		= const_cast<atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &>( GetCutsceneFile().GetConcatDataList() );

	concatDataList.Reset();

	for(int i=0; i < GetCutscenePartFile().GetNumParts(); ++i)
	{
		RexRageCutscenePart* pPart =  GetCutscenePartFile().GetPart(i);

		cutfCutsceneFile2::SConcatData item;
		item.cSceneName = pPart->GetName();
		item.iRangeStart = pPart->GetCutsceneFile()->GetRangeStart();
		item.iRangeEnd = pPart->GetCutsceneFile()->GetRangeEnd();

		concatDataList.Append() = item;
	}

	//// All parts are validated so must have the same offsets set, so just use the first
	if(GetCutscenePartFile().GetNumParts() > 0)
	{
		GetCutsceneFile().SetOffset(GetCutscenePartFile().GetPart(0)->GetCutsceneFile()->GetOffset());
		GetCutsceneFile().SetRotation(GetCutscenePartFile().GetPart(0)->GetCutsceneFile()->GetRotation());
	}
}

} // namespace rage
