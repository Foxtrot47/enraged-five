#include <list>
#include <vector>

using namespace rage;

#include "keylist.h"
#include "math/simplemath.h"

//#############################################################################

KeyList::KeyList()
{

}

void KeyList::AddKey( int iFrame, float fValue )
{
    if ( iFrame < 0 )
    {
        return;
    }

    KeyItem item;
    item.iFrame = iFrame;
    item.fValue = fValue;

    m_keyItemList.push_back( item );
}

float KeyList::GetValue( int iFrame )
{   
    int iCount = (int)m_keyItemList.size();

    if ( (iFrame < 0) || (iCount == 0) )
    {
        return 0.0f;
    }

    if ( iFrame <= m_keyItemList[0].iFrame )
    {
        return m_keyItemList[0].fValue;
    }
   
    if ( iFrame >= m_keyItemList[iCount - 1].iFrame )
    {
        return m_keyItemList[iCount - 1].fValue;
    }

    for ( int i = 1; i < (int)m_keyItemList.size(); ++i )
    {
        if ( iFrame == m_keyItemList[i - 1].iFrame )
        {
            return m_keyItemList[i - 1].fValue;
        }

        if ( iFrame < m_keyItemList[i].iFrame )
        {
            float fValue1 = m_keyItemList[i - 1].fValue;
            int iFrame1 = m_keyItemList[i - 1].iFrame;
            
            float fValue2 = m_keyItemList[i].fValue;
            int iFrame2 = m_keyItemList[i].iFrame;
            
            // linear interpolation
            float fValue = fValue1 + ((fValue2 - fValue1) * ((float)(iFrame - iFrame1) / (float)(iFrame2 - iFrame1)));
            return fValue;
        }
    }

    return 0.0f;
}

//#############################################################################
