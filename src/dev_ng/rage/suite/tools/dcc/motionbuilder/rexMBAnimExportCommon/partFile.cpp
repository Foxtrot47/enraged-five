#include "atl/creator.h"
#include "partFile.h"
#include "partFile_parser.h"

#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"

#include "Logger.h"
#include "../rexMBRage/shared.h"

//#############################################################################

RexRageCutscenePart::RexRageCutscenePart() :
m_pMasterCutfile(NULL),
m_export(true)
{

}

RexRageCutscenePart::~RexRageCutscenePart()
{

}

void RexRageCutscenePart::UpdateObjects()
{
	Assert(m_pMasterCutfile != NULL);

	// nuke all the child data
	const_cast<atArray<cutfObject *>&>(m_cutfile.GetObjectList()).clear();
	const_cast<atArray<cutfEvent *>&>(m_cutfile.GetLoadEventList()).clear();
	const_cast<atArray<cutfEvent *>&>(m_cutfile.GetEventList()).clear();
	const_cast<atArray<cutfEventArgs *>&>(m_cutfile.GetEventArgsList()).clear();

	atArray<cutfObject *>& objectList = const_cast<atArray<cutfObject *>&>(m_cutfile.GetObjectList());

	for(int i=0; i < m_pMasterCutfile->GetObjectList().GetCount(); ++i)
	{
		objectList.PushAndGrow(m_pMasterCutfile->GetObjectList()[i]->Clone());
	}

	for(int i=0; i < objectList.GetCount(); ++i)
	{
		// Set the streaming names 
		cutfNamedAnimatedStreamedObject* pAnimatedStreamingObject = dynamic_cast<cutfNamedAnimatedStreamedObject*>(objectList[i]);
		if(pAnimatedStreamingObject)
		{
			pAnimatedStreamingObject->SetAnimStreamingBase(pAnimatedStreamingObject->GetDisplayName().c_str());
		}

		cutfNamedStreamedObject* pStreamingObject = dynamic_cast<cutfNamedStreamedObject*>(objectList[i]);
		if(pStreamingObject)
		{
			pStreamingObject->SetStreamingName(pStreamingObject->GetDisplayName().c_str());
		}

		cutfNamedAnimatedObject* pAnimatedObject = dynamic_cast<cutfNamedAnimatedObject*>(objectList[i]);
		if(pAnimatedObject)
		{
			pAnimatedObject->SetAnimStreamingBase(pAnimatedObject->GetDisplayName().c_str());
		}

		// Find all the rag events for an object from the master and add them to the part
		atArray<cutfEvent *> newEventList;
		m_pMasterCutfile->FindEventsForObjectIdOnly( objectList[i]->GetObjectId(), m_pMasterCutfile->GetEventList(), newEventList );

		for(int k=0; k < newEventList.GetCount(); ++k )
		{
			cutfEvent* pEvent = newEventList[k];

			if( rexMBAnimExportCommon::IsRagEvent(pEvent->GetEventId()))
			{
				cutfObjectIdEvent* pObjectIdEvent = dynamic_cast<cutfObjectIdEvent*>(pEvent->Clone());
				if(pObjectIdEvent)
				{
					if(pObjectIdEvent->GetEventArgs())
					{
						pObjectIdEvent->SetEventArgs(pObjectIdEvent->GetEventArgs()->Clone());
					}

					m_cutfile.AddEvent(pObjectIdEvent);
				}
			}
		}
	}

	for(int j=0; j < objectList.GetCount(); ++j)
	{
		rexMBAnimExportCommon::LoadObjectData(objectList[j], &m_cutfile);
	}

	// copy over draw distances from master, these will of being imported from rag. loadobjectdata wipes the events and re-creates the cameras
	// for the part, we want to keep the distance values
	atArray<cutfEvent *>& eventListOriginal = const_cast<atArray<cutfEvent *> &>(m_cutfile.GetEventList());
	atArray<cutfEvent *>& eventListMaster = const_cast<atArray<cutfEvent *> &>(m_pMasterCutfile->GetEventList());

	for(int i=0; i < eventListOriginal.GetCount(); ++i)
	{
		if(eventListOriginal[i]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
		{	
			cutfCameraCutEventArgs *pEventArgs = dynamic_cast<cutfCameraCutEventArgs*>(const_cast<cutfEventArgs *>( eventListOriginal[i]->GetEventArgs() ));
			if(pEventArgs)
			{
				for(int j=0; j < eventListMaster.GetCount(); ++j)
				{
					if(eventListMaster[j]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
					{
						cutfCameraCutEventArgs *pEventArgsCompare = dynamic_cast<cutfCameraCutEventArgs*>(const_cast<cutfEventArgs *>( eventListMaster[j]->GetEventArgs() ));
						if(pEventArgsCompare)
						{
							if(stricmp(pEventArgs->GetName().GetCStr(), pEventArgsCompare->GetName().GetCStr()) == 0)
							{
								pEventArgs->SetNearDrawDistance(pEventArgsCompare->GetNearDrawDistance());
								pEventArgs->SetFarDrawDistance(pEventArgsCompare->GetFarDrawDistance());
								pEventArgs->SetMapLodScale(pEventArgsCompare->GetMapLodScale());
								pEventArgs->SetReflectionLodRangeStart(pEventArgsCompare->GetReflectionLodRangeStart());
								pEventArgs->SetReflectionLodRangeEnd(pEventArgsCompare->GetReflectionLodRangeEnd());
								pEventArgs->SetReflectionSLodRangeStart(pEventArgsCompare->GetReflectionSLodRangeStart());
								pEventArgs->SetReflectionSLodRangeEnd(pEventArgsCompare->GetReflectionSLodRangeEnd());
								pEventArgs->SetLodMultHD(pEventArgsCompare->GetLodMultHD());
								pEventArgs->SetLodMultOrphanHD(pEventArgsCompare->GetLodMultOrphanHD());
								pEventArgs->SetLodMultLod(pEventArgsCompare->GetLodMultLod());
								pEventArgs->SetLodMultSlod1(pEventArgsCompare->GetLodMultSlod1());
								pEventArgs->SetLodMultSlod2(pEventArgsCompare->GetLodMultSlod2());
								pEventArgs->SetLodMultSlod3(pEventArgsCompare->GetLodMultSlod3());
								pEventArgs->SetLodMultSlod4(pEventArgsCompare->GetLodMultSlod4());
								pEventArgs->SetWaterReflectionFarClip(pEventArgsCompare->GetWaterReflectionFarClip());
								pEventArgs->SetLightSSAOInten(pEventArgsCompare->GetLightSSAOInten());
								pEventArgs->SetExposurePush(pEventArgsCompare->GetExposurePush());
								pEventArgs->SetCharacterLightParams(pEventArgsCompare->GetCharacterLightParams());
								pEventArgs->SetDisableHighQualityDof(pEventArgsCompare->GetDisableHighQualityDof());
							}
						}
					}
				}
			}
		}
	}
}

RexRageCutscenePartFile::RexRageCutscenePartFile()
{
	m_sectionMethodIndex = 0;
	m_sectionTimeDuration = 0;
}

void RexRageCutscenePartFile::AddPart(RexRageCutscenePart* pPart)
{
	Assert(pPart != NULL);

	if(pPart != NULL)
	{
		m_parts.PushAndGrow(pPart);
	}
}

void RexRageCutscenePartFile::DeletePart(s32 iIndex)
{
	m_parts.Delete(iIndex);
}

void RexRageCutscenePartFile::CopyFrom(const RexRageCutscenePartFile& partFile)
{
	for(int partIndex = 0; partIndex < partFile.m_parts.GetCount(); ++partIndex)
	{
		AddPart(partFile.m_parts[partIndex]->Clone());
	}

	SetAudio(partFile.m_audio);
	SetFolder(partFile.m_path);
	m_sectionMethodIndex = partFile.m_sectionMethodIndex;
	m_sectionTimeDuration = partFile.m_sectionTimeDuration;
}

bool RexRageCutscenePartFile::SaveFile(const char* pFilename)
{
	RexRageCutscenePartFile expandedPartFile;
	expandedPartFile.CopyFrom(*this);

	atString strExportDir(rexMBAnimExportCommon::GetProjectOrDlcAssetsDir(atString("")));
	strExportDir.Lowercase();
	strExportDir.Replace("/", "\\");

	expandedPartFile.m_path.Replace("/","\\");
	expandedPartFile.m_path.Replace(strExportDir, "$(assets)");

	for(int i = 0; i < m_parts.GetCount(); ++i)
	{
		atString newPath(m_parts[i]->GetCutsceneFileName());
		newPath.Lowercase();
		newPath.Replace("/", "\\");
		newPath.Replace(strExportDir, "$(assets)");	
		expandedPartFile.m_parts[i]->SetCutsceneFileName(newPath.c_str());
	}

	return PARSER.SaveObject( pFilename, "", &expandedPartFile, parManager::XML );
}

bool RexRageCutscenePartFile::LoadFile(const char* pFilename, bool bValidate)
{
	Clear();

	bool bResult = PARSER.LoadObject(pFilename, "", *this); 

	atString strExportDir(rexMBAnimExportCommon::GetProjectOrDlcAssetsDir(atString("")));
	strExportDir.Lowercase();
	strExportDir.Replace("/", "\\");

	m_path.Replace("$(assets)", strExportDir);

	for(int i = 0; i < m_parts.GetCount(); ++i)
	{
		atString newPath(m_parts[i]->GetCutsceneFileName());
		newPath.Lowercase();
		newPath.Replace("/", "\\");
		newPath.Replace("$(assets)", strExportDir);	
		m_parts[i]->SetCutsceneFileName(newPath.c_str());
	}

	// Load the cutxml files associated with our items
	for(int i =0 ; i < m_parts.GetCount(); ++i)
	{
		if(m_parts[i]->GetCutsceneFileName() != NULL)
		{
			if(!m_parts[i]->GetCutsceneFile()->LoadFile(m_parts[i]->GetCutsceneFileName()))
			{
				if(bValidate)
					ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to load cutfile '%s' for part '%s'", m_parts[i]->GetName(), m_parts[i]->GetCutsceneFileName());
				bResult = false;
			}
		}
		else
		{
			if(bValidate)
				ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to load cutfile for part '%s'. NULL reference.", m_parts[i]->GetName());
			bResult = false;
		}	
	}

	return bResult;
}

RexRageCutscenePart* RexRageCutscenePart::Clone()
{
	RexRageCutscenePart* pClone = rage_new RexRageCutscenePart(NULL, m_name.c_str());
	pClone->SetCutsceneFileName(GetCutsceneFileName());
	return pClone;
}

bool RexRageCutscenePart::ValidateDecalFrames()
{
	bool bResult = true;
	for(int i=0; i < m_cutfile.GetEventList().GetCount(); ++i)
	{
		if(m_cutfile.GetEventList()[i]->GetEventId() == CUTSCENE_TRIGGER_DECAL_EVENT)
		{
			for(int j=0; j < m_cutfile.GetEventList().GetCount(); ++j)
			{
				if(i != j && m_cutfile.GetEventList()[j]->GetEventId() == CUTSCENE_TRIGGER_DECAL_EVENT)
				{
					if(m_cutfile.GetEventList()[i]->GetTime() == m_cutfile.GetEventList()[j]->GetTime())
					{
						const cutfObjectIdEvent* pEvent = dynamic_cast<const cutfObjectIdEvent*>(m_cutfile.GetEventList()[i]);
						const cutfObjectIdEvent* pEventCompare = dynamic_cast<const cutfObjectIdEvent*>(m_cutfile.GetEventList()[j]);
						
						const cutfDecalObject* pDecalObject = dynamic_cast<const cutfDecalObject*>(m_cutfile.GetObjectList()[pEvent->GetObjectId()]);
						const cutfDecalObject* pDecalObjectCompare = dynamic_cast<const cutfDecalObject*>(m_cutfile.GetObjectList()[pEventCompare->GetObjectId()]);

						int iFrame = pEvent->GetTime()*CUTSCENE_FPS;
						ULOGGER_MOBO.SetProgressMessage("ERROR: Decals '%s' and '%s' are on the same frame '%d', this is invalid.", pDecalObject->GetName().GetCStr(), pDecalObjectCompare->GetName().GetCStr(), iFrame);
						bResult = false;
					}
				}
			}
		}
	}

	return bResult;
}

bool RexRageCutscenePart::SaveFile(const char* pFilename)
{
	UpdateObjects();

	if(!ValidateDecalFrames()) return false;

	// clone and save the file as we don't want these updates getting into the actual cutfile
	m_cutfile.CleanupUnusedEventArgs();
	cutfCutsceneFile2* pCutfileClone = m_cutfile.Clone();

	atArray<cutfCutsceneFile2::SDiscardedFrameData>& discardedDataList = const_cast<atArray<cutfCutsceneFile2::SDiscardedFrameData>&>(pCutfileClone->GetDiscardedFrameList());
	discardedDataList.clear();
	cutfCutsceneFile2::SDiscardedFrameData discardData(m_name.c_str(), rexMBAnimExportCommon::GetDiscardedFramesArray());
	
	discardedDataList.PushAndGrow(discardData);

	atBitSet &cutsceneFlags = const_cast<atBitSet &>( pCutfileClone->GetCutsceneFlags() );
	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_PART_FLAG, true );

	atArray<cutfEvent *>& eventList = const_cast<atArray<cutfEvent *> &>(pCutfileClone->GetEventList());
	for(int i=0; i < eventList.GetCount(); ++i)
	{
		cutfEvent* pEvent = eventList[i];

		if(rexMBAnimExportCommon::IsRagEvent(pEvent->GetEventId()) ||
			pEvent->GetEventId() == CUTSCENE_FADE_IN_EVENT ||
			pEvent->GetEventId() == CUTSCENE_FADE_OUT_EVENT)
		{
			int iFrame = rexMBAnimExportCommon::GetFrameFromTime(pEvent->GetTime());

			int iNewFrame = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames(iFrame);
			pEvent->SetTime(iNewFrame/CUTSCENE_FPS);
		}
	}

	pCutfileClone->SortLoadEvents();
	pCutfileClone->SortEvents();

	return pCutfileClone->SaveFile(pFilename);
}
