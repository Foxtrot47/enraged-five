// /animExport.cpp

#pragma warning(push)
#pragma warning(disable:4668)
#include <windows.h>
#pragma warning(pop)

#include <algorithm>
#include <queue>
#include <stack>
#include <sstream>
#include <vector>

#include "atl/string.h"
#include "file/device.h"
#include "file/asset.h"
#include "rexBase/animExportCtrl.h"
#include "rexBase/utility.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "crmetadata/properties.h"
#include "crmetadata/propertyattributes.h"

#include "crSkeleton/skeletondata.h"

#include "animExport.h"
#include "timerLog.h"

#include "rexMBRage/shared.h"

#include "vectormath/legacyconvert.h"

#include "Logger.h"

#include "perforce.h"

#include "clipxml/clipxml.h"

#pragma warning (disable:4239)

using namespace std;
using namespace rage;

//#############################################################################

//static const float TIME_TO_FRAME_TOLERANCE = 0.001f;

//#############################################################################

#define GENERIC_INT_TAG_NAME "GenericInt"
#define GENERIC_FLOAT_TAG_NAME "GenericFloat"
#define GENERIC_VECTOR3_TAG_NAME "GenericVector3"
#define GENERIC_VECTOR4_TAG_NAME "GenericVector4"
#define GENERIC_QUATERNION_TAG_NAME "GenericQuaternion"
#define GENERIC_MATRIX34_TAG_NAME "GenericMatrix34"
#define GENERIC_STRING_TAG_NAME "GenericString"
#define LABEL_TAG_NAME "Label"
#define BLOCK_TAG_NAME "Block"
#define LOOP_TAG_NAME "Loop"
#define LOOP_ID_TAG_NAME "LoopId"
#define FOCUS_TAG_NAME "Focus"


bool RexRageAnimExport::Initialize()
{
	return true;
}

//-----------------------------------------------------------------------------

void RexRageAnimExport::SetProgressCaption(const char* caption)
{
	ULOGGER_MOBO.SetProgressCaption(caption);
}

//-----------------------------------------------------------------------------

void RexRageAnimExport::SetProgressPercent(float percent) 
{
    float fItemRange = 100.0f / (float)m_iNumExportProgressItems;
    float fBatchProgress = (percent / 100.0f) * fItemRange;
    if ( m_iCurrentExportProgressItem > 0 )
    {
        fBatchProgress += (m_iCurrentExportProgressItem * fItemRange);
    }

    if ( fBatchProgress < 0.0f )
    {
        fBatchProgress = 0.0f;
    }
    else if ( fBatchProgress > 100.0f )
    {
        fBatchProgress = 100.0f;
    }

	ULOGGER_MOBO.SetProgressPercent(fBatchProgress);
}

//-----------------------------------------------------------------------------

void RexRageAnimExport::SetProgressMessage(const char* message)
{
	ULOGGER_MOBO.SetProgressMessage(message);
}

//-----------------------------------------------------------------------------

bool RexRageAnimExport::ValidateFacialAttributeFile(const RexAnimMarkupData* pAnimData)
{
	if(pAnimData->GetAttributeFilePath() != NULL && stricmp(pAnimData->GetAttributeFilePath(), "") != 0)
	{
		if(!ASSET.Exists(pAnimData->GetAttributeFilePath(), NULL))
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf_s( cMsg, RAGE_MAX_PATH, "ERROR: Facial attribute file '%s' does not exist.", pAnimData->GetAttributeFilePath() );

			ULOGGER_MOBO.SetProgressMessage(cMsg);
			return false;
		}
	}

	return true;
}

atString RexRageAnimExport::GetP4RevisionInfo(const char* pModelName)
{
	atString strModelName(pModelName);
	int index = strModelName.IndexOf(":");
	if(index != -1)
	{
		atString strNameSpace("");
		strNameSpace.Set(strModelName, 0, index);

		char cReferenceNode[RAGE_MAX_PATH];
		sprintf(cReferenceNode, "RS_Null:%s", strNameSpace.c_str());

		HFBModel referenceModel = RAGEFindModelByName(cReferenceNode);
		if(referenceModel != NULL)
		{
			HFBProperty pProperty = referenceModel->PropertyList.Find( "P4_Version" );
			if(pProperty != NULL)
			{
				FBPropertyString *pPropertyString = (FBPropertyString *)pProperty;

				char* pValue = (char*)pPropertyString->GetPropertyValue();
				if(pValue)
				{
					return atString(pValue);
				}
			}
		}

	}

	return atString("unknown");
}

//-----------------------------------------------------------------------------

bool RexRageAnimExport::ExecutePostRenderScript(bool bRunScript)
{
	if(bRunScript)
	{
		atString postRenderScript;
		if(!rexMBAnimExportCommon::GetAnimationPostRenderScript(postRenderScript))
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to evaluate animation post render script." );
			return false;
		}

		FBApplication app;
		if(!app.ExecuteScript(postRenderScript.c_str()))
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Failed to execute post render script '%s'.", postRenderScript.c_str() );
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExport::ProcessFaceMerge( const RexAnimMarkupData* pAnimData )
{
	char cToolsRoot[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cToolsRoot, RAGE_MAX_PATH);

	// Look for a face entry that has merge face enabled. Then look for its matching body entry which will have the same namespace.
	for(int i=0; i < pAnimData->GetNumCharacters(); ++i)
	{
		if(pAnimData->GetCharacter(i)->IsMarkedForFaceMerge() && (stricmp(pAnimData->GetCharacter(i)->GetModelType(), "Face") == 0))
		{
			atString atFaceNamespace = rexMBAnimExportCommon::GetNameSpaceFromModel(RAGEFindModelByName(pAnimData->GetCharacter(i)->GetInputModelName(0)));
			
			for(int j=0; j < pAnimData->GetNumCharacters(); ++j)
			{
				atString atBodyNamespace = rexMBAnimExportCommon::GetNameSpaceFromModel(RAGEFindModelByName(pAnimData->GetCharacter(j)->GetInputModelName(0)));

				if((stricmp(pAnimData->GetCharacter(j)->GetModelType(), "Body") == 0) && (atBodyNamespace == atFaceNamespace))
				{
					for(int k=0; k < pAnimData->GetNumSegments(); ++k)
					{
						const RexMbAnimSegmentData* pSegData = pAnimData->GetSegment(k);

						int pathCount = pSegData->GetNumAltOutputPaths();
						// We want it to run at least once.  GetClipFilePath checks if there are actually no alt 
						// output paths and deals with it accordingly.
						if ( pathCount == 0 ) { pathCount = 1; }
						for ( int idxPath = 0; idxPath < pathCount; idxPath++)
						{
							atString atBodyOutputAnim(GetAnimFilePath(pAnimData, pAnimData->GetCharacter(j), pSegData, idxPath));
							atString atFaceOutputAnim(GetAnimFilePath(pAnimData, pAnimData->GetCharacter(i), pSegData, idxPath));

							char cCmdLine[RAGE_MAX_PATH*2];
							sprintf(cCmdLine, "%s\\bin\\anim\\animcombine.exe -anims %s,%s -merge -sync -out %s -output -nocompression -nocompress", cToolsRoot, atBodyOutputAnim.c_str(), atFaceOutputAnim.c_str(), atBodyOutputAnim.c_str());

							int result = system(cCmdLine);
							if(result != 0)
							{
								ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
								return false;
							}
						}
					}
				}
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExport::ProcessBaseAnimationAdditive( const RexAnimMarkupData* pAnimData )
{
	char cToolsRoot[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariableA("RS_TOOLSROOT", cToolsRoot, RAGE_MAX_PATH);
	for(int i=0; i < pAnimData->GetNumCharacters(); ++i)
	{
		for(int k=0; k < pAnimData->GetNumSegments(); ++k)
		{
			const RexMbAnimSegmentData* pSegData = pAnimData->GetSegment(k);

			if(pSegData->m_bAdditiveBaseAnim)
			{
				int pathCount = pSegData->GetNumAltOutputPaths();
				// We want it to run at least once.  GetClipFilePath checks if there are actually no alt 
				// output paths and deals with it accordingly.
				if ( pathCount == 0 ) { pathCount = 1; }
				for ( int idxPath = 0; idxPath < pathCount; idxPath++)
				{
					atString atOutputAnim(GetAnimFilePath(pAnimData, pAnimData->GetCharacter(i), pSegData, idxPath));
					atString atBaseAnim(pSegData->GetAdditiveBaseAnimation());

					if(!ASSET.Exists(atBaseAnim.c_str(), NULL))
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Base animation file '%s' does not exist.", atBaseAnim.c_str() );
						return false;
					}

					char cCmdLine[RAGE_MAX_PATH*2];
					sprintf(cCmdLine, "%s\\bin\\anim\\animcombine.exe -anims %s,%s -subtract -time 0 -out %s", cToolsRoot, atOutputAnim.c_str(), atBaseAnim.c_str(), atOutputAnim.c_str());

					int result = system(cCmdLine);
					if(result != 0)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
						return false;
					}
				}
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExport::DoExport( const RexAnimMarkupData* pAnimData, bool bSample )
{
	rexMBAnimExportCommon::SetBaseLayerOnAllTakes();

    atArray<RexAnimMarkupData *> animDataList;
    animDataList.PushAndGrow( const_cast<RexAnimMarkupData *>( pAnimData ) );

	if(!ValidateFacialAttributeFile(pAnimData)) return false;

	rexMBAnimExportCommon::SetStoryMode(pAnimData->m_bStoryMode);
	rexMBAnimExportCommon::SetActivePart( pAnimData->m_bStoryMode ? atString("Shot_1") : atString(""));

	if(!rexMBAnimExportCommon::ValidateAndCreateStoryTrack(NULL, false, rexMBAnimExportCommon::GetFrameFromTime(rexMBAnimExportCommon::GetSceneStartTime()), rexMBAnimExportCommon::GetFrameFromTime(rexMBAnimExportCommon::GetSceneEndTime()), "")) return false;

	FBApplication application;
	if(!rexMBAnimExportCommon::IsFileUnderArtRoot(application.FBXFileName.AsString()))
	{
		return false;
	}

	rexMBAnimExportCommon::SetUseClipXml(false);

    bool bResult = DoExport( animDataList, bSample );

	bResult = bResult && ProcessFaceMerge(pAnimData);
	bResult = bResult && ProcessBaseAnimationAdditive(pAnimData);

	bResult = bResult && ExecutePostRenderScript(pAnimData->m_bRenderMode);

	return bResult;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExport::DoExport( const atArray< RexAnimMarkupData* > &animDataList, bool bSample )
{
#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::StartTimer("RexRageAnimExport::DoExport");
    }
#endif //PROFILE_ANIM_EXPORT

    bool bReturn = true;

	// reset the discarded frames, if we exported a cutscene and then changed to in-game we will possibly have some discarded frames hanging around
	rexMBAnimExportCommon::CalculateDiscardedFrames();

    rage::AnimExportCtrlSpec::RegisterParsableClasses();

    vector<RexMbAnimExportModel *> splitByModelExportModelArray;

    atArray<HFBTake> takeArray;
    atArray<vector<RexMbAnimExportModel *>> splitByTakeExportModelArray;

    int iMinStartFrame = -1;
    int iMaxEndFrame = -1;

	bool hasCamera = false;
	for( int i = 0; i < animDataList.GetCount(); i++ )
	{
		if( rexMBAnimExportCommon::HasCamera( animDataList[i] ) )
		{
			hasCamera = true;
			break;
		}
	}

    // take the list and build RexMbAnimExportModel by models and by takes
    for ( int i = 0; i < animDataList.GetCount(); ++i )
    {
        if ( !animDataList[i]->m_segmentFromTakes )
        {
            //There may be multiple root node / mover node pairs marked for export, so we need to split
            //them up into a seperate markup object for each pair
            vector< RexAnimMarkupData* > modelAnimDataArray;
            SplitAnimDataByModels( animDataList[i], modelAnimDataArray );

            for(vector< RexAnimMarkupData* >::iterator it = modelAnimDataArray.begin();
                it != modelAnimDataArray.end();
                ++it)
            {
                splitByModelExportModelArray.push_back( rage_new RexMbAnimExportModel( *it ) );

                // find our range
                for ( int j = 0; j < (*it)->GetNumSegments(); ++j )
                {
                    int iStartFrame = (*it)->GetSegment( j )->GetStartAsFrame();
                    if ( (iStartFrame != -1) && ((iMinStartFrame == -1) || (iStartFrame < iMinStartFrame)) )
                    {
                        iMinStartFrame = iStartFrame;
                    }

                    int iEndFrame = (*it)->GetSegment( j )->GetEndAsFrame();
                    if ( (iEndFrame != -1) && ((iMaxEndFrame == -1) || (iEndFrame > iMaxEndFrame)) )
                    {
                        iMaxEndFrame = iEndFrame;
                    }
                }
            }
        }
        else
        {
            //Sort the markup data so that each markup object contains
            //segments for only one take

			// LPXO: What this means is, each entry in modelAnimDataArray correlates
			// to a unique take/model combination.  i.e. In a 2 model, 2 take
			// scenario we will end up with 4 entries in modelAnimDataArray:
			//
			//	model_1 -> take_1
			//  model_2 -> take_1
			//  model_1 -> take_2
			//  model_2 -> take_2
			//
            vector< RexAnimMarkupData* > modelAnimDataArray;
            SplitAnimDataByTakes( animDataList[i], modelAnimDataArray );

			// LPXO: Next re-arrange the data so we have a 2 dimensional array:
			//		- The first dimension is an entry per take
			//		- The second dimension has an RexMbAnimExportModel object for each model
			//			in that take. NOTE: RexMbAnimExportModel has a constructor that
			//			takes a RexAnimMarkupData object and assigns to a member as you 
			//			can see below
			for(vector< RexAnimMarkupData* >::iterator it = modelAnimDataArray.begin();
				it != modelAnimDataArray.end();
				++it)
			{
				HFBTake hTake = (*it)->GetSegment( 0 )->GetTake();

				if ( m_system.Scene->Takes.Find(hTake) >= 0)
				{
					int index = takeArray.Find( hTake );
					if ( index == -1 )
					{
						// Add a new take entry
						index = takeArray.GetCount();
						takeArray.Grow() = hTake;

						splitByTakeExportModelArray.Grow();
					}
					// Add export model data to the correct take indexed element
					splitByTakeExportModelArray[index].push_back( rage_new RexMbAnimExportModel( *it ) );
				}
				else
				{
					const char* szTakeName = (*it)->GetSegment( 0 )->GetTakeName();

					char msgBuf[512];
					sprintf_s(msgBuf, 512, "ERROR: The take '%s' was specified for export, but is not present in the loaded FBX file, take skipped", szTakeName);
					SetProgressMessage( msgBuf );

					bReturn = false;
					break;
				}
			}
        }
    }

    if ( !bReturn )
    {
        //Clean up the temporary model based markup data 
        for ( vector<RexMbAnimExportModel *>::iterator it = splitByModelExportModelArray.begin(); it != splitByModelExportModelArray.end(); ++it )
        {
            delete (*it);
        }

        //Clean up the temporary model based markup data 
		DeleteVector<RexMbAnimExportModel>(splitByTakeExportModelArray);
        return false;
    }

    m_iNumExportProgressItems = splitByTakeExportModelArray.GetCount();
    m_iCurrentExportProgressItem = 0;

    if ( splitByModelExportModelArray.size() > 0 )
    {
        ++m_iNumExportProgressItems;

        float fStartTime = (iMinStartFrame != -1) ? (iMinStartFrame / rexMBAnimExportCommon::GetFPS()) : rexMBAnimExportCommon::GetSceneStartTime();
        float fEndTime = (iMaxEndFrame != -1) ? ((iMaxEndFrame)  / rexMBAnimExportCommon::GetFPS()) : rexMBAnimExportCommon::GetSceneEndTime();

        bReturn = DoExportInternal( splitByModelExportModelArray, fStartTime, fEndTime, bSample );        

        //Clean up the temporary model based markup data 
        for ( vector<RexMbAnimExportModel *>::iterator it = splitByModelExportModelArray.begin(); it != splitByModelExportModelArray.end(); ++it )
        {
            delete (*it);
        }

        if ( !bReturn )
        {
            //Clean up the temporary model based markup data 
			DeleteVector<RexMbAnimExportModel>(splitByTakeExportModelArray);

#if PROFILE_ANIM_EXPORT
            if(rexMBTimerLog::IsTimerLogEnabled())
            {
                rexMBTimerLog::EndTimer("RexRageAnimExport::DoExport");
            }
#endif //PROFILE_ANIM_EXPORT

            return bReturn;
        }
    }

    if ( splitByTakeExportModelArray.GetCount() > 0 )
    {
        //Store the take that is currently active in the scene
        HFBTake hActiveTake = m_system.CurrentTake;

		//Execute all Perforce operations.
		for ( int i = 0; i < (int)splitByTakeExportModelArray.GetCount(); ++i )
		{
			for ( int j = 0; j < (int)splitByTakeExportModelArray[i].size(); ++j )
			{
				const RexAnimMarkupData* pAnimData = splitByTakeExportModelArray[i][j]->GetAnimData();
				const RexCharAnimData* pCharData = pAnimData->GetCharacter(0);
				const RexMbAnimSegmentData* pSegData = pAnimData->GetSegment(0);
				const vector<RexMbAnimExportDataBase*> pExportDataList = splitByTakeExportModelArray[i][j]->GetExportDataList();

				int pathCount = pSegData->GetNumAltOutputPaths();
				// We want it to run at least once.  GetClipFilePath checks if there are actually no alt 
				// output paths and deals with it accordingly.
				if ( pathCount == 0 ) { pathCount = 1; }
				for ( int idxPath = 0; idxPath < pathCount; idxPath++)
				{
					char* pAnimPath = GetAnimFilePath(pAnimData, pCharData, pSegData, idxPath);
					perforce::SyncAndCheckout(pAnimPath, m_bInteractiveMode);

					char* pClipPath = GetClipFilePath(pAnimData, pCharData, pSegData, idxPath);
					perforce::SyncAndCheckout(pClipPath, m_bInteractiveMode);
				}
			}
		}

		perforce::ExecuteQueuedOperations(m_bInteractiveMode);

        for ( int i = 0; i < (int)splitByTakeExportModelArray.GetCount(); ++i )
        {
            m_system.CurrentTake = takeArray[i];
            
            char cMsg[512];
            sprintf_s( cMsg, 512, "Exporting Take '%s'.", m_system.CurrentTake->Name.AsString() );
            SetProgressCaption( cMsg );
            SetProgressMessage( cMsg );

            bReturn = DoExportInternal( splitByTakeExportModelArray[i], 
                rexMBAnimExportCommon::GetSceneStartTime(), rexMBAnimExportCommon::GetSceneEndTime(), bSample );

			for ( int j = 0; j < (int)splitByTakeExportModelArray[i].size(); ++j )
			{
				const RexAnimMarkupData* pAnimData = splitByTakeExportModelArray[i][j]->GetAnimData();
				const RexCharAnimData* pCharData = pAnimData->GetCharacter(0);
				const RexMbAnimSegmentData* pSegData = pAnimData->GetSegment(0);
				const vector<RexMbAnimExportDataBase*> pExportDataList = splitByTakeExportModelArray[i][j]->GetExportDataList();

				int pathCount = pSegData->GetNumAltOutputPaths();
				// We want it to run at least once.  GetClipFilePath checks if there are actually no alt 
				// output paths and deals with it accordingly.
				if ( pathCount == 0 ) { pathCount = 1; }
				for ( int idxPath = 0; idxPath < pathCount; idxPath++)
				{
					bReturn = bReturn && SaveClipData( pAnimData, pCharData, pSegData, pExportDataList, rexMBAnimExportCommon::GetSceneStartTime(), rexMBAnimExportCommon::GetSceneEndTime(), idxPath, hasCamera );
				}
			}

            if ( !bReturn )
            {
                break;
            }
        }

        //Clean up the temporary model based markup data 
		DeleteVector<RexMbAnimExportModel>(splitByTakeExportModelArray);

        //Restore the take to the active take at the start of the export
        m_system.CurrentTake = hActiveTake;
    }

#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::EndTimer("RexRageAnimExport::DoExport");
    }
#endif //PROFILE_ANIM_EXPORT

    return bReturn;
}

//-----------------------------------------------------------------------------

template<typename T>
void RexRageAnimExport::DeleteVector(atArray<vector<T *>> &inArray)
{
	for ( int i = 0; i < inArray.GetCount(); ++i )
	{
		for ( std::vector<T *>::iterator it = inArray[i].begin(); 
			it != inArray[i].end(); ++it )
		{
			delete (*it);
		}
	}
}

//-----------------------------------------------------------------------------

bool RexRageAnimExport::DoExportInternal( const vector<RexMbAnimExportModel *> &exportModelList, float fStartTime, float fEndTime, bool bSample )
{
#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::StartTimer("RexRageAnimExport::DoExportInternal");
    }
#endif //PROFILE_ANIM_EXPORT

    bool bResult = true;

#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::StartTimer("Initialize");
    }
#endif //PROFILE_ANIM_EXPORT

    //RexMbAnimExportModel::SetProgressCaptionCallback( MakeFunctor( *this, &RexRageAnimExport::SetProgressCaption ) );
    //RexMbAnimExportModel::SetProgressMessageCallback( MakeFunctor( *this, &RexRageAnimExport::SetProgressMessage ) );

    int nFrames = int((fEndTime - fStartTime + TIME_TO_FRAME_TOLERANCE) * rexMBAnimExportCommon::GetFPS()) + 1;

    vector< rexObjectGenericAnimation* > animList;
	int numModels = (int)exportModelList.size();
    for ( int i = 0; i < numModels; ++i )
    {
#if PROFILE_ANIM_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::StartTimer( exportModelList[i]->GetAnimData()->GetInputModelName( 0 ) );
        }
#endif //PROFILE_ANIM_EXPORT

        char cMsg[512];
        sprintf_s( cMsg, 512, "Initializing generic animation for root model '%s'.", exportModelList[i]->GetAnimData()->GetCharacter(0)->GetInputModelName( 0 ) );
        SetProgressCaption( cMsg );
        SetProgressMessage( cMsg );

        rexObjectGenericAnimation *pAnim = rage_new rexObjectGenericAnimation;
        animList.push_back( pAnim );

		RexMbAnimExportModel* currentModel = exportModelList[i];

		// Check for ctrl file override at the take level
		// If we have no root dof in the ctrl file then we don't want mover data also
		if( currentModel->GetAnimData()->GetSegmetFromTakes() )
		{
			RexMbAnimSegmentData* pMbAnimSegment = currentModel->GetAnimData()->GetSegment(0);
			RexCharAnimData* pMbCharData = currentModel->GetAnimData()->GetCharacter(0);
			bool ctrlFileOverride = false;
			atString animCtrlFilePath;

			if(pMbAnimSegment->m_ctrlFilePath != "(no spec file)")
			{
				animCtrlFilePath;
				rexMBAnimExportCommon::GetMotionBuilderAnimCtrlFilePathSetting( animCtrlFilePath );
				animCtrlFilePath += "\\";
				animCtrlFilePath += pMbAnimSegment->m_ctrlFilePath.c_str();
				ctrlFileOverride = true;
			}
			else if(strcmp(pMbCharData->GetCtrlFile(), "(no spec file)") != 0)
			{
				animCtrlFilePath;
				rexMBAnimExportCommon::GetMotionBuilderAnimCtrlFilePathSetting( animCtrlFilePath );
				animCtrlFilePath += "\\";
				animCtrlFilePath += pMbCharData->GetCtrlFile();
				ctrlFileOverride = true;
			}

			if(ctrlFileOverride)
			{
				AnimExportCtrlSpec ctrlFile;
				if(ctrlFile.LoadFromXML(animCtrlFilePath.c_str()))
				{
					const AnimExportCtrlTrackSpec* pTrackSpec = ctrlFile.FindTrackSpec("root");
					if(pTrackSpec == NULL)
					{
						//char cMsg[512];
						//sprintf_s( cMsg, 512, "Removed mover data for take %s because ctrl file %s does not have root track spec.", pMbAnimSegment->m_takeName.c_str(), animCtrlFilePath.c_str() );
						//SetProgressCaption( cMsg );

						pMbCharData->m_moverModelNames.clear();
					}
				}
			}
		}

		bResult = currentModel->Initialize( *pAnim, fStartTime, fEndTime, nFrames );

#if PROFILE_ANIM_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( exportModelList[i]->GetAnimData()->GetInputModelName( 0 ) );
        }
#endif //PROFILE_ANIM_EXPORT

         if ( !bResult )
        {
            break;
        }
    }

#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::EndTimer("Initialize");
    }
#endif //PROFILE_ANIM_EXPORT

    if ( !bResult )
    {
        for ( int i = 0; i < (int)animList.size(); ++i )
        {
            delete animList[i];
        }

#if PROFILE_ANIM_EXPORT
        if(rexMBTimerLog::IsTimerLogEnabled())
        {
            rexMBTimerLog::EndTimer("RexRageAnimExport::DoExportInternal");
        }
#endif //PROFILE_ANIM_EXPORT

        return false;
    }

    vector< pair< string, bool > >	segmentSourceCtrlCache;

    if ( bSample )
    {
        char cMsg[128];
        sprintf( cMsg, "Converting %d skeletal animation(s) in one pass.", exportModelList.size() );
        SetProgressCaption( cMsg );
        
        double deltaTime = 1.0 / (double)rexMBAnimExportCommon::GetFPS();

        FBSystem system;
        FBPlayerControl controller;       

        // We should only update the progress percent every few frames
        int iProgressIncrement = (int)((float)nFrames / 10.0f);
        int iProgressStep = iProgressIncrement;

        FBTime preSampleTime = system.LocalTime;

#if PROFILE_ANIM_EXPORT
        if(rexMBTimerLog::IsTimerLogEnabled())
        {
            rexMBTimerLog::StartTimer("RexMbAnimExportModel::CollectAnimData( sample )");
        }
#endif //PROFILE_ANIM_EXPORT

		int iStartFrame = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);

		for ( int i = 0; i < nFrames + rexMBAnimExportCommon::GetNumberOfDiscardedFrames(); i++ )
        {
			if(rexMBAnimExportCommon::DiscardFrame(i + iStartFrame)) continue;

            //Set the Motion Builder time and evaluate the scene
            double t = fStartTime + (deltaTime * (double)i);
            FBTime fbt;
            fbt.SetSecondDouble(t);
            controller.Goto(fbt);

            system.Scene->Evaluate();

            for ( int j = 0; j < (int)exportModelList.size(); ++j )
            {
                exportModelList[j]->CollectAnimData();
            }

            if ( i == iProgressStep )
            {
                SetProgressPercent( ((float)i / (float)nFrames) * 95.0f );
                iProgressStep += iProgressIncrement;
            }
        }

        controller.Goto(preSampleTime);

#if PROFILE_ANIM_EXPORT
        if(rexMBTimerLog::IsTimerLogEnabled())
        {
            rexMBTimerLog::EndTimer("RexMbAnimExportModel::CollectAnimData( sample )");
            rexMBTimerLog::StartTimer( "Serialize Animations" );
        }
#endif //PROFILE_ANIM_EXPORT

		for ( int i = 0; i < exportModelList.size(); ++i )
		{
			if(!ValidateAnimationData( exportModelList[i] ))
			{
				return false;
			}
		}

        sprintf( cMsg, "Serializing %d skeletal animation(s).", exportModelList.size() );
        SetProgressCaption( cMsg );

        int iNumSteps = (int)exportModelList.size();
        int iCurrentStep = 1;
        
        for ( int i = 0; i < (int)exportModelList.size(); ++i )
        {
#if PROFILE_ANIM_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::StartTimer( exportModelList[i]->GetAnimData()->GetInputModelName( 0 ) );
            }
#endif //PROFILE_ANIM_EXPORT

            char cMsg[512];
            sprintf_s( cMsg, 512, "Serializing generic animation for root model '%s'.", exportModelList[i]->GetAnimData()->GetCharacter(0)->GetInputModelName( 0 ) );
            SetProgressMessage( cMsg );

			const RexAnimMarkupData* pAnimData = exportModelList[i]->GetAnimData();
			const RexCharAnimData* pCharData = pAnimData->GetCharacter(0);
			const RexMbAnimSegmentData* pSegmentData = pAnimData->GetSegment(0);

			int pathCount = pSegmentData->GetNumAltOutputPaths();
			// We want it to run at least once.  GetAnimFilePath checks if there are actually no alt 
			// output paths and deals with it accordingly.
			if ( pathCount == 0 ) { pathCount = 1; }
			for ( int idxPath = 0; idxPath < pathCount; idxPath++)
			{
				char* pAnimPath = GetAnimFilePath(pAnimData, pCharData, pSegmentData, idxPath);
				char pOutputPath[RAGE_MAX_PATH];
				ASSET.RemoveNameFromPath(pOutputPath, RAGE_MAX_PATH, pAnimPath);

				bResult = exportModelList[i]->SerializeAnimation( *(animList[i]), pOutputPath );
			}

            SetProgressPercent( (((float)iCurrentStep / (float)iNumSteps) * 5.0f) + 95.0f );
            ++iCurrentStep;
            
#if PROFILE_ANIM_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( exportModelList[i]->GetAnimData()->GetInputModelName( 0 ) );
            }
#endif //PROFILE_ANIM_EXPORT
        
            if ( !bResult )
            {
                break;
            }
        }

#if PROFILE_ANIM_EXPORT
        if(rexMBTimerLog::IsTimerLogEnabled())
        {
            rexMBTimerLog::EndTimer( "Serialize Animations" );
        }
#endif //PROFILE_ANIM_EXPORT
    }
    else // if( bSample )
    {
        int iNumSteps = (int)exportModelList.size() * 2;
        int iCurrentStep = 1;

#if PROFILE_ANIM_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::StartTimer( "Convert and Serialize Animations" );
        }
#endif //PROFILE_CUTSCENE_EXPORT

        for ( int i = 0; i < (int)exportModelList.size(); ++i )
        {
#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::StartTimer( exportModelList[i]->GetAnimData()->GetInputModelName( 0 ) );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            char cMsg[512];
            sprintf_s( cMsg, 512, "Converting generic animation for root model '%s'", exportModelList[i]->GetAnimData()->GetCharacter(0)->GetInputModelName( 0 ) );
            SetProgressCaption( cMsg );

            exportModelList[i]->CollectAnimData( fStartTime, fEndTime, nFrames );

            SetProgressPercent( ((float)iCurrentStep / (float)iNumSteps) * 100.0f );
            ++iCurrentStep;

            sprintf_s( cMsg, 512, "Serializing generic animation for root model '%s'.", exportModelList[i]->GetAnimData()->GetCharacter(0)->GetInputModelName( 0 ) );
            SetProgressMessage( cMsg );

			const RexAnimMarkupData* pAnimData = exportModelList[i]->GetAnimData();
			const RexCharAnimData* pCharData = pAnimData->GetCharacter(0);
			const RexMbAnimSegmentData* pSegmentData = pAnimData->GetSegment(0);
			const vector<RexMbAnimExportDataBase*> pExportDataList = exportModelList[i]->GetExportDataList();
			
			bool hasCamera = rexMBAnimExportCommon::HasCamera( pAnimData );

			int pathCount = pSegmentData->GetNumAltOutputPaths();
			// We want it to run at least once.  GetAnimFilePath and GetClipFilePath checks if there are actually no alt 
			// output paths and deals with it accordingly.
			if ( pathCount == 0 ) { pathCount = 1; }
			for ( int idxPath = 0; idxPath < pathCount; idxPath++)
			{
				char* pAnimPath = GetAnimFilePath(pAnimData, pCharData, pSegmentData, idxPath);
				char pOutputPath[RAGE_MAX_PATH];
				ASSET.RemoveNameFromPath(pOutputPath, RAGE_MAX_PATH, pAnimPath);

				bResult = exportModelList[i]->SerializeAnimation( *(animList[i]), pOutputPath );
				bResult = bResult && SaveClipData( pAnimData, pCharData, pSegmentData, pExportDataList, rexMBAnimExportCommon::GetSceneStartTime(), rexMBAnimExportCommon::GetSceneEndTime(), idxPath, hasCamera );
			}

            SetProgressPercent( ((float)iCurrentStep / (float)iNumSteps) * 100.0f );
            ++iCurrentStep;

#if PROFILE_CUTSCENE_EXPORT
            if ( rexMBTimerLog::IsTimerLogEnabled() )
            {
                rexMBTimerLog::EndTimer( exportModelList[i]->GetAnimData()->GetInputModelName( 0 ) );
            }
#endif //PROFILE_CUTSCENE_EXPORT

            if ( !bResult )
            {
                break;
            }
        }

#if PROFILE_ANIM_EXPORT
        if ( rexMBTimerLog::IsTimerLogEnabled() )
        {
            rexMBTimerLog::EndTimer( "Convert and Serialize Animations" );
        }
#endif //PROFILE_CUTSCENE_EXPORT
    }

    // cleanup
    for ( int i = 0; i < (int)animList.size(); ++i )
    {
        delete animList[i];
    }

    SetProgressPercent( 100.0f );
    ++m_iCurrentExportProgressItem;

#if PROFILE_ANIM_EXPORT
    if(rexMBTimerLog::IsTimerLogEnabled())
    {
        rexMBTimerLog::EndTimer("RexRageAnimExport::DoExportInternal");
    }
#endif //PROFILE_ANIM_EXPORT

    return bResult;
}

bool RexRageAnimExport::ValidateAnimationData( RexMbAnimExportModel *pExportModel )
{
	bool bResult = true;
	const char* pTakeName = "";
	if(pExportModel->GetAnimData()->GetSegmetFromTakes())
	{
		pTakeName = pExportModel->GetAnimData()->GetSegment(0)->GetTakeName();
	}

	const std::vector<RexMbAnimExportDataBase *>& trackExportData = pExportModel->GetExportDataList();

	for(int i=0; i < trackExportData.size(); ++i)
	{
		RexMbAnimExportDataBase * pData = dynamic_cast<RexMbAnimExportDataBase *>(trackExportData[i]);

		if(pData)
		{
			const atArray<rexObjectGenericAnimation::TrackInfo *>& trackData = pData->GetTracks();
			for(int j=0; j < trackData.GetCount(); ++j)
			{
				rexObjectGenericAnimation::TrackInfoVector3 * vTrack = dynamic_cast<rexObjectGenericAnimation::TrackInfoVector3 *>(trackData[j]);
				if(vTrack)
				{
					
					for(int k=0; k < vTrack->GetNumKeys(); ++k)
					{
						Vector3 v;
						vTrack->GetVector3Key(k, v);

						if(trackData[j]->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
						{
							if(!rexMBAnimExportCommon::ValidateBoneOffset( pData->GetModel()->Name.AsString(), v ))
								break;
						}

						if(!IsFiniteAll(VECTOR3_TO_VEC3V(v)))
						{
							if(trackData[j]->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
								ULOGGER_MOBO.SetProgressMessage("ERROR: '%s'/'%s' has invalid translation data: %f, %f, %f", pData->GetModel()->LongName.AsString(), pTakeName, v.x, v.y, v.z);
							else if(trackData[j]->m_TrackID == rexObjectGenericAnimation::TRACK_SCALE)
								ULOGGER_MOBO.SetProgressMessage("ERROR: '%s'/'%s' has invalid scale data: %f, %f, %f", pData->GetModel()->LongName.AsString(), pTakeName, v.x, v.y, v.z);
							bResult = false;
							break;
						}
					}
				}

				rexObjectGenericAnimation::TrackInfoQuaternion * qTrack = dynamic_cast<rexObjectGenericAnimation::TrackInfoQuaternion *>(trackData[j]);
				if(qTrack)
				{
					for(int k=0; k < qTrack->GetNumKeys(); ++k)
					{
						Quaternion q;
						qTrack->GetQuaternionKey(k, q);

						if(!IsFiniteAll(QUATERNION_TO_QUATV(q)))
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: '%s'/'%s' has invalid rotation data: %f, %f, %f", pData->GetModel()->LongName.AsString(), pTakeName, q.x, q.y, q.z);
							bResult = false;
							break;
						}
					}
				}

				rexObjectGenericAnimation::TrackInfoFloat * fTrack = dynamic_cast<rexObjectGenericAnimation::TrackInfoFloat *>(trackData[j]);
				if(fTrack)
				{
					for(int k=0; k < fTrack->GetNumKeys(); ++k)
					{
						float f;
						fTrack->GetFloatKey(k, f);

						if(!FPIsFinite(f))
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: '%s'/'%s' has invalid float data: %f", pData->GetModel()->LongName.AsString(), pTakeName, f);
							bResult = false;
							break;
						}
					}
				}
			}
		}
	}

	return bResult;
}


//-----------------------------------------------------------------------------

//PURPOSE : The purpose of this method is to take a RexMbAnimData structure which
//has segments defined from multiple "Takes", and sort them out into multiple 
//RexMbAnimData structures which each contain segments only from the same take.
void RexRageAnimExport::SplitAnimDataByTakes(const RexAnimMarkupData* m_pAnimData, 
                                                  vector< RexAnimMarkupData* >& outSplitAnimData) const
{
    //Iterate over each segment in the source animdata, and create separate animdata structures
    //containing only segments from the same take
    int nTakes = m_pAnimData->GetNumSegments();
    for(int takeIdx=0; takeIdx < nTakes; takeIdx++)
    {
        RexMbAnimSegmentData* pSrcSegData = m_pAnimData->GetSegment(takeIdx);

        //If the take/segment isn't marked as being exportable, then just skip it
        if(!pSrcSegData->IsMarkedForExport())
            continue;

        //Look for an animdata structure that has already been added to the output list
        //that contains segments from the same take as the take from the source
        RexAnimMarkupData* pSplitAnimData = NULL;

        for( vector< RexAnimMarkupData* >::iterator it = outSplitAnimData.begin();
            it != outSplitAnimData.end(); 
            ++it)
        {
            RexAnimMarkupData* pCurAnimData = *it;
            if(pCurAnimData->GetNumSegments())
            {
                HFBTake take = pCurAnimData->GetSegment(0)->GetTake();
                if(take == pSrcSegData->GetTake())
                {
                    //Found a matching take...
                    pSplitAnimData = pCurAnimData;
                    break;
                }
            }
        }

        if(pSplitAnimData == NULL)
        {
            //No animdata structures have been created yet for segments from the source take
            //so create a new one
            pSplitAnimData = new RexAnimMarkupData();
        }

        //Copy across the global information from the source animdata structure.
		//
        pSplitAnimData->m_version = m_pAnimData->m_version;
        pSplitAnimData->m_outputPath = m_pAnimData->m_outputPath;
        pSplitAnimData->m_ctrlFilePath = m_pAnimData->m_ctrlFilePath;
		pSplitAnimData->m_originObjectName = m_pAnimData->m_originObjectName;
		pSplitAnimData->m_attributeFilePath = m_pAnimData->m_attributeFilePath;
        pSplitAnimData->m_segmentFromTakes = m_pAnimData->m_segmentFromTakes;
		pSplitAnimData->m_bUseInitOffset = m_pAnimData->m_bUseInitOffset;
		pSplitAnimData->m_bUsedInNMBlend = m_pAnimData->m_bUsedInNMBlend;
		pSplitAnimData->m_bUseTakeOutputPaths = m_pAnimData->m_bUseTakeOutputPaths;
		pSplitAnimData->m_bStoryMode = m_pAnimData->m_bStoryMode;
		pSplitAnimData->m_bEnableCameraDOF = m_pAnimData->m_bEnableCameraDOF;

		for( atArray<RexCharAnimData*>::const_iterator it = m_pAnimData->m_characters.begin();
			it != m_pAnimData->m_characters.end(); ++it)
		{
			
			const RexCharAnimData& character = *(*it);
			pSplitAnimData->m_characters.PushAndGrow( new RexCharAnimData( character ) );
		}
		

		//Copy the source segment, and add it to the segment list for the new animdata structure
		RexMbAnimSegmentData* pSplitSegData = new RexMbAnimSegmentData(*pSrcSegData);
		pSplitAnimData->m_segments.PushAndGrow(pSplitSegData);

		//The take has been isolated; now separate these animation data pieces
		//by models.
		//
		vector< RexAnimMarkupData* > splitByModels;
		SplitAnimDataByModels(pSplitAnimData, splitByModels);

        //Add the resulting animation data structures to the output list.
		outSplitAnimData.insert(outSplitAnimData.end(), 
			splitByModels.begin(),
			splitByModels.end());
    }
}

//-----------------------------------------------------------------------------

//PURPOSE : This method takes in an animation markup object that may have
//multiple root-mover model pairs specified for export, and splits it up
//into separate markup objects where each object contains only a reference
//to a single root-mover model pair.
void RexRageAnimExport::SplitAnimDataByModels(const RexAnimMarkupData* m_pAnimData, 
                                                   vector< RexAnimMarkupData* >& outSplitAnimData) const
{
    int nSrcCharacters = m_pAnimData->GetNumCharacters();

    if( nSrcCharacters == 1 )
    {
        RexAnimMarkupData* pSplitAnimData = new RexAnimMarkupData( *m_pAnimData );
        outSplitAnimData.push_back(pSplitAnimData);
        return;
    }

    for(int modelIdx = 0; modelIdx < nSrcCharacters; modelIdx++)
    {
        RexAnimMarkupData* pSplitAnimData = new RexAnimMarkupData();

        pSplitAnimData->m_version = m_pAnimData->m_version;
        pSplitAnimData->m_outputPath = m_pAnimData->m_outputPath;
        pSplitAnimData->m_ctrlFilePath = m_pAnimData->m_ctrlFilePath;
		pSplitAnimData->m_originObjectName = m_pAnimData->m_originObjectName;
		pSplitAnimData->m_attributeFilePath = m_pAnimData->m_attributeFilePath;
        pSplitAnimData->m_segmentFromTakes = m_pAnimData->m_segmentFromTakes;
		pSplitAnimData->m_bUseInitOffset = m_pAnimData->m_bUseInitOffset;
		pSplitAnimData->m_bUsedInNMBlend = m_pAnimData->m_bUsedInNMBlend;
		pSplitAnimData->m_bUseTakeOutputPaths = m_pAnimData->m_bUseTakeOutputPaths;
		pSplitAnimData->m_bStoryMode = m_pAnimData->m_bStoryMode;
		pSplitAnimData->m_bEnableCameraDOF = m_pAnimData->m_bEnableCameraDOF;

		RexCharAnimData *pNewCharData = new RexCharAnimData( *(m_pAnimData->m_characters[modelIdx]) );
		// Ignore if not marked for export
		if(!pNewCharData->IsMarkedForExport())
			continue;

        pSplitAnimData->m_characters.Grow() = pNewCharData;

        for(atArray< RexMbAnimSegmentData* >::const_iterator it = m_pAnimData->m_segments.begin();
            it != m_pAnimData->m_segments.end();
            ++it)
        {
            RexMbAnimSegmentData *pSegData = new RexMbAnimSegmentData( *(*it) );
			RexCharAnimData* pCharData = pSplitAnimData->m_characters[0];

            //Set just the root model and mover model names in the segment to match the split animdata model names
            pSegData->m_inputModelNames.clear();
            pSegData->m_inputModelNames.Grow() = pCharData->GetInputModelName(0);

            pSegData->m_moverModelNames.clear();
            pSegData->m_moverModelNames.Grow() = pCharData->GetMoverModelName(0);

			//Accept the model's chosen skeleton as the .skel file to use for this segment.
			pSegData->m_skeletonFilePath = pCharData->m_skeleton;

            pSplitAnimData->m_segments.Grow() = pSegData;
        }

        outSplitAnimData.push_back(pSplitAnimData);
    }//end for(int modelIdx...
}

//-----------------------------------------------------------------------------
char* RexRageAnimExport::GetClipFilePath(const RexAnimMarkupData* pAnimData, const RexCharAnimData* pCharData, const RexMbAnimSegmentData* pSegmentData, const int idxOutputPath)
{
	const char *cClipExtension = NULL;
	if(rexMBAnimExportCommon::GetUseClipXml())
		cClipExtension = ".clipxml";
	else
		cClipExtension = ".clip";
	static char cClipFilePath[RAGE_MAX_PATH]; 
	const char cDivider = '\\';
	const char* cDividerStr = "\\";
	const char cAltDivider = '/';
	const char* cOutputPath;
	const char* cAltOutputPath;

	//If true, get the take output path setting from SegmentData.
	// If not ticked then get the output path from CharData.
	if(pAnimData->m_bUseTakeOutputPaths == true && pSegmentData->GetNumAltOutputPaths() > 0) 
	{ 
		cAltOutputPath = pSegmentData->GetAltOutputPath(idxOutputPath); 
	}
	else { cAltOutputPath = pCharData->GetAltOutputPath(); }

	// Fallback on default output path if nothing specified
	if(strcmp(cAltOutputPath, "(none specified)") != 0)	{ cOutputPath = cAltOutputPath; }	
	else { cOutputPath = pAnimData->GetOutputPath(); }

	cClipFilePath[0] = 0;
	safecat( cClipFilePath, cOutputPath, sizeof( cClipFilePath ));
	
	int length = strlen(cClipFilePath);
	if ( cClipFilePath[length-1] != cDivider )
		safecat( cClipFilePath, cDividerStr, sizeof( cClipFilePath )); // for some reason safecat wont cat this so need to use strcat
	else if (cClipFilePath[length-1] != cAltDivider)
		cClipFilePath[length-1] = cDivider;

	safecat( cClipFilePath, pCharData->GetAnimPrefix(), sizeof( cClipFilePath ));

	// Use the Take Name override string if it doesn't equal null or empty.
	const char* overrideName = pSegmentData->GetTakeNameOverride();
	if ( overrideName == NULL || strlen(overrideName) == 0)
	{
		safecat( cClipFilePath, pSegmentData->GetTakeName(), sizeof( cClipFilePath ));
	}
	else
	{
		safecat( cClipFilePath,overrideName, sizeof( cClipFilePath ));
	}

	safecat( cClipFilePath, pCharData->GetAnimSuffix(), sizeof( cClipFilePath ));
	safecat( cClipFilePath, cClipExtension, sizeof( cClipFilePath ));
	return cClipFilePath;
}

//-----------------------------------------------------------------------------
char* RexRageAnimExport::GetAnimFilePath(const RexAnimMarkupData* pAnimData, const RexCharAnimData* pCharData, const RexMbAnimSegmentData* pSegmentData, const int idxOutputPath)
{
	const char *cAnimExtension = ".anim";
	static char cAnimFilePath[RAGE_MAX_PATH]; 
	const char cDivider = '\\';
	const char* cDividerStr = "\\";
	const char cAltDivider = '/';
	const char* cOutputPath;
	const char* cAltOutputPath;

	//If true, get the take output path setting from SegmentData, fallback on default if not.
	// If not ticked then get the output path from CharData.
	if(pAnimData->m_bUseTakeOutputPaths == true && pSegmentData->GetNumAltOutputPaths() > 0) 
	{ 
		cAltOutputPath = pSegmentData->GetAltOutputPath(idxOutputPath); 
	}
	else { cAltOutputPath = pCharData->GetAltOutputPath(); }

	// Fallback on default if nothing specified
	if(strcmp(cAltOutputPath, "(none specified)") != 0)	{ cOutputPath = cAltOutputPath; }	
	else { cOutputPath = pAnimData->GetOutputPath(); }

	cAnimFilePath[0] = 0;
	safecat( cAnimFilePath, cOutputPath, sizeof( cAnimFilePath ));
	
	int length = strlen(cAnimFilePath);
	if ( cAnimFilePath[length-1] != cDivider )
		safecat( cAnimFilePath, cDividerStr, sizeof( cAnimFilePath )); // for some reason safecat wont cat this so need to use strcat
	else if (cAnimFilePath[length-1] != cAltDivider)
		cAnimFilePath[length-1] = cDivider;

	safecat( cAnimFilePath, pCharData->GetAnimPrefix(), sizeof( cAnimFilePath ));

	// Use the Take Name override string if it doesn't equal null or empty.
	const char* overrideName = pSegmentData->GetTakeNameOverride();
	if ( overrideName == NULL || strlen(overrideName) == 0)
	{
		safecat( cAnimFilePath, pSegmentData->GetTakeName(), sizeof( cAnimFilePath ));
	}
	else
	{
		safecat( cAnimFilePath, overrideName, sizeof( cAnimFilePath ));
	}

	safecat( cAnimFilePath, pCharData->GetAnimSuffix(), sizeof( cAnimFilePath ));
	safecat( cAnimFilePath, cAnimExtension, sizeof( cAnimFilePath ));
	return cAnimFilePath;
}

//-----------------------------------------------------------------------------
bool RexRageAnimExport::CreateMoverFromString( const char* pMoverStr, HFBModel hRootModel )
{
	atString sMoverStr = atString( pMoverStr );
	atArray<atString> moverTokens;
	atArray<atString> moverHeaderTokens;

	int nPosKeys;
	int nRotKeys;
	atString sParentName("");
	HFBModel mover = NULL;

	mover = RAGEFindModelByName("mover");	
	// Create the mover if it doesn't already exist
	if( !mover )
	{
		mover = (HFBModel)(new FBModelCube( "mover" ));
		mover->Scaling = FBVector3d( 20.0, 20.0, 20.0 );
		FBModel* pDummyNode = hRootModel->Parent;
		if(pDummyNode)
		{
			mover->Parent = pDummyNode;
			// Align the mover with the dummy so all animations will be applied
			// in local space correctly
			mover->SetVector(FBVector3d(0.0, 0.0, 0.0), kModelTranslation, false);
		}
	}
	if(mover)
	{
		
		mover->Show = true;
		sMoverStr.Split( moverTokens, "," );
		if( moverTokens.GetCount() > 0 )
		{
			moverTokens[0].Split( moverHeaderTokens, " " );
			if( moverHeaderTokens.GetCount() >= 4 )
			{
				nPosKeys = atoi(moverHeaderTokens[1].c_str());
				nRotKeys = atoi(moverHeaderTokens[2].c_str());
				sParentName = moverHeaderTokens[3].c_str();

				FBAnimationNode* pMoverAnimNode = mover->AnimationNodeInGet();
				FBAnimationNode* pTransNode = rexMBAnimExportCommon::FindAnimByName(pMoverAnimNode,"Lcl Translation");
				FBAnimationNode* pTransXNode = rexMBAnimExportCommon::FindAnimByName(pTransNode,"X");
				FBAnimationNode* pTransYNode = rexMBAnimExportCommon::FindAnimByName(pTransNode,"Y");
				FBAnimationNode* pTransZNode = rexMBAnimExportCommon::FindAnimByName(pTransNode,"Z");

				FBAnimationNode* pRotNode = rexMBAnimExportCommon::FindAnimByName(pMoverAnimNode,"Lcl Rotation");
				FBAnimationNode* pRotXNode = rexMBAnimExportCommon::FindAnimByName(pRotNode,"X");
				FBAnimationNode* pRotYNode = rexMBAnimExportCommon::FindAnimByName(pRotNode,"Y");
				FBAnimationNode* pRotZNode = rexMBAnimExportCommon::FindAnimByName(pRotNode,"Z");

				for( int i = 1; i < ( nPosKeys + 1 ); i++ )
				{
					atArray<atString> lineTokens;
					atString sLineBuffer = moverTokens[i];
					sLineBuffer.Split( lineTokens, " " );

					FBTime time;

					float frame = (float) atof(lineTokens[0]);
					float xVal = (float) atof(lineTokens[1].c_str());
					float yVal = (float) atof(lineTokens[13].c_str());
					float zVal = (float) atof(lineTokens[25].c_str());

					time.SetTime(0,0, 0, (int)frame);
					pTransXNode->FCurve->KeyAdd( time, xVal );
					pTransYNode->FCurve->KeyAdd( time, yVal );
					pTransZNode->FCurve->KeyAdd( time, zVal );
				}

				for( int i = nPosKeys + 1; i < ( nRotKeys + nPosKeys + 1 ); i++ )
				{
					atArray<atString> lineTokens;
					atString sLineBuffer = moverTokens[i];
					sLineBuffer.Split( lineTokens, " " );

					FBTime time;

					float frame = (float) atof(lineTokens[0]);
					float xVal = (float) atof(lineTokens[1].c_str());
					float yVal = (float) atof(lineTokens[13].c_str());
					float zVal = (float) atof(lineTokens[25].c_str());

					time.SetTime(0,0, 0, (int)frame);
					pRotXNode->FCurve->KeyAdd( time, xVal );
					pRotYNode->FCurve->KeyAdd( time, yVal );
					pRotZNode->FCurve->KeyAdd( time, zVal );
				}
			}
		}
		
	}

	return true;
}

//-----------------------------------------------------------------------------
bool RexRageAnimExport::SaveClipData( const RexAnimMarkupData* pAnimData, const RexCharAnimData* pCharData, const RexMbAnimSegmentData* pSegData, const vector<RexMbAnimExportDataBase*>& pExportDataList, float fStartTime, float fEndTime, const int idxClipPath, bool bHasCamera)
{
	if(rexMBAnimExportCommon::GetUseClipXml())
	{
		char* cOutputClip = GetClipFilePath(pAnimData, pCharData, pSegData, idxClipPath);
		rexMBAnimExportCommon::ReplaceWhitespaceCharacters(cOutputClip);
		char* cInputAnim = GetAnimFilePath(pAnimData, pCharData, pSegData, idxClipPath);
		rexMBAnimExportCommon::ReplaceWhitespaceCharacters(cInputAnim);

		crAnimation* pAnim = crAnimation::AllocateAndLoad( cInputAnim );

		if(pAnim == NULL)
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Animation '%s' is not valid.", cInputAnim);
			return false;
		}

		ClipXML* clip = rage_new ClipXML();
		clip->Load(cOutputClip);
		clip->SetStart(0.0f);
		clip->SetEnd(pAnim->GetDuration());
	
		if( bHasCamera )
		{
			AddCameraCutTagBlocks(fStartTime, fEndTime, clip);
		}

		atString skelFileName = atString( pCharData->m_skeleton.c_str() );
		atString compFilePath = atString(ASSET.FileName(pAnimData->GetSegment(0)->m_compressionFilePath.c_str()));
	
		atString ctrlFilePath(pAnimData->m_ctrlFilePath.c_str());
		if(pAnimData->GetSegment(0)->m_ctrlFilePath != "(no spec file)")
		{
			ctrlFilePath = pAnimData->GetSegment(0)->m_ctrlFilePath.c_str();
		}
		else if(strcmp(pAnimData->GetCharacter(0)->GetCtrlFile(), "(no spec file)") != 0)
		{
			ctrlFilePath = pAnimData->GetCharacter(0)->GetCtrlFile();
		}

		PropertyAttributeStringXML* pCompressionFile = rage_new PropertyAttributeStringXML();
		pCompressionFile->SetName( COMPRESSION_FILE_PROPERTY_NAME );
		atString& compressionValue = pCompressionFile->GetString();
		compressionValue = compFilePath;

		PropertyXML* pCompressionFileProperty = rage_new PropertyXML();
		pCompressionFileProperty->SetName( COMPRESSION_FILE_PROPERTY_NAME );
		pCompressionFileProperty->AddAttribute( pCompressionFile );
		clip->AddProperty( pCompressionFileProperty );

		PropertyAttributeStringXML* pSkelFile = rage_new PropertyAttributeStringXML();
		pSkelFile->SetName( SKELETON_FILE_PROPERTY_NAME );
		atString& skelFileValue = pSkelFile->GetString();
		skelFileValue = skelFileName;

		PropertyXML* pSkelFileProperty = rage_new PropertyXML();
		pSkelFileProperty->SetName( SKELETON_FILE_PROPERTY_NAME );
		pSkelFileProperty->AddAttribute( pSkelFile );
		clip->AddProperty( pSkelFileProperty );

		PropertyAttributeIntXML* pPropLooping = rage_new PropertyAttributeIntXML();
		pPropLooping->SetName( LOOPING_PROPERTY_NAME );
		int& loopingValue = pPropLooping->GetInt();
		loopingValue = pSegData->m_bLooping;

		PropertyXML* pLoopingProperty = rage_new PropertyXML();
		pLoopingProperty->SetName( LOOPING_PROPERTY_NAME );
		pLoopingProperty->AddAttribute( pPropLooping );
		clip->AddProperty( pLoopingProperty );

		PropertyAttributeStringXML* pPropMover = rage_new PropertyAttributeStringXML();
		pPropMover->SetName( MOVER_PROPERTY_NAME );
		atString& moverValue = pPropMover->GetString();
		moverValue = pCharData->GetInputModelName(0);

		PropertyXML* pMoverProperty = rage_new PropertyXML();
		pMoverProperty->SetName( MOVER_PROPERTY_NAME );
		pMoverProperty->AddAttribute( pPropMover );
		clip->AddProperty( pMoverProperty );

		PropertyAttributeIntXML* pPropAdditive = rage_new PropertyAttributeIntXML();
		pPropAdditive->SetName( ADDITIVE_PROPERTY_NAME );
		int& additiveValue = pPropAdditive->GetInt();
		additiveValue = pSegData->m_bAdditive;

		PropertyXML* pAdditiveProperty = rage_new PropertyXML();
		pAdditiveProperty->SetName( ADDITIVE_PROPERTY_NAME );
		pAdditiveProperty->AddAttribute( pPropAdditive );
		clip->AddProperty( pAdditiveProperty );

		PropertyAttributeIntXML* pPropLinearCompression = rage_new PropertyAttributeIntXML();
		pPropLinearCompression->SetName( LINEAR_COMPRESSION_PROPERTY_NAME );
		int& linearCompressionValue = pPropLinearCompression->GetInt();
		linearCompressionValue = pSegData->m_bLinearCompression;

		PropertyXML* pLinearCompressionProperty = rage_new PropertyXML();
		pLinearCompressionProperty->SetName( LINEAR_COMPRESSION_PROPERTY_NAME );
		pLinearCompressionProperty->AddAttribute( pPropLinearCompression );
		clip->AddProperty( pLinearCompressionProperty );

		PropertyAttributeStringXML* pPropControlFile = rage_new PropertyAttributeStringXML();
		pPropControlFile->SetName( CONTROL_FILE_PROPERTY_NAME );
		atString& controlFileValue = pPropControlFile->GetString();
		controlFileValue = ctrlFilePath;

		PropertyXML* pControlFileProperty = rage_new PropertyXML();
		pControlFileProperty->SetName( CONTROL_FILE_PROPERTY_NAME );
		pControlFileProperty->AddAttribute( pPropControlFile );
		clip->AddProperty( pControlFileProperty );

		PropertyAttributeIntXML* pPropUsedInNMBlend = rage_new PropertyAttributeIntXML();
		int& usedInNMBlendValue = pPropUsedInNMBlend->GetInt();
		usedInNMBlendValue = pAnimData->m_bUsedInNMBlend;

		PropertyXML* pUsedInNMBlendProperty = rage_new PropertyXML();
		pUsedInNMBlendProperty->SetName( USED_IN_NMBLEND_PROPERTY_NAME );
		pUsedInNMBlendProperty->AddAttribute( pPropUsedInNMBlend );
		clip->AddProperty( pUsedInNMBlendProperty );

		PropertyAttributeStringXML* pUserName = rage_new PropertyAttributeStringXML();
		pUserName->SetName( SOURCE_USER_NAME );
		atString& usernameValue = pUserName->GetString();
		usernameValue = rexMBAnimExportCommon::GetLocalUserName();

		PropertyXML* pUserNameProperty = rage_new PropertyXML();
		pUserNameProperty->SetName( SOURCE_USER_NAME );
		pUserNameProperty->AddAttribute( pUserName );
		clip->AddProperty( pUserNameProperty );

		PropertyAttributeIntXML* pHashUserName = rage_new PropertyAttributeIntXML();
		pHashUserName->SetName( SOURCE_HASH_USER_NAME );
		int& hashUsernameValue = pHashUserName->GetInt();
		hashUsernameValue = atHashString(rexMBAnimExportCommon::GetLocalUserName().c_str()).GetHash();

		PropertyXML* pHashUserNameProperty = rage_new PropertyXML();
		pHashUserNameProperty->SetName( SOURCE_HASH_USER_NAME );
		pHashUserNameProperty->AddAttribute( pHashUserName );
		clip->AddProperty( pHashUserNameProperty );

		PropertyAttributeStringXML* pAbsoluteFBXFile = rage_new PropertyAttributeStringXML();
		pAbsoluteFBXFile->SetName( ABSOLUTE_FBXFILE_PROPERTY_NAME );
		atString& absoluteFbxFileValue = pAbsoluteFBXFile->GetString();
		char cAbsoluteFileName[RAGE_MAX_PATH];
		FBApplication application;
		sprintf(cAbsoluteFileName, "%s", application.FBXFileName.AsString());
		absoluteFbxFileValue = cAbsoluteFileName;

		PropertyXML* pAbsoluteFBXFileProperty = rage_new PropertyXML();
		pAbsoluteFBXFileProperty->SetName( ABSOLUTE_FBXFILE_PROPERTY_NAME );
		pAbsoluteFBXFileProperty->AddAttribute( pAbsoluteFBXFile );
		clip->AddProperty( pAbsoluteFBXFileProperty );

		PropertyAttributeStringXML* pFBXFile = rage_new PropertyAttributeStringXML();
		pFBXFile->SetName( FBXFILE_PROPERTY_NAME );
		atString& fbxFileValue = pFBXFile->GetString();
		char cFileName[RAGE_MAX_PATH];
		//FBApplication application;
		sprintf(cFileName, "%s", application.FBXFileName.AsString());

		// Replace the art path with $(art)
		atString artPath = rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString(""));
		artPath.Lowercase();
		artPath.Replace("/","\\");

		fbxFileValue = cFileName;
		fbxFileValue.Lowercase();
		fbxFileValue.Replace("/","\\");

		fbxFileValue.Replace(artPath.c_str(), "$(art)");

		PropertyXML* pFBXFileProperty = rage_new PropertyXML();
		pFBXFileProperty->SetName( FBXFILE_PROPERTY_NAME );
		pFBXFileProperty->AddAttribute( pFBXFile );
		clip->AddProperty( pFBXFileProperty );

		PropertyAttributeStringXML* pVersionAttribute = rage_new PropertyAttributeStringXML();
		pVersionAttribute->SetName( EXPORTER_VERSION_PROPERTY_NAME );
		atString& versionValue = pVersionAttribute->GetString();
		char cVersion[RAGE_MAX_PATH];
		sprintf(cVersion, "%d.%d.%d", s_ingame_exporter_version.Major, s_ingame_exporter_version.Minor, s_ingame_exporter_version.Revision);
		versionValue = cVersion;

		PropertyXML* pVersionProperty = rage_new PropertyXML();
		pVersionProperty->SetName( EXPORTER_VERSION_PROPERTY_NAME );
		pVersionProperty->AddAttribute( pVersionAttribute );
		clip->AddProperty( pVersionProperty );

		PropertyAttributeStringXML* pP4Revision = rage_new PropertyAttributeStringXML();
		pP4Revision->SetName( PERFORCE_REVISION_PROPERTY_NAME );
		atString& p4RevisionValue = pP4Revision->GetString();
		p4RevisionValue = GetP4RevisionInfo(pCharData->GetInputModelName(0));

		PropertyXML* pP4RevisionProperty = rage_new PropertyXML();
		pP4RevisionProperty->SetName( PERFORCE_REVISION_PROPERTY_NAME );
		pP4RevisionProperty->AddAttribute( pP4Revision );
		clip->AddProperty( pP4RevisionProperty );

		if( stricmp(pCharData->GetModelType(), "camera") == 0 )
		{
			PropertyAttributeBoolXML* pCameraApproved = rage_new PropertyAttributeBoolXML();
			pCameraApproved->SetName( CAMERA_APPROVED_PROPERTY_NAME );
			bool& cameraApprovedValue = pCameraApproved->GetBool();
			cameraApprovedValue = false;

			if(clip->FindProperty(CAMERA_APPROVED_PROPERTY_NAME))
			{
				PropertyXML* pCameraApprovedProperty = rage_new PropertyXML();
				pCameraApprovedProperty->SetName( CAMERA_APPROVED_PROPERTY_NAME );
				pCameraApprovedProperty->AddAttribute( pCameraApproved );
			
				clip->AddProperty(pCameraApprovedProperty);
			}
		}

		// We check if the scene changed since last export to flag it dirty. Check the discarded frame string and the start/end range. If any of these
		// differ then mark the scene dirty. This will need to be marked as non-dirty externally.
		//if(pAnimData->m_bStoryMode)
		//{
			atString strDiscardedFrames("");
			rexMBAnimExportCommon::GetDiscardFrameArrayAsString(strDiscardedFrames);

			if(clip->FindProperty(DISCARDED_FRAMES_PROPERTY_NAME) && clip->FindProperty(RANGE_START_PROPERTY_NAME) && clip->FindProperty(RANGE_END_PROPERTY_NAME))
			{
				PropertyAttributeStringXML* pPropDiscarded = dynamic_cast<PropertyAttributeStringXML*>(clip->FindProperty(DISCARDED_FRAMES_PROPERTY_NAME)->GetAttribute(DISCARDED_FRAMES_PROPERTY_NAME));
				PropertyAttributeIntXML* pPropStartRange = dynamic_cast<PropertyAttributeIntXML*>(clip->FindProperty(DISCARDED_FRAMES_PROPERTY_NAME)->GetAttribute(RANGE_START_PROPERTY_NAME));
				PropertyAttributeIntXML* pPropEndRange = dynamic_cast<PropertyAttributeIntXML*>(clip->FindProperty(DISCARDED_FRAMES_PROPERTY_NAME)->GetAttribute(RANGE_END_PROPERTY_NAME));
				if(pPropDiscarded && pPropStartRange && pPropEndRange)
				{
					if(stricmp(strDiscardedFrames.c_str(), pPropDiscarded->GetString().c_str()) != 0 ||
						rexMBAnimExportCommon::GetFrameFromTime(fStartTime) != pPropStartRange->GetInt() ||
						rexMBAnimExportCommon::GetFrameFromTime(fEndTime) != pPropEndRange->GetInt())
					{
						PropertyAttributeBoolXML* pPropDirty = rage_new PropertyAttributeBoolXML();
						pPropDirty->SetName( SCENE_DIRTY_PROPERTY_NAME );
						bool& dirtySceneValue = pPropDirty->GetBool();
						dirtySceneValue = true;

						PropertyXML* pDirtyProperty = rage_new PropertyXML();
						pDirtyProperty->SetName( SCENE_DIRTY_PROPERTY_NAME );
						pDirtyProperty->AddAttribute( pPropDirty );
						clip->AddProperty( pDirtyProperty );
					}
				}
			}

			PropertyAttributeStringXML* pPropDiscardedFrames = rage_new PropertyAttributeStringXML();
			pPropDiscardedFrames->SetName( DISCARDED_FRAMES_PROPERTY_NAME );
			atString& discardedFramesValue = pPropDiscardedFrames->GetString();
			discardedFramesValue = strDiscardedFrames;

			PropertyXML* pDiscardedFramesProperty = rage_new PropertyXML();
			pDiscardedFramesProperty->SetName( DISCARDED_FRAMES_PROPERTY_NAME );
			pDiscardedFramesProperty->AddAttribute( pPropDiscardedFrames );
			clip->AddProperty( pDiscardedFramesProperty );
		//}

		PropertyAttributeIntXML* pPropStartRange = rage_new PropertyAttributeIntXML();
		pPropStartRange->SetName( RANGE_START_PROPERTY_NAME );
		int& startRangeValue = pPropStartRange->GetInt();
		startRangeValue = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);

		PropertyXML* pStartRangeProperty = rage_new PropertyXML();
		pStartRangeProperty->SetName( RANGE_START_PROPERTY_NAME );
		pStartRangeProperty->AddAttribute( pPropStartRange );
		clip->AddProperty( pStartRangeProperty );

		PropertyAttributeIntXML* pPropEndRange = rage_new PropertyAttributeIntXML();
		pPropEndRange->SetName( RANGE_END_PROPERTY_NAME );
		int& endRangeValue = pPropEndRange->GetInt();
		endRangeValue = rexMBAnimExportCommon::GetFrameFromTime(fEndTime);

		PropertyXML* pEndRangeProperty = rage_new PropertyXML();
		pEndRangeProperty->SetName( RANGE_END_PROPERTY_NAME );
		pEndRangeProperty->AddAttribute( pPropEndRange );
		clip->AddProperty( pEndRangeProperty );

		// If we're using an initial offset then extract the first frame of animation from the mover
		if( pAnimData->m_bUseInitOffset )
		{
			FBSystem system;
			FBPlayerControl controller;       

			FBTime preSampleTime = system.LocalTime;

			FBTime fbt;
			fbt.SetSecondDouble(fStartTime);
			controller.Goto(fbt);

			system.Scene->Evaluate();

			// If the new origin object exists in the scene then we use this
			HFBModel originModel = RAGEFindModelByName((char*)pAnimData->m_originObjectName.c_str());
			if(originModel != NULL)
			{
				for ( vector<RexMbAnimExportDataBase *>::const_iterator it = pExportDataList.begin(); it != pExportDataList.end(); ++it )
				{
					atString sBoneName = atString((const char*)(*it)->GetModel()->LongName);

					atString sBoneNameOriginal = sBoneName;
					int colonPos = sBoneName.LastIndexOf(":");
					sBoneName.Set(sBoneName, colonPos + 1, sBoneName.GetLength() - colonPos);

					if(sBoneName == "mover")
					{
						HFBModel moverModel = RAGEFindModelByName((char*)sBoneNameOriginal.c_str());
						FBMatrix moverTransformationMatrixFB;
						moverTransformationMatrixFB.Identity();
						moverModel->GetMatrix(moverTransformationMatrixFB, kModelTransformation, true);

						Matrix34 moverTransformationMatrixRage;
						moverTransformationMatrixRage.Identity();
						FBMatrixToRageMatrix34(moverTransformationMatrixFB, moverTransformationMatrixRage);
						moverTransformationMatrixRage.Normalize();

						// Get the matrix for the origin object
						FBMatrix originTransformationMatrixFB;
						originTransformationMatrixFB.Identity();
						originModel->GetMatrix(originTransformationMatrixFB, kModelTransformation, true);

						Matrix34 originTransformationMatrixRage;
						originTransformationMatrixRage.Identity();
						FBMatrixToRageMatrix34(originTransformationMatrixFB, originTransformationMatrixRage);
						originTransformationMatrixRage.Normalize();

						//////////////////////////////////////////////////////////////////////////

						std::string sMoverDummyName = sBoneNameOriginal.c_str();
						colonPos = (int)sMoverDummyName.find_last_of(":");
						sMoverDummyName = sMoverDummyName.substr(0,colonPos);
						sMoverDummyName = sMoverDummyName.append(":Dummy01");
					
						Matrix34 moverDummyTransformationMatrixRage;
						moverDummyTransformationMatrixRage.Identity();

						HFBModel moverDummyModel = RAGEFindModelByName((char*)sMoverDummyName.c_str());
						if(moverDummyModel)
						{
							FBMatrix moverDummyTransformationMatrixFB;
							moverDummyModel->GetMatrix(moverDummyTransformationMatrixFB, kModelTransformation, true);

						
							FBMatrixToRageMatrix34(moverDummyTransformationMatrixFB, moverDummyTransformationMatrixRage);
							moverDummyTransformationMatrixRage.Normalize();
						}
						else
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to find Dummy01 object, expected %s", sMoverDummyName.c_str());
							return false;
						}

						std::string sOriginDummyName = (char*)pAnimData->m_originObjectName.c_str();
						colonPos = (int)sOriginDummyName.find_last_of(":");
						sOriginDummyName = sOriginDummyName.substr(0,colonPos);
						sOriginDummyName = sOriginDummyName.append(":Dummy01");
					
						Matrix34 originDummyTransformationMatrixRage;
						originDummyTransformationMatrixRage.Identity();

						HFBModel originDummyModel = RAGEFindModelByName((char*)sOriginDummyName.c_str());
						if(originDummyModel)
						{
							FBMatrix originDummyTransformationMatrixFB;
							originDummyModel->GetMatrix(originDummyTransformationMatrixFB, kModelTransformation, true);

						
							FBMatrixToRageMatrix34(originDummyTransformationMatrixFB, originDummyTransformationMatrixRage);
							originDummyTransformationMatrixRage.Normalize();
						}
					
						originTransformationMatrixRage.DotTranspose(originDummyTransformationMatrixRage);
						moverTransformationMatrixRage.DotTranspose(moverDummyTransformationMatrixRage);

		
						moverTransformationMatrixRage.DotTranspose(originTransformationMatrixRage);
					
						// Build our final translation vector and rotation quaternion
						Vec3V clipVec(moverTransformationMatrixRage.d[0],moverTransformationMatrixRage.d[1],moverTransformationMatrixRage.d[2]);
						clipVec /= ScalarVFromF32(100.f);

						Quaternion moverQuaternion;
						moverTransformationMatrixRage.ToQuaternion(moverQuaternion);
						moverQuaternion.Normalize();

						PropertyAttributeQuaternionXML* pPropInitOffsetRot = rage_new PropertyAttributeQuaternionXML();
						pPropInitOffsetRot->SetName( INIT_OFFSET_ROT_PROPERTY_NAME );
						pPropInitOffsetRot->FromQuaternion(QUATERNION_TO_QUATV(moverQuaternion));

						PropertyXML* pInitOffsetRotationProperty = rage_new PropertyXML();
						pInitOffsetRotationProperty->SetName(INIT_OFFSET_ROT_PROPERTY_NAME);	
						pInitOffsetRotationProperty->AddAttribute(pPropInitOffsetRot);
						clip->AddProperty( pInitOffsetRotationProperty );

						PropertyAttributeVector3XML* pPropInitOffsetPos = rage_new PropertyAttributeVector3XML();
						pPropInitOffsetPos->SetName( INIT_OFFSET_POS_PROPERTY_NAME );
						Vec3V_Ref offsetPositionValue = pPropInitOffsetPos->GetVector3();
						offsetPositionValue = clipVec;

						PropertyXML* pInitOffsetPositionProperty = rage_new PropertyXML();
						pInitOffsetPositionProperty->SetName(INIT_OFFSET_POS_PROPERTY_NAME);	
						pInitOffsetPositionProperty->AddAttribute(pPropInitOffsetPos);
						clip->AddProperty( pInitOffsetPositionProperty );
					}
				}
			}
			else
			{
				for ( vector<RexMbAnimExportDataBase *>::const_iterator it = pExportDataList.begin(); it != pExportDataList.end(); ++it )
				{
					atString sBoneName = atString((const char*)(*it)->GetModel()->LongName);
					atString sBoneNameOriginal = sBoneName;

					int colonPos = sBoneName.LastIndexOf(":");
					sBoneName.Set(sBoneName, colonPos + 1, sBoneName.GetLength() - colonPos);

					if(sBoneName == "mover")
					{
						HFBModel moverModel = RAGEFindModelByName((char*)sBoneNameOriginal.c_str());
						FBMatrix moverTransformationMatrixFB;
						moverModel->GetMatrix(moverTransformationMatrixFB, kModelTransformation, true);

						Matrix34 moverTransformationMatrixRage;
						moverTransformationMatrixRage.Identity();
						FBMatrixToRageMatrix34(moverTransformationMatrixFB, moverTransformationMatrixRage);
						moverTransformationMatrixRage.Normalize();

						//////////////////////////////////////////////////////////////////////////

						std::string sDummyName = sBoneNameOriginal.c_str();
						colonPos = (int)sDummyName.find_last_of(":");
						sDummyName = sDummyName.substr(0,colonPos);
						sDummyName = sDummyName.append(":Dummy01");

						HFBModel dummyModel = RAGEFindModelByName((char*)sDummyName.c_str());
						if(dummyModel)
						{
							FBMatrix dummyTransformationMatrixFB;
							dummyModel->GetMatrix(dummyTransformationMatrixFB, kModelTransformation, true);

							Matrix34 dummyTransformationMatrixRage;
							dummyTransformationMatrixRage.Identity();
							FBMatrixToRageMatrix34(dummyTransformationMatrixFB, dummyTransformationMatrixRage);
							dummyTransformationMatrixRage.Normalize();

							moverTransformationMatrixRage.DotTranspose(dummyTransformationMatrixRage);
						}

						//////////////////////////////////////////////////////////////////////////

						Quaternion moverQuaternion;
						moverTransformationMatrixRage.ToQuaternion(moverQuaternion);

						moverQuaternion.Normalize();
						QuatV clipQuat(RC_QUATV(moverQuaternion));

						Vec3V clipVec(moverTransformationMatrixRage.d[0],moverTransformationMatrixRage.d[1],moverTransformationMatrixRage.d[2]);
						clipVec /= ScalarVFromF32(100.f);

						//////////////////////////////////////////////////////////////////////////

						PropertyAttributeQuaternionXML* pPropInitOffsetRot = rage_new PropertyAttributeQuaternionXML();
						pPropInitOffsetRot->SetName( INIT_OFFSET_ROT_PROPERTY_NAME );
						pPropInitOffsetRot->FromQuaternion(QUATERNION_TO_QUATV(moverQuaternion));

						PropertyXML* pInitOffsetRotationProperty = rage_new PropertyXML();
						pInitOffsetRotationProperty->SetName(INIT_OFFSET_ROT_PROPERTY_NAME);
						pInitOffsetRotationProperty->AddAttribute(pPropInitOffsetRot);
						clip->AddProperty( pInitOffsetRotationProperty );

						PropertyAttributeVector3XML* pPropInitOffsetPos = rage_new PropertyAttributeVector3XML();
						pPropInitOffsetPos->SetName( INIT_OFFSET_POS_PROPERTY_NAME );
						Vec3V_Ref offsetPositionValue = pPropInitOffsetPos->GetVector3();
						offsetPositionValue = clipVec;

						PropertyXML* pInitOffsetPositionProperty = rage_new PropertyXML();
						pInitOffsetPositionProperty->SetName(INIT_OFFSET_POS_PROPERTY_NAME);			
						pInitOffsetPositionProperty->AddAttribute(pPropInitOffsetPos);
						clip->AddProperty( pInitOffsetPositionProperty );
						break;
					}
				}
			}

			controller.Goto(preSampleTime);
		}

		for( int i = 0; i < clip->GetTags().GetCount(); ++i)
		{
			TagXML* tag = clip->GetTags()[i];

			/*if(tag->GetDuration() < 0 || tag->GetDuration() > 1)
			{
			ULOGGER_MOBO.SetProgressMessage("WARNING: Tag %s has an invalid phase of %f", tag->GetName(), tag->GetDuration());
			}*/

			if(tag->GetStart() > tag->GetEnd())
			{
				ULOGGER_MOBO.SetProgressMessage("WARNING: Tag %s has a start phase %f greater than its end phase %f", tag->GetProperty().GetName(), tag->GetStart(), tag->GetEnd());
			}
		}

		clip->Save( cOutputClip );

		delete clip;
	}
	else
	{
		char* cOutputClip = GetClipFilePath(pAnimData, pCharData, pSegData, idxClipPath);
		rexMBAnimExportCommon::ReplaceWhitespaceCharacters(cOutputClip);
		char* cInputAnim = GetAnimFilePath(pAnimData, pCharData, pSegData, idxClipPath);
		rexMBAnimExportCommon::ReplaceWhitespaceCharacters(cInputAnim);

		crClipAnimation* pClipAnim = static_cast<crClipAnimation*>(crClipAnimation::AllocateAndLoad(cOutputClip));
		crAnimation* pAnim = crAnimation::AllocateAndLoad( cInputAnim );
		if(!pClipAnim)
		{
			if( pAnim )
			{
				pClipAnim = crClipAnimation::AllocateAndCreate( *pAnim );
			}
			else
			{
				pClipAnim = crClipAnimation::Allocate( );
			}
		}
		else
		{
			if( pAnim )
			{
				pClipAnim->SetAnimation( *pAnim );
			}
		}

		if(pAnim == NULL)
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Animation '%s' is not valid.", cInputAnim);
			return false;
		}

		pClipAnim->SetStartEndTimes(0.0f, pClipAnim->GetAnimation()->GetDuration());

		if( bHasCamera )
		{
			AddCameraCutTagBlocks(fStartTime, fEndTime, pClipAnim);
		}

		atString skelFileName = atString( pCharData->m_skeleton.c_str() );
		atString compFilePath = atString(ASSET.FileName(pAnimData->GetSegment(0)->m_compressionFilePath.c_str()));

		atString ctrlFilePath(pAnimData->m_ctrlFilePath.c_str());
		if(pAnimData->GetSegment(0)->m_ctrlFilePath != "(no spec file)")
		{
			ctrlFilePath = pAnimData->GetSegment(0)->m_ctrlFilePath.c_str();
		}
		else if(strcmp(pAnimData->GetCharacter(0)->GetCtrlFile(), "(no spec file)") != 0)
		{
			ctrlFilePath = pAnimData->GetCharacter(0)->GetCtrlFile();
		}

		crProperties* pProperties = pClipAnim->GetProperties();

		pProperties->RemoveProperty(INIT_OFFSET_ROT_PROPERTY_NAME);
		pProperties->RemoveProperty(INIT_OFFSET_POS_PROPERTY_NAME);

		crPropertyAttributeString pCompressionFile;
		pCompressionFile.SetName( COMPRESSION_FILE_PROPERTY_NAME );
		atString& compressionValue = pCompressionFile.GetString();
		compressionValue = compFilePath;

		crProperty pCompressionFileProperty;
		pCompressionFileProperty.SetName( COMPRESSION_FILE_PROPERTY_NAME );
		pCompressionFileProperty.AddAttribute( pCompressionFile );
		pProperties->AddProperty( pCompressionFileProperty );


		crPropertyAttributeString pSkelFile;
		pSkelFile.SetName( SKELETON_FILE_PROPERTY_NAME );
		atString& skelFileValue = pSkelFile.GetString();
		skelFileValue = skelFileName;

		crProperty pSkelFileProperty;
		pSkelFileProperty.SetName( SKELETON_FILE_PROPERTY_NAME );
		pSkelFileProperty.AddAttribute( pSkelFile );
		pProperties->AddProperty( pSkelFileProperty );

		crPropertyAttributeInt pPropLooping;
		pPropLooping.SetName( LOOPING_PROPERTY_NAME );
		int& loopingValue = pPropLooping.GetInt();
		loopingValue = pSegData->m_bLooping;

		crProperty pLoopingProperty;
		pLoopingProperty.SetName( LOOPING_PROPERTY_NAME );
		pLoopingProperty.AddAttribute( pPropLooping );
		pProperties->AddProperty( pLoopingProperty );

		crPropertyAttributeString pPropMover;
		pPropMover.SetName( MOVER_PROPERTY_NAME );
		atString& moverValue = pPropMover.GetString();
		moverValue = pCharData->GetInputModelName(0);

		crProperty pMoverProperty;
		pMoverProperty.SetName( MOVER_PROPERTY_NAME );
		pMoverProperty.AddAttribute( pPropMover );
		pProperties->AddProperty( pMoverProperty );

		crPropertyAttributeInt pPropAdditive;
		pPropAdditive.SetName( ADDITIVE_PROPERTY_NAME );
		int& additiveValue = pPropAdditive.GetInt();
		additiveValue = pSegData->m_bAdditive;

		crProperty pAdditiveProperty;
		pAdditiveProperty.SetName( ADDITIVE_PROPERTY_NAME );
		pAdditiveProperty.AddAttribute( pPropAdditive );
		pProperties->AddProperty( pAdditiveProperty );

		crPropertyAttributeInt pPropAdditiveBA;
		pPropAdditiveBA.SetName( ADDITIVE_BASE_ANIM_PROPERTY_NAME );
		int& additiveBAValue = pPropAdditiveBA.GetInt();
		additiveBAValue = pSegData->m_bAdditiveBaseAnim;

		crProperty pAdditiveBAProperty;
		pAdditiveBAProperty.SetName( ADDITIVE_BASE_ANIM_PROPERTY_NAME );
		pAdditiveBAProperty.AddAttribute( pPropAdditiveBA );
		pProperties->AddProperty( pAdditiveBAProperty );

		crPropertyAttributeInt pPropRevision;
		pPropRevision.SetName( PERFORCE_REVISION_PROPERTY_NAME );
		int& revisionValue = pPropRevision.GetInt();
		int rev = -1;
		perforce::GetRevision(cOutputClip, rev, m_bInteractiveMode);
		revisionValue = rev != -1 ? rev + 1 : 0;

		crProperty pRevisionProperty;
		pRevisionProperty.SetName( PERFORCE_REVISION_PROPERTY_NAME );
		pRevisionProperty.AddAttribute( pPropRevision );
		pProperties->AddProperty( pRevisionProperty );

		crPropertyAttributeInt pPropLinearCompression;
		pPropLinearCompression.SetName( LINEAR_COMPRESSION_PROPERTY_NAME );
		int& linearCompressionValue = pPropLinearCompression.GetInt();
		linearCompressionValue = pSegData->m_bLinearCompression;

		crProperty pLinearCompressionProperty;
		pLinearCompressionProperty.SetName( LINEAR_COMPRESSION_PROPERTY_NAME );
		pLinearCompressionProperty.AddAttribute( pPropLinearCompression );
		pProperties->AddProperty( pLinearCompressionProperty );

		crPropertyAttributeString pPropControlFile;
		pPropControlFile.SetName( CONTROL_FILE_PROPERTY_NAME );
		atString& controlFileValue = pPropControlFile.GetString();
		controlFileValue = ctrlFilePath;

		crProperty pControlFileProperty;
		pControlFileProperty.SetName( CONTROL_FILE_PROPERTY_NAME );
		pControlFileProperty.AddAttribute( pPropControlFile );
		pProperties->AddProperty( pControlFileProperty );

		crPropertyAttributeInt pPropUsedInNMBlend;
		pPropUsedInNMBlend.SetName( USED_IN_NMBLEND_PROPERTY_NAME );
		int& usedInNMBlendValue = pPropUsedInNMBlend.GetInt();
		usedInNMBlendValue = pAnimData->m_bUsedInNMBlend;

		crProperty pUsedInNMBlendProperty;
		pUsedInNMBlendProperty.SetName( USED_IN_NMBLEND_PROPERTY_NAME );
		pUsedInNMBlendProperty.AddAttribute( pPropUsedInNMBlend );
		pProperties->AddProperty( pUsedInNMBlendProperty );

		crPropertyAttributeString pUserName;
		pUserName.SetName( SOURCE_USER_NAME );
		atString& usernameValue = pUserName.GetString();
		usernameValue = rexMBAnimExportCommon::GetLocalUserName();

		crProperty pUserNameProperty;
		pUserNameProperty.SetName( SOURCE_USER_NAME );
		pUserNameProperty.AddAttribute( pUserName );
		pProperties->AddProperty( pUserNameProperty );

		crPropertyAttributeInt pHashUserName;
		pHashUserName.SetName( SOURCE_HASH_USER_NAME );
		int& hashUsernameValue = pHashUserName.GetInt();
		hashUsernameValue = atHashString(rexMBAnimExportCommon::GetLocalUserName().c_str()).GetHash();

		crProperty pHashUserNameProperty;
		pHashUserNameProperty.SetName( SOURCE_HASH_USER_NAME );
		pHashUserNameProperty.AddAttribute( pHashUserName );
		pProperties->AddProperty( pHashUserNameProperty );


		crPropertyAttributeString pAbsoluteFBXFile;
		pAbsoluteFBXFile.SetName( ABSOLUTE_FBXFILE_PROPERTY_NAME );
		atString& absoluteFbxFileValue = pAbsoluteFBXFile.GetString();
		char cAbsoluteFileName[RAGE_MAX_PATH];
		FBApplication application;
		sprintf(cAbsoluteFileName, "%s", application.FBXFileName.AsString());
		absoluteFbxFileValue = cAbsoluteFileName;
		
		crProperty pAbsoluteFBXFileProperty;
		pAbsoluteFBXFileProperty.SetName( ABSOLUTE_FBXFILE_PROPERTY_NAME );
		pAbsoluteFBXFileProperty.AddAttribute( pAbsoluteFBXFile );
		pProperties->AddProperty( pAbsoluteFBXFileProperty );

		crPropertyAttributeString pFBXFile;
		pFBXFile.SetName( FBXFILE_PROPERTY_NAME );
		atString& fbxFileValue = pFBXFile.GetString();
		char cFileName[RAGE_MAX_PATH];
		//FBApplication application;
		sprintf(cFileName, "%s", application.FBXFileName.AsString());

		// Replace the art path with $(art)
		atString artPath = rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString(""));
		artPath.Lowercase();
		artPath.Replace("/","\\");

		fbxFileValue = cFileName;
		fbxFileValue.Lowercase();
		fbxFileValue.Replace("/","\\");

		fbxFileValue.Replace(artPath.c_str(), "$(art)");

		crProperty pFBXFileProperty;
		pFBXFileProperty.SetName( FBXFILE_PROPERTY_NAME );
		pFBXFileProperty.AddAttribute( pFBXFile );
		pProperties->AddProperty( pFBXFileProperty );

		crPropertyAttributeString pVersionAttribute;
		pVersionAttribute.SetName( EXPORTER_VERSION_PROPERTY_NAME );
		atString& versionValue = pVersionAttribute.GetString();
		char cVersion[RAGE_MAX_PATH];
		sprintf(cVersion, "%d.%d.%d", s_ingame_exporter_version.Major, s_ingame_exporter_version.Minor, s_ingame_exporter_version.Revision);
		versionValue = cVersion;

		crProperty pVersionProperty;
		pVersionProperty.SetName( EXPORTER_VERSION_PROPERTY_NAME );
		pVersionProperty.AddAttribute( pVersionAttribute );
		pProperties->AddProperty( pVersionProperty );

		if( stricmp(pCharData->GetModelType(), "camera") == 0 )
		{
			crPropertyAttributeBool pCameraApproved;
			pCameraApproved.SetName( CAMERA_APPROVED_PROPERTY_NAME );
			bool& cameraApprovedValue = pCameraApproved.GetBool();
			cameraApprovedValue = false;

			crProperty pCameraApprovedProperty;
			pCameraApprovedProperty.SetName( CAMERA_APPROVED_PROPERTY_NAME );
			pCameraApprovedProperty.AddAttribute( pCameraApproved );

			if( pProperties->FindProperty(pCameraApprovedProperty.GetKey()) == NULL )
				pProperties->AddProperty( pCameraApprovedProperty );
		}

		// We check if the scene changed since last export to flag it dirty. Check the discarded frame string and the start/end range. If any of these
		// differ then mark the scene dirty. This will need to be marked as non-dirty externally.
		//if(pAnimData->m_bStoryMode)
		//{
			atString strDiscardedFrames("");
			rexMBAnimExportCommon::GetDiscardFrameArrayAsString(strDiscardedFrames);

			if(pProperties->FindProperty(DISCARDED_FRAMES_PROPERTY_NAME) && pProperties->FindProperty(RANGE_START_PROPERTY_NAME) && pProperties->FindProperty(RANGE_END_PROPERTY_NAME))
			{
				crPropertyAttributeString* pPropDiscarded = dynamic_cast<crPropertyAttributeString*>(pProperties->FindProperty(DISCARDED_FRAMES_PROPERTY_NAME)->GetAttribute(DISCARDED_FRAMES_PROPERTY_NAME));
				crPropertyAttributeInt* pPropStartRange = dynamic_cast<crPropertyAttributeInt*>(pProperties->FindProperty(RANGE_START_PROPERTY_NAME)->GetAttribute(RANGE_START_PROPERTY_NAME));
				crPropertyAttributeInt* pPropEndRange = dynamic_cast<crPropertyAttributeInt*>(pProperties->FindProperty(RANGE_END_PROPERTY_NAME)->GetAttribute(RANGE_END_PROPERTY_NAME));
				if(pPropDiscarded && pPropStartRange && pPropEndRange)
				{
					if(stricmp(strDiscardedFrames.c_str(), pPropDiscarded->GetString().c_str()) != 0 ||
						rexMBAnimExportCommon::GetFrameFromTime(fStartTime) != pPropStartRange->GetInt() ||
						rexMBAnimExportCommon::GetFrameFromTime(fEndTime) != pPropEndRange->GetInt())
					{
						crPropertyAttributeBool pPropDirty;
						pPropDirty.SetName( SCENE_DIRTY_PROPERTY_NAME );
						bool& dirtySceneValue = pPropDirty.GetBool();
						dirtySceneValue = true;

						crProperty pDirtyProperty;
						pDirtyProperty.SetName( SCENE_DIRTY_PROPERTY_NAME );
						pDirtyProperty.AddAttribute( pPropDirty );
						pProperties->AddProperty( pDirtyProperty );			
					}
				}
			}

			crPropertyAttributeString pPropDiscardedFrames;
			pPropDiscardedFrames.SetName( DISCARDED_FRAMES_PROPERTY_NAME );
			atString& discardedFramesValue = pPropDiscardedFrames.GetString();
			discardedFramesValue = strDiscardedFrames;

			crProperty pDiscardedFramesProperty;
			pDiscardedFramesProperty.SetName( DISCARDED_FRAMES_PROPERTY_NAME );
			pDiscardedFramesProperty.AddAttribute( pPropDiscardedFrames );
			pProperties->AddProperty( pDiscardedFramesProperty );
		//}

		crPropertyAttributeInt pPropStartRange;
		pPropStartRange.SetName( RANGE_START_PROPERTY_NAME );
		int& startRangeValue = pPropStartRange.GetInt();
		startRangeValue = rexMBAnimExportCommon::GetFrameFromTime(fStartTime);

		crProperty pStartRangeProperty;
		pStartRangeProperty.SetName( RANGE_START_PROPERTY_NAME );
		pStartRangeProperty.AddAttribute( pPropStartRange );
		pProperties->AddProperty( pStartRangeProperty );

		crPropertyAttributeInt pPropEndRange;
		pPropEndRange.SetName( RANGE_END_PROPERTY_NAME );
		int& endRangeValue = pPropEndRange.GetInt();
		endRangeValue = rexMBAnimExportCommon::GetFrameFromTime(fEndTime);

		crProperty pEndRangeProperty;
		pEndRangeProperty.SetName( RANGE_END_PROPERTY_NAME );
		pEndRangeProperty.AddAttribute( pPropEndRange );
		pProperties->AddProperty( pEndRangeProperty );

		// If we're using an initial offset then extract the first frame of animation from the mover
		if( pAnimData->m_bUseInitOffset )
		{
			FBSystem system;
			FBPlayerControl controller;       

			FBTime preSampleTime = system.LocalTime;

			FBTime fbt;
			fbt.SetSecondDouble(fStartTime);
			controller.Goto(fbt);

			system.Scene->Evaluate();

			// If the new origin object exists in the scene then we use this
			HFBModel originModel = RAGEFindModelByName((char*)pAnimData->m_originObjectName.c_str());
			if(originModel != NULL)
			{
				for ( vector<RexMbAnimExportDataBase *>::const_iterator it = pExportDataList.begin(); it != pExportDataList.end(); ++it )
				{
					atString sBoneName = atString((const char*)(*it)->GetModel()->LongName);

					atString sBoneNameOriginal = sBoneName;
					int colonPos = sBoneName.LastIndexOf(":");
					sBoneName.Set(sBoneName, colonPos + 1, sBoneName.GetLength() - colonPos);

					if(sBoneName == "mover")
					{
						HFBModel moverModel = RAGEFindModelByName((char*)sBoneNameOriginal.c_str());
						FBMatrix moverTransformationMatrixFB;
						moverTransformationMatrixFB.Identity();
						moverModel->GetMatrix(moverTransformationMatrixFB, kModelTransformation, true);

						Matrix34 moverTransformationMatrixRage;
						moverTransformationMatrixRage.Identity();
						FBMatrixToRageMatrix34(moverTransformationMatrixFB, moverTransformationMatrixRage);
						moverTransformationMatrixRage.Normalize();

						// Get the matrix for the origin object
						FBMatrix originTransformationMatrixFB;
						originTransformationMatrixFB.Identity();
						originModel->GetMatrix(originTransformationMatrixFB, kModelTransformation, true);

						Matrix34 originTransformationMatrixRage;
						originTransformationMatrixRage.Identity();
						FBMatrixToRageMatrix34(originTransformationMatrixFB, originTransformationMatrixRage);
						originTransformationMatrixRage.Normalize();

						//////////////////////////////////////////////////////////////////////////

						std::string sMoverDummyName = sBoneNameOriginal.c_str();
						colonPos = (int)sMoverDummyName.find_last_of(":");
						sMoverDummyName = sMoverDummyName.substr(0,colonPos);
						sMoverDummyName = sMoverDummyName.append(":Dummy01");

						Matrix34 moverDummyTransformationMatrixRage;
						moverDummyTransformationMatrixRage.Identity();

						HFBModel moverDummyModel = RAGEFindModelByName((char*)sMoverDummyName.c_str());
						if(moverDummyModel)
						{
							FBMatrix moverDummyTransformationMatrixFB;
							moverDummyModel->GetMatrix(moverDummyTransformationMatrixFB, kModelTransformation, true);


							FBMatrixToRageMatrix34(moverDummyTransformationMatrixFB, moverDummyTransformationMatrixRage);
							moverDummyTransformationMatrixRage.Normalize();
						}
						else
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to find Dummy01 object, expected %s", sMoverDummyName.c_str());
							return false;
						}

						std::string sOriginDummyName = (char*)pAnimData->m_originObjectName.c_str();
						colonPos = (int)sOriginDummyName.find_last_of(":");
						sOriginDummyName = sOriginDummyName.substr(0,colonPos);
						sOriginDummyName = sOriginDummyName.append(":Dummy01");

						Matrix34 originDummyTransformationMatrixRage;
						originDummyTransformationMatrixRage.Identity();

						HFBModel originDummyModel = RAGEFindModelByName((char*)sOriginDummyName.c_str());
						if(originDummyModel)
						{
							FBMatrix originDummyTransformationMatrixFB;
							originDummyModel->GetMatrix(originDummyTransformationMatrixFB, kModelTransformation, true);


							FBMatrixToRageMatrix34(originDummyTransformationMatrixFB, originDummyTransformationMatrixRage);
							originDummyTransformationMatrixRage.Normalize();
						}

						originTransformationMatrixRage.DotTranspose(originDummyTransformationMatrixRage);
						moverTransformationMatrixRage.DotTranspose(moverDummyTransformationMatrixRage);


						moverTransformationMatrixRage.DotTranspose(originTransformationMatrixRage);

						// Build our final translation vector and rotation quaternion
						Vec3V clipVec(moverTransformationMatrixRage.d[0],moverTransformationMatrixRage.d[1],moverTransformationMatrixRage.d[2]);
						clipVec /= ScalarVFromF32(100.f);

						Quaternion moverQuaternion;
						moverTransformationMatrixRage.ToQuaternion(moverQuaternion);
						moverQuaternion.Normalize();

						crPropertyAttributeQuaternion pPropInitOffsetRot;
						pPropInitOffsetRot.SetName( INIT_OFFSET_ROT_PROPERTY_NAME );
						QuatV_Ref offsetValue = pPropInitOffsetRot.GetQuaternion();
						offsetValue = QUATERNION_TO_QUATV(moverQuaternion);

						crProperty pInitOffsetRotationProperty;
						pInitOffsetRotationProperty.SetName(INIT_OFFSET_ROT_PROPERTY_NAME);	
						pInitOffsetRotationProperty.AddAttribute(pPropInitOffsetRot);
						pProperties->AddProperty( pInitOffsetRotationProperty );

						crPropertyAttributeVector3 pPropInitOffsetPos;
						pPropInitOffsetPos.SetName( INIT_OFFSET_POS_PROPERTY_NAME );
						Vec3V_Ref offsetPositionValue = pPropInitOffsetPos.GetVector3();
						offsetPositionValue = clipVec;

						crProperty pInitOffsetPositionProperty;
						pInitOffsetPositionProperty.SetName(INIT_OFFSET_POS_PROPERTY_NAME);	
						pInitOffsetPositionProperty.AddAttribute(pPropInitOffsetPos);
						pProperties->AddProperty( pInitOffsetPositionProperty );
					}
				}
			}
			else
			{
				for ( vector<RexMbAnimExportDataBase *>::const_iterator it = pExportDataList.begin(); it != pExportDataList.end(); ++it )
				{
					atString sBoneName = atString((const char*)(*it)->GetModel()->LongName);
					atString sBoneNameOriginal = sBoneName;

					int colonPos = sBoneName.LastIndexOf(":");
					sBoneName.Set(sBoneName, colonPos + 1, sBoneName.GetLength() - colonPos);

					if(sBoneName == "mover")
					{
						HFBModel moverModel = RAGEFindModelByName((char*)sBoneNameOriginal.c_str());
						FBMatrix moverTransformationMatrixFB;
						moverModel->GetMatrix(moverTransformationMatrixFB, kModelTransformation, true);

						Matrix34 moverTransformationMatrixRage;
						moverTransformationMatrixRage.Identity();
						FBMatrixToRageMatrix34(moverTransformationMatrixFB, moverTransformationMatrixRage);
						moverTransformationMatrixRage.Normalize();

						//////////////////////////////////////////////////////////////////////////

						std::string sDummyName = sBoneNameOriginal.c_str();
						colonPos = (int)sDummyName.find_last_of(":");
						sDummyName = sDummyName.substr(0,colonPos);
						sDummyName = sDummyName.append(":Dummy01");

						HFBModel dummyModel = RAGEFindModelByName((char*)sDummyName.c_str());
						if(dummyModel)
						{
							FBMatrix dummyTransformationMatrixFB;
							dummyModel->GetMatrix(dummyTransformationMatrixFB, kModelTransformation, true);

							Matrix34 dummyTransformationMatrixRage;
							dummyTransformationMatrixRage.Identity();
							FBMatrixToRageMatrix34(dummyTransformationMatrixFB, dummyTransformationMatrixRage);
							dummyTransformationMatrixRage.Normalize();

							moverTransformationMatrixRage.DotTranspose(dummyTransformationMatrixRage);
						}

						//////////////////////////////////////////////////////////////////////////

						Quaternion moverQuaternion;
						moverTransformationMatrixRage.ToQuaternion(moverQuaternion);

						moverQuaternion.Normalize();
						QuatV clipQuat(RC_QUATV(moverQuaternion));

						Vec3V clipVec(moverTransformationMatrixRage.d[0],moverTransformationMatrixRage.d[1],moverTransformationMatrixRage.d[2]);
						clipVec /= ScalarVFromF32(100.f);

						//////////////////////////////////////////////////////////////////////////

						crPropertyAttributeQuaternion pPropInitOffsetRot;
						pPropInitOffsetRot.SetName( INIT_OFFSET_ROT_PROPERTY_NAME );
						QuatV_Ref offsetValue = pPropInitOffsetRot.GetQuaternion();
						offsetValue = QUATERNION_TO_QUATV(moverQuaternion);

						crProperty pInitOffsetRotationProperty;
						pInitOffsetRotationProperty.SetName(INIT_OFFSET_ROT_PROPERTY_NAME);
						pInitOffsetRotationProperty.AddAttribute(pPropInitOffsetRot);
						pProperties->AddProperty( pInitOffsetRotationProperty );

						crPropertyAttributeVector3 pPropInitOffsetPos;
						pPropInitOffsetPos.SetName( INIT_OFFSET_POS_PROPERTY_NAME );
						Vec3V_Ref offsetPositionValue = pPropInitOffsetPos.GetVector3();
						offsetPositionValue = clipVec;

						crProperty pInitOffsetPositionProperty;
						pInitOffsetPositionProperty.SetName(INIT_OFFSET_POS_PROPERTY_NAME);			
						pInitOffsetPositionProperty.AddAttribute(pPropInitOffsetPos);
						pProperties->AddProperty( pInitOffsetPositionProperty );
						break;
					}
				}
			}

			controller.Goto(preSampleTime);
		}

		for( int i = 0; i < pClipAnim->GetTags()->GetNumTags(); ++i)
		{
			crTag* tag = const_cast<crTag*>(pClipAnim->GetTags()->GetTag(i));

			if(tag->GetDuration() < 0 || tag->GetDuration() > 1)
			{
				ULOGGER_MOBO.SetProgressMessage("WARNING: Tag %s has an invalid phase of %f", tag->GetName(), tag->GetDuration());
			}

			if(tag->GetStart() > tag->GetEnd())
			{
				ULOGGER_MOBO.SetProgressMessage("WARNING: Tag %s has a start phase %f greater than its end phase %f", tag->GetName(), tag->GetStart(), tag->GetEnd());
			}
		}

		pClipAnim->Save( cOutputClip );
	}

	return true;
}

void RexRageAnimExport::AddCameraCutTagBlocks( float fStartTime, float fEndTime, ClipXML* clip )
{
	atArray<float> cameraCutTimeList;
	rexMBAnimExportCommon::GetCameraCutTimes( cameraCutTimeList );

	for(int i=0; i < clip->GetTags().GetCount(); ++i)
	{
		TagXML* blockTag = clip->GetTags()[i];
		if(stricmp(blockTag->GetProperty().GetName(),"Block") == 0)
		{
			clip->GetTags().Delete(i);
			--i;
		}
	}

	for ( int j = 0; j < cameraCutTimeList.GetCount(); ++j )
	{		if ( (cameraCutTimeList[j] < fStartTime) || (cameraCutTimeList[j] == 0) )
	{
		continue;
	}

	if ( cameraCutTimeList[j] > fEndTime )
	{
		break;
	}

#if USE_NEW_BLOCK_TAGS
	// add a block tag for each camera cut
	//float thirtiethSecond = 1.0f / rexMBAnimExportCommon::GetFPS();

	// The time from the cut is on the end of the block so we pull it back half a frame to make the time be the mid. Then we calc by -/+
	// half a frame either side for the start/end.
	float halfFrame = (0.5f * (1.0f / CUTSCENE_FPS));
	// MPW - Move the cut back half a frame. The incoming cut times need to be brought back in future and this can be removed. The cut times are
	// currently pulled back in the build process to avoid disruption.
	float fTime = cameraCutTimeList[j] - halfFrame;
	float time = (fTime - fStartTime) - halfFrame;
	float startTime = clip->ConvertTimeToPhase( ( time > 0 ) ? (time - BLOCK_TAG_PAD) : 0 );
	float endTime = clip->ConvertTimeToPhase( (fTime - fStartTime) + halfFrame + BLOCK_TAG_PAD ) ;

	TagXML* pBlockTag = rage_new TagXML();
	pBlockTag->GetProperty().SetName( "Block" );
	pBlockTag->SetStart( startTime );
	pBlockTag->SetEnd( endTime );
	clip->GetTags().PushAndGrow(pBlockTag);
#else
	// add a block tag for each camera cut
	float thirtiethSecond = 1.0f / rexMBAnimExportCommon::GetFPS();
	float startTime = pClipAnim->ConvertTimeToPhase( cameraCutTimeList[j] - thirtiethSecond );
	float endTime = pClipAnim->ConvertTimeToPhase( cameraCutTimeList[j] + thirtiethSecond );

	crTag pBlockTag;
	pBlockTag.GetProperty().SetName( "Block" );
	pBlockTag.SetStart( startTime );
	pBlockTag.SetEnd( endTime );
	pClipAnim->GetTags()->AddUniqueTag( pBlockTag );
#endif
	}

}

void RexRageAnimExport::AddCameraCutTagBlocks( float fStartTime, float fEndTime, crClipAnimation *pClipAnim )
{
	atArray<float> cameraCutTimeList;
	rexMBAnimExportCommon::GetCameraCutTimes( cameraCutTimeList );

	ULOGGER_MOBO.SetProgressMessage("Wiping '%d' clip blocking tags.", pClipAnim->GetTags()->GetNumTags("Block"));

	// Wipe any block tags in the clip before we add any new ones, since the clip is appended it will have old cut blocking tags in.
	for( int i = 0; i < pClipAnim->GetTags()->GetNumTags(); ++i)
	{
		crTag* blockTag = const_cast<crTag*>(pClipAnim->GetTags()->GetTag(i));
		if(stricmp(blockTag->GetName(),"Block") == 0)
		{
			pClipAnim->GetTags()->RemoveTag(*blockTag);
			--i;
		}
	}

	ULOGGER_MOBO.SetProgressMessage("Clip now contains '%d' blocking tags after wipe.", pClipAnim->GetTags()->GetNumTags("Block"));

	for ( int j = 0; j < cameraCutTimeList.GetCount(); ++j )
	{
		if ( (cameraCutTimeList[j] < fStartTime) || (cameraCutTimeList[j] == 0) )
		{
			continue;
		}

		if ( cameraCutTimeList[j] > fEndTime )
		{
			break;
		}

#if USE_NEW_BLOCK_TAGS
		// add a block tag for each camera cut
		//float thirtiethSecond = 1.0f / rexMBAnimExportCommon::GetFPS();
		
		// The time from the cut is on the end of the block so we pull it back half a frame to make the time be the mid. Then we calc by -/+
		// half a frame either side for the start/end.
		float halfFrame = (0.5f * (1.0f / CUTSCENE_FPS));
		// MPW - Move the cut back half a frame. The incoming cut times need to be brought back in future and this can be removed. The cut times are
		// currently pulled back in the build process to avoid disruption.
		float fTime = cameraCutTimeList[j] - halfFrame;
		float time = (fTime - fStartTime) - halfFrame;
		float startTime = pClipAnim->ConvertTimeToPhase( ( time > 0 ) ? (time - BLOCK_TAG_PAD) : 0 );
		float endTime = pClipAnim->ConvertTimeToPhase( (fTime - fStartTime) + halfFrame + BLOCK_TAG_PAD ) ;

		crTag pBlockTag;
		pBlockTag.GetProperty().SetName( "Block" );
		pBlockTag.SetStart( startTime );
		pBlockTag.SetEnd( endTime );
		pClipAnim->GetTags()->AddUniqueTag( pBlockTag );
#else
		// add a block tag for each camera cut
		float thirtiethSecond = 1.0f / rexMBAnimExportCommon::GetFPS();
		float startTime = pClipAnim->ConvertTimeToPhase( cameraCutTimeList[j] - thirtiethSecond );
		float endTime = pClipAnim->ConvertTimeToPhase( cameraCutTimeList[j] + thirtiethSecond );

		crTag pBlockTag;
		pBlockTag.GetProperty().SetName( "Block" );
		pBlockTag.SetStart( startTime );
		pBlockTag.SetEnd( endTime );
		pClipAnim->GetTags()->AddUniqueTag( pBlockTag );
#endif
	}
}

//-----------------------------------------------------------------------------
bool RexRageAnimExport::SaveClipDataAllTakes( const RexAnimMarkupData* pAnimData )
{
	bool bResult = true;
	std::string erroneousTakes;

	if( pAnimData->m_segmentFromTakes )
	{
		bool hasCamera = rexMBAnimExportCommon::HasCamera( pAnimData );
			
		//Store the take that is currently active in the scene
		HFBTake hActiveTake = m_system.CurrentTake;

		//Sort the markup data so that each markup object contains
		//segments for only one take.
		vector< RexAnimMarkupData* > splitByTakeAnimDataArray;
		SplitAnimDataByTakes(pAnimData, splitByTakeAnimDataArray);

		atArray<HFBTake> takeArray;
		atArray<vector<RexMbAnimExportModel *>> splitByTakeExportModelArray;

		for(vector< RexAnimMarkupData* >::iterator it = splitByTakeAnimDataArray.begin();
			it != splitByTakeAnimDataArray.end();
			++it)
			

			if ( m_system.Scene->Takes.Find(hActiveTake) >= 0)
			{
				int index = takeArray.Find( hActiveTake );
				if ( index == -1 )
				{
					index = takeArray.GetCount();
					takeArray.Grow() = hActiveTake;

					splitByTakeExportModelArray.Grow();
				}

				splitByTakeExportModelArray[index].push_back( rage_new RexMbAnimExportModel( *it ) );
			}
			else
			{
				const char* szTakeName = (*it)->GetSegment( 0 )->GetTakeName();

				char msgBuf[512];
				sprintf_s(msgBuf, 512, "ERROR: The take '%s' was specified for export, but is not present in the loaded FBX file, take skipped", szTakeName);
				SetProgressMessage( msgBuf );

				bResult = false;
				break;
			}
		m_ErrorMessage = "";

		//Queue up the Perforce commands prior to exporting.
		for ( int i = 0; i < (int)splitByTakeExportModelArray.size(); ++i )
		{
			int nTakes = (int)splitByTakeAnimDataArray.size();
			for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
			{
				RexAnimMarkupData* pTakeAnimData = splitByTakeAnimDataArray[takeIdx];
				Assert(pTakeAnimData);

				//All the segments will be from the same take, so just get the take name off
				//of the first segment
				RexMbAnimSegmentData* pSegData = pTakeAnimData->GetSegment(0);
				Assert(pSegData);

				int pathCount = pSegData->GetNumAltOutputPaths();
				// We want it to run at least once.  GetClipFilePath checks if there are actually no alt 
				// output paths and deals with it accordingly.
				if ( pathCount == 0 ) { pathCount = 1; }
				for ( int idxPath = 0; idxPath < pathCount; idxPath++)
				{
					char* pClipPath = GetClipFilePath(pTakeAnimData, pTakeAnimData->GetCharacter(0), pSegData, idxPath);
					perforce::SyncAndCheckout(pClipPath, m_bInteractiveMode);
				}
			}
		}

		perforce::ExecuteQueuedOperations(m_bInteractiveMode);

		for ( int i = 0; i < (int)splitByTakeExportModelArray.size(); ++i )
		{
			int nTakes = (int)splitByTakeAnimDataArray.size();
			for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
			{
				RexAnimMarkupData* pTakeAnimData = splitByTakeAnimDataArray[takeIdx];
				Assert(pTakeAnimData);

				//All the segments will be from the same take, so just get the take name off
				//of the first segment
				RexMbAnimSegmentData* pSegData = pTakeAnimData->GetSegment(0);
				Assert(pSegData);
				
				const vector<RexMbAnimExportDataBase*> pExportDataList = splitByTakeExportModelArray[i][takeIdx]->GetExportDataList();

				bool clipDataResult = true;
				int pathCount = pSegData->GetNumAltOutputPaths();
				// We want it to run at least once.  GetClipFilePath checks if there are actually no alt 
				// output paths and deals with it accordingly.
				if ( pathCount == 0 ) { pathCount = 1; }
				for ( int idxPath = 0; idxPath < pathCount; idxPath++)
				{
					clipDataResult = clipDataResult && SaveClipData( pTakeAnimData, pTakeAnimData->GetCharacter(0), pSegData, pExportDataList, rexMBAnimExportCommon::GetSceneStartTime(),rexMBAnimExportCommon::GetSceneEndTime(), idxPath, hasCamera );
				}

				if(clipDataResult == false)
				{
					erroneousTakes.append(pSegData->m_take->Name.AsString());
					erroneousTakes.append("\n");
					bResult = false;
				}	
			}
		}

		if(m_ErrorMessage.length() > 0)
		{
			if(m_bInteractiveMode)
				FBMessageBox("Animation Export Error", const_cast<char*>(m_ErrorMessage.c_str()), "OK");
			else
				Errorf(m_ErrorMessage.c_str());
		}
	}

	if(bResult == false)
	{
		if(m_bInteractiveMode)
		{
			std::string takeMessageBuffer("Failed to save clip data for the following takes.  An .anim file was not found and one must be exported before proceeding.  Takes:\n\n");
			takeMessageBuffer.append(erroneousTakes);
			FBMessageBox("REX Animation Export Error", const_cast<char*>(takeMessageBuffer.c_str()), "OK");
		}
		else
		{
			Errorf("Failed to save clip data for the following takes.  An .anim file was not found and one must be exported before proceeding.  Takes:", erroneousTakes.c_str());
		}
	}

	return bResult;
}

//-----------------------------------------------------------------------------
void RexRageAnimExport::ClearModelProperties(const HFBModel hModel)
{
	for(int index = 0; index < hModel->PropertyList.GetCount(); index++ )
	{
		FBProperty* pCustomProperty = hModel->PropertyList[index];
		if(pCustomProperty->IsUserProperty() == true)
		{
			hModel->PropertyRemove(pCustomProperty);
			--index;
		}
	}
}

//-----------------------------------------------------------------------------
void RexRageAnimExport::ClearTakeProperties(const HFBTake hTake)
{
	// Remove all existing properties on the takes.
	for(int index = 0; index < hTake->PropertyList.GetCount(); index++)
	{
		FBProperty* pCustomProperty = hTake->PropertyList[index];
		if(pCustomProperty->IsUserProperty() == true)
		{
			hTake->PropertyRemove(pCustomProperty);
			--index;
		}
	}
}

bool RexRageAnimExport::IsPowerOf2(int iNum)
{
	return ((iNum>0) && (iNum & (iNum-1))==0);
}

//-----------------------------------------------------------------------------
bool RexRageAnimExport::LoadClipData( /*const*/ RexAnimMarkupData* pAnimData,  const RexCharAnimData* pCharData, RexMbAnimSegmentData* pSegData )
{
	HFBTake hTake = pSegData->GetTake();
	m_system.CurrentTake = hTake;

	const char* rootModelName = pCharData->GetInputModelName(0);
	HFBModel hRootModel = RAGEFindModelByName( const_cast<char*>( rootModelName ) );

	//Remove all properties and user objects on the takes.
	ClearTakeProperties(hTake);

	//Passes along index 0 for the clip alt output path number, if nothing is saved then it
	//will get it from the default output path
	char* cInputClip = GetClipFilePath(pAnimData, pCharData, pSegData, 0);
#if HACK_MP3
	ASSET.SetUseStackedPath(true);
#endif
	crClip* pClipAnim = crClip::AllocateAndLoad(cInputClip);
	if( !pClipAnim )
	{
#if HACK_MP3
		ASSET.SetUseStackedPath(false);
#endif
		return false;
	}
#if HACK_MP3
	ASSET.SetUseStackedPath(false);
#endif

	if(pClipAnim)
	{
		// Now for the properties
		// All properties will only need to be loaded the first time an FBX is loaded
		// since they will be stored in the FBX from then on in		
		crProperties* pProperties = pClipAnim->GetProperties();
		if( pProperties )
		{
			crProperty* pPropMover = pProperties->FindProperty( MOVER_PROPERTY_NAME );

			if( pPropMover )
			{
				crPropertyAttributeString* pMoverAttribute = static_cast<crPropertyAttributeString*>(pPropMover->GetAttribute(MOVER_PROPERTY_NAME));
				if ( pMoverAttribute )
				{
					const char* pPropValue = pMoverAttribute->GetString();
					CreateMoverFromString( pPropValue, hRootModel );
					pProperties->RemoveProperty(MOVER_PROPERTY_NAME);
				}
			}

			crProperty* pPropAdditive = pProperties->FindProperty( ADDITIVE_PROPERTY_NAME );
			if( pPropAdditive )
			{
				crPropertyAttributeInt* pAdditiveAttribute = static_cast<crPropertyAttributeInt*>(pPropAdditive->GetAttribute(ADDITIVE_PROPERTY_NAME));
				if ( pAdditiveAttribute ) 
				{
					pSegData->m_bAdditive = pAdditiveAttribute->GetInt() == 1 ? true : false;
					pProperties->RemoveProperty(ADDITIVE_PROPERTY_NAME);
				}
			}

			crProperty* pPropAdditiveBA = pProperties->FindProperty( ADDITIVE_BASE_ANIM_PROPERTY_NAME );
			if( pPropAdditiveBA )
			{
				crPropertyAttributeInt* pAdditiveBAAttribute = static_cast<crPropertyAttributeInt*>(pPropAdditiveBA->GetAttribute(ADDITIVE_BASE_ANIM_PROPERTY_NAME));
				if ( pAdditiveBAAttribute ) 
				{
					pSegData->m_bAdditiveBaseAnim = pAdditiveBAAttribute->GetInt() == 1 ? true : false;
					pProperties->RemoveProperty(ADDITIVE_BASE_ANIM_PROPERTY_NAME);
				}
			}

			crProperty* pPropLinearCompression = pProperties->FindProperty( LINEAR_COMPRESSION_PROPERTY_NAME );
			if( pPropLinearCompression )
			{
				crPropertyAttributeInt* pLinearCompressionAttribute = static_cast<crPropertyAttributeInt*>(pPropLinearCompression->GetAttribute(LINEAR_COMPRESSION_PROPERTY_NAME));
				if ( pLinearCompressionAttribute ) 
				{
					pSegData->m_bLinearCompression = pLinearCompressionAttribute->GetInt() == 1 ? true : false;
					pProperties->RemoveProperty(LINEAR_COMPRESSION_PROPERTY_NAME);
				}
			}

			crProperty* pPropCompressFile = pProperties->FindProperty( COMPRESSION_FILE_PROPERTY_NAME );
			if( pPropCompressFile )
			{
				crPropertyAttributeString* pCompressFileAttribute = static_cast<crPropertyAttributeString*>(pPropCompressFile->GetAttribute(COMPRESSION_FILE_PROPERTY_NAME));
				if ( pCompressFileAttribute )
				{
					pSegData->m_compressionFilePath = ASSET.FileName(pCompressFileAttribute->GetString());
					pProperties->RemoveProperty(COMPRESSION_FILE_PROPERTY_NAME);
				}
			}

			crProperty* pPropCtrlFile = pProperties->FindProperty( CONTROL_FILE_PROPERTY_NAME );
			if( pPropCtrlFile )
			{
				crPropertyAttributeString* pControlFileAttribute = static_cast<crPropertyAttributeString*>(pPropCtrlFile->GetAttribute(CONTROL_FILE_PROPERTY_NAME));
				if ( pControlFileAttribute )
				{
					//pSegData->m_ctrlFilePath = ASSET.FileName(pControlFileAttribute->GetString());
					pProperties->RemoveProperty(CONTROL_FILE_PROPERTY_NAME);
				}
			}

			crProperty* pPropLooping = pProperties->FindProperty( LOOPING_PROPERTY_NAME );
			if( pPropLooping )
			{
				crPropertyAttributeInt* pLoopingAttribute = static_cast<crPropertyAttributeInt*>(pPropLooping->GetAttribute( LOOPING_PROPERTY_NAME ));
				if ( pLoopingAttribute )
				{
					pSegData->m_bLooping = pLoopingAttribute->GetInt() == 1 ? true : false;
					pProperties->RemoveProperty(LOOPING_PROPERTY_NAME);
				}
			}

			crProperty* pPropUsedInNMBlend = pProperties->FindProperty( USED_IN_NMBLEND_PROPERTY_NAME );
			if( pPropUsedInNMBlend )
			{
				crPropertyAttributeInt* pUseInNMBlendAttribute = static_cast<crPropertyAttributeInt*>(pPropUsedInNMBlend->GetAttribute( USED_IN_NMBLEND_PROPERTY_NAME ));
				if ( pUseInNMBlendAttribute  )
				{
					pAnimData->m_bUsedInNMBlend = pUseInNMBlendAttribute->GetInt() == 1 ? true : false;
					pProperties->RemoveProperty(USED_IN_NMBLEND_PROPERTY_NAME);
				}
			}
		}
		

		delete pClipAnim;
	}

	return true;
}

//-----------------------------------------------------------------------------
bool RexRageAnimExport::LoadClipDataAllTakes( RexAnimMarkupData* pAnimData )
{
	if( pAnimData->m_segmentFromTakes )
	{
		//Remove all MetadataTagObjects on takes.  Properties do not persist, but the properties' associated user objects do.
		//This will prevent having loner user objects cluttering the scene, causing confusion.
		//
		int takeCount = m_system.Scene->Takes.GetCount();
		for(int takeIndex = 0; takeIndex < takeCount; ++takeIndex)
		{
			HFBTake takeComponent = (HFBTake) m_system.Scene->Takes.GetAt(takeIndex);
			ClearTakeProperties(takeComponent);
		}	

		int nCharacters = pAnimData->GetNumCharacters();
		int nSegs = pAnimData->GetNumSegments();

		for(int characterIndex = 0; characterIndex < nCharacters; ++characterIndex)
		{
			RexCharAnimData* pCharData = pAnimData->GetCharacter(characterIndex);

			//NOTE:  We support having the same root model specified for multiple 'characters'.
			//At the moment, the below code will erase the data from any previous clip loading on that character.
			//
			const char* rootModelName = pCharData->GetInputModelName(0);
			HFBModel hRootModel = RAGEFindModelByName( const_cast<char*>( rootModelName ) );

			if(!hRootModel->Is(FBCamera::TypeInfo)) // Only ever one camera
				ClearModelProperties(hRootModel);

			HFBTake currentTake = m_system.CurrentTake;

			for(int segIdx = 0; segIdx < nSegs; segIdx++)
			{
				RexMbAnimSegmentData* pFileSegmentData = pAnimData->GetSegment(segIdx);
				// Get the root model
				if( pFileSegmentData->GetNumInputModels() > 0 )
				{
					if( hRootModel )
					{
						if(LoadClipData( pAnimData, pCharData, pFileSegmentData ) == false)
						{
							ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to load .clip file for take %s.", pFileSegmentData->m_takeName.c_str());
						}
					}
					else
					{ 
						ULOGGER_MOBO.SetProgressMessage("ERROR: Missing root model %s from scene.  Unable to load .clip file for take %s.", rootModelName, pFileSegmentData->m_takeName.c_str());
					}
				}		
			}
			m_system.CurrentTake = currentTake;
		}
	}

	return true;
}

//#############################################################################

