// 
// rexMbRage/userObjects.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/creator.h"
#include "userObjects.h"
#include "UserObjects/AnimMarkup.h"
#include "UserObjects/AnimMarkup_parser.h" //This is a hack; without this, you will get linker errors since these objects are parsable.
#include "UserObjects/AnimMarkup.cpp"

#include <Windows.h>

typedef MetadataTagObject* (*FindMetadataTagObjectFunc) (HFBProperty property, HFBComponent component, bool bCreate);
typedef void (*RemoveMetadataTagObjectFunc) (HFBProperty property, HFBComponent component);
typedef void (*RemoveMetadataTagObjectsOnComponentFunc) (HFBComponent component);
typedef void (*RemoveAllMetadataTagObjectFunc) ();

typedef RexRageAnimExportPData* (*FindPersistentDataFunc) (bool bCreate);
typedef void (*SetPersistentDataFunc) (RexMbAnimData* pData);
typedef RexAnimMarkupData* (*GetPersistentDataFunc) ();
typedef void (*DeletePersistentDataFunc) ();
typedef RexAnimMarkupData* (*GetAnimMarkupDataFunc) ();
typedef void (*SetAnimMarkupDataFunc) (RexAnimMarkupData* pData);

typedef RexRageCutsceneExportPData* (*FindCutsceneMarkupDataFunc) (bool bCreate);

typedef bool (*HasLoadedClipsFunc)(void);
typedef void (*SetHasLoadedClipsFunc)(bool set);

typedef bool (*HasReloadedClipsFunc)(void);
typedef void (*SetHasReloadedClipsFunc)(bool set);

//typedef HFBAnimationNode (*GetAnimationNodeFromCacheFunc)(HFBTake hTake, HFBProperty hProperty);
typedef void (*QuickAnimationNodeCacheUpdateFunc)(HFBTake hTake, HFBProperty hProperty);

typedef void (*NotifyPropertyEventFunc)(HFBTake hTake, HFBProperty hProperty, PropertyChangeEnum eAction);
typedef void (*NotifyEventFunc) ();

HMODULE	m_UserObjectsDll = NULL;

// PURPOSE: Initializes the handle to the UserObjects.dll.  Note that instead of calling this 
// at DLL Load-time, as in the loading of this plugin, we have deferred to when a member function is called.
// The DLL does not load successfully when this plug-in is only being initialized.
// RETURN:	bool
bool UserObjects::Initialize()
{
	m_UserObjectsDll = LoadLibrary("UserObjects.dll");
	if(m_UserObjectsDll == NULL)
		return false;

	return true;
}

void UserObjects::Destroy()
{
	if(m_UserObjectsDll != NULL)
	{
		FreeLibrary(m_UserObjectsDll);
	}
}

MetadataTagObject*	UserObjects::FindMetadataTagObject(HFBProperty property, HFBComponent component, bool bCreate)
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return NULL;
	}

	FindMetadataTagObjectFunc getMetadataTagFunc = (FindMetadataTagObjectFunc) GetProcAddress(m_UserObjectsDll, "FindMetadataTagObject");
	return getMetadataTagFunc(property, component, bCreate); 
}

void UserObjects::RemoveMetadataTagObject(HFBProperty property, HFBComponent component)
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	RemoveMetadataTagObjectFunc removeMetadataTagObjectFunc = (RemoveMetadataTagObjectFunc) GetProcAddress(m_UserObjectsDll, "RemoveMetadataTagObject");
	removeMetadataTagObjectFunc(property, component); 
}

void UserObjects::RemoveMetadataTagObjectsOnComponent(HFBComponent component)
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	RemoveMetadataTagObjectsOnComponentFunc removeMetadataTagObjectFunc = (RemoveMetadataTagObjectsOnComponentFunc) GetProcAddress(m_UserObjectsDll, "RemoveMetadataTagObjectsOnComponent");
	removeMetadataTagObjectFunc(component); 
}

void UserObjects::RemoveAllMetadataTagObject()
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	RemoveAllMetadataTagObjectFunc removeAllMetadataTagObjectFunc = (RemoveAllMetadataTagObjectFunc) GetProcAddress(m_UserObjectsDll, "RemoveAllMetadataTagObject");
	removeAllMetadataTagObjectFunc(); 
}

RexRageAnimExportPData* UserObjects::FindPersistentData(bool bCreate)
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return NULL;
	}

	FindPersistentDataFunc findPersistentDataFunc = (FindPersistentDataFunc) GetProcAddress(m_UserObjectsDll, "FindAnimationMarkupData");
	return findPersistentDataFunc(bCreate); 
}

void UserObjects::DeletePersistentData()
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	DeletePersistentDataFunc deletePersistentDataFunc = (DeletePersistentDataFunc) GetProcAddress(m_UserObjectsDll, "DeleteAnimationMarkupData");
	return deletePersistentDataFunc(); 
}

void UserObjects::SetAnimationMarkupData(RexAnimMarkupData* pData)
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	SetAnimMarkupDataFunc setAnimMarkupDataFunc = (SetAnimMarkupDataFunc) GetProcAddress(m_UserObjectsDll, "SetAnimationMarkupData");
	setAnimMarkupDataFunc(pData); 
}

RexAnimMarkupData*	UserObjects::GetAnimationMarkupData()
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return NULL;
	}

	GetAnimMarkupDataFunc getAnimMarkupFunc = (GetAnimMarkupDataFunc) GetProcAddress(m_UserObjectsDll, "GetAnimationMarkupData");
	return getAnimMarkupFunc(); 
}

RexRageCutsceneExportPData* UserObjects::FindCutsceneMarkupData(bool bCreate)
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return NULL;
	}

	FindCutsceneMarkupDataFunc findPersistentDataFunc = (FindCutsceneMarkupDataFunc) GetProcAddress(m_UserObjectsDll, "FindCutsceneMarkupData");
	return findPersistentDataFunc(bCreate); 
}

void UserObjects::DeleteCutsceneMarkupData()
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	DeletePersistentDataFunc deletePersistentDataFunc = (DeletePersistentDataFunc) GetProcAddress(m_UserObjectsDll, "DeleteCutsceneMarkupData");
	return deletePersistentDataFunc(); 
}

bool UserObjects::HasLoadedClips()
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return false;
	}

	HasLoadedClipsFunc hasLoadedClipsFunc = (HasLoadedClipsFunc) GetProcAddress(m_UserObjectsDll, "HasLoadedClips");
	return hasLoadedClipsFunc(); 
}

void UserObjects::SetHasLoadedClips(bool set)
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	SetHasLoadedClipsFunc setHasLoadedClipsFunc = (SetHasLoadedClipsFunc) GetProcAddress(m_UserObjectsDll, "SetHasLoadedClips");
	setHasLoadedClipsFunc(set); 
}

bool UserObjects::HasReloadedClips()
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return false;
	}

	HasReloadedClipsFunc hasReloadedClipsFunc = (HasReloadedClipsFunc) GetProcAddress(m_UserObjectsDll, "HasReloadedClips");
	return hasReloadedClipsFunc(); 
}

void UserObjects::SetHasReloadedClips(bool set)
{
	if(m_UserObjectsDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	SetHasReloadedClipsFunc setHasReloadedClipsFunc = (SetHasLoadedClipsFunc) GetProcAddress(m_UserObjectsDll, "SetHasReloadedClips");
	setHasReloadedClipsFunc(set); 
}