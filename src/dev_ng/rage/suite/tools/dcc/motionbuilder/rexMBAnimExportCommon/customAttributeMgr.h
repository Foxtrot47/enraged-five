#pragma once

#include <stdio.h>
#include <string.h>
#include "atl/string.h"
#include <fstream>
#include <vector>
#include <map>

using namespace rage;

#define ATTRIBUTE_LINE_ENTRIES 2

// Handles animated custom attributes on a facial model which has being imported from max.
class CustomAttributeMgr
{
public:

	// Load a specific attribute file, this file is created by the max expression exporter when exporting expressions
	// Read in each line of the file and fill in a map, mapping the target string of the expression to the full xml driver attribute name
	// eg. con_browRaise_cl.IM_ATTRS_A.Squeeze,con_browRaise_cl_con_browRaise_cl_Squeeze
	void LoadAttributeFile(atString filename);

	// Look for a specific attribute by name, if found then return true and return the corresponding map entry by reference
	bool FindAttribute(atString find, atString& second);

private:

	void SplitString(const char* str, std::vector<atString>& vec);

	std::map<atString, atString>	m_mapAttributes;
};