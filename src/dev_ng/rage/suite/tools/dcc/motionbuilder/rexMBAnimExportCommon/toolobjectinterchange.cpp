// 
// rexMbRage/toolobjectinterchange.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "toolobjectinterchange.h"

#include "UserObjects/OpenRealitySDK.h"
#include "cutsceneExport.h"
#include "keylist.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "vector/matrix34.h"
#include "../rexMBRage/shared.h"

#define RAYFIRE_METRES_TO_CM 100

namespace rage {

//##############################################################################

IToolObjectInterchange::IToolObjectInterchange()
{

}

IToolObjectInterchange::~IToolObjectInterchange()
{

}

void IToolObjectInterchange::FilterEventsForObject( const cutfCutsceneFile2 *pCutfile, const cutfObject *pObject, 
                                                   atArray<cutfEvent *> &objectEventList, s32 iEventId1, s32 iEventId2, 
                                                   atArray<int> &eventIndexList1, atArray<int> &eventIndexList2 )
{
    pCutfile->FindEventsForObjectIdOnly( pObject->GetObjectId(), pCutfile->GetEventList(), objectEventList );

    for ( int i = 0; i < objectEventList.GetCount(); ++i )
    {
        if ( objectEventList[i]->GetEventId() == iEventId1 )
        {
            eventIndexList1.Grow() = i;
        }
        else if ( objectEventList[i]->GetEventId() == iEventId2 )
        {
            eventIndexList2.Grow() = i;
        }
    }
}

void IToolObjectInterchange::RemoveExtraEvents( cutfCutsceneFile2 *pCutfile, atArray<cutfEvent *> &objectEventList, 
                                               int iStartIndex1, int iStartIndex2, 
                                               atArray<s32> &indexList1, atArray<s32> &indexList2 )
{
    // remove extra start times
    RemoveExtraEvents( pCutfile, objectEventList, iStartIndex1, indexList1 );

    // remove extra end times
    RemoveExtraEvents( pCutfile, objectEventList, iStartIndex2, indexList2 );
}

void IToolObjectInterchange::CreateVisibilityEvents( cutfCutsceneFile2* pCutFile, cutfObject *pObject, s32 iObjectId,
													atArray<cutfEvent *> &objectEventList, s32 iVisibleEventId, s32 iInvisibleEventId, 
													atArray<int> &visibleEventIndexList, atArray<int> &invisibleEventIndexList,
													cutfEventArgs *pEventArgsToClone )
{
	int iVisibleEventIndex = 0;
	int iInvisibleEventIndex = 0;

	bool bIsActive = false;
	bool bIsTrigger = false;

	// look for Visibility keys.  if we find any, those determine the set and clear anim events
	HFBModel model = rexMBAnimExportCommon::FindModelForObject( pObject );
	if ( model != NULL )
	{
		bool bAddedEvents = false;

		HFBAnimationNode animNode = model->AnimationNode;
		if ( animNode != NULL )
		{
			// LPXO: Horrible.  The cutscene guys dont want to have to keyframe a value so this is
			// the least offensive modification I can think of.
			// MPW: We only want FX_Trigger to be available on cutfParticleEffectObject and FX_Active to be available on cutfAnimatedParticleEffect
			HFBAnimationNode visNode = rexMBAnimExportCommon::FindAnimByName( animNode, "FX_Active" );
			if( visNode )
			{
				if(dynamic_cast<cutfParticleEffectObject*>(pObject) && !dynamic_cast<cutfDecalObject*>(pObject))
				{
					ULOGGER_MOBO.SetProgressMessage( "WARNING: For an FX_Active animated particle effect you need to use 'Animated Particle Effect'." );
				}
				else
					bIsActive = true;
			}
			else
			{
				visNode = rexMBAnimExportCommon::FindAnimByName( animNode, "FX_Trigger" );
				if( visNode )
				{
					if(dynamic_cast<cutfAnimatedParticleEffectObject*>(pObject))
					{
						ULOGGER_MOBO.SetProgressMessage( "WARNING: For an FX_Trigger particle effect you need to use 'Particle Effect'." );
					}
					else
						bIsTrigger = true;
				}
				else
				{
					visNode = rexMBAnimExportCommon::FindAnimByName( animNode, "Visibility" );
				}

			}

			if ( visNode != NULL )
			{
				float fStartTime = -1.0f;
				float fEndTime = -1.0f;
				bool bFoundStart = false;
				bool bFoundEnd = false;

				int iKeyCount = visNode->FCurve->Keys.GetCount();
				int i = 0;
				while ( i < iKeyCount )
				{
					float fKeyTime = rexMBAnimExportCommon::GetSeconds( visNode->FCurve->Keys[i].Time );
					int iKeyFrame = rexMBAnimExportCommon::GetFrameFromTime(fKeyTime);
					int iNewFrame = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames(iKeyFrame);
					fKeyTime = iNewFrame/rexMBAnimExportCommon::GetFPS();
					//if ( (iNewFrame >= pCutsceneExport->GetStartFrame()) && (iNewFrame <= pCutsceneExport->GetEndFrame()) )
					// GetStartFrame was always 0 in the old exporter, assets have keys at 0 and end.
					if ( (iNewFrame >= 0) && (iNewFrame <= pCutFile->GetRangeEnd()) )
					{
						if( bIsTrigger )
						{
							if ( !bFoundStart )
							{
								fStartTime = fKeyTime;
								bFoundStart = true;
								fEndTime = fKeyTime+(1.0f/rexMBAnimExportCommon::GetFPS());
								bFoundEnd = true;
							}
						}
						else if( bIsActive )
						{
							if ( !bFoundStart )
							{
								fStartTime = fKeyTime;
								bFoundStart = true;
							}
							else if ( !bFoundEnd )
							{
								fEndTime = fKeyTime;
								bFoundEnd = true;

							}
						}
						else
						{
							if ( !bFoundStart )
							{
								if ( visNode->FCurve->Keys[i].Value != 0.0f )
								{
									fStartTime = fKeyTime;
									bFoundStart = true;
								}
							}
							else if ( !bFoundEnd )
							{
								if ( visNode->FCurve->Keys[i].Value == 0.0f )
								{
									fEndTime = fKeyTime;
									bFoundEnd = true;
								}
							}
						}
					}

					++i;

					if ( bFoundStart && bFoundEnd )
					{
						if(!bIsTrigger && !bIsActive)
						{
							fStartTime = pCutFile->GetRangeStart()/30.0f;
							fEndTime = pCutFile->GetTotalDuration();
						}

						if ( fStartTime == fEndTime )
						{
							cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( pObject );

							char cMsg[512];
							sprintf( cMsg, "WARNING: Tried to add both the '%s' and '%s' events on frame %d for '%s'.  Please check your Visibility keyframes.", 
								cutfEvent::GetDisplayName( iVisibleEventId ), cutfEvent::GetDisplayName( iInvisibleEventId ),
								iKeyFrame, pNamedObject->GetName().GetCStr() );

							ULOGGER_MOBO.SetProgressMessage( cMsg );
						}
						else
						{
							cutfEvent *pEvent = NULL;
							cutfEventArgs *pEventArgs = NULL;

							if ( iVisibleEventIndex < visibleEventIndexList.GetCount() )
							{
								pEvent = objectEventList[visibleEventIndexList[iVisibleEventIndex]];
								pEvent->SetTime( fStartTime );

								pEventArgs = const_cast<cutfEventArgs *>( pEvent->GetEventArgs() );

								++iVisibleEventIndex;
							}
							else
							{
								pEventArgs = pEventArgsToClone->Clone();

								pEvent = rage_new cutfObjectIdEvent( iObjectId, fStartTime, iVisibleEventId, pEventArgs );

								pCutFile->AddEvent( pEvent );

								objectEventList.PushAndGrow(pEvent);
								visibleEventIndexList.PushAndGrow(objectEventList.GetCount() - 1);
								++iVisibleEventIndex;
							}                        

							if ( iInvisibleEventIndex < invisibleEventIndexList.GetCount() )
							{
								objectEventList[invisibleEventIndexList[iInvisibleEventIndex]]->SetTime( fEndTime );
								objectEventList[invisibleEventIndexList[iInvisibleEventIndex]]->SetEventArgs( pEventArgs );

								++iInvisibleEventIndex;
							}
							else
							{
								pEvent = rage_new cutfObjectIdEvent( iObjectId, fEndTime, iInvisibleEventId, pEventArgs );
								pCutFile->AddEvent( pEvent );

								objectEventList.PushAndGrow(pEvent);
								invisibleEventIndexList.PushAndGrow(objectEventList.GetCount() - 1);
								++iInvisibleEventIndex;
							}
						}

						fStartTime = -1.0f;
						fEndTime = -1.0f;
						bFoundStart = false;
						bFoundEnd = false;
					}
				}                

				if ( bFoundStart && !bFoundEnd )
				{
					if(!bIsTrigger && !bIsActive)
					{
						fStartTime = pCutFile->GetRangeStart()/30.0f;
					}

					fEndTime = pCutFile->GetTotalDuration();
					if ( fStartTime < fEndTime )
					{
						cutfEvent *pEvent = NULL;
						cutfEventArgs *pEventArgs = NULL;

						if ( iVisibleEventIndex < visibleEventIndexList.GetCount() )
						{
							pEvent = objectEventList[visibleEventIndexList[iVisibleEventIndex]];
							pEvent->SetTime( fStartTime );

							pEventArgs = const_cast<cutfEventArgs *>( pEvent->GetEventArgs() );

							++iVisibleEventIndex;
						}
						else
						{
							pEventArgs = pEventArgsToClone->Clone();

							pEvent = rage_new cutfObjectIdEvent( iObjectId, fStartTime, iVisibleEventId, pEventArgs );

							pCutFile->AddEvent( pEvent );

							objectEventList.PushAndGrow(pEvent);
							visibleEventIndexList.PushAndGrow(objectEventList.GetCount() - 1);
							++iVisibleEventIndex;
						}                        

						if ( iInvisibleEventIndex < invisibleEventIndexList.GetCount() )
						{
							objectEventList[invisibleEventIndexList[iInvisibleEventIndex]]->SetTime( fEndTime );
							objectEventList[invisibleEventIndexList[iInvisibleEventIndex]]->SetEventArgs( pEventArgs );

							++iInvisibleEventIndex;
						}
						else
						{
							pEvent = rage_new cutfObjectIdEvent( iObjectId, fEndTime, iInvisibleEventId, pEventArgs ) ;
							pCutFile->AddEvent( pEvent );

							objectEventList.PushAndGrow(pEvent);
							invisibleEventIndexList.PushAndGrow(objectEventList.GetCount() - 1);
							++iInvisibleEventIndex;
						}

						fStartTime = -1.0f;
						fEndTime = -1.0f;
						bFoundStart = false;
						bFoundEnd = false;
					}
				}

				// Regardless of what we just did, we'll assume we added events (or added no events if the animation has been
				// disabled for the entire length of the cutscene).
				bAddedEvents = iKeyCount > 0;
			}
		}

		// we haven't added any events (i.e. Visibility had no keys), so add them at the start and end of the scene
		if ( !bAddedEvents && model->Visibility )
		{
			cutfEvent *pEvent = NULL;
			cutfEventArgs *pEventArgs = NULL;

			if ( iVisibleEventIndex < visibleEventIndexList.GetCount() )
			{
				pEvent = objectEventList[visibleEventIndexList[iVisibleEventIndex]];
				pEvent->SetTime( 0.0f );

				pEventArgs = const_cast<cutfEventArgs *>( pEvent->GetEventArgs() );

				++iVisibleEventIndex;
			}
			else
			{
				pEventArgs = pEventArgsToClone->Clone();

				pEvent = rage_new cutfObjectIdEvent( iObjectId, 0.0f, iVisibleEventId, pEventArgs );

				pCutFile->AddEvent( pEvent );

				objectEventList.PushAndGrow(pEvent);
				visibleEventIndexList.PushAndGrow(objectEventList.GetCount() - 1);
				++iVisibleEventIndex;
			}

			if ( invisibleEventIndexList.GetCount() > 0 )
			{
				objectEventList[invisibleEventIndexList[invisibleEventIndexList.GetCount() - 1]]->SetTime( pCutFile->GetTotalDuration() );
				objectEventList[invisibleEventIndexList[invisibleEventIndexList.GetCount() - 1]]->SetEventArgs( pEventArgs );

				invisibleEventIndexList[invisibleEventIndexList.GetCount() - 1] = -1;
			}
			else
			{
				pEvent = rage_new cutfObjectIdEvent( iObjectId, pCutFile->GetTotalDuration(), iInvisibleEventId, pEventArgs );
				pCutFile->AddEvent( pEvent );

				objectEventList.PushAndGrow(pEvent);
				invisibleEventIndexList.PushAndGrow(objectEventList.GetCount() - 1);
				++iInvisibleEventIndex;
			}        

			bAddedEvents = true;
		}
		else if ( bAddedEvents && iVisibleEventIndex == 0)
		{
			char cMsg[512];
			sprintf( cMsg, "WARNING: The object '%s' is not visible because of its \"Visibilty\" track content.  This object will not be seen in the exported cutscene.  Add a valid visibility key to have this object appear.", 
				pObject->GetDisplayName().c_str() );

			ULOGGER_MOBO.SetProgressMessage( cMsg );
		}
	}

	// remove the extra start and end times
	RemoveExtraEvents( pCutFile, objectEventList, iVisibleEventIndex, iInvisibleEventIndex, 
		visibleEventIndexList, invisibleEventIndexList );
}

void IToolObjectInterchange::AddTypeFile( cutfObject *pObject )
{
	cutfPropModelObject *pModelObject = dynamic_cast<cutfPropModelObject *>(pObject); // Limit to props
	if(pModelObject)
	{
		atString strModelName(pModelObject->GetName().GetCStr());
		strModelName.Set(strModelName.c_str(), strModelName.GetLength(), 0, (strModelName.IndexOf(":") != -1 ? strModelName.IndexOf(":") : strModelName.GetLength()) );
		strModelName += ":Dummy01";
			
		HFBModel pModel = RAGEFindModelByName(strModelName.c_str());
		if( pModel )
		{
			HFBProperty pProperty = pModel->PropertyList.Find("UDP3DSMAX");
			if( pProperty )
			{
				FBPropertyString *pPropertyString = (FBPropertyString *)pProperty;

				char* pValue = (char*)pPropertyString->GetPropertyValue();
				if(pValue)
				{
					// UDP string is split by spaces if there are multiple
					atString strValue(pValue);

					int iStartIndex = strValue.IndexOf("MAXFILE=");
					if(iStartIndex != -1)
					{
						int iEndIndex = strValue.IndexOf(" ", iStartIndex);
						if(iEndIndex != -1)
						{
							strValue.Set(strValue.c_str(), strValue.GetLength(), iStartIndex+8, iEndIndex-iStartIndex);
						}
						else
						{
							strValue.Set(strValue.c_str(), strValue.GetLength(), iStartIndex+8);
						}

						// strip the path and extension
						char cDest[RAGE_MAX_PATH];
						ASSET.RemoveExtensionFromPath(cDest, RAGE_MAX_PATH, strValue.c_str());

						const char* pFilename = ASSET.FileName(cDest);
						if(pFilename)
						{
							pModelObject->SetTypeFile( atHashString(pFilename) );
						}
					}
				}
			}
		}
	}
}

void IToolObjectInterchange::RemoveExtraEvents( cutfCutsceneFile2 *pCutfile, atArray<cutfEvent *> &objectEventList, 
                                               int iStartIndex, atArray<s32> &indexList )
{
    for ( int i = iStartIndex; i < indexList.GetCount(); ++i )
    {
        if ( indexList[i] == -1 )
        {
            continue;
        }

        pCutfile->RemoveEvent( objectEventList[indexList[i]] );
    }
}

//##############################################################################

AssetManagerToolObjectInterchange::AssetManagerToolObjectInterchange()
{

}

AssetManagerToolObjectInterchange::~AssetManagerToolObjectInterchange()
{

}

void AssetManagerToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    CreateLoadSceneEvent( pObject, pCutFile );

    CreateLoadOverlaysEvent( pObject, pCutFile );

    CreateFixupObjectsEvent( pObject, pCutFile );

    CreateLoadModelsEvent( pObject, pCutFile );

    CreateLoadParticleEffectsEvent( pObject, pCutFile );
}

void AssetManagerToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
    // for now, do nothing
}

void AssetManagerToolObjectInterchange::CreateObjectIdListEvent( s32 iEventType, s32 iObjectType, atArray<cutfEvent *> &eventList,
                                                                cutfObject *pObject, cutfCutsceneFile2 *pCutfile )
{
    int index = -1;
	for ( int i = 0; i < eventList.GetCount(); ++i )
	{
		if ( eventList[i]->GetEventId() == iEventType )
		{
			index = i;
			break;
		}
	}    

    atArray<cutfObject *> objectList;
    pCutfile->FindObjectsOfType( iObjectType, objectList );
    if ( objectList.GetCount() == 0 )
    {
        // no need for this event, so delete it
        if ( index != -1 )
        {
            pCutfile->RemoveEvent( eventList[index] );
        }
    }
    else
    {
        atArray<s32> objectIdList;
        objectIdList.Reserve( objectList.GetCount() );
        for ( int i = 0; i < objectList.GetCount(); ++i )
        {
            objectIdList.Append() = objectList[i]->GetObjectId();
        }

        cutfObjectIdListEventArgs *pObjectIdListEventArgs = NULL;
        if ( index == -1 )
        {
            pObjectIdListEventArgs = rage_new cutfObjectIdListEventArgs();

            pCutfile->AddLoadEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), 0.0f, 
                iEventType, pObjectIdListEventArgs ) );
        }
        else
        {
            pObjectIdListEventArgs 
                = dynamic_cast<cutfObjectIdListEventArgs *>( const_cast<cutfEventArgs *>( eventList[index]->GetEventArgs() ) );
        }

        if ( pObjectIdListEventArgs != NULL )
        {
            atArray<s32> &eventObjectIdList = const_cast<atArray<s32> &>( pObjectIdListEventArgs->GetObjectIdList() );
            eventObjectIdList.Assume( objectIdList );
        }
    }
}

void AssetManagerToolObjectInterchange::CreateLoadSceneEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    int index = -1;
 
    atArray<cutfEvent *> &loadEventList = const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() );
    for ( int i = 0; i < loadEventList.GetCount(); ++i )
    {
        if ( loadEventList[i]->GetEventId() == CUTSCENE_LOAD_SCENE_EVENT )
        {
            index = i;
            break;
        }
    }    

    cutfLoadSceneEventArgs *pLoadSceneEventArgs = NULL;
    if ( index == -1 )
    {
        pLoadSceneEventArgs = rage_new cutfLoadSceneEventArgs();

        pCutFile->AddLoadEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), 0.0f, CUTSCENE_LOAD_SCENE_EVENT,
            pLoadSceneEventArgs ) );
    }
    else
    {
        pLoadSceneEventArgs 
            = dynamic_cast<cutfLoadSceneEventArgs *>( const_cast<cutfEventArgs *>( loadEventList[index]->GetEventArgs() ) );
    }

    if ( pLoadSceneEventArgs )
    {
        pLoadSceneEventArgs->SetName( NULL );   // this is obsolete, but kept for backwards compatibility
        pLoadSceneEventArgs->SetOffset( pCutFile->GetOffset() );
        pLoadSceneEventArgs->SetRotation( pCutFile->GetRotation() );
    }
}

void AssetManagerToolObjectInterchange::CreateAddBlockingBoundsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    CreateObjectIdListEvent( CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT, CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE, 
        const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() ), pObject, pCutFile );
}

void AssetManagerToolObjectInterchange::CreateAddRemovalBoundsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    CreateObjectIdListEvent( CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT, CUTSCENE_REMOVAL_BOUNDS_OBJECT_TYPE, 
        const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() ), pObject, pCutFile );
}

void AssetManagerToolObjectInterchange::CreateLoadOverlaysEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    CreateObjectIdListEvent( CUTSCENE_LOAD_OVERLAYS_EVENT, CUTSCENE_OVERLAY_OBJECT_TYPE, 
        const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() ), pObject, pCutFile );
}

void AssetManagerToolObjectInterchange::CreateLoadSubtitlesEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    int index = -1;
	int i = 0;

    atArray<cutfEvent *> &loadEventList = const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() );
    for ( i = 0; i < loadEventList.GetCount(); ++i )
    {
        if ( loadEventList[i]->GetEventId() == CUTSCENE_LOAD_SUBTITLES_EVENT )
        {
            index = i;
            break;
        }
    }

    atArray<cutfObject *> subtitleObjectList;
    pCutFile->FindObjectsOfType( CUTSCENE_SUBTITLE_OBJECT_TYPE, subtitleObjectList );

    if ( subtitleObjectList.GetCount() == 0 )
    {
        if ( index != -1 )
        {
            pCutFile->RemoveLoadEvent( loadEventList[i] );
        }

        return;
    }

    cutfNameEventArgs *pNameEventArgs = NULL;
    if ( index == -1 )
    {
        pNameEventArgs = rage_new cutfNameEventArgs( pCutFile->GetSceneName() );

        pCutFile->AddLoadEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), 0.0f, CUTSCENE_LOAD_SUBTITLES_EVENT,
            pNameEventArgs ) );
    }
    else
    {
        pNameEventArgs 
            = dynamic_cast<cutfNameEventArgs *>( const_cast<cutfEventArgs *>( loadEventList[index]->GetEventArgs() ) );
        
        pNameEventArgs->SetName( pCutFile->GetSceneName() );
    }
}

void AssetManagerToolObjectInterchange::CreateHideObjectsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    CreateObjectIdListEvent( CUTSCENE_HIDE_OBJECTS_EVENT, CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE, 
        const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() ), pObject, pCutFile );
}

void AssetManagerToolObjectInterchange::CreateFixupObjectsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    CreateObjectIdListEvent( CUTSCENE_FIXUP_OBJECTS_EVENT, CUTSCENE_FIXUP_MODEL_OBJECT_TYPE, 
        const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() ), pObject, pCutFile );
}

void AssetManagerToolObjectInterchange::CreateLoadModelsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    CreateObjectIdListEvent( CUTSCENE_LOAD_MODELS_EVENT, CUTSCENE_MODEL_OBJECT_TYPE, 
        const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() ), pObject, pCutFile );
}

void AssetManagerToolObjectInterchange::CreateLoadParticleEffectsEvent( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
	for ( int i = 0; i < pCutFile->GetLoadEventList().GetCount(); ++i )
	{
		if ( pCutFile->GetLoadEventList()[i]->GetEventId() == CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT )
		{
			pCutFile->RemoveLoadEvent(pCutFile->GetLoadEventList()[i]);
			--i;
		}
	}    

	atArray<cutfObject *> objectList;
	pCutFile->FindObjectsOfType( CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE, objectList );
	pCutFile->FindObjectsOfType( CUTSCENE_PARTICLE_EFFECT_OBJECT_TYPE, objectList );
	if ( objectList.GetCount() != 0 )
	{
		atArray<s32> objectIdList;
		objectIdList.Reserve( objectList.GetCount() );
		for ( int i = 0; i < objectList.GetCount(); ++i )
		{
			objectIdList.Append() = objectList[i]->GetObjectId();
		}

		cutfObjectIdListEventArgs* pObjectIdListEventArgs = rage_new cutfObjectIdListEventArgs();

		pCutFile->AddLoadEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), 0.0f, 
			CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT, pObjectIdListEventArgs ) );

		atArray<s32> &eventObjectIdList = const_cast<atArray<s32> &>( pObjectIdListEventArgs->GetObjectIdList() );
		eventObjectIdList.Assume( objectIdList );
	}
}

//##############################################################################

AnimationManagerToolObjectInterchange::AnimationManagerToolObjectInterchange()
{

}

AnimationManagerToolObjectInterchange::~AnimationManagerToolObjectInterchange()
{

}

void AnimationManagerToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    int index = -1;
    
    // add/edit the load event
    atArray<cutfEvent *> &loadEventList = const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() );
    for ( int i = 0; i < loadEventList.GetCount(); ++i )
    {
        if ( loadEventList[i]->GetEventId() == CUTSCENE_LOAD_ANIM_DICT_EVENT )
        {
            index = i;
            break;
        }
    }

    cutfNameEventArgs *pNameEventArgs = NULL;
    if ( index == -1 )
    {
        pNameEventArgs = rage_new cutfNameEventArgs();

        pCutFile->AddLoadEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), 0.0f,
            CUTSCENE_LOAD_ANIM_DICT_EVENT, pNameEventArgs ) );
    }
    else
    {
        pNameEventArgs = dynamic_cast<cutfNameEventArgs *>( const_cast<cutfEventArgs *>( loadEventList[index]->GetEventArgs() ) );
    }
	
    if ( pNameEventArgs != NULL )
    {
        pNameEventArgs->SetName( "dict" );  // make them all the same name
    }
}

void AnimationManagerToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport*/ )
{
    // for now, do nothing
}

//##############################################################################

OverlayToolObjectInterchange::OverlayToolObjectInterchange()
{

}

OverlayToolObjectInterchange::~OverlayToolObjectInterchange()
{

}

void OverlayToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{  
    // find all of the existing hide and show events
    atArray<cutfEvent *> overlayEventList;
    atArray<int> showOverlayEventIndexList;
    atArray<int> hideOverlayEventIndexList;
    FilterEventsForObject( pCutFile, pObject, overlayEventList, CUTSCENE_SHOW_OVERLAY_EVENT, CUTSCENE_HIDE_OVERLAY_EVENT, 
        showOverlayEventIndexList, hideOverlayEventIndexList );

    int iShowEventIndex = 0;
    int iHideEventIndex = 0;

    atArray<cutfObject *> cameraObjectList;
    pCutFile->FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );
    if ( cameraObjectList.GetCount() > 0 )
    {
		HFBModel pCameraModel = rexMBAnimExportCommon::FindModelForObject( cameraObjectList[0] );
        if ( pCameraModel != NULL )
        {
            cutfOverlayObject *pOverlayObj = dynamic_cast<cutfOverlayObject *>( pObject );

            atArray<OverlayEntry> overlayList;
			rexMBAnimExportCommon::GetOverlayFrames( pCameraModel, pOverlayObj->GetName(), overlayList, pCutFile );

            for ( int i = 0; i < overlayList.GetCount(); i++ )
            {
                cutfEventArgs *pEventArgs = NULL;

				if(overlayList[i].bStart)
				{
					// add/edit the start time
					float fStartTime = overlayList[i].iStart / rexMBAnimExportCommon::GetFPS();
					if ( iShowEventIndex < showOverlayEventIndexList.GetCount() )
					{
						overlayEventList[showOverlayEventIndexList[iShowEventIndex]]->SetTime( fStartTime );
						pEventArgs = const_cast<cutfEventArgs *>( overlayEventList[showOverlayEventIndexList[iShowEventIndex]]->GetEventArgs() );
						++iShowEventIndex;
					}
					else
					{
						pEventArgs = rage_new cutfEventArgs;
						pCutFile->AddEvent( 
							rage_new cutfObjectIdEvent( pObject->GetObjectId(), fStartTime, CUTSCENE_SHOW_OVERLAY_EVENT, pEventArgs ) );
					}          
				}
				else
				{
					// add/edit the end time
					float fEndTime = overlayList[i].iEnd / rexMBAnimExportCommon::GetFPS();
					if ( iHideEventIndex < hideOverlayEventIndexList.GetCount() )
					{
						overlayEventList[hideOverlayEventIndexList[iHideEventIndex]]->SetTime( fEndTime );
						overlayEventList[hideOverlayEventIndexList[iHideEventIndex]]->SetEventArgs( pEventArgs );
						++iHideEventIndex;
					}
					else
					{
						pCutFile->AddEvent( 
							rage_new cutfObjectIdEvent( pObject->GetObjectId(), fEndTime, CUTSCENE_HIDE_OVERLAY_EVENT, pEventArgs ) );
					}      
				}       
            }
        }
    }

    // remove the extra start and end times
    RemoveExtraEvents( pCutFile, overlayEventList, iShowEventIndex, iHideEventIndex, 
        showOverlayEventIndexList, hideOverlayEventIndexList );
}

void OverlayToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
    // for now, do nothing
}

//##############################################################################

DecalToolObjectInterchange::DecalToolObjectInterchange()
{

}

DecalToolObjectInterchange::~DecalToolObjectInterchange()
{

}

void DecalToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
	atArray<cutfEvent *> animEventList;

	atArray<int> playAnimEventIndexList;
	atArray<int> stopAnimEventIndexList;
	FilterEventsForObject( pCutFile, pObject, animEventList, CUTSCENE_TRIGGER_DECAL_EVENT, CUTSCENE_REMOVE_DECAL_EVENT, 
		playAnimEventIndexList, stopAnimEventIndexList );

	cutfDecalEventArgs eventArgsToClone;

	// Creates the events for the Visibility property.
	CreateVisibilityEvents( pCutFile, pObject, pObject->GetObjectId(), animEventList,
		CUTSCENE_TRIGGER_DECAL_EVENT, CUTSCENE_REMOVE_DECAL_EVENT,
		playAnimEventIndexList, stopAnimEventIndexList, &eventArgsToClone );

	animEventList.Reset();
	pCutFile->FindEventsForObjectIdOnly(pObject->GetObjectId(), pCutFile->GetEventList(), animEventList, false);

	for(int i=0; i < animEventList.GetCount(); i+=2)
	{
		cutfDecalEventArgs* pDecalEventArgs = dynamic_cast<cutfDecalEventArgs*>(const_cast<cutfEventArgs*>(animEventList[i]->GetEventArgs()));
		if(pDecalEventArgs)
		{
			pDecalEventArgs->SetLifeTime(animEventList[i+1]->GetTime() - animEventList[i]->GetTime());

			HFBModel pModel = rexMBAnimExportCommon::FindModelForObject(pObject);
			if(pModel)
			{
				FBPropertyDouble* pProperty = (FBPropertyDouble*)pModel->PropertyList.Find("dclinit_Height");
				if(pProperty != NULL)
				{
					double data=0;
					pProperty->GetData(&data, sizeof(data));

					pDecalEventArgs->SetHeight(data);
				}

				pProperty = (FBPropertyDouble*)pModel->PropertyList.Find("dclinit_Width");
				if(pProperty != NULL)
				{
					double data=0;
					pProperty->GetData(&data, sizeof(data));

					pDecalEventArgs->SetWidth(data);
				}

				FBPropertyColorAndAlpha* pPropertyColor = (FBPropertyColorAndAlpha*)pModel->PropertyList.Find("dclinit_Color");
				if(pPropertyColor != NULL)
				{
					FBColorAndAlpha data;
					pPropertyColor->GetData(&data, sizeof(data));

					pDecalEventArgs->SetColor(Color32((float)(data[0]), (float)(data[1]), (float)(data[2]), (float)(data[3])));
				}

				// Get the child which is the null and rip its matrix out - this will be the null
				HFBModel pNull = pModel->Children[0];
				if(pNull)
				{
					FBMatrix nullTransformationMatrixFB;
					nullTransformationMatrixFB.Identity();
					pNull->GetMatrix(nullTransformationMatrixFB, kModelTransformation, true);

					Matrix34 nullTransformationMatrixRage;
					nullTransformationMatrixRage.Identity();
					FBMatrixToRageMatrix34(nullTransformationMatrixFB, nullTransformationMatrixRage);
					nullTransformationMatrixRage.Normalize();

					nullTransformationMatrixRage.RotateFullY(PI);

					// Zup
					Quaternion quatYUp;
					Quaternion quatZUp;
					nullTransformationMatrixRage.ToQuaternion(quatYUp);
					quatZUp.x = -quatYUp.x;
					quatZUp.y = quatYUp.z;
					quatZUp.z = quatYUp.y;
					quatZUp.w = quatYUp.w;

					float y = nullTransformationMatrixRage.d.y;
					nullTransformationMatrixRage.d.x = -nullTransformationMatrixRage.d.x;
					nullTransformationMatrixRage.d.y = nullTransformationMatrixRage.d.z;
					nullTransformationMatrixRage.d.z = y;  

					nullTransformationMatrixRage.d *= MOTIONBUILDER_UNIT_SCALE;

					pDecalEventArgs->SetPosition(nullTransformationMatrixRage.d);

					pDecalEventArgs->SetRotationQuaternion(Vector4(quatZUp.x, quatZUp.y, quatZUp.z, quatZUp.w));
				}
			}
		}
	}
}

void DecalToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
	// for now, do nothing
}

//##############################################################################

ScreenFadeToolObjectInterchange::ScreenFadeToolObjectInterchange()
{

}

ScreenFadeToolObjectInterchange::~ScreenFadeToolObjectInterchange()
{

}

void ScreenFadeToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    atArray<cutfEvent *> &loadEventList = const_cast<atArray<cutfEvent *> &>( pCutFile->GetLoadEventList() );
    atArray<cutfEvent *> screenFadeLoadEventList;
    pCutFile->FindEventsForObjectIdOnly( pObject->GetObjectId(), loadEventList, screenFadeLoadEventList );

    // find all of the existing fade in and fade out events
    atArray<cutfEvent *> screenFadeEventList;
    atArray<int> fadeInEventIndexList;
    atArray<int> fadeOutEventIndexList;
    FilterEventsForObject( pCutFile, pObject, screenFadeEventList, CUTSCENE_FADE_IN_EVENT, CUTSCENE_FADE_OUT_EVENT,
        fadeInEventIndexList, fadeOutEventIndexList );

    int iFadeInEventIndex = 0;
    int iFadeOutEventIndex = 0;

    // find the screen fade model
	HFBModel pScreenFadeModel = rexMBAnimExportCommon::FindModelForObject( pObject );
    if ( pScreenFadeModel != NULL )
    {
        atArray<ScreenFadeEntry> screenFadeList;
		rexMBAnimExportCommon::GetScreenFadeFrames( pScreenFadeModel, screenFadeList, pCutFile );       

        for ( int i = 0; i < screenFadeList.GetCount(); i++ )
        {
            if ( screenFadeList[i].bFadeOut )
            {
                // add/edit the fade out
                float fFadeOutTime = screenFadeList[i].fStart;
                float fFadeOutDuration = screenFadeList[i].fEnd - screenFadeList[i].fStart;
                if ( iFadeOutEventIndex < fadeOutEventIndexList.GetCount() )
                {
                    screenFadeEventList[fadeOutEventIndexList[iFadeOutEventIndex]]->SetTime( fFadeOutTime );

                    cutfScreenFadeEventArgs *pScreenFadeEventArgs = dynamic_cast<cutfScreenFadeEventArgs *>( const_cast<cutfEventArgs *>( 
                        screenFadeEventList[fadeOutEventIndexList[iFadeOutEventIndex]]->GetEventArgs() ) );
                    pScreenFadeEventArgs->SetFloat1( fFadeOutDuration );
                    pScreenFadeEventArgs->SetColor( screenFadeList[i].color );

                    ++iFadeOutEventIndex;
                }
                else
                {
                    pCutFile->AddEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), fFadeOutTime, 
                        CUTSCENE_FADE_OUT_EVENT, rage_new cutfScreenFadeEventArgs( fFadeOutDuration, screenFadeList[i].color ) ) );
                }            
            }
            else
            {
                // add/edit the fade in
                float fFadeInTime = screenFadeList[i].fStart;
                float fFadeInDuration = screenFadeList[i].fEnd - screenFadeList[i].fStart;
                if ( iFadeInEventIndex < fadeInEventIndexList.GetCount() )
                {
                    screenFadeEventList[fadeInEventIndexList[iFadeInEventIndex]]->SetTime( fFadeInTime );

                    cutfScreenFadeEventArgs *pScreenFadeEventArgs 
                        = dynamic_cast<cutfScreenFadeEventArgs *>( const_cast<cutfEventArgs *>( 
                        screenFadeEventList[fadeInEventIndexList[iFadeInEventIndex]]->GetEventArgs() ) );
                    pScreenFadeEventArgs->SetFloat1( fFadeInDuration );
                    pScreenFadeEventArgs->SetColor( screenFadeList[i].color );

                    ++iFadeInEventIndex;
                }
                else
                {
                    pCutFile->AddEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), fFadeInTime, 
                        CUTSCENE_FADE_IN_EVENT, rage_new cutfScreenFadeEventArgs( fFadeInDuration, screenFadeList[i].color ) ) );
                }
            }
        }
    }

    // remove the extra start and end times
    RemoveExtraEvents( pCutFile, screenFadeEventList, iFadeInEventIndex, iFadeOutEventIndex, 
        fadeInEventIndexList, fadeOutEventIndexList );
}

void ScreenFadeToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
    // for now, do nothing
}

//##############################################################################

AnimatedObjectToolObjectInterchange::AnimatedObjectToolObjectInterchange()
{

}

AnimatedObjectToolObjectInterchange::~AnimatedObjectToolObjectInterchange()
{

}

void AnimatedObjectToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    char name[CUTSCENE_OBJNAMELEN];
    safecpy( name, pObject->GetDisplayName().c_str(), sizeof(name) );
    rexMBAnimExportCommon::ReplaceWhitespaceCharacters( name );

    CreateAnimEvents( pObject, pCutFile, name );

	AddTypeFile( pObject );
}

void AnimatedObjectToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport*/ )
{
    // for now, do nothing
}

void AnimatedObjectToolObjectInterchange::CreateAnimEvents( cutfObject *pObject, cutfCutsceneFile2* pCutFile, const char* /*pAnimName*/ )
{
    // find the Animation Manager
    atArray<cutfObject *> animMgrObjList;
    pCutFile->FindObjectsOfType( CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE, animMgrObjList );
    Assert( animMgrObjList.GetCount() > 0 );

    // find all of the set anim and clear events
    atArray<cutfEvent *> animEventList;
    atArray<int> setAnimEventIndexList;
    atArray<int> clearAnimEventIndexList;
    FilterEventsForObject( pCutFile, animMgrObjList[0], animEventList, CUTSCENE_SET_ANIM_EVENT, CUTSCENE_CLEAR_ANIM_EVENT, 
        setAnimEventIndexList, clearAnimEventIndexList );

    // Perform an extra filtering step.  We are only looking for event args that reference us
    for ( int i = 0; i < setAnimEventIndexList.GetCount(); )
    {
        const cutfEvent *pEvent = animEventList[setAnimEventIndexList[i]];
        const cutfObjectIdEventArgs *pEventArgs = dynamic_cast<const cutfObjectIdEventArgs *>( pEvent->GetEventArgs() );
        if ( (pEventArgs != NULL) && (pEventArgs->GetObjectId() != pObject->GetObjectId()) )
        {
            setAnimEventIndexList.Delete( i );
        }
        else
        {
            ++i;
        }
    }

    for ( int i = 0; i < clearAnimEventIndexList.GetCount(); )
    {
        const cutfEvent *pEvent = animEventList[clearAnimEventIndexList[i]];
        const cutfObjectIdEventArgs *pEventArgs = dynamic_cast<const cutfObjectIdEventArgs *>( pEvent->GetEventArgs() );
        if ( (pEventArgs != NULL) && (pEventArgs->GetObjectId() != pObject->GetObjectId()) )
        {
            clearAnimEventIndexList.Delete( i );
        }
        else
        {
            ++i;
        }
    }

    cutfObjectIdEventArgs eventArgsToClone;
    eventArgsToClone.SetObjectId( pObject->GetObjectId() );

    // Creates the events for the Visibility property
    CreateVisibilityEvents( pCutFile, pObject, animMgrObjList[0]->GetObjectId(), animEventList,
        CUTSCENE_SET_ANIM_EVENT, CUTSCENE_CLEAR_ANIM_EVENT,
        setAnimEventIndexList, clearAnimEventIndexList, &eventArgsToClone );
}

//##############################################################################

AnimatedParticleEffectObjectToolObjectInterchange::AnimatedParticleEffectObjectToolObjectInterchange() { } 

AnimatedParticleEffectObjectToolObjectInterchange::~AnimatedParticleEffectObjectToolObjectInterchange() { } 

void AnimatedParticleEffectObjectToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
	char name[CUTSCENE_OBJNAMELEN];
	safecpy( name, pObject->GetDisplayName().c_str(), sizeof(name) );
	rexMBAnimExportCommon::ReplaceWhitespaceCharacters( name );

	CreateAnimEvents( pObject, pCutFile, name );
}

void AnimatedParticleEffectObjectToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
	// for now, do nothing
}


void AnimatedParticleEffectObjectToolObjectInterchange::CreateAnimEvents( cutfObject *pObject, cutfCutsceneFile2* pCutFile, const char* /*pAnimName*/ )
{
	// Find the Animation Manager.
	atArray<cutfObject *> animMgrObjList;
	pCutFile->FindObjectsOfType( CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE, animMgrObjList );
	Assert( animMgrObjList.GetCount() > 0 );

	// find all of the set anim and clear events
	atArray<cutfEvent *> animEventList;
	atArray<int> setAnimEventIndexList;
	atArray<int> clearAnimEventIndexList;
	FilterEventsForObject( pCutFile, animMgrObjList[0], animEventList, CUTSCENE_SET_ANIM_EVENT, CUTSCENE_CLEAR_ANIM_EVENT, 
		setAnimEventIndexList, clearAnimEventIndexList );

	// Remove all SET and CLEAR items in the list for the event arguments that references us.
	//
	for ( int i = 0; i < setAnimEventIndexList.GetCount(); )
	{
		cutfEvent *pEvent = animEventList[setAnimEventIndexList[i]];
		const cutfObjectIdEventArgs *pEventArgs = dynamic_cast<const cutfObjectIdEventArgs *>( pEvent->GetEventArgs() );
		if ( (pEventArgs != NULL) && (pEventArgs->GetObjectId() != pObject->GetObjectId()) )
		{
			setAnimEventIndexList.Delete( i );
		}
		else
		{
			pCutFile->RemoveEvent(pEvent);
			++i;
		}
	}

	for ( int i = 0; i < clearAnimEventIndexList.GetCount(); )
	{
		cutfEvent *pEvent = animEventList[clearAnimEventIndexList[i]];
		const cutfObjectIdEventArgs *pEventArgs = dynamic_cast<const cutfObjectIdEventArgs *>( pEvent->GetEventArgs() );
		if ( (pEventArgs != NULL) && (pEventArgs->GetObjectId() != pObject->GetObjectId()) )
		{
			clearAnimEventIndexList.Delete( i );
		}
		else
		{
			pCutFile->RemoveEvent(pEvent);
			++i;
		}
	}

	if(dynamic_cast<cutfAnimatedParticleEffectObject*>(pObject))
	{
		//Add a CUTSCENE_SET_ANIM_EVENT at the zeroth frame of the animation,
		//and a CUTSCENE_CLEAR_ANIM_EVENT at the last frame of the animation.
		//
		float animStartTime = 0.0f;
		float animEndTime = pCutFile->GetTotalDuration();

		//Then we need to create a new set event.
		cutfObjectIdEventArgs* eventArgs = new cutfObjectIdEventArgs(pObject->GetObjectId());
		cutfEvent* pNewSetEvent = rage_new cutfObjectIdEvent( animMgrObjList[0]->GetObjectId(), animStartTime, CUTSCENE_SET_ANIM_EVENT, eventArgs );
		pCutFile->AddEvent( pNewSetEvent );

		//Then we need to create a new set event.
		cutfEvent* pNewClearEvent = rage_new cutfObjectIdEvent( animMgrObjList[0]->GetObjectId(), animEndTime, CUTSCENE_CLEAR_ANIM_EVENT, eventArgs );
		pCutFile->AddEvent( pNewClearEvent );
	}

	// find all of the set anim and clear events
	animEventList.Reset();
	atArray<int> playAnimEventIndexList;
	atArray<int> stopAnimEventIndexList;
	FilterEventsForObject( pCutFile, pObject, animEventList, CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT, CUTSCENE_STOP_PARTICLE_EFFECT_EVENT, 
		playAnimEventIndexList, stopAnimEventIndexList );

	cutfPlayParticleEffectEventArgs eventArgsToClone;

	// Creates the events for the Visibility property.
	CreateVisibilityEvents( pCutFile, pObject, pObject->GetObjectId(), animEventList,
		CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT, CUTSCENE_STOP_PARTICLE_EFFECT_EVENT,
		playAnimEventIndexList, stopAnimEventIndexList, &eventArgsToClone );

    // Find all of the events for this object
	atArray<cutfEvent*> objectEventList;
	pCutFile->FindEventsForObjectIdOnly( pObject->GetObjectId(), 
        pCutFile->GetEventList(), objectEventList );

	for ( int index = 0; index < objectEventList.GetCount(); ++index )
	{
		cutfEvent *pEvent = objectEventList[index];
		if ( pEvent->GetEventId() == CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT )
		{
			cutfEventArgs* pEventArgs = const_cast<cutfEventArgs*>(pEvent->GetEventArgs());
			cutfPlayParticleEffectEventArgs* pPlayEffectEventArgs = dynamic_cast<cutfPlayParticleEffectEventArgs*>(pEventArgs);

			cutfAnimatedParticleEffectObject* pAnimatedParticleEffectObj = dynamic_cast<cutfAnimatedParticleEffectObject*>(pObject);
			cutfParticleEffectObject* pParticleEffectObj = dynamic_cast<cutfParticleEffectObject*>(pObject);
			if(pAnimatedParticleEffectObj)
			{
				atString strParticleModelName(pAnimatedParticleEffectObj->GetName().GetCStr());

				HFBModel pParticleModel = RAGEFindModelByName(strParticleModelName.c_str());

				HFBModel pConstrainedModel = rexMBAnimExportCommon::GetConstraintObject(pParticleModel);
				if(pConstrainedModel)
				{
					pPlayEffectEventArgs->SetAttachBoneHash(atHash16U(rexMBAnimExportCommon::GetBoneID(pConstrainedModel, true).c_str()));

					bool bFoundAttachedParent = false;
					for(int j=0; j < pCutFile->GetObjectList().GetCount(); j++)
					{
						cutfNamedObject* pNamedObj = dynamic_cast<cutfNamedObject*>(pCutFile->GetObjectList()[j]);
						if(pNamedObj)
						{
							HFBModel pModel = RAGEFindModelByName(pNamedObj->GetName().GetCStr());
							if(pModel)
							{
								if(RecurseFindBone(pModel, atString(pConstrainedModel->LongName.AsString())))
								{
									pPlayEffectEventArgs->SetAttachParentId(pNamedObj->GetObjectId());

									// root
									if(pModel == pConstrainedModel)
									{
										pPlayEffectEventArgs->SetAttachBoneHash(0);
									}

									bFoundAttachedParent = true;
								}
							}
						}
					}

					if(!bFoundAttachedParent)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to determine the attached parent for bone '%s' on particle '%s'. Is the bone valid under the root hierarchy?", pConstrainedModel->LongName.AsString(), strParticleModelName.c_str());
						//return false;
					}
				}
			}
			else if(pParticleEffectObj)
			{
				atString strParticleModelName(pParticleEffectObj->GetName().GetCStr());

				FBMatrix attachedTransformation, particleTransformation;
				HFBModel pParticleModel = RAGEFindModelByName(strParticleModelName.c_str());

				pParticleModel->GetMatrix(particleTransformation, kModelTransformation, true);

				Matrix34 attachedTransformationRage, particleTransformationRage;
				FBMatrixToRageMatrix34(particleTransformation, particleTransformationRage);

				HFBModel pConstrainedModel = rexMBAnimExportCommon::GetConstraintObject(pParticleModel);
				if(pConstrainedModel)
				{
					// THIS IS DIRTY HACK. NEEDS TO BE FIXED CORRECTLY LATER
					// MPW - THIS IS BECAUSE OF THE WAY PARTICLES ARE ATTACHED, SINCE THE DUMMY CAN POTENTIALLY HAVE -90 ROTATION
					// IT FUCKS THE PARTICLE RELATIVE POSITION SINCE THE PARTICLE HAS NO DUMMY.

					char cDummyNode[RAGE_MAX_PATH];
					sprintf(cDummyNode, "%s:Dummy01", rexMBAnimExportCommon::GetNameSpaceFromModel(pConstrainedModel).c_str());

					HFBModel pDummyModel = RAGEFindModelByName(cDummyNode);
					if(pDummyModel)
					{
						FBVector3d oldRotation;
						pDummyModel->GetVector(oldRotation, kModelRotation, true);
						pDummyModel->SetVector(FBVector3d(0,0,0), kModelRotation, true);

						pConstrainedModel->GetMatrix(attachedTransformation, kModelTransformation, true);

						pDummyModel->SetVector(oldRotation, kModelRotation, true);
					}
					else
					{
						pConstrainedModel->GetMatrix(attachedTransformation, kModelTransformation, true);
					}

					// THIS IS DIRTY HACK. NEEDS TO BE FIXED CORRECTLY LATER

					FBMatrixToRageMatrix34(attachedTransformation, attachedTransformationRage);

					particleTransformationRage.Normalize();
					attachedTransformationRage.Normalize();

					particleTransformationRage.DotTranspose(attachedTransformationRage);

					pPlayEffectEventArgs->SetAttachBoneHash(atHash16U(rexMBAnimExportCommon::GetBoneID(pConstrainedModel, true).c_str()));

					bool bFoundAttachedParent = false;
					for(int j=0; j < pCutFile->GetObjectList().GetCount(); j++)
					{
						cutfNamedObject* pNamedObj = dynamic_cast<cutfNamedObject*>(pCutFile->GetObjectList()[j]);
						if(pNamedObj)
						{
							HFBModel pModel = RAGEFindModelByName(pNamedObj->GetName().GetCStr());
							if(pModel)
							{
								if(RecurseFindBone(pModel, atString(pConstrainedModel->LongName.AsString())))
								{
									pPlayEffectEventArgs->SetAttachParentId(pNamedObj->GetObjectId());
									bFoundAttachedParent=true;

									// root
									if(pModel == pConstrainedModel)
									{
										pPlayEffectEventArgs->SetAttachBoneHash(0);
									}
								}
							}
						}
					}

					if(!bFoundAttachedParent)
					{
						ULOGGER_MOBO.SetProgressMessage("ERROR: Unable to determine the attached parent for bone '%s' on particle '%s'. Is the bone valid under the root hierarchy?", pConstrainedModel->LongName.AsString(), strParticleModelName.c_str());
						//return false;
					}
				}

				if(!pConstrainedModel)
				{
					particleTransformationRage.RotateFullY(PI);
				}

				Quaternion qRotation;
				particleTransformationRage.ToQuaternion(qRotation);

				if(!pConstrainedModel)
				{
					float y = qRotation.y;
					qRotation.x = -qRotation.x;
					qRotation.y = qRotation.z;
					qRotation.z = y;
				}

				Vector4 vRotation(qRotation.x, qRotation.y, qRotation.z, qRotation.w);

				pPlayEffectEventArgs->SetInitialRotation(vRotation);

				if(!pConstrainedModel)
				{
					float y = particleTransformationRage.d.y;
					particleTransformationRage.d.x = -particleTransformationRage.d.x;
					particleTransformationRage.d.y = particleTransformationRage.d.z;
					particleTransformationRage.d.z = y;
				}

				particleTransformationRage.d *= MOTIONBUILDER_UNIT_SCALE;

				pPlayEffectEventArgs->SetInitialOffset(particleTransformationRage.d);
			}
		}
	}
}

//##############################################################################

PedModelObjectToolObjectInterchange::PedModelObjectToolObjectInterchange()
{

}

PedModelObjectToolObjectInterchange::~PedModelObjectToolObjectInterchange()
{

}

void PedModelObjectToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    AnimatedObjectToolObjectInterchange::LoadObjectDataFromTool( pObject, pCutFile );

    // look for a face animation
    cutfPedModelObject *pPedModelObject = dynamic_cast<cutfPedModelObject *>( pObject );
    
    char cName[CUTSCENE_OBJNAMELEN];
    safecpy( cName, pPedModelObject->GetDisplayName().c_str(), sizeof(cName) );
    rexMBAnimExportCommon::ReplaceWhitespaceCharacters( cName );

    char cFaceDirectory[RAGE_MAX_PATH];
	strcpy_s(cFaceDirectory, RAGE_MAX_PATH, pCutFile->GetFaceDirectory());

    char cFaceAnimFilename[RAGE_MAX_PATH];
    safecpy( cFaceAnimFilename, 
        pPedModelObject->GetDefaultFaceAnimationFilename( cFaceDirectory ).GetCStr(), sizeof(cFaceAnimFilename) );

    pPedModelObject->SetFoundDefaultFaceAnimation( (pPedModelObject->GetFaceAnimationNodeName().GetCStr() != NULL) && strcmpi(pPedModelObject->GetFaceAnimationNodeName().GetCStr(),"") != 0 && ASSET.Exists( cFaceAnimFilename, "" ) );
	pPedModelObject->SetFaceAndBodyAreMerged( false );

    if ( pPedModelObject->FoundDefaultFaceAnimation() )
    {
        char cMsg[512];
        sprintf( cMsg, "Found default face animation '%s' for Ped Model '%s'.", cFaceAnimFilename, pPedModelObject->GetDisplayName().c_str() );
        ULOGGER_MOBO.SetProgressMessage( cMsg );
    }

    if ( pPedModelObject->OverrideFaceAnimation() )
    {
        char cMsg[512];
        if ( (pPedModelObject->GetOverrideFaceAnimationFilename().GetCStr() != NULL) && (strlen( pPedModelObject->GetOverrideFaceAnimationFilename().GetCStr() ) > 0) )
        {
            if ( ASSET.Exists( pPedModelObject->GetOverrideFaceAnimationFilename().GetCStr(), "" ) )
            {
                sprintf( cMsg, "Found overridden face animation '%s' for Ped Model '%s'.", 
                    pPedModelObject->GetOverrideFaceAnimationFilename().GetCStr(), pPedModelObject->GetDisplayName().c_str() );
            }
            else
            {
                sprintf( cMsg, "WARNING:  Could not find overridden face animation '%s' for Ped Model '%s'.", 
                    pPedModelObject->GetOverrideFaceAnimationFilename().GetCStr(), pPedModelObject->GetDisplayName().c_str() );
            }
        }
        else
        {
            sprintf( cMsg, "The default face animation for the Ped Model '%s' is being overridden.  No face has been provided.", 
                pPedModelObject->GetDisplayName().c_str() );
        }

        ULOGGER_MOBO.SetProgressMessage( cMsg );
    }
}

void PedModelObjectToolObjectInterchange::SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile)
{
    AnimatedObjectToolObjectInterchange::SaveObjectDataToTool( pObject, pCutFile );
}

//##############################################################################

CameraToolObjectInterchange::CameraToolObjectInterchange()
{

}

CameraToolObjectInterchange::~CameraToolObjectInterchange()
{

}

void CameraToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    AnimatedObjectToolObjectInterchange::LoadObjectDataFromTool( pObject, pCutFile );
    
    // retrieve the default near/far draw distances
	HFBModel pModel = rexMBAnimExportCommon::FindModelForObject( pObject );
    if ( pModel != NULL )
    {
        HFBCamera pCamera = (HFBCamera)pModel;
                
        cutfCameraObject *pCameraObject = dynamic_cast<cutfCameraObject *>( pObject );
        pCameraObject->SetNearDrawDistance( (float)pCamera->NearPlaneDistance * 0.01f );
        pCameraObject->SetFarDrawDistance( (float)pCamera->FarPlaneDistance * 0.01f );
    }

    // add/edit the Camera Cut events
    atArray<cutfEvent *> cameraEventList;
    atArray<int> cameraCutEventIndexList;
    atArray<int> emptyEventIndexList;
    FilterEventsForObject( pCutFile, pObject, cameraEventList, CUTSCENE_CAMERA_CUT_EVENT, -1, 
        cameraCutEventIndexList, emptyEventIndexList );
    
    int iCameraCutEventIndex = 0;

    atArray<SCameraCutData> cameraCutDataList;
	rexMBAnimExportCommon::GetCameraCutData( cameraCutDataList, pCutFile );
    for ( int i = 0; i < cameraCutDataList.GetCount(); ++i )
    {
        if ( iCameraCutEventIndex < cameraCutEventIndexList.GetCount() )
        {
            cutfEvent *pEvent = cameraEventList[cameraCutEventIndexList[iCameraCutEventIndex]];
            pEvent->SetTime( cameraCutDataList[i].fTime );

            const cutfEventArgs *pEventArgs = pEvent->GetEventArgs();
            if ( pEventArgs->GetType() != CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE )
            {
                // upgrade to the new event args
                cutfCameraCutEventArgs *pCameraCutEventArgs = rage_new cutfCameraCutEventArgs( cameraCutDataList[i].cName, 
                    cameraCutDataList[i].vPosition, cameraCutDataList[i].vRotationQuaternion );

                pEvent->SetEventArgs( pCameraCutEventArgs );
                pCutFile->AddEventArgs( pCameraCutEventArgs );

                pCutFile->RemoveEventArgs( const_cast<cutfEventArgs *>( pEventArgs ) );
            }
            else
            {
                cutfCameraCutEventArgs *pCameraCutEventArgs 
                    = dynamic_cast<cutfCameraCutEventArgs *>( const_cast<cutfEventArgs *>( pEvent->GetEventArgs() ) );
                pCameraCutEventArgs->SetName( cameraCutDataList[i].cName );
                pCameraCutEventArgs->SetPosition( cameraCutDataList[i].vPosition );
                pCameraCutEventArgs->SetRotationQuaternion( cameraCutDataList[i].vRotationQuaternion );
            }

            ++iCameraCutEventIndex;
        }
        else
        {
            cutfObjectIdEvent *pEvent = rage_new cutfObjectIdEvent( pObject->GetObjectId(), cameraCutDataList[i].fTime, 
                CUTSCENE_CAMERA_CUT_EVENT, rage_new cutfCameraCutEventArgs( cameraCutDataList[i].cName, 
                cameraCutDataList[i].vPosition, cameraCutDataList[i].vRotationQuaternion ) );
           pCutFile->AddEvent( pEvent );
        }        
    }

    for ( int i = iCameraCutEventIndex; i < cameraCutEventIndexList.GetCount(); ++i )
    {
        cutfEvent *pEvent = cameraEventList[cameraCutEventIndexList[i]];
        pCutFile->RemoveEvent( pEvent );
    }

	// Add blend event here.
	atArray<int> cameraBlendEventList;
	atArray<int> emptyBlendEventList;
	FilterEventsForObject( pCutFile, pObject, cameraEventList, CUTSCENE_BLENDOUT_CAMERA_EVENT, -1, 
		cameraBlendEventList, emptyBlendEventList );

	if(cameraBlendEventList.GetCount() > 0)
	{
		if(pCutFile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_USE_BLENDOUT_CAMERA_FLAG))
		{
			if(pCutFile->GetBlendOutCutsceneOffset() == 0 ||
				pCutFile->GetBlendOutCutsceneDuration() == 0)
			{
				atArray<int> cameraCutEventIndexList;
				atArray<int> emptyEventIndexList;
				FilterEventsForObject( pCutFile, pObject, cameraEventList, CUTSCENE_CAMERA_CUT_EVENT, -1, 
					cameraCutEventIndexList, emptyEventIndexList );

				for(int i=cameraCutEventIndexList.GetCount()-1; i > -1; --i)
				{
					cutfEvent* pEvent = cameraEventList[cameraCutEventIndexList[i]];

					float fEndTime = ((pCutFile->GetRangeEnd()+1)/CUTSCENE_FPS);
					if(pEvent->GetTime() <= fEndTime)
					{
						if(pCutFile->GetBlendOutCutsceneOffset() == 0)
							pCutFile->SetBlendOutCutsceneOffset( (fEndTime-pEvent->GetTime())*CUTSCENE_FPS );

						if(pCutFile->GetBlendOutCutsceneDuration() == 0)
							pCutFile->SetBlendOutCutsceneDuration( (fEndTime-pEvent->GetTime())*CUTSCENE_FPS );

						break;
					}
				}
			}

			float fEventTime = ((pCutFile->GetRangeEnd()+1)/CUTSCENE_FPS);
			fEventTime -= (pCutFile->GetBlendOutCutsceneOffset()/CUTSCENE_FPS);

			cutfEvent *pEvent = cameraEventList[cameraBlendEventList[0]];
			pEvent->SetTime(fEventTime);
		}
		else
		{
			cutfEvent *pEvent = cameraEventList[cameraBlendEventList[0]];
			pCutFile->RemoveEvent( pEvent );
		}
	}
	else
	{
		if(pCutFile->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_USE_BLENDOUT_CAMERA_FLAG))
		{
			if(pCutFile->GetBlendOutCutsceneOffset() == 0 ||
				pCutFile->GetBlendOutCutsceneDuration() == 0)
			{
				atArray<int> cameraCutEventIndexList;
				atArray<int> emptyEventIndexList;
				FilterEventsForObject( pCutFile, pObject, cameraEventList, CUTSCENE_CAMERA_CUT_EVENT, -1, 
					cameraCutEventIndexList, emptyEventIndexList );

				for(int i=cameraCutEventIndexList.GetCount()-1; i > -1; --i)
				{
					cutfEvent* pEvent = cameraEventList[cameraCutEventIndexList[i]];

					float fEndTime = ((pCutFile->GetRangeEnd()+1)/CUTSCENE_FPS);
					if(pEvent->GetTime() <= fEndTime)
					{
						if(pCutFile->GetBlendOutCutsceneOffset() == 0)
							pCutFile->SetBlendOutCutsceneOffset( (fEndTime-pEvent->GetTime())*CUTSCENE_FPS );

						if(pCutFile->GetBlendOutCutsceneDuration() == 0)
							pCutFile->SetBlendOutCutsceneDuration( (fEndTime-pEvent->GetTime())*CUTSCENE_FPS );

						break;
					}
				}
			}

			float fEventTime = ((pCutFile->GetRangeEnd()+1)/CUTSCENE_FPS);
			fEventTime -= (pCutFile->GetBlendOutCutsceneOffset()/CUTSCENE_FPS);

			cutfObjectIdEvent *pEvent = rage_new cutfObjectIdEvent( pObject->GetObjectId(), fEventTime, 
				CUTSCENE_BLENDOUT_CAMERA_EVENT, rage_new cutfEventArgs() );
			pCutFile->AddEvent( pEvent );
		}
	}
}

void CameraToolObjectInterchange::SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    AnimatedObjectToolObjectInterchange::SaveObjectDataToTool( pObject, pCutFile );
}

//##############################################################################

VehicleModelToolObjectInterchange::VehicleModelToolObjectInterchange()
{

}

VehicleModelToolObjectInterchange::~VehicleModelToolObjectInterchange()
{

}

void VehicleModelToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    AnimatedObjectToolObjectInterchange::LoadObjectDataFromTool( pObject, pCutFile );

    cutfVehicleModelObject *pVehicleObj = dynamic_cast<cutfVehicleModelObject *>( pObject );

	HFBModel model = rexMBAnimExportCommon::FindModelForObject( pObject );
    if ( model != NULL )
    {
        atArray<atString> &removeBoneNameList = const_cast<atArray<atString> &>( pVehicleObj->GetRemoveBoneNameList() );
		removeBoneNameList.clear();
		
        GenerateVehicleRemovalInfo( model, removeBoneNameList, 
            pCutFile->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_TRANSLATE_BONE_IDS_FLAG ) );
    }
}

void VehicleModelToolObjectInterchange::SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    AnimatedObjectToolObjectInterchange::SaveObjectDataToTool( pObject, pCutFile );

    // for now, do nothing
}

void VehicleModelToolObjectInterchange::GenerateVehicleRemovalInfo( HFBModel hVehBone, atArray<atString>& vehicleRemovalList, 
                                                                   bool bTranslateBoneIds )
{
	HFBProperty hRemoveBone = hVehBone->PropertyList.Find( "RemoveMe" );
    if ( hRemoveBone )
    {
        bool bRemoveBone;
        hRemoveBone->GetData( &bRemoveBone,  sizeof(bRemoveBone) );
        if ( bRemoveBone )
        {
            atString boneName = rexMBAnimExportCommon::GetBoneID( hVehBone, bTranslateBoneIds );

			bool bFound=false;
			for(int i=0; i < vehicleRemovalList.GetCount(); ++i)
			{
				atString boneNameCompare = vehicleRemovalList[i];

				if(strcmp(boneNameCompare.c_str(),boneName.c_str()) == 0)
				{
					bFound=true;
					break;
				}
			}

			if(!bFound)
			{
				vehicleRemovalList.PushAndGrow( boneName );
			}
        }
    }

    for ( int i = 0; i < hVehBone->Children.GetCount(); ++i )
    {
        HFBModel hModelCast = (HFBModel)hVehBone->Children[i];
        GenerateVehicleRemovalInfo( hModelCast, vehicleRemovalList, bTranslateBoneIds );
    }
}

//##############################################################################

LightToolObjectInterchange::LightToolObjectInterchange()
{

}

LightToolObjectInterchange::~LightToolObjectInterchange()
{

}

void LightToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    AnimatedObjectToolObjectInterchange::LoadObjectDataFromTool( pObject, pCutFile );

    cutfLightObject *pLightObj = dynamic_cast<cutfLightObject *>( pObject );

	HFBModel model = rexMBAnimExportCommon::FindModelForObject( pObject );
    if ( model != NULL )
    {
        HFBLight light = (HFBLight)model;
        switch ( light->LightType )
        {
        case kFBLightTypePoint:
            pLightObj->SetLightType( CUTSCENE_POINT_LIGHT_TYPE );
            break;
        case kFBLightTypeSpot:
            pLightObj->SetLightType( CUTSCENE_SPOT_LIGHT_TYPE );
            break;
        case kFBLightTypeInfinite:
        default:
            pLightObj->SetLightType( CUTSCENE_DIRECTIONAL_LIGHT_TYPE );
            break;
        }
    }

	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_01);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_02);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_03);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_04);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_05);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_06);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_07);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_08);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_09);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_10);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_11);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_12);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_13);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_14);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_15);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_16);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_17);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_18);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_19);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_20);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_21);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_22);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_23);
	pLightObj->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_24);
}

void LightToolObjectInterchange::SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    AnimatedObjectToolObjectInterchange::SaveObjectDataToTool( pObject, pCutFile );

    // for now, do nothing
}

//##############################################################################

BlockingBoundsToolObjectInterchange::BlockingBoundsToolObjectInterchange()
{

}

BlockingBoundsToolObjectInterchange::~BlockingBoundsToolObjectInterchange()
{

}

void BlockingBoundsToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    LoadBoundsDataFromTool( pObject, pCutFile );

    // find all of the activate deactivate events
    atArray<cutfEvent *> objectEventList;
    atArray<int> visibleEventIndexList;
    atArray<int> invisibleEventIndexList;
    FilterEventsForObject( pCutFile, pObject, objectEventList, 
        CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT, CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT, 
        visibleEventIndexList, invisibleEventIndexList );

    cutfEventArgs eventArgsToClone;

    // Creates the events for the Visibility property
    CreateVisibilityEvents( pCutFile, pObject, pObject->GetObjectId(), objectEventList, 
        CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT, CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT, 
        visibleEventIndexList, invisibleEventIndexList, &eventArgsToClone );
}

void BlockingBoundsToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
    // for now, do nothing
}

void BlockingBoundsToolObjectInterchange::LoadBoundsDataFromTool( cutfObject *pObject, cutfCutsceneFile2* /*pCutFile*/ )
{
    cutfBlockingBoundsObject *pBlockBoundsObject = dynamic_cast<cutfBlockingBoundsObject *>( pObject );

	HFBModel model = rexMBAnimExportCommon::FindModelForObject( pObject );
    if ( model != NULL )
    {
        Matrix34 boundTransformMtx;

        FBMatrix fbBoundTransformMtx;
        model->GetMatrix( fbBoundTransformMtx );

        // Grab the objects transform
        rexMBAnimExportCommon::ConvertToRageMaxtrixFromMotionBuilder( fbBoundTransformMtx, boundTransformMtx );	

        atArray<Vector3> geomFloorVerts;
        atArray<Vector3> geomCeilVerts;

        // Loop through the vert count and store all the cubes vertices
        HFBGeometry boundGeom = model->Geometry;
        for ( int i = 0; i < boundGeom->VertexCount(); i++)
        {
            FBVertex vert = boundGeom->VertexGet( i );
            Vector3 vertPos = Vector3( vert.mValue[0] / 100.0f, vert.mValue[1] / 100.0f, vert.mValue[2] / 100.0f );
            if ( vertPos.y < 0.0f )
            {
                boundTransformMtx.Transform( vertPos );
                geomFloorVerts.PushAndGrow( vertPos );
            }
            else
            {
                boundTransformMtx.Transform( vertPos );
                geomCeilVerts.PushAndGrow( vertPos );
            }
        }

        atArray<Vector3> vCorners;
        vCorners.Grow() = geomFloorVerts[0];

        for ( int i = 1; i < geomFloorVerts.GetCount(); ++i )
        {
            bool vUnique = true;
            for ( int j = 0; j < vCorners.GetCount(); j++ )
            {
                if ( geomFloorVerts[i] == vCorners[j] )
                {
                    vUnique = false;
                }
            }

            if ( vUnique )
            {
                vCorners.Grow() = geomFloorVerts[i];
            }
        }

        if ( vCorners.GetCount() == 4 )
        {
            if ( geomCeilVerts.GetCount() > 0 )
            {
                pBlockBoundsObject->SetHeight( geomCeilVerts[0].GetY() - vCorners[0].GetY() );
            }
            else
            {
                // old method, not reliable
                FBVector3d fbScale;
                model->GetVector( fbScale, kModelScaling );

                // Get the height form the scaling.  Seems weird but this is motionbuilder!
                pBlockBoundsObject->SetHeight( ( (float) fbScale[1] / 100.0f) * 2.0f );
            }

            for ( int i = 0; i < 4; ++i )
            {
                pBlockBoundsObject->SetCorner( i, vCorners[i] );
            }
        }
    }
}

//##############################################################################

RemovalBoundsToolObjectInterchange::RemovalBoundsToolObjectInterchange()
{

}

RemovalBoundsToolObjectInterchange::~RemovalBoundsToolObjectInterchange()
{

}

void RemovalBoundsToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    LoadBoundsDataFromTool( pObject, pCutFile );

    // find all of the activate/deactivate events
    atArray<cutfEvent *> objectEventList;
    atArray<int> activateEventIndexList;
    atArray<int> deactivateEventIndexList;
    FilterEventsForObject( pCutFile, pObject, objectEventList, 
        CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT, CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT, 
        activateEventIndexList, deactivateEventIndexList );

    int iActivateEventIndex = 0;
    int iDeactivateEventIndex = 0;

    cutfEventArgs *pEventArgs = NULL;

    if ( activateEventIndexList.GetCount() > 0 )
    {
        cutfEvent *pEvent = objectEventList[activateEventIndexList[iActivateEventIndex]];
        pEvent->SetTime( 0.0f );

        pEventArgs = const_cast<cutfEventArgs *>( pEvent->GetEventArgs() );

        ++iActivateEventIndex;
    }
    else
    {
        pEventArgs = rage_new cutfEventArgs();

        pCutFile->AddEvent( 
            rage_new cutfObjectIdEvent( pObject->GetObjectId(), 0.0f, CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT, pEventArgs ) );
    }

    if ( deactivateEventIndexList.GetCount() > 0 )
    {
        cutfEvent *pEvent = objectEventList[deactivateEventIndexList[iDeactivateEventIndex]];
        pEvent->SetTime( pCutFile->GetTotalDuration() );
        pEvent->SetEventArgs( pEventArgs );

        ++iDeactivateEventIndex;
    }
    else
    {
        pCutFile->AddEvent( 
            rage_new cutfObjectIdEvent( pObject->GetObjectId(), pCutFile->GetTotalDuration(), 
            CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT, pEventArgs ) );
    }

    // remove the extra start and end times
    RemoveExtraEvents( pCutFile, objectEventList, iActivateEventIndex, iDeactivateEventIndex, 
        activateEventIndexList, deactivateEventIndexList );
}

void RemovalBoundsToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
    // for now, do nothing
}

//##############################################################################

AudioToolObjectInterchange::AudioToolObjectInterchange()
{

}

AudioToolObjectInterchange::~AudioToolObjectInterchange()
{

}

void AudioToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
    cutfAudioObject *pAudioObject = dynamic_cast<cutfAudioObject *>( pObject );

    // add/edit the load event
    atArray<cutfEvent *> loadAudioEventList;
    pCutFile->FindEventsForObjectIdOnly( pObject->GetObjectId(), 
        pCutFile->GetLoadEventList(), loadAudioEventList );
    if ( loadAudioEventList.GetCount() > 0 )
    {
        cutfNameEventArgs *pNameEventArgs = dynamic_cast<cutfNameEventArgs *>( const_cast<cutfEventArgs *>( loadAudioEventList[0]->GetEventArgs() ) );
        pNameEventArgs->SetName( pAudioObject->GetDisplayName().c_str() );
    }
    else
    {
        pCutFile->AddLoadEvent( 
            rage_new cutfObjectIdEvent( pAudioObject->GetObjectId(), 0.0f, 
            CUTSCENE_LOAD_AUDIO_EVENT, rage_new cutfNameEventArgs( pAudioObject->GetDisplayName().c_str() ) ) );
    }

    atArray<cutfEvent *> audioEventList;
    atArray<int> playAudioEventIndexList;
    atArray<int> stopAudioEventIndexList;
    FilterEventsForObject( pCutFile, pObject, audioEventList, CUTSCENE_PLAY_AUDIO_EVENT, CUTSCENE_STOP_AUDIO_EVENT, 
        playAudioEventIndexList, stopAudioEventIndexList );

    int iPlayEventIndex = 0;
    int iStopEventIndex = 0;

    cutfEventArgs *pEventArgs = NULL;

    if ( playAudioEventIndexList.GetCount() > 0 )
    {
        cutfEvent *pEvent = audioEventList[playAudioEventIndexList[iPlayEventIndex]];
        pEvent->SetTime( 0.0f );
        
        pEventArgs = const_cast<cutfEventArgs *>( pEvent->GetEventArgs() );
        
        cutfNameEventArgs *pNameEventArgs = dynamic_cast<cutfNameEventArgs *>( pEventArgs );
        pNameEventArgs->SetName( pAudioObject->GetDisplayName().c_str() );

        ++iPlayEventIndex;
    }
    else
    {
        pEventArgs = rage_new cutfNameEventArgs( pAudioObject->GetDisplayName().c_str() );

        pCutFile->AddEvent( 
            rage_new cutfObjectIdEvent( pAudioObject->GetObjectId(), 0.0f, 
            CUTSCENE_PLAY_AUDIO_EVENT, pEventArgs ) );
    }

    if ( stopAudioEventIndexList.GetCount() > 0 )
    {
        cutfEvent *pEvent = audioEventList[stopAudioEventIndexList[iStopEventIndex]];
        pEvent->SetTime( pCutFile->GetTotalDuration() );
        pEvent->SetEventArgs( pEventArgs );

        ++iStopEventIndex;
    }
    else
    {
        pCutFile->AddEvent( 
            rage_new cutfObjectIdEvent( pAudioObject->GetObjectId(), pCutFile->GetTotalDuration(), 
            CUTSCENE_STOP_AUDIO_EVENT, pEventArgs ) );
    }

	// Set audio offset here, since audio files generally start from 0, we also need this for concat
	atArray<cutfObject *>& objectList = const_cast<atArray<cutfObject *> &>(pCutFile->GetObjectList());
	for(int i=0; i < objectList.GetCount(); ++i)
	{
		cutfAudioObject* pAudioObject = dynamic_cast<cutfAudioObject*>(objectList[i]);
		if(pAudioObject != NULL)
		{
			pAudioObject->SetOffset(pCutFile->GetRangeStart()/CUTSCENE_FPS);
		}
	}

    // remove the extra start and end times
    RemoveExtraEvents( pCutFile, audioEventList, iPlayEventIndex, iStopEventIndex, 
        playAudioEventIndexList, stopAudioEventIndexList );
}

void AudioToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
    // for now, do nothing
}

//##############################################################################

RayfireToolObjectInterchange::RayfireToolObjectInterchange()
{

}

RayfireToolObjectInterchange::~RayfireToolObjectInterchange()
{

}

void RayfireToolObjectInterchange::LoadObjectDataFromTool( cutfObject* /*pObject*/, cutfCutsceneFile2* pCutFile )
{
	atArray<cutfObject *>& atObjects = const_cast<atArray<cutfObject *> &>( pCutFile->GetObjectList() );
	for(int i=0; i < atObjects.GetCount(); ++i)
	{
		cutfRayfireObject* pRayfireObject = dynamic_cast<cutfRayfireObject*>(atObjects[i]);
		if(pRayfireObject)
		{
			pCutFile->RemoveObjectFromEventList(pRayfireObject->GetObjectId(), const_cast<atArray<cutfEvent*> &>(pCutFile->GetEventList()));
			pCutFile->RemoveObjectFromEventList(pRayfireObject->GetObjectId(), const_cast<atArray<cutfEvent*> &>(pCutFile->GetLoadEventList()));

			FBStory& lStory = FBStory::TheOne();
			HFBStoryFolder lFolder = lStory.RootFolder;
			FBPropertyListStoryFolder lRootChildren = lFolder->Childs;

			for(int k= 0; k < lRootChildren.GetCount(); k++)
			{
				HFBStoryFolder lChildFolder = (HFBStoryFolder)lRootChildren.GetAt(k);

				const char* pFolderName = (const char*)lChildFolder->LongName;

				if(strcmpi("Rayfire", pFolderName) == 0) // Rayfire is in a sub story folder
				{
					for (int j = 0; j < lChildFolder->Tracks.GetCount(); j++)
					{
						HFBStoryTrack lTrack = lChildFolder->Tracks[j];

						if(lTrack->Type.AsInt() == kFBStoryTrackAnimation)
						{
							const char* pTrackName = (const char*)lTrack->LongName;

							if(strcmpi(pRayfireObject->GetDisplayName().c_str(), pTrackName) == 0)
							{
								for (int l = 0; l < lTrack->Clips.GetCount(); l++)
								{
									HFBStoryClip lClip = lTrack->Clips[l];

									// We have to retrieve the start/stop as string because as int, returns 0, brilliant!
									char cStartTime[RAGE_MAX_PATH];
									char cEndTime[RAGE_MAX_PATH];

									strcpy(cStartTime,(char*)lClip->Start.AsString());
									strcpy(cEndTime,(char*)lClip->Stop.AsString());

									atString strEndTime(cEndTime); // The end time has a * within, ie 256*, so we remove it
									if(strEndTime.EndsWith("*"))
									{
										cEndTime[strlen(cEndTime)-1] = 0;
									}

									float fStartTime = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames((float)atof(cStartTime)) / rexMBAnimExportCommon::GetFPS();
									float fEndTime = rexMBAnimExportCommon::GetFrameAfterDiscardedFrames((float)atof(cEndTime)) / rexMBAnimExportCommon::GetFPS();

									int iObjectID = pRayfireObject->GetObjectId();

									HFBModel pModel = RAGEFindModelByName((char*)pRayfireObject->GetName().GetCStr());
									if(pModel != NULL)
									{
										FBVector3d vPosition;
										pModel->GetVector(vPosition, kModelTranslation, false);

										pRayfireObject->SetStartPosition(Vector3(vPosition[0],vPosition[1],vPosition[2]));
									}

									if(fStartTime < (pCutFile->GetRangeStart()/CUTSCENE_FPS) || fEndTime > pCutFile->GetTotalDuration()) 
									{
										ULOGGER_MOBO.SetProgressMessage("WARNING: Rayfire '%s' has being discarded, out of range.", pRayfireObject->GetName().GetCStr());
										continue;
									}

									cutfObjectIdNameEventArgs* pEventArgs = rage_new cutfObjectIdNameEventArgs(iObjectID, pTrackName);
									pCutFile->AddLoadEvent( rage_new cutfObjectIdEvent(0, fStartTime, CUTSCENE_LOAD_RAYFIRE_EVENT, pEventArgs) );
									pCutFile->AddLoadEvent( rage_new cutfObjectIdEvent(0, fEndTime, CUTSCENE_UNLOAD_RAYFIRE_EVENT, pEventArgs) );
									pCutFile->AddEvent( rage_new cutfObjectIdEvent(iObjectID, fStartTime, CUTSCENE_SET_ANIM_EVENT, pEventArgs) );
									pCutFile->AddEvent( rage_new cutfObjectIdEvent(iObjectID, fEndTime, CUTSCENE_CLEAR_ANIM_EVENT, pEventArgs) );
								}
							}
						}
					}
				}
			}
		}
	}

	pCutFile->SortEvents();
	pCutFile->SortLoadEvents();
}

void RayfireToolObjectInterchange::SaveObjectDataToTool( cutfObject* /*pObject*/, cutfCutsceneFile2* /*pCutsceneExport */)
{
	// for now, do nothing
}

//##############################################################################

WeaponModelToolObjectInterchange::WeaponModelToolObjectInterchange()
{

}

WeaponModelToolObjectInterchange::~WeaponModelToolObjectInterchange()
{

}

void WeaponModelToolObjectInterchange::LoadObjectDataFromTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
	AnimatedObjectToolObjectInterchange::LoadObjectDataFromTool( pObject, pCutFile );
}

void WeaponModelToolObjectInterchange::SaveObjectDataToTool( cutfObject *pObject, cutfCutsceneFile2* pCutFile )
{
	AnimatedObjectToolObjectInterchange::SaveObjectDataToTool( pObject, pCutFile );

	// for now, do nothing
}

//##############################################################################

} // namespace rage
