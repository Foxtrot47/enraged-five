#include "pipeline.h"

#include "file/limits.h"
#include "file/asset.h"
#include "file/device.h"

#include "configParser/configGameView.h"
#include "configParser/configExportView.h"

#include "Logger.h"

using namespace configParser;

static ConfigGameView gs_GameView;
static ConfigExportView gs_ExportView;

//////////////////////////////////////////////////////////////////////////
bool rexMBAnimExportCommon::BuildIngameZips( atMap<atString,atString> inputMap )
{
	bool result = true;

	return result;
}

struct EnumDataPair
{
	const char* m_Path;
	atArray<atString>* m_PathList;
};

//////////////////////////////////////////////////////////////////////////
void CB_FindDirectoriesCallback( const fiFindData &data, void *userArg )
{
	EnumDataPair* enumDataPair = reinterpret_cast<EnumDataPair*>(userArg);
	if (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		const char* oldPath = enumDataPair->m_Path;

		atVarString newPath("%s/%s", oldPath, data.m_Name);

		EnumDataPair newEnumDataPair;
		newEnumDataPair.m_Path = newPath;
		enumDataPair->m_PathList->PushAndGrow(newPath);
		newEnumDataPair.m_PathList = enumDataPair->m_PathList;

		ASSET.EnumFiles( newPath, &CB_FindDirectoriesCallback, &newEnumDataPair );
	}

}

//////////////////////////////////////////////////////////////////////////
bool rexMBAnimExportCommon::ProcessAllIngameZips( atString rootdir, bool rebuild )
{
	bool result = true;
	EnumDataPair enumDataPair;
	atArray<atString> directories;

	enumDataPair.m_Path = rootdir;
	enumDataPair.m_PathList = &directories;
	
	ASSET.EnumFiles(rootdir,CB_FindDirectoriesCallback,&enumDataPair);

	rexMBAnimExportCommon::ProcessIngameZips( directories, rebuild, false );
	return result;
}

//////////////////////////////////////////////////////////////////////////
bool rexMBAnimExportCommon::ProcessIngameZips( const atArray<atString>& content, bool rebuild, bool preview, bool nopopups )
{
	bool result = true;

	atString convertExe;
	char cToolsRootPath[RAGE_MAX_PATH];

	gs_GameView.GetToolsRootDir( cToolsRootPath, sizeof(cToolsRootPath) );

	convertExe = atString( atString(cToolsRootPath), "\\ironlib\\lib\\RSG.Pipeline.Convert.exe" );

	char cCmdLine[RAGE_MAX_PATH * 2];
	memset(cCmdLine, 0, RAGE_MAX_PATH * 2);
	safecat( cCmdLine, convertExe.c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " " , sizeof(cCmdLine) );
	
	if( preview )
		safecat( cCmdLine, " --preview" , sizeof(cCmdLine) );
	if( rebuild )
		safecat( cCmdLine, " --rebuild" , sizeof(cCmdLine) );
	if ( nopopups )
		safecat( cCmdLine, " --nopopups", sizeof(cCmdLine) );

	char cDefaultBranch[RAGE_MAX_PATH];
	if(gs_GameView.GetDefaultBranch( cDefaultBranch, sizeof(cDefaultBranch)))
	{
		safecat( cCmdLine, " --branch", sizeof(cCmdLine) );
		safecat( cCmdLine, " ", sizeof(cCmdLine) );
		safecat( cCmdLine, cDefaultBranch, sizeof(cCmdLine) );
	}

	for(atArray<const atString>::iterator contentIter = content.begin(); 
		contentIter != content.end(); contentIter++)
	{
		safecat( cCmdLine, " " , sizeof(cCmdLine) );
		safecat( cCmdLine, (*contentIter).c_str() , sizeof(cCmdLine) );
	}

	ULOGGER_MOBO.SetProgressMessage(cCmdLine);

	int cmdResult = system( cCmdLine );
	if ( cmdResult != 0 )
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: cmd '%s' returned %d.", cCmdLine, result );
		result = false;
	}

	return result;
}

//////////////////////////////////////////////////////////////////////////
atString rexMBAnimExportCommon::GetAssetsDir()
{
	char cAssetsRootDir[RAGE_MAX_PATH];

	gs_GameView.GetAssetsDir( cAssetsRootDir, sizeof(cAssetsRootDir) );
	return atString(cAssetsRootDir);
}

atString rexMBAnimExportCommon::GetExportDir()
{
	char cExportRootDir[RAGE_MAX_PATH];

	gs_GameView.GetExportDir( cExportRootDir, sizeof(cExportRootDir) );
	return atString(cExportRootDir);
}

//////////////////////////////////////////////////////////////////////////
atString rexMBAnimExportCommon::GetIngameAssetsDir()
{
	char cAssetsRootDir[RAGE_MAX_PATH];

	gs_GameView.GetAssetsDir( cAssetsRootDir, sizeof(cAssetsRootDir) );
	atVarString inGameAssetDir("%s/export/anim/ingame/", cAssetsRootDir);
	return (inGameAssetDir);
}

//////////////////////////////////////////////////////////////////////////
atString rexMBAnimExportCommon::GetCutsceneAssetsDir()
{
	char cAssetsRootDir[RAGE_MAX_PATH];

	gs_GameView.GetAssetsDir( cAssetsRootDir, sizeof(cAssetsRootDir) );
	atVarString inGameAssetDir("%s/export/anim/cutscene/", cAssetsRootDir);
	return (inGameAssetDir);
}

//////////////////////////////////////////////////////////////////////////
atString rexMBAnimExportCommon::GetArtDir()
{
	char cArtDir[RAGE_MAX_PATH];

	gs_GameView.GetArtDir( cArtDir, sizeof(cArtDir) );
	atVarString artDir("%s", cArtDir);
	return (artDir);
}



