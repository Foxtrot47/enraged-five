#include "syncedscene.h"

// THIS FILE IS A COPY OF SHARED GAME CODE, THIS CODE HOWEVER WONT COMPILE ON X64 AT PRESENT. WHEN ITS FIXED TO DO SO, USE SHARED CODE NOT THIS FILE
//rage/gta5/dev/rage/framework/src/fwanimation/syncedscene.cpp

#include "parser/macros.h"
#include "parser/manager.h"

using namespace rage;

#include "syncedscene_parser.h"

namespace rage {


//////////////////////////////////////////////////////////////////////////
//	Class to describe an attachment info for a synced scene 
//////////////////////////////////////////////////////////////////////////

	fwSyncedSceneEntityAttachmentInfo::fwSyncedSceneEntityAttachmentInfo()
		: m_Offset(VEC3_ZERO)
		, m_Rot(VEC3_ZERO)
		, m_ParentBone(-1)
		, m_ChildBone(-1)
		, m_flags(0)
	{
	}

	fwSyncedSceneEntityAttachmentInfo::~fwSyncedSceneEntityAttachmentInfo()
	{
	}

	//////////////////////////////////////////////////////////////////////////
	//	Class to describe an entity for a synced scene 
	//////////////////////////////////////////////////////////////////////////

	fwSyncedSceneEntity::fwSyncedSceneEntity()
	{
	}

	fwSyncedSceneEntity::fwSyncedSceneEntity(u32 Type, const atHashString& AnimDict, const atHashString& AnimName, const atHashString& ModelName)
	{
		m_Type = Type;
		m_AnimDict = AnimDict;
		m_AnimName = AnimName;
		m_ModelName = ModelName;
	}

	fwSyncedSceneEntity::~fwSyncedSceneEntity()
	{
	}

	void fwSyncedSceneEntity::SetAttachmentInfo(const Vector3& vOff, const Vector3& vRot, s32 ParentBone, s32 ChildBone)
	{
		m_AttachedInfo.SetOffset(vOff);
		m_AttachedInfo.SetRotation(vRot);
		m_AttachedInfo.SetParentBone(ParentBone);
		m_AttachedInfo.SetChildBone(ChildBone);
	}

	void fwSyncedSceneEntity::AddAttachedEntity(fwSyncedSceneEntity& SyncedEntity)
	{
		m_AttachedEntities.PushAndGrow(SyncedEntity); 
	}

	//////////////////////////////////////////////////////////////////////////
	//	Class for storing scene information 
	//////////////////////////////////////////////////////////////////////////

	void CSyncedSceneInfo::AddLocation(const Vector3& rotation, const Vector3& origin, const char* name)
	{
		CSyncedSceneLocation newLocation;
		newLocation.SetLocationRotation(rotation);
		newLocation.SetLocationOrigin(origin);
		newLocation.SetLocationName(name);

		m_Locations.PushAndGrow(newLocation);
	}

	//////////////////////////////////////////////////////////////////////////
	//	Manager for saving and loading synced scenes via the tools 
	//////////////////////////////////////////////////////////////////////////

	fwSyncedSceneEntityManager::~fwSyncedSceneEntityManager()
	{
	}

	fwSyncedSceneEntityManager::fwSyncedSceneEntityManager()
	{
	}

	bool fwSyncedSceneEntityManager::Save(const char * name)
	{
		bool bfileSaved = false;

		Displayf("Attempting to save synced scene %s", name);

		if (!name || name[0] == '\0')
		{
			Assertf(0, "Synced scene name must have at least one char. Saving file as \"default\"");
			name = "default";
		}

		ASSET.PushFolder("assets:/anim/synced_scenes/");

		if (PARSER.SaveObject(name, "xml", this))
		{
			bfileSaved = true;
		}
		else
		{
			Assertf(0, "Failed to save synced scene %s", name);
		}

		ASSET.PopFolder();

		return bfileSaved;
	}

	bool fwSyncedSceneEntityManager::Load(const char * name)
	{
		bool bfileLoaded = false;

		Displayf("Attempting to load synced scene %s", name);

		ShutDown(); //clear any existing data that might be in the set

		ASSET.PushFolder("assets:/anim/synced_scenes/");

		if (PARSER.LoadObject(name, "xml", *this))
		{
			bfileLoaded = true;
		}
		else
		{
			Assertf(0, "Failed to load synced scene %s", name);
		}

		ASSET.PopFolder();

		return bfileLoaded;
	}

	void fwSyncedSceneEntityManager::StoreSyncedSceneEntity(SyncedSceneTypes Type, const atHashString& AnimDict, const atHashString& AnimName, const atHashString& ModelName)
	{
		fwSyncedSceneEntity* NewEntity = rage_new fwSyncedSceneEntity((u32)Type, AnimDict, AnimName, ModelName); 
		m_SyncedSceneEntites.PushAndGrow(*NewEntity);
	}

	void fwSyncedSceneEntityManager::ShutDown()
	{
		m_SyncedSceneEntites.Reset();
		m_SceneInfo.Reset();
	}

} // namespace rage

/*#endif*/ // __BANK