
#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"

using namespace rage;

namespace rexMBAnimExportCommon
{
	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	bool BuildIngameZips( atMap<atString,atString> inputMap );

	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	bool ProcessAllIngameZips( atString rootdir, bool rebuild );

	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	bool ProcessIngameZips( const atArray<atString>& content, bool rebuild = false, bool preview = false, bool nopopups = false );

	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	atString GetAssetsDir();

	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	atString GetExportDir();

	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	atString GetIngameAssetsDir();

	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	atString GetCutsceneAssetsDir();

	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	atString GetArtDir();
}
