#include "Logger.h"

#include "file/asset.h"

CLogger::CLogger()
{
	m_pProgressBar = NULL;
	m_strLogFilename = "";
}

CLogger *CLogger::_instance=NULL;
CLogger& CLogger::GetInstance()
{
	if (_instance == NULL)
	{
		_instance = new CLogger;
	}

	return *_instance;
}

void CLogger::ResetProgress()
{
	if ( m_pProgressBar != NULL )
	{
#if K_KERNEL_VERSION > 10000
		m_pProgressBar->ProgressDone();
#else
		m_pProgressBar->Caption = FBString( "Ready" );
		m_pProgressBar->Text = FBString("");
		m_pProgressBar->Percent = 0;
#endif
	}
}

void CLogger::SetProgressCaption( const char *pCaption, ... )
{
	char cMsg[RAGE_MAX_PATH*4];
	memset(cMsg,0,RAGE_MAX_PATH*4);
	va_list args;
	va_start (args, pCaption);
	vsprintf_s (cMsg, RAGE_MAX_PATH*4, pCaption, args);
	va_end (args);

	if ( m_pProgressBar != NULL )
	{
		m_pProgressBar->Caption = FBString( cMsg );
	}
}

void CLogger::SetProgressMessage(const char * pMessage, ...)
{
	char cMsg[RAGE_MAX_PATH*4];
	memset(cMsg,0,RAGE_MAX_PATH*4);
	va_list args;
	va_start (args, pMessage);
	vsprintf_s (cMsg, RAGE_MAX_PATH*4, pMessage, args);
	va_end (args);

	UniversalLogHelper::SetProgressMessage(cMsg);
}

void CLogger::SetProgressPercent( float fPercent )
{
	if ( m_pProgressBar != NULL )
	{
		if ( fPercent < 0.0f )
		{
			fPercent = 0.0f;
		}
		else if ( fPercent > 100.0f )
		{
			fPercent = 100.0f;
		}

		m_pProgressBar->Percent = (int) fPercent;
	}
}

void CLogger::PreExport( const char * pLogFilename, const char* pContext )
{
	Assert( m_pProgressBar == NULL );
	m_pProgressBar = rage_new FBProgress;
#if K_KERNEL_VERSION > 10000
	m_pProgressBar->ProgressBegin();
#endif
	m_pProgressBar->Caption = FBString( "" );
	m_pProgressBar->Percent = 0;

	if(pLogFilename)
	{
		if(pContext)
			InitPath(pLogFilename, pContext);
		else
			InitPath(pLogFilename);

		ImmediateFlush(false);

		m_strLogFilename = pLogFilename;
	}
}

void CLogger::PostExport( bool bShowDialog, bool bResult, const char* ptrTitle, const char* ptrMessage )
{
	ResetProgress();

	if( m_pProgressBar != NULL )
	{
		delete m_pProgressBar;
		m_pProgressBar = NULL;
	}

	UniversalLogHelper::PostExport( bShowDialog, false );

	if(bShowDialog)
	{
		if(bResult)
		{
			FBMessageBox((char*)ptrTitle, (char*)ptrMessage, "OK");
		}
	}
}

void CLogger::PostExport( const char* pLogDir, bool bShowDialog, bool bResult, const char* ptrTitle, const char* ptrMessage )
{
	ResetProgress();

	if( m_pProgressBar != NULL )
	{
		delete m_pProgressBar;
		m_pProgressBar = NULL;
	}

	UniversalLogHelper::PostExport( pLogDir, bShowDialog, false );

	if(bShowDialog)
	{
		if(bResult)
		{
			FBMessageBox((char*)ptrTitle, (char*)ptrMessage, "OK");
		}
	}
}

const char* CLogger::GetLogFilename()
{
	return m_strLogFilename.c_str();
}

const char* CLogger::GetProcessLogDirectory()
{
	static atString logDir;
	char chrToolsRoot[RAGE_MAX_PATH];
	if(GetEnvironmentVariable("RS_TOOLSROOT", chrToolsRoot, RAGE_MAX_PATH))
	{
		char chrLogDir[RAGE_MAX_PATH];
		DWORD PID = GetCurrentProcessId();
		sprintf(chrLogDir,"%s\\logs\\motionbuilder.%i",chrToolsRoot,PID);
		logDir = chrLogDir;
	}
	else
	{
		logDir = "";
	}
	return logDir.c_str();
}

void CLogger::ClearProcessLogDirectory()
{
	char chrSubLogDir[RAGE_MAX_PATH];
	DWORD PID = GetCurrentProcessId();
	sprintf(chrSubLogDir,"motionbuilder.%i",PID);
	rexMBAnimExportCommon::ClearLogPathContents(chrSubLogDir);
}