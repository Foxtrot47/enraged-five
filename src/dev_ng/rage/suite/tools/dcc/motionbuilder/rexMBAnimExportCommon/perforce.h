#pragma once
#include <string>
#include "atl/string.h"
#include "atl/array.h"

using namespace rage;

class perforce
{
public:

	static void SyncAndCheckout(const char* pDir, const char* pFile, bool bInteractive);
	static void SyncAndCheckout(const char* pFullPath, bool bInteractive);
	static void SyncAndMarkForDelete(const char* pDir, const char* pFile, bool bInteractive);
	static void SyncAndMarkForDelete(const char* pFullPath, bool bInteractive);
	static void MarkForAdd(const char* pDir, const char* pFile, bool bInteractive);
	static void MarkForDelete(const char* pDir, const char* pFile, bool bInteractive);
	static void Checkout(const char* pDir, const char* pFile, bool bInteractive);
	static void Checkout(const char* pFullPath, bool bInteractive);
	static void Sync(const char* pFullPath, bool bInteractive);
	static void Sync(const char* pDir, const char* pFile, bool bForce, bool bInteractive);
	static void CreateChangelist(const char* pDescription, const char* pDir, bool bInteractive);
	static void Revert(const char* pDir, const char* pFile, bool bInteractive);
	static void GetRevision(const char* pDir, int& revision, bool bInteractive);

	static void ExecuteQueuedOperations(bool bInteractive);

private:
	static void ParseRevisionInfo(int& revision, atString strOutput);
	static void GetInfo(const char* pDir, atString& strP4Info, bool bInteractive);
	static void CreateChangelistDescription(const char* pDescription, atString& atDescriptionFile, bool bInteractive);
	static bool IsSessionValid(const atString& strOutput, bool bInteractive);
	static void ShowErrorMessage(const char* command, const atString& strOutput, bool bInteractive);
	static void WarnIfCheckedOut(const char* cProjectRoot, const char* pFiles, bool bInteractive);

	static void SyncInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive);
	static void AddInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive);
	static void CheckoutInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive);
	static void DeleteInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive);
	static void RevertInternal(const char* cProjectRoot, const char* pFiles, bool bInteractive);

	static void ConvertSpecialCharacters(std::string& inString);
	static void ReplaceSpecialCharacters(std::string& inString);

	static atArray<atString> filesQueuedForSync;
	static atArray<atString> filesQueuedForAdd;
	static atArray<atString> filesQueuedForCheckout;
	static atArray<atString> filesQueuedForDelete;
	static atArray<atString> filesQueuedForRevert;

};