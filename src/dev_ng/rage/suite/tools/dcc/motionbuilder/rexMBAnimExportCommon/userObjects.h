// 
// rexMbRage/userObjects.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __COMMON_USER_OBJECTS_H__
#define __COMMON_USER_OBJECTS_H__

#include "rexMBAnimExportCommon\utility.h"
#include "UserObjects\AnimMarkup.h"
#include "UserObjects\Utility.h"

class MetadataTagObject;
class RexRageAnimExportPData;
class RexAnimMarkupData;
class RexRageCutsceneExportPData;

class UserObjects
{
public:
	static bool Initialize(); 
	static void Destroy();

	static MetadataTagObject*	FindMetadataTagObject(HFBProperty property, HFBComponent component, bool bCreate);
	static void					RemoveMetadataTagObject(HFBProperty property, HFBComponent component);
	static void					RemoveMetadataTagObjectsOnComponent(HFBComponent component);
	static void					RemoveAllMetadataTagObject();

	static RexRageAnimExportPData*	FindPersistentData(bool bCreate);
	static void						DeletePersistentData();
	static RexAnimMarkupData*		LoadMarkupFromString( const char* szInput );
	static void						SetAnimationMarkupData(RexAnimMarkupData* pData);
	static RexAnimMarkupData*		GetAnimationMarkupData();

	static RexRageCutsceneExportPData* FindCutsceneMarkupData(bool bCreate);
	static void							DeleteCutsceneMarkupData();

	static bool						HasLoadedClips();
	static void						SetHasLoadedClips(bool set);

	static bool						HasReloadedClips();
	static void						SetHasReloadedClips(bool set);
};	

#endif //__COMMON_USER_OBJECTS_H__
