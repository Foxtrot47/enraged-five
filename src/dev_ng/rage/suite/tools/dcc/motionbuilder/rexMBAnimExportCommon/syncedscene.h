#ifndef SYNCEDSCENE_H_
#define SYNCEDSCENE_H_

// THIS FILE IS A COPY OF SHARED GAME CODE, THIS CODE HOWEVER WONT COMPILE ON X64 AT PRESENT. WHEN ITS FIXED TO DO SO, USE SHARED CODE NOT THIS FILE
//rage/gta5/dev/rage/framework/src/fwanimation/syncedscene.h

#include "atl/array.h"
#include "atl/hashstring.h"
#include "atl/string.h"
#include "parser/macros.h"
#include "vector/vector3.h"

namespace rage {

	class fwEntity;

	enum SyncedSceneTypes
	{
		SST_PED,
		SST_OBJECT,
		SST_CAMERA,
		SST_VEHICLE
	};

	class fwSyncedSceneEntityAttachmentInfo
	{
	public:
		fwSyncedSceneEntityAttachmentInfo();
		~fwSyncedSceneEntityAttachmentInfo(); 

		Vector3 GetOffset() const { return m_Offset; }
		Vector3 GetRotation() const  { return m_Rot; }
		s32 GetParentBone() const { return m_ParentBone; }
		s32 GetChildBone() const { return m_ChildBone; }

		void SetOffset(const Vector3& vec) { m_Offset = vec; }
		void SetRotation(const Vector3& vec) { m_Rot = vec; }
		void SetParentBone(s32 Bone) { m_ParentBone = Bone; }
		void SetChildBone(s32 Bone) { m_ChildBone = Bone; }

		PAR_SIMPLE_PARSABLE;
	private:
		Vector3 m_Offset; 
		Vector3 m_Rot; 
		s32 m_ParentBone;
		s32 m_ChildBone; 
		s32 m_flags; 
	};

	class fwSyncedSceneEntity
	{
	public:
		fwSyncedSceneEntity(); 

		fwSyncedSceneEntity(u32 Type, const atHashString& AnimDict, const atHashString& AnimName, const atHashString& ModelName); 
		~fwSyncedSceneEntity();

		u32 GetType() const { return m_Type; }
		atHashString GetModelName() const { return m_ModelName; }
		atHashString GetAnimDict() const { return m_AnimDict; }
		atHashString GetAnimName() const { return m_AnimName; }

		void SetAttachmentInfo(const Vector3& vOff, const Vector3& vRot, s32 ParentBone, s32 ChildBone); 

		void AddAttachedEntity(fwSyncedSceneEntity& SyncedEntity); 

		atArray<fwSyncedSceneEntity>& GetAttachedEntities() { return m_AttachedEntities; } 

		const fwSyncedSceneEntityAttachmentInfo& GetAttachementInfo() { return m_AttachedInfo; }

		PAR_SIMPLE_PARSABLE;

	private:
		u32 m_Type; 
		atHashString m_ModelName; 
		atHashString m_AnimDict; 
		atHashString m_AnimName; 

		atArray<fwSyncedSceneEntity> m_AttachedEntities;

		fwSyncedSceneEntityAttachmentInfo m_AttachedInfo; 
	};

	class CSyncedSceneLocation
	{
	public:
		CSyncedSceneLocation() {;}
		~CSyncedSceneLocation() {;} 

		Vector3 GetLocationRotation() const	{ return m_LocationRotation; }
		Vector3 GetLocationOrigin() const { return m_LocationOrigin; }
		atString GetLocationName() { return m_LocationName; }

		void SetLocationRotation(const Vector3& vec) { m_LocationRotation = vec; }
		void SetLocationOrigin(const Vector3& vec) { m_LocationOrigin = vec; }
		void SetLocationName(const char *name) { m_LocationName = name; }

		PAR_SIMPLE_PARSABLE;
	private:
		atString m_LocationName;
		Vector3 m_LocationRotation;
		Vector3 m_LocationOrigin;
	};

	class CSyncedSceneInfo
	{
	public:
		CSyncedSceneInfo() {;} 
		~CSyncedSceneInfo() {;}

		void Reset() { m_SceneName.Reset(); ClearLocations(); }

		void ClearLocations() { m_Locations.clear(); }
		int GetNumLocations() { return m_Locations.GetCount(); }
		void AddLocation(const Vector3& rotation, const Vector3& origin, const char* name);
		Vector3 GetLocationRotation(int idx) { return m_Locations[idx].GetLocationRotation(); }
		Vector3 GetLocationOrigin(int idx) { return m_Locations[idx].GetLocationOrigin(); }
		atString GetLocationName(int idx) { return m_Locations[idx].GetLocationName(); }

		void SetSceneName(const char *name) { m_SceneName = name; }
		atString GetSceneName() { return m_SceneName; }

		PAR_SIMPLE_PARSABLE;
	private:
		atString m_SceneName;
		atArray<CSyncedSceneLocation> m_Locations;
	};



	class fwSyncedSceneEntityManager
	{
	public:

		virtual ~fwSyncedSceneEntityManager();

		fwSyncedSceneEntityManager();

		virtual bool Save(const char * name);

		virtual bool Load(const char * name);

		PAR_SIMPLE_PARSABLE;

	public:

		void StoreSyncedSceneEntity(SyncedSceneTypes Type, const atHashString& AnimDict, const atHashString& AnimName, const atHashString& ModelName);

		void ShutDown();				//clear any data from the manager;

		atArray<fwSyncedSceneEntity> m_SyncedSceneEntites; 

		atArray<fwEntity*> m_pEntities;

	//public:

		CSyncedSceneInfo m_SceneInfo;
	};

fwSyncedSceneEntityManager &GetSyncedSceneEntityManager();

} // namespace rage

// #endif // __BANK

#endif // SYNCEDSCENE_H_
