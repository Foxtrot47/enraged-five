#include "atl/creator.h"
#include "rexMBRage.h"

#include <Windows.h>

//typedef char* (*ExportSyncedSceneFileFunc) (rage::Vector3 position, rage::Vector3 rotation);
typedef char* (*GetSceneInitialOffsetOriginFunc) ();
typedef rage::Vector3* (*GetSyncedSceneRotationFunc) ();
typedef rage::Vector3* (*GetSyncedScenePositionFunc) ();

HMODULE	m_rexMBRageDll = NULL;

// PURPOSE: Initializes the handle to the UserObjects.dll.  Note that instead of calling this 
// at DLL Load-time, as in the loading of this plugin, we have deferred to when a member function is called.
// The DLL does not load successfully when this plug-in is only being initialized.
// RETURN:	bool
bool rexMBRage::Initialize()
{
	m_rexMBRageDll = LoadLibrary("rexMBRage.dll");
	if(m_rexMBRageDll == NULL)
		return false;

	return true;
}

void rexMBRage::Destroy()
{
	if(m_rexMBRageDll != NULL)
	{
		FreeLibrary(m_rexMBRageDll);
	}
}

//char* rexMBRage::ExportSyncedSceneFile(rage::Vector3 position, rage::Vector3 rotation)
//{
//	if(m_rexMBRageDll == NULL)
//	{
//		if(Initialize() == false)
//			return NULL;
//	}
//
//	ExportSyncedSceneFileFunc exportSyncedSceneFunc = (ExportSyncedSceneFileFunc) GetProcAddress(m_rexMBRageDll, "ExportSyncedSceneFile"); 
//	return exportSyncedSceneFunc(position, rotation);
//}

char* rexMBRage::GetSceneInitialOffsetOrigin()
{
	if(m_rexMBRageDll == NULL)
	{
		if(Initialize() == false)
			return NULL;
	}

	GetSceneInitialOffsetOriginFunc getSceneOriginFunc = (GetSceneInitialOffsetOriginFunc) GetProcAddress(m_rexMBRageDll, "GetSceneInitialOffsetOrigin"); 
	return getSceneOriginFunc();
}

rage::Vector3* rexMBRage::GetSyncedScenePosition()
{
	if(m_rexMBRageDll == NULL)
	{
		if(Initialize() == false)
			return NULL;
	}

	GetSyncedScenePositionFunc getScenePositionFunc = (GetSyncedScenePositionFunc) GetProcAddress(m_rexMBRageDll, "GetSyncedScenePosition"); 
	return getScenePositionFunc();
}

rage::Vector3* rexMBRage::GetSyncedSceneRotation()
{
	if(m_rexMBRageDll == NULL)
	{
		if(Initialize() == false)
			return NULL;
	}

	GetSyncedSceneRotationFunc getSceneRotationFunc = (GetSyncedSceneRotationFunc) GetProcAddress(m_rexMBRageDll, "GetSyncedSceneRotation"); 
	return getSceneRotationFunc();
}