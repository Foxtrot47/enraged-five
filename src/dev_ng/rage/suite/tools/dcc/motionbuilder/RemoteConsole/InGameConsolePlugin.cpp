// 
// inGameConsolePlugin.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "InGameConsolePlugin.h"
#include <fbsdk\fbsdk.h>

#include "configParser/configGameView.h"
using namespace configParser;

#include "atl\string.h"
#include "cutscene\cutsmanager.h"

#include "rageRemoteConsole\RemoteConsole.h"
#include "rexMBAnimExportCommon\userObjects.h"
#include "rexMBRage/animExportTool.h"

#include "rexMBRage/shared.h"
#include "rexMBRage.h"

using namespace rage;

//--- Registration defines
#define ORTOOLNOTE__CLASS	InGameConsolePlugin
#define ORTOOLNOTE__LABEL	"In-Game Console"		//This corresponds to the name in the menu.
#define ORTOOLNOTE__DESC	"In-Game Console"

#pragma warning(push)
#pragma warning(disable:4100)

//--- FiLMBOX implementation and registration
FBToolImplementation(	ORTOOLNOTE__CLASS	);
FBRegisterTool		(	ORTOOLNOTE__CLASS,
					 ORTOOLNOTE__LABEL,
					 ORTOOLNOTE__DESC,
					 FB_DEFAULT_SDK_ICON			);	// Icon filename (default=Open Reality icon)

#pragma warning(pop)

#define COMMAND_BUFFER_SIZE 1024

//NOTE:  Between 3D Studio Max and MotionBuilder, the scale differs by 100 (meters, centimeters, respective).
#define MOTIONBUILDER_UNIT_SCALE 1.0f / 100.0f

//Widget Names. 
//These names correspond to the paths in RAG.  If these paths change, functionality will be disabled.

static const char* CameraOverridesWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Override Cam";
static atHashString CameraOverridesWidgetHash(CameraOverridesWidgetName);

static const char* CameraOverrideUsingMatrixWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Override Cam Using Matrix (relative to selected location)";

static const char* CameraMatrixWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Cam Matrix";
static atHashString CameraMatrixWidgetHash(CameraMatrixWidgetName);

static const char* CameraFOVWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Cam FOV";
static const char* CameraDOFNearOutOfFocusWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Near Out Of Focus Dof Plane";
static atHashString CameraDOFNearOutOfFocusWidgetHash(CameraDOFNearOutOfFocusWidgetName);

static const char* CameraDOFNearInFocusWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Near In Focus Dof Plane";
static atHashString CameraDOFNearInFocusWidgetHash(CameraDOFNearInFocusWidgetName);

static const char* CameraDOFFarInFocusWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Far In Focus Dof Plane";
static atHashString CameraDOFFarInFocusWidgetHash(CameraDOFFarInFocusWidgetName);

static const char* CameraDOFFarOutOfFocusWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Far Out Of Focus Dof Plane";
static atHashString CameraDOFFarOutOfFocusWidgetHash(CameraDOFFarOutOfFocusWidgetName);

static const char* CameraShallowDofWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Use Shallow Dof";
static atHashString CameraShallowDofWidgetHash(CameraShallowDofWidgetName);

static const char* CameraMotionBlurWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Camera Editing/Camera Overrides/Cam Motion Blur";
static atHashString CameraMotionBlurWidgetHash(CameraMotionBlurWidgetName);

static const char* AnimSyncedSceneWidgetName = "Anim SyncedScene Editor";
static const char* AnimSyncedSceneToggle = "Anim SyncedScene Editor/Toggle Anim SyncedScene Editor bank";

static const char* AnimSyncedSceneNameWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Scene Name";
static const char* AnimSyncedLoadButton = "Anim SyncedScene Editor/Anim synced scene editor/Load Synced Scene";

static const char* AnimSyncedScenePositionWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Locations/Location_0/Master Position";
static const char* AnimSyncedSceneRotationHeadingWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Locations/Location_0/Master Heading";
static const char* AnimSyncedSceneRotationPitchWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Locations/Location_0/Master Pitch";
static const char* AnimSyncedSceneRotationRollWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Locations/Location_0/Master Roll";

static const char* AnimSyncedMasterPhaseWidgetName = "Anim SyncedScene Editor/Anim synced scene editor/Master phase";


bool InGameConsolePlugin::m_IsConnected = false;
rage::RemoteConsole InGameConsolePlugin::m_RemoteConsole;

static const Version s_version = Version(1, 0, 2);

using namespace rage;

/************************************************
*	Tool creation function.
************************************************/
bool InGameConsolePlugin::FBCreate()
{
	m_SelectedModel = NULL;
	m_LastFrame = -1;
	m_IsPlayingScene = false;
	m_StatusCheckInterval = 1.0f;
	m_SelectionChanged = false;
	m_LastTime = 0;
	m_TimeIntervalMs = 66;  //In milliseconds.

	m_WidgetExistsTimeout = 1000 * 20; //20 second timeout.

	UICreate	();

	// Version
	char cBuffer[32];
	sprintf(cBuffer,"Version: %i.%i.%i", s_version.Major, s_version.Minor, s_version.Revision);
	m_versionLabel.Caption = cBuffer;
	m_versionLabel.Justify = kFBTextJustifyRight;

	return true;
}

/************************************************
*	Tool destruction function.
************************************************/
void InGameConsolePlugin::FBDestroy()
{
	if ( m_RemoteConsole.IsConnected() == true )
	{
		Disconnect();
	}

	//Remove any event.
	OnIdle.Remove(this, (FBCallback) &InGameConsolePlugin::EventToolIdle);
	m_Application.OnFileOpenCompleted.Remove(this, (FBCallback) &InGameConsolePlugin::OnFileOpenCompleted);
	m_Scene->OnChange.Remove( this, (FBCallback) &InGameConsolePlugin::EventSceneChange );
	m_Scene->OnTakeChange.Remove( this, (FBCallback)&InGameConsolePlugin::EventSceneOnTakeChange);
	m_ConnectButton.OnClick.Remove(this, (FBCallback) &InGameConsolePlugin::OnConnectButtonClick);
}

// PURPOSE: Initializes the internal structures to be used in the loading and saving of this metadata.
// RETURN:	void
void InGameConsolePlugin::InitClass()
{
	
}

// PURPOSE: Shuts down the internal structures to be used in the loading and saving of this metadata.
// RETURN:	void
void InGameConsolePlugin::ShutdownClass()
{
	
}

/************************************************
*	Create the UI (Assign regions & controls).
************************************************/
void InGameConsolePlugin::UICreate()
{
	m_Scene = m_System.Scene;

	StartSize[0] = 200;
	StartSize[1] = 310;

	MinSize[0] = 200;
	MinSize[1] = 310;

	int iAttachTopY = rexMBAnimExportCommon::c_iMargin;

	// Version
	AddRegion( "VersionLabel", "VersionLabel",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iLabelWidth*2, kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "VersionLabel", m_versionLabel );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "IpAddressLabel",	"IpAddressLabel",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iMargin*4,			kFBAttachNone,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "IpAddressLabel",	m_ipLabel		);

	AddRegion	( "IpAddressTextBox",	"IpAddressTextBox",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"IpAddressLabel",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "IpAddressTextBox",	m_ipTextBox		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "ConnectButton",	"ConnectButton",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "ConnectButton",	m_ConnectButton		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "PlayCutsceneButton",	"PlayCutsceneButton",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "PlayCutsceneButton",	m_PlaySceneButton		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "SendSceneData",	"SendSceneData",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "SendSceneData",	m_SendSceneDataCheckbox		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;
	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "SendLocationData",	"SendLocationData",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "SendLocationData",	m_SendLocationDataCheckbox		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion( "LocationHeadingNumericUpDown", "LocationHeadingNumericUpDown",
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationHeadingNumericUpDown", m_locationHeadingNumericUpDown );

	AddRegion( "LocationHeadingLabel", "LocationHeadingLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "LocationHeadingNumericUpDown", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationHeadingLabel", m_locationHeadingLabel );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion( "LocationPitchNumericUpDown", "LocationPitchNumericUpDown",
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationPitchNumericUpDown", m_locationPitchNumericUpDown );

	AddRegion( "LocationPitchLabel", "LocationPitchLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "LocationPitchNumericUpDown", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationPitchLabel", m_locationPitchLabel );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion( "LocationRollNumericUpDown", "LocationRollNumericUpDown",
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationRollNumericUpDown", m_locationRollNumericUpDown );

	AddRegion( "LocationRollLabel", "LocationRollLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "LocationRollNumericUpDown", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationRollLabel", m_locationRollLabel );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion( "LocationXNumericUpDown", "LocationXNumericUpDown",
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationXNumericUpDown", m_locationXNumericUpDown );

	AddRegion( "LocationYNumericUpDown", "LocationYNumericUpDown",
		rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"LocationXNumericUpDown",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationYNumericUpDown", m_locationYNumericUpDown );

	AddRegion( "LocationZNumericUpDown", "LocationZNumericUpDown",
		rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"LocationYNumericUpDown",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationZNumericUpDown", m_locationZNumericUpDown );

	AddRegion( "LocationPositionLabel", "LocationPositionLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "LocationZNumericUpDown", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LocationPositionLabel", m_locationPositionLabel );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "SaveLocationButton",	"SaveLocationButton",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "SaveLocationButton",	m_locationSaveButton );

	m_locationHeadingLabel.Caption = "Heading";
	m_locationPitchLabel.Caption = "Pitch";
	m_locationRollLabel.Caption = "Roll";
	m_locationPositionLabel.Caption = "Pos.";

	m_SendSceneDataCheckbox.Style = kFBCheckbox;
	m_SendSceneDataCheckbox.Caption = "Interactive Mode";
	m_SendSceneDataCheckbox.State = CHECKBOX_UNCHECKED;
	m_SendSceneDataCheckbox.Enabled = false;

	m_SendLocationDataCheckbox.Caption = "Edit Location Live";
	m_SendLocationDataCheckbox.State = CHECKBOX_UNCHECKED;
	m_SendLocationDataCheckbox.Style = kFBCheckbox;
	m_SendLocationDataCheckbox.Enabled = false;
	m_SendLocationDataCheckbox.OnClick.Add(this, (FBCallback) &InGameConsolePlugin::SendLocationCheckboxClick);

	m_locationSaveButton.OnClick.Add(this, (FBCallback) &InGameConsolePlugin::SaveLocationButtonClick);
	m_locationSaveButton.Caption = "Save Location";

	//Event Handling.
	OnIdle.Add(this, (FBCallback) &InGameConsolePlugin::EventToolIdle);
	m_Application.OnFileOpenCompleted.Add(this, (FBCallback) &InGameConsolePlugin::OnFileOpenCompleted);

	m_Scene->OnChange.Add( this, (FBCallback) &InGameConsolePlugin::EventSceneChange );
	m_Scene->OnTakeChange.Add( this, (FBCallback)&InGameConsolePlugin::EventSceneOnTakeChange);

	m_ipLabel.Caption = "IP:";
	m_ipLabel.Justify = kFBTextJustifyRight;
	m_ipTextBox.Text = "localhost";

	m_ConnectButton.OnClick.Add(this, (FBCallback) &InGameConsolePlugin::OnConnectButtonClick);
	m_ConnectButton.Caption = "Connect";

	m_PlaySceneButton.OnClick.Add(this, (FBCallback) &InGameConsolePlugin::OnPlaySceneButtonClick);
	m_PlaySceneButton.Caption = "Play Synced Scene";
	m_PlaySceneButton.Enabled = false;

	SetLocationUI(false);
}

void InGameConsolePlugin::Connect()
{
	m_ipTextBox.Enabled = false;
	m_ConnectButton.Caption = "Disconnect";
	m_IsConnected = true;
	if ( m_RemoteConsole.Connect(m_ipTextBox.Text.AsString()) == false )
	{
		std::string errorStr = "Unable to connect to ";
		errorStr += m_ipTextBox.Text.AsString();
		errorStr += ".  Please try reconnecting the Remote Console.";
		FBMessageBox("Initialization Error", const_cast<char*>(errorStr.c_str()), "OK");

		Disconnect();
		return;
	}

	//After we have initialize all of these widgets, set update watches
	//on both of these banks.  RAG is currently not stable if you put a 
	//watch on a widget where it may change underneath.
	m_RemoteConsole.AddUpdates(AnimSyncedSceneWidgetName);
	
	EventSelectionChanged(NULL, NULL);
}

void InGameConsolePlugin::Disconnect()
{
	m_ipTextBox.Enabled = true;
	m_ConnectButton.Caption = "Connect";
	m_IsConnected = false;

	m_RemoteConsole.RemoveUpdates(AnimSyncedSceneWidgetName);
	m_RemoteConsole.Disconnect();

	m_PlaySceneButton.Enabled = false;
	m_SendSceneDataCheckbox.Enabled = false;
	m_SendLocationDataCheckbox.Enabled = false;

	SetLocationUI(false);

	StopScene();
}

void InGameConsolePlugin::SetLocationUI(bool bState)
{
	m_locationRollNumericUpDown.Enabled = bState;
	m_locationHeadingNumericUpDown.Enabled = bState;
	m_locationPitchNumericUpDown.Enabled = bState;
	m_locationRollLabel.Enabled = bState;
	m_locationHeadingLabel.Enabled = bState;
	m_locationPitchLabel.Enabled = bState;
	m_locationSaveButton.Enabled = bState;
	m_locationXNumericUpDown.Enabled = bState;
	m_locationYNumericUpDown.Enabled = bState;
	m_locationZNumericUpDown.Enabled = bState;
	m_locationPositionLabel.Enabled = bState;
}

void InGameConsolePlugin::PlayScene()
{
	LoadLocationSettings();
	//Vector3* position = rexMBRage::GetSyncedScenePosition();
	//Vector3* rotation = rexMBRage::GetSyncedSceneRotation();

	const char* pFile = NULL;//rexMBRage::ExportSyncedSceneFile((position) ? *position : VEC3_ZERO, (rotation) ? *rotation : VEC3_ZERO);
	if(pFile != NULL)
	{
		const char* pFileName = ASSET.FileName(pFile);
		char cFileWithoutExtension[RAGE_MAX_PATH];
		ASSET.RemoveExtensionFromPath(cFileWithoutExtension, RAGE_MAX_PATH, pFileName);

		m_RemoteConsole.WriteStringWidget(AnimSyncedSceneNameWidgetName, cFileWithoutExtension);
		m_RemoteConsole.PressWidgetButton(AnimSyncedLoadButton);

		m_PlaySceneButton.Caption = "Stop Synced Scene";
		m_IsPlayingScene = true;
		m_SendSceneDataCheckbox.State = CHECKBOX_CHECKED;
	}
}

void InGameConsolePlugin::StopScene()
{
	m_PlaySceneButton.Caption = "Play Synced Scene";
	m_IsPlayingScene = false;
	m_SendSceneDataCheckbox.State = CHECKBOX_UNCHECKED;
}

void InGameConsolePlugin::LoadLocationSettings()
{
	Vector3* position = rexMBRage::GetSyncedScenePosition();
	Vector3* rotation = rexMBRage::GetSyncedSceneRotation();

	if(position)
	{
		m_locationXNumericUpDown.Value = position->GetX();
		m_locationYNumericUpDown.Value = position->GetY();
		m_locationZNumericUpDown.Value = position->GetZ();
	}

	if(rotation)
	{
		m_locationHeadingNumericUpDown.Value = rotation->GetX();
		m_locationPitchNumericUpDown.Value = rotation->GetY();
		m_locationRollNumericUpDown.Value = rotation->GetZ();
	}
}

void InGameConsolePlugin::SaveLocationButtonClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	Vector3 rotation(m_locationHeadingNumericUpDown.Value, m_locationPitchNumericUpDown.Value, m_locationRollNumericUpDown.Value);
	Vector3 translation(m_locationXNumericUpDown.Value, m_locationYNumericUpDown.Value, m_locationZNumericUpDown.Value);
	//rexMBRage::ExportSyncedSceneFile(translation, rotation);
}

void InGameConsolePlugin::SendLocationCheckboxClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	SetLocationUI(m_SendLocationDataCheckbox.State == CHECKBOX_CHECKED);
}

void InGameConsolePlugin::OnConnectButtonClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if ( m_RemoteConsole.IsConnected() == true )
	{
		Disconnect();
	}
	else
	{
		Connect();

		m_PlaySceneButton.Enabled = true;
		m_SendSceneDataCheckbox.Enabled = true;
		m_SendLocationDataCheckbox.Enabled = true;

		//NOTE: The CutsceneBankToggle will toggle the bank on and off by convention of the runtime system.
		//The remote console requires the bank to be on and needs to avoid inadvertently toggling it off.
		//Without changing the runtime, test for a widget that the cutscene bank will have if it exists.  
		//If that widget does not exist, then turn on the bank.
		if ( m_RemoteConsole.WidgetExists(AnimSyncedSceneNameWidgetName) == false )
		{
			m_RemoteConsole.PressWidgetButton(AnimSyncedSceneToggle);
		}
	}
}

void InGameConsolePlugin::OnPlaySceneButtonClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if ( m_IsPlayingScene == false )
	{
		PlayScene();
	}
	else
	{
		StopScene();	
	}
}

void InGameConsolePlugin::EventToolIdle( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	bool isConnected = m_RemoteConsole.IsConnected();

	if ( m_IsConnected == true && isConnected == true)
	{
		if (m_IsPlayingScene && (m_SendSceneDataCheckbox.State == CHECKBOX_CHECKED))
		{
			SendCurrentFrame();

			u32 time = m_Timer.GetSystemMsTime();
			u32 lastIncrement = (time - m_LastTime);
			if(lastIncrement >= m_TimeIntervalMs)
			{
				OnSelectionChange(true); //Reselect the entities.
				SendCameraData();
				SendLocationData();

				m_LastTime = m_Timer.GetSystemMsTime();
				m_Timer.Reset();
			}
		}
	}
	else if ( m_IsConnected == true && isConnected == false)
	{
		//The connection has been lost; notify the user interface.
		Disconnect();
	}
}

void InGameConsolePlugin::OnFileOpenCompleted( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	m_SelectedModel = NULL;

	//If a cutscene is played, stop it when a new file has been opened.
	if (m_IsPlayingScene == true)
	{
		StopScene();
	}
}

void InGameConsolePlugin::EventSceneOnTakeChange(HISender /*pSender*/, HKEvent pEvent)
{
	FBEventTakeChange lEvent( pEvent );
	//HFBTake hTake = lEvent.Take;

	if(lEvent.Type == kFBTakeChangeOpened)
	{
		StopScene();
	}
}

void InGameConsolePlugin::EventSceneChange( HISender pSender, HKEvent pEvent )
{
	FBEventSceneChange lEvent( pEvent );

	switch( lEvent.Type )
	{
	case kFBSceneChangeSelect:
		{
			EventSelectionChanged(pSender, pEvent);
		}
		break;

	default:
		break;
	}

	m_Scene = m_System.Scene;
}

void InGameConsolePlugin::EventSelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ ) 
{
	OnSelectionChange(false);
}

void InGameConsolePlugin::OnSelectionChange(bool /* force */)
{
	m_SelectionChanged = true;

	if ( m_IsPlayingScene == false)
	{
		return;	
	}

	FBModelList		selectedModels;
	FBGetSelectedModels( selectedModels, NULL, true );

	int lSelectedCount = selectedModels.GetCount();
	if(lSelectedCount > 1)
	{
		ResetSelection();
		return;
	}
	else if(lSelectedCount == 0)
	{
		ResetSelection();
		return;
	}

	HFBModel previousModel = m_SelectedModel;
	m_SelectedModel = selectedModels[0];

	if ( previousModel == m_SelectedModel )
	{
		return;
	}
}

void InGameConsolePlugin::ResetSelection()
{
	if ( m_RemoteConsole.IsConnected() == false )
	{
		return;
	}

	if ( m_SelectedModel == NULL || m_SelectedModel->Is(FBLight::TypeInfo))
	{
		m_RemoteConsole.WriteBoolWidget(CameraOverrideUsingMatrixWidgetName, false);
		m_RemoteConsole.WriteBoolWidget(CameraOverridesWidgetName, false);
	}

	m_SelectedModel = NULL;
}

// Acquires the current frame in the scene.
int InGameConsolePlugin::GetCurrentFrame()
{
	// Motionbuilder action timeline frame number
	const char* timeStr = m_System.LocalTime.AsString();
	int frame;
	sscanf(timeStr, "%d", &frame);
	return frame;
}

void InGameConsolePlugin::SendLocationData()
{
	if( m_SendLocationDataCheckbox.State != CHECKBOX_CHECKED)
	{
		return;
	}

	Vector3 location(m_locationXNumericUpDown.Value, m_locationYNumericUpDown.Value, m_locationZNumericUpDown.Value);
	m_RemoteConsole.WriteVector3Widget(AnimSyncedScenePositionWidgetName, RCC_VEC3V(location));

	m_RemoteConsole.WriteFloatWidget(AnimSyncedSceneRotationHeadingWidgetName, m_locationHeadingNumericUpDown.Value);
	m_RemoteConsole.WriteFloatWidget(AnimSyncedSceneRotationPitchWidgetName, m_locationPitchNumericUpDown.Value);
	m_RemoteConsole.WriteFloatWidget(AnimSyncedSceneRotationRollWidgetName, m_locationRollNumericUpDown.Value);
}

void InGameConsolePlugin::SendCameraData()
{
	if ( m_SelectedModel == NULL || m_SelectedModel->Is(FBCamera::TypeInfo) == false)
	{
		return;
	}
	
	HFBCamera selectedCamera = (HFBCamera) m_SelectedModel;

	m_RemoteConsole.WriteBoolWidget(CameraOverrideUsingMatrixWidgetName, true);
	
	Matrix34 cameraMatrix = GetMatrix(selectedCamera);
	if (m_CachedMatrices[CameraMatrixWidgetHash].IsNotEqual(cameraMatrix) || m_SelectionChanged)
	{
		m_RemoteConsole.WriteMatrix34Widget(CameraMatrixWidgetName, MATRIX34_TO_MAT34V(cameraMatrix));
		m_CachedMatrices[CameraMatrixWidgetHash] = cameraMatrix;
	}

	float fieldOfView = GetFOV(selectedCamera);
	if (m_CachedFloats[CameraFOVWidgetName] != fieldOfView || m_SelectionChanged)
	{
		m_RemoteConsole.WriteFloatWidget(CameraFOVWidgetName, fieldOfView);
		m_CachedFloats[CameraFOVWidgetName] = fieldOfView;
	}

	Vec4V depthOfFieldVector = GetDOF(selectedCamera);
	
	if (m_CachedFloats[CameraDOFNearOutOfFocusWidgetHash] != depthOfFieldVector.GetX().Getf() || m_SelectionChanged)
	{
		m_RemoteConsole.WriteFloatWidget(CameraDOFNearOutOfFocusWidgetName, depthOfFieldVector.GetX().Getf());
		m_CachedFloats[CameraDOFNearOutOfFocusWidgetHash] = depthOfFieldVector.GetX().Getf();
	}

	if (m_CachedFloats[CameraDOFNearInFocusWidgetHash] != depthOfFieldVector.GetY().Getf() || m_SelectionChanged)
	{
		m_RemoteConsole.WriteFloatWidget(CameraDOFNearInFocusWidgetName, depthOfFieldVector.GetY().Getf());
		m_CachedFloats[CameraDOFNearInFocusWidgetHash] = depthOfFieldVector.GetY().Getf();
	}

	if (m_CachedFloats[CameraDOFFarInFocusWidgetHash] != depthOfFieldVector.GetZ().Getf() || m_SelectionChanged)
	{
		m_RemoteConsole.WriteFloatWidget(CameraDOFFarInFocusWidgetName, depthOfFieldVector.GetZ().Getf());
		m_CachedFloats[CameraDOFFarInFocusWidgetHash] = depthOfFieldVector.GetZ().Getf();
	}

	if (m_CachedFloats[CameraDOFFarOutOfFocusWidgetHash] != depthOfFieldVector.GetW().Getf() || m_SelectionChanged)
	{
		m_RemoteConsole.WriteFloatWidget(CameraDOFFarOutOfFocusWidgetName, depthOfFieldVector.GetW().Getf());
		m_CachedFloats[CameraDOFFarOutOfFocusWidgetHash] = depthOfFieldVector.GetW().Getf();
	}

	bool shallowDOFStrength = GetShallowDOFStrength(selectedCamera);
	if (m_CachedBools[CameraShallowDofWidgetHash] != shallowDOFStrength || m_SelectionChanged)
	{
		m_RemoteConsole.WriteBoolWidget(CameraShallowDofWidgetName, shallowDOFStrength);
		m_CachedBools[CameraShallowDofWidgetHash] = shallowDOFStrength;
	}

	float motionBlur = GetMotionBlur(selectedCamera);
	if (m_CachedFloats[CameraMotionBlurWidgetHash] != motionBlur || m_SelectionChanged)
	{
		m_RemoteConsole.WriteFloatWidget(CameraMotionBlurWidgetName, motionBlur);
		m_CachedFloats[CameraMotionBlurWidgetName] = motionBlur;
	}		
}

void InGameConsolePlugin::SendCurrentFrame()
{
	float fPhase = GetCurrentFrame() / (rexMBAnimExportCommon::GetAnimEndTime() * rexMBAnimExportCommon::GetFPS());

	m_RemoteConsole.WriteFloatWidget(AnimSyncedMasterPhaseWidgetName, fPhase );
}

float InGameConsolePlugin::GetFOV(HFBCamera selectedCamera)
{
	return (float)selectedCamera->FieldOfView;
}

bool InGameConsolePlugin::GetShallowDOFStrength(HFBCamera selectedCamera)
{
	HFBProperty pProperty = selectedCamera->PropertyList.Find("Shallow_DOF");
	if ( pProperty != NULL )
	{
		bool bValue;
		pProperty->GetData( &bValue, sizeof(bValue) );

		return bValue;
	}

	return false;
}

float InGameConsolePlugin::GetMotionBlur(HFBCamera selectedCamera)
{
	HFBProperty pProperty = selectedCamera->PropertyList.Find("Motion_Blur");
	if ( pProperty != NULL )
	{
		double fValue;
		pProperty->GetData( &fValue, sizeof(fValue) );

		return (float)fValue;
	}

	return 0;
}

Vec4V_Out InGameConsolePlugin::GetDOF(HFBCamera selectedCamera)
{
	char cName[RAGE_MAX_PATH*2];
	safecpy( cName, selectedCamera->Name.AsString() );

	char cNearPlaneName[RAGE_MAX_PATH*4];
	sprintf( cNearPlaneName, "%s NearPlane", cName );

	char cFarPlaneName[RAGE_MAX_PATH*4];
	sprintf( cFarPlaneName, "%s FarPlane", cName );

	char cNearDOFStrengthPlaneName[1024];
	sprintf( cNearDOFStrengthPlaneName, "%s DOF_Strength_NearPlane", cName );

	char cFarDOFStrengthPlaneName[1024];
	sprintf( cFarDOFStrengthPlaneName, "%s DOF_Strength_FarPlane", cName );

	HFBModel pNearInFocusPlaneModel = RAGEFindModelByName( cNearPlaneName );
	HFBModel pFarInFocusPlaneModel = RAGEFindModelByName( cFarPlaneName );
	HFBModel pNearOutOfFocusPlaneModel = RAGEFindModelByName( cNearDOFStrengthPlaneName );
	HFBModel pFarOutOfFocusPlaneModel = RAGEFindModelByName( cFarDOFStrengthPlaneName );

	// backwards compatibility
	if ( (pNearInFocusPlaneModel == NULL) && (pFarInFocusPlaneModel == NULL) )
	{
		pNearInFocusPlaneModel = RAGEFindModelByName( "NearPlane" );
		pFarInFocusPlaneModel = RAGEFindModelByName( "FarPlane" );
	}

	double fNearOutOfFocusPlaneDistance = 0.0f;
	double fNearInFocusPlaneDistance = 0.1f;
	double fFarInFocusPlaneDistance = 150.0f;
	double fFarOutOfFocusPlaneDistance = 175.0f;

	if ( pNearInFocusPlaneModel && pFarInFocusPlaneModel )
	{            
		FBVector3d nearTrans = pNearInFocusPlaneModel->Translation;
		fNearInFocusPlaneDistance = nearTrans[0];

		FBVector3d farTrans = pFarInFocusPlaneModel->Translation;
		fFarInFocusPlaneDistance = farTrans[0];
	}

	if ( pNearOutOfFocusPlaneModel && pFarOutOfFocusPlaneModel )
	{            
		FBVector3d nearTrans = pNearOutOfFocusPlaneModel->Translation;
		fNearOutOfFocusPlaneDistance = nearTrans[0];

		FBVector3d farTrans = pFarOutOfFocusPlaneModel->Translation;
		fFarOutOfFocusPlaneDistance = farTrans[0];
	}

	return Vec4V((float)(fNearOutOfFocusPlaneDistance * MOTIONBUILDER_UNIT_SCALE), 
		(float)(fNearInFocusPlaneDistance * MOTIONBUILDER_UNIT_SCALE), 
		(float)(fFarInFocusPlaneDistance * MOTIONBUILDER_UNIT_SCALE), 
		(float)(fFarOutOfFocusPlaneDistance * MOTIONBUILDER_UNIT_SCALE));
}

Matrix34 InGameConsolePlugin::GetMatrix(HFBCamera selectedModel)
{
	// We are sending Euler angles to the game, so here we:
	// 1) Get the camera translation/rotation
	// 2) Point it at the interest
	// 3) Add Roll
	// 4) Z-up conversion
	// 5) Translate into game space

	FBQuaternion quatOut;
	FBRVector vecIn;

	FBVector3d rot = selectedModel->Rotation;
	vecIn = FBRVector( rot[0], rot[1], rot[2] );

	//static FBRVector vec2 = FBRVector(0,0,0);

	//if(vec2 != vecIn)
	//{
	//	char buf2[RAGE_MAX_PATH];
	//	sprintf(buf2, "DIR: %f, %f, %f\n", vec2[0], vec2[1], vec2[2]);
	//	OutputDebugString(buf2);
	//	vec2 = vecIn;
	//}

	Quaternion quat;
	Vector3 vec;

	FBRotationToQuaternion( quatOut, vecIn );
	quat.x = (float)quatOut[0];
	quat.y = (float)quatOut[1];
	quat.z = (float)quatOut[2];
	quat.w = (float)quatOut[3];		

	FBVector3d trans = selectedModel->Translation;
	vec.x = (float)(trans[0] * MOTIONBUILDER_UNIT_SCALE);
	vec.y = (float)(trans[1] * MOTIONBUILDER_UNIT_SCALE);
	vec.z = (float)(trans[2] * MOTIONBUILDER_UNIT_SCALE);

	Matrix34 cameraMatrix;
	cameraMatrix.Identity();
	cameraMatrix.FromQuaternion( quat );
	cameraMatrix.d = vec;

	Vector3 upVector = Vector3( 0.0f, 1.0f, 0.0f );
	Vector3 upCameraVector;

	// transform up vector into camera space
	cameraMatrix.Transform( upVector, upCameraVector );
	upCameraVector = upCameraVector - cameraMatrix.d;
	upCameraVector.Normalize();

	Matrix34 cameraMatrixCopy = cameraMatrix;
	cameraMatrix.Identity3x3();

	if ( selectedModel->Interest )
	{
		Vector3 vec;

		FBVector3d trans = selectedModel->Interest->Translation;
		vec.x = (float)(trans[0] * MOTIONBUILDER_UNIT_SCALE);
		vec.y = (float)(trans[1] * MOTIONBUILDER_UNIT_SCALE);
		vec.z = (float)(trans[2] * MOTIONBUILDER_UNIT_SCALE);

		rexMBAnimExportCommon::LookAt( vec, cameraMatrix, upCameraVector );
	}
	else
	{
		// Create a fake interest, our camera needs some kind of point of interest for the direction
		Vector3 forwardVector(1.0f,0.0f,0.0f);
		Vector3 forwardCameraVector;
		Vector3 forwardWorldVector;

		Matrix34 camMatrix = cameraMatrixCopy;
		camMatrix.Transform(forwardVector, forwardCameraVector); // transfer into camera space
		Matrix34 worldMatrix;
		worldMatrix.Identity();
		worldMatrix.UnTransform(forwardCameraVector, forwardWorldVector); // transfer into world space

		rexMBAnimExportCommon::LookAt( forwardWorldVector, cameraMatrix, upCameraVector );
	}

	// Add roll
	cameraMatrix.RotateLocalZ( (float)(selectedModel->Roll * DtoR) );

	// Fix up the cameras otherwise they are 180 the wrong way in-game (previously fixed up at runtime by tom)
	cameraMatrix.RotateFullY(PI);

	// Zup
	Quaternion quatYUp;
	Quaternion quatZUp;
	cameraMatrix.ToQuaternion(quatYUp);
	quatZUp.x = -quatYUp.x;
	quatZUp.y = quatYUp.z;
	quatZUp.z = quatYUp.y;
	quatZUp.w = quatYUp.w;
	cameraMatrix.FromQuaternion(quatZUp);

	float y = cameraMatrix.d.y;
	cameraMatrix.d.x = -cameraMatrix.d.x;
	cameraMatrix.d.y = cameraMatrix.d.z;
	cameraMatrix.d.z = y; 

	char* pInitialOffsetOrigin = rexMBRage::GetSceneInitialOffsetOrigin();
	if(pInitialOffsetOrigin)
	{
		// MPW - DIRTY
		HFBModel originModel = RAGEFindModelByName(pInitialOffsetOrigin);
		if(originModel != NULL)
		{
			// Since Eulers are a cunt we cant just remove the parents rotation via matrices, so 
			// I have to add this hack to remove the rotation from the model then add it again later
			// this then drops down to the parent so I can retrieve values without the dummy rotation
			//////////////////////////////////////////////////////////////////////////
			std::string sOriginDummyName = pInitialOffsetOrigin;
			int colonPos = (int)sOriginDummyName.find_last_of(":");
			sOriginDummyName = sOriginDummyName.substr(0,colonPos);
			sOriginDummyName = sOriginDummyName.append(":Dummy01");

			FBSystem system;
			FBVector3d originalVector;
			HFBModel originModelParent = RAGEFindModelByName(sOriginDummyName.c_str());
			if(originModelParent != NULL)
			{
				originModelParent->GetVector(originalVector,kModelRotation,false);
				originModelParent->SetVector(FBVector3d(0,0,0),kModelRotation,false);
				system.Scene->Evaluate();
			}
			//////////////////////////////////////////////////////////////////////////

			FBMatrix originMatrixFB;
			originModel->GetMatrix(originMatrixFB, kModelTransformation, true);

			//////////////////////////////////////////////////////////////////////////
			if(originModelParent != NULL)
			{
				originModelParent->SetVector(originalVector,kModelRotation,false);
				system.Scene->Evaluate();
			}
			//////////////////////////////////////////////////////////////////////////

			Matrix34 originMatrixRage;
			originMatrixRage.Identity();
			FBMatrixToRageMatrix34(originMatrixFB, originMatrixRage);
			originMatrixRage.Normalize();

			originMatrixRage.d /= 100;

			cameraMatrix.DotTranspose(originMatrixRage);
		}
	}

	return cameraMatrix;
}