// RemoteConsole/DllMain.h

#pragma once

#ifdef REMOTE_CONSOLE_EXPORTS 
#define REMOTE_CONSOLE_API __declspec(dllexport)
#else
#define REMOTE_CONSOLE_API __declspec(dllimport)
#endif

#ifdef __cplusplus
#   define EXTERN_C     extern "C"
#else
#   define EXTERN_C
#endif // __cplusplus



