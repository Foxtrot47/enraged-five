// 
// inGameConsolePlugin.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __INGAME_CONSOLE_H__
#define __INGAME_CONSOLE_H__

#include "cutscene/cutsmanager.h"
#include "system/timer.h"
#include "vector/vector3.h"

#include "RemoteConsole/DllMain.h"
#include "rexMBAnimExportCommon/utility.h"
#include "rageRemoteConsole/RemoteConsole.h"

//--- SDK include
#include <fbsdk/fbsdk.h>

//Forward Declarations
namespace rage
{
	class parTree;
	class parTreeNode;
}

class InGameConsolePlugin : public FBTool
{
	//--- FiLMBOX Tool declaration.
	FBToolDeclare( InGameConsolePlugin, FBTool );

public:
	//--- FiLMBOX Construction/Destruction,
	virtual bool FBCreate           ();		//!< FiLMBOX Creation function.
	virtual void FBDestroy          ();		//!< FiLMBOX Destruction function.

	static void	InitClass();
	static void	ShutdownClass();

private:
	//--- UI Management
	void    UICreate	            ();

	void	EventToolIdle			( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void	EventSceneChange		( HISender pSender, HKEvent pEvent );
	void	EventSelectionChanged	( HISender pSender, HKEvent pEvent );
	void	OnFileOpenCompleted		( HISender pSender, HKEvent pEvent);
	void	EventSceneOnTakeChange	( HISender pSender, HKEvent pEvent);

	void	Connect();
	void	Disconnect();

	void	OnConnectButtonClick	(HISender /*pSender*/, HKEvent /*pEvent*/);
	void	OnPlaySceneButtonClick	(HISender /*pSender*/, HKEvent /*pEvent*/);
	void	SaveLocationButtonClick	(HISender /*pSender*/, HKEvent /*pEvent*/);
	void	SendLocationCheckboxClick	(HISender /*pSender*/, HKEvent /*pEvent*/);

	void	OnSelectionChange(bool /* force */);
	void	ResetSelection();

	int		GetCurrentFrame();

	void	SendLocationData();
	void	SendCameraData();
	void	SendCurrentFrame();

	// Camera
	Matrix34	GetMatrix(HFBCamera selectedModel);
	float		GetFOV(HFBCamera selectedCamera);
	Vec4V_Out	GetDOF(HFBCamera selectedCamera);
	bool		GetShallowDOFStrength(HFBCamera selectedCamera);
	float		GetMotionBlur(HFBCamera selectedCamera);

	void		PlayScene();
	void		StopScene();

	void		SetLocationUI(bool bState);
	void		LoadLocationSettings();

	FBSystem	    m_System;
	FBApplication	m_Application;
	HFBScene		m_Scene;

	FBButton		m_ConnectButton;
	FBButton		m_PlaySceneButton;
	FBButton		m_SendSceneDataCheckbox;
	FBButton		m_SendLocationDataCheckbox;
	FBEdit			m_ipTextBox;
	FBLabel			m_ipLabel;
	FBLabel			m_versionLabel;
	FBEditNumber	m_locationXNumericUpDown;
	FBEditNumber	m_locationYNumericUpDown;
	FBEditNumber	m_locationZNumericUpDown;
	FBEditNumber	m_locationHeadingNumericUpDown;
	FBEditNumber	m_locationPitchNumericUpDown;
	FBEditNumber	m_locationRollNumericUpDown;

	FBLayoutRegion m_cutfileInfoOptionsLayoutRegion;
	FBLayout m_cutfileInfoOptionsLayout;

	FBButton		m_locationSaveButton;

	FBLabel m_locationHeadingLabel;
	FBLabel m_locationPitchLabel;
	FBLabel m_locationRollLabel;
	FBLabel m_locationPositionLabel;

	HFBModel				m_SelectedModel;
	rage::sysTimer			m_Timer;
	rage::u32				m_LastTime;
	float					m_TimeIntervalMs;
	rage::sysTimer			m_SlowTimer;

	int					m_LastFrame;
	float				m_StatusCheckInterval;

public:

	static bool			m_IsConnected;
	static rage::RemoteConsole		m_RemoteConsole;

	bool				m_IsPlayingScene;
	
	int					m_WidgetExistsTimeout;

	bool				m_SelectionChanged;

	atMap<atHashString, float> m_CachedFloats;
	atMap<atHashString, bool> m_CachedBools;
	atMap<atHashString, Matrix34> m_CachedMatrices;
};

#endif // __REMOTE_CONSOLE_H__
