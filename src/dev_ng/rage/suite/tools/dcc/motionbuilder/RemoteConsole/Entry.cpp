#include <windows.h>
#include <commctrl.h>

#include "RemoteConsole\RemoteConsolePlugin.h"
#include "RemoteConsole/InGameConsolePlugin.h"
#include "parser/manager.h"
#include "system/param.h"
#include "rexMBAnimExportCommon/utility.h"
#include "rexMBAnimExportCommon/userObjects.h"

using namespace rage;

#pragma warning(push)
#pragma warning(disable:4100)

FBLibraryDeclare( remoteconsole )
{
	FBLibraryRegister( RemoteConsolePlugin );

	FBLibraryRegister( InGameConsolePlugin );
}

FBLibraryDeclareEnd;

#pragma warning(pop)

bool FBLibrary::LibInit()	
{ 
	RemoteConsolePlugin::InitClass();

	INIT_PARSER;

	return true; 
}

bool FBLibrary::LibOpen()	{ return true; }
bool FBLibrary::LibReady()	{ return true; }
bool FBLibrary::LibClose()	{ return true; }

bool FBLibrary::LibRelease()
{ 
	RemoteConsolePlugin::ShutdownClass();
	SHUTDOWN_PARSER;
	return true; 
}

