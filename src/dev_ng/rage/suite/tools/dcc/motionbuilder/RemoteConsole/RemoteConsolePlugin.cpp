// 
// metadataEditor.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "RemoteConsolePlugin.h"
#include <fbsdk/fbsdk.h>

#include "configParser/configGameView.h"
using namespace configParser;

#include "atl/string.h"
#include "cutscene/cutsmanager.h"
#include "data/growbuffer.h"
#include "file/device.h"
#include "system/endian.h"

#include "rageRemoteConsole/RemoteConsole.h"
#include "UserObjects/cutsceneExportToolPdata.h"
#include "rexMBAnimExportCommon/userObjects.h"

#include "rexMBRage/shared.h"

#include "RSG.Managed.Services/RSG.Managed.Services.Unmanaged.h"

using namespace rage;

//--- Registration defines
#define ORTOOLNOTE__CLASS	RemoteConsolePlugin
#define ORTOOLNOTE__LABEL	"Remote Console"		//This corresponds to the name in the menu.
#define ORTOOLNOTE__DESC	"Remote Console"

#pragma warning(push)
#pragma warning(disable:4100)

//--- FiLMBOX implementation and registration
FBToolImplementation(	ORTOOLNOTE__CLASS	);
FBRegisterTool		(	ORTOOLNOTE__CLASS,
					 ORTOOLNOTE__LABEL,
					 ORTOOLNOTE__DESC,
					 FB_DEFAULT_SDK_ICON			);	// Icon filename (default=Open Reality icon)

#pragma warning(pop)

#define COMMAND_BUFFER_SIZE 1024
#define FALLOFF_CUSTOM_PROPERTY_NAME "Fall Off"

//NOTE:  Between 3D Studio Max and MotionBuilder, the scale differs by 100 (meters, centimeters, respective).
#define MOTIONBUILDER_UNIT_SCALE 1.0f / 100.0f

//Widget Names. 
//These names correspond to the paths in RAG.  If these paths change, functionality will be disabled.

//static const char* ParticleSelectorWidgetName = "Cut Scene Debug/Current Scene/Particle Effects Editing/Particle Effects Overrides/Ptfx Selector";
static const char* ParticleSelectorWidgetName = "Cut Scene Debug/Current Scene/Particle Effects Editing/Particle Effects Overrides/Prop Selector";
static const char* ParticleOverrideWidgetName = "Cut Scene Debug/Current Scene/Particle Effects Editing/Particle Effects Overrides/Override Ptfx";
static const char* ParticlePositionWidgetName = "Cut Scene Debug/Current Scene/Particle Effects Editing/Particle Effects Overrides/PTFX Position";
static const char* ParticleRotationWidgetName = "Cut Scene Debug/Current Scene/Particle Effects Editing/Particle Effects Overrides/PTFX Rotation";

static const char* LightDebugDisplayWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Debug Draw";
static const char* LightSelectorWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Light Selector";
static const char* LightSelectorNone = "(none)";

static const char* LightTypeWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Type";

static const char* LightIntensityOverrideWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Override Intensity";
static const char* LightIntensityWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Intensity";

static const char* LightFalloffOverrideWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Override FallOff";
static const char* LightFalloffWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/FallOff";

static const char* LightAngleOverrideWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Override ConeAngle";
static const char* LightAngleWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/ConeAngle (degrees)";

static const char* LightColourOverrideWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Override Color";
static const char* LightColourWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Color";

static const char* LightPositionOverrideWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Override Position";
static const char* LightPositionWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Position";

static const char* LightDirectionOverrideWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Override Direction";
static const char* LightDirectionWidgetName = "Cut Scene Debug/Current Scene/Light Editing/Selected Light/Direction";

static const char* CutsceneCurrentFrameWidgetName = "Cut Scene Debug/Current Scene/Play Back/Current Frame";
static const char* CutscenePlaybackWidgetName = "Cut Scene Debug/Current Scene/Play Back/Playback";

static const char* CutsceneSceneOrientationWidgetName = "Cut Scene Debug/Current Scene/Scene Properties/Scene Offset";
static const char* CutsceneSceneRotationWidgetName = "Cut Scene Debug/Current Scene/Scene Properties/Scene Rotation";
static const char* CutsceneStartFrameWidgetName = "Cut Scene Debug/Current Scene/Scene Properties/Start Frame";
static const char* CutsceneEndFrameWidgetName = "Cut Scene Debug/Current Scene/Scene Properties/End Frame";

static const char* CutsceneBank = "Cut Scene Debug";
static const char* CutsceneSceneList = "Cut Scene Debug/Cut Scenes";
static const char* CutsceneBankToggle = "Cut Scene Debug/Toggle Cut Scene Debug bank";
static const char* CutsceneSceneStartButton = "Cut Scene Debug/Start or End Selected Cut scene";
static const char* CutsceneSkipToFrame = "Cut Scene Debug/Current Scene/Play Back/SkipToFrame";
static const char* CutsceneStopSelectedCutScene = "Cut Scene Debug/Start or End Selected Cut scene";

static const char* PreviewFolderEnable = "Preview Folder/Enable";
static const char* PreviewFolderFlush = "Preview Folder/Flush scene";

static const char* DebugDirectorActiveModeWidgetName = "Camera/Debug director/Active mode";
static const char* DebugDirectoryFreeMode = "Free";

static const char* CutsceneGetStateCommand = "cs_getstate";
static const char* CutsceneLightIdCommand = "cs_lightid";

static const char* CameraEditingWidgetName = "Cut Scene Debug/Current Scene/Camera Editing";
static const char* CameraOverridesWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Override Cam";

static const char* CameraOverrideUsingMatrixWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Override Cam Using Matrix";
static const char* CameraMatrixWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Cam Matrix";
static const char* CameraPositionWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Cam Position";
static const char* CameraRotationWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Cam Rotation";
static const char* CameraFOVWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Cam FOV";
static const char* CameraDOFNearOutOfFocusWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Near Out Of Focus Dof Plane";
static const char* CameraDOFNearInFocusWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Near In Focus Dof Plane";
static const char* CameraDOFFarInFocusWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Far In Focus Dof Plane";
static const char* CameraDOFFarOutOfFocusWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Far Out Of Focus Dof Plane";
static const char* CameraShallowDofWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Use Shallow Dof";
static const char* CameraSimpleDofWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Use Simple Dof";
static const char* CameraMotionBlurWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Cam Motion Blur";
static const char* CameraCircleOfConfusionWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Circle of Confusion (Dof Plane Strength)"; 
static const char* CameraFocusWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Circle of Confusion (Dof Plane Strength)"; 
static const char* CameraCircleOfConfusionNightWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Circle of Confusion (Dof Plane Strength)";
static const char* CameraDepthOfFieldStateWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Dof State";

static const char* CutsceneDataWidgetName = "Cut Scene Debug/Current Scene/Cutscene Data";
static const char* CameraEditingDataWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Override Data";

static const char* RendererBankName = "Renderer";
static const char* RendererWidgetName = "Renderer/Create Renderer widgets";
static const char* RendererPostFXWidgetName = "Renderer/Post FX/Create PostFX widgets";

static const char* RendererPostFXFrameCaptureWidgetName = "Renderer/Post FX/Continuous Frame Capture/Single Frame Capture";

bool RemoteConsolePlugin::m_IsConnected = false;
bool RemoteConsolePlugin::m_ReloadCutsceneData = false;
bool RemoteConsolePlugin::m_StartCutscene = false;
bool RemoteConsolePlugin::m_StopCutscene = false;
rage::RemoteConsole RemoteConsolePlugin::m_RemoteConsole;

static const Version s_version = Version(1, 3, 4);

using namespace rage;

/************************************************
*	Global Functions.
************************************************/

bool IsRemoteConsoleConnected()
{
	if ( RemoteConsolePlugin::m_IsConnected == true)
	{
		return true;
	}

	return false;
}

void NotifyCutsceneUpdate()
{
	RemoteConsolePlugin::m_ReloadCutsceneData = true;	
}

void NotifyCutscenePatchStarted()
{
	RemoteConsolePlugin::m_StopCutscene = true;
	
	RemoteConsolePlugin::FlushScene();
}

void NotifyCutscenePatchCompleted()
{
	RemoteConsolePlugin::m_StartCutscene = true;
}

/************************************************
*	Tool creation function.
************************************************/
bool RemoteConsolePlugin::FBCreate()
{
	m_SelectedModel = NULL;
	m_LastFrame = -1;
	m_InitializeCutscene = false;
	m_InitializeCapture = false;
	m_IsPlayingCutscene = false;
	m_SendCutsceneData = true;
	m_FollowCameraSwitcher = false;
	m_EditMode = true;
	m_Capture = false;

	m_CurrentGameFrame = -1;
	m_CurrentSceneFrame = -1;
	m_CurrentFrameWithinPart = -1;

	m_WidgetExistsTimeout = 1000 * 20; //20 second timeout.

	LoadCutsceneExportData();
	UICreate	();

	// Version
	char cBuffer[32];
	sprintf(cBuffer,"Version: %i.%i.%i", s_version.Major, s_version.Minor, s_version.Revision);
	m_versionLabel.Caption = cBuffer;
	m_versionLabel.Justify = kFBTextJustifyRight;

	return true;
}

/************************************************
*	Tool destruction function.
************************************************/
void RemoteConsolePlugin::FBDestroy()
{
	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		if(Remote::IsConnected() == true)
		{
			Disconnect();
		}
	}
	else
	{
		if ( m_RemoteConsole.IsConnected() == true )
		{
			Disconnect();
		}
	}

	//Remove any event.
	OnIdle.Remove(this, (FBCallback) &RemoteConsolePlugin::EventToolIdle);
	m_Application.OnFileOpenCompleted.Remove(this, (FBCallback) &RemoteConsolePlugin::OnFileOpenCompleted);
	m_Scene->OnChange.Remove( this, (FBCallback) &RemoteConsolePlugin::EventSceneChange );
	m_ConnectButton.OnClick.Remove(this, (FBCallback) &RemoteConsolePlugin::OnConnectButtonClick);
}

// PURPOSE: Initializes the internal structures to be used in the loading and saving of this metadata.
// RETURN:	void
void RemoteConsolePlugin::InitClass()
{
	
}

// PURPOSE: Shuts down the internal structures to be used in the loading and saving of this metadata.
// RETURN:	void
void RemoteConsolePlugin::ShutdownClass()
{
	
}

/************************************************
*	Create the UI (Assign regions & controls).
************************************************/
void RemoteConsolePlugin::UICreate()
{
	m_Scene = m_System.Scene;

	StartSize[0] = 200;
	StartSize[1] = 360;

	MinSize[0] = 200;
	MinSize[1] = 360;

	int iAttachTopY = rexMBAnimExportCommon::c_iMargin;

	// Version
	AddRegion( "VersionLabel", "VersionLabel",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iLabelWidth*2, kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "VersionLabel", m_versionLabel );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "IpAddressLabel",	"IpAddressLabel",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iMargin*4,			kFBAttachNone,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "IpAddressLabel",	m_ipLabel		);

	AddRegion	( "IpAddressTextBox",	"IpAddressTextBox",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"IpAddressLabel",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "IpAddressTextBox",	m_ipTextBox		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "ConnectButton",	"ConnectButton",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "ConnectButton",	m_ConnectButton		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "PlayCutsceneButton",	"PlayCutsceneButton",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "PlayCutsceneButton",	m_PlayCutsceneButton		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "PlayCaptureCutsceneButton",	"PlayCaptureCutsceneButton",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "PlayCaptureCutsceneButton",	m_PlayCaptureCutsceneButton		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "SendCutsceneData",	"SendCutsceneData",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "SendCutsceneData",	m_SendCutsceneDataCheckbox		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "FollowCameraSwitcher",	"FollowCameraSwitcher",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "FollowCameraSwitcher",	m_FollowCameraSwitcherCheckbox		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "EditMode",	"EditMode",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "EditMode",	m_EditModeCheckbox		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "DofStateLabel",	"DofStateLabel",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachLeft,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		rexMBAnimExportCommon::c_iMargin*12,			kFBAttachNone,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "DofStateLabel",	m_dofStateLabel		);

	AddRegion	( "DofStateComboBox",	"DofStateComboBox",		
		rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"DofStateLabel",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "DofStateComboBox",	m_dofStateComboBox		);

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	AddRegion	( "WCFMode",	"WCFMode",		
		rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iLabelWidth*2, kFBAttachRight,		"",	1.0,
		iAttachTopY,								kFBAttachTop,		"",	1.0,
		-rexMBAnimExportCommon::c_iMargin,			kFBAttachRight,		"",	1.0,
		rexMBAnimExportCommon::c_iRowHeight,		kFBAttachNone,		"",	1.0 );
	SetControl	( "WCFMode",	m_WCFModeCheckbox );

	//Event Handling.
	OnIdle.Add(this, (FBCallback) &RemoteConsolePlugin::EventToolIdle);
	m_Application.OnFileOpenCompleted.Add(this, (FBCallback) &RemoteConsolePlugin::OnFileOpenCompleted);

	m_Scene->OnChange.Add( this, (FBCallback) &RemoteConsolePlugin::EventSceneChange );

	m_ipLabel.Caption = "IP:";
	m_ipLabel.Justify = kFBTextJustifyRight;
	m_ipTextBox.Text = "127.0.0.1";

	m_ConnectButton.OnClick.Add(this, (FBCallback) &RemoteConsolePlugin::OnConnectButtonClick);
	m_ConnectButton.Caption = "Connect";

	m_PlayCutsceneButton.OnClick.Add(this, (FBCallback) &RemoteConsolePlugin::OnPlayCutsceneButtonClick);
	m_PlayCutsceneButton.Caption = "Play Cutscene";
	m_PlayCutsceneButton.Enabled = false;

	m_PlayCaptureCutsceneButton.OnClick.Add(this, (FBCallback) &RemoteConsolePlugin::OnPlayCaptureCutsceneButtonClick);
	m_PlayCaptureCutsceneButton.Caption = "Start Capture";
	m_PlayCaptureCutsceneButton.Enabled = false;

	m_SendCutsceneDataCheckbox.Style = kFBCheckbox;
	m_SendCutsceneDataCheckbox.Caption = "Interactive Mode";
	m_SendCutsceneDataCheckbox.State = CHECKBOX_CHECKED;
	m_SendCutsceneDataCheckbox.OnClick.Add(this, (FBCallback) &RemoteConsolePlugin::OnUseGameCutsceneDataClick);
	m_SendCutsceneDataCheckbox.Enabled = false;

	m_FollowCameraSwitcherCheckbox.Style = kFBCheckbox;
	m_FollowCameraSwitcherCheckbox.Caption = "Track Camera Switcher";
	m_FollowCameraSwitcherCheckbox.State = CHECKBOX_UNCHECKED;
	m_FollowCameraSwitcherCheckbox.OnClick.Add(this, (FBCallback) &RemoteConsolePlugin::OnFollowCameraSwitcherClick);
	m_FollowCameraSwitcherCheckbox.Enabled = false;

	m_EditModeCheckbox.Style = kFBCheckbox;
	m_EditModeCheckbox.Caption = "Story Edit Mode";
	m_EditModeCheckbox.State = CHECKBOX_CHECKED;
	m_EditModeCheckbox.OnClick.Add(this, (FBCallback) &RemoteConsolePlugin::OnEditModeClick);
	m_EditModeCheckbox.Enabled = false;

	m_dofStateLabel.Caption = "DOF State:";
	m_dofStateLabel.Justify = kFBTextJustifyRight;
	m_dofStateComboBox.Style = kFBDropDownList;
	m_dofStateComboBox.Enabled = false;
	m_dofStateLabel.Enabled = false;

	atArray<atString> dofStates;
	dofStates.PushAndGrow(atString("DOF_DATA_DRIVEN"));
	dofStates.PushAndGrow(atString("DOF_FORCED_ON (Preview Only)"));
	dofStates.PushAndGrow(atString("DOF_FORCED_OFF (Preview Only)"));

	for(int i=0; i < dofStates.GetCount(); ++i)
	{
		m_dofStateComboBox.Items.Add(dofStates[i].c_str());
	}

	m_WCFModeCheckbox.Style = kFBCheckbox;
	//m_WCFModeCheckbox.Justify = kFBTextJustifyRight;
	m_WCFModeCheckbox.Caption = "WCF Mode";
	m_WCFModeCheckbox.State = CHECKBOX_CHECKED;
}

void RemoteConsolePlugin::Connect()
{
	m_ipTextBox.Enabled = false;
	m_ConnectButton.Caption = "Disconnect";

	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		if ( Remote::Connect(m_ipTextBox.Text.AsString()) == false )
		{
			std::string errorStr = "Unable to connect to ";
			errorStr += m_ipTextBox.Text.AsString();
			errorStr += ".  Please try reconnecting the Remote Console.";
			FBMessageBox("Initialization Error", const_cast<char*>(errorStr.c_str()), "OK");

			Disconnect();
			return;
		}

		//After we have initialize all of these widgets, set update watches
		//on both of these banks.  RAG is currently not stable if you put a 
		//watch on a widget where it may change underneath.
		Remote::AddBankReferenceByPath(CutsceneBank);
	}
	else
	{
		if ( m_RemoteConsole.Connect(m_ipTextBox.Text.AsString()) == false )
		{
			std::string errorStr = "Unable to connect to ";
			errorStr += m_ipTextBox.Text.AsString();
			errorStr += ".  Please try reconnecting the Remote Console.";
			FBMessageBox("Initialization Error", const_cast<char*>(errorStr.c_str()), "OK");

			Disconnect();
			return;
		}

		//After we have initialize all of these widgets, set update watches
		//on both of these banks.  RAG is currently not stable if you put a 
		//watch on a widget where it may change underneath.
		m_RemoteConsole.AddUpdates(CutsceneBank);
	}

	m_ReloadCutsceneData = true;
	m_InitializeCutscene = true;

	m_CutsceneState = GetCutsceneState();

	if(IsCutsceneActive())
	{
		m_PlayCutsceneButton.Caption = "Stop Cutscene";
		m_InitializeCutscene = false;
		m_IsPlayingCutscene = true;
	}
	
	EventSelectionChanged(NULL, NULL);

	atString platformName;

	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		platformName = (Remote::GetPlatform() ? Remote::GetPlatform() : "");
	}
	else
	{
		m_RemoteConsole.GetPlatform(platformName);
	}

	if (stricmp(platformName, "Xbox360") == 0 
		|| stricmp(platformName, "PlayStation3") == 0)
	{
		RemoteConsole::SwapEndianness(true);
	}
	else
	{
		RemoteConsole::SwapEndianness(false);
	}

	m_IsConnected = true;
}

void RemoteConsolePlugin::Disconnect()
{	
	//ResetOverrides();
	m_ipTextBox.Enabled = true;
	m_ConnectButton.Caption = "Connect";
	m_IsConnected = false;
	m_InitializeCutscene = false;

	m_PlayCutsceneButton.Enabled = false;
	m_PlayCaptureCutsceneButton.Enabled = false;
	m_SendCutsceneDataCheckbox.Enabled = false;
	m_FollowCameraSwitcherCheckbox.Enabled = false;
	m_EditModeCheckbox.Enabled = false;
	m_dofStateComboBox.Enabled = false;
	m_dofStateLabel.Enabled = false;

	ResetSelection(true);

	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		Remote::Disconnect();
	}
	else
	{
		m_RemoteConsole.RemoveUpdates(CutsceneBank);
		m_RemoteConsole.Disconnect();
	}
}

void RemoteConsolePlugin::FlushScene()
{
	/*if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		Remote::WriteBoolWidgetByPath(PreviewFolderEnable, true);
		Remote::PressButtonByPath(PreviewFolderFlush);
		Remote::SendSyncCommand();
	}
	else*/
	{
		m_RemoteConsole.WriteBoolWidget(PreviewFolderEnable, true);
		m_RemoteConsole.PressWidgetButton(PreviewFolderFlush);
		m_RemoteConsole.SendSyncCommand();
	}
	
}

bool RemoteConsolePlugin::IsDataValid()
{
	return m_CutsceneExportData.GetCutsceneFile().GetConcatDataList().GetCount() > 0;
}

void RemoteConsolePlugin::PlayCutscene()
{
	if(!IsDataValid())
	{
		FBMessageBox("Play Cutscene Error", "Cutscene was unable to be played because the MotionBuilder data is not valid. Open the Cutscene Exporter to load this data, or re-export the scene if it is new.", "OK");
		return;
	}

	const char* sceneName = m_CutsceneExportData.GetSceneName();
	if ( sceneName != NULL && strlen(sceneName) != 0)
	{
		atString sceneNameStr(sceneName);
		if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
		{
			Remote::WriteStringWidgetByPath(CutsceneSceneList, sceneNameStr);
			Remote::SendSyncCommand();
			Remote::PressButtonByPath(CutsceneSceneStartButton);
		}
		else
		{
			m_RemoteConsole.WriteStringWidget(CutsceneSceneList, sceneNameStr);
			m_RemoteConsole.SendSyncCommand();
			m_RemoteConsole.PressWidgetButton(CutsceneSceneStartButton);
		}

		m_PlayCutsceneButton.Caption = "Stop Cutscene";
		m_InitializeCutscene = true;
	}
	else
	{
		FBMessageBox("Play Cutscene Error", "Cutscene was unable to be played because no scene name has been specified.", "OK");
	}
}

void RemoteConsolePlugin::StopCutscene()
{
	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		Remote::PressButtonByPath(CutsceneStopSelectedCutScene);
	}
	else
	{
		m_RemoteConsole.PressWidgetButton(CutsceneStopSelectedCutScene);
	}

	m_PlayCutsceneButton.Caption = "Play Cutscene";
	m_IsPlayingCutscene = false;
	m_InitializeCutscene = false;
}

void RemoteConsolePlugin::OnConnectButtonClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	bool bConnected = false;
	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		bConnected = Remote::IsConnected();
	}
	else
	{
		bConnected = m_RemoteConsole.IsConnected();
	}

	if(bConnected == true)
	{
		if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
		{
			m_RemoteConsole.RemoveUpdates(CutsceneBank);
		}

		Disconnect();
	}
	else
	{
		Connect();

		m_CurrentGameFrame = -1;
		m_CurrentSceneFrame = -1;
		m_CurrentFrameWithinPart = -1;
		m_LastFrame = -1;
		m_PlayCutsceneButton.Enabled = true;
		m_PlayCaptureCutsceneButton.Enabled = true;
		m_SendCutsceneDataCheckbox.Enabled = true;
		m_FollowCameraSwitcherCheckbox.Enabled = true;
		m_EditModeCheckbox.Enabled = true;
		m_dofStateComboBox.Enabled = true;
		m_dofStateLabel.Enabled = true;

		//NOTE: The CutsceneBankToggle will toggle the bank on and off by convention of the runtime system.
		//The remote console requires the bank to be on and needs to avoid inadvertently toggling it off.
		//Without changing the runtime, test for a widget that the cutscene bank will have if it exists.  
		//If that widget does not exist, then turn on the bank.
		if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
		{
			if ( Remote::WidgetExistsByPath(CutsceneSceneList) == false )
			{
				Remote::PressButtonByPath(CutsceneBankToggle);
			}

			Remote::SendSyncCommand();
		}
		else
		{
			if ( m_RemoteConsole.WidgetExists(CutsceneSceneList) == false )
			{
				m_RemoteConsole.PressWidgetButton(CutsceneBankToggle);
			}

			m_RemoteConsole.SendSyncCommand();
		}
		
		//Initialize all widget ids to be used in the Remote Console.
		m_WidgetIds[CutsceneCurrentFrameWidgetName] = m_RemoteConsole.GetWidgetId(CutsceneCurrentFrameWidgetName);
		m_WidgetIds[CutsceneDataWidgetName] = m_RemoteConsole.GetWidgetId(CutsceneDataWidgetName);
		m_WidgetIds[CameraEditingDataWidgetName] = m_RemoteConsole.GetWidgetId(CameraEditingDataWidgetName);
		m_WidgetIds[CameraOverrideUsingMatrixWidgetName] = m_RemoteConsole.GetWidgetId(CameraOverrideUsingMatrixWidgetName);
		m_WidgetIds[CameraOverridesWidgetName] = m_RemoteConsole.GetWidgetId(CameraOverridesWidgetName);
		m_WidgetIds[CameraMatrixWidgetName] = m_RemoteConsole.GetWidgetId(CameraMatrixWidgetName);
		m_WidgetIds[CameraDOFNearOutOfFocusWidgetName] = m_RemoteConsole.GetWidgetId(CameraDOFNearOutOfFocusWidgetName);
		m_WidgetIds[CameraDOFNearInFocusWidgetName] = m_RemoteConsole.GetWidgetId(CameraDOFNearInFocusWidgetName);
		m_WidgetIds[CameraDOFFarInFocusWidgetName] = m_RemoteConsole.GetWidgetId(CameraDOFFarInFocusWidgetName);
		m_WidgetIds[CameraShallowDofWidgetName] = m_RemoteConsole.GetWidgetId(CameraShallowDofWidgetName);
		m_WidgetIds[CameraSimpleDofWidgetName] = m_RemoteConsole.GetWidgetId(CameraSimpleDofWidgetName);
		m_WidgetIds[CameraMotionBlurWidgetName] = m_RemoteConsole.GetWidgetId(CameraMotionBlurWidgetName);
		m_WidgetIds[CameraCircleOfConfusionWidgetName] = m_RemoteConsole.GetWidgetId(CameraCircleOfConfusionWidgetName);

		m_WidgetIds[ParticleSelectorWidgetName] = m_RemoteConsole.GetWidgetId(ParticleSelectorWidgetName);
		m_WidgetIds[ParticleOverrideWidgetName] = m_RemoteConsole.GetWidgetId(ParticleOverrideWidgetName);
		m_WidgetIds[ParticlePositionWidgetName] = m_RemoteConsole.GetWidgetId(ParticlePositionWidgetName);
		m_WidgetIds[ParticleRotationWidgetName] = m_RemoteConsole.GetWidgetId(ParticleRotationWidgetName);

		m_WidgetIds[LightSelectorWidgetName] = m_RemoteConsole.GetWidgetId(LightSelectorWidgetName);
		m_WidgetIds[LightDebugDisplayWidgetName] = m_RemoteConsole.GetWidgetId(LightDebugDisplayWidgetName);
		m_WidgetIds[LightIntensityOverrideWidgetName] = m_RemoteConsole.GetWidgetId(LightIntensityOverrideWidgetName);
		m_WidgetIds[LightIntensityWidgetName] = m_RemoteConsole.GetWidgetId(LightIntensityWidgetName);
		m_WidgetIds[LightColourOverrideWidgetName] = m_RemoteConsole.GetWidgetId(LightColourOverrideWidgetName);
		m_WidgetIds[LightColourOverrideWidgetName] = m_RemoteConsole.GetWidgetId(LightColourOverrideWidgetName);
		m_WidgetIds[LightColourWidgetName] = m_RemoteConsole.GetWidgetId(LightColourWidgetName);
		m_WidgetIds[LightAngleOverrideWidgetName] = m_RemoteConsole.GetWidgetId(LightAngleOverrideWidgetName);
		m_WidgetIds[LightAngleOverrideWidgetName] = m_RemoteConsole.GetWidgetId(LightAngleOverrideWidgetName);
		m_WidgetIds[LightAngleWidgetName] = m_RemoteConsole.GetWidgetId(LightAngleWidgetName);
		m_WidgetIds[LightFalloffOverrideWidgetName] = m_RemoteConsole.GetWidgetId(LightFalloffOverrideWidgetName);
		m_WidgetIds[LightFalloffWidgetName] = m_RemoteConsole.GetWidgetId(LightFalloffWidgetName);
		m_WidgetIds[LightPositionOverrideWidgetName] = m_RemoteConsole.GetWidgetId(LightPositionOverrideWidgetName);
		m_WidgetIds[LightPositionWidgetName] = m_RemoteConsole.GetWidgetId(LightPositionWidgetName);
		m_WidgetIds[LightDirectionOverrideWidgetName] = m_RemoteConsole.GetWidgetId(LightDirectionOverrideWidgetName);
		m_WidgetIds[LightDirectionWidgetName] = m_RemoteConsole.GetWidgetId(LightDirectionWidgetName);
	}
}

void RemoteConsolePlugin::OnPlayCutsceneButtonClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if ( m_IsPlayingCutscene == false )
	{
		PlayCutscene();
	}
	else
	{
		StopCutscene();	
	}
}

void RemoteConsolePlugin::OnPlayCaptureCutsceneButtonClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	m_Capture = !m_Capture;
	m_InitializeCapture = m_Capture;

	if(!m_Capture)
	{
		m_PlayCaptureCutsceneButton.Caption = "Start Capture";
	}
	else
	{
		if ( m_RemoteConsole.WidgetExists(RendererWidgetName) == true )
		{
			//Then we need to press this button to create these widgets.
			m_RemoteConsole.PressWidgetButton(RendererWidgetName);

			if ( m_RemoteConsole.WidgetExistsCallback(RendererPostFXWidgetName, m_WidgetExistsTimeout) == false )
			{
				std::string errorStr = "Unable to initialize the widget \"";
				errorStr += RendererWidgetName;
				errorStr += "\".  Please try reconnecting the Remote Console.";
				FBMessageBox("Initialization Error", const_cast<char*>(errorStr.c_str()), "OK");

				Disconnect();
				return;
			}
		}

		if ( m_RemoteConsole.WidgetExists(RendererPostFXWidgetName) == true )
		{
			m_RemoteConsole.SendSyncCommand();
			m_RemoteConsole.PressWidgetButton(RendererPostFXWidgetName);
		}

		if ( m_RemoteConsole.WidgetExistsCallback(RendererPostFXFrameCaptureWidgetName, m_WidgetExistsTimeout) == false )
		{
			std::string errorStr = "Unable to initialize the widget \"";
			errorStr += RendererPostFXWidgetName;
			errorStr += "\".  Please try reconnecting the Remote Console.";
			FBMessageBox("Initialization Error", const_cast<char*>(errorStr.c_str()), "OK");

			Disconnect();
			return;
		}
		
		//This path is hardcoded in the runtime, but requires us to create the directory
		//for files to be created in it.
		const char* frameCaptureDirectory = "X:\\framecap";
		CreateDirectory(frameCaptureDirectory, NULL);

		m_WidgetIds[RendererPostFXFrameCaptureWidgetName] = m_RemoteConsole.GetWidgetId(RendererPostFXFrameCaptureWidgetName);
	}
}

void RemoteConsolePlugin::OnUseGameCutsceneDataClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if ( m_SendCutsceneDataCheckbox.State == CHECKBOX_CHECKED )
	{	
		m_SendCutsceneData = true;

		if ( m_IsPlayingCutscene == true )
		{
			if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
			{
				Remote::PressVCRButtonByPath(CutscenePlaybackWidgetName, Remote::Pause);
			}
			else
			{
				m_RemoteConsole.SendVCRCommand(CutscenePlaybackWidgetName, RemoteConsole::VCR_PAUSE);
			}
		}
	}
	else
	{
		//ResetOverrides();
		m_SendCutsceneData = false;

		if ( m_IsPlayingCutscene == true )
		{
			if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
			{
				Remote::PressVCRButtonByPath(CutscenePlaybackWidgetName, Remote::PlayForwards);
			}
			else
			{
				m_RemoteConsole.SendVCRCommand(CutscenePlaybackWidgetName, RemoteConsole::VCR_PLAYFORWARDS);
			}
		}
	}
}

void RemoteConsolePlugin::OnFollowCameraSwitcherClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if ( m_FollowCameraSwitcherCheckbox.State == CHECKBOX_CHECKED )
	{	
		m_FollowCameraSwitcher = true;

		m_CameraSwitcherAnimNode = NULL;
		HFBModel pCameraSwitcher = RAGEFindModelByName( "Camera Switcher" );
		if ( pCameraSwitcher )
		{
			HFBAnimationNode pSwitcherAnimNode = pCameraSwitcher->AnimationNodeInGet();
			if ( pSwitcherAnimNode )
			{
				HFBAnimationNode pAnimNode = rexMBAnimExportCommon::FindAnimByName( pSwitcherAnimNode, "Camera Index" );
				if ( pAnimNode )
				{
					m_CameraSwitcherAnimNode = pAnimNode;
				}
			}
		}
	}
	else
	{
		m_FollowCameraSwitcher = false;
		if (m_IsConnected == true)
			ResetSelection(true); 
	}
}

void RemoteConsolePlugin::OnEditModeClick(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if ( m_EditModeCheckbox.State == CHECKBOX_CHECKED )
	{	
		m_EditMode = true;
	}
	else
	{
		m_EditMode = false;
	}
}

void RemoteConsolePlugin::ReadCutsceneData()
{
	m_CutsceneState = GetCutsceneState();
	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		m_CurrentGameFrame = Remote::ReadIntWidgetByPath(CutsceneCurrentFrameWidgetName);
	}
	else
	{
		m_CurrentGameFrame = m_RemoteConsole.ReadIntWidgetStream(m_WidgetIds[CutsceneCurrentFrameWidgetName]);
	}
}

void RemoteConsolePlugin::EventToolIdle( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if ( m_ReloadCutsceneData == true )
	{
		if ( m_SendCutsceneData == true )
		{
			LoadCutsceneExportData();
			UpdateCutsceneExportData();
			m_ReloadCutsceneData = false;
		}
	}

	if ( m_StopCutscene == true )
	{
		if ( m_IsPlayingCutscene == true )
			StopCutscene();

		m_StopCutscene = false;
	}
	else if ( m_StartCutscene == true )
	{
		if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
		{
			Remote::WriteBoolWidgetByPath(PreviewFolderEnable, true);
		}
		else
		{
			m_RemoteConsole.WriteBoolWidget(PreviewFolderEnable, true);
		}
		
		if ( m_IsPlayingCutscene == false )
		{
			PlayCutscene();
			m_StartCutscene = false;
			m_SendingOverridden = true;

			SetDataSending(false);
		}
	}

	//if ( m_IsConnected == true )
	{
		bool isConnected  = false;
		if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
		{
			isConnected = Remote::IsConnected();
		}
		else
		{
			isConnected = m_RemoteConsole.IsConnected();
		}
		
		if ( isConnected == false)
		{
			//The connection has been lost; notify the user interface.
			Disconnect();
		}
		else
		{
			m_CurrentSceneFrame = GetCurrentFrame();
			m_CurrentFrameWithinPart = GetGameFrameFromMotionbuilderFrame(m_CurrentSceneFrame);
			ReadCutsceneData();
			//Update the playback widget.
			if ( m_InitializeCutscene )
			{
				//Update the playback widget.
				if ( m_IsPlayingCutscene == false && IsCutsceneActive() == true )
				{
					m_ReloadCutsceneData = true;
					EventSelectionChanged(NULL, NULL);

					if ( m_CurrentGameFrame > 0 )
					{
						if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
						{
							Remote::WriteIntWidgetByPath(CutsceneCurrentFrameWidgetName, 0);
						}
						else
						{
							m_RemoteConsole.WriteIntWidgetStream(m_WidgetIds[CutsceneCurrentFrameWidgetName], 0);
						}

						m_InitializeCutscene = false;
						m_IsPlayingCutscene = true;
					}
				}
			}

			if (m_IsPlayingCutscene && m_InitializeCapture && m_Capture)
			{
				//If the MotionBuilder scene is not in range, ask the user if they'd like to set it.
				if(!WithinRange(m_CurrentSceneFrame))
				{
					int returnValue = FBMessageBox("Capture Cutscene", "Would you like to set the MotionBuilder scene to the first frame of the cutscene?  If \"No\" is selected, users will have to set the MotionBuilder scene manually to be in range for capturing to begin.", "Yes", "No");
					if ( returnValue == 1)
					{
						//Sync up to the same frame in MotionBuilder if capture is on.
						s32 sceneFrame = GetSceneFrame(0);
						FBPlayerControl playerControl;
						double deltaTime = 1.0 / (double)rexMBAnimExportCommon::GetFPS();
						double t = deltaTime * (double)(sceneFrame);
						FBTime fbt;
						fbt.SetSecondDouble( t );
						playerControl.Goto( fbt );
					}

					//Sync the game with the current frame in MotionBuilder.
					m_Capture = false;
					SendCurrentFrame();
					m_Capture = true;
				}

				m_CurrentGameFrame = -1;
				m_LastFrame = -1;
				m_InitializeCapture = false;
			}

			if ( m_IsPlayingCutscene )
			{
				if (m_CutsceneState == cutsManager::CUTSCENE_IDLE_STATE)
				{
					//If the cutscene has already completed between the time we could communicate
					//with the game properly.  Occurs on very short cutscenes.
					m_PlayCutsceneButton.Caption = "Play Cutscene";
					m_InitializeCutscene = false;
					m_IsPlayingCutscene = false;
					return;
				}

				if ( m_SendCutsceneData == true )
				{
					if(WithinRange(m_CurrentSceneFrame))
					{
						bool capture = m_Capture; //Cache this variable so users can't stop capturing mid-frame.

						int gameFrame = SendCurrentFrame();
						
						ReadSwitcher();

						OnSelectionChange(true); //Reselect the entities.

						SendCameraData();
						SendLightData();
						SendParticleData();

						if(capture)
						{
							s32 gameCurrentFrame = 0;
							if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
							{
								gameCurrentFrame = Remote::ReadIntWidgetById(m_WidgetIds[CutsceneCurrentFrameWidgetName]);
							}
							else
							{
								gameCurrentFrame = m_RemoteConsole.ReadIntWidgetStream(m_WidgetIds[CutsceneCurrentFrameWidgetName]);
							}
							
							//if (gameCurrentFrame != gameFrame) // Throtteling only seems to happen on a join because the game skips a frame due to the blocking tag on 
							// the join.
							//{
							//	//The game hasn't been updated yet.  Reset the time; don't do anything.
							//	return;
							//}

							//Compute the next frame to move MotionBuilder to.
							s32 sceneFrame = GetSceneFrame(gameFrame+1);
							FBPlayerControl playerControl;
							double deltaTime = 1.0 / (double)rexMBAnimExportCommon::GetFPS();
							double t = deltaTime * (double)(sceneFrame);
							FBTime fbt;
							fbt.SetSecondDouble( t );
							playerControl.Goto( fbt );
						}
					}
				}
			}
		}
	}
}

void RemoteConsolePlugin::OnFileOpenCompleted( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	m_SelectedModel = NULL;

	//If a cutscene is played, stop it when a new file has been opened.
	if (m_IsPlayingCutscene == true)
	{
		OnPlayCutsceneButtonClick(NULL, NULL);
	}

	m_ReloadCutsceneData = true;
}

void RemoteConsolePlugin::EventSceneChange( HISender pSender, HKEvent pEvent )
{
	FBEventSceneChange lEvent( pEvent );

	switch( lEvent.Type )
	{
	case kFBSceneChangeSelect:
		{
			EventSelectionChanged(pSender, pEvent);
		}
		break;

	default:
		break;
	}

	m_Scene = m_System.Scene;
}

void RemoteConsolePlugin::EventSelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ ) 
{
	OnSelectionChange(false);
}

void RemoteConsolePlugin::ReadSwitcher()
{
	if(!m_FollowCameraSwitcher) return;

	if ( m_CameraSwitcherAnimNode )
	{
		FBSystem system;
		for ( int i = 0; i < m_CameraSwitcherAnimNode->FCurve->Keys.GetCount(); ++i )
		{
			FBTime startTime = (FBTime)m_CameraSwitcherAnimNode->FCurve->Keys[i].Time;
			float fStartTime = (float)startTime.GetSecondDouble();            

			int iCameraIndex = (int)m_CameraSwitcherAnimNode->FCurve->Keys[i].Value;
			HFBCamera pCamera = system.Scene->Cameras[iCameraIndex + 6];    // add 6 to skip past the built-in cameras
			if(pCamera)
			{
				if(i+1 < m_CameraSwitcherAnimNode->FCurve->Keys.GetCount())
				{
					FBTime timeCompare = (FBTime)m_CameraSwitcherAnimNode->FCurve->Keys[i+1].Time;
					float fTimeCompare = (float)timeCompare.GetSecondDouble();
					int frameCompare = (int)(fTimeCompare*CUTSCENE_FPS);
					int startFrame = (int)(fStartTime*CUTSCENE_FPS);

					if(m_CurrentSceneFrame >= startFrame && m_CurrentSceneFrame < frameCompare)
					{
						if ( pCamera->Selected == false )
						{
							rexMBAnimExportCommon::DeselectAllModels();
							pCamera->Selected = true;
						}

						return;
					}
				}
				else
				{
					if ( pCamera->Selected == false )
					{
						rexMBAnimExportCommon::DeselectAllModels();
						pCamera->Selected = true;
					}
					return;
				}
			}
		}
	}
}

void RemoteConsolePlugin::OnSelectionChange(bool /* force */)
{
	if ( m_IsPlayingCutscene == false)
	{
		return;	
	}

	FBModelList		selectedModels;
	FBGetSelectedModels( selectedModels, NULL, true );

	int lSelectedCount = selectedModels.GetCount();
	if(lSelectedCount == 0)
	{
		return;
	}
	else if(lSelectedCount > 1)
	{
		ResetSelection(false);
		return;
	}

	HFBModel previousModel = m_SelectedModel;
	m_SelectedModel = selectedModels[0];

	if ( previousModel == m_SelectedModel )
	{
		return;
	}

	EventFrameChange();  //Simulate a frame change, which acquires all the current information of that entity on the frame.

	if( m_SelectedModel->Is(FBCamera::TypeInfo))
	{
		// Camera type
	}
	else if(atString(m_SelectedModel->LongName.AsString()).EndsWith(":effect"))
	{
		atString strParticleName(m_SelectedModel->LongName.AsString());
		strParticleName.Set(strParticleName, 0, (strParticleName.IndexOf(":")) ? strParticleName.IndexOf(":") : strParticleName.GetLength());

		//This particle is currently in the cutscene.
		if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
		{
			Remote::WriteStringWidgetByPath(ParticleSelectorWidgetName, strParticleName.c_str());
			Remote::SendSyncCommand();
		}
		else
		{
			m_RemoteConsole.WriteStringWidget(ParticleSelectorWidgetName, strParticleName.c_str());
			m_RemoteConsole.SendSyncCommand();
		}
	}
	else if ( m_SelectedModel->Is(FBLight::TypeInfo) )
	{
		//Select the actual light.
		char lightName[CUTSCENE_OBJNAMELEN] = {0};
		strcpy_s(lightName, CUTSCENE_OBJNAMELEN, m_SelectedModel->Name.AsString());

		char safeLightName[CUTSCENE_OBJNAMELEN];
		strcpy_s(safeLightName, CUTSCENE_OBJNAMELEN, lightName);
		
		size_t index = 0;
		size_t length = strlen(safeLightName);
		while(index < length) { if(safeLightName[index] == ' ') safeLightName[index] = '-'; index++; }  //Convert any spaces into hyphens

		char command[MAX_COMMAND_LENGTH];
		sprintf_s(command, "%s %s", CutsceneLightIdCommand, safeLightName);

		atString result;
		if ( m_RemoteConsole.SendCommand(command, result) == true )
		{
			int lightId = 0;
			sscanf(result, "%d", &lightId);
			if(lightId > 0)
			{
				if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
				{
					Remote::WriteBoolWidgetById(m_WidgetIds[LightDebugDisplayWidgetName], true);
					Remote::WriteStringWidgetById(m_WidgetIds[LightSelectorWidgetName], lightName);
					Remote::SendSyncCommand();
				}
				else
				{
					//Enable the debug display.
					m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[LightDebugDisplayWidgetName], true);

					//This light is currently in the cutscene.
					m_RemoteConsole.WriteStringWidgetStream(m_WidgetIds[LightSelectorWidgetName], lightName);
					m_RemoteConsole.SendSyncCommand();
				}

				//Perform any validations.
				HFBLight selectedLight = (HFBLight) m_SelectedModel;
				float falloff = GetFalloff(selectedLight);
				if ( falloff == -1.0f )
				{
					//The falloff custom property does not exist.
					FBMessageBox("Property Warning", "The selected light does not have a custom property named \"Fall Off\" and will not appear in-game until one is created and its value is greater than 0.", "OK" );
				}
				else if ( falloff == 0.0f )
				{
					FBMessageBox("Property Warning", "The selected light's custom \"Fall Off\" property has a value of 0 and will not appear in-game until it is made non-zero.", "OK" );
				}
			}
			else
			{
				//The object is new.  Reload the cutscene.
				//
				ResetSelection(true);
			}
		}
	}
	else
	{
		m_SelectedModel = previousModel;
		ResetSelection(false);
	}
}

void RemoteConsolePlugin::ResetSelection(bool force)
{
	if ( m_SelectedModel != NULL || force)
	{
		rexMBAnimExportCommon::DeselectAllModels();
		m_SelectedModel = NULL;

		//Flush all data before sending these commands
		if(m_WCFModeCheckbox.State != CHECKBOX_CHECKED)
		{
			m_RemoteConsole.SendSyncCommand();
			m_RemoteConsole.WriteStringWidgetStream(m_WidgetIds[LightSelectorWidgetName], LightSelectorNone);
			m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[LightDebugDisplayWidgetName], false);
			m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[CameraOverrideUsingMatrixWidgetName], false);
			m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[CameraOverridesWidgetName], false);
		}
	}
}

// Acquires the current frame in the scene.
int RemoteConsolePlugin::GetCurrentFrame()
{
	// Motionbuilder action timeline frame number
	const char* timeStr = m_System.LocalTime.AsString();
	int frame;
	sscanf(timeStr, "%d", &frame);
	return frame;
}

// Get the current frame from within the scene, this calculates the frame with all its discarded frame ranges removed (including prior parts).
int RemoteConsolePlugin::GetCurrentGameFrame()
{
	// Acquire the current frame in the MotionBuilder scene.
	return GetGameFrameFromMotionbuilderFrame(m_CurrentSceneFrame);
}

bool RemoteConsolePlugin::WithinRange(s32 frame)
{
	if(m_EditMode)
	{
		return WithinEST(frame);
	}
	
	atString name;
	return WithinShot(frame, name);
}

bool RemoteConsolePlugin::WithinShot(s32 frame, atString& name)
{
	for(int i=0; i < m_CutsceneExportData.GetCutsceneFile().GetConcatDataList().GetCount(); ++i)
	{
		if(frame >= m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeStart &&
			frame <= m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeEnd)
		{
			name = m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].cSceneName.GetCStr();
			return true;
		}
	}

	return false;
}

bool RemoteConsolePlugin::WithinEST(s32 frame)
{
	atString dummy;
	return WithinEST(frame, dummy);
}

bool RemoteConsolePlugin::WithinEST(s32 frame, atString& name)
{
	//rexMBAnimExportCommon::SetBaseLayerOnAllTakes();

	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			// Validate that the shot track we find is actually within the exporter
			// TODO

			// Iterate all the clips in each track to see if the frame exists within one of the clips.
			for (int j = 0; j < lTrack->Clips.GetCount(); ++j)
			{
				HFBStoryClip lClip = lTrack->Clips[j];

				int iFirstLoop = atoi(lClip->PropertyList.Find("FirstLoopMarkIn")->AsString());
				int iLastLoop = atoi(lClip->PropertyList.Find("LastLoopMarkOut")->AsString());

				if(frame >= iFirstLoop && frame <= iLastLoop)
				{
					name = (const char*)lTrack->LongName;
					return true;
				}
			}
		}
	}

	return false;
}

// Get the current frame from within the part, this calculates the frame with its discarded frame ranges removed.
int RemoteConsolePlugin::GetGameFrameFromMotionbuilderFrame(s32 frame)
{	
	if(m_EditMode)// If in story mode
	{
		atString name("");
		if(WithinEST(frame, name))
		{
			// We work in shot space so we don't worry about any other shots around us
			int iShotStart = 0;
			int iGameStart = 0;
			atString atCurrentShot("");

			for(int i=0; i < m_CutsceneExportData.GetCutsceneFile().GetConcatDataList().GetCount(); ++i)
			{
				char cName[RAGE_MAX_PATH];
				sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].cSceneName.GetCStr());

				if(strcmpi(cName, name.c_str()) == 0)
				{
					atCurrentShot = m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].cSceneName.GetCStr();
					iShotStart = m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeStart;
					break;
				}

				iGameStart += ((m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeEnd + 1) - m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeStart);
			}

			int iDiff = frame - iShotStart;
			int iResult = iGameStart + iDiff;

			rexMBAnimExportCommon::SetActivePart(atCurrentShot);
			rexMBAnimExportCommon::CalculateDiscardedFrames(false);
			atArray<int>& discardedArray = rexMBAnimExportCommon::GetDiscardedFramesArray();

			for(int i=0; i < discardedArray.GetCount(); i+=2)
			{
				if(frame >= discardedArray[i+1])
				{
					iResult -= discardedArray[i+1]-discardedArray[i];
				}
			}

			return iResult;
		}
	}
	else 
	{
		atString name("");
		if(WithinShot(frame, name))
		{
			// We work in shot space so we don't worry about any other shots around us
			int iShotStart = 0;
			int iGameStart = 0;
			atString atCurrentShot("");

			for(int i=0; i < m_CutsceneExportData.GetCutsceneFile().GetConcatDataList().GetCount(); ++i)
			{
				if(strcmpi(m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].cSceneName.GetCStr(), name.c_str()) == 0)
				{
					atCurrentShot = m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].cSceneName.GetCStr();
					iShotStart = m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeStart;
					break;
				}

				iGameStart += ((m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeEnd + 1) - m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeStart);
			}

			int iDiff = frame - iShotStart;
			int iResult = iGameStart + iDiff;

			return iResult;
		}
	}

	return -1;
}

// Acquires the frame in MotionBuilder that corresponds to a game frame.
int RemoteConsolePlugin::GetSceneFrame(const int& gameFrame)
{
	int currentFrame = gameFrame;

	currentFrame += m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[0].iRangeStart;

	if(m_EditMode)// If in story mode
	{
		rexMBAnimExportCommon::SetActivePart(atString(""));
		for(int i=0; i < m_CutsceneExportData.GetCutsceneFile().GetConcatDataList().GetCount(); ++i)
		{
			rexMBAnimExportCommon::SetActivePart(atString(m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].cSceneName.GetCStr()));

			rexMBAnimExportCommon::CalculateDiscardedFrames(false);
			atArray<int>& discardedArray = rexMBAnimExportCommon::GetDiscardedFramesArray();

			if(rexMBAnimExportCommon::DiscardFrame(currentFrame))
			{
				for(int i=0; i < discardedArray.GetCount(); i+=2)
				{
					if(currentFrame >= discardedArray[i] && currentFrame < discardedArray[i+1]) // discard if frame is within a discard range
					{
						currentFrame += (discardedArray[i+1] - discardedArray[i]);
					}
				}
			}
		}
	}

	return currentFrame;
}

void RemoteConsolePlugin::EventFrameChange()
{
	//Handle any events for changing the current frame.
	if ( m_SendingOverridden == true )
	{
		m_SendingOverridden = false;
		SetDataSending(true);
	}
}

void RemoteConsolePlugin::SendCameraData()
{
	if ( m_SelectedModel == NULL || m_SelectedModel->Is(FBCamera::TypeInfo) == false)
	{
		return;
	}

	//// SET DOF MODE

	m_RemoteConsole.WriteStringWidget(CameraDepthOfFieldStateWidgetName, m_dofStateComboBox.Items[m_dofStateComboBox.ItemIndex]);

	//////
	
	m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[CameraOverrideUsingMatrixWidgetName], true);
	m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[CameraOverridesWidgetName], true);

	HFBCamera selectedCamera = (HFBCamera) m_SelectedModel;

	//Create the data buffer.
	datGrowBuffer growBuffer;
	growBuffer.Init(NULL, 0);
	growBuffer.Preallocate(1024);

	bool overrideValue = true;  //Since we're sending the camera data, we're always going to override these widgets.
	RemoteConsole::WriteStream(overrideValue, growBuffer); //CameraOverrideUsingMatrixWidgetName
	RemoteConsole::WriteStream(overrideValue, growBuffer); //CameraOverridesWidgetName

	bool shallowDOFStrength = false; //GetShallowDOFStrength(selectedCamera);
	RemoteConsole::WriteStream(shallowDOFStrength, growBuffer);  //CameraShallowDofWidgetName

	bool simpleDOFStrength = false; //GetSimpleDOFStrength(selectedCamera);
	RemoteConsole::WriteStream(simpleDOFStrength, growBuffer);  //CameraSimpleDofWidgetName

	Matrix34 cameraMatrix = GetMatrix(selectedCamera);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(0, 0), growBuffer);//CameraMatrixWidgetHash
	RemoteConsole::WriteStream(cameraMatrix.GetElement(0, 1), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(0, 2), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(1, 0), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(1, 1), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(1, 2), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(2, 0), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(2, 1), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(2, 2), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(3, 0), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(3, 1), growBuffer);
	RemoteConsole::WriteStream(cameraMatrix.GetElement(3, 2), growBuffer);

	float fieldOfView = GetFOV(selectedCamera);
	RemoteConsole::WriteStream(fieldOfView, growBuffer); //CameraMatrixWidgetHash

	Vec4V depthOfFieldVector = GetDOF(selectedCamera);
	float dofValue = depthOfFieldVector.GetX().Getf();
	RemoteConsole::WriteStream(dofValue, growBuffer); //CameraDOFNearOutOfFocusWidgetName

	dofValue = depthOfFieldVector.GetY().Getf();
	RemoteConsole::WriteStream(dofValue, growBuffer); //CameraDOFNearInFocusWidgetName

	dofValue = depthOfFieldVector.GetZ().Getf();
	RemoteConsole::WriteStream(dofValue, growBuffer); //CameraDOFFarInFocusWidgetName

	dofValue = depthOfFieldVector.GetW().Getf();
	RemoteConsole::WriteStream(dofValue, growBuffer); //CameraDOFFarOutOfFocusWidgetName

	float motionBlur = GetMotionBlur(selectedCamera);
	RemoteConsole::WriteStream(motionBlur, growBuffer);  //CameraMotionBlurWidgetName

	float circleOfConfusion = (float)GetCoC(selectedCamera);
	RemoteConsole::WriteStream(circleOfConfusion, growBuffer);  //CameraCircleOfConfusionWidgetName

	float cameraFocus = (float)GetCameraFocus(selectedCamera);
	RemoteConsole::WriteStream(cameraFocus, growBuffer);  //CameraFocusWidgetName

	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		Remote::WriteDataWidgetByPath(CameraEditingDataWidgetName, growBuffer.GetBuffer(), growBuffer.Length());
	}
	else
	{
		m_RemoteConsole.WriteDataStream(m_WidgetIds[CameraEditingDataWidgetName], growBuffer.GetBuffer(), growBuffer.Length());
	}
}

void RemoteConsolePlugin::SendParticleData()
{
	if ( m_SelectedModel == NULL || atString(m_SelectedModel->LongName.AsString()).EndsWith(":effect") == false)
	{
		return;
	}

	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		Remote::WriteBoolWidgetById(m_WidgetIds[ParticleOverrideWidgetName], true);

		Vec3V positionVector_ = GetPosition(m_SelectedModel);
		Remote::Vector3 positionVector(positionVector_.GetX().Getf(), positionVector_.GetY().Getf(), positionVector_.GetZ().Getf());
		Remote::WriteVector3WidgetById(m_WidgetIds[ParticlePositionWidgetName], positionVector);

		Vec3V rotationVector = GetDirection(m_SelectedModel);
		// Particles widgets expect rotation in degrees...
		Remote::Vector3 rotationVectorDegrees(rotationVector.GetX().Getf() * RtoD, rotationVector.GetY().Getf() * RtoD, rotationVector.GetZ().Getf() * RtoD);
		Remote::WriteVector3WidgetById(m_WidgetIds[ParticleRotationWidgetName], rotationVectorDegrees);
	}
	else
	{
		m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[ParticleOverrideWidgetName], true);

		Vec3V positionVector = GetPosition(m_SelectedModel);
		m_RemoteConsole.WriteVector3WidgetStream(m_WidgetIds[ParticlePositionWidgetName], positionVector);

		Vec3V rotationVector = GetDirection(m_SelectedModel);
		// Particles widgets expect rotation in degrees...
		Vec3V rotationVectorDegrees(rotationVector.GetX().Getf() * RtoD, rotationVector.GetY().Getf() * RtoD, rotationVector.GetZ().Getf() * RtoD);
		m_RemoteConsole.WriteVector3WidgetStream(m_WidgetIds[ParticleRotationWidgetName], rotationVectorDegrees);
	}
}

void RemoteConsolePlugin::SendLightData()
{
	if ( m_SelectedModel == NULL || m_SelectedModel->Is(FBLight::TypeInfo) == false)
	{
		return;
	}

	HFBLight selectedLight = (HFBLight) m_SelectedModel;
	/*const char* lightType = NULL;
	switch( selectedLight->LightType )
	{
	case kFBLightTypePoint:  //CUTSCENE_POINT_LIGHT_TYPE
		lightType = "Point";
		break;

	case kFBLightTypeInfinite:  //CUTSCENE_SPOT_LIGHT_TYPE
		lightType =  "Spot";
		break;

	case kFBLightTypeSpot:  //CUTSCENE_DIRECTIONAL_LIGHT_TYPE
		lightType = "Directional";
		break;
	}

	if ( lightType != NULL )
	{
		m_RemoteConsole.WriteStringWidget(LightTypeWidgetName, lightType);	
	}*/

	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		//Intensity
		float intensity = GetIntensity(selectedLight);

		Remote::WriteBoolWidgetById(m_WidgetIds[LightIntensityOverrideWidgetName], true);
		Remote::WriteFloatWidgetById(m_WidgetIds[LightIntensityWidgetName], intensity);

		FBColor color = selectedLight->DiffuseColor;
		Remote::Vector3 colorVector( (float) color[0], (float) color[1], (float) color[2]);

		Remote::WriteBoolWidgetById(m_WidgetIds[LightColourOverrideWidgetName], true);
		Remote::WriteVector3WidgetById(m_WidgetIds[LightColourWidgetName], colorVector);	

		float angle = (float) selectedLight->ConeAngle;

		Remote::WriteBoolWidgetById(m_WidgetIds[LightAngleOverrideWidgetName], true);
		Remote::WriteFloatWidgetById(m_WidgetIds[LightAngleWidgetName], angle);		

		float falloff = GetFalloff(selectedLight);

		Remote::WriteBoolWidgetById(m_WidgetIds[LightFalloffOverrideWidgetName], true);
		Remote::WriteFloatWidgetById(m_WidgetIds[LightFalloffWidgetName], falloff);

		Vec3V positionVector_ = GetPosition(selectedLight);
		Remote::Vector3 positionVector(positionVector_.GetX().Getf(), positionVector_.GetY().Getf(), positionVector_.GetZ().Getf());

		Remote::WriteBoolWidgetById(m_WidgetIds[LightPositionOverrideWidgetName], true);
		Remote::WriteVector3WidgetById(m_WidgetIds[LightPositionWidgetName], positionVector);

		Vec3V rotationVector_ = GetDirection(selectedLight);
		Remote::Vector3 rotationVector(rotationVector_.GetX().Getf(), rotationVector_.GetY().Getf(), rotationVector_.GetZ().Getf());

		Remote::WriteBoolWidgetById(m_WidgetIds[LightDirectionOverrideWidgetName], true);

		Remote::WriteVector3WidgetById(m_WidgetIds[LightDirectionWidgetName], rotationVector);
	}
	else
	{
		//Intensity
		float intensity = GetIntensity(selectedLight);

		m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[LightIntensityOverrideWidgetName], true);
		m_RemoteConsole.WriteFloatWidgetStream(m_WidgetIds[LightIntensityWidgetName], intensity);

		FBColor color = selectedLight->DiffuseColor;
		Vec3V colorVector( (float) color[0], (float) color[1], (float) color[2]);
	
		m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[LightColourOverrideWidgetName], true);
		m_RemoteConsole.WriteVector3WidgetStream(m_WidgetIds[LightColourWidgetName], colorVector);	

		float angle = (float) selectedLight->ConeAngle;

		m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[LightAngleOverrideWidgetName], true);
		m_RemoteConsole.WriteFloatWidgetStream(m_WidgetIds[LightAngleWidgetName], angle);		

		float falloff = GetFalloff(selectedLight);

		m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[LightFalloffOverrideWidgetName], true);
		m_RemoteConsole.WriteFloatWidgetStream(m_WidgetIds[LightFalloffWidgetName], falloff);

		Vec3V positionVector = GetPosition(selectedLight);

		m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[LightPositionOverrideWidgetName], true);
		m_RemoteConsole.WriteVector3WidgetStream(m_WidgetIds[LightPositionWidgetName], positionVector);

		Vec3V rotationVector = GetDirection(selectedLight);

		m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[LightDirectionOverrideWidgetName], true);

		m_RemoteConsole.WriteVector3WidgetStream(m_WidgetIds[LightDirectionWidgetName], rotationVector);
	}
}

bool RemoteConsolePlugin::IsCurrentFrameInRange()
{
	// Get the current frame within the part. This is not relative to the whole scene as we need to check if its within range.
	for(int i=0; i < m_CutsceneExportData.GetCutsceneFile().GetConcatDataList().GetCount(); ++i)
	{
		// The ranges from the GetConcatDataList are actual ranges and have the discarded frame ranges removed. 
		s32 startFrame = m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeStart;
		s32 endFrame = m_CutsceneExportData.GetCutsceneFile().GetConcatDataList()[i].iRangeEnd;
		if ( m_CurrentFrameWithinPart >= startFrame && m_CurrentFrameWithinPart <= endFrame )
		{
			return true;
		}
	}

	return false;
}

int RemoteConsolePlugin::SendCurrentFrame()
{
	if ( m_SendCutsceneData == false )
		return -1;

	s32 currentFrame = GetCurrentGameFrame();
	s32 gameCurrentFrame = m_CurrentGameFrame;
	
	if(!m_Capture)
	{
		if(gameCurrentFrame == currentFrame) return -1;
	}

	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		Remote::WriteIntWidgetByPath(CutsceneCurrentFrameWidgetName, currentFrame);
	}
	else
	{
		m_RemoteConsole.WriteIntWidgetStream(m_WidgetIds[CutsceneCurrentFrameWidgetName], currentFrame);
	}

	if ( m_LastFrame != currentFrame )
	{
		m_LastFrame = currentFrame;
	}

	if(m_Capture)
	{
		if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
		{
			Remote::WriteBoolWidgetById(m_WidgetIds[RendererPostFXFrameCaptureWidgetName], false);
			Remote::WriteBoolWidgetById(m_WidgetIds[RendererPostFXFrameCaptureWidgetName], true);
			Remote::SendSyncCommand();
		}
		else
		{
			m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[RendererPostFXFrameCaptureWidgetName], false);
			m_RemoteConsole.WriteBoolWidgetStream(m_WidgetIds[RendererPostFXFrameCaptureWidgetName], true);
			m_RemoteConsole.SendSyncCommand();
		}
	}

	EventFrameChange();
	return currentFrame;
}

float RemoteConsolePlugin::GetIntensity(HFBLight selectedLight)
{
	float intensity = (float) selectedLight->Intensity;
	if ( intensity < 0.0f )
	{
		intensity = 0.0f;
	}
	else if (intensity > 100.0f )
	{
		intensity = 100.0f;
	}

	bool visible = (selectedLight->Visibility == 0.0) ? false : true;
	if ( visible == false )
	{
		intensity = 0.0f;
	}

	return intensity;
}

Vec3V_Out RemoteConsolePlugin::GetPosition(HFBModel selectedModel)
{
	FBVector3d position;
	selectedModel->GetVector(position);

	Vector3 positionVector((float) position[0], (float) -position[2], (float) position[1]);
	positionVector.Scale(MOTIONBUILDER_UNIT_SCALE);

	float rotationRadians = DtoR * m_CutsceneExportData.GetCutsceneFile().GetRotation();
	positionVector.RotateZ( rotationRadians );

	Vector3 sceneOffset = m_CutsceneExportData.GetCutsceneFile().GetOffset();
	positionVector += sceneOffset;
	return Vec3V(positionVector.GetX(), positionVector.GetY(), positionVector.GetZ());
}

float RemoteConsolePlugin::GetFOV(HFBCamera selectedCamera)
{
	return (float)selectedCamera->FieldOfView;
}

bool RemoteConsolePlugin::GetShallowDOFStrength(HFBCamera selectedCamera)
{
	HFBProperty pProperty = selectedCamera->PropertyList.Find("Shallow_DOF");
	if ( pProperty != NULL )
	{
		bool bValue;
		pProperty->GetData( &bValue, sizeof(bValue) );

		return bValue;
	}

	return false;
}

bool RemoteConsolePlugin::GetSimpleDOFStrength(HFBCamera selectedCamera)
{
	HFBProperty pProperty = selectedCamera->PropertyList.Find("Simple_DOF");
	if ( pProperty != NULL )
	{
		bool bValue;
		pProperty->GetData( &bValue, sizeof(bValue) );

		return bValue;
	}

	return false;
}

float RemoteConsolePlugin::GetMotionBlur(HFBCamera selectedCamera)
{
	HFBProperty pProperty = selectedCamera->PropertyList.Find("Motion_Blur");
	if ( pProperty != NULL )
	{
		double fValue;
		pProperty->GetData( &fValue, sizeof(fValue) );

		return (float)fValue;
	}

	return 0;
}

int RemoteConsolePlugin::GetCoC(HFBCamera selectedCamera)
{
	HFBProperty pProperty = selectedCamera->PropertyList.Find("CoC");
	if ( pProperty != NULL )
	{
		int iValue;
		pProperty->GetData( &iValue, sizeof(iValue) );

		return iValue;
	}

	return 0;
}

float RemoteConsolePlugin::GetCameraFocus(HFBCamera selectedCamera)
{
	HFBProperty pProperty = selectedCamera->PropertyList.Find("FOCUS (cm)");
	if ( pProperty != NULL )
	{
		double fValue;
		pProperty->GetData( &fValue, sizeof(fValue) );

		return (float)fValue;
	}

	return 0;
}

Vec4V_Out RemoteConsolePlugin::GetDOF(HFBCamera selectedCamera)
{
	char cName[RAGE_MAX_PATH*2];
	safecpy( cName, selectedCamera->Name.AsString() );

	char cNearPlaneName[RAGE_MAX_PATH*4];
	sprintf( cNearPlaneName, "%s NearPlane", cName );

	char cFarPlaneName[RAGE_MAX_PATH*4];
	sprintf( cFarPlaneName, "%s FarPlane", cName );

	char cNearDOFStrengthPlaneName[1024];
	sprintf( cNearDOFStrengthPlaneName, "%s DOF_Strength_NearPlane", cName );

	char cFarDOFStrengthPlaneName[1024];
	sprintf( cFarDOFStrengthPlaneName, "%s DOF_Strength_FarPlane", cName );

	HFBModel pNearInFocusPlaneModel = RAGEFindModelByName( cNearPlaneName );
	HFBModel pFarInFocusPlaneModel = RAGEFindModelByName( cFarPlaneName );
	HFBModel pNearOutOfFocusPlaneModel = RAGEFindModelByName( cNearDOFStrengthPlaneName );
	HFBModel pFarOutOfFocusPlaneModel = RAGEFindModelByName( cFarDOFStrengthPlaneName );

	// backwards compatibility
	if ( (pNearInFocusPlaneModel == NULL) && (pFarInFocusPlaneModel == NULL) )
	{
		pNearInFocusPlaneModel = RAGEFindModelByName( "NearPlane" );
		pFarInFocusPlaneModel = RAGEFindModelByName( "FarPlane" );
	}

	double fNearOutOfFocusPlaneDistance = 0.0f;
	double fNearInFocusPlaneDistance = 0.1f;
	double fFarInFocusPlaneDistance = 150.0f;
	double fFarOutOfFocusPlaneDistance = 175.0f;

	if ( pNearInFocusPlaneModel && pFarInFocusPlaneModel )
	{            
		FBVector3d nearTrans = pNearInFocusPlaneModel->Translation;
		fNearInFocusPlaneDistance = nearTrans[0];

		FBVector3d farTrans = pFarInFocusPlaneModel->Translation;
		fFarInFocusPlaneDistance = farTrans[0];
	}

	if ( pNearOutOfFocusPlaneModel && pFarOutOfFocusPlaneModel )
	{            
		FBVector3d nearTrans = pNearOutOfFocusPlaneModel->Translation;
		fNearOutOfFocusPlaneDistance = nearTrans[0];

		FBVector3d farTrans = pFarOutOfFocusPlaneModel->Translation;
		fFarOutOfFocusPlaneDistance = farTrans[0];
	}

	return Vec4V((float)(fNearOutOfFocusPlaneDistance * MOTIONBUILDER_UNIT_SCALE), 
		(float)(fNearInFocusPlaneDistance * MOTIONBUILDER_UNIT_SCALE), 
		(float)(fFarInFocusPlaneDistance * MOTIONBUILDER_UNIT_SCALE), 
		(float)(fFarOutOfFocusPlaneDistance * MOTIONBUILDER_UNIT_SCALE));
}

Matrix34 RemoteConsolePlugin::GetMatrix(HFBCamera selectedModel)
{
	// We are sending Euler angles to the game, so here we:
	// 1) Get the camera translation/rotation
	// 2) Point it at the interest
	// 3) Add Roll
	// 4) Z-up conversion
	// 5) Translate into game space

	FBQuaternion quatOut;
	FBRVector vecIn;

	FBVector3d rot = selectedModel->Rotation;
	vecIn = FBRVector( rot[0], rot[1], rot[2] );

	//static FBRVector vec2 = FBRVector(0,0,0);

	//if(vec2 != vecIn)
	//{
	//	char buf2[RAGE_MAX_PATH];
	//	sprintf(buf2, "DIR: %f, %f, %f\n", vec2[0], vec2[1], vec2[2]);
	//	OutputDebugString(buf2);
	//	vec2 = vecIn;
	//}

	Quaternion quat;
	Vector3 vec;

	FBRotationToQuaternion( quatOut, vecIn );
	quat.x = (float)quatOut[0];
	quat.y = (float)quatOut[1];
	quat.z = (float)quatOut[2];
	quat.w = (float)quatOut[3];		

	FBVector3d trans = selectedModel->Translation;
	vec.x = (float)(trans[0] * MOTIONBUILDER_UNIT_SCALE);
	vec.y = (float)(trans[1] * MOTIONBUILDER_UNIT_SCALE);
	vec.z = (float)(trans[2] * MOTIONBUILDER_UNIT_SCALE);

	Matrix34 cameraMatrix;
	cameraMatrix.Identity();
	cameraMatrix.FromQuaternion( quat );
	cameraMatrix.d = vec;

	Vector3 upVector = Vector3( 0.0f, 1.0f, 0.0f );
	Vector3 upCameraVector;

	// transform up vector into camera space
	cameraMatrix.Transform( upVector, upCameraVector );
	upCameraVector = upCameraVector - cameraMatrix.d;
	upCameraVector.Normalize();

	Matrix34 cameraMatrixCopy = cameraMatrix;
	cameraMatrix.Identity3x3();

	if ( selectedModel->Interest )
	{
		Vector3 vec;

		FBVector3d trans = selectedModel->Interest->Translation;
		vec.x = (float)(trans[0] * MOTIONBUILDER_UNIT_SCALE);
		vec.y = (float)(trans[1] * MOTIONBUILDER_UNIT_SCALE);
		vec.z = (float)(trans[2] * MOTIONBUILDER_UNIT_SCALE);

		rexMBAnimExportCommon::LookAt( vec, cameraMatrix, upCameraVector );
	}
	else
	{
		// Create a fake interest, our camera needs some kind of point of interest for the direction
		Vector3 forwardVector(1.0f,0.0f,0.0f);
		Vector3 forwardCameraVector;
		Vector3 forwardWorldVector;

		Matrix34 camMatrix = cameraMatrixCopy;
		camMatrix.Transform(forwardVector, forwardCameraVector); // transfer into camera space
		Matrix34 worldMatrix;
		worldMatrix.Identity();
		worldMatrix.UnTransform(forwardCameraVector, forwardWorldVector); // transfer into world space

		rexMBAnimExportCommon::LookAt( forwardWorldVector, cameraMatrix, upCameraVector );
	}

	// Add roll
	cameraMatrix.RotateLocalZ( (float)(selectedModel->Roll * DtoR) );

	// Fix up the cameras otherwise they are 180 the wrong way in-game (previously fixed up at runtime by tom)
	cameraMatrix.RotateFullY(PI);

	// Zup
	Quaternion quatYUp;
	Quaternion quatZUp;
	cameraMatrix.ToQuaternion(quatYUp);
	quatZUp.x = -quatYUp.x;
	quatZUp.y = quatYUp.z;
	quatZUp.z = quatYUp.y;
	quatZUp.w = quatYUp.w;
	cameraMatrix.FromQuaternion(quatZUp);

	float y = cameraMatrix.d.y;
	cameraMatrix.d.x = -cameraMatrix.d.x;
	cameraMatrix.d.y = cameraMatrix.d.z;
	cameraMatrix.d.z = y;  

	// Translate the matrix into game space
	Matrix34 sceneOrientation;
	sceneOrientation.Identity();
	sceneOrientation.Translate(m_CutsceneExportData.GetCutsceneFile().GetOffset());
	sceneOrientation.RotateLocalZ(m_CutsceneExportData.GetCutsceneFile().GetRotation()*DtoR);
	cameraMatrix.Dot(sceneOrientation);

	return cameraMatrix;

	//Vector3 lightRotationEulers;
	//cameraMatrix.ToEulersXYZ(lightRotationEulers);

	//// RAG accepts degrees not radians
	//return Vec3V(lightRotationEulers.GetX() * RtoD, lightRotationEulers.GetY() * RtoD, lightRotationEulers.GetZ() * RtoD);
}

Vec3V_Out RemoteConsolePlugin::GetDirection(HFBCamera selectedModel)
{
	// We are sending Euler angles to the game, so here we:
	// 1) Get the camera translation/rotation
	// 2) Point it at the interest
	// 3) Add Roll
	// 4) Z-up conversion
	// 5) Translate into game space

	FBQuaternion quatOut;
	FBRVector vecIn;

	FBVector3d rot = selectedModel->Rotation;
	vecIn = FBRVector( rot[0], rot[1], rot[2] );

	//static FBRVector vec2 = FBRVector(0,0,0);

	//if(vec2 != vecIn)
	//{
	//	char buf2[RAGE_MAX_PATH];
	//	sprintf(buf2, "DIR: %f, %f, %f\n", vec2[0], vec2[1], vec2[2]);
	//	OutputDebugString(buf2);
	//	vec2 = vecIn;
	//}

	Quaternion quat;
	Vector3 vec;

	FBRotationToQuaternion( quatOut, vecIn );
	quat.x = (float)quatOut[0];
	quat.y = (float)quatOut[1];
	quat.z = (float)quatOut[2];
	quat.w = (float)quatOut[3];		

	FBVector3d trans = selectedModel->Translation;
	vec.x = (float)(trans[0] * MOTIONBUILDER_UNIT_SCALE);
	vec.y = (float)(trans[1] * MOTIONBUILDER_UNIT_SCALE);
	vec.z = (float)(trans[2] * MOTIONBUILDER_UNIT_SCALE);

	Matrix34 cameraMatrix;
	cameraMatrix.Identity();
	cameraMatrix.FromQuaternion( quat );
	cameraMatrix.d = vec;

	Vector3 upVector = Vector3( 0.0f, 1.0f, 0.0f );
	Vector3 upCameraVector;

	// transform up vector into camera space
	cameraMatrix.Transform( upVector, upCameraVector );
	upCameraVector = upCameraVector - cameraMatrix.d;
	upCameraVector.Normalize();

	Matrix34 cameraMatrixCopy = cameraMatrix;
	cameraMatrix.Identity3x3();

	if ( selectedModel->Interest )
	{
		Vector3 vec;

		FBVector3d trans = selectedModel->Interest->Translation;
		vec.x = (float)(trans[0] * MOTIONBUILDER_UNIT_SCALE);
		vec.y = (float)(trans[1] * MOTIONBUILDER_UNIT_SCALE);
		vec.z = (float)(trans[2] * MOTIONBUILDER_UNIT_SCALE);

		rexMBAnimExportCommon::LookAt( vec, cameraMatrix, upCameraVector );
	}
	else
	{
		// Create a fake interest, our camera needs some kind of point of interest for the direction
		Vector3 forwardVector(1.0f,0.0f,0.0f);
		Vector3 forwardCameraVector;
		Vector3 forwardWorldVector;

		Matrix34 camMatrix = cameraMatrixCopy;
		camMatrix.Transform(forwardVector, forwardCameraVector); // transfer into camera space
		Matrix34 worldMatrix;
		worldMatrix.Identity();
		worldMatrix.UnTransform(forwardCameraVector, forwardWorldVector); // transfer into world space

		rexMBAnimExportCommon::LookAt( forwardWorldVector, cameraMatrix, upCameraVector );
	}

	// Add roll
	cameraMatrix.RotateLocalZ( (float)(selectedModel->Roll * DtoR) );

	// Fix up the cameras otherwise they are 180 the wrong way in-game (previously fixed up at runtime by tom)
	cameraMatrix.RotateFullY(PI);

	// Zup
	Quaternion quatYUp;
	Quaternion quatZUp;
	cameraMatrix.ToQuaternion(quatYUp);
	quatZUp.x = -quatYUp.x;
	quatZUp.y = quatYUp.z;
	quatZUp.z = quatYUp.y;
	quatZUp.w = quatYUp.w;
	cameraMatrix.FromQuaternion(quatZUp);

	float y = cameraMatrix.d.y;
	cameraMatrix.d.x = -cameraMatrix.d.x;
	cameraMatrix.d.y = cameraMatrix.d.z;
	cameraMatrix.d.z = y;  

	// Translate the matrix into game space
	Matrix34 sceneOrientation;
	sceneOrientation.Identity();
	sceneOrientation.Translate(m_CutsceneExportData.GetCutsceneFile().GetOffset());
	sceneOrientation.RotateLocalZ(m_CutsceneExportData.GetCutsceneFile().GetRotation()*DtoR);
	cameraMatrix.Dot(sceneOrientation);

	Vector3 lightRotationEulers;
	cameraMatrix.ToEulersXYZ(lightRotationEulers);

	// RAG accepts degrees not radians
	return Vec3V(lightRotationEulers.GetX() * RtoD, lightRotationEulers.GetY() * RtoD, lightRotationEulers.GetZ() * RtoD);
}

Vec3V_Out RemoteConsolePlugin::GetDirection(HFBModel selectedModel)
{
	// We are sending Euler angles to the game, so here we:
	// 1) Get the light's local to world transform in the MB coordinate system
	// 2) Build a MB to Rage transform
	// 3) Multiply through to get a Rage space local to world transform
	// 4) Convert to Euler angles (with Y & Z negated)

	FBMatrix lightToWorldTransform;
	selectedModel->GetMatrix(lightToWorldTransform);

	Matrix34 lightToWorldTransformRage;
	FBMatrixToRageMatrix34(lightToWorldTransform, lightToWorldTransformRage);

	if(selectedModel->Is(FBLight::TypeInfo))
	{
		lightToWorldTransformRage.RotateFullY(PI);

		Quaternion quatYUp;
		Quaternion quatZUp;
		lightToWorldTransformRage.ToQuaternion(quatYUp);
		quatZUp.x = -quatYUp.x;
		quatZUp.y = quatYUp.z;
		quatZUp.z = quatYUp.y;
		quatZUp.w = quatYUp.w;
		lightToWorldTransformRage.FromQuaternion(quatZUp);

		Vector3 vDirection(VEC3_ZERO);
		lightToWorldTransformRage.Transform3x3(Vector3(0,0,-1), vDirection);

		return Vec3V(vDirection.GetX(), vDirection.GetY(), vDirection.GetZ());
	}
	else
	{
		Matrix34 motionBuilderToRageTransform(
			1.0f, 0.0f,  0.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 1.0f,  0.0f,
			0.0f, 0.0f,  0.0f);

		lightToWorldTransformRage.Dot(motionBuilderToRageTransform);

		Vector3 lightRotationEulers;
		lightToWorldTransformRage.ToEulers(lightRotationEulers, "xyz");

		float rotationRadians = DtoR * m_CutsceneExportData.GetCutsceneFile().GetRotation();
		lightRotationEulers.RotateZ( rotationRadians );

		return Vec3V(lightRotationEulers.GetX(), -lightRotationEulers.GetY(), -lightRotationEulers.GetZ());
	}
}

float RemoteConsolePlugin::GetFalloff(HFBLight selectedLight)
{
	//NOTE:  "Fall Off" is a custom property that is explicitly searched for within the export pipeline.
	HFBProperty falloffProperty = selectedLight->PropertyList.Find(FALLOFF_CUSTOM_PROPERTY_NAME);
	if ( falloffProperty != NULL )
	{
		int propertyType = falloffProperty->GetPropertyType();
		if ( propertyType == kFBPT_double)
		{
			double falloff = 0.0f;
			falloffProperty->GetData(&falloff, sizeof(double));
			return (float) falloff;
		}
	}

	return -1.0f;
}

//void RemoteConsolePlugin::ResetOverrides()
//{
//	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
//	{
//		Remote::WriteBoolWidgetByPath(LightIntensityOverrideWidgetName, false);
//		Remote::WriteBoolWidgetByPath(LightColourOverrideWidgetName, false);
//		Remote::WriteBoolWidgetByPath(LightAngleOverrideWidgetName, false);
//		Remote::WriteBoolWidgetByPath(LightFalloffOverrideWidgetName, false);
//		Remote::WriteBoolWidgetByPath(LightPositionOverrideWidgetName, false);
//		Remote::WriteBoolWidgetByPath(LightDirectionOverrideWidgetName, false);
//	}
//	else
//	{
//		m_RemoteConsole.WriteBoolWidget(LightIntensityOverrideWidgetName, false);
//		m_RemoteConsole.WriteBoolWidget(LightColourOverrideWidgetName, false);
//		m_RemoteConsole.WriteBoolWidget(LightAngleOverrideWidgetName, false);
//		m_RemoteConsole.WriteBoolWidget(LightFalloffOverrideWidgetName, false);
//		m_RemoteConsole.WriteBoolWidget(LightPositionOverrideWidgetName, false);
//		m_RemoteConsole.WriteBoolWidget(LightDirectionOverrideWidgetName, false);
//	}
//}

void RemoteConsolePlugin::LoadCutsceneExportData()
{
	RexRageCutsceneExportPData* pMarkupData = UserObjects::FindCutsceneMarkupData(false);
	if ( pMarkupData == NULL )
	{
		//TODO:  Error; unable to find cutscene information.
		return;
	}

	FBString dataStr = pMarkupData->m_Data;

	if(dataStr.GetLen())
	{
		m_CutsceneExportData.LoadCutsceneDataFromString( (char*)dataStr );
		m_CutsceneExportData.ValidateCutsceneExportData( );
	}
}

void RemoteConsolePlugin::UpdateCutsceneExportData()
{
	if ( m_IsConnected == false )
		return;
		
	if ( m_IsPlayingCutscene == false )
		return;

	if ( m_SendCutsceneData == false )
		return;

	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		Vector3 offset = m_CutsceneExportData.GetCutsceneFile().GetOffset();
		Remote::Vector3 offsetVector(offset[0], offset[1], offset[2]);
		Remote::WriteVector3WidgetByPath(CutsceneSceneOrientationWidgetName, offsetVector);

		float rotation = m_CutsceneExportData.GetCutsceneFile().GetRotation();
		Remote::WriteFloatWidgetByPath(CutsceneSceneRotationWidgetName, rotation);
	}
	else
	{
		Vector3 offset = m_CutsceneExportData.GetCutsceneFile().GetOffset();
		Vec3V offsetVector(offset[0], offset[1], offset[2]);
		m_RemoteConsole.WriteVector3Widget(CutsceneSceneOrientationWidgetName, offsetVector);

		float rotation = m_CutsceneExportData.GetCutsceneFile().GetRotation();
		m_RemoteConsole.WriteFloatWidget(CutsceneSceneRotationWidgetName, rotation);
	}

	//s32 startFrame = m_CutsceneExportData.GetCutsceneFile().GetRangeStart();
	//m_RemoteConsole.WriteIntWidget(CutsceneStartFrameWidgetName, startFrame);

	//s32 endFrame = m_CutsceneExportData.GetCutsceneFile().GetRangeEnd();
	//m_RemoteConsole.WriteIntWidget(CutsceneEndFrameWidgetName, endFrame);
}

void RemoteConsolePlugin::SetDataSending(bool set)
{
	m_SendCutsceneData = set;

	if ( m_SendCutsceneData == true )
	{
		m_SendCutsceneDataCheckbox.State = CHECKBOX_CHECKED;
	}
	else
	{
		m_SendCutsceneDataCheckbox.State = CHECKBOX_UNCHECKED;
	}
}

bool RemoteConsolePlugin::IsCutsceneActive()
{
	return (m_CutsceneState == cutsManager::CUTSCENE_PLAY_STATE || m_CutsceneState == cutsManager::CUTSCENE_PAUSED_STATE);
}

bool RemoteConsolePlugin::IsCutscenePlaying()
{
	return (m_CutsceneState == cutsManager::CUTSCENE_PLAY_STATE);
}

cutsManager::ECutsceneState RemoteConsolePlugin::GetCutsceneState()
{
	if(m_WCFModeCheckbox.State == CHECKBOX_CHECKED)
	{
		const char* result = Remote::SendCommand(CutsceneGetStateCommand);
		if(result != NULL)
		{
			cutsManager::ECutsceneState cutsceneState = (cutsManager::ECutsceneState) atoi(result);
			return cutsceneState;
		}
	}
	else
	{
		atString result;
		if ( m_RemoteConsole.SendCommand(CutsceneGetStateCommand, result) == true )
		{
			if ( result.c_str() != NULL )
			{
				cutsManager::ECutsceneState cutsceneState = (cutsManager::ECutsceneState) atoi(result.c_str());
				return cutsceneState;
			}
		}
	}

	return cutsManager::CUTSCENE_IDLE_STATE;
}
