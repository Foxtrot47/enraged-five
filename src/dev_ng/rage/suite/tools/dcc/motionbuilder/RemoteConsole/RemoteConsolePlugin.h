// 
// metadataEditor.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __REMOTE_CONSOLE_H__
#define __REMOTE_CONSOLE_H__

#include "cutscene/cutsmanager.h"
#include "system/timer.h"
#include "vector/vector3.h"

#include "RemoteConsole/DllMain.h"
#include "rexMBAnimExportCommon/utility.h"
#include "rexMBAnimExportCommon/cutsceneExport.h"
#include "rageRemoteConsole/RemoteConsole.h"
#include "UserObjects/cutsceneExportToolPdata.h"

//--- SDK include
#include <fbsdk/fbsdk.h>

//Forward Declarations
namespace rage
{
	class parTree;
	class parTreeNode;
}

struct LightData
{
	LightData() { Reset(); }

	void Reset() 
	{
		m_Intensity = 0.0f;
		m_Colour.ZeroComponents();
		m_Angle = 0.0f;
		m_Falloff = 0.0f;
		m_Position.ZeroComponents();
		m_Direction.ZeroComponents();
	}

	float m_Intensity;
	Vec3V m_Colour;
	float m_Angle;
	float m_Falloff;
	Vec3V m_Position;
	Vec3V m_Direction;
};

class RemoteConsolePlugin : public FBTool
{
	//--- FiLMBOX Tool declaration.
	FBToolDeclare( RemoteConsolePlugin, FBTool );

public:
	//--- FiLMBOX Construction/Destruction,
	virtual bool FBCreate           ();		//!< FiLMBOX Creation function.
	virtual void FBDestroy          ();		//!< FiLMBOX Destruction function.

	static void	InitClass();
	static void	ShutdownClass();

private:
	//--- UI Management
	void    UICreate	            ();

	void	EventToolIdle			( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void	EventSceneChange		( HISender pSender, HKEvent pEvent );
	void	EventSelectionChanged	( HISender pSender, HKEvent pEvent );
	void	OnFileOpenCompleted		( HISender pSender, HKEvent pEvent);
	void	EventFrameChange();

	void	Connect();
	void	Disconnect();

	void	OnConnectButtonClick	(HISender /*pSender*/, HKEvent /*pEvent*/);
	void	OnPlayCutsceneButtonClick	(HISender /*pSender*/, HKEvent /*pEvent*/);
	void	OnPlayCaptureCutsceneButtonClick	(HISender /*pSender*/, HKEvent /*pEvent*/);
	void	OnUseGameCutsceneDataClick(HISender /*pSender*/, HKEvent /*pEvent*/);
	void	OnFollowCameraSwitcherClick(HISender /*pSender*/, HKEvent /*pEvent*/);
	void	OnEditModeClick(HISender /*pSender*/, HKEvent /*pEvent*/);

	void	OnSelectionChange(bool /* force */);
	void	ResetSelection(bool force);
	//void	ResetOverrides();
	int		GetGameFrameFromMotionbuilderFrame(s32 frame);

	int		GetCurrentFrame();
	int		GetSceneFrame(const int& gameFrame);

	int		GetCurrentGameFrame();

	void	ReadCutsceneData();

	void	SendParticleData();
	void	SendLightData();
	void	SendCameraData();
	void	ReadSwitcher();
	bool	IsCurrentFrameInRange();
	int		SendCurrentFrame();

	// Light
	float		GetIntensity(HFBLight selectedLight);
	Vec3V_Out	GetDirection(HFBModel selectedModel);
	float		GetFalloff(HFBLight selectedLight);

	// Camera
	Vec3V_Out	GetDirection(HFBCamera selectedCamera); // cameras have interests/roll and other shit to deal with
	Matrix34	GetMatrix(HFBCamera selectedModel);
	float		GetFOV(HFBCamera selectedCamera);
	Vec4V_Out	GetDOF(HFBCamera selectedCamera);
	//float		GetDOFStrength(HFBCamera selectedCamera);
	bool		GetShallowDOFStrength(HFBCamera selectedCamera);
	bool		GetSimpleDOFStrength(HFBCamera selectedCamera);
	float		GetMotionBlur(HFBCamera selectedCamera);
	int			GetCoC(HFBCamera selectedCamera);
	float		GetCameraFocus(HFBCamera selectedCamera);

	// General
	Vec3V_Out	GetPosition(HFBModel selectedModel);

	void		LoadCutsceneExportData();
	void		UpdateCutsceneExportData();

	void		PlayCutscene();
	void		StopCutscene();

	bool		IsCutsceneActive();
	bool		IsCutscenePlaying();

	rage::cutsManager::ECutsceneState GetCutsceneState();

	void		SetDataSending(bool set);

	bool		IsDataValid();
	bool		WithinEST(s32 frame);
	bool		WithinEST(s32 frame, atString& name);
	bool		WithinShot(s32 frame, atString& name);
	bool		WithinRange(s32 frame);

	FBSystem	    m_System;
	FBApplication	m_Application;
	HFBScene		m_Scene;

	FBButton		m_ConnectButton;
	FBButton		m_PlayCutsceneButton;
	FBButton		m_PlayCaptureCutsceneButton;
	FBButton		m_SendCutsceneDataCheckbox;
	FBButton		m_FollowCameraSwitcherCheckbox;
	FBButton		m_EditModeCheckbox;
	FBEdit			m_ipTextBox;
	FBLabel			m_ipLabel;
	FBLabel			m_versionLabel;
	FBButton		m_WCFModeCheckbox;

	FBLabel			m_dofStateLabel;
	FBList			m_dofStateComboBox;

	HFBModel				m_SelectedModel;
	float					m_TimeIntervalMs;
	RexRageCutsceneExport	m_CutsceneExportData;

	int					m_LastFrame;

public:
	static void			FlushScene();

	static bool			m_IsConnected;
	static bool			m_ReloadCutsceneData;	//Reloads the MotionBuilder markup for the scene's cutscene
	static bool			m_StartCutscene;		
	static bool			m_StopCutscene;
	static rage::RemoteConsole		m_RemoteConsole;

	rage::cutsManager::ECutsceneState		m_CutsceneState;

	bool				m_InitializeCutscene;
	bool				m_IsPlayingCutscene;
	bool				m_InitializeCapture;
	bool				m_SendCutsceneData; //Serves as an override to stop sending information from MotionBuilder to the game, but allow users to scrub.
	bool				m_SendingOverridden; //Determines if sending cutscene has been overridden via a preview command.
	bool				m_FollowCameraSwitcher;
	bool				m_EditMode;
	bool				m_Capture;

	int					m_WidgetExistsTimeout;

	HFBAnimationNode	m_CameraSwitcherAnimNode;
	atMap<atHashString, int> m_WidgetIds;

	int					m_CurrentGameFrame;
	int					m_CurrentSceneFrame;
	int					m_CurrentFrameWithinPart;
};


/////Global Functions
// These were causing multiple definition compile error with functions declared in \\rage\suite\tools\dcc\motionbuilder\RemoteConsole\RemoteConsolePlugin.cpp
//EXTERN_C REMOTE_CONSOLE_API bool IsRemoteConsoleConnected();
//EXTERN_C REMOTE_CONSOLE_API void NotifyCutsceneUpdate();
//EXTERN_C REMOTE_CONSOLE_API void NotifyCutscenePatchStarted();
//EXTERN_C REMOTE_CONSOLE_API void NotifyCutscenePatchCompleted();

#endif // __REMOTE_CONSOLE_H__
