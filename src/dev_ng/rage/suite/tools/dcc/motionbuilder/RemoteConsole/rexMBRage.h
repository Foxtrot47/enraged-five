// 
// rexMbRage/userObjects.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __COMMON_REXMBRAGE_H__
#define __COMMON_REXMBRAGE_H__

#include "vector/vector3.h"

class rexMBRage
{
public:
	static bool Initialize(); 
	static void Destroy();

	//static char*					ExportSyncedSceneFile(rage::Vector3 position, rage::Vector3 rotation);
	static char*					GetSceneInitialOffsetOrigin();
	static rage::Vector3*			GetSyncedScenePosition();
	static rage::Vector3*			GetSyncedSceneRotation();
};	

#endif //__COMMON_REXMBRAGE_H__