// /animExportToolPdata.h

#ifndef __ANIM_EXPORT_TOOL_PDATA_H__
#define __ANIM_EXPORT_TOOL_PDATA_H__

#include "DllMain.h"
#include "parser/manager.h"
#include "OpenRealitySDK.h"
#include "AnimMarkup.h"
#include "Utility.h"

#pragma warning(disable:4481)

using namespace rage;

//-----------------------------------------------------------------------------

class RexRageAnimExportPData : public FBUserObject 
{
	FBClassDeclare(RexRageAnimExportPData, FBUserObject)
	FBDeclareUserObject(RexRageAnimExportPData);

public:
RexRageAnimExportPData( const char* pName = NULL, HIObject pObject=NULL );

	 //--- FiLMBOX Construction/Destruction,
    virtual bool FBCreate();        //!< FiLMBOX Creation function.
    virtual void FBDestroy();       //!< FiLMBOX Destruction function.

    virtual bool FbxStore( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );
    virtual bool FbxRetrieve( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );

	FBString			m_Data;
};

//-----------------------------------------------------------------------------
EXTERN_C USER_OBJECT_API RexRageAnimExportPData* FindAnimationMarkupData(bool bCreate);
EXTERN_C USER_OBJECT_API void SetAnimationMarkupData(RexAnimMarkupData* pData);
EXTERN_C USER_OBJECT_API RexAnimMarkupData* GetAnimationMarkupData();
EXTERN_C USER_OBJECT_API void	DeleteAnimationMarkupData();

#endif //__ANIM_EXPORT_TOOL_PDATA_H__
