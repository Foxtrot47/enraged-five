// 
// Utility.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __USER_OBJECTS_UTILITY_H__
#define __USER_OBJECTS_UTILITY_H__

#include "OpenRealitySDK.h"
#include "DllMain.h"

#include "atl/array.h"
#include "atl/string.h"

#include <string>

//NOTE: Changing this enumeration will require a change to UserObjects\Utility.h to be also changed.
enum PropertyChangeEnum
{
	PROPERTY_ADDED,
	PROPERTY_REMOVED,
	PROPERTY_EDITED
};

namespace UserObjectUtility
{
	// PURPOSE:  Transform all "from" strings to the "to" string within the given string.
	void StringReplaceAll(std::string& str, const std::string& from, const std::string& to);

	// PURPOSE: Divides the input string into chunks of the given length.
	// PARAMS:
	//    pInput - the input string
	//    size - the maximum size of each string (including the null-terminator)
	//    output - the output list of strings
	void SplitStringByLength( const char *pInput, int size, rage::atArray<rage::atString> &output );

	// PURPOSE: Merges the given list of strings into one long string
	// PARAMS:
	//    input - the input list of strings
	// RETURNS: The merged string or NULL on error.  Delete this manually when finished.
	const char* ConcatenateStrings( const rage::atArray<rage::atString> &input );

	// PURPOSE: Determines whether the program is in batch mode.
	// PARAMS:
	//    input - the input list of strings
	// RETURNS: The merged string or NULL on error.  Delete this manually when finished.
	std::string GetAnimExportMarkupFilePath();

	//PURPOSE : This method will take in a string name of a take, and locate
	//the actual structure for that take in the current scene.
	bool FindTake(const char* szTakeName, HFBTake& retTake);

	//PURPOSE : Determines if the specified take still exists in the scene.
	bool TakeExists(const HFBTake take);
}

// PURPOSE: Implemented to maintain compatibility between 2010 and 2012.
// PARAMS:
HFBModel RAGEFindModelByName(const char* pModelName);

// NOTE:  This used to exist within the MotionBuilder common library, although with the DLLs now split,
// the static variable would actually exist once per DLL.  This functionality has been moved into 
// the single UserObject DLL so there is now one instance.
//
EXTERN_C USER_OBJECT_API bool HasLoadedClips();
EXTERN_C USER_OBJECT_API void SetHasLoadedClips(bool set);
EXTERN_C USER_OBJECT_API bool HasReloadedClips();
EXTERN_C USER_OBJECT_API void SetHasReloadedClips(bool set);
//EXTERN_C USER_OBJECT_API HFBAnimationNode GetAnimationNodeFromCache(HFBTake hTake, HFBProperty hProperty);
EXTERN_C USER_OBJECT_API void QuickAnimationNodeCacheUpdate(HFBTake hTake, HFBProperty hProperty);
EXTERN_C USER_OBJECT_API void NotifyFileChange();
EXTERN_C USER_OBJECT_API void NotifyTakeChange();
EXTERN_C USER_OBJECT_API void NotifyPropertyChange(HFBTake hTake, HFBProperty hProperty, PropertyChangeEnum eAction);


#endif //__USER_OBJECTS_UTILITY_H__