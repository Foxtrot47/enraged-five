// UserObjects/ForceInclude.h

#include <windows.h>
#include <commctrl.h>

#if defined(_DEBUG)
	#if defined(_M_X64) || defined(_M_AMD64)
		#include "forceinclude/win64_tooldebug.h"
	#else
		#include "forceinclude/win32_tooldebug.h"
	#endif

#elif defined(NDEBUG)
	#if defined(_M_X64) || defined(_M_AMD64)
		#include "forceinclude/win64_toolrelease.h"
	#else
		#include "forceinclude/win32_toolrelease.h"
	#endif

#else
	#if defined(_M_X64) || defined(_M_AMD64)
		#include "forceinclude/win64_toolbeta.h"
	#else
		#include "forceinclude/win32_toolbeta.h"
	#endif
#endif
