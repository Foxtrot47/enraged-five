// UserObjects/DllMain.h

#pragma once

#ifdef USER_OBJECT_EXPORTS 
#define USER_OBJECT_API __declspec(dllexport)
#else
#define USER_OBJECT_API __declspec(dllimport)
#endif

#ifdef __cplusplus
#   define EXTERN_C     extern "C"
#else
#   define EXTERN_C
#endif // __cplusplus
