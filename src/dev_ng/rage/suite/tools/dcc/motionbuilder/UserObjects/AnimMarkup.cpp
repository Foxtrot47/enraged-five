// 
// AnimMarkup.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#include "atl/creator.h"
#include "AnimMarkup.h"
#include "AnimMarkup_parser.h"

#include <sstream>

#include "atl/string.h"
#include "file/device.h"
#include "file/asset.h"

#include "OpenRealitySDK.h"
#include "rexMBAnimExportCommon/utility.h"
#include "userobjects/Utility.h"

using namespace rage;

//#############################################################################

atString NormalizeName(atString& name)
{
	//Replace all the spaces with underscores
	int len = name.length();
	for (int i = 0; i < len; i++)
	{
		if (isspace(name[i]))
		{
			name[i] = '_';
		}
	}

	return name;
}

bool RexMbAnimData::SaveToFile(const char* szFilePath)
{
	return PARSER.SaveObject(szFilePath, "", this);
}

bool RexMbAnimData::SaveToString(char** output)
{
	// 8192 is not big enough for a scene with 50+ takes
	char buffer[131072];
	char memFileName[256];

	//Save the object into a memory mapped file buffer
	memset(buffer,0,131072);

	fiDevice::MakeMemoryFileName(memFileName, 256, &buffer, 131072, false, "RexMbAnimData");
	fiStream* pMemFile = ASSET.Open(memFileName, "");

	PARSER.SaveObject(pMemFile, this);
	pMemFile->Close();

	//Copy the data from the memory mapped file buffer into the null terminated output string
	int outSize = (int)strlen(buffer);
	(*output) = new char[outSize+1];
	memset(*output, 0, outSize+1);
	strcpy(*output, buffer);

	return true;
}

RexMbAnimData* RexMbAnimData::LoadFromString(const char* szInput)
{
	if( (szInput == NULL) || (strlen(szInput) == 0))
		return NULL;

	char memFileName[256];
	int  inputSize = (int)strlen(szInput);
	fiDevice::MakeMemoryFileName(memFileName, 256, szInput, inputSize, false, "RexMbAnimData");
	fiStream* pMemFile = ASSET.Open(memFileName, "");

	RexMbAnimData* pRetData = new RexMbAnimData();

	if(!PARSER.LoadObject<RexMbAnimData>(pMemFile, *pRetData))
	{
		pMemFile->Close();
		delete pRetData;
		return NULL;
	}
	else
	{
		pMemFile->Close();
		return pRetData;
	}
}

bool RexMbAnimData::ValidateAnimExportData(bool bShowDialogs)
{
	//Validate the output path
	if(strlen(GetOutputPath()) == 0)
	{
		if(bShowDialogs)
		{
			FBMessageBox("REX Animation Export - User Error", "Please enter a valid output path.", "OK");
		}
		else
		{
			Errorf("Markup data validation failed - No output path has been specified.");
		}
		return false;
	}

	//Validate the root model name
	if(!GetNumInputModels() || (strlen(GetInputModelName(0)) == 0))
	{
		if(bShowDialogs)
		{
			FBMessageBox("REX Animation Export - User Error", "Please provide a valid root model", "OK");
		}
		else
		{
			Errorf("Markup data validation failed - No root model has been specified.");
		}
		return false;
	}

	//Validate that prefixes and/or suffixes have been setup if multiple models are tagged 
	if(GetNumInputModels() > 1)
	{
		if(GetSegmetFromTakes())
		{
			int nPrefixes = GetNumAnimationPrefixes();
			int nSuffixes = GetNumAnimationSuffixes();

			if(nPrefixes != nSuffixes)
			{
				Assertf(false, "Prefix and suffix arrays have gone out of sync, there are %d prefixes and %d suffixes", nPrefixes, nSuffixes);
			}

			bool bHasPrefixOrSuffix = false;

			//Check that prefixes and suffixes have been assigned
			for(int idx=0; idx < nPrefixes; idx++)
			{
				if ( (strlen(GetAnimationPrefix(idx)) != 0) || (strlen(GetAnimationSuffix(idx))) )
				{
					bHasPrefixOrSuffix = true;
					break;
				}
			}

			if(!bHasPrefixOrSuffix)
			{
				if(bShowDialogs)
				{
					FBMessageBox("REX Animation Export - Error", "Multiple models have been tagged for animation export, but no animation prefixes or\nsuffixes have been assigned, animations would overrwrite one another.", "OK");
				}
				else
				{
					Errorf("Multiple models have been tagged for animation export, but no animation prefixes or suffixes have been assigned, animations would overrwrite one another.");
				}

				return false;
			}

			//Check for prefixes and suffixes that have been assgined, but collide with each other
			map< string, int > animTagMap;
			for(int idx=0; idx < nPrefixes; idx++)
			{
				stringstream ss;
				ss << GetAnimationPrefix(idx) << "|" << GetAnimationSuffix(idx);
				map< string, int >::iterator it = animTagMap.find( ss.str() );
				if( it != animTagMap.end() )
				{
					if(bShowDialogs)
					{
						char msgBuffer[512];
						snprintf(msgBuffer, 512, "The animation prefix/suffix assigned to the animations for model %s collide\n with the prefix/suffix assigned to the animations for model %s.\nThese animations would overwrite each other.",
							GetInputModelName(idx), GetInputModelName(it->second));

						FBMessageBox("REX Animation Export - Error", msgBuffer, "OK");
					}
					else
					{
						Errorf("Markup data validation vailed - The animation prefix/suffix for model %s collides with the prefix/suffics for model %s",
							GetInputModelName(idx), GetInputModelName(it->second));
					}
					return false;
				}
				else
				{
					animTagMap[ ss.str() ] = idx;
				}
			}
		}
	}

	std::vector<std::string> segmentNames;

	int nSegments = GetNumSegments();

	if(!nSegments)
	{
		if(bShowDialogs)
		{
			FBMessageBox("REX Animation Export - User Warning", "There are no segments to export", "OK");
		}
		else
		{
			Warningf("Markup data validation warning - No segments/takes have been defined for export.");
		}
		return false;
	}
	else
	{
		//Validate the segment data
		for(int segIdx = 0; segIdx < nSegments; segIdx++)
		{
			RexMbAnimSegmentData* pSegData = GetSegment(segIdx);

			//Validate the segment name
			vector<string>::iterator findIt = find(segmentNames.begin(), segmentNames.end(), pSegData->GetSegmentName());
			if(findIt != segmentNames.end())
			{
				char msgBuffer[512];
				sprintf_s(msgBuffer, 512, "Markup data validation error - A duplicate segment name '%s' has been entered, export aborted.", pSegData->GetSegmentName());

				if(bShowDialogs)
				{	
					FBMessageBox("REX Animation Export - User Error", msgBuffer, "OK");
				}
				else
				{
					Errorf("%s", msgBuffer);
				}
				return false;
			}
			segmentNames.push_back(pSegData->GetSegmentName());

			//Validate the frame ranges
			if( pSegData->GetStartAsFrame() > pSegData->GetEndAsFrame() )
			{
				char msgBuffer[512];
				sprintf_s(msgBuffer, 512, "Markup data validation error - Segment '%s, has a start frame of %d which is after its end frame of %d, export aborted",
					pSegData->GetSegmentName(), pSegData->GetStartAsFrame(), pSegData->GetEndAsFrame());

				if(bShowDialogs)
				{	
					FBMessageBox("REX Animation Export - User Error", msgBuffer, "OK");
				}
				else
				{
					Errorf("%s", msgBuffer);
				}
				return false;
			}

			if( (pSegData->GetStartAsFrame() == pSegData->GetEndAsFrame()) &&
				(pSegData->GetStartAsFrame() != -1) )
			{
				char msgBuffer[512];
				sprintf_s(msgBuffer, 512, "Markup data validation error - Segment '%s', has both its start and end frames set to '%s', export aborted.",
					pSegData->GetSegmentName(), pSegData->GetStartAsFrame());

				if(bShowDialogs)
				{	
					FBMessageBox("REX Animation Export - User Error", msgBuffer, "OK");
				}
				else
				{
					Errorf("%s", msgBuffer);
				}
				return false;
			}
		}
	}

	return true;
}

bool RexMbAnimData::RemoveInputModel(const char* szModelName)
{
	atArray< atString >::iterator rIt = m_inputModelNames.begin();
	atArray< atString >::iterator mIt = m_moverModelNames.begin();

	while( rIt != m_inputModelNames.end() )
	{
		atString& rootName = (*rIt);

		if( strcmp( rootName.c_str(), szModelName ) == 0 )
		{
			m_inputModelNames.erase( rIt );
			m_moverModelNames.erase( mIt );
			return true;
		}

		rIt++; 
		mIt++;
	}
	return false;
}

bool RexMbAnimData::RemoveMoverModel(const char* szMoverName)
{
	for(atArray< atString >::iterator mIt = m_moverModelNames.begin();
		mIt != m_moverModelNames.end();
		++mIt)
	{
		atString& moverName = (*mIt);

		if( strcmp( moverName.c_str(), szMoverName ) == 0 )
		{
			m_moverModelNames.erase( mIt );
			return true;
		}
	}

	return false;
}

bool RexMbAnimData::RemoveSegmentByTakeName( const char* szTakeName )
{
	for(atArray< RexMbAnimSegmentData* >::iterator it = m_segments.begin();
		it != m_segments.end();
		++it)
	{
		RexMbAnimSegmentData* pSegData = (*it);

		if( strcmp( pSegData->GetTakeName(), szTakeName ) == 0 )
		{
			delete pSegData;
			m_segments.erase( it );
			return true;
		}
	}

	return false;
}

bool RexAnimMarkupData::SaveToFile(const char* szFilePath)
{
	return PARSER.SaveObject(szFilePath, "", this);
}

bool RexAnimMarkupData::SaveToString(char** output)
{
	// 8192 is not big enough for a scene with 50+ takes
	char buffer[131072];
	char memFileName[256];

	//Save the object into a memory mapped file buffer
	memset(buffer,0,131072);

	fiDevice::MakeMemoryFileName(memFileName, 256, &buffer, 131072, false, "RexAnimMarkupData");
	fiStream* pMemFile = ASSET.Open(memFileName, "");

	PARSER.SaveObject(pMemFile, this);
	pMemFile->Close();

	//Copy the data from the memory mapped file buffer into the null terminated output string
	int outSize = (int)strlen(buffer);
	(*output) = new char[outSize+1];
	memset(*output, 0, outSize+1);
	strcpy(*output, buffer);

	return true;
}

RexAnimMarkupData* RexAnimMarkupData::LoadFromString(const char* szInput)
{
	if( (szInput == NULL) || (strlen(szInput) == 0))
		return NULL;

	char memFileName[256];
	int  inputSize = (int)strlen(szInput);
	fiDevice::MakeMemoryFileName(memFileName, 256, szInput, inputSize, false, "RexAnimMarkupData");
	fiStream* pMemFile = ASSET.Open(memFileName, "");

	RexAnimMarkupData* pRetData = new RexAnimMarkupData();

	if(!PARSER.LoadObject<RexAnimMarkupData>(pMemFile, *pRetData))
	{
		pMemFile->Close();
		delete pRetData;
		return NULL;
	}
	else
	{
		pMemFile->Close();
		return pRetData;
	}
}

bool RexAnimMarkupData::ValidateAnimExportData(bool bShowDialogs)
{
	//Validate the output path
	if(strlen(GetOutputPath()) == 0)
	{
		if(bShowDialogs)
		{
			FBMessageBox("REX Animation Export - User Error", "Please enter a valid default output path.", "OK");
		}
		else
		{
			Errorf("Markup data validation failed - No default output path has been specified.");
		}
		return false;
	}

	atString atOutputPath(GetOutputPath());
	atOutputPath.Replace("/", "\\");
	atOutputPath.Lowercase();

	atString atProjectRoot(m_projectRoot.c_str());
	atProjectRoot.Replace("/", "\\");
	atProjectRoot.Lowercase();

	if(!atOutputPath.StartsWith(atProjectRoot.c_str()))
	{
		if(bShowDialogs)
		{
			FBMessageBox("REX Animation Export - User Error", "Please enter a valid default output path.", "OK");
		}
		else
		{
			Errorf("Markup data validation failed - No default output path has been specified.");
		}
		return false;
	}

	//Validate the root model name
	if(GetNumCharacters() == 0)
	{
		if(bShowDialogs)
		{
			FBMessageBox("REX Animation Export - User Error", "Please provide a valid root model", "OK");
		}
		else
		{
			Errorf("Markup data validation failed - No root model has been specified.");
		}
		return false;
	}

	for(int characterIndex = 0; characterIndex < GetNumCharacters(); ++characterIndex)
	{
		RexCharAnimData* pCharData = GetCharacter(characterIndex);
		
		if(stricmp(pCharData->GetCtrlFile(),"(no spec file)") == 0)
		{
			if(bShowDialogs)
			{
				char msgBuffer[512];
				snprintf(msgBuffer, 512, "Model %s has no Ctrl file specified.",
					pCharData->GetInputModelName(0));

				FBMessageBox("REX Animation Export - Error", msgBuffer, "OK");
			}
			else
			{
				Errorf("Model %s has no Ctrl file specified.", pCharData->GetInputModelName(0));
			}
			return false;
		}

		if(stricmp(pCharData->GetModelType(), "body") == 0)
		{
			if(stricmp(pCharData->GetSkeletonFile(), DefaultSkelFile) == 0)
			{
				if(bShowDialogs)
				{
					char msgBuffer[512];
					snprintf(msgBuffer, 512, "The skeleton assigned to the animations for model %s is invalid.",
						pCharData->GetInputModelName(0));

					FBMessageBox("REX Animation Export - Error", msgBuffer, "OK");
				}
				else
				{
					Errorf("The skeleton assigned to the animations for model %s is invalid.", pCharData->GetInputModelName(0));
				}

				return false;
			}
		}
	}

	//Validate that prefixes and/or suffixes have been setup if multiple models are tagged 
	if(GetNumCharacters() > 1)
	{
		if(GetSegmetFromTakes())
		{
			for(int characterIndex = 0; characterIndex < GetNumCharacters(); ++characterIndex)
			{
				RexCharAnimData* pCharData = GetCharacter(characterIndex);

				if(pCharData->m_modelType == "Camera")
				{
					HFBModel cameraModel = RAGEFindModelByName((char*)pCharData->m_inputModelNames[0].c_str());
					if(cameraModel->Parent != NULL)
					{
						char msgBuffer[512];
						snprintf(msgBuffer, 512, "The camera '%s' has a parent, this setup is invalid.", pCharData->m_inputModelNames[0].c_str());

						FBMessageBox("REX Animation Export - Error", msgBuffer, "OK");
						return false;
					}
				}
			}

			//Check for prefixes and suffixes that have been assgined, but collide with each other
			map< string, RexCharAnimData* > animTagMap;
			for(int characterIndex = 0; characterIndex < GetNumCharacters(); ++characterIndex)
			{
				RexCharAnimData* pCharData = GetCharacter(characterIndex);
	
				atString charOutputPath = atString(pCharData->GetAltOutputPath());
				if(strcmp(charOutputPath, "(none specified)") == 0)
				{
					charOutputPath = m_outputPath;
				}

				std::stringstream ss;
				ss << pCharData->m_animPrefix << "|" << pCharData->m_animSuffix << "|" << charOutputPath;
				map< string, RexCharAnimData* >::iterator it = animTagMap.find( ss.str() );
				if( it != animTagMap.end() )
				{
					if(bShowDialogs)
					{
						char msgBuffer[512];
						snprintf(msgBuffer, 512, "The animation prefix/suffix assigned to the animations for model %s collide\n with the prefix/suffix assigned to the animations for model %s.\nThese animations would overwrite each other.",
							pCharData->GetInputModelName(0), it->second->GetInputModelName(0));

						FBMessageBox("REX Animation Export - Error", msgBuffer, "OK");
					}
					else
					{
						Errorf("Markup data validation vailed - The animation prefix/suffix for model %s collides with the prefix/suffics for model %s",
							pCharData->GetInputModelName(0),  it->second->GetInputModelName(0));
					}
					return false;
				}
				else
				{
					animTagMap[ ss.str() ] = pCharData;
				}
			}
		}
	}

	std::vector<std::string> segmentNames;
	std::vector<std::string> outputPaths;

	int nSegments = GetNumSegments();

	if(!nSegments)
	{
		if(bShowDialogs)
		{
			FBMessageBox("REX Animation Export - User Warning", "There are no segments to export", "OK");
		}
		else
		{
			Warningf("Markup data validation warning - No segments/takes have been defined for export.");
		}
		return false;
	}
	else
	{
		bool bNoTakes = true;
		for(int segIdx = 0; segIdx < nSegments; segIdx++)
		{
			RexMbAnimSegmentData* pSegData = GetSegment(segIdx);

			if(pSegData->m_bExport)
				bNoTakes = false;
		}

		if(bNoTakes)
		{
			if(bShowDialogs)
			{
				FBMessageBox("REX Animation Export - User Error", "There are no segments selected to export", "OK");
			}
			else
			{
				Errorf("Markup data validation warning - No segments/takes have been selected for export.");
			}
			return false;
		}

		//Validate the segment data
		for(int segIdx = 0; segIdx < nSegments; segIdx++)
		{
			RexMbAnimSegmentData* pSegData = GetSegment(segIdx);

			if (GetSegmetFromTakes() && pSegData->m_takeName != nullptr)
			{
				// If we're making segments from takes, ensure the segment name is the take name. (url:bugstar:5804852)
				pSegData->m_segmentName = pSegData->m_takeName;
				NormalizeName(pSegData->m_segmentName);
			}

			//Validate the segment name
			std::vector<std::string>::iterator findIt = find(segmentNames.begin(), segmentNames.end(), pSegData->GetSegmentName());
			if(findIt != segmentNames.end())
			{
				char msgBuffer[512];
				sprintf_s(msgBuffer, 512, "Markup data validation error - A duplicate segment name '%s' has been entered, export aborted.", pSegData->GetSegmentName());

				if(bShowDialogs)
				{	
					FBMessageBox("REX Animation Export - User Error", msgBuffer, "OK");
				}
				else
				{
					Errorf("%s", msgBuffer);
				}
				return false;
			}
			segmentNames.push_back(pSegData->GetSegmentName());

			if ( pSegData->IsMarkedForExport() )
			{
				int pathCount = pSegData->GetNumAltOutputPaths();
				// We want it to run at least once. GetOutputFilePath checks if there are actually no alt 
				// output paths and deals with it accordingly.
				if ( pathCount == 0 ) { pathCount = 1; }

				const RexCharAnimData* pCharData = this->GetCharacter(0);
				if ( pCharData->IsMarkedForExport() )
				{
					for ( int idxPath = 0; idxPath < pathCount; idxPath++)
					{
						std::string outputPath = std::string(this->GetOutputFilePath(pCharData, pSegData, idxPath));
						std::vector<std::string>::iterator outputfindIt = find(outputPaths.begin(), outputPaths.end(), outputPath);
						if(outputfindIt != outputPaths.end())
						{
							char msgBuffer[512];
							sprintf_s(msgBuffer, 512, "Markup data validation error - A duplicate output path '%s' has been specified, export aborted.", outputPath);

							if(bShowDialogs)
							{	
								FBMessageBox("REX Animation Export - User Error", msgBuffer, "OK");
							}
							else
							{
								Errorf("%s", msgBuffer);
							}

							return false;
						}

						outputPaths.push_back(outputPath);
					}
				}
			}

			//Validate the frame ranges
			if( pSegData->GetStartAsFrame() > pSegData->GetEndAsFrame() )
			{
				char msgBuffer[512];
				sprintf_s(msgBuffer, 512, "Markup data validation error - Segment '%s, has a start frame of %d which is after its end frame of %d, export aborted",
					pSegData->GetSegmentName(), pSegData->GetStartAsFrame(), pSegData->GetEndAsFrame());

				if(bShowDialogs)
				{	
					FBMessageBox("REX Animation Export - User Error", msgBuffer, "OK");
				}
				else
				{
					Errorf("%s", msgBuffer);
				}
				return false;
			}

			if( (pSegData->GetStartAsFrame() == pSegData->GetEndAsFrame()) &&
				(pSegData->GetStartAsFrame() != -1) )
			{
				char msgBuffer[512];
				sprintf_s(msgBuffer, 512, "Markup data validation error - Segment '%s', has both its start and end frames set to '%s', export aborted.",
					pSegData->GetSegmentName(), pSegData->GetStartAsFrame());

				if(bShowDialogs)
				{	
					FBMessageBox("REX Animation Export - User Error", msgBuffer, "OK");
				}
				else
				{
					Errorf("%s", msgBuffer);
				}
				return false;
			}
		}
	}

	// Validate the initial offset is setup correctly
	if(m_bUseInitOffset)
	{
		bool bResult = true;

		std::string strOriginObject(m_originObjectName.c_str());
		if(stricmp(strOriginObject.c_str(), "") != 0)
		{
			std::string strMoverName("");

			int colonPos = (int)strOriginObject.find_last_of(":");
			if(colonPos != -1)
			{
				strMoverName = strOriginObject.substr(0,colonPos);
				strMoverName = strMoverName.append(":mover");
			}

			HFBModel moverModel = RAGEFindModelByName((char*)strMoverName.c_str());
			if(moverModel != NULL)
			{
				FBVector3d scaleVector;
				moverModel->GetVector(scaleVector, kModelScaling, true);

				if((static_cast<float>(static_cast<int>(scaleVector.mValue[0] * 10.)) / 10.) > 1 || (static_cast<float>(static_cast<int>(scaleVector.mValue[1] * 10.)) / 10.) > 1 || (static_cast<float>(static_cast<int>(scaleVector.mValue[2] * 10.)) / 10.) > 1 )
				{
					bResult = false;
				}

				std::string strDummyName("");
				colonPos = (int)strOriginObject.find_last_of(":");
				if(colonPos != -1)
				{
					strDummyName = strOriginObject.substr(0,colonPos);
					strDummyName = strDummyName.append(":Dummy01");
				}

				HFBModel moverDummyModel = RAGEFindModelByName((char*)strDummyName.c_str());
				if(moverDummyModel != NULL)
				{
					FBVector3d dummyScaleVector;
					moverDummyModel->GetVector(dummyScaleVector, kModelScaling, true);

					if((static_cast<float>(static_cast<int>(dummyScaleVector.mValue[0] * 10.)) / 10.) > 1 || (static_cast<float>(static_cast<int>(dummyScaleVector.mValue[1] * 10.)) / 10.) > 1 || (static_cast<float>(static_cast<int>(dummyScaleVector.mValue[2] * 10.)) / 10.) > 1 )
					{
						bResult = false;
					}
				}
				else
				{
					bResult = false;
				}	
			}
			else
			{
				bResult = false;
			}

			if(!bResult)
			{
				char msgBuffer[MAX_PATH];
				sprintf(msgBuffer, "Origin object invalid. Use reference tool to merge in exportOrigin_Mover.fbx (characters/models/ingame) and use the exportOrigin_Mover:mover as origin object.");

				if(bShowDialogs)
				{	
					FBMessageBox("Error", msgBuffer, "OK");
				}
				else
				{
					Errorf("%s", msgBuffer);
				}

				return false;
			}
		}
	}
	
	return true;
}

bool RexAnimMarkupData::RemoveInputModel(const char* szModelName)
{
	for(atArray<RexCharAnimData*>::iterator it = m_characters.begin(); it != m_characters.end(); ++it)
	{
		RexCharAnimData* character = *(it);
		if( strcmp(character->GetInputModelName(0), szModelName) == 0)
		{
			m_characters.erase(it);
			return true;
		}
	}

	return false;
}

bool RexAnimMarkupData::RemoveMoverModel(const char* szMoverName)
{
	for(atArray<RexCharAnimData*>::iterator it = m_characters.begin(); it != m_characters.end(); ++it)
	{
		RexCharAnimData* character = *(it);
		if( strcmp(character->GetMoverModelName(0), szMoverName) == 0)
		{
			character->m_moverModelNames[0] = "";
			return true;
		}
	}

	return false;
}

bool RexAnimMarkupData::RemoveSegmentByTakeName( const char* szTakeName )
{
	for(atArray< RexMbAnimSegmentData* >::iterator it = m_segments.begin();
		it != m_segments.end();
		++it)
	{
		RexMbAnimSegmentData* pSegData = (*it);

		if( strcmp( pSegData->GetTakeName(), szTakeName ) == 0 )
		{
			delete pSegData;
			m_segments.erase( it );
			return true;
		}
	}

	return false;
}

char* RexAnimMarkupData::GetOutputFilePath(const RexCharAnimData* pCharData, const RexMbAnimSegmentData* pSegmentData, const int idxOutputPath)
{
	static char cFullOutputPath[RAGE_MAX_PATH]; 
	const char* cDividor = "\\";
	const char* cOutputPath;
	const char* cAltOutputPath;

	//If true, get the take output path setting from SegmentData.
	// If not ticked then get the output path from CharData.
	if(this->m_bUseTakeOutputPaths == true && pSegmentData->GetNumAltOutputPaths() > 0) 
	{ 
		cAltOutputPath = pSegmentData->GetAltOutputPath(idxOutputPath);
	}
	else { cAltOutputPath = pCharData->GetAltOutputPath(); }

	// Fallback on default output path if nothing specified
	if(strcmp(cAltOutputPath, "(none specified)") != 0)
	{
		cOutputPath = cAltOutputPath;
	}	
	else
	{
		cOutputPath = this->GetOutputPath();
	}

	cFullOutputPath[0] = 0;
	safecat( cFullOutputPath, cOutputPath, sizeof( cFullOutputPath ));
	safecat( cFullOutputPath, cDividor, sizeof( cFullOutputPath )); // for some reason safecat wont cat this so need to use strcat
	safecat( cFullOutputPath, pCharData->GetAnimPrefix(), sizeof( cFullOutputPath ));

	// Use the Take Name override string if it doesn't equal null or empty.
	const char* overrideName = pSegmentData->GetTakeNameOverride();
	if ( overrideName == NULL || strlen(overrideName) == 0)
	{
		safecat( cFullOutputPath, pSegmentData->GetTakeName(), sizeof( cFullOutputPath ));
	}
	else
	{
		safecat( cFullOutputPath,overrideName, sizeof( cFullOutputPath ));
	}

	safecat( cFullOutputPath, pCharData->GetAnimSuffix(), sizeof( cFullOutputPath ));
	return cFullOutputPath;
}







