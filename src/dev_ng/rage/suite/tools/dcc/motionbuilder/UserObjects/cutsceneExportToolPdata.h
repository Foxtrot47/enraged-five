// /cutsceneExportToolPdata.h

#ifndef __CUTSCENE_EXPORT_TOOL_PDATA_H__
#define __CUTSCENE_EXPORT_TOOL_PDATA_H__

#include "UserObjects/utility.h"

#pragma warning(disable:4481)

//-----------------------------------------------------------------------------

class RexRageCutsceneExportPData : public FBUserObject 
{
	FBClassDeclare(RexRageCutsceneExportPData, FBUserObject)
	FBDeclareUserObject(RexRageCutsceneExportPData);

public:
	RexRageCutsceneExportPData( const char* pName = NULL, HIObject pObject=NULL );

	 //--- FiLMBOX Construction/Destruction,
    virtual bool FBCreate();        //!< FiLMBOX Creation function.
    virtual void FBDestroy();       //!< FiLMBOX Destruction function.

    virtual bool FbxStore( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );
    virtual bool FbxRetrieve( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );

	FBString	m_Data;
};

//-----------------------------------------------------------------------------
EXTERN_C USER_OBJECT_API RexRageCutsceneExportPData* FindCutsceneMarkupData(bool bCreate);
EXTERN_C USER_OBJECT_API void	DeleteCutsceneMarkupData();

#endif //__CUTSCENE_EXPORT_TOOL_PDATA_H__
