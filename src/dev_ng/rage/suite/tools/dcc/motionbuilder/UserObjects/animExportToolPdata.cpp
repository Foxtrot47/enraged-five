// /animExportToolPdata.cpp
#include <string>
#include <atl/array.h>
#include <atl/string.h>
#include "Utility.h"

#include "animExportToolPdata.h"
#include "cutsceneExportToolPdata.h"

using namespace std;
using namespace rage;

//--- FiLMBOX Registration & Implementation.
FBClassImplementation(  RexRageAnimExportPData    );
FBUserObjectImplement(  RexRageAnimExportPData,
                        "Class objects used to hold RexRageAnimExportTool tool data.",
                        FB_DEFAULT_SDK_ICON     );                                          //Register UserObject class
FBElementClassImplementation( RexRageAnimExportPData, FB_DEFAULT_SDK_ICON );                //Register to the asset system

#define REX_ANIM_EXPORT_MARKUP_VERSION_2 "RexRageAnimExportTool2"
#define REX_ANIM_EXPORT_MARKUP_VERSION_3 "RexRageAnimExportTool3"

//-----------------------------------------------------------------------------

RexRageAnimExportPData::RexRageAnimExportPData( const char* pName, HIObject pObject )
: FBUserObject(pName, pObject)
{
	FBClassInit;
	m_Data = "";
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportPData::FBCreate()
{
    return true;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportPData::FBDestroy()
{
}

//-----------------------------------------------------------------------------
bool RexRageAnimExportPData::FbxStore( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat )
{
    if( pStoreWhat == kAttributes )
    {
		// Fix for FBX ASCII format, the markup data is broken if it is saved with double quotes in so we swap them in and out when
		// saving/loading
		std::string strData(m_Data);
		if((char*)m_Data != NULL)
		{
			UserObjectUtility::StringReplaceAll(strData, "\"", "*+~");

			atArray<atString> dataStrSplit;
			UserObjectUtility::SplitStringByLength( (char*)strData.c_str(), 4096, dataStrSplit );

			pFbxObject->FieldWriteBegin(REX_ANIM_EXPORT_MARKUP_VERSION_3);
			{
				pFbxObject->FieldWriteI( dataStrSplit.GetCount() );
				for ( int i = 0; i < dataStrSplit.GetCount(); ++i )
				{
					pFbxObject->FieldWriteC( const_cast<char *>( dataStrSplit[i].c_str() ) );
				}

			}

			pFbxObject->FieldWriteEnd();
		}
	}

    return true;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportPData::FbxRetrieve( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat )
{
    if( pStoreWhat == kAttributes )
    {
		RexAnimMarkupData* pAnimData = NULL;
		const char* pDataStr = NULL;
		if ( pFbxObject->FieldReadBegin(REX_ANIM_EXPORT_MARKUP_VERSION_2) )
		{
			int iCount = pFbxObject->FieldReadI();

			atArray<atString> fieldDataSplit;
			fieldDataSplit.Reserve( iCount );
			for ( int i = 0; i < iCount; ++i )
			{
				char* pFieldData = (char*)pFbxObject->FieldReadC();
				if ( pFieldData == NULL )
				{
					break;
				}

				fieldDataSplit.Append() = pFieldData;
			}

			m_Data = UserObjectUtility::ConcatenateStrings( fieldDataSplit );
			pFbxObject->FieldReadEnd();

			//Fix-up the m_Takes list at retrieve-time.  
			//We would ideally want to store Component References, although MotionBuilder doesn't appear to 
			//allow you to do that (yet?).  At least I ran into issues where take component references wouldn't
			//be properly saved.
			//
			RexMbAnimData* pOldAnimData = RexMbAnimData::LoadFromString( (char*)m_Data );
			if(pOldAnimData != NULL)
			{
				pAnimData = new RexAnimMarkupData();
				pAnimData->Convert(pOldAnimData);
				
				char *pConvertedData;
				pAnimData->SaveToString(&pConvertedData);
				m_Data = pConvertedData;
			}
		}
		else if ( pFbxObject->FieldReadBegin(REX_ANIM_EXPORT_MARKUP_VERSION_3) )
		{
			int iCount = pFbxObject->FieldReadI();

			atArray<atString> fieldDataSplit;
			fieldDataSplit.Reserve( iCount );
			for ( int i = 0; i < iCount; ++i )
			{
				char* pFieldData = (char*)pFbxObject->FieldReadC();
				if ( pFieldData == NULL )
				{
					break;
				}

				fieldDataSplit.Append() = pFieldData;
			}
			char* test1 = new char[5];
			delete [] test1;
			char* test = rage_new char[5];
			delete [] test;
			pDataStr = UserObjectUtility::ConcatenateStrings( fieldDataSplit );
			pFbxObject->FieldReadEnd();

			if ( (pDataStr != NULL) && (strlen( pDataStr ) > 0) )
			{
				// Fix for FBX ASCII format, the markup data is broken if it is saved with double quotes in so we swap them in and out when
				// saving/loading
				std::string strData(pDataStr);
				UserObjectUtility::StringReplaceAll(strData, "*+~", "\"");

				//Set the string data member of this userobject to the data loaded from the file
				m_Data = strData.c_str();

				delete [] pDataStr;
			}
			
			pAnimData = RexAnimMarkupData::LoadFromString( (char*)m_Data );
		}

		if(pAnimData)
			delete pAnimData;
    }

    return true;
}

//-----------------------------------------------------------------------------
//Global Functions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void FlushBogusAnimationMarkupData()
{
	// First we want to see if there is any infos for this tool present in the scene
	FBSystem m_System;
	HFBScene lScene = m_System.Scene;
	int lIdx = 0;

	for( lIdx = 0; lIdx < lScene->UserObjects.GetCount(); ++lIdx ) 
	{
		HFBUserObject lObject = lScene->UserObjects[lIdx];
		if( lObject->Is( RexRageAnimExportPData::TypeInfo ) ) 
		{
			RexRageAnimExportPData* pData =(RexRageAnimExportPData*)lObject;
			if( strcmp( pData->Name.AsString(), "RexRageAnimExportMarkup") != 0)
			{
				pData->FBDelete();
			}
		}
		else if(lObject->Is( RexRageCutsceneExportPData::TypeInfo ) )
		{
			RexRageCutsceneExportPData* pData =(RexRageCutsceneExportPData*)lObject;
			if( strcmp( pData->Name.AsString(), "RexRageCutsceneExportMarkup") != 0)
			{
				pData->FBDelete();
			}
		}
	}
}

RexRageAnimExportPData* FindAnimationMarkupData( bool bCreate )
{
	FlushBogusAnimationMarkupData();
	// First we want to see if there is any infos for this tool present in the scene
	FBSystem m_System;
	HFBScene lScene = m_System.Scene;
	int lIdx = 0;

	for( lIdx = 0; lIdx < lScene->UserObjects.GetCount(); ++lIdx ) 
	{
		HFBUserObject lObject = lScene->UserObjects[lIdx];
		if( lObject->Is( RexRageAnimExportPData::TypeInfo )) 
		{
			return (RexRageAnimExportPData*)lObject;
		}
	}

	RexRageAnimExportPData* lData = 0;

	if( bCreate ) 
	{
		lData = new RexRageAnimExportPData( "RexRageAnimExportMarkup" );
	}

	return lData;
}



//-----------------------------------------------------------------------------

void DeleteAnimationMarkupData()
{
	 // Importing other FBX files into the current FBX file will create duplicate persistent data fields.
	RexRageAnimExportPData* lData = FindAnimationMarkupData( false );
	while( lData ) 
	{
		lData->FBDelete();
		lData = FindAnimationMarkupData( false );
	}
}


//-----------------------------------------------------------------------------

RexAnimMarkupData* GetAnimationMarkupData()
{
	RexRageAnimExportPData* userObject = FindAnimationMarkupData( true );
	FBString dataStr = userObject->m_Data;

	if(dataStr.GetLen())
	{
		RexAnimMarkupData* pAnimData = RexAnimMarkupData::LoadFromString( (char*)dataStr );
		if ( pAnimData )
		{
			//Attached segments to their associated takes.
			if(pAnimData->m_segmentFromTakes == true)
			{
				int takeIndex = 0;
				for(atArray<RexMbAnimSegmentData*>::iterator iter = pAnimData->m_segments.begin(); iter != pAnimData->m_segments.end(); ++iter)
				{
					RexMbAnimSegmentData* segment = *(iter);

					HFBTake hTake;
					if(UserObjectUtility::FindTake(segment->m_takeName.c_str(), hTake))
					{
						segment->m_take = hTake;
					}

					//Ensure that this take still exists.  Users by now could have deleted it.
					if(segment->m_take != NULL && UserObjectUtility::TakeExists(segment->m_take) == true)
					{
						segment->m_takeName = segment->m_take->Name.AsString();
						takeIndex++;
					}
					else
					{
						pAnimData->m_segments.erase(iter);
						--iter;

						//Since we have removed a take object from m_Takes, we do not need to 
						//add a --takeIndex line.  
					}
				}
			}

			return pAnimData;
		}
	}

	return NULL;
}

//-----------------------------------------------------------------------------

void SetAnimationMarkupData(RexAnimMarkupData* pData)
{
	//Ensure that the Take Names of all the segments are correct.
	if ( pData->m_segmentFromTakes == true)
	{
		for(atArray<RexMbAnimSegmentData*>::iterator iter = pData->m_segments.begin(); iter != pData->m_segments.end(); ++iter)
		{
			RexMbAnimSegmentData* segment = *(iter);
			if(segment->m_take == NULL)
			{
				//This take was deleted or can not be found any longer.
				//Remove this segment.
				pData->m_segments.erase(iter);
				--iter;
			}
			else
			{
				segment->m_takeName = segment->m_take->Name.AsString();
			}
		}
	}

	char* pStrData = NULL;
	if( pData->SaveToString(&pStrData) )
	{
		// Clear out any old data fields
		DeleteAnimationMarkupData();

		RexRageAnimExportPData* userObject = FindAnimationMarkupData( true );
		userObject->m_Data = pStrData;

		delete [] pStrData;
	}
}
