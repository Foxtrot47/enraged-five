// 
// OpenRealitySDK.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __OPEN_REALITY_SDK_H__
#define __OPEN_REALITY_SDK_H__

#pragma warning(push)
#pragma warning(disable:4263)
#pragma warning(disable:4265)
#pragma warning(disable:4481)

#include <fbsdk/fbsdk.h>
#undef new

#pragma warning(pop)

typedef FBTake* HFBTake;
typedef FBModel* HFBModel;
typedef FBAnimationNode* HFBAnimationNode;
typedef FBProperty* HFBProperty;
typedef FBFbxObject* HFBFbxObject;
typedef FBScene* HFBScene;
typedef FBUserObject* HFBUserObject;
typedef FBShader* HFBShader;
typedef FBModelNull* HFBModelNull;
typedef FBBox* HFBBox;
typedef FBConstraint* HFBConstraint;
typedef FBFolder* HFBFolder;
typedef FBMaterial* HFBMaterial;
typedef FBStoryTrack* HFBStoryTrack;
typedef FBStoryFolder* HFBStoryFolder;
typedef FBStoryClip* HFBStoryClip;
typedef FBCamera* HFBCamera;
typedef FBGeometry* HFBGeometry;
typedef FBComponent* HFBComponent;
typedef FBLight* HFBLight;
typedef FBAudioClip* HFBAudioClip;
typedef FBSet* HFBSet;
typedef FBTexture* HFBTexture;

#endif //__OPEN_REALITY_SDK_H__