// /AnimMarkup.h

#ifndef __ANIM_MARKUP_H__
#define __ANIM_MARKUP_H__

#include <map>
#include <string>
#include <vector>

#include "atl/array.h"
#include "atl/string.h"

#include "parser/manager.h"

#include "OpenRealitySDK.h"

//#############################################################################

#define REXMB_ANIM_DATA_VERSION	1

//#############################################################################

//PURPOSE : Class to hold information about an animation segment to be exported.
class RexMbAnimSegmentData
{
public:
	//PURPOSE : Default constructor
	RexMbAnimSegmentData() 
		: m_bExport(true)
		, m_takeNameOverride( "" )
		, m_additiveBaseAnimation( "" )
		, m_bLooping(false)
		, m_bAutoplay(true)
		, m_iStartFrame(-1)
		, m_iEndFrame(-1)
		, m_bAdditive(false)
		, m_bAdditiveBaseAnim(false)
		, m_bLinearCompression(false)
		, m_ctrlFilePath( "(no spec file)" )
		, m_take(NULL)
		// Still no decision on whether we want to define defaults for these or force animators to make the decision
		// Tempting to put defaults in modulesettings 
		// NOTE: 1/13/2010 - Modulesettings is being deprecated and to make future integrations easier, this hack has been 
		// put in rather than supporting the modulesettings system for defaults.
#if HACK_MP3
		, m_compressionFilePath( "A_Compress.txt" )
#else
		, m_compressionFilePath( "Default_Compress.txt" )
#endif
		, m_skeletonFilePath( "player.skel" )
		, m_ambientFlags( 0 )
	{};

	//PURPOSE : Copy constructor
	RexMbAnimSegmentData(const RexMbAnimSegmentData& other)
		: m_segmentName(other.m_segmentName)
		, m_takeName(other.m_takeName)
		, m_takeNameOverride(other.m_takeNameOverride)
		, m_additiveBaseAnimation(other.m_additiveBaseAnimation)
		, m_take(other.m_take)
		, m_bExport(other.m_bExport)
		, m_bLooping(other.m_bLooping)
		, m_bAutoplay(other.m_bAutoplay)
		, m_iStartFrame(other.m_iStartFrame)
		, m_iEndFrame(other.m_iEndFrame)
		, m_bAdditive(other.m_bAdditive)
		, m_bAdditiveBaseAnim(other.m_bAdditiveBaseAnim)
		, m_bLinearCompression(other.m_bLinearCompression)
		, m_ctrlFilePath(other.m_ctrlFilePath)
		, m_compressionFilePath(other.m_compressionFilePath)
		, m_skeletonFilePath(other.m_skeletonFilePath)
		, m_ambientFlags(other.m_ambientFlags)
	{
		for(int i=0; i<other.m_inputModelNames.GetCount(); i++)
		{
			m_inputModelNames.PushAndGrow(other.m_inputModelNames[i]);
		}
		
		for(int i=0; i<other.m_moverModelNames.GetCount(); i++)
		{
			m_moverModelNames.PushAndGrow(other.m_moverModelNames[i]);
		}

		for(int i=0; i<other.m_altOutputPaths.GetCount(); i++)
		{
			m_altOutputPaths.PushAndGrow(other.m_altOutputPaths[i]);
		}
	}

	//PURPOSE : Destructor
	virtual ~RexMbAnimSegmentData()
	{
		m_inputModelNames.clear();
		m_moverModelNames.clear();
		m_altOutputPaths.clear();
	}

	//PURPOSE : Get the name of the segment
	const char*	GetSegmentName() const { return m_segmentName.c_str(); }

	//PURPOSE : Returns the take object.
	HFBTake		GetTake() const { return m_take; }

	//PURPOSE:  Gets the name of the associated take.
	const char *GetTakeName() const { return  m_takeName.c_str(); }

	//PURPOSE:  Gets the override name of the associated take.
	const char *GetTakeNameOverride() const { return  m_takeNameOverride.c_str(); }

	//PURPOSE:  Gets the base animation used for the additive blending.
	const char *GetAdditiveBaseAnimation() const { return m_additiveBaseAnimation.c_str(); }

	//PURPOSE : Get the number of input (root) models marked for the segment
	int	GetNumInputModels() const { return (int)m_inputModelNames.size(); }

	//PURPOSE : Get the name of an input model
	const char*	GetInputModelName(int idx) const { Assert(idx < (int)m_inputModelNames.size()); return m_inputModelNames[idx].c_str(); }

	//PURPOSE : Get the number of mover models marked for the segment
	int	GetNumMoverModels() const { return (int)m_moverModelNames.size(); }

	//PURPOSE : Get the name of a mover model
	const char*	GetMoverModelName(int idx) const { Assert(idx < (int)m_moverModelNames.size()); return m_moverModelNames[idx].c_str(); }

	//PURPOSE : Get the file path of the compression file
	const char* GetCompressionFile() const { return m_compressionFilePath.c_str(); }

	//PURPOSE : Returns if the segment should be exported or not
	bool IsMarkedForExport() const { return m_bExport; }

	//PURPOSE : Returns if the segment has been marked as a looping animation
	bool IsLooping() const { return m_bLooping; }

	//PURPOSE : Returns if the segment has been marked as an auto-play animation
	bool IsAutoPlay() const { return m_bAutoplay; }

	//PURPOSE : Returns the starting frame for the segment
	int	GetStartAsFrame() const { return m_iStartFrame; }

	//PURPOSE : Returns the ending frame for the segement
	int	GetEndAsFrame() const { return m_iEndFrame; }

	//PURPOSE : Returns if the segment has been marked as an additive animation
	bool IsAdditive() const { return m_bAdditive; }

	//PURPOSE : Returns if the segment has been marked as an additive animation versus a base animation
	bool IsAdditiveBA() const { return m_bAdditiveBaseAnim; }

	//PURPOSE : Returns if the segment has been marked for linear compression
	bool HasLinearCompression() const { return m_bLinearCompression; }

	//PURPOSE : Get the number of alt output paths for the segment
	int	GetNumAltOutputPaths() const { return (int)m_altOutputPaths.size(); }

	//PURPOSE : Returns an alt output path
	const char*	GetAltOutputPath(int idx) const { Assert(idx < (int)m_altOutputPaths.size()); return m_altOutputPaths[idx].c_str(); }

	rage::atString					m_segmentName;
	rage::atString					m_takeName;
	rage::atString					m_takeNameOverride;
	rage::atString					m_additiveBaseAnimation;
	HFBTake							m_take;
	rage::atArray<rage::atString>	m_inputModelNames;
	rage::atArray<rage::atString>	m_moverModelNames;

	bool				m_bExport;
	bool				m_bLooping;
	bool				m_bAutoplay;
	int					m_iStartFrame;
	int					m_iEndFrame;

	// Below should be surrouned by #if HACK_GTA4 BUT the parser can't handle that, so we need to have them all the time :(
	// #if HACK_GTA4
	bool							m_bAdditive;
	bool							m_bAdditiveBaseAnim;
	bool							m_bLinearCompression;
	rage::atString					m_ctrlFilePath;  // Control path override to define selection sets for export on a per animation basis
	rage::atString					m_compressionFilePath;
	rage::atString					m_skeletonFilePath;
	rage::atArray<rage::atString>	m_altOutputPaths;
	int								m_ambientFlags;
	// #endif // HACK_GTA4
	// Above should be surrouned by #if HACK_GTA4 BUT the parser can't handle that, so we need to have them all the time :(

	PAR_PARSABLE;
};

class RexAdditionalAnimData
{
public:
	//PURPOSE : Destructor
	virtual ~RexAdditionalAnimData()
	{ }

	rage::atString	m_trackType;
	rage::atString	m_attributeName;
	rage::atString  m_trackId;
	rage::atString	m_modelName;

	PAR_PARSABLE;
};

class RexCharAnimData
{
public:
	//PURPOSE : Default constructor
	RexCharAnimData() : m_animPrefix("")
		, m_animSuffix("")
		, m_skeleton("")
		, m_modelType("")
		, m_facialAttribute("")
		, m_charCtrlFile("(no spec file)")
		, m_altOutputPath("(none specified)")
		, m_bExport(true)
		, m_bMergeFace(false)
	{};

	//PURPOSE : Copy constructor
	//This has been moved to the header 
	RexCharAnimData(const RexCharAnimData& other) :
		m_animPrefix(other.m_animPrefix)
		, m_animSuffix(other.m_animSuffix)
		, m_skeleton(other.m_skeleton)
		, m_modelType(other.m_modelType)
		, m_facialAttribute(other.m_facialAttribute)
		, m_charCtrlFile(other.m_charCtrlFile)
		, m_altOutputPath(other.m_altOutputPath)
		, m_bExport(other.m_bExport)
		, m_bMergeFace(other.m_bMergeFace)
	{
		for(int i=0; i<other.m_inputModelNames.GetCount(); i++)
		{
			m_inputModelNames.PushAndGrow(other.m_inputModelNames[i]);
		}
		
		for(int i=0; i<other.m_moverModelNames.GetCount(); i++)
		{
			m_moverModelNames.PushAndGrow(other.m_moverModelNames[i]);
		}

		for(int i=0; i<other.m_additionalModels.GetCount(); i++)
		{
			m_additionalModels.PushAndGrow(other.m_additionalModels[i]);
		}
	}

	//PURPOSE : Destructor
	virtual ~RexCharAnimData()
	{ }

	int			GetNumInputModels() const { return (int) m_inputModelNames.size(); }
	const char* GetInputModelName(int index) const { return m_inputModelNames[index].c_str(); }

	int			GetNumMoverModels() const { return (int) m_moverModelNames.size(); }
	const char* GetMoverModelName(int index) const { return m_moverModelNames[index].c_str(); }

	const char* GetAnimPrefix() const { return m_animPrefix.c_str(); }
	const char* GetAnimSuffix() const { return m_animSuffix.c_str(); }
	const char* GetSkeletonFile() const { return m_skeleton.c_str(); }
	const char* GetModelType() const { return m_modelType.c_str(); }
	const char* GetFacialAttribute() const { return m_facialAttribute.c_str(); }
	const char* GetCtrlFile() const { return m_charCtrlFile.c_str(); }
	const char* GetAltOutputPath() const { return m_altOutputPath.c_str(); }

	bool		IsMarkedForExport() const { return m_bExport; }
	bool		IsMarkedForFaceMerge() const { return m_bMergeFace; }

	rage::atArray<rage::atString>	m_inputModelNames;
	rage::atArray<rage::atString>	m_moverModelNames;
	rage::atString m_animPrefix;
	rage::atString	m_animSuffix;
	rage::atString	m_skeleton;
	rage::atString	m_modelType;
	rage::atString m_facialAttribute;
	rage::atString m_charCtrlFile;
	rage::atString m_altOutputPath;
	rage::atArray< RexAdditionalAnimData* > m_additionalModels;

	bool			m_bExport;
	bool			m_bMergeFace;

	PAR_PARSABLE;
};


class IRexMbAnimData
{
	//PURPOSE : Saves the class out to an XML file
	//PARAMS : szFilePath - full path of the file to save the structure to
	virtual bool SaveToFile(const char* szFilePath) = 0;

	//PURPOSE : Saves the class to a string buffer
	//PARAMS : output - resulting string buffer.
	//NOTES : The method will allocate a new buffer to hold the serialized structure
	//in, but it is up to the caller to free this buffer
	virtual bool SaveToString(char** output) = 0;

	//PURPOSE : Returns the version of the class data
	virtual int	GetVersion() const = 0;

	//PURPOSE : Returns the full path to where the animation data should be written
	virtual const char*	GetOutputPath() const = 0;

	//PURPOSE : Returns the full path to the animation control file to be used when exporting
	//the animation
	virtual const char*	GetControlFilePath() const = 0;

	//PURPOSE : Returns the origin objects name
	virtual const char*	GetOriginObjectName() const = 0;

	//PURPOSE : Returns the number of input (root) models that have been marked for export
	virtual int	GetNumInputModels() const = 0;

	//PURPOSE : Returns the name of an input model given it's index
	virtual const char*	GetInputModelName(int idx) const  = 0;

	//PURPOSE : Removes the input model (and it's corresponding mover). Returns true if the model was removed, false otherwise
	virtual bool RemoveInputModel(const char* szModelName) = 0;

	//PURPOSE : Returns the number of mover models that have been marked for export
	virtual int	GetNumMoverModels() const  = 0;

	//PURPOSE : Returns the name of a mover model given it's index
	virtual const char*	GetMoverModelName(int idx) const  = 0;

	//PURPOSE : Removes all references to the mover. Returns true if the mover was removed, false otherwise
	virtual bool RemoveMoverModel(const char* szMoverName)  = 0;

	//PURPOSE : Returns the number of animtation prefix entries.  This should always equal the number of models and movers, with the 
	//			exception of legacy data, which won't have any prefixes available
	virtual int			GetNumAnimationPrefixes() const = 0;

	//PURPOSE : Get the animation prefix for the animations for a particular mover/model pair
	virtual const char* GetAnimationPrefix(int idx) const = 0;

	//PURPOSE : Returns the number of animtation suffix entries.  This should always equal the number of models and movers, with the 
	//			exception of legacy data, which won't have any prefixes available
	virtual int			GetNumAnimationSuffixes() const = 0;

	//PURPOSE : Get the animation suffix for the animations for a particular mover/model pair
	virtual const char* GetAnimationSuffix(int idx) const = 0;

	//PURPOSE : Returns the number of animtation suffix entries.  This should always equal the number of models and movers, with the 
	//			exception of legacy data, which won't have any prefixes available
	virtual int			GetNumSkeletonFiles() const = 0;

	//PURPOSE : Get the animation suffix for the animations for a particular mover/model pair
	virtual const char* GetSkeletonFile(int idx) const = 0;

	//PURPOSE : Return the number of segments marked for export
	virtual int	GetNumSegments() const = 0;

	//PURPOSE : Retrieve the segment data given it's index
	virtual RexMbAnimSegmentData*	GetSegment(int idx) const = 0;

	//PURPOSE : Removes a segment based on the specified take name.  Returns true if the segment was removed, false otherwise
	virtual bool RemoveSegmentByTakeName( const char* szTakeName ) = 0;

	//PURPOSE : Returns if the animation data has been set to be segmented by takes
	virtual bool GetSegmetFromTakes() const = 0;

	//PURPOSE : Validates that the supplied markup data is ok for use in export
	virtual bool	ValidateAnimExportData(bool bShowDialogs = false) = 0;

	//PURPOSE : Returns the full path to where the animation data should be written
	virtual const char*	GetProjectName() const = 0;
};

//PURPOSE : Class to hold markup information about how and what to export an animation  
//for from Motion Builder
//
//NOTE: This object has been deprecated in turn for RexAnimMarkupData due to a new data layout. 
//This is maintained for backward compatibility, where at some later time we'll be able to remove this.
//
class RexMbAnimData : public IRexMbAnimData
{
public:
	//PURPOSE : Default constructor
	RexMbAnimData() 
		: m_version(REXMB_ANIM_DATA_VERSION)
		, m_segmentFromTakes(false)
	{};

	//PURPOSE : Copy constructor
	//This has been moved to the header 
	RexMbAnimData(const RexMbAnimData& other)
		: m_version(other.m_version)
		, m_outputPath(other.m_outputPath)
		, m_originObjectName(other.m_originObjectName)
		, m_attributeFilePath(other.m_attributeFilePath)
		, m_ctrlFilePath(other.m_ctrlFilePath)
		, m_segmentFromTakes(other.m_segmentFromTakes)
		, m_useParentScale(other.m_useParentScale)
	{
		m_inputModelNames.insert( m_inputModelNames.begin(), other.m_inputModelNames.begin(), other.m_inputModelNames.end() );
		m_moverModelNames.insert( m_moverModelNames.begin(), other.m_moverModelNames.begin(), other.m_moverModelNames.end() );
		m_animPrefixes.insert( m_animPrefixes.begin(), other.m_animPrefixes.begin(), other.m_animPrefixes.end() );
		m_animSuffixes.insert( m_animSuffixes.begin(), other.m_animSuffixes.begin(), other.m_animSuffixes.end() );
		m_skeletons.insert( m_skeletons.begin(), other.m_skeletons.begin(), other.m_skeletons.end() );

		for(rage::atArray< RexMbAnimSegmentData* >::const_iterator it = other.m_segments.begin();
			it != other.m_segments.end();
			++it)
		{
			RexMbAnimSegmentData *pSegData = new RexMbAnimSegmentData( *(*it) );
			m_segments.PushAndGrow(pSegData);
		}
	}

	//PURPOSE : Destructor
	virtual ~RexMbAnimData()
	{
		for(rage::atArray< RexMbAnimSegmentData* >::iterator it = m_segments.begin();
			it != m_segments.end();
			++it)
		{
			delete (*it);
		}
	}

	//PURPOSE : Saves the class out to an XML file
	//PARAMS : szFilePath - full path of the file to save the structure to
	bool SaveToFile(const char* szFilePath);

	//PURPOSE : Saves the class to a string buffer
	//PARAMS : output - resulting string buffer.
	//NOTES : The method will allocate a new buffer to hold the serialized structure
	//in, but it is up to the caller to free this buffer
	bool SaveToString(char** output);

	//PURPOSE : Loads the class data from a string buffer
	static RexMbAnimData* LoadFromString(const char* szInput);

	//PURPOSE : Returns the version of the class data
	int	GetVersion() const { return m_version; }

	//PURPOSE : Returns the full path to where the animation data should be written
	const char*	GetOutputPath() const { return m_outputPath.c_str(); }

	//PURPOSE : Returns the full path to the animation control file to be used when exporting
	//the animation
	const char*	GetControlFilePath() const { return m_ctrlFilePath.c_str(); }

	//PURPOSE : Returns the origin objects name
	const char*	GetOriginObjectName() const { return m_originObjectName.c_str(); }

	//PURPOSE : Returns the full path to the attribute file used on facial animation
	const char*	GetAttributeFilePath() const { return m_attributeFilePath.c_str(); }

	//PURPOSE : Returns the number of input (root) models that have been marked for export
	int	GetNumInputModels() const { return (int)m_inputModelNames.size(); }

	//PURPOSE : Returns the name of an input model given it's index
	const char*	GetInputModelName(int idx) const { Assert(idx < (int)m_inputModelNames.size()); return m_inputModelNames[idx].c_str(); }

	//PURPOSE : Removes the input model (and it's corresponding mover). Returns true if the model was removed, false otherwise
	bool RemoveInputModel(const char* szModelName);

	//PURPOSE : Returns the number of mover models that have been marked for export
	int	GetNumMoverModels() const { return (int)m_moverModelNames.size(); }

	//PURPOSE : Returns the name of a mover model given it's index
	const char*	GetMoverModelName(int idx) const { Assert(idx < (int)m_moverModelNames.size()); return m_moverModelNames[idx].c_str(); }

	//PURPOSE : Removes all references to the mover. Returns true if the mover was removed, false otherwise
	bool RemoveMoverModel(const char* szMoverName);

	//PURPOSE : Returns the number of animation prefix entries.  This should always equal the number of models and movers, with the 
	//			exception of legacy data, which won't have any prefixes available
	int			GetNumAnimationPrefixes() const { return (int)m_animPrefixes.size(); }

	//PURPOSE : Get the animation prefix for the animations for a particular mover/model pair
	const char* GetAnimationPrefix(int idx) const { Assert(idx < (int)m_animPrefixes.size()); return m_animPrefixes[idx].c_str(); }

	//PURPOSE : Returns the number of animation suffix entries.  This should always equal the number of models and movers, with the 
	//			exception of legacy data, which won't have any prefixes available
	int			GetNumAnimationSuffixes() const { return (int)m_animSuffixes.size(); }


	//PURPOSE : Get the animation suffix for the animations for a particular mover/model pair
	const char* GetAnimationSuffix(int idx) const { Assert(idx < (int)m_animSuffixes.size()); return m_animSuffixes[idx].c_str(); }

	//PURPOSE : Returns the number of skeleton entries.  This should always equal the number of models and movers, with the 
	//			exception of legacy data, which won't have any prefixes available
	int			GetNumSkeletonFiles() const { return (int)m_skeletons.size(); }

	//PURPOSE : Get the animation suffix for the animations for a particular mover/model pair
	const char* GetSkeletonFile(int idx) const { Assert(idx < (int)m_skeletons.size()); return m_skeletons[idx].c_str(); };

	//PURPOSE : Return the number of segments marked for export
	int	GetNumSegments() const { return (int)m_segments.size(); }

	//PURPOSE : Retrieve the segment data given it's index
	RexMbAnimSegmentData*	GetSegment(int idx) const { Assert(idx < (int)m_segments.size()); return m_segments[idx]; }

	//PURPOSE : Removes a segment based on the specified take name.  Returns true if the segment was removed, false otherwise
	bool RemoveSegmentByTakeName( const char* szTakeName );

	//PURPOSE : Returns if the animation data has been set to be segmented by takes
	bool GetSegmetFromTakes() const { return m_segmentFromTakes; }

	//PURPOSE : Validates that the supplied markup data is ok for use in export
	bool	ValidateAnimExportData(bool bShowDialogs = false);

	//PURPOSE : Returns the full path to where the animation data should be written
	const char*	GetProjectName() const { return m_project.c_str(); }

	int										m_version;
	rage::atString								m_outputPath;
	rage::atString								m_ctrlFilePath;
	rage::atString								m_originObjectName;
	rage::atString								m_attributeFilePath;

	rage::atArray< rage::atString >				m_inputModelNames;
	rage::atArray< rage::atString >				m_moverModelNames;
	rage::atArray< rage::atString >				m_animPrefixes;
	rage::atArray< rage::atString >				m_animSuffixes;
	rage::atArray< rage::atString >				m_skeletons;

	bool									m_segmentFromTakes;
	rage::atArray< RexMbAnimSegmentData* >	m_segments;
	bool									m_useParentScale;

	rage::atString								m_project;

	PAR_PARSABLE;
};

class IAnimationMarkupData
{
	//PURPOSE : Saves the class out to an XML file
	//PARAMS : szFilePath - full path of the file to save the structure to
	virtual bool SaveToFile(const char* szFilePath) = 0;

	//PURPOSE : Saves the class to a string buffer
	//PARAMS : output - resulting string buffer.
	//NOTES : The method will allocate a new buffer to hold the serialized structure
	//in, but it is up to the caller to free this buffer
	virtual bool SaveToString(char** output) = 0;

	//PURPOSE : Returns the version of the class data
	virtual int	GetVersion() const = 0;

	//PURPOSE : Returns the full path to where the animation data should be written
	virtual const char*	GetOutputPath() const = 0;

	//PURPOSE : Returns the full path to the animation control file to be used when exporting
	//the animation
	virtual const char*	GetControlFilePath() const = 0;

	//PURPOSE : Returns the number of input (root) models that have been marked for export
	virtual int	GetNumCharacters() const = 0;

	//PURPOSE : Returns the character in the given index.
	virtual RexCharAnimData*	GetCharacter(int idx) const  = 0;

	//PURPOSE : Removes the input model (and it's corresponding mover). Returns true if the model was removed, false otherwise
	virtual bool RemoveInputModel(const char* szModelName) = 0;

	//PURPOSE : Removes all references to the mover. Returns true if the mover was removed, false otherwise
	virtual bool RemoveMoverModel(const char* szMoverName)  = 0;

	//PURPOSE : Return the number of segments marked for export
	virtual int	GetNumSegments() const = 0;

	//PURPOSE : Retrieve the segment data given it's index
	virtual RexMbAnimSegmentData*	GetSegment(int idx) const = 0;

	//PURPOSE : Removes a segment based on the specified take name.  Returns true if the segment was removed, false otherwise
	virtual bool RemoveSegmentByTakeName( const char* szTakeName ) = 0;

	//PURPOSE : Returns if the animation data has been set to be segmented by takes
	virtual bool GetSegmetFromTakes() const = 0;

	//PURPOSE : Validates that the supplied markup data is ok for use in export
	virtual bool	ValidateAnimExportData(bool bShowDialogs = false) = 0;

	//PURPOSE : Returns the full path to where the animation data should be written
	virtual const char*	GetProjectName() const = 0;
};

//PURPOSE : Class to hold markup information about how and what to export an animation  
//for from Motion Builder
class RexAnimMarkupData : public IAnimationMarkupData
{
public:
	//PURPOSE : Default constructor
	RexAnimMarkupData() 
		: m_version(REXMB_ANIM_DATA_VERSION)
		, m_segmentFromTakes(false)
		, m_bUseTakeOutputPaths(false)
		, m_bUseInitOffset(false)
		, m_bUsedInNMBlend(false)
		, m_bStoryMode(false)
		, m_bEnableCameraDOF(false)
	{};

	//PURPOSE : Conversion function to the new format.  The old format is specified in argument.
	void Convert(const RexMbAnimData* other)
	{
		m_project = other->m_project;
		m_version = other->m_version;
		m_outputPath = other->m_outputPath;
		m_originObjectName = other->m_originObjectName;
		m_attributeFilePath = other->m_attributeFilePath;
		m_ctrlFilePath = other->m_ctrlFilePath;
		m_segmentFromTakes = other->m_segmentFromTakes;
		m_useParentScale = other->m_useParentScale;
		m_bUseTakeOutputPaths = false;	// Won't exist in old format files
		m_bStoryMode = false;	// Won't exist in old format files
		m_bEnableCameraDOF = false; // Won't exist in old format files

		int numModels = other->GetNumInputModels();
		for(int modelIndex = 0; modelIndex < numModels; ++modelIndex)
		{
			RexCharAnimData* charData = new RexCharAnimData();

			charData->m_inputModelNames.clear();
			charData->m_moverModelNames.clear();

			charData->m_inputModelNames.PushAndGrow( other->m_inputModelNames[modelIndex] );
			charData->m_moverModelNames.PushAndGrow( other->m_moverModelNames[modelIndex] );

			if( other->m_animPrefixes.size() > 0)
				charData->m_animPrefix = other->GetAnimationPrefix(modelIndex);

			if( other->m_animSuffixes.size() > 0)
				charData->m_animSuffix = other->GetAnimationSuffix(modelIndex);

			if( other->m_skeletons.size() > 0)
				charData->m_skeleton = other->GetSkeletonFile(modelIndex);

			m_characters.PushAndGrow(charData);
		}

		for(rage::atArray< RexMbAnimSegmentData* >::const_iterator it = other->m_segments.begin();
			it != other->m_segments.end();
			++it)
		{
			RexMbAnimSegmentData *pSegData = new RexMbAnimSegmentData( *(*it) );
			m_segments.PushAndGrow(pSegData);
		}
	}

	//PURPOSE : Copy constructor
	//This has been moved to the header 
	RexAnimMarkupData(const RexAnimMarkupData& other)
		: m_version(other.m_version)
		, m_outputPath(other.m_outputPath)
		, m_ctrlFilePath(other.m_ctrlFilePath)
		, m_originObjectName(other.m_originObjectName)
		, m_attributeFilePath(other.m_attributeFilePath)
		, m_segmentFromTakes(other.m_segmentFromTakes)
		, m_useParentScale(other.m_useParentScale)
		, m_bUseTakeOutputPaths(other.m_bUseTakeOutputPaths)
		, m_bUseInitOffset(other.m_bUseInitOffset)
		, m_bUsedInNMBlend(other.m_bUsedInNMBlend)
		, m_bStoryMode(other.m_bStoryMode)
		, m_bEnableCameraDOF(other.m_bEnableCameraDOF)
	{
		for(rage::atArray< RexCharAnimData* >::const_iterator it = other.m_characters.begin();
			it != other.m_characters.end();
			++it)
		{
			RexCharAnimData *pCharData = new RexCharAnimData( *(*it) );
			m_characters.PushAndGrow(pCharData);
		}

		for(rage::atArray< RexMbAnimSegmentData* >::const_iterator it = other.m_segments.begin();
			it != other.m_segments.end();
			++it)
		{
			RexMbAnimSegmentData *pSegData = new RexMbAnimSegmentData( *(*it) );
			m_segments.PushAndGrow(pSegData);
		}
	}

	//PURPOSE : Destructor
	virtual ~RexAnimMarkupData()
	{
		for(rage::atArray< RexCharAnimData* >::iterator it = m_characters.begin();
			it != m_characters.end();
			++it)
		{
			delete (*it);
		}

		for(rage::atArray< RexMbAnimSegmentData* >::iterator it = m_segments.begin();
			it != m_segments.end();
			++it)
		{
			delete (*it);
		}
	}

	//PURPOSE : Saves the class out to an XML file
	//PARAMS : szFilePath - full path of the file to save the structure to
	bool SaveToFile(const char* szFilePath);

	//PURPOSE : Saves the class to a string buffer
	//PARAMS : output - resulting string buffer.
	//NOTES : The method will allocate a new buffer to hold the serialized structure
	//in, but it is up to the caller to free this buffer
	bool SaveToString(char** output);

	//PURPOSE : Loads the class data from a string buffer
	static RexAnimMarkupData* LoadFromString(const char* szInput);

	//PURPOSE : Returns the version of the class data
	int	GetVersion() const { return m_version; }

	//PURPOSE : Returns the full path to where the animation data should be written
	const char*	GetOutputPath() const { return m_outputPath.c_str(); }
		
	//PURPOSE : Returns the full path to the animation control file to be used when exporting
	//the animation
	const char*	GetControlFilePath() const { return m_ctrlFilePath.c_str(); }

	//PURPOSE : Returns the origin object name
	const char*	GetOriginObjectName() const { return m_originObjectName.c_str(); }

	//PURPOSE : Returns the full path to the attribute file used when exporting facial animation
	const char*	GetAttributeFilePath() const { return m_attributeFilePath.c_str(); }

	//PURPOSE : Returns the number of input (root) models that have been marked for export
	int	GetNumCharacters() const { return (int)m_characters.size(); }

	//PURPOSE : Returns the character in the given index.
	RexCharAnimData*	GetCharacter(int idx) const  { Assert(idx < (int)m_characters.size()); return m_characters[idx]; }

	//PURPOSE : Removes the input model (and it's corresponding mover). Returns true if the model was removed, false otherwise
	bool RemoveInputModel(const char* szModelName);

	//PURPOSE : Removes all references to the mover. Returns true if the mover was removed, false otherwise
	bool RemoveMoverModel(const char* szMoverName);

	//PURPOSE : Return the number of segments marked for export
	int	GetNumSegments() const { return (int)m_segments.size(); }

	//PURPOSE : Retrieve the segment data given it's index
	RexMbAnimSegmentData*	GetSegment(int idx) const { Assert(idx < (int)m_segments.size()); return m_segments[idx]; }

	//PURPOSE : Removes a segment based on the specified take name.  Returns true if the segment was removed, false otherwise
	bool RemoveSegmentByTakeName( const char* szTakeName );

	//PURPOSE : Returns if the animation data has been set to be segmented by takes
	bool GetSegmetFromTakes() const { return m_segmentFromTakes; }

	//PURPOSE : Validates that the supplied markup data is ok for use in export
	bool	ValidateAnimExportData(bool bShowDialogs = false);

	//PURPOSE : Returns the full path to where the animation data should be written
	const char*	GetProjectName() const { return m_project.c_str(); }

	int										m_version;
	rage::atString								m_outputPath;
	rage::atString								m_ctrlFilePath;
	rage::atString								m_originObjectName;
	rage::atString								m_attributeFilePath;

	rage::atArray< RexCharAnimData* >			m_characters;

	bool									m_segmentFromTakes;
	rage::atArray< RexMbAnimSegmentData* >	m_segments;
	bool									m_useParentScale;
	bool									m_bUseTakeOutputPaths;
	bool									m_bUseInitOffset;
	bool									m_bUsedInNMBlend;
	bool									m_bStoryMode;
	bool									m_bEnableCameraDOF;
	bool									m_bRenderMode;

	rage::atString								m_project;
	rage::atString								m_projectRoot;

	PAR_PARSABLE;
	
private:
	char* GetOutputFilePath(const RexCharAnimData* pCharData, const RexMbAnimSegmentData* pSegmentData, const int idxOutputPath);

};

#endif //__ANIM_MARKUP_H__
