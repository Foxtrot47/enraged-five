<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<structdef type="::RexMbAnimSegmentData">
	<string name="m_segmentName" noInit="true" type="atString"/>
	<string name="m_takeName" noInit="true" type="atString"/>
	<string init="" name="m_takeNameOverride" noInit="true" type="atString"/>
  <string init="" name="m_additiveBaseAnimation" noInit="true" type="atString"/>
  <array name="m_inputModelNames" type="atArray">
	<string type="atString"/>
	</array>
	<array name="m_moverModelNames" type="atArray">
	<string type="atString"/>
	</array>
	<bool init="true" name="m_bExport"/>
	<bool init="true" name="m_bLooping"/>
	<bool init="true" name="m_bAutoplay"/>
	<int init="-1" name="m_iStartFrame"/>
	<int init="-1" name="m_iEndFrame"/>
	<bool init="false" name="m_bAdditive"/>
  <bool init="false" name="m_bAdditiveBaseAnim"/>
	<bool init="false" name="m_bLinearCompression"/>
	<string name="m_ctrlFilePath" noInit="true" type="atString"/>
	<string name="m_compressionFilePath" noInit="true" type="atString"/>
	<int init="0" name="m_ambientFlags"/>
	<array name="m_altOutputPaths" type="atArray">
	<string type="atString"/>
	</array>
</structdef>

<structdef type="::RexAdditionalAnimData">
	<string name="m_trackType" noInit="true" type="atString"/>
	<string name="m_attributeName" noInit="true" type="atString"/>
	<string name="m_modelName" noInit="true" type="atString"/>
</structdef>

<structdef type="::RexCharAnimData">
	<array name="m_inputModelNames" type="atArray">
	<string type="atString"/>
	</array>
	<array name="m_moverModelNames" type="atArray">
	<string type="atString"/>
	</array>
	<string name="m_animPrefix" type="atString"/>
	<string name="m_animSuffix" type="atString"/>
	<string name="m_skeleton" type="atString"/>
	<string name="m_modelType" type="atString"/>
	<string name="m_facialAttribute" type="atString"/>
	<string name="m_charCtrlFile" type="atString"/>
	<string name="m_altOutputPath" type="atString"/>
	<bool init="true" name="m_bExport"/>
  <bool init="false" name="m_bMergeFace"/>
	<array name="m_additionalModels" type="atArray">
	<pointer policy="owner" type="::RexAdditionalAnimData"/>
	</array>
</structdef>

<structdef type="RexMbAnimData">
	<int init="false" name="m_version"/>
	<string name="m_outputPath" type="atString"/>
	<string name="m_ctrlFilePath" type="atString"/>
	<string name="m_originObjectName" type="atString"/>
	<string name="m_attributeFilePath" type="atString"/>
	<array name="m_inputModelNames" type="atArray">
	<string type="atString"/>
	</array>
	<array name="m_moverModelNames" type="atArray">
	<string type="atString"/>
	</array>
	<array name="m_animPrefixes" type="atArray">
	<string type="atString"/>
	</array>
	<array name="m_animSuffixes" type="atArray">
	<string type="atString"/>
	</array>
	<array name="m_skeletons" type="atArray">
	<string type="atString"/>
	</array>
	<bool name="m_segmentFromTakes"/>
	<array name="m_segments" type="atArray">
	<pointer policy="owner" type="::RexMbAnimSegmentData"/>
	</array>
	<string name="m_project" type="atString"/>
</structdef>

<structdef type="RexAnimMarkupData">
	<int init="false" name="m_version"/>
	<string name="m_outputPath" type="atString"/>
	<string name="m_ctrlFilePath" type="atString"/>
	<string name="m_originObjectName" type="atString"/>
	<string name="m_attributeFilePath" type="atString"/>
	<array name="m_characters" type="atArray">
	<pointer policy="owner" type="::RexCharAnimData"/>
	</array>
	<bool name="m_segmentFromTakes"/>
	<array name="m_segments" type="atArray">
	<pointer policy="owner" type="::RexMbAnimSegmentData"/>
	</array>
	<string name="m_project" type="atString"/>
	<bool name="m_bUseInitOffset"/>
	<bool name="m_bUsedInNMBlend"/>
	<bool name="m_bUseTakeOutputPaths"/>
	<bool name="m_bStoryMode"/>
  <bool name="m_bEnableCameraDOF"/>
  <bool name="m_bRenderMode"/>
</structdef>

</ParserSchema>