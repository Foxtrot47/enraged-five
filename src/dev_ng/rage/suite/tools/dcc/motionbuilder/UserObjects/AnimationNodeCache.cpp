// 
// UserObjects/AnimationNodeCache.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "UserObjects\AnimationNodeCache.h"
#include "UserObjects\Utility.h"
#include "atl\map.h"

using namespace rage;

AnimationNodeCache::AnimationNodeCache() { }
AnimationNodeCache::~AnimationNodeCache() 
{
	Delete();
}

HFBAnimationNode AnimationNodeCache::Get(HFBTake hTake, HFBProperty hProperty)
{
	if ( hTake == NULL || hProperty == NULL)
		return NULL;

	if ( hProperty->IsAnimatable() == false ) 
		return NULL;

	PropertyNodeMap** pPropertyNodeMapPtr = m_AnimationNodeCache.Access(hTake);
	if(pPropertyNodeMapPtr == NULL)
	{
		PropertyNodeMap* pPropertyNodeMap = new PropertyNodeMap();

		//Create this entry in the map first.
		m_AnimationNodeCache.Insert(hTake, pPropertyNodeMap);

		//Correct the pointer to point to the newly allocated map.
		pPropertyNodeMapPtr = &pPropertyNodeMap;
	}

	PropertyNodeMap* pPropertyNodeMap = *(pPropertyNodeMapPtr);
	Assert(pPropertyNodeMap);

	PropertyNodeInfo* propertyNodeInfo = pPropertyNodeMap->Access(hProperty);
	HFBAnimationNode pAnimationNode = NULL;

	//If the property node information has not been found, or if the property node needs to be updated
	//and the request is for the current take.
	if ( propertyNodeInfo == NULL 
			|| ( propertyNodeInfo->m_Status == NEEDS_UPDATE && m_System.CurrentTake == hTake ) ) 
	{
		//Given the take and the property, acquire the AnimationNode for that property. 
		HFBTake activeTake = m_System.CurrentTake;
		if ( m_System.CurrentTake != hTake ) 
		{
			activeTake = m_System.CurrentTake;
			m_System.CurrentTake = hTake;
		}

		HFBPropertyAnimatable lAnimatableProperty = (HFBPropertyAnimatable)hProperty;
		HFBAnimationNode      lNode = lAnimatableProperty->GetAnimationNode(hTake);
		if ( lNode != NULL )
		{
			PropertyNodeInfo newInfo;
			newInfo.m_Status = UPDATED;
			newInfo.m_AnimationNode = lNode;

			pPropertyNodeMap->Insert(hProperty, newInfo);
		}

		if ( m_System.CurrentTake != activeTake )
		{
			m_System.CurrentTake = activeTake;
		}

		PropertyNodeInfo* propertyNodeInfo = pPropertyNodeMap->Access(hProperty);
		pAnimationNode = propertyNodeInfo->m_AnimationNode;
	}
	else
	{
		pAnimationNode = propertyNodeInfo->m_AnimationNode;
	}

	return pAnimationNode;
}

void AnimationNodeCache::QuickCacheUpdate(HFBTake hTake, HFBProperty hProperty)
{
	PropertyNodeMap** pPropertyNodeMapPtr = m_AnimationNodeCache.Access(hTake);
	if(pPropertyNodeMapPtr == NULL)
	{
		PropertyNodeMap* pPropertyNodeMap = new PropertyNodeMap();

		//Create this entry in the map first.
		m_AnimationNodeCache.Insert(hTake, pPropertyNodeMap);

		//Correct the pointer to point to the newly allocated map.
		pPropertyNodeMapPtr = &pPropertyNodeMap;
	}

	PropertyNodeMap* pPropertyNodeMap = *(pPropertyNodeMapPtr);
	Assert(pPropertyNodeMap);

	PropertyNodeInfo* propertyNodeInfo = pPropertyNodeMap->Access(hProperty);

	if ( propertyNodeInfo == NULL ) 
	{
		//Mark this node as needing to be updated.
		PropertyNodeInfo newInfo;
		newInfo.m_Status = NEEDS_UPDATE;
		newInfo.m_AnimationNode = NULL;

		pPropertyNodeMap->Insert(hProperty, newInfo);
	}

	//If the PropertyNodeInfo is already available, then do nothing.
}

//TODO: Update when a take is deleted?  Ultimately it shouldn't matter -- the  memory shouldn't be 
//accessed.

void AnimationNodeCache::UpdateProperty(HFBTake hTake, HFBProperty hProperty, PropertyChangeEnum eAction)
{
	if ( hTake == NULL || hProperty == NULL)
		return;

	if( eAction == PROPERTY_REMOVED )
	{
		PropertyNodeMap** pPropertyNodeMapPtr = m_AnimationNodeCache.Access(hTake);
		if(pPropertyNodeMapPtr == NULL)
			return;

		PropertyNodeMap* pPropertyNodeMap = *(pPropertyNodeMapPtr);
		Assert(pPropertyNodeMap);

		pPropertyNodeMap->Delete(hProperty);
	}
}

void AnimationNodeCache::ProcessTake()
{
	if (m_ProcessedTakes.Find(m_System.CurrentTake) >= 0)
	{
		return;
	}

	m_ProcessedTakes.PushAndGrow(m_System.CurrentTake);

	FBPropertyListComponent components = m_System.Scene->Components;
	int componentCount = components.GetCount();
	for(int componentIndex = 0; componentIndex < componentCount; ++componentIndex)
	{
		HFBComponent component = components.GetAt(componentIndex);
		if ( component->Is(FBModel::TypeInfo) )
		{
			int propertyCount = component->PropertyList.GetCount();
			for(int propertyIndex = 0; propertyIndex < propertyCount; ++propertyIndex)
			{
				HFBProperty property = component->PropertyList[propertyIndex];
				if (property->IsAnimatable() == true)
				{
					HFBPropertyAnimatable lAnimatableProperty = (HFBPropertyAnimatable)property;

					if(lAnimatableProperty->IsUserProperty() == false)
						continue;

					if(lAnimatableProperty->IsAnimated() == false)
						continue;

					Get(m_System.CurrentTake, property);
				}
			}
		}
	}
}

void AnimationNodeCache::Delete()
{
	TakePropertyNodeMap::Iterator iter = m_AnimationNodeCache.CreateIterator();
	for ( iter.Start(); iter.AtEnd() == false; iter.Next() )
	{
		PropertyNodeMap* propertyNodeMap = iter.GetData();
		if ( propertyNodeMap != NULL )
		{
			propertyNodeMap->Kill();
			delete propertyNodeMap;
		}
	}

	m_AnimationNodeCache.Kill();
	m_ProcessedTakes.Reset();
}