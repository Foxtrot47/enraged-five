// 
// Utility.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
//#include "system/new.h"
#include "atl/array.h"
#include "Utility.h"

#include "OpenRealitySDK.h"

#include <string>

#include "system\exec.h"
#include "file/limits.h"
#include "file/asset.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------
// Implemented to replace FBFindModelByName which is deprecated in MB2012. This function acts like FBFindModelByName by using
// FBFindObjectByName, this does two checks as RAGEFindModelByName will find the model with or without namespace.
HFBModel RAGEFindModelByName(const char* pModelName)
{
	HFBModel pModel = NULL;

	FBComponentList pList;
	FBFindObjectsByName(pModelName, pList, true, true);

	if(pList.GetCount() == 0)
	{
		FBFindObjectsByName(pModelName, pList, false, true);
		if(pList.GetCount() != 0)
		{
			pModel = (HFBModel)pList.GetAt(0);
		}
	}
	else
	{
		pModel = (HFBModel)pList.GetAt(0);
	}

	return pModel;
}

//-----------------------------------------------------------------------------
void UserObjectUtility::StringReplaceAll(std::string& str, const std::string& from, const std::string& to) 
{     
	size_t start_pos = 0;     
	while((start_pos = str.find(from, start_pos)) != std::string::npos) 
	{         
		str.replace(start_pos, from.length(), to);         
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'     
	}
}

//-----------------------------------------------------------------------------
void UserObjectUtility::SplitStringByLength( const char *pInput, int size, atArray<atString> &output )
{
	int iTotalLen = (int)strlen( pInput );
	if ( iTotalLen < size )
	{
		output.Grow() = pInput;
		return;
	}

	char *pBuffer = rage_new char[size];

	char *pPtr = const_cast<char *>( pInput );
	int iLen = iTotalLen;
	while ( iLen > 0 )
	{
		if ( iLen >= size )
		{
			iLen = size - 1; 
		}

		safecpy( pBuffer, pPtr, iLen + 1 );
		output.Grow() = pBuffer;

		pPtr += iLen;
		iLen = (int)strlen( pPtr );
	}

	delete [] pBuffer;
}

//-----------------------------------------------------------------------------

const char* UserObjectUtility::ConcatenateStrings( const atArray<atString> &input )
{
	int iTotalLen = 1;  // for the null-terminator
	for ( int i = 0; i < input.GetCount(); ++i )
	{
		iTotalLen += input[i].GetLength();
	}

	char *pOutput = rage_new char[iTotalLen];
	pOutput[0] = 0;

	for ( int i = 0; i < input.GetCount(); ++i )
	{
		strcat( pOutput, input[i].c_str() );
	}

	return pOutput;
}

//-----------------------------------------------------------------------------

//PURPOSE : This method will take in a string name of a take, and locate
//the actual structure for that take in the current scene.
bool UserObjectUtility::FindTake(const char* szTakeName, HFBTake& retTake)
{
	FBSystem m_system;
	HFBScene hScene = m_system.Scene;

	int nTakes = hScene->Takes.GetCount();
	for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
	{
		HFBTake hTake = hScene->Takes[takeIdx];
		const char* szCurTakeName = hTake->Name.AsString();

		if(strcmp(szTakeName, szCurTakeName) == 0)
		{
			retTake = hTake;
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

//PURPOSE : This method will take in a string name of a take, and locate
//the actual structure for that take in the current scene.
bool UserObjectUtility::TakeExists(const HFBTake take)
{
	FBSystem m_system;
	HFBScene hScene = m_system.Scene;

	int nTakes = hScene->Takes.GetCount();
	for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
	{
		HFBTake hSceneTake = hScene->Takes[takeIdx];

		if(take == hSceneTake)
		{
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

std::string UserObjectUtility::GetAnimExportMarkupFilePath()
{
	string retMarkupFilePath;

	FBApplication fbApp;

	char cFbxFilename[RAGE_MAX_PATH];
	safecpy( cFbxFilename, fbApp.FBXFileName.AsString(), sizeof(cFbxFilename) );
	if ( (int)strlen(cFbxFilename) > 0 )
	{
		char cTempFilename[RAGE_MAX_PATH];

		char cTempDir[RAGE_MAX_PATH];
		if ( sysGetEnv( "TEMP", cTempDir, sizeof(cTempDir) ) )
		{
			sprintf( cTempFilename, "%s\\%s", cTempDir, ASSET.FileName( cFbxFilename ) );
		}
		else
		{
			sprintf( cTempFilename, "c:\\%s", ASSET.FileName( cFbxFilename ) );
		}

		ASSET.RemoveExtensionFromPath( cTempFilename, sizeof(cTempFilename), cTempFilename );
		safecat( cTempFilename, ".rexmb", sizeof(cTempFilename) );

		retMarkupFilePath = cTempFilename;
	}

	return retMarkupFilePath;
}

//-----------------------------------------------------------------------------
static bool s_HasLoadedClips = false;
bool HasLoadedClips()
{
	return s_HasLoadedClips;
}

//-----------------------------------------------------------------------------

void SetHasLoadedClips(bool set)
{
	s_HasLoadedClips = set;
}


//-----------------------------------------------------------------------------
static bool s_HasReloadedClips = false;
bool HasReloadedClips()
{
	return s_HasReloadedClips;
}

//-----------------------------------------------------------------------------

void SetHasReloadedClips(bool set)
{
	s_HasReloadedClips = set;
}

