// 
// UserObjects/AnimationNodeCache.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __ANIMATION_NODE_CACHE_H__
#define __ANIMATION_NODE_CACHE_H__

#include "atl\map.h"
#include "atl\array.h"
#include "UserObjects\OpenRealitySDK.h"
#include "UserObjects\Utility.h"

class AnimationNodeCache
{
public:
	AnimationNodeCache();
	~AnimationNodeCache();

	HFBAnimationNode Get(HFBTake hTake, HFBProperty property);
	void UpdateProperty(HFBTake hTake, HFBProperty hProperty, PropertyChangeEnum eAction);
	void QuickCacheUpdate(HFBTake hTake, HFBProperty hProperty);

	void ProcessTake();
	void Delete();

private:
	enum AnimationNodeStatus
	{
		UPDATED,
		NEEDS_UPDATE,
		UNKNOWN
	};

	struct PropertyNodeInfo
	{
		PropertyNodeInfo() : m_AnimationNode(NULL), m_Status(UNKNOWN) { }
		HFBAnimationNode m_AnimationNode;
		AnimationNodeStatus m_Status;
	};

	typedef rage::atMap<HFBProperty, PropertyNodeInfo> PropertyNodeMap;
	typedef rage::atMap<HFBTake, PropertyNodeMap*> TakePropertyNodeMap;
	TakePropertyNodeMap m_AnimationNodeCache;
	rage::atArray<HFBTake> m_ProcessedTakes;

	FBSystem m_System;
};
#endif //__ANIMATION_NODE_CACHE_H__