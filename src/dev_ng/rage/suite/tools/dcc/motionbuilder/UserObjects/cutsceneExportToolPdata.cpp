// /cutsceneExportToolPdata.cpp
//#include "system/new.h"
#include "atl/array.h"
#include <string>

#include "UserObjects/Utility.h"
#include "cutsceneExportToolPdata.h"

using namespace std;
using namespace rage;

//--- FiLMBOX Registration & Implementation.
FBClassImplementation(  RexRageCutsceneExportPData    );
FBUserObjectImplement(  RexRageCutsceneExportPData,
                        "Class objects used to hold RexRageCutsceneExportTool tool data.",
                        FB_DEFAULT_SDK_ICON     );                                          //Register UserObject class
FBElementClassImplementation( RexRageCutsceneExportPData, FB_DEFAULT_SDK_ICON );            //Register to the asset system

//-----------------------------------------------------------------------------

RexRageCutsceneExportPData::RexRageCutsceneExportPData( const char* pName, HIObject pObject )
	: FBUserObject(pName, pObject)
{
	FBClassInit;
	m_Data = "";
}

//-----------------------------------------------------------------------------

bool RexRageCutsceneExportPData::FBCreate()
{
    return true;
}

//-----------------------------------------------------------------------------

void RexRageCutsceneExportPData::FBDestroy()
{
}

//-----------------------------------------------------------------------------

bool RexRageCutsceneExportPData::FbxStore( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat )
{
    if( pStoreWhat == kAttributes )
    {	
		// Fix for FBX ASCII format, the markup data is broken if it is saved with double quotes in so we swap them in and out when
		// saving/loading
		std::string strData(m_Data);
		UserObjectUtility::StringReplaceAll(strData, "\"", "*+~");

		atArray<atString> dataStrSplit;
        UserObjectUtility::SplitStringByLength( (char*)strData.c_str(), 4096, dataStrSplit );

		pFbxObject->FieldWriteBegin("RexRageCutsceneExportTool2");
		{
            pFbxObject->FieldWriteI( dataStrSplit.GetCount() );
            for ( int i = 0; i < dataStrSplit.GetCount(); ++i )
            {
                pFbxObject->FieldWriteC( const_cast<char *>( dataStrSplit[i].c_str() ) );
            }
		}

		pFbxObject->FieldWriteEnd();			
    }

    return true;
}

//-----------------------------------------------------------------------------

bool RexRageCutsceneExportPData::FbxRetrieve( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat )
{
    if( pStoreWhat == kAttributes )
    {
		const char* pDataStr = NULL;
		
		if ( pFbxObject->FieldReadBegin("RexRageCutsceneExportTool2") )
		{
			int iCount = pFbxObject->FieldReadI();

			atArray<atString> fieldDataSplit;
			fieldDataSplit.Reserve( iCount );
			for ( int i = 0; i < iCount; ++i )
			{
				char* pFieldData = (char*)pFbxObject->FieldReadC();
				if ( pFieldData == NULL )
				{
					break;
				}

				fieldDataSplit.Append() = pFieldData;
			}

			pDataStr = UserObjectUtility::ConcatenateStrings( fieldDataSplit );
		}

		pFbxObject->FieldReadEnd();

		if( (pDataStr != NULL) && (strlen(pDataStr) > 0) )
		{
			// Fix for FBX ASCII format, the markup data is broken if it is saved with double quotes in so we swap them in and out when
			// saving/loading
			std::string strData(pDataStr);
			UserObjectUtility::StringReplaceAll(strData, "*+~", "\"");

			//Set the string data member of this userobject to the data loaded from the file
			m_Data = strData.c_str();

			delete [] pDataStr;
		}
    }
  
    return true;
}


//-----------------------------------------------------------------------------
//Global Functions
//-----------------------------------------------------------------------------

RexRageCutsceneExportPData* FindCutsceneMarkupData( bool bCreate )
{
	FBSystem system;

	// First we want to see if the userobject actually exists in the scene..
	HFBScene lScene = system.Scene;
	int lIdx = 0;

	for( lIdx = 0; lIdx < lScene->UserObjects.GetCount(); ++lIdx ) 
	{
		HFBUserObject lObject = lScene->UserObjects[lIdx];
		if( lObject->Is( RexRageCutsceneExportPData::TypeInfo )) 
		{
			return (RexRageCutsceneExportPData*)lObject;
		}
	}

	RexRageCutsceneExportPData* lData = 0;

	if( bCreate ) 
	{
		//The user object doesn't exist so create one...
		lData = new RexRageCutsceneExportPData( "RexRageCutsceneExportMarkup" );
	}

	return lData;
}

void DeleteCutsceneMarkupData()
{
	// Importing other FBX files into the current FBX file will create duplicate persistent data fields
	RexRageCutsceneExportPData* lData = FindCutsceneMarkupData( false );
	while ( lData ) 
	{
		lData->FBDelete();

		lData = FindCutsceneMarkupData( false );
	}
}
