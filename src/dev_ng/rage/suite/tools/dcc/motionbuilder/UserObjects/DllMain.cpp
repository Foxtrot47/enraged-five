// DllMain.cpp : Defines the entry point for the DLL application.
//
#include "DllMain.h"
#include "UserObjects/animExportToolPdata.h"
#include "UserObjects/cutsceneExportToolPdata.h"
#include "system/param.h"
#include "parser/manager.h"
#include "OpenRealitySDK.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////
//
#pragma warning(push)
#pragma warning(disable:4100)
FBLibraryDeclare( orimpexpgames )
{
	FBLibraryRegister( RexRageAnimExportPData );
	FBLibraryRegisterElement( RexRageAnimExportPData );
	
	FBLibraryRegister( RexRageCutsceneExportPData );
	FBLibraryRegisterElement( RexRageCutsceneExportPData );

}
FBLibraryDeclareEnd;

#pragma warning(pop)

bool FBLibrary::LibInit()	{ 

	char appName[] = "UserObjects.dll";
	char* p_Argv[1];

	p_Argv[0] = appName;

	sysParam::Init(1,p_Argv);
	INIT_PARSER;

	return true; 
}

bool FBLibrary::LibOpen()	{ return true; }
bool FBLibrary::LibReady()	{ return true; }
bool FBLibrary::LibClose()	{ return true; }

bool FBLibrary::LibRelease()
{ 
	SHUTDOWN_PARSER;
	return true; 
}



////////////////////////////////////////////////////////////////////////////////
