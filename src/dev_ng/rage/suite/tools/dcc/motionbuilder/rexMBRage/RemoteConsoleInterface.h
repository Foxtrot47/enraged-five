// 
// RemoteConsole/RemoteConsoleInterface.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REMOTE_CONSOLE_INTERFACE_H__
#define __REMOTE_CONSOLE_INTERFACE_H__

#include "rexMBAnimExportCommon\utility.h"
#include "UserObjects\AnimMarkup.h"
#include "UserObjects\Utility.h"

class MetadataTagObject;
class RexRageAnimExportPData;
class RexAnimMarkupData;
class RexRageCutsceneExportPData;

class RemoteConsoleInterface
{
public:
	static bool Initialize(); 
	static void Destroy();

	static bool IsRemoteConsoleConnected();
	static void NotifyCutsceneUpdate();
	static void NotifyCutscenePatchStarted();
	static void NotifyCutscenePatchCompleted();
};	

#endif //__REMOTE_CONSOLE_INTERFACE_H__
