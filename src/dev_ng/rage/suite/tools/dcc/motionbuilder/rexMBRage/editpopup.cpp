// 
// rexMbRage/editpopup.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "editpopup.h"

#include "atl/array.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "rexMBAnimExportCommon/utility.h"
#include <sstream>

#include "configParser/configGameView.h"
#include "configParser/configExportView.h"
using namespace configParser;

static ConfigGameView gs_GameView;

namespace rage {

EditAttributesPopup::EditAttributesPopup()
: m_iAttachTopY(rexMBAnimExportCommon::c_iMargin)
, m_iSpreadsheetSelRowId(-1)
, m_iRowRef(0)
, m_mode(NO_MODE)
{

}

EditAttributesPopup::~EditAttributesPopup()
{

}

void EditAttributesPopup::UIInit()
{
    UICreate();
    UIConfigure();

    ClearSpreadsheet();
    EnableEditFields( false, false );
}

void EditAttributesPopup::UICreate()
{
    /* Here's a text-based representation of the Add Models Popup.
    {} = combo boxes (surrounds the name)
    () = button (surrounds the name)
    [ ] = check box
    _ = text box (spans the width of the text)
    |- = spread sheet borders and columns

    --------------------------------------------------
    |-----------||---Type---|--Value------|----------| (    Edit    )
    |           ||          |             |          | (     Add    )
    |           ||          |             |          | (   Remove   )
    |           ||          |             |          | ( Remove All )
    --------------------------------------------------
     Name: ___________________________________________ 
     Type: {                                         } (    Done    )
    Value: ___________________________________________ ( Cancel Edit)

                                                (   OK   ) ( Cancel )
    */

    int iButtonAttachRightX = -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth;

    Region.X = 200;
    Region.Y = 200;
    Region.Width = 500;
    Region.Height = m_iAttachTopY + (rexMBAnimExportCommon::c_iRowHeight * 10) + rexMBAnimExportCommon::c_iMargin;

    // Spreadsheet
    AddRegion( "Spreadsheet", "Spreadsheet",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        iButtonAttachRightX - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        -rexMBAnimExportCommon::c_iMargin - (rexMBAnimExportCommon::c_iRowHeight * 5), kFBAttachBottom, NULL, 1.0 );
    SetControl( "Spreadsheet", m_spreadsheet );

    // Edit
    AddRegion( "EditButton", "EditButton",
        iButtonAttachRightX, kFBAttachRight, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "EditButton", m_editButton );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Add
    AddRegion( "AddButton", "AddButton",
        iButtonAttachRightX, kFBAttachRight, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "AddButton", m_addButton );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Remove
    AddRegion( "RemoveButton", "RemoveButton",
        iButtonAttachRightX, kFBAttachRight, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "RemoveButton", m_removeButton );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // RemoveAll
    AddRegion( "RemoveAllButton", "RemoveAllButton",
        iButtonAttachRightX, kFBAttachRight, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "RemoveAllButton", m_removeAllButton );

    // Name
    AddRegion( "NameLabel", "NameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iRowHeight * 4), kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "NameLabel", m_nameLabel );

    AddRegion( "NameTextBox", "NameTextBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iRowHeight * 4), kFBAttachBottom, "NULL", 1.0,
        iButtonAttachRightX - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "NameTextBox", m_nameTextBox );

    // Done
    AddRegion( "DoneEditButton", "DoneEditButton",
        iButtonAttachRightX, kFBAttachRight, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iRowHeight * 4), kFBAttachBottom, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "DoneEditButton", m_doneEditButton );

    // Type
    AddRegion( "TypeLabel", "TypeLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iRowHeight * 3), kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "TypeLabel", m_typeLabel );

    AddRegion( "TypeComboBox", "TypeComboBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iRowHeight * 3), kFBAttachBottom, "NULL", 1.0,
        iButtonAttachRightX - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "TypeComboBox", m_typeComboBox );

    // Cancel Edit
    AddRegion( "CancelEditButton", "CancelEditButton",
        iButtonAttachRightX, kFBAttachRight, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iRowHeight * 3), kFBAttachBottom, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "CancelEditButton", m_cancelEditButton );

    // Value
    AddRegion( "ValueLabel", "ValueLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iRowHeight * 2), kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ValueLabel", m_valueLabel );

    AddRegion( "ValueTextBox", "ValueTextBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iRowHeight * 2), kFBAttachBottom, "NULL", 1.0,
        iButtonAttachRightX - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ValueTextBox", m_valueTextBox );

    int iAttachBottomY = -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight;

	// Test
	AddRegion( "CreateSeamlessTriggerAttributesButton", "CreateSeamlessTriggerAttributesButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachBottomY-rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "NULL", 1.0,
		rexMBAnimExportCommon::c_iButtonWidth*3, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "CreateSeamlessTriggerAttributesButton", m_createTriggerAttributes );

    // Cancel
    AddRegion( "CancelButton", "CancelButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "CancelButton", m_cancelButton );

    // Ok
    AddRegion( "OkButton", "OkButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachLeft, "CancelButton", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OkButton", m_okButton );
}

void EditAttributesPopup::UIConfigure()
{
    // Spreadsheet
    m_spreadsheet.Caption = "Attribute";
    m_spreadsheet.OnRowClick.Add( this, (FBCallback)&EditAttributesPopup::Spreadsheet_RowClick );

    // Edit
    m_editButton.Caption = "Edit";
    m_editButton.OnClick.Add( this, (FBCallback)&EditAttributesPopup::EditButton_Click );

    // Add
    m_addButton.Caption = "Add";
    m_addButton.OnClick.Add( this, (FBCallback)&EditAttributesPopup::AddButton_Click );

    // Remove
    m_removeButton.Caption = "Remove";
    m_removeButton.OnClick.Add( this, (FBCallback)&EditAttributesPopup::RemoveButton_Click );

    // Remove All
    m_removeAllButton.Caption = "Remove All";
    m_removeAllButton.OnClick.Add( this, (FBCallback)&EditAttributesPopup::RemoveAllButton_Click );

    // Name
    m_nameLabel.Caption = "Name:";
    m_nameLabel.Justify = kFBTextJustifyRight;

    m_nameTextBox.OnChange.Add( this, (FBCallback)&EditAttributesPopup::NameTextBox_Change );

    // Type
    m_typeLabel.Caption = "Type:";
    m_typeLabel.Justify = kFBTextJustifyRight;

    m_typeComboBox.Items.Add( "String" );
    m_typeComboBox.Items.Add( "Int" );
    m_typeComboBox.Items.Add( "Float" );
    m_typeComboBox.Items.Add( "Bool" );
    
    // Value
    m_valueLabel.Caption = "Value:";
    m_valueLabel.Justify = kFBTextJustifyRight;

    m_valueTextBox.OnChange.Add( this, (FBCallback)&EditAttributesPopup::ValueTextBox_Change );

    // Done
    m_doneEditButton.Caption = "Done";
    m_doneEditButton.OnClick.Add( this, (FBCallback)&EditAttributesPopup::DoneEditButton_Click );

    // Cancel Edit
    m_cancelEditButton.Caption = "Cancel Edit";
    m_cancelEditButton.OnClick.Add( this, (FBCallback)&EditAttributesPopup::CancelEditButton_Click );

	// Cancel Edit
	m_createTriggerAttributes.Caption = "Create Seamless Trigger Attributes";
	m_createTriggerAttributes.OnClick.Add( this, (FBCallback)&EditAttributesPopup::CreateTriggerAttributesButton_Click );

    // Ok
    m_okButton.Caption = "OK";
    m_okButton.OnClick.Add( this, (FBCallback)&EditAttributesPopup::OkButton_Click );

    // Cancel
    m_cancelButton.Caption = "Cancel";
    m_cancelButton.OnClick.Add( this, (FBCallback)&EditAttributesPopup::CancelButton_Click );
}

void EditAttributesPopup::SetAttributes( const parAttributeList& attributeList )
{
    ClearSpreadsheet();

    m_attributeList.CopyFrom( attributeList );

    atArray<parAttribute>& attributes = m_attributeList.GetAttributeArray();
    for ( int i = 0; i < attributes.GetCount(); ++i )
    {
        AddRowToSpreadsheet( attributes[i] );
    }

    m_nameTextBox.Text = "";
    m_typeComboBox.ItemIndex = 0;
    m_valueTextBox.Text = "";

    EnableEditFields( false, false );
}

void EditAttributesPopup::EnableEditFields( bool bEnable, bool bAddAttribute )
{
    if ( bEnable )
    {
        m_mode = bAddAttribute ? ADD_MODE : EDIT_MODE;
    }
    else
    {
        m_mode = NO_MODE;
    }

    if ( m_mode == NO_MODE )
    {
        m_addButton.Enabled = true;
        m_editButton.Enabled = m_iSpreadsheetSelRowId > -1;
        m_removeButton.Enabled = m_iSpreadsheetSelRowId > -1;
        m_removeAllButton.Enabled = m_attributeList.GetAttributeArray().GetCount() > 0;
    }
    else
    {
        m_addButton.Enabled = false;
        m_editButton.Enabled = false;
        m_removeButton.Enabled = false;
        m_removeAllButton.Enabled = false;
    }

    m_nameLabel.Enabled = bEnable;
    m_nameTextBox.Enabled = bEnable && bAddAttribute;

    m_typeLabel.Enabled = bEnable;
    m_typeComboBox.Enabled = bEnable && bAddAttribute;

    m_valueLabel.Enabled = bEnable;
    m_valueTextBox.Enabled = bEnable;

    if ( bEnable )
    {
        m_doneEditButton.Enabled = (strlen( m_nameTextBox.Text.AsString() ) > 0) && (strlen( m_valueTextBox.Text.AsString() ) > 0);
    }
    else
    {
        m_doneEditButton.Enabled = false;
    }

    m_cancelEditButton.Enabled = bEnable;
}

void EditAttributesPopup::ClearSpreadsheet()
{
    m_spreadsheet.Clear();
    
    m_spreadsheet.GetColumn( NAME_SPREADSHEET_COLUMN ).Width = 125;

    m_spreadsheet.ColumnAdd( "Type" );
    m_spreadsheet.GetColumn( TYPE_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_spreadsheet.GetColumn( TYPE_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
    m_spreadsheet.GetColumn( TYPE_SPREADSHEET_COLUMN ).Width = 125;

    m_spreadsheet.ColumnAdd( "Value" );
    m_spreadsheet.GetColumn( VALUE_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_spreadsheet.GetColumn( VALUE_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
    m_spreadsheet.GetColumn( VALUE_SPREADSHEET_COLUMN ).Width = 100;

    m_iSpreadsheetSelRowId = -1;
    m_iRowRef = 0;
}

void EditAttributesPopup::AddRowToSpreadsheet( const parAttribute &attribute )
{
    m_spreadsheet.RowAdd( const_cast<char *>( attribute.GetName() ), m_iRowRef );

    switch ( attribute.GetType() )
    {
    case parAttribute::STRING:
        m_spreadsheet.SetCell( m_iRowRef, TYPE_SPREADSHEET_COLUMN, "String" );
        break;
    case parAttribute::INT64:
        m_spreadsheet.SetCell( m_iRowRef, TYPE_SPREADSHEET_COLUMN, "Int" );
        break;
    case parAttribute::DOUBLE:
        m_spreadsheet.SetCell( m_iRowRef, TYPE_SPREADSHEET_COLUMN, "Float" );
        break;
    case parAttribute::BOOL:
        m_spreadsheet.SetCell( m_iRowRef, TYPE_SPREADSHEET_COLUMN, "Bool" );
        break;
    }

    char buf[128];
    m_spreadsheet.SetCell( m_iRowRef, VALUE_SPREADSHEET_COLUMN, const_cast<char *>( attribute.GetStringRepr( buf, sizeof(buf) ) ) );

    ++m_iRowRef;
}

void EditAttributesPopup::Spreadsheet_RowClick( HISender /*pSender*/, HKEvent pEvent )
{
    if ( m_mode == NO_MODE )
    {
        FBEventSpread e( pEvent );
        m_iSpreadsheetSelRowId = e.Row;

        EnableEditFields( false, false );
    }
}

void EditAttributesPopup::EditButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( m_iSpreadsheetSelRowId > -1 )
    {
        parAttribute attribute = m_attributeList.GetAttributeArray()[m_iSpreadsheetSelRowId];
        m_nameTextBox.Text = const_cast<char *>( attribute.GetName() );
        m_typeComboBox.ItemIndex = attribute.GetType();
        
        char buf[128];
        m_valueTextBox.Text = const_cast<char *>( attribute.GetStringRepr( buf, sizeof(buf) ) );

        EnableEditFields( true, false );
    }
}

void EditAttributesPopup::AddButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    m_nameTextBox.Text = "";
    m_typeComboBox.ItemIndex = 0;
    m_valueTextBox.Text = "";

    EnableEditFields( true, true );
}

void EditAttributesPopup::RemoveButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( m_iSpreadsheetSelRowId > -1 )
    {
        m_attributeList.GetAttributeArray().Delete( m_iSpreadsheetSelRowId );

        FBSpreadRow row = m_spreadsheet.GetRow( m_iSpreadsheetSelRowId );
        row.Remove();
        
        m_iSpreadsheetSelRowId = -1;
        EnableEditFields( false, false );
    }
}

void EditAttributesPopup::RemoveAllButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    ClearSpreadsheet();

    m_attributeList.Reset();

    EnableEditFields( false, false );
}

void EditAttributesPopup::NameTextBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    m_doneEditButton.Enabled = (strlen( m_nameTextBox.Text ) > 0) && (strlen( m_valueTextBox.Text ) > 0);
}

void EditAttributesPopup::ValueTextBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    m_doneEditButton.Enabled = (strlen( m_nameTextBox.Text ) > 0) && (strlen( m_valueTextBox.Text ) > 0);
}

void EditAttributesPopup::DoneEditButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	AddAttribute(m_nameTextBox.Text, m_typeComboBox.ItemIndex, m_valueTextBox.Text.AsString());
}

void EditAttributesPopup::AddAttribute(const char* attrname, const int type, const char* val)
{
	char name[128];
	safecpy( name, (char *)attrname, sizeof(name) );
	rexMBAnimExportCommon::ReplaceWhitespaceCharacters( name );

	// Verify that our name is unique
	bool bIsUnique = true;

	atArray<parAttribute>& attributes = m_attributeList.GetAttributeArray();
	if ( m_mode == ADD_MODE )
	{
		for ( int i = 0; i < attributes.GetCount(); ++i )
		{
			if ( strcmp( attributes[i].GetName(), name ) == 0 )
			{
				bIsUnique = false;
				break;
			}
		}
	}

	if ( bIsUnique )
	{
		parAttribute *pAttribute = NULL;
		if ( m_mode == ADD_MODE )
		{
			int iRef = attributes.GetCount();
			m_attributeList.AddAttribute( name, val );
			pAttribute = &(attributes[iRef]);        
		}
		else if ( m_iSpreadsheetSelRowId > -1 )
		{
			pAttribute = &(attributes[m_iSpreadsheetSelRowId]);
			pAttribute->SetValue( val );
		}

		if ( pAttribute != NULL )
		{
			switch ( type )
			{
			case parAttribute::INT64:
				pAttribute->ConvertToInt64();
				break;
			case parAttribute::DOUBLE:
				pAttribute->ConvertToDouble();
				break;
			case parAttribute::BOOL:
				pAttribute->ConvertToBool();
				break;
			}

			if ( m_mode == ADD_MODE )
			{
				AddRowToSpreadsheet( *pAttribute );
			}
			else if ( m_iSpreadsheetSelRowId > -1 )
			{
				char buf[128];
				m_spreadsheet.SetCell( m_iSpreadsheetSelRowId, VALUE_SPREADSHEET_COLUMN, 
					const_cast<char *>( pAttribute->GetStringRepr( buf, sizeof(buf) ) ) );
			}
		}

		EnableEditFields( false, false );
	}
	else
	{
		FBMessageBox( "Duplicate Attribute", "An attribute with that name already exists.  Please enter another.", "OK" );
	}
}

void EditAttributesPopup::CancelEditButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    EnableEditFields( false, false );
}

void EditAttributesPopup::CreateTriggerAttributesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	HFBModel model = RAGEFindModelByName("Seamless_Trigger");
	if(model != NULL)
	{
		RemoveAllButton_Click( NULL, NULL );

		int val=0;
		std::stringstream stream;

		FBPropertyInt* prop = (FBPropertyInt*)model->PropertyList.Find("Radius");
		if(prop != NULL)
		{
			val=0;
			prop->GetData(&val,sizeof(val));		
			
			stream << val;

			EnableEditFields( true, true );
			AddAttribute("Radius", 2, stream.str().c_str());
		}

		prop = (FBPropertyInt*)model->PropertyList.Find("Orient Angle");
		if(prop != NULL)
		{
			val=0;
			prop->GetData(&val,sizeof(val));

			stream.str("");
			stream << val;

			EnableEditFields( true, true );
			AddAttribute("Orient Angle", 2, stream.str().c_str());
		}

		prop = (FBPropertyInt*)model->PropertyList.Find("Approach Angle");
		if(prop != NULL)
		{
			val=0;
			prop->GetData(&val,sizeof(val));

			stream.str("");
			stream << val;

			EnableEditFields( true, true );
			AddAttribute("Approach Angle", 2, stream.str().c_str());
		}

		FBVector3d mbTransVector;
		model->GetVector(mbTransVector, kModelTranslation, false);

		m_triggerOffsetXNumericUpDown.Value = mbTransVector[0];
		m_triggerOffsetYNumericUpDown.Value = mbTransVector[1];
		m_triggerOffsetZNumericUpDown.Value = mbTransVector[2];
	}
}

void EditAttributesPopup::OkButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    Close( true );
}

void EditAttributesPopup::CancelButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    Close( false );
}

//##############################################################################

EditSceneAttributesPopup::EditSceneAttributesPopup()
{

}

EditSceneAttributesPopup::~EditSceneAttributesPopup()
{

}

void EditSceneAttributesPopup::SetSettings( const Vector3& vSceneOffset, const Vector3& vTriggerOffset, float fSceneRotation, const parAttributeList& attributeList )
{
    SetSceneOffset( vSceneOffset );
	SetTriggerOffset( vTriggerOffset );
    SetSceneRotation( fSceneRotation );
    SetAttributes( attributeList );
}

void EditSceneAttributesPopup::UICreate()
{
    // Scene Offset
    AddRegion( "SceneOffsetLabel", "SceneOffsetLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "SceneOffsetLabel", m_sceneOffsetLabel );	

    AddRegion( "SceneOffsetXNumericUpDown", "SceneOffsetXNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "SceneOffsetXNumericUpDown", m_sceneOffsetXNumericUpDown );

    AddRegion( "SceneOffsetYNumericUpDown", "SceneOffsetYNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight,	kFBAttachNone, NULL, 1.0 );
    SetControl( "SceneOffsetYNumericUpDown", m_sceneOffsetYNumericUpDown );

    AddRegion( "SceneOffsetZNumericUpDown", "SceneOffsetZNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 2), kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "SceneOffsetZNumericUpDown", m_sceneOffsetZNumericUpDown );

    // Scene Rotation
    AddRegion( "SceneRotationLabel", "SceneRotationLabel",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 3) + (rexMBAnimExportCommon::c_iMargin * 2), kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth + 20, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "SceneRotationLabel", m_sceneRotationLabel );

    AddRegion( "SceneRotation",	"SceneRotation",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 3) + (rexMBAnimExportCommon::c_iMargin * 2) + rexMBAnimExportCommon::c_iLabelWidth + 20 + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "SceneRotation", m_sceneRotationNumericUpDown );

    AddRegion( "SceneRotationUnitsLabel",	"SceneRotationUnitsLabel",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 3) + (rexMBAnimExportCommon::c_iMargin * 2) + rexMBAnimExportCommon::c_iLabelWidth + 20 + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iNumericUpDownWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        50, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "SceneRotationUnitsLabel", m_sceneRotationUnitsLabel );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Scene Offset
	AddRegion( "TriggerOffsetLabel", "TriggerOffsetLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "TriggerOffsetLabel", m_triggerOffsetLabel );	

	AddRegion( "TriggerOffsetXNumericUpDown", "TriggerOffsetXNumericUpDown",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "TriggerOffsetXNumericUpDown", m_triggerOffsetXNumericUpDown );

	AddRegion( "TriggerOffsetYNumericUpDown", "TriggerOffsetYNumericUpDown",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight,	kFBAttachNone, NULL, 1.0 );
	SetControl( "TriggerOffsetYNumericUpDown", m_triggerOffsetYNumericUpDown );

	AddRegion( "TriggerOffsetZNumericUpDown", "TriggerOffsetZNumericUpDown",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 2), kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "TriggerOffsetZNumericUpDown", m_triggerOffsetZNumericUpDown );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditAttributesPopup::UICreate();
}

void EditSceneAttributesPopup::UIConfigure()
{
    Caption = "Edit Scene Properties";

    // Scene Offset
    m_sceneOffsetLabel.Caption = "Scene Offset:";
    m_sceneOffsetLabel.Justify = kFBTextJustifyRight;  

	// Trigger Offset
	m_triggerOffsetLabel.Caption = "Trigger Offset:";
	m_triggerOffsetLabel.Justify = kFBTextJustifyRight;   

    // Scene Rotation
    m_sceneRotationLabel.Caption = "Scene Rotation:";
    m_sceneRotationLabel.Justify = kFBTextJustifyRight;

    m_sceneRotationNumericUpDown.Min = 0;
    m_sceneRotationNumericUpDown.Max = 359;
    m_sceneRotationNumericUpDown.Precision = 0;

    m_sceneRotationUnitsLabel.Caption = "degrees";

    SetSceneOffset( Vector3( 0.0f, 0.0f, 0.0f ) );
    SetSceneRotation( 0.0f );

    EditAttributesPopup::UIConfigure();
}

//##############################################################################

EditObjectAttributesPopup::EditObjectAttributesPopup()
: m_pObject(NULL)
{

}

EditObjectAttributesPopup::~EditObjectAttributesPopup()
{

}


void EditObjectAttributesPopup::SetObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    m_pObject = pObject;

    char title[256];
    sprintf( title, "Edit %s '%s'", pObject->GetTypeName(), pObject->GetDisplayName().c_str() );
    Caption = title;

    SetAttributes( pObject->GetAttributeList() );

    SetUIFromObject( m_pObject, pCutFile );
}

cutfObject* EditObjectAttributesPopup::GetObject()
{
    cutfObject *pNewObject = m_pObject->Clone();

    parAttributeList &attributes = const_cast<parAttributeList &>( pNewObject->GetAttributeList() );
    attributes.CopyFrom( GetAttributes() );

    SetObjectFromUI( pNewObject );

    return pNewObject;
}

void EditObjectAttributesPopup::SetUIFromObject( const cutfObject* /*pObject*/, const cutfCutsceneFile2* /*pCutFile */)
{

}

void EditObjectAttributesPopup::SetObjectFromUI( cutfObject* /*pObject*/ )
{

}

//##############################################################################

EditOverlayObjectAttributesPopup::EditOverlayObjectAttributesPopup()
{

}

EditOverlayObjectAttributesPopup::~EditOverlayObjectAttributesPopup()
{

}

void EditOverlayObjectAttributesPopup::UICreate()
{
	AddRegion( "OverlayTypeLabel", "OverlayTypeLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "OverlayTypeLabel", m_overlayTypeLabel );

	AddRegion( "OverlayTypeComboBox", "OverlayTypeComboBox",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "OverlayTypeComboBox", m_overlayTypeComboBox );

	m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	AddRegion( "RenderTargetLabel", "RenderTargetLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "RenderTargetLabel", m_renderTargetNameLabel );

	AddRegion( "RenderTargetTextBox", "RenderTargetTextBox",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "RenderTargetTextBox", m_renderTargetNameTextBox );

	m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	AddRegion( "ModelNameLabel", "ModelNameLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ModelNameLabel", m_modelObjectNameLabel );

	AddRegion( "ModelNameComboBox", "ModelNameComboBox",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ModelNameComboBox", m_modelObjectNameTextbox );

	m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	EditObjectAttributesPopup::UICreate();
}

void EditOverlayObjectAttributesPopup::UIConfigure()
{
	m_overlayTypeLabel.Caption = "Overlay Type:";
	m_overlayTypeLabel.Justify = kFBTextJustifyRight;
	m_overlayTypeLabel.Enabled = true;

	m_renderTargetNameLabel.Caption = "Render Target:";
	m_renderTargetNameLabel.Justify = kFBTextJustifyRight;

	m_modelObjectNameLabel.Caption = "Model Name:";
	m_modelObjectNameLabel.Justify = kFBTextJustifyRight;

	EditObjectAttributesPopup::UIConfigure();
}

void EditOverlayObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
	EditObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

	const cutfOverlayObject *pOverlayObj = dynamic_cast<cutfOverlayObject*>((cutfObject*)pObject);

	u32 uiOverlayType = pOverlayObj->GetOverlayType();
	if(uiOverlayType < (u32)m_overlayTypeComboBox.Items.GetCount())
	{
		m_overlayTypeComboBox.ItemIndex = uiOverlayType;
	}

	m_renderTargetNameTextBox.Text = (char*)pOverlayObj->GetRenderTargetName();

	atHashString modelObjectName(pOverlayObj->GetModelHashName());
	m_modelObjectNameTextbox.Text = (modelObjectName.GetCStr() != NULL) ? (char*)modelObjectName.GetCStr() : "";
}

void EditOverlayObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
	cutfOverlayObject *pOverlayObj = dynamic_cast<cutfOverlayObject*>(pObject);
	if(pOverlayObj)
	{
		pOverlayObj->SetRenderTargetName(m_renderTargetNameTextBox.Text);
		pOverlayObj->SetOverlayType(m_overlayTypeComboBox.ItemIndex);

		atHashString modelHash(m_modelObjectNameTextbox.Text);
		pOverlayObj->SetModelHashName(modelHash);
	}

	EditObjectAttributesPopup::SetObjectFromUI( pObject );
}

void EditOverlayObjectAttributesPopup::SetOverlayTypes( const atArray<atString>& atEntries )
{
	for(int i=0; i < atEntries.GetCount(); ++i)
	{
		m_overlayTypeComboBox.Items.Add((char*)atEntries[i].c_str());
	}
}

//##############################################################################

EditCameraObjectAttributesPopup::EditCameraObjectAttributesPopup()
{

}

EditCameraObjectAttributesPopup::~EditCameraObjectAttributesPopup()
{

}

void EditCameraObjectAttributesPopup::UICreate()
{
    // Near Draw Distance
    AddRegion( "NearDrawDistanceLabel", "NearDrawDistanceLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "NearDrawDistanceLabel", m_nearDrawDistanceLabel );

    AddRegion( "NearDrawDistanceTextBox", "NearDrawDistanceTextBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "NearDrawDistanceTextBox", m_nearDrawDistanceTextBox );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Far Draw Distance
    AddRegion( "FarDrawDistanceLabel", "FarDrawDistanceLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "FarDrawDistanceLabel", m_farDrawDistanceLabel );

    AddRegion( "FarDrawDistanceTextBox", "FarDrawDistanceTextBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "FarDrawDistanceTextBox", m_farDrawDistanceTextBox );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditObjectAttributesPopup::UICreate();
}

void EditCameraObjectAttributesPopup::UIConfigure()
{
    // Far Draw Distance
    m_nearDrawDistanceLabel.Caption = "Near Draw Distance:";
    m_nearDrawDistanceLabel.Justify = kFBTextJustifyRight;

    // Far Draw Distance
    m_farDrawDistanceLabel.Caption = "Far Draw Distance:";
    m_farDrawDistanceLabel.Justify = kFBTextJustifyRight;

    EditObjectAttributesPopup::UIConfigure();
}

void EditCameraObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    EditObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

    const cutfCameraObject *pCameraObj = dynamic_cast<const cutfCameraObject *>( pObject );

    char cTemp[16];
    sprintf( cTemp, "%f", pCameraObj->GetNearDrawDistance() );
    m_nearDrawDistanceTextBox.Caption = cTemp;

    sprintf( cTemp, "%f", pCameraObj->GetFarDrawDistance() );
    m_farDrawDistanceTextBox.Caption = cTemp;
}

void EditCameraObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
    EditObjectAttributesPopup::SetObjectFromUI( pObject );

    // nothing more to do.  everything else is read-only
}

//##############################################################################

EditModelObjectAttributesPopup::EditModelObjectAttributesPopup()
{

}

EditModelObjectAttributesPopup::~EditModelObjectAttributesPopup()
{

}

void EditModelObjectAttributesPopup::UICreate()
{
	// Model Name
    AddRegion( "ModelNameLabel", "ModelNameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ModelNameLabel", m_modelNameLabel );

    AddRegion( "ModelNameTextBox", "ModelNameTextBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ModelNameTextBox", m_modelNameTextBox );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Anim Export Control Spec File
    AddRegion( "AnimExportCtrlSpecFileLabel", "AnimExportCtrlSpecFileLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "AnimExportCtrlSpecFileLabel", m_animExportCtrlSpecFileLabel );

    AddRegion( "AnimExportCtrlSpecFileComboBox", "AnimExportCtrlSpecFileComboBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "AnimExportCtrlSpecFileComboBox", m_animExportCtrlSpecFileComboBox );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Face Export Control Spec File
	AddRegion( "FaceExportCtrlSpecFileLabel", "FaceExportCtrlSpecFileLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "FaceExportCtrlSpecFileLabel", m_faceExportCtrlSpecFileLabel );

	AddRegion( "FaceExportCtrlSpecFileComboBox", "FaceExportCtrlSpecFileComboBox",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "FaceExportCtrlSpecFileComboBox", m_faceExportCtrlSpecFileComboBox );

	m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Compression
    AddRegion( "AnimCompressionFileLabel", "AnimCompressionFileLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "AnimCompressionFileLabel", m_animCompressionFileLabel );

    AddRegion( "AnimCcompressionFileComboBox", "AnimCcompressionFileComboBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "AnimCcompressionFileComboBox", m_animCcompressionFileComboBox );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Face Attributes
	AddRegion( "FaceAttributesLabel", "FaceAttributesLabel",
		rexMBAnimExportCommon::c_iMargin - 25, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "FaceAttributesLabel", m_FaceAttributesLabel ); 

	AddRegion( "FaceAttributesFilenameTextBox", "FaceAttributesFilenameTextBox",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin - 35, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "FaceAttributesFilenameTextBox", m_FaceAttributesFilenameTextBox );

	AddRegion( "BrowseFaceAttributesButton", "BrowseFaceAttributesButton",
		-rexMBAnimExportCommon::c_iMargin - 30, kFBAttachRight, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		30, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "BrowseFaceAttributesButton", m_browseFaceAttributesButton );

	m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Handles
	AddRegion( "ModelHandleFileLabel", "ModelHandleFileLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ModelHandleFileLabel", m_modelHandleFileLabel );

	AddRegion( "ModelHandleFileComboBox", "ModelHandleFileComboBox",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ModelHandleFileComboBox", m_modelHandleFileComboBox );

	m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditObjectAttributesPopup::UICreate();
}

void EditModelObjectAttributesPopup::UIConfigure()
{
    // Model Name
    m_modelNameLabel.Caption = "Full Name:";
    m_modelNameLabel.Justify = kFBTextJustifyRight;

    // Anim Export Control Spec File
    m_animExportCtrlSpecFileLabel.Caption = "Anim Spec:";
    m_animExportCtrlSpecFileLabel.Justify = kFBTextJustifyRight;

    m_animExportCtrlSpecFileComboBox.Style = kFBDropDownList;

	// Anim Export Face Control Spec File
	m_faceExportCtrlSpecFileLabel.Caption = "Face Spec:";
	m_faceExportCtrlSpecFileLabel.Justify = kFBTextJustifyRight;

	m_faceExportCtrlSpecFileComboBox.Style = kFBDropDownList;

    // Compression
    m_animCompressionFileLabel.Caption = "Compression:";
    m_animCompressionFileLabel.Justify = kFBTextJustifyRight;

    m_animCcompressionFileComboBox.Style = kFBDropDownList;

	m_modelHandleFileLabel.Caption = "Handle:";
	m_modelHandleFileLabel.Justify = kFBTextJustifyRight;
	m_modelHandleFileComboBox.Style = kFBDropDownList;

	// Face Attributes
	m_FaceAttributesLabel.Caption = "Face Attributes:";
	m_FaceAttributesLabel.Justify = kFBTextJustifyRight;

    m_FaceAttributesFilenameTextBox.Enabled = true;
    m_FaceAttributesFilenameTextBox.Text = "";

    m_browseFaceAttributesButton.Caption = "...";
    m_browseFaceAttributesButton.Enabled = true;
    m_browseFaceAttributesButton.OnClick.Add( this, (FBCallback)&EditModelObjectAttributesPopup::BrowseFaceAttributesButton_Click );

    // Open File Popup
    m_openFilePopup.Filter = "*.attributes";
    m_openFilePopup.Style = kFBFilePopupOpen;
   
    EditObjectAttributesPopup::UIConfigure();
}

void EditModelObjectAttributesPopup::BrowseFaceAttributesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_openFilePopup.Caption = "Select Face Attributes File";

	if ( m_openFilePopup.Execute() )
	{
		char cFullFilename[RAGE_MAX_PATH];
		safecpy( cFullFilename, (const char*)m_openFilePopup.FullFilename, sizeof(cFullFilename) );
		m_FaceAttributesFilenameTextBox.Text = cFullFilename;
	}
}

void EditModelObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    EditObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

    // Fresh the combo box list every time
    atArray<atString> specFilenames;
    atArray<atString> compressFilenames;
    rexMBAnimExportCommon::FindAnimCtrlFilePathFiles( specFilenames );
	rexMBAnimExportCommon::FindAnimCompressionFilePathFiles( compressFilenames );

    m_animExportCtrlSpecFileComboBox.Items.Clear();
    m_animExportCtrlSpecFileComboBox.Items.Add( "(no spec file)" );
    for ( int i = 0; i < specFilenames.GetCount(); ++i )
    {
        m_animExportCtrlSpecFileComboBox.Items.Add( const_cast<char *>( specFilenames[i].c_str() ) );
    }

	m_faceExportCtrlSpecFileComboBox.Items.Clear();
	m_faceExportCtrlSpecFileComboBox.Items.Add( "(no spec file)" );
	for ( int i = 0; i < specFilenames.GetCount(); ++i )
	{
		m_faceExportCtrlSpecFileComboBox.Items.Add( const_cast<char *>( specFilenames[i].c_str() ) );
	}

    m_animCcompressionFileComboBox.Items.Clear();
    m_animCcompressionFileComboBox.Items.Add( "(no compression)" );
    for ( int i = 0; i < compressFilenames.GetCount(); ++i )
    {
        m_animCcompressionFileComboBox.Items.Add( const_cast<char *>( compressFilenames[i].c_str() ) );
    }

    const cutfModelObject *pModelObj = dynamic_cast<const cutfModelObject *>( pObject );

    m_modelNameTextBox.Caption = const_cast<char *>( pModelObj->GetName().GetCStr() );

    // find the index that corresponds to our spec file setting
	ConstString specFilename(pModelObj->GetAnimExportCtrlSpecFile().GetCStr() == NULL ? "" : pModelObj->GetAnimExportCtrlSpecFile().GetCStr());
    m_animExportCtrlSpecFileComboBox.ItemIndex = 0;
    for ( int i = 1; i < m_animExportCtrlSpecFileComboBox.Items.GetCount(); ++i )
    {
        char cSpecFile[CUTSCENE_OBJNAMELEN];
        safecpy( cSpecFile, m_animExportCtrlSpecFileComboBox.Items.GetAt( i ), sizeof(cSpecFile) );

        if ( stricmp( cSpecFile, specFilename.c_str() ) == 0 )
        {
            m_animExportCtrlSpecFileComboBox.ItemIndex = i;
            break;
        }
    }

	// find the index that corresponds to our spec file setting
	specFilename = pModelObj->GetFaceExportCtrlSpecFile().GetCStr() == NULL ? "" : pModelObj->GetFaceExportCtrlSpecFile().GetCStr();
	m_faceExportCtrlSpecFileComboBox.ItemIndex = 0;
	for ( int i = 1; i < m_faceExportCtrlSpecFileComboBox.Items.GetCount(); ++i )
	{
		char cSpecFile[CUTSCENE_OBJNAMELEN];
		safecpy( cSpecFile, m_faceExportCtrlSpecFileComboBox.Items.GetAt( i ), sizeof(cSpecFile) );

		if ( stricmp( cSpecFile, specFilename.c_str() ) == 0 )
		{
			m_faceExportCtrlSpecFileComboBox.ItemIndex = i;
			break;
		}
	}

	// find the index that corresponds to our handle name
	ConstString handle(pModelObj->GetHandle().GetCStr() == NULL ? "" : pModelObj->GetHandle().GetCStr());
	m_modelHandleFileComboBox.ItemIndex = 0;
	for ( int i = 1; i < m_modelHandleFileComboBox.Items.GetCount(); ++i )
	{
		char cHandle[CUTSCENE_OBJNAMELEN];
		safecpy( cHandle, m_modelHandleFileComboBox.Items.GetAt( i ), sizeof(cHandle) );

		if ( stricmp( cHandle, handle.c_str() ) == 0 )
		{
			m_modelHandleFileComboBox.ItemIndex = i;
			break;
		}
	}

    // find the index that corresponds to our compress file setting
    ConstString compressFilename(pModelObj->GetAnimCompressionFile().GetCStr() == NULL ? "" : pModelObj->GetAnimCompressionFile().GetCStr());
    m_animCcompressionFileComboBox.ItemIndex = 0;
    for ( int i = 1; i < m_animCcompressionFileComboBox.Items.GetCount(); ++i )
    {
        char cCompressionFile[CUTSCENE_OBJNAMELEN];
        safecpy( cCompressionFile, m_animCcompressionFileComboBox.Items.GetAt( i ), sizeof(cCompressionFile) );

        if ( stricmp( cCompressionFile, compressFilename.c_str() ) == 0 )
        {
            m_animCcompressionFileComboBox.ItemIndex = i;
            break;
        }
    }
}

void EditModelObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
    EditObjectAttributesPopup::SetObjectFromUI( pObject );

    cutfModelObject *pModelObj = dynamic_cast<cutfModelObject *>( pObject );

    if ( m_animExportCtrlSpecFileComboBox.ItemIndex <= 0 )
    {
        pModelObj->SetAnimExportCtrlSpecFile( "" );
    }
    else
    {
        pModelObj->SetAnimExportCtrlSpecFile( m_animExportCtrlSpecFileComboBox.Items.GetAt( m_animExportCtrlSpecFileComboBox.ItemIndex ) );
    }

	if ( m_faceExportCtrlSpecFileComboBox.ItemIndex <= 0 )
	{
		pModelObj->SetFaceExportCtrlSpecFile( "" );
	}
	else
	{
		pModelObj->SetFaceExportCtrlSpecFile( m_faceExportCtrlSpecFileComboBox.Items.GetAt( m_faceExportCtrlSpecFileComboBox.ItemIndex ) );
	}

	if ( m_modelHandleFileComboBox.ItemIndex <= 0 )
	{
		pModelObj->SetHandle( "" );
	}
	else
	{
		pModelObj->SetHandle( m_modelHandleFileComboBox.Items.GetAt( m_modelHandleFileComboBox.ItemIndex ) );
	}

    if ( m_animCcompressionFileComboBox.ItemIndex <= 0 )
    {
        pModelObj->SetAnimCompressionFile( "" );
    }
    else
    {
        pModelObj->SetAnimCompressionFile( m_animCcompressionFileComboBox.Items.GetAt( m_animCcompressionFileComboBox.ItemIndex ) );
    }
}

void EditModelObjectAttributesPopup::SetHandles( const atArray<HandleEntry>& atEntries )
{
	m_modelHandleFileComboBox.Items.Clear();
	m_modelHandleFileComboBox.Items.Add("(no handle)");

	// We need to filter out the handles here, only add ones which are for the cutscene and type
	for(int i=0; i < atEntries.GetCount(); ++i)
	{
		HandleEntry handle = atEntries[i];
		bool bAddHandle = false;

		if(dynamic_cast<EditPedModelObjectAttributesPopup*>(this))
		{
			if(strcmpi(handle.strCategory.c_str(), "PED") == 0) // Filter out the category
			{
				bAddHandle = true;
			}
		}
		else if(dynamic_cast<EditVehicleModelObjectAttributesPopup*>(this)) // Filter out the category
		{
			if(strcmpi(handle.strCategory.c_str(), "VEHICLE") == 0)
			{
				bAddHandle = true;
			}
		}
		else if (dynamic_cast<EditModelObjectAttributesPopup*>(this)) // Filter out the category
		{
			if(strcmpi(handle.strCategory.c_str(), "PROP") == 0)
			{
				bAddHandle = true;
			}
		}

		// Grab the scene filename and the path, we use this to figure out if the handle is cutscene/mission specific
		FBApplication application;
		const char* pFullPath = (const char*)application.FBXFileName;

		const char* pFileName = ASSET.FileName(pFullPath);
		char cFileName[RAGE_MAX_PATH];
		ASSET.RemoveExtensionFromPath(cFileName, sizeof(cFileName), pFileName);
		
		char cPath[RAGE_MAX_PATH];
		ASSET.RemoveNameFromPath( cPath, sizeof(cPath), pFullPath );

		if(strcmpi(handle.strType.c_str(), "Mission:") == 0)
		{
			atString strPath(cPath);

			if(strPath.IndexOf(handle.strName.c_str()) == -1) // Mission specific handle
			{
				bAddHandle = false;
			}
		}

		if(strcmpi(handle.strType.c_str(), "Cutscene specific:") == 0)
		{
			atString strFileName(cFileName);

			if(strcmpi(strFileName.c_str(), handle.strName.c_str()) != 0) // cutscene specific handle
			{
				bAddHandle=false;
			}
		}

		if(!bAddHandle) continue;

		m_modelHandleFileComboBox.Items.Add((char*)atEntries[i].strHandle.c_str());
	}
}

//##############################################################################

EditPedModelObjectAttributesPopup::EditPedModelObjectAttributesPopup()
{

}

EditPedModelObjectAttributesPopup::~EditPedModelObjectAttributesPopup()
{

}

void EditPedModelObjectAttributesPopup::UICreate()
{
    // Found Face Animation
    AddRegion( "DefaultFaceAnimationLabel", "DefaultFaceAnimationLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "DefaultFaceAnimationLabel", m_defaultFaceAnimationLabel );

    AddRegion( "FoundDefaultFaceAnimationCheckBox", "FoundDefaultFaceAnimationCheckBox",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iLabelWidth, kFBAttachRight, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "FoundDefaultFaceAnimationCheckBox", m_foundDefaultFaceAnimationCheckBox );
    
    AddRegion( "DefaultFaceAnimationTextBox", "DefaultFaceAnimationTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "DefaultFaceAnimationLabel", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "FoundDefaultFaceAnimationCheckBox", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "DefaultFaceAnimationTextBox", m_defaultFaceAnimationTextBox );
   
    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Override Face Animation
    AddRegion( "OverrideFaceAnimationLabel", "OverrideFaceAnimationLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OverrideFaceAnimationLabel", m_overrideFaceAnimationLabel );    

    AddRegion( "OverrideFaceAnimationCheckBox", "OverrideFaceAnimationCheckBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "OverrideFaceAnimationLabel", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin * 3, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OverrideFaceAnimationCheckBox", m_overrideFaceAnimationCheckBox );

    AddRegion( "BrowseFaceAnimationButton", "BrowseFaceAnimationButton",
        -rexMBAnimExportCommon::c_iMargin - 30, kFBAttachRight, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        30, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "BrowseFaceAnimationButton", m_browseFaceAnimationButton );

    AddRegion( "FaceAnimationaceFilenameTextBox", "FaceAnimationaceFilenameTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "OverrideFaceAnimationCheckBox", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "BrowseFaceAnimationButton", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "FaceAnimationaceFilenameTextBox", m_overrideFaceAnimationFilenameTextBox );    

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditModelObjectAttributesPopup::UICreate();
}

void EditPedModelObjectAttributesPopup::UIConfigure()
{
    // Found Face Animation
    m_defaultFaceAnimationLabel.Caption = "Default Face Anim:";
    m_defaultFaceAnimationLabel.Justify = kFBTextJustifyRight;

    m_foundDefaultFaceAnimationCheckBox.Caption = "File Found";
    m_foundDefaultFaceAnimationCheckBox.Enabled = false;
    m_foundDefaultFaceAnimationCheckBox.Style = kFBCheckbox;

    // Override Face Animation
    m_overrideFaceAnimationLabel.Caption = "Override Face Anim:";
    m_overrideFaceAnimationLabel.Justify = kFBTextJustifyRight;

    m_overrideFaceAnimationCheckBox.OnClick.Add( this, (FBCallback)&EditPedModelObjectAttributesPopup::OverrideFaceAnimationCheckBox_CheckChanged );
    m_overrideFaceAnimationCheckBox.Style = kFBCheckbox;

    m_overrideFaceAnimationFilenameTextBox.Enabled = false;
    m_overrideFaceAnimationFilenameTextBox.Text = "";

    m_browseFaceAnimationButton.Caption = "...";
    m_browseFaceAnimationButton.Enabled = false;
    m_browseFaceAnimationButton.OnClick.Add( this, (FBCallback)&EditPedModelObjectAttributesPopup::BrowseFaceAnimationButton_Click );

    // Open File Popup
    m_openFilePopup.Filter = "*.anim;*.clip";
    m_openFilePopup.Style = kFBFilePopupOpen;

    EditModelObjectAttributesPopup::UIConfigure();
}

void EditPedModelObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    EditModelObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

    const cutfPedModelObject *pPedModelObj = dynamic_cast<const cutfPedModelObject *>( pObject );

    m_defaultFaceAnimationTextBox.Caption = const_cast<char *>( pPedModelObj->GetDefaultFaceAnimationFilename( pCutFile->GetFaceDirectory() ).GetCStr() );
    m_foundDefaultFaceAnimationCheckBox.State = pPedModelObj->FoundDefaultFaceAnimation() ? 1 : 0;
    
    m_overrideFaceAnimationCheckBox.State = pPedModelObj->OverrideFaceAnimation() ? 1 : 0;
    m_overrideFaceAnimationFilenameTextBox.Text 
        = (pPedModelObj->GetOverrideFaceAnimationFilename() != NULL) ? const_cast<char *>( pPedModelObj->GetOverrideFaceAnimationFilename().GetCStr() ) : "";

	char czExportDir[RAGE_MAX_PATH];
	gs_GameView.GetAssetsDir(czExportDir, RAGE_MAX_PATH);
	atString strExportDir(czExportDir);
	strExportDir.Lowercase();
	strExportDir.Replace("/", "\\");

	atString strFaceAttributes(pPedModelObj->GetFaceAttributesFilename().GetCStr());
	strFaceAttributes.Lowercase();
	strFaceAttributes.Replace("/", "\\");
	strFaceAttributes.Replace("$(assets)", strExportDir);

	m_FaceAttributesFilenameTextBox.Text = (char*)strFaceAttributes.c_str();
}

void EditPedModelObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
    cutfPedModelObject *pPedModelObj = dynamic_cast<cutfPedModelObject *>( pObject );
    pPedModelObj->SetOverrideFaceAnimation( m_overrideFaceAnimationCheckBox.State != 0 );
	pPedModelObj->SetOverrideFaceAnimationFilename( (const char*)m_overrideFaceAnimationFilenameTextBox.Text );

	char czExportDir[RAGE_MAX_PATH];
	gs_GameView.GetAssetsDir(czExportDir, RAGE_MAX_PATH);
	atString strExportDir(czExportDir);
	strExportDir.Lowercase();
	strExportDir.Replace("/", "\\");

	atString strFaceAttributes((const char*)m_FaceAttributesFilenameTextBox.Text);
	strFaceAttributes.Lowercase();
	strFaceAttributes.Replace("/", "\\");
	strFaceAttributes.Replace(strExportDir, "$(assets)");

	pPedModelObj->SetFaceAttributesFilename( strFaceAttributes.c_str() );

    EditModelObjectAttributesPopup::SetObjectFromUI( pObject );
}

void EditPedModelObjectAttributesPopup::OverrideFaceAnimationCheckBox_CheckChanged( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    bool bEnabled = m_overrideFaceAnimationCheckBox.State != 0;
    m_overrideFaceAnimationFilenameTextBox.Enabled = bEnabled;
    m_browseFaceAnimationButton.Enabled = bEnabled;
}

void EditPedModelObjectAttributesPopup::BrowseFaceAnimationButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    char cTitle[128];
    sprintf( cTitle, "Select Face Animation File for Ped Model '%s'", m_pObject->GetDisplayName().c_str() );

    m_openFilePopup.Caption = cTitle;

    if ( m_openFilePopup.Execute() )
    {
        char cFullFilename[RAGE_MAX_PATH];
		safecpy( cFullFilename, (const char*)m_openFilePopup.FullFilename, sizeof(cFullFilename) );
        m_overrideFaceAnimationFilenameTextBox.Text = cFullFilename;
    }
}

//##############################################################################

EditVehicleModelObjectAttributesPopup::EditVehicleModelObjectAttributesPopup()
{

}

EditVehicleModelObjectAttributesPopup::~EditVehicleModelObjectAttributesPopup()
{

}

void EditVehicleModelObjectAttributesPopup::UICreate()
{
    // Remove Bones
    AddRegion( "RemoveBonesLabel", "RemoveBonesLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "RemoveBonesLabel", m_removeBonesLabel );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Remove Bones Spreadsheet
    AddRegion( "RemoveBonesList", "RemoveBonesList",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iRowHeight * 5, kFBAttachNone, NULL, 1.0 );
    SetControl( "RemoveBonesList", m_removeBonesList );

	m_iAttachTopY += (rexMBAnimExportCommon::c_iRowHeight * 5) + rexMBAnimExportCommon::c_iMargin;

	AddRegion( "DamageLabel", "DamageLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "DamageLabel", m_canApplyRealDamageLabel );

	AddRegion( "DamageCheckbox", "DamageCheckbox",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "DamageCheckbox", m_canApplyRealDamageCheckbox );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditModelObjectAttributesPopup::UICreate();
}

void EditVehicleModelObjectAttributesPopup::UIConfigure()
{
    // Remove Bones
    m_removeBonesLabel.Caption = "Remove Bones:";

    m_removeBonesList.Style = kFBVerticalList;

	m_canApplyRealDamageLabel.Caption = "Real Damage:";
	m_canApplyRealDamageLabel.Justify = kFBTextJustifyRight;

	m_canApplyRealDamageCheckbox.Style = kFBCheckbox;
    
    EditModelObjectAttributesPopup::UIConfigure();
}

void EditVehicleModelObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    EditModelObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

    const cutfVehicleModelObject *pVehicleModelObj = dynamic_cast<const cutfVehicleModelObject *>( pObject );

    m_removeBonesList.Items.Clear();
    const atArray<atString>& removeBoneNameList = pVehicleModelObj->GetRemoveBoneNameList();
    for ( int i = 0; i < removeBoneNameList.GetCount(); ++i )
    {
        m_removeBonesList.Items.Add( const_cast<char *>( removeBoneNameList[i].c_str() ) );
    }

	m_canApplyRealDamageCheckbox.State = pVehicleModelObj->GetCanApplyRealDamage() ? 1 : 0;
}

void EditVehicleModelObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
    EditModelObjectAttributesPopup::SetObjectFromUI( pObject );

	cutfVehicleModelObject *pVehicleModelObj = dynamic_cast<cutfVehicleModelObject *>( pObject );

	pVehicleModelObj->SetCanApplyRealDamage(m_canApplyRealDamageCheckbox.State == 1 ? true : false );
}

//##############################################################################

EditFindModelObjectAttributesPopup::EditFindModelObjectAttributesPopup()
{

}

EditFindModelObjectAttributesPopup::~EditFindModelObjectAttributesPopup()
{

}

void EditFindModelObjectAttributesPopup::UICreate()
{
    // Position
    AddRegion( "PositionLabel", "PositionLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "PositionLabel", m_positionLabel );

    AddRegion( "PositionXNumericUpDown", "PositionXNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "PositionXNumericUpDown", m_positionXNumericUpDown );

    AddRegion( "PositionYNumericUpDown", "PositionYNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "PositionYNumericUpDown", m_positionYNumericUpDown );

    AddRegion( "PositionZNumericUpDown", "PositionZNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 2), kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "PositionZNumericUpDown", m_positionZNumericUpDown );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Radius
    AddRegion( "RadiusLabel", "RadiusLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "RadiusLabel", m_radiusLabel );

    AddRegion( "RadiusNumericUpDown", "RadiusNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "RadiusNumericUpDown", m_radiusNumericUpDown );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditObjectAttributesPopup::UICreate();
}

void EditFindModelObjectAttributesPopup::UIConfigure()
{
    // Position
    m_positionLabel.Caption = "Position:";
    m_positionLabel.Justify = kFBTextJustifyRight;

    m_radiusLabel.Caption = "Radius:";
    m_radiusLabel.Justify = kFBTextJustifyRight;

    EditObjectAttributesPopup::UIConfigure();
}

void EditFindModelObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    EditObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

    const cutfFindModelObject *pFindModelObj = dynamic_cast<const cutfFindModelObject *>( pObject );
    
    const Vector3 &pos = pFindModelObj->GetPosition();
    m_positionXNumericUpDown.Value = pos.GetX();
    m_positionYNumericUpDown.Value = pos.GetY();
    m_positionZNumericUpDown.Value = pos.GetZ();

    m_radiusNumericUpDown.Value = pFindModelObj->GetRadius();
}

void EditFindModelObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
    EditObjectAttributesPopup::SetObjectFromUI( pObject );

    cutfFindModelObject *pFindModelObj = dynamic_cast<cutfFindModelObject *>( pObject );

    pFindModelObj->SetPosition( Vector3( float(m_positionXNumericUpDown.Value), float(m_positionYNumericUpDown.Value), float(m_positionZNumericUpDown.Value) ) );
    pFindModelObj->SetRadius( float(m_radiusNumericUpDown.Value) );
}

//##############################################################################

EditBlockingBoundsObjectAttributesPopup::EditBlockingBoundsObjectAttributesPopup()
{

}

EditBlockingBoundsObjectAttributesPopup::~EditBlockingBoundsObjectAttributesPopup()
{

}

void EditBlockingBoundsObjectAttributesPopup::UICreate()
{
    // Corner 1
    AddRegion( "Corner1Label", "Corner1Label",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner1Label", m_corner1Label );

    AddRegion( "Corner1XNumericUpDown", "Corner1XNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner1XNumericUpDown", m_corner1XNumericUpDown );

    AddRegion( "Corner1YNumericUpDown", "Corner1YNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner1YNumericUpDown", m_corner1YNumericUpDown );

    AddRegion( "Corner1ZNumericUpDown", "Corner1ZNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 2), kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner1ZNumericUpDown", m_corner1ZNumericUpDown );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Corner 2
    AddRegion( "Corner2Label", "Corner2Label",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner2Label", m_corner2Label );

    AddRegion( "Corner2XNumericUpDown", "Corner2XNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner2XNumericUpDown", m_corner2XNumericUpDown );

    AddRegion( "Corner2YNumericUpDown", "Corner2YNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner2YNumericUpDown", m_corner2YNumericUpDown );

    AddRegion( "Corner2ZNumericUpDown", "Corner2ZNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 2), kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner2ZNumericUpDown", m_corner2ZNumericUpDown );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Corner 3
    AddRegion( "Corner3Label", "Corner3Label",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner3Label", m_corner3Label );

    AddRegion( "Corner3XNumericUpDown", "Corner3XNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner3XNumericUpDown", m_corner3XNumericUpDown );

    AddRegion( "Corner3YNumericUpDown", "Corner3YNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner3YNumericUpDown", m_corner3YNumericUpDown );

    AddRegion( "Corner3ZNumericUpDown", "Corner3ZNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 2), kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner3ZNumericUpDown", m_corner3ZNumericUpDown );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Corner 4
    AddRegion( "Corner4Label", "Corner4Label",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner4Label", m_corner4Label );

    AddRegion( "Corner4XNumericUpDown", "Corner4XNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner4XNumericUpDown", m_corner4XNumericUpDown );

    AddRegion( "Corner4YNumericUpDown", "Corner4YNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner4YNumericUpDown", m_corner4YNumericUpDown );

    AddRegion( "Corner4ZNumericUpDown", "Corner4ZNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin + (rexMBAnimExportCommon::c_iNumericUpDownWidth * 2), kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "Corner4ZNumericUpDown", m_corner4ZNumericUpDown );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Height
    AddRegion( "HeightLabel", "HeightLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "HeightLabel", m_heightLabel );

    AddRegion( "HeightNumericUpDown", "HeightNumericUpDown",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "HeightNumericUpDown", m_heightNumericUpDown );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditObjectAttributesPopup::UICreate();
}

void EditBlockingBoundsObjectAttributesPopup::UIConfigure()
{
    // Corner 1
    m_corner1Label.Caption = "Corner 1:";
    m_corner1Label.Justify = kFBTextJustifyRight;
    
    m_corner1XNumericUpDown.Enabled = false;
    m_corner1YNumericUpDown.Enabled = false;
    m_corner1ZNumericUpDown.Enabled = false;

    // Corner 2
    m_corner2Label.Caption = "Corner 2:";
    m_corner2Label.Justify = kFBTextJustifyRight;
    
    m_corner2XNumericUpDown.Enabled = false;
    m_corner2YNumericUpDown.Enabled = false;
    m_corner2ZNumericUpDown.Enabled = false;

    // Corner 3
    m_corner3Label.Caption = "Corner 3:";
    m_corner3Label.Justify = kFBTextJustifyRight;
    
    m_corner3XNumericUpDown.Enabled = false;
    m_corner3YNumericUpDown.Enabled = false;
    m_corner3ZNumericUpDown.Enabled = false;

    // Corner 4
    m_corner4Label.Caption = "Corner 4:";
    m_corner4Label.Justify = kFBTextJustifyRight;
    
    m_corner4XNumericUpDown.Enabled = false;
    m_corner4YNumericUpDown.Enabled = false;
    m_corner4ZNumericUpDown.Enabled = false;

    // Height
    m_heightLabel.Caption = "Height:";
    m_heightLabel.Justify = kFBTextJustifyRight;
    
    m_heightNumericUpDown.Enabled = false;

    EditObjectAttributesPopup::UIConfigure();
}

void EditBlockingBoundsObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    EditObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

    const cutfBlockingBoundsObject *pBlockingBoundsObj = dynamic_cast<const cutfBlockingBoundsObject *>( pObject );

    const Vector3 &corner1 = pBlockingBoundsObj->GetCorner( 0 );
    m_corner1XNumericUpDown.Value = corner1.GetX();
    m_corner1YNumericUpDown.Value = corner1.GetY();
    m_corner1ZNumericUpDown.Value = corner1.GetZ();

    const Vector3 &corner2 = pBlockingBoundsObj->GetCorner( 1 );
    m_corner2XNumericUpDown.Value = corner2.GetX();
    m_corner2YNumericUpDown.Value = corner2.GetY();
    m_corner2ZNumericUpDown.Value = corner2.GetZ();

    const Vector3 &corner3 = pBlockingBoundsObj->GetCorner( 2 );
    m_corner3XNumericUpDown.Value = corner3.GetX();
    m_corner3YNumericUpDown.Value = corner3.GetY();
    m_corner3ZNumericUpDown.Value = corner3.GetZ();

    const Vector3 &corner4 = pBlockingBoundsObj->GetCorner( 3 );
    m_corner4XNumericUpDown.Value = corner4.GetX();
    m_corner4YNumericUpDown.Value = corner4.GetY();
    m_corner4ZNumericUpDown.Value = corner4.GetZ();

    m_heightNumericUpDown.Value = pBlockingBoundsObj->GetHeight();
}

void EditBlockingBoundsObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
    EditObjectAttributesPopup::SetObjectFromUI( pObject );

    // nothing to do because we're working off a clone of the original object and everything is read-only
}

//##############################################################################

EditLightObjectAttributesPopup::EditLightObjectAttributesPopup()
{

}

EditLightObjectAttributesPopup::~EditLightObjectAttributesPopup()
{

}

void EditLightObjectAttributesPopup::UICreate()
{
    // Light Type
    AddRegion( "m_lightTypeLabel", "m_lightTypeLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "m_lightTypeLabel", m_lightTypeLabel );

    AddRegion( "LightType", "LightType",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "LightType", m_lightType );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	AddRegion( "m_lightPropertyLabel", "m_lightPropertyLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "m_lightPropertyLabel", m_lightPropertyLabel );

	AddRegion( "LightProperty", "LightProperty",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LightProperty", m_lightPropertyFileComboBox );

	m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditObjectAttributesPopup::UICreate();
}

void EditLightObjectAttributesPopup::UIConfigure()
{
    // Light Label
    m_lightTypeLabel.Caption = "Light Type:";
    m_lightTypeLabel.Justify = kFBTextJustifyRight;

	m_lightPropertyLabel.Caption = "Light Property:";
	m_lightPropertyLabel.Justify = kFBTextJustifyRight;

    EditObjectAttributesPopup::UIConfigure();
}

void EditLightObjectAttributesPopup::SetProperties( const atArray<atString>& atEntries )
{
	for(int i=0; i < atEntries.GetCount(); ++i)
	{
		m_lightPropertyFileComboBox.Items.Add((char*)atEntries[i].c_str());
	}
}

void EditLightObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    EditObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

    const cutfLightObject *pLightObj = dynamic_cast<const cutfLightObject *>( pObject );

    switch ( pLightObj->GetLightType() )
    {
    case CUTSCENE_DIRECTIONAL_LIGHT_TYPE:
        m_lightType.Caption = "Directional";
        break;
    case CUTSCENE_SPOT_LIGHT_TYPE:
        m_lightType.Caption = "Spot";
        break;
    case CUTSCENE_POINT_LIGHT_TYPE:
        m_lightType.Caption = "Point";
        break;
    default:
        m_lightType.Caption = "(unknown)";
        break;
    }

	atString strLightProperty;

	switch( pLightObj->GetLightProperty() )
	{
	case cutfLightObject::LP_NO_PROPERTY:
		strLightProperty = "LP_NO_PROPERTY";
		break;
	case cutfLightObject::LP_CASTS_SHADOWS:
		strLightProperty = "LP_CASTS_SHADOWS";
		break;
	case cutfLightObject::LP_ENVIRONMENT:
		strLightProperty = "LP_ENVIRONMENT";
		break;
	}

	m_lightPropertyFileComboBox.ItemIndex = 0;
	for ( int i = 1; i < m_lightPropertyFileComboBox.Items.GetCount(); ++i )
	{
		char cProperty[CUTSCENE_OBJNAMELEN];
		safecpy( cProperty, m_lightPropertyFileComboBox.Items.GetAt( i ), sizeof(cProperty) );

		if ( stricmp( cProperty, strLightProperty.c_str() ) == 0 )
		{
			m_lightPropertyFileComboBox.ItemIndex = i;
			break;
		}
	}

}

void EditLightObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
    EditObjectAttributesPopup::SetObjectFromUI( pObject );

	cutfLightObject *pLightObj = dynamic_cast<cutfLightObject *>( pObject );

	atString strProperty(m_lightPropertyFileComboBox.Items.GetAt( m_lightPropertyFileComboBox.ItemIndex ));

	if(stricmp(strProperty.c_str(), "LP_NO_PROPERTY") == 0)
	{
		pLightObj->SetLightProperty(cutfLightObject::LP_NO_PROPERTY);
	}
	else if(stricmp(strProperty.c_str(), "LP_CASTS_SHADOWS") == 0)
	{
		pLightObj->SetLightProperty(cutfLightObject::LP_CASTS_SHADOWS);
	}
	else if(stricmp(strProperty.c_str(), "LP_ENVIRONMENT") == 0)
	{
		pLightObj->SetLightProperty(cutfLightObject::LP_ENVIRONMENT);
	}
}

//##############################################################################

EditAnimatedParticleEffectObjectAttributesPopup::EditAnimatedParticleEffectObjectAttributesPopup()
{

}

EditAnimatedParticleEffectObjectAttributesPopup::~EditAnimatedParticleEffectObjectAttributesPopup()
{

}

void EditAnimatedParticleEffectObjectAttributesPopup::UICreate()
{
    // Model Name
    AddRegion( "ModelNameLabel", "ModelNameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ModelNameLabel", m_modelNameLabel );

    AddRegion( "ModelNameTextBox", "ModelNameTextBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        m_iAttachTopY, kFBAttachLeft, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ModelNameTextBox", m_modelNameTextBox );

    m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    EditObjectAttributesPopup::UICreate();
}

void EditAnimatedParticleEffectObjectAttributesPopup::UIConfigure()
{
    // Model Name
    m_modelNameLabel.Caption = "Model Name:";
    m_modelNameLabel.Justify = kFBTextJustifyRight;

    EditObjectAttributesPopup::UIConfigure();
}

void EditAnimatedParticleEffectObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
    EditObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

    const cutfAnimatedParticleEffectObject *pAnimatedParticleEffectObj = dynamic_cast<const cutfAnimatedParticleEffectObject *>( pObject );
	if(pAnimatedParticleEffectObj)
	{
		m_modelNameTextBox.Caption = const_cast<char *>( pAnimatedParticleEffectObj->GetName().GetCStr() );
	}

	const cutfParticleEffectObject *pParticleEffectObj = dynamic_cast<const cutfParticleEffectObject *>( pObject );
	if(pParticleEffectObj)
	{
		m_modelNameTextBox.Caption = const_cast<char *>( pParticleEffectObj->GetName().GetCStr() );
	}
}

void EditAnimatedParticleEffectObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
    EditObjectAttributesPopup::SetObjectFromUI( pObject );
}

//##############################################################################

EditWeaponModelObjectAttributesPopup::EditWeaponModelObjectAttributesPopup()
{

}

EditWeaponModelObjectAttributesPopup::~EditWeaponModelObjectAttributesPopup()
{

}

void EditWeaponModelObjectAttributesPopup::SetProperties( const atArray<atString>& atEntries )
{
	for(int i=0; i < atEntries.GetCount(); ++i)
	{
		m_weaponTypeFileComboBox.Items.Add((char*)atEntries[i].c_str());
	}
}

void EditWeaponModelObjectAttributesPopup::UICreate()
{
	AddRegion( "m_weaponTypeLabel", "m_weaponTypeLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "m_weaponTypeLabel", m_weaponTypeLabel );

	AddRegion( "WeaponType", "WeaponType",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "WeaponType", m_weaponTypeFileComboBox ); 

	m_iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	EditModelObjectAttributesPopup::UICreate();
}

void EditWeaponModelObjectAttributesPopup::UIConfigure()
{
	m_weaponTypeLabel.Caption = "Weapon Type:";
	m_weaponTypeLabel.Justify = kFBTextJustifyRight;

	EditModelObjectAttributesPopup::UIConfigure();
}

void EditWeaponModelObjectAttributesPopup::SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile )
{
	EditModelObjectAttributesPopup::SetUIFromObject( pObject, pCutFile );

	const cutfWeaponModelObject *pWeaponObj = dynamic_cast<const cutfWeaponModelObject *>( pObject );

	atString strWeaponType("");

	switch( pWeaponObj->GetGenericWeaponType() )
	{
	case CUTSCENE_NO_GENERIC_WEAPON_TYPE:
		strWeaponType = "CUTSCENE_NO_GENERIC_WEAPON_TYPE";
		break;
	case CUTSCENE_ONE_HANDED_GUN_GENERIC_WEAPON_TYPE:
		strWeaponType = "CUTSCENE_ONE_HANDED_GUN_GENERIC_WEAPON_TYPE";
		break;
	case CUTSCENE_TWO_HANDED_GUN_GENERIC_WEAPON_TYPE:
		strWeaponType = "CUTSCENE_TWO_HANDED_GUN_GENERIC_WEAPON_TYPE";
		break;
	}

	m_weaponTypeFileComboBox.ItemIndex = 0;
	for ( int i = 1; i < m_weaponTypeFileComboBox.Items.GetCount(); ++i )
	{
		char cType[CUTSCENE_OBJNAMELEN];
		safecpy( cType, m_weaponTypeFileComboBox.Items.GetAt( i ), sizeof(cType) );

		if ( stricmp( cType, strWeaponType.c_str() ) == 0 )
		{
			m_weaponTypeFileComboBox.ItemIndex = i;
			break;
		}
	}
}

void EditWeaponModelObjectAttributesPopup::SetObjectFromUI( cutfObject *pObject )
{
	cutfWeaponModelObject *pWeaponObj = dynamic_cast<cutfWeaponModelObject *>( pObject );

	atString strType(m_weaponTypeFileComboBox.Items.GetAt( m_weaponTypeFileComboBox.ItemIndex ));

	if(stricmp(strType.c_str(), "CUTSCENE_NO_GENERIC_WEAPON_TYPE") == 0)
	{
		pWeaponObj->SetGenericWeaponType(CUTSCENE_NO_GENERIC_WEAPON_TYPE);
	}
	else if(stricmp(strType.c_str(), "CUTSCENE_ONE_HANDED_GUN_GENERIC_WEAPON_TYPE") == 0)
	{
		pWeaponObj->SetGenericWeaponType(CUTSCENE_ONE_HANDED_GUN_GENERIC_WEAPON_TYPE);
	}
	else if(stricmp(strType.c_str(), "CUTSCENE_TWO_HANDED_GUN_GENERIC_WEAPON_TYPE") == 0)
	{
		pWeaponObj->SetGenericWeaponType(CUTSCENE_TWO_HANDED_GUN_GENERIC_WEAPON_TYPE);
	}

	EditModelObjectAttributesPopup::SetObjectFromUI( pObject );
}

//##############################################################################

} // namespace rage
