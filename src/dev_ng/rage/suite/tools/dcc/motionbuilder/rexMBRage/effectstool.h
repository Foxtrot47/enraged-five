#ifndef __REXMBRAGE_EFFECTSTOOL_H__
#define __REXMBRAGE_EFFECTSTOOL_H__

// SDK include
#include <list>
#include "rexMBAnimExportCommon/utility.h"


class RexRageEffectsTool : public FBTool
{
	// Tool declaration
	FBToolDeclare( RexRageEffectsTool, FBTool );

public:
	virtual bool FBCreate();		// Constructor
	virtual void FBDestroy();		// Destructor

private:
	void UICreate();
	void UIConfigure();
	
    void HelpButton_Click( HISender pSender, HKEvent pEvent );
	
    void FxListComboBox_Changed( HISender pSender,HKEvent pEvent );
    void CreateEffectButton_Click( HISender pSender,HKEvent pEvent );
	void CreateDecalButton_Click( HISender pSender,HKEvent pEvent );
    void RefreshEffectsButton_Click( HISender pSender,HKEvent pEvent );
	void RefreshDecalsButton_Click( HISender pSender,HKEvent pEvent );
	void ToggleRootCheckbox_Click( HISender pSender,HKEvent pEvent );

    void CreateDOFObjectsButton_Click( HISender pSender,HKEvent pEvent );
    void CreateScreenFadeObjectButton_Click( HISender pSender,HKEvent pEvent );
    
    void LightIntensitySlider_Changed( HISender pSender,HKEvent pEvent );
    void LightIntensityNumericUpDown_Changed( HISender pSender,HKEvent pEvent );
	void IncreaseLightIntensityButton_Click( HISender pSender,HKEvent pEvent );
	void DecreaseLightIntensityButton_Click( HISender pSender,HKEvent pEvent );

	void UpdateSceneLights( bool bIncrease );

	bool LoadFxListFile( const char *pFilename );

	int GetConstraintIndex(char* cConstraintName);

	void RefreshDecalList();

    FBLabel m_rmptfxLabel;
    FBLabel m_rmptfxPathLabel;
	FBLabel m_scriptDatLabel;
	FBLabel m_scriptDatPathLabel;

    FBLabel m_fxListLabel;
    FBList m_fxListFileComboBox;
	FBButton m_toggleRootCheckbox;

    FBLabel m_effectNamesLabel;
	FBList m_effectNamesComboBox;
	FBButton m_createEffectButton;
    FBButton m_refreshEffectsButton;

	FBLabel m_decalNamesLabel;
	FBList m_decalNamesComboBox;
	FBButton m_createDecalButton;
	FBButton m_refreshDecalButton;
	
    FBButton m_createDOFObjectsButton;
    FBButton m_createScreenFadeObjectButton;

    FBLabel m_lightIntensityLabel;
	FBSlider m_lightIntensitySlider;
    FBEditNumber m_lightIntensityNumericUpDown;
	FBButton m_increaseLightIntensityButton;
	FBButton m_decreaseLightIntensityButton;
};

#endif // __REXMBRAGE_EFFECTSTOOL_H__
