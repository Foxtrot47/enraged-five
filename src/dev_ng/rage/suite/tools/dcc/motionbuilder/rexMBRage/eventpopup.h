// 
// rexMBRage/eventpopup.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REXMBRAGE_EVENTPOPUP_H__ 
#define __REXMBRAGE_EVENTPOPUP_H__ 

#include "atl/map.h"
#include "rexMBAnimExportCommon/utility.h"
#include "cutfile/cutfdefines.h"
#include "init/ini.h"
#include "parsercore/attribute.h"

namespace rage {

class cutfObject;
class cutfCutsceneFile2;
class cutfEvent;
class cutfEventArgs;
class cutfEventDef;
class cutfEventDefs;
class cutfEventArgsDef;

//##############################################################################

class EditEventPopup : public FBPopup
{
public:
    EditEventPopup( cutfCutsceneFile2 *pCutFile, const cutfEventDefs *pEventDefs=NULL );
    virtual ~EditEventPopup();

    // PURPOSE: Clears the UI and deletes everything.
    void Shutdown();

    // PURPOSE: Updates the UI with the latest data contains in the cutfile
    void RefreshUI();

    // PURPOSE: Clears the UI.
    void Clear();

private:
    enum EEventSpreadsheetColumn
    {
        EVENT_NAME_SPREADSHEET_COLUMN = -1,
        EVENT_TYPE_SPREADSHEET_COLUMN,
        EVENT_RECIPIENT_SPREADSHEET_COLUMN,
        EVENT_FRAME_SPREADSHEET_COLUMN,
        EVENT_ARGS_SPREADSHEET_COLUMN,
    };

    enum EEventEditMode
    {
        NO_EVENT_MODE,
        ADD_EVENT_MODE,
        EDIT_EVENT_MODE
    };

    enum EEventArgsSpreadsheetColumn
    {
        EVENT_ARGS_NAME_SPREADSHEET_COLUMN = -1,
        EVENT_ARGS_REFERENCE_COUNT_SPREADSHEET_COLUMN,
    };

    enum EEventArgsEditMode
    {
        NO_EVENT_ARGS_MODE,
        ADD_EVENT_ARGS_MODE,
        EDIT_EVENT_ARGS_MODE,
        ADD_EVENT_ARGS_ATTRIBUTE_MODE,
        EDIT_EVENT_ARGS_ATTRIBUTE_MODE
    };

    enum EAttributeSpreadsheetColumn
    {
        ATTRIBUTE_NAME_SPREADSHEET_COLUMN = -1,
        ATTRIBUTE_TYPE_SPREADSHEET_COLUMN,
        ATTRIBUTE_VALUE_SPREADSHEET_COLUMN,
        ATTRIBUTE_DESCRIPTION_SPREADSHEET_COLUMN
    };

    struct SEditEventArgsData
    {
        cutfEventArgs *pEventArgs;
        const cutfEventArgsDef *pEventArgsDef;
        ConstString displayName;

        SEditEventArgsData()
            : pEventArgs(NULL)
            , pEventArgsDef(NULL)
            , displayName("")
        {

        }
    };

    struct SEditEventData
    {
        cutfEvent *pEvent;
        const cutfEventDef *pEventDef;
        bool bIsLoadEvent;

        SEditEventArgsData *pEventArgsData;

        SEditEventData()
            : pEvent(NULL)
            , pEventDef(NULL)
            , bIsLoadEvent(false)
            , pEventArgsData(NULL)
        {

        }
    };

    void UICreate();

    void UICreateEventLayout();

    void UICreateEventListLayout();

    void UICreateEventEditLayout();

    void UICreateEventArgsLayout();

    void UICreateEventArgsListLayout();

    void UICreateEventArgsEditLayout();

    void UIConfigure();

    void UIConfigureEventLayout();

    void UIConfigureEventListLayout();

    void UIConfigureEventEditLayout();

    void UIConfigureEventArgsLayout();

    void UIConfigureEventArgsListLayout();

    void UIConfigureEventArgsEditLayout();

    EEventEditMode GetEventEditMode() const;

    void SetEventEditMode( EEventEditMode mode );

    EEventArgsEditMode GetEventArgsEditMode() const;

    void SetEventArgsEditMode( EEventArgsEditMode mode );

    bool HaveUnsavedEventChanges() const;

    void SetHaveUnsavedEventChanges( bool bHaveChanges );

    bool HaveUnsavedEventArgsChanges() const;

    void SetHaveUnsavedEventArgsChanges( bool bHaveChanges );

    bool HaveUnsavedAttributeChanges() const;

    void SetHaveUnsavedAttributeChanges( bool bHaveChanges );

    void CreateBuiltInEventDefs();

    void ConstructEventDef( s32 iEventId1, s32 iEventId2, const cutfEventArgsDef *pEventArgsDef );

    SEditEventData* CreateEventData( const cutfEvent *pEvent );

    void DestroyEventData( SEditEventData* &pEventData );
    
    SEditEventArgsData* CreateEventArgsData( const cutfEventArgs *pEventArgs, const cutfEventArgsDef *pEventArgsDef=NULL );

    void DestroyEventArgsData( SEditEventArgsData* &pEventArgsData );

    void ClearEventSpreadsheet();

    void ClearEventArgsSpreadsheet();

    void ClearAttributeSpreadsheet();

    void AddRowToEventSpreadsheet( SEditEventData *pEventData );

    void RemoveRowFromEventSpreadsheet( SEditEventData *pEventData );

    void AddRowToEventArgsSpreadsheet( SEditEventArgsData *pEventArgsData );

    void RemoveRowFromEventArgsSpreadsheet( SEditEventArgsData *pEventArgsData );

    void AddRowToAttributeSpreadsheet( parAttribute *pAttribute );

    void RemoveRowFromAttributesSpreadsheet( parAttribute *pAttribute );

    void UpdateEventInSpreadsheet( const SEditEventData *pEventData );

    void UpdateUIFromEventArgsReferenceCount();

    void SetUIFromEvent( SEditEventData *pEventData );

    bool SetEventFromUI( SEditEventData *pEventData );

    void SetUIFromEventArgs( SEditEventArgsData *pEventArgsData );

    bool SetEventArgsFromUI( SEditEventArgsData *pEventArgsData );

    void SetUIFromAttribute( parAttribute *pAttribute );

    bool SetAttributeFromUI( parAttribute *pAttribute );

    bool PromptToSaveEvent( bool bAllowCancel );

    bool PromptToSaveEventArgs( bool bAllowCancel );

    bool PromptToSaveAttribute( bool bAllowCancel );

    void CopyToAttributeList( SEditEventArgsData *pEventArgsData );

    void CopyFromAttributeList( SEditEventArgsData *pEventArgsData );

    cutfCutsceneFile2* m_pCutFile;
    const cutfEventDefs* m_pEventDefs;

    EEventEditMode m_eventEditMode;
    EEventArgsEditMode m_eventArgsEditMode;
    EEventArgsEditMode m_lastEventArgsEditMode;

    SEditEventData* m_pCurrentEventData;
    SEditEventArgsData* m_pCurrentEventArgsData;
    parAttribute *m_pCurrentAttribute;

    atMap<s32, const cutfEventDef *> m_builtInEventDefsMap;
    atMap<s32, const cutfEventArgsDef *> m_builtInEventArgsDefsMap;
    atMap<const cutfEventArgsDef *, int> m_eventArgsDefCountMap;

    atArray<SEditEventData *> m_eventDataList;
    atMap<SEditEventData *, int> m_eventDataRowRefMap;
    atMap<int, SEditEventData *> m_rowRefEventDataMap;
    
    atArray<SEditEventArgsData *> m_eventArgsDataList;
    atMap<SEditEventArgsData *, int> m_eventArgsDataRowRefMap;
    atMap<int, SEditEventArgsData *> m_rowRefEventArgsDataMap;
    atArray<SEditEventArgsData *> m_eventArgsDataComboBoxList;
    
    parAttributeList m_currentAttributeList;
    atMap<parAttribute *, int> m_attributeRowRefMap;
    atMap<int, parAttribute *> m_rowRefAttributeMap;

    int m_iEventSpreadsheetRowRef;
    int m_iEventArgsSpreadsheetRowRef;
    int m_iAttributeSpreadsheetRowRef;

    int m_iRestrictedFromEditingAttributeCount;
    int m_iRestrictedFromRemovalAttributeCount;    

    bool m_bModifiedCutfile;
    bool m_bHhaveUnsavedEventChanges;
    bool m_bHaveUnsavedEventArgsChanges;
    bool m_bHaveUnsavedAttributeChanges;

    bool m_bDisplayAll;
    bool m_bUserModifiedDisplayAll;

    FBLayout m_eventsLayout;
    FBLayout m_eventListLayout;
    FBLayout m_eventEditLayout;
    FBLayout m_eventArgsLayout;
    FBLayout m_eventArgsListLayout;
    FBLayout m_eventArgsEditLayout;

    FBLayoutRegion m_eventEditLayoutRegion;
    FBLayoutRegion m_eventArgsEditLayoutRegion;
    FBLayoutRegion m_eventArgsAttributeEditLayoutRegion;

    FBSpread m_eventSpreadsheet;

    FBButton m_addEventButton;
    FBButton m_editEventButton;
    FBButton m_removeEventButton;
    FBButton m_sortEventsButton;

    FBLabel m_eventNameLabel;
    FBLabel m_eventNameTextBox;

    FBLabel m_eventTypeLabel;
    FBButton m_eventTypeCheckBox;

    FBLabel m_eventRecipientLabel;
    FBList m_eventRecipientComboBox;    

    FBLabel m_eventFrameLabel;
    FBEditNumber m_eventFrameNumericUpDown;

    FBLabel m_eventEventArgsLabel;
    FBList m_eventEventArgsComboBox;

    FBButton m_saveEventChangesButton;
    FBButton m_discardEventChangesButton;

    FBSpread m_eventArgsSpreadsheet;

    FBButton m_addEventArgsButton;
    FBButton m_editEventArgsButton;
    FBButton m_removeEventArgsButton;
	FBButton m_cleanupEventArgsButton;

    FBLabel m_eventArgsNameLabel;
    FBLabel m_eventArgsNameTextBox;

    FBSpread m_attributeSpreadsheet;

    FBButton m_editAttributeButton;
    FBButton m_addAttributeButton;
    FBButton m_removeAttributeButton;
    FBButton m_removeAllAttributesButton;

    FBLabel m_attributeNameLabel;
    FBEdit m_attributeNameTextBox;

    FBLabel m_attributeTypeLabel;
    FBList m_attributeTypeComboBox;

    FBLabel m_attributeValueLabel;
    FBEdit m_attributeValueTextBox;

    FBButton m_doneEditAttributeButton;
    FBButton m_cancelEditAttributeButton;

    FBButton m_saveEventArgsChangesButton;
    FBButton m_discardEventArgsChangesButton;

    FBButton m_closeButton;

    void EventSpreadsheet_RowClick( HISender pSender, HKEvent pEvent );

    void AddEventButton_Click( HISender pSender, HKEvent pEvent );
    void EditEventButton_Click( HISender pSender, HKEvent pEvent );
    void RemoveEventButton_Click( HISender pSender, HKEvent pEvent );
    void SortEventsButton_Click( HISender pSender, HKEvent pEvent );

    void EventTypeCheckBox_Click( HISender pSender, HKEvent pEvent );
    void EventRecipientComboBox_Change( HISender pSender, HKEvent pEvent );
    void EventFrameNumericUpDown_Change( HISender pSender, HKEvent pEvent );
    void EventEventArgsComboBox_Change( HISender pSender, HKEvent pEvent );

    void SaveEventChangesButton_Click( HISender pSender, HKEvent pEvent );
    void DiscardEventChangesButton_Click( HISender pSender, HKEvent pEvent );

    void EventArgsSpreadsheet_RowClick( HISender pSender, HKEvent pEventArgs );

    void AddEventArgsButton_Click( HISender pSender, HKEvent pEventArgs );
    void EditEventArgsButton_Click( HISender pSender, HKEvent pEventArgs );
    void RemoveEventArgsButton_Click( HISender pSender, HKEvent pEventArgs );
    void CleanupEventArgsButton_Click( HISender pSender, HKEvent pEventArgs );

    void AttributeSpreadsheet_RowClick( HISender pSender, HKEvent pEvent );

    void EditAttributeButton_Click( HISender pSender, HKEvent pEvent );
    void AddAttributeButton_Click( HISender pSender, HKEvent pEvent );
    void RemoveAttributeButton_Click( HISender pSender, HKEvent pEvent );
    void RemoveAllAttributesButton_Click( HISender pSender, HKEvent pEvent );

    void AttributeNameTextBox_Change( HISender pSender, HKEvent pEvent );
    void AttributeTypeComboBox_Change( HISender pSender, HKEvent pEvent );
    void AttributeValueTextBox_Change( HISender pSender, HKEvent pEvent );

    void DoneEditAttributeButton_Click( HISender pSender, HKEvent pEvent );
    void CancelEditAttributeButton_Click( HISender pSender, HKEvent pEvent );

    void SaveEventArgsChangesButton_Click( HISender pSender, HKEvent pEventArgs );
    void DiscardEventArgsChangesButton_Click( HISender pSender, HKEvent pEventArgs );

    void HelpButton_Click( HISender pSender, HKEvent pEventArgs );
    void CloseButton_Click( HISender pSender, HKEvent pEventArgs );
};

inline EditEventPopup::EEventEditMode EditEventPopup::GetEventEditMode() const
{
    return m_eventEditMode;
}

inline EditEventPopup::EEventArgsEditMode EditEventPopup::GetEventArgsEditMode() const
{
    return m_eventArgsEditMode;
}

inline bool EditEventPopup::HaveUnsavedEventChanges() const
{
    return m_bHhaveUnsavedEventChanges;
}

inline void EditEventPopup::SetHaveUnsavedEventChanges( bool bHaveChanges )
{
    m_bHhaveUnsavedEventChanges = bHaveChanges;

    if ( GetEventEditMode() != NO_EVENT_MODE )
    {
        m_saveEventChangesButton.Enabled = bHaveChanges;
    }
}

inline bool EditEventPopup::HaveUnsavedEventArgsChanges() const
{
    return m_bHaveUnsavedEventArgsChanges;
}

inline void EditEventPopup::SetHaveUnsavedEventArgsChanges( bool bHaveChanges )
{
    m_bHaveUnsavedEventArgsChanges = bHaveChanges;

    if ( GetEventArgsEditMode() != NO_EVENT_ARGS_MODE )
    {
        m_saveEventArgsChangesButton.Enabled = bHaveChanges;
    }
}

inline bool EditEventPopup::HaveUnsavedAttributeChanges() const
{
    return m_bHaveUnsavedAttributeChanges;
}

inline void EditEventPopup::SetHaveUnsavedAttributeChanges( bool bHaveChanges )
{
    m_bHaveUnsavedAttributeChanges = bHaveChanges;

    if ( (GetEventArgsEditMode() == ADD_EVENT_ARGS_ATTRIBUTE_MODE) || (GetEventArgsEditMode() == EDIT_EVENT_ARGS_ATTRIBUTE_MODE) )
    {
        m_doneEditAttributeButton.Enabled = bHaveChanges;
    }
}

//##############################################################################

class NewEventPopup : public FBPopup
{
public:
    NewEventPopup( const cutfEventDefs *pEventDefs );
    virtual ~NewEventPopup();

    const cutfEventDef* GetEventDef() const;

private:

    // PURPOSE: The standard space around the edges of the popup and between controls
    //    on the same line.
    static const int c_iMargin = 5;

    // PURPOSE: The standard width of a label, when it is attached to the left side
    //    and precedes an editable field (TextBox, ComboBox, etc.)
    static const int c_iLabelWidth = 75;

    // PURPOSE: The standard width of a button.
    static const int c_iButtonWidth = 60;

    static const int c_iNumericUpDownWidth = 50;

    // PURPOSE: The height of TextBoxes, ComboBoxes, Labels, etc.
    static const int c_iFieldHeight = 16;

    // PURPOSE: The height of a row
    static const int c_iRowHeight = 20;

    void UICreate();
    void UIConfigure();

    const cutfEventDefs *m_pEventDefs;

    FBLabel m_eventDefNameLabel;
    FBList m_eventDefComboBox;

    FBButton m_okButton;
    FBButton m_cancelButton;

    void OkButton_Click( HISender pSender, HKEvent pEventArgs );
    void CancelButton_Click( HISender pSender, HKEvent pEventArgs );
};

//##############################################################################

class NewEventArgsPopup : public FBPopup
{
public:
    NewEventArgsPopup( const cutfEventDefs *pEventDefs );
    virtual ~NewEventArgsPopup();

    const cutfEventArgsDef* GetEventArgsDef() const;

private:

    // PURPOSE: The standard space around the edges of the popup and between controls
    //    on the same line.
    static const int c_iMargin = 5;

    // PURPOSE: The standard width of a label, when it is attached to the left side
    //    and precedes an editable field (TextBox, ComboBox, etc.)
    static const int c_iLabelWidth = 75;

    // PURPOSE: The standard width of a button.
    static const int c_iButtonWidth = 60;

    static const int c_iNumericUpDownWidth = 50;

    // PURPOSE: The height of TextBoxes, ComboBoxes, Labels, etc.
    static const int c_iFieldHeight = 16;

    // PURPOSE: The height of a row
    static const int c_iRowHeight = 20;

    void UICreate();
    void UIConfigure();

    const cutfEventDefs *m_pEventDefs;

    FBLabel m_eventArgsDefNameLabel;
    FBList m_eventArgsDefComboBox;

    FBButton m_okButton;
    FBButton m_cancelButton;

    void OkButton_Click( HISender pSender, HKEvent pEventArgs );
    void CancelButton_Click( HISender pSender, HKEvent pEventArgs );
};

//##############################################################################

} // namespace rage

#endif __REXMBRAGE_EVENTPOPUP_H__
