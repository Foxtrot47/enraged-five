// /animExportTool.h

#ifndef __ANIM_EXPORT_TOOL_H__
#define __ANIM_EXPORT_TOOL_H__

#include <string>
#include <vector>
#include <map>
#include <list>
#include <algorithm>

#include "parser/manager.h"
#include "rexMBAnimExportCommon/userObjects.h"
#include "rexMBAnimExportCommon/animExport.h"
#include "rexMBAnimExportCommon/utility.h"

using namespace rage;

//-----------------------------------------------------------------------------

class RexRageAnimExportTool : public FBTool
{
    FBToolDeclare( RexRageAnimExportTool, FBTool );

public:
	enum eSegmentSheetColumn
    {
		kSegmentIndex = -1,
        kSegmentName,
        kStartFrame,
        kEndFrame,
#if !HACK_GTA4
		kLooping,
#endif // HACK_GTA4
		kAutoPlay,
		kMover
    };
	
	enum eTakeSheetColumn
	{
		kTakeIndex = -1,
		kTakeName,
		kTakeExport,
		kTakeAdditive,
		kTakeAdditiveBA,
		kTakeLinearCompression,
#if !HACK_GTA4
		kTakeLooping,
#endif // !HACK_GTA4
		kTakeCtrlFile,
		kTakeCompressionFile,
		kTakeAltOutputPaths,
		kTakeAddOutputPath,
		kTakeRemoveOutputPath,
		kTakeNameOverride,
		kTakeAdditiveBaseAnimName,
		kTakeAddAdditiveName,
		kTakeRemoveAdditiveName
		
	};

	enum eModelPairSheetColumn
	{
		kModelIndex = -1,
		kRootModelName,
		kMoverModelName,
		kAnimNamePrefix,
		kAnimNameSuffix,
		kCharCtrlFile,
		kModelType,
		kSkeletonFile,
		kFacialAttribute,
		kModelExport,
		kAltOutputPath,
		kMergeFaceFile,
		
	};

	struct AnimDictionary
	{
		AnimDictionary(atString outPath, atString relPath, atString dictName, atString rootDir)
		{
			OutputPath = outPath;
			RelativePath = relPath;
			DictionaryName = dictName;
			RootDirectoryName = rootDir;
		}
		AnimDictionary(){}
		atString OutputPath;
		atString RelativePath;
		atString DictionaryName;
		atString RootDirectoryName;
	};

public:
	//FiLMBOX TOOL interface...
	virtual bool FBCreate();
    virtual void FBDestroy();
	virtual bool FbxStore ( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );
	virtual bool FbxRetrieve ( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );

public:
	//General UI Presentation Methods
    virtual void UICreate();
	virtual void UICreateSegmentLayout();
	virtual void UICreateTakeLayout();

    virtual void UIConfigure();
	virtual void UIConfigureSegmentLayout();
	virtual void UIConfigureTakeLayout();

	//Progress Callback Handlers
	void UpdateProgressCaption(const char* caption);
	void UpdateProgressPercent(float percent);
    void UpdateProgressMessage( const char* message );

	//UI Callback Handlers
	void UsePerforceIntegrationButton_Click( HISender pSender, HKEvent pEvent );
    void HelpButton_Click( HISender pSender, HKEvent pEvent );
	void OuputPathPicker_Click( HISender pSender, HKEvent pEvent );
	void ControlFilePathPicker_Click( HISender pSender, HKEvent pEvent );
	void DefaultCompressionPathPicker_Click( HISender pSender, HKEvent pEvent );
	void DefaultCompressionSet_Click( HISender pSender, HKEvent pEvent );
	void AttributeFilePathPicker_Click( HISender pSender, HKEvent pEvent );
	void AddSegmentButton_Click( HISender pSender, HKEvent pEvent );
	void RemoveSegmentButton_Click( HISender pSender, HKEvent pEvent );
	void ClearSegmentsButton_Click( HISender pSender, HKEvent pEvent );
	static RexRageAnimExportTool* GetCurrentAnimExportToolInst();
	static void Export( RexRageAnimExportTool* rexRageAnimExportTool, bool bBatchExport=false );
	void ExportButton_Click( HISender pSender, HKEvent pEvent );
	void BuildButton_Click( HISender pSender, HKEvent pEvent );
	void InitOffsetCheckbox_Click( HISender pSender, HKEvent pEvent );
	void UsedInNBBlendCheckbox_Click( HISender pSender, HKEvent pEvent );
	void SaveClipButton_Click( HISender pSender, HKEvent pEvent );
	void LoadClipButton_Click( HISender pSender, HKEvent pEvent );
	void LoadMarkupButton_Click( HISender pSender, HKEvent pEvent );
	void SaveMarkupButton_Click( HISender pSender, HKEvent pEvent );
	void RefreshMarkupButton_Click( HISender pSender, HKEvent pEvent );
	void SegmentFromTakesButton_Click( HISender pSender, HKEvent pEvent);
	void AddRootButton_Click( HISender pSender, HKEvent pEvent);
	void RemoveRootButton_Click( HISender pSender, HKEvent pEvent);
	void AddMoverButton_Click( HISender pSender, HKEvent pEvent);
	void RemoveMoverButton_Click( HISender pSender, HKEvent pEvent);
	void AddAdditionalModelsButton_Click( HISender pSender, HKEvent pEvent);
	
	void RefreshTakesButton_Click( HISender pSender, HKEvent pEvent);
	void EnableAllTakesButton_Click( HISender pSender, HKEvent pEvent);
	void DisableAllTakesButton_Click( HISender pSender, HKEvent pEvent);
	void OpenExportFolder_Click( HISender pSender, HKEvent pEvent);

	void OutputPathEdit_Change( HISender pSender, HKEvent pEvent);
	void ControlFilePathEdit_Change( HISender pSender, HKEvent pEvent);

	void OriginObject_Click( HISender pSender, HKEvent pEvent);
	void ClearOriginObject_Click( HISender pSender, HKEvent pEvent);
	
	void SegmentSheet_RowClick( HISender pSender, HKEvent pEvent );
	void SegmentSheet_CellChange( HISender pSender, HKEvent pEvent );
	void TakeSheet_RowClick( HISender pSender, HKEvent pEvent );
	void TakeSheet_CellChange( HISender pSender, HKEvent pEvent);
	void ModelPairSheet_RowClick( HISender pSender, HKEvent pEvent);
	void ModelPairSheet_CellChange( HISender pSender, HKEvent pEvent);

	//System/App Event Handlers

	void FileNew_Event(HISender pSender, HKEvent pEvent);
	void FileNewCompleted_Event(HISender pSender, HKEvent pEvent);
	void FileOpenCompleted_Event(HISender pSender, HKEvent pEvent);
	void FileSave_Event(HISender pSender, HKEvent pEvent);
	void FileSaveCompleted_Event(HISender pSender, HKEvent pEvent);

	void SceneOnTakeChange_Event(HISender pSender, HKEvent pEvent);

	//UI Utility methods
	void			UIRefreshTemplateFiles();
	void			UIRestoreDefaults();
	void			UIPopulate(RexAnimMarkupData* pAnimData);
	void			UIPopulateTakeSheet(RexAnimMarkupData* pAnimData);
	void			UIResetModelPairSpreadsheet();
	void			UIResetSegmentSpreadsheet();
	void			UIResetTakeSpreadsheet();
	void			UISetModelPairSheetIndices();
	void			UISetSegSheetIndices();
	void			UISetTakeSheetIndices();
	void			UIResetProjectList();

	//void ExportSyncedSceneFile();
	static bool GetAnimationDictionaryList(RexRageAnimExportTool* rexRageAnimExportTool, atArray<AnimDictionary>& animDictionaries);

	struct AetRootMoverPair
	{
		AetRootMoverPair() { }

		string m_rootModel;
		string m_moverModel;
		string m_animPrefix;
		string m_animSuffix;
		string m_skeletonFile;
		string m_modelType;
		string m_facialAttribute;
		string m_charCtrlFile;
		string m_altOutputPath;
		bool m_modelExport;
		bool m_mergeFace;
		atArray< AetAdditionalModel* > m_additionalModels;
	};

	// PURPOSE: Updates a model pair row in the spreadsheet, updating all values from the model
	void			UpdateModelPairRow(AetRootMoverPair* pModelPair, int rowId);
	// PURPOSE: Adds a model pair to the user interface control; increments internal tracking variables.
	void			AddModelPairRow(AetRootMoverPair* pModelPair);
	AetRootMoverPair* GetModelPairRow(unsigned int idx);
	AetRootMoverPair* GetModelPairRow(const char* rootModelName, int& rowId);

	// PURPOSE: Verifies that all takes are added to the spreadsheets.
	// This is used to sync up the UI's data with the scene, when users delete, rename and rearrange takes.
	void AddMissingTakesToSpreadsheet(atString &ctrlPathsStr, atString& compressionPathsStr);
	void RemoveDuplicateTakesFromSpreadsheet();

	//Misc Utility methods
	RexAnimMarkupData*	CollectAnimExportData(bool bSkipExportCheck = false);
	bool			ValidateMarkupData(RexAnimMarkupData* pAnimData);

	//Utility Functions for customizing animation markup data directly (via script).
	bool EditRootEntry(const char* rootName, const char* moverName, char* skeletonFile, char* modelType, char* controlFile, char* facialAttributeFile);
	bool SetExportModel(const char* rootName, bool value);
	bool SetMergeFacial(const char* rootName, bool value);
	bool SetAltOutputPath(const char* rootName, const char* altOutputPath);
	bool PopulateTakeExportInfo();
	bool SetTakeExportSetting(const char* takeName, bool setExport);
	bool SetTakeAdditiveSetting(const char* takeName, bool setAdditive);
	bool SetTakeControlFile(const char* takeName, const char* controlFile);
	bool SetTakeCompressionFile(const char* takeName, const char* compressionFile);

	//Persistant Data methods
	bool					DeriveDictionaryRelPathAndName(atString& outputPath, atString& relativeOutputPath, atString& dictionaryName, atString& rootDir);
	atString				DeriveOutputPathFromSourceFbx();
	
	// PURPOSE: Get output path for currently loaded FBX
	static const char* GetExportPath();

	// PURPOSE: Get output path for currently loaded FBX
	// PARAMS:
	//	pNewExportPath - The new path
	static void SetExportPath( const char* pNewExportPath );
	static void SetControlFile( const char * pControlFile );
	static bool SetModelPairNamespace( int idx, const char* pNewNameSpace );
	static int GetModelCount();

	void AddAdditionalModelEntry( const char* pTargetModelName, const char* pModelName, const char* pTrackName, const char* pAttributeName );

protected:
	enum eLayoutType
	{
		kSegment = 0,
		kTake,

		//Must remain last in enum
		kLayoutCount
	};

	//Fixed user interface constants 
	static const int cUiMargin = 5;
    static const int cUiRowHeight = 20;
    static const int cUiShortLabelWidth = 50;
    static const int cUiLabelWidth = 100;
    static const int cUiFieldHeight = 16;
    static const int cUiButtonWidth = 75;
    static const int cUiColorWidth = 200;
    static const int cUiNumericUpDownWidth = 50;
	static const int cUiAttachLeftX = cUiMargin;
    static const int cUiAttachRightX = -cUiMargin;

	FBLabel			m_versionLabel;
	FBButton		m_usePerforceIntegration;    
    FBButton        m_helpButton;
	
	FBLabel			m_outputPathLabel;
	FBEdit			m_outputPathEdit;
	FBButton		m_outputPathButton;

	FBLabel			m_ctrlFilePathLabel;
	FBEdit			m_ctrlFilePathEdit;
	FBButton		m_ctrlFilePathButton;

	FBLabel			m_defaultCompressionLabel;
	FBEdit			m_defaultCompressionEdit;
	FBButton		m_defaultCompressionButton;
	FBButton		m_defaultCompressionSetButton;

	FBLabel			m_originObjectLabel;
	FBEdit			m_originObjectEdit;
	FBButton		m_originObjectButton;
	FBButton		m_clearOriginObjectButton;

	FBLabel			m_attributeFilePathLabel;
	FBEdit			m_attributeFilePathEdit;
	FBButton		m_attributeFilePathButton;

	FBSpread		m_modelPairSheet;
	FBButton		m_addRootButton;
	FBButton		m_addMoverButton;
	FBButton		m_removeRootButton;
	FBButton		m_removeMoverButton;
	FBButton		m_addAdditionalButton;

	FBButton		m_segmentFromTakesButton;
    FBButton        m_useParentScaleButton;
	FBButton		m_HideWarnings;
	FBButton		m_useTakeOutputPathsButton;

	FBButton		m_exportButton;
	FBButton		m_buildButton;
	FBButton		m_buildAllButton;
	FBButton		m_previewCheckbox;
	FBButton		m_initOffsetCheckbox;
	FBButton		m_usedInNMBlendCheckbox;
	FBButton		m_usedStoryModeCheckbox;
	FBButton		m_usedCameraDOFCheckbox;
	FBButton		m_renderModeCheckbox;
	//FBButton		m_PreviewButton;
	FBButton		m_saveClipButton;
	FBButton		m_loadClipButton;

	FBButton		m_saveMarkupButton;
	FBButton		m_loadMarkupButton;
	FBButton		m_refreshMarkupButton;

	FBButton		m_saveSyncedSceneButton;

	FBLayout		m_layout[kLayoutCount];
	eLayoutType		m_layoutType;

	//Segment based layout
	FBSpread		m_segmentSheet;
	FBButton		m_addSegmentButton;
	FBButton		m_removeSegmentButton;
	FBButton		m_clearSegmentsButton;

	//Take based layout
	FBSpread		m_takeSheet;
	FBButton		m_refreshTakesButton;
	FBButton		m_enableAllTakesButton;
	FBButton		m_disableAllTakesButton;
	FBButton		m_openExportFolderButton;
	
	FBSystem		m_system;
	FBApplication	m_app;

	int				m_iSegSpreadRowId;
	int				m_iSegSheetSelRowIndex;

	int				m_iTakeSpreadRowId;
	int				m_iTakeSheetSelRowIndex;

	int				m_iModelPairSpreadRowId;
	int				m_iModelPairSheetSelRowIndex;

	typedef std::map<int, RexMbAnimSegmentData*> ROWID_TO_SEG_MAP;
	ROWID_TO_SEG_MAP m_rowIdToAnimSegMap;

	typedef std::map<int, RexMbAnimSegmentData*> ROWID_TO_TAKE_MAP;
	ROWID_TO_TAKE_MAP m_rowIdToTakeMap;

	static const char* m_DefaultSpecFile;

	typedef std::map<int, AetRootMoverPair*> ROWID_TO_MODELPAIR_MAP;
	ROWID_TO_MODELPAIR_MAP m_rowIdToModelPairMap;

	// We have to store the control and compression paths as as far as
	// I can ascertain you can only access the index of a combo box
	// that is embedded in a table view
	static atArray<atString> ms_ctrlPathTemplates;
	static atArray<atString> ms_compressionPathTemplates;
	static atArray<atString> ms_skeletonFiles;
};

extern "C" __declspec(dllexport) char* GetSceneInitialOffsetOrigin();
extern "C" __declspec(dllexport) Vector3* GetSyncedSceneRotation();
extern "C" __declspec(dllexport) Vector3* GetSyncedScenePosition();

//-----------------------------------------------------------------------------

#endif // __ANIM_EXPORT_TOOL_H__
