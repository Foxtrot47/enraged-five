// 
// rexMBRage/editpopup.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REXMBRAGE_EDITPOPUP_H__ 
#define __REXMBRAGE_EDITPOPUP_H__ 

#include "cutfile/cutfdefines.h"
#include "init/ini.h"
#include "parsercore/attribute.h"
#include "vector/vector3.h"
#include "rexMBAnimExportCommon/utility.h"
#include "cutsceneExportTool.h"

namespace rage {

class cutfObject;
class cutfCutsceneFile2;

//##############################################################################

// PURPOSE: A class for displaying and editing a parAttributeList as a popup.
class EditAttributesPopup : public FBPopup
{
public:
    enum ESpreadsheetColumn
    {
        NAME_SPREADSHEET_COLUMN = -1,
        TYPE_SPREADSHEET_COLUMN,
        VALUE_SPREADSHEET_COLUMN
    };

    EditAttributesPopup();
    virtual ~EditAttributesPopup();

    // PURPOSE: Initializes the UI by calling UICreate(), UIConfigure() and other setup functions.
    // PARAMS:
    // RETURNS:
    // NOTES:
    void UIInit();

    // PURPOSE: Fills the UI from the given attributes list
    // PARAMS:
    //    attributeList - the attributes to edit
    void SetAttributes( const parAttributeList& attributeList );

    // PURPOSE: Returns the new attributes.  The original attributeList is not touched.
    // RETURNS: The new attributes
    const parAttributeList& GetAttributes() const;

protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: The y-value to start attacking controls to.  Derived classes should change this
    //  before calling the parent class's UICreate() function.
    int m_iAttachTopY;

	FBEditNumber m_triggerOffsetXNumericUpDown;
	FBEditNumber m_triggerOffsetYNumericUpDown;
	FBEditNumber m_triggerOffsetZNumericUpDown;

private:
    enum EEditMode
    {
        NO_MODE,
        ADD_MODE,
        EDIT_MODE
    };

    void EnableEditFields( bool bEnable, bool bAddAttribute );

    void ClearSpreadsheet();

    void AddRowToSpreadsheet( const parAttribute &attribute );

    EEditMode m_mode;
    int m_iSpreadsheetSelRowId;
    int m_iRowRef;
    parAttributeList m_attributeList;

    FBSpread m_spreadsheet;
    
    FBButton m_editButton;
    FBButton m_addButton;
    FBButton m_removeButton;
    FBButton m_removeAllButton;

    FBLabel m_nameLabel;
    FBEdit m_nameTextBox;

    FBLabel m_typeLabel;
    FBList m_typeComboBox;

    FBLabel m_valueLabel;
    FBEdit m_valueTextBox;

    FBButton m_doneEditButton;
    FBButton m_cancelEditButton;

	FBButton m_createTriggerAttributes;

    FBButton m_okButton;
    FBButton m_cancelButton;

    void Spreadsheet_RowClick( HISender pSender, HKEvent pEvent );

    void EditButton_Click( HISender pSender, HKEvent pEvent );
    void AddButton_Click( HISender pSender, HKEvent pEvent );
    void RemoveButton_Click( HISender pSender, HKEvent pEvent );
    void RemoveAllButton_Click( HISender pSender, HKEvent pEvent );

    void NameTextBox_Change( HISender pSender, HKEvent pEvent );
    void ValueTextBox_Change( HISender pSender, HKEvent pEvent );

    void DoneEditButton_Click( HISender pSender, HKEvent pEvent );
    void CancelEditButton_Click( HISender pSender, HKEvent pEvent );

    void HelpButton_Click( HISender pSender, HKEvent pEvent );
    void OkButton_Click( HISender pSender, HKEvent pEvent );
    void CancelButton_Click( HISender pSender, HKEvent pEvent );

	void CreateTriggerAttributesButton_Click( HISender pSender, HKEvent pEvent );

	void AddAttribute(const char* attrname, const int type, const char* val);
};

inline const parAttributeList& EditAttributesPopup::GetAttributes() const
{
    return m_attributeList;
}

//##############################################################################

class EditSceneAttributesPopup : public EditAttributesPopup
{
public:
    EditSceneAttributesPopup();
    virtual ~EditSceneAttributesPopup();

    Vector3 GetSceneOffset() const;
    void SetSceneOffset( const Vector3 &vOffset );

	Vector3 GetTriggerOffset() const;
	void SetTriggerOffset( const Vector3 &vOffset );

    float GetSceneRotation() const;
    void SetSceneRotation( float fDegrees );

    void SetSettings( const Vector3& vSceneOffset, const Vector3& vTriggerOffset, float fSceneRotation, const parAttributeList& attributeList );

    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

protected:

    FBLabel m_sceneOffsetLabel;
    FBEditNumber m_sceneOffsetXNumericUpDown;
    FBEditNumber m_sceneOffsetYNumericUpDown;
    FBEditNumber m_sceneOffsetZNumericUpDown;

	FBLabel m_triggerOffsetLabel;

    FBLabel m_sceneRotationLabel;
    FBEditNumber m_sceneRotationNumericUpDown;
    FBLabel m_sceneRotationUnitsLabel;
};

inline Vector3 EditSceneAttributesPopup::GetSceneOffset() const
{
    return Vector3( (float)m_sceneOffsetXNumericUpDown.Value, (float)m_sceneOffsetYNumericUpDown.Value, (float)m_sceneOffsetZNumericUpDown.Value );
}

inline void EditSceneAttributesPopup::SetSceneOffset( const Vector3& vOffset )
{
    m_sceneOffsetXNumericUpDown.Value = vOffset.GetX();
    m_sceneOffsetYNumericUpDown.Value = vOffset.GetY();
    m_sceneOffsetZNumericUpDown.Value = vOffset.GetZ();
}

inline Vector3 EditSceneAttributesPopup::GetTriggerOffset() const
{
	return Vector3( (float)m_triggerOffsetXNumericUpDown.Value, (float)m_triggerOffsetYNumericUpDown.Value, (float)m_triggerOffsetZNumericUpDown.Value );
}

inline void EditSceneAttributesPopup::SetTriggerOffset( const Vector3& vOffset )
{
	m_triggerOffsetXNumericUpDown.Value = vOffset.GetX();
	m_triggerOffsetYNumericUpDown.Value = vOffset.GetY();
	m_triggerOffsetZNumericUpDown.Value = vOffset.GetZ();
}

inline float EditSceneAttributesPopup::GetSceneRotation() const
{
    return (float)m_sceneRotationNumericUpDown.Value;
}

inline void EditSceneAttributesPopup::SetSceneRotation( float fRotation )
{
    m_sceneRotationNumericUpDown.Value = fRotation;
}

//##############################################################################

// PURPOSE: A base class for displaying and editing a cutfObject as a popup.
class EditObjectAttributesPopup : public EditAttributesPopup
{
public:
    EditObjectAttributesPopup();
    virtual ~EditObjectAttributesPopup();

    // PURPOSE: Fills the UI with the object's attributes
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    void SetObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Returns a clone of the original object, with the new attributes
    // RETURNS: A clone of the original object
    cutfObject* GetObject();

protected:
    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );

    // PURPOSE: The original object that we are editing.  The contents of this will not be modified
    const cutfObject *m_pObject;
};

//##############################################################################

class EditOverlayObjectAttributesPopup : public EditObjectAttributesPopup
{
public:
	EditOverlayObjectAttributesPopup();
	virtual ~EditOverlayObjectAttributesPopup();

	void SetOverlayTypes( const atArray<atString>& atEntries );

protected:
	// PURPOSE: Adds the regions to the popup
	// NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
	virtual void UICreate();

	// PURPOSE: Edits the labels, adds the event handlers, etc..
	virtual void UIConfigure();

	// PURPOSE: Fills the custom portion of this popup with the editable
	//    data and values from the object
	// PARAMS:
	//    pObject - the object to edit
	//    pCutFile - the cutfile that contains the object
	virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

	// PURPOSE: Stores the data and values from the custom portion of this popup
	//    back to the object
	// PARAMS:
	//    pObject - the object to edit
	virtual void SetObjectFromUI( cutfObject *pObject );

	FBLabel m_overlayTypeLabel;
	FBList m_overlayTypeComboBox;

	FBLabel m_renderTargetNameLabel;
	FBEdit m_renderTargetNameTextBox;

	FBLabel m_modelObjectNameLabel;
	FBEdit m_modelObjectNameTextbox;
};

//##############################################################################

class EditCameraObjectAttributesPopup : public EditObjectAttributesPopup
{
public:
    EditCameraObjectAttributesPopup();
    virtual ~EditCameraObjectAttributesPopup();

protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );

private:
    FBLabel m_nearDrawDistanceLabel;
    FBLabel m_nearDrawDistanceTextBox;

    FBLabel m_farDrawDistanceLabel;
    FBLabel m_farDrawDistanceTextBox;
};

//#############################################################################

class EditModelObjectAttributesPopup : public EditObjectAttributesPopup
{
	
public:
    EditModelObjectAttributesPopup();
    virtual ~EditModelObjectAttributesPopup();

	// PURPOSE: Sets the available handles for the model object.
	// PARAMS:
	//    atEntries - the handles available.
	void SetHandles( const atArray<HandleEntry>& atEntries );

protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );

	FBEdit m_FaceAttributesFilenameTextBox;

private:
    FBLabel m_modelNameLabel;
    FBLabel m_modelNameTextBox;

    FBLabel m_animExportCtrlSpecFileLabel;
    FBList m_animExportCtrlSpecFileComboBox;

	FBLabel m_faceExportCtrlSpecFileLabel;
	FBList m_faceExportCtrlSpecFileComboBox;
    
    FBLabel m_animCompressionFileLabel;
    FBList m_animCcompressionFileComboBox;

	FBLabel m_modelHandleFileLabel;
	FBList m_modelHandleFileComboBox;

	FBLabel m_FaceAttributesLabel;

	FBButton m_browseFaceAttributesButton;

	FBFilePopup m_openFilePopup;

	void BrowseFaceAttributesButton_Click( HISender pSender, HKEvent pEvent );
};

//##############################################################################

class EditPedModelObjectAttributesPopup : public EditModelObjectAttributesPopup
{
public:
    EditPedModelObjectAttributesPopup();
    virtual ~EditPedModelObjectAttributesPopup();

protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );



private:
    FBLabel m_defaultFaceAnimationLabel;
    FBLabel m_defaultFaceAnimationTextBox;
    FBButton m_foundDefaultFaceAnimationCheckBox;

    FBLabel m_overrideFaceAnimationLabel;
    FBButton m_overrideFaceAnimationCheckBox;
    FBEdit m_overrideFaceAnimationFilenameTextBox;
    FBButton m_browseFaceAnimationButton;

    FBFilePopup m_openFilePopup;

    void OverrideFaceAnimationCheckBox_CheckChanged( HISender pSender, HKEvent pEvent );
    void BrowseFaceAnimationButton_Click( HISender pSender, HKEvent pEvent );
};

//##############################################################################

class EditVehicleModelObjectAttributesPopup : public EditModelObjectAttributesPopup
{
public:
    EditVehicleModelObjectAttributesPopup();
    virtual ~EditVehicleModelObjectAttributesPopup();
    
protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );

private:
    FBLabel m_removeBonesLabel;
    FBList m_removeBonesList;
	FBLabel m_canApplyRealDamageLabel;
	FBButton m_canApplyRealDamageCheckbox;
};

//##############################################################################

class EditFindModelObjectAttributesPopup : public EditObjectAttributesPopup
{
public:
    EditFindModelObjectAttributesPopup();
    virtual ~EditFindModelObjectAttributesPopup();

protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );

private:
    FBLabel m_positionLabel;
    FBEditNumber m_positionXNumericUpDown;
    FBEditNumber m_positionYNumericUpDown;
    FBEditNumber m_positionZNumericUpDown;

    FBLabel m_radiusLabel;
    FBEditNumber m_radiusNumericUpDown;
};

//##############################################################################

class EditBlockingBoundsObjectAttributesPopup : public EditObjectAttributesPopup
{
public:
    EditBlockingBoundsObjectAttributesPopup();
    virtual ~EditBlockingBoundsObjectAttributesPopup();

protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );

private:
    FBLabel m_corner1Label;
    FBEditNumber m_corner1XNumericUpDown;
    FBEditNumber m_corner1YNumericUpDown;
    FBEditNumber m_corner1ZNumericUpDown;

    FBLabel m_corner2Label;
    FBEditNumber m_corner2XNumericUpDown;
    FBEditNumber m_corner2YNumericUpDown;
    FBEditNumber m_corner2ZNumericUpDown;

    FBLabel m_corner3Label;
    FBEditNumber m_corner3XNumericUpDown;
    FBEditNumber m_corner3YNumericUpDown;
    FBEditNumber m_corner3ZNumericUpDown;

    FBLabel m_corner4Label;
    FBEditNumber m_corner4XNumericUpDown;
    FBEditNumber m_corner4YNumericUpDown;
    FBEditNumber m_corner4ZNumericUpDown;

    FBLabel m_heightLabel;
    FBEditNumber m_heightNumericUpDown;
};

//##############################################################################

class EditLightObjectAttributesPopup : public EditObjectAttributesPopup
{
public:
    EditLightObjectAttributesPopup();
    virtual ~EditLightObjectAttributesPopup();

	// PURPOSE: Sets the available handles for the model object.
	// PARAMS:
	//    atEntries - the handles available.
	void SetProperties( const atArray<atString>& atEntries );

protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );

private:
    FBLabel m_lightTypeLabel;
    FBLabel m_lightType;

	FBLabel m_lightPropertyLabel;
	FBList m_lightPropertyFileComboBox;
};

//##############################################################################

class EditAnimatedParticleEffectObjectAttributesPopup : public EditObjectAttributesPopup
{
public:
    EditAnimatedParticleEffectObjectAttributesPopup();
    virtual ~EditAnimatedParticleEffectObjectAttributesPopup();

protected:
    // PURPOSE: Adds the regions to the popup
    // NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
    virtual void UICreate();

    // PURPOSE: Edits the labels, adds the event handlers, etc..
    virtual void UIConfigure();

    // PURPOSE: Fills the custom portion of this popup with the editable
    //    data and values from the object
    // PARAMS:
    //    pObject - the object to edit
    //    pCutFile - the cutfile that contains the object
    virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

    // PURPOSE: Stores the data and values from the custom portion of this popup
    //    back to the object
    // PARAMS:
    //    pObject - the object to edit
    virtual void SetObjectFromUI( cutfObject *pObject );

private:
    FBLabel m_modelNameLabel;
    FBLabel m_modelNameTextBox;
};

//##############################################################################

class EditWeaponModelObjectAttributesPopup : public EditModelObjectAttributesPopup
{
public:
	EditWeaponModelObjectAttributesPopup();
	virtual ~EditWeaponModelObjectAttributesPopup();

	// PURPOSE: Sets the available handles for the model object.
	// PARAMS:
	//    atEntries - the handles available.
	void SetProperties( const atArray<atString>& atEntries );

protected:
	// PURPOSE: Adds the regions to the popup
	// NOTES: In derivied classes, always call the base version of this function AFTER your custom controls.  See m_iAttachTopY.
	virtual void UICreate();

	// PURPOSE: Edits the labels, adds the event handlers, etc..
	virtual void UIConfigure();

	// PURPOSE: Fills the custom portion of this popup with the editable
	//    data and values from the object
	// PARAMS:
	//    pObject - the object to edit
	//    pCutFile - the cutfile that contains the object
	virtual void SetUIFromObject( const cutfObject *pObject, const cutfCutsceneFile2 *pCutFile );

	// PURPOSE: Stores the data and values from the custom portion of this popup
	//    back to the object
	// PARAMS:
	//    pObject - the object to edit
	virtual void SetObjectFromUI( cutfObject *pObject );



private:
	FBLabel m_weaponTypeLabel;
	FBList m_weaponTypeFileComboBox;
};

//##############################################################################

} // namespace rage

#endif // __REXMBRAGE_EDITPOPUP_H__ 
