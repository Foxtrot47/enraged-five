
#include "concattool.h"

#include <iostream>
#include <fstream>
#include <list>
#include "file/asset.h"
#include "file/device.h"
#include "rexMBAnimExportCommon/utility.h"
#include "system/exec.h"
#include "rexMBAnimExportCommon/Logger.h"
#include "rexMBAnimExportCommon/perforce.h"

#define	CONCATTOOL__CLASSNAME	RexRageConcatTool
#define CONCATTOOL__CLASSSTR	"RexRageConcatTool"
#define	CONCATTOOL__CLASS		CONCATTOOL__CLASSNAME
#define	CONCATTOOL__LABEL		"Rex Rage Cut-Scene Concat"
#define	CONCATTOOL__DESC		"Rex Rage Cut-Scene Concat Tool"

#pragma warning(push)
#pragma warning(disable:4100)

FBToolImplementation(	CONCATTOOL__CLASS	);
FBRegisterTool		(	CONCATTOOL__CLASS,
						CONCATTOOL__LABEL,
						CONCATTOOL__DESC,
						FB_DEFAULT_SDK_ICON		);

#pragma warning(pop)

using namespace std;

static const Version s_version = Version(1, 1, 2);

bool RexRageConcatTool::FBCreate()
{
	UICreate();
	UIConfigure(); 

	PopulateSectioningMethodNames();

	m_sectioningMethodComboBox.Items.Clear();
	for ( int i = 0; i < m_sectioningMethodNames.GetCount(); ++i )
	{
		m_sectioningMethodComboBox.Items.Add( const_cast<char *>( m_sectioningMethodNames[i].c_str() ) );
	}

	return true;
}

void RexRageConcatTool::FBDestroy()
{

}

atString RexRageConcatTool::GetBaseDir(const char* pName)
{
	char cRemovedName[RAGE_MAX_PATH];
	ASSET.RemoveNameFromPath(cRemovedName, RAGE_MAX_PATH, pName);

	atArray<atString> atSplitPath;
	atString(cRemovedName).Split(atSplitPath, "\\", true);

	return atSplitPath[atSplitPath.GetCount()-1];
}

void RexRageConcatTool::UICreate()
{
    StartSize[0] = 600;
    StartSize[1] = (20 * rexMBAnimExportCommon::c_iRowHeight) + (rexMBAnimExportCommon::c_iMargin * 2);

    MinSize[0] = 600;
    MinSize[1] = (20 * rexMBAnimExportCommon::c_iRowHeight) + (rexMBAnimExportCommon::c_iMargin * 2);

    MaxSize[0] = 600;
    MaxSize[1] = (20 * rexMBAnimExportCommon::c_iRowHeight) + (rexMBAnimExportCommon::c_iMargin * 2);

	int iAttachTopY = rexMBAnimExportCommon::c_iMargin;

	// Import Cut File
	AddRegion( "ImportCutFileButton", "ImportCutFileButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ImportCutFileButton", m_importCutListButton );

	AddRegion( "ImportConcatFileButton", "ImportConcatFileButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ImportCutFileButton", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ImportConcatFileButton", m_importConcatListButton );

	AddRegion( "ResetButton", "ResetButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ImportConcatFileButton", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ResetButton", m_resetButton );

	// Version
	AddRegion( "VersionLabel", "VersionLabel",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iLabelWidth*2, kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "VersionLabel", m_versionLabel );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// CutfileInfoLayout
	AddRegion( "CutfileInfoLayout", "CutfileInfoLayout", 
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
		iAttachTopY, kFBAttachTop, "NULL", 1.0f,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iRowHeight, kFBAttachBottom, NULL, 1.0f );    
	SetControl( "CutfileInfoLayout", m_cutfileInfoLayout );

	m_cutfileInfoLayout.AddRegion( "CutfileInfoLayoutRegion", "CutfileInfoLayoutRegion",
		rexMBAnimExportCommon::c_iMargin * 2, kFBAttachLeft, "NULL", 1.0f,
		rexMBAnimExportCommon::c_iMargin * 3, kFBAttachTop, "NULL", 1.0f,
		-rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, NULL, 1.0f,
		-rexMBAnimExportCommon::c_iMargin * 2, kFBAttachBottom, NULL, 1.0f );
	m_cutfileInfoLayout.SetControl( "CutfileInfoLayoutRegion", m_cutfileInfoLayoutRegion );

	m_cutfileInfoLayout.AddRegion( "Spreadsheet", "Spreadsheet",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		-350, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
		-100, kFBAttachBottom, "CutfileInfoLayoutRegion", 1.0 );
	m_cutfileInfoLayout.SetControl( "Spreadsheet", m_listSource );

	m_cutfileInfoLayout.AddRegion( "Spreadsheet2", "Spreadsheet2",
		(rexMBAnimExportCommon::c_iMargin*2) + rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachRight, "Spreadsheet", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		- rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
		-100, kFBAttachBottom, "CutfileInfoLayoutRegion", 1.0 );
	m_cutfileInfoLayout.SetControl( "Spreadsheet2", m_listDest );

	m_cutfileInfoLayout.AddRegion( "AddButton", "AddButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "Spreadsheet", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoLayout.SetControl( "AddButton", m_addButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	m_cutfileInfoLayout.AddRegion( "RemoveButton", "RemoveButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "Spreadsheet", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoLayout.SetControl( "RemoveButton", m_removeButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	m_cutfileInfoLayout.AddRegion( "RemoveAllButton", "RemoveAllButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "Spreadsheet", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoLayout.SetControl( "RemoveAllButton", m_removeAllButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	m_cutfileInfoLayout.AddRegion( "MoveUpButton", "MoveUpButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "Spreadsheet", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoLayout.SetControl( "MoveUpButton", m_moveUpButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	m_cutfileInfoLayout.AddRegion( "MoveDownButton", "MoveDownButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "Spreadsheet", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoLayout.SetControl( "MoveDownButton", m_moveDownButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight*9;

	AddRegion( "OptionsLayout", "OptionsLayout", 
		rexMBAnimExportCommon::c_iMargin*3, kFBAttachLeft, "CutfileInfoLayoutRegion", 1.0f,
		-130, kFBAttachBottom, "CutfileInfoLayoutRegion", 1.0f,
		- rexMBAnimExportCommon::c_iMargin*3, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0f,
		-rexMBAnimExportCommon::c_iMargin*7, kFBAttachBottom, "CutfileInfoLayoutRegion", 1.0f );    
	SetControl( "OptionsLayout", m_optionsLayout );

	m_optionsLayout.AddRegion( "OptionsLayoutRegion", "OptionsLayoutRegion",
		rexMBAnimExportCommon::c_iMargin * 2, kFBAttachLeft, "NULL", 1.0f,
		rexMBAnimExportCommon::c_iMargin * 3, kFBAttachTop, "NULL", 1.0f,
		-rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, NULL, 1.0f,
		-rexMBAnimExportCommon::c_iMargin * 2, kFBAttachBottom, NULL, 1.0f );
	m_optionsLayout.SetControl( "OptionsLayoutRegion", m_optionsLayoutRegion );

	m_optionsLayout.AddRegion( "AudioLabel", "AudioLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iMargin*2, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iLabelWidth*2, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "AudioLabel", m_audioLabel );

	m_optionsLayout.AddRegion( "AudioTextBox", "AudioTextBox",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "AudioLabel", 1.0,
		rexMBAnimExportCommon::c_iMargin*2, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		- (rexMBAnimExportCommon::c_iButtonWidth/2), kFBAttachRight, "AudioBrowseButton", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "AudioTextBox", m_audioTextBox );

	m_optionsLayout.AddRegion( "AudioBrowseButton", "AudioBrowseButton",
		-rexMBAnimExportCommon::c_iButtonWidth/2, kFBAttachRight, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iMargin*2, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "AudioBrowseButton", m_audioBrowseButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	m_optionsLayout.AddRegion( "OutputLabel", "OutputLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iMargin*2 + rexMBAnimExportCommon::c_iRowHeight, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iLabelWidth*2, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "OutputLabel", m_outputLabel );

	m_optionsLayout.AddRegion( "OutputTextBox", "OutputTextBox",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "OutputLabel", 1.0,
		rexMBAnimExportCommon::c_iMargin*2 + rexMBAnimExportCommon::c_iRowHeight, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		- (rexMBAnimExportCommon::c_iButtonWidth/2), kFBAttachRight, "OutputBrowseButton", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "OutputTextBox", m_outputTextBox );

	m_optionsLayout.AddRegion( "OutputBrowseButton", "OutputBrowseButton",
		-rexMBAnimExportCommon::c_iButtonWidth/2, kFBAttachRight, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iMargin*2 + rexMBAnimExportCommon::c_iRowHeight, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "OutputBrowseButton", m_outputBrowseButton );

	//
	// Sectioning
	m_optionsLayout.AddRegion( "SectioningMethodLabel", "SectioningMethodLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iMargin*2 + rexMBAnimExportCommon::c_iRowHeight*2, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iLabelWidth*2, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "SectioningMethodLabel", m_sectioningMethodLabel );

	m_optionsLayout.AddRegion( "SectioningMethodList", "SectioningMethodList",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SectioningMethodLabel", 1.0,
		rexMBAnimExportCommon::c_iMargin*2 + rexMBAnimExportCommon::c_iRowHeight*2, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		250, kFBAttachNone, "NULL", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "SectioningMethodList", m_sectioningMethodComboBox );

	m_optionsLayout.AddRegion( "SectionDurationNumericUpDown", "SectionDurationNumericUpDown",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SectioningMethodList", 1.0,
		rexMBAnimExportCommon::c_iMargin*2 + rexMBAnimExportCommon::c_iRowHeight*2, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		50, kFBAttachNone, "NULL", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "SectionDurationNumericUpDown", m_sectionDurationNumericUpDown );

	m_optionsLayout.AddRegion( "SectionDurationLabel", "SectionDurationLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SectionDurationNumericUpDown",	1.0,
		rexMBAnimExportCommon::c_iMargin*2 + rexMBAnimExportCommon::c_iRowHeight*2, kFBAttachTop, "OptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_optionsLayout.SetControl( "SectionDurationLabel", m_sectionDurationLabel );

	//

	AddRegion( "SaveButton", "SaveButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iRowHeight, kFBAttachBottom, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0 );
	SetControl( "SaveButton", m_saveButton );

#if !CUTSCENE_ASSET_PIPELINE_3
	AddRegion( "ConcatButton", "ConcatButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SaveButton", 1.0,
		-rexMBAnimExportCommon::c_iRowHeight, kFBAttachBottom, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0 );
	SetControl( "ConcatButton", m_concatButton );
#endif

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

}

void RexRageConcatTool::AddButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	for(int i=0; i < m_listSource.Items.GetCount(); ++i)
	{
		if(m_listSource.IsSelected(i))
		{
			RexRageCutscenePart **ppObject = m_spreadsheetRowRefToPartMap.Access( i );
			if ( ppObject != NULL )
			{
				AddRowToDestinationList(*ppObject);

				EnableButtons();
			}
		}
	}
}

void RexRageConcatTool::RemoveButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	for(int i=0; i < m_listDest.Items.GetCount(); ++i)
	{
		if(m_listDest.IsSelected(i))
		{
			m_spreadsheetRowRefToPartMapDest.Delete( i );

			m_listDest.Items.RemoveAt(i);
		}
	}

	// Re-add as lists dont match the map
	atMap<int, RexRageCutscenePart *> spreadsheetCopy = m_spreadsheetRowRefToPartMapDest;
	ClearDestList();

	atMap<int, RexRageCutscenePart *>::Iterator entry = spreadsheetCopy.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		if(entry.GetData() != NULL)
		{
			AddRowToDestinationList(entry.GetData());
		}
	}

	EnableButtons();
}

void RexRageConcatTool::RemoveAllButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	ClearDestList();
}

void RexRageConcatTool::MoveUpButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	for(int i=0; i < m_listDest.Items.GetCount(); ++i)
	{
		if(m_listDest.IsSelected(i))
		{
			if(i == 0) continue;

			atString temp((char*)m_listDest.Items[i-1]);
			m_listDest.Items.SetAt(i-1, (char*)m_listDest.Items[i]);
			m_listDest.Items.SetAt(i, (char*)temp.c_str());

			RexRageCutscenePart **ppObject = m_spreadsheetRowRefToPartMapDest.Access( i );
			RexRageCutscenePart **ppObject2 = m_spreadsheetRowRefToPartMapDest.Access( i-1 );
			RexRageCutscenePart ppObjectTemp = **ppObject2;
			**ppObject2 = **ppObject;
			**ppObject = ppObjectTemp;
		}
	}

	DeSelectAll();
}

void RexRageConcatTool::MoveDownButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	for(int i=0; i < m_listDest.Items.GetCount(); ++i)
	{
		if(m_listDest.IsSelected(i))
		{
			if(i == m_listDest.Items.GetCount()-1) continue;

			atString temp((char*)m_listDest.Items[i+1]);
			m_listDest.Items.SetAt(i+1, (char*)m_listDest.Items[i]);
			m_listDest.Items.SetAt(i, (char*)temp.c_str());

			RexRageCutscenePart **ppObject = m_spreadsheetRowRefToPartMapDest.Access( i );
			RexRageCutscenePart **ppObject2 = m_spreadsheetRowRefToPartMapDest.Access( i+1 );
			RexRageCutscenePart ppObjectTemp = **ppObject2;
			**ppObject2 = **ppObject;
			**ppObject = ppObjectTemp;
		}
	}

	DeSelectAll();
}

void RexRageConcatTool::OutputPathButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_openFolderPopup.Caption = "Browse Scene";

	atString atAssetPath("");
	rexMBAnimExportCommon::GetAssetPath(atAssetPath);

	char cCutScenePath[RAGE_MAX_PATH * 2];
	sprintf( cCutScenePath, "%s\\cuts", atAssetPath.c_str() );

	m_openFolderPopup.Path = cCutScenePath;

	if ( m_openFolderPopup.Execute() )
	{
		char cFullFilename[RAGE_MAX_PATH];
		safecpy( cFullFilename, (const char*)m_openFolderPopup.Path, sizeof(cFullFilename) );
		m_outputTextBox.Text = cFullFilename;

		EnableButtons();
	}
}

void RexRageConcatTool::DeSelectAll()
{
	for(int i=0; i < m_listDest.Items.GetCount(); ++i)
	{
		m_listDest.Selected(i, false);
	}
}

bool RexRageConcatTool::Save(const char* pFile)
{
	RexRageCutscenePartFile f;

	atMap<s32, RexRageCutscenePart *>::Iterator entry = m_spreadsheetRowRefToPartMapDest.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		f.AddPart(entry.GetData()->Clone());
	}

	f.SetAudio(m_audioTextBox.Text);
	f.SetFolder(m_outputTextBox.Text);

	f.SetSectionMethodDuration(m_sectionDurationNumericUpDown.Value);
	f.SetSectionMethodIndex(m_sectioningMethodComboBox.ItemIndex);

	perforce::Checkout(m_openFilePopup.FullFilename, true);
	perforce::ExecuteQueuedOperations(true);

	return f.SaveFile(pFile);
}

void RexRageConcatTool::SaveButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_openFilePopup.Style = kFBFilePopupSave;
	m_openFilePopup.Filter = "*.concatlist";

	char cValue[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable( "RS_PROJROOT", cValue, sizeof(cValue) );

	char cCutScenePath[RAGE_MAX_PATH * 4];
	cCutScenePath[0] = 0;
	safecat( cCutScenePath, cValue, sizeof(cCutScenePath) );
#if !HACK_MP3
	safecat( cCutScenePath, "\\assets\\cuts\\!!Cutlists", sizeof(cCutScenePath) );
#else
	safecat( cCutScenePath, "\\payne_art\\anim\\exportcutscene", sizeof(cCutScenePath) );
#endif

	m_openFilePopup.Path = cCutScenePath;

	if(m_openFilePopup.Execute())
	{
		Save(m_openFilePopup.FullFilename);
	}
}

void RexRageConcatTool::ImportCutPartButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_openFilePopup.Caption = "Import Cut Part";
	m_openFilePopup.Filter = "*.cutpart";
	m_openFilePopup.Style = kFBFilePopupOpen;

	atString atAssetPath("");
	rexMBAnimExportCommon::GetAssetPath(atAssetPath);

	char cExportPath[RAGE_MAX_PATH];
	sprintf_s(cExportPath, RAGE_MAX_PATH, "%s\\cuts", atAssetPath.c_str());

	m_openFilePopup.Path = const_cast<char *>( cExportPath );

	if ( m_openFilePopup.Execute() )
	{
		ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/log", NULL),"rexmbrage");

		char cFullPath[RAGE_MAX_PATH];
		safecpy( cFullPath, (const char*)m_openFilePopup.FullFilename, sizeof(cFullPath) );

		RexRageCutscenePartFile p;
		bool bResult = p.LoadFile(cFullPath);
		if(!bResult)
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Failed to load '%s'", cFullPath);
		}

		for(int i=0; i < p.GetNumParts(); ++i)
		{
			AddRowToSourceList(p.GetPart(i)->Clone());
		}

		ULOGGER_MOBO.PostExport(true);
	}
} 

void RexRageConcatTool::ImportConcatListButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_openFilePopup.Caption = "Import Concat List";
	m_openFilePopup.Filter = "*.concatlist";
	m_openFilePopup.Style = kFBFilePopupOpen;

	atString atAssetPath("");
	rexMBAnimExportCommon::GetAssetPath(atAssetPath);

	char cExportPath[RAGE_MAX_PATH];
	sprintf_s(cExportPath, RAGE_MAX_PATH, "%s\\cuts\\!!cutlists", atAssetPath.c_str());

	m_openFilePopup.Path = const_cast<char *>( cExportPath );

	if ( m_openFilePopup.Execute() )
	{
		ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/log", NULL),"rexmbrage");

		char cFullPath[RAGE_MAX_PATH];
		safecpy( cFullPath, (const char*)m_openFilePopup.FullFilename, sizeof(cFullPath) );

		RexRageCutscenePartFile p;
		p.LoadFile(cFullPath, false);

		ClearDestList();

		for(int i=0; i < p.GetNumParts(); ++i)
		{
			AddRowToDestinationList(p.GetPart(i)->Clone());
		}

		m_audioTextBox.Text = (char*)p.GetAudio();
		m_outputTextBox.Text = (char*)p.GetFolder();
		m_sectioningMethodComboBox.ItemIndex = p.GetSectionMethodIndex();
		m_sectionDurationNumericUpDown.Value = p.GetSectionMethodDuration();

		EnableButtons();

		ULOGGER_MOBO.PostExport(true);
	}
} 

void RexRageConcatTool::ResetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_audioTextBox.Text = "";
	m_outputTextBox.Text = "";
	ClearDestList();
	ClearSourceList();
}

void RexRageConcatTool::UIConfigure()
{
	// Version
	char cBuffer[32];
	sprintf(cBuffer,"Version: %i.%i.%i", s_version.Major, s_version.Minor, s_version.Revision);
	m_versionLabel.Caption = cBuffer;
	m_versionLabel.Justify = kFBTextJustifyRight;

	m_cutfileInfoLayout.SetRegionTitle( "CutfileInfoLayoutRegion", "Concat Information" );
	m_cutfileInfoLayout.SetBorder( "CutfileInfoLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

	m_optionsLayout.SetRegionTitle( "OptionsLayoutRegion", "Settings" );
	m_optionsLayout.SetBorder( "OptionsLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

	m_importCutListButton.Caption = "Import Cut Part";
	m_importCutListButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::ImportCutPartButton_Click );

	m_importConcatListButton.Caption = "Import Concat List";
	m_importConcatListButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::ImportConcatListButton_Click );

	m_resetButton.Caption = "Reset";
	m_resetButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::ResetButton_Click );

	m_addButton.Caption = "Add";
	m_addButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::AddButton_Click );

	m_removeButton.Caption = "Remove";
	m_removeButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::RemoveButton_Click );

	m_removeAllButton.Caption = "Remove All";
	m_removeAllButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::RemoveAllButton_Click );

	m_moveUpButton.Caption = "Move Up";
	m_moveUpButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::MoveUpButton_Click );
	m_moveDownButton.Caption = "Move Down";
	m_moveDownButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::MoveDownButton_Click );

	m_audioLabel.Caption = "Audio Name:";
	m_audioLabel.Justify = kFBTextJustifyRight;

	m_outputLabel.Caption = "Output Path:";
	m_outputLabel.Justify = kFBTextJustifyRight;

	m_saveButton.Caption = "Save Concat List";
	m_saveButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::SaveButton_Click );

	m_outputBrowseButton.Caption = "...";
	m_outputBrowseButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::OutputPathButton_Click );

	m_audioBrowseButton.Caption = "...";
	m_audioBrowseButton.OnClick.Add( this, (FBCallback)&RexRageConcatTool::AudioBrowseButton_Click );

	m_listSource.Style = kFBVerticalList;
	m_listSource.MultiSelect = true;
	m_listDest.Style = kFBVerticalList;

	// Sectioning
	m_sectioningMethodLabel.Caption = "Split Animations Into Sections:";
	m_sectioningMethodLabel.Justify = kFBTextJustifyRight;

	m_sectioningMethodComboBox.Style = kFBDropDownList;
	m_sectioningMethodComboBox.ExtendedSelect = true;
	m_sectioningMethodComboBox.OnChange.Add( this, (FBCallback)&RexRageConcatTool::SectioningMethodComboBox_SelectionChanged );

	m_sectionDurationNumericUpDown.Min = 0;
	m_sectionDurationNumericUpDown.Precision = 0.0;
	m_sectionDurationNumericUpDown.Value = 8.0f;
	m_sectionDurationNumericUpDown.OnChange.Add( this, (FBCallback)&RexRageConcatTool::ValidateSectionTime_Click );

	m_sectionDurationLabel.Caption = "seconds each";

	ClearSourceList();
	ClearDestList();
}

void RexRageConcatTool::PopulateSectioningMethodNames()
{
	m_sectioningMethodNames.Reserve( NUM_SECTIONING_METHODS );
	m_sectioningMethodNames.Append() = "(none)";
	m_sectioningMethodNames.Append() = "by Time Slices of";
	m_sectioningMethodNames.Append() = "by Camera Cuts";
	m_sectioningMethodNames.Append() = "by Child Sectioning"; // Use the childs sectioning
}

void RexRageConcatTool::SectioningMethodComboBox_SelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	SetSectioningMethod( m_sectioningMethodComboBox.ItemIndex );
}

void RexRageConcatTool::ValidateSectionTime_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_sectionDurationNumericUpDown.Value < MINIMUM_CUTSCENE_SECTION_TIME)
	{
		char msg[RAGE_MAX_PATH];
		sprintf( msg, "Section time %f is less than the minimum section time limit %d.", (float)m_sectionDurationNumericUpDown.Value, MINIMUM_CUTSCENE_SECTION_TIME);

		FBMessageBox( "Error", msg, "OK" );
		m_sectionDurationNumericUpDown.Value = MINIMUM_CUTSCENE_SECTION_TIME;
	}
}

void RexRageConcatTool::SetSectioningMethod( s32 method )
{
	if ( (method >= 0) && (method < m_sectioningMethodComboBox.Items.GetCount()) )
	{
		m_sectioningMethodComboBox.ItemIndex = method;

		m_sectionDurationNumericUpDown.Enabled = (method == TIME_SLICE_SECTIONING_METHOD);
		m_sectionDurationLabel.Enabled = method == TIME_SLICE_SECTIONING_METHOD;
	}
	else
	{
		m_sectionDurationNumericUpDown.Enabled = false;
		m_sectionDurationLabel.Enabled = false;
	}
}

void RexRageConcatTool::AudioBrowseButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_openFilePopup.Style = kFBFilePopupOpen;
	m_openFilePopup.Filter = "*.wav";

	atString atArtPath("");
	rexMBAnimExportCommon::GetArtPath(atArtPath);

	char cCutScenePath[RAGE_MAX_PATH * 2];
	sprintf( cCutScenePath, "%s\\animation\resources\\audio\\cutscene", atArtPath.c_str() );

	m_openFilePopup.Path = cCutScenePath;

	if ( m_openFilePopup.Execute() )
	{
		char cFullFilename[RAGE_MAX_PATH];
		safecpy( cFullFilename, (const char*)m_openFilePopup.FileName, sizeof(cFullFilename) );
		m_audioTextBox.Text = cFullFilename;

		EnableButtons();
	}
}

s32 RexRageConcatTool::AddRowToSourceList( RexRageCutscenePart* pPart )
{
	char cEntry[RAGE_MAX_PATH];
	sprintf_s( cEntry, RAGE_MAX_PATH, "%s - %s", GetBaseDir((char*)pPart->GetCutsceneFileName()).c_str() ,(char*)pPart->GetName() );
	m_listSource.Items.Add( cEntry );

	m_spreadsheetRowRefToPartMap.Insert( m_iSpreadsheetSourceRowId, pPart );

	++m_iSpreadsheetSourceRowId;

	return -1;
}

void RexRageConcatTool::GetMergeSceneNames(atArray<atString>& atMergeNames)
{
	atString atAssetPath("");
	rexMBAnimExportCommon::GetAssetPath(atAssetPath);

	char cMergeFolderPath[RAGE_MAX_PATH * 2];
	sprintf( cMergeFolderPath, "%s\\cuts\\!!mergelists", atAssetPath.c_str() );

	atArray<atString> mergeFiles;
	ASSET.EnumFiles( cMergeFolderPath, &CB_FindFirstFileAll, &mergeFiles );

	for(int i=0; i < mergeFiles.GetCount(); ++i)
	{
		parTree* pTree = PARSER.LoadTree( mergeFiles[i].c_str(), "" );
		if ( !pTree )
		{
			continue;
		}

		parTreeNode* pRootNode = pTree->GetRoot();

		parTreeNode* pPedsNode = pRootNode->FindChildWithName("path");
		char* pData = pPedsNode->GetData();

		atMergeNames.PushAndGrow(GetBaseDir(pData));
	}
}

s32 RexRageConcatTool::AddRowToDestinationList( RexRageCutscenePart* pPart )
{
	char cEntry[RAGE_MAX_PATH];
	sprintf_s( cEntry, RAGE_MAX_PATH, "%s - %s", GetBaseDir((char*)pPart->GetCutsceneFileName()).c_str() ,(char*)pPart->GetName() );
	m_listDest.Items.Add( cEntry );

	m_spreadsheetRowRefToPartMapDest.Insert( m_iSpreadsheetDestRowId, pPart );

	++m_iSpreadsheetDestRowId;

	return -1;
}

void RexRageConcatTool::ClearSourceList()
{
	m_spreadsheetRowRefToPartMap.Kill();   

	m_listSource.Items.Clear();

	m_iSpreadsheetSelSourceRowId = -1;
	m_iSpreadsheetSourceRowId = 0;
}

void RexRageConcatTool::ClearDestList()
{
	m_spreadsheetRowRefToPartMapDest.Kill();  

	m_listDest.Items.Clear();

	m_iSpreadsheetDestSelRowId = -1;
	m_iSpreadsheetDestRowId = 0;

	EnableButtons();
}

void RexRageConcatTool::EnableButtons()
{
	if(m_listDest.Items.GetCount() && (stricmp(m_outputTextBox.Text, "") != 0))
	{
		m_saveButton.Enabled = true;
	}
	else
	{
		m_saveButton.Enabled = false;
	}
}