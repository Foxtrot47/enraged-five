#pragma once
#include <fbsdk/fbsdk.h>

static void FBMatrixToRageMatrix44(FBMatrix& in, Matrix44& out)
{
	out.a.Set( (float) in(0,0), (float) in(0,1), (float) in(0,2), (float) in(0,3) );
	out.b.Set( (float) in(1,0), (float) in(1,1), (float) in(1,2), (float) in(1,3) );
	out.c.Set( (float) in(2,0), (float) in(2,1), (float) in(2,2), (float) in(2,3) );
	out.d.Set( (float) in(3,0), (float) in(3,1), (float) in(3,2), (float) in(3,3) );
}

static void FBMatrixToRageMatrix34(FBMatrix& in, Matrix34& out)
{
	out.a.Set( (float) in(0,0), (float) in(0,1), (float) in(0,2) );
	out.b.Set( (float) in(1,0), (float) in(1,1), (float) in(1,2) );
	out.c.Set( (float) in(2,0), (float) in(2,1), (float) in(2,2) );
	out.d.Set( (float) in(3,0), (float) in(3,1), (float) in(3,2) );
}

static void RageMatrix34ToFBMatrix(Matrix34& in, FBMatrix& out)
{
	out(0,0) = in.a.x;	out(0,1) = in.a.y;	out(0,2) = in.a.z;
	out(1,0) = in.b.x;	out(1,1) = in.b.y;	out(1,2) = in.b.z;
	out(2,0) = in.c.x;	out(2,1) = in.c.y;	out(2,2) = in.c.z;
	out(3,0) = in.d.x;	out(3,1) = in.d.y;	out(3,2) = in.d.z;
	/*out.a.Set( (float) in(0,0), (float) in(0,1), (float) in(0,2) );
	out.b.Set( (float) in(1,0), (float) in(1,1), (float) in(1,2) );
	out.c.Set( (float) in(2,0), (float) in(2,1), (float) in(2,2) );
	out.d.Set( (float) in(3,0), (float) in(3,1), (float) in(3,2) );*/
}