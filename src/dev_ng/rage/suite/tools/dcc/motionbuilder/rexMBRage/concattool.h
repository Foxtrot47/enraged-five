#ifndef __REXMBRAGE_CONCATTOOL_H__
#define __REXMBRAGE_CONCATTOOL_H__

// SDK include
#include <list>
#include "rexMBAnimExportCommon/utility.h"
#include "rexMBAnimExportCommon/partFile.h"

class RexRageConcatTool : public FBTool
{
	enum ESpreadsheetColumn
	{
		BLANK=-1,
		SCENE
	};

	// Tool declaration
	FBToolDeclare( RexRageConcatTool, FBTool );

public:
	virtual bool FBCreate();		// Constructor
	virtual void FBDestroy();		// Destructor

private:
	void UICreate();
	void UIConfigure();

	void ImportCutPartButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void ImportConcatListButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void AddButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void RemoveButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void RemoveAllButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void SaveButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void MoveUpButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void MoveDownButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void OutputPathButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void AudioBrowseButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void ResetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void SectioningMethodComboBox_SelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void ValidateSectionTime_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	s32 AddRowToSourceList( RexRageCutscenePart* pPart );
	s32 AddRowToDestinationList( RexRageCutscenePart* pPart );
	void ClearSourceList();
	void ClearDestList();
	void DeSelectAll();
	atString GetBaseDir(const char* pName);
	bool Save(const char* pFile);
	void EnableButtons();
	void GetMergeSceneNames(atArray<atString>& atMergeNames);

	void PopulateSectioningMethodNames();
	void SetSectioningMethod( s32 method );

	int m_iSpreadsheetSourceRowId;
	int m_iSpreadsheetSelSourceRowId;

	int m_iSpreadsheetDestRowId;
	int m_iSpreadsheetDestSelRowId;

	atMap<int, RexRageCutscenePart *> m_spreadsheetRowRefToPartMap;
	atMap<int, RexRageCutscenePart *> m_spreadsheetRowRefToPartMapDest;

	FBLayout m_cutfileInfoLayout;
	FBLayoutRegion m_cutfileInfoLayoutRegion;
	FBLayout m_optionsLayout;
	FBLayoutRegion m_optionsLayoutRegion;

	FBButton m_importCutListButton;
	FBButton m_importConcatListButton;
	FBButton m_resetButton;
	FBLabel m_versionLabel;
	FBButton m_addButton;
	FBButton m_saveButton;
	FBLabel m_audioLabel;
	FBEdit m_audioTextBox;
	FBButton m_removeButton;
	FBButton m_removeAllButton;
	FBLabel m_outputLabel;
	FBEdit m_outputTextBox;
	FBButton m_outputBrowseButton;
	FBButton m_audioBrowseButton;
	FBButton m_moveUpButton;
	FBButton m_moveDownButton;

	FBLabel m_sectioningMethodLabel;
	FBList m_sectioningMethodComboBox;
	FBEditNumber m_sectionDurationNumericUpDown;
	FBLabel m_sectionDurationLabel; 

	atArray<ConstString> m_sectioningMethodNames;

	FBFilePopup m_openFilePopup;
	FBFolderPopup m_openFolderPopup;

	FBList m_listSource;
	FBList m_listDest;
};

#endif // __REXMBRAGE_EFFECTSTOOL_H__
