// /animExportToolCmgr.cpp

#include "animExportToolCmgr.h"

//--- Registration defines
#define ANIMEXPORTCUSTOMMGR__CLASS ANIMEXPORTCUSTOMMGR__CLASSNAME
#define ANIMEXPORTCUSTOMMGR__NAME  ANIMEXPORTCUSTOMMGR__CLASSSTR

//--- FiLMBOX implementation and registration
FBCustomManagerImplementation( ANIMEXPORTCUSTOMMGR__CLASS  );  // Manager class name.
FBRegisterCustomManager( ANIMEXPORTCUSTOMMGR__CLASS );         // Manager class name.

//-----------------------------------------------------------------------------

bool RexRageAnimExportCustomMgr::FBCreate()
{
	return true;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportCustomMgr::FBDestroy()
{
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportCustomMgr::Init()
{
	return true;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportCustomMgr::Open()
{
	return true;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportCustomMgr::Clear()
{
	return true;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportCustomMgr::Close()
{
	return true;
}

//-----------------------------------------------------------------------------
