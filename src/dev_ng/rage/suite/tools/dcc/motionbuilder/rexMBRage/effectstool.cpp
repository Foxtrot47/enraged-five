
#include "effectstool.h"

#include <iostream>
#include <fstream>
#include <list>
#include "file/asset.h"
#include "file/device.h"
#include "rexMBAnimExportCommon/utility.h"
#include "system/exec.h"

#include "configParser/configGameView.h"
#include "configParser/configExportView.h"
using namespace configParser;

#define	EFFECTSTOOL__CLASSNAME	RexRageEffectsTool
#define EFFECTSTOOL__CLASSSTR	"RexRageEffectsTool"
#define	EFFECTSTOOL__CLASS		EFFECTSTOOL__CLASSNAME
#define	EFFECTSTOOL__LABEL		"Rex Rage Effects"
#define	EFFECTSTOOL__DESC		"Rex Rage Effects Tool"

#pragma warning(push)
#pragma warning(disable:4100)

FBToolImplementation(	EFFECTSTOOL__CLASS	);
FBRegisterTool		(	EFFECTSTOOL__CLASS,
						EFFECTSTOOL__LABEL,
						EFFECTSTOOL__DESC,
						FB_DEFAULT_SDK_ICON		);

#pragma warning(pop)

using namespace std;

static float DEFAULT_NEAR_DEPTH_OF_FIELD = 0.5f;
static float DEFAULT_FAR_DEPTH_OF_FIELD = 15.0f;

void FindFxListFilesCallback(const fiFindData &data, void *userArg)
{
    atArray<atString>& filenames = *((atArray<atString>*)userArg);

    const char *pExtension = ASSET.FindExtensionInPath( data.m_Name );
    if ( pExtension == NULL )
    {
        return;
    }

    ++pExtension;

    if ( stricmp( pExtension, "fxlist" ) == 0 )
    {
        filenames.PushAndGrow( atString(data.m_Name) );
    }
}

bool RexRageEffectsTool::FBCreate()
{
	UICreate();
	UIConfigure();    

    RefreshEffectsButton_Click( NULL, NULL );

	RefreshDecalList();

	return true;
}

void RexRageEffectsTool::FBDestroy()
{

}

void RexRageEffectsTool::UICreate()
{
     /* Here's a text-based representation of the Effects Window.
    {} = combo boxes (surrounds the name)
    () = button (surrounds the name)
	[] = checkbox
    _ = text box (spans the width of the text)
    <--> slider
                                                   ( Help )
    Rmptfx Path:						  (  [] Root Path )
        Fx List: ________________________ (    Refresh    )
        Effects: {                      } ( Create Effect )

    ( Create DOF Objects )

    Global Light Intensity: <-------------> ____ ( + )( - )
    */

    int iWideLabelWidth = 105;

    StartSize[0] = 500;
    StartSize[1] = (10 * rexMBAnimExportCommon::c_iRowHeight) + (rexMBAnimExportCommon::c_iMargin * 2);

    MinSize[0] = 500;
    MinSize[1] = (10 * rexMBAnimExportCommon::c_iRowHeight) + (rexMBAnimExportCommon::c_iMargin * 2);

    MaxSize[0] = 500;
    MaxSize[1] = (10 * rexMBAnimExportCommon::c_iRowHeight) + (rexMBAnimExportCommon::c_iMargin * 2);

    int iAttachTopY = rexMBAnimExportCommon::c_iMargin;

	// Rmptfx Path
	AddRegion( "scriptDatLabel", "scriptDatLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, "", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "scriptDatLabel", m_scriptDatLabel );

	AddRegion( "scriptDatPathLabel", "scriptDatPathLabel",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "scriptDatPathLabel", m_scriptDatPathLabel );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Rmptfx Path
    AddRegion( "RmptfxLabel", "RmptfxLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "RmptfxLabel", m_rmptfxLabel );

    AddRegion( "RmptfxPathLabel", "RmptfxPathLabel",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "RmptfxPathLabel", m_rmptfxPathLabel );

	AddRegion( "ToggleRootCheckbox", "ToggleRootCheckbox",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iCheckBoxWidth, kFBAttachRight, "", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "ToggleRootCheckbox", m_toggleRootCheckbox );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Fx File
    AddRegion( "FxListLabel", "FxListLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "FxListLabel", m_fxListLabel );

    AddRegion( "FxListFileComboBox", "FxListFileComboBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "FxListFileComboBox", m_fxListFileComboBox );

    AddRegion( "RefreshEffectsButton", "RefreshEffectsButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "RefreshEffectsButton", m_refreshEffectsButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Effect Names
    AddRegion( "EffectNamesLabel", "EffectNamesLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "EffectNamesLabel", m_effectNamesLabel );

    AddRegion( "EffectNamesComboBox", "EffectNamesComboBox",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "EffectNamesComboBox", m_effectNamesComboBox );

    AddRegion( "CreateEffectButton", "CreateEffectButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "CreateEffectButton", m_createEffectButton );
    
    iAttachTopY += (rexMBAnimExportCommon::c_iRowHeight * 2);

	// Effect Names
	AddRegion( "DecalNamesLabel", "DecalNamesLabel",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, "", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "DecalNamesLabel", m_decalNamesLabel );

	AddRegion( "DecalNamesComboBox", "DecalNamesComboBox",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "DecalNamesComboBox", m_decalNamesComboBox );

	AddRegion( "RefreshDecalButton", "RefreshDecalButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "RefreshDecalButton", m_refreshDecalButton );

	AddRegion( "CreateDecalButton", "CreateDecalButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "", 1.0,
		iAttachTopY, kFBAttachTop, "", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "CreateDecalButton", m_createDecalButton );

	iAttachTopY += (rexMBAnimExportCommon::c_iRowHeight * 2);

    int iCreateButtonWidth = 250 - (rexMBAnimExportCommon::c_iMargin * 2);

    // Create DOF Objects
	AddRegion( "CreateDOFObjectsButton", "CreateDOFObjectsButton",
	    rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
	    iAttachTopY, kFBAttachTop, "", 1.0,
	    iCreateButtonWidth, kFBAttachLeft, "", 1.0,
	    rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "CreateDOFObjectsButton", m_createDOFObjectsButton );

    // Create Screen Fade Object
    AddRegion( "CreateScreenFadeObjectButton", "CreateScreenFadeObjectButton",
        iCreateButtonWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
        iAttachTopY, kFBAttachTop, "", 1.0,
        iCreateButtonWidth + rexMBAnimExportCommon::c_iMargin + iCreateButtonWidth, kFBAttachLeft, "", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
    SetControl( "CreateScreenFadeObjectButton", m_createScreenFadeObjectButton );

    iAttachTopY += (rexMBAnimExportCommon::c_iRowHeight * 2);

    // Global Light Intensity
	AddRegion( "LightIntensityLabel", "LightIntensityLabel",
	    rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
	    iAttachTopY, kFBAttachTop, "", 1.0,
	    iWideLabelWidth, kFBAttachNone, "", 1.0,
	    rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "LightIntensityLabel", m_lightIntensityLabel );

	AddRegion( "LightIntensitySlider", "LightIntensitySlider",
	    rexMBAnimExportCommon::c_iMargin + iWideLabelWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "", 1.0,
	    iAttachTopY, kFBAttachTop, "", 1.0,
	    -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iNumericUpDownWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
	    rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "LightIntensitySlider", m_lightIntensitySlider );

	AddRegion( "LightIntensityNumericUpDown", "LightIntensityNumericUpDown",
	    -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachRight, "", 1.0,
	    iAttachTopY, kFBAttachTop, "", 1.0,
	    -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
	    rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "LightIntensityNumericUpDown", m_lightIntensityNumericUpDown );

	AddRegion( "IncreaseLightIntensityButton", "IncreaseLightIntensityButton",
	    -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachRight, "", 1.0,
	    iAttachTopY, kFBAttachTop, "", 1.0,
	    -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
	    rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "IncreaseLightIntensityButton", m_increaseLightIntensityButton );

	AddRegion( "DecreaseLightIntensityButton", "DecreaseLightIntensityButton",
	    -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachRight, "", 1.0,
	    iAttachTopY, kFBAttachTop, "", 1.0,
	    -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "", 1.0,
	    rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, "", 1.0 );
	SetControl( "DecreaseLightIntensityButton", m_decreaseLightIntensityButton );
}

void RexRageEffectsTool::UIConfigure()
{
    // Rmptfx Path
    m_rmptfxLabel.Caption = "Rmptfx Path:";
    m_rmptfxLabel.Justify = kFBTextJustifyRight;

    m_rmptfxPathLabel.Caption = "";

	m_scriptDatLabel.Caption = "Script Path:";
	m_scriptDatLabel.Justify = kFBTextJustifyRight;

	m_scriptDatPathLabel.Caption = "";

	m_toggleRootCheckbox.Caption = "Root Dir";
	m_toggleRootCheckbox.Style = kFBCheckbox;
	m_toggleRootCheckbox.State = true;
	m_toggleRootCheckbox.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::ToggleRootCheckbox_Click );

    // FX List
    m_fxListLabel.Caption = "Fx List:";
    m_fxListLabel.Justify = kFBTextJustifyRight;

    m_fxListFileComboBox.Style = kFBDropDownList;
    m_fxListFileComboBox.OnChange.Add( this, (FBCallback)&RexRageEffectsTool::FxListComboBox_Changed );
    
    // Effect Name
    m_effectNamesLabel.Caption = "Effects:";
    m_effectNamesLabel.Justify = kFBTextJustifyRight;

	m_effectNamesComboBox.Style = kFBDropDownList;
	m_effectNamesComboBox.ExtendedSelect = true;

    m_createEffectButton.Caption = "Create";
    m_createEffectButton.Enabled = false;
	m_createEffectButton.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::CreateEffectButton_Click );

	m_refreshDecalButton.Caption = "Refresh";
	m_refreshDecalButton.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::RefreshDecalsButton_Click );

    m_refreshEffectsButton.Caption = "Refresh";
    m_refreshEffectsButton.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::RefreshEffectsButton_Click );

	// Decal Name
	m_decalNamesLabel.Caption = "Decals:";
	m_decalNamesLabel.Justify = kFBTextJustifyRight;

	m_decalNamesComboBox.Style = kFBDropDownList;
	m_decalNamesComboBox.ExtendedSelect = true;

	m_createDecalButton.Caption = "Create";
	m_createDecalButton.Enabled = false;
	m_createDecalButton.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::CreateDecalButton_Click );

    // Create DOF Objects
	m_createDOFObjectsButton.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::CreateDOFObjectsButton_Click );
    m_createDOFObjectsButton.Caption = "Create DOF Objects";

    // Create Screen Fade Object
    m_createScreenFadeObjectButton.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::CreateScreenFadeObjectButton_Click );
    m_createScreenFadeObjectButton.Caption = "Create Screen Fade Object";

    // Global Light Intensity
    m_lightIntensityLabel.Caption = "Global Light Intensity:";

	m_lightIntensitySlider.Min = 0.0;
	m_lightIntensitySlider.Max = 100.0;
	m_lightIntensitySlider.Orientation = kFBHorizontal;
	m_lightIntensitySlider.OnChange.Add( this, (FBCallback)&RexRageEffectsTool::LightIntensitySlider_Changed );
	
	m_lightIntensityNumericUpDown.Value = 0.0;
	m_lightIntensityNumericUpDown.OnChange.Add( this, (FBCallback)&RexRageEffectsTool::LightIntensityNumericUpDown_Changed );

	m_increaseLightIntensityButton.Caption = "+";
	m_increaseLightIntensityButton.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::IncreaseLightIntensityButton_Click );

	m_decreaseLightIntensityButton.Caption = "-";
	m_decreaseLightIntensityButton.OnClick.Add( this, (FBCallback)&RexRageEffectsTool::DecreaseLightIntensityButton_Click );	
}

void RexRageEffectsTool::ToggleRootCheckbox_Click(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	RefreshEffectsButton_Click(NULL,NULL);
}

void RexRageEffectsTool::FxListComboBox_Changed( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( m_fxListFileComboBox.ItemIndex == -1 )
    {
        m_createEffectButton.Enabled = false;
        return;
    }

    char cFxListFilename[RAGE_MAX_PATH];
	sprintf( cFxListFilename, "%s/fxlists/%s", (const char*)m_rmptfxPathLabel.Caption, 
        (char *)m_fxListFileComboBox.Items.GetAt( m_fxListFileComboBox.ItemIndex ) );

    if ( LoadFxListFile( cFxListFilename ) )
    {
        m_createEffectButton.Enabled = m_effectNamesComboBox.Items.GetCount() > 0;
    }
    else
    {
        m_createEffectButton.Enabled = false;
    }
}

void RexRageEffectsTool::CreateDecalButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	char cDecalListName[RAGE_MAX_PATH];
	safecpy( cDecalListName, (char *)m_decalNamesComboBox.Items.GetAt( m_decalNamesComboBox.ItemIndex ), sizeof(cDecalListName) );

	HFBModel pDecalModel = rexMBAnimExportCommon::CreateDecal( cDecalListName );
	if ( pDecalModel == NULL )
	{
		FBMessageBox( "Create Decal Error", "Unable to create the placeholder decal model.", "OK" );
		return;
	}

	rexMBAnimExportCommon::AddDecalProperties(pDecalModel);

	// select the model
	FBModelList selectedModels;
	FBGetSelectedModels( selectedModels, NULL, true );
	for ( int i = 0; i < selectedModels.GetCount(); ++i )
	{
		selectedModels[i]->Selected = false;
	}

	pDecalModel->Selected = true;
}

void RexRageEffectsTool::CreateEffectButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    // create the model
    char cRmptfxName[RAGE_MAX_PATH];
    safecpy( cRmptfxName, (char *)m_effectNamesComboBox.Items.GetAt( m_effectNamesComboBox.ItemIndex ), sizeof(cRmptfxName) );

	char cRmptfxListName[RAGE_MAX_PATH];
	safecpy( cRmptfxListName, (char *)m_fxListFileComboBox.Items.GetAt( m_fxListFileComboBox.ItemIndex ), sizeof(cRmptfxListName) );

    HFBModelNull pParticleEffectModel = rexMBAnimExportCommon::CreateParticleEffect( cRmptfxName );
    if ( pParticleEffectModel == NULL )
    {
        FBMessageBox( "Create Effect Error", "Unable to create the placeholder particle effect model.", "OK" );
        return;
    }

    // add our custom properties
    char cRmptfxPath[RAGE_MAX_PATH];
	safecpy( cRmptfxPath, (const char*)m_rmptfxPathLabel.Caption, sizeof(cRmptfxPath) );

    rexMBAnimExportCommon::AddParticleEffectProperties( pParticleEffectModel, cRmptfxPath, cRmptfxName, cRmptfxListName );

    // select the model
    FBModelList selectedModels;
    FBGetSelectedModels( selectedModels, NULL, true );
    for ( int i = 0; i < selectedModels.GetCount(); ++i )
    {
        selectedModels[i]->Selected = false;
    }

    pParticleEffectModel->Selected = true;
}

void RexRageEffectsTool::RefreshDecalList()
{
	char cProjRoot[RAGE_MAX_PATH];
	if(!m_toggleRootCheckbox.State && rexMBAnimExportCommon::IsDlc(atString("")))
	{
		ConfigGameView gameView;
		char cBranch[RAGE_MAX_PATH];
		gameView.GetBranch( cBranch, RAGE_MAX_PATH );
		atString atRoot = rexMBAnimExportCommon::GetProjectOrDlcRootDir(atString(""));
		sprintf(cProjRoot, "%s\\build\\%s", atRoot.c_str(), cBranch);
	}
	else
	{
		rexMBAnimExportCommon::GetEnvironmentVariable("RS_BUILDBRANCH", cProjRoot, RAGE_MAX_PATH);
	}
	char cScriptFXFile[RAGE_MAX_PATH];
	sprintf(cScriptFXFile, "%s\\common\\data\\effects\\decals_cs.dat", cProjRoot );

	string line;
	ifstream infile;
	infile.open (cScriptFXFile);

	FBApplication app;
	char czFileName[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(czFileName, RAGE_MAX_PATH, ASSET.FileName(app.FBXFileName.AsString()));

	bool bAdd =false;
	m_decalNamesComboBox.Items.Clear();
	if(infile.is_open())
	{
		while (!infile.eof()) 
		{
			getline(infile,line); 

			atString strLine(line.c_str());
			strLine.Trim();
			strLine.Lowercase();
			atString strFileName(atString("#"), czFileName);
			strFileName.Lowercase();

			if(strLine.StartsWith(strFileName) ||
				strLine.StartsWith("#common"))
			{
				bAdd = true;
				continue;
			}

			if(strLine == "")
				bAdd=false;

			if(bAdd)
			{
				atArray<atString> atLineEntries;
				strLine.Split(atLineEntries, "\t",true);

				if(atLineEntries.GetCount() > 0)
				m_decalNamesComboBox.Items.Add((char*)atLineEntries[0].c_str());
			}
		}
	}

	if(m_decalNamesComboBox.Items.GetCount() > 0)
		m_createDecalButton.Enabled = true;
}

void RexRageEffectsTool::RefreshDecalsButton_Click( HISender /*pSender*/,HKEvent /*pEvent*/ )
{
	RefreshDecalList();
}

void RexRageEffectsTool::RefreshEffectsButton_Click( HISender pSender,HKEvent pEvent )
{
	atString atArtRoot;
	if (m_toggleRootCheckbox.State)
	{
		rexMBAnimExportCommon::GetArtPath(atArtRoot);
	}
	else
	{
		atArtRoot = rexMBAnimExportCommon::GetProjectOrDlcArtDir(atString(""));
	}
	char cRmptfxPath[RAGE_MAX_PATH];
	sprintf(cRmptfxPath, "%s\\VFX\\rmptfx", atArtRoot.c_str() );
	m_rmptfxPathLabel.Caption = cRmptfxPath;

	char cProjRoot[RAGE_MAX_PATH];
	if(!m_toggleRootCheckbox.State && rexMBAnimExportCommon::IsDlc(atString("")))
	{
		ConfigGameView gameView;
		char cBranch[RAGE_MAX_PATH];
		gameView.GetBranch( cBranch, RAGE_MAX_PATH );
		atString atRoot = rexMBAnimExportCommon::GetProjectOrDlcRootDir(atString(""));
		sprintf(cProjRoot, "%s\\build\\%s", atRoot.c_str(), cBranch);
	}
	else
	{
		rexMBAnimExportCommon::GetEnvironmentVariable("RS_BUILDBRANCH", cProjRoot, RAGE_MAX_PATH);
	}
	char cScriptFXFile[RAGE_MAX_PATH];
	sprintf(cScriptFXFile, "%s\\common\\data\\effects\\scriptfx.dat", cProjRoot );

	m_scriptDatPathLabel.Caption = cScriptFXFile;

	string line;
	ifstream infile;
	infile.open (cScriptFXFile);

	FBApplication app;
	char czFileName[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(czFileName, RAGE_MAX_PATH, ASSET.FileName(app.FBXFileName.AsString()));

	m_fxListFileComboBox.Items.Clear();
	m_effectNamesComboBox.Items.Clear();
	if(infile.is_open())
	{
		while (!infile.eof()) 
		{
			getline(infile,line); 

			atString strLine(line.c_str());
			strLine.Lowercase();
			atString strFileName(czFileName);
			strFileName.Lowercase();

			if(strLine.StartsWith(strFileName))
			{
				atArray<atString> atLineEntries;
				strLine.Split(atLineEntries, "\t",true);

				if(atLineEntries.GetCount() != 2) continue;

				atLineEntries[1].Trim();

				atString strFxList(atLineEntries[1], ".fxlist");

				if(m_fxListFileComboBox.Items.Find((char*)strFxList.c_str()) == -1)
					m_fxListFileComboBox.Items.Add( const_cast<char *>( strFxList.c_str() ) );

				m_fxListFileComboBox.ItemIndex = 0;

				FxListComboBox_Changed( pSender, pEvent );
			}
		}
		
		infile.close();
	}   
}

int RexRageEffectsTool::GetConstraintIndex(char* cConstraintName)
{
	FBConstraintManager& lCM = FBConstraintManager::TheOne();

	for(int i=0; i<lCM.TypeGetCount(); i++)
	{
		//const char* cDebug = lCM.TypeGetName(i);
		if(strcmp(lCM.TypeGetName(i), cConstraintName) == 0)
		{
			return i;
		}
		
	}
	return -1;
}

//----------------------------------------------------------------------------------
// Create null and plane objs for DOF represenation on selected camera
//----------------------------------------------------------------------------------
void RexRageEffectsTool::CreateDOFObjectsButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    FBModelList selectedModels;
    FBGetSelectedModels( selectedModels, NULL, true );
    
    HFBCamera pCamera = NULL;

    bool bFoundCamera = false;
    char cName[512];
    char cDOFName[1024];

    for ( int i = 0; i < selectedModels.GetCount(); ++i )
    {
        HFBModel pModel = selectedModels.GetAt( i );
        if ( pModel->Is( FBCamera::TypeInfo ) )
        {
            bFoundCamera = true;

            safecpy( cName, pModel->Name.AsString() );
            
            sprintf( cDOFName, "%s DOF", cName );
            HFBModel pDOFModel = RAGEFindModelByName( cDOFName );
            if ( pDOFModel == NULL )
            {
                pCamera = (HFBCamera)pModel;
                break;
            }            
        }
    }

    if ( pCamera == NULL )
    {
        if ( bFoundCamera )
        {
            FBMessageBox( "Create DOF Objects", "All of the selected cameras have an associated DOF Object already.", "OK" );
        }
        else
        {
            FBMessageBox( "Create DOF Objects", "The DOF Objects require a camera to be selected.  Please select one and try again.", "OK" );
        }

        return;
    }

    if ( pCamera->Interest == NULL )
    {
        FBMessageBox( "Create DOF Objects", "No Camera Interest found.  Unable to create DOF Objects.", "OK" );
        return;
    }

    char cNearPlaneName[1024];
    sprintf( cNearPlaneName, "%s NearPlane", cName );
    
    char cFarPlaneName[1024];
    sprintf( cFarPlaneName, "%s FarPlane", cName );

	HFBModelNull hDOFMark = new FBModelNull( cDOFName );
	HFBModel hNearPlane = new FBModelPlane( cNearPlaneName );
	HFBModel hFarPlane = new FBModelPlane( cFarPlaneName );

	FBConstraintManager& lCM = FBConstraintManager::TheOne();
	HFBConstraint hDOFConstraint;
	HFBConstraint hInterestConstraint;

	hDOFMark->Show = true;
	hNearPlane->Show = true;
	hFarPlane->Show = true;

	// Begin DOF setting
	bool bActive = true;
	FBVector3d vRotMax = FBVector3d(0.0f, 0.0f, 90.0f);

	hNearPlane->PropertyList.Find("RotationActive")->SetData( &bActive );
	hNearPlane->PropertyList.Find("TranslationActive")->SetData( &bActive );
	hNearPlane->PropertyList.Find("TranslationMinX")->SetData( &bActive );
	hNearPlane->PropertyList.Find("TranslationMinY")->SetData( &bActive );
	hNearPlane->PropertyList.Find("TranslationMinZ")->SetData( &bActive );
	hNearPlane->PropertyList.Find("TranslationMaxY")->SetData( &bActive );
	hNearPlane->PropertyList.Find("TranslationMaxZ")->SetData( &bActive );
	hNearPlane->PropertyList.Find("RotationMinX")->SetData( &bActive );
	hNearPlane->PropertyList.Find("RotationMinY")->SetData( &bActive );
	hNearPlane->PropertyList.Find("RotationMinZ")->SetData( &bActive );
	hNearPlane->PropertyList.Find("RotationMaxX")->SetData( &bActive );
	hNearPlane->PropertyList.Find("RotationMaxY")->SetData( &bActive );
	hNearPlane->PropertyList.Find("RotationMaxZ")->SetData( &bActive );
	hNearPlane->PropertyList.Find("RotationMax")->SetData( &vRotMax );
	hNearPlane->PropertyList.Find("RotationMin")->SetData( &vRotMax );

	hFarPlane->PropertyList.Find("RotationActive")->SetData( &bActive );
	hFarPlane->PropertyList.Find("TranslationActive")->SetData( &bActive );
	hFarPlane->PropertyList.Find("TranslationMinX")->SetData( &bActive );
	hFarPlane->PropertyList.Find("TranslationMinY")->SetData( &bActive );
	hFarPlane->PropertyList.Find("TranslationMinZ")->SetData( &bActive );
	hFarPlane->PropertyList.Find("TranslationMaxY")->SetData( &bActive );
	hFarPlane->PropertyList.Find("TranslationMaxZ")->SetData( &bActive );
	hFarPlane->PropertyList.Find("RotationMinX")->SetData( &bActive );
	hFarPlane->PropertyList.Find("RotationMinY")->SetData( &bActive );
	hFarPlane->PropertyList.Find("RotationMinZ")->SetData( &bActive );
	hFarPlane->PropertyList.Find("RotationMaxX")->SetData( &bActive );
	hFarPlane->PropertyList.Find("RotationMaxY")->SetData( &bActive );
	hFarPlane->PropertyList.Find("RotationMaxZ")->SetData( &bActive );
	hFarPlane->PropertyList.Find("RotationMax")->SetData( &vRotMax );
	hFarPlane->PropertyList.Find("RotationMin")->SetData( &vRotMax );		
	// End DOF setting

	int iRotationIdx = GetConstraintIndex("Rotation");
	int iParentChildIdx = GetConstraintIndex("Parent/Child");
			
	// Begin constraint creation
	hDOFConstraint = lCM.TypeCreateConstraint(iParentChildIdx); //Parent/Child
	hInterestConstraint = lCM.TypeCreateConstraint(iRotationIdx); //Rotation
	
	FBSystem system;
	system.Scene->Constraints.Add(hDOFConstraint);
	system.Scene->Constraints.Add(hInterestConstraint);
	
	hDOFConstraint->ReferenceAdd(0, hDOFMark);
	hDOFConstraint->ReferenceAdd(1, pCamera);
	hInterestConstraint->ReferenceAdd(0, pCamera->Interest);
	hInterestConstraint->ReferenceAdd(1, pCamera);
	
	hDOFConstraint->Active = true;
	hInterestConstraint->Active = true;

	// End constraint creation

	hNearPlane->Rotation = FBVector3d(0.0f, 0.0f, 90.0f);
	hFarPlane->Rotation = FBVector3d(0.0f, 0.0f, 90.0f);
	
	hNearPlane->Parent = hDOFMark;
	hFarPlane->Parent = hDOFMark;

	// Begin shader/material creation
	HFBShader pShader = rexMBAnimExportCommon::CreateShader("LightedShader", "DOFLighted");
	HFBProperty pTransProp = pShader->PropertyList.Find("Transparency");
	HFBProperty pFactorProp = pShader->PropertyList.Find("Alpha");
	HFBMaterial	pNearMat	= new FBMaterial("nearDOFmat");
	HFBMaterial	hFarMat	= new FBMaterial("farDOFmat");	
	HFBProperty pNearDiff = pNearMat->PropertyList.Find("Diffuse");
	HFBProperty hFarDiff = hFarMat->PropertyList.Find("Diffuse");
	HFBProperty pNearEmiss = pNearMat->PropertyList.Find("Emissive");
	HFBProperty hFarEmiss = hFarMat->PropertyList.Find("Emissive");
	
	int iTransIdx = 2;
	double dFactor = 0.31;
	FBVector3d vNearDiff(0.80f, 0.28f, 0.01f);
	FBVector3d vFarDiff(0.0f, 0.0f, 1.0f);
	
	pTransProp->SetData(&iTransIdx); // Set transparency to translucent
	pFactorProp->SetData(&dFactor); // Set alpha
	pNearDiff->SetData(&vNearDiff);
	hFarDiff->SetData(&vFarDiff);
	pNearEmiss->SetData(&vNearDiff);
	hFarEmiss->SetData(&vFarDiff);
	// End shader/material  creation

	// Apply shader and materials
	hNearPlane->Shaders.Add(pShader);
	hFarPlane->Shaders.Add(pShader);
	HFBGeometry pGeom;
	pGeom = hNearPlane->Geometry;
	//pGeom->Materials.Add(pNearMat);
	pGeom = hFarPlane->Geometry;
	//pGeom->Materials.Add(hFarMat);

    hNearPlane->Translation = FBVector3<double>( DEFAULT_NEAR_DEPTH_OF_FIELD * 100.0, 0.0, 0.0 );
    hFarPlane->Translation = FBVector3<double>( DEFAULT_FAR_DEPTH_OF_FIELD * 100.0, 0.0, 0.0 );        
}

void RexRageEffectsTool::CreateScreenFadeObjectButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    FBModelList selectedModels;
    FBGetSelectedModels( selectedModels, NULL, true );

    HFBCamera pCamera = NULL;

    bool bFoundCamera = false;
    char cName[512];
    char cDOFName[1024];

    for ( int i = 0; i < selectedModels.GetCount(); ++i )
    {
        HFBModel pModel = selectedModels.GetAt( i );
        if ( pModel->Is( FBCamera::TypeInfo ) )
        {
            bFoundCamera = true;

            safecpy( cName, pModel->Name.AsString() );

            sprintf( cDOFName, "%s Screen Fade", cName );
            HFBModel pDOFModel = RAGEFindModelByName( cDOFName );
            if ( pDOFModel == NULL )
            {
                pCamera = (HFBCamera)pModel;
                break;
            }            
        }
    }

    if ( pCamera == NULL )
    {
        if ( bFoundCamera )
        {
            FBMessageBox( "Create Screen Fade Object", "All of the selected cameras have an associated Screen Fade Object already.", "OK" );
        }
        else
        {
            FBMessageBox( "Create Screen Fade Object", "The Screen Fade Object requires a camera to be selected.  Please select one and try again.", "OK" );
        }

        return;
    }

    char cScreenFadeName[1024];
    sprintf( cScreenFadeName, "%s Screen Fade", cName );

    HFBModel pScreenFadeModel = new FBModelPlane( cScreenFadeName );

    pScreenFadeModel->Show = true;

    FBVector3d rotation = pCamera->Rotation;
    pScreenFadeModel->Rotation = rotation;

    FBVector3d translation = pCamera->Translation;
    pScreenFadeModel->Translation = translation;

    // Begin DOF setting
    bool bActive = true;
    pScreenFadeModel->PropertyList.Find( "RotationActive" )->SetData( &bActive );
    pScreenFadeModel->PropertyList.Find( "TranslationActive" )->SetData( &bActive );
    // End DOF setting

    // Begin constraint creation
	FBConstraintManager& lCM = FBConstraintManager::TheOne();
    int iParentChildIdx = GetConstraintIndex( "Parent/Child" );
    HFBConstraint pScreenFadeConstraint = lCM.TypeCreateConstraint( iParentChildIdx );

	FBSystem system;
	system.Scene->Constraints.Add(pScreenFadeConstraint);

    pScreenFadeConstraint->ReferenceAdd( 0, pScreenFadeModel );
    pScreenFadeConstraint->ReferenceAdd( 1, pCamera );

	pScreenFadeConstraint->Active = true;

    char cPropertName[1024];
    sprintf( cPropertName, "%s.Offset R", cName );

    rotation[0] = 0.0;
    rotation[1] = 0.0;
    rotation[2] = 90.0;     // rotate 90 degrees about the z-axis so that the plane covers the camera lens
    pScreenFadeConstraint->PropertyList.Find( cPropertName )->SetData( &rotation );

    sprintf( cPropertName, "%s.Offset T", cName );

    translation[0] = 11.0;  // This is the minimum distance away from the camera before the plane is in view
    translation[1] = 0.0;
    translation[2] = 0.0;
    pScreenFadeConstraint->PropertyList.Find( cPropertName )->SetData( &translation );
    // End constraint creation

    // Begin shader/material creation
    HFBShader pShader = rexMBAnimExportCommon::CreateShader( "FlatShader", "ScreenFadeShader" );
    
    // Set transparency to accurate transparency
    int iTransparencyTypeIndex = 1;
    pShader->PropertyList.Find( "Transparency" )->SetData( &iTransparencyTypeIndex );
    
    // make visible, the material will control the transparency
    double dAlpha = 1.0;
    pShader->PropertyList.Find( "Alpha" )->SetData( &dAlpha );
    
    HFBMaterial	pMaterial = new FBMaterial( "ScreenFadeMaterial" );

    // default to black
    FBVector3d vColor( 0.0, 0.0, 0.0 );
    pMaterial->PropertyList.Find( "Diffuse" )->SetData( &vColor );
    pMaterial->PropertyList.Find( "Emissive" )->SetData( &vColor );

    // make transparent
    double dOpacity = 0.0;
    pMaterial->PropertyList.Find( "Opacity" )->SetData( &dOpacity );
    // End shader/material  creation

    // Apply shader and materials
    pScreenFadeModel->Shaders.Add( pShader );

    //HFBGeometry pGeometry = pScreenFadeModel->Geometry;
    //pGeometry->Materials.Add( pMaterial );
    // End shader/material creation
}

void RexRageEffectsTool::IncreaseLightIntensityButton_Click( HISender /*pSender*/, HKEvent /*pEvent */)
{
	UpdateSceneLights( true );
}

void RexRageEffectsTool::DecreaseLightIntensityButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	UpdateSceneLights( false );
}

void RexRageEffectsTool::LightIntensitySlider_Changed( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_lightIntensityNumericUpDown.Value = (float)m_lightIntensitySlider.Value;
}

void RexRageEffectsTool::LightIntensityNumericUpDown_Changed( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_lightIntensitySlider.Value = (double)m_lightIntensityNumericUpDown.Value;
}

//----------------------------------------------------------------------------------
// Parse effect file for cutscene effects
//----------------------------------------------------------------------------------
bool RexRageEffectsTool::LoadFxListFile( const char *pFilename )
{	
    fiStream *pStream = ASSET.Open( pFilename, NULL );
	if ( pStream )
	{
        m_effectNamesComboBox.Items.Clear();

        atString cutsceneSectionName("");
        rexMBAnimExportCommon::GetMotionBuilderFxListCutsceneSectionName( cutsceneSectionName );
        cutsceneSectionName.Lowercase();

        bool bInCutsceneSection = cutsceneSectionName == "all";

        while ( pStream->Tell() != pStream->Size() - 1 )
        {
            char cBuffer[1024];
            if ( !rexMBAnimExportCommon::ReadLine( pStream, cBuffer, 1024 ) )
            {
                break;
            }

            atString line( cBuffer );
            line.Lowercase();
            if ( line.StartsWith( "## " ) )
            {
                if ( cutsceneSectionName != "all" )
                {
                    if ( bInCutsceneSection )
                    {
                        // we're done
                        break;
                    }
                    else if ( line.IndexOf( cutsceneSectionName ) == 3 )
                    {
                        // we can start adding particle effects
                        bInCutsceneSection = true;
                    }
                }
            }
            else if ( bInCutsceneSection )
            {
                if ( !line.StartsWith( "#" ) )
                {
					line.Trim();
                    // add the particle effect
                    m_effectNamesComboBox.Items.Add( (char*)line.c_str(), 0 );
                }
            }
        }

        pStream->Close();

        return true;
	}

    return false;
}

void RexRageEffectsTool::UpdateSceneLights(bool bIncrease)
{
    FBSystem system;
    HFBScene lScene = system.Scene;
    FBPropertyListLight lightsList = lScene->Lights;

	for ( int i = 0; i < lightsList.GetCount(); ++i )
	{
		HFBLight pLight = lightsList[i];
		HFBAnimationNode pAnimNode = pLight->AnimationNodeInGet();
		if ( pAnimNode )
		{
			HFBAnimationNode hIntensityNode = rexMBAnimExportCommon::FindAnimByName( pAnimNode, "Intensity" );
			if ( hIntensityNode )
			{
				int iKeyCount = hIntensityNode->FCurve->Keys.GetCount();
				for ( int i = 0; i < iKeyCount; ++i )
				{                    
					if ( bIncrease )
                    {
                        hIntensityNode->FCurve->Keys[i].Value 
                            = hIntensityNode->FCurve->Keys[i].Value + ((float) (m_lightIntensitySlider.Value / 100.0) * hIntensityNode->FCurve->Keys[i].Value);
                    }
					else
                    {
                        hIntensityNode->FCurve->Keys[i].Value 
                            = hIntensityNode->FCurve->Keys[i].Value - ((float) (m_lightIntensitySlider.Value / 100.0) * hIntensityNode->FCurve->Keys[i].Value);
                    }
				}

                continue;
			}
		}
        
        // if we didn't find the animation node, just change the property
        if ( bIncrease )
        {
            pLight->Intensity = pLight->Intensity + ((m_lightIntensitySlider.Value / 100.0) * pLight->Intensity);
        }
        else
        {
            pLight->Intensity = pLight->Intensity - ((m_lightIntensitySlider.Value / 100.0) * pLight->Intensity);
        }
	}
}

