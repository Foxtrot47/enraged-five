// /animExportToolCmgr.h

#ifndef __ANIM_EXPORT_TOOL_CMGR_H__
#define __ANIM_EXPORT_TOOL_CMGR_H__

#include "rexMBAnimExportCommon/utility.h"

#define ANIMEXPORTCUSTOMMGR__CLASSNAME RexRageAnimExportCustomMgr
#define ANIMEXPORTCUSTOMMGR__CLASSSTR  "RexRageAnimExportCustomMgr"

//-----------------------------------------------------------------------------

//This whole class exists only to call FBPopNormalTool on the animation exporter
//which allows for the tool to load data from files before the user has opened the
//animation export tool for the first time... a waste of code, but it works.

class RexRageAnimExportCustomMgr : public FBCustomManager
{
    //--- FiLMBOX box declaration.
    FBCustomManagerDeclare( RexRageAnimExportCustomMgr );

public:
    virtual bool FBCreate();        //!< FiLMBOX creation function.
    virtual void FBDestroy();       //!< FiLMBOX destruction function.

    virtual bool Init();
    virtual bool Open();
    virtual bool Clear();
    virtual bool Close();

private:

};

//-----------------------------------------------------------------------------

#endif //__ANIM_EXPORT_TOOL_CMGR_H__

