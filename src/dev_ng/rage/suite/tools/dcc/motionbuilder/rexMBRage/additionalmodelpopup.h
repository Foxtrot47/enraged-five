// 
// rexMBRage/additionalmodelpopup.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REXMBRAGE_ADDITIONALMODEL_H__ 
#define __REXMBRAGE_ADDITIONALMODEL_H__ 

#include "cutfile/cutfdefines.h"
#include "init/ini.h"
#include "parsercore/attribute.h"
#include "vector/vector3.h"
#include "rexMBAnimExportCommon/utility.h"
#include "animExportTool.h"

namespace rage {

	class cutfObject;
	class cutfCutsceneFile2;

	//##############################################################################

	// PURPOSE: A class for displaying and editing a parAttributeList as a popup.
	class AdditionalModelsPopup : public FBPopup
	{
	public:
		enum eModelSheetColumn
		{
			kModelIndex = -1,
			kModelName,
			kTrackType,
			kAttributeName
		};

		AdditionalModelsPopup(atArray< AetAdditionalModel* >& models);
		virtual ~AdditionalModelsPopup();

		virtual void UICreate();

		// PURPOSE: Edits the labels, adds the event handlers, etc..
		virtual void UIConfigure();

		void AddRootButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/);
		void OKButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/);
		void SpreadSheet_CellChange( HISender /*pSender*/, HKEvent pEvent);
		void SpreadSheet_RowClick( HISender /*pSender*/, HKEvent pEvent);
		void RemoveModelButton_Click( HISender /*pSender*/, HKEvent pEvent);

		void SetData();
		void AddRow(AetAdditionalModel* pModelPair);

	private:

		int				m_iAttachTopY;
		int				m_iModelPairSpreadRowId;
		int				m_iModelPairSheetSelRowIndex;
		std::map<int, AetAdditionalModel*> m_rowIdToModelPairMap;
		FBSpread		m_spreadsheet;
		FBButton		m_addRootButton;
		FBButton		m_removeButton;
		FBButton		m_okButton;

		atArray< AetAdditionalModel* >& m_additionalModels;
	};

	//##############################################################################
}

#endif // __REXMBRAGE_ADDITIONALMODEL_H__ 
