// /animExportTool.cpp
#include <string>
#include <sstream>
#include <vector>

#include "atl/functor.h"
#include "file/device.h"

#include "rexMBAnimExportCommon/timerLog.h"
#include "rexMBAnimExportCommon/utility.h"

#include "UserObjects/AnimMarkup.h"

#include "animExportTool.h"

#include "rexMBAnimExportCommon/Logger.h"
#include "rexMBAnimExportCommon/syncedscene.h"

#include "additionalmodelpopup.h"
#include "configParser/configGameView.h"
#include "configParser/configExportView.h"

#include "rexMBAnimExportCommon/pipeline.h"
#include "rexMBAnimExportCommon/perforce.h"

#define	EXPORTTOOL__CLASSNAME	RexRageAnimExportTool
#define EXPORTTOOL__CLASSSTR	"RexRageAnimExportTool"
#define	EXPORTTOOL__CLASS		EXPORTTOOL__CLASSNAME
#define	EXPORTTOOL__LABEL		"Rex Rage Animation Export"
#define	EXPORTTOOL__DESC		"Rex Rage Animation Export Tool"

#define EXPORTTOOL_UNSET_OUTPUT_PATH	"(none specified)"

#pragma warning(push)
#pragma warning(disable:4100)

FBToolImplementation( EXPORTTOOL__CLASS );
FBRegisterTool(	EXPORTTOOL__CLASS, EXPORTTOOL__LABEL, EXPORTTOOL__DESC, FB_DEFAULT_SDK_ICON	);

#pragma warning(pop)

using namespace std;
using namespace rage;
using namespace configParser;

//-----------------------------------------------------------------------------

static atString s_ModelTypesStr("Body~Face~Camera");

static ConfigGameView gs_GameView;
static ConfigExportView gs_ExportView;

static RexRageAnimExportTool *s_pCurrentAnimExportTool = NULL;
atArray<atString> RexRageAnimExportTool::ms_ctrlPathTemplates;
atArray<atString> RexRageAnimExportTool::ms_compressionPathTemplates;
atArray<atString> RexRageAnimExportTool::ms_skeletonFiles;

const char* RexRageAnimExportTool::m_DefaultSpecFile = "(no spec file)";


RexRageAnimExportTool* RexRageAnimExportTool::GetCurrentAnimExportToolInst() { return s_pCurrentAnimExportTool; }

//-----------------------------------------------------------------------------

string NormalizeName(const char* szName)
{
	string stdName(szName);

	//Replace all the spaces with underscores
	size_t len = stdName.size();
	for(size_t i=0; i < len; i++)
	{
		if(isspace(stdName[i]))
		{
			stdName[i] = '_';
		}
	}

	return stdName;
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportTool::FBCreate()
{
	m_iSegSpreadRowId = 0;
	m_iSegSheetSelRowIndex = -1;

	//Setup the UI
	UICreate();
    UIConfigure();

	//Setup the various spreadsheets
	UIRestoreDefaults();

	//Add system based callbacks
	m_system.Scene->OnTakeChange.Add( this, (FBCallback)&RexRageAnimExportTool::SceneOnTakeChange_Event);

	//Add application based callbacks
	m_app.OnFileNew.Add( this, (FBCallback)&RexRageAnimExportTool::FileNew_Event);
	m_app.OnFileNewCompleted.Add( this, (FBCallback)&RexRageAnimExportTool::FileNewCompleted_Event);
	m_app.OnFileOpenCompleted.Add( this, (FBCallback)&RexRageAnimExportTool::FileOpenCompleted_Event);
	m_app.OnFileSave.Add(this, (FBCallback)&RexRageAnimExportTool::FileSave_Event);
	m_app.OnFileSaveCompleted.Add(this, (FBCallback)&RexRageAnimExportTool::FileSaveCompleted_Event);

	//This will only get called the first time the tool is created, so we need to check for persistent data
	//in the currently opened file and load it into the tool.  After the tool has been opened once then
	//the loading of persistent data will take place in the file-open completed callback...
	if(UserObjects::FindPersistentData(false))
	{
		RexAnimMarkupData* pAnimData = UserObjects::GetAnimationMarkupData();
		if(pAnimData)
		{
			ValidateMarkupData(pAnimData);

			RexRageAnimExport animExporter;		
			animExporter.SetInteractiveMode(true);
			
			char cLogFilename[RAGE_MAX_PATH*2];
			sprintf_s( cLogFilename, RAGE_MAX_PATH, "%s\\log.ulog", m_outputPathEdit.Text.AsString() );
			//ULOGGER_MOBO.PreExport( cLogFilename );
			/*bool bRes =*/ animExporter.LoadClipDataAllTakes( pAnimData );
			//ULOGGER_MOBO.PostExport( !bRes );

			UIPopulate(pAnimData);

			rexMBAnimExportCommon::LoadBoneTranslationConfig( "Ped" );

			delete pAnimData;
		}
	}
	else
	{
		atString ctrlFile("");
		if(rexMBAnimExportCommon::GetDefaultControlFile(ctrlFile) == true)
			m_ctrlFilePathEdit.Text = const_cast<char*>(ctrlFile.c_str());

		rexMBAnimExportCommon::LoadBoneTranslationConfig( "Ped" );
	}

	rexMBAnimExportCommon::LoadBoneOffsetValidationConfigFile();
	rexMBAnimExportCommon::LoadAdditionalModelConfigFile();

	rexMBAnimExportCommon::SetNewOrigin((m_initOffsetCheckbox.State != kFBButtonState0), atString(m_originObjectEdit.Text.AsString()));

	char cAssetDir[RAGE_MAX_PATH];
	gs_GameView.GetAssetsDir(cAssetDir, RAGE_MAX_PATH);

	char cSyncedSceneDir[RAGE_MAX_PATH];
	sprintf(cSyncedSceneDir, "%s\\anim\\synced_scenes\\...", cAssetDir);
	perforce::Sync(cSyncedSceneDir, true);
	perforce::ExecuteQueuedOperations(true);

	s_pCurrentAnimExportTool = this;

	return true;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::FBDestroy()
{	
	UIRestoreDefaults();

	//Remove system based callbacks
	m_system.Scene->OnTakeChange.Remove( this, (FBCallback)&RexRageAnimExportTool::SceneOnTakeChange_Event);

	//Remove application based callbacks
	m_app.OnFileNew.Remove( this, (FBCallback)&RexRageAnimExportTool::FileNew_Event);
	m_app.OnFileNewCompleted.Remove( this, (FBCallback)&RexRageAnimExportTool::FileNewCompleted_Event);
	m_app.OnFileOpenCompleted.Remove( this, (FBCallback)&RexRageAnimExportTool::FileOpenCompleted_Event);
	m_app.OnFileSave.Remove( this, (FBCallback)&RexRageAnimExportTool::FileSave_Event);
}

bool RexRageAnimExportTool::FbxStore ( HFBFbxObject /*pFbxObject*/, kFbxObjectStore /*pStoreWhat*/ )  { return true; }
bool RexRageAnimExportTool::FbxRetrieve ( HFBFbxObject /*pFbxObject*/, kFbxObjectStore /*pStoreWhat*/ ) { return true; }

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UICreate()
{
    StartSize[0] = 1280;
    StartSize[1] = (26 * cUiRowHeight) + (cUiMargin * 2);

    MinSize[0] = 750;
    MinSize[1] = (26 * cUiRowHeight) + (cUiMargin * 2);

    int iAttachTopY = cUiMargin;
	int iAttachBottomHeight = (-2 * cUiRowHeight) - cUiMargin;

	int iFilePathEditWidth = 350;

	// Version
	AddRegion( "VersionLabel", "VersionLabel",
		cUiAttachRightX - (cUiButtonWidth + cUiLabelWidth), kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiAttachRightX - (cUiButtonWidth + 20), kFBAttachRight, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "VersionLabel", m_versionLabel );

	// Perforce Integration
	AddRegion( "PerforceCheckbox", "PerforceCheckbox",
		rexMBAnimExportCommon::c_iMargin + ( 6* (rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin)), kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "PerforceCheckbox", m_usePerforceIntegration );

    // Help
    AddRegion( "HelpButton", "HelpButton",
        cUiAttachRightX - cUiButtonWidth, kFBAttachRight, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        cUiAttachRightX, kFBAttachRight, NULL, 1.0,
        cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "HelpButton", m_helpButton );

    iAttachTopY += cUiRowHeight;

	//Begin Output Path...
	AddRegion( "OuputPathLabel", "OuputPathLabel",
        cUiAttachLeftX, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        cUiLabelWidth, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OuputPathLabel", m_outputPathLabel );

    AddRegion( "OutputPathEdit", "OutputPathEdit",
        cUiAttachLeftX + cUiLabelWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        iFilePathEditWidth, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OutputPathEdit", m_outputPathEdit );

	 AddRegion( "OuputPathButton", "OuputPathButton",
        cUiAttachLeftX + cUiLabelWidth + cUiMargin + iFilePathEditWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        cUiButtonWidth, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OuputPathButton", m_outputPathButton );

	//...End Output Path

	iAttachTopY += cUiRowHeight;

	//Begin Control File Path...
	
	AddRegion( "ControlFilePathLabel", "ControlFilePathLabel",
        cUiAttachLeftX, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        cUiLabelWidth, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ControlFilePathLabel", m_ctrlFilePathLabel );

    AddRegion( "ControlFilePathEdit", "ControlFilePathEdit",
        cUiAttachLeftX + cUiLabelWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        iFilePathEditWidth, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ControlFilePathEdit", m_ctrlFilePathEdit );

	 AddRegion( "ControlFilePathButton", "ControlFilePathButton",
        cUiAttachLeftX + cUiLabelWidth + cUiMargin + iFilePathEditWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        cUiButtonWidth, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ControlFilePathButton", m_ctrlFilePathButton );

	//...End Control File Path

	iAttachTopY += cUiRowHeight;

	//Begin Control File Path...

	AddRegion( "DefaultCompressionTemplateLabel", "DefaultCompressionTemplateLabel",
		cUiAttachLeftX, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiLabelWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "DefaultCompressionTemplateLabel", m_defaultCompressionLabel );

	AddRegion( "DefaultCompressionTemplateEdit", "DefaultCompressionTemplateEdit",
		cUiAttachLeftX + cUiLabelWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		iFilePathEditWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "DefaultCompressionTemplateEdit", m_defaultCompressionEdit );

	AddRegion( "DefaultCompressionTemplateButton", "DefaultCompressionTemplateButton",
		cUiAttachLeftX + cUiLabelWidth + cUiMargin + iFilePathEditWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "DefaultCompressionTemplateButton", m_defaultCompressionButton );

	AddRegion( "DefaultCompressionTemplateSetButton", "DefaultCompressionTemplateSetButton",
		cUiAttachLeftX + cUiLabelWidth + cUiMargin + iFilePathEditWidth + cUiMargin + cUiButtonWidth, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "DefaultCompressionTemplateSetButton", m_defaultCompressionSetButton );

	//...End Control File Path

	iAttachTopY += cUiRowHeight;

	//Begin Control File Path...

	AddRegion( "AttributeFilePathLabel", "AttributeFilePathLabel",
		cUiAttachLeftX, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiLabelWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "AttributeFilePathLabel", m_attributeFilePathLabel );

	AddRegion( "AttributeFilePathEdit", "AttributeFilePathEdit",
		cUiAttachLeftX + cUiLabelWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		iFilePathEditWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "AttributeFilePathEdit", m_attributeFilePathEdit );

	AddRegion( "AttributeFilePathButton", "AttributeFilePathButton",
		cUiAttachLeftX + cUiLabelWidth + cUiMargin + iFilePathEditWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "AttributeFilePathButton", m_attributeFilePathButton );

	//...End Control File Path

	iAttachTopY += cUiRowHeight;

	//Begin Origin Object...

	AddRegion( "OriginObjectLabel", "OriginObjectLabel",
		cUiAttachLeftX, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiLabelWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "OriginObjectLabel", m_originObjectLabel );

	AddRegion( "OriginObjectEdit", "OriginObjectEdit",
		cUiAttachLeftX + cUiLabelWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		iFilePathEditWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "OriginObjectEdit", m_originObjectEdit );

	AddRegion( "OriginObjectButton", "OriginObjectButton",
		cUiAttachLeftX + cUiLabelWidth + cUiMargin + iFilePathEditWidth + cUiMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "OriginObjectButton", m_originObjectButton );

	AddRegion( "ClearOriginObjectButton", "ClearOriginObjectButton",
		cUiAttachLeftX + cUiLabelWidth + cUiMargin + iFilePathEditWidth + cUiMargin + cUiButtonWidth, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ClearOriginObjectButton", m_clearOriginObjectButton );

	//...End Origin Object

	iAttachTopY += (2 * cUiRowHeight);

	//Begin Model-Mover Pair Tagging...

	AddRegion( "ModelPairSheet", "ModelPairSheet",
		cUiAttachLeftX, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiAttachRightX - ( 2 * (cUiButtonWidth + (2*cUiMargin))) - 20, kFBAttachRight, "", 1.0,
		0, kFBAttachHeight, "", 0.4f);
	SetControl( "ModelPairSheet", m_modelPairSheet );

	AddRegion( "AddRootModel", "AddRootModel",
		cUiAttachRightX - (cUiButtonWidth * 2) - (2*cUiMargin) - 20, kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth + 10, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0);
	SetControl( "AddRootModel", m_addRootButton );

	AddRegion( "RemoveRootModel", "RemoveRootModel",
		cUiAttachRightX - cUiButtonWidth - cUiMargin - 10, kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth + 10, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0);
	SetControl( "RemoveRootModel", m_removeRootButton );

	AddRegion( "AddMoverModel", "AddMoverModel",
		cUiAttachRightX - (cUiButtonWidth * 2) - (2*cUiMargin) - 20, kFBAttachRight, "NULL", 1.0,
		iAttachTopY + cUiFieldHeight + cUiMargin, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth + 10, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0);
	SetControl( "AddMoverModel", m_addMoverButton );

	AddRegion( "RemoveMoverModel", "RemoveMoverModel",
		cUiAttachRightX - cUiButtonWidth - cUiMargin - 10, kFBAttachRight, "NULL", 1.0,
		iAttachTopY + cUiFieldHeight + cUiMargin, kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth + 10, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0);
	SetControl( "RemoveMoverModel", m_removeMoverButton );

	AddRegion( "AddAdditionalModels", "AddAdditionalModels",
		cUiAttachRightX - (cUiButtonWidth * 2) - (2*cUiMargin) - 20, kFBAttachRight, "NULL", 1.0,
		iAttachTopY + ((cUiFieldHeight + cUiMargin)*2), kFBAttachTop, "NULL", 1.0,
		cUiButtonWidth + 10, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0);
	SetControl( "AddAdditionalModels", m_addAdditionalButton );

	//...End  Model-Mover Pair Tagging

	iAttachTopY += (cUiFieldHeight * 10) + cUiMargin;

	//Begin Segment From Takes Button...

	AddRegion( "SegmentFromTakesButton", "SegmentFromTakesButton",
		cUiAttachLeftX, kFBAttachLeft, "NULL", 1.0,
		10, kFBAttachBottom, "ModelPairSheet", 1.0,
		200, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "SegmentFromTakesButton", m_segmentFromTakesButton);

	//..End Segment From Takes Button

    //Begin Use Parent Scale Button...

    AddRegion( "UseParentScaleButton", "UseParentScaleButton",
        cUiMargin, kFBAttachRight, "SegmentFromTakesButton", 1.0,
        10, kFBAttachBottom, "ModelPairSheet", 1.0,
        200, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "UseParentScaleButton", m_useParentScaleButton);

    //..End Use Parent Scale Button

	//Begin Hide Warnings Button...

	AddRegion( "HideWarningsButton", "HideWarningsButton",
		cUiMargin + 400, kFBAttachNone, "HideWarningsButton", 1.0,
		10, kFBAttachBottom, "ModelPairSheet", 1.0,
		200, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "HideWarningsButton", m_HideWarnings);

	//..End Hide Warnings Button

	//Begin Hide Warnings Button...

	AddRegion( "UseTakeOutputPaths", "UseTakeOutputPaths",
		cUiMargin + 575, kFBAttachNone, "UseTakeOutputPaths", 1.0,
		10, kFBAttachBottom, "ModelPairSheet", 1.0,
		200, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "UseTakeOutputPaths", m_useTakeOutputPathsButton);

	//..End Hide Warnings Button

	iAttachTopY += (cUiRowHeight * 2);

	//Layouts
	AddRegion( "Layout", "Layout",
		cUiAttachLeftX, kFBAttachLeft, "", 1.0,
		10, kFBAttachBottom, "UseTakeOutputPaths", 1.0,
		cUiAttachRightX, kFBAttachRight, "", 1.0,
		iAttachBottomHeight, kFBAttachBottom, "", 1.0);
	SetControl("Layout", m_layout[kSegment]);

	//Export Button
	AddRegion( "Export", "Export",
        cUiAttachLeftX, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomHeight + cUiMargin, kFBAttachBottom, "NULL", 1.0,
        cUiButtonWidth + 15, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight + 15, kFBAttachNone, NULL, 1.0 );
    SetControl( "Export", m_exportButton );

	// Build Button
	AddRegion( "Build", "Build",
        cUiAttachLeftX + 30 + cUiButtonWidth, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomHeight + cUiMargin, kFBAttachBottom, "NULL", 1.0,
        cUiButtonWidth + 15, kFBAttachNone, NULL, 1.0,
        cUiFieldHeight + 15, kFBAttachNone, NULL, 1.0 );
    SetControl( "Build", m_buildButton );

	// Preview checkbox
	AddRegion( "Preview", "Preview",
		cUiAttachLeftX + 2 * (30 + cUiButtonWidth), kFBAttachLeft, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin , kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth + 15, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight + 15, kFBAttachNone, NULL, 1.0 );
	SetControl( "Preview", m_previewCheckbox );

	// Preview checkbox
	AddRegion( "UsingCameraDOF", "UsingCameraDOF",
		cUiAttachLeftX + 3 * (30 + cUiButtonWidth), kFBAttachLeft, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin , kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth + 15, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight + 15, kFBAttachNone, NULL, 1.0 );
	SetControl( "UsingCameraDOF", m_usedCameraDOFCheckbox );

	// Init Offset checkbox
	AddRegion( "ApplyInitOffset", "ApplyInitOffset",
		cUiAttachLeftX + 4 * (30 + cUiButtonWidth), kFBAttachLeft, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin , kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth + 30, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight + 15, kFBAttachNone, NULL, 1.0 );
	SetControl( "ApplyInitOffset", m_initOffsetCheckbox );

	// Using NM Blend checkbox
	AddRegion( "UsingNMBlend", "UsingNMBlend",
		cUiAttachLeftX + 5 * (30 + cUiButtonWidth), kFBAttachLeft, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin , kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth + 35, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight + 15, kFBAttachNone, NULL, 1.0 );
	SetControl( "UsingNMBlend", m_usedInNMBlendCheckbox );

	// Story mode checkbox
	AddRegion( "UsingStoryMode", "UsingStoryMode",
		cUiAttachLeftX + 6 * (30 + cUiButtonWidth) + 20, kFBAttachLeft, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin , kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth + 35, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight + 15, kFBAttachNone, NULL, 1.0 );
	SetControl( "UsingStoryMode", m_usedStoryModeCheckbox );

	// Render checkbox
	AddRegion( "RenderMode", "RenderMode",
		cUiAttachLeftX + 7 * (30 + cUiButtonWidth), kFBAttachLeft, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin , kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth + 35, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight + 15, kFBAttachNone, NULL, 1.0 );
	SetControl( "RenderMode", m_renderModeCheckbox );

	AddRegion( "SaveClip", "SaveClip",
		cUiAttachRightX - cUiButtonWidth + 4 * (- cUiMargin - cUiButtonWidth), kFBAttachRight, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin, kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "SaveClip", m_saveClipButton );

	//Load Markup Button
	AddRegion( "LoadClip", "LoadClip",
		cUiAttachRightX - cUiButtonWidth + 3 * (- cUiMargin - cUiButtonWidth), kFBAttachRight, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin, kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LoadClip", m_loadClipButton );

	//Refresh Markup Button
	AddRegion( "RefreshMarkup", "RefreshMarkup",
		cUiAttachRightX - cUiButtonWidth + 2 * (- cUiMargin - cUiButtonWidth), kFBAttachRight, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin, kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "RefreshMarkup", m_refreshMarkupButton );

	//Save Markup Button
	AddRegion( "SaveMarkup", "SaveMarkup",
		cUiAttachRightX - cUiButtonWidth - cUiMargin - cUiButtonWidth, kFBAttachRight, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin, kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "SaveMarkup", m_saveMarkupButton );

	//Load Markup Button
	AddRegion( "LoadMarkup", "LoadMarkup",
		cUiAttachRightX - cUiButtonWidth, kFBAttachRight, "NULL", 1.0,
		iAttachBottomHeight + cUiMargin, kFBAttachBottom, "NULL", 1.0,
		cUiButtonWidth, kFBAttachNone, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "LoadMarkup", m_loadMarkupButton );

	UICreateSegmentLayout();
	UICreateTakeLayout();
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UICreateSegmentLayout()
{
	 int iAttachTopY = cUiMargin;

	//Begin Segment Spreadsheet
	m_layout[kSegment].AddRegion( "SegmentSheet", "SegmentSheet",
				0, kFBAttachLeft, "", 1.0,
				0, kFBAttachTop, "", 1.0,
				cUiAttachRightX - (cUiButtonWidth + 20) - cUiMargin, kFBAttachRight, "", 1.0,
				-cUiMargin, kFBAttachBottom, "", 1.0 );
    m_layout[kSegment].SetControl( "SegmentSheet", m_segmentSheet );
	
	//...End Segment Spreadsheet

	//Add segment
    m_layout[kSegment].AddRegion( "AddSegment", "AddSegment",
				cUiAttachRightX - (cUiButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
				iAttachTopY, kFBAttachTop, "NULL", 1.0,
				cUiAttachRightX, kFBAttachRight, NULL, 1.0,
				cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_layout[kSegment].SetControl( "AddSegment", m_addSegmentButton );
	
	iAttachTopY += cUiRowHeight;

	//Remove segment
	m_layout[kSegment].AddRegion( "RemoveSegment", "RemoveSegment",
				cUiAttachRightX - (cUiButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
				iAttachTopY, kFBAttachTop, "NULL", 1.0,
				cUiAttachRightX, kFBAttachRight, NULL, 1.0,
				cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_layout[kSegment].SetControl( "RemoveSegment", m_removeSegmentButton );

	iAttachTopY += cUiRowHeight;

	//Clear segments
	m_layout[kSegment].AddRegion( "ClearSegments", "ClearSegments",
				cUiAttachRightX - (cUiButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
				iAttachTopY, kFBAttachTop, "NULL", 1.0,
				cUiAttachRightX, kFBAttachRight, NULL, 1.0,
				cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	 m_layout[kSegment].SetControl( "ClearSegments", m_clearSegmentsButton );
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UICreateTakeLayout()
{
	 int iAttachTopY = cUiMargin;

	//Begin Takes Spreadsheet...
	m_layout[kTake].AddRegion( "TakeSheet", "TakeSheet",
		0, kFBAttachLeft, "", 1.0,
		0, kFBAttachTop, "", 1.0,
		cUiAttachRightX - (cUiButtonWidth + 20 + 65) - cUiMargin, kFBAttachRight, "", 1.0,
		-cUiMargin, kFBAttachBottom, "", 1.0 );
	m_layout[kTake].SetControl( "TakeSheet", m_takeSheet );
	//..End Takes Spreadsheet

	//Refresh Takes
	m_layout[kTake].AddRegion( "RefreshTakes", "RefreshTakes",
		cUiAttachRightX - (cUiButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiAttachRightX, kFBAttachRight, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_layout[kTake].SetControl( "RefreshTakes", m_refreshTakesButton);

	iAttachTopY += cUiRowHeight;

	//Enable All Takes
	m_layout[kTake].AddRegion( "EnableAllTakes", "EnableAllTakes",
		cUiAttachRightX - (cUiButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		cUiAttachRightX, kFBAttachRight, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_layout[kTake].SetControl( "EnableAllTakes", m_enableAllTakesButton );

	iAttachTopY += cUiRowHeight;

	//Disable All Takes
	m_layout[kTake].AddRegion( "DisableAllTakes", "DisableAllTakes",
		cUiAttachRightX - (cUiButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 2.0,
		cUiAttachRightX, kFBAttachRight, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_layout[kTake].SetControl( "DisableAllTakes", m_disableAllTakesButton );

	iAttachTopY += cUiRowHeight;

	//Disable All Takes
	m_layout[kTake].AddRegion( "OpenExportFolder", "OpenExportFolder",
		cUiAttachRightX - (cUiButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 2.0,
		cUiAttachRightX, kFBAttachRight, NULL, 1.0,
		cUiFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_layout[kTake].SetControl( "OpenExportFolder", m_openExportFolderButton );
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIConfigure()
{
	// Version
	char cBuffer[32];
	sprintf(cBuffer,"Version: %i.%i.%i", s_ingame_exporter_version.Major, s_ingame_exporter_version.Minor, s_ingame_exporter_version.Revision);
	m_versionLabel.Caption = cBuffer;
	m_versionLabel.Justify = kFBTextJustifyRight;

	//Perforce
	m_usePerforceIntegration.Width = 100;
	m_usePerforceIntegration.Caption = "PERFORCE";
	m_usePerforceIntegration.Style = kFBCheckbox;
	m_usePerforceIntegration.State = 1;
	m_usePerforceIntegration.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::UsePerforceIntegrationButton_Click );

    // Help
    m_helpButton.Caption = "Help";
    m_helpButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::HelpButton_Click );

	//Output Path
	m_outputPathLabel.Caption = "Default Output Path";
    m_outputPathLabel.Justify = kFBTextJustifyRight;

	m_outputPathButton.Caption = "...";
    m_outputPathButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::OuputPathPicker_Click );
	m_outputPathEdit.OnChange.Add( this, (FBCallback)&RexRageAnimExportTool::OutputPathEdit_Change );

	//Control File Path
	m_ctrlFilePathLabel.Caption = "Default Control File";
	m_ctrlFilePathLabel.Justify = kFBTextJustifyRight;

	m_ctrlFilePathButton.Caption = "...";
	m_ctrlFilePathButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::ControlFilePathPicker_Click );

	m_ctrlFilePathEdit.OnChange.Add( this, (FBCallback)&RexRageAnimExportTool::ControlFilePathEdit_Change );

	// Default Compression
	m_defaultCompressionLabel.Caption = "Default Compression";
	m_defaultCompressionLabel.Justify = kFBTextJustifyRight;

	m_defaultCompressionButton.Caption = "...";
	m_defaultCompressionButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::DefaultCompressionPathPicker_Click);

	m_defaultCompressionSetButton.Caption = "Set";
	m_defaultCompressionSetButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::DefaultCompressionSet_Click);

	//New Origin Node
	m_originObjectLabel.Caption = "Origin Object";
	m_originObjectLabel.Justify = kFBTextJustifyRight;

	m_originObjectButton.Caption = "Select Object";
	m_originObjectButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::OriginObject_Click );
	m_originObjectEdit.ReadOnly=true;
	m_clearOriginObjectButton.Caption = "Clear";
	m_clearOriginObjectButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::ClearOriginObject_Click );

	//Attribute File Path
	m_attributeFilePathLabel.Caption = "Facial Attr File";
	m_attributeFilePathLabel.Justify = kFBTextJustifyRight;

	m_attributeFilePathButton.Caption = "...";
	m_attributeFilePathButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::AttributeFilePathPicker_Click );
	
	//Model Pair Sheet
	m_modelPairSheet.Caption = "Index";
	m_modelPairSheet.MultiSelect = false;
	m_modelPairSheet.OnRowClick.Add( this, (FBCallback)&RexRageAnimExportTool::ModelPairSheet_RowClick );
	m_modelPairSheet.OnCellChange.Add( this, (FBCallback)&RexRageAnimExportTool::ModelPairSheet_CellChange );
	
	//Add Root Button
	m_addRootButton.Caption = "Add Root";
	m_addRootButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::AddRootButton_Click );

	//Remove Root Button
	m_removeRootButton.Caption = "Remove Root";
	m_removeRootButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::RemoveRootButton_Click );

	//Add Mover Button
	m_addMoverButton.Caption = "Add Mover";
	m_addMoverButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::AddMoverButton_Click );

	//Remove Mover Button
	m_removeMoverButton.Caption = "Remove Mover";
	m_removeMoverButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::RemoveMoverButton_Click );

	m_addAdditionalButton.Caption = "Add Model(s)";
	m_addAdditionalButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::AddAdditionalModelsButton_Click );

	//Segment From Takes Button
	m_segmentFromTakesButton.Caption = "Segment From Takes";
	m_segmentFromTakesButton.Style = kFB2States;
	m_segmentFromTakesButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::SegmentFromTakesButton_Click );

    //Use Parent Scale Button
    m_useParentScaleButton.Caption = "Use Parent Scale";
    m_useParentScaleButton.Style = kFB2States;

	//Use Parent Scale Button
	m_HideWarnings.Caption = "Hide Warnings";
	m_HideWarnings.Style = kFB2States;
	m_HideWarnings.State = kFBButtonState1;

	m_useTakeOutputPathsButton.Caption = "Use Take Output Path";
	m_useTakeOutputPathsButton.Style = kFB2States;
	
	//Configure the segment based and take based layouts
	UIConfigureSegmentLayout();
	UIConfigureTakeLayout();
	
	//Export Button
	m_exportButton.Caption = "Export";
	m_exportButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::ExportButton_Click );

	//Build Button
	m_buildButton.Caption = "Build";
	m_buildButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::BuildButton_Click );

	// Preview checkbox
	m_previewCheckbox.Caption = "Preview";
	m_previewCheckbox.Style = kFBCheckbox;

	m_usedCameraDOFCheckbox.Caption = "Enable DOF";
	m_usedCameraDOFCheckbox.Style = kFBCheckbox;

	m_usedStoryModeCheckbox.Caption = "Story Mode";
	m_usedStoryModeCheckbox.Style = kFBCheckbox;

	m_renderModeCheckbox.Caption = "Render";
	m_renderModeCheckbox.Style = kFBCheckbox;

	// Init Offset checkbox
	m_initOffsetCheckbox.Caption = "Use Init Offset";
	m_initOffsetCheckbox.Style = kFBCheckbox;
	m_initOffsetCheckbox.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::InitOffsetCheckbox_Click );

	// NM Blend checkbox
	m_usedInNMBlendCheckbox.Caption = "Used In NM Blend";
	m_usedInNMBlendCheckbox.Style = kFBCheckbox;
	m_usedInNMBlendCheckbox.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::UsedInNBBlendCheckbox_Click );

	m_saveClipButton.Caption = "Save Clips";
	m_saveClipButton.OnClick.Add( this, (FBCallback) &RexRageAnimExportTool::SaveClipButton_Click );

	m_loadClipButton.Caption = "Load Clips";
	m_loadClipButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::LoadClipButton_Click );

	//Save Markup Button
	m_saveMarkupButton.Caption = "Save Markup";
	m_saveMarkupButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::SaveMarkupButton_Click);

	m_refreshMarkupButton.Caption = "Clear Markup";
	m_refreshMarkupButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::RefreshMarkupButton_Click);

	//Load Markup Button
	m_loadMarkupButton.Caption = "Load Markup";
	m_loadMarkupButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::LoadMarkupButton_Click);
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIConfigureSegmentLayout()
{
	//Segmentation Spreadsheet
	m_segmentSheet.Caption = "Index";
	m_segmentSheet.MultiSelect = false;
	m_segmentSheet.OnRowClick.Add( this, (FBCallback)&RexRageAnimExportTool::SegmentSheet_RowClick );
	m_segmentSheet.OnCellChange.Add( this, (FBCallback)&RexRageAnimExportTool::SegmentSheet_CellChange );
	
	//Add Segment Button
	m_addSegmentButton.Caption = "Add Segment";
	m_addSegmentButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::AddSegmentButton_Click );

	//Remove Segment Button
	m_removeSegmentButton.Caption = "Remove Segment";
	m_removeSegmentButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::RemoveSegmentButton_Click );
	
	//Clear Segments Button
	m_clearSegmentsButton.Caption = "Clear Segments";
	m_clearSegmentsButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::ClearSegmentsButton_Click );
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIConfigureTakeLayout()
{
	//Takes Spreadsheet
	m_takeSheet.Caption = "Index";
	m_takeSheet.MultiSelect = false;
	m_takeSheet.OnRowClick.Add( this, (FBCallback)&RexRageAnimExportTool::TakeSheet_RowClick );
	m_takeSheet.OnCellChange.Add( this, (FBCallback)&RexRageAnimExportTool::TakeSheet_CellChange);

	//Refresh Takes Button
	m_refreshTakesButton.Caption = "Refresh Takes";
	m_refreshTakesButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::RefreshTakesButton_Click );

	//Enable All Takes Button
	m_enableAllTakesButton.Caption = "Enable All";
	m_enableAllTakesButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::EnableAllTakesButton_Click );

	//Disable All Takes Button
	m_disableAllTakesButton.Caption = "Disable All";
	m_disableAllTakesButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::DisableAllTakesButton_Click );

	//Open Export Folder
	m_openExportFolderButton.Caption = "Open Export Dir";
	m_openExportFolderButton.OnClick.Add( this, (FBCallback)&RexRageAnimExportTool::OpenExportFolder_Click );
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIRestoreDefaults()
{
	//Clear out the dialog fields...
#if HACK_MP3
		m_outputPathEdit.Text = "X:\\payne\\payne_art\\anim";
#else	
		m_outputPathEdit.Text = "";
#endif

#if HACK_MP3
		m_ctrlFilePathEdit.Text = "X:\\tools\\dcc\\current\\motionbuilder\\config\\control_templates";
#else 
		m_ctrlFilePathEdit.Text = "";
#endif

	//Reset the model pair sheet to its default clear state
	UIResetModelPairSpreadsheet();

	//Reset the segment spread sheet to its default cleared state
	UIResetSegmentSpreadsheet();

	//Reset the take spread sheet to its default cleard state
	UIResetTakeSpreadsheet();

	//Refreshes the list of specific files used in the exporter (skeleton, control, compression).
	UIRefreshTemplateFiles();
}

//-----------------------------------------------------------------------------
void RexRageAnimExportTool::UIRefreshTemplateFiles()
{
	// First, instantiate all internal variables, defaults.
	// Populate compression, control and skeleton file strings to provide combo boxes for each take
	ms_ctrlPathTemplates.Reset();
	ms_compressionPathTemplates.Reset();
	ms_skeletonFiles.Reset();

	ms_ctrlPathTemplates.PushAndGrow( atString(m_DefaultSpecFile) );
	ms_skeletonFiles.PushAndGrow( atString(DefaultSkelFile) );

	rexMBAnimExportCommon::FindAnimCtrlFilePathFiles( ms_ctrlPathTemplates );
	rexMBAnimExportCommon::FindAnimCompressionFilePathFiles( ms_compressionPathTemplates );
	rexMBAnimExportCommon::FindSkelFilePathFiles( ms_skeletonFiles );

};
//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIPopulate(RexAnimMarkupData* pAnimData)
{
	//Reset the entire interface.
	UIRestoreDefaults();

	//Set the output path
	m_outputPathEdit.Text = const_cast<char*>(pAnimData->m_outputPath.c_str());

	m_originObjectEdit.Text = const_cast<char*>(pAnimData->m_originObjectName.c_str());

	m_attributeFilePathEdit.Text = const_cast<char*>(pAnimData->m_attributeFilePath.c_str());

	//Set the init offset flag
	m_initOffsetCheckbox.State = pAnimData->m_bUseInitOffset;

	// Set the used for nm blend flag
	m_usedInNMBlendCheckbox.State = pAnimData->m_bUsedInNMBlend;

	m_usedStoryModeCheckbox.State = pAnimData->m_bStoryMode;

	m_usedCameraDOFCheckbox.State = pAnimData->m_bEnableCameraDOF;

	m_renderModeCheckbox.State = pAnimData->m_bRenderMode;

	//Set the control file path
#if HACK_GTA4
	atArray<atString> split;
	atString ctrlFilePath = atString(pAnimData->m_ctrlFilePath.c_str());
	ctrlFilePath.Split(split, "\\");
	if( split.GetCount() >= 1 )
		m_ctrlFilePathEdit.Text = const_cast<char*>(split[split.GetCount()-1].c_str());
	else
		m_ctrlFilePathEdit.Text = "";
#else
	m_ctrlFilePathEdit.Text = const_cast<char*>(pAnimData->m_ctrlFilePath.c_str());
#endif // HACK_GTA4

	int nCharacters = pAnimData->GetNumCharacters();
	for( int charIndex = 0; charIndex < nCharacters; charIndex++)
	{
		RexCharAnimData* pCharData = pAnimData->GetCharacter(charIndex);
		char* szModelName = const_cast<char*>(pCharData->GetInputModelName(0));
		char* szMoverName = const_cast<char*>(pCharData->GetMoverModelName(0));
		char* szAnimPrefix = const_cast<char*>(pCharData->GetAnimPrefix());
		char* szAnimSuffix = const_cast<char*>(pCharData->GetAnimSuffix());
		char* szSkeletonFile = const_cast<char*>(pCharData->GetSkeletonFile());
		char* szCharCtrlFile = const_cast<char*>(pCharData->GetCtrlFile());
		char* szModelType = const_cast<char*>(pCharData->GetModelType());
		char* szFacialAttribute = const_cast<char*>(pCharData->GetFacialAttribute());
		char* szAltOutputPath = const_cast<char*>(pCharData->GetAltOutputPath());
		bool bExportModel = pCharData->IsMarkedForExport();
		bool bMergeFace = pCharData->IsMarkedForFaceMerge();

		if(strlen(szSkeletonFile) == 0)
		{
			atString skelFile("");
			rexMBAnimExportCommon::GetDefaultSkeletonFile(skelFile);
			szSkeletonFile = const_cast<char*>(skelFile.c_str());			
		}
		
		//Create the model/mover pair data store for the newly added model
		AetRootMoverPair* pModelPair = new AetRootMoverPair();
		pModelPair->m_rootModel = szModelName;
		pModelPair->m_moverModel = szMoverName;
		pModelPair->m_animPrefix = szAnimPrefix;
		pModelPair->m_animSuffix = szAnimSuffix;
		pModelPair->m_skeletonFile = szSkeletonFile;
		pModelPair->m_modelType = szModelType;
		pModelPair->m_facialAttribute = szFacialAttribute;
		pModelPair->m_charCtrlFile = szCharCtrlFile;
		pModelPair->m_altOutputPath = szAltOutputPath;
		pModelPair->m_modelExport = bExportModel;
		pModelPair->m_mergeFace = bMergeFace;

		//Create the additional model data store for the newly added model
		for(int i=0; i < pCharData->m_additionalModels.GetCount(); ++i)
		{
			AetAdditionalModel* pAdditionalModel = new AetAdditionalModel();
			pAdditionalModel->m_modelName = pCharData->m_additionalModels[i]->m_modelName;
			pAdditionalModel->m_trackType = pCharData->m_additionalModels[i]->m_trackType;
			pAdditionalModel->m_attributeName = pCharData->m_additionalModels[i]->m_attributeName;

			pModelPair->m_additionalModels.PushAndGrow(pAdditionalModel);
		}

		AddModelPairRow(pModelPair);
	}

	UISetModelPairSheetIndices();


	if(pAnimData->m_segmentFromTakes)
	{
		m_segmentFromTakesButton.State = kFBButtonState1;
		SetControl("Layout", m_layout[kTake]);
	}
	else
	{
		m_segmentFromTakesButton.State = kFBButtonState0;
		SetControl("Layout", m_layout[kSegment]);
	}

	if(pAnimData->m_segmentFromTakes)
	{
		UIPopulateTakeSheet(pAnimData);
	}
	else
	{
		//Populate the segment spread sheet
		int nSegs = pAnimData->GetNumSegments();
		for(int segIdx=0; segIdx < nSegs; segIdx++)
		{
			RexMbAnimSegmentData* pFileSegmentData = pAnimData->GetSegment(segIdx);
			Assert(pFileSegmentData);

			m_segmentSheet.RowAdd( "", m_iSegSpreadRowId );
			m_segmentSheet.SetCell( m_iSegSpreadRowId, kSegmentName, const_cast<char*>(pFileSegmentData->GetSegmentName()) );
			m_segmentSheet.SetCell( m_iSegSpreadRowId, kStartFrame, pFileSegmentData->GetStartAsFrame());
			m_segmentSheet.SetCell( m_iSegSpreadRowId, kEndFrame, pFileSegmentData->GetEndAsFrame());
#if !HACK_GTA4
			m_segmentSheet.SetCell( m_iSegSpreadRowId, kLooping, pFileSegmentData->IsLooping());
#endif // HACK_GTA4
			m_segmentSheet.SetCell( m_iSegSpreadRowId, kAutoPlay, pFileSegmentData->IsAutoPlay());

			//Make a copy of the segment data and store it in the segment spreadsheet map
			RexMbAnimSegmentData* pSpreadSegmentData = new RexMbAnimSegmentData(*pFileSegmentData);
			m_rowIdToAnimSegMap[m_iSegSpreadRowId] = pSpreadSegmentData;
			m_iSegSpreadRowId++;
		}

		//Update the segment indices column 
		UISetSegSheetIndices();
	}

	m_useParentScaleButton.State = pAnimData->m_useParentScale ? kFBButtonState1 : kFBButtonState0;

	// Set the use take output path flag
	m_useTakeOutputPathsButton.State = pAnimData->m_bUseTakeOutputPaths ? kFBButtonState1 : kFBButtonState0;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIPopulateTakeSheet(RexAnimMarkupData* pAnimData)
{
	atString ctrlPathsStr("");
	atString compressionPathsStr("");

	rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( ms_ctrlPathTemplates, ctrlPathsStr );
	rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( ms_compressionPathTemplates, compressionPathsStr );
	
	//When "Takes" are being used to define the output animation segmentation, it is assumed
	//that there is one segment setup for each take.
	int nSegs = pAnimData->GetNumSegments();
	int nNumTakes = m_system.Scene->Takes.GetCount();
	for(int takeIndex = 0; takeIndex < nNumTakes; ++takeIndex)
	{
		HFBTake take = (HFBTake) m_system.Scene->Takes.GetAt(takeIndex);
		for(int segIdx=0; segIdx < nSegs; segIdx++)
		{
			RexMbAnimSegmentData* pFileSegmentData = pAnimData->GetSegment(segIdx);
			Assert(pFileSegmentData);

			if(strcmp(pFileSegmentData->GetTakeName(), take->Name.AsString()) != 0)
				continue;

			atString takeAltOutputPathsStr("");
			rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( pFileSegmentData->m_altOutputPaths, takeAltOutputPathsStr ); 

			m_takeSheet.RowAdd( "", m_iTakeSpreadRowId );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeName, const_cast<char*>(pFileSegmentData->GetTakeName()) );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeExport, pFileSegmentData->IsMarkedForExport() );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAdditive, pFileSegmentData->IsAdditive() );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAdditiveBA, pFileSegmentData->IsAdditiveBA() );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeLinearCompression, pFileSegmentData->HasLinearCompression() );

			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeCtrlFile, const_cast<char*>( ctrlPathsStr.c_str() ) );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeCompressionFile, const_cast<char*>( compressionPathsStr.c_str() ) );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAltOutputPaths, const_cast<char*>( takeAltOutputPathsStr.c_str() ) );
			// Set alt output paths index to be 0 no matter what is in it (not important)
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAltOutputPaths, 0 );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAddOutputPath, "Add" );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeRemoveOutputPath, "Remove" );

			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAdditiveBaseAnimName, const_cast<char*>(pFileSegmentData->GetAdditiveBaseAnimation()) );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAddAdditiveName, "Add" );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeRemoveAdditiveName, "Remove" );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeNameOverride, const_cast<char*>(pFileSegmentData->GetTakeNameOverride()) );
			const char*	celVal = NULL;
			m_takeSheet.GetCell( m_iTakeSpreadRowId, kTakeCtrlFile, celVal );
			atString ctrlFileStr = atString( pFileSegmentData->m_ctrlFilePath.c_str() );
			atString ctrlComboStr = atString( celVal );
			int ctrlFileIdx = rexMBAnimExportCommon::ResolveComboIndexFromString( ctrlFileStr, ctrlComboStr );
			if ( ctrlFileIdx == -1 ) ctrlFileIdx = 0;
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeCtrlFile, ctrlFileIdx );

			m_takeSheet.GetCell( m_iTakeSpreadRowId, kTakeCompressionFile, celVal );
			atString compressFileStr = atString( pFileSegmentData->m_compressionFilePath.c_str() );
			atString compressComboStr = atString( celVal );
			int compressFileIdx = rexMBAnimExportCommon::ResolveComboIndexFromString( compressFileStr, compressComboStr );
			if ( compressFileIdx == -1 ) compressFileIdx = 0;
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeCompressionFile, compressFileIdx );

#if !HACK_GTA4
			m_takeSheet.GetCell( m_iTakeSpreadRowId, kTakeLooping, celVal );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeLooping, pFileSegmentData->IsLooping() );
#endif // !HACK_GTA4

			m_takeSheet.GetCell( m_iTakeSpreadRowId, kTakeAdditive, celVal );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAdditive, pFileSegmentData->IsAdditive() );

			m_takeSheet.GetCell( m_iTakeSpreadRowId, kTakeAdditiveBA, celVal );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAdditiveBA, pFileSegmentData->IsAdditiveBA() );

			m_takeSheet.GetCell( m_iTakeSpreadRowId, kTakeLinearCompression, celVal );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeLinearCompression, pFileSegmentData->HasLinearCompression() );

			//Make a copy of the segment data and store it in the take spreadsheet map
			RexMbAnimSegmentData* pSpreadTakeData = new RexMbAnimSegmentData(*pFileSegmentData);
			m_rowIdToTakeMap[m_iTakeSpreadRowId] = pSpreadTakeData;
			m_iTakeSpreadRowId++;
		}
	}

	//Add all takes in the scene missing in the spreadsheet to the spreadsheet.
	AddMissingTakesToSpreadsheet(ctrlPathsStr, compressionPathsStr);		

	//Update the take indices column
	UISetTakeSheetIndices();
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIResetModelPairSpreadsheet()
{
	for(ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.begin();
		it != m_rowIdToModelPairMap.end();
		++it)
	{
		delete it->second;
	}
	m_rowIdToModelPairMap.clear();

	//Remove all the items in the spread sheet
	m_modelPairSheet.Clear();
	
	//Restore the column names and sizing
	m_modelPairSheet.GetColumn( kModelIndex ).Width = 60;

	m_modelPairSheet.ColumnAdd( "Root Model Name" );
	m_modelPairSheet.GetColumn( kRootModelName ).Justify = kFBTextJustifyLeft;
	m_modelPairSheet.GetColumn( kRootModelName ).ReadOnly = true;
	m_modelPairSheet.GetColumn( kRootModelName ).Style = kFBCellStyleString;
	m_modelPairSheet.GetColumn( kRootModelName ).Width = 155;

	m_modelPairSheet.ColumnAdd( "Mover Model Name" );
	m_modelPairSheet.GetColumn( kMoverModelName ).Justify = kFBTextJustifyLeft;
	m_modelPairSheet.GetColumn( kMoverModelName ).ReadOnly = true;
	m_modelPairSheet.GetColumn( kMoverModelName ).Style = kFBCellStyleString;
	m_modelPairSheet.GetColumn( kMoverModelName ).Width = 155;

	m_modelPairSheet.ColumnAdd( "Anim Prefix" );
	m_modelPairSheet.GetColumn( kAnimNamePrefix ).Justify = kFBTextJustifyLeft;
	m_modelPairSheet.GetColumn( kAnimNamePrefix ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kAnimNamePrefix ).Style = kFBCellStyleString;
	m_modelPairSheet.GetColumn( kAnimNamePrefix ).Width = 65;

	m_modelPairSheet.ColumnAdd( "Anim Suffix" );
	m_modelPairSheet.GetColumn( kAnimNameSuffix ).Justify = kFBTextJustifyLeft;
	m_modelPairSheet.GetColumn( kAnimNameSuffix ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kAnimNameSuffix ).Style = kFBCellStyleString;
	m_modelPairSheet.GetColumn( kAnimNameSuffix ).Width = 65;

	m_modelPairSheet.ColumnAdd( "Ctrl File" );
	m_modelPairSheet.GetColumn( kCharCtrlFile ).Justify = kFBTextJustifyLeft;
	m_modelPairSheet.GetColumn( kCharCtrlFile ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kCharCtrlFile ).Style = kFBCellStyleMenu;
	m_modelPairSheet.GetColumn( kCharCtrlFile ).Width = 100;

	m_modelPairSheet.ColumnAdd( "Model Type" );
	m_modelPairSheet.GetColumn( kModelType ).Justify = kFBTextJustifyLeft;
	m_modelPairSheet.GetColumn( kModelType ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kModelType ).Style = kFBCellStyleMenu;
	m_modelPairSheet.GetColumn( kModelType ).Width = 100;

	m_modelPairSheet.ColumnAdd( "Skeleton" );
	m_modelPairSheet.GetColumn( kSkeletonFile ).Justify = kFBTextJustifyLeft;
	m_modelPairSheet.GetColumn( kSkeletonFile ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kSkeletonFile ).Style = kFBCellStyleMenu;
	m_modelPairSheet.GetColumn( kSkeletonFile ).Width = 100;

	m_modelPairSheet.ColumnAdd( "Facial Attribute" );
	m_modelPairSheet.GetColumn( kFacialAttribute ).Justify = kFBTextJustifyRight;
	m_modelPairSheet.GetColumn( kFacialAttribute ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kFacialAttribute ).Style = kFBCellStyleButton;
	m_modelPairSheet.GetColumn( kFacialAttribute ).Width = 200;

	m_modelPairSheet.ColumnAdd( "Export" );
	m_modelPairSheet.GetColumn( kModelExport ).Justify = kFBTextJustifyCenter;
	m_modelPairSheet.GetColumn( kModelExport ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kModelExport ).Style = kFBCellStyle2StatesButton;
	m_modelPairSheet.GetColumn( kModelExport ).Width = 50;

	m_modelPairSheet.ColumnAdd( "Output Path" );
	m_modelPairSheet.GetColumn( kAltOutputPath ).Justify = kFBTextJustifyLeft;
	m_modelPairSheet.GetColumn( kAltOutputPath ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kAltOutputPath ).Style = kFBCellStyleButton;
	m_modelPairSheet.GetColumn( kAltOutputPath ).Width = 320;

	m_modelPairSheet.ColumnAdd( "Merge Face" );
	m_modelPairSheet.GetColumn( kMergeFaceFile ).Justify = kFBTextJustifyCenter;
	m_modelPairSheet.GetColumn( kMergeFaceFile ).ReadOnly = false;
	m_modelPairSheet.GetColumn( kMergeFaceFile ).Style = kFBCellStyle2StatesButton;
	m_modelPairSheet.GetColumn( kMergeFaceFile ).Width = 50;

	//Reset the row id counter
	m_iModelPairSpreadRowId = 0; 

	//Reset the selected row
	m_iModelPairSheetSelRowIndex = -1;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIResetSegmentSpreadsheet()
{
	//Clear out all the backend segment data store attached to the segment spread sheet
	for(ROWID_TO_SEG_MAP::iterator it = m_rowIdToAnimSegMap.begin();
		it != m_rowIdToAnimSegMap.end();
		++it)
	{
		delete it->second;
	}
	m_rowIdToAnimSegMap.clear();

	//Remove all the items in the spread sheet
    m_segmentSheet.Clear();

	//Restore the column names and sizing
    m_segmentSheet.GetColumn( kSegmentIndex ).Width = 60;
	
	m_segmentSheet.ColumnAdd( "Segment Name" );
    m_segmentSheet.GetColumn( kSegmentName ).Justify = kFBTextJustifyLeft;
    m_segmentSheet.GetColumn( kSegmentName ).ReadOnly = false;
    m_segmentSheet.GetColumn( kSegmentName ).Style = kFBCellStyleString;
    m_segmentSheet.GetColumn( kSegmentName ).Width = 275;

    m_segmentSheet.ColumnAdd( "Start Frame" );
    m_segmentSheet.GetColumn( kStartFrame ).Justify = kFBTextJustifyRight;
    m_segmentSheet.GetColumn( kStartFrame ).ReadOnly = false;
    m_segmentSheet.GetColumn( kStartFrame ).Style = kFBCellStyleInteger;
    m_segmentSheet.GetColumn( kStartFrame ).Width = 80;

    m_segmentSheet.ColumnAdd( "End Frame" );
    m_segmentSheet.GetColumn( kEndFrame ).Justify = kFBTextJustifyRight;
    m_segmentSheet.GetColumn( kEndFrame ).ReadOnly = false;
    m_segmentSheet.GetColumn( kEndFrame ).Style = kFBCellStyleInteger;
    m_segmentSheet.GetColumn( kEndFrame ).Width = 80;

#if !HACK_GTA4
    m_segmentSheet.ColumnAdd( "Looping" );
    m_segmentSheet.GetColumn( kLooping ).ReadOnly = false;
    m_segmentSheet.GetColumn( kLooping ).Style = kFBCellStyle2StatesButton;
    m_segmentSheet.GetColumn( kLooping ).Width = 60;
#endif // HACK_GTA4

    m_segmentSheet.ColumnAdd( "Auto-Play" );
    m_segmentSheet.GetColumn( kAutoPlay ).ReadOnly = false;
    m_segmentSheet.GetColumn( kAutoPlay ).Style = kFBCellStyle2StatesButton;
    m_segmentSheet.GetColumn( kAutoPlay ).Width = 60;

	//Reset the row id counter
	m_iSegSpreadRowId = 0;

	//Reset the selected row
	m_iSegSheetSelRowIndex = -1;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UIResetTakeSpreadsheet()
{
	//Clear out all the backend take data store attached to the take spread sheet
	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
		it != m_rowIdToTakeMap.end();
		++it)
	{
		delete it->second;
	}
	m_rowIdToTakeMap.clear();

	//Remove all the items in the spread sheet
    m_takeSheet.Clear();

	//Restore the column names and sizing
    m_takeSheet.GetColumn( kTakeIndex ).Width = 60;
	
	m_takeSheet.ColumnAdd( "Take Name" );
    m_takeSheet.GetColumn( kTakeName ).Justify = kFBTextJustifyLeft;
    m_takeSheet.GetColumn( kTakeName ).ReadOnly = true;
    m_takeSheet.GetColumn( kTakeName ).Style = kFBCellStyleString;
    m_takeSheet.GetColumn( kTakeName ).Width = 250;

	m_takeSheet.ColumnAdd( "Export" );
    m_takeSheet.GetColumn( kTakeExport ).ReadOnly = false;
    m_takeSheet.GetColumn( kTakeExport ).Style = kFBCellStyle2StatesButton;
    m_takeSheet.GetColumn( kTakeExport ).Width = 50;
	m_takeSheet.GetColumn( kTakeExport ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Additive FF" );
    m_takeSheet.GetColumn( kTakeAdditive ).ReadOnly = false;
    m_takeSheet.GetColumn( kTakeAdditive ).Style = kFBCellStyle2StatesButton;
    m_takeSheet.GetColumn( kTakeAdditive ).Width = 65;
	m_takeSheet.GetColumn( kTakeAdditive ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Additive BA" );
	m_takeSheet.GetColumn( kTakeAdditiveBA ).ReadOnly = false;
	m_takeSheet.GetColumn( kTakeAdditiveBA ).Style = kFBCellStyle2StatesButton;
	m_takeSheet.GetColumn( kTakeAdditiveBA ).Width = 65;
	m_takeSheet.GetColumn( kTakeAdditiveBA ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Linear C." );
	m_takeSheet.GetColumn( kTakeLinearCompression ).ReadOnly = false;
	m_takeSheet.GetColumn( kTakeLinearCompression ).Style = kFBCellStyle2StatesButton;
	m_takeSheet.GetColumn( kTakeLinearCompression ).Width = 50;
	m_takeSheet.GetColumn( kTakeLinearCompression ).Justify = kFBTextJustifyCenter;

#if !HACK_GTA4
	m_takeSheet.ColumnAdd( "Looping" );
	m_takeSheet.GetColumn( kTakeLooping ).ReadOnly = false;
	m_takeSheet.GetColumn( kTakeLooping ).Style = kFBCellStyle2StatesButton;
	m_takeSheet.GetColumn( kTakeLooping ).Width = 50;
	m_takeSheet.GetColumn( kTakeLooping ).Justify = kFBTextJustifyCenter;
#endif // !HACK_GTA4

	m_takeSheet.ColumnAdd( "Ctrl File" );
    m_takeSheet.GetColumn( kTakeCtrlFile ).ReadOnly = false;
    m_takeSheet.GetColumn( kTakeCtrlFile ).Style = kFBCellStyleMenu;
    m_takeSheet.GetColumn( kTakeCtrlFile ).Width = 100;
	m_takeSheet.GetColumn( kTakeCtrlFile ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Compression" );
    m_takeSheet.GetColumn( kTakeCompressionFile ).ReadOnly = false;
    m_takeSheet.GetColumn( kTakeCompressionFile ).Style = kFBCellStyleMenu;
    m_takeSheet.GetColumn( kTakeCompressionFile ).Width = 100;
	m_takeSheet.GetColumn( kTakeCompressionFile ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Output Paths" );
	m_takeSheet.GetColumn( kTakeAltOutputPaths ).ReadOnly = false;
	m_takeSheet.GetColumn( kTakeAltOutputPaths ).Style = kFBCellStyleMenu;
	m_takeSheet.GetColumn( kTakeAltOutputPaths ).Width = 300;
	m_takeSheet.GetColumn( kTakeAltOutputPaths ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Add" );
	m_takeSheet.GetColumn( kTakeAddOutputPath ).ReadOnly = false;
	m_takeSheet.GetColumn( kTakeAddOutputPath ).Style = kFBCellStyleButton;
	m_takeSheet.GetColumn( kTakeAddOutputPath ).Width = 40;
	m_takeSheet.GetColumn( kTakeAddOutputPath ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Remove" );
	m_takeSheet.GetColumn( kTakeRemoveOutputPath ).ReadOnly = false;
	m_takeSheet.GetColumn( kTakeRemoveOutputPath ).Style = kFBCellStyleButton;
	m_takeSheet.GetColumn( kTakeRemoveOutputPath ).Width = 60;
	m_takeSheet.GetColumn( kTakeRemoveOutputPath ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Name" );
    m_takeSheet.GetColumn( kTakeNameOverride ).Justify = kFBTextJustifyLeft;
	m_takeSheet.GetColumn( kTakeNameOverride ).ReadOnly = false;
    m_takeSheet.GetColumn( kTakeNameOverride ).Style = kFBCellStyleString;
    m_takeSheet.GetColumn( kTakeNameOverride ).Width = 200;

	m_takeSheet.ColumnAdd( "Additive Base Anim" );
	m_takeSheet.GetColumn( kTakeAdditiveBaseAnimName ).Justify = kFBTextJustifyLeft;
	m_takeSheet.GetColumn( kTakeAdditiveBaseAnimName ).ReadOnly = true;
	m_takeSheet.GetColumn( kTakeAdditiveBaseAnimName ).Style = kFBCellStyleString;
	m_takeSheet.GetColumn( kTakeAdditiveBaseAnimName ).Width = 200;

	m_takeSheet.ColumnAdd( "Add" );
	m_takeSheet.GetColumn( kTakeAddAdditiveName ).ReadOnly = false;
	m_takeSheet.GetColumn( kTakeAddAdditiveName ).Style = kFBCellStyleButton;
	m_takeSheet.GetColumn( kTakeAddAdditiveName ).Width = 40;
	m_takeSheet.GetColumn( kTakeAddAdditiveName ).Justify = kFBTextJustifyCenter;

	m_takeSheet.ColumnAdd( "Remove" );
	m_takeSheet.GetColumn( kTakeRemoveAdditiveName ).ReadOnly = false;
	m_takeSheet.GetColumn( kTakeRemoveAdditiveName ).Style = kFBCellStyleButton;
	m_takeSheet.GetColumn( kTakeRemoveAdditiveName ).Width = 60;
	m_takeSheet.GetColumn( kTakeRemoveAdditiveName ).Justify = kFBTextJustifyCenter;

	//Reset the row id counter
	m_iTakeSpreadRowId = 0;

	//Reset the selected row
	m_iTakeSheetSelRowIndex = -1;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UISetModelPairSheetIndices()
{
	char buf[8];
	int index = 0;
	
	for(ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.begin();
		it != m_rowIdToModelPairMap.end();
		++it)
	{
		int rowId = it->first;
		_itoa(index, buf, 10);
		m_modelPairSheet.GetRow(rowId).Caption = FBString(buf);
		index++;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UISetSegSheetIndices()
{
	char buf[8];
	int index = 0;
	
	for(ROWID_TO_SEG_MAP::iterator it = m_rowIdToAnimSegMap.begin();
		it != m_rowIdToAnimSegMap.end();
		++it)
	{
		int rowId = it->first;
		_itoa(index, buf, 10);
		m_segmentSheet.GetRow(rowId).Caption = FBString(buf);
		index++;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UISetTakeSheetIndices()
{
	char buf[8];
	int index = 0;
	
	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
		it != m_rowIdToTakeMap.end();
		++it)
	{
		int rowId = it->first;
		_itoa(index, buf, 10);
		m_takeSheet.GetRow(rowId).Caption = FBString(buf);
		index++;
	}
}

//-----------------------------------------------------------------------------

RexAnimMarkupData* RexRageAnimExportTool::CollectAnimExportData(bool bSkipExportCheck)
{
	//Collect the non-segment data from the dialog form...
	string outputPath = m_outputPathEdit.Text.AsString();
	string ctrlFilePath = m_ctrlFilePathEdit.Text.AsString();
	string originObjectName = m_originObjectEdit.Text.AsString();
	string attributeFilePath = m_attributeFilePathEdit.Text.AsString();
		
	//Create and fill out the structure we'll pass to the animation exporter
	RexAnimMarkupData* pAnimData = new RexAnimMarkupData();

	pAnimData->m_outputPath = outputPath.c_str();
	pAnimData->m_ctrlFilePath = ctrlFilePath.c_str();
	pAnimData->m_originObjectName = originObjectName.c_str();
	pAnimData->m_attributeFilePath = attributeFilePath.c_str();
	pAnimData->m_project = rexMBAnimExportCommon::GetProjectName();

	rexMBAnimExportCommon::SetNewOrigin((m_initOffsetCheckbox.State != kFBButtonState0), pAnimData->m_originObjectName);
	
	for(ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.begin();
		it != m_rowIdToModelPairMap.end();
		++it)
	{
		AetRootMoverPair* pModelPair = it->second;

		if(pModelPair->m_modelExport == false && bSkipExportCheck == false) continue;

		RexCharAnimData* pCharData = new RexCharAnimData();
		pCharData->m_inputModelNames.PushAndGrow(atString(pModelPair->m_rootModel.c_str()));
		pCharData->m_moverModelNames.PushAndGrow(atString(pModelPair->m_moverModel.c_str()));
		pCharData->m_animPrefix = pModelPair->m_animPrefix.c_str();
		pCharData->m_animSuffix = pModelPair->m_animSuffix.c_str();
		pCharData->m_skeleton = pModelPair->m_skeletonFile.c_str();
		pCharData->m_modelType = pModelPair->m_modelType.c_str();
		pCharData->m_facialAttribute = pModelPair->m_facialAttribute.c_str();
		pCharData->m_charCtrlFile = pModelPair->m_charCtrlFile.c_str();
		pCharData->m_altOutputPath = pModelPair->m_altOutputPath.c_str();
		pCharData->m_bExport = pModelPair->m_modelExport;
		pCharData->m_bMergeFace = pModelPair->m_mergeFace;

		//Collect the additional model data
		for(int i=0; i < pModelPair->m_additionalModels.GetCount(); ++i)
		{
			RexAdditionalAnimData* pAdditional = new RexAdditionalAnimData();
			pAdditional->m_modelName = pModelPair->m_additionalModels[i]->m_modelName.c_str();
			pAdditional->m_trackType = pModelPair->m_additionalModels[i]->m_trackType.c_str();
			pAdditional->m_attributeName = pModelPair->m_additionalModels[i]->m_attributeName.c_str();
			
			std::map<string, string>::iterator itr = rexMBAnimExportCommon::GetAdditionalModelTrackTypes().find(string(pAdditional->m_trackType.c_str()));
			if(itr != rexMBAnimExportCommon::GetAdditionalModelTrackTypes().end())
			{
				pAdditional->m_trackId = itr->second.c_str();
			}

			pCharData->m_additionalModels.PushAndGrow(pAdditional);
		}
		
		pAnimData->m_characters.Grow() = pCharData;
	}

	if(m_segmentFromTakesButton.State == kFBButtonState0)
	{
		//Segmentation has been specified directly by the user
		pAnimData->m_segmentFromTakes = false;

		//Iterate over the segments in the spread sheet collecting the segment data
		for(ROWID_TO_SEG_MAP::iterator it = m_rowIdToAnimSegMap.begin();
			it != m_rowIdToAnimSegMap.end();
			++it)
		{
			RexMbAnimSegmentData* pSrcSegData = it->second;
			
			//Create a copy of the segment data
			RexMbAnimSegmentData* pOutSegData = new RexMbAnimSegmentData(*pSrcSegData);

			pOutSegData->m_inputModelNames.clear();
			pOutSegData->m_moverModelNames.clear();
			for(int characterIndex = 0; characterIndex < pAnimData->GetNumCharacters(); ++characterIndex)
			{
				RexCharAnimData* pCharData = pAnimData->GetCharacter(characterIndex);
				for(int i=0; i<pCharData->m_inputModelNames.GetCount(); i++)
				{
					pOutSegData->m_inputModelNames.PushAndGrow(pCharData->m_inputModelNames[i]);
				}
				
				for(int i=0; i<pCharData->m_moverModelNames.GetCount(); i++)
				{
					pOutSegData->m_moverModelNames.PushAndGrow(pCharData->m_moverModelNames[i]);
				}
			}
												  	
			pAnimData->m_segments.Grow() = pOutSegData;
		}
	}
	else
	{
		//Segmentation has been specified from Takes that have been setup in MB
		pAnimData->m_segmentFromTakes = true;

		//Iterate over the takes in the spread sheet, building up the segment data
		for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
			it != m_rowIdToTakeMap.end();
			++it)
		{
			RexMbAnimSegmentData* pSrcSegData = it->second;
			
			//Create a copy of the segment data
			RexMbAnimSegmentData* pOutSegData = new RexMbAnimSegmentData(*pSrcSegData);

			pOutSegData->m_inputModelNames.clear();
			pOutSegData->m_moverModelNames.clear();
			for(int characterIndex = 0; characterIndex < pAnimData->GetNumCharacters(); ++characterIndex)
			{
				RexCharAnimData* pCharData = pAnimData->GetCharacter(characterIndex);			
				for(int i=0; i<pCharData->m_inputModelNames.GetCount(); i++)
				{
					pOutSegData->m_inputModelNames.PushAndGrow(pCharData->m_inputModelNames[i]);
				}
				
				for(int i=0; i<pCharData->m_moverModelNames.GetCount(); i++)
				{
					pOutSegData->m_moverModelNames.PushAndGrow(pCharData->m_moverModelNames[i]);
				}
			}

			pAnimData->m_segments.Grow() = pOutSegData;
		}
	}

    pAnimData->m_useParentScale = m_useParentScaleButton.State != kFBButtonState0;
	pAnimData->m_bUseInitOffset = m_initOffsetCheckbox.State != kFBButtonState0;
	pAnimData->m_bUsedInNMBlend = m_usedInNMBlendCheckbox.State != kFBButtonState0;
	pAnimData->m_bUseTakeOutputPaths = m_useTakeOutputPathsButton.State != kFBButtonState0;
	pAnimData->m_bStoryMode = m_usedStoryModeCheckbox.State != kFBButtonState0;
	pAnimData->m_bEnableCameraDOF = m_usedCameraDOFCheckbox.State != kFBButtonState0;
	pAnimData->m_bRenderMode = m_renderModeCheckbox.State != kFBButtonState0;

	return pAnimData;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UsePerforceIntegrationButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);
} 

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::HelpButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{   
	system("start https://devstar.rockstargames.com/wiki/index.php/InGameExport");
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::OuputPathPicker_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	FBFolderPopup dirPicker;
	char cSourceDir[_MAX_PATH];

	if (strlen(m_outputPathEdit.Text.AsString()) == 0)
	{
		char cArtDir[_MAX_PATH];
		gs_GameView.GetArtDir( cArtDir, _MAX_PATH );
		sprintf(cSourceDir, "%s/anim/export_mb/", cArtDir );
	}
	else
	{
		strcpy(cSourceDir, m_outputPathEdit.Text.AsString());
	}

	dirPicker.Path = cSourceDir;

	if(dirPicker.Execute())
	{
		char cOutputPath[RAGE_MAX_PATH * 4];
		cOutputPath[0] = 0;

		safecat( cOutputPath, (const char*)dirPicker.Path, sizeof(cOutputPath) );
		safecat( cOutputPath, "\\", sizeof(cOutputPath) );

		m_outputPathEdit.Text = cOutputPath;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::ControlFilePathPicker_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	FBFilePopup filePicker;

	filePicker.Style = kFBFilePopupOpen;
	filePicker.Filter = "*.xml";

	filePicker.Path = m_ctrlFilePathEdit.Text.AsString();

	if(filePicker.Execute())
	{
#if HACK_GTA4
		m_ctrlFilePathEdit.Text = (const char*)filePicker.FileName;
#else
		m_ctrlFilePathEdit.Text = (char*)filePicker.FullFilename;
#endif // HACK_GTA4
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::DefaultCompressionPathPicker_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	FBFilePopup filePicker;

	filePicker.Style = kFBFilePopupOpen;
	filePicker.Filter = "*.txt";

	char cValue[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cValue, RAGE_MAX_PATH);

	char cConfigFile[RAGE_MAX_PATH];
	sprintf(cConfigFile, "%s\\etc\\config\\anim\\compression_templates", cValue);

	filePicker.Path = cConfigFile;

	if(filePicker.Execute())
	{
#if HACK_GTA4
		m_defaultCompressionEdit.Text = (const char*)filePicker.FileName;
#else
		m_defaultCompressionEdit.Text = (char*)filePicker.FullFilename;
#endif // HACK_GTA4
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::DefaultCompressionSet_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	const char* compressionTemplate = (const char*)m_defaultCompressionEdit.Text;

	if (!compressionTemplate)
		return;

	HFBScene hScene = m_system.Scene;
	int nTakes = hScene->Takes.GetCount();
	for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
	{
		HFBTake hTake = hScene->Takes[takeIdx];

		//Check to see if the take is already listed in the take spreadsheet
		for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
			it != m_rowIdToTakeMap.end();
			++it)
		{
			RexMbAnimSegmentData* pSegmentData = it->second;
			if( pSegmentData->GetTake() == hTake)
			{
				pSegmentData->m_compressionFilePath = compressionTemplate;
				break;
			}
		}
	}

	// Refresh the take sheet.
	RexAnimMarkupData* pExportData = CollectAnimExportData(true);
	UIPopulate(pExportData);
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::AttributeFilePathPicker_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	FBFilePopup filePicker;

	filePicker.Style = kFBFilePopupOpen;
	filePicker.Filter = "*.attributes";

	char cProjRoot[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_PROJROOT", cProjRoot, RAGE_MAX_PATH);

	char cExportPath[RAGE_MAX_PATH];
	sprintf_s(cExportPath, RAGE_MAX_PATH, "%s\\assets\\anim\\expressions\\", cProjRoot);

	filePicker.Path = cExportPath;

	if(filePicker.Execute())
	{
#if HACK_GTA4
		m_attributeFilePathEdit.Text = (const char*)filePicker.FullFilename;
#else
		m_attributeFilePathEdit.Text = (char*)filePicker.FullFilename;
#endif // HACK_GTA4
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::SegmentSheet_RowClick( HISender /*pSender*/, HKEvent pEvent )
{
	FBEventSpread e( pEvent );
    m_iSegSheetSelRowIndex = e.Row;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::SegmentSheet_CellChange( HISender /*pSender*/, HKEvent pEvent )
{
	FBEventSpread	spreadEvent(pEvent);

	int iRow = spreadEvent.Row;
	int	iColumn = spreadEvent.Column;
	
	ROWID_TO_SEG_MAP::iterator it = m_rowIdToAnimSegMap.find(iRow);
	Assertf( it != m_rowIdToAnimSegMap.end(), "SegmentSheet_CellChange, failed to find row id of '%d' in segment map", iRow);

	RexMbAnimSegmentData *pSegData = it->second;

	switch(iColumn)
	{
	case kSegmentName:
		{
			const char*	celVal = NULL;
			m_segmentSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_segmentName = celVal;
			break;
		}
	case kStartFrame:
		{
			int	celVal;
			m_segmentSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_iStartFrame = celVal;
			break;
		}
	case kEndFrame:
		{
			int celVal;
			m_segmentSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_iEndFrame = celVal;
			break;
		}
#if !HACK_GTA4
	case kLooping:
		{
			int celVal;
			m_segmentSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_bLooping = (celVal == 1 ? true : false);
			break;
		}
#endif // HACK_GTA4
	case kAutoPlay:
		{
			int celVal;
			m_segmentSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_bAutoplay = (celVal == 1 ? true : false);
			break;
		}
	default:
		Assertf(0, "SegmentSheet_CellChange, Unknown column id '%d'", iColumn);
		break;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::TakeSheet_RowClick( HISender /*pSender*/, HKEvent pEvent )
{
	FBEventSpread e( pEvent );
    m_iTakeSheetSelRowIndex = e.Row;

	int action = e.Action;
	Displayf("Action %d\n", action);
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::TakeSheet_CellChange( HISender /*pSender*/, HKEvent pEvent)
{
	FBEventSpread	spreadEvent(pEvent);

	int iRow = spreadEvent.Row;
	int	iColumn = spreadEvent.Column;
	
	ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.find(iRow);
	Assertf( it != m_rowIdToTakeMap.end(), "TakeSheet_CellChange, failed to find row id of '%d' in take map", iRow);

	RexMbAnimSegmentData *pSegData = it->second;

	switch(iColumn)
	{
	case kTakeExport:
		{
			int celVal;
			m_takeSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_bExport = (celVal == 1 ? true : false);
			break;
		}
	case kTakeAdditive:
		{
			int celVal;
			m_takeSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_bAdditive = (celVal == 1 ? true : false);
			break;
		}
	case kTakeAdditiveBA:
		{
			int celVal;
			m_takeSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_bAdditiveBaseAnim = (celVal == 1 ? true : false);
			break;
		}
	case kTakeLinearCompression:
		{
			int celVal;
			m_takeSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_bLinearCompression = (celVal == 1 ? true : false);
			break;
		}
#if !HACK_GTA4
	case kTakeLooping:
		{
			int celVal;
			m_takeSheet.GetCell( iRow, iColumn, celVal);
			pSegData->m_bLooping = (celVal == 1 ? true : false);
			break;
		}
#endif // !HACK_GTA4
	case kTakeCtrlFile:
		{
			const char*	celVal = NULL;
			int		celValIdx;
			atString celValName("");

			m_takeSheet.GetCell( iRow, iColumn, celVal );
			m_takeSheet.GetCell( iRow, iColumn, celValIdx );
			atString cellValues(celVal);
			celValName = rexMBAnimExportCommon::ResolveComboValueFromDelimitedString( celValIdx, cellValues );
			pSegData->m_ctrlFilePath = celValName;
			break;
		}
	case kTakeCompressionFile:
		{
			const char*	celVal = NULL;
			int		celValIdx;
			atString celValName("");

			m_takeSheet.GetCell( iRow, iColumn, celVal );
			m_takeSheet.GetCell( iRow, iColumn, celValIdx );
			atString cellValues(celVal);
			celValName = rexMBAnimExportCommon::ResolveComboValueFromDelimitedString( celValIdx, cellValues );
			pSegData->m_compressionFilePath = celValName;
			break;
		}
	case kTakeAltOutputPaths:
		{
			// Don't need to do anything special when the selection changes, as the current entry is found
			// when wanting to remove an output path
			break;
		}
	case kTakeAddOutputPath:
		{
			const char* celVal = NULL;
			m_takeSheet.GetCell( iRow, kTakeAltOutputPaths, celVal );
			
			FBFolderPopup dirPicker;

			// Set default file directory path to be the default output path if it's not empty
			if(strcmp("", m_outputPathEdit.Text.AsString()) != 0)
			{
				dirPicker.Path = m_outputPathEdit.Text.AsString();
			}

			char cOutputPath[RAGE_MAX_PATH * 4];
			cOutputPath[0] = 0;

			if(dirPicker.Execute())
			{
				safecat( cOutputPath, (const char*)dirPicker.Path, sizeof(cOutputPath) );
				safecat( cOutputPath, "\\", sizeof(cOutputPath) );

				pSegData->m_altOutputPaths.PushAndGrow(atString(cOutputPath));
				atString takeAltOutputPathsStr("");
				rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( pSegData->m_altOutputPaths, takeAltOutputPathsStr ); 
				m_takeSheet.SetCell( iRow, kTakeAltOutputPaths, const_cast<char*>( takeAltOutputPathsStr.c_str() ) );
				// Set to max index as it's the one we just added
				m_takeSheet.SetCell( iRow, kTakeAltOutputPaths, ( pSegData->GetNumAltOutputPaths() - 1) );
			}

			break;
		}
	case kTakeRemoveOutputPath:
		{
			if ( pSegData->GetNumAltOutputPaths() != 0 )
			{			
				int		celValIdx;
				m_takeSheet.GetCell( iRow, kTakeAltOutputPaths, celValIdx );

				atString takeAltOutputPathsStr("");				
				pSegData->m_altOutputPaths.Delete(celValIdx);
				rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( pSegData->m_altOutputPaths, takeAltOutputPathsStr ); 
				m_takeSheet.SetCell( iRow, kTakeAltOutputPaths, const_cast<char*>( takeAltOutputPathsStr.c_str() ) );			
				// Just set back to first value after a remove
				m_takeSheet.SetCell( iRow, kTakeAltOutputPaths, 0 );
			}
			break;
		}	
	case kTakeNameOverride:
		{
			const char*	celVal = NULL;
			m_takeSheet.GetCell( iRow, iColumn, celVal );
			pSegData->m_takeNameOverride = celVal;
			break;
		}
	case kTakeAddAdditiveName:
		{
			FBFilePopup filePicker;

			filePicker.Style = kFBFilePopupOpen;
			filePicker.Filter = "*.anim";

			filePicker.Path = m_outputPathEdit.Text;

			if(filePicker.Execute())
			{
				pSegData->m_additiveBaseAnimation = atString((const char*)filePicker.FullFilename);
				m_takeSheet.SetCell( iRow, kTakeAdditiveBaseAnimName, (const char*)filePicker.FullFilename );
			}

			break;
		}
	case kTakeRemoveAdditiveName:
		{
			if ( stricmp(pSegData->GetAdditiveBaseAnimation(), "") != 0 )
			{	
				pSegData->m_additiveBaseAnimation = "";
				m_takeSheet.SetCell( iRow, kTakeAdditiveBaseAnimName, "" );
			}
			break;
		}	
	default:
		Assertf(0, "TakeSheet_CellChange, unknown column id '%d'", iColumn);
		break;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::ModelPairSheet_RowClick( HISender /*pSender*/, HKEvent pEvent)
{
	FBEventSpread e( pEvent );
    m_iModelPairSheetSelRowIndex = e.Row;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::ModelPairSheet_CellChange( HISender /*pSender*/, HKEvent pEvent)
{
	FBEventSpread	spreadEvent(pEvent);

	int iRow = spreadEvent.Row;
	int	iColumn = spreadEvent.Column;
	
	ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.find(iRow);
	Assertf( it != m_rowIdToModelPairMap.end(), "ModelPairSheet_CellChange, failed to find row id of '%d' in model map", iRow);

	AetRootMoverPair* pModelPair = it->second;

	switch(iColumn)
	{
	case kRootModelName:
		{
			const char*	celVal = NULL;
			m_modelPairSheet.GetCell( iRow, iColumn, celVal);
			pModelPair->m_rootModel = celVal;
			break;
		}
	case kMoverModelName:
		{
			const char*	celVal = NULL;
			m_modelPairSheet.GetCell( iRow, iColumn, celVal);
			pModelPair->m_moverModel = celVal;
			break;
		}
	case kAnimNamePrefix:
		{
			const char*	celVal = NULL;
			m_modelPairSheet.GetCell( iRow, iColumn, celVal);
			pModelPair->m_animPrefix = celVal;
			break;
		}
	case kAnimNameSuffix:
		{
			const char*	celVal = NULL;
			m_modelPairSheet.GetCell( iRow, iColumn, celVal);
			pModelPair->m_animSuffix = celVal;
			break;
		}
	case kSkeletonFile:
		{
			const char*	celVal = NULL;
			int		celValIdx;
			atString celValName("");

			m_modelPairSheet.GetCell( iRow, iColumn, celVal );
			m_modelPairSheet.GetCell( iRow, iColumn, celValIdx );
			atString cellValues(celVal);
			celValName = rexMBAnimExportCommon::ResolveComboValueFromDelimitedString( celValIdx, cellValues );
			pModelPair->m_skeletonFile = celValName;
			break;
		}
	case kModelType:
		{
			const char*	celVal = NULL;
			int		celValIdx;
			atString celValName("");

			m_modelPairSheet.GetCell( iRow, iColumn, celVal );
			m_modelPairSheet.GetCell( iRow, iColumn, celValIdx );
			atString cellValues(celVal);
			celValName = rexMBAnimExportCommon::ResolveComboValueFromDelimitedString( celValIdx, cellValues );
			pModelPair->m_modelType = celValName;
			break;
		}
	case kFacialAttribute:
		{
			const char*	celVal = NULL;
			m_modelPairSheet.GetCell( iRow, kFacialAttribute, celVal );

			FBFilePopup filePicker;

			filePicker.Style = kFBFilePopupOpen;
			filePicker.Filter = "*.attributes";

			// Set default file directory path to be the default output path if it's not empty
			if(strcmp("", celVal) == 0)
			{
				char cProjRoot[RAGE_MAX_PATH];
				rexMBAnimExportCommon::GetEnvironmentVariable("RS_PROJROOT", cProjRoot, RAGE_MAX_PATH);

				char cExportPath[RAGE_MAX_PATH];
				sprintf_s(cExportPath, RAGE_MAX_PATH, "%s\\assets\\anim\\expressions\\", cProjRoot);

				filePicker.Path = cExportPath;;
			}

			if(filePicker.Execute())
			{
				// Set to max index as it's the one we just added
				m_modelPairSheet.SetCell( iRow, kFacialAttribute, filePicker.FullFilename );
				pModelPair->m_facialAttribute = filePicker.FullFilename;
			}

			break;
		}
	case kCharCtrlFile:
		{
			const char*	celVal = NULL;
			int		celValIdx;
			atString celValName("");

			m_modelPairSheet.GetCell( iRow, iColumn, celVal );
			m_modelPairSheet.GetCell( iRow, iColumn, celValIdx );
			atString cellValues(celVal);
			celValName = rexMBAnimExportCommon::ResolveComboValueFromDelimitedString( celValIdx, cellValues );
			pModelPair->m_charCtrlFile = celValName;
			break;
		}
	case kAltOutputPath:
		{
			const char*	celVal = NULL;
			m_modelPairSheet.GetCell( iRow, iColumn, celVal );

			FBFolderPopup dirPicker;
			// If there is already an output path set, use that as the starting path in the directory picker
			if ( strcmp(celVal,EXPORTTOOL_UNSET_OUTPUT_PATH) != 0 )
			{
				dirPicker.Path = celVal;
			}
			// Or set default file directory path to be the default output path if it's not empty
			else if(strcmp("", m_outputPathEdit.Text.AsString()) != 0)
			{
				dirPicker.Path = m_outputPathEdit.Text.AsString();
			}

			char cOutputPath[RAGE_MAX_PATH * 4];
			cOutputPath[0] = 0;

			if(dirPicker.Execute())
			{
				safecat( cOutputPath, (const char*)dirPicker.Path, sizeof(cOutputPath) );
				safecat( cOutputPath, "\\", sizeof(cOutputPath) );
			}
			else
			{
				safecat( cOutputPath, EXPORTTOOL_UNSET_OUTPUT_PATH );
			}

			pModelPair->m_altOutputPath = cOutputPath;
			m_modelPairSheet.SetCell( iRow, kAltOutputPath, cOutputPath );
			break;
		}
	case kModelExport:
		{
			int celVal;
			m_modelPairSheet.GetCell( iRow, iColumn, celVal);
			pModelPair->m_modelExport = (celVal == 1 ? true : false);
			break;
		}
	case kMergeFaceFile:
		{
			int celVal;
			m_modelPairSheet.GetCell( iRow, iColumn, celVal);
			pModelPair->m_mergeFace = (celVal == 1 ? true : false);
			break;
		}
	default:
		Assertf(0, "ModelPairSheet_CellChange, unknown column id '%d'", iColumn);
		break;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::AddRootButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	FBModelList selectedModels;
    FBGetSelectedModels( selectedModels, NULL, true );
	
	int nSelectedModels = selectedModels.GetCount();
	if(nSelectedModels)
	{
		for(int modelIdx = 0; modelIdx < nSelectedModels; modelIdx++)
		{
			//Get the name of the selected model
			const char* szModelName = (const char*)selectedModels[modelIdx]->LongName;

			// Ensure ther eis only one namespace
			atString sModelname(szModelName);
			atArray<atString> modelNameTokens;
			sModelname.Split(modelNameTokens, ":");
			if(modelNameTokens.GetCount() <= 2)
			{
				//Create the model/mover pair data store for the newly added model
				//NOTE: Skeleton file's default is set internally by constructor.
				AetRootMoverPair* pModelPair = new AetRootMoverPair();
				pModelPair->m_rootModel = szModelName;
				pModelPair->m_moverModel = "";
				pModelPair->m_animPrefix = "";
				pModelPair->m_animSuffix = "";
				if(ms_skeletonFiles.GetCount()>0)
					pModelPair->m_skeletonFile = ms_skeletonFiles[0];
				else
					pModelPair->m_skeletonFile = "";
				pModelPair->m_charCtrlFile = "(no spec file)";
				pModelPair->m_altOutputPath = EXPORTTOOL_UNSET_OUTPUT_PATH;
				pModelPair->m_modelExport = true;
				pModelPair->m_mergeFace = false;

				//Add a row to the model/mover spread sheet
				AddModelPairRow(pModelPair);

				//Set the newly added row as selected.  (m_iModelPairSpreadRowId is incremented in AddModelPairRow.)
				m_modelPairSheet.GetRow( m_iModelPairSpreadRowId-1 ).Selected = true;
				m_iModelPairSheetSelRowIndex = m_iModelPairSpreadRowId-1;

				UISetModelPairSheetIndices();
			}
			else
			{
				FBMessageBox("Error", "Model contains more than one namespace.  Check for multiple ':' characters.", "OK");
			}
		}
	}
	else
	{
		FBMessageBox("Error", "No model selected to add.", "OK");
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::RemoveRootButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if(m_iModelPairSheetSelRowIndex != -1)
	{
		ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.find(m_iModelPairSheetSelRowIndex);
		Assertf( it != m_rowIdToModelPairMap.end(), "Model spread-sheet row ids are out of sync, selected row id isn't in the model map!");

		//Free the segment data
		delete it->second;

		//Remove the item from the row map
		m_rowIdToModelPairMap.erase(it);

		//Remove the row from the spread-sheet
		FBSpreadRow row = m_modelPairSheet.GetRow( m_iModelPairSheetSelRowIndex );
		row.Remove();

		m_iModelPairSheetSelRowIndex = -1;

		UISetModelPairSheetIndices();
	}
	else
	{
		FBMessageBox("Error", "No model selected in the model sheet.\nPlease select a row in the model sheet to remove\nfor (click in the index column of the sheet to select).", "OK");
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::AddMoverButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	FBModelList selectedModels;
    FBGetSelectedModels( selectedModels, NULL, true );
	
	int nSelectedModels = selectedModels.GetCount();
	if( nSelectedModels == 0 )
	{
		FBMessageBox("Error", "No model selected to add.", "OK");
	}
	else if( nSelectedModels > 1)
	{
		FBMessageBox("Error", "Please select only one model to set as a mover model.", "OK");
	}
	else
	{
		if(m_iModelPairSheetSelRowIndex != -1)
		{
			ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.find(m_iModelPairSheetSelRowIndex);
			Assertf( it != m_rowIdToModelPairMap.end(), "Root Model/Mover spread-sheet row ids are out of sync, selected row id isn't in the model map!");

			//Get the name of the selected model
			const char* szModelName = (const char*)selectedModels[0]->LongName;

			AetRootMoverPair* pModelPair = it->second;
			pModelPair->m_moverModel = szModelName;

			m_modelPairSheet.SetCell( m_iModelPairSheetSelRowIndex, kMoverModelName, const_cast<char*>(pModelPair->m_moverModel.c_str()));
		}
		else
		{
			FBMessageBox("Error", "No model selected in the model sheet.\nPlease select a row in the model sheet to set the mover\nfor (click in the index column of the sheet to select).", "OK");
		}
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::RemoveMoverButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if(m_iModelPairSheetSelRowIndex != -1)
	{
		ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.find(m_iModelPairSheetSelRowIndex);
		Assertf( it != m_rowIdToModelPairMap.end(), "Model spread-sheet row ids are out of sync, selected row id isn't in the model map!");

		m_modelPairSheet.SetCell( m_iModelPairSheetSelRowIndex, kMoverModelName, "");

		AetRootMoverPair* pModelPair = it->second;
		pModelPair->m_moverModel = "";
	}
	else
	{
		FBMessageBox("Error", "No model selected in the model sheet.\nPlease select a row in the model sheet to remove\nfor (click in the index column of the sheet to select).", "OK");
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::AddSegmentButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	string segmentName;

	const char* szFBXPath = (const char*)m_app.FBXFileName;
	if(szFBXPath && strlen(szFBXPath))
	{
		string fileName = ASSET.FileName(szFBXPath);
		size_t dotPos = fileName.find_last_of('.');
		segmentName = fileName.substr(0, dotPos);
	}
	else
	{
		stringstream segStream;
		segStream << "defaultSegment" << m_iSegSpreadRowId;
		segmentName = segStream.str();
	}

	int startFrame = -1;
	int endFrame = -1;

	m_segmentSheet.RowAdd( "", m_iSegSpreadRowId );
	m_segmentSheet.SetCell( m_iSegSpreadRowId, kSegmentName, const_cast<char*>(segmentName.c_str())) ;
	m_segmentSheet.SetCell( m_iSegSpreadRowId, kStartFrame, startFrame);
	m_segmentSheet.SetCell( m_iSegSpreadRowId, kEndFrame, endFrame);
#if !HACK_GTA4
	m_segmentSheet.SetCell( m_iSegSpreadRowId, kLooping, true);
#endif // HACK_GTA4
	m_segmentSheet.SetCell( m_iSegSpreadRowId, kAutoPlay, true);

	RexMbAnimSegmentData* pSegmentData = new RexMbAnimSegmentData();
	pSegmentData->m_segmentName = segmentName.c_str();
	pSegmentData->m_iStartFrame = startFrame;
	pSegmentData->m_iEndFrame = endFrame;
	pSegmentData->m_bLooping = true;
	pSegmentData->m_bAutoplay = true;

	m_rowIdToAnimSegMap[m_iSegSpreadRowId] = pSegmentData;

	m_iSegSpreadRowId++;

	UISetSegSheetIndices();
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::RemoveSegmentButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_iSegSheetSelRowIndex != -1)
	{
		ROWID_TO_SEG_MAP::iterator it = m_rowIdToAnimSegMap.find(m_iSegSheetSelRowIndex);
		Assertf( it != m_rowIdToAnimSegMap.end(), "Segment spread-sheet row ids are out of sync, selected row id isn't in the segment map!");

		//Free the segment data
		delete it->second;

		//Remove the item from the row map
		m_rowIdToAnimSegMap.erase(it);

		//Remove the row from the spread-sheet
		FBSpreadRow row = m_segmentSheet.GetRow( m_iSegSheetSelRowIndex );
		row.Remove();

		m_iSegSheetSelRowIndex = -1;
	}

	UISetSegSheetIndices();
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::AddAdditionalModelsButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if(m_iModelPairSheetSelRowIndex != -1)
	{
		ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.find(m_iModelPairSheetSelRowIndex);
		Assertf( it != m_rowIdToModelPairMap.end(), "Root Model/Mover spread-sheet row ids are out of sync, selected row id isn't in the model map!");

		AetRootMoverPair* pModelPair = it->second;

		AdditionalModelsPopup *pObjectEditPopup = rage_new AdditionalModelsPopup(pModelPair->m_additionalModels);
		pObjectEditPopup->UICreate();
		pObjectEditPopup->UIConfigure();
		pObjectEditPopup->SetData();

		pObjectEditPopup->Show(this);
	}
	else
	{
		FBMessageBox("Error", "No model selected to add.", "OK");
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::ClearSegmentsButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	UIResetSegmentSpreadsheet();
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::UpdateModelPairRow(AetRootMoverPair* pModelPair, int rowId)
{
	atString skeletonPathsStr("");
	atString ctrlPathsStr("");

	rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray(ms_skeletonFiles, skeletonPathsStr);
	rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray(ms_ctrlPathTemplates, ctrlPathsStr);

	//Add the row and set it with all information.
	m_modelPairSheet.SetCell(rowId, kRootModelName, const_cast<char*>(pModelPair->m_rootModel.c_str()));
	m_modelPairSheet.SetCell(rowId, kMoverModelName, const_cast<char*>(pModelPair->m_moverModel.c_str()));
	m_modelPairSheet.SetCell(rowId, kAnimNamePrefix, const_cast<char*>(pModelPair->m_animPrefix.c_str()));
	m_modelPairSheet.SetCell(rowId, kAnimNameSuffix, const_cast<char*>(pModelPair->m_animSuffix.c_str()));
	m_modelPairSheet.SetCell(rowId, kSkeletonFile, const_cast<char*>(skeletonPathsStr.c_str()));
	m_modelPairSheet.SetCell(rowId, kModelType, const_cast<char*>(s_ModelTypesStr.c_str()));
	m_modelPairSheet.SetCell(rowId, kFacialAttribute, const_cast<char*>(pModelPair->m_facialAttribute.c_str()));
	m_modelPairSheet.SetCell(rowId, kCharCtrlFile, const_cast<char*>(ctrlPathsStr.c_str()));
	m_modelPairSheet.SetCell(rowId, kAltOutputPath, const_cast<char*>(pModelPair->m_altOutputPath.c_str()));
	m_modelPairSheet.SetCell(rowId, kModelExport, pModelPair->m_modelExport);
	m_modelPairSheet.SetCell(rowId, kMergeFaceFile, pModelPair->m_mergeFace);

	//Set the index of the skeleton file from the drop down.
	const char*	celVal = NULL;
	m_modelPairSheet.GetCell(rowId, kSkeletonFile, celVal);
	atString skeletonFileStr = atString(pModelPair->m_skeletonFile.c_str());
	atString skeletonComboStr = atString(celVal);
	int skeletonFileIdx = rexMBAnimExportCommon::ResolveComboIndexFromString(skeletonFileStr, skeletonComboStr);
	if (skeletonFileIdx == -1) skeletonFileIdx = 0;
	m_modelPairSheet.SetCell(rowId, kSkeletonFile, skeletonFileIdx);

	//Set the index of the ctrl file from the drop down.
	celVal = NULL;
	m_modelPairSheet.GetCell(rowId, kCharCtrlFile, celVal);
	atString ctrlFileStr = atString(pModelPair->m_charCtrlFile.c_str());
	atString ctrlComboStr = atString(celVal);
	int ctrlFileIdx = rexMBAnimExportCommon::ResolveComboIndexFromString(ctrlFileStr, ctrlComboStr);
	if (ctrlFileIdx == -1) ctrlFileIdx = 0;
	m_modelPairSheet.SetCell(rowId, kCharCtrlFile, ctrlFileIdx);

	//Set the index of the model type from the drop down.
	m_modelPairSheet.GetCell(rowId, kModelType, celVal);

	atString modelTypeStr = atString(pModelPair->m_modelType.c_str());
	atString modelTypeComboStr = atString(celVal);
	int modelTypeIdx = rexMBAnimExportCommon::ResolveComboIndexFromString(modelTypeStr, modelTypeComboStr);
	if (modelTypeIdx == -1) modelTypeIdx = 0;
	m_modelPairSheet.SetCell(rowId, kModelType, modelTypeIdx);
	atString cellValue(celVal);
	pModelPair->m_modelType = rexMBAnimExportCommon::ResolveComboValueFromDelimitedString(modelTypeIdx, cellValue);
}
//-----------------------------------------------------------------------------
void RexRageAnimExportTool::AddModelPairRow(AetRootMoverPair* pModelPair)
{
	//Add the row and set it with all information.
	m_modelPairSheet.RowAdd( "", m_iModelPairSpreadRowId );

	// Update the new row with data from the model pair
	UpdateModelPairRow(pModelPair, m_iModelPairSpreadRowId);

	//Add it to the map.
	m_rowIdToModelPairMap[m_iModelPairSpreadRowId] = pModelPair;

	// Increment the rowId count
	m_iModelPairSpreadRowId++;
}
//-----------------------------------------------------------------------------
RexRageAnimExportTool::AetRootMoverPair* RexRageAnimExportTool::GetModelPairRow(unsigned int idx)
{
	if (idx<m_rowIdToModelPairMap.size())
		return m_rowIdToModelPairMap.at(idx);
	else
		return NULL;
}
//-----------------------------------------------------------------------------
RexRageAnimExportTool::AetRootMoverPair* RexRageAnimExportTool::GetModelPairRow(const char* rootModelName, int& rowId)
{
	AetRootMoverPair* pModelPair = NULL;

	for (ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.begin();
		it != m_rowIdToModelPairMap.end();
		++it)
	{
		AetRootMoverPair* pair = it->second;

		if (stricmp(pair->m_rootModel.c_str(), rootModelName) == 0)
		{
			rowId = it->first;
			pModelPair = pair;
			break;
		}
	}

	return pModelPair;
}
//-----------------------------------------------------------------------------
char* GetSceneInitialOffsetOrigin()
{
	atString atOrigin("");
	if(rexMBAnimExportCommon::GetNewOrigin(atOrigin))
	{
		char* cBuffer = rage_new char[RAGE_MAX_PATH];
		safecpy(cBuffer, atOrigin.c_str(), RAGE_MAX_PATH);
		return cBuffer;
	}
	
	return NULL;
}
//-----------------------------------------------------------------------------
Vector3* GetSyncedScenePosition()
{
	if(s_pCurrentAnimExportTool)
	{
		RexAnimMarkupData* pExportData = s_pCurrentAnimExportTool->CollectAnimExportData();

		atString outputPath(pExportData->GetOutputPath());
		atArray<atString> outputPathTokens;
		outputPath.Split( outputPathTokens, "\\" );

		FBSystem system;

		char cSceneName[RAGE_MAX_PATH];
		sprintf_s(cSceneName, "%s_%s_%s", outputPathTokens[outputPathTokens.GetCount()-3].c_str(), outputPathTokens[outputPathTokens.GetCount()-2].c_str(), system.CurrentTake->Name.AsString());

		char* pOutputFile = rage_new char[RAGE_MAX_PATH];
		char cAssetPath[RAGE_MAX_PATH];
		rexMBAnimExportCommon::GetEnvironmentVariable("RS_PROJROOT", cAssetPath, RAGE_MAX_PATH);
		sprintf_s(pOutputFile, RAGE_MAX_PATH, "%s\\assets\\anim\\synced_scenes\\%s.xml", cAssetPath, cSceneName);

		if(ASSET.Exists(pOutputFile, NULL))
		{
			fwSyncedSceneEntityManager manager;
			if(manager.Load(pOutputFile))
			{
				if(manager.m_SceneInfo.GetNumLocations())
				{
					return rage_new Vector3(manager.m_SceneInfo.GetLocationOrigin(0));
				}
			}
		}

		FBVector3d vTranslation = FBVector3d(0.f,0.f,0.f);

		HFBModel fbmOrigin = RAGEFindModelByName(pExportData->GetOriginObjectName());
		if(fbmOrigin)
		{
			fbmOrigin->GetVector(vTranslation, kModelTranslation, true);
		}

		return rage_new Vector3(-vTranslation[0]/100,vTranslation[2]/100,vTranslation[1]/100);
	}

	return NULL;
}
//-----------------------------------------------------------------------------
Vector3* GetSyncedSceneRotation()
{
	if(s_pCurrentAnimExportTool)
	{
		RexAnimMarkupData* pExportData = s_pCurrentAnimExportTool->CollectAnimExportData();

		atString outputPath(pExportData->GetOutputPath());
		atArray<atString> outputPathTokens;
		outputPath.Split( outputPathTokens, "\\" );

		FBSystem system;

		char cSceneName[RAGE_MAX_PATH];
		sprintf_s(cSceneName, "%s_%s_%s", outputPathTokens[outputPathTokens.GetCount()-3].c_str(), outputPathTokens[outputPathTokens.GetCount()-2].c_str(), system.CurrentTake->Name.AsString());

		char* pOutputFile = rage_new char[RAGE_MAX_PATH];
		char cAssetPath[RAGE_MAX_PATH];
		rexMBAnimExportCommon::GetEnvironmentVariable("RS_PROJROOT", cAssetPath, RAGE_MAX_PATH);
		sprintf_s(pOutputFile, RAGE_MAX_PATH, "%s\\assets\\anim\\synced_scenes\\%s.xml", cAssetPath, cSceneName);

		if(ASSET.Exists(pOutputFile, NULL))
		{
			fwSyncedSceneEntityManager manager;
			if(manager.Load(pOutputFile))
			{
				if(manager.m_SceneInfo.GetNumLocations())
				{
					return rage_new Vector3(manager.m_SceneInfo.GetLocationRotation(0));
				}
			}
		}

		FBVector3d vRotation = FBVector3d(0.f,0.f,0.f);

		HFBModel fbmOrigin = RAGEFindModelByName(pExportData->GetOriginObjectName());
		if(fbmOrigin)
		{
			fbmOrigin->GetVector(vRotation, kModelRotation, true);
		}

		return rage_new Vector3(-vRotation[0]/100,vRotation[2]/100,vRotation[1]/100);
	}

	return NULL;
}

bool RexRageAnimExportTool::GetAnimationDictionaryList(RexRageAnimExportTool* rexRageAnimExportTool, atArray<AnimDictionary>& animDictionaries)
{
	RexAnimMarkupData* pExportData = rexRageAnimExportTool->CollectAnimExportData();

	string outputPath = rexRageAnimExportTool->m_outputPathEdit.Text.AsString();

	atString outPath;
	atString relPath;
	atString name;
	atString rootDir;
	int numItems = 0;

	bool usingTakeOutputPaths = rexRageAnimExportTool->m_useTakeOutputPathsButton.State == kFBButtonState1;

	// If we're using the take alt output paths then collect dictionary names from them
	if(usingTakeOutputPaths)
	{
		numItems = pExportData->GetNumSegments();
	}
	// Otherwise we loop through each character and check their alt output paths
	else
	{
		numItems = pExportData->GetNumCharacters();
	}

	atArray<atString> outputPaths;
	for (int i = 0; i < numItems; i++)
	{			
		// Can be multiple output paths for each take
		if(usingTakeOutputPaths)
		{
			RexMbAnimSegmentData* pSegData = pExportData->GetSegment(i);

			if ( pSegData->IsMarkedForExport() ) // If take is marked for export
			{
				if(pSegData->GetNumAltOutputPaths() > 0) // If we have a take output path, else drop back to default
				{
					for (int idxAltPath = 0; idxAltPath < pSegData->GetNumAltOutputPaths(); idxAltPath++)
					{
						atString altPath = atString(pSegData->GetAltOutputPath(idxAltPath));
						outputPaths.PushAndGrow(altPath);
					}	
				}
				else
				{
					for(int j=0; j < pExportData->GetNumCharacters(); ++j) // If we have no take outputs we drop back to the chars and add theirs
					{
						if(pExportData->GetCharacter(j)->m_bExport)
						{
							atString altPath = atString(pExportData->GetCharacter(j)->GetAltOutputPath());
							outputPaths.PushAndGrow(altPath);
						}
					}
				}
			}
		}
		else
		{
			if(pExportData->GetCharacter(i)->m_bExport) // If we are not using takes, drop back to the chars
			{
				atString altPath = atString(pExportData->GetCharacter(i)->GetAltOutputPath());
				outputPaths.PushAndGrow(altPath);
			}
		}			
	}

	animDictionaries.Reserve(outputPaths.GetCount());

	for (int idxAltPath = 0; idxAltPath < outputPaths.GetCount(); idxAltPath++) 
	{
		outPath = "";
		relPath = "";
		name = "";
		rootDir = "";

		if(strcmp(outputPaths[idxAltPath], EXPORTTOOL_UNSET_OUTPUT_PATH) == 0)
		{
			outPath = (atString)outputPath.c_str();
		}
		else 
		{
			outPath = (atString)outputPaths[idxAltPath];
		}
		if(!rexRageAnimExportTool->DeriveDictionaryRelPathAndName(outPath, relPath, name, rootDir))
		{
			return false;
		}

		bool bAlreadyInList = false;
		// Check to see if this dictionary is already in the list to export
		for(int j = 0; j < animDictionaries.GetCount();j++)
		{
			if(strcmp(animDictionaries[j].DictionaryName, name) == 0)	
			{
				bAlreadyInList = true;
				break;
			}
		}
		// Add dictionary name and paths to the atArray to export
		if(!bAlreadyInList)
		{
			AnimDictionary animDictData = AnimDictionary(outPath, relPath, name, rootDir);
			animDictionaries.PushAndGrow(animDictData);
		}
	}

	outputPaths.Reset();
	return true;
}

void RexRageAnimExportTool::Export( RexRageAnimExportTool* rexRageAnimExportTool, bool bBatchExport )
{
#if ASSET_PIPELINE_3
	atString animExportDir = rexMBAnimExportCommon::GetProjectOrDlcIngameExportDir(atString(""));
	animExportDir.Replace("/", "\\");
#endif // ASSET_PIPELINE_3
#if PROFILE_ANIM_EXPORT
	string timeOutputPath = m_outputPathEdit.Text.AsString();
	timeOutputPath.append("\\RexRageAnimExportTool.timing");
	rexMBTimerLog::Open(timeOutputPath.c_str());
#endif //PROFILE_ANIM_EXPORT

	if(bBatchExport)
	{
		if(s_pCurrentAnimExportTool == NULL)
		{
			FBMessageBox("Error", "Animation Exporter UI is not open.", "OK");
			return;
		}
	}

	RexAnimMarkupData* pExportData = rexRageAnimExportTool->CollectAnimExportData();

	if(pExportData && pExportData->ValidateAnimExportData(true))
	{
		RexRageAnimExport animExporter;		
		animExporter.SetInteractiveMode(true);

		//Initalize and use the defaults for all settings paths...
		if(!animExporter.Initialize())
		{
			//Note : The animation exporter object will provide details to the user on the cause of the initialization failure
			delete pExportData;
#if PROFILE_ANIM_EXPORT
			rexMBTimerLog::Close();
#endif //PROFILE_ANIM_EXPORT
			return;
		}

		char cLogFilename[RAGE_MAX_PATH];
		sprintf_s( cLogFilename, RAGE_MAX_PATH, "%s\\log.ulog", rexRageAnimExportTool->m_outputPathEdit.Text.AsString() );
		ULOGGER_MOBO.ClearProcessLogDirectory();
		ULOGGER_MOBO.PreExport( cLogFilename, "rexmbrage" );

		if(!rexMBAnimExportCommon::ValidateBones(pExportData))
		{
			ULOGGER_MOBO.PostExport( !bBatchExport );
			return;
		}

		if(!rexMBAnimExportCommon::ValidateIKRoot(pExportData))
		{
			ULOGGER_MOBO.PostExport( !bBatchExport );
			return;
		}

		if (!rexMBAnimExportCommon::ValidateCompressionTemplates(pExportData))
		{
			ULOGGER_MOBO.PostExport( !bBatchExport );
			return;
		}

		bool bRes = animExporter.DoExport( pExportData );

		char cCmdLine[RAGE_MAX_PATH * 4];
		char cProgMsg[RAGE_MAX_PATH * 5];

		atArray<AnimDictionary> animDictionaries;
		if(!GetAnimationDictionaryList(rexRageAnimExportTool, animDictionaries))
		{
			ULOGGER_MOBO.PostExport( !bBatchExport );
			return;
		}

		for( int i = 0; i < animDictionaries.GetCount(); i++ )
		{
			cCmdLine[0] = 0;
			cProgMsg[0] = 0;

			// Don't want a build to triggered on export
#if !HACK_MP3
			safecpy( cProgMsg, "Pack file content command: ", sizeof(cProgMsg) );
#if USE_NEW_ANIM_PIPELINE

			bRes = bRes && rexMBAnimExportCommon::BuildDictionary( animDictionaries[i].OutputPath, animDictionaries[i].RootDirectoryName, animDictionaries[i].DictionaryName );
			if( bRes && rexRageAnimExportTool->m_previewCheckbox.State )
			{
#if ASSET_PIPELINE_3
				atArray<atString> contentArray;
				contentArray.PushAndGrow(atVarString("%s/%s/%s%s", animExportDir.c_str(), animDictionaries[i].RootDirectoryName.c_str(), animDictionaries[i].DictionaryName.c_str(), ".icd.zip"));
				bRes = bRes && rexMBAnimExportCommon::ProcessIngameZips( contentArray, false, true, true );
#else
				bRes = bRes && rexMBAnimExportCommon::PreviewDictionary( animDictionaries[i].DictionaryName, outputPathTokens[assetPathTokens.GetCount()] );
#endif // ASSET_PIPELINE_3
			}
#else
			bRes = bRes && rexMBAnimExportCommon::BuildAnimationDictionariesUsingContentSystem( animDictionaries[i].OutputPath, animDictionaries[i].OutputPath, animDictionaries[i].DictionaryName, animDictionaries[i].RelativePath, "clip_mb", cCmdLine, false, rexRageAnimExportTool->m_previewCheckbox.State ? 1 : 0);
#endif // USE_NEW_ANIM_PIPELINE
			safecat( cProgMsg, cCmdLine, sizeof(cProgMsg) );
			animExporter.SetProgressMessage( cProgMsg );
#endif // !HACK_MP3

		}
		ULOGGER_MOBO.AddAdditionalLogDir(ULOGGER_MOBO.GetProcessLogDirectory());
		ULOGGER_MOBO.PostExport( !bBatchExport );
		
		animDictionaries.Reset();
		delete pExportData;
	}

#if PROFILE_ANIM_EXPORT
	rexMBTimerLog::Close();
#endif //PROFILE_ANIM_EXPORT
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::ExportButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetExporterMode(INGAME_EXPORTER);
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);

	// Split the export logic from the callback so we can call it from python
	Export(this);
}

//-----------------------------------------------------------------------------
void RexRageAnimExportTool::BuildButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetExporterMode(INGAME_EXPORTER);
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);

	char cLogFilename[RAGE_MAX_PATH*2];
	sprintf_s( cLogFilename, RAGE_MAX_PATH, "%s\\buildlog.ulog", m_outputPathEdit.Text.AsString() );

	ULOGGER_MOBO.ClearProcessLogDirectory();

	ULOGGER_MOBO.PreExport(cLogFilename, "rexmbrage");
	
#if ASSET_PIPELINE_3
	atArray<AnimDictionary> animDictionaries;
	atArray<atString> contentArray;

	GetAnimationDictionaryList(this, animDictionaries);

	atString animExportDir = rexMBAnimExportCommon::GetProjectOrDlcIngameExportDir(atString(""));
	for( int i=0; i<animDictionaries.GetCount(); i++ )
	{
		contentArray.PushAndGrow( atString(animExportDir, animDictionaries[i].RootDirectoryName) );
	}
	rexMBAnimExportCommon::ProcessIngameZips( contentArray, false, false, true );
#elif USE_NEW_ANIM_PIPELINE
	atArray<AnimDictionary> animDictionaries;
	GetAnimationDictionaryList(this, animDictionaries);
	
	for( int i=0; i<animDictionaries.GetCount(); i++ )
	{
		atString outputRoot(animDictionaries[i].RelativePath);
		atArray<atString> outputRootTokens;
		outputRoot.Split( outputRootTokens, "\\", true );
		if( outputRootTokens.GetCount() > 0 )
		{

			rexMBAnimExportCommon::BuildRPF( true, animDictionaries[i].DictionaryName, outputRootTokens[0] );
		}	
		else
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Output root directory '%s' is in the wrong format.", outputRoot );
		}
	}

#else
	atString rootAssetPath("");
	rexMBAnimExportCommon::GetMotionBuilderRootAssetPathSetting( rootAssetPath );

	rexMBAnimExportCommon::BuildImageUsingContentSystem( false, "clip_mb", "clip", rootAssetPath.c_str(), false );

	char cToolsRoot[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cToolsRoot, RAGE_MAX_PATH);

	char cImageLog[RAGE_MAX_PATH];
	sprintf(cImageLog, "%s\\logs\\image_file.ulog", cToolsRoot);

	ULOGGER_MOBO.AddAdditionalLog(cImageLog);
#endif
	
	ULOGGER_MOBO.AddAdditionalLogDir(ULOGGER_MOBO.GetProcessLogDirectory());
	ULOGGER_MOBO.PostExport();
}

//-----------------------------------------------------------------------------	
void RexRageAnimExportTool::InitOffsetCheckbox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetNewOrigin((m_initOffsetCheckbox.State != kFBButtonState0), atString(m_originObjectEdit.Text.AsString()));
	/*
	RexAnimMarkupData* pExportData = CollectAnimExportData();
	if(this->m_initOffsetCheckbox.State == 1) 
		pExportData->m_bUseInitOffset = true;
	else
		pExportData->m_bUseInitOffset = false;
		*/
}

//-----------------------------------------------------------------------------	
void RexRageAnimExportTool::UsedInNBBlendCheckbox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	/*
	RexAnimMarkupData* pExportData = CollectAnimExportData();
	if(this->m_initOffsetCheckbox.State == 1) 
		pExportData->m_bUseInitOffset = true;
	else
		pExportData->m_bUseInitOffset = false;
		*/
}


//-----------------------------------------------------------------------------
void RexRageAnimExportTool::SaveClipButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	RexRageAnimExport animExporter;		
	animExporter.SetInteractiveMode(true);

	RexAnimMarkupData* pExportData = CollectAnimExportData();
	if(pExportData && pExportData->ValidateAnimExportData(true))
	{
		//Initialize and use the defaults for all settings paths...
		if(!animExporter.Initialize())
		{
			//Note : The animation exporter object will provide details to the user on the cause of the initialization failure
			delete pExportData;
			return;
		}

		bool bRes = animExporter.SaveClipDataAllTakes( pExportData );
		if(bRes)
		{
			FBMessageBox("REX Animation Export", "Clip Save Succeeded.", "OK");
		}
		else
		{
			FBMessageBox("REX Animation Export", "Clip Save Failed.", "OK");
		}
	}
	if( pExportData )
		delete pExportData;

}

//-----------------------------------------------------------------------------
void RexRageAnimExportTool::LoadClipButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	RexRageAnimExport animExporter;		
	animExporter.SetInteractiveMode(true);
	UserObjects::SetHasLoadedClips(false); 

	RexAnimMarkupData* pExportData = CollectAnimExportData(true);
	if(pExportData && pExportData->ValidateAnimExportData(true))
	{
		//Initalize and use the defaults for all settings paths...
		if(!animExporter.Initialize())
		{
			//Note : The animation exporter object will provide details to the user on the cause of the initialization failure
			delete pExportData;
			return;
		}

		char cLogFilename[RAGE_MAX_PATH*2];
		sprintf_s( cLogFilename, RAGE_MAX_PATH, "%s\\log.ulog", m_outputPathEdit.Text.AsString() );
		ULOGGER_MOBO.PreExport( cLogFilename, "rexmbrage" );
		
		bool bRes = animExporter.LoadClipDataAllTakes( pExportData );
		if(bRes)
		{
			UserObjects::SetHasReloadedClips(true);
			UIPopulate( pExportData );
		}
		
		ULOGGER_MOBO.PostExport( !bRes );		
	}
	

	if( pExportData )
		delete pExportData;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::LoadMarkupButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	FBFilePopup fbpop;

	fbpop.Style = kFBFilePopupOpen;
	fbpop.FileName = "";
	fbpop.Caption = "Load Markup";
	fbpop.Filter = "*.rexmb";

	if(fbpop.Execute())
	{
		string markupFilePath = fbpop.FullFilename.AsString();
		
		RexAnimMarkupData* pAnimData = new RexAnimMarkupData();
		if(PARSER.LoadObject<RexAnimMarkupData>(markupFilePath.c_str(), "", *pAnimData))
		{
			ValidateMarkupData( pAnimData );
			UIPopulate(pAnimData);
		}

		delete pAnimData;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::RefreshMarkupButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	RexAnimMarkupData* pExportData = CollectAnimExportData(true);
	if(pExportData)
	{
		char cTempPath[RAGE_MAX_PATH];
		GetTempPath(RAGE_MAX_PATH, cTempPath);		
		
		char cTempFile[RAGE_MAX_PATH];
		sprintf_s(cTempFile, RAGE_MAX_PATH, "%s\\tempMarkup.rexmb", cTempPath);

		if(pExportData->SaveToFile(cTempFile))
		{
			RexAnimMarkupData* pAnimData = new RexAnimMarkupData();
			if(PARSER.LoadObject<RexAnimMarkupData>(cTempFile, "", *pAnimData))
			{
				ValidateMarkupData( pAnimData );
				UIPopulate(pAnimData);
			}

			delete pAnimData;
		}

		DeleteFile(cTempFile);
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::SaveMarkupButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	FBFilePopup fbpop;

	fbpop.Style = kFBFilePopupSave;
	fbpop.FileName = "";
	fbpop.Caption = "Save Markup";
	fbpop.Filter = "*.rexmb";

	if(fbpop.Execute())
	{
		string markupFilePath = fbpop.FullFilename.AsString();
		
		unsigned int fiAttr = ASSET.GetAttributes(markupFilePath.c_str(), "");
		if( (fiAttr != FILE_ATTRIBUTE_INVALID) && (fiAttr & FILE_ATTRIBUTE_READONLY)  )
		{
			FBMessageBox( "Markup Save Failed", "Destination file is marked as read-only, markup file save failed.", "OK");
			return;
		}

		RexAnimMarkupData* pExportData = CollectAnimExportData(true);
		if(pExportData)
		{
			pExportData->SaveToFile(markupFilePath.c_str());
			delete pExportData;
		}
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::SegmentFromTakesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_segmentFromTakesButton.State == kFBButtonState0)
	{	
		//Segment State
		SetControl("Layout", m_layout[kSegment]);
	}
	else
	{
		//Takes State
		SetControl("Layout", m_layout[kTake]);
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::RefreshTakesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	//Build up a list of row ids for takes that are in the take sheet 
	//but are not available in the current scene.
	vector< int > keysToRemoveArray;

	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
		it != m_rowIdToTakeMap.end();
		++it)
	{
		RexMbAnimSegmentData* pSegmentData = it->second;
		if(rexMBAnimExportCommon::TakeExists(pSegmentData->GetTake()) == false)
		{
			//The take listed in the take spread sheet is no longer in the current scene
			keysToRemoveArray.push_back(it->first);
		}
	}

	//Remove all the entries from the take sheet that are not in the current scene
	for( vector< int >::const_iterator cIt = keysToRemoveArray.begin();
		cIt != keysToRemoveArray.end();
		++cIt)
	{
		int rowId = *cIt;

		//Remove the row from the spread-sheet
		FBSpreadRow row = m_takeSheet.GetRow( rowId );
		row.Remove();

		//Free the segment data
		ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.find( rowId );
		delete it->second;
		
		//Remove the entry from the rowId to take map
		m_rowIdToTakeMap.erase( it );
	}

	// Populate compression and control file strings to provide combo boxes for each take
	ms_ctrlPathTemplates.Reset();
	ms_compressionPathTemplates.Reset();
	ms_skeletonFiles.Reset();

	ms_ctrlPathTemplates.PushAndGrow( atString(m_DefaultSpecFile) );

	UIRefreshTemplateFiles(); 

	atString ctrlPathsStr("");
	atString compressionPathsStr("");
	rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( ms_ctrlPathTemplates, ctrlPathsStr );
	rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( ms_compressionPathTemplates, compressionPathsStr );
	
	RemoveDuplicateTakesFromSpreadsheet();
	AddMissingTakesToSpreadsheet(ctrlPathsStr, compressionPathsStr);

	UISetTakeSheetIndices();
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::RemoveDuplicateTakesFromSpreadsheet()
{	
	vector< int > keysToRemoveArray;
	rage::atArray<rage::atString> takeNames;

	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
		it != m_rowIdToTakeMap.end();
		++it)
	{
		RexMbAnimSegmentData* pSegmentData = it->second;
		if (takeNames.ReverseFind(atString(pSegmentData->GetTakeName())) == -1)
		{
			takeNames.PushAndGrow(atString(pSegmentData->GetTakeName()));
		}
		else
		{
			keysToRemoveArray.push_back(it->first);
		}
	}

	//Remove all the entries from the take sheet that are duplicates
	for( vector< int >::const_iterator cIt = keysToRemoveArray.begin();
		cIt != keysToRemoveArray.end();
		++cIt)
	{
		int rowId = *cIt;

		//Remove the row from the spread-sheet
		FBSpreadRow row = m_takeSheet.GetRow( rowId );
		row.Remove();

		//Free the segment data
		ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.find( rowId );
		delete it->second;

		//Remove the entry from the rowId to take map
		m_rowIdToTakeMap.erase( it );
	}
}

void RexRageAnimExportTool::AddMissingTakesToSpreadsheet(atString &ctrlPathsStr, atString& compressionPathsStr)
{
	HFBScene hScene = m_system.Scene;
	int nTakes = hScene->Takes.GetCount();
	for(int takeIdx = 0; takeIdx < nTakes; takeIdx++)
	{
		HFBTake hTake = hScene->Takes[takeIdx];
		bool bTakeExists = false;

		//Check to see if the take is already listed in the take spreadsheet
		for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
			it != m_rowIdToTakeMap.end();
			++it)
		{
			RexMbAnimSegmentData* pSegmentData = it->second;
			if( pSegmentData->GetTake() == hTake)
			{
				bTakeExists = true;
				break;
			}
		}

		//If the take isn't already in the take spreadsheet then add it
		if(!bTakeExists)
		{
			RexMbAnimSegmentData* pSegmentData = new RexMbAnimSegmentData();
			const char *szTakeName = (const char*)hTake->Name;

			//The segment name ends up dictating the output animation name, so it needs to be normalized
			//to remove spaces and characters that can't be in a file name
			pSegmentData->m_segmentName = NormalizeName(szTakeName).c_str();

			pSegmentData->m_takeName = szTakeName;
			pSegmentData->m_take = hTake;
			pSegmentData->m_iStartFrame = -1;
			pSegmentData->m_iEndFrame = -1;
			pSegmentData->m_bLooping = false;
			pSegmentData->m_bAutoplay = true;

			m_takeSheet.RowAdd( "", m_iTakeSpreadRowId );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeName, const_cast<char*>(pSegmentData->m_takeName.c_str()));
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeExport, true);
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeCtrlFile, const_cast<char*>( ctrlPathsStr.c_str() ) );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeCompressionFile, const_cast<char*>( compressionPathsStr.c_str() ) );
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAltOutputPaths, 0);
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAddOutputPath, "Add");
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeRemoveOutputPath, "Remove");
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAddAdditiveName, "Add");
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeRemoveAdditiveName, "Remove");

			// default to the ingame default compression
			atString strDefault("Ingame_Default_Compress.txt");
			int compressFileIdx = rexMBAnimExportCommon::ResolveComboIndexFromString( strDefault, compressionPathsStr );
			if ( compressFileIdx == -1 ) compressFileIdx = 0;
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeCompressionFile, compressFileIdx );

#if !HACK_GTA4
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeLooping, pSegmentData->m_bLooping);
#endif // HACK_GTA4
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAdditive, pSegmentData->m_bAdditive);
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeAdditiveBA, pSegmentData->m_bAdditiveBaseAnim);
			m_takeSheet.SetCell( m_iTakeSpreadRowId, kTakeLinearCompression, pSegmentData->m_bLinearCompression);

			m_rowIdToTakeMap[m_iTakeSpreadRowId] = pSegmentData;

			m_iTakeSpreadRowId++;
		}
	}

	UISetTakeSheetIndices();
}


//-----------------------------------------------------------------------------

void RexRageAnimExportTool::EnableAllTakesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
			it != m_rowIdToTakeMap.end();
			++it)
	{
		int rowId = it->first;
		RexMbAnimSegmentData* pSegmentData = it->second;

		m_takeSheet.SetCell( rowId, kTakeExport, true );
		pSegmentData->m_bExport = true;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::OpenExportFolder_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	char cLogFilename[RAGE_MAX_PATH*2];
	sprintf_s( cLogFilename, RAGE_MAX_PATH, "explorer %s", m_outputPathEdit.Text.AsString() );

	system(cLogFilename);
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::DisableAllTakesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
			it != m_rowIdToTakeMap.end();
			++it)
	{
		int rowId = it->first;
		RexMbAnimSegmentData* pSegmentData = it->second;

		m_takeSheet.SetCell( rowId, kTakeExport, false );
		pSegmentData->m_bExport = false;
	}
}

//-----------------------------------------------------------------------------
	
void RexRageAnimExportTool::OutputPathEdit_Change( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	//TODO : Validate output path
}

//-----------------------------------------------------------------------------
void RexRageAnimExportTool::ControlFilePathEdit_Change( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	//TODO : Validate control file path
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::ClearOriginObject_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	m_originObjectEdit.Text = "";
	rexMBAnimExportCommon::SetNewOrigin((m_initOffsetCheckbox.State != kFBButtonState0), atString(""));
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::OriginObject_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	FBModelList selectedModels;
	FBGetSelectedModels( selectedModels, NULL, true );

	int nSelectedModels = selectedModels.GetCount();
	if(nSelectedModels)
	{
		//Get the name of the selected model
		const char* szModelName = (const char*)selectedModels[0]->LongName;

		m_originObjectEdit.Text = szModelName;

		rexMBAnimExportCommon::SetNewOrigin((m_initOffsetCheckbox.State != kFBButtonState0), atString(szModelName));
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::SceneOnTakeChange_Event(HISender /*pSender*/, HKEvent pEvent)
{
	FBEventTakeChange lEvent( pEvent );
	HFBTake hTake = lEvent.Take;

	if(lEvent.Type == kFBTakeChangeRemoved)
	{
		//Find the take in the spreadsheet
		ROWID_TO_TAKE_MAP::iterator itTake;

		for(itTake = m_rowIdToTakeMap.begin();
			itTake != m_rowIdToTakeMap.end();
			++itTake)
		{
			RexMbAnimSegmentData* pSegmentData = itTake->second;
			if( pSegmentData->m_take == hTake)
			{
				break;
			}
		}

		//If the take is listed in the take spread sheet then remove it...
		if(itTake != m_rowIdToTakeMap.end())
		{
			//Remove the row from the spread-sheet
			FBSpreadRow row = m_takeSheet.GetRow( itTake->first );
			row.Remove();

			//Free the segment data
			delete itTake->second;
			
			//Remove the item from the row map
			m_rowIdToTakeMap.erase(itTake);
		}

		UISetTakeSheetIndices();
	}
	else if(lEvent.Type == kFBTakeChangeRenamed)
	{
		ROWID_TO_TAKE_MAP::iterator itTake;
		RexMbAnimSegmentData* pSegmentData = NULL;
		for(itTake = m_rowIdToTakeMap.begin();
			itTake != m_rowIdToTakeMap.end();
			++itTake)
		{
			pSegmentData = itTake->second;
			if( pSegmentData->m_take == hTake)
			{
				pSegmentData->m_takeName = hTake->Name.AsString();
				rexMBAnimExportCommon::ReplaceWhitespaceCharacters(pSegmentData->m_takeName);
				hTake->Name = (char*)pSegmentData->m_takeName.c_str();
				pSegmentData->m_segmentName = pSegmentData->m_takeName;
				break;
			}
		}

		//If the take is listed in the take spread sheet then remove it...
		if(itTake != m_rowIdToTakeMap.end())
		{
			//Update the row in the spreadsheet
			m_takeSheet.SetCell( itTake->first, kTakeName, const_cast<char*>(pSegmentData->m_takeName.c_str()));
		}
	}
	else if(lEvent.Type == kFBTakeChangeAdded)
	{
		atString ctrlPathsStr("");
		atString compressionPathsStr("");

		rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( ms_ctrlPathTemplates, ctrlPathsStr );
		rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( ms_compressionPathTemplates, compressionPathsStr );
		AddMissingTakesToSpreadsheet(ctrlPathsStr, compressionPathsStr);		
	}
	else if(lEvent.Type == kFBTakeChangeMoved)
	{
		//Unfortunately because the OpenReality SDK sucks, we can't take an existing row
		//and insert it into a new index.  We have to regenerate the entire take sheet.
		//
		RexAnimMarkupData* pExportData = CollectAnimExportData(true);
		UIPopulate(pExportData);
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::FileNew_Event(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	//A new file has been requested by the application, but hasn't been created yet
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::FileNewCompleted_Event(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	//The file new event has completed, so we need to set the tools UI option to the defaults
	UIRestoreDefaults();
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::FileOpenCompleted_Event(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	UIRestoreDefaults();

	//Load any persistent data stored in the file
	if(UserObjects::FindPersistentData(false))
	{
		RexAnimMarkupData* pAnimData = UserObjects::GetAnimationMarkupData();
		if(pAnimData)
		{
			ValidateMarkupData(pAnimData);

			UserObjects::SetHasLoadedClips(false);
			
	
			RexRageAnimExport animExporter;		
			animExporter.SetInteractiveMode(true);
			
			rexMBAnimExportCommon::LoadBoneTranslationConfig( "Ped" );
			
			char cLogFilename[RAGE_MAX_PATH*2];
			sprintf_s( cLogFilename, RAGE_MAX_PATH, "%s\\log.ulog", m_outputPathEdit.Text.AsString() );
			
			ULOGGER_MOBO.PreExport( cLogFilename, "rexmbrage" );
			bool bRes = animExporter.LoadClipDataAllTakes( pAnimData );
			ULOGGER_MOBO.PostExport( !bRes );

			UIPopulate(pAnimData);

			delete pAnimData;
		}
	}
	else
	{
		atString ctrlFile("");
		if(rexMBAnimExportCommon::GetDefaultControlFile(ctrlFile) == true)
			m_ctrlFilePathEdit.Text = const_cast<char*>(ctrlFile.c_str());

		rexMBAnimExportCommon::LoadBoneTranslationConfig( "Ped" );
	}
}

//-----------------------------------------------------------------------------
//This event is called from the application when a file save has been requested
//but nothing has been saved yet.
void RexRageAnimExportTool::FileSave_Event(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	//Check to see if the persistent data object for the tool exists in the scene
	RexAnimMarkupData* pExportData = CollectAnimExportData(true);
	if ( pExportData )
	{
		UserObjects::SetAnimationMarkupData(pExportData);
		delete pExportData;
	}
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::FileSaveCompleted_Event(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	FBApplication app;

	char czFileName[MAX_PATH];
	ASSET.RemoveExtensionFromPath(czFileName,MAX_PATH,app.FBXFileName.AsString());

	// FBX File has not being saved to .fbx yet, its still in .tmp format.
	char czTmpFileName[MAX_PATH];
	sprintf(czTmpFileName, "%s.%s", czFileName, "tmp");

	FILE* pFile = fopen(czTmpFileName,"r");
	if(pFile)
	{
		char cHeader[5];
		fread(cHeader,4,1,pFile);
		cHeader[4] = '\0';

		if(stricmp(cHeader,"; FB") == 0)
		{
			FBMessageBox("Warning", "File is FBX ASCII Format", "OK");
		}

		fclose(pFile);
	}
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportTool::ValidateMarkupData(RexAnimMarkupData* pAnimData)
{
	ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("anim/validatemarkup", ""), "rexmbrage");

	bool bHasErrors = false;

	//Make sure the animation control file exists
	if(pAnimData->m_ctrlFilePath.GetLength())
	{
		atString animCtrlFilePath("");
		atString animCtrlFileFullPath("");

		
		rexMBAnimExportCommon::GetMotionBuilderAnimCtrlFilePathSetting( animCtrlFilePath );
		
		animCtrlFileFullPath = atString( animCtrlFilePath, pAnimData->m_ctrlFilePath.c_str() );
		int ctrlFileAttr = ASSET.GetAttributes(animCtrlFileFullPath.c_str(), "");
		if( ctrlFileAttr == FILE_ATTRIBUTE_INVALID )
		{
			ULOGGER_MOBO.SetProgressMessage("ERROR: Animation control file path '%s' doesn't exist!", pAnimData->m_ctrlFilePath.c_str());
			bHasErrors = true;
		}
	}
	
	//Validate that the root node models exist in the scene
	int nCharacters = pAnimData->GetNumCharacters();
	if(nCharacters)
	{
		vector< string > missingRootModels;
		vector< string > missingMoverModels;

		//Check to make sure each referenced model exists
		for(int charIndex = 0; charIndex < nCharacters; charIndex++)
		{
			RexCharAnimData* pCharData = pAnimData->GetCharacter(charIndex);
			char* szRootModelName = const_cast<char*>(pCharData->GetInputModelName(0));
			if(strlen(szRootModelName) && !RAGEFindModelByName(szRootModelName))
			{	
				ULOGGER_MOBO.SetProgressMessage("ERROR: The model '%s' is marked as a root node, but can't be located in the scene.", szRootModelName );
				bHasErrors = true;
				missingRootModels.push_back( szRootModelName );
			}

			//Check to make sure each referenced mover model exists.
			//
			char* szMoverModelName = const_cast<char*>(pCharData->GetMoverModelName(0));
			if(strlen(szMoverModelName) && !RAGEFindModelByName(szMoverModelName))
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: The model '%s' is marked as a mover node, but can't be located in the scene.", szMoverModelName );
				bHasErrors = true;
				missingMoverModels.push_back( szMoverModelName );
			}

			bool bFoundSkeletonFile = false;
			const char *szSkeletonFile = pCharData->GetSkeletonFile();
			if(stricmp(szSkeletonFile, "") == 0)
			{
				//HACK:  This is a hack to handle default skeleton files.
				//
				atString skeletonFile("");
				rexMBAnimExportCommon::GetDefaultSkeletonFile(skeletonFile);
				pCharData->m_skeleton = skeletonFile.c_str();
				bFoundSkeletonFile = true;
			}
			else
			{
				for(int fileIndex = 0; fileIndex < ms_skeletonFiles.GetCount(); fileIndex++)
				{
					if(stricmp(szSkeletonFile, ms_skeletonFiles[fileIndex].c_str()) == 0)
					{
						bFoundSkeletonFile = true;
						break;
					}
				}
			}

			if( bFoundSkeletonFile == false)
			{
				atString skeletonFile("");
				rexMBAnimExportCommon::GetDefaultSkeletonFile(skeletonFile);
				pCharData->m_skeleton = skeletonFile.c_str();

				ULOGGER_MOBO.SetProgressMessage("ERROR: Skeleton file '%s' for character '%s' doesn't exist!  Setting to default '%s'...", szSkeletonFile, szRootModelName, skeletonFile.c_str());
				bHasErrors = true;
			}

			const char *szAltOutputPath = pCharData->m_altOutputPath.c_str();
			if(stricmp(szAltOutputPath, EXPORTTOOL_UNSET_OUTPUT_PATH) != 0)
			{
				if (strlen(szAltOutputPath) != 0 )
				{		
					pCharData->m_altOutputPath = szAltOutputPath;
				}
				else
				{
					pCharData->m_altOutputPath = EXPORTTOOL_UNSET_OUTPUT_PATH;
				}
			}
		}

		//For the movers that could not be found, remove them from the animation markup
		for(vector<string>::iterator it = missingMoverModels.begin();
			it != missingMoverModels.end();
			++it)
		{
			pAnimData->RemoveMoverModel( (*it).c_str() );
		}

		//For the models that could not be found, remove them from the animation markup
		for(vector<string>::iterator it = missingRootModels.begin();
			it != missingRootModels.end();
			++it)
		{
			pAnimData->RemoveInputModel( (*it).c_str() );
		}

	}

	if(pAnimData->m_segmentFromTakes)
	{
		vector< string > missingTakes;

		//When "Takes" are being used to define the output animation segmentation, it is assumed
		//that there is one segment setup for each take.
		int nSegs = pAnimData->GetNumSegments();
		for(int segIdx=0; segIdx < nSegs; segIdx++)
		{
			RexMbAnimSegmentData* pFileSegmentData = pAnimData->GetSegment(segIdx);
			char* szTakeName = const_cast<char*>(pFileSegmentData->GetTakeName());

			// The takes attached to the segment do not match with an equality check which leads to dupe takes later down the line, we match the name
			// then if we find the take just re-set it. This stops AddMissingTakes to dupe as it will populate the UI and then add missing takes.
			HFBTake hTake;
			if(rexMBAnimExportCommon::FindTake(szTakeName, hTake) == false)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: The take '%s' is marked for export, but can't be located in the scene.", szTakeName );
				bHasErrors = true;
				missingTakes.push_back( szTakeName );
			}
			else
			{
				pFileSegmentData->m_take = hTake;
			}

			//Verify that the compression file is valid.
			bool bFoundCompressionFile = false;
			const char *szCompressionFilePath = pFileSegmentData->m_compressionFilePath.c_str();
			if(stricmp(szCompressionFilePath, "(no compression file)") == 0)
			{
				bFoundCompressionFile = true;
			}
			else
			{
				for(int fileIndex = 0; fileIndex < ms_compressionPathTemplates.GetCount(); fileIndex++)
				{
					if(stricmp(szCompressionFilePath, ms_compressionPathTemplates[fileIndex].c_str()) == 0)
					{
						bFoundCompressionFile = true;
						break;
					}
				}
			}

			if( bFoundCompressionFile == false)
			{
				atString compressionFile("");
				if(rexMBAnimExportCommon::GetDefaultCompressionFile(compressionFile) == true && compressionFile.length() != 0)
				{
					pFileSegmentData->m_compressionFilePath = compressionFile.c_str();
					ULOGGER_MOBO.SetProgressMessage("ERROR: Compression file '%s' for segment '%s' doesn't exist! Setting default to '%s'...", szCompressionFilePath, pFileSegmentData->m_takeName.c_str(), compressionFile.c_str());
				}
				else
				{
					ULOGGER_MOBO.SetProgressMessage("ERROR: Compression file '%s' for segment '%s' doesn't exist! No default has been specified to set to.", szCompressionFilePath, pFileSegmentData->m_takeName.c_str());
				}

				bHasErrors = true;
			}

			//Verify that the Control File is valid.
			bool bFoundControlFile = false;
			const char *szControlFilePath = pFileSegmentData->m_ctrlFilePath.c_str();
			if(stricmp(szControlFilePath, "(no control file)") == 0)
			{
				bFoundControlFile = true;
			}
			else
			{
				for(int fileIndex = 0; fileIndex < ms_ctrlPathTemplates.GetCount(); fileIndex++)
				{
					if(stricmp(szControlFilePath, ms_ctrlPathTemplates[fileIndex].c_str()) == 0)
					{
						bFoundControlFile = true;
						break;
					}
				}
			}

			if( bFoundControlFile == false)
			{
				atString ctrlFile("");
				if(rexMBAnimExportCommon::GetDefaultControlFile(ctrlFile) == true)
				{
					pFileSegmentData->m_ctrlFilePath = ctrlFile.c_str();
					ULOGGER_MOBO.SetProgressMessage("ERROR: Control file '%s' for segment '%s' doesn't exist! Setting default to '%s'...", szControlFilePath, pFileSegmentData->m_takeName.c_str(), ctrlFile.c_str());
				}
				else
				{
					ULOGGER_MOBO.SetProgressMessage("ERROR: Control file '%s' for segment '%s' doesn't exist! No default has been specified to set to.", szControlFilePath, pFileSegmentData->m_takeName.c_str());
				}

				bHasErrors = true;
			}
		}

		//For the takes that don't exist, remove them from the animation markup
		for(vector< string >::iterator it = missingTakes.begin();
			it != missingTakes.end();
			++it)
		{
			pAnimData->RemoveSegmentByTakeName( (*it).c_str() );
		}
	}

	if(bHasErrors)
	{
		ULOGGER_MOBO.PostExport();
	}

	return bHasErrors;
}

//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::DeriveDictionaryRelPathAndName(atString& outputPath, atString& relativeOutputPath, atString& dictionaryName, atString& rootDir)
{
	outputPath += "\\"; // add a slash as our dictionary names break if the artist doesnt add one to the output path
	atString tempOutputPath(outputPath);
	atArray<atString> tokensRelative;

	bool bUnderRoot = false;

	atArray<atString> rootAssetPaths;
	rexMBAnimExportCommon::GetProjectOrDlcIngameSourceExportDir(rootAssetPaths, atString(""));
	for( int j = 0; j < rootAssetPaths.GetCount(); ++j)
	{
		atString rootAssetPath(rootAssetPaths[j]);
		rootAssetPath.Replace("/", "\\");
		rootAssetPath.Lowercase();

		tempOutputPath.Replace("/", "\\");
		tempOutputPath.Lowercase();

		if(tempOutputPath.StartsWith(rootAssetPath))
		{
			tempOutputPath.Replace(rootAssetPath.c_str(), "");
			bUnderRoot = true;
		}
	}

	if(!bUnderRoot)
	{
		ULOGGER_MOBO.SetProgressMessage("ERROR: Output Path '%s' is not under expected path(s) '%s'", outputPath.c_str(), rexMBAnimExportCommon::ConcatenateStrings(rootAssetPaths, " , "));
		return false;
	}

	// Build the dictionary name from the relative path
	relativeOutputPath = tempOutputPath;
	relativeOutputPath.Split(tokensRelative, "\\", true);
	rootDir = tokensRelative[0];
	tempOutputPath.Replace( "\\", "" );
	dictionaryName = tempOutputPath;

	return true;
}

//-----------------------------------------------------------------------------
atString RexRageAnimExportTool::DeriveOutputPathFromSourceFbx()
{
	atArray<atString> fbxPathTokens;
	atArray<atString> rootAssetPaths;
	atString newOutputPath("");
	
	const char* szFBXPath = (const char*)m_app.FBXFileName;
	atString sFbxFilePath(szFBXPath);

	sFbxFilePath.Split( fbxPathTokens, "\\" );
	rexMBAnimExportCommon::GetMotionBuilderRootAssetPathSetting( rootAssetPaths );
	newOutputPath = rootAssetPaths[0];

	for( int i=5; i<(fbxPathTokens.GetCount()-1); i++ )
	{
		newOutputPath = atString(newOutputPath, fbxPathTokens[i]);
		newOutputPath = atString(newOutputPath, "\\");
	}

	return newOutputPath;
}

//-----------------------------------------------------------------------------
const char* RexRageAnimExportTool::GetExportPath()
{
	return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->m_outputPathEdit.Text;
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::SetExportPath( const char* pNewExportPath )
{
	RexRageAnimExportTool::GetCurrentAnimExportToolInst()->m_outputPathEdit.Text = const_cast<char*>(pNewExportPath);
}

//-----------------------------------------------------------------------------

void RexRageAnimExportTool::SetControlFile(const char * pControlFile )
{
	RexRageAnimExportTool::GetCurrentAnimExportToolInst()->m_ctrlFilePathEdit.Text = const_cast<char*>(pControlFile);
}

//-----------------------------------------------------------------------------

bool RexRageAnimExportTool::SetModelPairNamespace( int idx, const char* pNewNameSpace )
{

	for(ROWID_TO_MODELPAIR_MAP::iterator it = RexRageAnimExportTool::GetCurrentAnimExportToolInst()->m_rowIdToModelPairMap.begin();
		it != RexRageAnimExportTool::GetCurrentAnimExportToolInst()->m_rowIdToModelPairMap.end();
		++it)
	{
		int rowId = it->first;
		if( rowId == idx )
		{
			AetRootMoverPair* rootMoverPair = it->second;
			atString sRootModel(rootMoverPair->m_rootModel.c_str());
			atString sMoverModel(rootMoverPair->m_moverModel.c_str());

			atArray<atString> modelTokens;
			atArray<atString> moverTokens;
			sRootModel.Split(modelTokens, ":");
			sMoverModel.Split(moverTokens, ":");

			if( modelTokens.GetCount() > 0 )
			{
				sRootModel = atString(atString(pNewNameSpace), atString(":"));
				sRootModel = atString(sRootModel, modelTokens[modelTokens.GetCount()-1]);

				sMoverModel = atString(atString(pNewNameSpace), atString(":"));
				sMoverModel = atString(sMoverModel, moverTokens[modelTokens.GetCount()-1]);

				rootMoverPair->m_rootModel = sRootModel.c_str();
				rootMoverPair->m_moverModel = sMoverModel.c_str();

				RexRageAnimExportTool::GetCurrentAnimExportToolInst()->m_modelPairSheet.SetCell( idx, kRootModelName, const_cast<char*>(rootMoverPair->m_rootModel.c_str()));
				RexRageAnimExportTool::GetCurrentAnimExportToolInst()->m_modelPairSheet.SetCell( idx, kMoverModelName, const_cast<char*>(rootMoverPair->m_moverModel.c_str()));

				RexRageAnimExportTool::GetCurrentAnimExportToolInst()->UISetModelPairSheetIndices();
			}
			else
				return false;
		}
	}
	return true;
}

//-----------------------------------------------------------------------------

int RexRageAnimExportTool::GetModelCount()
{
	return (int) RexRageAnimExportTool::GetCurrentAnimExportToolInst()->m_rowIdToModelPairMap.size();
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::EditRootEntry(const char* rootName, const char* moverName, char* skeletonFile, char* modelType, char* controlFile, char* facialAttributeFile)
{
	if ( rootName == NULL || moverName == NULL)
		return false;

	HFBModel rootModel = RAGEFindModelByName(const_cast<char*>(rootName));
	if ( rootModel == NULL )
		return false;

	HFBModel moverModel = RAGEFindModelByName(const_cast<char*>(moverName));
	if ( moverModel == NULL )
		return false;

	//Ensure that the skeleton file exists.
	bool skeletonFileExists = false;
	for(int fileIndex = 0; fileIndex < ms_skeletonFiles.GetCount(); fileIndex++)
	{
		if(stricmp(skeletonFile, ms_skeletonFiles[fileIndex].c_str()) == 0)
		{
			skeletonFileExists = true;
			break;
		}
	}

	if ( skeletonFileExists == false )
		return false;

	atString modelTypeStr = atString( modelType );
	atString modelTypeComboStr = atString( s_ModelTypesStr );
	int modelTypeIdx = rexMBAnimExportCommon::ResolveComboIndexFromString( modelTypeStr, modelTypeComboStr );
	if ( modelTypeIdx == -1 )
		return false;

	bool controlFileExists = false;
	for(int fileIndex = 0; fileIndex < ms_ctrlPathTemplates.GetCount(); fileIndex++)
	{
		if ( stricmp(controlFile, ms_ctrlPathTemplates[fileIndex].c_str()) == 0)
		{
			controlFileExists = true;
			break;
		}
	}

	if ( controlFileExists == false )
		return false;

	const char* szRootModelName = (const char*)rootModel->LongName;
	const char* szMoverModelName = (const char*)moverModel->LongName;

	//If there is already an entry for the rootModel, then refrain from adding a new one.
	int rowId = 0;
	AetRootMoverPair* pModelPair = GetModelPairRow(szRootModelName, rowId);

	//If the entry didn't previously exist.
	if ( pModelPair == NULL )
	{
		pModelPair = new AetRootMoverPair();
		pModelPair->m_rootModel = szRootModelName;
		pModelPair->m_moverModel = szMoverModelName;
		pModelPair->m_animPrefix = "";
		pModelPair->m_animSuffix = "";
		pModelPair->m_skeletonFile = skeletonFile;
		pModelPair->m_modelType = modelType;
		pModelPair->m_facialAttribute = facialAttributeFile;
		pModelPair->m_charCtrlFile = controlFile;
		pModelPair->m_altOutputPath = EXPORTTOOL_UNSET_OUTPUT_PATH;
		pModelPair->m_modelExport = true;
		pModelPair->m_mergeFace = false;
		
		//Add a row to the model/mover spread sheet
		AddModelPairRow(pModelPair);

		//Set the newly added row as selected.  (m_iModelPairSpreadRowId is incremented in AddModelPairRow.)
		m_modelPairSheet.GetRow( m_iModelPairSpreadRowId-1 ).Selected = true;
		m_iModelPairSheetSelRowIndex = m_iModelPairSpreadRowId-1;
	}
	else 
	{
		pModelPair->m_rootModel = szRootModelName;
		pModelPair->m_moverModel = szMoverModelName;
		pModelPair->m_animPrefix = "";
		pModelPair->m_animSuffix = "";
		pModelPair->m_skeletonFile = skeletonFile;
		pModelPair->m_modelType = modelType;
		pModelPair->m_facialAttribute = facialAttributeFile;
		pModelPair->m_charCtrlFile = controlFile;

		UpdateModelPairRow(pModelPair, rowId);

		m_modelPairSheet.GetRow( rowId ).Selected = true;
		m_iModelPairSheetSelRowIndex = rowId;
	}

	UISetModelPairSheetIndices();
	return true;
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::SetExportModel(const char* rootName, bool value)
{
	if (rootName == NULL)
		return false;

	HFBModel rootModel = RAGEFindModelByName(const_cast<char*>(rootName));
	if (rootModel == NULL)
		return false;

	const char* szRootModelName = (const char*)rootModel->LongName;

	int rowId = 0;
	AetRootMoverPair* pModelPair = GetModelPairRow(szRootModelName, rowId);
	if (pModelPair == NULL)
	{
		return false;
	}

	pModelPair->m_modelExport = value;
	UpdateModelPairRow(pModelPair, rowId);
	return true;
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::SetMergeFacial(const char* rootName, bool value)
{
	if (rootName == NULL)
		return false;

	HFBModel rootModel = RAGEFindModelByName(const_cast<char*>(rootName));
	if (rootModel == NULL)
		return false;

	const char* szRootModelName = (const char*)rootModel->LongName;

	int rowId = 0;
	AetRootMoverPair* pModelPair = GetModelPairRow(szRootModelName, rowId);
	if (pModelPair == NULL)
	{
		return false;
	}

	pModelPair->m_mergeFace = value;
	UpdateModelPairRow(pModelPair, rowId);
	return true;
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::SetAltOutputPath(const char* rootName, const char* altOutputPath)
{
	if (rootName == NULL)
		return false;

	HFBModel rootModel = RAGEFindModelByName(const_cast<char*>(rootName));
	if (rootModel == NULL)
		return false;

	const char* szRootModelName = (const char*)rootModel->LongName;

	int rowId = 0;
	AetRootMoverPair* pModelPair = GetModelPairRow(szRootModelName, rowId);
	if (pModelPair == NULL)
	{
		return false;
	}

	pModelPair->m_altOutputPath = altOutputPath;
	UpdateModelPairRow(pModelPair, rowId);
	return true;
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::PopulateTakeExportInfo()
{
	m_segmentFromTakesButton.State = kFBButtonState1;
	SegmentFromTakesButton_Click(NULL, NULL);
	RefreshTakesButton_Click(NULL, NULL);
	EnableAllTakesButton_Click(NULL, NULL);
	return true;
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::SetTakeExportSetting(const char* takeName, bool setExport)
{
	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
		it != m_rowIdToTakeMap.end();
		++it)
	{
		int rowId = it->first;
		RexMbAnimSegmentData* pSegmentData = it->second;

		if ( stricmp(pSegmentData->GetTakeName(), takeName) == 0)
		{
			m_takeSheet.SetCell( rowId, kTakeExport, setExport );
			pSegmentData->m_bExport = setExport;
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::SetTakeAdditiveSetting(const char* takeName, bool setAdditive)
{
	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
		it != m_rowIdToTakeMap.end();
		++it)
	{
		int rowId = it->first;
		RexMbAnimSegmentData* pSegmentData = it->second;

		if ( stricmp(pSegmentData->GetTakeName(), takeName) == 0)
		{
			m_takeSheet.SetCell( rowId, kTakeAdditive, setAdditive );
			pSegmentData->m_bAdditive = setAdditive;
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::SetTakeControlFile(const char* takeName, const char* controlFile)
{
	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
		it != m_rowIdToTakeMap.end();
		++it)
	{
		int rowId = it->first;
		RexMbAnimSegmentData* pSegmentData = it->second;

		if ( stricmp(pSegmentData->GetTakeName(), takeName) == 0)
		{
			atString ctrlPathsStr("");
			atString controlFileStr(controlFile);
			rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( ms_ctrlPathTemplates, ctrlPathsStr );
			int ctrlFileIdx = rexMBAnimExportCommon::ResolveComboIndexFromString( controlFileStr, ctrlPathsStr );
			if ( ctrlFileIdx == -1 )
			{
				return false;
			}
			else
			{
				m_takeSheet.SetCell( rowId, kTakeCtrlFile, ctrlFileIdx );
				pSegmentData->m_ctrlFilePath = controlFile;
				return true;
			}
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
bool RexRageAnimExportTool::SetTakeCompressionFile(const char* takeName, const char* compressionFile)
{
	for(ROWID_TO_TAKE_MAP::iterator it = m_rowIdToTakeMap.begin();
		it != m_rowIdToTakeMap.end();
		++it)
	{
		int rowId = it->first;
		RexMbAnimSegmentData* pSegmentData = it->second;

		if ( stricmp(pSegmentData->GetTakeName(), takeName) == 0)
		{
			atString compressionPathsStr("");
			atString compressFileStr(compressionFile);
			rexMBAnimExportCommon::BuildSpreadSheetComboBoxStringFromArray( ms_ctrlPathTemplates, compressionPathsStr );
			int compressFileIdx = rexMBAnimExportCommon::ResolveComboIndexFromString( compressFileStr, compressionPathsStr );
			if ( compressFileIdx == -1 )
			{
				return false;
			}
			else
			{
				m_takeSheet.SetCell( rowId, kTakeCompressionFile, compressFileIdx );
				pSegmentData->m_compressionFilePath = compressionFile;
				return true;
			}
		}
	}

	return false;
}
//-------------------------------------------------
void RexRageAnimExportTool::AddAdditionalModelEntry( const char* pTargetModelName, const char* pModelName, const char* pTrackName, const char* pAttributeName )
{
	for(ROWID_TO_MODELPAIR_MAP::iterator it = m_rowIdToModelPairMap.begin();
		it != m_rowIdToModelPairMap.end();
		++it)
	{
		AetRootMoverPair* pModelPair = it->second;

		if(pModelPair->m_modelType != "Body" ||
			(stricmp(pModelPair->m_rootModel.c_str(), pTargetModelName) != 0)) continue;

		AetAdditionalModel* pAdditionalModel = rage_new AetAdditionalModel();
		pAdditionalModel->m_modelName = pModelName;
		pAdditionalModel->m_trackType = pTrackName;
		pAdditionalModel->m_attributeName = pAttributeName;

		bool found=false;

		for(int i=0; i < pModelPair->m_additionalModels.GetCount(); ++i)
		{
			if(*pAdditionalModel == *pModelPair->m_additionalModels[i])
				found=true;
		}

		if(found == false)
			pModelPair->m_additionalModels.PushAndGrow(pAdditionalModel);
	}
}