#pragma once
#include "partpopup.h"
#include "editpopup.h"
#include "RemoteConsoleInterface.h"
#include "cutsceneExportTool.h"

using namespace rage;

//-----------------------------------------------------------------------------

PartPopup::~PartPopup()
{
	if ( m_pEditEventPopup != NULL )
	{
		m_pEditEventPopup->Close();
		m_pEditEventPopup->Shutdown();
		m_pEditEventPopup = NULL;
	}
}

//-----------------------------------------------------------------------------

void PartPopup::UICreate()
{
	Region.X = 200;
	Region.Y = 200;
	Region.Width = 680;
	Region.Height = 265;

	AddRegion( "CutfileInfoOptionsLayout", "CutfileInfoOptionsLayout", 
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoLayoutRegion", 1.0f,
		rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0f,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0f,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, "CutfileInfoLayoutRegion", 1.0f );    
	SetControl( "CutfileInfoOptionsLayout", m_cutfileInfoOptionsLayout );

	// CutfileInfoOptionsLayoutRegion
	m_cutfileInfoOptionsLayout.AddRegion( "CutfileInfoOptionsLayoutRegion", "CutfileInfoOptionsLayoutRegion",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoLayoutRegion", 1.0f,
		rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0f,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0f,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, "CutfileInfoLayoutRegion", 1.0f );
	m_cutfileInfoOptionsLayout.SetControl( "CutfileInfoOptionsLayoutRegion", m_cutfileInfoOptionsLayoutRegion );

	int iAttachTopY = rexMBAnimExportCommon::c_iMargin * 2;
	int iColumn2of2AttachLeftX = (635 / 3) * 2;
	int iColumn2Of3AttachLeftX = 635 / 2;
	int iColumn3of3AttachLeftX = (635 / 4) * 3;

	   // Range
	   m_cutfileInfoOptionsLayout.AddRegion( "RangeLabel","RangeLabel",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iLabelWidth/2, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "RangeLabel", m_rangeLabel );

	   m_cutfileInfoOptionsLayout.AddRegion( "RangeStartNumericUpDown", "RangeStartNumericUpDown",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "RangeLabel", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "RangeStartNumericUpDown", m_rangeStartNumericUpDown );

	   m_cutfileInfoOptionsLayout.AddRegion( "RangeEndNumericUpDown", "RangeEndNumericUpDown",
	       0, kFBAttachRight, "RangeStartNumericUpDown", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "RangeEndNumericUpDown", m_rangeEndNumericUpDown );   

	m_cutfileInfoOptionsLayout.AddRegion( "RangeNormalEndNumericUpDown", "RangeNormalEndNumericUpDown",
		0, kFBAttachRight, "RangeEndNumericUpDown", 1.0,
		iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoOptionsLayout.SetControl( "RangeNormalEndNumericUpDown", m_rangeNormalEndNumericUpDown ); 

	   // Edit Scene Attributes
	   m_cutfileInfoOptionsLayout.AddRegion( "EditSceneAttributesButton", "EditSceneAttributesButton",
	       rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, "RangeNormalEndNumericUpDown", 1.0f,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0f,
	       rexMBAnimExportCommon::c_iWideButtonWidth-3, kFBAttachNone, NULL, 1.0f,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
	   m_cutfileInfoOptionsLayout.SetControl( "EditSceneAttributesButton", m_editScenePropertiesButton );

	   // Cleanup CutFile
	   m_cutfileInfoOptionsLayout.AddRegion( "CleanupCutFileButton", "CleanupCutFileButton",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EditSceneAttributesButton", 1.0f,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0f,
	       rexMBAnimExportCommon::c_iWideButtonWidth-3, kFBAttachNone, NULL, 1.0f,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
	   m_cutfileInfoOptionsLayout.SetControl( "CleanupCutFileButton", m_cleanupCutFileButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	   // Use Parent Scale
	   m_cutfileInfoOptionsLayout.AddRegion( "UseParentScaleCheckBox", "UseParentScaleCheckBox",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       175, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "UseParentScaleCheckBox", m_useParentScaleCheckBox );    

	   // Disable DOF
	   m_cutfileInfoOptionsLayout.AddRegion( "UseInGameDOFEnd", "UseInGameDOFEnd",
	       iColumn2Of3AttachLeftX, kFBAttachLeft, "NULL", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       150, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "UseInGameDOFEnd", m_useInGameDOFEndCheckBox );

	   // Use Story Mode
	   m_cutfileInfoOptionsLayout.AddRegion( "UseStoryModeCheckBox", "UseStoryModeCheckBox",
	       iColumn3of3AttachLeftX, kFBAttachLeft, "NULL", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       150, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "UseStoryModeCheckBox", m_useStoryMode );

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	   // Use Audio Events In Concat
	   m_cutfileInfoOptionsLayout.AddRegion( "AudioEventsInConcatCheckBox", "AudioEventsInConcatCheckBox",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       175, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "AudioEventsInConcatCheckBox", m_useAudioEventsInConcatCheckBox );

	// Use InGame DOF Start
	m_cutfileInfoOptionsLayout.AddRegion( "UseInGameDOFStart", "UseInGameDOFStart",
		iColumn2Of3AttachLeftX, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		150, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoOptionsLayout.SetControl( "UseInGameDOFStart", m_useInGameDOFStartCheckBox );

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	   // Camera Catchup
	   m_cutfileInfoOptionsLayout.AddRegion( "CameraCatchUpCheckbox", "CameraCatchUpCheckbox",
		   rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		   225, kFBAttachNone, NULL, 1.0,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "CameraCatchUpCheckbox", m_useCatchUpCameraCheckbox );

	   // Use InGame DOF Start 2nd cut
	   m_cutfileInfoOptionsLayout.AddRegion( "UseInGameDOFSecondCutStart", "UseInGameDOFSecondCutStart",
		   iColumn2Of3AttachLeftX, kFBAttachLeft, "NULL", 1.0,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		   150, kFBAttachNone, NULL, 1.0,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "UseInGameDOFSecondCutStart", m_useInGameDOFSecondCutStartCheckBox );

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	   // Fade Out Game (at beginning)
	   m_cutfileInfoOptionsLayout.AddRegion( "FadeOutGameAtBeginningCheckBox", "FadeOutGameAtBeginningCheckBox",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       200, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeOutGameAtBeginningCheckBox", m_fadeOutGameAtBeginningCheckBox );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeOutGameAtBeginningDurationNumericUpDown", "FadeOutGameAtBeginningDurationNumericUpDown",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FadeOutGameAtBeginningCheckBox", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeOutGameAtBeginningDurationNumericUpDown", m_fadeOutGameAtBeginningDurationNumericUpDown );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeOutGameAtBeginningDurationLabel", "FadeOutGameAtBeginningDurationLabel",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FadeOutGameAtBeginningDurationNumericUpDown", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeOutGameAtBeginningDurationLabel", m_fadeOutGameAtBeginningDurationLabel );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeAtBeginningEditColor","FadeAtBeginningEditColor",
	       iColumn2of2AttachLeftX, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iColorWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeAtBeginningEditColor", m_fadeAtBeginningEditColor ); 

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	   // Fade In Cutscene (at beginning)
	   m_cutfileInfoOptionsLayout.AddRegion( "FadeInCutsceneAtBeginningCheckBox", "FadeInCutsceneAtBeginningCheckBox",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       200, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeInCutsceneAtBeginningCheckBox", m_fadeInCutsceneAtBeginningCheckBox );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeInCutsceneAtBeginningDurationNumericUpDown", "FadeInCutsceneAtBeginningDurationNumericUpDown",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FadeInCutsceneAtBeginningCheckBox", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeInCutsceneAtBeginningDurationNumericUpDown", m_fadeInCutsceneAtBeginningDurationNumericUpDown );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeInCutsceneAtBeginningDurationLabel", "FadeInCutsceneAtBeginningDurationLabel",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FadeInCutsceneAtBeginningDurationNumericUpDown", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeInCutsceneAtBeginningDurationLabel", m_fadeInCutsceneAtBeginningDurationLabel );

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	   // Fade Out Cutscene (at end)
	   m_cutfileInfoOptionsLayout.AddRegion( "FadeOutCutsceneAtEndCheckBox","FadeOutCutsceneAtEndCheckBox",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       200, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeOutCutsceneAtEndCheckBox", m_fadeOutCutsceneAtEndCheckBox );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeOutCutsceneAtEndDurationNumericUpDown","FadeOutCutsceneAtEndDurationNumericUpDown",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FadeOutCutsceneAtEndCheckBox", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeOutCutsceneAtEndDurationNumericUpDown", m_fadeOutCutsceneAtEndDurationNumericUpDown );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeOutCutsceneAtEndDurationLabel","FadeOutCutsceneAtEndDurationLabel",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FadeOutCutsceneAtEndDurationNumericUpDown", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeOutCutsceneAtEndDurationLabel", m_fadeOutCutsceneAtEndDurationLabel );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeAtEndEditColor","FadeAtEndEditColor",
	       iColumn2of2AttachLeftX, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iColorWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeAtEndEditColor", m_fadeAtEndEditColor ); 

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	   // Fade In Game (at end)
	   m_cutfileInfoOptionsLayout.AddRegion( "FadeInGameAtEndCheckBox","FadeInGameAtEndCheckBox",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       200, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeInGameAtEndCheckBox", m_fadeInGameAtEndCheckBox );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeInGameAtEndDurationNumericUpDown","FadeInGameAtEndDurationNumericUpDown",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FadeInGameAtEndCheckBox", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeInGameAtEndDurationNumericUpDown", m_fadeInGameAtEndDurationNumericUpDown );

	   m_cutfileInfoOptionsLayout.AddRegion( "FadeInGameAtEndDurationLabel","FadeInGameAtEndDurationLabel",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FadeInGameAtEndDurationNumericUpDown", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "FadeInGameAtEndDurationLabel", m_fadeInGameAtEndDurationLabel );

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	   // Blend out (at end)
	   m_cutfileInfoOptionsLayout.AddRegion( "BlendOutGameAtEndCheckBox","BlendOutGameAtEndCheckBox",
		   rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		   200, kFBAttachNone, NULL, 1.0,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "BlendOutGameAtEndCheckBox", m_blendOutGameAtEndCheckBox );

	   m_cutfileInfoOptionsLayout.AddRegion( "BlendOutGameAtEndDurationNumericUpDown","BlendOutGameAtEndDurationNumericUpDown",
		   rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "BlendOutGameAtEndCheckBox", 1.0,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		   rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "BlendOutGameAtEndDurationNumericUpDown", m_blendOutGameAtEndDurationNumericUpDown );

	   m_cutfileInfoOptionsLayout.AddRegion( "BlendOutGameAtEndDurationLabel","BlendOutGameAtEndDurationLabel",
		   rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "BlendOutGameAtEndDurationNumericUpDown", 1.0,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		   rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "BlendOutGameAtEndDurationLabel", m_blendOutGameAtEndDurationLabel );

	   m_cutfileInfoOptionsLayout.AddRegion( "BlendOutGameAtEndOffsetNumericUpDown","BlendOutGameAtEndOffsetNumericUpDown",
		   iColumn2Of3AttachLeftX, kFBAttachLeft, "NULL", 1.0,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		   rexMBAnimExportCommon::c_iNumericUpDownWidth, kFBAttachNone, NULL, 1.0,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "BlendOutGameAtEndOffsetNumericUpDown", m_blendOutGameAtEndOffsetNumericUpDown );

	   m_cutfileInfoOptionsLayout.AddRegion( "BlendOutGameAtEndOffsetLabel","BlendOutGameAtEndOffsetLabel",
		   rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "BlendOutGameAtEndOffsetNumericUpDown", 1.0,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
		   rexMBAnimExportCommon::c_iShortLabelWidth, kFBAttachNone, NULL, 1.0,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "BlendOutGameAtEndOffsetLabel", m_blendOutGameAtEndOffsetLabel );

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;
	   
	   // Sectioning
	   m_cutfileInfoOptionsLayout.AddRegion( "SectioningMethodLabel", "SectioningMethodLabel",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "SectioningMethodLabel", m_sectioningMethodLabel );

	   m_cutfileInfoOptionsLayout.AddRegion( "SectioningMethodList", "SectioningMethodList",
	       rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SectioningMethodLabel", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iLabelWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iNumericUpDownWidth - (rexMBAnimExportCommon::c_iMargin*2), kFBAttachRight, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "SectioningMethodList", m_sectioningMethodComboBox );

	   m_cutfileInfoOptionsLayout.AddRegion( "SectionDurationNumericUpDown", "SectionDurationNumericUpDown",
	       -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iLabelWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iNumericUpDownWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoOptionsLayoutRegion", 1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iNumericUpDownWidth+10, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "SectionDurationNumericUpDown", m_sectionDurationNumericUpDown );

	   m_cutfileInfoOptionsLayout.AddRegion( "SectionDurationLabel", "SectionDurationLabel",
	       /*-rexMBAnimExportCommon::c_iMargin*/ - rexMBAnimExportCommon::c_iLabelWidth, kFBAttachRight, "CutfileInfoOptionsLayoutRegion",	1.0,
	       iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0,
	       rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
	       rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	   m_cutfileInfoOptionsLayout.SetControl( "SectionDurationLabel", m_sectionDurationLabel );

	   iAttachTopY += rexMBAnimExportCommon::c_iRowHeight + rexMBAnimExportCommon::c_iMargin;

	   // Cancel
	   m_cutfileInfoOptionsLayout.AddRegion( "CancelButton", "CancelButton",
		   -rexMBAnimExportCommon::c_iMargin - (rexMBAnimExportCommon::c_iWideButtonWidth/2), kFBAttachRight, "NULL", 1.0f,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0f,
		   rexMBAnimExportCommon::c_iWideButtonWidth/2, kFBAttachNone, NULL, 1.0f,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
	   m_cutfileInfoOptionsLayout.SetControl( "CancelButton", m_cancelButton );

	   // Ok
	   m_cutfileInfoOptionsLayout.AddRegion( "OKButton", "OKButton",
		   -rexMBAnimExportCommon::c_iMargin - (rexMBAnimExportCommon::c_iWideButtonWidth/2), kFBAttachLeft, "CancelButton", 1.0f,
		   iAttachTopY, kFBAttachTop, "CutfileInfoOptionsLayoutRegion", 1.0f,
		   rexMBAnimExportCommon::c_iWideButtonWidth/2, kFBAttachNone, NULL, 1.0f,
		   rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
	   m_cutfileInfoOptionsLayout.SetControl( "OKButton", m_okButton );
}

//-----------------------------------------------------------------------------

void PartPopup::OKButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	UpdateCutfileFromUI();
	Close();
}

//-----------------------------------------------------------------------------

void PartPopup::CloseButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	UpdateCutfileFromUI();
	Close();
}

//-----------------------------------------------------------------------------

void PartPopup::UIConfigure()
{
	Caption = "Edit Part";

	// CutfileInfoLayoutRegion
	m_cutfileInfoOptionsLayout.SetRegionTitle( "CutfileInfoOptionsLayoutRegion", "Options" );
	m_cutfileInfoOptionsLayout.SetBorder( "CutfileInfoOptionsLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

	// Edit Scene Attributes
	m_editScenePropertiesButton.Caption = "Edit Scene Properties";
	m_editScenePropertiesButton.OnClick.Add( this, (FBCallback)&PartPopup::EditScenePropertiesButton_Click );

	// Cleanup Cutfile
	m_cleanupCutFileButton.Caption = "Cleanup CutFile";
	m_cleanupCutFileButton.Enabled = false;
	m_cleanupCutFileButton.OnClick.Add( this, (FBCallback)&PartPopup::CleanupCutFileButton_Click );

	// OK
	m_okButton.Caption = "OK";
	m_okButton.Enabled = true;
	m_okButton.OnClick.Add( this, (FBCallback)&PartPopup::OKButton_Click );

	// Cancel
	m_cancelButton.Caption = "Cancel";
	m_cancelButton.Enabled = true;
	m_cancelButton.OnClick.Add( this, (FBCallback)&PartPopup::CloseButton_Click );

	// Range
	m_rangeLabel.Caption = "Range:";
	m_rangeLabel.Justify = kFBTextJustifyLeft;

	m_rangeStartNumericUpDown.Min = 0;
	m_rangeStartNumericUpDown.Precision = 0;

	m_rangeEndNumericUpDown.Min = 0;
	m_rangeEndNumericUpDown.Precision = 0;

	m_rangeNormalEndNumericUpDown.Min = 0;
	m_rangeNormalEndNumericUpDown.Precision = 0;
	m_rangeNormalEndNumericUpDown.Enabled = false;

	// Fade Out Game (at beginning)
	m_fadeOutGameAtBeginningCheckBox.Caption = "Fade Out Game (at beginning):";
	m_fadeOutGameAtBeginningCheckBox.OnClick.Add( this, (FBCallback)&PartPopup::FadeOutGameAtBeginningCheckBox_Click );
	m_fadeOutGameAtBeginningCheckBox.Style = kFBCheckbox;
	m_fadeOutGameAtBeginningCheckBox.State = 1;

	m_fadeOutGameAtBeginningDurationNumericUpDown.Min = 0;
	m_fadeOutGameAtBeginningDurationNumericUpDown.Enabled = true;

	m_fadeOutGameAtBeginningDurationLabel.Caption = "seconds";
	m_fadeOutGameAtBeginningDurationLabel.Enabled = true;

	m_fadeAtBeginningEditColor.ColorMode = 3;

	// Fade In Cutscene (at beginning)
	m_fadeInCutsceneAtBeginningCheckBox.Caption = "Fade In Cutscene (at beginning):";
	m_fadeInCutsceneAtBeginningCheckBox.OnClick.Add( this, (FBCallback)&PartPopup::FadeInCutsceneAtBeginningCheckBox_Click );
	m_fadeInCutsceneAtBeginningCheckBox.Style = kFBCheckbox;
	m_fadeInCutsceneAtBeginningCheckBox.State = 1;

	m_fadeInCutsceneAtBeginningDurationNumericUpDown.Min = 0;
	m_fadeInCutsceneAtBeginningDurationNumericUpDown.Enabled = true;

	m_fadeInCutsceneAtBeginningDurationLabel.Caption = "seconds";
	m_fadeInCutsceneAtBeginningDurationLabel.Enabled = true;

	m_fadeAtBeginningEditColor.ColorMode = 3;

	// Fade Out Cutscene (at end)
	m_fadeOutCutsceneAtEndCheckBox.Caption = "Fade Out Cutscene (at end):";
	m_fadeOutCutsceneAtEndCheckBox.OnClick.Add( this, (FBCallback)&PartPopup::FadeOutCutsceneAtEndCheckBox_Click );
	m_fadeOutCutsceneAtEndCheckBox.Style = kFBCheckbox;
	m_fadeOutCutsceneAtEndCheckBox.State = 1;

	m_fadeOutCutsceneAtEndDurationNumericUpDown.Min = 0;
	m_fadeOutCutsceneAtEndDurationNumericUpDown.Enabled = true;

	m_fadeOutCutsceneAtEndDurationLabel.Caption = "seconds";
	m_fadeOutCutsceneAtEndDurationLabel.Enabled = true;

	m_fadeAtEndEditColor.ColorMode = 3;

	// Fade In Game (at end)
	m_fadeInGameAtEndCheckBox.Caption = "Fade In Game (at end):";
	m_fadeInGameAtEndCheckBox.OnClick.Add( this, (FBCallback)&PartPopup::FadeInGameAtEndCheckBox_Click );
	m_fadeInGameAtEndCheckBox.Style = kFBCheckbox;
	m_fadeInGameAtEndCheckBox.State = 1;

	m_fadeInGameAtEndDurationNumericUpDown.Min = 0;
	m_fadeInGameAtEndDurationNumericUpDown.Enabled = true;

	m_fadeInGameAtEndDurationLabel.Caption = "seconds";
	m_fadeInGameAtEndDurationLabel.Enabled = true;

	m_fadeAtEndEditColor.ColorMode = 3;

	m_blendOutGameAtEndCheckBox.Style = kFBCheckbox;
	m_blendOutGameAtEndCheckBox.Caption = "Camera Blend Out (at end):";
	m_blendOutGameAtEndCheckBox.OnClick.Add( this, (FBCallback)&PartPopup::BlendOutGameAtEndCheckBox_Click );
	m_blendOutGameAtEndDurationNumericUpDown.Min = 0;
	m_blendOutGameAtEndDurationNumericUpDown.Precision = 0;
	m_blendOutGameAtEndDurationNumericUpDown.OnChange.Add( this, (FBCallback)&PartPopup::BlendOutDuration_Change );
	m_blendOutGameAtEndDurationLabel.Caption = "duration";

	m_blendOutGameAtEndOffsetNumericUpDown.Min = 0;
	m_blendOutGameAtEndOffsetNumericUpDown.Precision = 0;
	m_blendOutGameAtEndOffsetNumericUpDown.OnChange.Add( this, (FBCallback)&PartPopup::BlendOutOffset_Change );
	m_blendOutGameAtEndOffsetLabel.Caption = "offset";

	// Use One Audio
	m_useStoryMode.Caption = "Use Story Mode";
	m_useStoryMode.Style = kFBCheckbox;

	// Use Parent Scale
	m_useParentScaleCheckBox.Caption = "Use Parent Scale";
	m_useParentScaleCheckBox.Style = kFBCheckbox;

	// Use Camera Catchup
	m_useCatchUpCameraCheckbox.Caption = "Use Catchup Camera (in game)";
	m_useCatchUpCameraCheckbox.Style = kFBCheckbox;

	m_useInGameDOFSecondCutStartCheckBox.Caption = "Enable DOF (2nd cut)";
	m_useInGameDOFSecondCutStartCheckBox.Style = kFBCheckbox;
	m_useInGameDOFSecondCutStartCheckBox.OnClick.Add( this, (FBCallback)&PartPopup::UseInGameDOFSecondCutStart_SelectionChanged );

	m_useInGameDOFStartCheckBox.Caption = "Enable DOF (1st cut)";
	m_useInGameDOFStartCheckBox.Style = kFBCheckbox;
	m_useInGameDOFStartCheckBox.OnClick.Add( this, (FBCallback)&PartPopup::UseInGameDOFStart_SelectionChanged );

	m_useInGameDOFEndCheckBox.Caption = "Disable DOF (last cut)";
	m_useInGameDOFEndCheckBox.Style = kFBCheckbox;

	m_useAudioEventsInConcatCheckBox.Caption = "Use Audio Events In Concat";
	m_useAudioEventsInConcatCheckBox.Style = kFBCheckbox;

	// Sectioning
	m_sectioningMethodLabel.Caption = "Split Animations Into Sections:";
	m_sectioningMethodLabel.Justify = kFBTextJustifyRight;

	m_sectioningMethodComboBox.Style = kFBDropDownList;
	m_sectioningMethodComboBox.ExtendedSelect = true;
	m_sectioningMethodComboBox.OnChange.Add( this, (FBCallback)&PartPopup::SectioningMethodComboBox_SelectionChanged );

	m_sectionDurationNumericUpDown.Min = 0;
	m_sectionDurationNumericUpDown.Precision = 0.0;
	m_sectionDurationNumericUpDown.Value = 8.0f;
	m_sectionDurationNumericUpDown.OnChange.Add( this, (FBCallback)&PartPopup::ValidateSectionTime_Click );

	m_sectionDurationLabel.Caption = "seconds each";

	Setup();
}

//-----------------------------------------------------------------------------

void PartPopup::FadeOutGameAtBeginningCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	SetFadeOutGameAtBeginning( m_fadeOutGameAtBeginningCheckBox.State == 1 );

	if(m_fadeInCutsceneAtBeginningCheckBox.State != 1)
	{
		m_fadeInCutsceneAtBeginningCheckBox.State = 1;
		m_fadeInCutsceneAtBeginningDurationNumericUpDown.Enabled = true;
		m_fadeInCutsceneAtBeginningDurationLabel.Enabled = true;
	}
	else
	{
		m_fadeInCutsceneAtBeginningCheckBox.State = 0;
		m_fadeInCutsceneAtBeginningDurationNumericUpDown.Enabled = false;
		m_fadeInCutsceneAtBeginningDurationLabel.Enabled = false;
	}
}

//-----------------------------------------------------------------------------

void PartPopup::FadeInCutsceneAtBeginningCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	SetFadeInCutsceneAtBeginning( m_fadeInCutsceneAtBeginningCheckBox.State == 1 );

	if(m_fadeOutGameAtBeginningCheckBox.State != 1)
	{
		m_fadeOutGameAtBeginningCheckBox.State = 1;
		m_fadeOutGameAtBeginningDurationNumericUpDown.Enabled = true;
		m_fadeOutGameAtBeginningDurationLabel.Enabled = true;
	}
	else
	{
		m_fadeOutGameAtBeginningCheckBox.State = 0;
		m_fadeOutGameAtBeginningDurationNumericUpDown.Enabled = false;
		m_fadeOutGameAtBeginningDurationLabel.Enabled = false;
	}
}

//-----------------------------------------------------------------------------

void PartPopup::FadeOutCutsceneAtEndCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	SetFadeOutCutsceneAtEnd( m_fadeOutCutsceneAtEndCheckBox.State == 1 );

	if(m_fadeInGameAtEndCheckBox.State != 1)
	{
		m_fadeInGameAtEndCheckBox.State = 1;
		m_fadeInGameAtEndDurationNumericUpDown.Enabled = true;
		m_fadeInGameAtEndDurationLabel.Enabled = true;
	}
	else
	{
		m_fadeInGameAtEndCheckBox.State = 0;
		m_fadeInGameAtEndDurationNumericUpDown.Enabled = false;
		m_fadeInGameAtEndDurationLabel.Enabled = false;
	}
}

//-----------------------------------------------------------------------------

void PartPopup::FadeInGameAtEndCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	SetFadeInGameAtEnd( m_fadeInGameAtEndCheckBox.State == 1 );

	if(m_fadeOutCutsceneAtEndCheckBox.State != 1)
	{
		m_fadeOutCutsceneAtEndCheckBox.State = 1;
		m_fadeOutCutsceneAtEndDurationNumericUpDown.Enabled = true;
		m_fadeOutCutsceneAtEndDurationLabel.Enabled = true;
	}
	else
	{
		m_fadeOutCutsceneAtEndCheckBox.State = 0;
		m_fadeOutCutsceneAtEndDurationNumericUpDown.Enabled = false;
		m_fadeOutCutsceneAtEndDurationLabel.Enabled = false;
	}

}

//-----------------------------------------------------------------------------

void PartPopup::BlendOutGameAtEndCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_blendOutGameAtEndCheckBox.State == 1)
	{
		m_blendOutGameAtEndDurationNumericUpDown.Enabled = true;
		m_blendOutGameAtEndDurationLabel.Enabled = true;
		m_blendOutGameAtEndOffsetNumericUpDown.Enabled = true;
		m_blendOutGameAtEndOffsetLabel.Enabled = true;
	}
	else
	{
		m_blendOutGameAtEndDurationNumericUpDown.Enabled = false;
		m_blendOutGameAtEndDurationLabel.Enabled = false;
		m_blendOutGameAtEndOffsetNumericUpDown.Enabled = false;
		m_blendOutGameAtEndOffsetLabel.Enabled = false;

		m_blendOutGameAtEndDurationNumericUpDown.Value = 0;
		m_blendOutGameAtEndOffsetNumericUpDown.Value = 0;
	}
}

//-----------------------------------------------------------------------------

void PartPopup::BlendOutDuration_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_blendOutGameAtEndOffsetNumericUpDown.Value = (float)m_blendOutGameAtEndDurationNumericUpDown.Value;
}

//-----------------------------------------------------------------------------

void PartPopup::BlendOutOffset_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	m_blendOutGameAtEndDurationNumericUpDown.Value = (float)m_blendOutGameAtEndOffsetNumericUpDown.Value;
}

//-----------------------------------------------------------------------------

void PartPopup::ValidateSectionTime_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_sectionDurationNumericUpDown.Value < MINIMUM_CUTSCENE_SECTION_TIME)
	{
		char msg[RAGE_MAX_PATH];
		sprintf( msg, "Section time %f is less than the minimum section time limit %d.", (float)m_sectionDurationNumericUpDown.Value, MINIMUM_CUTSCENE_SECTION_TIME);

		FBMessageBox( "Error", msg, "OK" );
		m_sectionDurationNumericUpDown.Value = MINIMUM_CUTSCENE_SECTION_TIME;
	}
}

//-----------------------------------------------------------------------------

void PartPopup::SectioningMethodComboBox_SelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	SetSectioningMethod( m_sectioningMethodComboBox.ItemIndex );
}

void PartPopup::UseInGameDOFStart_SelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_useInGameDOFStartCheckBox.State == kFBButtonState1)
	{
		m_useInGameDOFSecondCutStartCheckBox.State = kFBButtonState0;
	}
}

void PartPopup::UseInGameDOFSecondCutStart_SelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(m_useInGameDOFSecondCutStartCheckBox.State == kFBButtonState1)
	{
		m_useInGameDOFStartCheckBox.State = kFBButtonState0;
	}
}

//-----------------------------------------------------------------------------

void PartPopup::PopulateSectioningMethodNames()
{
	m_sectioningMethodNames.Reserve( NUM_SECTIONING_METHODS );
	m_sectioningMethodNames.Append() = "(none)";
	m_sectioningMethodNames.Append() = "by Time Slices of";
	m_sectioningMethodNames.Append() = "by Camera Cuts";
	m_sectioningMethodNames.Append() = "by Section Split Property";
}

//-----------------------------------------------------------------------------

void PartPopup::LoadEventDefsFile()
{
	atString eventDefsPath;
	if ( rexMBAnimExportCommon::GetMotionBuilderEventDefsPathSetting( eventDefsPath ) && ASSET.Exists( eventDefsPath.c_str(), NULL ) )
	{
		m_eventDefs.LoadFile( eventDefsPath.c_str() );
	}
}

//-----------------------------------------------------------------------------

void PartPopup::Setup()
{
	LoadEventDefsFile();
	PopulateSectioningMethodNames();
	SetFadeOutGameAtBeginning( false );
	SetFadeInCutsceneAtBeginning( false );
	SetFadeOutCutsceneAtEnd( false );
	SetFadeInGameAtEnd( false );
	SetUseBlendOutCamera(false);

	m_cSceneName[0] = 0;
	m_vSceneOffset.Zero();
	m_fSceneRotation = 0.0f;
	m_vTriggerOffset.Zero();

	m_sectioningMethodComboBox.Items.Clear();
	for ( int i = 0; i < m_sectioningMethodNames.GetCount(); ++i )
	{
		m_sectioningMethodComboBox.Items.Add( const_cast<char *>( m_sectioningMethodNames[i].c_str() ) );
	}

	m_pEditEventPopup = NULL;

	SetSectioningMethod( 0 );
}

//-----------------------------------------------------------------------------

void PartPopup::ShowDialog(RexRageCutscenePart* pPart, RexRageCutsceneExportTool* pExport)
{
	m_pPart = pPart;
	m_pExport = pExport;

	char cCaption[RAGE_MAX_PATH];
	sprintf_s(cCaption, RAGE_MAX_PATH, "Edit Part (%s)", m_pPart->GetName());
	Caption = cCaption;

	UpdateUIFromCutfile();

	Show();
}

//-----------------------------------------------------------------------------

void PartPopup::CleanupCutFileButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	int iResult = FBMessageBox( "Cleanup CutFile", "Are you sure you want to cleanup unused data from the cutfile?  This operation cannot be undone.",
		"Yes", "No" );
	if ( iResult == 1 )
	{
		m_pPart->GetCutsceneFile()->CleanupUnusedEventArgs();

		m_cleanupCutFileButton.Enabled = false;
	}
}

//-----------------------------------------------------------------------------

void PartPopup::EditScenePropertiesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	EditSceneAttributesPopup popup;
	popup.UIInit();
	popup.Modal = true;

	popup.SetSettings( GetSceneOffset(), GetTriggerOffset(), GetSceneRotation(), m_sceneAttributeList );

	if ( popup.Show( this ) )
	{
		SetSceneOffset( popup.GetSceneOffset() );
		SetTriggerOffset( popup.GetTriggerOffset() );
		SetSceneRotation( popup.GetSceneRotation() );
		m_sceneAttributeList.CopyFrom( popup.GetAttributes() );

		UpdateCutfileFromUI();

		m_pExport->SaveToPersistentData();
		RemoteConsoleInterface::NotifyCutsceneUpdate();
	}
}

//-----------------------------------------------------------------------------

void PartPopup::UpdateCutfileExternal(RexRageCutscenePart* pPart)
{
	m_pPart = pPart;

	UpdateUIFromCutfile();
	UpdateCutfileFromUI();
}

//-----------------------------------------------------------------------------

void PartPopup::UpdateCutfileFromUI()
{
	// Set the top-level data    
	m_pPart->GetCutsceneFile()->SetFadeAtBeginningColor( GetFadeAtBeginningColor() );
	m_pPart->GetCutsceneFile()->SetFadeAtEndColor( GetFadeAtEndColor() );
	m_pPart->GetCutsceneFile()->SetFadeInCutsceneAtBeginningDuration( GetFadeInCutsceneAtBeginningDuration() );
	m_pPart->GetCutsceneFile()->SetFadeInGameAtEndDuration( GetFadeInGameAtEndDuration() );
	m_pPart->GetCutsceneFile()->SetFadeOutCutsceneAtEndDuration( GetFadeOutCutsceneAtEndDuration() );
	m_pPart->GetCutsceneFile()->SetFadeOutGameAtBeginningDuration( GetFadeOutGameAtBeginningDuration() );
	m_pPart->GetCutsceneFile()->SetOffset( GetSceneOffset() );
	m_pPart->GetCutsceneFile()->SetRotation( GetSceneRotation() );
	m_pPart->GetCutsceneFile()->SetSectionByTimeSliceDuration( GetSectionByTimeSliceDuration() );   
	m_pPart->GetCutsceneFile()->SetTriggerOffset( GetTriggerOffset() );

	s32 startFrame = GetRangeStart();
	s32 endFrame = GetRangeEnd();
	if ( startFrame == 0 )
	{
		startFrame = s32(rexMBAnimExportCommon::GetAnimStartTime() * rexMBAnimExportCommon::GetFPS());
	}

	if ( endFrame == 0 )
	{
		endFrame = s32(rexMBAnimExportCommon::GetAnimEndTime() * rexMBAnimExportCommon::GetFPS());
	}

	// FIX_MIKE : End range stays the same but we want to set the duration to include the last frame so we get example. 10-1200 as 1901 frames
	m_pPart->GetCutsceneFile()->SetRangeEnd( endFrame );
	m_pPart->GetCutsceneFile()->SetRangeStart( startFrame );
	m_pPart->GetCutsceneFile()->SetTotalDuration( ((endFrame /*- startFrame*/) + 1) / rexMBAnimExportCommon::GetFPS() );   // Zero-based Timeline: the exported start is 0

	parAttributeList &attributes = const_cast<parAttributeList &>( m_pPart->GetCutsceneFile()->GetAttributeList() );
	attributes.CopyFrom( m_sceneAttributeList );

	atBitSet &cutsceneFlags = const_cast<atBitSet &>( m_pPart->GetCutsceneFile()->GetCutsceneFlags() );
	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_IN_GAME_FLAG, FadeInGameAtEnd() );
	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_OUT_FLAG, FadeOutCutsceneAtEnd() );

	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_OUT_GAME_FLAG, FadeOutGameAtBeginning() );
	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_IN_FLAG, FadeInCutsceneAtBeginning() );

	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_STORY_MODE_FLAG, UseStoryMode() ); 
	rexMBAnimExportCommon::SetStoryMode(UseStoryMode()); 

	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG, GetSectioningMethod() == CAMERA_CUT_SECTIONING_METHOD );
	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG, GetSectioningMethod() == TIME_SLICE_SECTIONING_METHOD );
	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG, GetSectioningMethod() == SECTION_SPLIT_PROPERTY_SECTIONING_METHOD );

	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_PARENT_SCALE_FLAG, UseParentScale() );

	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_FLAG, UseInGameDOFStart() );
	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_END_FLAG, UseInGameDOFEnd() );
	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_SECOND_CUT_FLAG, UseInGameDOFSecondCutStart() );


	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_CATCHUP_CAMERA_FLAG, UseCatchUpCamera() );

	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_BLENDOUT_CAMERA_FLAG, UseBlendOutCamera() );

	cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_AUDIO_EVENTS_CONCAT_FLAG, UseAudioEventsInConcat() );

	if(UseBlendOutCamera())
	{
		m_pPart->GetCutsceneFile()->SetBlendOutCutsceneDuration( GetBlendOutAtEndDuration() );
		m_pPart->GetCutsceneFile()->SetBlendOutCutsceneOffset( GetBlendOutAtEndOffset() );
	}
	else
	{
		m_pPart->GetCutsceneFile()->SetBlendOutCutsceneDuration( 0 );
		m_pPart->GetCutsceneFile()->SetBlendOutCutsceneOffset( 0 );
	}

	atArray<float> &cameraCutList = const_cast<atArray<float> &>( m_pPart->GetCutsceneFile()->GetCameraCutList() );
	rexMBAnimExportCommon::GetCameraCutTimes( cameraCutList );

	atArray<cutfObject *> cameraObjectList;
	m_pPart->GetCutsceneFile()->FindObjectsOfType(CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList);

	if(cameraObjectList.GetCount())
	{
		HFBModel pCamera = RAGEFindModelByName(cameraObjectList[0]->GetDisplayName());
		if(pCamera)
		{
			// get the section splits
			atArray<float> &sectionSplitList = const_cast<atArray<float> &>( m_pPart->GetCutsceneFile()->GetSectionSplitList() );
			rexMBAnimExportCommon::GetSectionSplitTimes( pCamera, sectionSplitList );
		}
	}

	// Store the old non-story mode end range if we ever want to go back
	m_rangeNormalEndNumericUpDown.Enabled = true;

	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;
	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			char cName[RAGE_MAX_PATH];
			sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", m_pPart->GetName());

			if(strcmpi(pTrackName, cName) != 0) continue; // We only care about the Edited Shot Track
			{
				if(lTrack->Clips.GetCount() > 0)
				{
					// Have to convert to a string then int as the AsInt() doesnt work. ffs.
					HFBStoryClip lClip = lTrack->Clips[lTrack->Clips.GetCount()-1];
					char* pStop = (char*)lClip->ShotActionStop.AsString();
					m_rangeNormalEndNumericUpDown.Value = (s32)atoi(pStop);
				}
			}
		}
	}

	m_rangeNormalEndNumericUpDown.Enabled = false;
	m_pPart->GetCutsceneFile()->SetAltRangeEnd( (s32)m_rangeNormalEndNumericUpDown.Value );

	// camera blend
	if(m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_USE_BLENDOUT_CAMERA_FLAG))
	{
		SetBlendOutAtEndDuration( m_pPart->GetCutsceneFile()->GetBlendOutCutsceneDuration() );
		SetBlendOutAtEndOffset( m_pPart->GetCutsceneFile()->GetBlendOutCutsceneOffset() );
	}

	m_pPart->GetCutsceneFile()->SortLoadEvents();
	m_pPart->GetCutsceneFile()->SortEvents();
}

//-----------------------------------------------------------------------------

void PartPopup::UpdateUIFromCutfile()
{
	SetSceneOffset( m_pPart->GetCutsceneFile()->GetOffset() );
	SetSceneRotation( m_pPart->GetCutsceneFile()->GetRotation() );
	SetFadeAtBeginningColor( m_pPart->GetCutsceneFile()->GetFadeAtBeginningColor() );
	SetFadeAtEndColor( m_pPart->GetCutsceneFile()->GetFadeAtEndColor() );
	SetFadeInCutsceneAtBeginning( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_IN_FLAG ) );
	SetFadeInCutsceneAtBeginningDuration(m_pPart->GetCutsceneFile()->GetFadeInCutsceneAtBeginningDuration() );
	SetFadeInGameAtEnd( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_IN_GAME_FLAG ) );
	SetFadeInGameAtEndDuration( m_pPart->GetCutsceneFile()->GetFadeInGameAtEndDuration() );
	SetFadeOutCutsceneAtEnd( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_OUT_FLAG ) );
	SetFadeOutCutsceneAtEndDuration( m_pPart->GetCutsceneFile()->GetFadeOutCutsceneAtEndDuration() );
	SetFadeOutGameAtBeginning( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_OUT_GAME_FLAG ) );
	SetFadeOutGameAtBeginningDuration( m_pPart->GetCutsceneFile()->GetFadeOutGameAtBeginningDuration() );
	SetRangeEnd( m_pPart->GetCutsceneFile()->GetRangeEnd() );
	SetRangeStart( m_pPart->GetCutsceneFile()->GetRangeStart() );
	SetAltRangeEnd( m_pPart->GetCutsceneFile()->GetAltRangeEnd() );
	SetSectionByTimeSliceDuration( m_pPart->GetCutsceneFile()->GetSectionByTimeSliceDuration() );
	SetUseParentSale(m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_PARENT_SCALE_FLAG ) );
	SetUseStoryMode( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_STORY_MODE_FLAG ) );
	rexMBAnimExportCommon::SetStoryMode( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_STORY_MODE_FLAG ) );
	SetUseAudioEventsInConcat( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_AUDIO_EVENTS_CONCAT_FLAG ) );

	atArray<cutfEventArgs *> unusedEventArgsList;
	m_pPart->GetCutsceneFile()->GetUnusedEventArgs( unusedEventArgsList );
	m_cleanupCutFileButton.Enabled = unusedEventArgsList.GetCount() > 0;

	SetUseInGameDOFStart(m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_FLAG ) );
	SetUseInGameDOFSecondCutStart(m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_SECOND_CUT_FLAG ) );
	SetUseInGameDOFEnd(m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_END_FLAG ) );

	SetUseCatchUpCamera(m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_CATCHUP_CAMERA_FLAG ) );

	SetUseBlendOutCamera(m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_BLENDOUT_CAMERA_FLAG ) );
	SetBlendOutAtEndDuration( m_pPart->GetCutsceneFile()->GetBlendOutCutsceneDuration() );
	SetBlendOutAtEndOffset( m_pPart->GetCutsceneFile()->GetBlendOutCutsceneOffset() );

	parAttributeList &attributes = const_cast<parAttributeList &>( m_pPart->GetCutsceneFile()->GetAttributeList() );
	m_sceneAttributeList.CopyFrom(attributes);

	SetTriggerOffset(m_pPart->GetCutsceneFile()->GetTriggerOffset());

	if ( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG ) )
	{
		SetSectioningMethod( CAMERA_CUT_SECTIONING_METHOD );
	}
	else if ( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG ) )
	{
		SetSectioningMethod( TIME_SLICE_SECTIONING_METHOD );
	}
	else if ( m_pPart->GetCutsceneFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG ) )
	{
		SetSectioningMethod( SECTION_SPLIT_PROPERTY_SECTIONING_METHOD );
	}
	else
	{
		SetSectioningMethod( NO_SECTIONING_METHOD );
	}
}
