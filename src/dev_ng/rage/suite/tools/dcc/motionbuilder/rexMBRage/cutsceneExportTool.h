// 
// rexMBRage/cutsceneExportTool.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REXMBRAGE_CUTSCENEEXPORTTOOL_H__ 
#define __REXMBRAGE_CUTSCENEEXPORTTOOL_H__ 

#include "addpopup.h"
#include <fbsdk/fbsdk.h>

#include "cutfile/cutfile2.h"
#include "atl/array.h"
#include "atl/map.h"
#include "diag/channel.h"
#include "file/limits.h"
#include "init/ini.h"
#include "parsercore/attribute.h"
#include "rexGeneric/objectAnimation.h"
#include "rexMBAnimExportCommon/cutsceneExport.h"
#include "vector/color32.h"
#include "rexMBAnimExportCommon/utility.h"
#include <fbsdk/fbsdk.h>
#include "UserObjects/cutsceneExportToolPdata.h"

#include "partpopup.h"

#include "rexMBAnimExportCommon/partFile.h"

namespace rage {

class cutfEvent;
class cutfEventArgs;
class cutfObject;
class fiStream;
class EditEventPopup;
class EditObjectAttributesPopup;
class IToolObjectInterchange;

struct HandleEntry
{
	std::string strType;
	std::string strModel;
	std::string strHandle;
	std::string strName;
	std::string strCategory;
};

//##############################################################################

class RexRageCutsceneExportTool : public FBTool
{
    FBToolDeclare( RexRageCutsceneExportTool, FBTool );

public:
    enum ESpreadsheetObjectsColumn
    {
        NAME_SPREADSHEET_COLUMN = -1,
        ID_SPREADSHEET_COLUMN,
        SIZE_SPREADSHEET_COLUMN,
        TYPE_SPREADSHEET_COLUMN,
        EXPORT_SPREADSHEET_COLUMN,
		FACE_SPREADSHEET_COLUMN,
		HANDLE_SPREADSHEET_COLUMN
	};

	enum ESpreadsheetItemsColumn
	{
		SHOT_EXPORT_SPREADSHEET_COLUMN,
		SHOT_RANGE_SPREADSHEET_COLUMN
	};

    virtual bool FBCreate();
    virtual void FBDestroy();
    virtual void UICreate();
    virtual void UIConfigure();
    virtual bool FbxStore( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );
    virtual bool FbxRetrieve( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );

	static RexRageCutsceneExportTool* GetCurrentCutsceneExportToolInst();
	static RexRageCutsceneExportTool* CreateCurrentCutsceneExportToolInst();

	RexRageCutsceneExport* GetCutsceneExport(){ return &m_cutsceneExport; }

	// PURPOSE: Loads the cut file
	// PARAMS:
	//    pFullPath - the full filename to load
	// RETURNS: true on success, otherwise false
	virtual bool OpenCutFile( const char *pFullPath );

	// PURPOSE: Loads the part file
	// PARAMS:
	//    pFullPath - the full filename to load
	// RETURNS: true on success, otherwise false
	bool OpenPartFile( const char *pFullPath );

private:

    void SetSceneName( const char *pSceneName, bool doValidate = true );
    void SetExportPath( const char* pExportPath );
    void SetFaceExportPath( const char *pFaceDir );

    void RefreshExportInfo();

    // PURPOSE: Adds a row to the spreadsheet for the given cutfObject.    
    // PARAMS:
    //    pObject - the cutfObject to add a row for
    void AddRowToSpreadsheet( cutfObject *pObject );

	void SetDefaultSpecFile( cutfObject* pObject );

    // PURPOSE: Removes the row with the given reference id
    // PARAMS:
    //    iRef - the reference id to delete
    void RemoveRowFromSpreadsheet( s32 iRef );

	// PURPOSE: Removes the row with the given reference id
	// PARAMS:
	//    iRef - the reference id to delete
	void RemoveRowFromPartSpreadsheet( s32 iRef );

    void ClearSpreadsheet();
    
    void RefreshShreadsheetSizes();

    // PURPOSE: Clears the settings, thus starting a new file.
    void NewCutFile();  

    // PURPOSE: Exports the animations for all models in the list.
    // PARAMS:
    //    modelObjects - the list of models and their associated cutfObject
    // RETURNS: true on success, otherwise false
    virtual bool ExportModelAnimations( atArray<SModelObject> &modelObjects, atArray<SModelObject> &otherModelObjects, bool bInteractive = true );

    // PURPOSE: Exports the animation(s) for the given camera.
    // PARAMS:
    //    camera - the model to export
    // RETURNS: true on success, otherwise false
    virtual bool ExportCameraAnimation( SModelObject &cameraModelObject, bool bInteractive = true );

	// PURPOSE: Exports the face animation(s) for the given models.
	// PARAMS:
	//    pedModelObjects - the models to export
	// RETURNS: true on success, otherwise false
	virtual bool ExportFaceAnimation( atArray<SModelObject> &pedModelObjects );

    // PURPOSE: Finds and returns all models with animation.  They must be added to the
    //    Spreadsheet and will only be returned if it is marked for export.
    // PARAMS:
    //    modelObjects - the list to fill
    // RETURNS: true, if any are found
    bool FindSpreadsheetAnimationModels( atArray<SModelObject> &modelObjects );

    // PURPOSE: Finds and returns all selected models with animation.  They must be added to the
    //    Spreadsheet and will only be returned if it is marked for export.
    // PARAMS:
    //    modelObjects - the list to fill
    // RETURNS: true, if any are found
    bool FindSelectedAnimationModels( atArray<SModelObject> &modelObjects );

    // PURPOSE: Finds and returns the model for the camera.  It must be added to the
    //    Spreadsheet and will only be returned if it is marked for export.
    // PARAMS:
    //    cameraModelObject - the SModelObject to fill
    // RETURNS: true, if it is found
    bool FindCameraAnimationModel( SModelObject &cameraModelObject );

	// PURPOSE: Finds and returns the model for the peds.  It must be added to the
	//    Spreadsheet and will only be returned if it is marked for export.
	// PARAMS:
	//    pedModelObjects - array of model objects
	// RETURNS: true, if it is found
	bool FindPedAnimationModels( atArray<SModelObject> &pedModelObjects );

    // PURPOSE: Sets up the cut scene export instance with our current settings in preparation for export
    void SetupCutsceneExport(bool bInteractive = true, bool doValidation = true);

	// PURPOSE: Sets the model objects handle.
	// PARAMS:
	//    pModel - the SModelObject to set the handle on
	void SetModelHandle( cutfModelObject* pModel );

	// PURPOSE: Loads the cutscene csv handle file.
	void PopulateFromCutsceneHandleFile();

	// PURPOSE: Populate the light types.
	void PopulateLightProperties();

	// PURPOSE: Populate the overlay types.
	void PopulateOverlayTypes();

	// PURPOSE: Populate the weapon types.
	void PopulateWeaponTypes();

	// PURPOSE: Populate the part spreadsheet with parts
	void PopulatePartSpreadsheetFromFile();

	bool ValidateAnimationFiles();

    void FileNewCompleted_Event( HISender pSender, HKEvent pEvent );
	void FileSave_Event( HISender pSender, HKEvent pEvent );   
	void FileSaveCompleted_Event( HISender pSender, HKEvent pEvent );  
	void FileOpenCompleted_Event( HISender pSender, HKEvent pEvent );

protected:
    void UICreateCutfileInfoLayout();
    void UICreateExportInfoLayout();
	void UICreateCutfileInfoOptionsLayout();
	void UICreateShotInfoLayout();
    void UIConfigureExportInfoLayout();
    void UIConfigureCutfileInfoLayout();  
	void UIConfigureShotInfoLayout();

    // PURPOSE: Sets the Export Path, and Face Directory based on the current project, config, and scene name.
    void UpdatePaths(bool doValidation = true);

    // PURPOSE: Sets the UI controls with data from the cut file.
    void UpdateUIFromCutfile();

	// PURPOSE: Sets the cutfile with data from the UI controls.
	void UpdateCutfileFromUI();

	//Purpose: Updates the export flag on each shot according to export checkbox in ui
	void UpdateShotExportStatus();

    // PURPOSE: Populates our internal list with the cutfObjects that can be used as templates
    //    for models in the scene.
    virtual void PopulateComponentObjectTemplateList();

    // PURPOSE: Populates our internal list with the cutfObjects that can be used as templates
    //    for non-models in the scene.
    virtual void PopulateNonComponentObjectTemplateList();

    // PURPOSE: Populates our internal list with the EditAttributesPopup that corresponds
    //    to each of the cutfObject types.
    virtual void PopulateObjectTypeToEditPopupMap();

    // PURPOSE: Ensures that all objects are using sequential ids.  This is required for the run-time.
    void RefreshSpreadsheetObjectIds();

    // PURPOSE: Indicates whether the object can be manually edited or not.
    // PARAMS:
    //    pObject - the object to check
    // RETURNS: true if it can be edited, otherwise false.
    bool CanEditObject( cutfObject *pObject ) const;

    // PURPOSE: Indicates whether the object can be manually removed from the scene or not.
    // PARAMS:
    //    pObject - the object to check
    // RETURNS: true if it can be removed, otherwise false.
    bool CanRemoveObject( cutfObject *pObject ) const;

	// PURPOSE: Indicates whether the part can be manually edited or not.
	// PARAMS:
	//    pPart - the part to check
	// RETURNS: true if it can be edited, otherwise false.
	bool CanEditPart( RexRageCutscenePart* pPart ) const;

public:
	// Exposed for use in python hooks
	bool Export(bool bInteractive = true);
	bool ExportCamera(bool bInteractive = true);

	const char* GetSceneName() const;
	const char* GetExportPath() const;
	const char* GetFaceExportPath() const;

	// PURPOSE: Adds a row to the spreadsheet for the given RageItemObject.    
	// PARAMS:
	//    pObject - the cutfObject to add a row for
	void AddRowToPartSpreadsheet( RexRageCutscenePart* pShot );

	RageFBSpread* GetObjectSpreadsheet(){return &m_spreadsheetObjects;}
	RageFBSpread* GetPartSpreadsheet(){return &m_spreadsheetParts;}
	void SetShotExportStatus(int index, bool bExport);
	
	void UpdatePartsFromUI();

	bool ImportCutFile(const char* pCutFile);
	bool ImportCutPart(const char* pCutPart);
	bool AddCameraToSpreadsheet(const char* pCameraName, bool bAddFade);
	bool AddFaceToSpreadsheet( const char* pTargetName, const char* pFaceName );
	bool RemoveFaceFromSpreadsheet( const char* pTargetName );
	bool AddPedToSpreadsheet( const char* pTargetName, bool bAddFace);
	bool AddPropToSpreadsheet( const char* pTargetName);
	bool AddVehicleToSpreadsheet( const char* pTargetName);
	bool AddAudioToSpreadsheet( const char* pTargetName);
	bool AddWeaponToSpreadsheet( const char* pTargetName);
	template <class modelType> bool AddModelObjectToSpreadsheet( const char* pTargetName, bool bAddFace = false );
	template <class modelType> bool AddObjectToSpreadsheet( const char* pTargetName );
	bool RemoveFromSpreadsheet(const char* pName);
	void RemoveAllFromSpreadsheet();
	void SetConcatMode(bool bEnabled);

	void ClearPartSpreadsheet();
	// PURPOSE: Enables or disables the buttons based on what is currently in the spreadsheet.
	void EnableButtons();

protected:

    int m_iObjectsSpreadsheetRowId;
    int m_iObjectsSpreadsheetSelRowId;

	int m_iPartSpreadsheetRowId;
	int m_iPartSpreadsheetSelRowId;

    atMap<int, cutfObject *> m_spreadsheetObjectsRowRefToObjectMap;
	atMap<int, RexRageCutscenePart *> m_spreadsheetRowRefToPartMap;

    atArray<cutfObject *> m_pComponentObjectTemplateList;
    atArray<cutfObject *> m_pNonComponentObjectTemplateList;
    atMap<ConstString, EditObjectAttributesPopup *> m_pObjectTypeToEditPopupMap;

    RexRageCutsceneExport m_cutsceneExport;
    FBProgress *m_pProgressBar;

    char m_cSceneName[CUTSCENE_OBJNAMELEN];

    FBLayout m_cutfileInfoLayout;
    FBLayout m_exportInfoLayout;
	FBLayout m_shotInfoLayout;

    FBLayoutRegion m_cutfileInfoLayoutRegion;
    FBLayoutRegion m_exportInfoLayoutRegion;
	FBLayoutRegion m_shotInfoLayoutRegion;

	FBLabel	m_versionLabel;
	FBButton m_usePerforceIntegration;    
	FBButton m_useAutoConcat;

    FBButton m_importCutFileButton; 
	FBButton m_importCutListButton; 
	FBButton m_refreshHandleFileButton;
	FBButton m_editSceneEventsButton;
	FBButton m_verifyCutsceneButton;
	FBButton m_helpButton;

    FBFilePopup m_openFilePopup;
    FBFilePopup m_saveFilePopup;

    RageFBSpread m_spreadsheetObjects;
	RageFBSpread m_spreadsheetParts;
    FBButton m_editSpreadsheetButton;
    FBButton m_addToSpreadsheetButton;
	FBButton m_editPartSpreadsheetButton;
	FBButton m_addPartToSpreadsheetButton;
	FBButton m_removePartFromSpreadsheetButton;
    FBButton m_removeAllFromSpreadsheetButton;
	FBButton m_removeAllPartFromSpreadsheetButton;
    FBButton m_removeFromSpreadsheetButton;
    FBButton m_refreshSpreadsheetButton;
	FBButton m_addFaceSpreadsheetButton;
	FBButton m_removeFaceSpreadsheetButton;
	FBButton m_selectModelButton;
	FBButton m_moveUpPartFromSpreadsheetButton;
	FBButton m_moveDownPartFromSpreadsheetButton;
	FBButton m_duplicatePartSpreadsheetButton;
	FBButton m_syncRangesSpreadsheetButton;

    FBLabel m_sceneSizeLabel;
       
    FBLabel m_sceneNameLabel;
    FBEdit m_sceneNameTextBox;

	FBButton m_openPathButton;

    FBLabel m_exportPathLabel;
    FBLabel m_exportPathTextBox;

    FBLabel m_faceExportPathLabel;
    FBLabel m_faceExportPathTextBox;

    FBButton m_exportSelectedAnimationsButton;
    FBButton m_exportSpreadsheetAnimationsButton;
    FBButton m_buildAnimationDictionariesButton;
    
    FBButton m_exportCameraAnimationButton;
	FBButton m_exportCutFileButton;
    FBButton m_patchAnimationDictionariesButton;
	FBButton m_exportFaceAnimationButton;

    FBApplication m_Application;

    AddComponentsPopup m_addComponentsPopup;
    AddNonComponentPopup m_addNonComponentPopup;
	PartPopup m_partPopup;
	EditEventPopup* m_pEditEventPopup;

	atArray<HandleEntry> m_vHandleEntries;
	atArray<atString> m_vLightPropertyEntries;
	atArray<atString> m_vOverlayTypeEntries;
	atArray<atString> m_vWeaponTypeEntries;

    void ImportCutFileButton_Click( HISender pSender, HKEvent pEvent );
	void ImportCutListButton_Click( HISender pSender, HKEvent pEvent );
	void RefreshHandleFileButton_Click( HISender pSender, HKEvent pEvent );
	void VerifyCutsceneButton_Click( HISender pSender, HKEvent pEvent );

    void Spreadsheet_RowClick( HISender pSender, HKEvent pEvent );
	void Spreadsheet_DragDrop( HISender pSender, HKEvent pEvent );
    void EditSpreadsheetButton_Click( HISender pSender, HKEvent pEvent );
    void AddToSpreadsheetButton_Click( HISender pSender,HKEvent pEvent );
    void RemoveAllFromSpreadsheetButton_Click( HISender pSender, HKEvent pEvent );
    void RemoveFromSpreadsheetButton_Click( HISender pSender, HKEvent pEvent );
    void RefreshSpreadsheetButton_Click( HISender pSender, HKEvent pEvent );
	void AddFaceSpreadsheetButton_Click( HISender pSender, HKEvent pEvent );
	void RemoveFaceSpreadsheetButton_Click( HISender pSender, HKEvent pEvent );
	void SelectModelButton_Click( HISender pSender, HKEvent pEvent );

	void AddToPartSpreadsheetButton_Click( HISender pSender,HKEvent pEvent );
	void RemoveAllFromPartSpreadsheetButton_Click( HISender pSender,HKEvent pEvent );
	void PartSpreadsheet_RowClick( HISender /*pSender*/, HKEvent pEvent );
	void EditPartSpreadsheetButton_Click( HISender pSender, HKEvent pEvent );
	void RemoveFromPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void MoveUpPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void MoveDownPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void DuplicatePartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void SyncRangesSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );

    void SectioningMethodComboBox_SelectionChanged( HISender pSender, HKEvent pEvent );
	void UseStoryModeButton_Click( HISender pSender, HKEvent pEvent );
	void UsePerforceIntegrationButton_Click( HISender pSender, HKEvent pEvent );
	void EditSceneEventsButton_Click( HISender pSender, HKEvent pEvent );

	void OpenPathButton_Click( HISender /* pSender */, HKEvent /* pEvent */);
	void HelpButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );

    void SceneNameTextBox_TextChanged( HISender /* pSender */, HKEvent /* pEvent */ );

	void ExportFaceAnimationButton_Click( HISender pSender, HKEvent pEvent );
    void ExportSelectedAnimationsButton_Click( HISender pSender, HKEvent pEvent );
    void ExportSpreadsheetAnimationsButton_Click( HISender pSender, HKEvent pEvent );
	void BuildAnimationDictionariesButton_Click( HISender pSender, HKEvent pEvent );
    
    void ExportCameraAnimationButton_Click( HISender pSender, HKEvent pEvent );
    void ExportCutFileButton_Click( HISender pSender, HKEvent pEvent );
    void PatchAnimationDictionariesButton_Click( HISender pSender, HKEvent pEvent );

	void ValidateSectionTime_Click( HISender pSender, HKEvent pEvent );

	string ValidateSceneNameLength(string strSceneName);
	void SetFaceAnimationNode(cutfPedModelObject *pPedModelObj, const char* czNode, int idx);
	void AddToSpreadSheet(const FBModelList& models);

	bool GetAnimationModels(atArray<SModelObject>& modelObjects);

	void RemoveStreamProcessedEvents(cutfCutsceneFile2& cutFile);
	void RenameStoryTrack(const char* pPartName);
	void SetRangesFromStoryTrack(const char* pName, cutfCutsceneFile2* pCutfile);
	void RefreshPartRanges();
	void FindAndAddFaceNode(cutfObject* pObject);

	void WriteLocalCutsceneProcessorFile();

	//Persistant Data methods
	RexRageCutsceneExportPData* FindPersistentData( bool bCreate = true );
	void						DeletePersistentData();
	void						LoadFromPersistentData();
public:
	void						SaveToPersistentData();
};

inline const char* RexRageCutsceneExportTool::GetSceneName() const
{
    return m_cSceneName;
}

inline const char* RexRageCutsceneExportTool::GetExportPath() const
{
    return m_exportPathTextBox.Caption;
}

inline void RexRageCutsceneExportTool::SetExportPath( const char* pExportPath )
{
    m_exportPathTextBox.Caption = const_cast<char *>( pExportPath );
}

inline const char* RexRageCutsceneExportTool::GetFaceExportPath() const
{
    return m_faceExportPathTextBox.Caption;
}

inline void RexRageCutsceneExportTool::SetFaceExportPath( const char *pFaceDir )
{
    m_faceExportPathTextBox.Caption = const_cast<char *>( pFaceDir );
}

//##############################################################################

} // namespace rage

#endif // __REXMBRAGE_CUTSCENEEXPORTTOOL_H__ 
