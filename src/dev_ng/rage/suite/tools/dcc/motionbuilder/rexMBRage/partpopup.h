#pragma once

#include "cutfile/cutfdefines.h"
#include "init/ini.h"
#include "parsercore/attribute.h"
#include "vector/vector3.h"
#include "rexMBAnimExportCommon/utility.h"
#include "rexMBAnimExportCommon/cutsceneExport.h"
//#include "cutsceneExportTool.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfeventdef.h"
#include "eventpopup.h"

namespace rage {
class RexRageCutsceneExportTool;
}

class PartPopup : public FBPopup
{
public:

	PartPopup()
	{
		UICreate();
		UIConfigure();
	};

	virtual ~PartPopup();

	void CleanupCutFileButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void CloseButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void OKButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void SectioningMethodComboBox_SelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void ValidateSectionTime_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void FadeInGameAtEndCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void FadeOutCutsceneAtEndCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void FadeInCutsceneAtBeginningCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void FadeOutGameAtBeginningCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void EditScenePropertiesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ );
	void BlendOutGameAtEndCheckBox_Click( HISender pSender, HKEvent pEvent );
	void BlendOutDuration_Change( HISender pSender, HKEvent pEvent );
	void BlendOutOffset_Change( HISender pSender, HKEvent pEvent );
	void UseInGameDOFStart_SelectionChanged( HISender pSender, HKEvent pEvent );
	void UseInGameDOFSecondCutStart_SelectionChanged( HISender /*pSender*/, HKEvent /*pEvent*/ );

	void ShowDialog(RexRageCutscenePart* pPart, RexRageCutsceneExportTool* pExport);

	void Setup();
	void PopulateSectioningMethodNames();

	bool FadeOutGameAtBeginning() const;
	void SetFadeOutGameAtBeginning( bool bFadeOut );

	float GetFadeOutGameAtBeginningDuration() const;
	void SetFadeOutGameAtBeginningDuration( float fSeconds );

	bool FadeInCutsceneAtBeginning() const;
	void SetFadeInCutsceneAtBeginning( bool bFadeIn );

	float GetFadeInCutsceneAtBeginningDuration() const;
	void SetFadeInCutsceneAtBeginningDuration( float fSeconds );

	Color32 GetFadeAtBeginningColor() const;
	void SetFadeAtBeginningColor( const Color32 &color );

	bool FadeOutCutsceneAtEnd() const;
	void SetFadeOutCutsceneAtEnd( bool bFadeOut );

	float GetFadeOutCutsceneAtEndDuration() const;
	void SetFadeOutCutsceneAtEndDuration( float fSeconds );

	bool FadeInGameAtEnd() const;
	void SetFadeInGameAtEnd( bool bFadeIn );

	float GetFadeInGameAtEndDuration() const;
	void SetFadeInGameAtEndDuration( float fSeconds );

	Color32 GetFadeAtEndColor() const;
	void SetFadeAtEndColor( const Color32 &color );

	bool UseStoryMode() const;
	void SetUseStoryMode( bool bUseOne );

	bool UseAudioEventsInConcat() const;
	void SetUseAudioEventsInConcat( bool bUseOne );

	bool UseParentScale() const;
	void SetUseParentSale( bool bUseParentScale );

	bool UseInGameDOFStart() const;
	void SetUseInGameDOFStart( bool bUseInGameDOFStart );

	bool UseInGameDOFSecondCutStart() const;
	void SetUseInGameDOFSecondCutStart( bool bUseInGameDOFStart );

	bool UseInGameDOFEnd() const;
	void SetUseInGameDOFEnd( bool bUseInGameDOFEnd );

	bool UseCatchUpCamera() const;
	void SetUseCatchUpCamera( bool bUseCatchUpCamera );

	s32 GetSectioningMethod() const;
	void SetSectioningMethod( s32 method );

	float GetSectionByTimeSliceDuration() const;
	void SetSectionByTimeSliceDuration( float fSeconds );

	void UpdateCutfileFromUI();
	void UpdateUIFromCutfile();

	void LoadEventDefsFile();

	void UpdateCutfileExternal(RexRageCutscenePart* pPart);

	const Vector3& GetSceneOffset() const;
	void SetSceneOffset( const Vector3& vOffset );

	const Vector3& GetTriggerOffset() const;
	void SetTriggerOffset( const Vector3& vOffset );

	float GetSceneRotation() const;
	void SetSceneRotation( float fRotation );

	RexRageCutsceneExportTool* m_pExport;
	RexRageCutscenePart* m_pPart;
	cutfEventDefs m_eventDefs;
	EditEventPopup *m_pEditEventPopup;

	char m_cSceneName[CUTSCENE_OBJNAMELEN];
	Vector3 m_vSceneOffset;
	Vector3 m_vTriggerOffset;
	float m_fSceneRotation;
	parAttributeList m_sceneAttributeList;

	atArray<ConstString> m_sectioningMethodNames;

	FBLayoutRegion m_cutfileInfoOptionsLayoutRegion;
	FBLayout m_cutfileInfoOptionsLayout;

	FBLabel m_rangeLabel;
	FBEditNumber m_rangeStartNumericUpDown;
	FBEditNumber m_rangeEndNumericUpDown;
	FBEditNumber m_rangeNormalEndNumericUpDown;

	FBButton m_editScenePropertiesButton;
	FBButton m_cleanupCutFileButton;

	FBButton m_fadeOutGameAtBeginningCheckBox;
	FBEditNumber m_fadeOutGameAtBeginningDurationNumericUpDown;
	FBLabel m_fadeOutGameAtBeginningDurationLabel;

	FBButton m_fadeInCutsceneAtBeginningCheckBox;
	FBEditNumber m_fadeInCutsceneAtBeginningDurationNumericUpDown;
	FBLabel m_fadeInCutsceneAtBeginningDurationLabel;

	FBEditColor m_fadeAtBeginningEditColor;

	FBButton m_fadeInGameAtEndCheckBox;
	FBEditNumber m_fadeInGameAtEndDurationNumericUpDown;
	FBLabel m_fadeInGameAtEndDurationLabel;

	FBButton m_fadeOutCutsceneAtEndCheckBox;
	FBEditNumber m_fadeOutCutsceneAtEndDurationNumericUpDown;
	FBLabel m_fadeOutCutsceneAtEndDurationLabel;

	FBEditColor m_fadeAtEndEditColor;

	FBButton m_blendOutGameAtEndCheckBox;
	FBEditNumber m_blendOutGameAtEndDurationNumericUpDown;
	FBLabel m_blendOutGameAtEndDurationLabel;
	FBEditNumber m_blendOutGameAtEndOffsetNumericUpDown;
	FBLabel m_blendOutGameAtEndOffsetLabel;
	FBButton m_useAudioEventsInConcatCheckBox;

	FBButton m_useStoryMode;    
	FBButton m_useParentScaleCheckBox;
	FBButton m_useInGameDOFStartCheckBox;
	FBButton m_useInGameDOFEndCheckBox;
	FBButton m_useCatchUpCameraCheckbox;
	FBButton m_useInGameDOFSecondCutStartCheckBox;

	FBLabel m_sectioningMethodLabel;
	FBList m_sectioningMethodComboBox;
	FBEditNumber m_sectionDurationNumericUpDown;
	FBLabel m_sectionDurationLabel; 

	FBButton m_okButton;
	FBButton m_cancelButton;

	s32 GetRangeStart() const;
	void SetRangeStart( s32 iFrame );

	s32 GetRangeEnd() const;
	void SetRangeEnd( s32 iFrame );
	void SetAltRangeEnd( s32 iFrame );

	float GetBlendOutAtEndDuration() const;
	void SetBlendOutAtEndDuration( float fSeconds );

	float GetBlendOutAtEndOffset() const;
	void SetBlendOutAtEndOffset( float fSeconds );

	bool UseBlendOutCamera() const;
	void SetUseBlendOutCamera( bool bBlendOutCamera );

	void UICreate();
	void UIConfigure();
};

inline const Vector3& PartPopup::GetSceneOffset() const
{
	return m_vSceneOffset;
}

inline void PartPopup::SetSceneOffset( const Vector3& vOffset )
{
	m_vSceneOffset = vOffset;
}

inline const Vector3& PartPopup::GetTriggerOffset() const
{
	return m_vTriggerOffset;
}

inline void PartPopup::SetTriggerOffset( const Vector3& vOffset )
{
	m_vTriggerOffset = vOffset;
}

inline float PartPopup::GetSceneRotation() const
{
	return m_fSceneRotation;
}

inline void PartPopup::SetSceneRotation( float fRotation )
{
	m_fSceneRotation = fRotation;
}

inline s32 PartPopup::GetRangeStart() const
{
	return (s32)m_rangeStartNumericUpDown.Value;
}

inline void PartPopup::SetRangeStart( s32 iFrame )
{
	m_rangeStartNumericUpDown.Value = iFrame;
}

inline s32 PartPopup::GetRangeEnd() const
{
	return (s32)m_rangeEndNumericUpDown.Value;
}

inline void PartPopup::SetRangeEnd( s32 iFrame )
{
	m_rangeEndNumericUpDown.Value = iFrame;
}

inline void PartPopup::SetAltRangeEnd( s32 iFrame )
{
	m_rangeNormalEndNumericUpDown.Value = iFrame;
}

inline float PartPopup::GetBlendOutAtEndDuration() const
{
	return (float)m_blendOutGameAtEndDurationNumericUpDown.Value;
}

inline void PartPopup::SetBlendOutAtEndDuration( float fSeconds )
{
	m_blendOutGameAtEndDurationNumericUpDown.Value = fSeconds;
}

inline float PartPopup::GetBlendOutAtEndOffset() const
{
	return (float)m_blendOutGameAtEndOffsetNumericUpDown.Value;
}

inline void PartPopup::SetBlendOutAtEndOffset( float fSeconds )
{
	m_blendOutGameAtEndOffsetNumericUpDown.Value = fSeconds;
}


inline bool PartPopup::FadeOutGameAtBeginning() const
{
	return m_fadeOutGameAtBeginningCheckBox.State == 1;
}

inline void PartPopup::SetFadeOutGameAtBeginning( bool bFadeOut )
{
	m_fadeOutGameAtBeginningCheckBox.State = bFadeOut ? 1 : 0;

	m_fadeOutGameAtBeginningDurationNumericUpDown.Enabled = bFadeOut;
	m_fadeOutGameAtBeginningDurationLabel.Enabled = bFadeOut;

	m_fadeAtBeginningEditColor.Enabled = (bFadeOut || FadeInCutsceneAtBeginning());
}

inline float PartPopup::GetFadeOutGameAtBeginningDuration() const
{
	return (float)m_fadeOutGameAtBeginningDurationNumericUpDown.Value;
}

inline void PartPopup::SetFadeOutGameAtBeginningDuration( float fSeconds )
{
	m_fadeOutGameAtBeginningDurationNumericUpDown.Value = fSeconds;
}

inline bool PartPopup::FadeInCutsceneAtBeginning() const
{
	return m_fadeInCutsceneAtBeginningCheckBox.State == 1;
}

inline void PartPopup::SetFadeInCutsceneAtBeginning( bool bFadeIn )
{
	m_fadeInCutsceneAtBeginningCheckBox.State = bFadeIn ? 1 : 0;

	m_fadeInCutsceneAtBeginningDurationNumericUpDown.Enabled = bFadeIn;
	m_fadeInCutsceneAtBeginningDurationLabel.Enabled = bFadeIn;

	m_fadeAtBeginningEditColor.Enabled = (bFadeIn || FadeOutGameAtBeginning());
}

inline float PartPopup::GetFadeInCutsceneAtBeginningDuration() const
{
	return (float)m_fadeInCutsceneAtBeginningDurationNumericUpDown.Value;
}

inline void PartPopup::SetFadeInCutsceneAtBeginningDuration( float fSeconds )
{
	m_fadeInCutsceneAtBeginningDurationNumericUpDown.Value = fSeconds;
}

inline Color32 PartPopup::GetFadeAtBeginningColor() const
{
	FBColor color = m_fadeAtBeginningEditColor.Value;
	return Color32( (float)color[0], (float)color[1], (float)color[2], 1.0f);//, (float)color[3] );
}

inline void PartPopup::SetFadeAtBeginningColor( const Color32 &color )
{
	m_fadeAtBeginningEditColor.Value = FBColor( color.GetRedf(), color.GetGreenf(), color.GetBluef());//, color.GetAlphaf() );
}

inline bool PartPopup::FadeOutCutsceneAtEnd() const
{
	return m_fadeOutCutsceneAtEndCheckBox.State == 1;
}

inline void PartPopup::SetFadeOutCutsceneAtEnd( bool bFadeOut )
{
	m_fadeOutCutsceneAtEndCheckBox.State = bFadeOut ? 1 : 0;

	m_fadeOutCutsceneAtEndDurationNumericUpDown.Enabled = bFadeOut;
	m_fadeOutCutsceneAtEndDurationLabel.Enabled = bFadeOut;

	m_fadeAtEndEditColor.Enabled = (bFadeOut || FadeInGameAtEnd());
}

inline float PartPopup::GetFadeOutCutsceneAtEndDuration() const
{
	return (float)m_fadeOutCutsceneAtEndDurationNumericUpDown.Value;
}

inline void PartPopup::SetFadeOutCutsceneAtEndDuration( float fSeconds )
{
	m_fadeOutCutsceneAtEndDurationNumericUpDown.Value = fSeconds;
}

inline bool PartPopup::FadeInGameAtEnd() const
{
	return m_fadeInGameAtEndCheckBox.State == 1;
}

inline void PartPopup::SetFadeInGameAtEnd( bool bFadeIn )
{
	m_fadeInGameAtEndCheckBox.State = bFadeIn ? 1 : 0;

	m_fadeInGameAtEndDurationNumericUpDown.Enabled = bFadeIn;
	m_fadeInGameAtEndDurationLabel.Enabled = bFadeIn;

	m_fadeAtEndEditColor.Enabled = (bFadeIn || FadeOutCutsceneAtEnd());
}

inline float PartPopup::GetFadeInGameAtEndDuration() const
{
	return (float)m_fadeInGameAtEndDurationNumericUpDown.Value;
}

inline void PartPopup::SetFadeInGameAtEndDuration( float fSeconds )
{
	m_fadeInGameAtEndDurationNumericUpDown.Value = fSeconds;
}

inline Color32 PartPopup::GetFadeAtEndColor() const
{
	FBColor color = m_fadeAtEndEditColor.Value;
	return Color32( (float)color[0], (float)color[1], (float)color[2], 1.0f);//, (float)color[3] );
}

inline void PartPopup::SetFadeAtEndColor( const Color32 &color )
{
	m_fadeAtEndEditColor.Value = FBColor( color.GetRedf(), color.GetGreenf(), color.GetBluef() );
}

inline bool PartPopup::UseStoryMode() const
{
	return m_useStoryMode.State == 1;
}

inline void PartPopup::SetUseStoryMode( bool bUseOne )
{
	m_useStoryMode.State = bUseOne ? 1 : 0;
}

inline bool PartPopup::UseAudioEventsInConcat() const
{
	return m_useAudioEventsInConcatCheckBox.State == 1;
}

inline void PartPopup::SetUseAudioEventsInConcat( bool bUseOne )
{
	m_useAudioEventsInConcatCheckBox.State = bUseOne ? 1 : 0;
}

inline bool PartPopup::UseParentScale() const
{
	return m_useParentScaleCheckBox.State == 1;
}

inline void PartPopup::SetUseParentSale( bool bUseParentScale )
{
	m_useParentScaleCheckBox.State = bUseParentScale ? 1 : 0;
}

inline bool PartPopup::UseInGameDOFStart() const
{
	return m_useInGameDOFStartCheckBox.State == 1;
}

inline bool PartPopup::UseInGameDOFSecondCutStart() const
{
	return m_useInGameDOFSecondCutStartCheckBox.State == 1;
}

inline void PartPopup::SetUseInGameDOFStart( bool bUseInGameDOFStart )
{
	m_useInGameDOFStartCheckBox.State = bUseInGameDOFStart ? 1 : 0;
}

inline void PartPopup::SetUseInGameDOFSecondCutStart( bool bUseInGameDOFStart )
{
	m_useInGameDOFSecondCutStartCheckBox.State = bUseInGameDOFStart ? 1 : 0;
}

inline bool PartPopup::UseInGameDOFEnd() const
{
	return m_useInGameDOFEndCheckBox.State == 1;
}

inline void PartPopup::SetUseInGameDOFEnd( bool bUseInGameDOFEnd )
{
	m_useInGameDOFEndCheckBox.State = bUseInGameDOFEnd ? 1 : 0;
}

inline bool PartPopup::UseCatchUpCamera() const
{
	return m_useCatchUpCameraCheckbox.State == 1;
}

inline void PartPopup::SetUseCatchUpCamera( bool bUseCatchUpCamera )
{
	m_useCatchUpCameraCheckbox.State = bUseCatchUpCamera ? 1: 0;
}

inline bool PartPopup::UseBlendOutCamera() const
{
	return m_blendOutGameAtEndCheckBox.State == 1;
}

inline void PartPopup::SetUseBlendOutCamera( bool bBlendOutCamera )
{
	m_blendOutGameAtEndCheckBox.State = bBlendOutCamera ? 1: 0;

	m_blendOutGameAtEndDurationNumericUpDown.Enabled = bBlendOutCamera;
	m_blendOutGameAtEndOffsetNumericUpDown.Enabled = bBlendOutCamera;
	m_blendOutGameAtEndDurationLabel.Enabled = bBlendOutCamera;
	m_blendOutGameAtEndOffsetLabel.Enabled = bBlendOutCamera;
}

inline s32 PartPopup::GetSectioningMethod() const
{
	return m_sectioningMethodComboBox.ItemIndex;
}

inline void PartPopup::SetSectioningMethod( s32 method )
{
	if ( (method >= 0) && (method < m_sectioningMethodComboBox.Items.GetCount()) )
	{
		m_sectioningMethodComboBox.ItemIndex = method;

		m_sectionDurationNumericUpDown.Enabled = (method == TIME_SLICE_SECTIONING_METHOD);
		m_sectionDurationLabel.Enabled = method == TIME_SLICE_SECTIONING_METHOD;
	}
	else
	{
		m_sectionDurationNumericUpDown.Enabled = false;
		m_sectionDurationLabel.Enabled = false;
	}
}

inline float PartPopup::GetSectionByTimeSliceDuration() const
{
	int iSectionTime = (int)m_sectionDurationNumericUpDown.Value;
	return (float)iSectionTime;
}

inline void PartPopup::SetSectionByTimeSliceDuration( float fSeconds )
{
	m_sectionDurationNumericUpDown.Value = fSeconds;
}