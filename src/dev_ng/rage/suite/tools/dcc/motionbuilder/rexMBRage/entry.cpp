#include <windows.h>
#include <commctrl.h>

#include "parser/manager.h"
#include "rexMBAnimExportCommon/cutsceneExport.h"
#include "system/param.h"
#include "rexMBAnimExportCommon/utility.h"
#include "rexMBRage/animExportTool.h"
#include "rexMBRage/cutsceneExportTool.h"

#include "rexMBAnimExportCommon/userObjects.h"
#include "system/exec.h"

#include "shared.h"

//Dirty, but does the trick for now.
static char* NULL_STRING = new char[1];

using namespace rage;

char* ReturnNullString()
{
	NULL_STRING[0]='\0';
	return NULL_STRING;
}

__declspec(dllexport) bool IsCutsceneExporterInitialized_Py()
{
	return (RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst() != NULL);
}

__declspec(dllexport) bool CutsceneExporterInitialize_Py()
{
	if(RexRageCutsceneExportTool::CreateCurrentCutsceneExportToolInst())
	{
		return true;
	}

	return false;
}

__declspec(dllexport) const char* GetCutsceneSceneName_Py( ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetSceneName();
	}

	return ReturnNullString();
}

__declspec(dllexport) const char* GetCutsceneExportPath_Py( ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetExportPath();
	}

	return ReturnNullString();
}

__declspec(dllexport) const char* GetCutsceneFaceExportPath_Py( ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetFaceExportPath();
	}

	return ReturnNullString();
}

__declspec(dllexport) int GetNumberCutsceneShots_Py( ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts();
	}

	return 0;
}

__declspec(dllexport) void ClearCutsceneShots_Py( ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->ClearPartSpreadsheet();
		RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().Clear();
	}
}

__declspec(dllexport) const char* GetCutsceneShotName_Py( int index ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
			return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetName();
	}

	return ReturnNullString();
}

__declspec(dllexport) int GetCutsceneShotStartRange_Py( int index ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
			return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetRangeStart();
	}

	return -1;
}

__declspec(dllexport) int GetCutsceneShotEndRange_Py( int index ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
			return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetRangeEnd();
	}

	return -1;
}

__declspec(dllexport) bool SetCutsceneShotStartRange_Py( int index , int iRangeStart) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->SetRangeStart(iRangeStart);
			char cRange[RAGE_MAX_PATH];
			sprintf_s(cRange, RAGE_MAX_PATH, "%d - %d", iRangeStart, RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetRangeEnd());
			RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetPartSpreadsheet()->SetCell( index, RexRageCutsceneExportTool::SHOT_RANGE_SPREADSHEET_COLUMN, cRange );
			return true;
		}
	}

	return false;
}

__declspec(dllexport) bool SetCutsceneShotEndRange_Py( int index , int iRangeEnd) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->SetRangeEnd(iRangeEnd);
			char cRange[RAGE_MAX_PATH];
			sprintf_s(cRange, RAGE_MAX_PATH, "%d - %d", RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetRangeStart(), iRangeEnd);
			RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetPartSpreadsheet()->SetCell( index, RexRageCutsceneExportTool::SHOT_RANGE_SPREADSHEET_COLUMN, cRange );
			return true;
		}
	}

	return false;
}

__declspec(dllexport) bool SetCutsceneStoryMode_Py( bool bStoryMode) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		for(int i=0; i < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts(); ++i)
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(i)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_STORY_MODE_FLAG, bStoryMode ); 
		}

		rexMBAnimExportCommon::SetStoryMode(bStoryMode); 
		return true;
	}

	return false;
}

__declspec(dllexport) bool SetCutsceneShotDisableDOF_Py( int index , bool bDisableDOF) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_END_FLAG, bDisableDOF );
			return true;
		}
	}

	return false;
}

__declspec(dllexport) bool SetCutsceneShotEnableDOFFirstCut_Py( int index , bool bEnableDOF) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_FLAG, bEnableDOF );
			return true;
		}
	}

	return false;
}

__declspec(dllexport) bool SetCutsceneShotEnableDOFSecondCut_Py( int index , bool bEnableDOF) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_IN_GAME_DOF_START_SECOND_CUT_FLAG, bEnableDOF );
			return true;
		}
	}

	return false;
}

__declspec(dllexport) bool SetCutsceneShotCatchupCamera_Py(int index, bool bEnable)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_CATCHUP_CAMERA_FLAG, bEnable );
			return true;
		}
	}
	return false;
}

__declspec(dllexport) bool SetCutsceneShotBlendOut_Py(int index, bool bEnable, int duration, int offset)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			RexRageCutscenePart* part = RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index);
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( part->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_BLENDOUT_CAMERA_FLAG, bEnable );
			if (bEnable)
			{	
				part->GetCutsceneFile()->SetBlendOutCutsceneDuration(duration);
				part->GetCutsceneFile()->SetBlendOutCutsceneOffset(offset);
			}
			return true;
		}
	}
	return false;
}

__declspec(dllexport) bool SetCutsceneShotFadeOutGame_Py(int index, bool bEnable, float duration)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_OUT_GAME_FLAG, bEnable );
			if (bEnable)
				RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->SetFadeOutGameAtBeginningDuration(duration);
			return true;
		}
	}
	return false;
}

__declspec(dllexport) bool SetCutsceneShotFadeInGame_Py(int index, bool bEnable, float duration)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_IN_GAME_FLAG, bEnable );
			if (bEnable)
				RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->SetFadeInGameAtEndDuration(duration);
			return true;
		}
	}
	return false;
}
__declspec(dllexport) bool SetCutsceneShotFadeOutCutscene_Py(int index, bool bEnable, float duration)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_OUT_FLAG, bEnable );
			if (bEnable)
				RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->SetFadeOutCutsceneAtEndDuration(duration);
			return true;
		}
	}
	return false;
}

__declspec(dllexport) bool SetCutsceneShotFadeInCutscene_Py(int index, bool bEnable, float duration)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_IN_FLAG, bEnable );
			if (bEnable)
				RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->SetFadeInCutsceneAtBeginningDuration(duration);
			return true;
		}
	}
	return false;
}

__declspec(dllexport) bool SetCutsceneShotSectioning_Py(int index, int option)
{
	if(option < NUM_SECTIONING_METHODS && RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			atBitSet &cutsceneFlags = const_cast<atBitSet &>( RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->GetCutsceneFlags() );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG, option!=NO_SECTIONING_METHOD );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_DURATION_FLAG, option==TIME_SLICE_SECTIONING_METHOD );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG, option==CAMERA_CUT_SECTIONING_METHOD );
			cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_SECTION_BY_SPLIT_FLAG, option==SECTION_SPLIT_PROPERTY_SECTIONING_METHOD );
			return true;
		}
	}
	return false;
}

__declspec(dllexport) bool SetCutsceneShotSectioningDuration_Py(int index, float duration)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts() && duration >= MINIMUM_CUTSCENE_SECTION_TIME)
		{
			RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(index)->GetCutsceneFile()->SetSectionByTimeSliceDuration(duration);
			return true;
		}
	}
	return false;
}

__declspec(dllexport) bool AddCutsceneShot_Py( const char* pShotName ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		RexRageCutscenePart* pPart = rage_new RexRageCutscenePart(&RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutsceneFile(), pShotName);
		RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->AddRowToPartSpreadsheet( pPart );
		RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().AddPart(pPart);
		RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->EnableButtons();
		return true;
	}

	return false;
}

__declspec(dllexport) int GetNumberCutsceneObjects_Py( ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutsceneFile().GetObjectList().GetCount();
	}

	return 0;
}

__declspec(dllexport) const char* GetCutsceneObjectName_Py( int index ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutsceneFile().GetObjectList().GetCount())
			return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutsceneFile().GetObjectList()[index]->GetDisplayName().c_str();
	}

	return ReturnNullString();
}

__declspec(dllexport) bool SetCutsceneObjectHandle_Py( int index , const char* pObjName) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutsceneFile().GetObjectList().GetCount())
		{
			cutfModelObject* pModelObject = dynamic_cast<cutfModelObject*>(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutsceneFile().GetObjectList()[index]);
			if(pModelObject)
			{
				pModelObject->SetHandle(pObjName);
				RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetObjectSpreadsheet()->SetCell(index, RexRageCutsceneExportTool::HANDLE_SPREADSHEET_COLUMN, (char*)pModelObject->GetHandle().GetCStr() );
				return true;
			}
		}
	}
	return false;
}

__declspec(dllexport) const char* GetCutsceneObjectHandle_Py( int index ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutsceneFile().GetObjectList().GetCount())
		{
			cutfModelObject* pModelObject = dynamic_cast<cutfModelObject*>(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutsceneFile().GetObjectList()[index]);
			if(pModelObject)
			{
				return pModelObject->GetHandle().GetCStr();
			}
		}
	}
	
	return ReturnNullString();
}

__declspec(dllexport) void SetCutsceneOffsets_Py( float fX, float fY, float fZ, float fOrientation ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		for(int i=0; i < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts(); ++i)
		{
			Vector3 vPosition(fX, fY, fZ);
			RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(i)->GetCutsceneFile()->SetOffset(vPosition);
			RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(i)->GetCutsceneFile()->SetRotation(fOrientation);
		}
	}
}

__declspec(dllexport) float GetCutsceneOffsetsX_Py() 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(0)->GetCutsceneFile()->GetOffset().GetX();
		}
	}

	return -1;
}

__declspec(dllexport) float GetCutsceneOffsetsY_Py() 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(0)->GetCutsceneFile()->GetOffset().GetY();
		}
	}

	return -1;
}

__declspec(dllexport) float GetCutsceneOffsetsZ_Py() 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(0)->GetCutsceneFile()->GetOffset().GetZ();
		}
	}

	return -1;
}

__declspec(dllexport) float GetCutsceneOrientation_Py() 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetPart(0)->GetCutsceneFile()->GetRotation();
		}
	}

	return -1;
}

__declspec(dllexport) void SetCutsceneShotExportStatus_Py(int index, bool bShotStatus) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		if(index < RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->GetCutsceneExport()->GetCutscenePartFile().GetNumParts())
		{
			RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->SetShotExportStatus(index, bShotStatus);
		}
	}
}

__declspec(dllexport) bool ExportActiveCutscene_Py(bool bP4Integration, bool bZip )
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		RexRageCutsceneExportTool& cutsceneTool = *RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst();
		RexRageCutsceneExport& cutsceneExporter = *cutsceneTool.GetCutsceneExport();

		cutsceneExporter.SetInteractiveMode( false );
		rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
		rexMBAnimExportCommon::SetUsePerforceIntegration(bP4Integration);
		rexMBAnimExportCommon::SetUseWarnIfCheckedOut(false);

		bool bResult = true;

		bResult = bResult && cutsceneTool.Export(false);

		ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/process/cutfile", cutsceneTool.GetSceneName()), "rexmbrage");

		char cLogDir[RAGE_MAX_PATH];
		sprintf(cLogDir, "%s\\cutscene\\process", rexMBAnimExportCommon::GetDefaultLogDir().c_str());

		cutsceneTool.UpdatePartsFromUI();

		bResult = bResult && cutsceneExporter.SaveCutFile();
		bResult = bResult && cutsceneExporter.SaveItemFile();
		bResult = bResult && cutsceneExporter.JoinScenes(true);
		if (bZip)
			bResult = bResult && cutsceneExporter.BuildZip();

		char msg[RAGE_MAX_PATH];
		if (bResult)
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has succeeded.");
		else
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has failed.");
		ULOGGER_MOBO.SetProgressMessage(msg);

		ULOGGER_MOBO.PostExport( cLogDir, false, bResult, "", "" );

		return bResult;
	}
	return false;
}

__declspec(dllexport) bool ExportCameraAnim_Py(bool bP4Integration, bool bZip )
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		RexRageCutsceneExportTool& cutsceneTool = *RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst();
		RexRageCutsceneExport& cutsceneExporter = *cutsceneTool.GetCutsceneExport();

		cutsceneExporter.SetInteractiveMode( false );
		rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
		rexMBAnimExportCommon::SetUsePerforceIntegration(bP4Integration);
		rexMBAnimExportCommon::SetUseWarnIfCheckedOut(false);

		bool bResult = true;

		bResult = cutsceneTool.ExportCamera(false);

		ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/process/cutfile", cutsceneTool.GetSceneName()), "rexmbrage");

		char cLogDir[RAGE_MAX_PATH];
		sprintf(cLogDir, "%s\\cutscene\\process", rexMBAnimExportCommon::GetDefaultLogDir().c_str());

		cutsceneTool.UpdatePartsFromUI();

		bResult = bResult && cutsceneExporter.SaveCutFile();
		bResult = bResult && cutsceneExporter.SaveItemFile();
		bResult = bResult && cutsceneExporter.JoinScenes(true);
		if (bZip)
			bResult = bResult && cutsceneExporter.BuildZip();

		char msg[RAGE_MAX_PATH];
		if (bResult)
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has succeeded.");
		else
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has failed.");
		ULOGGER_MOBO.SetProgressMessage(msg);

		ULOGGER_MOBO.PostExport( cLogDir, false, bResult, "", "" );

		return bResult;
	}
	return false;
}

__declspec(dllexport) bool BuildActiveCutscene_Py(bool bP4Integration, bool bPreview )
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		RexRageCutsceneExportTool& cutsceneTool = *RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst();
		RexRageCutsceneExport& cutsceneExporter = *cutsceneTool.GetCutsceneExport();

		cutsceneExporter.SetInteractiveMode( false );
		rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
		rexMBAnimExportCommon::SetUsePerforceIntegration(bP4Integration);
		rexMBAnimExportCommon::SetUseWarnIfCheckedOut(false);

		ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/process/cutfile", cutsceneTool.GetSceneName()), "rexmbrage");
		char cLogDir[RAGE_MAX_PATH];
		sprintf(cLogDir, "%s\\cutscene\\process", rexMBAnimExportCommon::GetDefaultLogDir().c_str());

		bool bResult = cutsceneExporter.BuildAnimationDictionaries(bPreview);

		char msg[RAGE_MAX_PATH];
		if (bResult)
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has succeeded.");
		else
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has failed.");
		ULOGGER_MOBO.SetProgressMessage(msg);

		ULOGGER_MOBO.PostExport( cLogDir, false, bResult, "", "" );

		return bResult;
	}
	return false;
}

__declspec(dllexport) bool ExportBuildActiveCutscene_Py(bool bP4Integration, bool bPreview ) 
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		RexRageCutsceneExportTool& cutsceneTool = *RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst();
		RexRageCutsceneExport& cutsceneExporter = *cutsceneTool.GetCutsceneExport();
		
		cutsceneExporter.SetInteractiveMode( false );
		rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
		rexMBAnimExportCommon::SetUsePerforceIntegration(bP4Integration);
		rexMBAnimExportCommon::SetUseWarnIfCheckedOut(false);

		bool bResult = true;

		bResult = bResult && cutsceneTool.Export(false);

		// MPW: logs are created within cutsceneexport for the export/build but the logs are in the cutsceneexporttool for savecutfile.
		// This needs moving if possible at some point, for now we just create a log on the fly to grab any errors.
		ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/process/cutfile", cutsceneTool.GetSceneName()), "rexmbrage");

		char cLogDir[RAGE_MAX_PATH];
		sprintf(cLogDir, "%s\\cutscene\\process", rexMBAnimExportCommon::GetDefaultLogDir().c_str());

		cutsceneTool.UpdatePartsFromUI();

		bResult = bResult && cutsceneExporter.SaveCutFile();
		bResult = bResult && cutsceneExporter.SaveItemFile();
		bResult = bResult && cutsceneExporter.JoinScenes(true);
		bResult = bResult && cutsceneExporter.BuildZip();
		bResult = bResult && cutsceneExporter.BuildAnimationDictionaries(bPreview);

		char msg[RAGE_MAX_PATH];
		if (bResult)
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has succeeded.");
		else
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has failed.");
		ULOGGER_MOBO.SetProgressMessage(msg);

		ULOGGER_MOBO.PostExport( cLogDir, false, bResult, "", "" );

		return bResult;
		
	}
	return false;
}

__declspec(dllexport) bool ExportBuildCutscene2_Py( bool bImportCutFile, bool bP4Integration, bool bPreview ) 
{ 
	RexRageCutsceneExport cutsceneExporter;
	cutsceneExporter.SetInteractiveMode( false );

	rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
	rexMBAnimExportCommon::SetUsePerforceIntegration(bP4Integration);
	rexMBAnimExportCommon::SetUseWarnIfCheckedOut(false);

	atString assetPath = rexMBAnimExportCommon::GetProjectOrDlcAssetsDir(atString(""));
	char czPartFile[RAGE_MAX_PATH];
	sprintf(czPartFile, "%s\\cuts\\%s\\data.cutpart", assetPath.c_str(), rexMBAnimExportCommon::GetFBXFileName());

	if(cutsceneExporter.OpenPartFile( czPartFile ))
	{
		bool bResult = true;

		// load persistent data from the fbx, we need this for outsource so they can edit the fbx and save then we can batch it local.
		FBString dataStr = UserObjects::FindCutsceneMarkupData( true )->m_Data;
		if(dataStr.GetLen())
		{
			bResult = cutsceneExporter.LoadCutsceneDataFromString( (char*)dataStr );
		}
		else // If we cant load persistant, try and load the master
		{
			char cPath[RAGE_MAX_PATH];
			ASSET.RemoveNameFromPath(cPath, RAGE_MAX_PATH, czPartFile);

			char cMaster[RAGE_MAX_PATH];
			sprintf(cMaster, "%s/data_master.cutxml", cPath);

			// We load the master, we need the object data, since we have no UI the persistent data isnt loaded.
			bResult = cutsceneExporter.OpenCutFile(cMaster);
		}

		if (bImportCutFile)
		{
			char cPath[RAGE_MAX_PATH];
			ASSET.RemoveNameFromPath(cPath, RAGE_MAX_PATH, czPartFile);

			char cCutFile[RAGE_MAX_PATH];
			sprintf(cCutFile, "%s/data.cutxml", cPath);

			bResult = cutsceneExporter.OpenCutFile( cCutFile );
		}

		atArray<SModelObject> otherModelObjects;
		atArray<SModelObject> modelObjects;
		cutsceneExporter.GenerateModelObjectList( modelObjects );

		cutsceneExporter.ValidateCutsceneExportData( );

		rexMBAnimExportCommon::LoadBoneTranslationConfig( "Ped" );
		rexMBAnimExportCommon::LoadBoneTranslationConfig( "Prop" );
		rexMBAnimExportCommon::LoadBoneTranslationConfig( "Vehicle" );

		bResult = bResult && cutsceneExporter.Export( modelObjects, otherModelObjects, false, true, false );

		// MPW: logs are created within cutsceneexport for the export/build but the logs are in the cutsceneexporttool for savecutfile.
		// This needs moving if possible at some point, for now we just create a log on the fly to grab any errors.
		ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/process/cutfile", ""), "rexmbrage");

		char cLogDir[RAGE_MAX_PATH];
		sprintf(cLogDir, "%s\\cutscene\\process", rexMBAnimExportCommon::GetDefaultLogDir().c_str());

		//if(bExportCutFile)
		{
			bResult = bResult && cutsceneExporter.SaveCutFile();
			bResult = bResult && cutsceneExporter.SaveItemFile();
			bResult = bResult && cutsceneExporter.JoinScenes(true);
			bResult = bResult && cutsceneExporter.BuildZip();
		}

		if(bResult)
		{
			bResult = bResult && cutsceneExporter.BuildAnimationDictionaries(bPreview);
		}

		char msg[RAGE_MAX_PATH];
		if (bResult)
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has succeeded.");
		else
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has failed.");
		ULOGGER_MOBO.SetProgressMessage(msg);

		ULOGGER_MOBO.PostExport( cLogDir, false, bResult, "", "" );
		return bResult;
	}

	return false; 
}

__declspec(dllexport) bool ExportBuildCutscene_Py( const char* pPartFile, bool /*bExportCutFile*/, bool bImportCutFile, bool bP4Integration, bool bPreview ) 
{ 
	RexRageCutsceneExport cutsceneExporter;
	cutsceneExporter.SetInteractiveMode( false );

	rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
	rexMBAnimExportCommon::SetUsePerforceIntegration(bP4Integration);
	rexMBAnimExportCommon::SetUseWarnIfCheckedOut(false);

	if(cutsceneExporter.OpenPartFile( pPartFile ))
	{
		bool bResult = true;

		// load persistent data from the fbx, we need this for outsource so they can edit the fbx and save then we can batch it local.
		FBString dataStr = UserObjects::FindCutsceneMarkupData( true )->m_Data;
		if(dataStr.GetLen())
		{
			bResult = cutsceneExporter.LoadCutsceneDataFromString( (char*)dataStr );
		}
		else // If we cant load persistant, try and load the master
		{
			char cPath[RAGE_MAX_PATH];
			ASSET.RemoveNameFromPath(cPath, RAGE_MAX_PATH, pPartFile);

			char cMaster[RAGE_MAX_PATH];
			sprintf(cMaster, "%s/data_master.cutxml", cPath);

			// We load the master, we need the object data, since we have no UI the persistent data isnt loaded.
			bResult = cutsceneExporter.OpenCutFile(cMaster);
		}
		
		if (bImportCutFile)
		{
			char cPath[RAGE_MAX_PATH];
			ASSET.RemoveNameFromPath(cPath, RAGE_MAX_PATH, pPartFile);

			char cCutFile[RAGE_MAX_PATH];
			sprintf(cCutFile, "%s/data.cutxml", cPath);

			bResult = cutsceneExporter.OpenCutFile( cCutFile );
		}

		atArray<SModelObject> otherModelObjects;
		atArray<SModelObject> modelObjects;
		cutsceneExporter.GenerateModelObjectList( modelObjects );

		cutsceneExporter.ValidateCutsceneExportData( );

		rexMBAnimExportCommon::LoadBoneTranslationConfig( "Ped" );
		rexMBAnimExportCommon::LoadBoneTranslationConfig( "Prop" );
		rexMBAnimExportCommon::LoadBoneTranslationConfig( "Vehicle" );

		bResult = bResult && cutsceneExporter.Export( modelObjects, otherModelObjects, false, true, false );

		// MPW: logs are created within cutsceneexport for the export/build but the logs are in the cutsceneexporttool for savecutfile.
		// This needs moving if possible at some point, for now we just create a log on the fly to grab any errors.
		ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/process/cutfile", ""), "rexmbrage");

		char cLogDir[RAGE_MAX_PATH];
		sprintf(cLogDir, "%s\\cutscene\\process", rexMBAnimExportCommon::GetDefaultLogDir().c_str());

		//if(bExportCutFile)
		{
			bResult = bResult && cutsceneExporter.SaveCutFile();
			bResult = bResult && cutsceneExporter.SaveItemFile();
			bResult = bResult && cutsceneExporter.JoinScenes(true);
			bResult = bResult && cutsceneExporter.BuildZip();
		}

		if(bResult)
		{
			bResult = bResult && cutsceneExporter.BuildAnimationDictionaries(bPreview);
		}

		char msg[RAGE_MAX_PATH];
		if (bResult)
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has succeeded.");
		else
			sprintf_s( msg, RAGE_MAX_PATH, "Exporting cutscene has failed.");
		ULOGGER_MOBO.SetProgressMessage(msg);

		ULOGGER_MOBO.PostExport( cLogDir, false, bResult, "", "" );


		return bResult;
	}

	return false; 
}

__declspec(dllexport) bool ImportCutFile_Py( const char* pCutFile ) 
{ 
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->ImportCutFile(pCutFile);
	}

	return false; 
}

__declspec(dllexport) bool ImportCutPart_Py( const char* pCutPart ) 
{ 
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->ImportCutPart(pCutPart);
	}

	return false; 
}

__declspec(dllexport) bool AddCutsceneCamera_Py( const char* pCameraName, bool bAddFade ) 
{ 
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->AddCameraToSpreadsheet(pCameraName, bAddFade);
	}

	return false; 
}

__declspec(dllexport) bool RemoveCutsceneObject_Py(const char* pName) 
{ 
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->RemoveFromSpreadsheet(pName);
	}

	return false; 
}

__declspec(dllexport) bool RemoveCutsceneAllObjects_Py() 
{ 
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->RemoveAllFromSpreadsheet();
		return true;
	}

	return false; 
}

__declspec(dllexport) bool Export_Py() 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		rexMBAnimExportCommon::SetExporterMode(INGAME_EXPORTER);

		RexRageAnimExportTool::Export(RexRageAnimExportTool::GetCurrentAnimExportToolInst(), true);

		return true;
	}

	return false; 
}

__declspec(dllexport) void AddAdditionalModelEntry_Py( const char* pTargetModel, const char* pModelName, const char* pTrackName, const char* pAttributeName ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		rexMBAnimExportCommon::SetExporterMode(INGAME_EXPORTER);

		RexRageAnimExportTool::GetCurrentAnimExportToolInst()->AddAdditionalModelEntry(pTargetModel, pModelName, pTrackName, pAttributeName);
	}
}

__declspec(dllexport) bool AddCutsceneFace_Py(const char* pTargetModel, const char* pFaceName)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->AddFaceToSpreadsheet(pTargetModel, pFaceName);
	}
	return false;
}

__declspec(dllexport) bool RemoveCutsceneFace_Py(const char* pTargetModel)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->RemoveFaceFromSpreadsheet(pTargetModel);
	}
	return false;
}

__declspec(dllexport) bool AddCutscenePed_Py(const char* pTargetModel, bool bAddFace)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->AddPedToSpreadsheet(pTargetModel, bAddFace);
	}
	return false;
}

__declspec(dllexport) bool AddCutsceneProp_Py(const char* pTargetModel)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->AddPropToSpreadsheet(pTargetModel);
	}
	return false;
}

__declspec(dllexport) bool AddCutsceneVehicle_Py(const char* pTargetModel)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->AddVehicleToSpreadsheet(pTargetModel);
	}
	return false;
}

__declspec(dllexport) bool AddCutsceneAudio_Py(const char* pTargetModel)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->AddAudioToSpreadsheet(pTargetModel);
	}
	return false;
}

__declspec(dllexport) bool AddCutsceneWeapon_Py(const char* pTargetModel)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		return RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->AddWeaponToSpreadsheet(pTargetModel);
	}
	return false;
}

__declspec(dllexport) const char* GetExportPath_Py() 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetExportPath();
	}

	return ReturnNullString();
}

__declspec(dllexport) void RenameClip_Py( const char* pName ) 
{ 
		FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			for (int j = 0; j < lTrack->Clips.GetCount(); j++)
			{
				HFBStoryClip lClip = lTrack->Clips[j];
				
				if(lClip->Selected)
				{
					lClip->Name = (char*)pName;
				}
			}
		}
	}
}

__declspec(dllexport) void SetExportPath_Py( const char* pExportPath ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		RexRageAnimExportTool::SetExportPath( pExportPath );
	}
}

__declspec(dllexport) void SetControlFile_Py( const char* pControlFile )
{
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		RexRageAnimExportTool::SetControlFile(pControlFile);
	}
}

__declspec(dllexport) void DeriveAndSetExportPath_Py( ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		atString sDerivedExportPath = RexRageAnimExportTool::GetCurrentAnimExportToolInst()->DeriveOutputPathFromSourceFbx();
		RexRageAnimExportTool::SetExportPath( sDerivedExportPath );
	}
}

__declspec(dllexport) bool SetNameSpace_Py( int idx, const char* pNewNamespace ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::SetModelPairNamespace( idx, pNewNamespace );	
	}

	return false;
}

__declspec(dllexport) int GetModelCount_Py() 
{
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetModelCount();
	}

	return 0;
}

__declspec(dllexport) bool IsAnimationExporterInitialized_Py()
{
	return (RexRageAnimExportTool::GetCurrentAnimExportToolInst() != NULL);
}

__declspec(dllexport) bool EditRootEntry_Py(const char* rootModel, const char* modelName, char* skeletonFile, char* modelType, char* specFile, char* facialAttributeFile)
{
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->EditRootEntry(rootModel, modelName, skeletonFile, modelType, specFile, facialAttributeFile);
	}

	return false;
}

__declspec(dllexport) bool SetExportModel_Py(const char* rootModel, bool value)
{
	if (RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->SetExportModel(rootModel, value);
	}

	return false;
}

__declspec(dllexport) bool SetMergeFacial_Py(const char* rootModel, bool value)
{
	if (RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->SetMergeFacial(rootModel, value);
	}

	return false;
}

__declspec(dllexport) bool SetAltOutputPath_Py(const char* rootModel, const char* altOutputPath)
{
	if (RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->SetAltOutputPath(rootModel, altOutputPath);
	}

	return false;
}

__declspec(dllexport) bool PopulateTakesExportInfo_Py()
{
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->PopulateTakeExportInfo();
	}

	return false;
}

__declspec(dllexport) bool SetTakeExportSetting_Py(const char* takeName, bool setExport)
{
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->SetTakeExportSetting(takeName, setExport);
	}

	return false;
}

__declspec(dllexport) bool SetTakeAdditiveSetting_Py(const char* takeName, bool setAdditive)
{
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->SetTakeAdditiveSetting(takeName, setAdditive);
	}

	return false;
}

__declspec(dllexport) bool SetTakeControlFile_Py(const char* takeName, const char* ctrlFile)
{
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->SetTakeControlFile(takeName, ctrlFile);
	}

	return false;
}

__declspec(dllexport) bool SetTakeCompressionFile_Py(const char* takeName, const char* compressionFile)
{
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		return RexRageAnimExportTool::GetCurrentAnimExportToolInst()->SetTakeCompressionFile(takeName, compressionFile);
	}
	
	return false;
}

__declspec(dllexport) const char* GetModelName_Py( unsigned int idx ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		RexRageAnimExportTool::AetRootMoverPair* SpreadsheetRow = RexRageAnimExportTool::GetCurrentAnimExportToolInst()->GetModelPairRow(idx);
		return (SpreadsheetRow)?SpreadsheetRow->m_rootModel.c_str():ReturnNullString();
	}
	return ReturnNullString();
}

__declspec(dllexport) const char* GetModelMover_Py( unsigned int idx ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		RexRageAnimExportTool::AetRootMoverPair* SpreadsheetRow = RexRageAnimExportTool::GetCurrentAnimExportToolInst()->GetModelPairRow(idx);
		return (SpreadsheetRow)?SpreadsheetRow->m_moverModel.c_str():ReturnNullString();
	}
	return ReturnNullString();
}

__declspec(dllexport) const char* GetModelSpecFile_Py( unsigned int idx ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		RexRageAnimExportTool::AetRootMoverPair* SpreadsheetRow = RexRageAnimExportTool::GetCurrentAnimExportToolInst()->GetModelPairRow(idx);
		return (SpreadsheetRow)?SpreadsheetRow->m_charCtrlFile.c_str():ReturnNullString();
	}
	return ReturnNullString();
}

__declspec(dllexport) const char* GetModelType_Py( unsigned int idx ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		RexRageAnimExportTool::AetRootMoverPair* SpreadsheetRow = RexRageAnimExportTool::GetCurrentAnimExportToolInst()->GetModelPairRow(idx);
		return (SpreadsheetRow)?SpreadsheetRow->m_modelType.c_str():ReturnNullString();
	}
	return ReturnNullString();
}

__declspec(dllexport) const char* GetModelSkeleton_Py( unsigned int idx ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		RexRageAnimExportTool::AetRootMoverPair* SpreadsheetRow = RexRageAnimExportTool::GetCurrentAnimExportToolInst()->GetModelPairRow(idx);
		return (SpreadsheetRow)?SpreadsheetRow->m_skeletonFile.c_str():ReturnNullString();
	}
	return ReturnNullString();
}

__declspec(dllexport) const char* GetModelFacialAttribute_Py( unsigned int idx ) 
{ 
	if(RexRageAnimExportTool::GetCurrentAnimExportToolInst())
	{
		RexRageAnimExportTool::AetRootMoverPair* SpreadsheetRow = RexRageAnimExportTool::GetCurrentAnimExportToolInst()->GetModelPairRow(idx);
		return (SpreadsheetRow)?SpreadsheetRow->m_facialAttribute.c_str():ReturnNullString();
	}
	return ReturnNullString();
}

__declspec(dllexport) bool SetCutsceneConcatMode_Py(bool bEnabled)
{
	if(RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst())
	{
		RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst()->SetConcatMode(bEnabled);
		
	}
	return false;
}

__declspec(dllexport) bool SetLightTransformInMotionbuilderSpace_Py(const char* name, float x, float y, float z, float quatx, float quaty, float quatz, float quatw)
{
	Quaternion quatYUp(quatx, quaty, quatz, quatw );
	Matrix34 rageMat;

	Quaternion quatZUp;
	quatZUp.x = -quatYUp.x;
	quatZUp.y = quatYUp.z;
	quatZUp.z = quatYUp.y;
	quatZUp.w = quatYUp.w;
	rageMat.FromQuaternion(quatZUp);

	rageMat.d = Vector3(x,y,z);

	float y2 = rageMat.d.y;
	rageMat.d.x = -rageMat.d.x;
	rageMat.d.y = rageMat.d.z;
	rageMat.d.z = y2;

	rageMat.RotateFullY(PI);

	FBMatrix mat;
	RageMatrix34ToFBMatrix(rageMat, mat);

	HFBModel m = RAGEFindModelByName(name);
	if(m)
	{
		m->SetMatrix(mat, kModelTransformation, false);
		return true;
	}

	return false;
}

#pragma warning(push)
#pragma warning(disable:4100)

FBLibraryDeclare( orimpexpgames )
{
	FBLibraryRegister( RexRageCutsceneExportTool );

    FBLibraryRegister( RexRageConcatTool );
	FBLibraryRegister( RexRageEffectsTool );
	
	FBLibraryRegister( RexRageAnimExportTool );
	FBLibraryRegister( RexRageAnimExportCustomMgr );
}

FBLibraryDeclareEnd;

#pragma warning(pop)

bool FBLibrary::LibInit()	{ 
	
	char appName[] = "rexMBRage.dll";
	char* p_Argv[1];

	p_Argv[0] = appName;

	sysParam::Init(1,p_Argv);

	INIT_PARSER;

    RexRageCutsceneExport::InitClass();
	return true; 
}

bool FBLibrary::LibOpen()	{ return true; }
bool FBLibrary::LibReady()	{ return true; }
bool FBLibrary::LibClose()	{ return true; }

bool FBLibrary::LibRelease()
{ 
    RexRageCutsceneExport::ShutdownClass();
	UserObjects::Destroy();
	SHUTDOWN_PARSER;
	return true; 
}

