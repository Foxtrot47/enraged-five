#include "additionalmodelpopup.h"

AdditionalModelsPopup::AdditionalModelsPopup(atArray< AetAdditionalModel* >& models) : m_additionalModels(models){}

AdditionalModelsPopup::~AdditionalModelsPopup(){}

void AdditionalModelsPopup::UICreate()
{
    Caption = "Add Model(s)";
	
	Region.X = 200;
	Region.Y = 300;
	Region.Width = 700;
	m_iAttachTopY = rexMBAnimExportCommon::c_iMargin;
	Region.Height = m_iAttachTopY + (rexMBAnimExportCommon::c_iRowHeight * 8) + rexMBAnimExportCommon::c_iMargin;

	// Spreadsheet
	AddRegion( "Spreadsheet", "Spreadsheet",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-(rexMBAnimExportCommon::c_iMargin*2) - (rexMBAnimExportCommon::c_iButtonWidth + 20), kFBAttachRight, NULL, 1.0,
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight , kFBAttachBottom, NULL, 1.0 );
	SetControl( "Spreadsheet", m_spreadsheet );

	AddRegion( "AddModel", "AddModel",
		-(rexMBAnimExportCommon::c_iMargin) - (rexMBAnimExportCommon::c_iButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
		m_iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0);
	SetControl( "AddModel", m_addRootButton );

	AddRegion( "RemoveModel", "RemoveModel",
		-(rexMBAnimExportCommon::c_iMargin) - (rexMBAnimExportCommon::c_iButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
		m_iAttachTopY + rexMBAnimExportCommon::c_iRowHeight, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0);
	SetControl( "RemoveModel", m_removeButton );

	AddRegion( "OkButton", "OkButton",
		-(rexMBAnimExportCommon::c_iMargin) - (rexMBAnimExportCommon::c_iButtonWidth + 20), kFBAttachRight, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0);
	SetControl( "OkButton", m_okButton );
}

void AdditionalModelsPopup::UIConfigure()
{
	m_addRootButton.Caption = "Add";
	m_addRootButton.OnClick.Add( this, (FBCallback)&AdditionalModelsPopup::AddRootButton_Click );

	m_okButton.Caption = "OK";
	m_okButton.OnClick.Add( this, (FBCallback)&AdditionalModelsPopup::OKButton_Click );

	m_removeButton.Caption = "Remove";
	m_removeButton.OnClick.Add( this, (FBCallback)&AdditionalModelsPopup::RemoveModelButton_Click );

	//Remove all the items in the spread sheet
	m_spreadsheet.Clear();

	//Restore the column names and sizing
	m_spreadsheet.GetColumn( kModelIndex ).Width = 60;
	m_spreadsheet.Caption = "Index";

	m_spreadsheet.ColumnAdd( "Model Name" );
	m_spreadsheet.GetColumn( kModelName ).Justify = kFBTextJustifyLeft;
	m_spreadsheet.GetColumn( kModelName ).ReadOnly = true;
	m_spreadsheet.GetColumn( kModelName ).Style = kFBCellStyleString;
	m_spreadsheet.GetColumn( kModelName ).Width = 155;

	m_spreadsheet.ColumnAdd( "Track Type" );
	m_spreadsheet.GetColumn( kTrackType ).Justify = kFBTextJustifyLeft;
	m_spreadsheet.GetColumn( kTrackType ).ReadOnly = false;
	m_spreadsheet.GetColumn( kTrackType ).Style = kFBCellStyleMenu;
	m_spreadsheet.GetColumn( kTrackType ).Width = 155;

	m_spreadsheet.ColumnAdd( "Attribute Name" );
	m_spreadsheet.GetColumn( kAttributeName ).Justify = kFBTextJustifyLeft;
	m_spreadsheet.GetColumn( kAttributeName ).ReadOnly = false;
	m_spreadsheet.GetColumn( kAttributeName ).Style = kFBCellStyleString;
	m_spreadsheet.GetColumn( kAttributeName ).Width = 155;

	m_spreadsheet.MultiSelect = false;
	m_spreadsheet.OnRowClick.Add( this, (FBCallback)&AdditionalModelsPopup::SpreadSheet_RowClick );
	m_spreadsheet.OnCellChange.Add( this, (FBCallback)&AdditionalModelsPopup::SpreadSheet_CellChange );

	m_iModelPairSheetSelRowIndex = -1;
}

void AdditionalModelsPopup::SetData()
{
	for(int i=0; i < m_additionalModels.GetCount(); ++i)
	{
		AddRow(m_additionalModels[i]);
	}
}

void AdditionalModelsPopup::OKButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	Close(TRUE);
}

void AdditionalModelsPopup::AddRootButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	FBModelList selectedModels;
	FBGetSelectedModels( selectedModels, NULL, true );

	int nSelectedModels = selectedModels.GetCount();
	if(nSelectedModels)
	{
		for(int modelIdx = 0; modelIdx < nSelectedModels; modelIdx++)
		{
			//Get the name of the selected model
			const char* szModelName = (const char*)selectedModels[modelIdx]->LongName;

			// Ensure ther eis only one namespace
			atString sModelname(szModelName);
			atArray<atString> modelNameTokens;
			sModelname.Split(modelNameTokens, ":");
			if(modelNameTokens.GetCount() <= 2)
			{
				AetAdditionalModel* p = rage_new AetAdditionalModel;
				p->m_modelName = sModelname.c_str();
				p->m_trackType = "genericControl";
				p->m_attributeName = "";
				AddRow(p);
				m_additionalModels.PushAndGrow(p);
			}
		}
	}
}

void AdditionalModelsPopup::AddRow(AetAdditionalModel* pModelPair)
{
	//Add the row and set it with all information.
	m_spreadsheet.RowAdd( "", m_iModelPairSpreadRowId );
	m_spreadsheet.SetCell( m_iModelPairSpreadRowId, kModelName, const_cast<char*>(pModelPair->m_modelName.c_str()));

	atString trackTypes;
	for( std::map<std::string, std::string>::iterator itr = rexMBAnimExportCommon::GetAdditionalModelTrackTypes().begin(); itr != rexMBAnimExportCommon::GetAdditionalModelTrackTypes().end(); ++itr)
	{
		trackTypes += itr->first.c_str();
		trackTypes += "~";
	}

	m_spreadsheet.SetCell( m_iModelPairSpreadRowId, kTrackType, const_cast<char*>(trackTypes.c_str()));
	m_spreadsheet.SetCell( m_iModelPairSpreadRowId, kAttributeName, const_cast<char*>(pModelPair->m_attributeName.c_str()));

	//Set the index of the track type file from the drop down.
	const char*	celVal = NULL;
	m_spreadsheet.GetCell( m_iModelPairSpreadRowId, kTrackType, celVal );
	atString trackTypeStr = atString( pModelPair->m_trackType.c_str() );
	atString trackTypeComboStr = atString( celVal );
	int trackTypeIdx = rexMBAnimExportCommon::ResolveComboIndexFromString( trackTypeStr, trackTypeComboStr );
	if ( trackTypeIdx == -1 ) trackTypeIdx = 0;
	m_spreadsheet.SetCell( m_iModelPairSpreadRowId, kTrackType, trackTypeIdx );

	//Add it to the map.
	m_rowIdToModelPairMap[m_iModelPairSpreadRowId] = pModelPair;

	m_iModelPairSpreadRowId++;
}

void AdditionalModelsPopup::SpreadSheet_CellChange( HISender /*pSender*/, HKEvent pEvent)
{
	FBEventSpread	spreadEvent(pEvent);

	int iRow = spreadEvent.Row;
	int	iColumn = spreadEvent.Column;

	std::map<int, AetAdditionalModel*>::iterator it = m_rowIdToModelPairMap.find(iRow);
	Assertf( it != m_rowIdToModelPairMap.end(), "SpreadSheet_CellChange, failed to find row id of '%d' in model map", iRow);

	AetAdditionalModel* pModelPair = it->second;

	switch(iColumn)
	{
	case kTrackType:
		{
			const char*	celVal = NULL;
			int		celValIdx;
			atString celValName("");

			m_spreadsheet.GetCell( iRow, iColumn, celVal );
			m_spreadsheet.GetCell( iRow, iColumn, celValIdx );
			atString cellValues(celVal);
			celValName = rexMBAnimExportCommon::ResolveComboValueFromDelimitedString( celValIdx, cellValues );
			pModelPair->m_trackType = celValName;
			break;
		}
	case kAttributeName:
		{
			const char*	celVal = NULL;
			m_spreadsheet.GetCell( iRow, iColumn, celVal);
			pModelPair->m_attributeName = celVal;
			break;
		}
	}
}

void AdditionalModelsPopup::SpreadSheet_RowClick( HISender /*pSender*/, HKEvent pEvent)
{
	FBEventSpread e( pEvent );
	m_iModelPairSheetSelRowIndex = e.Row;
}

void AdditionalModelsPopup::RemoveModelButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/)
{
	if(m_iModelPairSheetSelRowIndex != -1)
	{
		std::map<int, AetAdditionalModel*>::iterator it = m_rowIdToModelPairMap.find(m_iModelPairSheetSelRowIndex);
		Assertf( it != m_rowIdToModelPairMap.end(), "Model spread-sheet row ids are out of sync, selected row id isn't in the model map!");

		//Free the segment data
		delete it->second;

		//Remove the item from the row map
		m_rowIdToModelPairMap.erase(it);

		//Remove the row from the spread-sheet
		FBSpreadRow row = m_spreadsheet.GetRow( m_iModelPairSheetSelRowIndex );
		row.Remove();
		
		m_additionalModels.Delete(m_iModelPairSheetSelRowIndex);

		m_iModelPairSheetSelRowIndex = -1;
	}
}