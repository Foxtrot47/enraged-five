// 
// rexMBRage/adddialog.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REXMBRAGE_ADDPOPUP_H__ 
#define __REXMBRAGE_ADDPOPUP_H__ 

#include "atl/array.h"
#include "atl/map.h"
#include "rexMBAnimExportCommon/utility.h"

namespace rage {

class cutfObject;

//##############################################################################

// PURPOSE: A popup for adding components (models, audio) to the Export Window spreadsheet.
class AddComponentsPopup : public FBPopup
{
public:
    enum ESpreadsheetColumn
    {
        NAME_SPREADSHEET_COLUMN = -1,
        TYPE_SPREADSHEET_COLUMN,
        ADD_SPREADSHEET_COLUMN
    };

    AddComponentsPopup();
    ~AddComponentsPopup();

    void UICreate();
    void UIConfigure();

    // PURPOSE: Sets the list of objects that this popup is capable
    //    of creating.  The created objects will be clones of these.
    // PARAMS:
    //    pObjectList - the objects
    void SetTemplateObjectList( const atArray<cutfObject *>& pObjectList );

    // PURPOSE: Sets the models that we want to create objects for
    // PARAMS:
    //    modelList - the list of models
    void SetComponentList( atArray<HFBComponent> &componentList );

    // PURPOSE: Retrieves the list of objects to create for
    //    the selected models that were chosen to be added
    // RETURNS: The list of objects to add.
    // NOTES: This array will be empty when AddNonComponent() returns true.
    const atArray<cutfObject *>& GetObjectList() const;

    // PURPOSE: Indicates if we should launch the AddNonComponentPopup
    //    after this one is closed
    // RETURNS: true if we should launch the AddNonComponentPopup
    bool AddNonComponent() const;

private:
    void ClearSpreadsheet();

    void EnableOKButton();

    int DetermineTypeIndex( HFBComponent pCompoment ) const;

    void RememberAddState();

    atArray<cutfObject *> m_pTemplateObjectList;
    char m_cTypeNameMenu[1024];

    atArray<ConstString> m_cComponentNameList;
    atArray<cutfObject *> m_pObjectList;
    bool m_bAddNonComponent;

    atMap<ConstString, bool> m_cComponentCheckedMap;

    FBSpread m_spreadsheet;
    FBButton m_otherButton;
    FBButton m_okButton;
    FBButton m_cancelButton;

    void Spreadsheet_CellChange( HISender pSender, HKEvent pEvent );
    void OtherButton_Click( HISender pSender, HKEvent pEvent );
    void OkButton_Click( HISender pSender, HKEvent pEvent );
    void CancelButton_Click( HISender pSender, HKEvent pEvent );
};  

inline const atArray<cutfObject *>& AddComponentsPopup::GetObjectList() const
{
    return m_pObjectList;
}

inline bool AddComponentsPopup::AddNonComponent() const
{
    return m_bAddNonComponent;
}

//##############################################################################    

// PURPOSE: A popup for adding non-components to the Export Window spreadsheet.
class AddNonComponentPopup : public FBPopup
{
public:
    AddNonComponentPopup();
    ~AddNonComponentPopup();

    void UICreate();
    void UIConfigure();

    // PURPOSE: Sets the list of objects that this popup is capable
    //    of creating.  The created object will be a clone of one of these.
    // PARAMS:
    //    pObjectList - the objects
    void SetTemplateObjectList( const atArray<cutfObject *>& pObjectList );

    // PURPOSE: Retrieves the object to add to the scene.
    // RETURNS: The object to add to the scene
    cutfObject* GetObject() const;

private:
    atArray<cutfObject *> m_pTemplateObjectList;

    cutfObject* m_pObject;

    FBLabel m_objectTypeLabel;
    FBList m_objectTypeComboBox;
    FBLabel m_nameLabel;
    FBEdit m_nameTextBox;
    FBButton m_okButton;
    FBButton m_cancelButton;

    void ObjectTypeComboBox_Change( HISender pSender, HKEvent pEvent );
    void NameTextBox_Change( HISender pSender, HKEvent pEvent );
    void HelpButton_Click( HISender pSender, HKEvent pEvent );
    void OkButton_Click( HISender pSender, HKEvent pEvent );
    void CancelButton_Click( HISender pSender, HKEvent pEvent );
};  

inline cutfObject* AddNonComponentPopup::GetObject() const
{
    return m_pObject;
}

//##############################################################################    

// PURPOSE: A popup for adding non-components to the Export Window spreadsheet.
class AddPartPopup : public FBPopup
{
public:

	enum EItemTypes
	{
		ITEM_INTERNAL=0,
		ITEM_EXTERNAL
	};

	AddPartPopup();
	~AddPartPopup();

	void UICreate();
	void UIConfigure();

	// PURPOSE: Sets the list of objects that this popup is capable
	//    of creating.  The created object will be a clone of one of these.
	// PARAMS:
	//    pObjectList - the objects
	void SetTemplateObjectList(const atArray<atString>& pObjectList );

	const char* GetName() const;
	const char* GetTypeName();

private:

	FBLabel m_objectTypeLabel;
	FBList m_objectTypeComboBox;
	FBLabel m_nameLabel;
	FBEdit m_nameTextBox;
	FBButton m_okButton;
	FBButton m_cancelButton;

	void ObjectTypeComboBox_Change( HISender pSender, HKEvent pEvent );
	void NameTextBox_Change( HISender pSender, HKEvent pEvent );
	void HelpButton_Click( HISender pSender, HKEvent pEvent );
	void OkButton_Click( HISender pSender, HKEvent pEvent );
	void CancelButton_Click( HISender pSender, HKEvent pEvent );
};  

inline const char* AddPartPopup::GetName() const
{
	return m_nameTextBox.Text;
}

inline const char* AddPartPopup::GetTypeName()
{
	int iTypeIndex = m_objectTypeComboBox.ItemIndex;
	if ( iTypeIndex != -1 )
	{
		return m_objectTypeComboBox.Items.GetAt(iTypeIndex);
	}

	return "";
}

//##############################################################################    

} // namespace rage

#endif // __REXMBRAGE_ADDPOPUP_H__ 
