// 
// RemoteConsole\RemoteConsoleInterface.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "RemoteConsoleInterface.h"

#include <Windows.h>

HMODULE	m_RemoteConsoleDll = NULL;

typedef bool (*IsRemoteConsoleConnectedFunc) ();
typedef void (*NotifyFunc) ();

// PURPOSE: Initializes the handle to the REmoteConsole.dll.  Note that instead of calling this 
// at DLL Load-time, as in the loading of this plugin, we have deferred to when a member function is called.
// The DLL does not load successfully when this plug-in is only being initialized.
// RETURN:	bool
bool RemoteConsoleInterface::Initialize()
{
	m_RemoteConsoleDll = LoadLibrary("RemoteConsole.dll");
	if(m_RemoteConsoleDll == NULL)
		return false;

	return true;
}

void RemoteConsoleInterface::Destroy()
{
	if(m_RemoteConsoleDll != NULL)
	{
		FreeLibrary(m_RemoteConsoleDll);
	}
}

bool RemoteConsoleInterface::IsRemoteConsoleConnected()
{
	if(m_RemoteConsoleDll == NULL)
	{
		if(Initialize() == false)
			return NULL;
	}

	IsRemoteConsoleConnectedFunc isRemoteConsoleConnected = (IsRemoteConsoleConnectedFunc) GetProcAddress(m_RemoteConsoleDll, "IsRemoteConsoleConnected");
	if ( isRemoteConsoleConnected == NULL )
	{
		//Unable to find function process!
		return false; 
	}

	return isRemoteConsoleConnected(); 
}



void RemoteConsoleInterface::NotifyCutsceneUpdate()
{
	if(m_RemoteConsoleDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	NotifyFunc notifyCutsceneUpdate = (NotifyFunc) GetProcAddress(m_RemoteConsoleDll, "NotifyCutsceneUpdate");
	if ( notifyCutsceneUpdate == NULL )
	{
		//Unable to find function process!
		return; 
	}

	notifyCutsceneUpdate(); 
}

void RemoteConsoleInterface::NotifyCutscenePatchStarted()
{
	if(m_RemoteConsoleDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	NotifyFunc notifyPatchStart = (NotifyFunc) GetProcAddress(m_RemoteConsoleDll, "NotifyCutscenePatchStarted");
	if ( notifyPatchStart  == NULL )
	{
		//Unable to find function process!
		return; 
	}

	notifyPatchStart(); 
}

void RemoteConsoleInterface::NotifyCutscenePatchCompleted()
{
	if(m_RemoteConsoleDll == NULL)
	{
		if(Initialize() == false)
			return;
	}

	NotifyFunc notifyPatchComplete = (NotifyFunc) GetProcAddress(m_RemoteConsoleDll, "NotifyCutscenePatchCompleted");
	if ( notifyPatchComplete  == NULL )
	{
		//Unable to find function process!
		return; 
	}

	notifyPatchComplete(); 
}