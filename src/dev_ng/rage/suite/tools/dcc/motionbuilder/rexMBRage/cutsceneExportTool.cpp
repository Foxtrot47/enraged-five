// 
// rexMbRage/cutsceneExportTool.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "cutsceneExportTool.h"
#include "editpopup.h"
#include "addpopup.h"
#include "eventpopup.h"
#include "cutfile/cutfdefines.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "rexMBAnimExportCommon/Logger.h"
#include "rexMBAnimExportCommon/toolobjectinterchange.h"
#include "rexMBAnimExportCommon/timerLog.h"
#include "rexMBAnimExportCommon/utility.h"
#include "rexMBRage/RemoteConsoleInterface.h"
#include "rexMBAnimExportCommon/userObjects.h"
#include "system/exec.h"
#include "partpopup.h"
#include "rexMBAnimExportCommon/perforce.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>

#define	EXPORTTOOL__CLASSNAME	RexRageCutsceneExportTool
#define EXPORTTOOL__CLASSSTR	"RexRageCutsceneExportTool"
#define	EXPORTTOOL__CLASS		EXPORTTOOL__CLASSNAME
#define	EXPORTTOOL__LABEL		"Rex Rage Cut-Scene Export"
#define	EXPORTTOOL__DESC		"Rex Rage Cut-Scene Export Tool"

#pragma warning(push)
#pragma warning(disable:4100)

FBToolImplementation( EXPORTTOOL__CLASS );
FBRegisterTool(	EXPORTTOOL__CLASS, EXPORTTOOL__LABEL, EXPORTTOOL__DESC, FB_DEFAULT_SDK_ICON	);

#pragma warning(pop)

static RexRageCutsceneExportTool *s_pCurrentCutsceneExportTool = NULL;
#define NEW_CUTFILE_NAME "<new file>"

#define SCENENAME_MAX_SIZE 64

RexRageCutsceneExportTool* RexRageCutsceneExportTool::GetCurrentCutsceneExportToolInst() { return s_pCurrentCutsceneExportTool; }
RexRageCutsceneExportTool* RexRageCutsceneExportTool::CreateCurrentCutsceneExportToolInst()
{ 
	if(!s_pCurrentCutsceneExportTool)
	{
		s_pCurrentCutsceneExportTool = rage_new RexRageCutsceneExportTool(); 
		s_pCurrentCutsceneExportTool->FBCreate();
	}

	return s_pCurrentCutsceneExportTool;
};

namespace rage {

void FindCutFilesCallback(const fiFindData &data, void *userArg)
{
    atArray<atString>& filenames = *((atArray<atString>*)userArg);

    const char *pExtension = ASSET.FindExtensionInPath( data.m_Name );
    if ( pExtension == NULL )
    {
        return;
    }

    ++pExtension;
    
    if ( stricmp( pExtension, PI_CUTSCENE_XMLFILE_EXT ) == 0 )
    {
        filenames.PushAndGrow( atString(data.m_Name) );
    }
}
//-------------------------------------------------
static bool IsIllegalName( const char* pName )
{
    char name[512];
    safecpy( name, pName, sizeof(name) );

    char *pEnd = &(name[(int)strlen(name) - 1]);

    while ( (pEnd >= name) && (*pEnd != '-') )
    {
        --pEnd;
    }

    if ( (pEnd >= name) && (*pEnd == '-') )
    {
        ++pEnd;

        int len = (int)strlen( pEnd );
        for ( int i = 0; i < len; ++i )
        {
            if ( (*pEnd < '0') || (*pEnd > '9') )
            {
                return false;
            }
        }

        return true;
    }

    return false;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::FBCreate()
{
    m_iObjectsSpreadsheetRowId = 0;
    m_iObjectsSpreadsheetSelRowId = -1;
	m_iPartSpreadsheetRowId = 0;
	m_iPartSpreadsheetSelRowId = -1;
	m_pEditEventPopup = NULL;
	
	PopulateFromCutsceneHandleFile();
	PopulateLightProperties();
	PopulateOverlayTypes();
	PopulateWeaponTypes();
    PopulateComponentObjectTemplateList();
    PopulateNonComponentObjectTemplateList();
    PopulateObjectTypeToEditPopupMap();
	
    UICreate();
    UIConfigure();

	rexMBAnimExportCommon::LoadBoneTranslationConfig( "Ped" );
	rexMBAnimExportCommon::LoadBoneTranslationConfig( "Prop" );
	rexMBAnimExportCommon::LoadBoneTranslationConfig( "Vehicle" );

	rexMBAnimExportCommon::LoadFaceSettingsConfigFile();

    SetSceneName( "" ); 

    NewCutFile();

	// Attach application-level callbacks
    m_Application.OnFileNewCompleted.Add( this, (FBCallback)&RexRageCutsceneExportTool::FileNewCompleted_Event );
    m_Application.OnFileSave.Add( this, (FBCallback)&RexRageCutsceneExportTool::FileSave_Event );
	m_Application.OnFileSaveCompleted.Add( this, (FBCallback)&RexRageCutsceneExportTool::FileSaveCompleted_Event );
    m_Application.OnFileOpenCompleted.Add( this, (FBCallback)&RexRageCutsceneExportTool::FileOpenCompleted_Event );	    

    //This will only get called the first time the tool is created, so we need to check for persistent data
	//in the currently opened file and load it into the tool.  After the tool has been opened once then
	//the loading of persistent data will take place in the file-open completed callback...
	if(FindPersistentData(false))
	{
		LoadFromPersistentData();
	}    

	if(rexMBAnimExportCommon::IsDlc(atString("")))
	{
		SetSceneName(rexMBAnimExportCommon::GetFBXFileName());
		m_sceneNameTextBox.ReadOnly = true;
	}
	else
	{
		m_sceneNameTextBox.ReadOnly = false;
	}

	UpdatePaths();

	char cPartFilename[RAGE_MAX_PATH];
	sprintf(cPartFilename, "%s\\data.cutpart", GetExportPath());

	perforce::Sync(GetExportPath(), "...", false, true);
	perforce::ExecuteQueuedOperations(true);

	OpenPartFile(cPartFilename);
	PopulatePartSpreadsheetFromFile();

	s_pCurrentCutsceneExportTool = this;

    return true;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::FBDestroy()
{
	// Detach application-level callbacks
    m_Application.OnFileNewCompleted.Remove( this, (FBCallback)&RexRageCutsceneExportTool::FileNewCompleted_Event );
	m_Application.OnFileSave.Remove( this, (FBCallback)&RexRageCutsceneExportTool::FileSave_Event);
	m_Application.OnFileOpenCompleted.Remove( this, (FBCallback)&RexRageCutsceneExportTool::FileOpenCompleted_Event);
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UICreate()
{

#ifdef _OUTSOURCE
	StartSize[0] = 970;
	MinSize[0] = 970;
#else
	StartSize[0] = 925;
	MinSize[0] = 925;
#endif
    StartSize[1] = (35 * rexMBAnimExportCommon::c_iRowHeight) + (rexMBAnimExportCommon::c_iMargin * 4);    
    MinSize[1] = (35 * rexMBAnimExportCommon::c_iRowHeight) + (rexMBAnimExportCommon::c_iMargin * 4);

    int iAttachTopY = rexMBAnimExportCommon::c_iMargin;
    int iAttachBottomY = -((rexMBAnimExportCommon::c_iRowHeight * 17) + (rexMBAnimExportCommon::c_iMargin));
    int iExportInfoLayoutHeight = (rexMBAnimExportCommon::c_iRowHeight * 5) + (rexMBAnimExportCommon::c_iMargin * 4);
	int iShotInfoLayoutHeight = (rexMBAnimExportCommon::c_iRowHeight * 10) + (rexMBAnimExportCommon::c_iMargin * 4);

	// Import Cut File
    AddRegion( "ImportCutFileButton", "ImportCutFileButton",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ImportCutFileButton", m_importCutFileButton );

	// Import Cut List
	AddRegion( "ImportCutListButton", "ImportCutListButton",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ImportCutListButton", m_importCutListButton );

	// Refresh Handle File
	AddRegion( "RefreshHandleFileButton", "RefreshHandleFileButton",
		rexMBAnimExportCommon::c_iMargin + (2 * (rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin)), kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "RefreshHandleFileButton", m_refreshHandleFileButton );

	// Version
	AddRegion( "VersionLabel", "VersionLabel",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iLabelWidth*2, kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth -rexMBAnimExportCommon::c_iMargin , kFBAttachRight, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "VersionLabel", m_versionLabel );

	// Help
	AddRegion( "HelpButton", "HelpButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "HelpButton", m_helpButton );

	// Edit Scene Events
	AddRegion( "EditSceneEventsButton", "EditSceneEventsButton",
		rexMBAnimExportCommon::c_iMargin + (3 * (rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin)), kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "EditSceneEventsButton", m_editSceneEventsButton );

#ifdef _OUTSOURCE
	// Verify Cutscene 
	AddRegion( "VerifyCutsceneButton", "VerifyCutsceneButton",
		rexMBAnimExportCommon::c_iMargin + (4 * (rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin)), kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "VerifyCutsceneButton", m_verifyCutsceneButton );
	
		// Perforce Integration
	AddRegion( "PerforceCheckbox", "PerforceCheckbox",
		rexMBAnimExportCommon::c_iMargin + ( 4* (rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin)) + (rexMBAnimExportCommon::c_iMediumLabelWidth*2), kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "PerforceCheckbox", m_usePerforceIntegration );
#else
	// Perforce Integration
	AddRegion( "PerforceCheckbox", "PerforceCheckbox",
		rexMBAnimExportCommon::c_iMargin + ( 4* (rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin)), kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "PerforceCheckbox", m_usePerforceIntegration );

	// Perforce Integration
	AddRegion( "ConcatCheckbox", "ConcatCheckbox",
		rexMBAnimExportCommon::c_iMargin + ( 4* (rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin)) + (rexMBAnimExportCommon::c_iMediumLabelWidth), kFBAttachLeft, "NULL", 1.0,
		iAttachTopY, kFBAttachTop, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "ConcatCheckbox", m_useAutoConcat );
#endif
    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // CutfileInfoLayout
    AddRegion( "CutfileInfoLayout", "CutfileInfoLayout", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
        iAttachBottomY, kFBAttachBottom, NULL, 1.0f );    
    SetControl( "CutfileInfoLayout", m_cutfileInfoLayout );

    UICreateCutfileInfoLayout();

	iAttachBottomY -= rexMBAnimExportCommon::c_iMargin;

	// ShotInfoLayout
	AddRegion( "ShotInfoLayout", "ShotInfoLayout", 
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
		iAttachBottomY, kFBAttachBottom, "NULL", 1.0f,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
		iShotInfoLayoutHeight, kFBAttachNone, NULL, 1.0f );    
	SetControl( "ShotInfoLayout", m_shotInfoLayout );
	
	UICreateShotInfoLayout();

	iAttachBottomY += (iShotInfoLayoutHeight - rexMBAnimExportCommon::c_iRowHeight - rexMBAnimExportCommon::c_iMargin);
    
    // ExportInfoLayout
    AddRegion( "ExportInfoLayout", "ExportInfoLayout", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
        iExportInfoLayoutHeight, kFBAttachNone, NULL, 1.0f );    
    SetControl( "ExportInfoLayout", m_exportInfoLayout );

    UICreateExportInfoLayout();

    iAttachBottomY += (iExportInfoLayoutHeight - rexMBAnimExportCommon::c_iRowHeight);

    // Export Selected Animations
    AddRegion( "ExportSelectedAnimationsButton", "ExportSelectedAnimationsButton",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ExportSelectedAnimationsButton", m_exportSelectedAnimationsButton );

    // Export Spreadsheet Animations
    AddRegion( "ExportSpreadsheetAnimationsButton", "ExportSpreadsheetAnimationsButton",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iSpecialButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ExportSpreadsheetAnimationsButton", m_exportSpreadsheetAnimationsButton );

    // Export Face Anims
    AddRegion( "ExportFaceAnimationButton", "ExportFaceAnimationButton",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iSpecialButtonWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ExportFaceAnimationButton", m_exportFaceAnimationButton );

    // Build Animation Dictionaries
    AddRegion( "BuildAnimationDictionariesButton", "BuildAnimationDictionariesButton",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iSpecialButtonWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "BuildAnimationDictionariesButton", m_buildAnimationDictionariesButton );

	// Preview Animation Dictionaries
	AddRegion( "PatchAnimationDictionariesButton", "PatchAnimationDictionariesButton",
		rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iSpecialButtonWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
		iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
		rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	SetControl( "PatchAnimationDictionariesButton", m_patchAnimationDictionariesButton );

    iAttachBottomY += rexMBAnimExportCommon::c_iRowHeight;

    // Export Camera
    AddRegion( "ExportCameraAnimationButton", "ExportCameraAnimationButton",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ExportCameraAnimationButton", m_exportCameraAnimationButton );

    // Export Cut File
    AddRegion( "ExportCutFileButton", "ExportCutFileButton",
        rexMBAnimExportCommon::c_iMargin + rexMBAnimExportCommon::c_iWideButtonWidth + rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iSpecialButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ExportCutFileButton", m_exportCutFileButton );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UICreateCutfileInfoLayout()
{
    // CutfileInfoLayoutRegion
    m_cutfileInfoLayout.AddRegion( "CutfileInfoLayoutRegion", "CutfileInfoLayoutRegion",
        rexMBAnimExportCommon::c_iMargin * 2, kFBAttachLeft, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iMargin * 3, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin * 2, kFBAttachBottom, NULL, 1.0f );
    m_cutfileInfoLayout.SetControl( "CutfileInfoLayoutRegion", m_cutfileInfoLayoutRegion );

    int iAttachTopY = rexMBAnimExportCommon::c_iMargin * 2;
    int iAttachBottomY = -((rexMBAnimExportCommon::c_iRowHeight) /*+ (rexMBAnimExportCommon::c_iMargin * 2)*/);

    // Spreadsheet
    m_cutfileInfoLayout.AddRegion( "Spreadsheet", "Spreadsheet",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin - (rexMBAnimExportCommon::c_iButtonWidth*2) - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        iAttachBottomY-(rexMBAnimExportCommon::c_iMargin), kFBAttachBottom, "", 1.0 );
    m_cutfileInfoLayout.SetControl( "Spreadsheet", m_spreadsheetObjects );

    // Edit
    m_cutfileInfoLayout.AddRegion( "EditSpreadsheetButton", "EditSpreadsheetButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_cutfileInfoLayout.SetControl( "EditSpreadsheetButton", m_editSpreadsheetButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Add
    m_cutfileInfoLayout.AddRegion( "AddToSpreadsheetButton", "AddToSpreadsheetButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_cutfileInfoLayout.SetControl( "AddToSpreadsheetButton", m_addToSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Add Face
	m_cutfileInfoLayout.AddRegion( "AddFaceSpreadsheetButton", "AddFaceSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoLayout.SetControl( "AddFaceSpreadsheetButton", m_addFaceSpreadsheetButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Remove
    m_cutfileInfoLayout.AddRegion("RemoveFromSpreadsheetButton", "RemoveFromSpreadsheetButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_cutfileInfoLayout.SetControl( "RemoveFromSpreadsheetButton", m_removeFromSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Remove Face
	m_cutfileInfoLayout.AddRegion( "RemoveFaceSpreadsheetButton", "RemoveFaceSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoLayout.SetControl( "RemoveFaceSpreadsheetButton", m_removeFaceSpreadsheetButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Remove All
    m_cutfileInfoLayout.AddRegion("RemoveAllFromSpreadsheetButton", "RemoveAllFromSpreadsheetButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_cutfileInfoLayout.SetControl( "RemoveAllFromSpreadsheetButton", m_removeAllFromSpreadsheetButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Refresh
    m_cutfileInfoLayout.AddRegion( "RefreshSpreadsheetButton", "RefreshSpreadsheetButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_cutfileInfoLayout.SetControl( "RefreshSpreadsheetButton", m_refreshSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// SelectModel
	m_cutfileInfoLayout.AddRegion( "SelectModelButton", "SelectModelButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "CutfileInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "CutfileInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_cutfileInfoLayout.SetControl( "SelectModelButton", m_selectModelButton );

    iAttachBottomY += rexMBAnimExportCommon::c_iRowHeight;

    // Scene Size
    m_cutfileInfoLayout.AddRegion( "SceneSizeLabel", "SceneSizeLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "CutfileInfoLayoutRegion", 1.0,
        iAttachBottomY-(rexMBAnimExportCommon::c_iMargin*3), kFBAttachBottom, "CutfileInfoLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iWideButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_cutfileInfoLayout.SetControl( "SceneSizeLabel", m_sceneSizeLabel );

    iAttachBottomY += rexMBAnimExportCommon::c_iRowHeight;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UICreateShotInfoLayout()
{
	// SetupLayoutRegion
	m_shotInfoLayout.AddRegion( "ShotInfoLayoutRegion", "ShotInfoLayoutRegion",
		rexMBAnimExportCommon::c_iMargin * 2, kFBAttachLeft, "NULL", 1.0f,
		rexMBAnimExportCommon::c_iMargin * 3, kFBAttachTop, "NULL", 1.0f,
		-rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, NULL, 1.0f,
		-rexMBAnimExportCommon::c_iMargin * 6, kFBAttachBottom, NULL, 1.0f );
	m_shotInfoLayout.SetControl( "ShotInfoLayoutRegion", m_shotInfoLayoutRegion );

	int iAttachTopY = rexMBAnimExportCommon::c_iMargin * 2;

	// Spreadsheet
	m_shotInfoLayout.AddRegion( "Spreadsheet2", "Spreadsheet2",
		rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin - (rexMBAnimExportCommon::c_iButtonWidth*2) - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		/*iAttachBottomY-(rexMBAnimExportCommon::c_iMargin)*/180, kFBAttachTop, "", 1.0 );
	m_shotInfoLayout.SetControl( "Spreadsheet2", m_spreadsheetParts );

	// Edit
	m_shotInfoLayout.AddRegion( "EditSpreadsheetButton", "EditSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_shotInfoLayout.SetControl( "EditSpreadsheetButton", m_editPartSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Add
	m_shotInfoLayout.AddRegion( "AddToSpreadsheetButton", "AddToSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_shotInfoLayout.SetControl( "AddToSpreadsheetButton", m_addPartToSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Remove
	m_shotInfoLayout.AddRegion("RemoveFromSpreadsheetButton", "RemoveFromSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_shotInfoLayout.SetControl( "RemoveFromSpreadsheetButton", m_removePartFromSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Remove All
	m_shotInfoLayout.AddRegion("RemoveAllFromSpreadsheetButton", "RemoveAllFromSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_shotInfoLayout.SetControl( "RemoveAllFromSpreadsheetButton", m_removeAllPartFromSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Move Up
	m_shotInfoLayout.AddRegion("MoveUpSpreadsheetButton", "MoveUpSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_shotInfoLayout.SetControl( "MoveUpSpreadsheetButton", m_moveUpPartFromSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Move Down
	m_shotInfoLayout.AddRegion("MoveDownSpreadsheetButton", "MoveDownSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_shotInfoLayout.SetControl( "MoveDownSpreadsheetButton", m_moveDownPartFromSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Duplicate Shot
	m_shotInfoLayout.AddRegion("DuplicateSpreadsheetButton", "DuplicateSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_shotInfoLayout.SetControl( "DuplicateSpreadsheetButton", m_duplicatePartSpreadsheetButton );

	iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

	// Sync Ranges
	m_shotInfoLayout.AddRegion("SyncRangesSpreadsheetButton", "SyncRangesSpreadsheetButton",
		-rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth*2, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		iAttachTopY, kFBAttachTop, "ShotInfoLayoutRegion", 1.0,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ShotInfoLayoutRegion", 1.0,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
	m_shotInfoLayout.SetControl( "SyncRangesSpreadsheetButton", m_syncRangesSpreadsheetButton );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UICreateExportInfoLayout()
{
    // SetupLayoutRegion
    m_exportInfoLayout.AddRegion( "ExportInfoLayoutRegion", "ExportInfoLayoutRegion",
        rexMBAnimExportCommon::c_iMargin * 2, kFBAttachLeft, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iMargin * 3, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin * 6, kFBAttachBottom, NULL, 1.0f );
    m_exportInfoLayout.SetControl( "ExportInfoLayoutRegion", m_exportInfoLayoutRegion );

    int iAttachTopY = rexMBAnimExportCommon::c_iMargin * 2;

    // Scene Name
    m_exportInfoLayout.AddRegion( "SceneNameLabel", "SceneNameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "ExportInfoLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "ExportInfoLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_exportInfoLayout.SetControl( "SceneNameLabel", m_sceneNameLabel );

    m_exportInfoLayout.AddRegion( "SceneNameTextBox", "SceneNameTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SceneNameLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "ExportInfoLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ExportInfoLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_exportInfoLayout.SetControl( "SceneNameTextBox", m_sceneNameTextBox );

	m_exportInfoLayout.AddRegion( "OpenPathButton", "OpenPathButton",
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SceneNameTextBox", 1.0f,
		iAttachTopY, kFBAttachTop, "ExportInfoLayoutRegion", 1.0f,
		-rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ExportInfoLayoutRegion", 1.0f,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
	m_exportInfoLayout.SetControl( "OpenPathButton", m_openPathButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Export Path
    m_exportInfoLayout.AddRegion( "ExportPathLabel", "ExportPathLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "ExportInfoLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "ExportInfoLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_exportInfoLayout.SetControl( "ExportPathLabel", m_exportPathLabel );

    m_exportInfoLayout.AddRegion( "ExportPathTextBox", "ExportPathTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ExportPathLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "ExportInfoLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ExportInfoLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_exportInfoLayout.SetControl( "ExportPathTextBox", m_exportPathTextBox );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Face Export Path
    m_exportInfoLayout.AddRegion( "FaceExportPathLabel", "FaceExportPathLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "ExportInfoLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "ExportInfoLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLongLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_exportInfoLayout.SetControl( "FaceExportPathLabel", m_faceExportPathLabel );

    m_exportInfoLayout.AddRegion( "FaceExportPathTextBox", "FaceExportPathTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "FaceExportPathLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "ExportInfoLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ExportInfoLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_exportInfoLayout.SetControl( "FaceExportPathTextBox", m_faceExportPathTextBox );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UIConfigure()
{
	// Version
	char cBuffer[32];
	sprintf(cBuffer,"Version: %i.%i.%i", s_cutscene_exporter_version.Major, s_cutscene_exporter_version.Minor, s_cutscene_exporter_version.Revision);
	m_versionLabel.Caption = cBuffer;
	m_versionLabel.Justify = kFBTextJustifyRight;

	m_usePerforceIntegration.Width = 100;
	m_usePerforceIntegration.Caption = "PERFORCE";
	m_usePerforceIntegration.Style = kFBCheckbox;
	m_usePerforceIntegration.State = 1;
	m_usePerforceIntegration.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::UsePerforceIntegrationButton_Click );

	m_useAutoConcat.Width = 100;
	m_useAutoConcat.Caption = "CONCAT";
	m_useAutoConcat.Style = kFBCheckbox;
	m_useAutoConcat.State = 1;

	m_helpButton.Caption = "Help";
	m_helpButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::HelpButton_Click );

	m_editSceneEventsButton.Caption = "Edit Scene Events";
	m_editSceneEventsButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::EditSceneEventsButton_Click );

    char cFilter[64];
    sprintf( cFilter, "*.%s; *.%s", PI_CUTSCENE_XMLFILE_EXT, PI_CUTSCENE_BINFILE_EXT );

    // Open File Popup
    m_openFilePopup.Caption = "Import Cut File";
    m_openFilePopup.Filter = cFilter;
    m_openFilePopup.Style = kFBFilePopupOpen;

    sprintf( cFilter, "*.%s; *.%s", PI_CUTSCENE_XMLFILE_EXT, PI_CUTSCENE_BINFILE_EXT );

    // Save File Popup
    m_saveFilePopup.Caption = "Save Cut File";
    m_saveFilePopup.Filter = cFilter;
    m_saveFilePopup.Style = kFBFilePopupSave;

    // Import Cut File
    m_importCutFileButton.Caption = "Import Cut File";
    m_importCutFileButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::ImportCutFileButton_Click );

	// Refresh Cut File
	m_importCutListButton.Caption = "Import Cut Part";
	m_importCutListButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::ImportCutListButton_Click );

	// Refresh Handle File
	m_refreshHandleFileButton.Caption = "Refresh Handle File";
	m_refreshHandleFileButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::RefreshHandleFileButton_Click );

	m_verifyCutsceneButton.Caption = "Verify Cutscene";
	m_verifyCutsceneButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::VerifyCutsceneButton_Click );

    UIConfigureCutfileInfoLayout();
    
	UIConfigureShotInfoLayout();
    
	UIConfigureExportInfoLayout();

    // Export Model
    m_exportSelectedAnimationsButton.Caption = "Export Selected Anim(s)";
    m_exportSelectedAnimationsButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::ExportSelectedAnimationsButton_Click );

    // Export Scene Animations
    m_exportSpreadsheetAnimationsButton.Caption = "Export Spreadsheet Anim(s)";
    m_exportSpreadsheetAnimationsButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::ExportSpreadsheetAnimationsButton_Click );
    
    // Build Animation Dictionaries
    m_buildAnimationDictionariesButton.Caption = "Build Anim Dictionaries";
    m_buildAnimationDictionariesButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::BuildAnimationDictionariesButton_Click );    

    // Export Camera
    m_exportCameraAnimationButton.Caption = "Export Camera Anim";
    m_exportCameraAnimationButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::ExportCameraAnimationButton_Click );

    // Export Cut File
    m_exportCutFileButton.Caption = "Export Cut File(s)";
    m_exportCutFileButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::ExportCutFileButton_Click );

    // Patch Animation Dictionaries
    m_patchAnimationDictionariesButton.Caption = "Preview Anim Dictionaries";
    m_patchAnimationDictionariesButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::PatchAnimationDictionariesButton_Click );
    m_patchAnimationDictionariesButton.Enabled = true;

	m_exportFaceAnimationButton.Caption = "Export Face Anim(s)";
	m_exportFaceAnimationButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::ExportFaceAnimationButton_Click );
	m_exportFaceAnimationButton.Enabled = true;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UIConfigureCutfileInfoLayout()
{
    // CutfileInfoLayoutRegion
    m_cutfileInfoLayout.SetRegionTitle( "CutfileInfoLayoutRegion", "Cut Scene Information" );
    m_cutfileInfoLayout.SetBorder( "CutfileInfoLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

    // Spreadsheet
    m_spreadsheetObjects.OnRowClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::Spreadsheet_RowClick );    
	m_spreadsheetObjects.OnDragAndDrop.Add(this, (FBCallback)&RexRageCutsceneExportTool::Spreadsheet_DragDrop );

	m_spreadsheetParts.OnRowClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::PartSpreadsheet_RowClick ); 

    // Edit
    m_editSpreadsheetButton.Caption = "Edit";
    m_editSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::EditSpreadsheetButton_Click );
    m_editSpreadsheetButton.Enabled = false;

    // Add
    m_addToSpreadsheetButton.Caption = "Add";
    m_addToSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::AddToSpreadsheetButton_Click);

    // Remove
    m_removeFromSpreadsheetButton.Caption = "Remove";
    m_removeFromSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::RemoveFromSpreadsheetButton_Click );
    m_removeFromSpreadsheetButton.Enabled = false;

    // Remove All
    m_removeAllFromSpreadsheetButton.Caption = "Remove All";
    m_removeAllFromSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::RemoveAllFromSpreadsheetButton_Click );

    // Refresh
    m_refreshSpreadsheetButton.Caption = "Refresh";
    m_refreshSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::RefreshSpreadsheetButton_Click );

	// Select
	m_selectModelButton.Caption = "Select Model";
	m_selectModelButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::SelectModelButton_Click );

	// Add Face
	m_addFaceSpreadsheetButton.Caption = "Add Face";
	m_addFaceSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::AddFaceSpreadsheetButton_Click );
	m_addFaceSpreadsheetButton.Enabled = false;

	// Add Face
	m_removeFaceSpreadsheetButton.Caption = "Remove Face";
	m_removeFaceSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::RemoveFaceSpreadsheetButton_Click );
	m_removeFaceSpreadsheetButton.Enabled = false;

    // Scene Size
    m_sceneSizeLabel.Caption = "Scene Size: 0 K";
    m_sceneSizeLabel.Justify = kFBTextJustifyLeft;

    // Add Components Popup
    m_addComponentsPopup.Modal = true;
    m_addComponentsPopup.SetTemplateObjectList( m_pComponentObjectTemplateList );

    // Add Non Component Popup
    m_addNonComponentPopup.Modal = true;
    m_addNonComponentPopup.SetTemplateObjectList( m_pNonComponentObjectTemplateList );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UIConfigureShotInfoLayout()
{
	m_shotInfoLayout.SetRegionTitle( "ShotInfoLayoutRegion", "Part Information" );
	m_shotInfoLayout.SetBorder( "ShotInfoLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

	// Edit
	m_editPartSpreadsheetButton.Caption = "Edit";
	m_editPartSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::EditPartSpreadsheetButton_Click );
	m_editPartSpreadsheetButton.Enabled = false;

	// Add
	m_addPartToSpreadsheetButton.Caption = "Add";
	m_addPartToSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::AddToPartSpreadsheetButton_Click);

	// Remove
	m_removePartFromSpreadsheetButton.Caption = "Remove";
	m_removePartFromSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::RemoveFromPartSpreadsheetButton_Click );
	m_removePartFromSpreadsheetButton.Enabled = false;

	// Remove All
	m_removeAllPartFromSpreadsheetButton.Caption = "Remove All";
	m_removeAllPartFromSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::RemoveAllFromPartSpreadsheetButton_Click );

	// Move Up
	m_moveUpPartFromSpreadsheetButton.Caption = "Move Up";
	m_moveUpPartFromSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::MoveUpPartSpreadsheetButton_Click );

	// Move Down
	m_moveDownPartFromSpreadsheetButton.Caption = "Move Down";
	m_moveDownPartFromSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::MoveDownPartSpreadsheetButton_Click );

	m_duplicatePartSpreadsheetButton.Caption = "Duplicate";
	m_duplicatePartSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::DuplicatePartSpreadsheetButton_Click );

	m_syncRangesSpreadsheetButton.Caption = "Sync Ranges";
	m_syncRangesSpreadsheetButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::SyncRangesSpreadsheetButton_Click );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UIConfigureExportInfoLayout()
{
    // EventEditLayoutRegion
    m_exportInfoLayout.SetRegionTitle( "ExportInfoLayoutRegion", "Export Information" );
    m_exportInfoLayout.SetBorder( "ExportInfoLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

    // Scene Name
    m_sceneNameLabel.Caption = "Scene Name:";
    m_sceneNameLabel.Justify = kFBTextJustifyRight;

	if(rexMBAnimExportCommon::IsDlc(atString("")))
		m_sceneNameTextBox.ReadOnly = true;

    m_sceneNameTextBox.OnChange.Add( this, (FBCallback)&RexRageCutsceneExportTool::SceneNameTextBox_TextChanged );

	m_openPathButton.Caption = "...";
	m_openPathButton.OnClick.Add( this, (FBCallback)&RexRageCutsceneExportTool::OpenPathButton_Click );

    // Export Path
    m_exportPathLabel.Caption = "Export Path:";
    m_exportPathLabel.Justify = kFBTextJustifyRight;

    // Face Export Path
    m_faceExportPathLabel.Caption = "Face Export Path:";
    m_faceExportPathLabel.Justify = kFBTextJustifyRight;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::FbxStore( HFBFbxObject /*pFbxObject*/, kFbxObjectStore /*pStoreWhat*/ ) { return true; }
bool RexRageCutsceneExportTool::FbxRetrieve( HFBFbxObject /*pFbxObject*/, kFbxObjectStore /*pStoreWhat*/ ) { return true; }
//-------------------------------------------------
void RexRageCutsceneExportTool::SetSceneName( const char *pSceneName, bool doValidate )
{
    safecpy( m_cSceneName, pSceneName, sizeof(m_cSceneName) );

    m_sceneNameTextBox.Text = (char*)ValidateSceneNameLength((char *)pSceneName).c_str();

    UpdatePaths(doValidate);

    EnableButtons();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SetDefaultSpecFile( cutfObject* pObject )
{
	cutfVehicleModelObject* pVehicleModelObject = dynamic_cast<cutfVehicleModelObject*>(pObject);
	if(pVehicleModelObject)
	{
		if(pVehicleModelObject->GetAnimExportCtrlSpecFile().GetCStr() == NULL || (stricmp(pVehicleModelObject->GetAnimExportCtrlSpecFile().GetCStr(),"(no spec file)") == 0 ||
			stricmp(pVehicleModelObject->GetAnimExportCtrlSpecFile().GetCStr(),"") == 0))
		{
			pVehicleModelObject->SetAnimExportCtrlSpecFile("Cutscene_Vehicle_spec.xml");
		}
	}

	cutfPropModelObject* pPropModelObject = dynamic_cast<cutfPropModelObject*>(pObject);
	if(pPropModelObject)
	{
		if(pPropModelObject->GetAnimExportCtrlSpecFile().GetCStr() == NULL || (stricmp(pPropModelObject->GetAnimExportCtrlSpecFile().GetCStr(),"(no spec file)") == 0 ||
			stricmp(pPropModelObject->GetAnimExportCtrlSpecFile().GetCStr(),"") == 0))
		{
			pPropModelObject->SetAnimExportCtrlSpecFile("Cutscene_Prop_spec.xml");
		}
	}

	cutfPedModelObject* pPedModelObject = dynamic_cast<cutfPedModelObject*>(pObject);
	if(pPedModelObject)
	{
		if(pPedModelObject->GetAnimExportCtrlSpecFile().GetCStr() == NULL || (stricmp(pPedModelObject->GetAnimExportCtrlSpecFile().GetCStr(),"(no spec file)") == 0 ||
			stricmp(pPedModelObject->GetAnimExportCtrlSpecFile().GetCStr(),"") == 0))
		{
			pPedModelObject->SetAnimExportCtrlSpecFile("Cutscene_Ped_spec.xml");
		}
	}

	cutfWeaponModelObject* pWeaponModelObject = dynamic_cast<cutfWeaponModelObject*>(pObject);
	if(pWeaponModelObject)
	{
		if(pWeaponModelObject->GetAnimExportCtrlSpecFile().GetCStr() == NULL || (stricmp(pWeaponModelObject->GetAnimExportCtrlSpecFile().GetCStr(),"(no spec file)") == 0 ||
			stricmp(pWeaponModelObject->GetAnimExportCtrlSpecFile().GetCStr(),"") == 0))
		{
			pWeaponModelObject->SetAnimExportCtrlSpecFile("Cutscene_Prop_spec.xml");
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PopulateLightProperties()
{
	m_vLightPropertyEntries.Grow() = "LP_NO_PROPERTY"; // cutfLightObject::eLightProperty.LP_NO_PROPERTY
	m_vLightPropertyEntries.Grow() = "LP_CASTS_SHADOWS"; //cutfLightObject::eLightProperty.LP_CASTS_SHADOWS
	m_vLightPropertyEntries.Grow() = "LP_ENVIRONMENT"; // cutfLightObject::eLightProperty.LP_ENVIRONMENT
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PopulateOverlayTypes()
{
	m_vOverlayTypeEntries.Grow() = "SCALEFORM"; 
	m_vOverlayTypeEntries.Grow() = "BINK";
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PopulateWeaponTypes()
{
	m_vWeaponTypeEntries.Grow() = "CUTSCENE_NO_GENERIC_WEAPON_TYPE"; // ECutsceneGenericWeaponType.CUTSCENE_NO_GENERIC_WEAPON_TYPE
	m_vWeaponTypeEntries.Grow() = "CUTSCENE_ONE_HANDED_GUN_GENERIC_WEAPON_TYPE"; //ECutsceneGenericWeaponType.CUTSCENE_ONE_HANDED_GUN_GENERIC_WEAPON_TYPE
	m_vWeaponTypeEntries.Grow() = "CUTSCENE_TWO_HANDED_GUN_GENERIC_WEAPON_TYPE"; // ECutsceneGenericWeaponType.CUTSCENE_TWO_HANDED_GUN_GENERIC_WEAPON_TYPE
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PopulateFromCutsceneHandleFile()
{
	#define TYPE 0
	#define PED_MODELS 1
	#define PED_HANDLES 2
	#define PROP_MODELS 3
	#define PROP_HANDLES 4
	#define VEHICLE_MODELS 5
	#define VEHICLE_HANDLES 6

	bool bCommon=false;
	bool bMission=false;
	bool bCutscene=false;
	std::string strType="";
	std::string strName="";
	m_vHandleEntries.Reset();

	char cValue[RAGE_MAX_PATH];
	if ( rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cValue, sizeof(cValue)) == false )
		return;

	char cFilePath[RAGE_MAX_PATH * 4];
	cFilePath[0] = 0;
	safecat( cFilePath, cValue, sizeof(cFilePath) );
	safecat( cFilePath, "\\etc\\config\\cutscene\\Cutscene_Model_Handles.csv", sizeof(cFilePath) );

	std::ifstream data(cFilePath);

	if(data.is_open())
	{
		int iLineCount=0;
		std::string line;
		while(std::getline(data,line))
		{
			std::stringstream  lineStream(line);
			std::string        cell;

			std::string strModel="";
			std::string strHandle="";
			std::string strCategory="";

			if(iLineCount == 0) // Skip headers
			{
				iLineCount++;
				continue;
			}

			int iCellCount=0;
			while(std::getline(lineStream,cell,','))
			{
				if(cell != "")
				{
					if(iCellCount == TYPE)
					{
						if(cell == "Common:")
						{
							bCommon = true;
							strType = cell;
						}
						else if(cell == "Generic:")
						{
							bCommon = false;
							bMission = true;
							strType = cell;
						}
						else if(cell == "Mission:")
						{
							bCommon = false;
							bMission = true;
							strType = cell;
						}
						else if(cell == "Cutscene specific:")
						{
							bCommon = false;
							bMission = false;
							bCutscene = true;
							strType = cell;
						}
						else
						{
							// this is the mission or cutscene name
							strName = cell;
						}
					}

					if(iCellCount == PED_MODELS || iCellCount == PROP_MODELS || iCellCount == VEHICLE_MODELS)
					{
						strModel = cell;

						if(iCellCount == PED_MODELS) strCategory = "PED";
						if(iCellCount == PROP_MODELS) strCategory = "PROP";
						if(iCellCount == VEHICLE_MODELS) strCategory = "VEHICLE";
					}

					if(iCellCount == PED_HANDLES || iCellCount == PROP_HANDLES || iCellCount == VEHICLE_HANDLES)
					{
						strHandle = cell;
					}
				}

				if(strModel != "" && strHandle != "" )
				{
					HandleEntry entry;
					if(bCutscene || bMission)
						entry.strName = strName;
					entry.strType = strType;
					entry.strModel = strModel;
					entry.strHandle = strHandle;
					entry.strCategory = strCategory;

					m_vHandleEntries.Grow() = entry;

					strModel = "";
					strHandle = "";
				}

				iCellCount++;
			}

			iLineCount++;
		}

		data.close();
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SetModelHandle( cutfModelObject* pModel )
{
	if(pModel == NULL) return;

	atString strName = pModel->GetDisplayName();
	atString strSceneName(GetSceneName());
	atString strHandle("");

	for(int i=0; i < m_vHandleEntries.GetCount(); ++i)
	{
		HandleEntry entry = m_vHandleEntries[i];

		if(strcmpi(entry.strModel.c_str(),strName.c_str()) == 0)
		{
			if(strcmpi(entry.strType.c_str(),"Mission") == 0 || strcmpi(entry.strType.c_str(),"Cutscene specific") == 0)
			{
				if(strcmpi(strSceneName.c_str(),entry.strName.c_str()) == 0)
				{
					strHandle = entry.strHandle.c_str();
					break;
				}
			}
			else
			{
				strHandle = entry.strHandle.c_str();
				break;
			}
		}
	}

	if(strcmpi(strHandle.c_str(),"") != 0)
	{
		pModel->SetHandle(strHandle.c_str());
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::AddRowToPartSpreadsheet( RexRageCutscenePart* pPart )
{
	m_spreadsheetParts.RowAdd( (char*)pPart->GetName(), m_iPartSpreadsheetRowId );

	m_spreadsheetParts.SetCell( m_iPartSpreadsheetRowId, SHOT_EXPORT_SPREADSHEET_COLUMN, 1 );

	char czRange[RAGE_MAX_PATH];
	sprintf_s(czRange, RAGE_MAX_PATH, "%d - %d", pPart->GetCutsceneFile()->GetRangeStart(), pPart->GetCutsceneFile()->GetRangeEnd());
	m_spreadsheetParts.SetCell( m_iPartSpreadsheetRowId, SHOT_RANGE_SPREADSHEET_COLUMN, czRange );

	m_spreadsheetRowRefToPartMap.Insert( m_iPartSpreadsheetRowId, pPart );

	++m_iPartSpreadsheetRowId;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::AddRowToSpreadsheet( cutfObject *pObject )
{
    m_spreadsheetObjects.RowAdd( const_cast<char *>( pObject->GetDisplayName().c_str() ), m_iObjectsSpreadsheetRowId );
    
    m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, ID_SPREADSHEET_COLUMN, pObject->GetObjectId() );
    m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, SIZE_SPREADSHEET_COLUMN, 0 );
    m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, TYPE_SPREADSHEET_COLUMN, const_cast<char *>( pObject->GetTypeName() ) );

	cutfPedModelObject* pPedObject = dynamic_cast<cutfPedModelObject*>(pObject);
	cutfModelObject* pModelObject = dynamic_cast<cutfModelObject*>(pObject);
	if(pPedObject)
	{
		m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, FACE_SPREADSHEET_COLUMN, (char*)pPedObject->GetFaceAnimationNodeName().GetCStr() );
		m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, HANDLE_SPREADSHEET_COLUMN, (char*)pPedObject->GetHandle().GetCStr() );
	}
	else if(pModelObject)
	{
		m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, HANDLE_SPREADSHEET_COLUMN, (char*)pModelObject->GetHandle().GetCStr() );
	}
	else
	{
		m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, FACE_SPREADSHEET_COLUMN, "" );
		m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, HANDLE_SPREADSHEET_COLUMN, "" );
	}
	
    m_spreadsheetObjects.SetCell( m_iObjectsSpreadsheetRowId, EXPORT_SPREADSHEET_COLUMN, 1 );

	SetDefaultSpecFile(pObject);
	m_spreadsheetObjectsRowRefToObjectMap.Insert( m_iObjectsSpreadsheetRowId, pObject );

    ++m_iObjectsSpreadsheetRowId;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RemoveRowFromSpreadsheet( s32 iRef )
{
    if ( iRef > -1 )
    {
        m_spreadsheetObjectsRowRefToObjectMap.Delete( iRef );

        FBSpreadRow row = m_spreadsheetObjects.GetRow( iRef );
        row.Remove();
    }
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RemoveRowFromPartSpreadsheet( s32 iRef )
{
	if ( iRef > -1 )
	{
		m_spreadsheetRowRefToPartMap.Delete( iRef );

		FBSpreadRow row = m_spreadsheetParts.GetRow( iRef );
		row.Remove();
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ClearPartSpreadsheet()
{
	m_spreadsheetRowRefToPartMap.Kill();    

	m_spreadsheetParts.Clear();

	m_spreadsheetParts.GetColumn( NAME_SPREADSHEET_COLUMN ).Width = 125;

	m_spreadsheetParts.ColumnAdd( "Export" );
	m_spreadsheetParts.GetColumn( SHOT_EXPORT_SPREADSHEET_COLUMN ).ReadOnly = false;
	m_spreadsheetParts.GetColumn( SHOT_EXPORT_SPREADSHEET_COLUMN ).Style = kFBCellStyle2StatesButton;
	m_spreadsheetParts.GetColumn( SHOT_EXPORT_SPREADSHEET_COLUMN ).Width = 50;

	m_spreadsheetParts.ColumnAdd( "Range" );
	m_spreadsheetParts.GetColumn( SHOT_RANGE_SPREADSHEET_COLUMN ).ReadOnly = true;
	m_spreadsheetParts.GetColumn( SHOT_RANGE_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
	m_spreadsheetParts.GetColumn( SHOT_RANGE_SPREADSHEET_COLUMN ).Width = 100;

	m_iPartSpreadsheetSelRowId = -1;
	m_iPartSpreadsheetRowId = 0;

	EnableButtons();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ClearSpreadsheet()
{
    m_spreadsheetObjectsRowRefToObjectMap.Kill();    

    m_spreadsheetObjects.Clear();

    m_spreadsheetObjects.GetColumn( NAME_SPREADSHEET_COLUMN ).Width = 125;

    m_spreadsheetObjects.ColumnAdd( "ID" );
    m_spreadsheetObjects.GetColumn( ID_SPREADSHEET_COLUMN ).Justify = kFBTextJustifyRight;
    m_spreadsheetObjects.GetColumn( ID_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_spreadsheetObjects.GetColumn( ID_SPREADSHEET_COLUMN ).Style = kFBCellStyleInteger;
    m_spreadsheetObjects.GetColumn( ID_SPREADSHEET_COLUMN ).Width = 40;

    m_spreadsheetObjects.ColumnAdd( "Size (K)" );
    m_spreadsheetObjects.GetColumn( SIZE_SPREADSHEET_COLUMN ).Justify = kFBTextJustifyRight;
    m_spreadsheetObjects.GetColumn( SIZE_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_spreadsheetObjects.GetColumn( SIZE_SPREADSHEET_COLUMN ).Style = kFBCellStyleInteger;
    m_spreadsheetObjects.GetColumn( SIZE_SPREADSHEET_COLUMN ).Width = 80;

    m_spreadsheetObjects.ColumnAdd( "Type" );
    m_spreadsheetObjects.GetColumn( TYPE_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_spreadsheetObjects.GetColumn( TYPE_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
    m_spreadsheetObjects.GetColumn( TYPE_SPREADSHEET_COLUMN ).Width = 125;

    m_spreadsheetObjects.ColumnAdd( "Export" );
    m_spreadsheetObjects.GetColumn( EXPORT_SPREADSHEET_COLUMN ).ReadOnly = false;
    m_spreadsheetObjects.GetColumn( EXPORT_SPREADSHEET_COLUMN ).Style = kFBCellStyle2StatesButton;
    m_spreadsheetObjects.GetColumn( EXPORT_SPREADSHEET_COLUMN ).Width = 50;

	m_spreadsheetObjects.ColumnAdd( "Face" );
	m_spreadsheetObjects.GetColumn( FACE_SPREADSHEET_COLUMN ).ReadOnly = true;
	m_spreadsheetObjects.GetColumn( FACE_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
	m_spreadsheetObjects.GetColumn( FACE_SPREADSHEET_COLUMN ).Width = 125;

	m_spreadsheetObjects.ColumnAdd( "Handle" );
	m_spreadsheetObjects.GetColumn( HANDLE_SPREADSHEET_COLUMN ).ReadOnly = true;
	m_spreadsheetObjects.GetColumn( HANDLE_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
	m_spreadsheetObjects.GetColumn( HANDLE_SPREADSHEET_COLUMN ).Width = 125;

    m_iObjectsSpreadsheetSelRowId = -1;
    m_iObjectsSpreadsheetRowId = 0;

    EnableButtons();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RefreshShreadsheetSizes()
{
    int totalSize = 0;

    SetupCutsceneExport();

	const char* pExportPath = GetExportPath();

    // subtotal every item in the spreadsheet
    atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( !m_cutsceneExport.IsObjectAnimatable( entry.GetData() ) )
        {
            m_spreadsheetObjects.SetCell( entry.GetKey(), SIZE_SPREADSHEET_COLUMN, 0 );
            continue;
        }

        int entrySize = 0;

        atArray<atString> animFilenameList;
        m_cutsceneExport.GetExportFilenames( pExportPath, entry.GetData(), animFilenameList, true );

        for ( int i = 0; i < animFilenameList.GetCount(); ++i )
        {
            fiStream* pStream = fiStream::Open( animFilenameList[i] );
            if ( pStream )
            {
                int bytes = pStream->Size();
                pStream->Close();

                entrySize += (int)ceilf( bytes / 1024.0f );
            }
        }       

        m_spreadsheetObjects.SetCell( entry.GetKey(), SIZE_SPREADSHEET_COLUMN, entrySize );

        totalSize += entrySize;
    }

    char czSize[64];
    sprintf( czSize, "Total Size: %d K", totalSize );
    m_sceneSizeLabel.Caption = czSize;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::CanEditObject( cutfObject* /*pObject*/ ) const
{
    // FIXME: Artist Mode vs. Full-Access Mode
    return true;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::CanEditPart( RexRageCutscenePart* /*pPart*/ ) const
{
	return true;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::CanRemoveObject( cutfObject *pObject ) const
{
    return (pObject->GetType() != CUTSCENE_ASSET_MANAGER_OBJECT_TYPE)
        && (pObject->GetType() != CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE);
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::OpenCutFile( const char *pFullPath )
{
    return m_cutsceneExport.OpenCutFile( pFullPath );
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::OpenPartFile( const char *pFullPath )
{
	return m_cutsceneExport.OpenPartFile( pFullPath );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::NewCutFile()
{
    ClearSpreadsheet();
	ClearPartSpreadsheet();

	UpdateUIFromCutfile();

    SetSceneName( "" );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UpdateShotExportStatus( ) 
{
	atMap<s32, RexRageCutscenePart *>::Iterator entry = m_spreadsheetRowRefToPartMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{        
		RexRageCutscenePart* pPart = entry.GetData();

		if(pPart)
		{
			int iExport = 0;
			m_spreadsheetParts.GetCell( entry.GetKey(), SHOT_EXPORT_SPREADSHEET_COLUMN, iExport );

			pPart->SetCanExport(iExport == 1 ? true : false);
		}
	}
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::ExportModelAnimations( atArray<SModelObject> &modelObjects, atArray<SModelObject> &otherModelObjects, bool bInteractive )
{
	if(bInteractive)
	{
		//If not interactive, this is being called from python hook, and these are overridden there
		rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
		rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);
		rexMBAnimExportCommon::SetUseWarnIfCheckedOut(false);
	}

    SetupCutsceneExport(bInteractive);

#if PROFILE_CUTSCENE_EXPORT
    char cExportPath[RAGE_MAX_PATH];
    m_cutsceneExport.GetExportPath( cExportPath, sizeof(cExportPath) );

    char cTimerFilename[RAGE_MAX_PATH];
    sprintf( cTimerFilename, "%s\\ExportModelAnimations.timing", cExportPath );

    rexMBTimerLog::Open( cTimerFilename );
#endif //PROFILE_CUTSCENE_EXPORT

	UpdateShotExportStatus();

    bool bResult = m_cutsceneExport.Export( modelObjects, otherModelObjects, false, true, bInteractive );

#if PROFILE_CUTSCENE_EXPORT
    rexMBTimerLog::Close();
#endif //PROFILE_CUTSCENE_EXPORT

    return bResult;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::ExportCamera(bool bInteractive)
{
	UpdateCutfileFromUI();

	SModelObject cameraModelObject;
	FindCameraAnimationModel( cameraModelObject );
	bool bResult = false;
	if ( cameraModelObject.pModel != NULL )
	{
		// Save the cut file
		m_cutsceneExport.PrepareCutfileForAnimationExport();

		bResult = ExportCameraAnimation( cameraModelObject, bInteractive );

		RefreshShreadsheetSizes();
	}
	return bResult;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::ExportCameraAnimation( SModelObject &cameraModelObject, bool bInteractive )
{
    SetupCutsceneExport( bInteractive );
        
#if PROFILE_CUTSCENE_EXPORT
    char cExportPath[RAGE_MAX_PATH];
    m_cutsceneExport.GetExportPath( cExportPath, sizeof(cExportPath) );

    char cTimerFilename[RAGE_MAX_PATH];
    sprintf( cTimerFilename, "%s\\ExportCameraAnimation.timing", cExportPath );

    rexMBTimerLog::Open( cTimerFilename );
#endif //PROFILE_CUTSCENE_EXPORT

    atArray<SModelObject> objectModelList;
    objectModelList.PushAndGrow( cameraModelObject );

	atArray<SModelObject> otherObjectModelList;
	FindSpreadsheetAnimationModels( otherObjectModelList );

    bool bResult = m_cutsceneExport.Export( objectModelList, otherObjectModelList, false );

#if PROFILE_CUTSCENE_EXPORT
    rexMBTimerLog::Close();
#endif //PROFILE_CUTSCENE_EXPORT

    return bResult;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::ExportFaceAnimation( atArray<SModelObject> &pedModelObjects )
{
	SetupCutsceneExport();

#if PROFILE_CUTSCENE_EXPORT
	char cExportPath[RAGE_MAX_PATH];
	m_cutsceneExport.GetExportPath( cExportPath, sizeof(cExportPath) );

	char cTimerFilename[RAGE_MAX_PATH];
	sprintf( cTimerFilename, "%s\\ExportFaceAnimation.timing", cExportPath );

	rexMBTimerLog::Open( cTimerFilename );
#endif //PROFILE_CUTSCENE_EXPORT

	atArray<SModelObject> otherObjectModelList;
	FindSpreadsheetAnimationModels( otherObjectModelList );

	bool bResult = m_cutsceneExport.Export( pedModelObjects, otherObjectModelList, false, true, true, true );

#if PROFILE_CUTSCENE_EXPORT
	rexMBTimerLog::Close();
#endif //PROFILE_CUTSCENE_EXPORT

	return bResult;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::FindSpreadsheetAnimationModels( atArray<SModelObject> &modelObjects )
{
    atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {        
        if ( !m_cutsceneExport.IsObjectAnimatable( entry.GetData() ) )
        {
            continue;
        }

        int iExport = 0;
        m_spreadsheetObjects.GetCell( entry.GetKey(), EXPORT_SPREADSHEET_COLUMN, iExport );
        if ( iExport == 0 )
        {
            continue;
        }
       
        SModelObject modelObject;
		modelObject.pModel = rexMBAnimExportCommon::FindModelForObject( entry.GetData() );
        modelObject.pObject = entry.GetData();

        modelObjects.Grow() = modelObject;
    }

    return modelObjects.GetCount() > 0;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::FindSelectedAnimationModels( atArray<SModelObject> &modelObjects )
{
    FBModelList selectedModels;
    FBGetSelectedModels( selectedModels, NULL, true );

    atArray<SModelObject> spreadsheetModelObjects;
    FindSpreadsheetAnimationModels( spreadsheetModelObjects );

    for ( int i = 0; i < selectedModels.GetCount(); ++i )
    {
        char cSelectedModelName[256];
		safecpy( cSelectedModelName, (const char*)selectedModels.GetAt( i )->LongName, sizeof(cSelectedModelName) );

        for ( int j = 0; j < spreadsheetModelObjects.GetCount(); ++j )
        {
            cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( spreadsheetModelObjects[j].pObject );
            if ( strcmp( cSelectedModelName, pNamedObject->GetName().GetCStr() ) == 0 )
            {
                modelObjects.Grow() = spreadsheetModelObjects[j];
                break;
            }
        }
    }

    return modelObjects.GetCount() > 0;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::FindPedAnimationModels( atArray<SModelObject> &pedModelObjects )
{
	atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		if ( entry.GetData()->GetType() != CUTSCENE_MODEL_OBJECT_TYPE )
		{
			continue;
		}

		int iExport = 0;
		m_spreadsheetObjects.GetCell( entry.GetKey(), EXPORT_SPREADSHEET_COLUMN, iExport );
		if ( iExport == 0 )
		{
			continue;
		}

		cutfPedModelObject *pPedObject = dynamic_cast<cutfPedModelObject *>( entry.GetData() );
		if ( pPedObject != NULL )
		{
			rage::SModelObject* pedModelObject = rage_new rage::SModelObject();

			pedModelObject->pModel = RAGEFindModelByName( const_cast<char *>( pPedObject->GetName().GetCStr() ) );
			if ( pedModelObject->pModel != NULL )
			{
				pedModelObject->pObject = pPedObject;
				pedModelObjects.PushAndGrow(*pedModelObject);
			}
		}        
	}

	return pedModelObjects.GetCount() > 0;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::FindCameraAnimationModel( SModelObject &cameraModelObject )
{
    cameraModelObject.pModel = NULL;
    cameraModelObject.pObject = NULL;

    atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( entry.GetData()->GetType() != CUTSCENE_CAMERA_OBJECT_TYPE )
        {
            continue;
        }

        int iExport = 0;
        m_spreadsheetObjects.GetCell( entry.GetKey(), EXPORT_SPREADSHEET_COLUMN, iExport );
        if ( iExport == 0 )
        {
            continue;
        }

        cutfCameraObject *pCameraObject = dynamic_cast<cutfCameraObject *>( entry.GetData() );
        if ( pCameraObject != NULL )
        {
            cameraModelObject.pModel = RAGEFindModelByName( const_cast<char *>( pCameraObject->GetName().GetCStr() ) );
            if ( cameraModelObject.pModel != NULL )
            {
                cameraModelObject.pObject = pCameraObject;
                return true;
            }
        }        
    }

    return false;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::ValidateAnimationFiles()
{
	bool bResult = true;
	atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{        
		if ( !m_cutsceneExport.IsObjectAnimatable( entry.GetData() ) )
		{
			continue;
		}

		int iExport = 0;
		m_spreadsheetObjects.GetCell( entry.GetKey(), EXPORT_SPREADSHEET_COLUMN, iExport );
		if ( iExport == 0 )
		{
			atString strExportPath(GetExportPath());

			char cAnimFilename[RAGE_MAX_PATH];
			sprintf( cAnimFilename, "%s\\%s.anim", strExportPath.c_str(), ((cutfObject*)entry.GetData())->GetDisplayName().c_str() );

			rexMBAnimExportCommon::ReplaceWhitespaceCharacters( cAnimFilename );

			if(!ASSET.Exists( cAnimFilename, ""))
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: Non exported object's animation does not exist '%s'.", cAnimFilename );
				bResult = false;
			}
		}
	}

	return bResult;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SetupCutsceneExport(bool bInteractive, bool doValidate)
{
    m_cutsceneExport.SetInteractiveMode( bInteractive );
	if (doValidate)
		m_cutsceneExport.ValidateCutsceneExportData();
    m_cutsceneExport.SetSceneName( GetSceneName() );
    m_cutsceneExport.GetCutsceneFile().SetFaceDirectory( GetFaceExportPath() );
	m_cutsceneExport.GetCutsceneFile().SetSceneName( GetSceneName() );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UpdatePaths(bool doValidate)
{    
    SetupCutsceneExport(true, doValidate);

    char cExportPath[RAGE_MAX_PATH];
    m_cutsceneExport.GetExportPath( cExportPath, sizeof(cExportPath) );
    SetExportPath( cExportPath );

    char cFaceExportPath[RAGE_MAX_PATH];
    m_cutsceneExport.GetFaceExportPath( cFaceExportPath, sizeof(cFaceExportPath) );
    SetFaceExportPath( cFaceExportPath );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UpdateUIFromCutfile()
{
    ClearSpreadsheet();

    SetSceneName( m_cutsceneExport.GetSceneName() );

    char cExportPath[RAGE_MAX_PATH];
    m_cutsceneExport.GetExportPath( cExportPath, sizeof(cExportPath) );
    SetExportPath( cExportPath );

    char cFaceExportPath[RAGE_MAX_PATH];
    m_cutsceneExport.GetFaceExportPath( cFaceExportPath, sizeof(cFaceExportPath) );
    SetFaceExportPath( cFaceExportPath );

    bool bHaveAssetMgr = false;
    bool bHaveAnimMgr = false;

    atArray<cutfObject *> &objectList = const_cast<atArray<cutfObject *> &>( m_cutsceneExport.GetCutsceneFile().GetObjectList() );
    for ( int i = 0; i < objectList.GetCount(); ++i )
    {
        if ( objectList[i]->GetType() == CUTSCENE_ASSET_MANAGER_OBJECT_TYPE )
        {
            bHaveAssetMgr = true;
        }
        else if ( objectList[i]->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE )
        {
            bHaveAnimMgr = true;
        }

		rexMBAnimExportCommon::SaveObjectData( objectList[i], &m_cutsceneExport.GetCutsceneFile() );

        AddRowToSpreadsheet( objectList[i] );
    }

    // make sure we have a Scene Manager
    if ( !bHaveAssetMgr )
    {
        cutfAssetManagerObject *pAssetManagerObj = rage_new cutfAssetManagerObject();
        m_cutsceneExport.GetCutsceneFile().AddObject( pAssetManagerObj );
        AddRowToSpreadsheet( pAssetManagerObj );
    }

    if ( !bHaveAnimMgr )
    {
        cutfAnimationManagerObject *pAnimManagerObj = rage_new cutfAnimationManagerObject();
        m_cutsceneExport.GetCutsceneFile().AddObject( pAnimManagerObj );
        AddRowToSpreadsheet( pAnimManagerObj );
    }

    RefreshShreadsheetSizes();

    EnableButtons();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UpdateCutfileFromUI()
{
    SetupCutsceneExport();

    // Set the top-level data    
    m_cutsceneExport.GetCutsceneFile().SetFaceDirectory( GetFaceExportPath() );
	m_cutsceneExport.GetCutsceneFile().SetSceneName( GetSceneName() );
   
    atArray<float> &cameraCutList = const_cast<atArray<float> &>( m_cutsceneExport.GetCutsceneFile().GetCameraCutList() );
	rexMBAnimExportCommon::GetCameraCutTimes( cameraCutList );

    // If we have screen fades or overlays, make sure we have the appropriate object(s) created
    SModelObject cameraModelObject;
    if ( FindCameraAnimationModel( cameraModelObject ) )
    {
        // get the section splits
        atArray<float> &sectionSplitList = const_cast<atArray<float> &>( m_cutsceneExport.GetCutsceneFile().GetSectionSplitList() );
		rexMBAnimExportCommon::GetSectionSplitTimes( cameraModelObject.pModel, sectionSplitList );

        // find all of the overlays we have in the cutfile
        atArray<cutfObject *> overlayObjectList;
        m_cutsceneExport.GetCutsceneFile().FindObjectsOfType( CUTSCENE_OVERLAY_OBJECT_TYPE, overlayObjectList );

        // find all of the overlays we have in the scene
        atArray<ConstString> overlayNames;
		rexMBAnimExportCommon::GetOverlayNames( cameraModelObject.pModel, overlayNames );

        // create each overlay, if necessary
        if ( overlayNames.GetCount() > 0 )
        {
            // find or add the needed overlays
            for ( int i = 0; i < overlayNames.GetCount(); ++i )
            {
                bool bFound = false;
                for ( int j = 0; j < overlayObjectList.GetCount(); ++j )
                {
					// fix this check, was broken, GetDisplayName doesn't contain the :Overlay so would never match
					cutfFinalNamedObject* pNamedObj = dynamic_cast<cutfFinalNamedObject*>(overlayObjectList[j]);
					if(pNamedObj)
					{
						if ( stricmp( pNamedObj->GetName(), overlayNames[i].c_str() ) == 0 )
						{
							overlayObjectList.Delete( j );
							bFound = true;
							break;
						}
					}
                }

                if ( !bFound )
                {
                    cutfOverlayObject *pOverlayObject = rage_new cutfOverlayObject( 0, overlayNames[i].c_str() );                    
                    m_cutsceneExport.GetCutsceneFile().AddObject( pOverlayObject );
                    AddRowToSpreadsheet( pOverlayObject );
                }
            }
        }

        // remove unused overlays
        if ( overlayObjectList.GetCount() > 0 )
        {
            for ( int i = 0; i < overlayObjectList.GetCount(); ++i )
            {
                atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
                for ( entry.Start(); !entry.AtEnd(); entry.Next() )
                {
                    if ( entry.GetData() == overlayObjectList[i] )
                    {
                        m_cutsceneExport.GetCutsceneFile().RemoveObject( entry.GetData() );
                        RemoveRowFromSpreadsheet( entry.GetKey() );                    
                        break;
                    }
                }
            }

            RefreshSpreadsheetObjectIds();
        }
    }
	
    // Make sure we have the latest event defs file, if any
    m_cutsceneExport.LoadEventDefsFile();

	m_cutsceneExport.SetPartData();

    m_cutsceneExport.GetCutsceneFile().SortLoadEvents();
    m_cutsceneExport.GetCutsceneFile().SortEvents();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::EnableButtons()
{
    if ( m_spreadsheetObjectsRowRefToObjectMap.GetNumUsed() > 0 )
    {
        cutfObject **ppObject = m_spreadsheetObjectsRowRefToObjectMap.Access( m_iObjectsSpreadsheetSelRowId );
        if ( ppObject != NULL )
        {
			m_addFaceSpreadsheetButton.Enabled = CanEditObject( *ppObject );
            m_editSpreadsheetButton.Enabled = CanEditObject( *ppObject );
            m_removeFromSpreadsheetButton.Enabled = CanRemoveObject( *ppObject );
			m_removeFaceSpreadsheetButton.Enabled = CanRemoveObject( *ppObject );
        }
        else
        {
			m_addFaceSpreadsheetButton.Enabled = false;
            m_editSpreadsheetButton.Enabled = false;
            m_removeFromSpreadsheetButton.Enabled = false;
			m_removeFaceSpreadsheetButton.Enabled = false;
        }

        m_removeAllFromSpreadsheetButton.Enabled = m_spreadsheetObjectsRowRefToObjectMap.GetNumUsed() > 1;
    }
    else
    {
		m_addFaceSpreadsheetButton.Enabled = false;
        m_editSpreadsheetButton.Enabled = false;
        m_removeFromSpreadsheetButton.Enabled = false;
        m_removeAllFromSpreadsheetButton.Enabled = false;
		m_removeFaceSpreadsheetButton.Enabled = false;
    }    

	if ( m_spreadsheetRowRefToPartMap.GetNumUsed() == 0 )
	{
		m_editPartSpreadsheetButton.Enabled = false;
		m_removePartFromSpreadsheetButton.Enabled = false;
		m_addPartToSpreadsheetButton.Enabled = true;
	} 

    bool bCanExport = strlen( GetSceneName() ) > 0 ;
	bool bHasParts = m_spreadsheetRowRefToPartMap.GetNumUsed() > 0;

    SModelObject cameraModelObject;
    bool bHaveCamera = FindCameraAnimationModel( cameraModelObject );
    m_exportCameraAnimationButton.Enabled = bHaveCamera && bCanExport && bHasParts;

    atArray<SModelObject> spreadsheetModelObjects;
    m_exportSpreadsheetAnimationsButton.Enabled = FindSpreadsheetAnimationModels( spreadsheetModelObjects ) && bCanExport && bHasParts;
	m_exportFaceAnimationButton.Enabled = FindSpreadsheetAnimationModels( spreadsheetModelObjects ) && bCanExport && bHasParts;
    m_exportSelectedAnimationsButton.Enabled = bCanExport && bHasParts;

    m_exportCutFileButton.Enabled = bCanExport && bHasParts;
    m_buildAnimationDictionariesButton.Enabled = bCanExport && bHasParts;
    m_patchAnimationDictionariesButton.Enabled = bCanExport && bHasParts;

#ifdef _OUTSOURCE
	m_exportSpreadsheetAnimationsButton.Enabled = false;
	m_editPartSpreadsheetButton.Enabled = false;
	m_removePartFromSpreadsheetButton.Enabled = false;
	m_addPartToSpreadsheetButton.Enabled = false;
	m_exportSelectedAnimationsButton.Enabled = false;
	m_exportCameraAnimationButton.Enabled = false;
	m_exportCutFileButton.Enabled = false;
	m_buildAnimationDictionariesButton.Enabled = false;
	m_patchAnimationDictionariesButton.Enabled = false;
	m_exportFaceAnimationButton.Enabled = false;
#endif
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PopulateComponentObjectTemplateList()
{
    m_pComponentObjectTemplateList.Grow() = rage_new cutfAudioObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfBlockingBoundsObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfRemovalBoundsObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfCameraObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfLightObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfAnimatedParticleEffectObject;
	m_pComponentObjectTemplateList.Grow() = rage_new cutfParticleEffectObject;
	m_pComponentObjectTemplateList.Grow() = rage_new cutfDecalObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfPedModelObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfScreenFadeObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfVehicleModelObject;
	m_pComponentObjectTemplateList.Grow() = rage_new cutfWeaponModelObject;
    m_pComponentObjectTemplateList.Grow() = rage_new cutfPropModelObject;
	m_pComponentObjectTemplateList.Grow() = rage_new cutfRayfireObject;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PopulateNonComponentObjectTemplateList()
{
    m_pNonComponentObjectTemplateList.Grow() = rage_new cutfEventObject;
    m_pNonComponentObjectTemplateList.Grow() = rage_new cutfOverlayObject;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PopulateObjectTypeToEditPopupMap()
{
    // create the different popups
    EditObjectAttributesPopup *pObjectEditPopup = rage_new EditObjectAttributesPopup;
    pObjectEditPopup->UIInit();

	EditOverlayObjectAttributesPopup *pOverlayObjectEditPopup = rage_new EditOverlayObjectAttributesPopup;
	pOverlayObjectEditPopup->UIInit();
	pOverlayObjectEditPopup->SetOverlayTypes(m_vOverlayTypeEntries);

    EditCameraObjectAttributesPopup *pCameraObjectEditPopup = rage_new EditCameraObjectAttributesPopup;
    pCameraObjectEditPopup->UIInit();

    EditPedModelObjectAttributesPopup *pPedModelObjectEditPopup = rage_new EditPedModelObjectAttributesPopup;
    pPedModelObjectEditPopup->UIInit();
	pPedModelObjectEditPopup->SetHandles(m_vHandleEntries);

    EditVehicleModelObjectAttributesPopup *pVehicleModelObjectEditPopup = rage_new EditVehicleModelObjectAttributesPopup;
    pVehicleModelObjectEditPopup->UIInit();
	pVehicleModelObjectEditPopup->SetHandles(m_vHandleEntries);

    EditModelObjectAttributesPopup *pModelObjectEditPopup = rage_new EditModelObjectAttributesPopup;
    pModelObjectEditPopup->UIInit();
	pModelObjectEditPopup->SetHandles(m_vHandleEntries);
    
    EditBlockingBoundsObjectAttributesPopup *pBlockingBoundsEditPopup = rage_new EditBlockingBoundsObjectAttributesPopup;
    pBlockingBoundsEditPopup->UIInit();

    EditFindModelObjectAttributesPopup *pFindModelObjectEditPopup = rage_new EditFindModelObjectAttributesPopup;
    pFindModelObjectEditPopup->UIInit();

    EditLightObjectAttributesPopup *pEditLightObjectAttributesPopup = rage_new EditLightObjectAttributesPopup;
    pEditLightObjectAttributesPopup->UIInit();
	pEditLightObjectAttributesPopup->SetProperties(m_vLightPropertyEntries);

    EditAnimatedParticleEffectObjectAttributesPopup *pEditParticleEffectPopup = rage_new EditAnimatedParticleEffectObjectAttributesPopup;
    pEditParticleEffectPopup->UIInit();

	EditWeaponModelObjectAttributesPopup *pEditWeaponModelObjectPopup = rage_new EditWeaponModelObjectAttributesPopup;
	pEditWeaponModelObjectPopup->UIInit();
	pEditWeaponModelObjectPopup->SetProperties(m_vWeaponTypeEntries);
	pEditWeaponModelObjectPopup->SetHandles(m_vHandleEntries);

    // assign the popups to the known object types
    //m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfAssetManagerObject::GetStaticTypeName() ), pObjectEditPopup );
    //m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfAnimationManagerObject::GetStaticTypeName() ), pObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfOverlayObject::GetStaticTypeName() ), pOverlayObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfScreenFadeObject::GetStaticTypeName() ), pObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfSubtitleObject::GetStaticTypeName() ), pObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfPedModelObject::GetStaticTypeName() ), pPedModelObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfVehicleModelObject::GetStaticTypeName() ), pVehicleModelObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfPropModelObject::GetStaticTypeName() ), pModelObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfHiddenModelObject::GetStaticTypeName() ), pFindModelObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfFixupModelObject::GetStaticTypeName() ), pFindModelObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfBlockingBoundsObject::GetStaticTypeName() ), pBlockingBoundsEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfRemovalBoundsObject::GetStaticTypeName() ), pBlockingBoundsEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfLightObject::GetStaticTypeName() ), pEditLightObjectAttributesPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfCameraObject::GetStaticTypeName() ), pCameraObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfAudioObject::GetStaticTypeName() ), pObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfEventObject::GetStaticTypeName() ), pObjectEditPopup );
    m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfAnimatedParticleEffectObject::GetStaticTypeName() ), pEditParticleEffectPopup );
	m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfParticleEffectObject::GetStaticTypeName() ), pEditParticleEffectPopup );
	m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfWeaponModelObject::GetStaticTypeName() ), pEditWeaponModelObjectPopup );
	m_pObjectTypeToEditPopupMap.Insert( ConstString( cutfDecalObject::GetStaticTypeName() ), pObjectEditPopup );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RefreshSpreadsheetObjectIds()
{
    // update the spreadsheet
    atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        m_spreadsheetObjects.SetCell( entry.GetKey(), ID_SPREADSHEET_COLUMN, entry.GetData()->GetObjectId() );
    }
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ImportCutListButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	char cFilter[RAGE_MAX_PATH];
	sprintf( cFilter, "*.%s", "cutpart" );

	// Open File Popup
	m_openFilePopup.Caption = "Import Cut Part";
	m_openFilePopup.Filter = cFilter;
	m_openFilePopup.Style = kFBFilePopupOpen;

	atString exportPath(m_exportPathTextBox.Caption.AsString());
	exportPath.Replace("/","\\");
	m_openFilePopup.Path.SetString(exportPath.c_str());

	if ( m_openFilePopup.Execute() )
	{
		char cFullPath[RAGE_MAX_PATH];
		safecpy( cFullPath, (const char*)m_openFilePopup.FullFilename, sizeof(cFullPath) );

		bool bSuccess = ImportCutPart( cFullPath );
		if ( !bSuccess )
		{
			char cMsg[RAGE_MAX_PATH * 2];
			sprintf( cMsg, "Unable to load '%s'.", cFullPath );

			FBMessageBox( "Import Cut List Error", cMsg, "OK" );
			return;
		}
	}
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::ImportCutPart( const char* pCutPart )
{
	bool bSuccess = OpenPartFile( pCutPart );
	if(!bSuccess)
		return false;

	PopulatePartSpreadsheetFromFile();
	EnableButtons();
	SaveToPersistentData();

	RemoteConsoleInterface::NotifyCutsceneUpdate();

	return true;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PopulatePartSpreadsheetFromFile()
{
	ClearPartSpreadsheet();

	for(int i=0; i < m_cutsceneExport.GetCutscenePartFile().GetNumParts(); ++i)
	{
		AddRowToPartSpreadsheet(m_cutsceneExport.GetCutscenePartFile().GetPart(i));
	}

	SaveToPersistentData();
	EnableButtons();

	RemoteConsoleInterface::NotifyCutsceneUpdate();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SetRangesFromStoryTrack(const char* pName, cutfCutsceneFile2* pCutfile)
{
	FBStory& lStory = FBStory::TheOne();

	HFBStoryFolder lFolder = lStory.RootEditFolder;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			char cName[RAGE_MAX_PATH];
			sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", pName);

			if(strcmpi(pTrackName, cName) == 0)
			{
				if(lTrack->Clips.GetCount())
				{
					HFBStoryClip lStartClip = lTrack->Clips[0];
					int iStart = atoi(lStartClip->Start.AsString());

					pCutfile->SetRangeStart(iStart);

					HFBStoryClip lEndClip = lTrack->Clips[lTrack->Clips.GetCount()-1];
					int iEnd = atoi(lEndClip->Stop.AsString());

					pCutfile->SetRangeEnd(iEnd-1);
				}
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RenameStoryTrack(const char* pPartName)
{
	FBStory& lStory = FBStory::TheOne();
	HFBStoryFolder lFolder = lStory.RootEditFolder;

	for (int i = 0; i < lFolder->Tracks.GetCount(); ++i)
	{
		HFBStoryTrack lTrack = lFolder->Tracks[i];

		if(lTrack->Type.AsInt() == kFBStoryTrackShot)
		{
			const char* pTrackName = (const char*)lTrack->LongName;

			if(strcmpi(pTrackName, "Edited Shot Track") == 0)
			{
				char cPartTrack[RAGE_MAX_PATH];
				sprintf(cPartTrack, "%s_EST", pPartName);

				lTrack->Name = cPartTrack;
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RefreshHandleFileButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	PopulateFromCutsceneHandleFile();

	EditObjectAttributesPopup** pObjectEditPopup = m_pObjectTypeToEditPopupMap.Access(ConstString( cutfPedModelObject::GetStaticTypeName() ));
	
	if ( pObjectEditPopup != NULL )
	{
		EditPedModelObjectAttributesPopup* pPedModelObjectEditPopup = dynamic_cast<EditPedModelObjectAttributesPopup*>(*pObjectEditPopup);
		if ( pPedModelObjectEditPopup )	{ pPedModelObjectEditPopup->SetHandles(m_vHandleEntries); }
	}

	pObjectEditPopup = m_pObjectTypeToEditPopupMap.Access(ConstString( cutfVehicleModelObject::GetStaticTypeName() ));
	if ( pObjectEditPopup != NULL )
	{
		EditVehicleModelObjectAttributesPopup* pVehicleModelObjectEditPopup = dynamic_cast<EditVehicleModelObjectAttributesPopup*>(*pObjectEditPopup);
		if ( pVehicleModelObjectEditPopup )	{ pVehicleModelObjectEditPopup->SetHandles(m_vHandleEntries); }
	}

	pObjectEditPopup = m_pObjectTypeToEditPopupMap.Access(ConstString( cutfWeaponModelObject::GetStaticTypeName() ));
	if ( pObjectEditPopup != NULL )
	{
		EditWeaponModelObjectAttributesPopup* pWeaponObjectEditPopup = dynamic_cast<EditWeaponModelObjectAttributesPopup*>(*pObjectEditPopup);
		if ( pWeaponObjectEditPopup )	{ pWeaponObjectEditPopup->SetHandles(m_vHandleEntries); }
	}

	pObjectEditPopup = m_pObjectTypeToEditPopupMap.Access(ConstString( cutfPropModelObject::GetStaticTypeName() ));
	if ( pObjectEditPopup != NULL )
	{
		EditModelObjectAttributesPopup* pModelObjectEditPopup = dynamic_cast<EditModelObjectAttributesPopup*>(*pObjectEditPopup);
		if ( pModelObjectEditPopup )	{ pModelObjectEditPopup->SetHandles(m_vHandleEntries); }
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ImportCutFileButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	char cFilter[64];
	sprintf( cFilter, "*.%s; *.%s", PI_CUTSCENE_XMLFILE_EXT, PI_CUTSCENE_BINFILE_EXT );

	// Open File Popup
	m_openFilePopup.Caption = "Import Cut File";
	m_openFilePopup.Filter = cFilter;
	m_openFilePopup.Style = kFBFilePopupOpen;

	sprintf( cFilter, "*.%s; *.%s", PI_CUTSCENE_XMLFILE_EXT, PI_CUTSCENE_BINFILE_EXT );

	// Save File Popup
	m_saveFilePopup.Caption = "Save Cut File";
	m_saveFilePopup.Filter = cFilter;
	m_saveFilePopup.Style = kFBFilePopupSave;

    int iResult = FBMessageBox( "Import Cut File Warning", 
        "Importing a cut file will permanently overwrite the current cut scene data and cannot be undone.\n\nDo you wish to proceed?", 
        "Yes", "No" );
    if ( iResult == 2 )
    {
        return;
    }

	atString exportPath(m_exportPathTextBox.Caption.AsString());
	exportPath.Replace("/","\\");
	m_openFilePopup.Path.SetString(exportPath.c_str());

    if ( m_openFilePopup.Execute() )
    {
        char cFullPath[RAGE_MAX_PATH];
		safecpy( cFullPath, (const char*)m_openFilePopup.FullFilename, sizeof(cFullPath) );

		if (stricmp(ASSET.FileName(cFullPath),"data.cutxml")!=0)
		{
			int iResult = FBMessageBox( "Import Cut File Warning", 
				"Cut file selected is not named 'data.cutxml', you may have selected the wrong cutfile.\n\nDo you wish to proceed?", 
				"Yes", "No" );
			if ( iResult == 2 )
			{
				return;
			}
		}
		bool bSuccess = ImportCutFile(cFullPath);
		if ( !bSuccess )
		{
			char msg[RAGE_MAX_PATH * 2];
			sprintf( msg, "Unable to load '%s'.", cFullPath );

			FBMessageBox( "Import Cut File Error", msg, "OK" );
		}
    }
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::ImportCutFile( const char* pCutFile )
{
	bool bSuccess = OpenCutFile( pCutFile );

	UpdateUIFromCutfile();

	return bSuccess;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::VerifyCutsceneButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/verifycutscene", NULL),"rexmbrage");
	bool bErrors = false;

	bErrors = !rexMBAnimExportCommon::CheckFPS();
	
	const atArray<cutfObject *>& pObjectList = m_cutsceneExport.GetCutsceneFile().GetObjectList();
	for ( int i = 0; i < pObjectList.GetCount(); ++i )
	{
		// Check ped models
		cutfPedModelObject* pPedModelObject = dynamic_cast<cutfPedModelObject*>(pObjectList[i]);
		if(pPedModelObject)
		{
			if(pPedModelObject->GetFaceAttributesFilename().GetCStr() == NULL || strcmpi(pPedModelObject->GetFaceAttributesFilename().GetCStr(),"") == 0)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: No face attribute file set for %s", pPedModelObject->GetDisplayName().c_str());
				bErrors = true;
			}

			if(pPedModelObject->GetFaceExportCtrlSpecFile().GetCStr() == NULL || strcmpi(pPedModelObject->GetFaceExportCtrlSpecFile().GetCStr(),"") == 0)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: No face spec file set for %s", pPedModelObject->GetDisplayName().c_str());
				bErrors = true;
			}
		}
		
		// Check props
		cutfModelObject* pModelObject = dynamic_cast<cutfModelObject*>(pObjectList[i]);
		if(pModelObject)
		{
			if(pModelObject->GetHandle().GetCStr() == NULL || strcmpi(pModelObject->GetHandle().GetCStr(),"") == 0)
			{
				ULOGGER_MOBO.SetProgressMessage("ERROR: No valid handle set for %s", pModelObject->GetDisplayName().c_str());
				bErrors = true;
			}
		}		
	}
	ULOGGER_MOBO.PostExport(bErrors);
	if (!bErrors) 
	{
		FBMessageBox("", "Cutscene is OK!","Close");
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::Spreadsheet_DragDrop( HISender /*pSender*/, HKEvent pEvent )
{
	FBEventDragAndDrop e( pEvent );
	
	switch( e.State )
	{
	case kFBDragAndDropBegin:
	case kFBDragAndDropEnd:
	case kFBDragOnEmpty:
	case kFBDragOnEmptyDrop:
		break;
	case kFBDragAndDropDrag:
		{
			e.Accept();
		}
		break;
	case kFBDragAndDropDrop:
		{
			FBModelList models;
			int iCount = e.GetCount();
			for(int i=0; i < iCount; ++i)
			{
				HFBComponent c = e.Get(i);
				const char* cModelName = (const char*)c->LongName;
				if(cModelName != NULL)
				{
					HFBModel model = RAGEFindModelByName(cModelName);
					if(model != NULL)
					{
						models.Add(model);
					}
				}
			}

			if(models.GetCount() > 0)
			{
				AddToSpreadSheet(models);
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::Spreadsheet_RowClick( HISender /*pSender*/, HKEvent pEvent )
{
    FBEventSpread e( pEvent );
    m_iObjectsSpreadsheetSelRowId = e.Row;

    cutfObject **ppObject = m_spreadsheetObjectsRowRefToObjectMap.Access( m_iObjectsSpreadsheetSelRowId );
    if ( ppObject != NULL )
    {
        m_removeFromSpreadsheetButton.Enabled = CanRemoveObject( *ppObject );    
		m_removeFaceSpreadsheetButton.Enabled = CanRemoveObject( *ppObject );    
        m_editSpreadsheetButton.Enabled = CanEditObject( *ppObject );
		m_addFaceSpreadsheetButton.Enabled = CanEditObject( *ppObject );
    }
    else
    {
        m_removeFromSpreadsheetButton.Enabled = false;
        m_editSpreadsheetButton.Enabled = false;
		m_addFaceSpreadsheetButton.Enabled = false;
		m_removeFaceSpreadsheetButton.Enabled = false;
    }
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PartSpreadsheet_RowClick( HISender /*pSender*/, HKEvent pEvent )
{
	FBEventSpread e( pEvent );
	m_iPartSpreadsheetSelRowId = e.Row;

	RexRageCutscenePart** ppItem = m_spreadsheetRowRefToPartMap.Access( m_iPartSpreadsheetSelRowId );
	if ( ppItem != NULL )
	{
		m_removePartFromSpreadsheetButton.Enabled = true;
		m_editPartSpreadsheetButton.Enabled = CanEditPart(*ppItem);
	}
	else
	{
		m_removePartFromSpreadsheetButton.Enabled = false;
		m_editPartSpreadsheetButton.Enabled = false;
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::EditPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if ( m_iPartSpreadsheetSelRowId > -1 )
	{
		RexRageCutscenePart** pItem = m_spreadsheetRowRefToPartMap.Access( m_iPartSpreadsheetSelRowId );
		if ( pItem != NULL )
		{
			// Don't like this, we pass the exporter into it, only way we can update the RC on the fly. MPW
			m_partPopup.ShowDialog(*pItem, this);

			char cRange[RAGE_MAX_PATH];
			sprintf_s(cRange, RAGE_MAX_PATH, "%d - %d", (*pItem)->GetCutsceneFile()->GetRangeStart(), (*pItem)->GetCutsceneFile()->GetRangeEnd());
			m_spreadsheetParts.SetCell( m_iPartSpreadsheetSelRowId, SHOT_RANGE_SPREADSHEET_COLUMN, cRange );
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RefreshPartRanges()
{
	int count=0;
	atMap<s32, RexRageCutscenePart *>::Iterator entry = m_spreadsheetRowRefToPartMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next(), count++ )
	{        
		RexRageCutscenePart* pPart = entry.GetData();

		if(pPart)
		{
			char cRange[RAGE_MAX_PATH];
			sprintf_s(cRange, RAGE_MAX_PATH, "%d - %d", pPart->GetCutsceneFile()->GetRangeStart(), pPart->GetCutsceneFile()->GetRangeEnd());
			m_spreadsheetParts.SetCell( count, SHOT_RANGE_SPREADSHEET_COLUMN, cRange );
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::EditSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( m_iObjectsSpreadsheetSelRowId > -1 )
    {
        cutfObject **ppObject = m_spreadsheetObjectsRowRefToObjectMap.Access( m_iObjectsSpreadsheetSelRowId );
        if ( ppObject != NULL )
        {
            SetupCutsceneExport();
			rexMBAnimExportCommon::LoadObjectData( *ppObject, &m_cutsceneExport.GetCutsceneFile() );

            EditObjectAttributesPopup **ppPopup = m_pObjectTypeToEditPopupMap.Access( (*ppObject)->GetTypeName() );
            if ( ppPopup != NULL )
            {
                (*ppPopup)->Modal = true;
                (*ppPopup)->SetObject( *ppObject, &m_cutsceneExport.GetCutsceneFile() );

                if ( (*ppPopup)->Show( this ) )
                {
                    cutfObject *pOldObj = *ppObject;
                    cutfObject *pNewObj = (*ppPopup)->GetObject();

                    atArray<cutfObject *> &objectList = const_cast<atArray<cutfObject *> &>( m_cutsceneExport.GetCutsceneFile().GetObjectList() );
                    for ( int i = 0; i < objectList.GetCount(); ++i )
                    {
                        if ( objectList[i]->GetObjectId() == pOldObj->GetObjectId() )
                        {
                            objectList[i] = pNewObj;
                            break;
                        }
                    }

                    *ppObject = pNewObj;
                    delete pOldObj;

					cutfModelObject* pModelObject = dynamic_cast<cutfModelObject*>(pNewObj);
					if(pModelObject)
					{
						m_spreadsheetObjects.SetCell(m_iObjectsSpreadsheetSelRowId, HANDLE_SPREADSHEET_COLUMN, (char*)pModelObject->GetHandle().GetCStr() );
					}

					rexMBAnimExportCommon::SaveObjectData( pNewObj, &m_cutsceneExport.GetCutsceneFile() );
                }
            }
        }
    }
}
//-------------------------------------------------
void RexRageCutsceneExportTool::AddToPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	AddPartPopup nonComponentPopup;
	nonComponentPopup.UICreate();
	nonComponentPopup.UIConfigure();

	atArray<atString> list;
	list.Grow() = "Internal";

	nonComponentPopup.SetTemplateObjectList(list);
	if(nonComponentPopup.Show())
	{
		RexRageCutscenePart* pPart = rage_new RexRageCutscenePart(&m_cutsceneExport.GetCutsceneFile(), nonComponentPopup.GetName());

		AddRowToPartSpreadsheet( pPart );

		m_cutsceneExport.GetCutscenePartFile().AddPart( pPart );
	}

	EnableButtons();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::AddToSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    // see if we have any models selected
    FBModelList models;
    FBGetSelectedModels( models, NULL, true );

	AddToSpreadSheet(models);
}
//-------------------------------------------------
void RexRageCutsceneExportTool::FindAndAddFaceNode(cutfObject* pObject)
{
	cutfPedModelObject* pPedObject = dynamic_cast<cutfPedModelObject*>(pObject);

	if(pPedObject)
	{
		char cCutsceneFace[RAGE_MAX_PATH];
		sprintf(cCutsceneFace, "%s:faceControls_OFF", pObject->GetDisplayName().c_str());

		char cAmbientFace[RAGE_MAX_PATH];
		sprintf(cAmbientFace, "%s:Ambient_UI", pObject->GetDisplayName().c_str());

		HFBModel pCutsceneFace = RAGEFindModelByName(cCutsceneFace);
		HFBModel pAmbientFace = RAGEFindModelByName(cAmbientFace);

		if(pCutsceneFace)
		{
			SetFaceAnimationNode(pPedObject, cCutsceneFace, -1);
		}
		else if(pAmbientFace)
		{
			SetFaceAnimationNode(pPedObject, cAmbientFace, -1);
		}

		if(pCutsceneFace && pAmbientFace)
		{
			SetFaceAnimationNode(pPedObject, cCutsceneFace, -1);
		}
	}
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::AddFaceToSpreadsheet( const char* pTargetName, const char* pFaceName )
{
	cutfObject *pObject = NULL;
	int refIdx = -1;
	atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( entry.GetData() );
		if ( (pNamedObject != NULL) && (stricmp( pTargetName, pNamedObject->GetName().GetCStr() ) == 0) )
		{
			pObject = entry.GetData();
			refIdx = entry.GetKey();
			break;
		}
	}

	if ( pObject == NULL )
	{
		return false;
	}

	if(pObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE)
	{
		cutfPedModelObject *pPedModelObj = dynamic_cast<cutfPedModelObject *>( (pObject) );
		if(pPedModelObj)
		{
			if (strcmp(pFaceName,"")==0)
			{
				// Auto detect
				atString faceNodeName;
				if(rexMBAnimExportCommon::FindFaceNode(pObject, faceNodeName))
				{
					SetFaceAnimationNode(pPedModelObj, faceNodeName.c_str(), refIdx);
					return true;
				}
			}
			else
			{
				SetFaceAnimationNode(pPedModelObj, pFaceName, refIdx);
				return true;
			}
		}
	}
	return false;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::RemoveFaceFromSpreadsheet( const char* pTargetName )
{
	cutfObject *pObject = NULL;
	int refIdx = -1;
	atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( entry.GetData() );
		if ( (pNamedObject != NULL) && (stricmp( pTargetName, pNamedObject->GetName().GetCStr() ) == 0) )
		{
			pObject = entry.GetData();
			refIdx = entry.GetKey();
			break;
		}
	}

	if ( pObject == NULL )
	{
		return false;
	}

	if(pObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE)
	{
		cutfPedModelObject *pPedModelObj = dynamic_cast<cutfPedModelObject *>( (pObject) );
		if(pPedModelObj)
		{
			SetFaceAnimationNode(pPedModelObj, "", refIdx);
			return true;
		}
	}
	return false;
}
//-------------------------------------------------
template <class modelType>
bool RexRageCutsceneExportTool::AddModelObjectToSpreadsheet( const char* pTargetName, bool bAddFace )
{
	HFBModel pModel = RAGEFindModelByName( pTargetName );

	atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( entry.GetData() );
		if ( (pNamedObject != NULL) && (stricmp( pTargetName, pNamedObject->GetName().GetCStr() ) == 0) )
		{
			return false;
		}
	}

	if (pModel && !IsIllegalName(pTargetName))
	{
		modelType* pedObject = rage_new modelType();

		pedObject->SetName( pTargetName );

		// add the default spec file
		SetDefaultSpecFile(pedObject);

		// add the default compress file
		pedObject->SetAnimCompressionFile("Cutscene_Default_Compress.txt");

		if (bAddFace) 
			FindAndAddFaceNode(pedObject);

		SetModelHandle(pedObject);
			
		m_cutsceneExport.GetCutsceneFile().AddObject( pedObject );
		AddRowToSpreadsheet( pedObject ); 

		if (m_cutsceneExport.IsObjectAnimatable(pedObject))
		{
			bool bHaveAnimMgr = false;
			atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
			for ( entry.Start(); !entry.AtEnd(); entry.Next() )
			{
				if ( entry.GetData()->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE )
				{
					bHaveAnimMgr = true;
					break;
				}
			}

			if ( !bHaveAnimMgr )
			{
				cutfAnimationManagerObject *pAnimManagerObj = rage_new cutfAnimationManagerObject();
				m_cutsceneExport.GetCutsceneFile().AddObject( pAnimManagerObj );
				AddRowToSpreadsheet( pAnimManagerObj );
			}
		}

		return true;
	}
	return false;
}
//-------------------------------------------------
template <class modelType>
bool RexRageCutsceneExportTool::AddObjectToSpreadsheet( const char* pTargetName )
{
	atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( entry.GetData() );
		if ( (pNamedObject != NULL) && (stricmp( pTargetName, pNamedObject->GetName().GetCStr() ) == 0) )
		{
			return false;
		}
	}

	if (!IsIllegalName(pTargetName))
	{
		modelType* pedObject = rage_new modelType();

		pedObject->SetName( pTargetName );

		m_cutsceneExport.GetCutsceneFile().AddObject( pedObject );
		AddRowToSpreadsheet( pedObject ); 

		if (m_cutsceneExport.IsObjectAnimatable(pedObject))
		{
			bool bHaveAnimMgr = false;
			atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
			for ( entry.Start(); !entry.AtEnd(); entry.Next() )
			{
				if ( entry.GetData()->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE )
				{
					bHaveAnimMgr = true;
					break;
				}
			}

			if ( !bHaveAnimMgr )
			{
				cutfAnimationManagerObject *pAnimManagerObj = rage_new cutfAnimationManagerObject();
				m_cutsceneExport.GetCutsceneFile().AddObject( pAnimManagerObj );
				AddRowToSpreadsheet( pAnimManagerObj );
			}
		}

		return true;
	}
	return false;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::AddPedToSpreadsheet( const char* pTargetName, bool bAddFace )
{
	return AddModelObjectToSpreadsheet<cutfPedModelObject>(pTargetName, bAddFace);
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::AddPropToSpreadsheet( const char* pTargetName )
{
	return AddModelObjectToSpreadsheet<cutfPropModelObject>(pTargetName);
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::AddVehicleToSpreadsheet( const char* pTargetName )
{
	return AddModelObjectToSpreadsheet<cutfVehicleModelObject>(pTargetName);
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::AddAudioToSpreadsheet( const char* pTargetName )
{
	return AddObjectToSpreadsheet<cutfAudioObject>(pTargetName);
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::AddWeaponToSpreadsheet( const char* pTargetName )
{
	return AddModelObjectToSpreadsheet<cutfWeaponModelObject>(pTargetName);
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::AddCameraToSpreadsheet(const char* pCameraName, bool bAddFade)
{
	// Make sure there isn't already a cam in scene
	bool bSkipCamera = false;
	atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		if ( entry.GetData()->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE )
		{
			if (!bAddFade || stricmp(entry.GetData()->GetDisplayName().c_str(),pCameraName)!=0)
				return false;       
			else
			{
				bSkipCamera = true;
				break;
			}
		}
	}

	if (!bSkipCamera)
	{
		// Make sure the name matches is legal, and matches camera
		if (IsIllegalName(pCameraName))
			return false;
		FBSystem m_System;
		HFBScene lScene = m_System.Scene;
		bool bFound = false;
		for (int i=0; i<lScene->Cameras.GetCount(); ++i)
		{
			if (stricmp(lScene->Cameras.GetAt(i)->Name,pCameraName)==0 || stricmp(lScene->Cameras.GetAt(i)->LongName,pCameraName)==0)
			{
				bFound = true;
				break;
			}
		}
		if (!bFound)
			return false;

		cutfObject *pObject = rage_new cutfCameraObject;
		cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( pObject );
		pNamedObject->SetName( pCameraName );

		// see if we already have an anim manager
		bool bHaveAnimMgr = false;
		entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
		for ( entry.Start(); !entry.AtEnd(); entry.Next() )
		{
			if ( entry.GetData()->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE )
			{
				bHaveAnimMgr = true;
				break;
			}
		}

		if ( !bHaveAnimMgr )
		{
			cutfAnimationManagerObject *pAnimManagerObj = rage_new cutfAnimationManagerObject();
			m_cutsceneExport.GetCutsceneFile().AddObject( pAnimManagerObj );
			AddRowToSpreadsheet( pAnimManagerObj );
		}

		m_cutsceneExport.GetCutsceneFile().AddObject( pObject );
		AddRowToSpreadsheet( pObject );
	}

	// If we add the camera then look if there is a screen fade for this camera and add it aswell
	char cFadeModel[RAGE_MAX_PATH];
	sprintf(cFadeModel, "%s ScreenFade", pCameraName);

	HFBModel pFadeModel = RAGEFindModelByName(cFadeModel);
	if(bAddFade && pFadeModel != NULL && !IsIllegalName(pFadeModel->LongName))
	{
		bool bAdd = true;
		entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
		for ( entry.Start(); !entry.AtEnd(); entry.Next() )
		{
			cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( entry.GetData() );
			if ( (pNamedObject != NULL) && (stricmp( pFadeModel->LongName, pNamedObject->GetName().GetCStr() ) == 0) )
			{
				bAdd = false;
				break;
			}
		}

		if ( bAdd )
		{
			cutfObject *pFadeObject = rage_new cutfScreenFadeObject;
			cutfNamedObject *pNamedFadeObject = dynamic_cast<cutfNamedObject *>( pFadeObject );
			pNamedFadeObject->SetName( pFadeModel->LongName );
			m_cutsceneExport.GetCutsceneFile().AddObject( pFadeObject );
			AddRowToSpreadsheet( pFadeObject );
		}
		else if (bSkipCamera)
			return false;
	}
	else if (bSkipCamera)
		return false;

	EnableButtons();

	RefreshShreadsheetSizes();

	return true;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RemoveAllFromSpreadsheet()
{
	ClearSpreadsheet();

	m_cutsceneExport.GetCutsceneFile().Clear();

	// always have a scene manager object
	cutfAssetManagerObject *pAssetManagerObj = rage_new cutfAssetManagerObject();
	m_cutsceneExport.GetCutsceneFile().AddObject( pAssetManagerObj );
	AddRowToSpreadsheet( pAssetManagerObj );

	// always have an animation manager object
	cutfAnimationManagerObject *pAnimationManagerObj = rage_new cutfAnimationManagerObject();
	m_cutsceneExport.GetCutsceneFile().AddObject( pAnimationManagerObj );
	AddRowToSpreadsheet( pAnimationManagerObj );

	RefreshShreadsheetSizes();

	EnableButtons();
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::RemoveFromSpreadsheet(const char* pName)
{
	cutfObject *pObject = NULL;
	s32 objRowRef = -1;
	atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( entry.GetData() );
		if ( (pNamedObject != NULL) && (stricmp( pName, pNamedObject->GetName().GetCStr() ) == 0) )
		{
			pObject = entry.GetData();
			objRowRef = entry.GetKey();
			break;
		}
	}

	if ( pObject == NULL )
	{
		return false;
	}

	m_cutsceneExport.GetCutsceneFile().RemoveObject( pObject );
	RemoveRowFromSpreadsheet( objRowRef );

	m_iObjectsSpreadsheetSelRowId = -1;

	// see if we still need an anim mgr
	bool bNeedAnimMgr = false;
	s32 animMgrRowRef = -1;
	cutfObject *pAnimMgrObj = NULL;
	entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		if ( m_cutsceneExport.IsObjectAnimatable( entry.GetData() ) )
		{
			bNeedAnimMgr = true;
		}

		if ( entry.GetData()->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE )
		{
			animMgrRowRef = entry.GetKey();
			pAnimMgrObj = entry.GetData();
		}
	}

	// we don't need the anim mgr anymore, so remove it
	if ( (animMgrRowRef != -1) && !bNeedAnimMgr )
	{
		m_cutsceneExport.GetCutsceneFile().RemoveObject( pAnimMgrObj );
		RemoveRowFromSpreadsheet( animMgrRowRef );
	}

	RefreshSpreadsheetObjectIds();

	EnableButtons();

	return true;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::AddToSpreadSheet(const FBModelList& models)
{    
	atArray<HFBComponent> componentList;

	// only allow one camera to be added
	bool bCanAddCamera = true;
	bool bCanAddAudio = true;
	atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		if ( entry.GetData()->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE )
		{
			bCanAddCamera = false;            
		}

		if ( entry.GetData()->GetType() == CUTSCENE_AUDIO_OBJECT_TYPE )
		{
			bCanAddAudio = false;            
		}
	}

	// only add the models that we don't yet have
	if ( models.GetCount() > 0 )
	{
		for ( int i = 0; i < models.GetCount(); ++i )
		{
			char cName[128];
			safecpy( cName, (const char*)models[i]->LongName, sizeof(cName) );

			bool bAdd = true;

			atMap<s32, cutfObject *>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
			for ( entry.Start(); !entry.AtEnd(); entry.Next() )
			{
				cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( entry.GetData() );
				if ( (pNamedObject != NULL) && (strcmp( cName, pNamedObject->GetName().GetCStr() ) == 0) )
				{
					bAdd = false;
					break;
				}
			}

			if ( bAdd )
			{
				componentList.Grow() = models[i];
			}
		}
	}

	// see if we have any audio clips selected
	atArray<HFBAudioClip> audioClips;

	FBSystem system;

	for ( int i = 0; i < system.Scene->AudioClips.GetCount(); ++i )
	{
		HFBAudioClip pAudioClip = static_cast<HFBAudioClip>(system.Scene->AudioClips.GetAt( i ));

		// Yay, this works in ver. 2009!
		if ( pAudioClip->Selected )
		{
			audioClips.PushAndGrow( pAudioClip );
		}
	}

	// only add the audio clips that we don't yet have
	for ( int i = 0; i < audioClips.GetCount(); ++i )
	{
		char name[128];
		safecpy( name, (const char*)audioClips[i]->Name, sizeof(name) );

		bool bAdd = true;
		atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
		for ( entry.Start(); !entry.AtEnd(); entry.Next() )
		{
			if ( entry.GetData()->GetType() == CUTSCENE_AUDIO_OBJECT_TYPE )
			{
				const cutfAudioObject *pAudioObject = dynamic_cast<const cutfAudioObject *>( entry.GetData() );                    
				if ( strcmp( name, pAudioObject->GetName() ) == 0 )
				{
					bAdd = false;
					break;
				}
			}
		}

		if ( bAdd )
		{
			componentList.Grow() = audioClips[i];
		}
	}

	bool addedObjects = false;
	bool openAddNonComponentPopup = false;
	if ( componentList.GetCount() > 0 )
	{
		m_addComponentsPopup.SetComponentList( componentList );

		if ( m_addComponentsPopup.Show( this ) )
		{
			if ( m_addComponentsPopup.AddNonComponent() )
			{
				openAddNonComponentPopup = true;
			}
			else
			{
				const atArray<cutfObject *>& pObjectList = m_addComponentsPopup.GetObjectList();
				atArray<int> addModelList;
				addModelList.Reserve( pObjectList.GetCount() );
				bool bHaveModelToSkip = false;
				bool bHaveCameraToSkip = false;
				bool bHaveAudioToSkip = false;

				// see if we need an anim mgr
				bool bNeedAnimMgr = false;
				for ( int i = 0; i < pObjectList.GetCount(); ++i )
				{
					if ( IsIllegalName( pObjectList[i]->GetDisplayName().c_str() ) )
					{
						addModelList.PushAndGrow( 0 );
						bHaveModelToSkip = true;
					}
					else if ( (pObjectList[i]->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE) && !bCanAddCamera )
					{
						addModelList.PushAndGrow( -1 );
						bHaveCameraToSkip = true;
					}
					else if ( (pObjectList[i]->GetType() == CUTSCENE_AUDIO_OBJECT_TYPE) && !bCanAddAudio )
					{
						addModelList.PushAndGrow( -2 );
						bHaveAudioToSkip = true;
					}
					else if ( (pObjectList[i]->GetType() == CUTSCENE_MODEL_OBJECT_TYPE))
					{
						FindAndAddFaceNode(pObjectList[i]);
						addModelList.PushAndGrow( 1 );
					}
					else 
					{
						if ( m_cutsceneExport.IsObjectAnimatable( pObjectList[i] ) )
						{
							bNeedAnimMgr = true;
						}

						addModelList.PushAndGrow( 1 );
					}
				}

				if ( bNeedAnimMgr )
				{
					// see if we already have one
					bool bHaveAnimMgr = false;
					atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
					for ( entry.Start(); !entry.AtEnd(); entry.Next() )
					{
						if ( entry.GetData()->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE )
						{
							bHaveAnimMgr = true;
							break;
						}
					}

					if ( !bHaveAnimMgr )
					{
						cutfAnimationManagerObject *pAnimManagerObj = rage_new cutfAnimationManagerObject();
						m_cutsceneExport.GetCutsceneFile().AddObject( pAnimManagerObj );
						AddRowToSpreadsheet( pAnimManagerObj );
					}
				}

				int numAdded = 0;

				// now add the rows for the objects we're adding
				for ( int i = 0; i < pObjectList.GetCount(); ++i )
				{
					if ( addModelList[i] == 1 )
					{
						cutfModelObject* pModel = dynamic_cast<cutfModelObject*>(pObjectList[i]);
						if(pModel != NULL)
						{
							pModel->SetAnimCompressionFile("Cutscene_Default_Compress.txt");
							SetModelHandle(pModel);
							m_cutsceneExport.GetCutsceneFile().AddObject( pModel );
							AddRowToSpreadsheet( pModel ); 
						}
						else
						{
							m_cutsceneExport.GetCutsceneFile().AddObject( pObjectList[i] );
							AddRowToSpreadsheet( pObjectList[i] );

							// If we add the camera then look if there is a screen fade for this camera and add it aswell
							cutfCameraObject* pCamera = dynamic_cast<cutfCameraObject*>(pObjectList[i]);
							if(pCamera != NULL)
							{
								char cFadeModel[MAX_PATH];
								sprintf(cFadeModel, "%s ScreenFade", pCamera->GetName().GetCStr());

								HFBModel pFadeModel = RAGEFindModelByName(cFadeModel);
								if(pFadeModel != NULL)
								{
									FBModelList models;
									models.Add(pFadeModel);
									AddToSpreadSheet(models);
									continue;
								}
							}

							cutfParticleEffectObject* pParticleEffect = dynamic_cast<cutfParticleEffectObject*>(pObjectList[i]);
							if(pParticleEffect != NULL)
							{
								HFBModel pParticleModel = RAGEFindModelByName(pParticleEffect->GetName().GetCStr());
								if(pParticleModel)
								{
									HFBProperty pProperty = pParticleModel->PropertyList.Find("FX_List");
									if(pProperty)
									{
										FBPropertyString *pPropertyString = (FBPropertyString *)pProperty;

										char* pValue = (char*)pPropertyString->GetPropertyValue();
										if(pValue)
										{
											char czFx[MAX_PATH];
											czFx[0] = 0;
											const char* pFilename = ASSET.FileName(pValue);
											ASSET.RemoveExtensionFromPath(czFx,MAX_PATH,pFilename);

											pParticleEffect->SetFxListHash(czFx);
										}
									}
								}
							}

							cutfAnimatedParticleEffectObject* pAnimatedParticleEffect = dynamic_cast<cutfAnimatedParticleEffectObject*>(pObjectList[i]);
							if(pAnimatedParticleEffect != NULL)
							{
								HFBModel pParticleModel = RAGEFindModelByName(pAnimatedParticleEffect->GetName().GetCStr());
								if(pParticleModel)
								{
									HFBProperty pProperty = pParticleModel->PropertyList.Find("FX_List");
									if(pProperty)
									{
										FBPropertyString *pPropertyString = (FBPropertyString *)pProperty;

										char* pValue = (char*)pPropertyString->GetPropertyValue();
										if(pValue)
										{
											char czFx[MAX_PATH];
											czFx[0] = 0;
											const char* pFilename = ASSET.FileName(pValue);
											ASSET.RemoveExtensionFromPath(czFx,MAX_PATH,pFilename);

											pAnimatedParticleEffect->SetFxListHash(czFx);
										}
									}

									pProperty = pParticleModel->PropertyList.Find("FX_RuleFile");
									if(pProperty)
									{
										FBPropertyString *pPropertyString = (FBPropertyString *)pProperty;

										char* pValue = (char*)pPropertyString->GetPropertyValue();
										if(pValue)
										{
											// Get Evo Names
											atArray<ConstString> evoParamNameList;
											if ( rexMBAnimExportCommon::GetEvoParamNames( pValue, evoParamNameList ) )
											{
												for(int i=0; i < evoParamNameList.GetCount(); ++i)
												{
													parAttributeList &particleAttributes = const_cast<parAttributeList &>( pAnimatedParticleEffect->GetAttributeList() );
													
													particleAttributes.AddAttribute( "evoParam", atHash16U(evoParamNameList[i].c_str()) );
												}
											}
										}
									}
								}
							}
						}

						++numAdded;
					}
				}

				if ( bHaveAudioToSkip )
				{
					char title[32];
					sprintf( title, "Too Many Audio Tracks" );

					char msg[1024];
					safecpy( msg, "The following could not be added because only one audio track is allowed at any given time:\n", 1024 );

					for ( int i = 0; i < addModelList.GetCount(); ++i )
					{
						if ( addModelList[i] == -2 )
						{
							strcat_s( msg, sizeof(msg), "    " );
							strcat_s( msg, sizeof(msg), pObjectList[i]->GetDisplayName().c_str() );
							strcat_s( msg, sizeof(msg), "\n" );

							// cleanup
							delete pObjectList[i];
						}
					}

					strcat_s( msg, sizeof(msg), "\nPlease delete the existing audio track before trying to add a different one.\nSee the help file for more information on audio track creation for cut scenes." );

					FBMessageBox( title, msg, "OK" );
				}

				if ( bHaveCameraToSkip )
				{
					char title[32];
					sprintf( title, "Too Many Cameras" );

					char msg[1024];
					safecpy( msg, "The following could not be added because only one camera is allowed at any given time:\n", 1024 );

					for ( int i = 0; i < addModelList.GetCount(); ++i )
					{
						if ( addModelList[i] == -1 )
						{
							strcat_s( msg, sizeof(msg), "    " );
							strcat_s( msg, sizeof(msg), pObjectList[i]->GetDisplayName().c_str() );
							strcat_s( msg, sizeof(msg), "\n" );

							// cleanup
							delete pObjectList[i];
						}
					}

					strcat_s( msg, sizeof(msg), "\nPlease delete the existing camera before trying to add a different one.\nSee the help file for more information on camera creation for cut scenes." );

					FBMessageBox( title, msg, "OK" );
				}

				if ( bHaveModelToSkip )
				{
					char title[32];
					sprintf( title, "Illegal Naming Convention" );

					char msg[1024];
					safecpy( msg, "The following could not be added because of illegal naming conventions:\n", 1024 );

					for ( int i = 0; i < addModelList.GetCount(); ++i )
					{
						if ( addModelList[i] == 0 )
						{
							strcat_s( msg, sizeof(msg), "    " );
							strcat_s( msg, sizeof(msg), pObjectList[i]->GetDisplayName().c_str() );
							strcat_s( msg, sizeof(msg), "\n" );

							// cleanup
							delete pObjectList[i];
						}
					}

					strcat_s( msg, sizeof(msg), "\nModel names cannot end with a dash followed by a number.  Please rename and try again." );

					FBMessageBox( title, msg, "OK" );
				}

				addedObjects = numAdded > 0;
			}
		}
	}
	else
	{
		openAddNonComponentPopup = true;
	}

	if ( openAddNonComponentPopup )
	{
		if ( m_addNonComponentPopup.Show( this ) )
		{
			cutfObject *pObject = m_addNonComponentPopup.GetObject();

			if ( IsIllegalName( pObject->GetDisplayName().c_str() ) )
			{                
				char title[32];
				sprintf( title, "Illegal Naming Convention" );

				char msg[1024];
				safecpy( msg, "The following could not be added because of illegal naming conventions:\n", 1024 );

				strcat_s( msg, sizeof(msg), "    " );
				strcat_s( msg, sizeof(msg), pObject->GetDisplayName().c_str() );
				strcat_s( msg, sizeof(msg), "\n" );

				strcat_s( msg, sizeof(msg), "\nNon-Model names cannot end with a dash followed by a number.  Please rename and try again." );

				FBMessageBox( title, msg, "OK" );

				// cleanup
				delete pObject;
			}
			else
			{
				m_cutsceneExport.GetCutsceneFile().AddObject( pObject );
				AddRowToSpreadsheet( pObject );

				addedObjects = true;
			}
		}
	}

	if ( addedObjects )
	{
		EnableButtons();

		RefreshShreadsheetSizes();
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RemoveFromPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	RexRageCutscenePart **ppObject = m_spreadsheetRowRefToPartMap.Access( m_iPartSpreadsheetSelRowId );    
	if ( ppObject == NULL )
	{
		return;
	}

	RemoveRowFromPartSpreadsheet( m_iPartSpreadsheetSelRowId );

	m_cutsceneExport.GetCutscenePartFile().DeletePart( m_iPartSpreadsheetSelRowId );

	m_iPartSpreadsheetSelRowId = -1;

	EnableButtons();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RemoveFromSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    cutfObject **ppObject = m_spreadsheetObjectsRowRefToObjectMap.Access( m_iObjectsSpreadsheetSelRowId );    
    if ( ppObject == NULL )
    {
        return;
    }

    m_cutsceneExport.GetCutsceneFile().RemoveObject( *ppObject );
    RemoveRowFromSpreadsheet( m_iObjectsSpreadsheetSelRowId );

    m_iObjectsSpreadsheetSelRowId = -1;

    // see if we still need an anim mgr
    bool bNeedAnimMgr = false;
    s32 animMgrRowRef = -1;
    cutfObject *pAnimMgrObj = NULL;
    atMap<s32, cutfObject*>::Iterator entry = m_spreadsheetObjectsRowRefToObjectMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( m_cutsceneExport.IsObjectAnimatable( entry.GetData() ) )
        {
            bNeedAnimMgr = true;
        }

        if ( entry.GetData()->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE )
        {
            animMgrRowRef = entry.GetKey();
            pAnimMgrObj = entry.GetData();
        }
    }

    // we don't need the anim mgr anymore, so remove it
    if ( (animMgrRowRef != -1) && !bNeedAnimMgr )
    {
        m_cutsceneExport.GetCutsceneFile().RemoveObject( pAnimMgrObj );
        RemoveRowFromSpreadsheet( animMgrRowRef );
    }

    RefreshSpreadsheetObjectIds();

    EnableButtons();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::MoveUpPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(!m_cutsceneExport.GetCutscenePartFile().MoveUp(m_iPartSpreadsheetSelRowId)) return;

	PopulatePartSpreadsheetFromFile();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::MoveDownPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if(!m_cutsceneExport.GetCutscenePartFile().MoveDown(m_iPartSpreadsheetSelRowId)) return;

	PopulatePartSpreadsheetFromFile();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SyncRangesSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	for(int i=0; i < m_cutsceneExport.GetCutscenePartFile().GetNumParts(); ++i)
	{
		FBStory& lStory = FBStory::TheOne();
		HFBStoryFolder lFolder = lStory.RootEditFolder;

		for (int j = 0; j < lFolder->Tracks.GetCount(); ++j)
		{
			HFBStoryTrack lTrack = lFolder->Tracks[j];

			if(lTrack->Type.AsInt() == kFBStoryTrackShot)
			{
				const char* pTrackName = (const char*)lTrack->LongName;

				char cName[RAGE_MAX_PATH];
				sprintf_s(cName, RAGE_MAX_PATH, "%s_EST", m_cutsceneExport.GetCutscenePartFile().GetPart(i)->GetName());

				if(strcmpi(pTrackName, cName) == 0)
				{
					if(lTrack->Clips.GetCount())
					{
						HFBStoryClip lStartClip = lTrack->Clips[0];
						int iStart = atoi(lStartClip->Start.AsString());

						HFBStoryClip lEndClip = lTrack->Clips[lTrack->Clips.GetCount()-1];
						int iEnd = atoi(lEndClip->Stop.AsString());

						m_cutsceneExport.GetCutscenePartFile().GetPart(i)->GetCutsceneFile()->SetRangeStart(iStart);
						m_cutsceneExport.GetCutscenePartFile().GetPart(i)->GetCutsceneFile()->SetRangeEnd(iEnd-1);
					}
				}
			}
		}
	}

	PopulatePartSpreadsheetFromFile();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::DuplicatePartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if ( m_iPartSpreadsheetSelRowId > -1 )
	{
		RexRageCutscenePart** pItem = m_spreadsheetRowRefToPartMap.Access( m_iPartSpreadsheetSelRowId );
		if ( pItem != NULL )
		{
			int iCount=0;
			while(true)
			{
				iCount++;

				char cName[RAGE_MAX_PATH];
				sprintf(cName, "Shot_%d", iCount);

				bool bFoundShot=false;
				for(int i=0; i < m_cutsceneExport.GetCutscenePartFile().GetNumParts(); ++i)
				{
					if(stricmp(m_cutsceneExport.GetCutscenePartFile().GetPart(i)->GetName(), cName) == 0) 
					{
						bFoundShot=true;
						break;
					}
				}

				if(bFoundShot) continue;

				RexRageCutscenePart* pPart = rage_new RexRageCutscenePart(&m_cutsceneExport.GetCutsceneFile(), cName);
				(*pPart->GetCutsceneFile()) = *(*pItem)->GetCutsceneFile()->Clone();

				AddRowToPartSpreadsheet( pPart );

				m_cutsceneExport.GetCutscenePartFile().AddPart( pPart );

				PopulatePartSpreadsheetFromFile();
				return;
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RemoveAllFromPartSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	ClearPartSpreadsheet();

	m_cutsceneExport.GetCutscenePartFile().Clear();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RemoveAllFromSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    RemoveAllFromSpreadsheet();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RefreshSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    RefreshShreadsheetSizes();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SelectModelButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if ( m_iObjectsSpreadsheetSelRowId > -1 )
	{
		cutfObject **ppObject = m_spreadsheetObjectsRowRefToObjectMap.Access( m_iObjectsSpreadsheetSelRowId );
		if ( ppObject != NULL )
		{
			cutfObject* pObject = *ppObject;
			if(pObject)
			{
				cutfNamedObject* pNamedObject = dynamic_cast<cutfNamedObject*>(pObject);
				if(pNamedObject)
				{
					HFBModel pModel = RAGEFindModelByName(pNamedObject->GetName().GetCStr());
					if(pModel)
					{
						rexMBAnimExportCommon::DeselectAllModels();
						pModel->Selected = true;
					}
				}
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::RemoveFaceSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if ( m_iObjectsSpreadsheetSelRowId > -1 )
	{
		cutfObject **ppObject = m_spreadsheetObjectsRowRefToObjectMap.Access( m_iObjectsSpreadsheetSelRowId );
		if ( ppObject != NULL )
		{
			if((*ppObject)->GetType() == CUTSCENE_MODEL_OBJECT_TYPE)
			{
				cutfPedModelObject *pPedModelObj = dynamic_cast<cutfPedModelObject *>( (*ppObject) );
				if(pPedModelObj)
				{
					SetFaceAnimationNode(pPedModelObj, "", -1);
				}
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::AddFaceSpreadsheetButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	if ( m_iObjectsSpreadsheetSelRowId > -1 )
	{
		cutfObject **ppObject = m_spreadsheetObjectsRowRefToObjectMap.Access( m_iObjectsSpreadsheetSelRowId );
		if ( ppObject != NULL )
		{
			if((*ppObject)->GetType() == CUTSCENE_MODEL_OBJECT_TYPE)
			{
				cutfPedModelObject *pPedModelObj = dynamic_cast<cutfPedModelObject *>( (*ppObject) );
				if(pPedModelObj)
				{
					// see if we have any models selected
					FBModelList models;
					FBGetSelectedModels( models, NULL, true );

					// only add the models that we don't yet have
					if ( models.GetCount() > 0 )
					{
						SetFaceAnimationNode(pPedModelObj, (const char*)models[0]->LongName, -1);
					}
				}
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SetFaceAnimationNode(cutfPedModelObject *pPedModelObj, const char* czNode, int spreadsheetRowId)
{
	if(!pPedModelObj || !czNode) return;

	rexMBAnimExportCommon::SetFaceAnimationNode(pPedModelObj, czNode);

	if (spreadsheetRowId == -1)
		spreadsheetRowId = m_iObjectsSpreadsheetSelRowId;

	m_spreadsheetObjects.SetCell(spreadsheetRowId, FACE_SPREADSHEET_COLUMN, (char*)czNode );			
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UsePerforceIntegrationButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);
} 
//-------------------------------------------------
void RexRageCutsceneExportTool::EditSceneEventsButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	UpdateCutfileFromUI();

	if(m_pEditEventPopup)
		delete m_pEditEventPopup;

	m_pEditEventPopup = rage_new EditEventPopup( &m_cutsceneExport.GetCutsceneFile(), &m_cutsceneExport.GetEventDefs() );

	m_pEditEventPopup->Show( this );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::HelpButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{   
	system("start https://devstar.rockstargames.com/wiki/index.php/CutsceneAnimation");
}
//-------------------------------------------------
string RexRageCutsceneExportTool::ValidateSceneNameLength(string strSceneName)
{
	if(strSceneName.size() > SCENENAME_MAX_SIZE)
	{
		char msg[RAGE_MAX_PATH];
		sprintf( msg, "Scene name '%s' is %d characters long, the maximum is %d, this will be resized.", strSceneName.c_str(), strSceneName.size(), SCENENAME_MAX_SIZE );
		FBMessageBox( "Scene Name Length Error", msg , "OK" );

		strSceneName[SCENENAME_MAX_SIZE] = 0;
		return strSceneName;
	}

	return strSceneName;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SceneNameTextBox_TextChanged( HISender /* pSender */, HKEvent /* pEvent */)
{
	string strSceneName = ValidateSceneNameLength((const char*)m_sceneNameTextBox.Text);

	SetSceneName( strSceneName.c_str() );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::OpenPathButton_Click( HISender /* pSender */, HKEvent /* pEvent */)
{
	atString exportPath(m_exportPathTextBox.Caption.AsString());
	exportPath.Replace("/","\\");

	char cExportPath[RAGE_MAX_PATH*2];
	sprintf_s( cExportPath, RAGE_MAX_PATH, "explorer %s", exportPath.c_str() );

	system(cExportPath);
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ExportSelectedAnimationsButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	UpdateCutfileFromUI();

	atArray<SModelObject> modelObjects;
    if ( FindSelectedAnimationModels( modelObjects ) )
    {       
        // Save the cut file
        m_cutsceneExport.PrepareCutfileForAnimationExport();

		atArray<SModelObject> otherModelObjects;
		FindSpreadsheetAnimationModels( otherModelObjects );
       
        ExportModelAnimations( modelObjects, otherModelObjects );

        RefreshShreadsheetSizes();
    }
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::GetAnimationModels(atArray<SModelObject>& modelObjects)
{
	if ( FindSpreadsheetAnimationModels( modelObjects ) )
	{
		// Save the cut file
		m_cutsceneExport.PrepareCutfileForAnimationExport();

		return true;
	}

	return false;
}
//-------------------------------------------------
bool RexRageCutsceneExportTool::Export(bool bInteractive)
{
	atArray<SModelObject> otherModelObjects;
	atArray<SModelObject> modelObjects;
	if(GetAnimationModels(modelObjects))
	{
		bool bResult = ExportModelAnimations( modelObjects, otherModelObjects, bInteractive );

		RefreshShreadsheetSizes();
		RefreshPartRanges();

		return bResult;
	}

	return false;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ExportSpreadsheetAnimationsButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);

	Export();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::BuildAnimationDictionariesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);

	if(!rexMBAnimExportCommon::CheckWritableRPF())
	{
		return;
	}

	WriteLocalCutsceneProcessorFile();
	if(m_cutsceneExport.BuildAnimationDictionaries(false))
	{
		FBMessageBox("Build Animation Dictionaries", "Success!", "OK");
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ExportCameraAnimationButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	ExportCamera();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ExportFaceAnimationButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);

	UpdateCutfileFromUI();

	atArray<SModelObject> pedModelObjects;
	if ( FindPedAnimationModels( pedModelObjects ) )
	{
		// Save the cut file
		m_cutsceneExport.PrepareCutfileForAnimationExport();

		ExportFaceAnimation( pedModelObjects );

		RefreshShreadsheetSizes();
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::ExportCutFileButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);

	rexMBAnimExportCommon::ClearLogPathContents("cutscene\\process");
	ULOGGER_MOBO.ClearProcessLogDirectory();
	ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/process/cutfile", GetSceneName()), "rexmbrage");

	bool bResult = false;

	char cExportDir[RAGE_MAX_PATH];
	safecpy(cExportDir, m_cutsceneExport.GetSceneName(), RAGE_MAX_PATH);

	UpdateShotExportStatus();
	UpdateCutfileFromUI();

	for(int i=0; i < m_cutsceneExport.GetCutscenePartFile().GetNumParts(); ++i)
	{
		m_partPopup.UpdateCutfileExternal(m_cutsceneExport.GetCutscenePartFile().GetPart(i));
	}

	bResult = m_cutsceneExport.SaveCutFile();
	bResult = bResult && m_cutsceneExport.SaveItemFile();
	bResult = bResult && m_cutsceneExport.JoinScenes(true);
	bResult = bResult && m_cutsceneExport.BuildZip();

	RefreshPartRanges(); // Ranges can potentially be updated from the EST in ValidateAndCreateStoryTrack

	char cLogDir[RAGE_MAX_PATH];
	sprintf(cLogDir, "%s\\cutscene\\process", rexMBAnimExportCommon::GetDefaultLogDir().c_str());

	char cMsg[RAGE_MAX_PATH];
	sprintf(cMsg, "Success! '%s'", cLogDir);
	ULOGGER_MOBO.AddAdditionalLogDir(ULOGGER_MOBO.GetProcessLogDirectory());
	ULOGGER_MOBO.PostExport( cLogDir, true, bResult, "Export Cut File(s)", cMsg );
}
//-------------------------------------------------
void RexRageCutsceneExportTool::PatchAnimationDictionariesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	rexMBAnimExportCommon::SetExporterMode(CUTSCENE_EXPORTER);
	rexMBAnimExportCommon::SetUsePerforceIntegration(m_usePerforceIntegration.State == 1);

	WriteLocalCutsceneProcessorFile();
	m_cutsceneExport.BuildAnimationDictionaries(true);
}
//-------------------------------------------------
void RexRageCutsceneExportTool::FileNewCompleted_Event( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    NewCutFile();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::FileSave_Event(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	//Store the markup data from the tool into a userdata object that will
	//be saved with the scene.
	SaveToPersistentData();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::FileSaveCompleted_Event(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	FBApplication app;

	char czFileName[MAX_PATH];
	ASSET.RemoveExtensionFromPath(czFileName,MAX_PATH,app.FBXFileName.AsString());

	// FBX File has not being saved to .fbx yet, its still in .tmp format.
	char czTmpFileName[MAX_PATH];
	sprintf(czTmpFileName, "%s.%s", czFileName, "tmp");

	ULOGGER_MOBO.PreExport(rexMBAnimExportCommon::CreateLogFileName("cutscene/log", GetSceneName()), "rexmbrage");

	FILE* pFile = fopen(czTmpFileName,"r");
	if(pFile)
	{
		char cHeader[5];
		fread(cHeader,4,1,pFile);
		cHeader[4] = '\0';

		if(stricmp(cHeader,"; FB") == 0)
		{
			ULOGGER_MOBO.SetProgressMessage("WARNING: File is FBX ASCII Format");
		}

		fclose(pFile);
	}

	if(rexMBAnimExportCommon::IsDlc(atString("")))
	{
		SetSceneName(rexMBAnimExportCommon::GetFBXFileName(), false);
		m_sceneNameTextBox.ReadOnly = true;
	}
	else
	{
		m_sceneNameTextBox.ReadOnly = false;
	}

	ULOGGER_MOBO.PostExport();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::FileOpenCompleted_Event(HISender /*pSender*/, HKEvent /*pEvent*/)
{
	//Look for the userobject we used for storing the tool markup data and 
	//load the data off of it if it exists.
	if(FindPersistentData(false))
	{
		LoadFromPersistentData();
	}

	if(rexMBAnimExportCommon::IsDlc(atString("")))
	{
		SetSceneName(rexMBAnimExportCommon::GetFBXFileName());
		m_sceneNameTextBox.ReadOnly = true;
	}
	else
	{
		m_sceneNameTextBox.ReadOnly = false;
	}

	UpdatePaths();

	char cPartFilename[RAGE_MAX_PATH];
	sprintf(cPartFilename, "%s\\data.cutpart", GetExportPath());

	perforce::Sync(GetExportPath(), "...", false, true);
	perforce::ExecuteQueuedOperations(true);
	
	OpenPartFile(cPartFilename);
	PopulatePartSpreadsheetFromFile();
	RefreshHandleFileButton_Click(NULL, NULL);
	SaveToPersistentData();
	RemoteConsoleInterface::NotifyCutsceneUpdate();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::UpdatePartsFromUI()
{
	UpdateShotExportStatus();
	UpdateCutfileFromUI();

	for(int i=0; i < m_cutsceneExport.GetCutscenePartFile().GetNumParts(); ++i)
	{
		m_partPopup.UpdateCutfileExternal(m_cutsceneExport.GetCutscenePartFile().GetPart(i));
	}
}
//-------------------------------------------------
RexRageCutsceneExportPData* RexRageCutsceneExportTool::FindPersistentData( bool bCreate )
{
	RexRageCutsceneExportPData* lData = UserObjects::FindCutsceneMarkupData(bCreate);
    return lData;
}
//-------------------------------------------------
void RexRageCutsceneExportTool::DeletePersistentData()
{
	UserObjects::DeleteCutsceneMarkupData();
}
//-------------------------------------------------
void RexRageCutsceneExportTool::LoadFromPersistentData()
{
	FBString dataStr = UserObjects::FindCutsceneMarkupData( true )->m_Data;
	if(dataStr.GetLen())
	{
		m_cutsceneExport.LoadCutsceneDataFromString( (char*)dataStr );
        m_cutsceneExport.ValidateCutsceneExportData( );
		UpdateUIFromCutfile();
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SaveToPersistentData()
{
	UpdateCutfileFromUI();

	char* pDataStr = NULL;
    if ( m_cutsceneExport.SaveCutsceneDataToString( &pDataStr ) )
    {
        // Clear out old data fields
        DeletePersistentData();

		UserObjects::FindCutsceneMarkupData( true )->m_Data = pDataStr;
		delete [] pDataStr;
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::WriteLocalCutsceneProcessorFile()
{
	char cToolsRoot[RAGE_MAX_PATH];
	rexMBAnimExportCommon::GetEnvironmentVariable("RS_TOOLSROOT", cToolsRoot, RAGE_MAX_PATH);

	char czXmlFile[RAGE_MAX_PATH];
	sprintf(czXmlFile, "%s\\etc\\processors\\RSG.Pipeline.Processor.Animation.Cutscene.PreProcess.local.xml", cToolsRoot);

	if(ASSET.Exists(czXmlFile, NULL))
	{
		xmlDocPtr mDocument = xmlReadFile(czXmlFile, NULL, 0);

		xmlNodePtr rootNode = xmlDocGetRootElement(mDocument);
		if(rootNode)
		{
			for(xmlNodePtr elemContext = xmlFirstElementChild(rootNode); 
				elemContext; 
				elemContext = elemContext->next)
			{
				if(elemContext->type != XML_ELEMENT_NODE)
					continue;

				atString name((char*)xmlGetProp(elemContext, BAD_CAST "name"));
				if(stricmp(name.c_str(), "AutomaticallyBuildConcat") == 0)
				{
					xmlSetProp(elemContext, BAD_CAST "value", BAD_CAST ((m_useAutoConcat.State == 1) ? "true" : "false"));
				}
			}
		}

		xmlSaveFormatFileEnc(czXmlFile, mDocument, "UTF-8", 1);
	}
	else
	{
		xmlDocPtr mDocument = xmlNewDoc(BAD_CAST "1.0");
		xmlNodePtr root_node = xmlNewNode(NULL, BAD_CAST "Parameters");
		xmlDocSetRootElement(mDocument, root_node);

		xmlNodePtr elemZip = xmlNewChild(root_node, NULL, BAD_CAST "Parameter", NULL);
		xmlNewProp(elemZip, BAD_CAST "name", BAD_CAST "AutomaticallyBuildConcat");
		xmlNewProp(elemZip, BAD_CAST "type", BAD_CAST "bool");
		xmlNewProp(elemZip, BAD_CAST "value", BAD_CAST ((m_useAutoConcat.State == 1) ? "true" : "false"));

		xmlSaveFormatFileEnc(czXmlFile, mDocument, "UTF-8", 1);
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SetShotExportStatus(int index, bool bExport)
{
	if(index < m_cutsceneExport.GetCutscenePartFile().GetNumParts())
	{
		atMap<int, RexRageCutscenePart *>::Iterator entry = m_spreadsheetRowRefToPartMap.CreateIterator();
		for ( entry.Start(); !entry.AtEnd(); entry.Next() )
		{
			if(entry.GetData() == m_cutsceneExport.GetCutscenePartFile().GetPart(index))
			{
				m_spreadsheetParts.SetCell( entry.GetKey(), SHOT_EXPORT_SPREADSHEET_COLUMN, bExport ? 1 : 0 );
			}
		}
	}
}
//-------------------------------------------------
void RexRageCutsceneExportTool::SetConcatMode(bool bEnabled)
{
	m_useAutoConcat.State = (int)bEnabled;
}

} // namespace rage
