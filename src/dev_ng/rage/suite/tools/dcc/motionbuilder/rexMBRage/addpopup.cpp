// 
// rexMbRage/addpopup.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "addpopup.h"

#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "rexMBAnimExportCommon/utility.h"

namespace rage {

AddComponentsPopup::AddComponentsPopup()
: m_bAddNonComponent( false )
{
    m_cTypeNameMenu[0] = 0;

    UICreate();
    UIConfigure();
    ClearSpreadsheet();
}

AddComponentsPopup::~AddComponentsPopup()
{
    for ( int i = 0; i < m_pTemplateObjectList.GetCount(); ++i )
    {
        delete m_pTemplateObjectList[i];
    }

    m_pTemplateObjectList.Reset();

    m_cComponentCheckedMap.Kill();
}

void AddComponentsPopup::UICreate()
{
    /* Here's a text-based representation of the Add Models Popup.
    {} = combo boxes (surrounds the name)
    () = button (surrounds the name)
    [ ] = check box
    _ = text box (spans the width of the text)
    |- = spread sheet borders and columns

    --------------------------------------------------
    |-----------||---Type---|-Change Type-|-Add-|----|
    |           ||          |             |     |    |
    |           ||          |             |     |    |
    |           ||          |             |     |    |
    --------------------------------------------------

    ( Other...)                  (   OK   ) ( Cancel )
    */

    Region.X = 200;
    Region.Y = 200;
    Region.Width = 400;
    Region.Height = 300;

    int iAttachBottomY = -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iRowHeight - rexMBAnimExportCommon::c_iMargin;

    // Spreadsheet
    AddRegion( "Spreadsheet", "Spreadsheet",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        iAttachBottomY, kFBAttachBottom, NULL, 1.0 );
    SetControl( "Spreadsheet", m_spreadsheet );

    iAttachBottomY = -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight;

    // Other
    AddRegion( "OtherButton", "OtherButton",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachLeft, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OtherButton", m_otherButton );

    // Cancel
    AddRegion( "CancelButton", "CancelButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "CancelButton", m_cancelButton );

    // Ok
    AddRegion( "OkButton", "OkButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachLeft, "CancelButton", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OkButton", m_okButton );
}

void AddComponentsPopup::UIConfigure()
{
    Caption = "Add Components";

    // Spreadsheet
    m_spreadsheet.Caption = "Component";
    m_spreadsheet.OnCellChange.Add( this, (FBCallback)&AddComponentsPopup::Spreadsheet_CellChange );

    // Other
    m_otherButton.Caption = "Other...";
    m_otherButton.OnClick.Add( this, (FBCallback)&AddComponentsPopup::OtherButton_Click );

    // Ok
    m_okButton.Caption = "OK";
    m_okButton.OnClick.Add( this, (FBCallback)&AddComponentsPopup::OkButton_Click );

    // Cancel
    m_cancelButton.Caption = "Cancel";
    m_cancelButton.OnClick.Add( this, (FBCallback)&AddComponentsPopup::CancelButton_Click );
}

void AddComponentsPopup::SetTemplateObjectList( const atArray<cutfObject *>& pObjectList )
{
    m_cTypeNameMenu[0] = 0;

    m_pTemplateObjectList.Reserve( pObjectList.GetCount() );
    for ( int i = 0; i < pObjectList.GetCount(); ++i )
    {
        strcat_s( m_cTypeNameMenu, sizeof(m_cTypeNameMenu), pObjectList[i]->GetTypeName() );
        
        if ( i < pObjectList.GetCount() - 1 )
        {
            strcat_s( m_cTypeNameMenu, sizeof(m_cTypeNameMenu), "~" );
        }

        m_pTemplateObjectList.Append() = pObjectList[i]->Clone();
    }

    Assert( strlen( m_cTypeNameMenu ) < sizeof(m_cTypeNameMenu) );
}

void AddComponentsPopup::SetComponentList( atArray<HFBComponent> &componentList )
{
    ClearSpreadsheet();

    m_bAddNonComponent = false;

    m_cComponentNameList.Reset();
    for ( int i = 0; i < componentList.GetCount(); ++i )
    {       
        // store off the name because we can't retrieve anything from the first column
        char originalName[128];

        if ( componentList[i]->Is( FBModel::TypeInfo ) )
        {
            HFBModel pModel = dynamic_cast<HFBModel>( componentList[i] );
			safecpy( originalName, (const char*)pModel->LongName, sizeof(originalName) );
        }
        else
        {
			safecpy( originalName, (const char*)componentList[i]->Name, sizeof(originalName) );
        }

        m_cComponentNameList.Grow() = ConstString( originalName );

        // figure out the display name
        char name[128];
        safecpy( name, originalName, sizeof(name) );
        char* pToken = strtok( name, ":" );
        pToken = strtok( pToken, "." );

        m_spreadsheet.RowAdd( pToken, i );

        m_spreadsheet.SetCell( i, TYPE_SPREADSHEET_COLUMN, m_cTypeNameMenu );
        m_spreadsheet.SetCell( i, TYPE_SPREADSHEET_COLUMN, DetermineTypeIndex( componentList[i] ) );

        const bool *pAdd = NULL;
        pAdd = m_cComponentCheckedMap.Access( m_cComponentNameList[m_cComponentNameList.GetCount() - 1] );
        if ( pAdd )
        {
            m_spreadsheet.SetCell( i, ADD_SPREADSHEET_COLUMN, (*pAdd == false) ? 0 : 1 );
        }
        else
        {
            m_spreadsheet.SetCell( i, ADD_SPREADSHEET_COLUMN, 1 );
        }
    }

    EnableOKButton();
}

void AddComponentsPopup::ClearSpreadsheet()
{
    m_spreadsheet.Clear();

    m_spreadsheet.GetColumn( NAME_SPREADSHEET_COLUMN ).Width = 125;

    m_spreadsheet.ColumnAdd( "Type" );
    m_spreadsheet.GetColumn( TYPE_SPREADSHEET_COLUMN ).ReadOnly = false;
    m_spreadsheet.GetColumn( TYPE_SPREADSHEET_COLUMN ).Style = kFBCellStyleMenu;
    m_spreadsheet.GetColumn( TYPE_SPREADSHEET_COLUMN ).Width = 125;

    m_spreadsheet.ColumnAdd( "Add" );
    m_spreadsheet.GetColumn( ADD_SPREADSHEET_COLUMN ).ReadOnly = false;
    m_spreadsheet.GetColumn( ADD_SPREADSHEET_COLUMN ).Style = kFBCellStyle2StatesButton;
    m_spreadsheet.GetColumn( ADD_SPREADSHEET_COLUMN ).Width = 50;
}

void AddComponentsPopup::EnableOKButton()
{
    int iNumCanAdd = 0;

    for ( int i = 0; i < m_cComponentNameList.GetCount(); ++i )
    {
        int iAdd = 0;
        m_spreadsheet.GetCell( i, ADD_SPREADSHEET_COLUMN, iAdd );
        if ( iAdd == 0 )
        {
            continue;
        }

        int iTypeIndex = -1;
        m_spreadsheet.GetCell( i, TYPE_SPREADSHEET_COLUMN, iTypeIndex );
        if ( iTypeIndex == -1 )
        {
            continue;
        }

        ++iNumCanAdd;
    }

    m_okButton.Enabled = iNumCanAdd > 0;
}

int AddComponentsPopup::DetermineTypeIndex( HFBComponent pComponent ) const
{
    if ( pComponent == NULL )
    {
        return -1;
    }

    char name[128];

    if ( pComponent->Is( FBModel::TypeInfo ) )
    {
        HFBModel pModel = dynamic_cast<HFBModel>( pComponent );
		safecpy( name, (const char*)pModel->LongName, sizeof(name) );
    }
    else
    {
		safecpy( name, (const char*)pComponent->Name, sizeof(name) );
    }

    for ( int i = 0; i < m_pTemplateObjectList.GetCount(); ++i )
    {
        if ( m_pTemplateObjectList[i]->IsThisType( name ) )
        {
            return i;
        }
    }

	ECutsceneObjectType objectType = CUTSCENE_NUM_OBJECT_TYPES;
    if ( pComponent->Is( FBModel::TypeInfo ) )
    {
        return DetermineTypeIndex( dynamic_cast<HFBModel>( pComponent )->Parent );
    }
	else if ( pComponent->Is( FBLight::TypeInfo ) )
	{
		objectType = CUTSCENE_LIGHT_OBJECT_TYPE;
	}
	else if ( pComponent->Is( FBCamera::TypeInfo ) )
	{
		objectType = CUTSCENE_CAMERA_OBJECT_TYPE;
	}
    else if ( pComponent->Is( FBAudioClip::TypeInfo ) )
    {
		objectType = CUTSCENE_AUDIO_OBJECT_TYPE;
    }

	if ( objectType != CUTSCENE_NUM_OBJECT_TYPES)
	{
		for ( int i = 0; i < m_pTemplateObjectList.GetCount(); ++i )
		{
			if ( m_pTemplateObjectList[i]->GetType() == objectType )
			{
				return i;
			}
		}
	}

    return -1;
}

void AddComponentsPopup::RememberAddState()
{
    for ( int i = 0; i < m_cComponentNameList.GetCount(); ++i )
    {
        int iAdd = 0;
        m_spreadsheet.GetCell( i, ADD_SPREADSHEET_COLUMN, iAdd );

        m_cComponentCheckedMap[m_cComponentNameList[i]] = (iAdd == 0) ? false : true;
    }
}

void AddComponentsPopup::Spreadsheet_CellChange( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    EnableOKButton();   
}

void AddComponentsPopup::OtherButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    m_bAddNonComponent = true;

    RememberAddState();

    Close( true );
}

void AddComponentsPopup::OkButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    // look for default spec and compress files
    atArray<atString> specFilenames;
    atArray<atString> compressFilenames;
    rexMBAnimExportCommon::FindAnimCtrlFilePathFiles( specFilenames );
	rexMBAnimExportCommon::FindAnimCompressionFilePathFiles( compressFilenames );

    m_pObjectList.Reset();

    atArray<ConstString> invalidComponentNames;

    for ( int i = 0; i < m_cComponentNameList.GetCount(); ++i )
    {
        int iAdd = 0;
        m_spreadsheet.GetCell( i, ADD_SPREADSHEET_COLUMN, iAdd );
        if ( iAdd == 0 )
        {
            continue;
        }

        int iTypeIndex = -1;
        m_spreadsheet.GetCell( i, TYPE_SPREADSHEET_COLUMN, iTypeIndex );
        if ( iTypeIndex == -1 )
        {
            continue;
        }

        if ( strlen( m_cComponentNameList[i].c_str() ) >= CUTSCENE_OBJNAMELEN )
        {
            invalidComponentNames.Grow() = m_cComponentNameList[i];
            continue;
        }

        cutfObject *pObject = m_pTemplateObjectList[iTypeIndex]->Clone();
        if ( pObject != NULL )
		{
			cutfNamedObject *pNamedObject = dynamic_cast<cutfNamedObject *>( pObject );
			if ( pNamedObject != NULL )
			{
				pNamedObject->SetName( m_cComponentNameList[i].c_str() );
			}

            cutfFinalNamedObject *pFinalNamedObject = dynamic_cast<cutfFinalNamedObject *>( pObject );
            if ( pFinalNamedObject != NULL )
            {
                pFinalNamedObject->SetName( m_cComponentNameList[i].c_str() );
            }

            cutfModelObject *pModelObject = dynamic_cast<cutfModelObject *>( pObject );
            if ( pModelObject != NULL )
            {
                // try to find the default spec file
                atString defaultSpecFilename(pModelObject->GetDefaultAnimExportCtrlSpecFile().GetCStr());
                defaultSpecFilename.Lowercase();

                for ( int j = 0; j < specFilenames.GetCount(); ++j )
                {
                    atString specFilename(specFilenames[j]);
                    specFilename.Lowercase();

                    if ( specFilename == defaultSpecFilename )
                    {
                        pModelObject->SetAnimExportCtrlSpecFile( specFilenames[j].c_str() );
                        break;
                    }
                }

                // try to find the default compress file
                atString defaultCompressFilename(pModelObject->GetDefaultAnimCompression().GetCStr());
                defaultCompressFilename.Lowercase();

                for ( int j = 0; j < compressFilenames.GetCount(); ++j )
                {
                    atString compressFilename(compressFilenames[j]);
                    compressFilename.Lowercase();

                    if ( compressFilename == defaultCompressFilename )
                    {
                        pModelObject->SetAnimCompressionFile( compressFilenames[j] );
                        break;
                    }
                }
            }
                
            m_pObjectList.Grow() = pObject;
        }
    }

    if ( invalidComponentNames.GetCount() > 0 )
    {
        char cMsg[1024];
        sprintf( cMsg, "Names must be less than %d characters in length.  Please shorten the names of the following component(s) and try again:", 
            CUTSCENE_OBJNAMELEN );
        for ( int i = 0; i < invalidComponentNames.GetCount(); ++i )
        {
            safecat( cMsg, "\n", 1024 );
            safecat( cMsg, invalidComponentNames[i].c_str(), 1024 );
        }

        FBMessageBox( "Invalid Name Length(s)", cMsg, "OK" );
    }

    RememberAddState();

    Close( true );
}

void AddComponentsPopup::CancelButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    m_pObjectList.Reset();

    RememberAddState();

    Close( false );
}

//##############################################################################

AddNonComponentPopup::AddNonComponentPopup()
: m_pObject(NULL)
{
    UICreate();
    UIConfigure();
}

AddNonComponentPopup::~AddNonComponentPopup()
{
    for ( int i = 0; i < m_pTemplateObjectList.GetCount(); ++i )
    {
        delete m_pTemplateObjectList[i];
    }

    m_pTemplateObjectList.Reset();
}

void AddNonComponentPopup::UICreate()
{
    /* Here's a text-based representation of the Add Non-Models Popup.
    {} = combo boxes (surrounds the name)
    () = button (surrounds the name)
    [ ] = check box
    _ = text box (spans the width of the text)
    |- = spread sheet borders and columns

    Type: {                                         }

    Name: ___________________________________________

                                (   OK   ) ( Cancel )
    */

    Region.X = 200;
    Region.Y = 200;
    Region.Width = 400;
    Region.Height = (2 * rexMBAnimExportCommon::c_iMargin) + (rexMBAnimExportCommon::c_iRowHeight * 3);
    
    int iAttachTopY = rexMBAnimExportCommon::c_iMargin;

    // Type
    AddRegion( "ObjectTypeLabel", "ObjectTypeLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ObjectTypeLabel", m_objectTypeLabel );

    AddRegion( "ObjectTypeComboBox", "ObjectTypeComboBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ObjectTypeLabel",	1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ObjectTypeComboBox", m_objectTypeComboBox );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Name
    AddRegion( "NameLabel", "NameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "NameLabel", m_nameLabel );

    AddRegion( "NameTextBox", "NameTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "NameLabel", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "NameTextBox", m_nameTextBox );

    int iAttachBottomY = -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight;

    // Cancel
    AddRegion( "CancelButton", "CancelButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "CancelButton", m_cancelButton );

    // Ok
    AddRegion( "OkButton", "OkButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachLeft, "CancelButton", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OkButton", m_okButton );
}

void AddNonComponentPopup::UIConfigure()
{
    Caption = "Add Item";

    // Type
    m_objectTypeLabel.Caption = "Type:";
    m_objectTypeLabel.Justify = kFBTextJustifyRight;

    m_objectTypeComboBox.ExtendedSelect = true;
    m_objectTypeComboBox.OnChange.Add( this, (FBCallback)&AddNonComponentPopup::ObjectTypeComboBox_Change );
    m_objectTypeComboBox.Style = kFBDropDownList;    

    // Name
    m_nameLabel.Caption = "Name:";
    m_nameLabel.Justify = kFBTextJustifyRight;
    m_nameLabel.Enabled = false;

    m_nameTextBox.Enabled = false;
    m_nameTextBox.OnChange.Add( this, (FBCallback)&AddNonComponentPopup::NameTextBox_Change );

    // Ok
    m_okButton.Caption = "OK";
    m_okButton.OnClick.Add( this, (FBCallback)&AddNonComponentPopup::OkButton_Click );
    m_okButton.Enabled = false;

    // Cancel
    m_cancelButton.Caption = "Cancel";
    m_cancelButton.OnClick.Add( this, (FBCallback)&AddNonComponentPopup::CancelButton_Click );
}

void AddNonComponentPopup::SetTemplateObjectList( const atArray<cutfObject *>& pObjectList )
{
    m_objectTypeComboBox.Items.Clear();
    
    m_pTemplateObjectList.Reserve( pObjectList.GetCount() );
    for ( int i = 0; i < pObjectList.GetCount(); ++i )
    {
        m_pTemplateObjectList.Append() = pObjectList[i]->Clone();
        
        m_objectTypeComboBox.Items.Add( const_cast<char *>( pObjectList[i]->GetTypeName() ), i );
    }

    ObjectTypeComboBox_Change( NULL, NULL );
}

void AddNonComponentPopup::ObjectTypeComboBox_Change( HISender pSender, HKEvent pEvent )
{
    int iTypeIndex = m_objectTypeComboBox.ItemIndex;
    if ( iTypeIndex != -1 )
    {
        cutfFinalNamedObject *pNamedObject = dynamic_cast<cutfFinalNamedObject *>( m_pTemplateObjectList[iTypeIndex] );
        m_nameLabel.Enabled = pNamedObject != NULL;
        m_nameTextBox.Enabled = pNamedObject != NULL;
    }
    else
    {
        m_nameLabel.Enabled = false;
        m_nameTextBox.Enabled = false;
    }

    NameTextBox_Change( pSender, pEvent );
}

void AddNonComponentPopup::NameTextBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( m_nameTextBox.Enabled )
    {
        char name[CUTSCENE_OBJNAMELEN];
        safecpy( name, m_nameTextBox.Text.AsString(), sizeof(name) );
        m_okButton.Enabled = strlen( name ) > 0;
    }
    else
    {
        m_okButton.Enabled = true;
    }
}

void AddNonComponentPopup::OkButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    int iTypeIndex = m_objectTypeComboBox.ItemIndex;
    if ( iTypeIndex != -1 )
    {
        m_pObject = m_pTemplateObjectList[iTypeIndex]->Clone();

        cutfFinalNamedObject *pNamedObject = dynamic_cast<cutfFinalNamedObject *>( m_pObject );
        if ( pNamedObject != NULL )
        {
            pNamedObject->SetName( m_nameTextBox.Text );
        }
    }

    Close( true );
}

void AddNonComponentPopup::CancelButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    m_pObject = NULL;

    Close( false );
}

//###################################

AddPartPopup::AddPartPopup()
{
    UICreate();
    UIConfigure();
}

AddPartPopup::~AddPartPopup()
{
}

void AddPartPopup::UICreate()
{
    /* Here's a text-based representation of the Add Non-Models Popup.
    {} = combo boxes (surrounds the name)
    () = button (surrounds the name)
    [ ] = check box
    _ = text box (spans the width of the text)
    |- = spread sheet borders and columns

    Type: {                                         }

    Name: ___________________________________________

                                (   OK   ) ( Cancel )
    */

    Region.X = 200;
    Region.Y = 200;
    Region.Width = 400;
    Region.Height = (2 * rexMBAnimExportCommon::c_iMargin) + (rexMBAnimExportCommon::c_iRowHeight * 3);
    
    int iAttachTopY = rexMBAnimExportCommon::c_iMargin;

    // Type
    AddRegion( "ObjectTypeLabel", "ObjectTypeLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ObjectTypeLabel", m_objectTypeLabel );

    AddRegion( "ObjectTypeComboBox", "ObjectTypeComboBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "ObjectTypeLabel",	1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "ObjectTypeComboBox", m_objectTypeComboBox );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Name
    AddRegion( "NameLabel", "NameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "NameLabel", m_nameLabel );

    AddRegion( "NameTextBox", "NameTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "NameLabel", 1.0,
        iAttachTopY, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "NameTextBox", m_nameTextBox );

    int iAttachBottomY = -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight;

    // Cancel
    AddRegion( "CancelButton", "CancelButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "CancelButton", m_cancelButton );

    // Ok
    AddRegion( "OkButton", "OkButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachLeft, "CancelButton", 1.0,
        iAttachBottomY, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OkButton", m_okButton );
}

void AddPartPopup::UIConfigure()
{
    Caption = "Add Part";

    // Type
    m_objectTypeLabel.Caption = "Type:";
    m_objectTypeLabel.Justify = kFBTextJustifyRight;

    m_objectTypeComboBox.ExtendedSelect = true;
    m_objectTypeComboBox.OnChange.Add( this, (FBCallback)&AddPartPopup::ObjectTypeComboBox_Change );
    m_objectTypeComboBox.Style = kFBDropDownList;    

    // Name
    m_nameLabel.Caption = "Name:";
    m_nameLabel.Justify = kFBTextJustifyRight;
    m_nameLabel.Enabled = true;

    m_nameTextBox.Enabled = true;
    m_nameTextBox.OnChange.Add( this, (FBCallback)&AddPartPopup::NameTextBox_Change );

    // Ok
    m_okButton.Caption = "OK";
    m_okButton.OnClick.Add( this, (FBCallback)&AddPartPopup::OkButton_Click );
    m_okButton.Enabled = false;

    // Cancel
    m_cancelButton.Caption = "Cancel";
    m_cancelButton.OnClick.Add( this, (FBCallback)&AddPartPopup::CancelButton_Click );
}

void AddPartPopup::SetTemplateObjectList(const atArray<atString>& pObjectList )
{
	m_objectTypeComboBox.Items.Clear();

	for ( int i = 0; i < pObjectList.GetCount(); ++i )
	{
		m_objectTypeComboBox.Items.Add( (char*)pObjectList[i].c_str(), i );
	}
}

void AddPartPopup::ObjectTypeComboBox_Change( HISender pSender, HKEvent pEvent )
{
    int iTypeIndex = m_objectTypeComboBox.ItemIndex;
    if ( iTypeIndex != -1)
    {
        m_nameLabel.Enabled = true;
        m_nameTextBox.Enabled = true;
    }
    else
    {
        m_nameLabel.Enabled = false;
        m_nameTextBox.Enabled = false;
    }

    NameTextBox_Change( pSender, pEvent );
}

void AddPartPopup::NameTextBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( m_nameTextBox.Enabled )
    {
        char name[CUTSCENE_OBJNAMELEN];
        safecpy( name, m_nameTextBox.Text.AsString(), sizeof(name) );
        m_okButton.Enabled = strlen( name ) > 0;
    }
    else
    {
        m_okButton.Enabled = true;
    }
}

void AddPartPopup::OkButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    Close( true );
}

void AddPartPopup::CancelButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	Close( false );
}

} // namespace rage
