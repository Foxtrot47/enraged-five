// 
// rexMbRage/eventpopup.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "eventpopup.h"

#include "atl/array.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfeventdef.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfobject.h"
#include "file/asset.h"
#include "file/device.h"
#include "rexMBAnimExportCommon/utility.h"

// Copied from base/src/text/language.cpp
static const char *c_languageNames[]=
{
    "English",
    "Spanish",
    "French",
    "German",
    "Italian",
    "Portugese",
    "Japanese",
    "Chinese-traditional",
    "Chinese-simplified",
    "Korean",
    "Norwegian"
};

static const int c_numLanguageNames = 11;

namespace rage {

//##############################################################################

EditEventPopup::EditEventPopup( cutfCutsceneFile2 *pCutFile, const cutfEventDefs *pEventDefs )
: m_pCutFile(pCutFile)
, m_pEventDefs(pEventDefs)
, m_eventEditMode(NO_EVENT_MODE)
, m_eventArgsEditMode(NO_EVENT_ARGS_MODE)
, m_lastEventArgsEditMode(NO_EVENT_ARGS_MODE)
, m_pCurrentEventData(NULL)
, m_pCurrentEventArgsData(NULL)
, m_pCurrentAttribute(NULL)
, m_iEventSpreadsheetRowRef(0)
, m_iEventArgsSpreadsheetRowRef(0)
, m_iAttributeSpreadsheetRowRef(0)
, m_iRestrictedFromEditingAttributeCount(0)
, m_iRestrictedFromRemovalAttributeCount(0)
, m_bModifiedCutfile(false)
, m_bHhaveUnsavedEventChanges(false)
, m_bHaveUnsavedEventArgsChanges(false)
, m_bHaveUnsavedAttributeChanges(false)
, m_bDisplayAll(true)
, m_bUserModifiedDisplayAll(false)
{
    CreateBuiltInEventDefs();

    UICreate();
    UIConfigure();

    RefreshUI();
}

EditEventPopup::~EditEventPopup()
{
    Shutdown();
}

void EditEventPopup::RefreshUI()
{
    Clear();

    const atArray<cutfObject *> &objectList = m_pCutFile->GetObjectList();
    for ( int i = 0; i < objectList.GetCount(); ++i )
    {
        m_eventRecipientComboBox.Items.Add( const_cast<char *>( objectList[i]->GetDisplayName().c_str() ) );
    }

    const atArray<cutfEvent *> &loadEventList = m_pCutFile->GetLoadEventList();
    const atArray<cutfEvent *> &eventList = m_pCutFile->GetEventList();
    
    const atArray<cutfEventArgs *> &eventArgsList = m_pCutFile->GetEventArgsList();
    for ( int i = 0; i < eventArgsList.GetCount(); ++i )
    {
        const cutfEventArgsDef *pEventArgsDef = NULL;
        if ( eventArgsList[i]->GetRef() > 0 )
        {
            // find an event that uses the event args.  find its event args def if it is attached to a custom event
            for ( int j = 0; j < loadEventList.GetCount(); ++j )
            {
                if ( (loadEventList[j]->GetEventArgs() == eventArgsList[i]) 
                    && (loadEventList[j]->GetEventId() >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) )
                {
                    int iEventOffset = loadEventList[j]->GetEventId() - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
                    pEventArgsDef = m_pEventDefs->GetEventDef( iEventOffset )->GetEventArgsDef();
                    break;
                }
            }

            if ( pEventArgsDef == NULL )
            {
                for ( int j = 0; j < eventList.GetCount(); ++j )
                {
                    if ( (eventList[j]->GetEventArgs() == eventArgsList[i]) 
                        && (eventList[j]->GetEventId() >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) )
                    {
                        int iEventOffset = eventList[j]->GetEventId() - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
                        pEventArgsDef = m_pEventDefs->GetEventDef( iEventOffset )->GetEventArgsDef();
                        break;
                    }
                }
            }
        }

        if ( !m_bDisplayAll && (pEventArgsDef == NULL) )
        {
            continue;
        }

        SEditEventArgsData *pEventArgsData = CreateEventArgsData( eventArgsList[i], pEventArgsDef );
        if ( pEventArgsData != NULL )
        {
            AddRowToEventArgsSpreadsheet( pEventArgsData );

            if ( eventArgsList[i]->GetRef() == 0 )
            {
                m_cleanupEventArgsButton.Enabled = true;
            }
        }
    }

    for ( int i = 0; i < loadEventList.GetCount(); ++i )
    {
        if ( !m_bDisplayAll && (loadEventList[i]->GetEventId() < CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) )
        {
            continue;
        }

        SEditEventData *pEventData = CreateEventData( loadEventList[i] );
        if ( pEventData != NULL )
        {
            pEventData->bIsLoadEvent = true;

            AddRowToEventSpreadsheet( pEventData );
        }
    }

    for ( int i = 0; i < eventList.GetCount(); ++i )
    {
        if ( !m_bDisplayAll && (eventList[i]->GetEventId() < CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE) )
        {
            continue;
        }

        SEditEventData *pEventData = CreateEventData( eventList[i] );
        if ( pEventData != NULL )
        {
            pEventData->bIsLoadEvent = false;

            AddRowToEventSpreadsheet( pEventData );
        }
    }
}

void EditEventPopup::Shutdown()
{
    Clear();

    atMap<s32, const cutfEventDef *>::Iterator eventDefEntry = m_builtInEventDefsMap.CreateIterator();
    for ( eventDefEntry.Start(); !eventDefEntry.AtEnd(); eventDefEntry.Next() )
    {
        delete eventDefEntry.GetData();
    }

    m_builtInEventDefsMap.Kill();

    atMap<s32, const cutfEventArgsDef *>::Iterator eventArgsDefEntry = m_builtInEventArgsDefsMap.CreateIterator();
    for ( eventArgsDefEntry.Start(); !eventArgsDefEntry.AtEnd(); eventArgsDefEntry.Next() )
    {
        delete eventArgsDefEntry.GetData();
    }

    m_builtInEventArgsDefsMap.Kill();
}

void EditEventPopup::Clear()
{
    ClearEventSpreadsheet();
    ClearAttributeSpreadsheet();
    ClearEventArgsSpreadsheet();

    SetEventEditMode( NO_EVENT_MODE );
    SetEventArgsEditMode( NO_EVENT_ARGS_MODE );
    m_lastEventArgsEditMode = NO_EVENT_ARGS_MODE;

    m_eventRecipientComboBox.Items.Clear();
    m_eventEventArgsComboBox.Items.Clear();
    m_eventArgsDataComboBoxList.Reset();    

    m_bModifiedCutfile = false;
    SetHaveUnsavedEventChanges( false );
    SetHaveUnsavedEventArgsChanges( false );
    SetHaveUnsavedAttributeChanges( false );
}

void EditEventPopup::UICreate()
{
    /* Here's a text-based representation of the Edit Events Popup.
    {} = combo boxes (surrounds the name)
    () = button (surrounds the name)
    [ ] = check box
    _ = text box (spans the width of the text)
    |- = spread sheet borders and columns
    # = splitter

    ( Add Event ) ( Edit Event ) ( Remove Event )              #       Name: ________________________________________
    ---------------------------------------------------------- # Load Event: [ ]
    |---Event---||---Type---|-Recipient-|-Frame-|-Event Args-| #  Recipient: {                                      }
    |           ||          |           |       |            | #      Frame: ________________________________________
    |           ||          |           |       |            | # Event Args: {                                      }
    |           ||          |           |       |            | #
    ---------------------------------------------------------- #                                ( Save Changes ) ( Discard Changes )
    ########################################################################################################################################
    ( Add Event Args ) ( Edit Event Args ) ( Remove Event Args ) #     Name: ________________________________________
    ------------------------------------------------------------ # --------------------------------------------------
    |---Event Args---||-Reference Count-|----------------------| # |-----------||---Type---|--Value------|----------| (    Edit    )
    |                ||                 |                      | # |           ||          |             |          | (     Add    )
    |                ||                 |                      | # |           ||          |             |          | (   Remove   )
    |                ||                 |                      | # |           ||          |             |          | ( Remove All )
    |                ||                 |                      | # |--------------------------------------------------
    |                ||                 |                      | #     Name: ___________________________________________ 
    |                ||                 |                      | #     Type: {                                         } (    Done    )
    |                ||                 |                      | #    Value: ___________________________________________ ( Cancel Edit)
    |                ||                 |                      | # 
    ------------------------------------------------------------ #                                  ( Save Changes ) ( Discard Changes )
    

    (   Help   )                                                                                                            ( Close )
    */

    Caption = "Scene Events";

    Region.X = 100;
    Region.Y = 100;
    Region.Width = 1200;
    Region.Height = 900;

    int iBottomRegionHeight = -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iRowHeight - rexMBAnimExportCommon::c_iMargin;
    int iEventRegionHeight = (Region.Height - iBottomRegionHeight) / 2;

    // EventsLayout
    AddRegion( "EventsLayout", "EventsLayout", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
        iEventRegionHeight, kFBAttachNone, NULL, 1.0f );    
    SetControl( "EventsLayout", m_eventsLayout );
   
    UICreateEventLayout();

    // EventArgsLayout
    AddRegion( "EventArgsLayout", "EventArgsLayout", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iMargin * 2, kFBAttachBottom, "EventsLayout", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight - (rexMBAnimExportCommon::c_iMargin * 2), kFBAttachBottom, NULL, 1.0f );
    SetControl( "EventArgsLayout", m_eventArgsLayout );

    UICreateEventArgsLayout();

    // Done
    AddRegion( "CloseButton", "CloseButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "CloseButton", m_closeButton );
}

void EditEventPopup::UICreateEventLayout()
{
    // EventListLayout
    m_eventsLayout.AddRegion( "EventListLayout", "EventListLayout", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0f,
        (Region.Width / 3) * 2, kFBAttachNone, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0f );
    m_eventsLayout.SetControl( "EventListLayout", m_eventListLayout );

    // EventEditLayout
    m_eventsLayout.AddRegion( "EventEditLayout", "EventEditLayout", 
        rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, "EventListLayout", 1.0f,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0f );
    m_eventsLayout.SetControl( "EventEditLayout", m_eventEditLayout );

    UICreateEventListLayout();
    UICreateEventEditLayout();
}

void EditEventPopup::UICreateEventListLayout()
{
    int iAttachTopY = rexMBAnimExportCommon::c_iMargin;

    // Add
    m_eventListLayout.AddRegion( "AddEventButton", "AddEventButton", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventListLayout.SetControl( "AddEventButton", m_addEventButton );

    // Edit
    m_eventListLayout.AddRegion( "EditEventButton", "EditEventButton", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "AddEventButton", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventListLayout.SetControl( "EditEventButton", m_editEventButton );

    // Remove
    m_eventListLayout.AddRegion( "RemoveEventButton", "RemoveEventButton", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EditEventButton", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventListLayout.SetControl( "RemoveEventButton", m_removeEventButton );

    // Sort
    m_eventListLayout.AddRegion( "SortEventsButton", "SortEventsButton", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "DisplayAllCheckBox", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventListLayout.SetControl( "SortEventsButton", m_sortEventsButton );
    
    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Event Spreadsheet
    m_eventListLayout.AddRegion( "EventSpreadsheet", "EventSpreadsheet",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0f );
    m_eventListLayout.SetControl( "EventSpreadsheet", m_eventSpreadsheet );
}

void EditEventPopup::UICreateEventEditLayout()
{
    // EventEditLayoutRegion
    m_eventEditLayout.AddRegion( "EventEditLayoutRegion", "EventEditLayoutRegion",
        rexMBAnimExportCommon::c_iMargin * 2, kFBAttachLeft, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iMargin * 3, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin * 2, kFBAttachBottom, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventEditLayoutRegion", m_eventEditLayoutRegion );

    int iAttachTopY = rexMBAnimExportCommon::c_iMargin * 2;

    // Name
    m_eventEditLayout.AddRegion( "EventNameLabel", "EventNameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventEditLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventNameLabel", m_eventNameLabel );

    m_eventEditLayout.AddRegion( "EventNameTextBox", "EventNameTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventNameLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventNameTextBox", m_eventNameTextBox );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Type
    m_eventEditLayout.AddRegion( "EventTypeLabel", "EventTypeLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventEditLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventTypeLabel", m_eventTypeLabel );

    m_eventEditLayout.AddRegion( "EventTypeCheckBox", "EventTypeCheckBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventTypeLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventTypeCheckBox", m_eventTypeCheckBox );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Recipient
    m_eventEditLayout.AddRegion( "EventRecipientLabel", "EventRecipientLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventEditLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventRecipientLabel", m_eventRecipientLabel );

    m_eventEditLayout.AddRegion( "EventRecipientComboBox", "EventRecipientComboBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventRecipientLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventRecipientComboBox", m_eventRecipientComboBox );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Frame
    m_eventEditLayout.AddRegion( "EventFrameLabel", "EventFrameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventEditLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventFrameLabel", m_eventFrameLabel );

    m_eventEditLayout.AddRegion( "EventFrameNumericUpDown", "EventFrameNumericUpDown",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventFrameLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventFrameNumericUpDown", m_eventFrameNumericUpDown );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Event Args
    m_eventEditLayout.AddRegion( "EventEventArgsLabel", "EventEventArgsLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventEditLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventEventArgsLabel", m_eventEventArgsLabel );

    m_eventEditLayout.AddRegion( "EventEventArgsComboBox", "EventEventArgsComboBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventEventArgsLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "EventEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "EventEventArgsComboBox", m_eventEventArgsComboBox );

    iAttachTopY += (rexMBAnimExportCommon::c_iRowHeight * 2);

    // Save Changes
    m_eventEditLayout.AddRegion( "SaveEventChangesButton", "SaveEventChangesButton",
        -(rexMBAnimExportCommon::c_iButtonWidth * 2) - rexMBAnimExportCommon::c_iMargin - (rexMBAnimExportCommon::c_iButtonWidth * 2) - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth * 2, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "SaveEventChangesButton", m_saveEventChangesButton );
    
    // Discard Changes
    m_eventEditLayout.AddRegion( "DiscardEventChangesButton", "DiscardEventChangesButton",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SaveEventChangesButton", 1.0f,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "EventEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth * 2, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventEditLayout.SetControl( "DiscardEventChangesButton", m_discardEventChangesButton );
}

void EditEventPopup::UICreateEventArgsLayout()
{
    // EventArgsListLayout
    m_eventArgsLayout.AddRegion( "EventArgsListLayout", "EventArgsListLayout", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0f,
        Region.Width / 3, kFBAttachNone, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0f );
    m_eventArgsLayout.SetControl( "EventArgsListLayout", m_eventArgsListLayout );

    // EventArgsEditLayout
    m_eventArgsLayout.AddRegion( "EventArgsEditLayout", "EventArgsEditLayout", 
        rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, "EventArgsListLayout", 1.0f,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0f );
    m_eventArgsLayout.SetControl( "EventArgsEditLayout", m_eventArgsEditLayout );

    UICreateEventArgsListLayout();
    UICreateEventArgsEditLayout();
}

void EditEventPopup::UICreateEventArgsListLayout()
{
    int iAttachTopY = rexMBAnimExportCommon::c_iMargin;

    // Add
    m_eventArgsListLayout.AddRegion( "AddEventArgsButton", "AddEventArgsButton", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventArgsListLayout.SetControl( "AddEventArgsButton", m_addEventArgsButton );

    // Edit
    m_eventArgsListLayout.AddRegion( "EditEventArgsButton", "EditEventArgsButton", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "AddEventArgsButton", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventArgsListLayout.SetControl( "EditEventArgsButton", m_editEventArgsButton );

    // Remove
    m_eventArgsListLayout.AddRegion( "RemoveEventArgsButton", "RemoveEventArgsButton", 
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EditEventArgsButton", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
	m_eventArgsListLayout.SetControl( "RemoveEventArgsButton", m_removeEventArgsButton );

	// Cleanup
	m_eventArgsListLayout.AddRegion( "CleanupEventArgsButton", "CleanupEventArgsButton", 
		rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "RemoveEventArgsButton", 1.0f,
		iAttachTopY, kFBAttachTop, "NULL", 1.0f,
		rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0f,
		rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
	m_eventArgsListLayout.SetControl( "CleanupEventArgsButton", m_cleanupEventArgsButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Event Args Spreadsheet
    m_eventArgsListLayout.AddRegion( "EventArgsSpreadsheet", "EventArgsSpreadsheet",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0f,
        iAttachTopY, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachBottom, NULL, 1.0f );
    m_eventArgsListLayout.SetControl( "EventArgsSpreadsheet", m_eventArgsSpreadsheet );
}

void EditEventPopup::UICreateEventArgsEditLayout()
{
    int iAttachTopY = rexMBAnimExportCommon::c_iMargin * 2;
    int iAttributeLayoutRegionHeight = (rexMBAnimExportCommon::c_iRowHeight * 3) + (rexMBAnimExportCommon::c_iMargin * 3);
    int iAttachBottomY = -rexMBAnimExportCommon::c_iMargin - iAttributeLayoutRegionHeight - (rexMBAnimExportCommon::c_iRowHeight * 2);

    // EventArgsEditLayoutRegion
    m_eventArgsEditLayout.AddRegion( "EventArgsEditLayoutRegion", "EventArgsEditLayoutRegion",
        rexMBAnimExportCommon::c_iMargin * 2, kFBAttachLeft, "NULL", 1.0f,
        rexMBAnimExportCommon::c_iMargin * 3, kFBAttachTop, "NULL", 1.0f,
        -rexMBAnimExportCommon::c_iMargin * 2, kFBAttachRight, NULL, 1.0f,
        -rexMBAnimExportCommon::c_iMargin * 2, kFBAttachBottom, NULL, 1.0f );
    m_eventArgsEditLayout.SetControl( "EventArgsEditLayoutRegion", m_eventArgsEditLayoutRegion );

    // EventArgsAttributeEditLayoutRegion
    m_eventArgsEditLayout.AddRegion( "EventArgsAttributeEditLayoutRegion", "EventArgsAttributeEditLayoutRegion",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventArgsEditLayoutRegion", 1.0f,
        iAttachBottomY, kFBAttachBottom, "EventArgsEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsEditLayoutRegion", 1.0f,
        iAttributeLayoutRegionHeight, kFBAttachNone, NULL, 1.0f );
    m_eventArgsEditLayout.SetControl( "EventArgsAttributeEditLayoutRegion", m_eventArgsAttributeEditLayoutRegion );

    // Name
    m_eventArgsEditLayout.AddRegion( "EventArgsNameLabel", "EventArgsNameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventArgsEditLayoutRegion", 1.0f,
        iAttachTopY, kFBAttachTop, "EventArgsEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventArgsEditLayout.SetControl( "EventArgsNameLabel", m_eventArgsNameLabel );

    m_eventArgsEditLayout.AddRegion( "EventArgsNameTextBox", "EventArgsNameTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsNameLabel", 1.0f,
        iAttachTopY, kFBAttachTop, "EventArgsEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventArgsEditLayout.SetControl( "EventArgsNameTextBox", m_eventArgsNameTextBox );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Spreadsheet
    m_eventArgsEditLayout.AddRegion( "AttributeSpreadsheet", "AttributeSpreadsheet",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventArgsEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsEditLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsEditLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iRowHeight, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0 );
    m_eventArgsEditLayout.SetControl( "AttributeSpreadsheet", m_attributeSpreadsheet );

    iAttachBottomY += rexMBAnimExportCommon::c_iRowHeight;

    // Edit
    m_eventArgsEditLayout.AddRegion( "EditAttributeButton", "EditAttributeButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "EventArgsEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "EditAttributeButton", m_editAttributeButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Add
    m_eventArgsEditLayout.AddRegion( "AddAttributeButton", "AddAttributeButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "EventArgsEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "AddAttributeButton", m_addAttributeButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Remove
    m_eventArgsEditLayout.AddRegion( "RemoveAttributeButton", "RemoveAttributeButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "EventArgsEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "RemoveAttributeButton", m_removeAttributeButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // RemoveAll
    m_eventArgsEditLayout.AddRegion( "RemoveAllAttributesButton", "RemoveAllAttributesButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "EventArgsEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "RemoveAllAttributesButton", m_removeAllAttributesButton );

    iAttachTopY = rexMBAnimExportCommon::c_iMargin * 2;

    // Name
    m_eventArgsEditLayout.AddRegion( "AttributeNameLabel", "AttributeNameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventArgsAttributeEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "AttributeNameLabel", m_attributeNameLabel );

    m_eventArgsEditLayout.AddRegion( "AttributeNameTextBox", "AttributeNameTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "AttributeNameLabel", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsAttributeEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "AttributeNameTextBox", m_attributeNameTextBox );

    // Done
    m_eventArgsEditLayout.AddRegion( "DoneEditAttributeButton", "DoneEditAttributeButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "EventArgsAttributeEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "DoneEditAttributeButton", m_doneEditAttributeButton );
    
    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Type
    m_eventArgsEditLayout.AddRegion( "AttributeTypeLabel", "AttributeTypeLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventArgsAttributeEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "AttributeTypeLabel", m_attributeTypeLabel );

    m_eventArgsEditLayout.AddRegion( "AttributeTypeComboBox", "AttributeTypeComboBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "AttributeTypeLabel", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsAttributeEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "AttributeTypeComboBox", m_attributeTypeComboBox );

    // Cancel Edit
    m_eventArgsEditLayout.AddRegion( "CancelEditAttributeButton", "CancelEditAttributeButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "EventArgsAttributeEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "CancelEditAttributeButton", m_cancelEditAttributeButton );

    iAttachTopY += rexMBAnimExportCommon::c_iRowHeight;

    // Value
    m_eventArgsEditLayout.AddRegion( "AttributeValueLabel", "AttributeValueLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "EventArgsAttributeEditLayoutRegion", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "AttributeValueLabel", m_attributeValueLabel );

    m_eventArgsEditLayout.AddRegion( "AttributeValueTextBox", "AttributeValueTextBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "AttributeValueLabel", 1.0,
        iAttachTopY, kFBAttachTop, "EventArgsAttributeEditLayoutRegion", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsAttributeEditLayoutRegion", 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    m_eventArgsEditLayout.SetControl( "AttributeValueTextBox", m_attributeValueTextBox );

    // Save Changes
    m_eventArgsEditLayout.AddRegion( "SaveEventArgsChangesButton", "SaveEventArgsChangesButton",
        -(rexMBAnimExportCommon::c_iButtonWidth * 2) - rexMBAnimExportCommon::c_iMargin - (rexMBAnimExportCommon::c_iButtonWidth * 2) - rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsEditLayoutRegion", 1.0f,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "EventArgsEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth * 2, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventArgsEditLayout.SetControl( "SaveEventArgsChangesButton", m_saveEventArgsChangesButton );

    // Discard Changes
    m_eventArgsEditLayout.AddRegion( "DiscardEventArgsChangesButton", "DiscardEventArgsChangesButton",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "SaveEventArgsChangesButton", 1.0f,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "EventArgsEditLayoutRegion", 1.0f,
        rexMBAnimExportCommon::c_iButtonWidth * 2, kFBAttachNone, NULL, 1.0f,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0f );
    m_eventArgsEditLayout.SetControl( "DiscardEventArgsChangesButton", m_discardEventArgsChangesButton );
}

void EditEventPopup::UIConfigure()
{
    SetSplitStyle( "EventsLayout", kFBHSplit );

    UIConfigureEventLayout();
    UIConfigureEventArgsLayout();

    m_closeButton.Caption = "Close";
    m_closeButton.OnClick.Add( this, (FBCallback)&EditEventPopup::CloseButton_Click );
}

void EditEventPopup::UIConfigureEventLayout()
{
    m_eventsLayout.SetSplitStyle( "EventListLayout", kFBVSplit );
    
    UIConfigureEventListLayout();
    UIConfigureEventEditLayout();
}

void EditEventPopup::UIConfigureEventListLayout()
{
    // Add
    m_addEventButton.Caption = "Add";
    m_addEventButton.Enabled = true;/*m_pEventDefs->GetEventDefCount() > 0;*/
    m_addEventButton.OnClick.Add( this, (FBCallback)&EditEventPopup::AddEventButton_Click );    

    // Edit
    m_editEventButton.Caption = "Edit";
    m_editEventButton.OnClick.Add( this, (FBCallback)&EditEventPopup::EditEventButton_Click );
    m_editEventButton.Enabled = false;

    // Remove
    m_removeEventButton.Caption = "Remove";
    m_removeEventButton.OnClick.Add( this, (FBCallback)&EditEventPopup::RemoveEventButton_Click );
    m_removeEventButton.Enabled = true;

    // Sort
    m_sortEventsButton.Caption = "Sort";
    m_sortEventsButton.OnClick.Add( this, (FBCallback)&EditEventPopup::SortEventsButton_Click );

    // Event Spreadsheet
    m_eventSpreadsheet.Caption = "Events";
    m_eventSpreadsheet.OnRowClick.Add( this, (FBCallback)&EditEventPopup::EventSpreadsheet_RowClick );

    m_eventSpreadsheet.GetColumn( EVENT_NAME_SPREADSHEET_COLUMN ).Width = 150;

    ClearEventSpreadsheet();
}

void EditEventPopup::UIConfigureEventEditLayout()
{
    // EventEditLayoutRegion
    m_eventEditLayout.SetRegionTitle( "EventEditLayoutRegion", "Event" );
    m_eventEditLayout.SetBorder( "EventEditLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

    // Name
    m_eventNameLabel.Caption = "Name:";
    m_eventNameLabel.Justify = kFBTextJustifyRight;

    m_eventNameTextBox.Caption = "";

    // Type
    m_eventTypeLabel.Caption = "Load Event:";
    m_eventTypeLabel.Justify = kFBTextJustifyRight;

    m_eventTypeCheckBox.OnClick.Add( this, (FBCallback)&EditEventPopup::EventTypeCheckBox_Click );
    m_eventTypeCheckBox.Style = kFBCheckbox;

    // Recipient
    m_eventRecipientLabel.Caption = "Recipient";
    m_eventRecipientLabel.Justify = kFBTextJustifyRight;

    m_eventRecipientComboBox.OnChange.Add( this, (FBCallback)&EditEventPopup::EventRecipientComboBox_Change );
    m_eventRecipientComboBox.Style = kFBDropDownList;

    // Frame
    m_eventFrameLabel.Caption = "Frame:";
    m_eventFrameLabel.Justify = kFBTextJustifyRight;

    m_eventFrameNumericUpDown.Min = 0;
    m_eventFrameNumericUpDown.Max = 999999;
    m_eventFrameNumericUpDown.OnChange.Add( this, (FBCallback)&EditEventPopup::EventFrameNumericUpDown_Change );
    m_eventFrameNumericUpDown.Precision = 0;

	// Event Args
	m_eventEventArgsLabel.Caption = "Event Args:";
	m_eventEventArgsLabel.Justify = kFBTextJustifyRight;

	m_eventEventArgsComboBox.OnChange.Add( this, (FBCallback)&EditEventPopup::EventEventArgsComboBox_Change );
	m_eventEventArgsComboBox.Style = kFBDropDownList;

    // Save Changes
    m_saveEventChangesButton.Caption = "Save Changes";
    m_saveEventChangesButton.OnClick.Add( this, (FBCallback)&EditEventPopup::SaveEventChangesButton_Click );

    // Discard Changes
    m_discardEventChangesButton.Caption = "Discard Changes";
    m_discardEventChangesButton.OnClick.Add( this, (FBCallback)&EditEventPopup::DiscardEventChangesButton_Click );
}

void EditEventPopup::UIConfigureEventArgsLayout()
{
    // EventArgsListLayout
    m_eventArgsLayout.SetSplitStyle( "EventArgsListLayout", kFBVSplit );

    UIConfigureEventArgsListLayout();
    UIConfigureEventArgsEditLayout();
}

void EditEventPopup::UIConfigureEventArgsListLayout()
{
    // Add
    m_addEventArgsButton.Caption = "Add";
    m_addEventArgsButton.Enabled = true/*m_pEventDefs->GetEventArgsDefCount() > 0*/;
    m_addEventArgsButton.OnClick.Add( this, (FBCallback)&EditEventPopup::AddEventArgsButton_Click );

    // Edit
    m_editEventArgsButton.Caption = "Edit";
    m_editEventArgsButton.Enabled = false;
    m_editEventArgsButton.OnClick.Add( this, (FBCallback)&EditEventPopup::EditEventArgsButton_Click );

    // Remove
    m_removeEventArgsButton.Caption = "Remove";
    m_removeEventArgsButton.Enabled = false;
    m_removeEventArgsButton.OnClick.Add( this, (FBCallback)&EditEventPopup::RemoveEventArgsButton_Click );

	// Cleanup
	m_cleanupEventArgsButton.Caption = "Cleanup";
	m_cleanupEventArgsButton.Enabled = false;
	m_cleanupEventArgsButton.OnClick.Add( this, (FBCallback)&EditEventPopup::CleanupEventArgsButton_Click );

    // Event Args Spreadsheet
    m_eventArgsSpreadsheet.Caption = "Event Args";
    m_eventArgsSpreadsheet.OnRowClick.Add( this, (FBCallback)&EditEventPopup::EventArgsSpreadsheet_RowClick );

    ClearEventArgsSpreadsheet();
}

void EditEventPopup::UIConfigureEventArgsEditLayout()
{
    // EventArgsEditLayoutRegion
    m_eventArgsEditLayout.SetRegionTitle( "EventArgsEditLayoutRegion", "Event Args" );
    m_eventArgsEditLayout.SetBorder( "EventArgsEditLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

    // EventArgsAttributeEditLayoutRegion
    m_eventArgsEditLayout.SetRegionTitle( "EventArgsAttributeEditLayoutRegion", "Attribute" );
    m_eventArgsEditLayout.SetBorder( "EventArgsAttributeEditLayoutRegion", kFBEmbossBorder, true, true, 2, 2, 90.0f, 0 );

    // Name
    m_eventArgsNameLabel.Caption = "Name:";
    m_eventArgsNameLabel.Justify = kFBTextJustifyRight;

    m_eventArgsNameTextBox.Caption = "";

    // Attribute Spreadsheet
    m_attributeSpreadsheet.Caption = "Attribute";
    m_attributeSpreadsheet.OnRowClick.Add( this, (FBCallback)&EditEventPopup::AttributeSpreadsheet_RowClick );

    ClearAttributeSpreadsheet();

    // Edit
    m_editAttributeButton.Caption = "Edit";
    m_editAttributeButton.OnClick.Add( this, (FBCallback)&EditEventPopup::EditAttributeButton_Click );

    // Add
    m_addAttributeButton.Caption = "Add";
    m_addAttributeButton.OnClick.Add( this, (FBCallback)&EditEventPopup::AddAttributeButton_Click );

    // Remove
    m_removeAttributeButton.Caption = "Remove";
    m_removeAttributeButton.OnClick.Add( this, (FBCallback)&EditEventPopup::RemoveAttributeButton_Click );

    // Remove All
    m_removeAllAttributesButton.Caption = "Remove All";
    m_removeAllAttributesButton.OnClick.Add( this, (FBCallback)&EditEventPopup::RemoveAllAttributesButton_Click );

    // Name
    m_attributeNameLabel.Caption = "Name:";
    m_attributeNameLabel.Justify = kFBTextJustifyRight;

    m_attributeNameTextBox.OnChange.Add( this, (FBCallback)&EditEventPopup::AttributeNameTextBox_Change );

    // Type
    m_attributeTypeLabel.Caption = "Type:";
    m_attributeTypeLabel.Justify = kFBTextJustifyRight;

    m_attributeTypeComboBox.Items.Add( "String" );
    m_attributeTypeComboBox.Items.Add( "Int" );
    m_attributeTypeComboBox.Items.Add( "Float" );
    m_attributeTypeComboBox.Items.Add( "Bool" );

    m_attributeTypeComboBox.OnChange.Add( this, (FBCallback)&EditEventPopup::AttributeTypeComboBox_Change );
    m_attributeTypeComboBox.Style = kFBDropDownList;

    // Value
    m_attributeValueLabel.Caption = "Value:";
    m_attributeValueLabel.Justify = kFBTextJustifyRight;

    m_attributeValueTextBox.OnChange.Add( this, (FBCallback)&EditEventPopup::AttributeValueTextBox_Change );

    // Done
    m_doneEditAttributeButton.Caption = "Done";
    m_doneEditAttributeButton.OnClick.Add( this, (FBCallback)&EditEventPopup::DoneEditAttributeButton_Click );

    // Cancel Edit
    m_cancelEditAttributeButton.Caption = "Cancel Edit";
    m_cancelEditAttributeButton.OnClick.Add( this, (FBCallback)&EditEventPopup::CancelEditAttributeButton_Click );

    m_saveEventArgsChangesButton.Caption = "Save Changes";
    m_saveEventArgsChangesButton.OnClick.Add( this, (FBCallback)&EditEventPopup::SaveEventArgsChangesButton_Click );

    m_discardEventArgsChangesButton.Caption = "Discard Changes";
    m_discardEventArgsChangesButton.OnClick.Add( this, (FBCallback)&EditEventPopup::DiscardEventArgsChangesButton_Click );
}

void EditEventPopup::SetEventEditMode( EEventEditMode mode )
{
    m_eventEditMode = mode;

    switch ( m_eventEditMode )
    {
    case NO_EVENT_MODE:
        {
            m_eventNameLabel.Enabled = false;
            m_eventNameTextBox.Enabled = false;
            m_eventTypeLabel.Enabled = false;
            m_eventTypeCheckBox.Enabled = false;
            m_eventRecipientLabel.Enabled = false;
            m_eventRecipientComboBox.Enabled = false;
            m_eventFrameLabel.Enabled = false;
            m_eventFrameNumericUpDown.Enabled = false;
            m_eventEventArgsLabel.Enabled = false;
            m_eventEventArgsComboBox.Enabled = false;
            m_saveEventChangesButton.Enabled = false;
            m_discardEventChangesButton.Enabled = false;
        }
        break;
    case ADD_EVENT_MODE:
        {
            m_eventNameLabel.Enabled = true;
            m_eventNameTextBox.Enabled = true;
            m_eventTypeLabel.Enabled = true;
            m_eventTypeCheckBox.Enabled = true;
            m_eventRecipientLabel.Enabled = true;
            m_eventRecipientComboBox.Enabled = m_pCurrentEventData->pEvent->GetType() == CUTSCENE_OBJECT_ID_EVENT_TYPE;
            m_eventFrameLabel.Enabled = true;
            m_eventFrameNumericUpDown.Enabled = true;
            m_eventEventArgsLabel.Enabled = true;
            m_eventEventArgsComboBox.Enabled = m_pCurrentEventData->pEventDef->GetEventArgsDef() != NULL;
            m_saveEventChangesButton.Enabled = m_bHhaveUnsavedEventChanges;
            m_discardEventChangesButton.Enabled = true;
        }
        break;
    case EDIT_EVENT_MODE:
        {
            m_eventNameLabel.Enabled = true;
            m_eventNameTextBox.Enabled = true;
            m_eventTypeLabel.Enabled = true;
            m_eventTypeCheckBox.Enabled = false;
            m_eventRecipientLabel.Enabled = true;
            m_eventRecipientComboBox.Enabled = m_pCurrentEventData->pEvent->GetType() == CUTSCENE_OBJECT_ID_EVENT_TYPE;
            m_eventFrameLabel.Enabled = true;
            m_eventFrameNumericUpDown.Enabled = true;
            m_eventEventArgsLabel.Enabled = true;
            m_eventEventArgsComboBox.Enabled = m_pCurrentEventData->pEventDef->GetEventArgsDef() != NULL;
            m_saveEventChangesButton.Enabled = m_bHhaveUnsavedEventChanges;
            m_discardEventChangesButton.Enabled = true;
        }
        break;
    }
}

void EditEventPopup::SetEventArgsEditMode( EEventArgsEditMode mode )
{
    if ( ((mode == EDIT_EVENT_ARGS_MODE) || (mode == EDIT_EVENT_ARGS_ATTRIBUTE_MODE)) 
        && (m_pCurrentEventArgsData == NULL) )
    {
        mode = NO_EVENT_ARGS_MODE;
    }

    m_eventArgsEditMode = mode;

    switch ( m_eventArgsEditMode )
    {
    case NO_EVENT_ARGS_MODE:
        {
            m_eventArgsNameLabel.Enabled = false;
            m_eventArgsNameTextBox.Enabled = false;
            // m_attributeSpreadsheet.Enabled = false;  no accessor for this
            m_editAttributeButton.Enabled = false;
            m_addAttributeButton.Enabled = false;
            m_removeAttributeButton.Enabled = false;
            m_removeAllAttributesButton.Enabled = false;
            m_attributeNameLabel.Enabled = false;
            m_attributeNameTextBox.Enabled = false;
            m_attributeTypeLabel.Enabled = false;
            m_attributeTypeComboBox.Enabled = false;
            m_attributeValueLabel.Enabled = false;
            m_attributeValueTextBox.Enabled = false;
            m_doneEditAttributeButton.Enabled = false;
            m_cancelEditAttributeButton.Enabled = false;
            m_saveEventArgsChangesButton.Enabled = false;
            m_discardEventArgsChangesButton.Enabled = false;
        }
        break;
    case ADD_EVENT_ARGS_MODE:
    case EDIT_EVENT_ARGS_MODE:
        {
            m_eventArgsNameLabel.Enabled = true;
            m_eventArgsNameTextBox.Enabled = true;
            // m_attributeSpreadsheet.Enabled = true;  no accessor for this  
			m_editAttributeButton.Enabled = false;
            m_addAttributeButton.Enabled = true;
            m_removeAttributeButton.Enabled = false;
            m_removeAllAttributesButton.Enabled = m_currentAttributeList.GetAttributeArray().GetCount() > 0;
            m_attributeNameLabel.Enabled = false;
            m_attributeNameTextBox.Enabled = false;
            m_attributeTypeLabel.Enabled = false;
            m_attributeTypeComboBox.Enabled = false;
            m_attributeValueLabel.Enabled = false;
            m_attributeValueTextBox.Enabled = false;
            m_doneEditAttributeButton.Enabled = false;
            m_cancelEditAttributeButton.Enabled = false;
            m_saveEventArgsChangesButton.Enabled = m_bHaveUnsavedEventArgsChanges;
            m_discardEventArgsChangesButton.Enabled = true;
        }
        break;
    case ADD_EVENT_ARGS_ATTRIBUTE_MODE:
        {
            m_eventArgsNameLabel.Enabled = true;
            m_eventArgsNameTextBox.Enabled = true;
            // m_attributeSpreadsheet.Enabled = false;  no accessor for this
            m_editAttributeButton.Enabled = false;
            m_addAttributeButton.Enabled = false;
            m_removeAttributeButton.Enabled = false;
            m_removeAllAttributesButton.Enabled = false;
            m_attributeNameLabel.Enabled = true;
            m_attributeNameTextBox.Enabled = true;
            m_attributeTypeLabel.Enabled = true;
            m_attributeTypeComboBox.Enabled = true;
            m_attributeValueLabel.Enabled = true;
            m_attributeValueTextBox.Enabled = true;
            m_doneEditAttributeButton.Enabled = m_bHaveUnsavedAttributeChanges;
            m_cancelEditAttributeButton.Enabled = true;
            m_saveEventArgsChangesButton.Enabled = m_bHaveUnsavedEventArgsChanges;
            m_discardEventArgsChangesButton.Enabled = true;
        }
        break;
    case EDIT_EVENT_ARGS_ATTRIBUTE_MODE:
        {
            m_eventArgsNameLabel.Enabled = true;
            m_eventArgsNameTextBox.Enabled = true;
            // m_attributeSpreadsheet.Enabled = false;  no accessor for this
			m_editAttributeButton.Enabled = false;
            m_addAttributeButton.Enabled = false;
            m_removeAttributeButton.Enabled = false;
            m_removeAllAttributesButton.Enabled = false;
            m_attributeNameLabel.Enabled = true;
            m_attributeNameTextBox.Enabled = false;
            m_attributeTypeLabel.Enabled = true;
            m_attributeTypeComboBox.Enabled = false;
            m_attributeValueLabel.Enabled = true;
            m_attributeValueTextBox.Enabled = true;
            m_doneEditAttributeButton.Enabled = m_bHaveUnsavedAttributeChanges;
            m_cancelEditAttributeButton.Enabled = true;
            m_saveEventArgsChangesButton.Enabled = m_bHaveUnsavedEventArgsChanges;
            m_discardEventArgsChangesButton.Enabled = true;
        }
        break;
    }
}

void EditEventPopup::CreateBuiltInEventDefs()
{
    // cutfEventArgs
    cutfEventArgsDef *pGenericEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_GENERIC_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_GENERIC_EVENT_ARGS_TYPE ) );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_GENERIC_EVENT_ARGS_TYPE, pGenericEventArgsDef );

    // cutfEventArgsList
    cutfEventArgsDef *pEventArgsListEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_EVENT_ARGS_LIST_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_EVENT_ARGS_LIST_TYPE ) );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_EVENT_ARGS_LIST_TYPE, pEventArgsListEventArgsDef );

    // cutfNameEventArgs
    cutfEventArgsDef *pNameEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_NAME_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_NAME_EVENT_ARGS_TYPE ) );
    parAttributeList &nameAttList = const_cast<parAttributeList &>( pNameEventArgsDef->GetAttributeList() );
    nameAttList.AddAttribute( "Name", "" );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_NAME_EVENT_ARGS_TYPE, pNameEventArgsDef );

    // cutfFloatValueEventArgs
    cutfEventArgsDef *pFloatValueEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_FLOAT_VALUE_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_FLOAT_VALUE_EVENT_ARGS_TYPE ) );
    parAttributeList &floatValueAttList = const_cast<parAttributeList &>( pFloatValueEventArgsDef->GetAttributeList() );
    floatValueAttList.AddAttribute( "Value", 0.0f );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_FLOAT_VALUE_EVENT_ARGS_TYPE, pFloatValueEventArgsDef );

    // cutfTwoFloatValuesEventArgs
    cutfEventArgsDef *pTwoFloatValuesEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_TWO_FLOAT_VALUES_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_TWO_FLOAT_VALUES_EVENT_ARGS_TYPE ) );
    parAttributeList &twoFloatValuesAttList = const_cast<parAttributeList &>( pTwoFloatValuesEventArgsDef->GetAttributeList() );
    twoFloatValuesAttList.AddAttribute( "Value1", 0.0f );
    twoFloatValuesAttList.AddAttribute( "Value2", 0.0f );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_TWO_FLOAT_VALUES_EVENT_ARGS_TYPE, pTwoFloatValuesEventArgsDef );

    // cutfScreenFadeEventArgs
    cutfEventArgsDef *pScreenFadeEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_SCREEN_FADE_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_SCREEN_FADE_EVENT_ARGS_TYPE ) );
    parAttributeList &screenFadeAttList = const_cast<parAttributeList &>( pScreenFadeEventArgsDef->GetAttributeList() );
    screenFadeAttList.AddAttribute( "Duration", CUTSCENE_FADE_TIME );
    screenFadeAttList.AddAttribute( "ColorR", 0 );
    screenFadeAttList.AddAttribute( "ColorG", 0 );
    screenFadeAttList.AddAttribute( "ColorB", 0 );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_SCREEN_FADE_EVENT_ARGS_TYPE, pScreenFadeEventArgsDef );

    // cutfLoadSceneEventArgs
    cutfEventArgsDef *pLoadSceneEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_LOAD_SCENE_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_LOAD_SCENE_EVENT_ARGS_TYPE ) );
    parAttributeList &loadSceneAttList = const_cast<parAttributeList &>( pLoadSceneEventArgsDef->GetAttributeList() );
    loadSceneAttList.AddAttribute( "SceneName", "" );
    loadSceneAttList.AddAttribute( "OffsetX", 0.0f );
    loadSceneAttList.AddAttribute( "OffsetY", 0.0f );
    loadSceneAttList.AddAttribute( "OffsetZ", 0.0f );
    loadSceneAttList.AddAttribute( "Rotation", 0.0f );
    loadSceneAttList.AddAttribute( "ExtraRoomName", "" );
    loadSceneAttList.AddAttribute( "ExtraRoomX", 0.0f );
    loadSceneAttList.AddAttribute( "ExtraRoomY", 0.0f );
    loadSceneAttList.AddAttribute( "ExtraRoomZ", 0.0f );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_LOAD_SCENE_EVENT_ARGS_TYPE, pLoadSceneEventArgsDef );

    // cutfObjectIdEventArgs
    cutfEventArgsDef *pObjectIdEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE ) );
    parAttributeList &objectIdAttList = const_cast<parAttributeList &>( pObjectIdEventArgsDef->GetAttributeList() );
    objectIdAttList.AddAttribute( "ObjectId", -1 );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE, pObjectIdEventArgsDef );

    // cutfObjectIdNameEventArgs
    cutfEventArgsDef *pObjectIdNameEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE ) );
    parAttributeList &objectIdNameAttList = const_cast<parAttributeList &>( pObjectIdNameEventArgsDef->GetAttributeList() );
    objectIdNameAttList.AddAttribute( "ObjectId", -1 );
    objectIdNameAttList.AddAttribute( "Name", "" );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE, pObjectIdNameEventArgsDef );

    // cutfAttachmentEventArgs
    cutfEventArgsDef *pAttachmentEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE ) );
    parAttributeList &attachmentAttList = const_cast<parAttributeList &>( pAttachmentEventArgsDef->GetAttributeList() );
    attachmentAttList.AddAttribute( "ObjectId", -1 );
    attachmentAttList.AddAttribute( "BoneName", "" );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE, pAttachmentEventArgsDef );

    // cutfObjectIdListEventArgs
    cutfEventArgsDef *pObjectIdListEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE ) );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE, pObjectIdListEventArgsDef );

    // cutfPlayParticleEffectEventArgs
    cutfEventArgsDef *pPlayParticleEffectEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT_ARGS_TYPE ) );
    //parAttributeList &playParticleEffectAttList = const_cast<parAttributeList &>( pPlayParticleEffectEventArgsDef->GetAttributeList() );
    //playParticleEffectAttList.AddAttribute( "PositionX", 0.0f );
    //playParticleEffectAttList.AddAttribute( "PositionY", 0.0f );
    //playParticleEffectAttList.AddAttribute( "PositionZ", 0.0f );
    //playParticleEffectAttList.AddAttribute( "DirectionX", 0.0f );
    //playParticleEffectAttList.AddAttribute( "DirectionY", 0.0f );
    //playParticleEffectAttList.AddAttribute( "DirectionZ", 0.0f );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT_ARGS_TYPE, pPlayParticleEffectEventArgsDef );

    // cutfSubtitleEventArgs
    cutfEventArgsDef *pSubtitleEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_SUBTITLE_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_SUBTITLE_EVENT_ARGS_TYPE ) );
    parAttributeList &subtitleAttList = const_cast<parAttributeList &>( pSubtitleEventArgsDef->GetAttributeList() );
    subtitleAttList.AddAttribute( "Identifier", "" );
    subtitleAttList.AddAttribute( "Language", "" );
    subtitleAttList.AddAttribute( "TransitionIn", 0 );
    subtitleAttList.AddAttribute( "TransitionInDuration", 0.0f );
    subtitleAttList.AddAttribute( "TransitionOut", 0 );
    subtitleAttList.AddAttribute( "TransitionOutDuration", 0.0f );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_SUBTITLE_EVENT_ARGS_TYPE, pSubtitleEventArgsDef );

    // cutfCameraCutEventArgs
    cutfEventArgsDef *pCameraCutEventArgsDef = rage_new cutfEventArgsDef( 
        CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
        cutfEventArgs::GetTypeName( CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE ) );
    parAttributeList &cameraCutAttList = const_cast<parAttributeList &>( pCameraCutEventArgsDef->GetAttributeList() );
    cameraCutAttList.AddAttribute( "Name", "" );
    cameraCutAttList.AddAttribute( "PositionX", 0.0f );
    cameraCutAttList.AddAttribute( "PositionY", 0.0f );
    cameraCutAttList.AddAttribute( "PositionZ", 0.0f );
    cameraCutAttList.AddAttribute( "RotationQuaternionX", 0.0f );
    cameraCutAttList.AddAttribute( "RotationQuaternionY", 0.0f );
    cameraCutAttList.AddAttribute( "RotationQuaternionZ", 0.0f );
    cameraCutAttList.AddAttribute( "RotationQuaternionW", 0.0f );
    cameraCutAttList.AddAttribute( "NearDrawDistance", CUTSCENE_DEFAULT_NEAR_CLIP );
    cameraCutAttList.AddAttribute( "FarDrawDistance", CUTSCENE_DEFAULT_FAR_CLIP );
    m_builtInEventArgsDefsMap.Insert( CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE, pCameraCutEventArgsDef );

	// cutfDecalEventArgs
	cutfEventArgsDef *pDecalEventArgsDef = rage_new cutfEventArgsDef( 
		CUTSCENE_DECAL_EVENT_ARGS - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
		cutfEventArgs::GetTypeName( CUTSCENE_DECAL_EVENT_ARGS ) );
	parAttributeList &decalAttList = const_cast<parAttributeList &>( pDecalEventArgsDef->GetAttributeList() );
	decalAttList.AddAttribute( "Name", "" );
	decalAttList.AddAttribute( "PositionX", 0.0f );
	decalAttList.AddAttribute( "PositionY", 0.0f );
	decalAttList.AddAttribute( "PositionZ", 0.0f );
	decalAttList.AddAttribute( "RotationQuaternionX", 0.0f );
	decalAttList.AddAttribute( "RotationQuaternionY", 0.0f );
	decalAttList.AddAttribute( "RotationQuaternionZ", 0.0f );
	decalAttList.AddAttribute( "RotationQuaternionW", 0.0f );
	decalAttList.AddAttribute( "Height", 0.0f );
	decalAttList.AddAttribute( "Width", 0.0f );
	decalAttList.AddAttribute( "LifeTime", 0.0f );
	m_builtInEventArgsDefsMap.Insert( CUTSCENE_DECAL_EVENT_ARGS, pDecalEventArgsDef );

	// cutfVehicleVariationEventArgs
	cutfEventArgsDef *pVehicleVariationEventArgsDef = rage_new cutfEventArgsDef( 
		CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
		cutfEventArgs::GetTypeName( CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE ) );
	parAttributeList &vehicleVariationAttList = const_cast<parAttributeList &>( pVehicleVariationEventArgsDef->GetAttributeList() ); 
	vehicleVariationAttList.AddAttribute( "MainBodyColour", 0 );
	vehicleVariationAttList.AddAttribute( "SecondBodyColour", 0 );
	vehicleVariationAttList.AddAttribute( "SpecularColour", 0 );
	vehicleVariationAttList.AddAttribute( "WheelTrimColour", 0 );
	vehicleVariationAttList.AddAttribute( "Livery", 0 );
	vehicleVariationAttList.AddAttribute( "DirtLevel", 0 );
	m_builtInEventArgsDefsMap.Insert( CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE, pVehicleVariationEventArgsDef );

	// cutfVehicleExtraVariationEventArgs
	cutfEventArgsDef *pVehicleExtraVariationEventArgsDef = rage_new cutfEventArgsDef( 
		CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
		cutfEventArgs::GetTypeName( CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE ) );
	//parAttributeList &vehicleExtraVariationAttList = const_cast<parAttributeList &>( pVehicleExtraVariationEventArgsDef->GetAttributeList() ); 
	//objectVariationAttList.AddAttribute( "MainBodyColour", 0 );
	//objectVariationAttList.AddAttribute( "SecondBodyColour", 0 );
	//objectVariationAttList.AddAttribute( "SpecularColour", 0 );
	//objectVariationAttList.AddAttribute( "WheelTrimColour", 0 );
	//objectVariationAttList.AddAttribute( "Livery", 0 );
	//objectVariationAttList.AddAttribute( "DirtLevel", 0 );
	m_builtInEventArgsDefsMap.Insert( CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE, pVehicleExtraVariationEventArgsDef );

	// cutfActorVariationEventArgs
	cutfEventArgsDef *pActorVariationEventArgsDef = rage_new cutfEventArgsDef( 
		CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
		cutfEventArgs::GetTypeName( CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE ) );
	parAttributeList &actorVariationAttList = const_cast<parAttributeList &>( pActorVariationEventArgsDef->GetAttributeList() );
	actorVariationAttList.AddAttribute( "ObjectId", 0 );
	actorVariationAttList.AddAttribute( "Component", 0 );
	actorVariationAttList.AddAttribute( "Drawable", 0 );
	actorVariationAttList.AddAttribute( "Texture", 0 );
	m_builtInEventArgsDefsMap.Insert( CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE, pActorVariationEventArgsDef );

	// cutfFloatBoolValueEventArgs
	cutfEventArgsDef *pFloatBoolValueEventArgsDef = rage_new cutfEventArgsDef( 
		CUTSCENE_FLOAT_BOOL_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
		cutfEventArgs::GetTypeName( CUTSCENE_FLOAT_BOOL_EVENT_ARGS_TYPE ) );
	m_builtInEventArgsDefsMap.Insert( CUTSCENE_FLOAT_BOOL_EVENT_ARGS_TYPE, pFloatBoolValueEventArgsDef );

	// cutfBoolValueEventArgs
	cutfEventArgsDef *pBoolValueEventArgsDef = rage_new cutfEventArgsDef( 
		CUTSCENE_BOOL_VALUE_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
		cutfEventArgs::GetTypeName( CUTSCENE_BOOL_VALUE_EVENT_ARGS_TYPE ) );
	m_builtInEventArgsDefsMap.Insert( CUTSCENE_BOOL_VALUE_EVENT_ARGS_TYPE, pBoolValueEventArgsDef );

	// cutfCascadeShadowEventArgs
	cutfEventArgsDef *pCascadeShadowEventArgsDef = rage_new cutfEventArgsDef( 
		CUTSCENE_CASCADE_SHADOW_EVENT_ARGS - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
		cutfEventArgs::GetTypeName( CUTSCENE_CASCADE_SHADOW_EVENT_ARGS ) );
	m_builtInEventArgsDefsMap.Insert( CUTSCENE_CASCADE_SHADOW_EVENT_ARGS, pCascadeShadowEventArgsDef );

	// cutfFinalNameEventArgs
	cutfEventArgsDef *pFinalNameEventArgsDef = rage_new cutfEventArgsDef( 
		CUTSCENE_FINAL_NAME_EVENT_ARGS_TYPE - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE, 
		cutfEventArgs::GetTypeName( CUTSCENE_FINAL_NAME_EVENT_ARGS_TYPE ) );
	m_builtInEventArgsDefsMap.Insert( CUTSCENE_FINAL_NAME_EVENT_ARGS_TYPE, pFinalNameEventArgsDef );

    // CUTSCENE_LOAD_SCENE_EVENT and CUTSCENE_UNLOAD_SCENE_EVENT
    ConstructEventDef( CUTSCENE_LOAD_SCENE_EVENT, CUTSCENE_UNLOAD_SCENE_EVENT, pLoadSceneEventArgsDef );

    // CUTSCENE_LOAD_ANIM_DICT_EVENT and CUTSCENE_UNLOAD_ANIM_DICT_EVENT
    ConstructEventDef( CUTSCENE_LOAD_ANIM_DICT_EVENT, CUTSCENE_UNLOAD_ANIM_DICT_EVENT, pNameEventArgsDef );

    // CUTSCENE_LOAD_AUDIO_EVENT and CUTSCENE_UNLOAD_AUDIO_EVENT
    ConstructEventDef( CUTSCENE_LOAD_AUDIO_EVENT, CUTSCENE_UNLOAD_AUDIO_EVENT, pNameEventArgsDef );

    // CUTSCENE_LOAD_MODELS_EVENT and CUTSCENE_UNLOAD_MODELS_EVENT
    ConstructEventDef( CUTSCENE_LOAD_MODELS_EVENT, CUTSCENE_UNLOAD_MODELS_EVENT, pObjectIdListEventArgsDef );

    // CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT and CUTSCENE_UNLOAD_PARTICLE_EFFECTS_EVENT
    ConstructEventDef( CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT, CUTSCENE_UNLOAD_PARTICLE_EFFECTS_EVENT, pObjectIdListEventArgsDef );

    // CUTSCENE_LOAD_OVERLAYS_EVENT and CUTSCENE_UNLOAD_OVERLAYS_EVENT
    ConstructEventDef( CUTSCENE_LOAD_OVERLAYS_EVENT, CUTSCENE_UNLOAD_OVERLAYS_EVENT, pObjectIdListEventArgsDef );

    // CUTSCENE_LOAD_SUBTITLES_EVENT and CUTSCENE_UNLOAD_SUBTITLES_EVENT
    ConstructEventDef( CUTSCENE_LOAD_SUBTITLES_EVENT, CUTSCENE_UNLOAD_SUBTITLES_EVENT, pNameEventArgsDef );

    // CUTSCENE_HIDE_OBJECTS_EVENT and CUTSCENE_SHOW_OBJECTS_EVENT
    ConstructEventDef( CUTSCENE_HIDE_OBJECTS_EVENT, CUTSCENE_SHOW_OBJECTS_EVENT, pObjectIdEventArgsDef );

    // CUTSCENE_FIXUP_OBJECTS_EVENT and CUTSCENE_REVERT_FIXUP_OBJECTS_EVENT
    ConstructEventDef( CUTSCENE_FIXUP_OBJECTS_EVENT, CUTSCENE_REVERT_FIXUP_OBJECTS_EVENT, pObjectIdListEventArgsDef );

    // CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT and CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT
    ConstructEventDef( CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT, CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT, pObjectIdEventArgsDef );

    // CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT and CUTSCENE_REMOVE_REMOVAL_BOUNDS_EVENT
    ConstructEventDef( CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT, CUTSCENE_REMOVE_REMOVAL_BOUNDS_EVENT, pObjectIdListEventArgsDef );

    // CUTSCENE_FADE_OUT_EVENT and CUTSCENE_FADE_IN_EVENT
    ConstructEventDef( CUTSCENE_FADE_OUT_EVENT, CUTSCENE_FADE_IN_EVENT, pScreenFadeEventArgsDef );

    // CUTSCENE_SET_ANIM_EVENT and CUTSCENE_CLEAR_ANIM_EVENT
    ConstructEventDef( CUTSCENE_SET_ANIM_EVENT, CUTSCENE_CLEAR_ANIM_EVENT, pObjectIdNameEventArgsDef );

    // CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT and CUTSCENE_STOP_PARTICLE_EFFECT_EVENT
    ConstructEventDef( CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT, CUTSCENE_STOP_PARTICLE_EFFECT_EVENT, pPlayParticleEffectEventArgsDef );

    // CUTSCENE_SHOW_OVERLAY_EVENT and CUTSCENE_HIDE_OVERLAY_EVENT
    ConstructEventDef( CUTSCENE_SHOW_OVERLAY_EVENT, CUTSCENE_HIDE_OVERLAY_EVENT, pGenericEventArgsDef );

    // CUTSCENE_PLAY_AUDIO_EVENT and CUTSCENE_STOP_AUDIO_EVENT
    ConstructEventDef( CUTSCENE_PLAY_AUDIO_EVENT, CUTSCENE_STOP_AUDIO_EVENT, pNameEventArgsDef );

    // CUTSCENE_SHOW_SUBTITLE_EVENT and CUTSCENE_HIDE_SUBTITLE_EVENT
    ConstructEventDef( CUTSCENE_SHOW_SUBTITLE_EVENT, CUTSCENE_HIDE_SUBTITLE_EVENT, pSubtitleEventArgsDef );

    // CUTSCENE_SET_DRAW_DISTANCE_EVENT
    ConstructEventDef( CUTSCENE_SET_DRAW_DISTANCE_EVENT, -1, pTwoFloatValuesEventArgsDef );
    
    // CUTSCENE_SET_ATTACHMENT_EVENT
    ConstructEventDef( CUTSCENE_SET_ATTACHMENT_EVENT, -1, pAttachmentEventArgsDef );
    
    // CUTSCENE_SET_VARIATION_EVENT
    ConstructEventDef( CUTSCENE_SET_VARIATION_EVENT, -1, pActorVariationEventArgsDef );

    // CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT and CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT
    ConstructEventDef( CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT, CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT, pGenericEventArgsDef );

    // CUTSCENE_HIDE_HIDDEN_OBJECT_EVENT and CUTSCENE_SHOW_HIDDEN_OBJECT_EVENT
    ConstructEventDef( CUTSCENE_HIDE_HIDDEN_OBJECT_EVENT, CUTSCENE_SHOW_HIDDEN_OBJECT_EVENT, pGenericEventArgsDef );

    // CUTSCENE_FIX_FIXUP_OBJECT_EVENT and CUTSCENE_REVERT_FIXUP_OBJECT_EVENT
    ConstructEventDef( CUTSCENE_FIX_FIXUP_OBJECT_EVENT, CUTSCENE_REVERT_FIXUP_OBJECT_EVENT, pGenericEventArgsDef );

    // CUTSCENE_CAMERA_CUT_EVENT
    ConstructEventDef( CUTSCENE_CAMERA_CUT_EVENT, -1, pCameraCutEventArgsDef );

    // CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT and CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT
    ConstructEventDef( CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT, CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT, pGenericEventArgsDef );

	// CUTSCENE_LOAD_RAYFIRE_EVENT and CUTSCENE_UNLOAD_RAYFIRE_EVENT
	ConstructEventDef( CUTSCENE_LOAD_RAYFIRE_EVENT, CUTSCENE_UNLOAD_RAYFIRE_EVENT, pObjectIdListEventArgsDef );

	// CUTSCENE_TRIGGER_DECAL_EVENT and CUTSCENE_REMOVE_DECAL_EVENT
	ConstructEventDef( CUTSCENE_TRIGGER_DECAL_EVENT, CUTSCENE_REMOVE_DECAL_EVENT, pDecalEventArgsDef );

	// CUTSCENE_ENABLE_CASCADE_SHADOW_BOUNDS_EVENT
	ConstructEventDef( CUTSCENE_ENABLE_CASCADE_SHADOW_BOUNDS_EVENT, -1, pCascadeShadowEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER, -1, pBoolValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE, -1, pBoolValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_UPDATE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_UPDATE, -1, pBoolValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_AIRCRAFT_MODE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_AIRCRAFT_MODE, -1, pBoolValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE, -1, pBoolValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_FLY_CAMERA_MODE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_FLY_CAMERA_MODE, -1, pBoolValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV, -1, pFloatValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV 
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV, -1, pFloatValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE, -1, pFloatValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE, -1, pFloatValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT, -1, pFloatValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE, -1, pFloatValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_MINMAX
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_MINMAX, -1, pTwoFloatValuesEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_MINMAX
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_MINMAX, -1, pTwoFloatValuesEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_DEPTH_BIAS
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_DEPTH_BIAS, -1, pFloatBoolValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_SLOPE_BIAS
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_SLOPE_BIAS, -1, pFloatBoolValueEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE, -1, pFinalNameEventArgsDef );

	// CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE
	ConstructEventDef( CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE, -1, pFloatValueEventArgsDef );

	for(int i=0 ; i < m_pEventDefs->GetEventDefCount(); ++i)
	{
		cutfEventDef* pEventDef = const_cast<cutfEventDef*>( m_pEventDefs->GetEventDef(i) );
		pEventDef->ConvertArgIndicesToPointers();
	}
}

void EditEventPopup::ConstructEventDef( s32 iEventId1, s32 iEventId2, const cutfEventArgsDef *pEventArgsDef ) 
{
    if ( (iEventId1 >= 0) && (iEventId1 < CUTSCENE_NUM_EVENTS) )
    {
        m_builtInEventDefsMap.Insert( iEventId1, 
            rage_new cutfEventDef( iEventId1 - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE,
            cutfEvent::GetDisplayName( iEventId1 ), cutfEvent::GetRank( iEventId1 ), 
            cutfEvent::GetOppositeEventId( iEventId1 ), pEventArgsDef ) );
    }

    if ( (iEventId2 >= 0) && (iEventId2 < CUTSCENE_NUM_EVENTS) )
    {
        m_builtInEventDefsMap.Insert( iEventId2, 
            rage_new cutfEventDef( iEventId2 - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE,
            cutfEvent::GetDisplayName( iEventId2 ), cutfEvent::GetRank( iEventId2 ), 
            cutfEvent::GetOppositeEventId( iEventId2 ), pEventArgsDef ) );
    }
}

EditEventPopup::SEditEventData* EditEventPopup::CreateEventData( const cutfEvent *pEvent )
{
    const cutfEventDef *pEventDef = NULL;

    if ( pEvent->GetEventId() < CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE )
    {
        const cutfEventDef **ppEventDef = m_builtInEventDefsMap.Access( pEvent->GetEventId() );
        if ( ppEventDef != NULL )
        {
            pEventDef = *ppEventDef;
        }
    }
    else
    {
        int iEventIdOffset = pEvent->GetEventId() - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
        if ( (iEventIdOffset >= 0) && (iEventIdOffset < m_pEventDefs->GetEventDefCount()) )
        {
            pEventDef = m_pEventDefs->GetEventDef( iEventIdOffset );
        }
    }

    if ( pEventDef != NULL )
    {
        SEditEventData *pEventData = rage_new SEditEventData;
        pEventData->pEvent = const_cast<cutfEvent *>( pEvent );
        pEventData->pEventDef = pEventDef;

        if ( (pEvent->GetEventArgs() != NULL) && (pEventDef->GetEventArgsDef() != NULL) )
        {
            pEventData->pEventArgsData = CreateEventArgsData( pEvent->GetEventArgs(), pEventDef->GetEventArgsDef() );
        }

        return pEventData;
    }

    return NULL;
}


void EditEventPopup::DestroyEventData( SEditEventData* &pEventData )
{
    const cutfEventArgs *pEventArgs = pEventData->pEvent->GetEventArgs();

    // check if we're the last reference, and add a phantom reference so we do not delete the event args 
    bool bPhantomReference = false;
    if ( (pEventArgs != NULL) && (pEventArgs->GetRef() <= 1) )
    {
        pEventArgs->AddRef();
        bPhantomReference = true;
    }

    if ( pEventData->bIsLoadEvent )
    {
        m_pCutFile->RemoveLoadEvent( pEventData->pEvent );
    }
    else
    {
        m_pCutFile->RemoveEvent( pEventData->pEvent );
    }

    if ( bPhantomReference )
    {
        pEventArgs->Release();
    }

    delete pEventData;
    pEventData = NULL;
}

EditEventPopup::SEditEventArgsData* EditEventPopup::CreateEventArgsData( const cutfEventArgs *pEventArgs, const cutfEventArgsDef *pEventArgsDef )
{
    // first, see if our edit event args data already exists
    for ( int i = 0; i < m_eventArgsDataList.GetCount(); ++i )
    {
        if ( m_eventArgsDataList[i]->pEventArgs == pEventArgs )
        {
            return m_eventArgsDataList[i]; 
        }
    }

    char cName[CUTSCENE_LONG_OBJNAMELEN];
    cName[0] = 0;

    bool bIsBuiltIn = false;
    if ( pEventArgsDef == NULL )
    {
        const cutfEventArgsDef **ppEventArgsDef = m_builtInEventArgsDefsMap.Access( pEventArgs->GetType() );
        if ( ppEventArgsDef != NULL )
        {
            pEventArgsDef = *ppEventArgsDef;
        }

        bIsBuiltIn = true;
    }

    int *pCount = m_eventArgsDefCountMap.Access( pEventArgsDef );
    if ( pCount == NULL )
    {
        // special case
        if ( (strcmp( pEventArgsDef->GetName(), "Generic" ) == 0) && !bIsBuiltIn )
        {
            // this is the first time we've used this event args def
            sprintf( cName, "%s_%d", pEventArgsDef->GetName(), 1 );

            m_eventArgsDefCountMap.Insert( pEventArgsDef, 2 );
        }
        else
        {
            // this is the first time we've used this event args def
            sprintf( cName, "%s_%d", pEventArgsDef->GetName(), 0 );

            m_eventArgsDefCountMap.Insert( pEventArgsDef, 1 );
        }
    }
    else
    {
        sprintf( cName, "%s_%d", pEventArgsDef->GetName(), *pCount );

        *pCount = *pCount + 1;
    }

    if ( strlen( cName ) > 0 )
    {
        SEditEventArgsData *pEventArgsData = rage_new SEditEventArgsData;
        pEventArgsData->pEventArgs = const_cast<cutfEventArgs *>( pEventArgs );
        pEventArgsData->pEventArgsDef = pEventArgsDef;
        pEventArgsData->displayName = cName;

        return pEventArgsData;
    }

    return NULL;
}

void EditEventPopup::DestroyEventArgsData( SEditEventArgsData* &pEventArgsData )
{    
    m_pCutFile->RemoveEventArgs( pEventArgsData->pEventArgs );

    delete pEventArgsData;
    pEventArgsData = NULL;
}

void EditEventPopup::ClearEventSpreadsheet()
{
    m_eventSpreadsheet.Clear();

    m_eventSpreadsheet.ColumnAdd( "Load Event" );
    m_eventSpreadsheet.GetColumn( EVENT_TYPE_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_eventSpreadsheet.GetColumn( EVENT_TYPE_SPREADSHEET_COLUMN ).Style = kFBCellStyle2StatesButton;
    m_eventSpreadsheet.GetColumn( EVENT_TYPE_SPREADSHEET_COLUMN ).Width = 70;

    m_eventSpreadsheet.ColumnAdd( "Recipient" );
    m_eventSpreadsheet.GetColumn( EVENT_RECIPIENT_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_eventSpreadsheet.GetColumn( EVENT_RECIPIENT_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
    m_eventSpreadsheet.GetColumn( EVENT_RECIPIENT_SPREADSHEET_COLUMN ).Width = 125;

    m_eventSpreadsheet.ColumnAdd( "Frame" );
    m_eventSpreadsheet.GetColumn( EVENT_FRAME_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_eventSpreadsheet.GetColumn( EVENT_FRAME_SPREADSHEET_COLUMN ).Style = kFBCellStyleInteger;
    m_eventSpreadsheet.GetColumn( EVENT_FRAME_SPREADSHEET_COLUMN ).Width = 60;

    m_eventSpreadsheet.ColumnAdd( "Event Args" );
    m_eventSpreadsheet.GetColumn( EVENT_ARGS_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_eventSpreadsheet.GetColumn( EVENT_ARGS_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
    m_eventSpreadsheet.GetColumn( EVENT_ARGS_SPREADSHEET_COLUMN ).Width = 125;

    if ( GetEventEditMode() == ADD_EVENT_MODE )
    {
        delete m_pCurrentEventData->pEvent;
        delete m_pCurrentEventData;
    }

    for ( int i = 0; i < m_eventDataList.GetCount(); ++i )
    {
        delete m_eventDataList[i];
    }
    
    m_eventDataList.Reset();
    
    m_pCurrentEventData = NULL;
    
    m_editEventButton.Enabled = false;
    m_removeEventButton.Enabled = false;

    m_eventDataRowRefMap.Kill();
    m_rowRefEventDataMap.Kill();
    m_iEventSpreadsheetRowRef = 0;
}

void EditEventPopup::ClearEventArgsSpreadsheet()
{  
    m_eventArgsSpreadsheet.Clear();

    m_eventArgsSpreadsheet.GetColumn( EVENT_ARGS_NAME_SPREADSHEET_COLUMN ).Width = 125;

    m_eventArgsSpreadsheet.ColumnAdd( "References" );
    m_eventArgsSpreadsheet.GetColumn( EVENT_ARGS_REFERENCE_COUNT_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_eventArgsSpreadsheet.GetColumn( EVENT_ARGS_REFERENCE_COUNT_SPREADSHEET_COLUMN ).Style = kFBCellStyleInteger;
    m_eventArgsSpreadsheet.GetColumn( EVENT_ARGS_REFERENCE_COUNT_SPREADSHEET_COLUMN ).Width = 90;

    bool bDeleteEventArgs = false;
    switch ( GetEventArgsEditMode() )
    {
    case ADD_EVENT_ARGS_MODE:
        bDeleteEventArgs = true;
        break;
    case ADD_EVENT_ARGS_ATTRIBUTE_MODE:
    case EDIT_EVENT_ARGS_ATTRIBUTE_MODE:
        bDeleteEventArgs = m_lastEventArgsEditMode == ADD_EVENT_ARGS_MODE;
        break;
    default:
        break;
    }
    
    if ( bDeleteEventArgs )
    {
        delete m_pCurrentEventArgsData->pEventArgs;
        delete m_pCurrentEventArgsData;
    }

    for ( int i = 0; i < m_eventArgsDataList.GetCount(); ++i )
    {
        delete m_eventArgsDataList[i];
    }

    m_eventArgsDataList.Reset();

    m_eventArgsDefCountMap.Kill();

    m_pCurrentEventArgsData = NULL;

    m_editEventArgsButton.Enabled = false;
    m_removeEventArgsButton.Enabled = false;
	m_cleanupEventArgsButton.Enabled = false;

    m_eventArgsDataRowRefMap.Kill();
    m_rowRefEventArgsDataMap.Kill();
    m_iEventArgsSpreadsheetRowRef = 0;
}

void EditEventPopup::ClearAttributeSpreadsheet()
{   
    m_attributeSpreadsheet.Clear();

    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_NAME_SPREADSHEET_COLUMN ).Width = 125;

    m_attributeSpreadsheet.ColumnAdd( "Type" );
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_TYPE_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_TYPE_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_TYPE_SPREADSHEET_COLUMN ).Width = 125;

    m_attributeSpreadsheet.ColumnAdd( "Value" );
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_VALUE_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_VALUE_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_VALUE_SPREADSHEET_COLUMN ).Width = 100;

    m_attributeSpreadsheet.ColumnAdd( "Description" );
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_DESCRIPTION_SPREADSHEET_COLUMN ).ReadOnly = true;
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_DESCRIPTION_SPREADSHEET_COLUMN ).Style = kFBCellStyleString;
    m_attributeSpreadsheet.GetColumn( ATTRIBUTE_DESCRIPTION_SPREADSHEET_COLUMN ).Width = 200;

    m_currentAttributeList.Reset();

    m_pCurrentAttribute = NULL;

    m_editAttributeButton.Enabled = false;
    m_removeAttributeButton.Enabled = false;

    m_attributeRowRefMap.Kill();
    m_rowRefAttributeMap.Kill();
    m_iAttributeSpreadsheetRowRef = 0;
}

void EditEventPopup::AddRowToEventSpreadsheet( SEditEventData *pEventData )
{
    if ( (pEventData == NULL) || (m_eventDataList.Find( pEventData ) != -1) )
    {
        return;
    }

    m_eventDataList.Grow() = pEventData;

    char cName[CUTSCENE_LONG_OBJNAMELEN];
    if ( pEventData->pEventDef != NULL )
    {
        pEventData->pEventDef->GetEnumName( cName, sizeof(cName) );
    }
    else
    {
        sprintf( cName, "Unknown (id=%d)", pEventData->pEvent->GetEventId() );
    }

    m_eventSpreadsheet.RowAdd( cName, m_iEventSpreadsheetRowRef );        

    m_eventDataRowRefMap.Insert( pEventData, m_iEventSpreadsheetRowRef );
    m_rowRefEventDataMap.Insert( m_iEventSpreadsheetRowRef, pEventData );

    UpdateEventInSpreadsheet( pEventData );

    ++m_iEventSpreadsheetRowRef;
}

void EditEventPopup::RemoveRowFromEventSpreadsheet( SEditEventData *pEventData )
{
    int *pRowRef = m_eventDataRowRefMap.Access( pEventData );
    if ( pRowRef != NULL )
    {
        FBSpreadRow row = m_eventSpreadsheet.GetRow( *pRowRef );
        row.Remove();
    }

    int iIndex = m_eventDataList.Find( pEventData );
    if ( iIndex != -1 )
    {
        m_eventDataList.Delete( iIndex );        
    }
}

void EditEventPopup::AddRowToEventArgsSpreadsheet( SEditEventArgsData *pEventArgsData )
{
    if ( (pEventArgsData == NULL) || (m_eventArgsDataList.Find( pEventArgsData ) != -1) )
    {
        return;
    }

    m_eventArgsDataList.Grow() = pEventArgsData;    

    m_eventArgsSpreadsheet.RowAdd( const_cast<char *>( pEventArgsData->displayName.c_str() ), m_iEventArgsSpreadsheetRowRef );

    m_eventArgsSpreadsheet.SetCell( m_iEventArgsSpreadsheetRowRef, EVENT_ARGS_REFERENCE_COUNT_SPREADSHEET_COLUMN, 
        pEventArgsData->pEventArgs->GetRef() );

    m_eventArgsDataRowRefMap.Insert( pEventArgsData, m_iEventArgsSpreadsheetRowRef );
    m_rowRefEventArgsDataMap.Insert( m_iEventArgsSpreadsheetRowRef, pEventArgsData );

    ++m_iEventArgsSpreadsheetRowRef;

    // keep the combo box up to date
    if ( (m_pCurrentEventData != NULL) && (m_pCurrentEventData->pEventDef->GetEventArgsDef() == pEventArgsData->pEventArgsDef) )
    {
        m_eventEventArgsComboBox.Items.Add( const_cast<char *>( pEventArgsData->displayName.c_str() ) );
        m_eventArgsDataComboBoxList.Grow() = pEventArgsData;
    }
}

void EditEventPopup::RemoveRowFromEventArgsSpreadsheet( SEditEventArgsData *pEventArgsData )
{
    int *pRowRef = m_eventArgsDataRowRefMap.Access( pEventArgsData );
    if ( pRowRef != NULL )
    {
        FBSpreadRow row = m_eventArgsSpreadsheet.GetRow( *pRowRef );
        row.Remove();
    }

    int iIndex = m_eventArgsDataList.Find( pEventArgsData );
    if ( iIndex != -1 )
    {       
        m_eventArgsDataList.Delete( iIndex );
    }

    iIndex = m_eventArgsDataComboBoxList.Find( pEventArgsData );
    if ( iIndex != -1 )
    {
        m_eventEventArgsComboBox.Items.RemoveAt( iIndex );
        m_eventArgsDataComboBoxList.Delete( iIndex );
    }    
}

void EditEventPopup::AddRowToAttributeSpreadsheet( parAttribute *pAttribute )
{
    m_attributeSpreadsheet.RowAdd( const_cast<char *>( pAttribute->GetName() ), m_iAttributeSpreadsheetRowRef );

    switch ( pAttribute->GetType() )
    {
    case parAttribute::STRING:
        m_attributeSpreadsheet.SetCell( m_iAttributeSpreadsheetRowRef, ATTRIBUTE_TYPE_SPREADSHEET_COLUMN, "String" );
        break;
    case parAttribute::INT64:
        m_attributeSpreadsheet.SetCell( m_iAttributeSpreadsheetRowRef, ATTRIBUTE_TYPE_SPREADSHEET_COLUMN, "Int" );
        break;
    case parAttribute::DOUBLE:
        m_attributeSpreadsheet.SetCell( m_iAttributeSpreadsheetRowRef, ATTRIBUTE_TYPE_SPREADSHEET_COLUMN, "Float" );
        break;
    case parAttribute::BOOL:
        m_attributeSpreadsheet.SetCell( m_iAttributeSpreadsheetRowRef, ATTRIBUTE_TYPE_SPREADSHEET_COLUMN, "Bool" );
        break;
    }

    char buf[128];
    m_attributeSpreadsheet.SetCell( m_iAttributeSpreadsheetRowRef, ATTRIBUTE_VALUE_SPREADSHEET_COLUMN, 
        const_cast<char *>( pAttribute->GetStringRepr( buf, sizeof(buf) ) ) );

    // special case
    atString name(pAttribute->GetName());
    if ( (pAttribute->GetType() == parAttribute::INT64) && name.StartsWith( "ObjectId" ) )
    {
        const atArray<cutfObject *> &objectList = m_pCutFile->GetObjectList();
        if ( (pAttribute->GetIntValue() >= 0) && (pAttribute->GetIntValue() < objectList.GetCount()) )
        {
            cutfObject *pObject = objectList[pAttribute->GetIntValue()];
            m_attributeSpreadsheet.SetCell( m_iAttributeSpreadsheetRowRef, ATTRIBUTE_DESCRIPTION_SPREADSHEET_COLUMN, 
                const_cast<char *>( pObject->GetDisplayName().c_str() ) );
        }
    }

    m_attributeRowRefMap.Insert( pAttribute, m_iAttributeSpreadsheetRowRef );
    m_rowRefAttributeMap.Insert( m_iAttributeSpreadsheetRowRef, pAttribute );

    ++m_iAttributeSpreadsheetRowRef;
}

void EditEventPopup::RemoveRowFromAttributesSpreadsheet( parAttribute *pAttribute )
{
    int *pRowRef = m_attributeRowRefMap.Access( pAttribute );
    if ( pRowRef != NULL )
    {
        FBSpreadRow row = m_attributeSpreadsheet.GetRow( *pRowRef );
        row.Remove();
    }

    atArray<parAttribute> &attList = m_currentAttributeList.GetAttributeArray();
    for ( int i = 0; i < attList.GetCount(); ++i )
    {
        if ( strcmp( attList[i].GetName(), pAttribute->GetName() ) == 0 )
        {
            attList.Delete( i );
            break;
        }
    }
}

void EditEventPopup::UpdateEventInSpreadsheet( const SEditEventData *pEventData )
{
    int *pRowRef = m_eventDataRowRefMap.Access( const_cast<SEditEventData *>( pEventData ) );
    if ( pRowRef != NULL )
    {
        m_eventSpreadsheet.SetCell( *pRowRef, EVENT_TYPE_SPREADSHEET_COLUMN, pEventData->bIsLoadEvent ? 1 : 0 );

        switch ( pEventData->pEvent->GetType() )
        {
        case CUTSCENE_EVENT_TYPE:
            m_eventSpreadsheet.SetCell( *pRowRef, EVENT_RECIPIENT_SPREADSHEET_COLUMN, "(none)" );
            break;
        case CUTSCENE_OBJECT_ID_EVENT_TYPE:
            {
                cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<cutfObjectIdEvent *>( pEventData->pEvent );

                const atArray<cutfObject *> &objectList = m_pCutFile->GetObjectList();
                for ( int i = 0; i < objectList.GetCount(); ++i )
                {
                    if ( objectList[i]->GetObjectId() == pObjectIdEvent->GetObjectId() )
                    {
                        m_eventSpreadsheet.SetCell( *pRowRef, EVENT_RECIPIENT_SPREADSHEET_COLUMN, 
                            const_cast<char *>( objectList[i]->GetDisplayName().c_str() ) );
                        break;
                    }
                }
            }
            break;
        case CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE:
            m_eventSpreadsheet.SetCell( *pRowRef, EVENT_RECIPIENT_SPREADSHEET_COLUMN, "(multiple)" );
            break;
        }

        m_eventSpreadsheet.SetCell( *pRowRef, EVENT_FRAME_SPREADSHEET_COLUMN, 
            rexMBAnimExportCommon::GetFrameFromTime(pEventData->pEvent->GetTime()) );

        if ( pEventData->pEventArgsData != NULL )
        {
            m_eventSpreadsheet.SetCell( *pRowRef, EVENT_ARGS_SPREADSHEET_COLUMN, const_cast<char *>( pEventData->pEventArgsData->displayName.c_str() ) );
        }
        else
        {
            m_eventSpreadsheet.SetCell( *pRowRef, EVENT_ARGS_SPREADSHEET_COLUMN, "(none)" );
        }
    }
}

void EditEventPopup::UpdateUIFromEventArgsReferenceCount()
{
    int iCount = 0;
    for ( int i = 0; i < m_eventArgsDataList.GetCount(); ++i )
    {
        m_eventArgsSpreadsheet.SetCell( i, EVENT_ARGS_REFERENCE_COUNT_SPREADSHEET_COLUMN, m_eventArgsDataList[i]->pEventArgs->GetRef() );

        if ( m_eventArgsDataList[i]->pEventArgs->GetRef() == 0 )
        {
            ++iCount;
        }
    }

    m_cleanupEventArgsButton.Enabled = iCount > 0;
}

void EditEventPopup::SetUIFromEvent( SEditEventData *pEventData )
{
    m_pCurrentEventData = pEventData;

    if ( pEventData == NULL )
    {
        m_eventNameTextBox.Caption = "";
        m_eventTypeCheckBox.State = 0;
        m_eventRecipientComboBox.ItemIndex = 0;
        m_eventFrameNumericUpDown.Value = 0;
        m_eventEventArgsComboBox.Items.Clear();
        m_eventArgsDataComboBoxList.Reset();

        SetHaveUnsavedEventChanges( false );

        return;
    }

    char cEnumName[CUTSCENE_LONG_OBJNAMELEN];
    pEventData->pEventDef->GetEnumName( cEnumName, sizeof(cEnumName) );
    m_eventNameTextBox.Caption = cEnumName;

    m_eventTypeCheckBox.State = pEventData->bIsLoadEvent ? 1 : 0;

    m_eventRecipientComboBox.ItemIndex = 0;
    if ( pEventData->pEvent->GetType() == CUTSCENE_OBJECT_ID_EVENT_TYPE )
    {
        cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<cutfObjectIdEvent *>( pEventData->pEvent );
        const atArray<cutfObject *> &objectList = m_pCutFile->GetObjectList();

        for ( int i = 0; i < objectList.GetCount(); ++i )
        {
            if ( objectList[i]->GetObjectId() == pObjectIdEvent->GetObjectId() )
            {
                m_eventRecipientComboBox.ItemIndex = i;
                break;
            }
        }
    }

    m_eventFrameNumericUpDown.Value = ceilf( pEventData->pEvent->GetTime() * rexMBAnimExportCommon::GetFPS() );

    m_eventEventArgsComboBox.Items.Clear();
    m_eventArgsDataComboBoxList.Reset();
    if ( pEventData->pEventDef->GetEventArgsDef() != NULL )
    {
        // fill the combo box with the instance names for the matching event args defs and look for the matching index, if any
        for ( int i = 0; i < m_eventArgsDataList.GetCount(); ++i )
        {
            if ( m_eventArgsDataList[i]->pEventArgsDef == pEventData->pEventDef->GetEventArgsDef() )
            {
                char *pName = const_cast<char *>( m_eventArgsDataList[i]->displayName.c_str() );
                m_eventEventArgsComboBox.Items.Add( pName );
                m_eventArgsDataComboBoxList.Grow() = m_eventArgsDataList[i];
            }
        }

        if ( pEventData->pEventArgsData != NULL )
        {
            m_eventEventArgsComboBox.ItemIndex = m_eventArgsDataComboBoxList.Find( pEventData->pEventArgsData );
        }
    }

    SetHaveUnsavedEventChanges( false );
}

bool EditEventPopup::SetEventFromUI( SEditEventData *pEventData )
{
    m_pCurrentEventData = pEventData;

    if ( pEventData->pEvent->GetType() == CUTSCENE_OBJECT_ID_EVENT_TYPE )
    {
        if ( m_eventRecipientComboBox.ItemIndex != -1 )
        {
            cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<cutfObjectIdEvent *>( pEventData->pEvent );
            const atArray<cutfObject *> &objectList = m_pCutFile->GetObjectList();
            pObjectIdEvent->SetObjectId( objectList[m_eventRecipientComboBox.ItemIndex]->GetObjectId() );
        }
        else
        {
            FBMessageBox( "Event Recipient Required", "This Event needs a Recipient.  Please select one and try again.", "OK" );
            return false;
        }
    }

	if ( (pEventData->pEventDef != NULL) && (pEventData->pEventDef->GetEventArgsDef() != NULL) )
	{
		if ( m_eventEventArgsComboBox.ItemIndex != -1 )
		{
			SEditEventArgsData *pEventArgsData = m_eventArgsDataComboBoxList[m_eventEventArgsComboBox.ItemIndex];
			pEventData->pEvent->SetEventArgs( pEventArgsData->pEventArgs );
			pEventData->pEventArgsData = pEventArgsData;
		}
		else
		{
			char msg[128];
			sprintf( msg, "This Event requires an EventArgs of type '%s'.  Please select or create one, then select it and try again.",
				pEventData->pEventDef->GetEventArgsDef()->GetName() );
			FBMessageBox( "Event Args Required", msg, "OK" );

			return false;
		}
	}

    pEventData->bIsLoadEvent = m_eventTypeCheckBox.State == 0 ? false : true;

    pEventData->pEvent->SetTime( ((float) m_eventFrameNumericUpDown.Value) / rexMBAnimExportCommon::GetFPS() );

    if ( GetEventEditMode() == EDIT_EVENT_MODE )
    {
        UpdateEventInSpreadsheet( pEventData );
    }
    else
    {
        AddRowToEventSpreadsheet( pEventData );

        if ( m_pCurrentEventData->bIsLoadEvent )
        {
            m_pCutFile->AddLoadEvent( m_pCurrentEventData->pEvent );
        }
        else
        {
            m_pCutFile->AddEvent( m_pCurrentEventData->pEvent );
        }
    }

    UpdateUIFromEventArgsReferenceCount();

    SetHaveUnsavedEventChanges( false );

    return true;
}

void EditEventPopup::SetUIFromEventArgs( SEditEventArgsData *pEventArgsData )
{
    m_pCurrentEventArgsData = pEventArgsData;

    if ( pEventArgsData == NULL )
    {
        m_eventArgsNameTextBox.Caption = "";
        ClearAttributeSpreadsheet();

        SetHaveUnsavedEventArgsChanges( false );

        return;
    }

    m_attributeNameTextBox.Text = "";
    m_attributeTypeComboBox.ItemIndex = 0;
    m_attributeValueTextBox.Text = "";

    m_eventArgsNameTextBox.Caption = const_cast<char *>( pEventArgsData->displayName.c_str() );

    ClearAttributeSpreadsheet();

    CopyToAttributeList( pEventArgsData );

    atArray<parAttribute> &attList = m_currentAttributeList.GetAttributeArray();
    for ( int i = 0; i < attList.GetCount(); ++i )
    {
        AddRowToAttributeSpreadsheet( &attList[i] );
    }

    SetHaveUnsavedEventArgsChanges( false );
}

bool EditEventPopup::SetEventArgsFromUI( SEditEventArgsData *pEventArgsData )
{
    m_pCurrentEventArgsData = pEventArgsData;

    CopyFromAttributeList( pEventArgsData );

    if ( GetEventArgsEditMode() == EDIT_EVENT_ARGS_MODE )
    {
        // nothing more to do
    }
    else
    {
        AddRowToEventArgsSpreadsheet( pEventArgsData );

        m_pCutFile->AddEventArgs( pEventArgsData->pEventArgs );
    }

    SetHaveUnsavedEventArgsChanges( false );

    return true;
}

void EditEventPopup::SetUIFromAttribute( parAttribute *pAttribute )
{
    m_pCurrentAttribute = pAttribute;

    if ( pAttribute == NULL )
    {
        m_attributeNameTextBox.Text = "";
        m_attributeTypeComboBox.ItemIndex = 0;
        m_attributeValueTextBox.Text = "";

        SetHaveUnsavedAttributeChanges( false );

        return;
    }

    m_attributeNameTextBox.Text = const_cast<char *>( pAttribute->GetName() );

    m_attributeTypeComboBox.ItemIndex = pAttribute->GetType();

    char cText[128];
    const char *pStringRepr = pAttribute->GetStringRepr( cText, sizeof(cText) );
    if ( pStringRepr != NULL )
    {
        m_attributeValueTextBox.Text = const_cast<char *>( pStringRepr );
    }
    else
    {
        m_attributeValueTextBox.Text = "";
    }

    SetHaveUnsavedAttributeChanges( false );
}

bool EditEventPopup::SetAttributeFromUI( parAttribute *pAttribute )
{
    m_pCurrentAttribute = pAttribute;

    if ( GetEventArgsEditMode() == ADD_EVENT_ARGS_ATTRIBUTE_MODE )
    {
        // make sure we have a name
        if ( strlen( (const char*)m_attributeNameTextBox.Text ) == 0 )
        {
            FBMessageBox( "Event Args Attribute Name Required", 
                "You are required to provide a name for each event args attribute.  Please add one and save again.", "OK" );
            return false;
        }

        char cName[CUTSCENE_LONG_OBJNAMELEN];
        safecpy( cName, (const char*)m_attributeNameTextBox.Text, sizeof(cName) );

        // Check for name collisions
        const parAttribute *pConstAtt = m_currentAttributeList.FindAttribute( cName );
        if ( pConstAtt != NULL )
        {
            FBMessageBox( "Event Args Attribute Name Collision", 
                "The event args attribute names must be unique.  Please change it and save again.", "OK" );
            return false;
        }

        m_currentAttributeList.AddAttribute( cName, m_attributeValueTextBox.Text.AsString() );
        pAttribute = m_currentAttributeList.FindAttribute( cName );
    }
    else
    {
        pAttribute->SetValue( m_attributeValueTextBox.Text.AsString() );
    }

    if ( pAttribute != NULL )
    {
        switch ( m_attributeTypeComboBox.ItemIndex )
        {
        case parAttribute::BOOL:
            pAttribute->ConvertToBool();
            break;
        case parAttribute::INT64:
            pAttribute->ConvertToInt64();
            break;
        case parAttribute::DOUBLE:
            pAttribute->ConvertToDouble();
            break;
        case parAttribute::STRING:
            break;
        }
    }

    if ( GetEventArgsEditMode() == ADD_EVENT_ARGS_ATTRIBUTE_MODE )
    {
        AddRowToAttributeSpreadsheet( pAttribute );
    }
    else
    {
        int *pRowRef = m_attributeRowRefMap.Access( pAttribute );
        if ( pRowRef != NULL )
        {
            m_attributeSpreadsheet.SetCell( *pRowRef, ATTRIBUTE_VALUE_SPREADSHEET_COLUMN, m_attributeValueTextBox.Text.AsString() );
        }
    }

    SetHaveUnsavedAttributeChanges( false );
    SetHaveUnsavedEventArgsChanges( true );

    return true;
}

bool EditEventPopup::PromptToSaveEvent( bool bAllowCancel )
{
    if ( HaveUnsavedEventChanges() )
    {
        int result = FBMessageBox( "Unsaved Event Changes", "You have unsaved edits to an event.  Would you like to save the changes before continuing?",
            "Yes", "No", bAllowCancel ? "Cancel" : NULL );
        if ( result == 1 )
        {
            SaveEventChangesButton_Click( NULL, NULL );
        }
        else if ( result == 3 )
        {
            return false;
        }
    }

    return true;
}

bool EditEventPopup::PromptToSaveEventArgs( bool bAllowCancel )
{
    if ( HaveUnsavedEventArgsChanges() || HaveUnsavedAttributeChanges() )
    {
        int result = FBMessageBox( "Unsaved Event Args Changes", "You have unsaved edits to an event args.  Would you like to save the changes before continuing?",
            "Yes", "No", bAllowCancel ? "Cancel" : NULL );
        if ( result == 1 )
        {
            if ( this->HaveUnsavedAttributeChanges() )
            {
                DoneEditAttributeButton_Click( NULL, NULL );
            }

            SaveEventArgsChangesButton_Click( NULL, NULL );
        }
        else if ( result == 3 )
        {
            return false;
        }
    }

    return true;
}

bool EditEventPopup::PromptToSaveAttribute( bool bAllowCancel )
{
    if ( HaveUnsavedAttributeChanges() )
    {
        int result = FBMessageBox( "Unsaved Event Args Attribute Changes", "You have unsaved edits to an event args attribute.  Would you like to save the changes before continuing?",
            "Yes", "No", bAllowCancel ? "Cancel" : NULL );
        if ( result == 1 )
        {
            if ( !SetAttributeFromUI( m_pCurrentAttribute ) )
            {
                return false;
            }
            else
            {
                SetEventArgsEditMode( m_lastEventArgsEditMode );
            }
        }
        else if ( result == 3 )
        {
            return false;
        }
        else
        {
            SetEventArgsEditMode( m_lastEventArgsEditMode );
        }
    }

    return true;
}

void EditEventPopup::CopyToAttributeList( SEditEventArgsData *pEventArgsData )
{
    m_currentAttributeList.Reset();
    m_iRestrictedFromEditingAttributeCount = 0;
    m_iRestrictedFromRemovalAttributeCount = 0;

    if ( pEventArgsData->pEventArgsDef != NULL )
    {
        m_currentAttributeList.CopyFrom( pEventArgsData->pEventArgsDef->GetAttributeList() );        

        parAttribute *pAtt = NULL;

        // add the event args attribute defs, if any
        switch ( pEventArgsData->pEventArgsDef->GetEventArgsType() )
        {
        case CUTSCENE_GENERIC_EVENT_ARGS_TYPE:
        case CUTSCENE_EVENT_ARGS_LIST_TYPE:
            {
                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 0;
            }
            break;
        case CUTSCENE_NAME_EVENT_ARGS_TYPE:
            {
                const cutfNameEventArgs *pNameEventArgs = dynamic_cast<const cutfNameEventArgs *>( pEventArgsData->pEventArgs );
                pAtt = m_currentAttributeList.FindAttribute( "Name" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pNameEventArgs->GetName().GetCStr() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 1;
            }
            break;
        case CUTSCENE_FLOAT_VALUE_EVENT_ARGS_TYPE:
            {
                const cutfFloatValueEventArgs *pFloatValueArgs = dynamic_cast<const cutfFloatValueEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "Value" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pFloatValueArgs->GetFloat1() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 1;
            }
            break;
        case CUTSCENE_TWO_FLOAT_VALUES_EVENT_ARGS_TYPE:
            {
                const cutfTwoFloatValuesEventArgs *pTwoFloatValuesArgs = dynamic_cast<const cutfTwoFloatValuesEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "Value1" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pTwoFloatValuesArgs->GetFloat1() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "Value2" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pTwoFloatValuesArgs->GetFloat2() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 2;
            }
            break;
        case CUTSCENE_SCREEN_FADE_EVENT_ARGS_TYPE:
            {
                const cutfScreenFadeEventArgs *pScreenFadeArgs = dynamic_cast<const cutfScreenFadeEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "Duration" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pScreenFadeArgs->GetFloat1() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "ColorR" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (s64)pScreenFadeArgs->GetColor().GetRed() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "ColorG" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (s64)pScreenFadeArgs->GetColor().GetGreen() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "ColorB" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (s64)pScreenFadeArgs->GetColor().GetBlue() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 4;
            }
            break;
        case CUTSCENE_LOAD_SCENE_EVENT_ARGS_TYPE:
            {
                // cutfLoadSceneEventArgs
                const cutfLoadSceneEventArgs *pLoadSceneEventArgs = dynamic_cast<const cutfLoadSceneEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "SceneName" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pLoadSceneEventArgs->GetName().GetCStr() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "OffsetX" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pLoadSceneEventArgs->GetOffset().GetX() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "OffsetY" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pLoadSceneEventArgs->GetOffset().GetY() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "OffsetZ" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pLoadSceneEventArgs->GetOffset().GetZ() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "Rotation" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pLoadSceneEventArgs->GetRotation() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 9;
            }
            break;
        case CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE:
            {
                const cutfObjectIdEventArgs *pObjectIdArgs = dynamic_cast<const cutfObjectIdEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "ObjectId" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (s64)pObjectIdArgs->GetObjectId() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 1;
            }
            break;
        case CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE:
            {
                const cutfObjectIdNameEventArgs *pObjectIdNameArgs = dynamic_cast<const cutfObjectIdNameEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "ObjectId" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (s64)pObjectIdNameArgs->GetObjectId() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "Name" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pObjectIdNameArgs->GetName().GetCStr() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 2;
            }
            break;
        case CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE:
            {
                const cutfAttachmentEventArgs *pAttachmentArgs = dynamic_cast<const cutfAttachmentEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "ObjectId" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (s64)pAttachmentArgs->GetObjectId() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "BoneName" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pAttachmentArgs->GetBoneName().GetCStr() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 2;
            }
            break;
        case CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE:
            {
                const cutfObjectIdListEventArgs *pObjectIdListArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEventArgsData->pEventArgs );

                // add one for each object id
                const atArray<s32> &objectIdList = pObjectIdListArgs->GetObjectIdList();
                for ( int i = 0; i < objectIdList.GetCount(); ++i )
                {
                    char cName[CUTSCENE_LONG_OBJNAMELEN];
                    sprintf( cName, "ObjectId_%d", i );

                    m_currentAttributeList.AddAttribute( cName, objectIdList[i] );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = objectIdList.GetCount();
            }
            break;
        case CUTSCENE_SUBTITLE_EVENT_ARGS_TYPE:
            {
                const cutfSubtitleEventArgs *pSubtitleEventArgs = dynamic_cast<const cutfSubtitleEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "Identifier" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pSubtitleEventArgs->GetName().GetCStr() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "Language" );
                if ( pAtt != NULL )
                {
                    if ( (pSubtitleEventArgs->GetLanguageID() >= 0) && (pSubtitleEventArgs->GetLanguageID() < c_numLanguageNames) )
                    {
                        pAtt->SetValue( c_languageNames[pSubtitleEventArgs->GetLanguageID()] );
                    }
                    else
                    {
                        pAtt->SetValue( "(out of range)" );
                    }
                }

                pAtt = m_currentAttributeList.FindAttribute( "TransitionIn" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (s64)pSubtitleEventArgs->GetTransitionIn() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "TransitionInDuration" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (double)pSubtitleEventArgs->GetTransitionInDuration() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "TransitionOut" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (s64)pSubtitleEventArgs->GetTransitionOut() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "TransitionOutDuration" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( (double)pSubtitleEventArgs->GetTransitionOutDuration() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 6;
            }
            break;
		case CUTSCENE_DECAL_EVENT_ARGS:
			{
				const cutfDecalEventArgs *pDecalEventArgs = dynamic_cast<const cutfDecalEventArgs *>( pEventArgsData->pEventArgs );

				pAtt = m_currentAttributeList.FindAttribute( "PositionX" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetPosition().GetX() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "PositionY" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetPosition().GetY() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "PositionZ" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetPosition().GetZ() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "RotationQuaternionX" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetRotationQuaternion().GetX() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "RotationQuaternionY" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetRotationQuaternion().GetY() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "RotationQuaternionZ" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetRotationQuaternion().GetZ() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "RotationQuaternionW" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetRotationQuaternion().GetW() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "Height" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetHeight() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "Width" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetWidth() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "LifeTime" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( pDecalEventArgs->GetLifeTime() );
				}
			}
			break;
        case CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE:
            {
                const cutfCameraCutEventArgs *pCameraCutEventArgs = dynamic_cast<const cutfCameraCutEventArgs *>( pEventArgsData->pEventArgs );

                pAtt = m_currentAttributeList.FindAttribute( "Name" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetName().GetCStr() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "PositionX" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetPosition().GetX() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "PositionY" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetPosition().GetY() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "PositionZ" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetPosition().GetZ() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "RotationQuaternionX" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetRotationQuaternion().GetX() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "RotationQuaternionY" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetRotationQuaternion().GetY() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "RotationQuaternionZ" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetRotationQuaternion().GetZ() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "RotationQuaternionW" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetRotationQuaternion().GetW() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "NearDrawDistance" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetNearDrawDistance() );
                }

                pAtt = m_currentAttributeList.FindAttribute( "FarDrawDistance" );
                if ( pAtt != NULL )
                {
                    pAtt->SetValue( pCameraCutEventArgs->GetFarDrawDistance() );
                }

                m_iRestrictedFromEditingAttributeCount = m_iRestrictedFromRemovalAttributeCount = 10;
            }
            break;
		case CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE:
			{
				const cutfObjectVariationEventArgs *pVariationEventArgs = dynamic_cast<const cutfObjectVariationEventArgs *>( pEventArgsData->pEventArgs );

				pAtt = m_currentAttributeList.FindAttribute( "ObjectId" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVariationEventArgs->GetObjectId() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "Component" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVariationEventArgs->GetComponent() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "Drawable" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVariationEventArgs->GetDrawable() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "Texture" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVariationEventArgs->GetTexture() );
				}
			}
			break;
		case CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE:
			{
				const cutfVehicleVariationEventArgs *pVehicleVariationEventArgs = dynamic_cast<const cutfVehicleVariationEventArgs *>( pEventArgsData->pEventArgs );

				pAtt = m_currentAttributeList.FindAttribute( "MainBodyColour" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVehicleVariationEventArgs->GetBodyColour() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "SecondBodyColour" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVehicleVariationEventArgs->GetSecondaryBodyColour() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "SpecularColour" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVehicleVariationEventArgs->GetSpecularBodyColour() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "WheelTrimColour" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVehicleVariationEventArgs->GetWheelTrimColour() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "Livery" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (s64)pVehicleVariationEventArgs->GetLiveryId() );
				}

				pAtt = m_currentAttributeList.FindAttribute( "DirtLevel" );
				if ( pAtt != NULL )
				{
					pAtt->SetValue( (double)pVehicleVariationEventArgs->GetDirtLevel() );
				}
			}
			break;
		case CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE:
			{
				const cutfVehicleExtraEventArgs *pVariationEventArgs = dynamic_cast<const cutfVehicleExtraEventArgs *>( pEventArgsData->pEventArgs );

				const atArray<rage::s32> &boneIdList = pVariationEventArgs->GetBoneIdList();
				for ( int i = 0; i < boneIdList.GetCount(); ++i )
				{
					char cName[CUTSCENE_LONG_OBJNAMELEN];
					sprintf( cName, "BoneId_%d", i );

					m_currentAttributeList.AddAttribute( cName, boneIdList[i] );
				}
			}
			break;
        default:
            {
                // find the attributes from the event args def
                atArray<parAttribute> &attList = const_cast<atArray<parAttribute> &>( m_currentAttributeList.GetAttributeArray() );
                for ( int i = 0; i < attList.GetCount(); ++i )
                {
                    const parAttribute *pConstAtt = pEventArgsData->pEventArgs->GetAttributeList().FindAttribute( attList[i].GetName() );
                    if ( pConstAtt != NULL )
                    {
                        switch ( attList[i].GetType() )
                        {
                        case parAttribute::BOOL:
                            attList[i].SetValue( pConstAtt->GetBoolValue() );
                            break;
                        case parAttribute::DOUBLE:
                            attList[i].SetValue( pConstAtt->GetFloatValue() );
                            break;
                        case parAttribute::INT64:
                            attList[i].SetValue( (s64)pConstAtt->GetIntValue() );
                            break;
                        case parAttribute::STRING:
                            attList[i].SetValue( pConstAtt->GetStringValue() );
                            break;
                        }
                    }
                }

                m_iRestrictedFromEditingAttributeCount = 0;
                m_iRestrictedFromRemovalAttributeCount = attList.GetCount();
            }
            break;
        }
    }

    // find the attributes that the user added
    const atArray<parAttribute> &attList = pEventArgsData->pEventArgs->GetAttributeList().GetAttributeArray();
    for ( int i = 0; i < attList.GetCount(); ++i )
    {
        if ( m_currentAttributeList.FindAttribute( attList[i].GetName() ) == NULL )
        {
            switch ( attList[i].GetType() )
            {
            case parAttribute::BOOL:
                m_currentAttributeList.AddAttribute( attList[i].GetName(), attList[i].GetBoolValue() );
                break;
            case parAttribute::DOUBLE:
                m_currentAttributeList.AddAttribute( attList[i].GetName(), attList[i].GetFloatValue() );
                break;
            case parAttribute::INT64:
                m_currentAttributeList.AddAttribute( attList[i].GetName(), attList[i].GetIntValue() );
                break;
            case parAttribute::STRING:
                m_currentAttributeList.AddAttribute( attList[i].GetName(), attList[i].GetStringValue() );
                break;
            }
        }
    }
}

//void EditEventPopup::CopyFromAttributeList( SEditEventArgsData *pEventArgsData )
//{
//    if ( pEventArgsData->pEventArgsDef != NULL )
//    {
//        // find the attributes from the event args def
//        const atArray<parAttribute> &attList = pEventArgsData->pEventArgsDef->GetAttributeList().GetAttributeArray();
//        for ( int i = 0; i < attList.GetCount(); ++i )
//        {
//            const parAttribute *pConstAtt = m_currentAttributeList.FindAttribute( attList[i].GetName() );
//
//			cutfObjectVariationEventArgs* pActorEventArgs = dynamic_cast<cutfObjectVariationEventArgs *>( pEventArgsData->pEventArgs );
//
//			if(pActorEventArgs)
//			{
//				if(strcmpi(pConstAtt->GetName(),"Texture") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//			        {
//			        case parAttribute::INT:
//			            pActorEventArgs->SetTexture( pConstAtt->GetIntValue() );
//			            break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//				else if(strcmpi(pConstAtt->GetName(),"Component") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pActorEventArgs->SetComponent( pConstAtt->GetIntValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//				else if(strcmpi(pConstAtt->GetName(),"ObjectId") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pActorEventArgs->SetObjectId( pConstAtt->GetIntValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//				else if(strcmpi(pConstAtt->GetName(),"Drawable") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pActorEventArgs->SetDrawable( pConstAtt->GetIntValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//			}
//
//			cutfVehicleVariationEventArgs* pVehicleEventArgs = dynamic_cast<cutfVehicleVariationEventArgs *>( pEventArgsData->pEventArgs );
//
//			if(pVehicleEventArgs)
//			{
//				if(strcmpi(pConstAtt->GetName(),"MainBodyColour") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pVehicleEventArgs->SetBodyColour( pConstAtt->GetIntValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//				else if(strcmpi(pConstAtt->GetName(),"SecondBodyColour") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pVehicleEventArgs->SetSecondaryBodyColour( pConstAtt->GetIntValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//				else if(strcmpi(pConstAtt->GetName(),"SpecularColour") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pVehicleEventArgs->SetSpecularColour( pConstAtt->GetIntValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//				else if(strcmpi(pConstAtt->GetName(),"WheelTrimColour") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pVehicleEventArgs->SetWheelTrimColour( pConstAtt->GetIntValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//				else if(strcmpi(pConstAtt->GetName(),"Livery") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pVehicleEventArgs->SetLiveryId( pConstAtt->GetIntValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//				else if(strcmpi(pConstAtt->GetName(),"DirtLevel") == 0)
//				{
//					switch ( pConstAtt->GetType() )
//					{
//					case parAttribute::INT:
//						pVehicleEventArgs->SetDirtLevel( pConstAtt->GetFloatValue() );
//						break;
//					case parAttribute::BOOL:
//					case parAttribute::FLOAT:
//					case parAttribute::STRING:
//						break;
//					}
//				}
//			}
//
//            //pAtt = const_cast<parAttribute *>( pEventArgsData->pEventArgs->GetAttributeList().FindAttribute( attList[i].GetName() ) );
//
//            //if ( pConstAtt != NULL )
//            //{
//            //    if ( pAtt != NULL )
//            //    {
//            //        switch ( pConstAtt->GetType() )
//            //        {
//            //        case parAttribute::BOOL:
//            //            pAtt->SetValue( pConstAtt->GetBoolValue() );
//            //            break;
//            //        case parAttribute::FLOAT:
//            //            pAtt->SetValue( pConstAtt->GetFloatValue() );
//            //            break;
//            //        case parAttribute::INT:
//            //            pAtt->SetValue( pConstAtt->GetIntValue() );
//            //            break;
//            //        case parAttribute::STRING:
//            //            pAtt->SetValue( pConstAtt->GetStringValue() );
//            //            break;
//            //        }
//            //    }
//            //    else
//            //    {
//            //        parAttributeList &atts = const_cast<parAttributeList &>( pEventArgsData->pEventArgs->GetAttributeList() );
//            //        switch ( pConstAtt->GetType() )
//            //        {
//            //        case parAttribute::BOOL:
//            //            atts.AddAttribute( pConstAtt->GetName(), pConstAtt->GetBoolValue() );
//            //            break;
//            //        case parAttribute::FLOAT:
//            //            atts.AddAttribute( pConstAtt->GetName(), pConstAtt->GetFloatValue() );
//            //            break;
//            //        case parAttribute::INT:
//            //            atts.AddAttribute( pConstAtt->GetName(), pConstAtt->GetIntValue() );
//            //            break;
//            //        case parAttribute::STRING:
//            //            atts.AddAttribute( pConstAtt->GetName(), pConstAtt->GetStringValue() );
//            //            break;
//            //        }
//            //    }
//            //}
//        }
//    }
//
//    // find the attributes that the user added
//    //atArray<parAttribute> &currentAttList = m_currentAttributeList.GetAttributeArray();
//    //parAttributeList &eventArgsAttList = const_cast<parAttributeList &>( pEventArgsData->pEventArgs->GetAttributeList() );
//    //for ( int i = 0; i < currentAttList.GetCount(); ++i )
//    //{
//    //    if ( pEventArgsData->pEventArgsDef->GetAttributeList().FindAttribute( currentAttList[i].GetName() ) == NULL )
//    //    {
//    //        parAttribute *pAtt = const_cast<parAttribute *>( eventArgsAttList.FindAttribute( currentAttList[i].GetName() ) );
//    //        if ( pAtt != NULL )
//    //        {
//    //            switch ( pAtt->GetType() )
//    //            {
//    //            case parAttribute::BOOL:
//    //                pAtt->SetValue( currentAttList[i].GetBoolValue() );
//    //                break;
//    //            case parAttribute::FLOAT:
//    //                pAtt->SetValue( currentAttList[i].GetFloatValue() );
//    //                break;
//    //            case parAttribute::INT:
//    //                pAtt->SetValue( currentAttList[i].GetIntValue() );
//    //                break;
//    //            case parAttribute::STRING:
//    //                pAtt->SetValue( currentAttList[i].GetStringValue() );
//    //                break;
//    //            }
//    //        }
//    //        else
//    //        {
//    //            switch ( currentAttList[i].GetType() )
//    //            {
//    //            case parAttribute::BOOL:
//    //                eventArgsAttList.AddAttribute( currentAttList[i].GetName(), currentAttList[i].GetBoolValue() );
//    //                break;
//    //            case parAttribute::FLOAT:
//    //                eventArgsAttList.AddAttribute( currentAttList[i].GetName(), currentAttList[i].GetFloatValue() );
//    //                break;
//    //            case parAttribute::INT:
//    //                eventArgsAttList.AddAttribute( currentAttList[i].GetName(), currentAttList[i].GetIntValue() );
//    //                break;
//    //            case parAttribute::STRING:
//    //                eventArgsAttList.AddAttribute( currentAttList[i].GetName(), currentAttList[i].GetStringValue() );
//    //                break;
//    //            }
//    //        }
//    //    }
//    //}
//
//    //// find the attributes that the user removed
//    //atArray<parAttribute> &attList = eventArgsAttList.GetAttributeArray();
//    //for ( int i = 0; i < attList.GetCount(); )
//    //{
//    //    if ( (pEventArgsData->pEventArgsDef->GetAttributeList().FindAttribute( attList[i].GetName() ) == NULL) 
//    //        && (m_currentAttributeList.FindAttribute( attList[i].GetName() ) == NULL) )
//    //    {
//    //        attList.Delete( i );
//    //    }
//    //    else
//    //    {
//    //        ++i;
//    //    }
//    //}
//}

void EditEventPopup::CopyFromAttributeList( SEditEventArgsData *pEventArgsData )
{
	if ( pEventArgsData->pEventArgsDef != NULL )
	{
		parAttribute *pAtt = NULL;

		// add the event args attribute defs, if any
		if ( pEventArgsData->pEventArgsDef->GetEventArgsType() >= CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE )
		{
			// find the attributes from the event args def
			const atArray<parAttribute> &attList = pEventArgsData->pEventArgsDef->GetAttributeList().GetAttributeArray();
			for ( int i = 0; i < attList.GetCount(); ++i )
			{
				const parAttribute *pConstAtt = m_currentAttributeList.FindAttribute( attList[i].GetName() );
				pAtt = const_cast<parAttribute *>( pEventArgsData->pEventArgs->GetAttributeList().FindAttribute( attList[i].GetName() ) );

				if ( pConstAtt != NULL )
				{
					if ( pAtt != NULL )
					{
						switch ( pConstAtt->GetType() )
						{
						case parAttribute::BOOL:
							pAtt->SetValue( pConstAtt->GetBoolValue() );
							break;
						case parAttribute::DOUBLE:
							pAtt->SetValue( pConstAtt->GetFloatValue() );
							break;
						case parAttribute::INT64:
							pAtt->SetValue( (s64)pConstAtt->GetIntValue() );
							break;
						case parAttribute::STRING:
							pAtt->SetValue( pConstAtt->GetStringValue() );
							break;
						}
					}
					else
					{
						parAttributeList &atts = const_cast<parAttributeList &>( pEventArgsData->pEventArgs->GetAttributeList() );
						switch ( pConstAtt->GetType() )
						{
						case parAttribute::BOOL:
							atts.AddAttribute( pConstAtt->GetName(), pConstAtt->GetBoolValue() );
							break;
						case parAttribute::DOUBLE:
							atts.AddAttribute( pConstAtt->GetName(), pConstAtt->GetFloatValue() );
							break;
						case parAttribute::INT64:
							atts.AddAttribute( pConstAtt->GetName(), pConstAtt->GetIntValue() );
							break;
						case parAttribute::STRING:
							atts.AddAttribute( pConstAtt->GetName(), pConstAtt->GetStringValue() );
							break;
						}
					}
				}
			}
		}
	}

	// find the attributes that the user added
	atArray<parAttribute> &currentAttList = m_currentAttributeList.GetAttributeArray();
	parAttributeList &eventArgsAttList = const_cast<parAttributeList &>( pEventArgsData->pEventArgs->GetAttributeList() );
	for ( int i = 0; i < currentAttList.GetCount(); ++i )
	{
		if ( pEventArgsData->pEventArgsDef->GetAttributeList().FindAttribute( currentAttList[i].GetName() ) == NULL )
		{
			parAttribute *pAtt = const_cast<parAttribute *>( eventArgsAttList.FindAttribute( currentAttList[i].GetName() ) );
			if ( pAtt != NULL )
			{
				switch ( pAtt->GetType() )
				{
				case parAttribute::BOOL:
					pAtt->SetValue( currentAttList[i].GetBoolValue() );
					break;
				case parAttribute::DOUBLE:
					pAtt->SetValue( currentAttList[i].GetFloatValue() );
					break;
				case parAttribute::INT64:
					pAtt->SetValue( (s64)currentAttList[i].GetIntValue() );
					break;
				case parAttribute::STRING:
					pAtt->SetValue( currentAttList[i].GetStringValue() );
					break;
				}
			}
			else
			{
				switch ( currentAttList[i].GetType() )
				{
				case parAttribute::BOOL:
					eventArgsAttList.AddAttribute( currentAttList[i].GetName(), currentAttList[i].GetBoolValue() );
					break;
				case parAttribute::DOUBLE:
					eventArgsAttList.AddAttribute( currentAttList[i].GetName(), currentAttList[i].GetFloatValue() );
					break;
				case parAttribute::INT64:
					eventArgsAttList.AddAttribute( currentAttList[i].GetName(), currentAttList[i].GetIntValue() );
					break;
				case parAttribute::STRING:
					eventArgsAttList.AddAttribute( currentAttList[i].GetName(), currentAttList[i].GetStringValue() );
					break;
				}
			}
		}
	}

	// find the attributes that the user removed
	atArray<parAttribute> &attList = eventArgsAttList.GetAttributeArray();
	for ( int i = 0; i < attList.GetCount(); )
	{
		if ( (pEventArgsData->pEventArgsDef->GetAttributeList().FindAttribute( attList[i].GetName() ) == NULL) 
			&& (m_currentAttributeList.FindAttribute( attList[i].GetName() ) == NULL) )
		{
			attList.Delete( i );
		}
		else
		{
			++i;
		}
	}
}

void EditEventPopup::EventSpreadsheet_RowClick( HISender /*pSender*/, HKEvent pEvent )
{
    FBEventSpread e( pEvent );    
    
    SEditEventData **ppEventData = m_rowRefEventDataMap.Access( e.Row );
    if ( ppEventData != NULL )
    {
        if ( PromptToSaveEvent( false ) )
        {
            SetUIFromEvent( *ppEventData );
        }

        //m_addEventButton.Enabled = true;
		m_removeEventButton.Enabled = true;
		m_editEventButton.Enabled = true;


        //bool bIsCustomEvent = (*ppEventData)->pEvent->GetEventId() >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
		//m_editEventButton.Enabled = bIsCustomEvent;
        //m_removeEventButton.Enabled = bIsCustomEvent;
    }
    else
    {
        m_addEventButton.Enabled = false;
        m_editEventButton.Enabled = false;
        m_removeEventButton.Enabled = false;

        SetUIFromEvent( NULL );
    }
}

void EditEventPopup::AddEventButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( PromptToSaveEvent( true ) )
    {
        NewEventPopup popup( m_pEventDefs );
        if ( popup.Show( this ) )
        {
            SEditEventData *pEventData = CreateEventData( 
                rage_new cutfObjectIdEvent( -1, 0.0f, popup.GetEventDef()->GetEventId() ) );
            if ( pEventData != NULL )
            {
                SetUIFromEvent( pEventData );
                SetEventEditMode( ADD_EVENT_MODE );
                SetHaveUnsavedEventChanges( true );
            }
        }
    }
}

void EditEventPopup::EditEventButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( (m_pCurrentEventData != NULL) && PromptToSaveEvent( true ) )
    {
        SetUIFromEvent( m_pCurrentEventData );
        SetEventEditMode( EDIT_EVENT_MODE );
    }
}

void EditEventPopup::RemoveEventButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( (m_pCurrentEventData != NULL) && PromptToSaveEvent( true ) )
    {
        RemoveRowFromEventSpreadsheet( m_pCurrentEventData );

        DestroyEventData( m_pCurrentEventData );

        UpdateUIFromEventArgsReferenceCount();

        m_editEventButton.Enabled = false;
        m_removeEventButton.Enabled = false;

        SetUIFromEvent( NULL );

        m_bModifiedCutfile = true;

        SetEventEditMode( NO_EVENT_MODE );
    }
}

void EditEventPopup::SortEventsButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    Clear();

    m_pCutFile->SortLoadEvents();
    m_pCutFile->SortEvents();

    RefreshUI();
}

void EditEventPopup::EventTypeCheckBox_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    SetHaveUnsavedEventChanges( true );
}

void EditEventPopup::EventRecipientComboBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    SetHaveUnsavedEventChanges( true );
}

void EditEventPopup::EventFrameNumericUpDown_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    SetHaveUnsavedEventChanges( true );
}

void EditEventPopup::EventEventArgsComboBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    SetHaveUnsavedEventChanges( true );
}

void EditEventPopup::SaveEventChangesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( SetEventFromUI( m_pCurrentEventData ) )
    {
        m_bModifiedCutfile = true;

        SetEventEditMode( NO_EVENT_MODE );
    }
}

void EditEventPopup::DiscardEventChangesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    switch ( GetEventEditMode() )
    {
    case NO_EVENT_MODE:
        // we should never get here
        break;
    case ADD_EVENT_MODE:
        {
            delete m_pCurrentEventData->pEvent;
            delete m_pCurrentEventData;
            SetUIFromEvent( NULL );
        }
        break;
    case EDIT_EVENT_MODE:
        {
            SetUIFromEvent( m_pCurrentEventData );
        }
        break;
    }

    SetEventEditMode( NO_EVENT_MODE );
}

void EditEventPopup::EventArgsSpreadsheet_RowClick( HISender /*pSender*/, HKEvent pEventArgs )
{
    FBEventSpread e( pEventArgs );
  
    SEditEventArgsData **ppEventArgsData = m_rowRefEventArgsDataMap.Access( e.Row );
    if ( ppEventArgsData != NULL )
    {
        if ( PromptToSaveEventArgs( false ) )
        {
            SetUIFromEventArgs( *ppEventArgsData );
        }

        //m_addEventArgsButton.Enabled = true;
        m_editEventArgsButton.Enabled = true;
        m_removeEventArgsButton.Enabled 
            = (m_builtInEventArgsDefsMap.Access( (*ppEventArgsData)->pEventArgsDef->GetEventArgsType() ) == NULL) 
            && ((*ppEventArgsData)->pEventArgs->GetRef() == 0);
    }
    else
    {
        m_addEventArgsButton.Enabled = false;
        m_editEventArgsButton.Enabled = false;
        m_removeEventArgsButton.Enabled = false;

        SetUIFromEventArgs( NULL );
    }
}

void EditEventPopup::AddEventArgsButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    if ( PromptToSaveEventArgs( true ) )
    {
        NewEventArgsPopup popup( m_pEventDefs );
        if ( popup.Show( this ) )
        {
            SEditEventArgsData *pEventArgsData = CreateEventArgsData( rage_new cutfEventArgs(), popup.GetEventArgsDef() );
            if ( pEventArgsData != NULL )
            {
                SetUIFromEventArgs( pEventArgsData );
                SetEventArgsEditMode( ADD_EVENT_ARGS_MODE );
                SetHaveUnsavedEventArgsChanges( true );
            }
        }
    }
}

void EditEventPopup::EditEventArgsButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    if ( (m_pCurrentEventArgsData != NULL) && PromptToSaveEventArgs( true ) )
    {
        SetUIFromEventArgs( m_pCurrentEventArgsData );
        SetEventArgsEditMode( EDIT_EVENT_ARGS_MODE );
    }
}

void EditEventPopup::RemoveEventArgsButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    if ( (m_pCurrentEventArgsData != NULL) && PromptToSaveEventArgs( true ) )
    {       
        RemoveRowFromEventArgsSpreadsheet( m_pCurrentEventArgsData );

        DestroyEventArgsData( m_pCurrentEventArgsData );

        m_editEventArgsButton.Enabled = false;
        m_removeEventArgsButton.Enabled = false;

        atArray<cutfEventArgs *> unusedEventArgsList;
        m_pCutFile->GetUnusedEventArgs( unusedEventArgsList );
		m_cleanupEventArgsButton.Enabled = unusedEventArgsList.GetCount() > 0;

        SetUIFromEventArgs( NULL );

        m_bModifiedCutfile = true;

        SetEventArgsEditMode( NO_EVENT_ARGS_MODE );
    }
}

void EditEventPopup::CleanupEventArgsButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
	int iResult = FBMessageBox( "Cleanup EventArgs", "Are you sure you want to delete the unreferenced EventArgs?  This operation cannot be undone.",
		"Yes", "No" );
	if ( iResult == 1 )
	{
		atArray<SEditEventArgsData *> unusedEventArgsDataList;
		for ( int i = 0; i < m_eventArgsDataList.GetCount(); ++i )
		{
			if ( m_eventArgsDataList[i]->pEventArgs->GetRef() == 0 )
			{
				unusedEventArgsDataList.Grow() = m_eventArgsDataList[i];
			}
		}

		for ( int i = 0; i < unusedEventArgsDataList.GetCount(); ++i )
		{
			RemoveRowFromEventArgsSpreadsheet( unusedEventArgsDataList[i] );

			DestroyEventArgsData( unusedEventArgsDataList[i] );
		}

		m_editEventArgsButton.Enabled = false;
		m_removeEventArgsButton.Enabled = false;
		m_cleanupEventArgsButton.Enabled = false;
		m_bModifiedCutfile = true;

		if ( GetEventArgsEditMode() == NO_EVENT_ARGS_MODE )
		{
			SetUIFromEventArgs( NULL );
		}
	}
}

void EditEventPopup::AttributeSpreadsheet_RowClick( HISender /*pSender*/, HKEvent pEvent )
{
    if ( (GetEventArgsEditMode() != EDIT_EVENT_ARGS_MODE) 
        && (GetEventArgsEditMode() != ADD_EVENT_ARGS_MODE) )
    {
        return;
    }

    FBEventSpread e( pEvent );

    parAttribute **ppAttribute = m_rowRefAttributeMap.Access( e.Row );
    if ( ppAttribute != NULL )
    {
        int iIndex = -1;
        
        atArray<parAttribute> &attList = m_currentAttributeList.GetAttributeArray();
        for ( int i = 0; i < attList.GetCount(); ++i )
        {
            if ( strcmp( (*ppAttribute)->GetName(), attList[i].GetName() ) == 0 )
            {
                iIndex = i;
                break;
            }
        }

        m_editAttributeButton.Enabled = iIndex >= m_iRestrictedFromEditingAttributeCount;

        m_removeAttributeButton.Enabled = iIndex >= m_iRestrictedFromRemovalAttributeCount;
        m_removeAllAttributesButton.Enabled = true;

        SetUIFromAttribute( *ppAttribute );
    }
    else
    {
        m_editAttributeButton.Enabled = false;
        m_removeAttributeButton.Enabled = false;
        m_removeAllAttributesButton.Enabled = m_currentAttributeList.GetAttributeArray().GetCount() > 0;

        SetUIFromAttribute( NULL );
    }
}

void EditEventPopup::EditAttributeButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( m_pCurrentAttribute != NULL ) 
    {
        int iIndex = -1;

        atArray<parAttribute> &attList = m_currentAttributeList.GetAttributeArray();
        for ( int i = 0; i < attList.GetCount(); ++i )
        {
            if ( strcmp( m_pCurrentAttribute->GetName(), attList[i].GetName() ) == 0 )
            {
                iIndex = i;
                break;
            }
        }

        if ( (iIndex >= m_iRestrictedFromEditingAttributeCount) && PromptToSaveAttribute( true ) )     
		{
            SetUIFromAttribute( m_pCurrentAttribute );

            m_lastEventArgsEditMode = GetEventArgsEditMode();
            SetEventArgsEditMode( EDIT_EVENT_ARGS_ATTRIBUTE_MODE );
        }
    }
}

void EditEventPopup::AddAttributeButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( PromptToSaveAttribute( true ) )
    {
        SetUIFromAttribute( NULL );

        m_lastEventArgsEditMode = GetEventArgsEditMode();
        SetEventArgsEditMode( ADD_EVENT_ARGS_ATTRIBUTE_MODE );
    }
}

void EditEventPopup::RemoveAttributeButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( m_pCurrentAttribute != NULL ) 
    {
        int iIndex = -1;

        atArray<parAttribute> &attList = m_currentAttributeList.GetAttributeArray();
        for ( int i = 0; i < attList.GetCount(); ++i )
        {
            if ( strcmp( m_pCurrentAttribute->GetName(), attList[i].GetName() ) == 0 )
            {
                iIndex = i;
                break;
            }
        }

        if ( (iIndex >= m_iRestrictedFromRemovalAttributeCount) && PromptToSaveAttribute( true ) )
        {
            RemoveRowFromAttributesSpreadsheet( m_pCurrentAttribute );

            m_editAttributeButton.Enabled = false;
            m_removeAttributeButton.Enabled = false;

            SetHaveUnsavedEventArgsChanges( true );

            SetEventArgsEditMode( GetEventArgsEditMode() ); // this will enable the controls appropriately
        }
    }
}

void EditEventPopup::RemoveAllAttributesButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    atArray<parAttribute> &attList = m_currentAttributeList.GetAttributeArray();
    if ( attList.GetCount() > m_iRestrictedFromRemovalAttributeCount )
    {
        while ( attList.GetCount() > m_iRestrictedFromRemovalAttributeCount )
        {
            RemoveRowFromAttributesSpreadsheet( &attList[attList.GetCount() - 1] );
            attList.Delete( attList.GetCount() - 1 );
        }

        m_editAttributeButton.Enabled = false;
        m_removeAttributeButton.Enabled = false;

        SetUIFromAttribute( NULL );

        SetHaveUnsavedEventArgsChanges( true );

        SetEventArgsEditMode( GetEventArgsEditMode() ); // this will enable the controls appropriately
    }
}

void EditEventPopup::AttributeNameTextBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    SetHaveUnsavedAttributeChanges( true );
}

void EditEventPopup::AttributeTypeComboBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    SetHaveUnsavedAttributeChanges( true );
}

void EditEventPopup::AttributeValueTextBox_Change( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    SetHaveUnsavedAttributeChanges( true );
}

void EditEventPopup::DoneEditAttributeButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    if ( SetAttributeFromUI( m_pCurrentAttribute ) )
    {
        SetEventArgsEditMode( m_lastEventArgsEditMode );
    }
}

void EditEventPopup::CancelEditAttributeButton_Click( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
    switch ( GetEventArgsEditMode() )
    {
    case ADD_EVENT_ARGS_ATTRIBUTE_MODE:
        SetUIFromAttribute( NULL );
        break;
    case EDIT_EVENT_ARGS_ATTRIBUTE_MODE:
        SetUIFromAttribute( m_pCurrentAttribute );
        break;
    default:
        // should never get here
        break;
    }

    SetEventArgsEditMode( m_lastEventArgsEditMode );
}

void EditEventPopup::SaveEventArgsChangesButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    if ( PromptToSaveAttribute( true ) && SetEventArgsFromUI( m_pCurrentEventArgsData ) )
    {
        m_cleanupEventArgsButton.Enabled = true;

        m_bModifiedCutfile = true;

        SetEventArgsEditMode( NO_EVENT_ARGS_MODE );
    }
}

void EditEventPopup::DiscardEventArgsChangesButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    switch ( GetEventArgsEditMode() )
    {
    case ADD_EVENT_ARGS_MODE:
        {
            delete m_pCurrentEventArgsData->pEventArgs;
            delete m_pCurrentEventArgsData;
            SetUIFromEventArgs( NULL );
        }
        break;
    case ADD_EVENT_ARGS_ATTRIBUTE_MODE:
        {
            SetUIFromAttribute( NULL );

            if ( m_lastEventArgsEditMode == ADD_EVENT_ARGS_MODE )
            {
                delete m_pCurrentEventArgsData->pEventArgs;
                delete m_pCurrentEventArgsData;
                SetUIFromEventArgs( NULL );
            }
            else if ( m_lastEventArgsEditMode == EDIT_EVENT_ARGS_MODE )
            {
                SetUIFromEventArgs( m_pCurrentEventArgsData );
            }
        }
        break;
    case EDIT_EVENT_ARGS_MODE:
        {
            SetUIFromEventArgs( m_pCurrentEventArgsData );
        }
        break;
    case EDIT_EVENT_ARGS_ATTRIBUTE_MODE:
        {
            SetUIFromAttribute( m_pCurrentAttribute );

            if ( m_lastEventArgsEditMode == ADD_EVENT_ARGS_MODE )
            {
                delete m_pCurrentEventArgsData->pEventArgs;
                delete m_pCurrentEventArgsData;
                SetUIFromEventArgs( NULL );
            }
            else if ( m_lastEventArgsEditMode == EDIT_EVENT_ARGS_MODE )
            {
                SetUIFromEventArgs( m_pCurrentEventArgsData );
            }
        }
        break;
    default:
        break;
    }

    SetEventArgsEditMode( NO_EVENT_ARGS_MODE );
}

void EditEventPopup::CloseButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    if ( PromptToSaveEvent( true ) && PromptToSaveEventArgs( true ) )
    {
        Clear();

        Close( m_bModifiedCutfile );
    }    
}

//##############################################################################

NewEventPopup::NewEventPopup( const cutfEventDefs *pEventDefs )
: m_pEventDefs(pEventDefs)
{
    UICreate();
    UIConfigure();
}

NewEventPopup::~NewEventPopup()
{

}

const cutfEventDef* NewEventPopup::GetEventDef() const
{
    if ( m_pEventDefs->GetEventDefCount() > 0 )
    {
        return m_pEventDefs->GetEventDef( m_eventDefComboBox.ItemIndex );
    }

    return NULL;
}

void NewEventPopup::UICreate()
{
    Caption = "Create New Event";

    Region.X = 200;
    Region.Y = 200;
    Region.Width = 300;
    Region.Height = (rexMBAnimExportCommon::c_iMargin * 2) + (rexMBAnimExportCommon::c_iRowHeight * 2);

    // Event Name
    AddRegion( "EventDefNameLabel", "EventDefNameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "EventDefNameLabel", m_eventDefNameLabel );

    AddRegion( "EventDefComboBox", "EventDefComboBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventDefNameLabel", 1.0,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "EventDefComboBox", m_eventDefComboBox );

    // OK
    AddRegion( "OkButton", "OkButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OkButton", m_okButton );

    // Cancel
    AddRegion( "CancelButton", "CancelButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "CancelButton", m_cancelButton );
}

void NewEventPopup::UIConfigure()
{
    // Event Name
    m_eventDefNameLabel.Caption = "Event Name:";
    m_eventDefNameLabel.Justify = kFBTextJustifyRight;

    m_eventDefComboBox.Style = kFBDropDownList;
    
    for ( int i = 0; i < m_pEventDefs->GetEventDefCount(); ++i )
    {
        char cEnumName[CUTSCENE_LONG_OBJNAMELEN];
        m_pEventDefs->GetEventDef( i )->GetEnumName( cEnumName, sizeof(cEnumName ) );

        m_eventDefComboBox.Items.Add( cEnumName );
    }

    if ( m_pEventDefs->GetEventDefCount() > 0 )
    {
        m_eventDefComboBox.ItemIndex = 0;
    }

    // OK
    m_okButton.Caption = "OK";
    m_okButton.OnClick.Add( this, (FBCallback)&NewEventPopup::OkButton_Click );

    // Cancel
    m_cancelButton.Caption = "Cancel";
    m_cancelButton.OnClick.Add( this, (FBCallback)&NewEventPopup::CancelButton_Click );
}

void NewEventPopup::OkButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    Close( true );
}

void NewEventPopup::CancelButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    Close( false );
}

//##############################################################################

NewEventArgsPopup::NewEventArgsPopup( const cutfEventDefs *pEventDefs )
: m_pEventDefs(pEventDefs)
{
    UICreate();
    UIConfigure();
}

NewEventArgsPopup::~NewEventArgsPopup()
{

}

const cutfEventArgsDef* NewEventArgsPopup::GetEventArgsDef() const
{
    if ( m_pEventDefs->GetEventArgsDefCount() > 0 )
    {
        return m_pEventDefs->GetEventArgsDef( m_eventArgsDefComboBox.ItemIndex );
    }

    return NULL;
}

void NewEventArgsPopup::UICreate()
{
    Caption = "Create New EventArgs";

    Region.X = 200;
    Region.Y = 200;
    Region.Width = 300;
    Region.Height = (rexMBAnimExportCommon::c_iMargin * 2) + (rexMBAnimExportCommon::c_iRowHeight * 2);

    // EventArgs Name
    AddRegion( "EventArgsDefNameLabel", "EventArgsDefNameLabel",
        rexMBAnimExportCommon::c_iMargin, kFBAttachLeft, "NULL", 1.0,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0,
        rexMBAnimExportCommon::c_iLabelWidth + 20, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "EventArgsDefNameLabel", m_eventArgsDefNameLabel );

    AddRegion( "EventArgsDefComboBox", "EventArgsDefComboBox",
        rexMBAnimExportCommon::c_iMargin, kFBAttachRight, "EventArgsDefNameLabel", 1.0,
        rexMBAnimExportCommon::c_iMargin, kFBAttachTop, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin, kFBAttachRight, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "EventArgsDefComboBox", m_eventArgsDefComboBox );

    // OK
    AddRegion( "OkButton", "OkButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth - rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "OkButton", m_okButton );

    // Cancel
    AddRegion( "CancelButton", "CancelButton",
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iButtonWidth, kFBAttachRight, "NULL", 1.0,
        -rexMBAnimExportCommon::c_iMargin - rexMBAnimExportCommon::c_iFieldHeight, kFBAttachBottom, "NULL", 1.0,
        rexMBAnimExportCommon::c_iButtonWidth, kFBAttachNone, NULL, 1.0,
        rexMBAnimExportCommon::c_iFieldHeight, kFBAttachNone, NULL, 1.0 );
    SetControl( "CancelButton", m_cancelButton );
}

void NewEventArgsPopup::UIConfigure()
{
    // EventArgs Name
    m_eventArgsDefNameLabel.Caption = "EventArgs Name:";
    m_eventArgsDefNameLabel.Justify = kFBTextJustifyRight;

    m_eventArgsDefComboBox.Style = kFBDropDownList;

    for ( int i = 0; i < m_pEventDefs->GetEventArgsDefCount(); ++i )
    {
        m_eventArgsDefComboBox.Items.Add( const_cast<char *>( m_pEventDefs->GetEventArgsDef( i )->GetName() ) );
    }

    if ( m_pEventDefs->GetEventArgsDefCount() > 0 )
    {
        m_eventArgsDefComboBox.ItemIndex = 0;
    }

    // OK
    m_okButton.Caption = "OK";
    m_okButton.OnClick.Add( this, (FBCallback)&NewEventArgsPopup::OkButton_Click );

    // Cancel
    m_cancelButton.Caption = "Cancel";
    m_cancelButton.OnClick.Add( this, (FBCallback)&NewEventArgsPopup::CancelButton_Click );
}

void NewEventArgsPopup::OkButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    Close( true );
}

void NewEventArgsPopup::CancelButton_Click( HISender /*pSender*/, HKEvent /*pEventArgs*/ )
{
    Close( false );
}

//##############################################################################

} // namespace rage
