// rexFaceFxRage.cpp : Defines the entry point for the console application.
//
#include "system/main.h"

#include <iostream>
#include "FxSDK.h"
#include "FxAnimSet.h"
#include "atl\array.h"
#include "atl\string.h"
#include "rexGeneric/objectAnimation.h"
#include "rexRage/serializerAnimation.h"
#include "cranimation/animation.h"
#include "cranimation/animTrack.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "crmetadata/properties.h"
#include "crmetadata/propertyattributes.h"
#include "system/exec.h"
#include "system/param.h"

using namespace rage;

const int FRAMES_PER_SECOND = 30;
const float MAX_START_TIME = 0.3f;

#define TIME_OF_PRE_DELAY_PROPERTY_NAME "TimeOfPreDelay"
#define COMPRESSION_FILE_PROPERTY_NAME "Compressionfile_DO_NOT_RESOURCE"

REQ_PARAM( input, "[rexFaceFxRage] Input files." );
REQ_PARAM( outputdir, "[rexFaceFxRage] Output directory." );

void PrintMessage()
{
	std::cout << "usage:" << std::endl;
	std::cout << "\trexFaceFxRage outputDir example.animset,example2.animset" << std::endl;
}

void Tokenize(std::string strInput, std::vector<std::string>& vOutput)
{
	char* pch = strtok ((char*)strInput.c_str(),",");
	while (pch != NULL)
	{
		vOutput.push_back(pch);
		pch = strtok (NULL, ",");
	}
}

void InitTracksInfo(int numFrames, OC3Ent::Face::FxAnim* pAnim, atArray<rexObjectGenericAnimation::TrackInfo*>& r_Tracks, const atArray<atString>& r_TrackNames, const atArray<int>& r_TrackIDs)
{
	r_Tracks.Reset();
	r_Tracks.Reserve(r_TrackNames.GetCount());

	for(s32 i=0; i<r_TrackNames.GetCount(); ++i)
	{
		rexObjectGenericAnimation::TrackInfo* p_Track = NULL;

		switch(r_TrackIDs[i])
		{
		case rexObjectGenericAnimation::TRACK_VISEMES:
			p_Track = new rexObjectGenericAnimation::TrackInfoFloat();
			break;
		}

		if(!p_Track)
			continue;

		p_Track->m_Name = r_TrackNames[i];
		p_Track->m_TrackID = r_TrackIDs[i];
		p_Track->m_StartTime = 0.f;
		p_Track->m_EndTime = pAnim->GetEndTime() + Min(-pAnim->GetStartTime(), MAX_START_TIME);
		p_Track->m_IsLocked = false;
		p_Track->ResetKeys(numFrames);

		r_Tracks.Append() = p_Track;
	}
}

void CreateBogusChunk(int numFrames, OC3Ent::Face::FxAnim* pAnim, atArray<rexObjectGenericAnimation::TrackInfo*>& r_Tracks)
{
	atArray<atString> TrackNames;
	atArray<int> TrackIDs;

	TrackNames.PushAndGrow(atString("translate"));
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_TRANSLATE);
	TrackNames.PushAndGrow(atString("rotate"));
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_ROTATE);

	r_Tracks.Reset();
	r_Tracks.Reserve(TrackNames.GetCount());

	for(s32 i=0; i<TrackNames.GetCount(); ++i)
	{
		rexObjectGenericAnimation::TrackInfo* p_Track = NULL;

		switch(TrackIDs[i])
		{
		case rexObjectGenericAnimation::TRACK_TRANSLATE:
			p_Track = new rexObjectGenericAnimation::TrackInfoVector3();
			break;
		case rexObjectGenericAnimation::TRACK_ROTATE:
			p_Track = new rexObjectGenericAnimation::TrackInfoQuaternion();
			break;
		}

		if(!p_Track)
			continue;

		p_Track->m_Name = TrackNames[i];
		p_Track->m_TrackID = TrackIDs[i];
		p_Track->m_StartTime = 0.f;
		p_Track->m_EndTime = pAnim->GetEndTime() + Min(-pAnim->GetStartTime(), MAX_START_TIME);
		p_Track->m_IsLocked = false;
		p_Track->ResetKeys(numFrames);

		r_Tracks.Append() = p_Track;
	}

	for ( int i = 0; i < numFrames; ++i)
	{
		for( int j=0; j < r_Tracks.GetCount(); ++j)
		{
			if(r_Tracks[j]->m_TrackID == rexObjectGenericAnimation::TRACK_ROTATE)
			{
				r_Tracks[j]->AppendQuaternionKey(Quaternion(0,0,0,1));	
			}
			else if(r_Tracks[j]->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
			{
				r_Tracks[j]->AppendVector3Key(Vector3(1,1,1));
			}
		}
	}
}

void ProcessData(const std::string& strOutDir, OC3Ent::Face::FxAnim* pAnim, atArray<atString> TrackNames, atArray<int> TrackIDs)
{
	rexObjectGenericAnimation* gnrcAnimation = rage_new rexObjectGenericAnimation;

	rexObjectGenericAnimation::ChunkInfo* p_Chunk = new rexObjectGenericAnimation::ChunkInfo();
	rexObjectGenericAnimation::ChunkInfo* p_BogusChunk = new rexObjectGenericAnimation::ChunkInfo();

	p_Chunk->m_Name = pAnim->GetNameAsCstr();
	p_Chunk->m_ShortName = p_Chunk->m_Name;
	p_Chunk->m_BoneIndex = 0;
	p_Chunk->m_Scale = Vector3(1.0f,1.0f,1.0f);
	p_Chunk->m_BoneID = "viseme";

	int numFrames = (int)((pAnim->GetEndTime() + Min(-pAnim->GetStartTime(), MAX_START_TIME)) * FRAMES_PER_SECOND) + 1;
	int numStartFrames = (int)(Min(-pAnim->GetStartTime(), MAX_START_TIME) * FRAMES_PER_SECOND);

	double startFrameScale = 1.0;
	if(-pAnim->GetStartTime() > MAX_START_TIME)
	{
		 startFrameScale = -pAnim->GetStartTime() / MAX_START_TIME;
	}

	InitTracksInfo(numFrames, pAnim, p_Chunk->m_Tracks, TrackNames, TrackIDs);

	double deltaTime = 1.0 / (double)FRAMES_PER_SECOND;

	for ( int i = 0; i < numFrames; i++ )
	{
		double t = (deltaTime * (double)(i-numStartFrames));
		if(t < 0.0)
		{
			t *= startFrameScale;
		}
		
		for( int j=0; j < p_Chunk->m_Tracks.GetCount(); ++j)
		{
			OC3Ent::Face::FxAnimCurve animCurve = pAnim->GetAnimCurve(j);

			OC3Ent::Face::FxReal val = animCurve.EvaluateAt((OC3Ent::Face::FxReal)t);

			p_Chunk->m_Tracks[j]->AppendFloatKey(val);
		}
	}

	// create a bogus track, this will be compressed and removed
	CreateBogusChunk(numFrames, pAnim, p_BogusChunk->m_Tracks);
	gnrcAnimation->m_RootChunks.PushAndGrow(p_BogusChunk);
	gnrcAnimation->m_GenericChunk = p_Chunk;

	char cAnimPath[RAGE_MAX_PATH];
	sprintf(cAnimPath, "%s//%s", strOutDir.c_str(), pAnim->GetNameAsCstr());

	p_BogusChunk->m_Name = cAnimPath;

	rexSerializerRAGEAnimation rexSer;
	rexSer.SetCompressionErrorTolerance(0.0f);

	rexResult res = rexResult::PERFECT;
	res.Combine(rexSer.Serialize( *gnrcAnimation ));

	if(res.Errors())
	{
		std::cout << "ERROR: Failed to save animation '" << cAnimPath << "'" << std::endl;
	}
	else
	{
		std::cout << "Processed animation '" << cAnimPath << "'" << std::endl;
	}

	crAnimation* pRageAnim = crAnimation::AllocateAndLoad( cAnimPath );
	if( pRageAnim )
	{
		crClipAnimation* pClipAnim = crClipAnimation::AllocateAndCreate( *pRageAnim );

		crProperties* pProperties = pClipAnim->GetProperties();

		crPropertyAttributeFloat pPreDelay;
		pPreDelay.SetName( TIME_OF_PRE_DELAY_PROPERTY_NAME );
		float& preDelayValue = pPreDelay.GetFloat();
		preDelayValue = Min(-pAnim->GetStartTime(), MAX_START_TIME);

		crProperty pPreDelayProperty;
		pPreDelayProperty.SetName( TIME_OF_PRE_DELAY_PROPERTY_NAME );
		pPreDelayProperty.AddAttribute( pPreDelay );
		pProperties->AddProperty( pPreDelayProperty );

		crPropertyAttributeString pCompressionFile;
		pCompressionFile.SetName( COMPRESSION_FILE_PROPERTY_NAME );
		atString& compressionValue = pCompressionFile.GetString();
		compressionValue = "MG_FaceFX.txt";

		crProperty pCompressionFileProperty;
		pCompressionFileProperty.SetName( COMPRESSION_FILE_PROPERTY_NAME );
		pCompressionFileProperty.AddAttribute( pCompressionFile );
		pProperties->AddProperty( pCompressionFileProperty );

		atString strName(ASSET.FileName(pClipAnim->GetAnimation()->GetName()));
		const_cast<crAnimation*>(pClipAnim->GetAnimation())->SetName(strName.c_str());

		if(!pClipAnim->Save(cAnimPath))
		{
			std::cout << "ERROR: Failed to save clip '" << cAnimPath << "'" << std::endl;
		}
		else
		{
			std::cout << "Processed clip '" << cAnimPath << "'" << std::endl;
		}
	}
}

int Main()
{
	crAnimation::InitClass(); 
	crClip::InitClass();

	std::wcout << "rexFaceFxRage..." << std::endl << std::endl;

	const int maxItems = 100;
	const int maxItemsBufSize = maxItems * RAGE_MAX_PATH;

	int numInputAnimSets = 0;
	const char* inputAnimSets[maxItems];
	char inputClipsBuf[maxItemsBufSize];

	numInputAnimSets = PARAM_input.GetArray( inputAnimSets, maxItems, inputClipsBuf, maxItemsBufSize );

	std::vector<std::string> vFiles;
	for ( int i = 0; i < numInputAnimSets; ++i )
		vFiles.push_back(inputAnimSets[i]);

	const char *pOutputDirectory;
	PARAM_outputdir.Get( pOutputDirectory );

	std::cout << "processing: " << vFiles.size() << " files." << std::endl;

	//CreateDirectoryA(argv[1], NULL);

	OC3Ent::Face::FxSDKStartup();

	for(std::vector<std::string>::const_iterator itr = vFiles.begin(); itr != vFiles.end(); ++itr)
	{
		OC3Ent::Face::FxAnimSet animSet;
		
		OC3Ent::Face::FxArchiveResult res = OC3Ent::Face::InternalUseOnly::FxLoadAnimSetFromFile(animSet, itr->c_str());
		if( res.result == OC3Ent::Face::FxArchiveResult::AR_NoError )
		{
			OC3Ent::Face::FxAnimGroup animGrp = animSet.GetAnimGroup();

			OC3Ent::Face::FxAnimGroup::AnimationIterator animIter = animGrp.BeginAnimations();
			OC3Ent::Face::FxAnimGroup::AnimationIterator animIterEnd = animGrp.EndAnimations();
			
			std::wcout << "processing: " << itr->c_str() << " with " << animGrp.GetNumAnims() << " anims." << std::endl << std::endl;

			for( ; animIter != animIterEnd; ++animIter )
			{
				OC3Ent::Face::FxAnim* pAnim = (*animIter).item;
				OC3Ent::Face::FxReal animDuration = pAnim->GetDuration();
				OC3Ent::Face::FxSize numCurves = pAnim->GetNumAnimCurves();
				const OC3Ent::Face::FxChar* pAnimName = pAnim->GetNameAsCstr();
				
				atArray<atString> TrackNames;
				atArray<int> TrackIDs;

				for(unsigned int i=0; i< numCurves; ++i)
				{
					OC3Ent::Face::FxAnimCurve animCurve = pAnim->GetAnimCurve(i);
					const OC3Ent::Face::FxChar* pName = animCurve.GetNameAsCstr();

					TrackNames.PushAndGrow(atString(pName));
					TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_VISEMES);
				}

				ProcessData(pOutputDirectory, pAnim, TrackNames, TrackIDs);
			}
		}
		else
		{
			switch ( res.result )
			{
			case OC3Ent::Face::FxArchiveResult::AR_NoError:
				break;
			case OC3Ent::Face::FxArchiveResult::AR_FailedToOpenStoreForReading:
				std::cout << "error: unable to open file for reading: " << itr->c_str() << std::endl << std::endl;
				break;
			case OC3Ent::Face::FxArchiveResult::AR_FormatError:
				std::cout << "error: unable to open file; invalid format: " << itr->c_str() << std::endl << std::endl;
				break;
			case OC3Ent::Face::FxArchiveResult::AR_NotAFaceFXArchive:
				std::cout << "error: unable to open file; not a facefx archive: " << itr->c_str() << std::endl << std::endl;
				break;
			case OC3Ent::Face::FxArchiveResult::AR_InternalError:
			case OC3Ent::Face::FxArchiveResult::AR_InvalidStore:
			case OC3Ent::Face::FxArchiveResult::AR_SaveOperationRefused:
			case OC3Ent::Face::FxArchiveResult::AR_FailedToOpenStoreForWriting:
				std::cout << "error: unable to open file for reading: " << itr->c_str() << std::endl << std::endl;
			}
		}
		
		std::cout << "processing complete.." << std::endl;
	}

	OC3Ent::Face::FxSDKShutdown();
	return 0;
}

