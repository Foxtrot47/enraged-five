#if defined(_DEBUG)

#include "forceinclude/win32_tooldebug.h"

#elif defined(NDEBUG)

#include "forceinclude/win32_toolrelease.h"

#else

#include "forceinclude/win32_toolbeta.h"

#endif