// /utility.h

#ifndef _ANIMEXPREXPORT_UTILITY_H_
#define _ANIMEXPREXPORT_UTILITY_H_


#include <sstream>
#include <atl/singleton.h>

#include "vector/Matrix34.h"

#include "AnimatedCustomAttributeWriter.h"
#include "HashNameWriter.h"
#include "RsULogWrapper/IFPULog.h"

using namespace std;
using namespace rage;

//-----------------------------------------------------------------------------
//Misc. ClassIds defined outside of the core SDK includes
#define IPOS_CONTROL_CLASS_ID		Class_ID(0x118f7e02,0xffee238a)
#define MR3_CLASS_ID				Class_ID(0x17bb6854, 0xa5cba2a3)
#define ISCALE_CONTROL_CLASS_ID		Class_ID(0x118F7C01, 0xFEEE238B)

//-----------------------------------------------------------------------------
//FloatLimitControl information defined outside the SDK includes in
// maxsdk/samples/controllers/LimitControl.h
#define BASELIMIT_LIMITEDCTRL_REF	1

//-----------------------------------------------------------------------------
// ExposeTransformNode class information
// From maxsdk/samples/objects/helpers/ExposeTransform/ExposeTransform.h and ExposeTransform.cpp
#define ExposeTransform_CLASS_ID	Class_ID(0x848361cb, 0x913cc083) 

#define EXPOSETMNODE_PBLOCK_EXPOSE		0
#define EXPOSETMNODE_PBLOCK_INFO		1

//Param Block 0 of ExposeTransform node
enum ExposeTransform_Expose_Params
{ 
	pb_localeuler,pb_localeulerx,pb_localeulery,pb_localeulerz,
	pb_worldeuler,pb_worldeulerx,pb_worldeulery,pb_worldeulerz,
	pb_localposition,pb_localpositionx,	pb_localpositiony,pb_localpositionz,
	pb_worldposition,pb_worldpositionx, pb_worldpositiony,pb_worldpositionz,
	pb_distance,
	pb_worldboundingbox_size,pb_worldboundingbox_width,pb_worldboundingbox_length,pb_worldboundingbox_height,
	pb_angle

};

//Param Block 1 of ExposeTransform node
enum ExposeTransform_Info_Params
{
	pb_expose_node,
	pb_reference_node,
	pb_stripnuscale,
	pb_x_order, pb_y_order,pb_z_order,
	pb_use_parent_as_reference,
	pb_use_time_offset,
	pb_time_offset
};

//-----------------------------------------------------------------------------
// ExposeController class information
// From maxsdk/samples/objects/helpers/ExposeTransform/ExposeControllers.h
#define FloatExposeController_CLASS_ID	Class_ID(0x72930021, 0x6951211)
#define Point3ExposeController_CLASS_ID	Class_ID(0x69f672c0, 0x27927afa)
#define EulerExposeController_CLASS_ID	Class_ID(0x19357028, 0x428c5294)

//-----------------------------------------------------------------------------


namespace exprExportUtility
{
	extern Tab<char *> s_ExprPatterns;

	bool IsNodeOfClassId(INode* pNode, Class_ID cid,  bool bStrictCompare = true);
//	bool IsControllerExpressionDriven(::Control *pControl);
	bool IsControllerExportable(::Control *pControl, INodePtr pNode, IFPULog *pLog=NULL);
	bool GetNormalizedNodeName(INode* pNode, atString& outName, bool bRemoveNamespace = true);
	bool GetBoneId(INode* pNode, string& outId);
	Modifier* GetMorphModifier(INode* p_Node);
	void GetLocalMatrix(INode* pNode, float fTime, Matrix34& outLocal);
	bool GetTrackIdOverrideTranslate(INode* pNode, string& outId);
	bool GetTrackIdOverrideRotate(INode* pNode, string& outId);
	bool GetTrackIdOverrideScale(INode* pNode, string& outId);
	bool GetRelativeTranslateOverride(INode* pNode, string& outId);
	bool GetRelativeRotateOverride(INode* pNode, string& outId);
	bool GetRelativeScaleOverride(INode* pNode);


	//A simple logging class for the expression exporter
	class exprExportLog
	{
	public:
		~exprExportLog() {};

		void	ResetLog() { m_log.str(""); }
		bool	SaveLog(const char* szFilePath);
		void	AppendEntry(const char* szEntry) { m_log << szEntry; }

	protected:
		exprExportLog() {};

	private:
		stringstream	m_log;
	};

	typedef atSingleton<exprExportLog> exprExportLogSingleton;
	typedef atSingleton<AnimatedCustomAttributeWriter> animatedCustomAttributeWriterSingleton;
	typedef atSingleton<HashNameWriter> hashNameWriterSingleton;

	#define EXPORTLOG				::exprExportUtility::exprExportLogSingleton::InstanceRef()
	#define INIT_EXPORTLOG			::exprExportUtility::exprExportLogSingleton::Instantiate()
	#define SHUTDOWN_EXPORTLOG		::exprExportUtility::exprExportLogSingleton::Destroy()

	#define ATTRIBUTELOG				::exprExportUtility::animatedCustomAttributeWriterSingleton::InstanceRef()
	#define INIT_ATTRIBUTELOG			::exprExportUtility::animatedCustomAttributeWriterSingleton::Instantiate()
	#define SHUTDOWN_ATTRIBUTELOG		::exprExportUtility::animatedCustomAttributeWriterSingleton::Destroy()

	#define HASHNAMELOG					::exprExportUtility::hashNameWriterSingleton::InstanceRef()
	#define INIT_HASHNAMELOG			::exprExportUtility::hashNameWriterSingleton::Instantiate()
	#define SHUTDOWN_HASHNAMELOG		::exprExportUtility::hashNameWriterSingleton::Destroy()

} // end namespace exprExportUtility

//-----------------------------------------------------------------------------

#endif //_ANIMEXPREXPORT_UTILITY_H_

