#pragma once

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

#pragma warning(push)
#pragma warning(disable:4265)

//#include "3dsmaxsdk_preinclude.h"
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
//SIMPLE TYPE
#include "ILockedTracks.h"
#include "iexprctrl.h"
#include "expr.h"
#include "notify.h"
#include "INodeTransformMonitor.h"

#include"utilapi.h"

#pragma warning(pop)

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// hack from exprctrl.cpp
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// scalar variables
class SVar {
public:
	SVar()	{ regNum = refID = subNum = -1; offset = 0; }
	TSTR	name;
	int		regNum;	// register number variable is assigned to
	int		refID;	// < 0 means constant, if >= 0 points at entry in the reference tab
	float	val;	// value, if constant
	int		subNum;	// < 0 means direct reference to object from reference tab, if >=0 the subAnim index in object
	int		offset;	// tick offset
};

class VVar {
public:
	VVar()	{ regNum = refID = subNum = -1; offset = 0; }
	TSTR	name;
	int		regNum;	// reg num this var is assigned to
	int		refID;	// < 0 means constant, if >= 0 points at entry in the reference tab
	Point3	val;	// value, if const
	int 	subNum;	// < 0 means direct reference to object from reference tab, if >=0 the subAnim index in object
	int		offset;	// tick offset
};

MakeTab(SVar);
MakeTab(VVar);

class VarRef {
public:
	VarRef()	{ client = NULL; refCt = 0; }
	VarRef(ReferenceTarget *c)	{ client = c; refCt = 1; }
	ReferenceTarget *client;	// the object referenced
	int				refCt;		// number of variables using this reference
};

MakeTab(VarRef);

class ExprControl;

// for the debug floater window
class ExprDebug : public TimeChangeCallback {
public:
	HWND hWnd;
	ExprControl *ec;
	TimeValue t;
	bool inUpdate;

	ExprDebug(HWND hParent, ExprControl *exprControl);
	~ExprDebug();

	void Invalidate();
	void SetTime(TimeValue tm)	{ t = tm; }
	void Update();
	void Init(HWND hWnd);

	void TimeChanged(TimeValue t)	{ SetTime(t); Update(); }
};

class ExprControl : public LockableStdControl, public IExprCtrl
{
public:
	int			type;
	Expr		expr;
	int			timeSlots[4];
	int			sRegCt;
	int			vRegCt;
	int			curIndex;
	Point3		curPosVal;
	float		curFloatVal;
	Interval	ivalid;
	Interval	range;
	IObjParam *	ip;
	SVarTab		sVars;
	VVarTab		vVars;
	VarRefTab	refTab;
	TSTR		desc;
	static HFONT	hFixedFont;
	HWND		hDlg;
	ExprDebug	*edbg;

	bool		doDelayedUpdateVarList;
	bool		delayedUpdateVarListMaintainSelection;
	bool		disabledDueToRecursion;

	bool		bThrowOnError;

	Animatable	*this_client;
	int			this_index;

	TSTR		lastEditedExpr;
	INode		*nodeTMBeingEvaled;

	//flag to avoid recursion
	int			evaluating;
	TimeValue	evaluatingTime;

	void	updRegCt(int val, int type);
	BOOL	dfnVar(int type, TCHAR *buf, int slot, int offset = 0);
	int		getVarCount(int type) { return type == SCALAR_VAR ? sVars.Count() : vVars.Count(); }
	TCHAR * getVarName(int type, int i);
	int		getVarOffset(int type, int i);
	int		getRegNum(int type, int i);
	float	getScalarValue(int i, TimeValue t);
	Point3	getVectorValue(int i, TimeValue t);
	BOOL	assignScalarValue(int i, float val, int offset = TIME_NegInfinity, BOOL fromFPS = FALSE);
	BOOL	assignVectorValue(int i, Point3 &val, int offset = TIME_NegInfinity, BOOL fromFPS = FALSE);
	BOOL	assignController(int type, int i, ReferenceTarget *client, int subNum, int offset = TIME_NegInfinity, BOOL fromFPS = FALSE);
	void	deleteAllVars();
	void    getNodeName(ReferenceTarget *client,TSTR &name);
	void	updateVarList(bool maintainSelection = false, bool delayedUpdate = false);
	void	dropUnusedRefs();

	TSTR	getTargetAsString(int type, int i);

	static void NotifyPreReset(void* param, NotifyInfo*);

	// FPS method helper methods
	int		findIndex(int type, TSTR &name);
	BOOL	assignNode(INode* node, int varIndex, int offset = TIME_NegInfinity);
	BOOL	FindControl(Control* cntrl, ReferenceTarget *owner = NULL);

	// Published Functions
	virtual bool		GetThrowOnError();
	virtual void		SetThrowOnError(bool bOn);

	virtual TSTR		PrintDetails();
	virtual BOOL		SetExpression(TSTR &expression);
	virtual TSTR		GetExpression();
	virtual int			NumScalars();
	virtual int			NumVectors();

	virtual TSTR		GetDescription();
	virtual BOOL		SetDescription(TSTR &expression);

	virtual BOOL		AddScalarTarget(TSTR &name, Value* target, int offset = TIME_NegInfinity, ReferenceTarget *owner = NULL);
	virtual BOOL		AddVectorTarget(TSTR &name, Value* target, int offset = TIME_NegInfinity, ReferenceTarget *owner = NULL); 

	virtual BOOL		AddScalarConstant(TSTR &name, float	val); 
	virtual BOOL		AddVectorConstant(TSTR &name, Point3 point);

	virtual BOOL		AddVectorNode(TSTR &name, INode* node, int offset = TIME_NegInfinity);

	virtual BOOL		SetScalarTarget(Value* which, Value* target, ReferenceTarget *owner = NULL);
	virtual BOOL		SetVectorTarget(Value* which, Value* target, ReferenceTarget *owner = NULL); 
	virtual BOOL		SetVectorNode(Value* which, INode* node);
	virtual BOOL		SetScalarConstant(Value* which, float val); 
	virtual BOOL		SetVectorConstant(Value* which, Point3 point);

	virtual BOOL		DeleteVariable(TSTR &name);
	virtual BOOL		RenameVariable(TSTR &oldName, TSTR &newName);

	virtual BOOL		VariableExists(TSTR &name);
	virtual void		Update();
	virtual TimeValue	GetOffset(TSTR &name);
	virtual BOOL		SetOffset(TSTR &name, TimeValue tick);

	virtual float		GetScalarConstant(Value* which, TimeValue t);
	virtual Value*		GetScalarTarget(Value* which, BOOL asController);
	virtual Point3		GetVectorConstant(Value* which);
	virtual Value*		GetVectorTarget(Value* which, BOOL asController);
	virtual INode*		GetVectorNode(Value* which);

	virtual float		GetScalarValue(Value* which, TimeValue t);
	virtual Point3		GetVectorValue(Value* which, TimeValue t);
	virtual FPValue		GetVarValue(TSTR &name, TimeValue t);

	virtual int			GetScalarType(Value* which);
	virtual int			GetVectorType(Value* which);

	virtual int			GetValueType(TSTR &name);

	virtual TSTR		GetVectorName(int index);
	virtual TSTR		GetScalarName(int index);

	virtual int			GetScalarIndex(TSTR &name);
	virtual int			GetVectorIndex(TSTR &name);

	ExprControl(int type, ExprControl &ctrl, RemapDir& remap);
	ExprControl(int type, BOOL loading);
	~ExprControl();

	// Animatable methods
	int TrackParamsType() {if(GetLocked()==false) return TRACKPARAMS_WHOLE; else return TRACKPARAMS_NONE; }
	Class_ID ClassID() { return Class_ID(0x6a154bda, 0x436117b9); }
	void DeleteThis() { delete this; }
	int IsKeyable() { return 0; }		
	BOOL IsAnimated() {return TRUE;}
	Interval GetTimeRange(DWORD flags) { return range; }
	void EditTimeRange(Interval range,DWORD flags);
	void Hold();
	void MapKeys( TimeMap *map, DWORD flags );

	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev );
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next );
	// Animatable's Schematic View methods
	SvGraphNodeReference SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags);
	TSTR SvGetRelTip(IGraphObjectManager *gom, IGraphNode *gNodeTarger, int id, IGraphNode *gNodeMaker);
	bool SvHandleRelDoubleClick(IGraphObjectManager *gom, IGraphNode *gNodeTarget, int id, IGraphNode *gNodeMaker);

	void EditTrackParams(
		TimeValue t,	// The horizontal position of where the user right clicked.
		ParamDimensionBase *dim,
		TCHAR *pname,
		HWND hParent,
		IObjParam *ip,
		DWORD flags);

	// Reference methods
	int NumRefs() { return StdControl::NumRefs() + refTab.Count(); }
	ReferenceTarget* GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	RefResult NotifyRefChanged(Interval, RefTargetHandle, PartID&, RefMessage);
	void RefDeleted();
	RefTargetHandle Clone(RemapDir& remap) { 
		DbgAssert(false); // should never be here. Should be in the Clone method of a derived class.
		ExprControl *ctrl = new ExprControl(this->type, *this, remap);
		CloneControl(ctrl,remap);
		BaseClone(this, ctrl, remap);
		return ctrl; 
	}		
	BOOL IsRealDependency(ReferenceTarget *rtarg)
	{
		int refID = FindRef(rtarg);
		if (refID < StdControl::NumRefs())
			return StdControl::IsRealDependency(rtarg);

		ReferenceTarget *client = refTab[refID - StdControl::NumRefs()].client;
		return (client && client->SuperClassID() == REF_TARGET_CLASS_ID && 
			client->ClassID() == NODETRANSFORMMONITOR_CLASS_ID);
	};

	virtual BOOL ShouldPersistWeakRef(RefTargetHandle rtarg) { 
		int refID = FindRef(rtarg);
		if (refID < StdControl::NumRefs())
			return StdControl::ShouldPersistWeakRef(rtarg);

		return TRUE; 
	};

	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);

	// Control methods
	void Copy(Control *from);

	BOOL IsLeaf() { return TRUE; }
	void GetValueLocalTime(TimeValue t, void *val, Interval &valid, GetSetMethod method=CTRL_ABSOLUTE);	
	void SetValueLocalTime(TimeValue t, void *val, int commit, GetSetMethod method) {}
	void Extrapolate(Interval range,TimeValue t,void *val,Interval &valid,int type);
	void *CreateTempValue();
	void DeleteTempValue(void *val);
	void ApplyValue(void *val, void *delta);
	void MultiplyValue(void *val, float m);

	// StdControl methods
	void GetAbsoluteControlValue(INode *node,TimeValue t,Point3 *pt,Interval &iv);

	// FPMixinInterface
	///////////////////////////////////////////////////////////////////////////
	virtual BaseInterface* GetInterface(Interface_ID in_interfaceID) { 
		if (in_interfaceID == IID_EXPR_CONTROL) 
			return (IExprCtrl*)this; 
		else 
			return FPMixinInterface::GetInterface(in_interfaceID);
	}

};

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++