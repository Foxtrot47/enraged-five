// /rageMaxAnimExprExport.h

#ifndef _RAGE_MAX_ANIMEXPREXPORT_H_
#define _RAGE_MAX_ANIMEXPREXPORT_H_

#include <sstream>
#include <map>

#include "animExprData/animExprData.h"
#include "animExprExportCommon/trackIdMgr.h"

#include "IMaxAnimExprExport.h"

#include "RsULogWrapper/IFPULog.h"

using namespace rage;

class IExprCtrl;

//-----------------------------------------------------------------------------

#define ANIMEXPREXPORT_GUP_CLASS_ID Class_ID(0xd2a62c8, 0x485c0c89)

//-----------------------------------------------------------------------------

ClassDesc* GetRageMaxAnimExprExportGupDesc();

//-----------------------------------------------------------------------------

class RageMaxAnimExprExportGUP : public GUP, public IMaxAnimExprExport
{
public:
	DWORD	Start();
	void	Stop();

private:
	BOOL	ExportBoneExpressions(AeFile& exprFile, AeFile& exprBoneFile, INode* pNode, BOOL bRecursive);
	BOOL	ExportMeshExpressions(AeFile& exprFile, AeFile& exprBoneFile, INode* pNode);
	BOOL	ExportMorpherExpressions(AeFile& exprFile, AeFile& exprBoneFile, INode* pNode, AeMesh* pMesh);

	AeController* CreateController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, BOOL& bZerodControl, BOOL bBlendShape, ::Control* pContainerControl = NULL);

	AeController* CreateFloatConstantController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* pContainerControl = NULL);
	AeController* CreateXYZController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, BOOL& bZerodControl, ::Control* pContainerControl = NULL);
	AeController* CreateBlendController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, BOOL& bZerodControl, ::Control* pContainerControl = NULL);
	AeController* CreateExpressionController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, BOOL& bZerodControl, BOOL bBlendShape, ::Control* pContainerControl = NULL);
	AeController* CreateFloatWireController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* pContainerControl = NULL);
	AeController* CreateLookAtController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* pContainerControl = NULL);
	AeController* CreatePositionConstraintController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* pContainerControl = NULL);
	AeController* CreateOrientationConstraintController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* pContainerControl = NULL);
	AeController* CreateJiggleController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* pContainerControl = NULL);
	
	BOOL CollectDriverComponentAttributes(INode* pDriverNode, const char* szDriverComponentId,
										  eComponentType& type, eComponentRotationOrder& rotOrder, eComponentAxis& axis);
	BOOL CreateDriver(AeFile& theFile, AeFile& exprBoneFile, ::Control* pControl, AeDriver** pOutDriver, AeDriverComponent** pOutDriverComponent );
	BOOL CreateDriver(AeFile& theFile, AeFile& exprBoneFile, INode* pDriverNode, const char* szDriverComponentId,
					  AeDriver** pOutDriver, AeDriverComponent** pOutDriverComponent);
	BOOL CreateDriverForAttribute(AeFile& theFile, AeFile& exprBoneFile, INode* pDriverNodeParent, INode* pDriverNode, const char* szTargetString, const char* szDriverComponentId,
		AeDriver** pOutDriver, AeDriverComponent** pOutDriverComponent);

	inline string GenerateUniqueXYZControllerName(const char* szPrefix);
	inline string GenerateUniqueExprControllerName(const char* szPrefix);
	inline string GenerateUniqueBlendControllerName(const char* szPrefix);
	inline string GenerateUniqueObjectSpaceBlendControllerName(const char* szPrefix);
	inline string GenerateUniqueLookAtControllerName(const char* szPrefix);
	inline string GenerateUniqueObjectSpaceControllerName(const char* szPrefix);
	inline string GenerateUniqueFloatConstantControllerName(const char* szPrefix);
	inline string GenerateUniqueControllerName(const char* szPrefix, const char* szControllerName);

	atString GetAttributeDriverName(const atString& czControl, const atString& czAttribute);

	void PrintToLog(const char* format, ...);
	int TrackIDLookup(const char* trackid);
	void AddToHashMap(const char* str);
	BOOL CheckParentNodeForTrackID(INode* pDriverNodeParent, const char* szDriverComponentId);
	void RouteAnimatedNormalMapData(AeFile& exprFile);
	void RouteDrivers(AeFile& exprFile);
	

private:
	enum ValueType
	{
		kController = 0,
		kConstant
	};

	//PURPOSE:  Parses an expression, replacing all scalar custom attributes with its constant values.
	TSTR GetExpression(IExprCtrl* pExprCtrl);
	TSTR GetExpression(IBaseWireControl* pExprCtrl, std::map<string, string>& mapScalars);
	TSTR FixExpressionParentheses(const char* pExpression);

	unsigned int	m_ctrlIdCounter;
	stringstream	m_msgBoxErrorStream;
	std::map<u16, atString> m_hashMap;
	atArray<atString> m_hashMapConflicts;
	BOOL			m_exportMainFile;

	IFPULog *mp_uLog;

	void Msg(const char *msg, const char *context=NULL);
	void Warn(const char *msg, const char *context=NULL);
	void Error(const char *msg, const char *context=NULL);

public:
	//IMaxAnimExprExport Interface
	enum
	{
		em_Export,
		em_setULogPath
	};

	BOOL Export(Tab<INode*>& nodeArr, TCHAR* outputPath, TCHAR* trackIdFilePath, BOOL bIndividualBones, BOOL bRecursive, Tab<char *> ignoreExprPatterns);
	BOOL SetULogPath(TCHAR* outputPath)
	{
		mp_uLog = RsULogInst();
		if(mp_uLog)
		{
			bool bResult = mp_uLog->InitPath(outputPath, "Expression Export", true);
			mp_uLog->ImmediateFlush(false);
			return bResult;
		}
		else
			return FALSE;
	}

	DECLARE_DESCRIPTOR(RageMaxAnimExprExportGUP)

	BEGIN_FUNCTION_MAP
		FN_1(em_setULogPath, TYPE_BOOL, SetULogPath, TYPE_STRING);
		FN_6(em_Export, TYPE_BOOL, Export, TYPE_INODE_TAB_BR, TYPE_STRING, TYPE_STRING, TYPE_BOOL, TYPE_BOOL, TYPE_STRING_TAB_BR);
	END_FUNCTION_MAP
};

//-----------------------------------------------------------------------------

inline string RageMaxAnimExprExportGUP::GenerateUniqueXYZControllerName(const char* szPrefix)
{
	return GenerateUniqueControllerName(szPrefix, "xyzCtrl");
}

inline string RageMaxAnimExprExportGUP::GenerateUniqueExprControllerName(const char* szPrefix)
{
	return GenerateUniqueControllerName(szPrefix, "exprCtrl");
}

inline string RageMaxAnimExprExportGUP::GenerateUniqueBlendControllerName(const char* szPrefix)
{
	return GenerateUniqueControllerName(szPrefix, "blendCtrl");
}

inline string RageMaxAnimExprExportGUP::GenerateUniqueObjectSpaceBlendControllerName(const char* szPrefix)
{
	return GenerateUniqueControllerName(szPrefix, "objBlendCtrl");
}

inline string RageMaxAnimExprExportGUP::GenerateUniqueLookAtControllerName(const char* szPrefix)
{
	return GenerateUniqueControllerName(szPrefix, "lookAtCtrl");
}

inline string RageMaxAnimExprExportGUP::GenerateUniqueObjectSpaceControllerName(const char* szPrefix)
{
	return GenerateUniqueControllerName(szPrefix, "objectSpaceCtrl");
}

inline string RageMaxAnimExprExportGUP::GenerateUniqueFloatConstantControllerName(const char* szPrefix)
{
	return GenerateUniqueControllerName(szPrefix, "floatConstantCtrl");
}

inline string RageMaxAnimExprExportGUP::GenerateUniqueControllerName(const char* szPrefix, const char* szControllerName)
{
	stringstream resultStream;
	resultStream << szPrefix << "_" << szControllerName << "_" << m_ctrlIdCounter++;
	return resultStream.str();
}

//-----------------------------------------------------------------------------

#endif //_RAGE_MAX_ANIMEXPREXPORT_H_

