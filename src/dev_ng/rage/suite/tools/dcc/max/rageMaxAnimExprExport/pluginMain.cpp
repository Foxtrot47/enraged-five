// /pluginMain.cpp

#include "system/param.h"
#include "diag/channel.h"
#include "parser/manager.h"

#include "resource.h"
#include "rageMaxAnimExprExport.h"
#include "utility.h"

using namespace rage;

//-----------------------------------------------------------------------------

HINSTANCE	hInstance;
BOOL		gbLibInitialized;

//-----------------------------------------------------------------------------

BOOL WINAPI DllMain(HINSTANCE hInstDll, ULONG fdwReason, LPVOID lpvReserved)
{
	hInstance = hInstDll;

	// This was being called prematurely while Max was still
	// running, causing issues when trying to use the parser
	//if(fdwReason == DLL_THREAD_DETACH)
	//{
	//	if(::rage::parManagerSingleton::IsInstantiated())
	//		SHUTDOWN_PARSER;
	//}

	return TRUE;
}

//-----------------------------------------------------------------------------

TCHAR *GetString(int id)
{
	static TCHAR buf[2048];

	if (hInstance)
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;

	return NULL;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_DESCRIPTION);
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) int LibNumberClasses() 
{
	return 1;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) ClassDesc* LibClassDesc(int i) 
{
	switch(i) {
		case 0: 
			return GetRageMaxAnimExprExportGupDesc();
		default: 
			return 0;
	}
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) ULONG LibVersion() 
{
	return VERSION_3DSMAX;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) ULONG CanAutoDefer()
{
	return 0;
}

//-----------------------------------------------------------------------------

static const char* sPrintMessage[DIAG_SEVERITY_COUNT] = 
{
	"Fatal Error: "
	"Error: ",
	"Warning: ",
	"",
	"",
	"",
	""
};

bool MAXPrinterFn(const diagChannel &,diagSeverity mode,const char *file,int line,const char *fmt, va_list args)
{
	const int bufferSize = RAGE_MAX_PATH*2;
	char strBufArg[bufferSize];
	ZeroMemory(strBufArg, bufferSize);
	//va_list args;
	//va_start (args, fmt);
	vsprintf_s(strBufArg, bufferSize, fmt, args);
	va_end (args);

	static char strBuf1[1024];
	formatf(strBuf1, sizeof(strBuf1), "%s(%d): %s%s\r\n", file, line, sPrintMessage[mode], strBufArg);

	EXPORTLOG.AppendEntry(strBuf1);

	return mode != DIAG_SEVERITY_FATAL;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) int LibInitialize(void)
{
	if(!gbLibInitialized)
	{
		char appName[] = "rageMaxAnimExprExport.gup";
		char* p_Argv[1];

		p_Argv[0] = appName;

		sysParam::Init(1,p_Argv);

		INIT_PARSER;
		INIT_EXPORTLOG;
		INIT_ATTRIBUTELOG;
		INIT_HASHNAMELOG;

		diagLogCallback = MAXPrinterFn;

		gbLibInitialized = TRUE;
	}

	return 1;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) int LibShutdown(void)
{
	//if(::rage::parManager::sm_Instance != NULL)
	//	SHUTDOWN_PARSER;

	if(::exprExportUtility::exprExportLogSingleton::IsInstantiated())
		SHUTDOWN_EXPORTLOG;

	return 1;
}

//-----------------------------------------------------------------------------

