// /IMaxAnimExprExport.h

#ifndef _IMAX_ANIMEXPREXPORT_H_
#define _IMAX_ANIMEXPREXPORT_H_

//-----------------------------------------------------------------------------

#define ANIMEXPREXPORT_INTERFACE	Interface_ID(0x2ccf5f8b, 0xc1f2bb8)

#define GetAnimExprExportInterface(cd) \
	(IMaxAnimExprExport*)(cd)->GetFPInterface(ANIMEXPREXPORT_INTERFACE)

//-----------------------------------------------------------------------------

class IMaxAnimExprExport : public FPStaticInterface
{
public:
	virtual ~IMaxAnimExprExport() {};

	virtual BOOL Export(Tab<INode*>& nodeArr, TCHAR* outPutPath, TCHAR* trackIdFilePath, BOOL bIndividualBones, BOOL bRecursive, Tab<char *> ignoreExprPatterns) = 0;
};

//-----------------------------------------------------------------------------

#endif //_IMAX_ANIMEXPREXPORT_H_
