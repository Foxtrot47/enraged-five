// /rageMaxAnimExprExport.cpp
#include <string>
#include <vector>
#include <stack>
#include <list>
//#include <map>
#include <queue>

#pragma warning(push)
#pragma warning(disable:4265)
#include "iexprCtrl.h"
#include "maxscrpt/numbers.h"
#pragma warning(pop)

#include <decomp.h>

#include "animExprScriptParser/exprParser.h"
#include "atl\string.h"
#include "vector\quaternion.h"
#include "vectormath\legacyconvert.h"

#include "resource.h"
#include "utility.h"
#include "rageMaxAnimExprExport.h"
#include "maxmorpher.h"

#include "ExpressionControl.h"

#pragma warning(push)
#pragma warning(disable:4265)
#include <IRsSpringCtrl.h> 
//#include "jiggle.h"
#pragma warning(pop)

using namespace std;
using namespace rage;

//#define IM_OUTSOURCE_TOOL 1

class ExprControl;
//-----------------------------------------------------------------------------

void CopyMaxMatrixToRageMatrix(const Matrix3& srcMtx, Matrix34& dstMtx)
{
	Point4 SrcRow;

	SrcRow = srcMtx.GetRow(0);
	dstMtx.a = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = srcMtx.GetRow(1);
	dstMtx.b = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = srcMtx.GetRow(2);
	dstMtx.c = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = srcMtx.GetRow(3);
	dstMtx.d = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
}

//-----------------------------------------------------------------------------

void CopyRageMatrixToMaxMatrix(const Matrix34& srcMtx, Matrix3& dstMtx)
{
	Point3 SrcRow;

	SrcRow = Point3(srcMtx.a.x,srcMtx.a.y,srcMtx.a.z);
	dstMtx.SetRow(0,SrcRow);
	SrcRow = Point3(srcMtx.b.x,srcMtx.b.y,srcMtx.b.z);
	dstMtx.SetRow(1,SrcRow);
	SrcRow = Point3(srcMtx.c.x,srcMtx.c.y,srcMtx.c.z);
	dstMtx.SetRow(2,SrcRow);
	SrcRow = Point3(srcMtx.d.x,srcMtx.d.y,srcMtx.d.z);
	dstMtx.SetRow(3,SrcRow);
}

bool ListControlHasLookAt(IListControl* pListControl)
{
	int numListItems = pListControl->GetListCount();

	for(int i=0; i<numListItems; i++)
	{
		::Control* pCtrl = pListControl->GetSubCtrl(i);
		if( pCtrl->ClassID() == Class_ID(LOOKAT_CONSTRAINT_CLASS_ID,0) )
		{
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

//NOTE: This enum was copied from the MaxSdk sample maxsdk/samples/controllers/lookat_cnstrnt.cpp.
//		It is used to get a look at controller's upnode from the controller's param block.
enum {	lookat_target_weight,		lookat_target_list,		lookat_relative, 
		lookat_vector_line_length,	set_orientation,		lookat_target_axis, 
		lookat_target_axis_flip,	lookat_upnode_axis,		lookat_upnode_world, 
		lookat_upnode_pick,			lookat_StoUP_axis,		lookat_StoUP_axis_flip, 
		viewline_length_abs,		lookat_upnode_control, };

//-----------------------------------------------------------------------------

//NOTE: This stub was copied from the LookAtConstRotation class in the the MaxSdk sample
//		maxsdk/samples/controllers/lookat_cnstrnt.cpp. It is used as a hack to gain access
//		to public members of the look at control that are not exposed by Max.
class LookAtStub : public Control
{
public:
	int unexplainedPad1;						// Not sure where these come from, but they are necessary
	int unexplainedPad2;

	Box3 bbox;

	int lookAtConstValidatorClassPad;			//lookAtConstValidatorClass validator;							
	int lookAtConstValidatorClassVTablePad1;
	int lookAtConstValidatorClassVTablePad2;
	int lookAtConstValidatorClassVTablePad3;

	HWND hWnd;
	int last_selection;
	int oldTargetNumber;

	Matrix3 sourceTM;
	float total_lookat_target_weight;

	int LookAtConstTimeChangeCallbackPad;		//LookAtConstTimeChangeCallback lookAtConstTimeChangeCallback;	
	int LookAtConstTimeChangeCallbackVTablePad;

	int IParamBlockPtrPad;						//IParamBlock2* pblock;

	DWORD flags;
	Quat curRot;
	Quat baseRotQuatLocal;
	Quat baseRotQuatWorld;
	Interval ivalid; 
	Quat InitialLookAtQuat;
	int initialLookAtFlag;
	Point3 baseEuler;
	Quat userRotQuat;
};

#include "MaxUtil/MaxUtil.h"

//-----------------------------------------------------------------------------

RageMaxAnimExprExportGUP sRageMaxAnimExprExportGUP (
	ANIMEXPREXPORT_INTERFACE, _T("rageAnimExprExport"), 0, NULL, FP_CORE,

	RageMaxAnimExprExportGUP::em_setULogPath, _T("setULogPath"), 0, TYPE_BOOL, 0, 1,
		_T("output path"), 0, TYPE_STRING,

	RageMaxAnimExprExportGUP::em_Export, _T("export"), 0, TYPE_BOOL, 0, 6,
		_T("node array"), 0, TYPE_INODE_TAB_BR,
		_T("output path"), 0, TYPE_STRING,
		_T("track id file path"), 0, TYPE_STRING,
		_T("individual bones"), 0, TYPE_BOOL,
		_T("recursive"), 0, TYPE_BOOL,
		_T("ignore wildcards"), 0, TYPE_STRING_TAB_BR,

	end
	);

//-----------------------------------------------------------------------------

class RageMaxAnimExprExportGUPClassDesc : public ClassDesc2
{
public:
	int				IsPublic()						{ return 1; }
	void*			Create(BOOL loading = FALSE)	{ return &sRageMaxAnimExprExportGUP; } 
	const TCHAR*	ClassName()						{ return GetString(IDS_ANIMEXPREXPORT); }
	SClass_ID		SuperClassID()					{ return GUP_CLASS_ID; } 
	Class_ID		ClassID()						{ return ANIMEXPREXPORT_GUP_CLASS_ID; }
	const TCHAR*	Category()						{ return GetString(IDS_CATEGORY); }
};

static RageMaxAnimExprExportGUPClassDesc AnimExprExportGUPDesc;

//-----------------------------------------------------------------------------

ClassDesc* GetRageMaxAnimExprExportGupDesc()
{
	return &AnimExprExportGUPDesc;
}

//-----------------------------------------------------------------------------

DWORD RageMaxAnimExprExportGUP::Start()
{
	mp_uLog = NULL;
	return GUPRESULT_KEEP;
}

//-----------------------------------------------------------------------------

void RageMaxAnimExprExportGUP::Stop()
{
}

void RageMaxAnimExprExportGUP::Msg(const char *msg, const char *context)
{
	if(mp_uLog)
		mp_uLog->LogMessage(msg, context);
	else
		PrintToLog(msg);
}
void RageMaxAnimExprExportGUP::Warn(const char *msg, const char *context)
{
	if(mp_uLog)
		mp_uLog->LogWarning(msg, context);
	else
		Warningf(msg);
}
void RageMaxAnimExprExportGUP::Error(const char *msg, const char *context)
{
	if(mp_uLog)
		mp_uLog->LogError(msg, context);
	else
		Errorf(msg);
}


//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreateFloatConstantController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* /*pContainerControl*/)
{
	if(pControl->NumKeys() > 1)
		Warningf("Warning: Controller has multiple keys to a constant controller. Node='%s', NumKeys=%d", pNode->GetName(), pControl->NumKeys());

	AeFloatConstantController* pFloatConstCtrl = new AeFloatConstantController();

	atString nrmNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, nrmNodeName);
	pFloatConstCtrl->m_id = GenerateUniqueFloatConstantControllerName(nrmNodeName.c_str()).c_str();

	char buffer[4096];
	sprintf(buffer, "Info: Creating Float Constant Controller '%s' for '%s'\r\n", pFloatConstCtrl->m_id.c_str(), nrmNodeName.c_str());
	Msg(buffer, nrmNodeName.c_str());
	//Displayf("Info: Creating Float Constant Controller '%s' for '%s'", pFloatConstCtrl->m_id.c_str(), nrmNodeName.c_str());

	Interval valid;
	pControl->GetValue(0, &pFloatConstCtrl->m_value, valid);

	return pFloatConstCtrl;
}

//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreateXYZController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, BOOL& bZerodControl, ::Control* pContainerControl)
{
	Assertf(pNode, "Cannot pass a NULL value for pNode to CreateXYZController.");
	Assertf(pControl, "Cannot pass NULL value for pControl to CreateXYZController.");

	atString nrmNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, nrmNodeName);

	char msgbuffer[4096];
	eComponentType type;
	Class_ID ctrlCid = pControl->ClassID();
	if (ctrlCid == IPOS_CONTROL_CLASS_ID)
	{
		type = kVector;
	}
	else if (ctrlCid == Class_ID(EULER_CONTROL_CLASS_ID, 0))
	{
		type = kQuat;
	}
	else if (ctrlCid == ISCALE_CONTROL_CLASS_ID)
	{
		type = kVector;
	}
	else
	{
		sprintf(msgbuffer, "Info: An unsupported controller type was passed to CreateXYZController.\r\n");
		Warn(msgbuffer, nrmNodeName.c_str());
		//Errorf("An unsupported controller type was passed to CreateXYZController.");
		return NULL;
	}

	AeXYZController* pXYZCtrl = new AeXYZController();

	pXYZCtrl->m_id = GenerateUniqueXYZControllerName(nrmNodeName.c_str()).c_str();
	pXYZCtrl->m_type = type;

	// First, try to make create expression controllers for the x, y, and z sub-controllers
	BOOL bBlendShape = FALSE; // Dummy
	pXYZCtrl->m_pXController = CreateController(theFile, exprBoneFile, pNode, pControl->GetXController(), bZerodControl, bBlendShape,  pControl);
	pXYZCtrl->m_pYController = CreateController(theFile, exprBoneFile, pNode, pControl->GetYController(), bZerodControl, bBlendShape, pControl);
	pXYZCtrl->m_pZController = CreateController(theFile, exprBoneFile, pNode, pControl->GetZController(), bZerodControl, bBlendShape, pControl);

	// If one of the sub-controllers was not supported, we cannot use this controller
	if (pXYZCtrl->m_pXController == NULL ||
		pXYZCtrl->m_pYController == NULL ||
		pXYZCtrl->m_pZController == NULL)
	{
		sprintf(msgbuffer, "Error: Failed to create XYZ controller %s, because one of the sub-controller types is not supported.\r\n", pXYZCtrl->m_id.c_str());
		Error(msgbuffer, pNode->GetName());
		//Errorf("Could not create XYZ controller because one of the sub-controller types is not supported.");
		delete pXYZCtrl;
		return NULL;
	}

	//TEST CODE: In the case of rotations the default pose for the joint comes from the skeleton data, so zero out any
	//float constant controllers that were generated.
	
	//NOTE : This may not be the correct solution here, it's possible that we may need to grab the rotation values for the node
	//from the bind pose, and remove them from the constants to get the correct default rotation constant to be evaluated in the
	//expression.
	/*if(type == kQuat)
	{
		AeFloatConstantController* pXFloatConstCtrl = dynamic_cast<AeFloatConstantController*>(pXYZCtrl->m_pXController);
		if(pXFloatConstCtrl)
		{
			pXFloatConstCtrl->m_value = 0.0f;
		}

		AeFloatConstantController* pYFloatConstCtrl = dynamic_cast<AeFloatConstantController*>(pXYZCtrl->m_pYController);
		if(pYFloatConstCtrl)
		{
			pYFloatConstCtrl->m_value = 0.0f;
		}

		AeFloatConstantController* pZFloatConstCtrl = dynamic_cast<AeFloatConstantController*>(pXYZCtrl->m_pZController);
		if(pZFloatConstCtrl)
		{
			pZFloatConstCtrl->m_value = 0.0f;
		}
	}*/

	return pXYZCtrl;
}

//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreateBlendController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, BOOL& bZerodControl, ::Control* pContainerControl)
{
	IListControl* pListControl = GetIListControlInterface(pControl);
	if(!pListControl)
	{
		return NULL;
	}

	atString nrmNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, nrmNodeName);

	char msgbuffer[4096];
	eComponentType type;
	Class_ID ctrlCid = pControl->ClassID();
	if (ctrlCid == Class_ID(FLOATLIST_CONTROL_CLASS_ID, 0))
	{
		type = kFloat;
	}
	else if (ctrlCid == Class_ID(POSLIST_CONTROL_CLASS_ID, 0))
	{
		type = kVector;
	}
	else if (ctrlCid == Class_ID(ROTLIST_CONTROL_CLASS_ID, 0))
	{
		type = kQuat;
	}
	else if (ctrlCid == Class_ID(SCALELIST_CONTROL_CLASS_ID, 0))
	{
		type = kVector;
	}
	else
	{
		sprintf(msgbuffer, "An unsupported controller type was passed to CreateBlendController.\r\n");
		Warn(msgbuffer, nrmNodeName.c_str());
		//Errorf("An unsupported controller type was passed to CreateBlendController.");
		return NULL;
	}
	
	AeBlendController* pBlendCtrl = new AeBlendController();

	pBlendCtrl->m_id = GenerateUniqueBlendControllerName(nrmNodeName.c_str()).c_str();
	pBlendCtrl->m_type = type;

	// Check if the list control contains a lookat control, if it does we ignore all other controls on the list. This stops issues with redundant
	// controls getting processed and creating invalid expression multiplication operations
	bool bContainsLookAt = ListControlHasLookAt(pListControl);
	
	int numListItems = pListControl->GetListCount();
	for(int i=0; i<numListItems; i++)
	{
		sprintf(msgbuffer, "Info: List item %d\r\n", i+1);
		Msg(msgbuffer, nrmNodeName.c_str());
		//Displayf("Info: List item %d", i+1);

		BOOL bBlendShape=FALSE; // Dummy
		::Control* pListItemCtrl = pListControl->GetSubCtrl(i);

		if( pListItemCtrl->ClassID() != Class_ID(LOOKAT_CONSTRAINT_CLASS_ID,0) && bContainsLookAt ) continue;

		AeController* pBlendItemController = CreateController(theFile, exprBoneFile, pNode, pListItemCtrl, bZerodControl, bBlendShape, pControl);
		if(pBlendItemController)
		{
			string relativeTranslate;
			if(exprExportUtility::GetRelativeTranslateOverride(pNode, relativeTranslate))
			{
				if(stricmp(relativeTranslate.c_str(),"FALSE") == 0)
				{
					if(pListControl->GetName(i) == CStr("Frozen Position"))
						continue;
				}
			}

			string relativeRotation;
			if(exprExportUtility::GetRelativeRotateOverride(pNode, relativeRotation))
			{
				if(stricmp(relativeRotation.c_str(),"TRUE") == 0)
				{
					if(pListControl->GetName(i) == CStr("Frozen Rotation"))
						continue;
				}
			}

			//HACK:  At the moment, this is a cheap way to prune out unwanted controllers.
			//Currently we simply don't want any XYZ/Euler Controllers that have all float constant sub-controllers with a value of zero.
			
			AeXYZController* pXYZController = dynamic_cast<AeXYZController*>(pBlendItemController);
			if(pXYZController != NULL)
			{
				AeFloatConstantController* pXFloatConstCtrl = dynamic_cast<AeFloatConstantController*>(pXYZController->m_pXController);
				AeFloatConstantController* pYFloatConstCtrl = dynamic_cast<AeFloatConstantController*>(pXYZController->m_pYController);
				AeFloatConstantController* pZFloatConstCtrl = dynamic_cast<AeFloatConstantController*>(pXYZController->m_pZController);
				if(pXFloatConstCtrl && pYFloatConstCtrl && pZFloatConstCtrl
					&& pXFloatConstCtrl->m_value == 0.0f && pYFloatConstCtrl->m_value == 0.0f && pZFloatConstCtrl->m_value == 0.0f ||
					pXFloatConstCtrl && pYFloatConstCtrl && pZFloatConstCtrl
						&& pXFloatConstCtrl->m_value == 1.0f && pYFloatConstCtrl->m_value == 1.0f && pZFloatConstCtrl->m_value == 1.0f)
				{
					continue;
				}				
			}
			
			AeBlendControllerItem* pBlendItem = new AeBlendControllerItem();
			pBlendItem->m_weight = pListControl->GetSubCtrlWeight(i, 0);
			pBlendItem->m_pController = pBlendItemController;
			pBlendCtrl->m_inputs.PushAndGrow(pBlendItem);
		}
	}

	if (pBlendCtrl->m_inputs.size() <= 0)
	{
		delete pBlendCtrl;
		return NULL;
	}

	return pBlendCtrl;
}

//-----------------------------------------------------------------------------

int GetControllerComponent(::Control* pCompCtrl, ::Control* pCtrl )
{
	if(pCtrl->GetXController() == pCompCtrl)
		return 0;
	else if(pCtrl->GetYController() == pCompCtrl)
		return 1;
	else if(pCtrl->GetZController() == pCompCtrl)
		return 2;
	else
		return -1;
}

//-----------------------------------------------------------------------------

class RefSearchDependentEnumProc : public DependentEnumProc
{
public:
	RefSearchDependentEnumProc(Class_ID searchCid)
		: m_searchCid(searchCid)
		, m_pMaker(NULL)
	{};

	virtual ~RefSearchDependentEnumProc() {};

	virtual int proc(ReferenceMaker *pMaker)
	{
		Class_ID cid = pMaker->ClassID();
		if( cid == m_searchCid)
		{
			m_pMaker = pMaker;
			return DEP_ENUM_HALT;
		}
		else
			return DEP_ENUM_CONTINUE;
	}

	ReferenceMaker* GetResult() { return m_pMaker; }

private:
	Class_ID m_searchCid;
	ReferenceMaker*	m_pMaker;
};

//-----------------------------------------------------------------------------

ReferenceMaker* FindRefererOfClassID(::ReferenceTarget* pRefTarget, Class_ID cid)
{
	RefSearchDependentEnumProc enumerator(cid);

	pRefTarget->DoEnumDependents(&enumerator);

	return enumerator.GetResult();
}

//-----------------------------------------------------------------------------

ReferenceTarget* FindReferenceOfClassID2(::ReferenceTarget* pRefTarget, Class_ID cid)
{
	stack< ::ReferenceTarget* > dfsStack;

#ifdef _DEBUG
	DebugPrint("FindReferenceOfClassID, Searching for ( %x, %x )\r\n", cid.PartA(), cid.PartB());
#endif //_DEBUG

	dfsStack.push( pRefTarget );
	
	while(!dfsStack.empty())
	{
		::ReferenceTarget* pCurTarget = dfsStack.top();
		dfsStack.pop();

#if (MAX_RELEASE >= 12000)
		DependentIterator pItem(pCurTarget);
		ReferenceMaker* maker = NULL;

		while(NULL != (maker = pItem.Next()))
		{
			RefMakerHandle hMaker = maker;
#else
		RefList& rList = pCurTarget->GetRefList();
		RefListItem* pRefItem = rList.FirstItem();
		while( pRefItem )
		{
			RefMakerHandle hMaker = pRefItem->maker;
#endif  //MAX_RELEASE >= 12000
			ReferenceTarget* pTarget = static_cast<ReferenceTarget*>(hMaker);

			Class_ID targetCid = pTarget->ClassID();

#ifdef _DEBUG
			MSTR mstrTargetClassName;
			pTarget->GetClassName( mstrTargetClassName );
			const char* szTargetClassName = mstrTargetClassName.data();
			DebugPrint("( %x, %x ) : %s\n", targetCid.PartA(), targetCid.PartB(), szTargetClassName );
#endif //_DEBUG

			if( targetCid == cid )
				return pTarget;
			
			dfsStack.push( pTarget );
#if (MAX_RELEASE < 12000)
			pRefItem = pRefItem->next;
#endif  
		}
	}

	return NULL;
}

//Finds a parent INode from a Control - fixes a problem that the expression
//exporter wasn't spitting out the correct driver of an expression.
//To do with assumptions about the way the reference system works.

//this could easily go into a general utility set for all plugins
//but I just want to get this checked in for a bug fix right now
//GRS 7/12/2009
class FindNodeFromDirectController : public DependentEnumProc
{
public:
	FindNodeFromDirectController(Class_ID cid, ::Control* pLookForControl): 
		m_id(cid),
		m_pLookForControl(pLookForControl),
		m_pFound(NULL)
	{
	}

	virtual ~FindNodeFromDirectController() {}

	Class_ID m_id;
	::Control* m_pLookForControl;
	INode* m_pFound;

	bool FindController(Control* check)
	{
		if(!check)
			return false;

		if(m_pLookForControl == check)
			return true;

		for(int i=0;i<check->NumSubs();i++)
		{
			//more general than what I had before but makes
			//the assumption that all subanims of a Control are
			//also derived from Control. If it falls over anywhere
			//near here then that assumption is wrong and more checking
			//is required on what get's passed through here.
			//GRS 10/12/2009

			Animatable* subanim = check->SubAnim(i);

			//if(subanim->IsSubClassOf() && FindController(subanim))
			if(FindController((Control*)subanim))
				return true;			
		}

		return false;
	}

	virtual int proc(ReferenceMaker *rmaker)
	{
		Class_ID targetCid = rmaker->ClassID();

		if( targetCid == m_id )
		{
			INode* pNode = static_cast<INode*>(rmaker);

			DebugPrint("Node: %s\n",pNode->GetName());

			if(FindController(pNode->GetTMController()))
			{
				m_pFound = pNode;
				return DEP_ENUM_HALT;
			}
		}		

		return DEP_ENUM_CONTINUE;
	};

	static INode* Find(::Control* pControl)
	{
		FindNodeFromDirectController proc(Class_ID(BASENODE_CLASS_ID,0),pControl);

		pControl->DoEnumDependents(&proc);

		return proc.m_pFound;
	}
};

//-----------------------------------------------------------------------------

ReferenceTarget* FindReferenceOfClassID(::ReferenceTarget* pRefTarget, Class_ID cid)
{

#ifdef _DEBUG
	int refIdx = 0;
	DebugPrint("FindReferenceOfClassID, Searching for ( %x, %x )", cid.PartA(), cid.PartB());
#endif //_DEBUG

#if (MAX_RELEASE >= 12000)
	DependentIterator pItem(pRefTarget);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		RefMakerHandle hMaker = maker;
#else
	RefList& rList = pRefTarget->GetRefList();
	RefListItem* pRefItem = rList.FirstItem();
	while(pRefItem)
	{
		RefMakerHandle hMaker = pRefItem->maker;
#endif  //MAX_RELEASE >= 12000
		ReferenceTarget* pTarget = static_cast<ReferenceTarget*>(hMaker);

		Class_ID targetCid = pTarget->ClassID();

#ifdef _DEBUG
		MSTR mstrTargetClassName;
		pTarget->GetClassName( mstrTargetClassName );
		const char* szTargetClassName = mstrTargetClassName.data();
		DebugPrint("\t%d : ( %x, %x ) : %s", refIdx++, targetCid.PartA(), targetCid.PartB(), szTargetClassName );
#endif //_DEBUG

		if(targetCid == cid)
			return pTarget;

#if (MAX_RELEASE < 12000)
		pRefItem = pRefItem->next;
#endif
	}
		
	return NULL;
}

//-----------------------------------------------------------------------------

eEvaluationSpace GetDriverComponentEvaluationSpace(INode* pNode, eComponentType cmpType)
{
	//char* pName = pNode->GetName();

	//The driver component should be evaluated in the "default" space unless the bone in MAX has had it's transorm
	//frozen.  This can be determined by inspecting the controllers assigned to the bones rotation and looking for 
	//a list controller that contains sub controllers, the first of which is an XYZ controller named
	//"Frozen Rotation" or "Frozed Position", and the second which is another XYZ controller
	if(cmpType == kQuat)
	{
		::Control* pTopRotationCtrl = pNode->GetTMController()->GetRotationController();
		Class_ID topCid = pTopRotationCtrl->ClassID();
		if(topCid == Class_ID(ROTLIST_CONTROL_CLASS_ID,0))
		{
			IListControl* pTopListCtrl = GetIListControlInterface(pTopRotationCtrl);
			Assertf(pTopRotationCtrl, "Failed to retrieve IListControl interface for rotation controller of node '%s'", pNode->GetName());
			int nListCount = pTopListCtrl->GetListCount();
			if(nListCount >= 2)
			{
				MSTR ctrl0Name = pTopListCtrl->GetName(0);
				::Control* pCtrl0 = pTopListCtrl->GetSubCtrl(0);
				Class_ID ctrl0Cid = pCtrl0->ClassID();

				::Control* pCtrl1 = pTopListCtrl->GetSubCtrl(1);
				Class_ID ctrl1Cid = pCtrl1->ClassID();

				if( (ctrl0Cid.PartA() == EULER_CONTROL_CLASS_ID) && 
					(ctrl0Name == CStr("Frozen Rotation")) &&
					(ctrl1Cid.PartA() == EULER_CONTROL_CLASS_ID) )
				{
					return kZeroed;
				}
			}
		}
	}
	else
	{
		::Control* pTopTranslateCtrl = pNode->GetTMController()->GetPositionController();
		Class_ID topCid = pTopTranslateCtrl->ClassID();
		if(topCid == Class_ID(POSLIST_CONTROL_CLASS_ID,0))
		{
			IListControl* pTopListCtrl = GetIListControlInterface(pTopTranslateCtrl);
			Assertf(pTopListCtrl, "Failed to retrieve IListControl interface for position controller of node '%s'", pNode->GetName());
			int nListCount = pTopListCtrl->GetListCount();
			if(nListCount >= 2)
			{
				MSTR ctrl0Name = pTopListCtrl->GetName(0);
				::Control* pCtrl0 = pTopListCtrl->GetSubCtrl(0);
				Class_ID ctrl0Cid = pCtrl0->ClassID();

				::Control* pCtrl1 = pTopListCtrl->GetSubCtrl(1);
				Class_ID ctrl1Cid = pCtrl1->ClassID();

				if( ((ctrl0Cid == IPOS_CONTROL_CLASS_ID) || (ctrl0Cid.PartA() == HYBRIDINTERP_POSITION_CLASS_ID)) &&
					(ctrl0Name == CStr("Frozen Position")) &&
					(ctrl1Cid == IPOS_CONTROL_CLASS_ID) )
				{
					return kZeroed;
				}
			}
		}
	}

	return kDefault;
}

//-----------------------------------------------------------------------------

BOOL RageMaxAnimExprExportGUP::CollectDriverComponentAttributes(INode* pDriverNode, const char* szDriverComponentId,
																eComponentType& type, eComponentRotationOrder& rotOrder, eComponentAxis& axis)
{
	rotOrder = kXYZ;	// Default rotation order

	if (strcmp(szDriverComponentId, "translateX") == 0)
	{
		type = kVector;
		axis = kXAxis;
	}
	else if (strcmp(szDriverComponentId, "translateY") == 0)
	{
		type = kVector;
		axis = kYAxis;
	}
	else if (strcmp(szDriverComponentId, "translateZ") == 0)
	{
		type = kVector;
		axis = kZAxis;
	}
	else if (strcmp(szDriverComponentId, "translate") == 0)
	{
		type = kVector;
		axis = kAxisAll;
	}
	else if (strcmp(szDriverComponentId, "scaleX") == 0)
	{
		type = kVector;
		axis = kXAxis;
	}
	else if (strcmp(szDriverComponentId, "scaleY") == 0)
	{
		type = kVector;
		axis = kYAxis;
	}
	else if (strcmp(szDriverComponentId, "scaleZ") == 0)
	{
		type = kVector;
		axis = kZAxis;
	}
	else if (strcmp(szDriverComponentId, "scale") == 0)
	{
		type = kVector;
		axis = kAxisAll;
	}
	else if (strcmp(szDriverComponentId, "rotateX") == 0)
	{
		type = kQuat;
		axis = kXAxis;
	}
	else if (strcmp(szDriverComponentId, "rotateY") == 0)
	{
		type = kQuat;
		axis = kYAxis;
	}
	else if (strcmp(szDriverComponentId, "rotateZ") == 0)
	{
		type = kQuat;
		axis = kZAxis;
	}
	else if (strcmp(szDriverComponentId, "rotate") == 0)
	{
		type = kQuat;
		axis = kAxisAll;

		if (exprExportUtility::IsNodeOfClassId(pDriverNode, BONE_OBJ_CLASSID))
		{
			::Control* pControl = pDriverNode->GetTMController()->GetRotationController();
			if (pControl)
			{
				IEulerControl* pEulerCtrl = (IEulerControl*)( pControl->GetInterface(I_EULERCTRL) );
				if (pEulerCtrl)
				{
					switch (pEulerCtrl->GetOrder())
					{
					case EULERTYPE_XYZ: rotOrder = kXYZ; break;
					case EULERTYPE_XZY: rotOrder = kXZY; break;
					case EULERTYPE_YXZ: rotOrder = kYXZ; break;
					case EULERTYPE_YZX: rotOrder = kYZX; break;
					case EULERTYPE_ZXY: rotOrder = kZXY; break;
					case EULERTYPE_ZYX: rotOrder = kZYX; break;
					default:
						Assertf(0, "Unknown rotation order found on bone '%s'", pDriverNode->GetName());
						break;
					}
				}
			}
		}
	}
	else
	{
		type = kFloat;
		axis = /*kAxisNone;*/ kAxisAll;
	}

	return TRUE;
}

//-----------------------------------------------------------------------------
#include "crAnimation/animTrack.h"

BOOL RageMaxAnimExprExportGUP::CreateDriver(AeFile& theFile, AeFile& exprBoneFile, INode* pDriverNode, const char* szDriverComponentId,
											AeDriver** pOutDriver, AeDriverComponent** pOutDriverComponent)
{
	atString szDriverId;
	exprExportUtility::GetNormalizedNodeName(pDriverNode, szDriverId);

	char msgbuffer[4096];

	AeDriver* pDriver = NULL;

	//Check to see if this driver has already been added to the library...
	for(atArray< AeDriver* >::iterator it = theFile.m_driverLibrary.begin();
		it != theFile.m_driverLibrary.end();
		++it)
	{
		AeDriver *pCurDriver = (*it);
		if(strcmpi(pCurDriver->m_id.c_str(),szDriverId.c_str())==0)
		{	
			//There is already an entry for this driver id
			pDriver = pCurDriver;
			(*pOutDriver) = pDriver;
			break;
		}
	}

	if(!pDriver)
	{
		//No driver entry was found with the supplied driver id, so create a new one
		pDriver = new AeDriver();

		pDriver->m_id = szDriverId.c_str();

		if(pDriverNode == GetCOREInterface()->GetRootNode() || 
			MaxUtil::IsRootSkelNode(pDriverNode) )
		{
			pDriver->m_boneId = 0;
		}
		else
		{
			string szBoneId;
			if (!exprExportUtility::GetBoneId(pDriverNode, szBoneId))
			{
				szBoneId = pDriver->m_id;
			}

			pDriver->m_boneId = crAnimTrack::ConvertBoneNameToId(szBoneId.c_str());
		}

		if(m_exportMainFile)
		{
			theFile.m_driverLibrary.PushAndGrow(pDriver);
		}
		else
		{
			exprBoneFile.m_driverLibrary.PushAndGrow(pDriver);
		}
		(*pOutDriver) = pDriver;
	}

	//Check to see if the driver has a component entry that matches the supplied component id
	bool bComponentExists = false;
	for(atArray< AeDriverComponent >::iterator it = pDriver->m_components.begin();
		it != pDriver->m_components.end();
		++it)
	{
		if(strcmpi((*it).m_id.c_str(),szDriverComponentId)==0)
		{
			(*pOutDriverComponent) = &(*it);
			bComponentExists = true;
			break;
		}
	}

	if(!bComponentExists)
	{
		//The component doesn't exist, so add it
		AeDriverComponent cmp;
		cmp.m_id = szDriverComponentId;

		// Based on the component id, collect the component's attributes
		CollectDriverComponentAttributes(pDriverNode, szDriverComponentId, cmp.m_type, cmp.m_rotOrder, cmp.m_axis);
		
		// Override track id from object properties

		atString strDriverComponent(szDriverComponentId);

		string trackIdOverride;
		if (exprExportUtility::GetTrackIdOverrideTranslate(pDriverNode, trackIdOverride) && (strDriverComponent.IndexOf("translate") != -1))
		{
			if(cmp.m_type == kVector)
			{
				cmp.m_trackId = TrackIDLookup(trackIdOverride.c_str());

				// MPW - Hack the face tint sliders so they are 1D and run as a float track not a vector along the Y. url:bugstar:1811598
				if(cmp.m_trackId == kTrackFacialTinting)
				{
					cmp.m_type = kFloat;
					cmp.m_axis = kAxisAll;
				}
			}

			if(cmp.m_type != kVector && cmp.m_type != kQuat && (cmp.m_trackId != kTrackFacialTinting))
			{
				cmp.m_trackId = kTrackFacialControl;
			}

			if(cmp.m_trackId == -1)
			{
				sprintf(msgbuffer, "Error: TrackId Override (Translate) is incorrect and not found in lookup. Override: %s on %s.", trackIdOverride.c_str(), pDriverNode->GetName());
				Error(msgbuffer);
			}
		}
		else if(exprExportUtility::GetTrackIdOverrideRotate(pDriverNode, trackIdOverride) && (strDriverComponent.IndexOf("rotate") != -1))
		{
			if(cmp.m_type == kQuat)
				cmp.m_trackId = TrackIDLookup(trackIdOverride.c_str());

			if(cmp.m_type != kVector && cmp.m_type != kQuat)
			{
				cmp.m_trackId = kTrackFacialControl;
			}

			if(cmp.m_trackId == -1)
			{
				sprintf(msgbuffer, "Error: TrackId Override (Rotate) is incorrect and not found in lookup. Override: %s on %s.", trackIdOverride.c_str(), pDriverNode->GetName());
				Error(msgbuffer);
			}
		}
		else if(exprExportUtility::GetTrackIdOverrideScale(pDriverNode, trackIdOverride) && (strDriverComponent.IndexOf("scale") != -1))
		{
			if(cmp.m_type == kVector)
				cmp.m_trackId = TrackIDLookup(trackIdOverride.c_str());

			if(cmp.m_type != kVector && cmp.m_type != kQuat)
			{
				cmp.m_trackId = kTrackFacialControl;
			}

			if(cmp.m_trackId == -1)
			{
				sprintf(msgbuffer, "Error: TrackId Override (Scale) is incorrect and not found in lookup. Override: %s on %s.", trackIdOverride.c_str(), pDriverNode->GetName());
				Error(msgbuffer);
			}
		}
		else
		{
			//If the driver node is a bone, and this is a transform based accessor then we need to assign the standard
			//bone animation track id's
			if( exprExportUtility::IsNodeOfClassId(pDriverNode,BONE_OBJ_CLASSID) ||
				exprExportUtility::IsNodeOfClassId(pDriverNode,SKELOBJ_CLASS_ID) )
			{
				Assertf( (cmp.m_type == kVector) || (cmp.m_type == kQuat), 
					"Found a joint driver component that isn't a quat or vector type. %.%", szDriverId, szDriverComponentId);

				if(cmp.m_type == kVector)
					cmp.m_trackId = kTrackBoneTranslation;
				else if(cmp.m_type == kQuat)
					cmp.m_trackId = kTrackBoneRotation;

				cmp.m_evalSpace = GetDriverComponentEvaluationSpace(pDriverNode, cmp.m_type);
			}
			else
			{
				//If we couldn't find a specific user assigned track-id then fallback on assigning a 
				//"generic" default based on the component type
				if(cmp.m_type == kVector)
				{
					cmp.m_trackId = kTrackFacialTranslation;
					sprintf(msgbuffer, "Warning: Failed to locate a valid track id for '%s.%s', defaulting to TRACK_FACIAL_TRANSLATION", pDriverNode->GetName(), szDriverComponentId);
					Warn(msgbuffer, pDriverNode->GetName());
				}
				else if(cmp.m_type == kQuat)
				{
					cmp.m_trackId = kTrackFacialRotation;
					sprintf(msgbuffer, "Warning: Failed to locate a valid track id for '%s.%s', defaulting to TRACK_FACIAL_ROTATION", pDriverNode->GetName(), szDriverComponentId);
					Warn(msgbuffer, pDriverNode->GetName());
				}
				else
				{
					cmp.m_trackId = kTrackFacialControl;
					sprintf(msgbuffer, "Warning: Failed to locate a valid track id for '%s.%s', defaulting to TRACK_FACIAL_CONTROL", pDriverNode->GetName(), szDriverComponentId);
					Warn(msgbuffer, pDriverNode->GetName());
				}
			}
		}

		

		pDriver->m_components.PushAndGrow(cmp);
		(*pOutDriverComponent) = &pDriver->m_components[pDriver->m_components.size() - 1];
	}

	return true;
}

// We need this to ignore track id's with 23 (ANIMATED NORMAL MAPS) - We dont want these added to the attributes file
BOOL RageMaxAnimExprExportGUP::CheckParentNodeForTrackID(INode* pDriverNodeParent, const char* szDriverComponentId)
{
	AeDriverComponent cmp;
	// Based on the component id, collect the component's attributes
	CollectDriverComponentAttributes(pDriverNodeParent, szDriverComponentId, cmp.m_type, cmp.m_rotOrder, cmp.m_axis);

	if(cmp.m_type == kFloat)
	{
		std::string trackIdOverride;

		char* pName = pDriverNodeParent->GetName();

		if(exprExportUtility::GetTrackIdOverrideRotate(pDriverNodeParent, trackIdOverride) || exprExportUtility::GetTrackIdOverrideTranslate(pDriverNodeParent, trackIdOverride))
		{
				if(TrackIDLookup(trackIdOverride.c_str()) == rage::kTrackAnimatedNormalMaps)
				{
					return false;
				}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

BOOL RageMaxAnimExprExportGUP::CreateDriverForAttribute(AeFile& theFile, AeFile& exprBoneFile, INode* pDriverNodeParent, INode* pDriverNode, const char* szTargetString, const char* szDriverComponentId,
											AeDriver** pOutDriver, AeDriverComponent** pOutDriverComponent)
{

	char msgbuffer[4096];
	atString szDriverId;
	exprExportUtility::GetNormalizedNodeName(pDriverNode, szDriverId);

	// Setup the driver id to be ctrl_attribute
	szDriverId = GetAttributeDriverName(szDriverId,atString(szDriverComponentId));

	// Need to ignore track id's with 23 (ANIMATED NORMAL MAPS)
	bool bAddAttribute = CheckParentNodeForTrackID(pDriverNodeParent, szDriverComponentId);

	AeDriver* pDriver = NULL;

	//Check to see if this driver has already been added to the library...
	for(atArray< AeDriver* >::iterator it = theFile.m_driverLibrary.begin();
		it != theFile.m_driverLibrary.end();
		++it)
	{
		AeDriver *pCurDriver = (*it);
		if(strcmpi(pCurDriver->m_id.c_str(),szDriverId.c_str())==0)
		{	
			//There is already an entry for this driver id
			pDriver = pCurDriver;
			(*pOutDriver) = pDriver;
			break;
		}
	}

	if(!pDriver)
	{
		//No driver entry was found with the supplied driver id, so create a new one
		pDriver = new AeDriver();

		pDriver->m_id = szDriverId.c_str();

		if(pDriverNode == GetCOREInterface()->GetRootNode())
		{
			pDriver->m_boneId = 0;
		}
		else
		{
			/*string szBoneId;
			if (!exprExportUtility::GetBoneId(pDriverNode, szBoneId))
			{
				szBoneId = pDriver->m_id;
			}*/

			pDriver->m_boneId = crAnimTrack::ConvertBoneNameToId(pDriver->m_id.c_str());
		}

		if(m_exportMainFile)
		{
			theFile.m_driverLibrary.PushAndGrow(pDriver);
		}
		else
		{
			exprBoneFile.m_driverLibrary.PushAndGrow(pDriver);
		}
		(*pOutDriver) = pDriver;
	}

	//Check to see if the driver has a component entry that matches the supplied component id
	bool bComponentExists = false;
	for(atArray< AeDriverComponent >::iterator it = pDriver->m_components.begin();
		it != pDriver->m_components.end();
		++it)
	{
		if(strcmpi((*it).m_id.c_str(),szDriverComponentId)==0)
		{
			(*pOutDriverComponent) = &(*it);
			bComponentExists = true;
			break;
		}
	}

	if(!bComponentExists)
	{
		//The component doesn't exist, so add it
		AeDriverComponent cmp;
		cmp.m_id = szDriverComponentId;

		// Based on the component id, collect the component's attributes
		CollectDriverComponentAttributes(pDriverNode, szDriverComponentId, cmp.m_type, cmp.m_rotOrder, cmp.m_axis);

		atString strDriverComponent(szDriverComponentId);

		string trackIdOverride;
		if (exprExportUtility::GetTrackIdOverrideTranslate(pDriverNode, trackIdOverride) && (strDriverComponent.IndexOf("translate") != -1))
		{
			if(cmp.m_type == kVector)
				cmp.m_trackId = TrackIDLookup(trackIdOverride.c_str());

			if(cmp.m_type != kVector && cmp.m_type != kQuat)
			{
				cmp.m_trackId = kTrackFacialControl;
			}

			if(cmp.m_trackId == -1)
			{
				sprintf(msgbuffer, "Error: TrackId Override (Translate) is incorrect and not found in lookup. Override: %s on %s.", trackIdOverride.c_str(), pDriverNode->GetName());
				Error(msgbuffer);
			}
		}
		else if(exprExportUtility::GetTrackIdOverrideRotate(pDriverNode, trackIdOverride) && (strDriverComponent.IndexOf("rotate") != -1))
		{
			if(cmp.m_type == kQuat)
				cmp.m_trackId = TrackIDLookup(trackIdOverride.c_str());

			if(cmp.m_type != kVector && cmp.m_type != kQuat)
			{
				cmp.m_trackId = kTrackFacialControl;
			}

			if(cmp.m_trackId == -1)
			{
				sprintf(msgbuffer, "Error: TrackId Override (Rotate) is incorrect and not found in lookup. Override: %s on %s.", trackIdOverride.c_str(), pDriverNode->GetName());
				Error(msgbuffer);
			}
		}
		else if(exprExportUtility::GetTrackIdOverrideScale(pDriverNode, trackIdOverride) && (strDriverComponent.IndexOf("scale") != -1))
		{
			if(cmp.m_type == kVector)
				cmp.m_trackId = TrackIDLookup(trackIdOverride.c_str());

			if(cmp.m_type != kVector && cmp.m_type != kQuat)
			{
				cmp.m_trackId = kTrackFacialControl;
			}

			if(cmp.m_trackId == -1)
			{
				sprintf(msgbuffer, "Error: TrackId Override (Scale) is incorrect and not found in lookup. Override: %s on %s.", trackIdOverride.c_str(), pDriverNode->GetName());
				Error(msgbuffer);
			}
		}
		else
		{
			//If the driver node is a bone, and this is a transform based accessor then we need to assign the standard
			//bone animation track id's
			if( exprExportUtility::IsNodeOfClassId(pDriverNode,BONE_OBJ_CLASSID) ||
				exprExportUtility::IsNodeOfClassId(pDriverNode,SKELOBJ_CLASS_ID) )
			{
				Assertf( (cmp.m_type == kVector) || (cmp.m_type == kQuat), 
					"Found a joint driver component that isn't a quat or vector type. %.%", szDriverId, szDriverComponentId);

				if(cmp.m_type == kVector)
					cmp.m_trackId = kTrackBoneTranslation;
				else if(cmp.m_type == kQuat)
					cmp.m_trackId = kTrackBoneRotation;

				cmp.m_evalSpace = GetDriverComponentEvaluationSpace(pDriverNode, cmp.m_type);
			}
			else
			{
				//If we couldn't find a specific user assigned track-id then fallback on assigning a 
				//"generic" default based on the component type
				if(cmp.m_type == kVector)
				{
					cmp.m_trackId = kTrackFacialTranslation;
					sprintf(msgbuffer, "Warning: Failed to locate a valid track id for '%s.%s', defaulting to TRACK_FACIAL_TRANSLATION", pDriverNode->GetName(), szDriverComponentId);
					Warn(msgbuffer, pDriverNode->GetName());
				}
				else if(cmp.m_type == kQuat)
				{
					cmp.m_trackId = kTrackFacialRotation;
					sprintf(msgbuffer, "Warning: Failed to locate a valid track id for '%s.%s', defaulting to TRACK_FACIAL_ROTATION", pDriverNode->GetName(), szDriverComponentId);
					Warn(msgbuffer, pDriverNode->GetName());
				}
				else
				{
					cmp.m_trackId = kTrackFacialControl;
					sprintf(msgbuffer, "Warning: Failed to locate a valid track id for '%s.%s', defaulting to TRACK_FACIAL_CONTROL", pDriverNode->GetName(), szDriverComponentId);
					Warn(msgbuffer, pDriverNode->GetName());
				}
			}

			if(bAddAttribute)
			{
				char str[RAGE_MAX_PATH*2];
				sprintf(str, "%s,%s", szTargetString, szDriverId.c_str() );
				ATTRIBUTELOG.AddEntry(atString(str));
			}
			
			AddToHashMap(szDriverId.c_str());
		}

		pDriver->m_components.PushAndGrow(cmp);
		(*pOutDriverComponent) = &pDriver->m_components[pDriver->m_components.size() - 1];
	}

	return true;
}

//-----------------------------------------------------------------------------

BOOL RageMaxAnimExprExportGUP::CreateDriver(AeFile& theFile, AeFile& exprBoneFile, ::Control* pControl, AeDriver** pOutDriver, AeDriverComponent** pOutDriverComponent )
{
	Class_ID ctrlCid = pControl->ClassID();

	char msgbuffer[4096];

	if( (ctrlCid.PartA() != HYBRIDINTERP_FLOAT_CLASS_ID) &&
		(ctrlCid.PartA() != LININTERP_FLOAT_CLASS_ID) &&
		(ctrlCid.PartA() != TCBINTERP_FLOAT_CLASS_ID) &&
		(ctrlCid.PartA() != FLOATLIMITCTRL_CLASS_ID) )
	{
		MSTR tstrTgtCtrlClassName;
#if (MAX_RELEASE >= 12000)
		pControl->GetClassName(tstrTgtCtrlClassName);
#else
		pControl->GetClassNameA(tstrTgtCtrlClassName);
#endif

		sprintf(msgbuffer, "Can't create a driver from a non-float type controller, Type = %s, Control Class ID = %x, %x\r\n", tstrTgtCtrlClassName.data(), pControl->ClassID().PartA(), pControl->ClassID().PartB());
		Warn(msgbuffer);
		//Errorf("ERROR - Can't create a driver from a non-float type controller, Control Class ID = %x, %x", pControl->ClassID().PartA(), pControl->ClassID().PartB());
		return FALSE;
	}

	string driverCompId;
	
	//Is this controller a sub-controller of a position or rotation controller
	ReferenceTarget* pRefTarget = NULL;

	pRefTarget = FindReferenceOfClassID(pControl,IPOS_CONTROL_CLASS_ID);
	if(pRefTarget) 
	{
		driverCompId = "translate";
	}
	else 
	{
		pRefTarget = FindReferenceOfClassID(pControl,Class_ID(EULER_CONTROL_CLASS_ID,0));
		if(pRefTarget)
		{
			driverCompId = "rotate";
		}
		else
		{
			pRefTarget = FindReferenceOfClassID(pControl,ISCALE_CONTROL_CLASS_ID);
			if(pRefTarget)
			{
				driverCompId = "scale";
			}
			else
			{
				pRefTarget = FindReferenceOfClassID(pControl, Class_ID(PARAMETER_BLOCK2_CLASS_ID, 0));
				if(pRefTarget)
				{
					//Assume here that this particular driver is being created for a scalar which is a custom attribute, not a controller.
					//In this case, a driver is not necessary.  Skip it, don't show an error.
					return FALSE;
				}
				else
				{
					if(pControl->ClassID().PartA() == HYBRIDINTERP_FLOAT_CLASS_ID)
					{
						sprintf(msgbuffer, "Error: Cannot create driver for a controller which is not a sub-controller of a PosXYZ controller. Pointing to a Limited Float Controller, this is not valid. Control Class ID = %x, %x\r\n", 
							pControl->ClassID().PartA(), pControl->ClassID().PartB());
						Error(msgbuffer);
					}
					else
					{
						sprintf(msgbuffer, "Error: Cannot create driver for a controller which is not a sub-controller of a PosXYZ controller. Control Class ID = %x, %x\r\n", 
							pControl->ClassID().PartA(), pControl->ClassID().PartB());
						Error(msgbuffer);
					}
					
					return FALSE;
				}
			}
		}
	}

	//Which component of the position/rotation controller is this controller under
	::Control* pRefControl = static_cast<::Control*>(pRefTarget);
	int componentIdx = GetControllerComponent(pControl, pRefControl);
	Assertf(componentIdx != -1, "Couldn't determine which component of a controller to use.");
	if(componentIdx == 0)
	{
		driverCompId += "X";
	}
	else if(componentIdx == 1)
	{
		driverCompId += "Y";
	}
	else if(componentIdx == 2)
	{
		driverCompId += "Z";
	}

#if 0
	//Trace back the references to get the node that is controlled by this controller
	ReferenceTarget* pPrsControlRefTarget = FindReferenceOfClassID(pRefControl, Class_ID(PRS_CONTROL_CLASS_ID,0));
	Assertf(pPrsControlRefTarget, "Found a controller that isn't a sub controller of a PRS controller, don't know how to deal with this.");
	ReferenceTarget* pINodeRefTarget = FindReferenceOfClassID(pPrsControlRefTarget, Class_ID(BASENODE_CLASS_ID,0));
	Assertf(pINodeRefTarget, "Found a PRS controller that isn't being referenced by an INode, don't know how to deal with this.");
	INode* pDriverNode = static_cast<INode*>(pINodeRefTarget);
#else
//	ReferenceTarget* pINodeRefTarget = FindReferenceOfClassID2(pRefControl, Class_ID(BASENODE_CLASS_ID,0));
//	Assertf(pINodeRefTarget, "Failed to find parent INode for controller");
//	INode* pDriverNode = static_cast<INode*>(pINodeRefTarget);

	INode* pDriverNode = FindNodeFromDirectController::Find(pControl);
	Assertf(pDriverNode, "Failed to find parent INode for controller");

#endif

	return CreateDriver(theFile, exprBoneFile, pDriverNode, driverCompId.c_str(), pOutDriver, pOutDriverComponent);
}

//-----------------------------------------------------------------------------
TSTR RageMaxAnimExprExportGUP::GetExpression(IExprCtrl* pExprCtrl)
{
	TSTR originalExpr = pExprCtrl->GetExpression();

	//For every custom attribute that has been found in the scalar list, search the expression to 
	//replace it's current constant with that type.
	//
	std::map< string, float >	exprCustomVariableFixupTable;

	int numScalars = pExprCtrl->NumScalars();
	
	//The interface for the expression controller uses 1-based indicies, and indices (1,4) are used
	//internally by the controller for referencing Ticks, Seconds, Frames, and Normalized Time
	for(int i = 5; i <= numScalars; i++)
	{
		Value* pIndexVal = Integer::heap_intern(i);

		TSTR tstrScalarName = pExprCtrl->GetScalarName(i-1);
		int type = pExprCtrl->GetScalarType(pIndexVal);

		if(type == kController)
		{
			Value* pValTarget = pExprCtrl->GetScalarTarget( pIndexVal, true );
			::Control* pTargetController = pValTarget->to_controller();
			Class_ID targetCid = pTargetController->ClassID();

			//A driver can't be created from this scalar, thus we can assume that it's a parameter block.
			//
			ReferenceTarget* pRefTarget = FindReferenceOfClassID(pTargetController, Class_ID(PARAMETER_BLOCK2_CLASS_ID, 0));
			if(pRefTarget)
			{
				//Deal with this scalar as a custom attribute.
				//Acquire this scalar's value as it is in Max.  

				ExprControl* l_pExprControl = static_cast<ExprControl*>(pExprCtrl);
				string targetString = l_pExprControl->getTargetAsString(SCALAR_VAR, i-5);

				// We only want to replace the value on non-animated custom attributes - we leave the animated scalars
				// in place so we can create drivers to them later.
				// The _a needs to be on the rollout not the attribute. This is how we define animated attributes.
				if(targetString.find("_a.") == string::npos && targetString.find("_A.") == string::npos)
				{
					float attributeValue = pExprCtrl->GetScalarValue(pIndexVal, 0.0f);
					exprCustomVariableFixupTable[tstrScalarName.data()] = attributeValue;
				}
			}
		}
		else if(type == kConstant)
		{
			float attributeValue = pExprCtrl->GetScalarValue(pIndexVal, 0.0f);
			exprCustomVariableFixupTable[tstrScalarName.data()] = attributeValue;
		}
	}

	atString expressionString(originalExpr);
	for( std::map< string, float >::iterator it = exprCustomVariableFixupTable.begin(); it != exprCustomVariableFixupTable.end(); ++it)
	{
		const atString searchString(it->first.c_str());
		char floatStr[128];
		float value = it->second;
		sprintf(floatStr, "%f", value);
		const atString replaceString(&floatStr[0]);
		expressionString.Replace(searchString, replaceString);
	}

	return TSTR(expressionString.c_str());
}

TSTR RageMaxAnimExprExportGUP::FixExpressionParentheses(const char* pExpression)
{
	const int MAX_LEN = 4096;
	if(strlen(pExpression)>MAX_LEN)
	{
		return "";
	}
	char fixedExpression[MAX_LEN] = {0};
	for(int index=0; index<strlen(pExpression) && index<MAX_LEN; index++)
	{
		if(	pExpression[index] != ' '  &&
			pExpression[index] != 0x0d && // new line characters \r\n
			pExpression[index] != 0x0a)
		{
			fixedExpression[strlen(fixedExpression)] = pExpression[index];
		}
	}

	std::string strExpression(fixedExpression);
	std::string strOriginalExpression(strExpression);

	bool bRun=false;
	while(bRun == false)
	{
		int startIndex = strExpression.find("if((");
		if(startIndex != -1)
		{
			int endIndex = strExpression.find("),");
			if(endIndex != -1)
			{
				strExpression.replace(endIndex, 2, ",");
				strExpression.replace(startIndex, 4, "if(");
				continue;
			}
		}

		bRun=true;
	}

	char msgbuffer[4096];
	if(stricmp(strOriginalExpression.c_str(), strExpression.c_str()) != 0)
	{
		sprintf(msgbuffer, "Info: EXPRESSION UPDATED:\n%s\nto\n%s\n", strOriginalExpression.c_str(), strExpression.c_str());
		Msg(msgbuffer);
	}

	return TSTR(strExpression.c_str());
}

//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreateExpressionController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, BOOL& bZerodControl, BOOL bBlendShape, ::Control* /*pContainerControl*/)
{
	char msgbuffer[4096];

	IExprCtrl* pExprCtrl = static_cast<IExprCtrl*>(pControl->GetInterface(IID_EXPR_CONTROL));
	Assertf(pExprCtrl, "CreateExpressionController was called with a controller that doesn't implement the IExprCtrl interface.");

	int numScalars = pExprCtrl->NumScalars();
	int numVectors = pExprCtrl->NumVectors();
	Assertf(numVectors == 0, "Can't export an expression controller that utilizes vector inputs");

	AeExpressionController* pExpressionController = new AeExpressionController();

	atString nrmNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, nrmNodeName);
	pExpressionController->m_id = GenerateUniqueExprControllerName(nrmNodeName.c_str()).c_str();

	sprintf(msgbuffer, "Info: Creating Expression Controller '%s' for '%s'.\r\n", pExpressionController->m_id.c_str(), nrmNodeName.c_str());
	Msg(msgbuffer, nrmNodeName.c_str());
	//Displayf("Info: Creating Expression Controller '%s' for '%s'.\n", pExpressionController->m_id.c_str(), nrmNodeName.c_str());

	// Build the expression, replacing scalars with values
	TSTR tstrExpression = GetExpression(pExprCtrl);
	tstrExpression = FixExpressionParentheses(tstrExpression);
	if(tstrExpression.length()<=0)
	{
		sprintf(msgbuffer, "Error: Expression fixup failed. Likely longer than 4096 characters: %s.\r\n", pExpressionController->m_id.c_str());
		Error(msgbuffer, nrmNodeName.c_str());
	}

	// Sliders in Max are percentage and we want the value within the range of 0-1. To do this, I just multiply by 0.01 in the expression.
	// Only on blendshapes though, so only on morphers
	if(bBlendShape == TRUE)
	{
		tstrExpression.Append(" * 0.01");
	}

	ExprParser theParser;
	ExprTree* pExpTree = theParser.ParseMaxScriptExprCtrlExpression(tstrExpression.data());
 	if(!pExpTree)
	{
		sprintf(msgbuffer, "Error: Unable to parse expression on %s.\r\n", pExpressionController->m_id.c_str());
		Error(msgbuffer, nrmNodeName.c_str());
		
		if(theParser.GetNodeStack().size() > 0)
		{
			sprintf(msgbuffer, "Error: Corrupt node stack. Stack has '%d' nodes remaining > 0.", theParser.GetNodeStack().size());
			Error(msgbuffer, nrmNodeName.c_str());
		}
		
		sprintf(msgbuffer, "%s\r\n", tstrExpression.data());
		Error(msgbuffer, nrmNodeName.c_str());
		sprintf(msgbuffer, "Error: Failed to create expression controller %s.\r\n", pExpressionController->m_id.c_str());
		Error(msgbuffer, nrmNodeName.c_str());

		//Errorf("Error: Unable to parse expression: %s.\n", tstrExpression.data());
		delete pExpressionController;
		return NULL;
	}

	pExpressionController->m_pExprTree = pExpTree;

	map< string, pair<string,string> >	exprCtrlVariableFixupTable;

	//The interface for the expression controller uses 1-based indicies, and indices (1,4) are used
	//internally by the controller for referencing Ticks, Seconds, Frames, and Normalized Time
	for(int i = 5; i <= numScalars; i++)
	{
		Value* pIndexVal = Integer::heap_intern(i);
		
		TSTR tstrScalarName = pExprCtrl->GetScalarName(i-1);
		int type = pExprCtrl->GetScalarType(pIndexVal);
		if (type == kConstant)
		{
			//Const scalars are dealt with earlier (in GetExpression)
			continue;
		}

		Value* pValTarget = pExprCtrl->GetScalarTarget( pIndexVal, true );
		::Control* pTargetController = pValTarget->to_controller();
		Class_ID targetCid = pTargetController->ClassID();

		//Check the scalars to see if they point to a position controller which has being zero'd out
		ExprControl* pExprControl = static_cast<ExprControl*>(pExprCtrl);
		string targetString = pExprControl->getTargetAsString(SCALAR_VAR, i-5);

		if(targetString.find("Zero_Pos_XYZ") != string::npos){ bZerodControl=TRUE; }
		if(targetString.find("Zero_Euler_XYZ") != string::npos){ bZerodControl=TRUE; }
		
		AeDriver* pDriver = NULL;
		AeDriverComponent* pDriverComponent = NULL;
		if(targetCid == FloatExposeController_CLASS_ID)
		{
			//The scalar target is a float expose controller, this controller is user by an expose transform node
			//so we need to locate the expose transform node, retrieve the node whose transform is being exposed
			//and find the correct transform controller to create a driver from.
			ReferenceMaker* pExposeTransformRef = FindRefererOfClassID( pTargetController, ExposeTransform_CLASS_ID);
			Assertf(pExposeTransformRef, "Failed to resolve the expose transform node being used as an input to the expression controller.");
			
			//The expose node parameter is part of the 1st param block in the expose transform node
			IParamBlock2* pETmInfoParamBlock = pExposeTransformRef->GetParamBlock(EXPOSETMNODE_PBLOCK_INFO);
			INode* pExposeNode = NULL;
			pETmInfoParamBlock->GetValue(pb_expose_node, 0, pExposeNode, FOREVER);
			Assertf(pExposeNode, "Parameter found which is wired to an expose transform node without a node assigned as the exposed node.");

			//Determine which transform component is being exposed...
			IParamBlock2* pEtmExposeParamBlock = pExposeTransformRef->GetParamBlock(EXPOSETMNODE_PBLOCK_EXPOSE);

			::Control* pExposeLocalEulerXController = NULL;
			::Control* pExposeLocalEulerYController = NULL;
			::Control* pExposeLocalEulerZController = NULL;
			::Control* pExposeLocalPosXController = NULL;
			::Control* pExposeLocalPosYController = NULL;
			::Control* pExposeLocalPosZController  = NULL;

#if (MAX_RELEASE >= 14000)
			pExposeLocalEulerXController = pEtmExposeParamBlock->GetControllerByIndex(pb_localeulerx, 0);
			pExposeLocalEulerYController = pEtmExposeParamBlock->GetControllerByIndex(pb_localeulery, 0);
			pExposeLocalEulerZController = pEtmExposeParamBlock->GetControllerByIndex(pb_localeulerz, 0);

			pExposeLocalPosXController = pEtmExposeParamBlock->GetControllerByIndex(pb_localpositionx, 0);
			pExposeLocalPosYController = pEtmExposeParamBlock->GetControllerByIndex(pb_localpositiony, 0);
			pExposeLocalPosZController = pEtmExposeParamBlock->GetControllerByIndex(pb_localpositionz, 0);
#else
			pExposeLocalEulerXController = pEtmExposeParamBlock->GetController(pb_localeulerx, 0);
			pExposeLocalEulerYController = pEtmExposeParamBlock->GetController(pb_localeulery, 0);
			pExposeLocalEulerZController = pEtmExposeParamBlock->GetController(pb_localeulerz, 0);

			pExposeLocalPosXController = pEtmExposeParamBlock->GetController(pb_localpositionx, 0);
			pExposeLocalPosYController = pEtmExposeParamBlock->GetController(pb_localpositiony, 0);
			pExposeLocalPosZController = pEtmExposeParamBlock->GetController(pb_localpositionz, 0);
#endif

			string exprDriverComponentName;

			if( pTargetController == pExposeLocalEulerXController )
			{
				exprDriverComponentName = "rotateX";
			}
			else if( pTargetController == pExposeLocalEulerYController )
			{
				exprDriverComponentName = "rotateY";
			}
			else if( pTargetController == pExposeLocalEulerZController )
			{
				exprDriverComponentName = "rotateZ";
			}
			else if( pTargetController == pExposeLocalPosXController )
			{
				exprDriverComponentName = "translateX";
			}
			else if( pTargetController == pExposeLocalPosYController )
			{
				exprDriverComponentName = "translateY";
			}
			else if( pTargetController == pExposeLocalPosZController )
			{
				exprDriverComponentName = "translateZ";
			}
			else
			{
				sprintf(msgbuffer, "Error: Failed to create expression controller %s, it is used on node '%s' is using an exposed transform that isn't in the local coordinate space\r\n", pExpressionController->m_id.c_str(), pNode->GetName());
				Error(msgbuffer, nrmNodeName.c_str());
				delete pExpressionController;
				
				//Errorf("Float Expression Controller used on node '%s' is using an exposed transform that isn't in the local coordinate space", pNode->GetName());
				return NULL;
			}

			CreateDriver(theFile, exprBoneFile, pExposeNode, exprDriverComponentName.c_str(), &pDriver, &pDriverComponent);
		}
		else
		{
			MSTR tstrTgtCtrlClassName;
#if (MAX_RELEASE >= 12000)
			pTargetController->GetClassName(tstrTgtCtrlClassName);
#else
			pTargetController->GetClassNameA(tstrTgtCtrlClassName);
#endif
			const char* szTgtCtrlClassName = tstrTgtCtrlClassName.data();

			if( CreateDriver(theFile, exprBoneFile, pTargetController, &pDriver, &pDriverComponent) == FALSE)
			{
				ReferenceTarget* pRefTarget = FindReferenceOfClassID(pTargetController, Class_ID(PARAMETER_BLOCK2_CLASS_ID, 0));
				if(pRefTarget)
				{
					//Assume here that this particular scalar is a custom attribute, not a controller.
					
					// we now need to get the target string and parse from it the controller node and if its animated or not with the rollout being _A
					// we however need to setup these so they have Ctrl_Attribute naming
					ExprControl* pExprControl = static_cast<ExprControl*>(pExprCtrl);
					string targetString = pExprControl->getTargetAsString(SCALAR_VAR, i-5);

					// We only want to create drivers for non-animated custom attributes
					// The _a needs to be on the rollout not the attribute. This is how we define animated attributes.
					if(targetString.find("_a.") != string::npos || targetString.find("_A.") != string::npos)
					{	
						size_t pos = targetString.find(".");
						std::string strControl = targetString.substr(1, pos-1);

						// some custom attributes are named in single quotes i.e 'Head_Blah' so we need to remove these to query the control itself
						atString strAmmendedControl(strControl.c_str());
						strAmmendedControl.Replace("'", "");

						INode* pNodeControl = GetCOREInterface()->GetINodeByName(strAmmendedControl.c_str());
						if(!pNodeControl)
						{
							sprintf(msgbuffer, "Error: Referenced scalar control node is NULL: %s\r\n", strAmmendedControl.c_str());
							Error(msgbuffer, nrmNodeName.c_str());
							//Errorf("Error: Referenced scalar control node is NULL: %s", strControl.c_str());
							continue;
						}	

						CreateDriverForAttribute(theFile, exprBoneFile, pNode, pNodeControl, targetString.c_str(), tstrScalarName, &pDriver, &pDriverComponent);
					}
					else
					{
						// if its non animated, skip it
						continue;
					}
				}
				else
				{
					sprintf(msgbuffer, "Error: Unable to create driver for node %s.\r\n", pNode->GetName());
					Error(msgbuffer, nrmNodeName.c_str());
					sprintf(msgbuffer, "Error: Failed to create expression controller %s.\r\n", pExpressionController->m_id.c_str());
					Error(msgbuffer, nrmNodeName.c_str());
					//Errorf("Unable to create driver for node %s.\n", pNode->GetName());
					delete pExpressionController;
					return NULL;
				}
			}
		}

		if(pDriver && pDriverComponent)
		{
			pExpressionController->AddDriver(pDriver, pDriverComponent);

			exprCtrlVariableFixupTable[tstrScalarName.data()] = pair<string,string>(pDriver->m_id.c_str(), pDriverComponent->m_id.c_str());
		}
	}

	//Walk through the expression tree updating the nameAttr pair types to properly reference
	//the actual driver node inputs instead of the expression controller variable names
	queue< ExprNodeBase* > exprNodeQueue;

	exprNodeQueue.push( pExpTree->m_pRoot );
	while(!exprNodeQueue.empty())
	{
		ExprNodeBase* pNode = exprNodeQueue.front();
		exprNodeQueue.pop();

		if(pNode->GetSuperType() == ExprNodeBase::kOperand &&
			((ExprNodeOperand*)pNode)->GetOperandType() == ExprNodeOperand::kNameAttrPair)
		{
			ExprNodeNameAttrPair* pNameAttrPair = static_cast<ExprNodeNameAttrPair*>(pNode);

			//Lookup the name of the variable in the fixup table
			map< string, pair<string,string> >::iterator it = exprCtrlVariableFixupTable.find(pNameAttrPair->m_name.c_str());
			if(it != exprCtrlVariableFixupTable.end())
			{
				pair<string, string>& tableNameAttrPair = it->second;

				pNameAttrPair->m_name = tableNameAttrPair.first.c_str();
				pNameAttrPair->m_attr = tableNameAttrPair.second.c_str();
			}
			else
			{
				sprintf(msgbuffer,  "Failed to locate expression controller variable '%s' in fixup tables.\r\n", pNameAttrPair->m_name.c_str());
				Error(msgbuffer, nrmNodeName.c_str());
				//Errorf( "Failed to locate expression controller variable '%s' in fixup tables.", pNameAttrPair->m_name.c_str());
			}
		}

		//Add the children to the traversal queue
		for(unsigned int i = 0; i < pNode->GetChildCount(); i++)
		{
			exprNodeQueue.push(pNode->GetChild(i));
		}
	}

	return pExpressionController;
}

int RageMaxAnimExprExportGUP::TrackIDLookup(const char* trackid)
{
	//Cleaning up the property string, as people sometimes leave trailing white space
	atString atTrackId(trackid);
	atTrackId.Trim();
	trackid = atTrackId.c_str();

	if(strcmpi(trackid, "TRACK_BONE_TRANSLATION") == 0)
	{
		return kTrackBoneTranslation;
	}
	else if(strcmpi(trackid, "TRACK_BONE_ROTATION") == 0)
	{
		return kTrackBoneRotation;
	}
	else if(strcmpi(trackid, "TRACK_FACIAL_CONTROL") == 0)
	{
		return kTrackFacialControl;
	}
	else if(strcmpi(trackid, "TRACK_FACIAL_TRANSLATION") == 0)
	{
		return kTrackFacialTranslation;
	}
	else if(strcmpi(trackid, "TRACK_FACIAL_ROTATION") == 0)
	{
		return kTrackFacialRotation;
	}
	else if(strcmpi(trackid, "TRACK_FACIAL_SCALE") == 0)
	{
		return kTrackFacialScale;
	}
	else if(strcmpi(trackid, "TRACK_GENERIC_CONTROL") == 0)
	{
		return kTrackGenericControl;
	}
	else if(strcmpi(trackid, "TRACK_GENERIC_TRANSLATION") == 0)
	{
		return kTrackGenericTranslation;
	}
	else if(strcmpi(trackid, "TRACK_GENERIC_ROTATION") == 0)
	{
		return kTrackGenericRotation;
	}
	else if(strcmpi(trackid, "TRACK_ANIMATED_NORMAL_MAPS") == 0)
	{
		return kTrackAnimatedNormalMaps;
	}
	else if(strcmpi(trackid, "TRACK_VISEMES") == 0)
	{
		return kTrackVisemes;
	}
	else if(strcmpi(trackid, "TRACK_FACIAL_TINTING") == 0)
	{
		return kTrackFacialTinting;
	}

	return -1;
}

void RageMaxAnimExprExportGUP::PrintToLog(const char* format, ...)
{
	char buffer[RAGE_MAX_PATH*2];
	ZeroMemory(buffer, RAGE_MAX_PATH*2);
	va_list args;
	va_start (args, format);
	vsprintf (buffer,format, args);
	va_end (args);

	string str(buffer);
	if(string::npos != str.find("Error:"))
	{
		m_msgBoxErrorStream << buffer;
	}

	mprintf(buffer); // print to listener
	EXPORTLOG.AppendEntry(buffer);
}

void RageMaxAnimExprExportGUP::AddToHashMap(const char* str)
{
	char msgbuffer[4096];
	//Check for any hash conflicts, these need to be caught on export rather than later

	u16 h = atHash16U(str);

	std::map<u16,atString>::iterator itr = m_hashMap.find(h);

	if(itr != m_hashMap.end())
	{
		if(strcmpi(itr->second.c_str(), str) != 0)
		{
			//Check if we have already reported this conflict, we don't want to report dupes
			for(int i=0; i < m_hashMapConflicts.GetCount(); ++i)
			{
				if(strcmpi(m_hashMapConflicts[i].c_str(), str) == 0 ||
					strcmpi(m_hashMapConflicts[i].c_str(), itr->second.c_str()) == 0)
				{
					return;
				}
			}

			m_hashMapConflicts.Grow() = str;
			m_hashMapConflicts.Grow() = itr->second.c_str();

			sprintf(msgbuffer, "Error: Hash clash between: %s and %s\n", str, itr->second.c_str());
			Error(msgbuffer);
			return;
		}
	}

	m_hashMap[h] = atString(str);
}

TSTR RageMaxAnimExprExportGUP::GetExpression(IBaseWireControl* pExprCtrl, std::map<std::string, std::string>& mapScalars)
{
	// create a map of animated custom attributes or replace the custom attributes with constants. an animated custom
	// attribute is defined by _a in the rollout name.

	// THIS FUNCTION NEEDS REFACTORING - WILL DO WHEN HAVE TIME

	std::string strExpression = pExprCtrl->get_expr_text(0);
	bool bDone=false;

	while(!bDone)
	{
		size_t pos = strExpression.find("$");
		if(pos != std::string::npos)
		{
			bool bFound=false;
			for(size_t i=pos; i < (int)strExpression.size(); ++i)
			{
				// 41 == Close parenthesis (or close bracket)
				// 32 == Space
				if(((int)strExpression[i]) == 41 || ((int)strExpression[i]) == 32)
				{
					std::string strScalar = strExpression.substr(pos, i-pos);

					// If a control is referenced then we cant tell the difference between it and an attribute so
					// we check if the following exist in the string, if they do then its a control and we check 
					// its parent type and replace it with the correct syntax. This syntax will enable a correct 
					// expression tree and also allow us to replace the YPosition with translateY so our drivers
					// work.

					size_t pos_x = strScalar.find(".X_Position");
					size_t pos_y = strScalar.find(".Y_Position");
					size_t pos_z = strScalar.find(".Z_Position");

					size_t rot_x = strScalar.find(".X_Rotation");
					size_t rot_y = strScalar.find(".Y_Rotation");
					size_t rot_z = strScalar.find(".Z_Rotation");

					if(pos_x != string::npos || pos_y != string::npos || pos_z != string::npos ||
						rot_x != string::npos || rot_y != string::npos || rot_z != string::npos)
					{
						Animatable* pAnimWireParent = pExprCtrl->get_wire_parent(0);
						Class_ID wireParentCid = pAnimWireParent->ClassID();

						if(wireParentCid.PartA() == FLOATLIMITCTRL_CLASS_ID)
						{		
							string strFloatLimit = "Limited_Controller__Bezier_Float";
							strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
							bFound=true;
							break;	
						}
						else if(wireParentCid.PartA() == EULER_CONTROL_CLASS_ID)
						{
							if(rot_x != string::npos)
							{
								string strFloatLimit = "X_Rotation";
								strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
							}
							else if(rot_y != string::npos)
							{
								string strFloatLimit = "Y_Rotation";
								strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
							}
							else if(rot_z != string::npos)
							{
								string strFloatLimit = "Z_Rotation";
								strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
							}
							bFound=true;
							break;
						}
						else if(wireParentCid == IPOS_CONTROL_CLASS_ID)
						{
							if(pos_x != string::npos)
							{
								string strFloatLimit = "X_Position";
								strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
							}
							else if(pos_y != string::npos)
							{
								string strFloatLimit = "Y_Position";
								strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
							}
							else if(pos_z != string::npos)
							{
								string strFloatLimit = "Z_Position";
								strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
							}
							bFound=true;
							break;
						}
					}
					else
					{
						size_t posName = strScalar.find("_A.");
						size_t posName1 = strScalar.find("_a.");
						if(posName != std::string::npos || posName1 != std::string::npos)
						{
							size_t p1 = strScalar.find(".");
							std::string strNode = strScalar.substr(1, p1-1);

							p1 = strScalar.find_last_of(".");
							std::string strVariable = strScalar.substr(p1+1,strScalar.size());

							mapScalars[strVariable] = strNode;

							strExpression = strExpression.replace(pos, strScalar.size(), strVariable.c_str(), strVariable.size());
							bFound=true;
							break;						
						}
						else
						{
							FPValue fpv;
							if(ExecuteMAXScriptScript((char*)strScalar.c_str(), FALSE, &fpv) == TRUE)
							{
								std::stringstream stream;
								stream << fpv.f;
								strExpression = strExpression.replace(pos, strScalar.size(), stream.str().c_str(), stream.str().size());
								bFound=true;
								break;
							}
						}
					}	
				}
			}

			if(bFound) continue;

			//did not find ) or space so must be the end
			std::string strScalar = strExpression.substr(pos, strExpression.size()-pos);

			size_t pos_x = strScalar.find(".X_Position");
			size_t pos_y = strScalar.find(".Y_Position");
			size_t pos_z = strScalar.find(".Z_Position");

			size_t rot_x = strScalar.find(".X_Rotation");
			size_t rot_y = strScalar.find(".Y_Rotation");
			size_t rot_z = strScalar.find(".Z_Rotation");

			if(pos_x != string::npos || pos_y != string::npos || pos_z != string::npos ||
				rot_x != string::npos || rot_y != string::npos || rot_z != string::npos)
			{
				Animatable* pAnimWireParent = pExprCtrl->get_wire_parent(0);
				Class_ID wireParentCid = pAnimWireParent->ClassID();

				if(wireParentCid.PartA() == FLOATLIMITCTRL_CLASS_ID)
				{		
					string strFloatLimit = "Limited_Controller__Bezier_Float";
					strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
					bDone=true;	
				}
				else if(wireParentCid.PartA() == EULER_CONTROL_CLASS_ID)
				{
					if(rot_x != string::npos)
					{
						string strFloatLimit = "X_Rotation";
						strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
					}
					else if(rot_y != string::npos)
					{
						string strFloatLimit = "Y_Rotation";
						strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
					}
					else if(rot_z != string::npos)
					{
						string strFloatLimit = "Z_Rotation";
						strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
					}
					bDone=true;	
				}
				else if(wireParentCid == IPOS_CONTROL_CLASS_ID)
				{
					if(pos_x != string::npos)
					{
						string strFloatLimit = "X_Position";
						strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
					}
					else if(pos_y != string::npos)
					{
						string strFloatLimit = "Y_Position";
						strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
					}
					else if(pos_z != string::npos)
					{
						string strFloatLimit = "Z_Position";
						strExpression = strExpression.replace(pos, strScalar.size(), strFloatLimit.c_str(), strFloatLimit.size());
					}
					bDone=true;	
				}
			}
			else
			{
				size_t posName = strScalar.find("_A.");
				size_t posName1 = strScalar.find("_a.");
				if(posName != std::string::npos || posName1 != std::string::npos)
				{
					size_t p1 = strScalar.find(".");
					std::string strNode = strScalar.substr(1, p1-1);

					p1 = strScalar.find_last_of(".");
					std::string strVariable = strScalar.substr(p1+1,strScalar.size());

					mapScalars[strVariable] = strNode;

					strExpression = strExpression.replace(pos, strScalar.size(), strVariable.c_str(), strVariable.size());
					bDone=true;			
				}
				else
				{
					// non animated (constant)
					FPValue fpv;
					if(ExecuteMAXScriptScript((char*)strScalar.c_str(), FALSE, &fpv) == TRUE)
					{
						std::stringstream stream;
						stream << fpv.f;
						strExpression = strExpression.replace(pos, strScalar.size(), stream.str().c_str(), stream.str().size());
						bFound=true;
						break;
					}
				}
			}
		}
		else
		{
			bDone=true;
		}
	}

	return strExpression.c_str();
}

//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreateFloatWireController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* pContainerControl)
{
	char msgbuffer[4096];
	IBaseWireControl* pWireCtrl = static_cast<IBaseWireControl*>(pControl->GetInterface(WIRE_CONTROLLER_INTERFACE));
	Assertf(pWireCtrl, "Failed to retrieve IBaseWireControl interface from controller");

	if( pWireCtrl->is_master() ||
		pWireCtrl->is_two_way() ||
		pWireCtrl->get_num_wires() <= 0 )
	{
		//Warningf("Found wire controller that is not a slave controller, exporting a constant float controller.");
		sprintf(msgbuffer, "Warning: Found wire controller that is not a slave controller, exporting a constant float controller.");
		Warn(msgbuffer, pNode->GetName());
		return CreateFloatConstantController(theFile, exprBoneFile, pNode, pControl);
	}

	AeExpressionController* pExpressionController = new AeExpressionController();

	atString nrmNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, nrmNodeName);
	pExpressionController->m_id = GenerateUniqueExprControllerName(nrmNodeName.c_str()).c_str();

// 	sprintf(msgbuffer, "Info: Creating Float Wire Controller '%s' for '%s'\r\n", pExpressionController->m_id.c_str(), nrmNodeName.c_str());
// 	Msg(msgbuffer, nrmNodeName);

	//Get the expression associated with the parameter wiring
	std::map< string, string > mapScalars;
	TSTR tstrExpression = GetExpression(pWireCtrl, mapScalars);

	//Create a generic expression tree to represent the param wiring expression
	ExprParser theParser;
	ExprTree* pExpTree = theParser.ParseMaxScriptExpression(tstrExpression.data());
	if(!pExpTree)
	{
		sprintf(msgbuffer, "Error: Unable to parse expression on %s.\r\n", pExpressionController->m_id.c_str());
		Error(msgbuffer, nrmNodeName.c_str());
		sprintf(msgbuffer, "%s\r\n", tstrExpression.data());
		Error(msgbuffer, nrmNodeName.c_str());
		sprintf(msgbuffer, "Error: Failed to create expression controller %s.\r\n", pExpressionController->m_id.c_str());
		Error(msgbuffer, nrmNodeName.c_str());

		//Errorf("Error: Unable to parse expression: %s.\n", tstrExpression.data());
		delete pExpressionController;
		return NULL;
	}

	pExpressionController->m_pExprTree = pExpTree;

	//Create an associated driver from the wiring data....
	AeDriver* pDriver = NULL;
	AeDriverComponent* pDriverComponent =  NULL;
	string exprDriverVarName;

	Animatable* pAnimWireParent = pWireCtrl->get_wire_parent(0);
	
#ifdef _DEBUG
	//Grab some useful debugging information
	MSTR mstrNodeName = pAnimWireParent->NodeName();
	const char* szNodeName = mstrNodeName.data();
	
	MSTR mstrClassName;
	pAnimWireParent->GetClassName( mstrClassName );
	const char* szClassName = mstrClassName.data();
#endif //_DEBUG

	SClass_ID wireParentSid = pAnimWireParent->SuperClassID();
	Class_ID wireParentCid = pAnimWireParent->ClassID();

	//Some error checking to handle cases that seem to pop up where a float wire control
	//is wired directly back to it's own containing control
	::Control* pCtrlWireParent = dynamic_cast<::Control*>(pAnimWireParent);
	if( pCtrlWireParent && (pCtrlWireParent == pContainerControl) )
	{
		sprintf(msgbuffer, "Error: Found cycle in parameter wiring, parameter is wired to it's own containing controller, exporting a constant float controller.  Node = '%s'\r\n", pNode->GetName());
		Error(msgbuffer, nrmNodeName.c_str());
		//Errorf("Error: Found cycle in parameter wiring, parameter is wired to it's own containing controller, exporting a constant float controller.  Node = '%s'", pNode->GetName());
		return CreateFloatConstantController(theFile, exprBoneFile, pNode, pControl);
	}

	if(wireParentCid.PartA() == PARAMETER_BLOCK2_CLASS_ID)
	{
		//If the parent controller is a parameter block its likely that this wire controller was connected to
		//a parameter of an expose transform helper object
		IParamBlock2* pBlock = static_cast<IParamBlock2*>(pAnimWireParent);

		ReferenceMaker* pOwner = pBlock->GetOwner();
		Class_ID ownerCid = pOwner->ClassID();
		if(ownerCid != ExposeTransform_CLASS_ID)
		{
			// we now assume this is a custom attribute

			if(mapScalars.size() == 0)
			{
				sprintf(msgbuffer, "Error: No scalars found, rig custom attributes possibly setup incorrectly.\r\n");
				Error(msgbuffer, nrmNodeName.c_str());
				//Errorf("Error: No scalars found, rig custom attributes possibly setup incorrectly.");
				return NULL;
			}

			for(map<string, string>::iterator itr = mapScalars.begin(); itr != mapScalars.end(); ++itr) 
			{
				INode* pNodeControl = GetCOREInterface()->GetINodeByName(itr->second.c_str());
				if(!pNodeControl)
				{
					sprintf(msgbuffer, "Error: Referenced scalar control node is NULL: %s\r\n", itr->second.c_str());
					Error(msgbuffer, nrmNodeName.c_str());
					//Errorf("Error: Referenced scalar control node is NULL: %s", itr->second.c_str());
					return NULL;
				}

				CreateDriverForAttribute(theFile, exprBoneFile, pNode, pNodeControl, "", itr->first.c_str(), &pDriver, &pDriverComponent);
			}
		}
		else
		{
			//The expose transform node utilizes four param blocks, param block
			//index 1 is the info block that contains the references to the node
			//that is being exposed.
			IParamBlock2* pETmInfoParamBlock = pOwner->GetParamBlock(EXPOSETMNODE_PBLOCK_INFO);

			//Get the node whose transform is being exposed
			INode* pExposeNode = NULL;
			pETmInfoParamBlock->GetValue(pb_expose_node, 0, pExposeNode, FOREVER);
			Assertf(pExposeNode, "Parameter found which is wired to an expose transform node without a node assigned as the exposed node.");

			//Since an expose transform node is connected as the driver then we know the following sub-anim indices relate as follows
			// 0 = Local_Euler_Rotation (vector)
			// 1 = Local_Euler_X
			// 2 = Local_Euler_Y
			// 3 = Local_Euler_Z
			// 4 = World_Euler_Rotation (vector)
			// 5 = World_Euler_X
			// 6 = World_Euler_Y
			// 7 = World_Euler_Z
			// 8 = Local_Position (vector)
			// 9 = Local_Position_X
			//10 = Local_Position_Y
			//11 = Local_Position_Z
			//12 = World_Position
			//13 = World_Position_X
			//14 = World_Position_Y
			//15 = World_Position_Z

			string exprDriverComponentName;

			int wireSubNum = pWireCtrl->get_wire_subnum(0);
			switch(wireSubNum)
			{
			case 0: //Local_Euler_Rotation (vector)
				Assertf(0,"Can't deal with paramaters wired to the Local_Euler_Rotation of an ExposeTransform Helper node.");
				return NULL;
				break;
			case 1: // Local_Euler_X
				{	
					exprDriverVarName = "Local_Euler_X";
					exprDriverComponentName = "rotateX";
				}//End case 2
				break;
			case 2: // Local_Euler_Y
				{
					exprDriverVarName = "Local_Euler_Y";
					exprDriverComponentName = "rotateY";
				}
				break;
			case 3: // Local_Euler_Z
				{
					exprDriverVarName = "Local_Euler_Z";
					exprDriverComponentName = "rotateZ";
				}
				break;
			case 9: // Local_Position_X
				{
					exprDriverVarName = "Local_Position_X";
					exprDriverComponentName = "translateX";
				}
				break;
			case 10: // Local_Position_Y
				{
					exprDriverVarName = "Local_Position_Y";
					exprDriverComponentName = "translateY";
				}
				break;
			case 11: // Local_Position_Z
				{
					exprDriverVarName = "Local_Position_Z";
					exprDriverComponentName = "translateZ";
				}
				break;

			default:
				Assertf(0,"Unhandled wire sub anim idex : %d", wireSubNum);
				return NULL;
			}//End switch(wireSubNum)

			CreateDriver(theFile, exprBoneFile, pExposeNode, exprDriverComponentName.c_str(), &pDriver, &pDriverComponent);
		}

	}//End if(wireParentCid.PartA() == PARAMETER_BLOCK2_CLASS_ID)
	else if(wireParentCid.PartA() == EULER_CONTROL_CLASS_ID)
	{
		int wireSubNum = pWireCtrl->get_wire_subnum(0);
		Assertf(wireSubNum < 3, "Parameter is wired to an EulerXYZ control, but the wire sub-anim index is %d", wireSubNum);
		::Control* pParentCtrl = static_cast<::Control*>(pAnimWireParent);

		//The controller is wired to an EulerXYZ controller
		::Control* pSrcDriverCtrl = NULL;

		if(wireSubNum == 0)
		{
			exprDriverVarName = "X_Rotation";
			pSrcDriverCtrl = pParentCtrl->GetXController();
		}
		else if(wireSubNum == 1)
		{
			exprDriverVarName = "Y_Rotation";
			pSrcDriverCtrl = pParentCtrl->GetYController();
		}
		else
		{
			exprDriverVarName = "Z_Rotation";
			pSrcDriverCtrl = pParentCtrl->GetZController();
		}

		CreateDriver(theFile, exprBoneFile, pSrcDriverCtrl, &pDriver, &pDriverComponent);

	}//End else if(wireParentCid.PartA() == EULER_CONTROL_CLASS_ID)
	else if(wireParentCid.PartA() == FLOATLIMITCTRL_CLASS_ID)
	{
		exprDriverVarName = "Limited_Controller__Bezier_Float";

		//The controller is wired to a LimitFloat controller
		::Control* pParentCtrl = static_cast<::Control*>(pAnimWireParent);

		CreateDriver(theFile, exprBoneFile, pParentCtrl, &pDriver, &pDriverComponent);

	}//End else if(wireParentCid.PartA() == FLOATLIMITCTRL_CLASS_ID)
	else if(wireParentCid == IPOS_CONTROL_CLASS_ID)
	{
		exprDriverVarName = "";

		int wireSubNum = pWireCtrl->get_wire_subnum(0);
		Assertf(wireSubNum < 3, "Parameter is wired to an Position XYZ control, but the wire sub-anim index is %d", wireSubNum);

		::Control* pParentCtrl = static_cast<::Control*>(pAnimWireParent);
	
		::Control* pSrcDriverCtrl = NULL;

		if(wireSubNum == 0)
		{
			exprDriverVarName = "X_Position";
			pSrcDriverCtrl = pParentCtrl->GetXController();
		}
		else if(wireSubNum == 1)
		{
			exprDriverVarName = "Y_Position";
			pSrcDriverCtrl = pParentCtrl->GetYController();
		}
		else
		{
			exprDriverVarName = "Z_Position";
			pSrcDriverCtrl = pParentCtrl->GetZController();
		}

		CreateDriver(theFile, exprBoneFile, pSrcDriverCtrl, &pDriver, &pDriverComponent);
	}
	else
	{
		//Unsupported type
		sprintf(msgbuffer, "Found a float wire controller that is wired to an unsupported type, exporting a constant float controller. Node='%s' Controller Class ID = %x, %x\r\n", pNode->GetName(), wireParentCid.PartA(), wireParentCid.PartB());
		Error(msgbuffer, nrmNodeName.c_str());
		//Errorf("Found a float wire controller that is wired to an unsupported type, exporting a constant float controller. Node='%s' Controller Class ID = %x, %x", pNode->GetName(), wireParentCid.PartA(), wireParentCid.PartB());
		return CreateFloatConstantController(theFile, exprBoneFile, pNode, pControl);
	}

	//Add the driver to the expression controller
	Assert(pDriver);
	Assert(pDriverComponent);

	if(pDriver && pDriverComponent)
	{
		pExpressionController->AddDriver(pDriver, pDriverComponent);
	}
	else
	{
		sprintf(msgbuffer, "Error: Driver creation failed, Node='%s'\r\n", pNode->GetName());
		Error(msgbuffer, nrmNodeName.c_str());
		sprintf(msgbuffer, "Error: Failed to create expression controller %s.\r\n", pExpressionController->m_id.c_str());
		Error(msgbuffer, nrmNodeName.c_str());
		//Errorf("Driver creation failed, Node='%s'", pNode->GetName());
		delete pExpressionController;
		return NULL;
	}

	//Update the expression tree to replace references to the expression driver variable with the actual
	//driver/driver component reference, as well as pick up any references to other drivers in the parameter
	//wiring expression.

	queue< ExprNodeBase* > exprNodeQueue;
	exprNodeQueue.push( pExpTree->m_pRoot );
	while(!exprNodeQueue.empty())
	{
		ExprNodeBase* pNode = exprNodeQueue.front();
		exprNodeQueue.pop();

		if(pNode->GetSuperType() == ExprNodeBase::kOperand &&
			((ExprNodeOperand*)pNode)->GetOperandType() == ExprNodeOperand::kNameAttrPair)
		{
			ExprNodeNameAttrPair* pNameAttrPair = static_cast<ExprNodeNameAttrPair*>(pNode);

			//If the name/attr pair node name is referencing the alias for the driver, then update
			//it with the physical driver name/attr information
			string sss = pNameAttrPair->m_name.c_str();

			if( exprDriverVarName.size() && (strcmpi(pNameAttrPair->m_name.c_str(),exprDriverVarName.c_str()) == 0) )
			{
				pNameAttrPair->m_name = pDriver->m_id;
				pNameAttrPair->m_attr = pDriverComponent->m_id;
			}
			else 
			{	
				if( (strcmpi( pDriver->m_id.c_str(), pNameAttrPair->m_name.c_str() ) != 0) ||
					(strcmpi( pDriverComponent->m_id.c_str(), pNameAttrPair->m_attr.c_str()) != 0) )
				{
					//A direct controller reference may have been used in the expression, in this case
					//cache the node in the expression, and we'll create the extra input drivers later
					map<string,string>::iterator itr = mapScalars.find(pNameAttrPair->m_name.c_str());
					if(itr != mapScalars.end())
					{
						pNameAttrPair->m_name = GetAttributeDriverName(atString(itr->second.c_str()),atString(itr->first.c_str())).c_str();
						pNameAttrPair->m_attr = itr->first.c_str();
					}
				}
			}
		}

		//Add the children to the traversal queue
		for(unsigned int i = 0; i < pNode->GetChildCount(); i++)
		{
			exprNodeQueue.push(pNode->GetChild(i));
		}
	}

	// Add any drivers for additional custom attributes in the expression
	for(map<string, string>::iterator p = mapScalars.begin(); p != mapScalars.end(); p++) 
	{
		// create attribute driver id for check
		string strDriverID = GetAttributeDriverName(atString(p->second.c_str()), atString(p->first.c_str()));
		if(!pExpressionController->HasDriver( strDriverID.c_str(), p->first.c_str()) )
		{
			INode* pNodeControl = GetCOREInterface()->GetINodeByName(p->second.c_str());
			CreateDriverForAttribute(theFile, exprBoneFile, pNode, pNodeControl, "", p->first.c_str(), &pDriver, &pDriverComponent);

			pExpressionController->AddDriver(pDriver, pDriverComponent);
		}
	}

	return pExpressionController;
}

atString RageMaxAnimExprExportGUP::GetAttributeDriverName(const atString& czControl, const atString& czAttribute)
{
	char cMsg[RAGE_MAX_PATH];
	sprintf(cMsg, "%s_%s", czControl.c_str(), czAttribute.c_str());

	return atString(cMsg);
}

//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreateLookAtController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* /*pContainerControl*/)
{
	//Errorf("Error: LookAt Controllers are currently not supported.");
	//return NULL;

	///////
	char msgbuffer[4096];

	Assertf(pNode, "Cannot pass a NULL value for pNode to CreateLookAtController.");
	Assertf(pControl, "Cannot pass a NULL value for pControl to CreateLookAtController.");

	atString nrmNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, nrmNodeName);

	ILookAtConstRotation* pLookAtCtrl = static_cast<ILookAtConstRotation*>(pControl->GetInterface(LOOKAT_CONSTRAINT_INTERFACE));
	if(pLookAtCtrl == NULL)
	{
		sprintf(msgbuffer, "Failed to retrieve ILookAtConstRotation interface from controller\r\n");
		Error(msgbuffer, nrmNodeName.c_str());
		//Errorf("Failed to retrieve ILookAtConstRotation interface from controller");
		return NULL;
	}

	INode* pParentNode = pNode->GetParentNode();
	INode* pUpnodeNode = pLookAtCtrl->GetParamBlock(0)->GetINode(lookat_upnode_pick, 0, 0);

	// Because the look at controller does not have normalized weights,
	// total up the weights so we can later normalize each weight
	float weightTotal = 0.0f;
	int numTargets = pLookAtCtrl->GetNumTargets();
	for (int i = 0; i < numTargets; ++i)
	{
		weightTotal += pLookAtCtrl->GetTargetWeight(i);
	}

	// Create a blend controller which will contain weighted object space
	// controllers for each of the look at targets of the look at controller
	AeBlendController* pBlendController = new AeBlendController();
	
	pBlendController->m_id = GenerateUniqueBlendControllerName(nrmNodeName.c_str()).c_str();
	pBlendController->m_type = kVector;
	for (int i = 0; i < pLookAtCtrl->GetNumTargets(); ++i)
	{
		INode* pTargetNode = pLookAtCtrl->GetNode(i);
		Assert(pTargetNode);

		// Create an object space controller that is driven by the translation
		// of this look at target node
		AeDriver* pDriver = NULL;
		AeDriverComponent* pDriverComponent = NULL;
		CreateDriver(theFile, exprBoneFile, pTargetNode, "translate", &pDriver, &pDriverComponent);
		Assert(pDriver);
		Assert(pDriverComponent);
		AeObjectSpaceController* pTargetOSController = new AeObjectSpaceController();

		atString nrmTgtNodeName;
		exprExportUtility::GetNormalizedNodeName(pTargetNode, nrmTgtNodeName);

		pTargetOSController->m_id = GenerateUniqueObjectSpaceControllerName(nrmTgtNodeName.c_str()).c_str();
		pTargetOSController->AddDriver(pDriver, pDriverComponent);

		// Add the new controller-weight pair to the blend controller
		AeBlendControllerItem* pBlendControllerItem = new AeBlendControllerItem();
		pBlendControllerItem->m_pController = pTargetOSController;
		pBlendControllerItem->m_weight = pLookAtCtrl->GetTargetWeight(i) / weightTotal;
		pBlendController->m_inputs.PushAndGrow(pBlendControllerItem);
	}

	if (pBlendController->m_inputs.size() <= 0)
	{
		delete pBlendController;
		return NULL;
	}

	// Create an object space controller that is driven by the translation
	// of the bone node
	AeDriver* pDriver = NULL;
	AeDriverComponent* pDriverComponent = NULL;
	CreateDriver(theFile, exprBoneFile, pNode, "translate", &pDriver, &pDriverComponent);
	Assert(pDriver);
	Assert(pDriverComponent);
	AeObjectSpaceController* pBoneOSController = new AeObjectSpaceController();

	pBoneOSController->m_id = GenerateUniqueObjectSpaceControllerName(nrmNodeName.c_str()).c_str();
	pBoneOSController->AddDriver(pDriver, pDriverComponent);

	// Create an object space controller that is driven by the translation
	// of the bone's parent node
	AeObjectSpaceController* pParentOSController = NULL;
	if (pParentNode != NULL)
	{
		pDriver = NULL;
		pDriverComponent = NULL;
		CreateDriver(theFile, exprBoneFile, pParentNode, "translate", &pDriver, &pDriverComponent);
		Assert(pDriver);
		Assert(pDriverComponent);
		pParentOSController = new AeObjectSpaceController();

		atString nrmParentNodeName;
		exprExportUtility::GetNormalizedNodeName(pParentNode, nrmParentNodeName);

		pParentOSController->m_id = GenerateUniqueObjectSpaceControllerName(nrmParentNodeName.c_str()).c_str();
		pParentOSController->AddDriver(pDriver, pDriverComponent);
	}

	// Create an object space controller that is driven by the rotation
	// of the look at controller's upnode node
	AeObjectSpaceController* pUpnodeOSController = NULL;
	if (pUpnodeNode != NULL)
	{
		pDriver = NULL;
		pDriverComponent = NULL;
		CreateDriver(theFile, exprBoneFile, pUpnodeNode, "rotate", &pDriver, &pDriverComponent);
		Assert(pDriver);
		Assert(pDriverComponent);
		pUpnodeOSController = new AeObjectSpaceController();

		atString nrmUpNodeName;
		exprExportUtility::GetNormalizedNodeName(pUpnodeNode, nrmUpNodeName);

		pUpnodeOSController->m_id = GenerateUniqueObjectSpaceControllerName(nrmUpNodeName.c_str()).c_str();
		pUpnodeOSController->AddDriver(pDriver, pDriverComponent);
	}

	eComponentAxis boneAxis = kAxisNone;
	eComponentAxis boneUpAxis = kAxisNone;
	eComponentAxis upnodeUpAxis = kAxisNone;
	bool flipBoneAxis = (pLookAtCtrl->GetTargetAxisFlip() == TRUE);
	bool flipBoneUpAxis = (pLookAtCtrl->GetStoUPAxisFlip() == TRUE);

	// Determine which axis the bone will align with the look at
	switch (pLookAtCtrl->GetTargetAxis())
	{
	case 0: boneAxis = (flipBoneAxis) ? kNegXAxis : kXAxis; break;
	case 1: boneAxis = (flipBoneAxis) ? kNegYAxis : kYAxis; break;
	case 2: boneAxis = (flipBoneAxis) ? kNegZAxis : kZAxis; break;
	}

	// Determine which axis the bone uses as its up axis
	switch (pLookAtCtrl->Get_StoUPAxis())
	{
	case 0: boneUpAxis = (flipBoneUpAxis) ? kNegXAxis : kXAxis; break;
	case 1: boneUpAxis = (flipBoneUpAxis) ? kNegYAxis : kYAxis; break;
	case 2: boneUpAxis = (flipBoneUpAxis) ? kNegZAxis : kZAxis; break;
	}

	// Determine which axis the upnode uses as its up axis
	switch (pLookAtCtrl->GetUpNodeAxis())
	{
	case 0: upnodeUpAxis = kXAxis; break;
	case 1: upnodeUpAxis = kYAxis; break;
	case 2: upnodeUpAxis = kZAxis; break;
	}

	// If the look at controller is storing the initial offset, get it
	Vec4V offset(0.0f, 0.0f, 0.0f, 1.0f);
	if (pLookAtCtrl->GetRelative() == TRUE)
	{
		Matrix3 nodeTransformRelative = pNode->GetNodeTM(0);
		
		pLookAtCtrl->SetRelative(FALSE);

		Matrix3 nodeTransformNonRelative = pNode->GetNodeTM(0);

		pLookAtCtrl->SetRelative(TRUE);

		Matrix34 rageNodeTransformRelative;
		CopyMaxMatrixToRageMatrix( nodeTransformRelative, rageNodeTransformRelative );

		Matrix34 rageNodeTransformNonRelative;
		CopyMaxMatrixToRageMatrix( nodeTransformNonRelative, rageNodeTransformNonRelative );

		rageNodeTransformRelative.Inverse();
		rageNodeTransformRelative.Dot(rageNodeTransformNonRelative);
		
		Quaternion offsetQuat;
		rageNodeTransformRelative.ToQuaternion(offsetQuat);

		// Pack the offset quaternion in a Vector4
		offset.SetXf(offsetQuat.x);
		offset.SetYf(offsetQuat.y);
		offset.SetZf(offsetQuat.z);
		offset.SetWf(offsetQuat.w);
	}
	
	// Now that we have all the information we need, create the look at controller
	AeLookAtController* pLookAtController = new AeLookAtController();
	
	pLookAtController->m_id = GenerateUniqueLookAtControllerName(nrmNodeName.c_str()).c_str();
	pLookAtController->m_pBlendController = pBlendController;
	pLookAtController->m_pBoneOSController = pBoneOSController;
	pLookAtController->m_pParentOSController = pParentOSController;
	pLookAtController->m_pUpnodeOSController = pUpnodeOSController;
	pLookAtController->m_boneAxis = boneAxis;
	pLookAtController->m_boneUpAxis = boneUpAxis;
	pLookAtController->m_upnodeUpAxis = upnodeUpAxis;
	pLookAtController->m_offset = offset;

	return pLookAtController;
}

//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreatePositionConstraintController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* /*pContainerControl*/)
{
	Assertf(pNode, "Cannot pass a NULL value for pNode to CreatePositionConstraintController.");
	Assertf(pControl, "Cannot pass a NULL value for pControl to CreatePositionConstraintController.");
	char msgbuffer[4096];
	atString normalizedNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, normalizedNodeName);

	//First determine what the position constraint's targets and weights are.
	//
	IPosConstPosition* pPositionConstCtrl = static_cast<IPosConstPosition*>(pControl->GetInterface(POS_CONSTRAINT_INTERFACE));
	if(pPositionConstCtrl == NULL)
	{
		sprintf(msgbuffer, "Failed to retrieve IPosConstPosition interface from controller.\r\n");
		Error(msgbuffer, normalizedNodeName.c_str());
		//Errorf("Failed to retrieve IPosConstPosition interface from controller.");
		return NULL;
	}

	//Position Constraint Controllers are in object space. 

	// Position Constraint controllers do not need to have normalized weights.  
	// Sum the weights so we can normalize them later.
	float weightTotal = 0.0f;
	int numTargets = pPositionConstCtrl->GetNumTargets();
	for (int targetIndex = 0; targetIndex < numTargets; ++targetIndex)
	{
		weightTotal += pPositionConstCtrl->GetTargetWeight(targetIndex);
	}

	// Create a blend controller which will contain weighted object space
	// controllers for each of the targets of the position constraint controller.
	//

	AeObjectSpaceBlendController* pBlendController = new AeObjectSpaceBlendController();
	pBlendController->m_id = GenerateUniqueObjectSpaceBlendControllerName(normalizedNodeName.c_str()).c_str();
	pBlendController->m_hashId = crAnimTrack::ConvertBoneNameToId(normalizedNodeName.c_str());
	pBlendController->m_trackId = kTrackBoneTranslation;
	pBlendController->m_type = kVector;

	for (int targetIndex = 0; targetIndex < pPositionConstCtrl->GetNumTargets(); ++targetIndex)
	{
		INode* pTargetNode = pPositionConstCtrl->GetNode(targetIndex);
		Assert(pTargetNode);

		// Create an object space controller that is driven by the translation
		// of this look at target node.
		//
		AeDriver* pDriver = NULL;
		AeDriverComponent* pDriverComponent = NULL;
		if( CreateDriver(theFile, exprBoneFile, pTargetNode, "translate", &pDriver, &pDriverComponent) == TRUE )
		{
			Assert(pDriver && pDriverComponent);

			AeObjectSpaceController* pTargetOSController = new AeObjectSpaceController();

			atString normalizedTargetNodeName;
			exprExportUtility::GetNormalizedNodeName(pTargetNode, normalizedTargetNodeName);

			pTargetOSController->m_id = GenerateUniqueObjectSpaceControllerName(normalizedTargetNodeName.c_str()).c_str();
			pTargetOSController->AddDriver(pDriver, pDriverComponent);

			// Add the new controller-weight pair to the blend controller.
			//
			AeBlendControllerItem* pBlendControllerItem = new AeBlendControllerItem();
			pBlendControllerItem->m_pController = pTargetOSController;
			pBlendControllerItem->m_weight = pPositionConstCtrl->GetTargetWeight(targetIndex) / weightTotal;
			pBlendController->m_inputs.PushAndGrow(pBlendControllerItem);
		}
	}

	if (pBlendController->m_inputs.size() <= 0)
	{
		delete pBlendController;
		return NULL;
	}

	return pBlendController;
}

//-----------------------------------------------------------------------------

// We need to scale the values we see in max to what we see in the game
#define MAX_TO_GAME_DAMPENING 2.5 // max * 2.5 == game
#define MAX_TO_GAME_TENSION 100 // max*100 == game
#define MAX_TO_GAME_CONSTRAINTS 4000 // max/4000 == game

AeController* RageMaxAnimExprExportGUP::CreateJiggleController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* pContainerControl)
{
	Assertf(pNode, "Cannot pass a NULL value for pNode to CreateJiggleController.");
	Assertf(pControl, "Cannot pass a NULL value for pControl to CreateJiggleController.");
	atString normalizedNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, normalizedNodeName);
	char msgbuffer[4096];
	IRsSpring* pJigglyBits = static_cast<IRsSpring*>(pControl->GetInterface(RS_SPRING_CONTROLLER_INTERFACE));
	if(pJigglyBits == NULL)
	{
		sprintf(msgbuffer, "Failed to retrieve IJiggle interface from controller.\r\n");
		Error(msgbuffer, normalizedNodeName.c_str());
		return NULL;
	}

	AeMotion* pMotion = new AeMotion();

	string szBoneId;
	exprExportUtility::GetBoneId(pNode, szBoneId);

	pMotion->m_boneId = crAnimTrack::ConvertBoneNameToId(szBoneId.c_str());
	pMotion->m_minConstraintAngular = Vec3V(V_ZERO);
	pMotion->m_maxConstraintAngular = Vec3V(V_ZERO);
	pMotion->m_minConstraintLinear = Vec3V(V_ZERO);
	pMotion->m_maxConstraintLinear = Vec3V(V_ZERO);
	pMotion->m_direction = Vec3V(V_ZERO);

	if( RS_SPRINGPOS == pJigglyBits->GetSpringType())
	{
		pMotion->m_strengthAngular = Vec3V(V_ZERO);
		pMotion->m_dampingAngular = Vec3V(V_ZERO);
		pMotion->m_strengthLinear = Vec3V(
			pJigglyBits->GetStrength(SPRING_PARAM_AXIS_X),
			pJigglyBits->GetStrength(SPRING_PARAM_AXIS_Y),
			pJigglyBits->GetStrength(SPRING_PARAM_AXIS_Z));
		pMotion->m_dampingLinear = Vec3V(
			pJigglyBits->GetDampening(SPRING_PARAM_AXIS_X),
			pJigglyBits->GetDampening(SPRING_PARAM_AXIS_Y),
			pJigglyBits->GetDampening(SPRING_PARAM_AXIS_Z));
		pMotion->m_minConstraintLinear = Vec3V(
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MIN),
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MIN),
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MIN));
		pMotion->m_maxConstraintLinear = Vec3V(
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MAX),
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MAX),
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MAX));
	}
	else if(RS_SPRINGROT == pJigglyBits->GetSpringType())
	{
		pMotion->m_strengthLinear = Vec3V(V_ZERO);
		pMotion->m_dampingLinear = Vec3V(V_ZERO);
		pMotion->m_strengthAngular = Vec3V(
			pJigglyBits->GetStrength(SPRING_PARAM_AXIS_X),
			pJigglyBits->GetStrength(SPRING_PARAM_AXIS_Y),
			pJigglyBits->GetStrength(SPRING_PARAM_AXIS_Z));
		pMotion->m_dampingAngular = Vec3V(
			pJigglyBits->GetDampening(SPRING_PARAM_AXIS_X),
			pJigglyBits->GetDampening(SPRING_PARAM_AXIS_Y),
			pJigglyBits->GetDampening(SPRING_PARAM_AXIS_Z));
		pMotion->m_minConstraintAngular = Vec3V(
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MIN),
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MIN),
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MIN));
		pMotion->m_maxConstraintAngular = Vec3V(
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MAX),
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MAX),
			pJigglyBits->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MAX));
	}
	else
	{
		sprintf(msgbuffer, "Error: Failed to retrieve IRsSpring controllers parent class ID.\r\n");
		Error(msgbuffer, normalizedNodeName.c_str());
		return NULL;
	}

	pMotion->m_gravity = Vec3V(
		pJigglyBits->GetGravity(SPRING_PARAM_AXIS_X),
		pJigglyBits->GetGravity(SPRING_PARAM_AXIS_Y),
		pJigglyBits->GetGravity(SPRING_PARAM_AXIS_Z));

	Vector3 tempVec(
		pJigglyBits->GetGravityAxis(SPRING_PARAM_AXIS_X),
		pJigglyBits->GetGravityAxis(SPRING_PARAM_AXIS_Y),
		pJigglyBits->GetGravityAxis(SPRING_PARAM_AXIS_Z));
	if(tempVec.Mag() > 0)
	{
		pMotion->m_direction = VECTOR3_TO_VEC3V(tempVec);
	}

	if(m_exportMainFile)
	{
		theFile.m_motionLibrary.PushAndGrow(pMotion);
	}
	else
	{
		exprBoneFile.m_motionLibrary.PushAndGrow(pMotion);
	}

	return NULL;
}

//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreateOrientationConstraintController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, ::Control* /*pContainerControl*/)
{
	Assertf(pNode, "Cannot pass a NULL value for pNode to CreatePositionConstraintController.");
	Assertf(pControl, "Cannot pass a NULL value for pControl to CreatePositionConstraintController.");
	atString normalizedNodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, normalizedNodeName);
	char msgbuffer[4096];

	//First determine what the position constraint's targets and weights are.
	//
	IOrientConstRotation* pOrientConstRotation = static_cast<IOrientConstRotation*>(pControl->GetInterface(ORIENT_CONSTRAINT_INTERFACE));
	if(pOrientConstRotation == NULL)
	{
		sprintf(msgbuffer, "Failed to retrieve IOrientConstRotation interface from controller.\r\n");
		Error(msgbuffer, normalizedNodeName.c_str());
		//Errorf("Failed to retrieve IOrientConstRotation interface from controller.");
		return NULL;
	}

	//Orientation Constraint Controllers are in object space. 

	// Orientation Constraint controllers do not need to have normalized weights.  
	// Sum the weights so we can normalize them later.
	float weightTotal = 0.0f;
	int numTargets = pOrientConstRotation->GetNumTargets();
	for (int targetIndex = 0; targetIndex < numTargets; ++targetIndex)
	{
		weightTotal += pOrientConstRotation->GetTargetWeight(targetIndex);
	}

	// Create a blend controller which will contain weighted object space
	// controllers for each of the targets of the position constraint controller.
	//

	AeObjectSpaceBlendController* pBlendController = new AeObjectSpaceBlendController();
	pBlendController->m_id = GenerateUniqueObjectSpaceBlendControllerName(normalizedNodeName.c_str()).c_str();
	pBlendController->m_hashId = crAnimTrack::ConvertBoneNameToId(normalizedNodeName.c_str());
	pBlendController->m_trackId = kTrackBoneRotation;
	pBlendController->m_type = kQuat;

	for (int targetIndex = 0; targetIndex < pOrientConstRotation->GetNumTargets(); ++targetIndex)
	{
		INode* pTargetNode = pOrientConstRotation->GetNode(targetIndex);
		Assert(pTargetNode);

		// Create an object space controller that is driven by the translation
		// of this look at target node.
		//
		AeDriver* pDriver = NULL;
		AeDriverComponent* pDriverComponent = NULL;
		if( CreateDriver(theFile, exprBoneFile, pTargetNode, "rotate", &pDriver, &pDriverComponent) == TRUE )
		{
			Assert(pDriver && pDriverComponent);

			AeObjectSpaceController* pTargetOSController = new AeObjectSpaceController();

			atString normalizedTargetNodeName;
			exprExportUtility::GetNormalizedNodeName(pTargetNode, normalizedTargetNodeName);

			pTargetOSController->m_id = GenerateUniqueObjectSpaceControllerName(normalizedTargetNodeName.c_str()).c_str();
			pTargetOSController->AddDriver(pDriver, pDriverComponent);

			// Add the new controller-weight pair to the blend controller.
			//
			AeBlendControllerItem* pBlendControllerItem = new AeBlendControllerItem();
			pBlendControllerItem->m_pController = pTargetOSController;
			pBlendControllerItem->m_weight = pOrientConstRotation->GetTargetWeight(targetIndex) / weightTotal;
			pBlendController->m_inputs.PushAndGrow(pBlendControllerItem);
		}
	}

	if (pBlendController->m_inputs.size() <= 0)
	{
		delete pBlendController;
		return NULL;
	}

	return pBlendController;
}

//-----------------------------------------------------------------------------

AeController* RageMaxAnimExprExportGUP::CreateController(AeFile& theFile, AeFile& exprBoneFile, INode* pNode, ::Control* pControl, BOOL& bZerodControl, BOOL bBlendShape, ::Control* pContainerControl)
{
	char msgbuffer[4096];
	if (pControl)
	{
		Class_ID ctrlCid = pControl->ClassID();

		if( ctrlCid.PartA() == HYBRIDINTERP_FLOAT_CLASS_ID ||
			ctrlCid.PartA() == LININTERP_FLOAT_CLASS_ID ||
			ctrlCid.PartA() == TCBINTERP_FLOAT_CLASS_ID )
		{
			return CreateFloatConstantController(theFile, exprBoneFile, pNode, pControl, pContainerControl);
		}
		else if( ctrlCid.PartA() == FLOATLIMITCTRL_CLASS_ID )
		{
			::Control* pLimitedControl = (::Control*)pControl->GetReference(BASELIMIT_LIMITEDCTRL_REF);
			Assertf(pLimitedControl, "FloatLimitControl assigned to transform of node '%s', as no limited controller", pNode->GetName());
			return CreateController(theFile, exprBoneFile, pNode, pLimitedControl, bZerodControl, bBlendShape, pControl);
		}
		else if( ctrlCid == IPOS_CONTROL_CLASS_ID ||
				 ctrlCid == Class_ID(EULER_CONTROL_CLASS_ID, 0) ||
				 ctrlCid == ISCALE_CONTROL_CLASS_ID)
		{
			return CreateXYZController(theFile, exprBoneFile, pNode, pControl, bZerodControl, pContainerControl);
		}
		else if( (ctrlCid == Class_ID(POSLIST_CONTROL_CLASS_ID,0)) ||
				 (ctrlCid == Class_ID(ROTLIST_CONTROL_CLASS_ID,0)) ||
				 (ctrlCid == Class_ID(SCALELIST_CONTROL_CLASS_ID,0)) ||
				 (ctrlCid == Class_ID(FLOATLIST_CONTROL_CLASS_ID,0)) )
		{
			return CreateBlendController(theFile, exprBoneFile, pNode, pControl, bZerodControl, pContainerControl);
		}
		else if( ctrlCid == Class_ID(EXPR_FLOAT_CONTROL_CLASS_ID,0) )
		{
			return CreateExpressionController(theFile, exprBoneFile, pNode, pControl, bZerodControl, bBlendShape, pContainerControl);
		}
		else if( ctrlCid == FLOAT_WIRE_CONTROL_CLASS_ID )
		{
			return CreateFloatWireController(theFile, exprBoneFile, pNode, pControl, pContainerControl);
		}
		else if( ctrlCid == Class_ID(LOOKAT_CONSTRAINT_CLASS_ID,0) )
		{
			return CreateLookAtController(theFile, exprBoneFile, pNode, pControl, pContainerControl);
		}
		else if (ctrlCid == Class_ID(POSITION_CONSTRAINT_CLASS_ID, 0 ) )
		{
			return CreatePositionConstraintController(theFile, exprBoneFile, pNode, pControl, pContainerControl);
		}
		else if (ctrlCid == Class_ID(ORIENTATION_CONSTRAINT_CLASS_ID, 0))
		{
			return CreateOrientationConstraintController(theFile, exprBoneFile, pNode, pControl, pContainerControl);
		}
		else if(	ctrlCid == RS_SPRING_POS_CLASS_ID ||
					ctrlCid == RS_SPRING_ROT_CLASS_ID ||
					ctrlCid == RS_SPRING_P3_CLASS_ID)
		{
			return CreateJiggleController(theFile, exprBoneFile, pNode, pControl, pContainerControl);
		}
		else
		{
			sprintf(msgbuffer, "Encountered unsupported controller type.\r\n");
			Warn(msgbuffer, pNode->GetName());
		}
	}
	
	return NULL;
}

//-----------------------------------------------------------------------------

BOOL RageMaxAnimExprExportGUP::ExportBoneExpressions(AeFile& exprFile, AeFile& exprBoneFile, INode* pNode, BOOL bRecursive)
{
	ATTRIBUTELOG.SetExpressionType(BONE_EXPRESSIONS);
	char msgbuffer[4096];

	sprintf(msgbuffer, "Info: Exporting expression for '%s'\r\n", pNode->GetName());
	Msg(msgbuffer, pNode->GetName());
	//Displayf("Info: Exporting expression for '%s'\n",pNode->GetName());

	BOOL bZerodControl=FALSE;
	atString nodeName;
	exprExportUtility::GetNormalizedNodeName(pNode, nodeName);

	AeBone *pAeBone = new AeBone();
	pAeBone->m_id = nodeName.c_str();
	pAeBone->m_vParentOffset = Vec3V(1.f,1.f,1.f); //Default this to be non-zero so we have relative offset by default. This can be overrridden in UDP relTrans=false
	pAeBone->m_vParentOrient = Vec4V(0.f,0.f,0.f,0.f); // Default this to 0,0,0,0 because its a invalid quat so JMiller can tell its not relative.

	//If the node is a root node, then it's bone id is automatically set to
	//zero, otherwise generate a hash id 
	if(pNode->GetParentNode() == GetCOREInterface()->GetRootNode() || 
		MaxUtil::IsRootSkelNode(pNode))
	{
		pAeBone->m_hashId = 0;
	}
	else
	{
		string boneId;
		if (!exprExportUtility::GetBoneId(pNode, boneId))
		{
			boneId = pAeBone->m_id;
		}
		
		pAeBone->m_hashId = crAnimTrack::ConvertBoneNameToId(boneId.c_str());
	}

	::Control* pPositionCtrl = pNode->GetTMController()->GetPositionController();
	if (pPositionCtrl && (
		exprExportUtility::IsControllerExportable(pPositionCtrl, pNode, mp_uLog)
		))
	{
		BOOL bBlendShape=FALSE; // Dummy
		AeController* pAeController = CreateController(exprFile, exprBoneFile, pNode, pPositionCtrl, bZerodControl, bBlendShape);
		if (pAeController)
		{
			AeBoneTransformComponent* pComponent = new AeBoneTransformComponent();
			pComponent->m_id = "translate";
			pComponent->m_controllerId = pAeController->m_id;
			pComponent->m_pController = pAeController;

			//Determine the track-id for the bone
			if( exprExportUtility::IsNodeOfClassId(pNode,BONE_OBJ_CLASSID) ||
				exprExportUtility::IsNodeOfClassId(pNode,SKELOBJ_CLASS_ID) ||
				exprExportUtility::IsNodeOfClassId(pNode, Class_ID(POINTHELP_CLASS_ID, 0)))
			{
				string trackIdOverride;
				if (exprExportUtility::GetTrackIdOverrideTranslate(pNode, trackIdOverride))
				{
					pComponent->m_trackId = TrackIDLookup(trackIdOverride.c_str());
				}
				else
				{
					pComponent->m_trackId = kTrackBoneTranslation;
				}	
			}
			else
			{
				string trackIdOverride;
				if (exprExportUtility::GetTrackIdOverrideTranslate(pNode, trackIdOverride))
				{
					pComponent->m_trackId = TrackIDLookup(trackIdOverride.c_str());
				}
				else
				{
					pComponent->m_trackId = kTrackBoneTranslation;
				}
			}

			if(pComponent->m_trackId == kTrackFacialControl)
				pComponent->m_id = "float";

			pAeBone->m_transform.PushAndGrow(pComponent);
			exprFile.m_controllerLibrary.PushAndGrow(pAeController);

			string relativeTransform;
			if(exprExportUtility::GetRelativeTranslateOverride(pNode, relativeTransform))
			{
				if(stricmp(relativeTransform.c_str(),"FALSE") == 0)
				{
					pAeBone->m_vParentOffset = Vec3V(V_ZERO);
					
					if(bZerodControl == TRUE)
					{
						Matrix3 matNode = pNode->GetNodeTM(0);
						Matrix3 matParent = pNode->GetParentTM(0);
						matParent.Invert();
						matNode = matNode * matParent;
						pAeBone->m_vParentOffset = Vec3V(matNode.GetRow(3).x, matNode.GetRow(3).y, matNode.GetRow(3).z); 
					}
				}
			}

			string relativeRotation;
			if(exprExportUtility::GetRelativeRotateOverride(pNode, relativeRotation))
			{
				if(stricmp(relativeRotation.c_str(),"TRUE") == 0)
				{
					if(bZerodControl == TRUE)
					{
						Matrix3 matNode = pNode->GetNodeTM(0);
						Matrix3 matParent = pNode->GetParentTM(0);
						matParent.Invert();
						matNode = matNode * matParent;

						Matrix34 MatObj;
						CopyMaxMatrixToRageMatrix(matNode,MatObj);
						Quaternion q;
						MatObj.ToQuaternion(q);
						pAeBone->m_vParentOrient = Vec4V(q.x, q.y, q.z, q.w);
					}
				}
			}
			if(exprExportUtility::GetRelativeScaleOverride(pNode))
			{
				AffineParts parts;
				decomp_affine(pNode->GetNodeTM(0), &parts);
				pAeBone->m_vParentScale = Vec3V(parts.k.x, parts.k.y, parts.k.z); 
			}
		}
	}

	bZerodControl = FALSE;

	::Control* pRotationCtrl = pNode->GetTMController()->GetRotationController();
	if (pRotationCtrl && exprExportUtility::IsControllerExportable(pRotationCtrl, pNode, mp_uLog))
	{
		BOOL bBlendShape=FALSE; // Dummy
		AeController* pAeController = CreateController(exprFile, exprBoneFile, pNode, pRotationCtrl, bZerodControl, bBlendShape);
		if (pAeController)
		{
			AeBoneTransformComponent* pComponent = new AeBoneTransformComponent();
			pComponent->m_id = "rotate";
			pComponent->m_controllerId = pAeController->m_id;
			pComponent->m_pController = pAeController;

			//Determine the track-id for the bone
			if( exprExportUtility::IsNodeOfClassId(pNode,BONE_OBJ_CLASSID) ||
				exprExportUtility::IsNodeOfClassId(pNode,SKELOBJ_CLASS_ID) ||
				exprExportUtility::IsNodeOfClassId(pNode, Class_ID(POINTHELP_CLASS_ID, 0)))
			{
				string trackIdOverride;
				if (exprExportUtility::GetTrackIdOverrideRotate(pNode, trackIdOverride))
				{
					pComponent->m_trackId = TrackIDLookup(trackIdOverride.c_str());
				}
				else
				{
					pComponent->m_trackId = kTrackBoneRotation;	
				}
			}
			else
			{
				string trackIdOverride;
				if (exprExportUtility::GetTrackIdOverrideRotate(pNode, trackIdOverride))
				{
					pComponent->m_trackId = TrackIDLookup(trackIdOverride.c_str());
				}
				else
				{
					//If there hasn't been a track id indicated in the track-id file, then just default to generic translation
					pComponent->m_trackId = kTrackBoneRotation;
				}
			}

			if(pComponent->m_trackId == kTrackFacialControl)
				pComponent->m_id = "float";

			pAeBone->m_transform.PushAndGrow(pComponent);
			exprFile.m_controllerLibrary.PushAndGrow(pAeController);

			string relativeTransform;
			if(exprExportUtility::GetRelativeTranslateOverride(pNode, relativeTransform))
			{
				if(stricmp(relativeTransform.c_str(),"FALSE") == 0)
				{
					pAeBone->m_vParentOffset = Vec3V(V_ZERO);

					if(bZerodControl == TRUE)
					{
						Matrix3 matNode = pNode->GetNodeTM(0);
						Matrix3 matParent = pNode->GetParentTM(0);
						matParent.Invert();
						matNode = matNode * matParent;
						pAeBone->m_vParentOffset = Vec3V(matNode.GetRow(3).x, matNode.GetRow(3).y, matNode.GetRow(3).z); 
					}
				}
			}

			string relativeRotation;
			if(exprExportUtility::GetRelativeRotateOverride(pNode, relativeRotation))
			{
				if(stricmp(relativeRotation.c_str(),"TRUE") == 0)
				{
					if(bZerodControl == TRUE)
					{
						Matrix3 matNode = pNode->GetNodeTM(0);
						Matrix3 matParent = pNode->GetParentTM(0);
						matParent.Invert();
						matNode = matNode * matParent;

						Matrix34 MatObj;
						CopyMaxMatrixToRageMatrix(matNode,MatObj);
						Quaternion q;
						MatObj.ToQuaternion(q);
						pAeBone->m_vParentOrient = Vec4V(q.x, q.y, q.z, q.w);
					}
				}
			}
			if(exprExportUtility::GetRelativeScaleOverride(pNode))
			{
				AffineParts parts;
				decomp_affine(pNode->GetNodeTM(0), &parts);
				pAeBone->m_vParentScale = Vec3V(parts.k.x, parts.k.y, parts.k.z); 
			}
		}
	}

	::Control* pScaleCtrl = pNode->GetTMController()->GetScaleController();
	if (pScaleCtrl && exprExportUtility::IsControllerExportable(pScaleCtrl, pNode, mp_uLog))
	{
		BOOL bBlendShape=FALSE; // Dummy
		AeController* pAeController = CreateController(exprFile, exprBoneFile, pNode, pScaleCtrl, bZerodControl, bBlendShape);
		if (pAeController)
		{
			AeBoneTransformComponent* pComponent = new AeBoneTransformComponent();
			pComponent->m_id = "scale";
			pComponent->m_controllerId = pAeController->m_id;
			pComponent->m_pController = pAeController;

			//Determine the track-id for the bone
			if( exprExportUtility::IsNodeOfClassId(pNode,BONE_OBJ_CLASSID) ||
				exprExportUtility::IsNodeOfClassId(pNode,SKELOBJ_CLASS_ID) ||
				exprExportUtility::IsNodeOfClassId(pNode, Class_ID(POINTHELP_CLASS_ID, 0)))
			{
				string trackIdOverride;
				if (exprExportUtility::GetTrackIdOverrideScale(pNode, trackIdOverride))
				{
					pComponent->m_trackId = TrackIDLookup(trackIdOverride.c_str());
				}
				else
				{
					pComponent->m_trackId = kTrackBoneScale;	
				}
			}
			else
			{
				string trackIdOverride;
				if (exprExportUtility::GetTrackIdOverrideScale(pNode, trackIdOverride))
				{
					pComponent->m_trackId = TrackIDLookup(trackIdOverride.c_str());
				}
				else
				{
					//If there hasn't been a track id indicated in the track-id file, then just default to generic translation
					pComponent->m_trackId = kTrackBoneScale;
				}
			}

			if(pComponent->m_trackId == kTrackFacialControl)
				pComponent->m_id = "float";

			pAeBone->m_transform.PushAndGrow(pComponent);
			exprFile.m_controllerLibrary.PushAndGrow(pAeController);

			string relativeTransform;
			if(exprExportUtility::GetRelativeTranslateOverride(pNode, relativeTransform))
			{
				if(stricmp(relativeTransform.c_str(),"FALSE") == 0)
				{
					pAeBone->m_vParentOffset = Vec3V(V_ZERO);

					if(bZerodControl == TRUE)
					{
						Matrix3 matNode = pNode->GetNodeTM(0);
						Matrix3 matParent = pNode->GetParentTM(0);
						matParent.Invert();
						matNode = matNode * matParent;
						pAeBone->m_vParentOffset = Vec3V(matNode.GetRow(3).x, matNode.GetRow(3).y, matNode.GetRow(3).z); 
					}
				}
			}

			string relativeRotation;
			if(exprExportUtility::GetRelativeRotateOverride(pNode, relativeRotation))
			{
				if(stricmp(relativeRotation.c_str(),"TRUE") == 0)
				{
					if(bZerodControl == TRUE)
					{
						Matrix3 matNode = pNode->GetNodeTM(0);
						Matrix3 matParent = pNode->GetParentTM(0);
						matParent.Invert();
						matNode = matNode * matParent;

						Matrix34 MatObj;
						CopyMaxMatrixToRageMatrix(matNode,MatObj);
						Quaternion q;
						MatObj.ToQuaternion(q);
						pAeBone->m_vParentOrient = Vec4V(q.x, q.y, q.z, q.w);
					}
				}
			}
			if(exprExportUtility::GetRelativeScaleOverride(pNode))
			{
				AffineParts parts;
				decomp_affine(pNode->GetNodeTM(0), &parts);
				pAeBone->m_vParentScale = Vec3V(parts.k.x, parts.k.y, parts.k.z); 
			}
		}
	}

	if(pAeBone->m_transform.size())
	{
		if(m_exportMainFile)
		{
			exprFile.m_boneLibrary.PushAndGrow(pAeBone);
		}
		else
		{
			exprBoneFile.m_boneLibrary.PushAndGrow(pAeBone);
		}
	}
	else
	{
		delete pAeBone;
	}
	
	if(bRecursive)
	{
		int childCount = pNode->NumberOfChildren();
		for(int i=0; i < childCount; i++)
		{
			ExportBoneExpressions(exprFile, exprBoneFile, pNode->GetChildNode(i), bRecursive);
		}
	}

	return TRUE;
}

//-----------------------------------------------------------------------------

BOOL RageMaxAnimExprExportGUP::ExportMorpherExpressions(AeFile& exprFile, AeFile& exprBoneFile, INode* pNode, AeMesh* pMesh)
{
	ATTRIBUTELOG.SetExpressionType(MESH_EXPRESSIONS);
	char msgbuffer[4096];

	Modifier* pMorphMod = exprExportUtility::GetMorphModifier(pNode);
	if(!pMorphMod)
		return TRUE;	//Nothing to do

	MorphR3* pMorpher = static_cast<MorphR3*>(pMorphMod);
	int chanCount = pMorpher->chanBank.size();
	
	for(int chanIdx=0; chanIdx<chanCount; chanIdx++)
	{
		morphChannel* pMorphChn = &pMorpher->chanBank[chanIdx];
		::Control* pMorphCtrl = pMorphChn->cblock->GetController(0);
		Assert(pMorphCtrl);

		if(exprExportUtility::IsControllerExportable(pMorphCtrl, pNode, mp_uLog))
		{
			AeMeshBlendTarget* pMeshTarget = new AeMeshBlendTarget();

			pMeshTarget->m_id = MaxUtil::GetUniqueBlendShapeName(pNode, pMorphChn->mName);
			pMeshTarget->m_hashId = crAnimTrack::ConvertBoneNameToId( pMeshTarget->m_id.c_str() );

			BOOL bZerodControl=FALSE; //Dummy
			BOOL bBlendShape=TRUE;
			AeController* pController = CreateController(exprFile, exprBoneFile, pNode, pMorphCtrl, bZerodControl, bBlendShape, NULL);
			if(pController)
			{
				pMeshTarget->m_pController = pController;
				pMeshTarget->m_controllerId = pController->m_id;

				exprFile.m_controllerLibrary.PushAndGrow(pController);

				pMesh->m_blendTargets.PushAndGrow(pMeshTarget);
			}
			else
			{
				sprintf(msgbuffer, "Error: Failed to create controller for expression assigned to morph target '%s', on node '%s'\r\n", pMorphChn->mName, pNode->GetName());
				Error(msgbuffer, pNode->GetName());
				//Errorf("Error: Failed to create controller for expression assigned to morph target '%s', on node '%s'", pMorphChn->mName, pNode->GetName());
				delete pMeshTarget;
			}
		}
	}

	return TRUE;
}

//-----------------------------------------------------------------------------


BOOL RageMaxAnimExprExportGUP::ExportMeshExpressions(AeFile& exprFile, AeFile& exprBoneFile, INode* pNode)
{
	AeMesh* pMesh = new AeMesh();

	exprExportUtility::GetNormalizedNodeName( pNode, pMesh->m_id );

	BOOL bRes = ExportMorpherExpressions(exprFile, exprBoneFile, pNode, pMesh);
	if(!bRes || !pMesh->m_blendTargets.size())
	{
		delete pMesh;
		return bRes;
	}
	
	if(m_exportMainFile)
	{
		exprFile.m_meshLibrary.PushAndGrow(pMesh);
	}
	else
	{
		exprBoneFile.m_meshLibrary.PushAndGrow(pMesh);
	}

	return TRUE;
}

//-----------------------------------------------------------------------------

void RageMaxAnimExprExportGUP::RouteDrivers(AeFile& exprFile)
{
	for(int i=0; i < exprFile.GetDriverCount(); ++i)
	{
		AeDriver* pDriver = exprFile.GetDriver(i);
		if(pDriver)
		{
			if(pDriver->m_components.size() == 1)
			{
				AeDriverComponent* pDriverComp = &pDriver->m_components[0];
				if(pDriverComp)
				{
					if(	pDriverComp->m_trackId == kTrackVisemes ||
						pDriverComp->m_trackId == kTrackGenericControl)
					{
						pDriverComp->m_axis = kAxisNone;
						pDriverComp->m_type = kFloat;

						char msgbuffer[4096];
						sprintf(msgbuffer, "Driver '%s' has Track id %d, converting to float\r\n", pDriver->m_id.c_str(), pDriverComp->m_trackId);
						Msg(msgbuffer);
					}
				}
			}
		}
	}
}

void RageMaxAnimExprExportGUP::RouteAnimatedNormalMapData(AeFile& exprFile)
{
	char msgbuffer[4096];
	for(int i=0; i < exprFile.GetBoneCount(); ++i)
	{
		AeBone* pBone = exprFile.GetBone(i);
		if(pBone)
		{
			if(pBone->GetTransformComponentCount() == 1)
			{
				AeBoneTransformComponent* pTransformComponent = pBone->GetTransformComponent(0);
				if(pTransformComponent)
				{
					if(pTransformComponent->m_trackId == kTrackAnimatedNormalMaps || pTransformComponent->m_trackId == kTrackFacialControl)
					{
						for(int j=0; j < exprFile.GetControllerCount(); ++j)
						{
							AeBlendController* pBlendController = dynamic_cast<AeBlendController*>(exprFile.GetController(j));
							if(pBlendController)
							{
								if(pBlendController->m_id == pTransformComponent->m_controllerId)
								{
									if(pBlendController->m_type == kVector)
									{
										// check for 1 dof

										for(int k=0; k < pBlendController->GetBlendItemCount(); ++k)
										{
											AeBlendControllerItem* pBlendControllerItem = pBlendController->GetBlendItem(k);
											if(pBlendControllerItem)
											{
												AeXYZController* pController = dynamic_cast<AeXYZController*>(pBlendControllerItem->m_pController);
												if(pController)
												{
													AeExpressionController* pNewExpressionController = NULL;

													vector<AeController*> vControllers;
													pController->GetContainedControllers(vControllers);

													int iNumExprControllers = 0;

													for(int l=0; l < vControllers.size(); ++l)
													{
														AeExpressionController* pExpressionController = dynamic_cast<AeExpressionController*>(vControllers[l]);
														if(pExpressionController)
														{
															iNumExprControllers++;
															pNewExpressionController = pExpressionController;
														}
													}

													if(iNumExprControllers == 1)
													{
														pBlendController->m_type = kFloat;
														pBlendController->GetBlendItem(k)->m_pController = pNewExpressionController;
														pTransformComponent->m_id = "float";

														sprintf(msgbuffer, "Info: Bone '%s' has track 23/24. Re-directed to be a float controller. Controller '%s' updated.\r\n",pBone->m_id.c_str(), pBlendController->m_id.c_str());
														Msg(msgbuffer);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------

BOOL RageMaxAnimExprExportGUP::Export(Tab<INode*>& nodeArr, TCHAR* outputPath, TCHAR* trackIdFilePath, BOOL bIndividualBones, BOOL bRecursive, Tab<char *> exprPatterns)
{
	AeFile exprFile;

	m_exportMainFile=!bIndividualBones;
	exprExportUtility::s_ExprPatterns.Resize(0);
	if(exprPatterns.Count()>0)
		exprExportUtility::s_ExprPatterns.Append(exprPatterns.Count(), exprPatterns.Addr(0));

	m_ctrlIdCounter = 0;
	m_msgBoxErrorStream.str("");
	m_hashMapConflicts.Reset();
	m_hashMap.clear();
	char msgbuffer[4096];

	EXPORTLOG.ResetLog();
	ATTRIBUTELOG.ResetLog();
	HASHNAMELOG.ResetLog();

	char outputBasePath[MAX_PATH];
	ASSET.RemoveNameFromPath(outputBasePath, MAX_PATH, outputPath);
	const char *filename = ASSET.FileName(outputPath);
	char logFilePath[MAX_PATH];
	char attributesLogFilePath[MAX_PATH];
	char hashNameLogFilePath[MAX_PATH];
	sprintf_s(logFilePath, MAX_PATH, "%s\\Logs\\%s.log", outputBasePath, filename);
	sprintf_s(attributesLogFilePath, MAX_PATH, "%s\\%s.attributes", outputBasePath, filename);
	char filenameWithoutExtension[MAX_PATH];
	ASSET.RemoveExtensionFromPath(filenameWithoutExtension,MAX_PATH,filename);
	sprintf_s(hashNameLogFilePath, MAX_PATH, "%s\\%s.expr.hashnames", outputBasePath, filenameWithoutExtension);

	//Iterate over the supplied nodes and export any expressions associated with them
	int numItems = nodeArr.Count();
	for(int i=0; i<numItems; i++)
	{
		INode* pNode = nodeArr[i];

		if(pNode == NULL)
		{
			sprintf(msgbuffer, "Error: Encountered a NULL INode pointer at index %d.  This specified node will not be processed.\r\n", i);
			Error(msgbuffer, pNode->GetName());
			//Errorf("Encountered a NULL INode pointer at index %d.  This specified node will not be processed.", i);
			continue;
		}

		//Get the class id of the nodes underlying object.
		Object* pObj = pNode->EvalWorldState(0).obj;
		if(pObj == NULL)
			continue;

		Class_ID cid = pObj->ClassID();

		bool bForceBoneExpressions = false;
		TSTR userProp;
		if(pNode->GetUserPropString("forceBoneExpr",userProp))
		{
			bForceBoneExpressions = true;
		}

		if( (cid == EPOLYOBJ_CLASS_ID) || (cid == Class_ID(POLYOBJ_CLASS_ID,0) ) && !bForceBoneExpressions)
		{
			AeFile exprBoneFile;
			ExportMeshExpressions(exprFile, exprBoneFile, pNode);

			if(!m_exportMainFile)
			{
				char bonePath[MAX_PATH];
				sprintf_s(bonePath, MAX_PATH, "%s\\Bones\\%s.xml", outputBasePath, pNode->GetName());

				PARSER.SaveObject(bonePath, "", &exprBoneFile);
			}
		}
		else
		{
			//Anything that isn't a mesh object is considered a "bone"...
			AeFile exprBoneFile;
			ExportBoneExpressions(exprFile, exprBoneFile, pNode, bRecursive);

			if(!m_exportMainFile)
			{
				char bonePath[MAX_PATH];
				sprintf_s(bonePath, MAX_PATH, "%s\\Bones\\%s.xml", outputBasePath, pNode->GetName());

				PARSER.SaveObject(bonePath, "", &exprBoneFile);
			}
		}
	}

	if(exprFile.GetControllerCount() == 0)
	{
		if(exprFile.m_motionLibrary.size() != 0)
		{
			sprintf(msgbuffer, "Warning: Expression file %s contains no controllers but contains %d motions\r\n", outputPath, exprFile.m_motionLibrary.size());
			Warn(msgbuffer);
		}
		else
		{
			sprintf(msgbuffer, "Error: Expression file %s contains no controllers\r\n", outputPath);
			Error(msgbuffer);
		}
	}

	if(m_msgBoxErrorStream.str() != "")
	{
		#ifndef IM_OUTSOURCE_TOOL
			m_msgBoxErrorStream << "\r\n\r\n View Log @ " << logFilePath;
		#endif

		if(IDCANCEL == MaxMsgBox(NULL, m_msgBoxErrorStream.str().c_str(), "Error Log", MB_OKCANCEL ))
		{
			#ifndef IM_OUTSOURCE_TOOL
				EXPORTLOG.SaveLog(logFilePath);
			#endif

			return FALSE;
		}
	}

	RouteAnimatedNormalMapData(exprFile);
	RouteDrivers(exprFile);

	if(mp_uLog)
	{
		mp_uLog->Flush();
	}

	bool bSaveRes = true;
	if(m_exportMainFile)
	{
		//Save the expression data
		Msg("Info: No main expression file configured for this export.\r\n");
		//Displayf("Info: Saving expression file : %s\n", outputPath);
		bSaveRes = PARSER.SaveObject(outputPath, "", &exprFile);
	}

	for(int i=0; i < exprFile.GetDriverCount(); ++i)
	{
		AeDriver* pDriver = exprFile.GetDriver(i);
		if(pDriver)
		{
			for(int j=0; j < pDriver->m_components.size(); ++j)
			{
				HASHNAMELOG.AddEntry(pDriver->m_components[j].m_trackId, pDriver->m_boneId, rage::atString(pDriver->m_id.c_str()));
			}
		}
	}


#ifndef IM_OUTSOURCE_TOOL
	ATTRIBUTELOG.SaveLog(atString(attributesLogFilePath));
	HASHNAMELOG.SaveLog(atString(hashNameLogFilePath));
	EXPORTLOG.SaveLog(logFilePath);
	return bSaveRes;
#else
	return bSaveRes;
#endif
}

//-----------------------------------------------------------------------------

