Project rageMaxAnimExprExport
Template max_configurations.xml

OutputFileName rageMaxAnimExprExport.gup
ConfigurationType dll

ForceInclude ForceInclude.h
ModuleDefinitionFile rageMaxAnimExprExport.def

IncludePath STLPORT
IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\suite\src
IncludePath $(RAGE_DIR)\suite\tools\dcc\libs
IncludePath $(RAGE_DIR)\3rdParty\Autodesk\3dsMax2011SDK\samples\controllers
IncludePath $(RAGE_DIR)\framework\tools\src\dcc\libs
IncludePath $(RAGE_DIR)\framework\tools\src\dcc\max\controllers\RsSpring


EmbeddedLibAll comctl32.lib acap.lib biped.lib bmm.lib composite.lib core.lib crowd.lib CustDlg.lib edmodel.lib expr.lib flt.lib geom.lib gfx.lib gup.lib helpsys.lib IGame.lib imageViewers.lib ManipSys.lib maxnet.lib Maxscrpt.lib maxutil.lib MenuMan.lib menus.lib mesh.lib MNMath.lib Paramblk2.lib particle.lib ParticleFlow.lib physique.lib Poly.lib RenderUtil.lib rtmax.lib SpringSys.lib tessint.lib viewfile.lib zlibdll.lib

Files {
	AnimatedCustomAttributeWriter.h
	ExpressionControl.h
	exprctrl.cpp
	ForceInclude.h
	iexprctrl.h
	IMaxAnimExprExport.h
	jiggle.h
	NodeTransformMonitor.cpp
	pluginMain.cpp
	rageMaxAnimExprExport.cpp
	rageMaxAnimExprExport.def
	rageMaxAnimExprExport.h
	rageMaxAnimExprExport.rc
	resource.h
	utility.h
	utility.cpp	
}