// /utility.cpp

#include "file/asset.h"
#include "utility.h"
#include <ispringctrl.h>
#include <IRsSpringCtrl.h> 
#include "MaxUtil/MaxUtil.h"

using namespace rage;

namespace exprExportUtility
{

Tab<char *> s_ExprPatterns;
static const s32 sTicksPerSec = 4800;

//-----------------------------------------------------------------------------

bool IsNodeOfClassId(INode* pNode, Class_ID cid, bool bStrictCompare)
{
	if(!pNode)
		return false;

	Object* pObj = pNode->EvalWorldState(0).obj;

	if(!pObj)
		return false;

	Class_ID objCid = pObj->ClassID();

	if(bStrictCompare)
	{
		if(objCid == cid)
			return true;
		else
			return false;
	}
	
	if(pObj->CanConvertToType(cid))
		return true;
	
	return false;
}

bool IsControllerExportable(::Control *pControl, INodePtr pNode, IFPULog *pLog)
{
	char controlName[1024] = {0};
	
	if(MaxUtil::GetControlName(pControl, pNode, controlName, 1024))
	{
		for (int patternindex =0; patternindex<s_ExprPatterns.Count();patternindex++)
		{
			if(MaxUtil::pattern_match(controlName, s_ExprPatterns[patternindex]))
			{
				if(pLog)
				{
					char msg[1024];
					sprintf_s(msg, "Not exporting control name \"%s\" due to given ignore pattern given to export: %s.", controlName, s_ExprPatterns[patternindex]);
					pLog->LogWarning(msg, controlName);
				}
				return false;
			}
		}
	}
	Class_ID ctrlCid = pControl->ClassID();
	if( (ctrlCid == FLOAT_WIRE_CONTROL_CLASS_ID) ||
		(ctrlCid == Class_ID(EXPR_FLOAT_CONTROL_CLASS_ID,0)) ||
		(ctrlCid == Class_ID(LOOKAT_CONSTRAINT_CLASS_ID,0)) ||
		(ctrlCid == Class_ID(POSITION_CONSTRAINT_CLASS_ID, 0)) ||
		(ctrlCid == Class_ID(ORIENTATION_CONSTRAINT_CLASS_ID, 0)) ||
		(ctrlCid == RS_SPRING_POS_CLASS_ID) ||
		(ctrlCid == RS_SPRING_ROT_CLASS_ID))
	{
		return true;
	}
	else
	{
		//If its a list controller then iterate over each element of the list
		//checking to see if any are expression driven
		IListControl *pIList = GetIListControlInterface(pControl);
		if(pIList)
		{
			int numItems = pIList->GetListCount();
			for(int i=0; i<numItems; i++)
			{
				::Control *pListCtrlItem = pIList->GetSubCtrl(i);
				if(pListCtrlItem)
				{
					if(IsControllerExportable(pListCtrlItem, pNode, pLog))
					{
						return true;
					}
				}
			}
		}
		else
		{
			int numSubs = pControl->NumSubs();
			for(int subIdx = 0; subIdx < numSubs; subIdx++)
			{	
				::Control* pCompCtrl = static_cast<::Control*>(pControl->SubAnim(subIdx));
				if(IsControllerExportable(pCompCtrl, pNode, pLog))
				{
					return true;
				}
			}
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

bool GetNormalizedNodeName(INode* pNode, atString& outName, bool bRemoveNamespace)
{
	string nodeName = pNode->GetName();

	if(bRemoveNamespace)
	{
		size_t colonPos = nodeName.find_last_of(':');
		if(colonPos != string::npos)
		{
			nodeName = nodeName.substr(colonPos+1);
		}
	}

	size_t nameLen = nodeName.size();
	for(size_t i=0; i<nameLen; i++)
	{
		if( (nodeName[i] == ':') ||
			(nodeName[i] == '|') ||
			(nodeName[i] == ' ') )
		{
			nodeName[i] = '_';
		}
	}

	outName = nodeName.c_str();

	return true;
}

//-----------------------------------------------------------------------------

bool GetBoneId(INode* pNode, string& outId)
{
	outId = "";

	TSTR userProp;
	if(MaxUtil::GetUserPropString(pNode, "tag", userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------

bool GetTrackIdOverrideTranslate(INode* pNode, string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("translateTrackType",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------

bool GetTrackIdOverrideRotate(INode* pNode, string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("rotateTrackType",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------

bool GetTrackIdOverrideScale(INode* pNode, string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("scaleTrackType",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------

bool GetRelativeTranslateOverride(INode* pNode, string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("relTrans",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------

bool GetRelativeRotateOverride(INode* pNode, string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("relRot",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

bool GetRelativeScaleOverride(INode* pNode)
{
	BOOL useRelScale = FALSE;
	BOOL hasProp = pNode->GetUserPropBool("relScale", useRelScale);
	return (hasProp && useRelScale);
}

//-----------------------------------------------------------------------------

Modifier* GetMorphModifier(INode* p_Node)
{
	Object* p_Obj = p_Node->GetObjectRef();

	if(!p_Obj)
		return NULL;

	while(p_Obj->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject* p_DerObj = (IDerivedObject*)(p_Obj);

		s32 Idx = 0,Count = p_DerObj->NumModifiers();

		while(Idx < Count)
		{
			Modifier* p_Mod = p_DerObj->GetModifier(Idx);

			Class_ID classID = p_Mod->ClassID();

			if(classID == MR3_CLASS_ID)
			{
				return ((Modifier*)p_Mod);
			}

			Idx++;
		}

		p_Obj = p_DerObj->GetObjRef();
	}

	return NULL;
}

//-----------------------------------------------------------------------------

void CopyMaxMatrixToRageMatrix(const Matrix3& srcMtx, Matrix34& dstMtx)
{
	Point4 SrcRow;

	SrcRow = srcMtx.GetRow(0);
	dstMtx.a = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = srcMtx.GetRow(1);
	dstMtx.b = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = srcMtx.GetRow(2);
	dstMtx.c = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = srcMtx.GetRow(3);
	dstMtx.d = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
}

//-----------------------------------------------------------------------------

void CopyRageMatrixToMaxMatrix(const Matrix34& srcMtx, Matrix3& dstMtx)
{
	Point3 SrcRow;

	SrcRow = Point3(srcMtx.a.x,srcMtx.a.y,srcMtx.a.z);
	dstMtx.SetRow(0,SrcRow);
	SrcRow = Point3(srcMtx.b.x,srcMtx.b.y,srcMtx.b.z);
	dstMtx.SetRow(1,SrcRow);
	SrcRow = Point3(srcMtx.c.x,srcMtx.c.y,srcMtx.c.z);
	dstMtx.SetRow(2,SrcRow);
	SrcRow = Point3(srcMtx.d.x,srcMtx.d.y,srcMtx.d.z);
	dstMtx.SetRow(3,SrcRow);
}

//-----------------------------------------------------------------------------

void GetLocalMatrix(INode* pNode, float fTime, Matrix34& outLocal)
{
	TimeValue TimeVal = fTime * sTicksPerSec;

	INode* pParentNode = pNode->GetParentNode();
	
	Matrix34 MatObj;
	Matrix34 MatParent;

	CopyMaxMatrixToRageMatrix(pNode->GetNodeTM(TimeVal),MatObj);
	MatParent.Identity();

	if(pParentNode != GetCOREInterface()->GetRootNode())
	{
		CopyMaxMatrixToRageMatrix(pParentNode->GetNodeTM(TimeVal),MatParent);
	}

	outLocal.DotTranspose(MatObj,MatParent);
}

//-----------------------------------------------------------------------------

bool exprExportLog::SaveLog(const char* szFilePath)
{
	ASSET.CreateLeadingPath(szFilePath);
	fiStream* pLogFile = ASSET.Create(szFilePath, "");
	if(pLogFile)
	{
		pLogFile->Write( m_log.str().c_str(), (int)m_log.str().size() );
		pLogFile->Close();
		return true;
	}
	else
		return false;
}

//-----------------------------------------------------------------------------

} //end namespace exprExportUtility

