/**********************************************************************
 *<
	FILE: jiggle.h

	DESCRIPTION:	A procedural Mass/Spring Position controller 
					Main header

	CREATED BY:		Adam Felt

	HISTORY: 

 *>	Copyright (c) 1999-2000, All Rights Reserved.
 **********************************************************************/

#ifndef __JIGGLE__H
#define __JIGGLE__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamm2.h"
#include "custcont.h"
#include "SpringSys.h"
#include "ISpringCtrl.h"
#include "Notify.h"
#include <ILockedTracks.h>

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define HAS_NO_PARENT  (1<<0)

#define X_EFFECT 1
#define Y_EFFECT 2
#define Z_EFFECT 3

class Jiggle;
class JiggleDlg;

//dialog stuff 
class DynMapDlgProc : public ParamMap2UserDlgProc {
	public:
		Jiggle *cont;
		IParamMap2 *paramMap;
		ICustButton		*iAddBone;
		ICustButton		*iDeleteBone;
		ISpinnerControl	*iTension;
		ISpinnerControl	*iDampening;
		ISpinnerControl *iMass;
		ISpinnerControl *iDrag;

		DynMapDlgProc(Jiggle *c) {cont = c; paramMap = NULL;}	
		INT_PTR DlgProc(TimeValue t,IParamMap2 *map,HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);		
		void DeleteThis() {delete this;}
		
		void InitParams();
		void DestroyParams();
		void Update(TimeValue t);
	};

class ForceMapDlgProc : public ParamMap2UserDlgProc {
	public:
		Jiggle *cont;		
		ForceMapDlgProc(Jiggle *c) {cont = c;}		
		INT_PTR DlgProc(TimeValue t,IParamMap2 *map,HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);		
		void DeleteThis() {delete this;}
	};
	
//About dialog handler
INT_PTR CALLBACK AboutRollupDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam );

//validator classes for the pick node paramblocks
class JiggleValidatorClass : public PBValidator
{
public:
Jiggle *cont;
private:
BOOL Validate(PB2Value &v) 
	{
	INode *node = (INode*) v.r;

	if (node->TestForLoop(FOREVER,(ReferenceMaker *) cont)!=REF_SUCCEED) return FALSE;

	const ObjectState& os = node->EvalWorldState(0);
	Object* ob = os.obj;
	if (ob!=NULL) 
			{	
			int id=(os.obj?os.obj->SuperClassID():SClass_ID(0));
			if (id==WSM_OBJECT_CLASS_ID)
				{
				Object* obref = node->GetObjectRef();

				BOOL ShouldBeHere=FALSE;
				if (obref != NULL)
				{
					WSMObject* wsmObj = static_cast<WSMObject*>(obref->GetInterface(I_WSMOBJECT));
					if (NULL == wsmObj && WSM_OBJECT_CLASS_ID == obref->SuperClassID()) 
					{
						wsmObj = static_cast<WSMObject*>(obref);
					}

					if (wsmObj)
					{
						ShouldBeHere = wsmObj->SupportsDynamics();
					}
				}

//				WSMObject *obref=(WSMObject*)node->GetObjectRef();
//				BOOL ShouldBeHere=obref->SupportsDynamics();

				return ShouldBeHere;
				}
			else return FALSE;
				  
			}
	return FALSE;

	};
};

class JiggleNodeValidatorClass : public PBValidator
{
public:
Jiggle *cont;
private:
BOOL Validate(PB2Value &v) 
	{
	INode *node = (INode*) v.r;

	if (node->TestForLoop(FOREVER,(ReferenceMaker *) cont)!=REF_SUCCEED) return FALSE;
	return TRUE;
	};
};

class CacheClass
{
	public:
		Point3 vel, pos;
};

class PickForceMode : 
	public PickModeCallback,
	public PickNodeCallback
{
public:		
	JiggleDlg* dlg;

	PickForceMode(JiggleDlg* d) {dlg=d;}
	BOOL HitTest(IObjParam* ip, HWND hWnd, ViewExp* vpt, IPoint2 m, int flags);
	BOOL Pick(IObjParam* ip, ViewExp* vpt);
	void EnterMode(IObjParam* ip);
	void ExitMode(IObjParam* ip);
	BOOL RightClick(IObjParam* ip, ViewExp* vpt);
	BOOL Filter(INode* node);		
	PickNodeCallback* GetFilter() {return this;}
	BOOL AllowMultiSelect(){ return true; }
};

class PickNodeMode : 
		public PickModeCallback,
		public PickNodeCallback {
	public:		
		Jiggle* cont;
		DynMapDlgProc* map;
		
		PickNodeMode(DynMapDlgProc *m) {map = m; cont = map->cont;}
		PickNodeMode(Jiggle *c) {map = NULL; cont = c;}

		BOOL HitTest(IObjParam* ip,HWND hWnd,ViewExp* vpt,IPoint2 m,int flags);
		BOOL Pick(IObjParam* ip,ViewExp* vpt);
		BOOL PickAnimatable(Animatable* anim);
		void EnterMode(IObjParam* ip);
		void ExitMode(IObjParam* ip);
		BOOL RightClick(IObjParam* ip,ViewExp* vpt);
		BOOL Filter(INode* node);		
		PickNodeCallback* GetFilter() {return this;}
		BOOL AllowMultiSelect(){ return true; }
	};


void UnRegisterJiggleWindow(HWND hWnd);


//Function Publishing stuff
#define GetSpringControllerInterface(cd) \
			(Jiggle *)(cd)->GetInterface(SPRING_CONTROLLER_INTERFACE)

class Jiggle : public LockableControl, public IJiggle, public SpringSysClient {
	public:
		
		int type;						//the type of controller it is
		bool validStart;
		INode *selfNode;
		BOOL ctrlValid;

		Tab<Matrix3> initState;

		Interval	ivalid;
		Interval	range;
		IParamBlock2 *dyn_pb;
		IParamBlock2 *force_pb;

		Control *posCtrl;			// ref 0
		byte flags;

		JiggleDlg* dlg;
		DynMapDlgProc* pmap;

		HWND	hParams1;  //dynamics dialog handle
		HWND	hParams2;  //force dialog handle
		PickNodeMode *pickNodeMode;
		
		static IObjParam *ip;
		JiggleValidatorClass validator;
		JiggleNodeValidatorClass node_validator;

		Jiggle(int type, BOOL loading);
		~Jiggle();

		Jiggle& operator=(const Jiggle& from);

		int	NumParamBlocks() { return 2; }					// return number of ParamBlocks in this instance
		IParamBlock2* GetParamBlock(int i) {if (i==0) return dyn_pb; 
						else if (i==1) return force_pb; 
						else return NULL;} // return i'th ParamBlock
		IParamBlock2* GetParamBlockByID(BlockID id) { if (dyn_pb->ID() == id) return dyn_pb;  
						else if (force_pb->ID() == id) return force_pb; 
						else return NULL; } // return id'd ParamBlock
		void BeginEditParams(IObjParam *ip, ULONG flags,Animatable *prev); 
		void EndEditParams(IObjParam *ip, ULONG flags,Animatable *next); 

		// Animatable's Schematic View methods
		SvGraphNodeReference SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags);
		TSTR SvGetRelTip(IGraphObjectManager *gom, IGraphNode *gNodeTarger, int id, IGraphNode *gNodeMaker);
		bool SvHandleRelDoubleClick(IGraphObjectManager *gom, IGraphNode *gNodeTarget, int id, IGraphNode *gNodeMaker);

		void EditTrackParams(
			TimeValue t,
			ParamDimensionBase *dim,
			TCHAR *pname,
			HWND hParent,
			IObjParam *ip,
			DWORD flags);
		int TrackParamsType(){if(GetLocked()==false) return TRACKPARAMS_WHOLE; else return TRACKPARAMS_NONE; }	

		void DeleteThis() {delete this;}		
		int IsKeyable() {return 0;}		
		BOOL IsAnimated() {return TRUE;}  //this could be done better

		int NumSubs()  {return 3;}
		Animatable* SubAnim(int i);
		TSTR SubAnimName(int i);
		int SubNumToRefNum(int subNum);
		BOOL ChangeParents(TimeValue t,const Matrix3& oldP,const Matrix3& newP,const Matrix3& tm);

		//From Animatable
		void CopyKeysFromTime(TimeValue src,TimeValue dst,DWORD flags) {posCtrl->CopyKeysFromTime(src,dst,flags);}
		BOOL IsKeyAtTime(TimeValue t,DWORD flags) {return posCtrl->IsKeyAtTime(t,flags);}
		BOOL GetNextKeyTime(TimeValue t,DWORD flags,TimeValue &nt) {return posCtrl->GetNextKeyTime(t,flags,nt);}
		int GetKeyTimes(Tab<TimeValue> &times,Interval range,DWORD flags) {return posCtrl->GetKeyTimes(times,range,flags);}
		int GetKeySelState(BitArray &sel,Interval range,DWORD flags) {return posCtrl->GetKeySelState(sel,range,flags);}

		virtual void MouseCycleStarted(TimeValue t);
		virtual void MouseCycleCompleted(TimeValue t);

		// Reference methods
		int NumRefs() {return 3;}		
		RefTargetHandle GetReference(int i);
		void SetReference(int i, RefTargetHandle rtarg);
		RefResult NotifyRefChanged(Interval, RefTargetHandle, PartID&, RefMessage);
		IOResult Save(ISave *isave);
		IOResult Load(ILoad *iload);
		BOOL AssignController(Animatable *control,int subAnim);

		// Control methods				
		void Copy(Control *from);
		BOOL IsLeaf() {return FALSE;}
		void SetValue(TimeValue t, void *val, int commit, GetSetMethod method);
		void CommitValue(TimeValue t) {posCtrl->CommitValue(t); ctrlValid = true; }
		void RestoreValue(TimeValue t) {posCtrl->RestoreValue(t);}	
		
		//FPMixinInterface methods
		BaseInterface* GetInterface(Interface_ID id) 
		{ 
			if (id == SPRING_CONTROLLER_INTERFACE) 
				return (Jiggle*)this; 
			else 
				return FPMixinInterface::GetInterface(id);
		} 

		//Local Methods
		Matrix3 GetCurrentTM(TimeValue t);
		BOOL SetSelfReference();
		Point3 ComputeValue(TimeValue t);
		void UpdateNodeList(bool updateSpinners = true);		
	
		//From IJiggle
		virtual float	GetMass();
		virtual float	GetDrag();
		virtual float	GetTension(int index);
		virtual float	GetDampening(int index);
		virtual void	SetMass(float mass, bool update=true);
		virtual void	SetDrag(float drag, bool update=true);
		virtual void	SetTension(int index, float tension, int absolute=1, bool update=true);
		virtual void	SetDampening(int index, float dampening, int absolute=1, bool update=true);
		virtual BOOL	AddSpring(INode *node);	
		virtual INT		GetSpringCount();
		virtual void	RemoveSpring(int which);
		virtual void	RemoveSpring(INode *node);
		virtual SpringSys* GetSpringSystem() { return partsys; }
		
		//From SpringSysClient
		Tab<Matrix3> GetForceMatrices(TimeValue t);
		Point3 GetDynamicsForces(TimeValue t, Point3 pos, Point3 vel);

		//Undo methods
		void HoldAll();
};

#endif