#pragma once

#include "atl/array.h"
#include "atl/string.h"
#include "file/asset.h"
#include "file/stream.h"

#include "animExprData/animExprData.h"
#include "cranimation/animtrack.h"

#define BONE_EXPRESSIONS 0
#define MESH_EXPRESSIONS 1

class AnimatedCustomAttributeWriter
{
public:

	AnimatedCustomAttributeWriter() : m_expressionType(0){}

	void SetExpressionType(const int expressionType)
	{
		m_expressionType = expressionType;
	}

	void ResetLog()
	{
		m_list.Reset();
	}

	void AddEntry(rage::atString str) 
	{
		if(m_expressionType != BONE_EXPRESSIONS) return;

		// We need to do some processing on the string first, the target string comes in as 
		// $con_ etc and we need to remove the $ as files imported into motionbuilder dont have this.
		str.Replace("$","");
		// target string in max is con_browRaise_cl.modifiers[#Attribute_Holder].IM_ATTRS_A.Squeeze
		// in motionbuilder its con_browRaise_cl.IM_ATTRS_A.Squeeze
		str.Replace(".modifiers[#Attribute_Holder]","");

		// some controls seem to have quotes within 'control', we need to remove these
		int iIndex = str.IndexOf("'");
		while(iIndex != -1)
		{
			str.Replace("'","");
			iIndex = str.IndexOf("'");
		}

		for(rage::atArray<rage::atString>::iterator it = m_list.begin(); it != m_list.end(); ++it)
		{
			if(*it == str)
			{
				return;
			}
		}

		m_list.PushAndGrow( str );
	}

	void SaveLog(rage::atString str)
	{
		rage::fiStream* pLogFile = rage::ASSET.Create(str.c_str(), "");
		if(pLogFile)
		{
			for(rage::atArray<rage::atString>::iterator it = m_list.begin(); it != m_list.end(); ++it)
			{
				pLogFile->Write( (*it).c_str(), (int)(*it).length() );
				pLogFile->Write("\r\n",2);
				
			}
			pLogFile->Close();
		}	
	}

private:
	rage::atArray<rage::atString>	m_list;
	int m_expressionType;
};