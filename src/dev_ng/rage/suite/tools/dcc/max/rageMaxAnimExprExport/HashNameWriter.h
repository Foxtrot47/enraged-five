#pragma once

#include "atl/array.h"
#include "atl/string.h"
#include "file/asset.h"
#include "file/stream.h"

#include "animExprData/animExprData.h"
#include "cranimation/animtrack.h"

#define BONE_EXPRESSIONS 0
#define MESH_EXPRESSIONS 1

class HashNameWriter
{
public:

	HashNameWriter() : m_expressionType(0){}

	void SetExpressionType(const int expressionType)
	{
		m_expressionType = expressionType;
	}

	void ResetLog()
	{
		m_list.Reset();
	}

	void AddEntry(int trackID, int boneHash, rage::atString boneID) 
	{
		if(m_expressionType != BONE_EXPRESSIONS) return;

		char msg[MAX_PATH];
		sprintf(msg, "%d,%d,%s", trackID, boneHash, boneID.c_str());

		for(int i=0; i < m_list.GetCount(); ++i)
		{
			if(stricmp(msg, m_list[i].c_str()) == 0) return;
		}

		m_list.PushAndGrow( rage::atString(msg) );
	}

	void SaveLog(rage::atString str)
	{
		rage::fiStream* pLogFile = rage::ASSET.Create(str.c_str(), "");
		if(pLogFile)
		{
			for(rage::atArray<rage::atString>::iterator it = m_list.begin(); it != m_list.end(); ++it)
			{
				pLogFile->Write( (*it).c_str(), (int)(*it).length() );
				pLogFile->Write("\r\n",2);

			}
			pLogFile->Close();
		}	
	}

private:
	rage::atArray<rage::atString>	m_list;
	int m_expressionType;
};