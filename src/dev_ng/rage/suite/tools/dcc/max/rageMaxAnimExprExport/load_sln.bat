SET RAGE_DIR=X:\gta5\src\dev\rage\
SET TOOLS_DEV=%RAGE_DIR%\framework\tools\src\dev
SET MAX_DEPLOY=%RAGE_DIR%\framework\tools\dcc\current\max2009

SET VISUAL_STUDIO="C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe"
SET VISUAL_STUDIO_X86="C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe"
SET SOLUTION="%RAGE_DIR%\suite\tools\dcc\max\rageMaxAnimExprExport\rageMaxAnimExprExport.sln"
  
IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 
       start "" %VISUAL_STUDIO_X86% %SOLUTION%
) ELSE (
       start "" %VISUAL_STUDIO% %SOLUTION%
)
