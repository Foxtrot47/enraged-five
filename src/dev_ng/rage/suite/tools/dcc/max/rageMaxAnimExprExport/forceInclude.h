// /forceInclude.h

#ifndef __MAX_FORCEINCLUDE_H__
#define __MAX_FORCEINCLUDE_H__

//this file is included through the forceinclude setting
//in the project so is included in every file

//we have to include max first because it defines some functions
//that are macros in the rage headers (Assert)

#include <max.h>
#include <bmmlib.h>
#include <guplib.h>
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <iparamwire.h>
#include <istdplug.h>
#include <modstack.h>
#include <iskin.h>
#include <inode.h>
#include <control.h>
#include <actiontable.h>
#include <iskinpose.h>
#include <imenuman.h>
#include <cs/bipexp.h>
#include <stdmat.h>
#include <maxscrpt/maxscrpt.h>

#if defined(_DEBUG) && defined(_M_X64)
#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
#include "forceinclude/win64_toolbeta.h"
#else
#include "forceinclude/win32_toolbeta.h"
#endif

#endif //__MAX_FORCEINCLUDE_H__
