// 
// animExprExportCommon/unitConvert.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef ANIMEXPREXPORTCOMMON_UNITCONVERT_H 
#define ANIMEXPREXPORTCOMMON_UNITCONVERT_H 

namespace rage {

//-----------------------------------------------------------------------------

class UnitConverter
{
public:
	enum EnumUnitTypeLinear
	{
		NONE = -1,
		MILLIMETERS = 0,
		CENTIMETERS,
		METERS,
		INCHES,
		FEET,
		YARDS,
	};
	enum {NUM_UNIT_TYPE_LINEARS=YARDS+1};

	static bool		StringNameToUnitTypeLinear(const char* szName, EnumUnitTypeLinear& retType);
	static float	GetUnitTypeLinearConversionFactor(EnumUnitTypeLinear fromUnits, EnumUnitTypeLinear toUnits);
};

//-----------------------------------------------------------------------------

} // namespace rage

#endif // ANIMEXPREXPORTCOMMON_UNITCONVERT_H 
