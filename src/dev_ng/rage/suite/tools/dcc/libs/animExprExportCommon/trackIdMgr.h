// /trackIdMgr.h

#ifndef _TRACK_ID_MGR_H_
#define _TRACK_ID_MGR_H_

#include <string>
#include <vector>

using namespace std;

//-----------------------------------------------------------------------------

class trackIdMgr
{
public:
	trackIdMgr();
	~trackIdMgr();

	bool LoadIdFile(const char* szFilePath);

	bool Lookup(const char* szNodeName, const char* szComponentName, unsigned int& retId) const;
	
private:
	struct trackIdRecord
	{
		trackIdRecord(const char* szNodeExpr, const char* szComponentExpr, unsigned int id)
			: m_nodeExpr(szNodeExpr)
			, m_compExpr(szComponentExpr)
			, m_trackId(id)
		{};

		string			m_nodeExpr;
		string			m_compExpr;
		unsigned int	m_trackId;
	};

private:
	void Reset(bool bAddKnown);
	void PopulateKnownIds();
	int  CompareExprToName(const char* szExpr, const char* name) const;

private:
	typedef vector< trackIdMgr::trackIdRecord* > ID_TABLE;
	ID_TABLE	m_idTable;
};

//-----------------------------------------------------------------------------

#endif //_TRACK_ID_MGR_H_

