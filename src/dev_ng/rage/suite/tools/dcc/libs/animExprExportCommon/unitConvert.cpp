// 
// animExprExportCommon/unitConvert.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#include <map>
#include <string>

#include "unitConvert.h"

using namespace rage;

namespace rage {

//-----------------------------------------------------------------------------

bool UnitConverter::StringNameToUnitTypeLinear(const char* szName, EnumUnitTypeLinear& retType)
{
	const char* unitTypeNameArray[NUM_UNIT_TYPE_LINEARS] =
	{
		"MILLIMETERS",
		"CENTIMETERS",
		"METERS",
		"INCHES",
		"FEET",
		"YARDS",
	};

	if(_strcmpi(szName,"NONE") == 0)
	{
		retType = NONE;
		return true;
	}

	for(int i=0; i<NUM_UNIT_TYPE_LINEARS; i++)
	{
		if(_strcmpi(unitTypeNameArray[i], szName) == 0)
		{
			retType = (EnumUnitTypeLinear)i;
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

float UnitConverter::GetUnitTypeLinearConversionFactor(EnumUnitTypeLinear fromUnits, EnumUnitTypeLinear toUnits)
{
	static float conversionMatrix[NUM_UNIT_TYPE_LINEARS][NUM_UNIT_TYPE_LINEARS] = 
	{	// MILLIMETERS	CENTIMETERS		METERS			INCHES			FEET			YARDS			
		{  1.0f,		0.1f,			0.001f,			0.0393700787f,	0.0032808399f,	0.0010936133f	},	//MILLIMETERS to ?
		{  10.0f,		1.0f,			0.01f,			0.393700787f,	0.032808399f,	0.010936133f	},	//CENTIMETERS to ?
		{  1000.0f,		100.0f,			1.0f,			39.3700787f,	3.2808399f,		1.0936133f		},	//METERS to ?
		{  25.4f,		2.54f,			0.0254f,		1.0f,			0.083333333f,	0.027777778f	},	//INCHES to ?
		{  304.8f,		30.48f,			0.348f,			12.0f,			1.0f,			0.333333333f	},	//FEET to ?
		{  914.4f,		91.44f,			0.9144f,		36.0f,			3.0f,			1.0f			}	//YARDS to ?
	};
	
	if (toUnits == NONE || fromUnits== NONE)
	{
		return 1.0f;
	}
	else 
	{
		return conversionMatrix[fromUnits][toUnits];
	}
}

//-----------------------------------------------------------------------------


} // namespace rage
