// /trackIdMgr.cpp

#include "cranimation/animtrack.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"

#include "trackIdMgr.h"

using namespace std;
using namespace rage;

//-----------------------------------------------------------------------------

trackIdMgr::trackIdMgr()
{
	//PopulateKnownIds();
}

//-----------------------------------------------------------------------------

trackIdMgr::~trackIdMgr()
{
	Reset(false);
}

//-----------------------------------------------------------------------------

bool trackIdMgr::LoadIdFile(const char* szFilePath)
{
	Reset(true);

	parTree* pTree = PARSER.LoadTree(szFilePath, "");
	if(!pTree)
	{
		Errorf("Failed to load track id file '%s'", szFilePath);
		return false;
	}

	parTreeNode* pRoot = pTree->GetRoot();
	
	for(parTreeNode::ChildNodeIterator cIt = pRoot->BeginChildren();
		cIt != pRoot->EndChildren();
		++cIt)
	{
		parTreeNode* pTrackNode = *cIt;
		
		parAttribute* pNodeExprAttr = pTrackNode->GetElement().FindAttribute("nodeExpr");
		if(!pNodeExprAttr)
		{
			Errorf("Invalid element in track id file '%s', required attribute 'nodeExpr' missing on track node.", szFilePath);
			delete pTree;
			return false;
		}

		parAttribute* pCompExprAttr = pTrackNode->GetElement().FindAttribute("compExpr");
		if(!pCompExprAttr)
		{
			Errorf("Invalud element in track id file '%s', required attribute 'compExpr' missing on track node.", szFilePath);
			delete pTree;
			return false;
		}
		
		parAttribute* pIdAttr = pTrackNode->GetElement().FindAttribute("id");
		if(!pIdAttr)
		{
			Errorf("Invalud elment in track id file '%s', required attribute 'id' missing on track node.", szFilePath);
			delete pTree;
			return false;
		}

		m_idTable.push_back(new trackIdRecord(pNodeExprAttr->GetStringValue(), pCompExprAttr->GetStringValue(), (unsigned int)atoi(pIdAttr->GetStringValue())));
	}

	delete pTree;
	return true;
}

//-----------------------------------------------------------------------------

bool trackIdMgr::Lookup(const char* szNodeName, const char* szComponentName, unsigned int& retId) const
{
	for(ID_TABLE::const_iterator it = m_idTable.begin();
		it != m_idTable.end();
		++it)
	{
		trackIdRecord* pRec = (*it);

		if( CompareExprToName(pRec->m_nodeExpr.c_str(), szNodeName) &&
			CompareExprToName(pRec->m_compExpr.c_str(), szComponentName) )
		{
			retId = pRec->m_trackId;
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------

void trackIdMgr::Reset(bool bAddKnown)
{
	for(ID_TABLE::iterator it = m_idTable.begin();
		it != m_idTable.end();
		++it)
	{
		delete (*it);
	}

	m_idTable.clear();

	if(bAddKnown)
		PopulateKnownIds();
}

//-----------------------------------------------------------------------------

void trackIdMgr::PopulateKnownIds()
{
}

//-----------------------------------------------------------------------------

int trackIdMgr::CompareExprToName(const char* szExpr, const char* name) const
{
	const char *cp = NULL, *mp = NULL;

	while ((*name) && (*szExpr != '*')) 
	{
		if ((*szExpr != *name) && (*szExpr != '?')) 
		{
		return 0;
		}
		szExpr++;
		name++;
	}

	while (*name) 
	{
		if (*szExpr == '*') 
		{
		if (!*++szExpr) 
		{
			return 1;
		}
		mp = szExpr;
		cp = name+1;
		} 
		else if ((*szExpr == *name) || (*szExpr == '?')) 
		{
		szExpr++;
		name++;
		} 
		else 
		{
		szExpr = mp;
		name = cp++;
		}
	}

	while (*szExpr == '*') 
	{
		szExpr++;
	}
	return !*szExpr;
}

//-----------------------------------------------------------------------------

