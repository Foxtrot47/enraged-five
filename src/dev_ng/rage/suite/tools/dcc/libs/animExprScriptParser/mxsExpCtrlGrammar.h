// /mxsGrammar.h

#ifndef _MXS_EXPRCTRL_GRAMMAR_H_
#define _MXS_EXPRCTRL_GRAMMAR_H_

#pragma warning(disable : 4709)
#pragma warning(disable : 4503)

#pragma warning(push)
#pragma warning(disable : 4668)

#include "boost/spirit.hpp"
#include "boost/spirit/phoenix/binders.hpp"
#include "boost/lambda/lambda.hpp"
#include "boost/lambda/bind.hpp"

#pragma warning(pop)

#include <string>

#include "math/amath.h"
#include "exprParser.h"

using namespace std;
using namespace boost::spirit;
using namespace phoenix;
using namespace rage;

//-----------------------------------------------------------------------------

struct SimpleMxsExprCtrlGrammar : public grammar<SimpleMxsExprCtrlGrammar>
{
	SimpleMxsExprCtrlGrammar(ExprParser& parser)
		: m_parser(parser)
	{};

	SimpleMxsExprCtrlGrammar& operator=(const SimpleMxsExprCtrlGrammar& other) 
	{
		other.m_parser = m_parser;
	};

	struct constant_closure :
		boost::spirit::closure<constant_closure, double>
	{
		member1 value;
	};

	struct string_closure : 
		boost::spirit::closure<string_closure, std::string>
	{
		member1 name;
	};

	struct assignment_closure :
		boost::spirit::closure<assignment_closure, std::string>
	{
		member1 lhs;
	};

	struct function_closure :
		boost::spirit::closure<function_closure, unsigned int>
	{
		member1 fn;
	};

	template <typename ScannerT>
	struct definition
	{
		definition(SimpleMxsExprCtrlGrammar const& self)
		{
			function =
				  ( unaryCmd 
						>> ch_p('(') 
						>> expression 
						>> ch_p(')')  [bind(&SimpleMxsExprCtrlGrammar::CreateUnaryOp)(self, function.fn)] )

				| ( binaryCmd 
						>> ch_p('(') 
						>> expression >> ch_p(',') 
						>> expression 
						>> ch_p(')') [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, function.fn)] )

				| ( ternaryCmd 
						>> ch_p('(') 
						>> conditional >> ch_p(',') 
						>> expression >> ch_p(',') 
						>> expression 
						>> ch_p(')') [bind(&SimpleMxsExprCtrlGrammar::CreateTernaryOp)(self, function.fn)])
				;

			//Note MAX trig fn's accept degree values as inputs, so use the degree input form
			//of all the trig functions
			unaryCmd =
				  (as_lower_d["abs"][function.fn = ExprNodeUnaryOperator::kAbs])
				| (as_lower_d["acos"][function.fn = ExprNodeUnaryOperator::kAcosD])			
				| (as_lower_d["asin"][function.fn = ExprNodeUnaryOperator::kAsinD])			
				| (as_lower_d["atan"][function.fn = ExprNodeUnaryOperator::kAtanD])
				| (as_lower_d["ceil"][function.fn = ExprNodeUnaryOperator::kCeil])
				| (as_lower_d["cosh"][function.fn = ExprNodeUnaryOperator::kCosH])
				| (as_lower_d["cos"][function.fn = ExprNodeUnaryOperator::kCosD])
				| (as_lower_d["degtorad"][function.fn = ExprNodeUnaryOperator::kDToR])
				| (as_lower_d["exp"][function.fn = ExprNodeUnaryOperator::kExp])
				| (as_lower_d["floor"][function.fn = ExprNodeUnaryOperator::kFloor])
				| (as_lower_d["ln"][function.fn = ExprNodeUnaryOperator::kLn])
				| (as_lower_d["log"][function.fn = ExprNodeUnaryOperator::kLog])
				| (as_lower_d["radtodeg"][function.fn = ExprNodeUnaryOperator::kRToD])
				| (as_lower_d["sinh"][function.fn = ExprNodeUnaryOperator::kSinH])
				| (as_lower_d["sin"][function.fn = ExprNodeUnaryOperator::kSinD])
				| (as_lower_d["sqrt"][function.fn = ExprNodeUnaryOperator::kSqrt])
				| (as_lower_d["tan"][function.fn = ExprNodeUnaryOperator::kTanD])
				| (as_lower_d["tanh"][function.fn = ExprNodeUnaryOperator::kTanH])
				;

			binaryCmd = 
				  (as_lower_d["max"][function.fn = ExprNodeBinaryOperator::kMax])
				| (as_lower_d["min"][function.fn = ExprNodeBinaryOperator::kMin])
				| (as_lower_d["mod"][function.fn = ExprNodeBinaryOperator::kMod])
				| (as_lower_d["pow"][function.fn = ExprNodeBinaryOperator::kPow])
				;

			ternaryCmd = 
				(as_lower_d["if"][function.fn = ExprNodeTernaryOperator::kIfThenElse])
				;

			identifier
				= lexeme_d
				[
					( alpha_p | '_' ) >> *( alnum_p | '_' )
				][identifier.name = construct_<std::string>(arg1, arg2)]
				;
	
			group
				= '('
				>> expression
				>> ')'
				;

			assignment
				= identifier[assignment.lhs = arg1]
					>> '='
					>> expression
					>> (end_p | eol_p) [bind(&SimpleMxsExprCtrlGrammar::CreateAssignment)(self, assignment.lhs)]
			;

			statement
				= (  assignment | expression ) >> ( end_p )
				;

			constant
				=  as_lower_d["pi"][bind(&SimpleMxsExprCtrlGrammar::CreateConstant)(self, PI)]
				|	longest_d 
					[ 
						int_p [constant.value = arg1]
						| real_p [constant.value = arg1]
					]
					[bind(&SimpleMxsExprCtrlGrammar::CreateConstant)(self, constant.value)]
				;

			factor
				= group
				| constant 
				| function
				| ( ch_p('-') >> factor )[bind(&SimpleMxsExprCtrlGrammar::CreateUnaryOp)(self, ExprNodeUnaryOperator::kNegate)]
				| identifier [bind(&SimpleMxsExprCtrlGrammar::CreateNameAttrPair)(self, arg1, "")]
				;

			term
				= factor
				>> *( ('*' >> factor [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kMultiply)])
					| ('/' >> factor [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kDivide)])
					)
				;

			expression
				= term
				>> *( ('+' >> term [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kAdd)] )
					| ('-' >> term [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kSubtract)])
					)
				;

			conditional
				= ( expression
					>> *( ('>' >> expression [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kGreaterThan)] )
						| (str_p(">=") >> expression [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kGreaterThanEqualTo)] )
						| ('<' >> expression [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kLessThan )] )
						| (str_p("<=") >> expression [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kLessThanEqualTo)] )
						| ('=' >> expression [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kEqualTo)] )
						| (str_p("!=") >> expression [bind(&SimpleMxsExprCtrlGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kNotEqualTo)] )
						) ) 
					| '(' >> conditional >> ')' 		
				;
		}

		rule<ScannerT> const& start() const { return statement; }

		rule<ScannerT>										statement, group, factor, term, expression, unaryCmd, binaryCmd, ternaryCmd, conditional;
		rule<ScannerT, function_closure::context_t>			function;
		rule<ScannerT, assignment_closure::context_t>		assignment;
		rule<ScannerT, string_closure::context_t>			identifier;
		rule<ScannerT, constant_closure::context_t>			constant;
	};

	//Aggregate the ExprParser create methods, since the actions from the grammar need to be const methods
	void CreateAssignment(const string& lhs) const { m_parser.CreateAssignment(lhs); }
	void CreateNameAttrPair(const string& name, const string& attr) const { m_parser.CreateNameAttrPair(name, attr); }
	void CreateConstant(double value) const { m_parser.CreateConstant(value); }
	

	void CreateUnaryOp(unsigned int type) const { m_parser.CreateUnaryOperator((ExprNodeUnaryOperator::eUnaryOpType)type); }
	void CreateBinaryOp(unsigned int type) const { m_parser.CreateBinaryOperator((ExprNodeBinaryOperator::eBinaryOpType)type); }
	void CreateTernaryOp(unsigned int type) const { m_parser.CreateTernaryOperator((ExprNodeTernaryOperator::eTernaryOpType)type); }

	ExprParser&	m_parser;
};




#endif // _MXS_GRAMMAR_H_

