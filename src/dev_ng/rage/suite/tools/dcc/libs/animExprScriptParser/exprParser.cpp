// /exprParser.cpp

#include "animExprData/exprTree.h"

#include "exprParser.h"
#include "melGrammar.h"
#include "mxsGrammar.h"
#include "mxsExpCtrlGrammar.h"

//-----------------------------------------------------------------------------

extern __THREAD int RAGE_LOG_DISABLE;


//-----------------------------------------------------------------------------

//This is necessary since with run time exception handling disaled, without it
//we'll get an unresolbed external on the throw_exception function
void boost::throw_exception(const std::exception& e)
{
	Errorf("Exception : %s\n", e.what());
}

//-----------------------------------------------------------------------------

void ExprParser::CreateUnaryOperator(ExprNodeUnaryOperator::eUnaryOpType type)
{
#if DEBUG_EXPRESSION_STACK
	Printf("CreateUnaryOperator : %d\n", (unsigned int)type);
#endif //DEBUG_EXPRESSION_STACK

	ExprNodeUnaryOperator* pNode = new ExprNodeUnaryOperator(type);
	m_pNodeStack.push(pNode);
}

//-----------------------------------------------------------------------------

void ExprParser::CreateBinaryOperator(ExprNodeBinaryOperator::eBinaryOpType type)
{
#if DEBUG_EXPRESSION_STACK
	Printf("CreateBinaryOperator : %d\n", (unsigned int)type);
#endif

	ExprNodeBinaryOperator* pNode = new ExprNodeBinaryOperator(type);
	m_pNodeStack.push(pNode);
}

//-----------------------------------------------------------------------------

void ExprParser::CreateTernaryOperator(ExprNodeTernaryOperator::eTernaryOpType type)
{
#if DEBUG_EXPRESSION_STACK
	Printf("CreateTernaryOperator : %d\n", (unsigned int)type);
#endif //DEBUG_EXPRESSION_STACK

	ExprNodeTernaryOperator* pNode = new ExprNodeTernaryOperator(type);
	m_pNodeStack.push(pNode);
}

//-----------------------------------------------------------------------------

void ExprParser::CreateAssignment(const string& lhs)
{
	CreateNameAttrPair(lhs);

#if DEBUG_EXPRESSION_STACK
	Printf("CreateAssignment\n");
#endif //DEBUG_EXPRESSION_STACK

	ExprNodeBinaryOperator* pNode = new ExprNodeBinaryOperator(ExprNodeBinaryOperator::kAssign);
	m_pNodeStack.push(pNode);
}

//-----------------------------------------------------------------------------

void ExprParser::CreateNameAttrPair(const string& name, const string& attr)
{
#if DEBUG_EXPRESSION_STACK
	Printf("CreateNameAttrPair, name='%s' attr='%s'\n", name.c_str(), attr.c_str());
#endif //DEBUG_EXPRESSION_STACK

	ExprNodeNameAttrPair* pNode = new ExprNodeNameAttrPair(name.c_str(), attr.c_str());
	m_pNodeStack.push(pNode);
}

//-----------------------------------------------------------------------------

void ExprParser::CreateNameAttrPair(const string& nameAttr)
{
#if DEBUG_EXPRESSION_STACK
	Printf("CreateNameAttrPair, nameAttr='%s'\n", nameAttr.c_str());
#endif //DEBUG_EXPRESSION_STACK

	size_t dotPos = nameAttr.find_last_of(".");
	
	if(dotPos != string::npos)
	{
		string name = nameAttr.substr(0, dotPos);
		string attr = nameAttr.substr(dotPos+1);

		ExprNodeNameAttrPair* pNode = new ExprNodeNameAttrPair(name.c_str(), attr.c_str());
		m_pNodeStack.push(pNode);
	}
	else
	{
		ExprNodeNameAttrPair* pNode = new ExprNodeNameAttrPair(nameAttr.c_str(), "");
		m_pNodeStack.push(pNode);
	}
}

//-----------------------------------------------------------------------------

void ExprParser::CreateTimeOperand(ExprNodeTime::eTimeUnits units)
{
#if DEBUG_EXPRESSION_STACK
	Printf("CreateTimeOperand, units='%d'\n", (unsigned int)units);
#endif //DEBUG_EXPRESSION_STACK

	ExprNodeTime* pNode = new ExprNodeTime( units );
	m_pNodeStack.push(pNode);
}

//-----------------------------------------------------------------------------

void ExprParser::CreateConstant(double value)
{
#if DEBUG_EXPRESSION_STACK
	Printf("CreateConstant, value='%f'\n", value);
#endif //DEBUG_EXPRESSION_STACK
	
	ExprNodeConstant* pNode = new ExprNodeConstant( (float)value);
	m_pNodeStack.push(pNode);
}

//-----------------------------------------------------------------------------

ExprNodeBase* ExprParser::CreateExpressionTreeInternal(ExprTree* pTree, ExprNodeBase* pNode, Language lang)
{
	ExprNodeBase* pTop = m_pNodeStack.top();
	m_pNodeStack.pop();

	pTop->SetParent(pNode);

	if(pTop->GetSuperType() == ExprNodeBase::kTernary)
	{
		ExprNodeTernaryOperator* pTernaryOpNode = static_cast<ExprNodeTernaryOperator*>(pTop);
		pTernaryOpNode->m_pRight = CreateExpressionTreeInternal(pTree, pTernaryOpNode, lang);
		pTernaryOpNode->m_pMiddle = CreateExpressionTreeInternal(pTree, pTernaryOpNode, lang);
		pTernaryOpNode->m_pLeft = CreateExpressionTreeInternal(pTree, pTernaryOpNode, lang);
		return pTernaryOpNode;
	}
	if(pTop->GetSuperType() == ExprNodeBase::kBinary)
	{
		ExprNodeBinaryOperator* pBinOpNode = static_cast<ExprNodeBinaryOperator*>(pTop);

		if( pBinOpNode->GetOperatorType() == ExprNodeBinaryOperator::kAssign)
		{
			pBinOpNode->m_pLeft = CreateExpressionTreeInternal(pTree, pBinOpNode, lang);
			pBinOpNode->m_pRight = CreateExpressionTreeInternal(pTree, pBinOpNode, lang);
		}
		else
		{
			pBinOpNode->m_pRight = CreateExpressionTreeInternal(pTree, pBinOpNode, lang);
			pBinOpNode->m_pLeft = CreateExpressionTreeInternal(pTree, pBinOpNode, lang);
		}
		return pBinOpNode;
	}
	else if(pTop->GetSuperType() == ExprNodeBase::kUnary)
	{
		ExprNodeUnaryOperator* pUnaryOpNode = static_cast<ExprNodeUnaryOperator*>(pTop);
		pUnaryOpNode->m_pChild = CreateExpressionTreeInternal(pTree, pUnaryOpNode, lang);
		return pUnaryOpNode;
	}
	else if(pTop->GetSuperType() == ExprNodeBase::kOperand)
	{
		ExprNodeOperand* pOperandNode = static_cast<ExprNodeOperand*>(pTop);
		return pOperandNode;
	}

	return NULL;
}

//-----------------------------------------------------------------------------

void ExprParser::ClearNodeStack()
{
	while(m_pNodeStack.size())
	{
		ExprNodeBase* pTop = m_pNodeStack.top();
		delete pTop;
		m_pNodeStack.pop();
	}
}

//-----------------------------------------------------------------------------

ExprTree* ExprParser::ParseMelExpression(const char* szExpression)
{
	++RAGE_LOG_DISABLE;
	
	SimpleMelGrammar	melGrammar(*this);
		
	parse_info<> info = parse(szExpression, melGrammar, space_p);

	--RAGE_LOG_DISABLE;

	if(info.full)
	{
		ExprTree* pNodeTree = rage_new ExprTree();
		pNodeTree->m_pRoot = CreateExpressionTreeInternal(pNodeTree, NULL, kMEL);
		Assertf( m_pNodeStack.size() == 0, "%d nodes remain on stack after expression tree creation.", m_pNodeStack.size() );
		return pNodeTree;
	}
	else
	{
		Errorf("MEL Expression parsing failed. Stopped at: '%s'", info.stop);
		ClearNodeStack();
		return NULL;
	}
}

//-----------------------------------------------------------------------------

ExprTree* ExprParser::ParseMaxScriptExpression(const char* szExpression)
{
	++RAGE_LOG_DISABLE;

	SimpleMxsGrammar	mxsGrammar(*this);

	parse_info<> info = parse(szExpression, mxsGrammar, space_p);

	--RAGE_LOG_DISABLE;

	if(info.full)
	{
		ExprTree* pNodeTree = rage_new ExprTree();
		pNodeTree->m_pRoot = CreateExpressionTreeInternal(pNodeTree, NULL, kMaxScript);
		Assertf( m_pNodeStack.size() == 0, "%d nodes remain on stack after expression tree creation.", m_pNodeStack.size() );
		return pNodeTree;
	}
	else
	{
		char error[512] = {0};
		memcpy_s(error, 512, info.stop, 400);
		if(strlen(info.stop)>400)
			strcat_s(error, "...");
		Errorf("MAXScript Expression parsing failed. Stopped at: '%s'", error);
		ClearNodeStack();
		return NULL;
	}
}

//-----------------------------------------------------------------------------

ExprTree* ExprParser::ParseMaxScriptExprCtrlExpression(const char* szExpression)
{
	++RAGE_LOG_DISABLE;

	SimpleMxsExprCtrlGrammar	mxsGrammar(*this);

	parse_info<> info = parse(szExpression, mxsGrammar, space_p);

	--RAGE_LOG_DISABLE;

	if(info.full)
	{
		ExprTree* pNodeTree = rage_new ExprTree();
		pNodeTree->m_pRoot = CreateExpressionTreeInternal(pNodeTree, NULL, kMaxScript);
		Assertf( m_pNodeStack.size() == 0, "%d nodes remain on stack after expression tree creation.", m_pNodeStack.size() );

		if(m_pNodeStack.size() > 0) // corrupt node stack, we should end with 0 nodes. this can be caused by a bad if statement.
		{
			return NULL;
		}

		return pNodeTree;
	}
	else
	{
		char error[512] = {0};
		memcpy_s(error, 512, info.stop, 400);
		if(strlen(info.stop)>400)
			strcat_s(error, "...");
		Errorf("MAXScript Expression Control Expression parsing failed. Stopped at: '%s'", error);
		ClearNodeStack();
		return NULL;
	}
}

//-----------------------------------------------------------------------------

