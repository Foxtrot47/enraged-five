// /mxsGrammar.h

#ifndef _MXS_GRAMMAR_H_
#define _MXS_GRAMMAR_H_

#pragma warning(disable : 4709)
#pragma warning(disable : 4503)

#pragma warning(push)
#pragma warning(disable : 4668)

#include "boost/spirit.hpp"
#include "boost/spirit/phoenix/binders.hpp"
#include "boost/lambda/lambda.hpp"
#include "boost/lambda/bind.hpp"

#pragma warning(pop)

#include "math/amath.h"
#include <string>

#include "exprParser.h"

using namespace std;
using namespace boost::spirit;
using namespace phoenix;
using namespace rage;

//-----------------------------------------------------------------------------

struct SimpleMxsGrammar : public grammar<SimpleMxsGrammar>
{
	SimpleMxsGrammar(ExprParser& parser)
		: m_parser(parser)
	{};

	SimpleMxsGrammar& operator=(const SimpleMxsGrammar& other) 
	{
		other.m_parser = m_parser;
	};

	struct constant_closure :
		boost::spirit::closure<constant_closure, double>
	{
		member1 value;
	};

	struct string_closure : 
		boost::spirit::closure<string_closure, std::string>
	{
		member1 name;
	};

	struct assignment_closure :
		boost::spirit::closure<assignment_closure, std::string>
	{
		member1 lhs;
	};

	struct function_closure :
		boost::spirit::closure<function_closure, unsigned int>
	{
		member1 fn;
	};

	template <typename ScannerT>
	struct definition
	{
		definition(SimpleMxsGrammar const& self)
		{
			function =
				  ( unaryCmd 
						>> ch_p('(') 
						>> ( ifexpr | expression )
						>> ch_p(')')  [bind(&SimpleMxsGrammar::CreateUnaryOp)(self, function.fn)] )

				| ( binaryCmd 
						>> ( ifexpr | expression )
						>> ( ifexpr | expression ) [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, function.fn)] )
				;

			//Note MAX trig fn's accept degree values as inputs, so use the degree input form
			//of all the trig functions
			unaryCmd =
				  (as_lower_d["abs"][function.fn = ExprNodeUnaryOperator::kAbs])
				| (as_lower_d["acos"][function.fn = ExprNodeUnaryOperator::kAcosD])
				| (as_lower_d["asin"][function.fn = ExprNodeUnaryOperator::kAsinD])
				| (as_lower_d["atan"][function.fn = ExprNodeUnaryOperator::kAtanD])
				| (as_lower_d["ceil"][function.fn = ExprNodeUnaryOperator::kCeil])
				| (as_lower_d["cosh"][function.fn = ExprNodeUnaryOperator::kCosH])
				| (as_lower_d["cos"][function.fn = ExprNodeUnaryOperator::kCosD])
				| (as_lower_d["degtorad"][function.fn = ExprNodeUnaryOperator::kDToR])
				| (as_lower_d["exp"][function.fn = ExprNodeUnaryOperator::kExp])
				| (as_lower_d["floor"][function.fn = ExprNodeUnaryOperator::kFloor])
				| (as_lower_d["ln"][function.fn = ExprNodeUnaryOperator::kLn])
				| (as_lower_d["log"][function.fn = ExprNodeUnaryOperator::kLog])
				| (as_lower_d["radtodeg"][function.fn = ExprNodeUnaryOperator::kRToD])
				| (as_lower_d["sinh"][function.fn = ExprNodeUnaryOperator::kSinH])
				| (as_lower_d["sin"][function.fn = ExprNodeUnaryOperator::kSinD])
				| (as_lower_d["sqrt"][function.fn = ExprNodeUnaryOperator::kSqrt])
				| (as_lower_d["tan"][function.fn = ExprNodeUnaryOperator::kTanD])
				| (as_lower_d["tanh"][function.fn = ExprNodeUnaryOperator::kTanH])
				;

			binaryCmd = 
				  (as_lower_d["mod"][function.fn = ExprNodeBinaryOperator::kMod])
				| (as_lower_d["pow"][function.fn = ExprNodeBinaryOperator::kPow])
				;
	
			//The bit of ugly looking code in the next three parser rules is intended to try and 
			//match direct references to controller values inside the expressions.  The basic
			//idea is to break down expression to a basic name attribute pair referencing 
			//the correct channel(s).  E.g. 
			// "$Jaw_Circle.rotation.controller[#X_Rotation]" becomes "Jaw_Circle.rotateX"
			ctrlReference
				= ( ch_p('$')
					>> identifier [ ctrlReference.name = arg1 ]
					>> ch_p('.')
					>> ( posCtrlRef [ ctrlReference.name += arg1 ]
						|rotCtrlRef [ ctrlReference.name += arg1 ]
					   )
					)
				;

			posCtrlRef
				= ( ( str_p("position") | str_p("pos") )
					>> ( (str_p(".controller") 
						  >> ch_p('[')
						  >> (  (as_lower_d["#x_position"][posCtrlRef.name = ".translateX"])
					  		  | (as_lower_d["#y_position"][posCtrlRef.name = ".translateY"])
					 		  | (as_lower_d["#z_position"][posCtrlRef.name = ".translateZ"])
							 )
						  >> ch_p(']')
						  >> *( ch_p('.')
								>> *(alnum_p | '[' | ']')
								>> str_p(".value")
							   )
						  )
						  | eps_p[posCtrlRef.name = ".translate"] 
					   )
				   )
				;

			rotCtrlRef
				= ( str_p("rotation")
					>> ( ( str_p(".controller")
							>> ch_p('[')
							>> (  (as_lower_d["#x_rotation"][rotCtrlRef.name = ".rotateX"])
							    | (as_lower_d["#y_rotation"][rotCtrlRef.name = ".rotateY"])
							    | (as_lower_d["#z_rotation"][rotCtrlRef.name = ".rotateZ"])
							   )
							>> ch_p(']')
							>> *( ch_p('.')
								  >> *(alnum_p | '[' | ']')
								  >> str_p(".value")
								 )
							)
							| eps_p[rotCtrlRef.name = ".rotate"]
						)
					)
				;

			identifier
				= lexeme_d
				[
					( alpha_p | '_' ) >> *( alnum_p | '_' )
				][identifier.name = construct_<std::string>(arg1, arg2)]
				;

			group
				= '('
				>> expression
				>> ')'
				;

			assignment
				=  identifier[assignment.lhs = arg1]
					>> '='
					>> expression
					>> (end_p | eol_p) [bind(&SimpleMxsGrammar::CreateAssignment)(self, assignment.lhs)]
			;

			ifexpr 
				= str_p("if")
				>> *(ch_p('(')) >> conditional >> *(ch_p(')')) >> str_p("then")
				>> expression
				>> ( (str_p("else") >> ( ifexpr | expression ))[bind(&SimpleMxsGrammar::CreateTernaryOp)(self,ExprNodeTernaryOperator::kIfThenElse)]
					//This catches the case of a dangling expression that isn't grouped with a final else clause in the script
					| (expression)[bind(&SimpleMxsGrammar::CreateTernaryOp)(self,ExprNodeTernaryOperator::kIfThenElse)]
					//This catches the case where there is no else clause
					| (end_p | eol_p)[bind(&SimpleMxsGrammar::CreateBinaryOp)(self,ExprNodeBinaryOperator::kIfThen)]
				   )
			;

			statement
				= (  assignment | ifexpr | expression ) >> ( end_p )
				;

			constant
				= as_lower_d["pi"][bind(&SimpleMxsGrammar::CreateConstant)(self, PI)]
				|	longest_d 
					[ 
						int_p [constant.value = arg1]
						| real_p [constant.value = arg1]
					]
					[bind(&SimpleMxsGrammar::CreateConstant)(self, constant.value)]
				;

			factor
				= constant 
				| group
				| function
				| ( ch_p('-') >> factor )[bind(&SimpleMxsGrammar::CreateUnaryOp)(self, ExprNodeUnaryOperator::kNegate)]
				| ctrlReference [bind(&SimpleMxsGrammar::CreateNameAttrPairFromCombined)(self, arg1)]
				| identifier [bind(&SimpleMxsGrammar::CreateNameAttrPairFromSplit)(self, arg1, "")]
				;

			term
				= factor
				>> *( ('*' >> factor [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kMultiply)])
					| ('/' >> factor [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kDivide)])
					)
				;

			expression
				= *(str_p("return"))
				>> term
				>> *( ('+' >> term [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kAdd)] )
					| ('-' >> term [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kSubtract)])
					)
				;

			conditional
				= expression
				>> *( ('>' >> expression [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kGreaterThan)] )
					| (str_p(">=") >> expression [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kGreaterThanEqualTo)] )
					| ('<' >> expression [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kLessThan )] )
					| (str_p("<=") >> expression [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kLessThanEqualTo)] )
					| ('=' >> expression [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kEqualTo)] )
					| (str_p("!=") >> expression [bind(&SimpleMxsGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kNotEqualTo)] )
					)
				;
		}

		rule<ScannerT> const& start() const { return statement; }

		rule<ScannerT>										statement, group, factor, term, expression, unaryCmd, binaryCmd, conditional, ifexpr;
		rule<ScannerT, function_closure::context_t>			function;
		rule<ScannerT, assignment_closure::context_t>		assignment;
		rule<ScannerT, string_closure::context_t>			identifier, ctrlReference, posCtrlRef, rotCtrlRef;
		rule<ScannerT, constant_closure::context_t>			constant;
	};

	//Aggregate the ExprParser create methods, since the actions from the grammar need to be const methods
	void CreateAssignment(const string& lhs) const { m_parser.CreateAssignment(lhs); }
	void CreateNameAttrPairFromCombined(const string& nameAttr) const { m_parser.CreateNameAttrPair(nameAttr); }
	void CreateNameAttrPairFromSplit(const string& name, const string& attr) const { m_parser.CreateNameAttrPair(name, attr); }
	void CreateConstant(double value) const { m_parser.CreateConstant(value); }
	

	void CreateUnaryOp(unsigned int type) const { m_parser.CreateUnaryOperator((ExprNodeUnaryOperator::eUnaryOpType)type); }
	void CreateBinaryOp(unsigned int type) const { m_parser.CreateBinaryOperator((ExprNodeBinaryOperator::eBinaryOpType)type); }
	void CreateTernaryOp(unsigned int type) const { m_parser.CreateTernaryOperator((ExprNodeTernaryOperator::eTernaryOpType)type); }

	ExprParser&	m_parser;
};




#endif // _MXS_GRAMMAR_H_

