// /melGrammar.h

#ifndef _MEL_GRAMMAR_H_
#define _MEL_GRAMMAR_H_

#pragma warning(disable : 4709)
#pragma warning(disable : 4503)

#pragma warning(push)
#pragma warning(disable : 4668)

#include "boost/spirit.hpp"
#include "boost/spirit/phoenix/binders.hpp"
#include "boost/lambda/lambda.hpp"
#include "boost/lambda/bind.hpp"

#pragma warning(pop)

#include <string>

#include "exprParser.h"

using namespace std;
using namespace boost::spirit;
using namespace phoenix;
using namespace rage;

//-----------------------------------------------------------------------------

struct SimpleMelGrammar : public grammar<SimpleMelGrammar>
{
	SimpleMelGrammar(ExprParser& parser)
		: m_parser(parser)
	{};

	SimpleMelGrammar& operator=(const SimpleMelGrammar& other) 
	{
		other.m_parser = m_parser;
	};

	struct constant_closure :
		boost::spirit::closure<constant_closure, double>
	{
		member1 value;
	};

	struct string_closure : 
		boost::spirit::closure<string_closure, std::string>
	{
		member1 name;
	};

	struct function_closure :
		boost::spirit::closure<function_closure, unsigned int>
	{
		member1 fn;
	};

	struct assignment_closure:
		boost::spirit::closure<assignment_closure, std::string>
	{
		member1 lhs;
	};

	template <typename ScannerT>
	struct definition
	{
		definition(SimpleMelGrammar const& self)
		{
			command = 
				( ternaryCmd
					>> (group | factor)
					>> (group | factor)
					>> (group | factor)[bind(&SimpleMelGrammar::CreateTernaryOp)(self, command.fn)]
				);

			ternaryCmd =
				(as_lower_d["clamp"][command.fn = ExprNodeTernaryOperator::kClamp])
				;

			function =
				( unaryFn 
						>> ch_p('(') 
						>> expression 
						>> ch_p(')')  [bind(&SimpleMelGrammar::CreateUnaryOp)(self, function.fn)] )

				| ( binaryFn 
						>> ch_p('(') 
						>> expression >> ch_p(',') 
						>> expression 
						>> ch_p(')') [bind(&SimpleMelGrammar::CreateBinaryOp)(self, function.fn)] )
				;

			unaryFn =
				  (as_lower_d["abs"][function.fn = ExprNodeUnaryOperator::kAbs])
				| (as_lower_d["acos"][function.fn = ExprNodeUnaryOperator::kAcos])
				| (as_lower_d["asin"][function.fn = ExprNodeUnaryOperator::kAsin])
				| (as_lower_d["atan"][function.fn = ExprNodeUnaryOperator::kATan])
				| (as_lower_d["ceil"][function.fn = ExprNodeUnaryOperator::kCeil])
				| (as_lower_d["cos"][function.fn = ExprNodeUnaryOperator::kCos])
				| (as_lower_d["deg_to_rad"][function.fn = ExprNodeUnaryOperator::kDToR])
				| (as_lower_d["exp"][function.fn = ExprNodeUnaryOperator::kExp])
				| (as_lower_d["floor"][function.fn = ExprNodeUnaryOperator::kFloor])
				| (as_lower_d["log10"][function.fn = ExprNodeUnaryOperator::kLn])
				| (as_lower_d["log"][function.fn = ExprNodeUnaryOperator::kLog])
				| (as_lower_d["rad_to_deg"][function.fn = ExprNodeUnaryOperator::kRToD])
				| (as_lower_d["sin"][function.fn = ExprNodeUnaryOperator::kSin])
				| (as_lower_d["sqrt"][function.fn = ExprNodeUnaryOperator::kSqrt])
				| (as_lower_d["tan"][function.fn = ExprNodeUnaryOperator::kTan])
				| (as_lower_d["cosd"][function.fn = ExprNodeUnaryOperator::kCosD])
				| (as_lower_d["sind"][function.fn = ExprNodeUnaryOperator::kSinD])
				| (as_lower_d["tand"][function.fn = ExprNodeUnaryOperator::kTanD])
				| (as_lower_d["acosd"][function.fn = ExprNodeUnaryOperator::kAcosD])
				| (as_lower_d["asind"][function.fn = ExprNodeUnaryOperator::kAsinD])
				| (as_lower_d["atand"][function.fn = ExprNodeUnaryOperator::kAtanD])
				;

			binaryFn = 
				  (as_lower_d["max"][function.fn = ExprNodeBinaryOperator::kMax])
				| (as_lower_d["min"][function.fn = ExprNodeBinaryOperator::kMin])
				| (as_lower_d["pow"][function.fn = ExprNodeBinaryOperator::kPow])
				;

			nameAttrPair
				= (identifier
					>> '.' 
					>> identifier)
				[nameAttrPair.name = construct_<std::string>(arg1, arg2)]
				;

			identifier
				= lexeme_d
				[
					( alpha_p | '_' | '|' ) >> *( alnum_p | '_' | '|' )
				][identifier.name = construct_<std::string>(arg1, arg2)]
				;

			assignment
				= nameAttrPair[assignment.lhs = arg1]
					>> '='
					>> (*(ch_p('`')))
					>> expression
					>> (*(ch_p('`')))
					>> (*(ch_p(';')))[bind(&SimpleMelGrammar::CreateAssignment)(self, assignment.lhs)]
			;

			ifexpr 
				= str_p("if")
				>> ch_p('(') >> conditional >> ch_p(')')
				>> code_block
				>> ( (str_p("else") >> ( ifexpr | code_block ))[bind(&SimpleMelGrammar::CreateTernaryOp)(self,ExprNodeTernaryOperator::kIfThenElse)]
					|
					(end_p | eol_p)[bind(&SimpleMelGrammar::CreateBinaryOp)(self,ExprNodeBinaryOperator::kIfThen)]
				   )
			;

			statement
				= (  assignment | ifexpr ) >> ( end_p | eol_p | ';' )
				;

			code_block
				= '{'
				>> (assignment | ifexpr)
				>> '}'
				;

			group
				= '('
				>> expression
				>> ')'
				;

			constant
				= longest_d 
				[ 
					int_p [constant.value = arg1]
					| real_p [constant.value = arg1]
				]
				[bind(&SimpleMelGrammar::CreateConstant)(self, constant.value)]
				;
			
			keyword
				=  (as_lower_d["time"][bind(&SimpleMelGrammar::CreateTimeOperand)(self, ExprNodeTime::kSeconds)])
				;

			factor
				= constant
				| keyword
				| group
				| command
				| function
				| ( ch_p('-') >> factor )[bind(&SimpleMelGrammar::CreateUnaryOp)(self, ExprNodeUnaryOperator::kNegate)]
				| nameAttrPair [bind(&SimpleMelGrammar::CreateNameAttrPair)(self, arg1)]
				;

			term
				= factor
				>> *( ('*' >> factor [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kMultiply)])
					| ('/' >> factor [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kDivide)])
					)
				;

			expression
				= term
				>> *( ('+' >> term [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kAdd)] )
					| ('-' >> term [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kSubtract)])
					)
				;

			conditional
				= expression
				>> *( ('>' >> expression [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kGreaterThan)] )
					| (str_p(">=") >> expression [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kGreaterThanEqualTo)] )
					| ('<' >> expression [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kLessThan )] )
					| (str_p("<=") >> expression [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kLessThanEqualTo)] )
					| (str_p("==") >> expression [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kEqualTo)] )
					| (str_p("!=") >> expression [bind(&SimpleMelGrammar::CreateBinaryOp)(self, ExprNodeBinaryOperator::kNotEqualTo)] )
					)
				;
		}

		rule<ScannerT> const& start() const { return statement; }

		rule<ScannerT>										statement, expression, term, factor, conditional, keyword, group, code_block, ifexpr, unaryFn, binaryFn, ternaryCmd;
		rule<ScannerT, assignment_closure::context_t>		assignment;
		rule<ScannerT, constant_closure::context_t>			constant;
		rule<ScannerT, string_closure::context_t>			nameAttrPair;
		rule<ScannerT, string_closure::context_t>			identifier;
		rule<ScannerT, function_closure::context_t>			function;
		rule<ScannerT, function_closure::context_t>			command;
	};

	//Aggregate the ExprParser create methods, since the actions from the grammar need to be const methods
	void CreateNameAttrPair(const string& nameAttr) const { m_parser.CreateNameAttrPair(nameAttr); }
	void CreateTimeOperand(unsigned int units) const { m_parser.CreateTimeOperand((ExprNodeTime::eTimeUnits)units); }
	
	void CreateAssignment(const string& lhs) const { m_parser.CreateAssignment(lhs); }
	void CreateConstant(double value) const { m_parser.CreateConstant(value); }
	
	void CreateUnaryOp(unsigned int type) const { m_parser.CreateUnaryOperator((ExprNodeUnaryOperator::eUnaryOpType)type); }
	void CreateBinaryOp(unsigned int type) const { m_parser.CreateBinaryOperator((ExprNodeBinaryOperator::eBinaryOpType)type); }
	void CreateTernaryOp(unsigned int type) const { m_parser.CreateTernaryOperator((ExprNodeTernaryOperator::eTernaryOpType)type); }

	ExprParser&	m_parser;
};

#endif // _MEL_GRAMMAR_H_

