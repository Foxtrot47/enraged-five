top: bottom

include ..\..\..\..\rage\build\Makefile.template

.PHONY: FORCE
TARGET = testAnimExprScriptParser
ELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.elf
SELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.self

bottom: $(SELF)

$(SELF): $(ELF)
	make_fself $(ELF) $(SELF)

LIBS += ..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib

..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\base\src\vcproj\RageCore -f RageCore.mk

LIBS += ..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib

..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\stlport\STLport-5.0RC5\src -f stlport.mk

LIBS += ..\animExprScriptParser/$(INTDIR)/animExprScriptParser.lib

..\animExprScriptParser/$(INTDIR)/animExprScriptParser.lib: FORCE
	$(MAKE) -C ..\animExprScriptParser -f animExprScriptParser.mk

LIBS += ..\..\..\..\rage\suite\src\animExprData/$(INTDIR)/animExprData.lib

..\..\..\..\rage\suite\src\animExprData/$(INTDIR)/animExprData.lib: FORCE
	$(MAKE) -C ..\..\..\..\rage\suite\src\animExprData -f animExprData.mk

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I ..\..\..\..\rage\base\src -I ..\..\..\..\rage\suite\src -I ..\..\..\..\rage\suite\tools -I ..\..\..\..\rage\3rdParty\boost_1_34_1

$(INTDIR)\testAnimExprScriptParser.obj: testAnimExprScriptParser.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(ELF): $(LIBS) $(INTDIR)\testAnimExprScriptParser.obj
	ppu-lv2-gcc -o $(ELF) $(INTDIR)\testAnimExprScriptParser.obj -Wl,--start-group $(LIBS) -Wl,--end-group $(LLIBS)
