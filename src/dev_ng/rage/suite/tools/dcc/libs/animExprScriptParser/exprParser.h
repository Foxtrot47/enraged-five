// /exprParser.h

#ifndef _EXPR_PARSER_H_
#define _EXPR_PARSER_H_

#include <string>
#include <stack>

#include "animExprData/exprTree.h"

using namespace std;
using namespace rage;

#define DEBUG_EXPRESSION_STACK 1

struct SimpleMelGrammar;
struct SimpleMxsGrammar;
struct SimpleMxsExprCtrlGrammar;

//-----------------------------------------------------------------------------

class ExprParser
{
public:
	friend struct SimpleMelGrammar;
	friend struct SimpleMxsGrammar;
	friend struct SimpleMxsExprCtrlGrammar;

	ExprParser() {};

	ExprTree*	ParseMelExpression(const char* szExpression);
	ExprTree*	ParseMaxScriptExpression(const char* szExpression);
	ExprTree*	ParseMaxScriptExprCtrlExpression(const char* szExpression);

	stack < ExprNodeBase* >& GetNodeStack() { return m_pNodeStack; }

private:
	void CreateNameAttrPair(const string& name, const string& attr);
	void CreateNameAttrPair(const string& nameAttr);
	void CreateTimeOperand(ExprNodeTime::eTimeUnits units);

	void CreateConstant(double value);
	void CreateAssignment(const string& lhs);

	void CreateUnaryOperator(ExprNodeUnaryOperator::eUnaryOpType type);
	void CreateBinaryOperator(ExprNodeBinaryOperator::eBinaryOpType type);
	void CreateTernaryOperator(ExprNodeTernaryOperator::eTernaryOpType type);
	
private:
	enum Language
	{
		kMEL,
		kMaxScript,
		kMaxScriptExprCtrl
	};

	void			ClearNodeStack();
	ExprNodeBase*	CreateExpressionTreeInternal(ExprTree* pTree, ExprNodeBase* pNode, Language lang);

	stack < ExprNodeBase* >		m_pNodeStack;
};

//-----------------------------------------------------------------------------

#endif //_EXPR_PARSER_H_

