// 
// /testAnimExprScriptParser.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "diag/output.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parser/tree.h"
#include "system/main.h"
#include "system/param.h"

#include "animExprScriptParser/exprParser.h"
#include "animExprScriptParser/melGrammar.h"
#include "animExprScriptParser/mxsGrammar.h"

using namespace std;
using namespace rage;

extern __THREAD int RAGE_LOG_DISABLE;

PARAM(outdir, "[testAnimExprScriptParser] Output directory where resulting xml files should be written");

//-----------------------------------------------------------------------------

int TestFile(const char* szFileName)
{
	fiStream* pTestFile = ASSET.Open(szFileName, "");
	if(!pTestFile)
	{
		Errorf("Failed to open test file '%s'", szFileName);
		return 1;
	}

	int fileSize = pTestFile->Size() + 1;

	char* fileBuffer = rage_new char[ fileSize ];
	memset(fileBuffer, 0, fileSize);

	pTestFile->Read(fileBuffer, fileSize);
	pTestFile->Close();

	int iRes = 0;

	ExprParser theParser;

	ExprTree* pResult = NULL;

	if(strcmpi(ASSET.FindExtensionInPath(szFileName), ".mel") == 0)
	{
		pResult = theParser.ParseMelExpression(fileBuffer);
	}
	else if(strcmpi(ASSET.FindExtensionInPath(szFileName), ".ms") == 0)
	{
		pResult = theParser.ParseMaxScriptExpression(fileBuffer);
	}
	else if(strcmpi(ASSET.FindExtensionInPath(szFileName), ".mec") == 0)
	{
		pResult = theParser.ParseMaxScriptExprCtrlExpression(fileBuffer);
	}
	else
	{
		delete [] fileBuffer;
		Errorf("Can't parse file of type '%s'.", ASSET.FindExtensionInPath(szFileName));
		return 1;
	}

	if(pResult)
	{
		Displayf("Test of file '%s' SUCCEEDED", szFileName);

		const char *szOutDir = NULL;
		if(PARAM_outdir.Get(szOutDir))
		{
			char baseNameBuffer[MAX_PATH];
			ASSET.BaseName(baseNameBuffer, MAX_PATH, szFileName);

			char pathBuffer[MAX_PATH];
			sprintf(pathBuffer, "%s\\expParse_%s.xml", szOutDir, baseNameBuffer);
			if(!PARSER.SaveObject(pathBuffer, "", pResult))
			{
				Errorf("Parser save failed on file : '%s'", pathBuffer);
				iRes = 1;
			}
		}
		delete pResult;
	}
	else
	{
		Displayf("Test of file '%s' FAILED", szFileName);
		iRes = 1;
	}

	delete [] fileBuffer;

	return iRes;
}

//-----------------------------------------------------------------------------

int Main()
{
	INIT_PARSER;
	
	ASSET.PushFolder("C:\\soft\\rage\\suite\\tools\\animExprScriptParser\\testScripts");
	
	/*TestFile("test_SimpleAssignment.mel");
	TestFile("test_SimpleAssign2.mel");
	TestFile("test_IfThen.mel");
	TestFile("test_IfThenElse.mel");
	TestFile("test_IfThenElseIf.mel");
	
	TestFile("test_Rdr2EyeExpr.mel");
	TestFile("test_singleQuoteExpr.mel");
	TestFile("test_rdr2CollarExpr_1.mel");*/

	TestFile("test_IfThenElse.ms");
	TestFile("test_IfThenElse2.ms");

	/*TestFile("test_BinaryFn.mec");
	TestFile("test_TernaryFn.mec");
	TestFile("test_complexExpression.mec");*/

	SHUTDOWN_PARSER;

	return 0;
}

//-----------------------------------------------------------------------------



