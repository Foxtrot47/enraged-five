//
// viewfrag\viewfrag.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//
// Suggested command lines:
// -file=$\fragments\piranha\entity.type
// -file=$\fragments\detectivedc\entity.type
// -file=$\fragments\hotdogred_rex\entity.type
// -file=$\fragments\env_sheet\env_sheet.type
// -file=$\rage_male_cloth\rage_male_cloth.type
//
// View GTA4 props:
// -zup -archive x:/gta/build/xbox360/data/maps/props/roadside/traffic.img -fragrsc -rscfile x:/gta/build/xbox360/data/maps/props/roadside/bm_nytraflite5a -shaderlib x:/gta/build/common/shaders -shaderdb x:/gta/build/common/shaders/db


#define ENABLE_RFS 1

#include "sample_physics/sample_physics.h"
#include "sample_fragment/fragworld.h"

#include "fragment/animation.h"
#include "fragment/cache.h"
#include "fragment/cachemanager.h"
#include "fragment/drawable.h"
#include "fragment/tune.h"
#include "fragment/typechild.h"

#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "math/random.h"
#include "paging/dictionary.h"
#include "pharticulated/articulatedcollider.h"
#include "phcore/phmath.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "rmptfx/ptxmanager.h"
#include "shaderlib\skinning_method_config.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"	 
#include "vectormath/vec3v.h"

#include "cloth/charactercloth.h"
#include "cloth/environmentcloth.h"
#include "pheffects/cloth_verlet.h"

#include "grmodel/geometry.h"

PARAM(file,		 "[viewfrag] Fragment type to load");
PARAM(rscfile,	 "[viewfrag] File to use to save resources (when using fragrsc)");
PARAM(tdfile,	 "[viewfrag] File to load texture dictionary from (when using fragrsc)");
PARAM(nmxmlfile, "[viewfrag] Natural Motion xml file to load from in addition to the type file");
PARAM(anim,      "[viewfrag] The animation to play on the character");
PARAM(fragrsc,   "[viewfrag] Build fragment type data into a resource");
PARAM(fragpage,  "[viewfrag] Use the pgRscBuilder system to load type data");
PARAM(regen,     "[viewfrag] When combined with fragrsc, rebuild resources even if they're good");
PARAM(noregen,   "[viewfrag] When combined with fragrsc, don't rebuild resources even if they're bad");

namespace rage {

EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);
EXT_PFD_DECLARE_ITEM(Models);

#if (__BANK && !__RESOURCECOMPILER)
datCallback	g_ClothManagerCallback = NullCB;
#endif 

}
namespace ragesamples {

using namespace rage;

static const u32 DOESNT_COLLIDE_WITH_TERRAIN = 1 << 1;

////////////////////////////////////////////////////////////////
// 

void InitShardInstCallback(phInst& inst)
{
	if(phArchetype* archetype = inst.GetArchetype())
	{
		archetype->SetTypeFlags(3);
		archetype->RemoveIncludeFlags(1 << 3);
	}
}

class fragmentSampleManager : public physicsSampleManager
{
public:
	fragmentSampleManager()
		: m_Type(NULL)
		, m_TextureDictionary(NULL)
		, m_Inst(NULL)
		, m_LoadedAnimation(NULL)
		, m_Animation(NULL)
		, m_AnimFrame(NULL)
		, m_AnimPhase(0.0f)
		, m_FrameRate(1.0f)
		, m_AnimBank(NULL)
		, m_AnimIndex(0)
		, m_LoopWidget(true)
	{
	}


	bool BlowWind (fragInst* UNUSED_PARAM(clothInst))
	{
		// #if 0'd out because it's not compiling
#if 0
		// Get the environment cloth from the fragment cache entry.
		Assert(clothInst);
		Assert(clothInst->GetCacheEntry());
		fragHierarchyInst* hierInst = clothInst->GetCacheEntry()->GetHierInst();
		clothController* clothController = NULL;
		if (hierInst->numEnvCloths>0)
		{
			clothController = hierInst->envCloths[0]->GetClothController();
		}
		else if (hierInst->numCharCloths>0)
		{
//			clothController = hierInst->charCloths[0]->GetClothController();
		}

		const int lodIndex = clothController->GetLOD();
		phVerletCloth* verletCloth = clothController->GetCloth(lodIndex);
		if (verletCloth && !verletCloth->IsRope())
		{
			static float flip = 1.0f;
			Vector3 wind( 0.0f, 0.0f, -m_Wind );
			atArray<Vec3V> vertexNormals;

			// we can get the vertex normals from the drawable, so we don't need to recalculate them
			int nNorm = verletCloth->GetNumVertices();
			vertexNormals.Resize( nNorm );
			clothController->GetVertexNormalsFromDrawable( (Vector3*)&(vertexNormals[0]) );
			
			static mthRandom rando;
			static float sdt = 0.0f;
			sdt += (1.0f + rando.GetFloat())*0.01f;
			flip = 0.5f + (sin( sdt ));
			

			// more resistance occurs at a vertex when it's vertex normal is aligned with the wind
			verletCloth->ApplyAirResistance( &(vertexNormals[0]), 1, 0.1f, wind*flip ); 
		}

		// Return true to indicate that the cloth should update.
		// A cloth-in-view check could also be done here, returning false to prevent it from updating when it's out of view.
#endif
		return true;
	}


	virtual void InitClient()
	{
#if __XENON
		AssertMsg(MTX_IN_VB == 0, "Turn off MTX_IN_VB in shaderlib\\skinning_method_config.h to fix rendering problems. These problems are due to not double buffering matrices so it does no affect games, only samples.");
#endif

		physicsSampleManager::InitClient();

#if __PFDRAW
        PFD_Solid.SetEnabled(false);
		PFD_Wireframe.SetEnabled(true);
        PFD_Models.SetEnabled(true);
#endif

		m_Mapper.Map(IOMS_KEYBOARD, KEY_F4, m_Reset);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_C, m_PutIntoCache);

		//RMPTFX Initialization
		ptxManager::InitClass(); 
		RMPTFXMGR.CreatePointPool(1000); 
		RMPTFXMGR.CreateEffectInstPool(1000); 

//		RMPTFXMGR.CreateDrawList(); 

#if __BANK
		RMPTFXMGR.InitDebug();
#endif //__BANK

		m_Demos.AddDemo(rage_new fragWorld("viewfrag"));
		if(GetZUp())
		{
			m_Demos.GetCurrentWorld()->SetUpAxis(2, GRAVITY);
		}

		// Set the pose with a rotation and offset.
		Vector3 position(40.0f,10.0f,-20.0f);
		Vector3 rotation(0.0f,0.75f*PI,0.0f);
		m_ObjectMatrix.Set(CreateRotatedMatrix(position,rotation));

		m_Demos.GetCurrentWorld()->Init(1000, 128, 10000, Vec3V(-10000.0f, -10000.0f, -10000.0f),  Vec3V(10000.0f, 10000.0f, 10000.0f), 128);
		m_Demos.GetCurrentWorld()->Activate();
		const bool triangulated = true;
		m_Demos.GetCurrentWorld()->ConstructTerrainPlane(triangulated, ~DOESNT_COLLIDE_WITH_TERRAIN, GetZUp(), m_ObjectMatrix.d);
		
		m_Wind = 0.08f;

		const char* file = "$/fragments/skinnedtable/entity.type";
	//	const char* file = "//sanf-rd-mkya01/share/rdr2/ASSETS/level/Territory/brittlebushTrawl/type.type";
	//	const char* file = "$/fragments/rabbit01/entity.type";
		PARAM_file.Get(file);

#if 0
		if (PARAM_fragrsc.Get())
		{
			const char *ext = "#td";

			char tdbuffer[512];
			const char* tdname;
			if (PARAM_tdfile.Get(tdname) == false)
			{
				tdname = tdbuffer;

				StringNormalize(tdbuffer, file, sizeof(tdbuffer));
				char* lastSlash = strrchr(tdbuffer, '/');
				AssertMsg(lastSlash, "File name must have slash in it for viewfrag to deduce the texture dictionary filename "
					"for you, either use a slash or provide -rscfile=");
				*lastSlash = '\0';
			}

			// This object must stay in scope and be unmodified until EndStream
			// completes.
			pgStreamerInfo info;
			datResourceMap map;
			if (pgRscBuilder::BeginStream(tdname,ext,grcTextureDefault::RORC_VERSION,info,map))
			{
				pgRscBuilder::EndStream(m_TextureDictionary,info,map,true /*block*/,true /*close*/,tdname);
				pgDictionary<grcTexture>::SetCurrent(m_TextureDictionary);
			}

			ext = "#ft";

			char pckbuffer[512];
			const char* pckname;
			if (PARAM_rscfile.Get(pckname) == false)
			{
				pckname = pckbuffer;

				StringNormalize(pckbuffer, file, sizeof(pckbuffer));
				char* lastSlash = strrchr(pckbuffer, '/');
				AssertMsg(lastSlash, "File name must have slash in it for viewfrag to deduce the rsc filename "
					                 "for you, either use a slash or provide -rscfile=");
				*lastSlash = '\0';
			}

#if !__FINAL && 0
			if (!PARAM_noregen.Get() && 
				(PARAM_regen.Get() || !pgRscBuilder::CheckBuild(file,pckname,ext,fragType::RORC_VERSION)))
			{
				pgRscBuilder::BeginBuild(1024 * 1024 * 40);
				m_Type = fragType::Load(file);
				if (!m_Type)
				{
					Quitf("Unable to find file '%s'.",file);
				}
				pgRscBuilder::SaveBuild(pckname,ext,rvFragObject);
				delete m_Type;
				pgRscBuilder::EndBuild(m_Type);
			}
#endif

			if (!pgRscBuilder::BeginStream(pckname,ext,fragType::RORC_VERSION,info,map))
				Quitf("Cannot stream '%s'",pckname);
			pgRscBuilder::EndStream(m_Type,info,map,true /*block*/,true /*close*/,pckname);

			m_Inst = rage_new fragInst(m_Type, m_ObjectMatrix);
			m_Inst->Insert(false);

			BANK_ONLY(FRAGTUNE->AddArchetypeWidgets(m_Type));
			BANK_ONLY(FRAGTUNE->RefreshTypeList());
		}
		else
#endif
		{
			const char* xmlFile = NULL;

			if (PARAM_nmxmlfile.Get(xmlFile))
			{
				m_Type = rage_new fragType;
				m_Type->fragType::LoadWithXML(file, xmlFile);
			}
			else
			{
				m_Type = fragType::Load(file);
			}

			if (!m_Type)
			{
				// Try appending "/entity.type" to the name and loading again.
				char typeFileName[256];
				typeFileName[255] = '\0';
				formatf(typeFileName,255,"%s/entity.type",file);
				m_Type = fragType::Load(typeFileName);
			}

			if (m_Type)
			{
				m_Inst = rage_new fragInst(m_Type, m_ObjectMatrix);
				m_Inst->Insert(false);

				// Refresh the fragment type list in the widgets. The new type was added to the list in Load() above.
				BANK_ONLY(FRAGTUNE->RefreshTypeList());
			}
		}

//		not used anymore
// 		fragManagerBase::ClothInterface::ClothPreUpdateFunc preUpdateCallback(this,&fragmentSampleManager::BlowWind);
// 		fragManagerBase::ClothInterface::SetEnvironmentClothPreUpdateFunc(preUpdateCallback);
// 		fragManagerBase::ClothInterface::SetCharacterClothPreUpdateFunc(preUpdateCallback);

	//	fragCacheEntry::UseMultithreadedClothUpdate( 2 );

		if (!m_Inst)
		{
			Quitf("Fragment type missing: %s", file);
		}

        m_DemoObject = m_Demos.GetCurrentWorld()->RequestObject();
        m_DemoObject->SetPhysInst(m_Inst);
        m_DemoObject->SetResetMatrix(m_ObjectMatrix);
        m_DemoObject->SetDrawSolidIfNoModel(false);
        m_Demos.GetCurrentWorld()->RegisterObject(m_DemoObject);

		int startingDemo = 0;

		m_Demos.SetCurrentDemo(startingDemo);

		if (PARAM_anim.Get())
		{
			const char* animName;
			PARAM_anim.Get(animName);

			m_LoadedAnimation = m_Animation = crAnimation::AllocateAndLoad(animName);
		}
	}

	virtual void InitCamera()
	{
		Vector3 lookFrom(m_ObjectMatrix.d);
		Vector3 lookTo(m_ObjectMatrix.d);
		int upAxis = 1;
		int forwardAxis = 2;
		if (GetZUp())
		{
			m_CamMgr.SetUseZUp(true);
			upAxis = 2;
			forwardAxis = 1;
		}

		const float elevation = 2.5f;
		const float stepBack = 5.0f;
		lookFrom[upAxis] += elevation;
		lookFrom.SubtractScaled(m_ObjectMatrix.GetVector(forwardAxis),stepBack);

		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update()
	{
		//EnableNanSignal(true);

		m_Mapper.Update();
		if(m_Reset.IsPressed())
		{
			//someone else is resetting the particles
			//RMPTFXMGR.Reset();
		}

		if (m_PutIntoCache.IsPressed() && m_Inst->GetType() && !m_Inst->GetCached())
		{
			m_Inst->PutIntoCache();
		}

		if (grcViewport::GetCurrent())
		{
			FRAGMGR->SetInterestFrustum(*grcViewport::GetCurrent(), GetCameraMatrix());
		}

		if (g_ptxManager::IsInstantiated())
		{
			RMPTFXMGR.BeginFrame();
//			RMPTFXMGR.Update();
		}		

		if (m_Animation && m_Inst->GetType() && !m_Inst->GetCached() && PHLEVEL->IsInactive(m_Inst->GetLevelIndex()))
//		if(m_Inst->GetType() && !m_Inst->GetCached())
//		if(!m_Inst->GetCached())
		{
			// Just for the heck of it let's get the instance into the cache as quickly as possible.  It's not strictly necessary to do so unless we animating it, though.
			m_Inst->PutIntoCache();
			Assert(m_Inst->GetCached());
		}

#if __BANK
		if (m_Inst->GetType() && m_AnimBank == NULL)
		{
			m_AnimBank = &BANKMGR.CreateBank("Animations");

			m_AnimBank->AddToggle("Loop", &m_LoopWidget);
			m_AnimBank->AddButton("Play", datCallback(MFA(fragmentSampleManager::BankPlayAnimation), this));
			m_AnimBank->AddSlider("Speed", &m_FrameRate, 0.0f, 10.0f, 0.1f);
			m_AnimBank->AddButton("Load from file", datCallback(MFA(fragmentSampleManager::LoadAnimation), this));

			m_AnimNames[0] = "<Loaded from file>";
			int numNames = 1;

			Assert(m_Inst->GetType());
			if (fragDrawable* drawable = m_Inst->GetType()->GetCommonDrawable())
			{
				for (int animIndex = 0; animIndex < drawable->GetAnimCount() && numNames < MAX_NUM_ANIMS; ++animIndex, ++numNames)
				{
					fragAnimation* anim = drawable->GetAnimation(animIndex);
					m_AnimNames[numNames] = anim->Name;
				}
			}

			m_AnimBank->AddCombo("Animation Name", &m_AnimIndex, numNames, m_AnimNames, datCallback(MFA(fragmentSampleManager::BankSelectAnimation),this));

			if (&m_Inst->GetType()->GetCommonDrawable()->GetShaderGroup())
			{
				m_Inst->GetType()->GetCommonDrawable()->GetShaderGroup().AddWidgets(BANKMGR.CreateBank("Shaders"));
			}
		}
#endif

		if (m_Animation && m_Demos.GetCurrentWorld()->ShouldUpdate())
		{
			// We have loaded an animation.
			if (!m_AnimFrame)
			{
				if (crSkeleton* skeleton = m_Inst->GetSkeleton())
				{
					m_AnimFrame = rage_new crFrame;
					const crSkeletonData& skeletonData = skeleton->GetSkeletonData();
					m_AnimFrame->InitCreateBoneAndMoverDofs(skeletonData, false);
				}
			}

			if (m_AnimFrame)
			{
				if (m_Inst->IsInLevel() && !PHLEVEL->IsActive(m_Inst->GetLevelIndex()) && m_Inst->GetCached())
				{
					// Have the animation pose the skeleton.
					// Note that this is only done if the instance is no longer active.
					m_Animation->CompositeFrame(m_AnimPhase, *m_AnimFrame, false, false);
					crSkeleton* skeleton = m_Inst->GetSkeleton();
					m_AnimFrame->Pose(*skeleton);

					fragHierarchyInst* hierInst = fragCacheManager::GetEntryFromInst(m_Inst)->GetHierInst();
					Assert(hierInst);
					Matrix34 linkWorld;
					const Mat34V* parentMatrix = &m_Inst->GetMatrix();

					if(hierInst->body != NULL && PHLEVEL->IsActive(m_Inst->GetLevelIndex()))
					{
						int linkIndex = hierInst->articulatedCollider->GetLinkFromComponent(0);
						phArticulatedBody* body = hierInst->body;

						linkWorld.Transpose(MAT34V_TO_MATRIX34(body->GetLink(linkIndex).GetMatrix()));
						linkWorld.d.Set(body->GetLink(linkIndex).GetPosition());
						linkWorld.Dot(RCC_MATRIX34(m_Inst->GetMatrix()));
						linkWorld.DotTranspose(m_Inst->GetTypePhysics()->GetLinkAttachmentMatrices()[0]);

						parentMatrix = &RCC_MAT34V(linkWorld);
					}

					skeleton->SetParentMtx(parentMatrix);
					skeleton->Update();
					skeleton->SetParentMtx(&m_Inst->GetMatrix());
					//m_Inst->SkeletonWasUpdated(reinterpret_cast<Matrix34*>(skeleton->GetObjectMtxs()), skeleton->GetSkeletonData().GetNumBones());
					// ^This used to be called, is this the correct replacement?
					m_Inst->PoseBoundsFromSkeleton(false, false);
				}

				m_AnimPhase += TIME.GetSeconds() * m_FrameRate * m_Demos.GetCurrentWorld()->GetTimeWarp();

				if (m_AnimPhase > m_Animation->GetDuration())
				{
					if (m_LoopWidget)
					{
						m_AnimPhase -= m_Animation->GetDuration();
					}
					else
					{
						m_AnimPhase = m_Animation->GetDuration();
					}
				}
			}
		}

		physicsSampleManager::Update();

	}

	virtual void DrawClient()
	{
		physicsSampleManager::DrawClient();
		if (g_ptxManager::IsInstantiated())
		{
//			RMPTFXMGR.Draw();
		}
	}

	virtual void ShutdownClient()
	{
		m_Demos.GetCurrentWorld()->RemoveObject(m_DemoObject);

		FRAGMGR->Reset();

		delete m_AnimFrame;
		delete m_LoadedAnimation;

		m_Inst->Remove();
		delete m_Inst;

		if (PARAM_fragrsc.Get())
		{
			pgRscBuilder::Delete(m_Type);
		}
		else
		{
			delete m_Type;
		}

		physicsSampleManager::ShutdownClient();

		m_DemoObject->SetPhysInst(NULL);
		delete m_DemoObject;

		if (g_ptxManager::IsInstantiated())
		{
			ptxManager::ShutdownClass();
		}
	}

	void DrawHelpClient()
	{
		physicsSampleManager::DrawHelpClient();

		char tmpString[100];
		formatf(tmpString, sizeof(tmpString), "broken: %s", m_Inst->GetBroken() ? "true" : "false");
		DrawString(tmpString, false);
		formatf(tmpString, sizeof(tmpString), "part broken: %s", m_Inst->GetPartBroken() ? "true" : "false");
		DrawString(tmpString, false);
	}

#if __BANK
	virtual void AddWidgetsClient ()
	{
		bkBank& bank = BANKMGR.CreateBank("ViewFrag");
		bank.AddSlider("Wind Speed",&m_Wind,-10.0f,10.0f,0.1f);

		physicsSampleManager::AddWidgetsClient();
	}
#endif

protected:
	virtual const char* GetSampleName() const
	{
		return "viewfrag";
	}

private:
	void BankSelectAnimation()
	{
		if (m_AnimIndex == 0)
		{
			m_Animation = m_LoadedAnimation;
		}
		else if (fragDrawable* drawable = m_Inst->GetType()->GetCommonDrawable())
		{
			m_Animation = drawable->GetAnimation(m_AnimIndex - 1)->Animation;
		}

		m_AnimPhase = 0.0f;
	}

	void BankPlayAnimation()
	{
		m_AnimPhase = 0.0f;
	}

	void LoadAnimation()
	{
#if __BANK
		if (const char* animFile = BANKMGR.OpenFile("*.anim", false, "Animation (*.anim)"))
		{
			delete m_LoadedAnimation;

			m_LoadedAnimation = m_Animation = crAnimation::AllocateAndLoad(animFile);

			m_AnimIndex = 0;
			m_AnimPhase = 0.0f;
		}
#endif
	}

	fragType*					m_Type;
	pgDictionary<grcTexture>*	m_TextureDictionary;
	fragInst*					m_Inst;
    phDemoObject*               m_DemoObject;
	Matrix34 m_ObjectMatrix;

	float m_Wind;

	// Animation related
	static const int			MAX_NUM_ANIMS = 256;
	crAnimation*				m_LoadedAnimation;
	crAnimation*				m_Animation;
	crFrame*				m_AnimFrame;
	float						m_AnimPhase;
	float						m_FrameRate;
	bkBank*						m_AnimBank;
	const char*					m_AnimNames[MAX_NUM_ANIMS];
	int							m_AnimIndex;
	bool						m_LoopWidget;

	ioMapper					m_Mapper;
	ioValue						m_Reset;
	ioValue						m_PutIntoCache;
};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::fragmentSampleManager samplePhysics;

		samplePhysics.SetFullAssetPath(RAGE_ASSET_ROOT);

		samplePhysics.Init();

		samplePhysics.UpdateLoop();

		samplePhysics.Shutdown();
	}

	return 0;
}





