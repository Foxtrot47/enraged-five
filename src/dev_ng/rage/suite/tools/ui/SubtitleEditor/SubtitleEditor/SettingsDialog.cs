using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.Forms;
using RSG.Base.SCM;
using SubtitleEditorCore;

namespace SubtitleEditor
{
    public partial class SettingsDialog : Form
    {
        public SettingsDialog()
        {
            InitializeComponent();            
        }

        #region Variables
        private string m_projectFilename = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;

                this.subtitleExporterSettingsControl.HelpProvider = this.helpProvider;
            }
        }

        public string ProjectFilename
        {
            get
            {
                return m_projectFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_projectFilename = value;
                }
                else
                {
                    m_projectFilename = string.Empty;
                }
            }
        }
        public string LocalizedTextOptionsFilename
        {
            get
            {
                return this.localizedTextOptionsFilenameTextBox.Text.Trim();
            }
            set
            {
                this.localizedTextOptionsFilenameTextBox.Text = value;
            }
        }

        public string SubtitleOptionsFilename
        {
            get
            {
                return this.subtitleOptionsFilenameTextBox.Text.Trim();
            }
            set
            {
                this.subtitleOptionsFilenameTextBox.Text = value;
            }
        }


        public rageSourceControlProviderSettings SourceControlProviderSettings
        {
            get
            {
                rageSourceControlProviderSettings settings = new rageSourceControlProviderSettings();
                
                settings.Enabled = this.sourceControlEnableCheckBox.Checked;
                settings.Providers = this.sourceControlProviderControl.Providers;
                settings.SelectedIndex = this.sourceControlProviderControl.SelectedIndex;

                return settings;
            }
            set
            {
                if ( value != null )
                {
                    this.sourceControlEnableCheckBox.Checked = value.Enabled;

                    // clone and add to the control
                    List<rageSourceControlProvider> providers = new List<rageSourceControlProvider>();
                    foreach ( rageSourceControlProvider provider in value.Providers )
                    {
                        providers.Add( provider.Clone() );
                    }

                    this.sourceControlProviderControl.Providers = providers;

                    this.sourceControlProviderControl.SelectedIndex = value.SelectedIndex;
                }
            }
        }

        public SubtitleExporterSettings SubtitleExporterSettings
        {
            get
            {
                return this.subtitleExporterSettingsControl.SubtitleExporterSettings;
            }
            set
            {
                this.subtitleExporterSettingsControl.SubtitleExporterSettings = value;
            }
        }
        #endregion

        #region Event Handlers

        #region General Tab
        private void browseLocalizedTextOptionsFilenameButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = "Localized Text Options Files (*.txtproj.options)|*.txtproj.options";
            this.openFileDialog.Title = "Select Localized Text Options File";

            string filename = this.LocalizedTextOptionsFilename;
            if ( !String.IsNullOrEmpty( filename ) )
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( filename );
                this.openFileDialog.FileName = Path.GetFileName( filename );
            }
            else
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( Application.ExecutablePath );
                this.openFileDialog.FileName = "LocalizedTextOptions.txtproj.options";
            }

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.LocalizedTextOptionsFilename = this.openFileDialog.FileName;
            }
        }

        private void browseSubtitleOptionsFilenameButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = "Subtitle Options Files (*.subproj.options)|*.subproj.options";
            this.openFileDialog.Title = "Select Subtitle Options File";

            string filename = this.SubtitleOptionsFilename;
            if ( !String.IsNullOrEmpty( filename ) )
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( filename );
                this.openFileDialog.FileName = Path.GetFileName( filename );
            }
            else
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( Application.ExecutablePath );
                this.openFileDialog.FileName = "SubtitleOptions.subproj.options";
            }

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.SubtitleOptionsFilename = this.openFileDialog.FileName;
            }
        }
        #endregion

        #region Source Control Tab
        private void sourceControlEnableCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.sourceControlProviderControl.Enabled = this.sourceControlEnableCheckBox.Checked;
        }
        #endregion

        private void okButton_Click( object sender, EventArgs e )
        {
            rageStatus status;
            this.subtitleExporterSettingsControl.Validate( out status );
            if ( !status.Success() )
            {
                rageMessageBox.ShowExclamation( this, 
                    String.Format( "{0}\n\nPlease correct the error and try again.", status.ErrorString ), 
                    "Exporter Settings Error" );
                return;
            }

            try
            {
                string filename = this.LocalizedTextOptionsFilename;
                if ( !String.IsNullOrEmpty( filename ) )
                {
                    if ( !String.IsNullOrEmpty( this.ProjectFilename ) )
                    {
                        filename = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( this.ProjectFilename ), filename ) );
                    }

                    if ( !File.Exists( filename ) )
                    {
                        rageMessageBox.ShowExclamation( this,
                        String.Format( "The Localized Text Options File '{0}' does not exist.\n\nPlease correct the error and try again.", filename ),
                        "General Settings Error" );
                        return;
                    }
                }
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowExclamation( this,
                    String.Format( "The following error occurred trying to validate the Localized Text Options Filename:\n\n{0}\n\nPlease correct the error and try again.", ex.Message ),
                    "General Settings Error" );
                return;
            }

            try
            {
                string filename = this.SubtitleOptionsFilename;
                if ( !String.IsNullOrEmpty( filename ) )
                {
                    if ( !String.IsNullOrEmpty( this.ProjectFilename ) )
                    {
                        filename = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( this.ProjectFilename ), filename ) );
                    }

                    if ( !File.Exists( filename ) )
                    {
                        rageMessageBox.ShowExclamation( this,
                        String.Format( "The Subtitle Options File '{0}' does not exist.\n\nPlease correct the error and try again.", filename ),
                        "General Settings Error" );
                        return;
                    }
                }
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowExclamation( this,
                    String.Format( "The following error occurred trying to validate the Subtitle Options Filename:\n\n{0}\n\nPlease correct the error and try again.", ex.Message ),
                    "General Settings Error" );
                return;
            }

            this.DialogResult = DialogResult.OK;
        }
        #endregion
    }
}