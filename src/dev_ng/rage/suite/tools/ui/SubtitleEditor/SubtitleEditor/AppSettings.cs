using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Serialization;
using RSG.Base.SCM;
using SubtitleEditorCore;

namespace SubtitleEditor
{
    public class AppSettings : rageSerializableObject
    {
        public AppSettings()
        {

        }

        #region Variables
        private ScriptControlSettings m_scriptControlSettings = new ScriptControlSettings();
        private PreviewControlSettings m_previewControlSettings = new PreviewControlSettings();
        private rageSourceControlProviderSettings m_sourceControlProviderSettings = new rageSourceControlProviderSettings();
        #endregion

        #region Properties
        public ScriptControlSettings ScriptControlSettings
        {
            get
            {
                return m_scriptControlSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_scriptControlSettings = value;
                }
                else
                {
                    m_scriptControlSettings.Reset();
                }
            }
        }

        public PreviewControlSettings PreviewControlSettings
        {
            get
            {
                return m_previewControlSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_previewControlSettings = value;
                }
                else
                {
                    m_previewControlSettings.Reset();
                }
            }
        }

        public rageSourceControlProviderSettings SourceControlProviderSettings
        {
            get
            {
                return m_sourceControlProviderSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_sourceControlProviderSettings = value;
                }
                else
                {
                    m_sourceControlProviderSettings.Reset();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is AppSettings )
            {
                return Equals( obj as AppSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            AppSettings settings = new AppSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is AppSettings )
            {
                CopyFrom( other as AppSettings );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.PreviewControlSettings.Reset();
            this.ScriptControlSettings.Reset();
            this.SourceControlProviderSettings.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( AppSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.PreviewControlSettings.CopyFrom( other.PreviewControlSettings );
            this.ScriptControlSettings.CopyFrom( other.ScriptControlSettings );
            this.SourceControlProviderSettings.CopyFrom( other.SourceControlProviderSettings );
        }

        public bool Equals( AppSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return this.PreviewControlSettings.Equals( other.PreviewControlSettings )
                && this.ScriptControlSettings.Equals( other.ScriptControlSettings )
                && this.SourceControlProviderSettings.Equals( other.SourceControlProviderSettings );
        }
        #endregion
    }
}
