namespace SubtitleEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( Form1 ) );
            LocalizedTextControls.DefaultLocalizedTextRenderer defaultLocalizedTextRenderer1 = new LocalizedTextControls.DefaultLocalizedTextRenderer();
            TD.SandDock.DockingRules dockingRules1 = new TD.SandDock.DockingRules();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.importLocalizedTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLocalizedTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.recentProjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localizedTextOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subtitleOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localizedTextWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previewWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scriptWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timelineWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.submitFeedbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localizedTextEditorDialogComponent = new LocalizedTextControls.LocalizedTextEditorDialogComponent( this.components );
            this.localizedTextEditorControl = new LocalizedTextControls.LocalizedTextEditorControl();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.languageToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.languageToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.newLanguageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.removeLanguageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.sandDockManager = new TD.SandDock.SandDockManager();
            this.previewTabbedDocument = new TD.SandDock.TabbedDocument();
            this.previewControl = new SubtitleEditorCore.PreviewControl();
            this.documentContainer1 = new TD.SandDock.DocumentContainer();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.subtitleProjectComponent = new SubtitleEditorCore.SubtitleProjectComponent( this.components );
            this.subtitleExporterComponent = new SubtitleEditorCore.SubtitleExporterComponent( this.components );
            this.dockContainer1 = new TD.SandDock.DockContainer();
            this.scriptDockableWindow = new TD.SandDock.DockableWindow();
            this.scriptControl = new SubtitleEditorCore.ScriptControl();
            this.outputDockableWindow = new TD.SandDock.DockableWindow();
            this.outputRichTextBox = new System.Windows.Forms.RichTextBox();
            this.outputRichTextBoxContextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.outputRichTextBoxFindToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.outputRichTextBoxCopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputRichTextBoxSelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputRichTextBoxSearchDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputRichTextBoxSearchUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dockContainer2 = new TD.SandDock.DockContainer();
            this.localizedTextDockableWindow = new TD.SandDock.DockableWindow();
            this.timelineDockableWindow = new TD.SandDock.DockableWindow();
            this.eventEditorControl = new EventEditorControls.EventEditorControl();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.previewTabbedDocument.SuspendLayout();
            this.documentContainer1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.dockContainer1.SuspendLayout();
            this.scriptDockableWindow.SuspendLayout();
            this.outputDockableWindow.SuspendLayout();
            this.outputRichTextBoxContextMenuStrip.SuspendLayout();
            this.dockContainer2.SuspendLayout();
            this.localizedTextDockableWindow.SuspendLayout();
            this.timelineDockableWindow.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.helpProvider.SetHelpKeyword( this.menuStrip, "Menus.html" );
            this.helpProvider.SetHelpNavigator( this.menuStrip, System.Windows.Forms.HelpNavigator.Topic );
            this.menuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.windowsToolStripMenuItem,
            this.helpToolStripMenuItem} );
            this.menuStrip.Location = new System.Drawing.Point( 0, 0 );
            this.menuStrip.Name = "menuStrip";
            this.helpProvider.SetShowHelp( this.menuStrip, true );
            this.menuStrip.Size = new System.Drawing.Size( 1016, 24 );
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator3,
            this.importLocalizedTextToolStripMenuItem,
            this.exportLocalizedTextToolStripMenuItem,
            this.toolStripSeparator1,
            this.recentProjectsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem} );
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size( 35, 20 );
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "newToolStripMenuItem.Image" )));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size( 200, 22 );
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler( this.newToolStripMenuItem_Click );
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "openToolStripMenuItem.Image" )));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size( 200, 22 );
            this.openToolStripMenuItem.Text = "&Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler( this.openToolStripMenuItem_Click );
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size( 197, 6 );
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "saveToolStripMenuItem.Image" )));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size( 200, 22 );
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler( this.saveToolStripMenuItem_Click );
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size( 200, 22 );
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler( this.saveAsToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 197, 6 );
            // 
            // importLocalizedTextToolStripMenuItem
            // 
            this.importLocalizedTextToolStripMenuItem.Name = "importLocalizedTextToolStripMenuItem";
            this.importLocalizedTextToolStripMenuItem.Size = new System.Drawing.Size( 200, 22 );
            this.importLocalizedTextToolStripMenuItem.Text = "&Import Localized Text...";
            this.importLocalizedTextToolStripMenuItem.Click += new System.EventHandler( this.importLocalizedTextToolStripMenuItem_Click );
            // 
            // exportLocalizedTextToolStripMenuItem
            // 
            this.exportLocalizedTextToolStripMenuItem.Name = "exportLocalizedTextToolStripMenuItem";
            this.exportLocalizedTextToolStripMenuItem.Size = new System.Drawing.Size( 200, 22 );
            this.exportLocalizedTextToolStripMenuItem.Text = "&Export Localized Text...";
            this.exportLocalizedTextToolStripMenuItem.Click += new System.EventHandler( this.exportLocalizedTextToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 197, 6 );
            // 
            // recentProjectsToolStripMenuItem
            // 
            this.recentProjectsToolStripMenuItem.Name = "recentProjectsToolStripMenuItem";
            this.recentProjectsToolStripMenuItem.Size = new System.Drawing.Size( 200, 22 );
            this.recentProjectsToolStripMenuItem.Text = "&Recent Projects";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 197, 6 );
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size( 200, 22 );
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler( this.exitToolStripMenuItem_Click );
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.localizedTextOptionsToolStripMenuItem,
            this.subtitleOptionsToolStripMenuItem,
            this.toolStripSeparator4,
            this.exportToolStripMenuItem} );
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size( 44, 20 );
            this.toolsToolStripMenuItem.Text = "&Tools";
            this.toolsToolStripMenuItem.DropDownOpening += new System.EventHandler( this.toolsToolStripMenuItem_DropDownOpening );
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size( 205, 22 );
            this.settingsToolStripMenuItem.Text = "&Settings...";
            this.settingsToolStripMenuItem.Click += new System.EventHandler( this.settingsToolStripMenuItem_Click );
            // 
            // localizedTextOptionsToolStripMenuItem
            // 
            this.localizedTextOptionsToolStripMenuItem.Name = "localizedTextOptionsToolStripMenuItem";
            this.localizedTextOptionsToolStripMenuItem.Size = new System.Drawing.Size( 205, 22 );
            this.localizedTextOptionsToolStripMenuItem.Text = "&Localized Text Options...";
            this.localizedTextOptionsToolStripMenuItem.Click += new System.EventHandler( this.localizedTextOptionsToolStripMenuItem_Click );
            // 
            // subtitleOptionsToolStripMenuItem
            // 
            this.subtitleOptionsToolStripMenuItem.Name = "subtitleOptionsToolStripMenuItem";
            this.subtitleOptionsToolStripMenuItem.Size = new System.Drawing.Size( 205, 22 );
            this.subtitleOptionsToolStripMenuItem.Text = "S&ubtitle Options...";
            this.subtitleOptionsToolStripMenuItem.Click += new System.EventHandler( this.subtitleOptionsToolStripMenuItem_Click );
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 202, 6 );
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.exportToolStripMenuItem.Size = new System.Drawing.Size( 205, 22 );
            this.exportToolStripMenuItem.Text = "&Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler( this.exportToolStripMenuItem_Click );
            // 
            // windowsToolStripMenuItem
            // 
            this.windowsToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.localizedTextWindowToolStripMenuItem,
            this.outputWindowToolStripMenuItem,
            this.previewWindowToolStripMenuItem,
            this.scriptWindowToolStripMenuItem,
            this.timelineWindowToolStripMenuItem} );
            this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
            this.windowsToolStripMenuItem.Size = new System.Drawing.Size( 62, 20 );
            this.windowsToolStripMenuItem.Text = "&Windows";
            // 
            // localizedTextWindowToolStripMenuItem
            // 
            this.localizedTextWindowToolStripMenuItem.Image = global::SubtitleEditor.Properties.Resources.LocalizedTextEditor;
            this.localizedTextWindowToolStripMenuItem.Name = "localizedTextWindowToolStripMenuItem";
            this.localizedTextWindowToolStripMenuItem.Size = new System.Drawing.Size( 153, 22 );
            this.localizedTextWindowToolStripMenuItem.Text = "Localized Text";
            // 
            // outputWindowToolStripMenuItem
            // 
            this.outputWindowToolStripMenuItem.Image = global::SubtitleEditor.Properties.Resources.Output;
            this.outputWindowToolStripMenuItem.Name = "outputWindowToolStripMenuItem";
            this.outputWindowToolStripMenuItem.Size = new System.Drawing.Size( 153, 22 );
            this.outputWindowToolStripMenuItem.Text = "&Output";
            // 
            // previewWindowToolStripMenuItem
            // 
            this.previewWindowToolStripMenuItem.Image = global::SubtitleEditor.Properties.Resources.FullScreenHS;
            this.previewWindowToolStripMenuItem.Name = "previewWindowToolStripMenuItem";
            this.previewWindowToolStripMenuItem.Size = new System.Drawing.Size( 153, 22 );
            this.previewWindowToolStripMenuItem.Text = "&Preview";
            // 
            // scriptWindowToolStripMenuItem
            // 
            this.scriptWindowToolStripMenuItem.Image = global::SubtitleEditor.Properties.Resources.Script;
            this.scriptWindowToolStripMenuItem.Name = "scriptWindowToolStripMenuItem";
            this.scriptWindowToolStripMenuItem.Size = new System.Drawing.Size( 153, 22 );
            this.scriptWindowToolStripMenuItem.Text = "&Script";
            // 
            // timelineWindowToolStripMenuItem
            // 
            this.timelineWindowToolStripMenuItem.Image = global::SubtitleEditor.Properties.Resources.EventEditor;
            this.timelineWindowToolStripMenuItem.Name = "timelineWindowToolStripMenuItem";
            this.timelineWindowToolStripMenuItem.Size = new System.Drawing.Size( 153, 22 );
            this.timelineWindowToolStripMenuItem.Text = "Timeline";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.toolStripSeparator7,
            this.submitFeedbackToolStripMenuItem,
            this.aboutToolStripMenuItem} );
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size( 40, 20 );
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.ShortcutKeyDisplayString = "F1";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.contentsToolStripMenuItem.Text = "&Contents";
            this.contentsToolStripMenuItem.Click += new System.EventHandler( this.contentsToolStripMenuItem_Click );
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size( 175, 6 );
            // 
            // submitFeedbackToolStripMenuItem
            // 
            this.submitFeedbackToolStripMenuItem.Name = "submitFeedbackToolStripMenuItem";
            this.submitFeedbackToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.submitFeedbackToolStripMenuItem.Text = "&Submit Feedback...";
            this.submitFeedbackToolStripMenuItem.Click += new System.EventHandler( this.submitFeedbackToolStripMenuItem_Click );
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler( this.aboutToolStripMenuItem_Click );
            // 
            // localizedTextEditorDialogComponent
            // 
            this.localizedTextEditorDialogComponent.LocalizedTextEditorControl = this.localizedTextEditorControl;
            // 
            // localizedTextEditorControl
            // 
            this.localizedTextEditorControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.localizedTextEditorControl.Enabled = false;
            this.helpProvider.SetHelpKeyword( this.localizedTextEditorControl, "LocalizedTextWindow.html" );
            this.helpProvider.SetHelpNavigator( this.localizedTextEditorControl, System.Windows.Forms.HelpNavigator.Topic );
            this.localizedTextEditorControl.Location = new System.Drawing.Point( 0, 0 );
            this.localizedTextEditorControl.Name = "localizedTextEditorControl";
            this.helpProvider.SetShowHelp( this.localizedTextEditorControl, true );
            this.localizedTextEditorControl.Size = new System.Drawing.Size( 1016, 276 );
            this.localizedTextEditorControl.TabIndex = 0;
            this.localizedTextEditorControl.TextRenderer = defaultLocalizedTextRenderer1;
			this.localizedTextEditorControl.TextDataPreChanged += new LocalizedTextControls.LocalizedTextDataPreChangedEventHandler(this.localizedTextEditorControl_TextDataPreChanged);
			this.localizedTextEditorControl.TextDataChanged += new LocalizedTextControls.LocalizedTextDataChangedEventHandler(this.localizedTextEditorControl_TextDataChanged);
            this.localizedTextEditorControl.ModifiedChanged += new System.EventHandler( this.localizedTextEditorControl_ModifiedChanged );
            this.localizedTextEditorControl.TextDataRemove += new LocalizedTextControls.LocalizedTextDataCancelEventHandler( this.localizedTextEditorControl_TextDataRemove );
            this.localizedTextEditorControl.TextDataAdded += new LocalizedTextControls.LocalizedTextDataIndexEventHandler( this.localizedTextEditorControl_TextDataAdded );
            this.localizedTextEditorControl.TextDataRemoved += new LocalizedTextControls.LocalizedTextDataIndexEventHandler( this.localizedTextEditorControl_TextDataRemoved );
            this.localizedTextEditorControl.TextDataMoved += new LocalizedTextControls.LocalizedTextDataMovedEventHandler( this.localizedTextEditorControl_TextDataMoved );
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.helpProvider.SetHelpKeyword( this.toolStrip, "Toolbar.html" );
            this.helpProvider.SetHelpNavigator( this.toolStrip, System.Windows.Forms.HelpNavigator.Topic );
            this.toolStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator8,
            this.languageToolStripLabel,
            this.languageToolStripComboBox,
            this.newLanguageToolStripButton,
            this.removeLanguageToolStripButton} );
            this.toolStrip.Location = new System.Drawing.Point( 0, 24 );
            this.toolStrip.Name = "toolStrip";
            this.helpProvider.SetShowHelp( this.toolStrip, true );
            this.toolStrip.Size = new System.Drawing.Size( 1016, 25 );
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "newToolStripButton.Image" )));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.newToolStripButton.Text = "&New";
            this.newToolStripButton.Click += new System.EventHandler( this.newToolStripMenuItem_Click );
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "openToolStripButton.Image" )));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.openToolStripButton.Text = "&Open";
            this.openToolStripButton.Click += new System.EventHandler( this.openToolStripMenuItem_Click );
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "saveToolStripButton.Image" )));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.Click += new System.EventHandler( this.saveToolStripMenuItem_Click );
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size( 6, 25 );
            // 
            // languageToolStripLabel
            // 
            this.languageToolStripLabel.Name = "languageToolStripLabel";
            this.languageToolStripLabel.Size = new System.Drawing.Size( 58, 22 );
            this.languageToolStripLabel.Text = "Language:";
            // 
            // languageToolStripComboBox
            // 
            this.languageToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.languageToolStripComboBox.Name = "languageToolStripComboBox";
            this.languageToolStripComboBox.Size = new System.Drawing.Size( 121, 25 );
            this.languageToolStripComboBox.SelectedIndexChanged += new System.EventHandler( this.languageToolStripComboBox_SelectedIndexChanged );
            // 
            // newLanguageToolStripButton
            // 
            this.newLanguageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newLanguageToolStripButton.Image = global::SubtitleEditor.Properties.Resources.NewCardHS;
            this.newLanguageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newLanguageToolStripButton.Name = "newLanguageToolStripButton";
            this.newLanguageToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.newLanguageToolStripButton.Text = "New Language";
            this.newLanguageToolStripButton.Click += new System.EventHandler( this.newLanguageToolStripButton_Click );
            // 
            // removeLanguageToolStripButton
            // 
            this.removeLanguageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.removeLanguageToolStripButton.Enabled = false;
            this.removeLanguageToolStripButton.Image = global::SubtitleEditor.Properties.Resources.DeleteHS;
            this.removeLanguageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeLanguageToolStripButton.Name = "removeLanguageToolStripButton";
            this.removeLanguageToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.removeLanguageToolStripButton.Text = "Remove Language";
            this.removeLanguageToolStripButton.Click += new System.EventHandler( this.removeLanguageToolStripButton_Click );
            // 
            // sandDockManager
            // 
            this.sandDockManager.AutoSaveLayout = true;
            this.sandDockManager.DockSystemContainer = this;
            this.sandDockManager.DocumentOverflow = TD.SandDock.DocumentOverflowMode.None;
            this.sandDockManager.MaximumDockContainerSize = 1000;
            this.sandDockManager.MinimumDockContainerSize = 200;
            this.sandDockManager.OwnerForm = this;
            this.sandDockManager.SerializeTabbedDocuments = true;
            // 
            // previewTabbedDocument
            // 
            this.previewTabbedDocument.AllowClose = false;
            this.previewTabbedDocument.Controls.Add( this.previewControl );
            dockingRules1.AllowDockBottom = false;
            dockingRules1.AllowDockLeft = false;
            dockingRules1.AllowDockRight = false;
            dockingRules1.AllowDockTop = false;
            dockingRules1.AllowFloat = true;
            dockingRules1.AllowTab = true;
            this.previewTabbedDocument.DockingRules = dockingRules1;
            this.previewTabbedDocument.FloatingSize = new System.Drawing.Size( 550, 400 );
            this.previewTabbedDocument.Guid = new System.Guid( "2a2b6a2b-3212-42d2-aeec-ac8c954eead7" );
            this.previewTabbedDocument.Location = new System.Drawing.Point( 1, 21 );
            this.previewTabbedDocument.Name = "previewTabbedDocument";
            this.previewTabbedDocument.PersistState = true;
            this.previewTabbedDocument.ShowOptions = false;
            this.previewTabbedDocument.Size = new System.Drawing.Size( 604, 328 );
            this.previewTabbedDocument.TabImage = global::SubtitleEditor.Properties.Resources.FullScreenHS;
            this.previewTabbedDocument.TabIndex = 0;
            this.previewTabbedDocument.Text = "Preview";
            // 
            // previewControl
            // 
            this.previewControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewControl.Enabled = false;
            this.helpProvider.SetHelpKeyword( this.previewControl, "PreviewWindow.html" );
            this.helpProvider.SetHelpNavigator( this.previewControl, System.Windows.Forms.HelpNavigator.Topic );
            this.previewControl.Location = new System.Drawing.Point( 0, 0 );
            this.previewControl.Name = "previewControl";
            this.helpProvider.SetShowHelp( this.previewControl, true );
            this.previewControl.Size = new System.Drawing.Size( 604, 328 );
            this.previewControl.TabIndex = 0;
            this.previewControl.FileClosed += new System.EventHandler( this.previewControl_FileClosed );
            this.previewControl.StartTimeChanged += new System.EventHandler( this.previewControl_StartTimeChanged );
            this.previewControl.EndTimeChanged += new System.EventHandler( this.previewControl_EndTimeChanged );
            this.previewControl.FileOpened += new System.EventHandler( this.previewControl_FileOpened );
            this.previewControl.CurrentTimeChanged += new System.EventHandler( this.previewControl_CurrentTimeChanged );
            // 
            // documentContainer1
            // 
            this.documentContainer1.ContentSize = 200;
            this.documentContainer1.Controls.Add( this.previewTabbedDocument );
            this.documentContainer1.LayoutSystem = new TD.SandDock.SplitLayoutSystem( new System.Drawing.SizeF( 250F, 400F ), System.Windows.Forms.Orientation.Horizontal, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.DocumentLayoutSystem(new System.Drawing.SizeF(550F, 400F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.previewTabbedDocument))}, this.previewTabbedDocument)))} );
            this.documentContainer1.Location = new System.Drawing.Point( 410, 49 );
            this.documentContainer1.Manager = this.sandDockManager;
            this.documentContainer1.Name = "documentContainer1";
            this.documentContainer1.Size = new System.Drawing.Size( 606, 350 );
            this.documentContainer1.TabIndex = 3;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel} );
            this.statusStrip.Location = new System.Drawing.Point( 0, 719 );
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size( 1016, 22 );
            this.statusStrip.TabIndex = 5;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size( 1001, 17 );
            this.toolStripStatusLabel.Spring = true;
            this.toolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // subtitleProjectComponent
            // 
            this.subtitleProjectComponent.FileExternallyModified += new System.EventHandler( this.subtitleProjectComponent_FileExternallyModified );
            this.subtitleProjectComponent.ModifiedChanged += new System.EventHandler( this.subtitleProjectComponent_ModifiedChanged );
            // 
            // subtitleExporterComponent
            // 
            this.subtitleExporterComponent.ExportBegin += new System.EventHandler( this.subtitleExporterComponent_ExportBegin );
            this.subtitleExporterComponent.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler( this.subtitleExporterComponent_ProgressChanged );
            this.subtitleExporterComponent.ExportComplete += new System.ComponentModel.RunWorkerCompletedEventHandler( this.subtitleExporterComponent_ExportComplete );
            this.subtitleExporterComponent.ExportMessage += new LocalizedTextControls.ExportMessageEventHandler( this.subtitleExporterComponent_ExportMessage );
            this.subtitleExporterComponent.CheckOutFile += new LocalizedTextControls.SourceControlEventHandler( this.subtitleExporterComponent_CheckOutFile );
            // 
            // dockContainer1
            // 
            this.dockContainer1.ContentSize = 406;
            this.dockContainer1.Controls.Add( this.scriptDockableWindow );
            this.dockContainer1.Controls.Add( this.outputDockableWindow );
            this.dockContainer1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockContainer1.LayoutSystem = new TD.SandDock.SplitLayoutSystem( new System.Drawing.SizeF( 250F, 400F ), System.Windows.Forms.Orientation.Horizontal, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.ControlLayoutSystem(new System.Drawing.SizeF(250F, 400F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.scriptDockableWindow)),
                        ((TD.SandDock.DockControl)(this.outputDockableWindow))}, this.scriptDockableWindow)))} );
            this.dockContainer1.Location = new System.Drawing.Point( 0, 49 );
            this.dockContainer1.Manager = this.sandDockManager;
            this.dockContainer1.Name = "dockContainer1";
            this.dockContainer1.Size = new System.Drawing.Size( 410, 350 );
            this.dockContainer1.TabIndex = 2;
            // 
            // scriptDockableWindow
            // 
            this.scriptDockableWindow.Controls.Add( this.scriptControl );
            this.scriptDockableWindow.Guid = new System.Guid( "b5bfe09e-aceb-4643-beca-cc7d0fcfd75e" );
            this.scriptDockableWindow.Location = new System.Drawing.Point( 0, 16 );
            this.scriptDockableWindow.Name = "scriptDockableWindow";
            this.scriptDockableWindow.ShowOptions = false;
            this.scriptDockableWindow.Size = new System.Drawing.Size( 406, 310 );
            this.scriptDockableWindow.TabImage = global::SubtitleEditor.Properties.Resources.Script;
            this.scriptDockableWindow.TabIndex = 0;
            this.scriptDockableWindow.Text = "Script";
            // 
            // scriptControl
            // 
            this.scriptControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptControl.Enabled = false;
            this.helpProvider.SetHelpKeyword( this.scriptControl, "ScriptWindow.html" );
            this.helpProvider.SetHelpNavigator( this.scriptControl, System.Windows.Forms.HelpNavigator.Topic );
            this.scriptControl.Location = new System.Drawing.Point( 0, 0 );
            this.scriptControl.Name = "scriptControl";
            this.helpProvider.SetShowHelp( this.scriptControl, true );
            this.scriptControl.Size = new System.Drawing.Size( 406, 310 );
            this.scriptControl.TabIndex = 0;
            this.scriptControl.FileClosed += new System.EventHandler( this.scriptControl_FileClosed );
            this.scriptControl.FileOpened += new System.EventHandler( this.scriptControl_FileOpened );
            this.scriptControl.AddLocalizedText += new SubtitleEditorCore.AddLocalizedTextEventHandler( this.scriptControl_AddLocalizedText );
            // 
            // outputDockableWindow
            // 
            this.outputDockableWindow.Controls.Add( this.outputRichTextBox );
            this.outputDockableWindow.Guid = new System.Guid( "1180b353-fbe2-43a9-b698-b63b97732535" );
            this.outputDockableWindow.Location = new System.Drawing.Point( 0, 0 );
            this.outputDockableWindow.Name = "outputDockableWindow";
            this.outputDockableWindow.ShowOptions = false;
            this.outputDockableWindow.Size = new System.Drawing.Size( 406, 310 );
            this.outputDockableWindow.TabImage = global::SubtitleEditor.Properties.Resources.Output;
            this.outputDockableWindow.TabIndex = 0;
            this.outputDockableWindow.Text = "Output";
            // 
            // outputRichTextBox
            // 
            this.outputRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.outputRichTextBox.ContextMenuStrip = this.outputRichTextBoxContextMenuStrip;
            this.outputRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpProvider.SetHelpKeyword( this.outputRichTextBox, "OutputWindow.html" );
            this.helpProvider.SetHelpNavigator( this.outputRichTextBox, System.Windows.Forms.HelpNavigator.Topic );
            this.outputRichTextBox.HideSelection = false;
            this.outputRichTextBox.Location = new System.Drawing.Point( 0, 0 );
            this.outputRichTextBox.Name = "outputRichTextBox";
            this.outputRichTextBox.ReadOnly = true;
            this.helpProvider.SetShowHelp( this.outputRichTextBox, true );
            this.outputRichTextBox.Size = new System.Drawing.Size( 406, 310 );
            this.outputRichTextBox.TabIndex = 0;
            this.outputRichTextBox.Text = "";
            this.outputRichTextBox.WordWrap = false;
            // 
            // outputRichTextBoxContextMenuStrip
            // 
            this.outputRichTextBoxContextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.outputRichTextBoxFindToolStripMenuItem,
            this.toolStripSeparator5,
            this.outputRichTextBoxCopyToolStripMenuItem,
            this.outputRichTextBoxSelectAllToolStripMenuItem,
            this.outputRichTextBoxSearchDownToolStripMenuItem,
            this.outputRichTextBoxSearchUpToolStripMenuItem,
            this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem,
            this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem} );
            this.outputRichTextBoxContextMenuStrip.Name = "outputRichTextBoxContextMenuStrip";
            this.outputRichTextBoxContextMenuStrip.Size = new System.Drawing.Size( 265, 164 );
            this.outputRichTextBoxContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler( this.outputRichTextBoxContextMenuStrip_Opening );
            // 
            // outputRichTextBoxFindToolStripMenuItem
            // 
            this.outputRichTextBoxFindToolStripMenuItem.Name = "outputRichTextBoxFindToolStripMenuItem";
            this.outputRichTextBoxFindToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.outputRichTextBoxFindToolStripMenuItem.Size = new System.Drawing.Size( 264, 22 );
            this.outputRichTextBoxFindToolStripMenuItem.Text = "&Find";
            this.outputRichTextBoxFindToolStripMenuItem.Click += new System.EventHandler( this.outputRichTextBoxFindToolStripMenuItem_Click );
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size( 261, 6 );
            // 
            // outputRichTextBoxCopyToolStripMenuItem
            // 
            this.outputRichTextBoxCopyToolStripMenuItem.Name = "outputRichTextBoxCopyToolStripMenuItem";
            this.outputRichTextBoxCopyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.outputRichTextBoxCopyToolStripMenuItem.Size = new System.Drawing.Size( 264, 22 );
            this.outputRichTextBoxCopyToolStripMenuItem.Text = "&Copy";
            this.outputRichTextBoxCopyToolStripMenuItem.Click += new System.EventHandler( this.outputRichTextBoxCopyToolStripMenuItem_Click );
            // 
            // outputRichTextBoxSelectAllToolStripMenuItem
            // 
            this.outputRichTextBoxSelectAllToolStripMenuItem.Name = "outputRichTextBoxSelectAllToolStripMenuItem";
            this.outputRichTextBoxSelectAllToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+A";
            this.outputRichTextBoxSelectAllToolStripMenuItem.Size = new System.Drawing.Size( 264, 22 );
            this.outputRichTextBoxSelectAllToolStripMenuItem.Text = "Select &All";
            this.outputRichTextBoxSelectAllToolStripMenuItem.Click += new System.EventHandler( this.outputRichTextBoxSelectAllToolStripMenuItem_Click );
            // 
            // outputRichTextBoxSearchDownToolStripMenuItem
            // 
            this.outputRichTextBoxSearchDownToolStripMenuItem.Name = "outputRichTextBoxSearchDownToolStripMenuItem";
            this.outputRichTextBoxSearchDownToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.outputRichTextBoxSearchDownToolStripMenuItem.Size = new System.Drawing.Size( 264, 22 );
            this.outputRichTextBoxSearchDownToolStripMenuItem.Text = "Search Down";
            this.outputRichTextBoxSearchDownToolStripMenuItem.Visible = false;
            this.outputRichTextBoxSearchDownToolStripMenuItem.Click += new System.EventHandler( this.outputRichTextBoxSearchDownToolStripMenuItem_Click );
            // 
            // outputRichTextBoxSearchUpToolStripMenuItem
            // 
            this.outputRichTextBoxSearchUpToolStripMenuItem.Name = "outputRichTextBoxSearchUpToolStripMenuItem";
            this.outputRichTextBoxSearchUpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this.outputRichTextBoxSearchUpToolStripMenuItem.Size = new System.Drawing.Size( 264, 22 );
            this.outputRichTextBoxSearchUpToolStripMenuItem.Text = "Search Up";
            this.outputRichTextBoxSearchUpToolStripMenuItem.Visible = false;
            this.outputRichTextBoxSearchUpToolStripMenuItem.Click += new System.EventHandler( this.outputRichTextBoxSearchUpToolStripMenuItem_Click );
            // 
            // outputRichTextBoxSearchHighlightedDownToolStripMenuItem
            // 
            this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem.Name = "outputRichTextBoxSearchHighlightedDownToolStripMenuItem";
            this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F3)));
            this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem.Size = new System.Drawing.Size( 264, 22 );
            this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem.Text = "Search Highlighted Down";
            this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem.Visible = false;
            this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem.Click += new System.EventHandler( this.outputRichTextBoxSearchHighlightedDownToolStripMenuItem_Click );
            // 
            // outputRichTextBoxSearchHighlightedUpToolStripMenuItem
            // 
            this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem.Name = "outputRichTextBoxSearchHighlightedUpToolStripMenuItem";
            this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.F3)));
            this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem.Size = new System.Drawing.Size( 264, 22 );
            this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem.Text = "Search Highlighted Up";
            this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem.Visible = false;
            this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem.Click += new System.EventHandler( this.outputRichTextBoxSearchHighlightedUpToolStripMenuItem_Click );
            // 
            // dockContainer2
            // 
            this.dockContainer2.ContentSize = 316;
            this.dockContainer2.Controls.Add( this.localizedTextDockableWindow );
            this.dockContainer2.Controls.Add( this.timelineDockableWindow );
            this.dockContainer2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockContainer2.LayoutSystem = new TD.SandDock.SplitLayoutSystem( new System.Drawing.SizeF( 250F, 400F ), System.Windows.Forms.Orientation.Vertical, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.ControlLayoutSystem(new System.Drawing.SizeF(250F, 417.9487F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.localizedTextDockableWindow)),
                        ((TD.SandDock.DockControl)(this.timelineDockableWindow))}, this.localizedTextDockableWindow)))} );
            this.dockContainer2.Location = new System.Drawing.Point( 0, 399 );
            this.dockContainer2.Manager = this.sandDockManager;
            this.dockContainer2.Name = "dockContainer2";
            this.dockContainer2.Size = new System.Drawing.Size( 1016, 320 );
            this.dockContainer2.TabIndex = 4;
            // 
            // localizedTextDockableWindow
            // 
            this.localizedTextDockableWindow.Controls.Add( this.localizedTextEditorControl );
            this.localizedTextDockableWindow.Guid = new System.Guid( "e5cf3a81-7a76-42f6-afea-1e91c518cff7" );
            this.localizedTextDockableWindow.Location = new System.Drawing.Point( 0, 20 );
            this.localizedTextDockableWindow.Name = "localizedTextDockableWindow";
            this.localizedTextDockableWindow.PrimaryControl = this.localizedTextEditorControl;
            this.localizedTextDockableWindow.ShowOptions = false;
            this.localizedTextDockableWindow.Size = new System.Drawing.Size( 1016, 276 );
            this.localizedTextDockableWindow.TabImage = global::SubtitleEditor.Properties.Resources.LocalizedTextEditor;
            this.localizedTextDockableWindow.TabIndex = 0;
            this.localizedTextDockableWindow.Text = "Localized Text";
            // 
            // timelineDockableWindow
            // 
            this.timelineDockableWindow.Controls.Add( this.eventEditorControl );
            this.timelineDockableWindow.Guid = new System.Guid( "18082f63-2cf8-44dc-9377-d0f0695c5503" );
            this.timelineDockableWindow.Location = new System.Drawing.Point( 0, 0 );
            this.timelineDockableWindow.Name = "timelineDockableWindow";
            this.timelineDockableWindow.PrimaryControl = this.eventEditorControl;
            this.timelineDockableWindow.ShowOptions = false;
            this.timelineDockableWindow.Size = new System.Drawing.Size( 1016, 276 );
            this.timelineDockableWindow.TabImage = global::SubtitleEditor.Properties.Resources.EventEditor;
            this.timelineDockableWindow.TabIndex = 0;
            this.timelineDockableWindow.Text = "Timeline";
            // 
            // eventEditorControl
            // 
            this.eventEditorControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventEditorControl.Enabled = false;
            this.helpProvider.SetHelpKeyword( this.eventEditorControl, "TimelineWindow.html" );
            this.helpProvider.SetHelpNavigator( this.eventEditorControl, System.Windows.Forms.HelpNavigator.Topic );
            this.eventEditorControl.Location = new System.Drawing.Point( 0, 0 );
            this.eventEditorControl.Name = "eventEditorControl";
            this.helpProvider.SetShowHelp( this.eventEditorControl, true );
            this.eventEditorControl.Size = new System.Drawing.Size( 1016, 276 );
            this.eventEditorControl.TabIndex = 0;
            this.eventEditorControl.EventCreate += new EventEditorControls.EventCreateOnTrackEventHandler( this.eventEditorControl_EventCreate );
            this.eventEditorControl.EventEdit += new EventEditorControls.EventOnTrackEventHandler( this.eventEditorControl_EventEdit );
            this.eventEditorControl.EventDelete += new EventEditorControls.EventOnTrackWithCancelEventHandler( this.eventEditorControl_EventDelete );
            this.eventEditorControl.EventMoved += new EventEditorControls.EventOnTrackEventHandler( this.eventEditorControl_EventMoved );
            this.eventEditorControl.EventMoving += new EventEditorControls.EventOnTrackEventHandler( this.eventEditorControl_EventMoving );
            // 
            // helpProvider
            // 
            this.helpProvider.HelpNamespace = "chm\\overview_SubtitleEditor.chm";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size( 1016, 741 );
            this.Controls.Add( this.documentContainer1 );
            this.Controls.Add( this.dockContainer1 );
            this.Controls.Add( this.dockContainer2 );
            this.Controls.Add( this.statusStrip );
            this.Controls.Add( this.toolStrip );
            this.Controls.Add( this.menuStrip );
            this.helpProvider.SetHelpNavigator( this, System.Windows.Forms.HelpNavigator.TableOfContents );
            this.Icon = ((System.Drawing.Icon)(resources.GetObject( "$this.Icon" )));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Form1";
            this.helpProvider.SetShowHelp( this, true );
            this.Text = "Subtitle Editor";
            this.Load += new System.EventHandler( this.Form1_Load );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.Form1_FormClosing );
            this.menuStrip.ResumeLayout( false );
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout( false );
            this.toolStrip.PerformLayout();
            this.previewTabbedDocument.ResumeLayout( false );
            this.documentContainer1.ResumeLayout( false );
            this.statusStrip.ResumeLayout( false );
            this.statusStrip.PerformLayout();
            this.dockContainer1.ResumeLayout( false );
            this.scriptDockableWindow.ResumeLayout( false );
            this.outputDockableWindow.ResumeLayout( false );
            this.outputRichTextBoxContextMenuStrip.ResumeLayout( false );
            this.dockContainer2.ResumeLayout( false );
            this.localizedTextDockableWindow.ResumeLayout( false );
            this.timelineDockableWindow.ResumeLayout( false );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private LocalizedTextControls.LocalizedTextEditorDialogComponent localizedTextEditorDialogComponent;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton removeLanguageToolStripButton;
        private System.Windows.Forms.ToolStripLabel languageToolStripLabel;
        private System.Windows.Forms.ToolStripComboBox languageToolStripComboBox;
        private TD.SandDock.SandDockManager sandDockManager;
        private TD.SandDock.DockableWindow scriptDockableWindow;
        private TD.SandDock.DockContainer dockContainer1;
        private TD.SandDock.DocumentContainer documentContainer1;
        private TD.SandDock.TabbedDocument previewTabbedDocument;
        private TD.SandDock.DockContainer dockContainer2;
        private TD.SandDock.DockableWindow localizedTextDockableWindow;
        private TD.SandDock.DockableWindow timelineDockableWindow;
        private LocalizedTextControls.LocalizedTextEditorControl localizedTextEditorControl;
        private EventEditorControls.EventEditorControl eventEditorControl;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private SubtitleEditorCore.SubtitleProjectComponent subtitleProjectComponent;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localizedTextOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private SubtitleEditorCore.ScriptControl scriptControl;
        private SubtitleEditorCore.PreviewControl previewControl;
        private System.Windows.Forms.ToolStripMenuItem importLocalizedTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportLocalizedTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem submitFeedbackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subtitleOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recentProjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private SubtitleEditorCore.SubtitleExporterComponent subtitleExporterComponent;
        private TD.SandDock.DockableWindow outputDockableWindow;
        private System.Windows.Forms.RichTextBox outputRichTextBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scriptWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previewWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localizedTextWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timelineWindowToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip outputRichTextBoxContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem outputRichTextBoxCopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputRichTextBoxSelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputRichTextBoxFindToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputRichTextBoxSearchDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputRichTextBoxSearchUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputRichTextBoxSearchHighlightedDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputRichTextBoxSearchHighlightedUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton newLanguageToolStripButton;
        private System.Windows.Forms.HelpProvider helpProvider;
    }
}

