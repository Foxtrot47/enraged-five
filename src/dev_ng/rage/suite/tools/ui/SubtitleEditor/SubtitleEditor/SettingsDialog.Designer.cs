namespace SubtitleEditor
{
    partial class SettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.generalTabPage = new System.Windows.Forms.TabPage();
            this.browseSubtitleOptionsFilenameButton = new System.Windows.Forms.Button();
            this.subtitleOptionsFilenameTextBox = new System.Windows.Forms.TextBox();
            this.subtitleOptionsFilenameLabel = new System.Windows.Forms.Label();
            this.browseLocalizedTextOptionsFilenameButton = new System.Windows.Forms.Button();
            this.localizedTextOptionsFilenameTextBox = new System.Windows.Forms.TextBox();
            this.localizedTextOptionsFilenameLabel = new System.Windows.Forms.Label();
            this.exportTabPage = new System.Windows.Forms.TabPage();
            this.subtitleExporterSettingsControl = new SubtitleEditorCore.SubtitleExporterSettingsControl();
            this.sourceControlTabPage = new System.Windows.Forms.TabPage();
            this.sourceControlEnableCheckBox = new System.Windows.Forms.CheckBox();
            this.sourceControlProviderControl = new RSG.Base.Forms.SourceControlProviderControl();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.tabControl.SuspendLayout();
            this.generalTabPage.SuspendLayout();
            this.exportTabPage.SuspendLayout();
            this.sourceControlTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add( this.generalTabPage );
            this.tabControl.Controls.Add( this.exportTabPage );
            this.tabControl.Controls.Add( this.sourceControlTabPage );
            this.tabControl.Location = new System.Drawing.Point( 12, 12 );
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size( 509, 344 );
            this.tabControl.TabIndex = 0;
            // 
            // generalTabPage
            // 
            this.generalTabPage.Controls.Add( this.browseSubtitleOptionsFilenameButton );
            this.generalTabPage.Controls.Add( this.subtitleOptionsFilenameTextBox );
            this.generalTabPage.Controls.Add( this.subtitleOptionsFilenameLabel );
            this.generalTabPage.Controls.Add( this.browseLocalizedTextOptionsFilenameButton );
            this.generalTabPage.Controls.Add( this.localizedTextOptionsFilenameTextBox );
            this.generalTabPage.Controls.Add( this.localizedTextOptionsFilenameLabel );
            this.helpProvider.SetHelpKeyword( this.generalTabPage, "" );
            this.generalTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.generalTabPage.Name = "generalTabPage";
            this.generalTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.helpProvider.SetShowHelp( this.generalTabPage, true );
            this.generalTabPage.Size = new System.Drawing.Size( 501, 318 );
            this.generalTabPage.TabIndex = 2;
            this.generalTabPage.Text = "General";
            this.generalTabPage.UseVisualStyleBackColor = true;
            // 
            // browseSubtitleOptionsFilenameButton
            // 
            this.browseSubtitleOptionsFilenameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.browseSubtitleOptionsFilenameButton, "Browse for the .subproj.options file." );
            this.browseSubtitleOptionsFilenameButton.Location = new System.Drawing.Point( 470, 30 );
            this.browseSubtitleOptionsFilenameButton.Name = "browseSubtitleOptionsFilenameButton";
            this.helpProvider.SetShowHelp( this.browseSubtitleOptionsFilenameButton, true );
            this.browseSubtitleOptionsFilenameButton.Size = new System.Drawing.Size( 25, 23 );
            this.browseSubtitleOptionsFilenameButton.TabIndex = 5;
            this.browseSubtitleOptionsFilenameButton.Text = "...";
            this.browseSubtitleOptionsFilenameButton.UseVisualStyleBackColor = true;
            this.browseSubtitleOptionsFilenameButton.Click += new System.EventHandler( this.browseSubtitleOptionsFilenameButton_Click );
            // 
            // subtitleOptionsFilenameTextBox
            // 
            this.subtitleOptionsFilenameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.subtitleOptionsFilenameTextBox, "The filename of the .subproj.options file to use for the current subtitle project" +
                    ".  Can be relative to the project." );
            this.subtitleOptionsFilenameTextBox.Location = new System.Drawing.Point( 172, 32 );
            this.subtitleOptionsFilenameTextBox.Name = "subtitleOptionsFilenameTextBox";
            this.helpProvider.SetShowHelp( this.subtitleOptionsFilenameTextBox, true );
            this.subtitleOptionsFilenameTextBox.Size = new System.Drawing.Size( 292, 20 );
            this.subtitleOptionsFilenameTextBox.TabIndex = 4;
            // 
            // subtitleOptionsFilenameLabel
            // 
            this.subtitleOptionsFilenameLabel.AutoSize = true;
            this.helpProvider.SetHelpString( this.subtitleOptionsFilenameLabel, "The filename of the .subproj.options file to use for the current subtitle project" +
                    ".  Can be relative to the project." );
            this.subtitleOptionsFilenameLabel.Location = new System.Drawing.Point( 40, 35 );
            this.subtitleOptionsFilenameLabel.Name = "subtitleOptionsFilenameLabel";
            this.helpProvider.SetShowHelp( this.subtitleOptionsFilenameLabel, true );
            this.subtitleOptionsFilenameLabel.Size = new System.Drawing.Size( 126, 13 );
            this.subtitleOptionsFilenameLabel.TabIndex = 3;
            this.subtitleOptionsFilenameLabel.Text = "Subtitle Options Filename";
            // 
            // browseLocalizedTextOptionsFilenameButton
            // 
            this.browseLocalizedTextOptionsFilenameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.browseLocalizedTextOptionsFilenameButton, "Browse for the .txtproj.options file." );
            this.browseLocalizedTextOptionsFilenameButton.Location = new System.Drawing.Point( 471, 4 );
            this.browseLocalizedTextOptionsFilenameButton.Name = "browseLocalizedTextOptionsFilenameButton";
            this.helpProvider.SetShowHelp( this.browseLocalizedTextOptionsFilenameButton, true );
            this.browseLocalizedTextOptionsFilenameButton.Size = new System.Drawing.Size( 24, 23 );
            this.browseLocalizedTextOptionsFilenameButton.TabIndex = 2;
            this.browseLocalizedTextOptionsFilenameButton.Text = "...";
            this.browseLocalizedTextOptionsFilenameButton.UseVisualStyleBackColor = true;
            this.browseLocalizedTextOptionsFilenameButton.Click += new System.EventHandler( this.browseLocalizedTextOptionsFilenameButton_Click );
            // 
            // localizedTextOptionsFilenameTextBox
            // 
            this.localizedTextOptionsFilenameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.localizedTextOptionsFilenameTextBox, "The filename of the .txtproj.options file to use for the current subtitle project" +
                    ".  Can be relative to the project." );
            this.localizedTextOptionsFilenameTextBox.Location = new System.Drawing.Point( 172, 6 );
            this.localizedTextOptionsFilenameTextBox.Name = "localizedTextOptionsFilenameTextBox";
            this.helpProvider.SetShowHelp( this.localizedTextOptionsFilenameTextBox, true );
            this.localizedTextOptionsFilenameTextBox.Size = new System.Drawing.Size( 293, 20 );
            this.localizedTextOptionsFilenameTextBox.TabIndex = 1;
            // 
            // localizedTextOptionsFilenameLabel
            // 
            this.localizedTextOptionsFilenameLabel.AutoSize = true;
            this.helpProvider.SetHelpString( this.localizedTextOptionsFilenameLabel, "The filename of the .txtproj.options file to use for the current subtitle project" +
                    ".  Can be relative to the project." );
            this.localizedTextOptionsFilenameLabel.Location = new System.Drawing.Point( 6, 9 );
            this.localizedTextOptionsFilenameLabel.Name = "localizedTextOptionsFilenameLabel";
            this.helpProvider.SetShowHelp( this.localizedTextOptionsFilenameLabel, true );
            this.localizedTextOptionsFilenameLabel.Size = new System.Drawing.Size( 160, 13 );
            this.localizedTextOptionsFilenameLabel.TabIndex = 0;
            this.localizedTextOptionsFilenameLabel.Text = "Localized Text Options Filename";
            // 
            // exportTabPage
            // 
            this.exportTabPage.Controls.Add( this.subtitleExporterSettingsControl );
            this.exportTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.exportTabPage.Name = "exportTabPage";
            this.exportTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.exportTabPage.Size = new System.Drawing.Size( 501, 318 );
            this.exportTabPage.TabIndex = 1;
            this.exportTabPage.Text = "Export";
            this.exportTabPage.UseVisualStyleBackColor = true;
            // 
            // subtitleExporterSettingsControl
            // 
            this.subtitleExporterSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpProvider.SetHelpString( this.subtitleExporterSettingsControl, "" );
            this.subtitleExporterSettingsControl.Location = new System.Drawing.Point( 3, 3 );
            this.subtitleExporterSettingsControl.Name = "subtitleExporterSettingsControl";
            this.helpProvider.SetShowHelp( this.subtitleExporterSettingsControl, true );
            this.subtitleExporterSettingsControl.Size = new System.Drawing.Size( 495, 312 );
            this.subtitleExporterSettingsControl.TabIndex = 0;
            // 
            // sourceControlTabPage
            // 
            this.sourceControlTabPage.Controls.Add( this.sourceControlEnableCheckBox );
            this.sourceControlTabPage.Controls.Add( this.sourceControlProviderControl );
            this.sourceControlTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.sourceControlTabPage.Name = "sourceControlTabPage";
            this.sourceControlTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.sourceControlTabPage.Size = new System.Drawing.Size( 501, 318 );
            this.sourceControlTabPage.TabIndex = 0;
            this.sourceControlTabPage.Text = "Source Control";
            this.sourceControlTabPage.UseVisualStyleBackColor = true;
            // 
            // sourceControlEnableCheckBox
            // 
            this.sourceControlEnableCheckBox.AutoSize = true;
            this.sourceControlEnableCheckBox.Location = new System.Drawing.Point( 6, 6 );
            this.sourceControlEnableCheckBox.Name = "sourceControlEnableCheckBox";
            this.sourceControlEnableCheckBox.Size = new System.Drawing.Size( 132, 17 );
            this.sourceControlEnableCheckBox.TabIndex = 1;
            this.sourceControlEnableCheckBox.Text = "Enable Source Control";
            this.sourceControlEnableCheckBox.UseVisualStyleBackColor = true;
            this.sourceControlEnableCheckBox.CheckedChanged += new System.EventHandler( this.sourceControlEnableCheckBox_CheckedChanged );
            // 
            // sourceControlProviderControl
            // 
            this.sourceControlProviderControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.sourceControlProviderControl.Enabled = false;
            this.sourceControlProviderControl.Location = new System.Drawing.Point( 6, 29 );
            this.sourceControlProviderControl.Name = "sourceControlProviderControl";
            this.sourceControlProviderControl.Size = new System.Drawing.Size( 489, 107 );
            this.sourceControlProviderControl.TabIndex = 0;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point( 365, 362 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler( this.okButton_Click );
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 446, 362 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.RestoreDirectory = true;
            // 
            // SettingsDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 533, 397 );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.tabControl );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.helpProvider.SetHelpKeyword( this, "SettingsDialog.html" );
            this.helpProvider.SetHelpNavigator( this, System.Windows.Forms.HelpNavigator.Topic );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size( 539, 422 );
            this.Name = "SettingsDialog";
            this.helpProvider.SetShowHelp( this, true );
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.tabControl.ResumeLayout( false );
            this.generalTabPage.ResumeLayout( false );
            this.generalTabPage.PerformLayout();
            this.exportTabPage.ResumeLayout( false );
            this.sourceControlTabPage.ResumeLayout( false );
            this.sourceControlTabPage.PerformLayout();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage sourceControlTabPage;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.CheckBox sourceControlEnableCheckBox;
        private RSG.Base.Forms.SourceControlProviderControl sourceControlProviderControl;
        private System.Windows.Forms.TabPage exportTabPage;
        private SubtitleEditorCore.SubtitleExporterSettingsControl subtitleExporterSettingsControl;
        private System.Windows.Forms.TabPage generalTabPage;
        private System.Windows.Forms.Button browseSubtitleOptionsFilenameButton;
        private System.Windows.Forms.TextBox subtitleOptionsFilenameTextBox;
        private System.Windows.Forms.Label subtitleOptionsFilenameLabel;
        private System.Windows.Forms.Button browseLocalizedTextOptionsFilenameButton;
        private System.Windows.Forms.TextBox localizedTextOptionsFilenameTextBox;
        private System.Windows.Forms.Label localizedTextOptionsFilenameLabel;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.HelpProvider helpProvider;

    }
}