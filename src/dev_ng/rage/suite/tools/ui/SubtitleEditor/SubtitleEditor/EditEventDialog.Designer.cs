namespace SubtitleEditor
{
    partial class EditEventDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.localizedTextLabel = new System.Windows.Forms.Label();
            this.localizedTextComboBox = new System.Windows.Forms.ComboBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.transitionInCheckBox = new System.Windows.Forms.CheckBox();
            this.transitionInNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.transitionInLabel = new System.Windows.Forms.Label();
            this.transitionOutCheckBox = new System.Windows.Forms.CheckBox();
            this.transitionInComboBox = new System.Windows.Forms.ComboBox();
            this.transitionOutComboBox = new System.Windows.Forms.ComboBox();
            this.transitionOutNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.transitionOutLabel = new System.Windows.Forms.Label();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            ((System.ComponentModel.ISupportInitialize)(this.transitionInNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transitionOutNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // localizedTextLabel
            // 
            this.localizedTextLabel.AutoSize = true;
            this.localizedTextLabel.Location = new System.Drawing.Point( 28, 13 );
            this.localizedTextLabel.Name = "localizedTextLabel";
            this.localizedTextLabel.Size = new System.Drawing.Size( 76, 13 );
            this.localizedTextLabel.TabIndex = 0;
            this.localizedTextLabel.Text = "Localized Text";
            // 
            // localizedTextComboBox
            // 
            this.localizedTextComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.localizedTextComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.localizedTextComboBox.FormattingEnabled = true;
            this.localizedTextComboBox.Location = new System.Drawing.Point( 110, 10 );
            this.localizedTextComboBox.Name = "localizedTextComboBox";
            this.localizedTextComboBox.Size = new System.Drawing.Size( 323, 21 );
            this.localizedTextComboBox.TabIndex = 1;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point( 277, 97 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 10;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler( this.okButton_Click );
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 358, 97 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 11;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // transitionInCheckBox
            // 
            this.transitionInCheckBox.AutoSize = true;
            this.transitionInCheckBox.Location = new System.Drawing.Point( 12, 37 );
            this.transitionInCheckBox.Name = "transitionInCheckBox";
            this.transitionInCheckBox.Size = new System.Drawing.Size( 84, 17 );
            this.transitionInCheckBox.TabIndex = 2;
            this.transitionInCheckBox.Text = "Transition In";
            this.transitionInCheckBox.UseVisualStyleBackColor = true;
            this.transitionInCheckBox.CheckedChanged += new System.EventHandler( this.transitionInCheckBox_CheckedChanged );
            // 
            // transitionInNumericUpDown
            // 
            this.transitionInNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.transitionInNumericUpDown.DecimalPlaces = 3;
            this.transitionInNumericUpDown.Enabled = false;
            this.transitionInNumericUpDown.Location = new System.Drawing.Point( 314, 37 );
            this.transitionInNumericUpDown.Name = "transitionInNumericUpDown";
            this.transitionInNumericUpDown.Size = new System.Drawing.Size( 66, 20 );
            this.transitionInNumericUpDown.TabIndex = 4;
            // 
            // transitionInLabel
            // 
            this.transitionInLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.transitionInLabel.AutoSize = true;
            this.transitionInLabel.Enabled = false;
            this.transitionInLabel.Location = new System.Drawing.Point( 386, 39 );
            this.transitionInLabel.Name = "transitionInLabel";
            this.transitionInLabel.Size = new System.Drawing.Size( 47, 13 );
            this.transitionInLabel.TabIndex = 5;
            this.transitionInLabel.Text = "seconds";
            // 
            // transitionOutCheckBox
            // 
            this.transitionOutCheckBox.AutoSize = true;
            this.transitionOutCheckBox.Location = new System.Drawing.Point( 12, 67 );
            this.transitionOutCheckBox.Name = "transitionOutCheckBox";
            this.transitionOutCheckBox.Size = new System.Drawing.Size( 92, 17 );
            this.transitionOutCheckBox.TabIndex = 6;
            this.transitionOutCheckBox.Text = "Transition Out";
            this.transitionOutCheckBox.UseVisualStyleBackColor = true;
            this.transitionOutCheckBox.CheckedChanged += new System.EventHandler( this.transitionOutCheckBox_CheckedChanged );
            // 
            // transitionInComboBox
            // 
            this.transitionInComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.transitionInComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.transitionInComboBox.Enabled = false;
            this.transitionInComboBox.FormattingEnabled = true;
            this.transitionInComboBox.Location = new System.Drawing.Point( 110, 36 );
            this.transitionInComboBox.Name = "transitionInComboBox";
            this.transitionInComboBox.Size = new System.Drawing.Size( 198, 21 );
            this.transitionInComboBox.TabIndex = 3;
            // 
            // transitionOutComboBox
            // 
            this.transitionOutComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.transitionOutComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.transitionOutComboBox.Enabled = false;
            this.transitionOutComboBox.FormattingEnabled = true;
            this.transitionOutComboBox.Location = new System.Drawing.Point( 110, 65 );
            this.transitionOutComboBox.Name = "transitionOutComboBox";
            this.transitionOutComboBox.Size = new System.Drawing.Size( 198, 21 );
            this.transitionOutComboBox.TabIndex = 7;
            // 
            // transitionOutNumericUpDown
            // 
            this.transitionOutNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.transitionOutNumericUpDown.DecimalPlaces = 3;
            this.transitionOutNumericUpDown.Enabled = false;
            this.transitionOutNumericUpDown.Location = new System.Drawing.Point( 314, 66 );
            this.transitionOutNumericUpDown.Name = "transitionOutNumericUpDown";
            this.transitionOutNumericUpDown.Size = new System.Drawing.Size( 66, 20 );
            this.transitionOutNumericUpDown.TabIndex = 8;
            // 
            // transitionOutLabel
            // 
            this.transitionOutLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.transitionOutLabel.AutoSize = true;
            this.transitionOutLabel.Enabled = false;
            this.transitionOutLabel.Location = new System.Drawing.Point( 386, 68 );
            this.transitionOutLabel.Name = "transitionOutLabel";
            this.transitionOutLabel.Size = new System.Drawing.Size( 47, 13 );
            this.transitionOutLabel.TabIndex = 9;
            this.transitionOutLabel.Text = "seconds";
            // 
            // EditEventDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 445, 132 );
            this.Controls.Add( this.transitionOutLabel );
            this.Controls.Add( this.transitionOutNumericUpDown );
            this.Controls.Add( this.transitionOutComboBox );
            this.Controls.Add( this.transitionInComboBox );
            this.Controls.Add( this.transitionOutCheckBox );
            this.Controls.Add( this.transitionInLabel );
            this.Controls.Add( this.transitionInNumericUpDown );
            this.Controls.Add( this.transitionInCheckBox );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.localizedTextComboBox );
            this.Controls.Add( this.localizedTextLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider.SetHelpKeyword( this, "TimelineWindow.html" );
            this.helpProvider.SetHelpNavigator( this, System.Windows.Forms.HelpNavigator.Topic );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditEventDialog";
            this.helpProvider.SetShowHelp( this, true );
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Event";
            ((System.ComponentModel.ISupportInitialize)(this.transitionInNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transitionOutNumericUpDown)).EndInit();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label localizedTextLabel;
        private System.Windows.Forms.ComboBox localizedTextComboBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.CheckBox transitionInCheckBox;
        private System.Windows.Forms.NumericUpDown transitionInNumericUpDown;
        private System.Windows.Forms.Label transitionInLabel;
        private System.Windows.Forms.CheckBox transitionOutCheckBox;
        private System.Windows.Forms.ComboBox transitionInComboBox;
        private System.Windows.Forms.ComboBox transitionOutComboBox;
        private System.Windows.Forms.NumericUpDown transitionOutNumericUpDown;
        private System.Windows.Forms.Label transitionOutLabel;
        private System.Windows.Forms.HelpProvider helpProvider;
    }
}