using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SubtitleEditor
{
    public partial class NewLanguageDialog : Form
    {
        public NewLanguageDialog()
        {
            InitializeComponent();
        }

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;
            }
        }

        public List<string> Languages
        {
            set
            {
                this.languageComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string language in value )
                    {
                        this.languageComboBox.Items.Add( language );
                    }

                    if ( this.languageComboBox.Items.Count > 0 )
                    {
                        this.languageComboBox.SelectedIndex = 0;
                    }
                }
            }
        }

        public string SelectedLanguage
        {
            get
            {
                return this.languageComboBox.SelectedItem as string;
            }
            set
            {
                if ( value != null )
                {
                    this.languageComboBox.SelectedIndex = this.languageComboBox.Items.IndexOf( value );
                }
                else
                {
                    this.languageComboBox.SelectedIndex = -1;
                }
            }
        }
        #endregion
    }
}