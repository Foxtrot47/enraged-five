using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using EventEditorControls;
using GlacialComponents.Controls.GlacialTreeList;
using LocalizedTextControls;
using RSG.Base.Command;
using RSG.Base.Drawing;
using RSG.Base.Forms;
using RSG.Base.SCM;
using SubtitleEditorCore;
using SubtitleEditor.Properties;
using TD.SandDock;

namespace SubtitleEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            InitializeAdditionalComponents();

            /*
            // DO NOT SUBMIT
            m_localizedTextOptions.FontNames.Add( "Courier New" );
            m_localizedTextOptions.FontNames.Add( "Ariel" );
            m_localizedTextOptions.FontNames.Add( "Times New Roman" );
            m_localizedTextOptions.FontNames.Add( "Bad Font" );
            m_localizedTextOptions.DefaultFontIndex = 0;
            m_localizedTextOptions.Languages.Add( "English" );
            m_localizedTextOptions.DefaultLanguageIndex = 0;

            this.subtitleProjectComponent.SubtitleProjectData.LanguageDataCollection.Items.Add( new LanguageData( "English" ) );
            this.subtitleProjectComponent.SubtitleProjectSettings.CurrentLanguage = "English";

            PopulateLanguageComboBoxFromProject();

            if ( this.languageToolStripComboBox.SelectedItem != null )
            {
                this.localizedTextEditorControl.LocalizedTextOptions = GenerateLocalizedTextOptionsForLanguage( this.languageToolStripComboBox.SelectedItem as string );
            }
            /**/

            /*
            string rtf = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033\\deflangfe1033{\\fonttbl{\\f0\\froman\\fprq2\\fcharset0 Times New Roman;}}"
                + "{\\colortbl ;\\red0\\green0\\blue255;\\red255\\green0\\blue0;}"
                + "{\\*\\generator Msftedit 5.41.15.1515;}\\viewkind4\\uc1\\pard\\b\\f0\\fs24 Bold\\b0\\par"
                + "\\par"
                + "\\i\\\\ \\{Italic\\}\\i0\\par"
                + "\\par"
                + "\\ul Underline\\par"
                + "\\par"
                + "\\cf1\\ulnone Bl\\ul ue\\cf0\\ulnone\\par"
                + "\\par"
                + "\\cf2 Red\\par"
                + "\\par"
                + "\\cf0\\'e9e\\f1\\u256?\\cf2\\f0\\par"
                + "}";

            TextWriter writer = new StreamWriter( @"c:\temp.xml", false, Encoding.Unicode );
            Utility.WriteRtf( rtf, writer );
            writer.Close();
            /**/
        }

        public Form1( bool admin )
            : this()
        {
            m_admin = admin;
        }

        public Form1( string projectFilename, bool admin )
            : this( admin )
        {
            m_startupProjectFilename = projectFilename;
        }

        private void InitializeAdditionalComponents()
        {
            CreateWindowMenuItems( this.localizedTextWindowToolStripMenuItem, this.localizedTextDockableWindow );
            CreateWindowMenuItems( this.outputWindowToolStripMenuItem, this.outputDockableWindow );
            CreateWindowMenuItems( this.previewWindowToolStripMenuItem, this.previewTabbedDocument );
            CreateWindowMenuItems( this.scriptWindowToolStripMenuItem, this.scriptDockableWindow );
            CreateWindowMenuItems( this.timelineWindowToolStripMenuItem, this.timelineDockableWindow );

            this.localizedTextEditorControl.HelpProvider = this.helpProvider;
            this.localizedTextEditorDialogComponent.HelpProvider = this.helpProvider;

            this.scriptControl.RichTextBoxSearchReplaceDialog.FindCurrentRichTextBox += new rageSearchReplaceRichTextBoxEventHandler( RichTextBoxSearchReplaceDialog_FindCurrentRichTextBox );            

            ToolStripMenuItem addToTimelineToolStripMenuItem = new ToolStripMenuItem( "Add To Timeline" );
            addToTimelineToolStripMenuItem.Click += new EventHandler( localizedTextEditorControlContextMenuStripAddToTimelineToolStripMenuItem_Click );

            this.localizedTextEditorControl.TreeList.ContextMenuStrip.Items.Add( new ToolStripSeparator() );
            this.localizedTextEditorControl.TreeList.ContextMenuStrip.Items.Add( addToTimelineToolStripMenuItem );
        }

        private void CreateWindowMenuItems( ToolStripMenuItem menuItem, DockControl dockCtrl )
        {
            ToolStripMenuItem item = new ToolStripMenuItem( "&Show" );
            item.Click += new EventHandler( showToolStripMenuItem_Click );
            item.Tag = dockCtrl;
            menuItem.DropDownItems.Add( item );

            item = new ToolStripMenuItem( "&Center" );
            item.Click += new EventHandler( centerToolStripMenuItem_Click );
            item.Tag = dockCtrl;
            menuItem.DropDownItems.Add( item );

            item = new ToolStripMenuItem( "&Dock" );
            item.Click += new EventHandler( dockToolStripMenuItem_Click );
            item.Tag = dockCtrl;
            menuItem.DropDownItems.Add( item );

            item = new ToolStripMenuItem( "&Float" );
            item.Click += new EventHandler( floatToolStripMenuItem_Click );
            item.Tag = dockCtrl;
            menuItem.DropDownItems.Add( item );
        }

        #region Variables
        private static bool sm_exitingAfterException = false;

        private bool m_admin = false;
        private string m_startupProjectFilename = null;

        private AppSettings m_appSettings = new AppSettings();
        private LocalizedTextOptions m_localizedTextOptions = new LocalizedTextOptions();
        private SubtitleOptions m_subtitleOptions = new SubtitleOptions();
        private rageSourceControlProviderSettings m_sourceControlProviderSettings = new rageSourceControlProviderSettings();

        private FeedbackForm m_feedbackForm = null;

        private Dictionary<int, List<EventControl>> m_textDataHashToEventControl = new Dictionary<int, List<EventControl>>();
        private Dictionary<int, Bitmap> m_textDataHashToBitmap = new Dictionary<int, Bitmap>();

        private string m_currentLanguage = String.Empty;

        private bool m_localizedTextEditorControlModified = false;
        private bool m_eventEditorControlModified = false;
        private bool m_previewControlModified = false;
        private bool m_scriptControlModified = false;
        private bool m_subtitleProjectModified = false;
        #endregion

        #region Properties
        public static bool ExitingAfterException
        {
            set
            {
                sm_exitingAfterException = value;
            }
        }

        public bool IsModified
        {
            get
            {
                return m_localizedTextEditorControlModified || m_eventEditorControlModified || m_previewControlModified
                    || m_scriptControlModified || m_subtitleProjectModified;
            }
        }
        #endregion

        #region Event Handlers
        private void Form1_Load( object sender, EventArgs e )
        {
            LoadLayout();
        }

        private void Form1_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( sm_exitingAfterException )
            {
                return;
            }

            if ( this.subtitleProjectComponent.IsModified )
            {
                DialogResult result = rageMessageBox.ShowQuestion( this,
                    String.Format( "{0}\n\nThis file has been modified.  Do you wish to save it before closing?",
                    this.subtitleProjectComponent.Filename ), "Project File Modified", MessageBoxButtons.YesNoCancel );

                if ( result == DialogResult.Yes )
                {
                    saveToolStripMenuItem_Click( sender, e );
                }
                else if ( result == DialogResult.Cancel )
                {
                    e.Cancel = true;
                    return;
                }
            }

            if ( e.CloseReason != CloseReason.WindowsShutDown )
            {
                SaveLayout();
            }

            CloseProject();
        }

        #region File Menu
        private void newToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.subtitleProjectComponent.IsModified )
            {
                DialogResult result = rageMessageBox.ShowQuestion( this,
                    String.Format( "{0}\n\nThis file has been modified.  Do you wish to save it before closing?", this.subtitleProjectComponent.Filename ),
                    "Project File Modified" );

                if ( result == DialogResult.Yes )
                {
                    saveToolStripMenuItem_Click( sender, e );
                }
            }

            CloseProject();

            this.subtitleProjectComponent.ProjectData.SubtitleOptionsFilename = m_subtitleOptions.CurrentFilename;
            this.subtitleProjectComponent.ProjectData.LocalizedTextOptionsFilename = m_localizedTextOptions.CurrentFilename;
        }

        private void openToolStripMenuItem_Click( object sender, EventArgs e )
        {
            DialogResult result;

            if ( this.subtitleProjectComponent.IsModified )
            {
                result = rageMessageBox.ShowQuestion( this,
                    String.Format( "{0}\n\nThis file has been modified.  Do you wish to save it before closing?", this.subtitleProjectComponent.Filename ),
                    "File Modified" );

                if ( result == DialogResult.Yes )
                {
                    saveToolStripMenuItem_Click( sender, e );
                }
            }

            if ( !String.IsNullOrEmpty( this.subtitleProjectComponent.Filename ) )
            {
                string directory = Path.GetDirectoryName( this.subtitleProjectComponent.Filename );
                if ( Directory.Exists( directory ) )
                {
                    this.openFileDialog.InitialDirectory = directory;
                }
            }

            this.openFileDialog.Filter = "Subtitle Project Files (*.subproj)|*.subproj";
            this.openFileDialog.Title = "Open Subtitle Project";

            result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                CloseProject();

                LoadProject( this.openFileDialog.FileName );
            }
        }

        private void saveToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( String.IsNullOrEmpty( this.subtitleProjectComponent.Filename ) )
            {
                saveAsToolStripMenuItem_Click( sender, e );
            }
            else
            {
                SaveProject( this.subtitleProjectComponent.Filename );
            }
        }

        private void saveAsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( !String.IsNullOrEmpty( this.subtitleProjectComponent.Filename ) )
            {
                string directory = Path.GetDirectoryName( this.subtitleProjectComponent.Filename );
                if ( Directory.Exists( directory ) )
                {
                    this.saveFileDialog.InitialDirectory = directory;
                }
            }

            this.saveFileDialog.Filter = "Subtitle Project Files (*.subproj)|*.subproj";
            this.saveFileDialog.Title = "Save Subtitle Project As";

            DialogResult result = this.saveFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                SaveProject( this.saveFileDialog.FileName );
            }
        }

        private void importLocalizedTextToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = "Localized Text Project Files (*.txtproj)|*.txtproj";
            this.openFileDialog.Title = "Import Localized Text Project";

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                ImportLocalizedText( this.openFileDialog.FileName );
            }
        }

        private void exportLocalizedTextToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.saveFileDialog.Filter = "Localized Text Project Files (*.txtproj)|*.txtproj";
            this.saveFileDialog.Title = "Export Localized Text Project";

            DialogResult result = this.saveFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                ExportLocalizedText( this.saveFileDialog.FileName );
            }
        }

        private void recentProjectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            DialogResult result;

            if ( this.subtitleProjectComponent.IsModified )
            {
                result = rageMessageBox.ShowQuestion( this,
                    String.Format( "{0}\n\nThis file has been modified.  Do you wish to save it before closing?", this.subtitleProjectComponent.Filename ),
                    "File Modified" );

                if ( result == DialogResult.Yes )
                {
                    saveToolStripMenuItem_Click( sender, e );
                }
            }

            CloseProject();

            LoadProject( (sender as ToolStripMenuItem).Text );
        }

        private void exitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.Close();
        }
        #endregion

        #region Tools Menu
        private void toolsToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.localizedTextOptionsToolStripMenuItem.Enabled = m_admin;
            this.subtitleOptionsToolStripMenuItem.Enabled = m_admin;
        }

        private void settingsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SettingsDialog dialog = new SettingsDialog();
            dialog.HelpNamespace = this.helpProvider.HelpNamespace;
            dialog.ProjectFilename = this.subtitleProjectComponent.Filename;
            dialog.LocalizedTextOptionsFilename = m_localizedTextOptions.CurrentFilename;
            dialog.SubtitleOptionsFilename = m_subtitleOptions.CurrentFilename;
            dialog.SourceControlProviderSettings = m_sourceControlProviderSettings;
            dialog.SubtitleExporterSettings = this.subtitleProjectComponent.ProjectData.ExporterSettings;

            DialogResult result = dialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                m_sourceControlProviderSettings = dialog.SourceControlProviderSettings;
                this.subtitleProjectComponent.ProjectSettings.SourceControlProviderSettings.CopyFrom( m_sourceControlProviderSettings );

                string filename = dialog.SubtitleOptionsFilename;
                if ( filename != m_subtitleOptions.CurrentFilename )
                {
                    if ( !String.IsNullOrEmpty( filename ) )
                    {
                        string relativeFilename = this.subtitleProjectComponent.ProjectData.GetRelativePath( filename );
                        string absoluteFilename = this.subtitleProjectComponent.ProjectData.GetAbsolutePath( relativeFilename );
                        if ( LoadSubtitleOptions( absoluteFilename ) )
                        {
                            this.subtitleProjectComponent.ProjectData.SubtitleOptionsFilename = m_subtitleOptions.CurrentFilename;

                            m_subtitleProjectModified = true;
                        }
                    }
                    else
                    {
                        this.subtitleProjectComponent.ProjectData.SubtitleOptionsFilename = filename;

                        m_subtitleOptions.Reset();

                        m_subtitleProjectModified = true;
                    }
                }

                filename = dialog.LocalizedTextOptionsFilename;
                if ( filename != m_subtitleOptions.CurrentFilename )
                {
                    if ( !String.IsNullOrEmpty( filename ) )
                    {
                        string relativeFilename = this.subtitleProjectComponent.ProjectData.GetRelativePath( filename );
                        string absoluteFilename = this.subtitleProjectComponent.ProjectData.GetAbsolutePath( relativeFilename );
                        if ( LoadLocalizedTextOptions( absoluteFilename ) )
                        {
                            this.subtitleProjectComponent.ProjectData.LocalizedTextOptionsFilename = m_localizedTextOptions.CurrentFilename;

                            m_subtitleProjectModified = true;
                        }
                    }
                    else
                    {
                        m_localizedTextOptions.Reset();

                        LocalizedTextLanguageOption languageOption = m_localizedTextOptions.LanguageOptionCollection.Find( m_currentLanguage );
                        if ( languageOption != null )
                        {
                            this.localizedTextEditorControl.LocalizedTextOptions = GenerateLocalizedTextOptionsForLanguage( languageOption );
                        }

                        this.subtitleProjectComponent.ProjectData.LocalizedTextOptionsFilename = filename;

                        m_subtitleProjectModified = true;
                    }
                }

                SubtitleExporterSettings settings = dialog.SubtitleExporterSettings;
                if ( !this.subtitleProjectComponent.ProjectData.ExporterSettings.Equals( settings ) )
                {
                    this.subtitleProjectComponent.ProjectData.ExporterSettings.CopyFrom( settings );

                    m_subtitleProjectModified = true;
                }

                this.subtitleProjectComponent.IsModified = this.IsModified;
            }
        }

        private void localizedTextOptionsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            LocalizedTextOptionsDialog dialog = new LocalizedTextOptionsDialog();
            dialog.HelpNamespace = this.helpProvider.HelpNamespace;
            dialog.CheckOutFile += new SourceControlEventHandler( optionsDialog_CheckOutFile );
            dialog.LocalizedTextOptions = m_localizedTextOptions;

            DialogResult result = dialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                LocalizedTextOptions options = dialog.LocalizedTextOptions;
                if ( !options.Equals( m_localizedTextOptions ) )
                {
                    // set the LocalizedTextEditorControl
                    m_localizedTextOptions = options;

                    InitializeFromLocalizedTextOptions();
                }
            }
        }

        private void subtitleOptionsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SubtitleOptionsDialog dialog = new SubtitleOptionsDialog();
            dialog.CheckOutFile += new SourceControlEventHandler( optionsDialog_CheckOutFile );
            dialog.SubtitleOptions = m_subtitleOptions;

            DialogResult result = dialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                SubtitleOptions options = dialog.SubtitleOptions;
                if ( !options.Equals( m_subtitleOptions ) )
                {
                    m_subtitleOptions = options;

                    if ( this.subtitleProjectComponent.ProjectData.SubtitleOptionsFilename != m_subtitleOptions.CurrentFilename )
                    {                        
                        this.subtitleProjectComponent.ProjectData.SubtitleOptionsFilename = m_subtitleOptions.CurrentFilename;

                        m_subtitleProjectModified = true;
                        this.subtitleProjectComponent.IsModified = this.IsModified;
                    }
                }
            }
        }

        private void optionsDialog_CheckOutFile( object sender, SourceControlEventArgs e )
        {
            try
            {
                rageStatus status;
                if ( !File.Exists( e.Filename ) || !m_sourceControlProviderSettings.IsSourceControlled( e.Filename, out status ) )
                {
                    e.Result = DialogResult.OK;
                    return;
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }

            while ( true )
            {
                // check out the file
                rageStatus status;
                m_sourceControlProviderSettings.CheckOut( e.Filename, out status );
                if ( status.Success() )
                {
                    e.Result = DialogResult.OK;
                    break;
                }

                e.Result = rageMessageBox.ShowExclamation( this, status.ErrorString, "Error Checking Out File", 
                    MessageBoxButtons.AbortRetryIgnore );
                if ( e.Result == DialogResult.Retry )
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
        }

        private void exportToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.IsModified )
            {
                // Save before export (we do some extra validation on save)
                saveToolStripMenuItem_Click( sender, e );
            }

            // Make sure we have a filename before trying to export
            if ( !String.IsNullOrEmpty( this.subtitleProjectComponent.Filename ) )
            {
                this.subtitleExporterComponent.LocalizedTextOptions = m_localizedTextOptions;
                this.subtitleExporterComponent.SubtitleOptions = m_subtitleOptions;
                this.subtitleExporterComponent.SubtitleProjectData = this.subtitleProjectComponent.ProjectData;
                this.subtitleExporterComponent.TimeLineComponent = this.eventEditorControl.TimeLine;

                this.subtitleExporterComponent.Export();
            }
        }
        #endregion

        #region Windows Menu
        private void showToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if ( window != null )
                {
                    window.Open( WindowOpenMethod.OnScreenActivate );
                }
            }
        }

        private void centerToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if ( window != null )
                {
                    if ( window.DockSituation == DockSituation.Floating )
                    {
                        Rectangle rect = this.Bounds;

                        int x = (rect.Width / 2) + rect.Left - (window.FloatingSize.Width / 2);
                        int y = (rect.Height / 2) + rect.Top - (window.FloatingSize.Height / 2);

                        window.FloatingLocation = new Point( x, y );
                    }
                }
            }
        }

        private void dockToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if ( window != null )
                {
                    window.OpenDocked();
                }
            }
        }

        private void floatToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if ( window != null )
                {
                    window.OpenFloating();
                }
            }
        }
        #endregion

        #region Help Menu
        private void contentsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string filename = Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), @"chm\overview_SubtitleEditor.chm" );
            if ( File.Exists( filename ) )
            {
                Process.Start( filename );
            }
        }

        private void submitFeedbackToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_feedbackForm == null )
            {
                m_feedbackForm = new FeedbackForm( "Subtitle Editor", Settings.Default.FeedbackFromEmailAddress,
                    Settings.Default.FeedbackToEmailAddress, this.Icon );
                m_feedbackForm.FromEmailAddressChanged += new FeedbackForm.FromEmailAddressChangedEventHandler( feedbackForm_FromEmailAddressChanged );

                Rectangle rect = this.Bounds;
                m_feedbackForm.StartPosition = FormStartPosition.Manual;

                int x = (rect.Width / 2) + rect.Left - (m_feedbackForm.Width / 2);
                int y = (rect.Height / 2) + rect.Top - (m_feedbackForm.Height / 2);

                m_feedbackForm.Location = new Point( x, y );
            }

            if ( m_feedbackForm.Visible )
            {
                if ( m_feedbackForm.WindowState == FormWindowState.Minimized )
                {
                    m_feedbackForm.WindowState = FormWindowState.Normal;
                }

                m_feedbackForm.Focus();
            }
            else
            {
                m_feedbackForm.Show();
            }
        }

        private void feedbackForm_FromEmailAddressChanged( object sender, FromEmailAddressChangedEventArgs e )
        {
            Settings.Default.FeedbackFromEmailAddress = e.FromEmailAddress;
        }

        private void aboutToolStripMenuItem_Click( object sender, EventArgs e )
        {
            rageMessageBox.ShowInformation( this, "Please direct questions, comments, and problems to the 'RSGSAN RAGE Cut Scene' mailing list.",
                "About..." );
        }
        #endregion

        #region ToolStrip
        private void newLanguageToolStripButton_Click( object sender, EventArgs e )
        {
            // find which of the available languages we haven't created yet
            List<string> languages = new List<string>();
            foreach ( LocalizedTextLanguageOption option in m_localizedTextOptions.LanguageOptionCollection )
            {
                if ( this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Find( option.Language ) == null )
                {
                    languages.Add( option.Language );
                }
            }

            if ( languages.Count > 0 )
            {
                // prompt the user for the language
                NewLanguageDialog dialog = new NewLanguageDialog();
                dialog.HelpNamespace = this.helpProvider.HelpNamespace;
                dialog.Languages = languages;

                DialogResult result = dialog.ShowDialog( this );
                if ( result == DialogResult.OK )
                {
                    this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Add( new SubtitleLanguageData( dialog.SelectedLanguage ) );

                    this.languageToolStripComboBox.Items.Add( dialog.SelectedLanguage );
                    this.languageToolStripComboBox.SelectedIndex = this.languageToolStripComboBox.Items.Count - 1;

                    m_subtitleProjectModified = true;
                    this.subtitleProjectComponent.IsModified = this.IsModified;
                }
            }
            else
            {
                // no more languages
                rageMessageBox.ShowExclamation( this, "All available languages have been created.", "Unable to Create New Language" );
            }
        }

        private void removeLanguageToolStripButton_Click( object sender, EventArgs e )
        {
            if ( this.languageToolStripComboBox.SelectedIndex  == -1 )
            {
                return;
            }

            DialogResult result = rageMessageBox.ShowWarning( this,
                String.Format( "You are about to remove all data for the {0} language.\n\nAre you sure you want to proceed?", 
                this.languageToolStripComboBox.SelectedItem as string ),
                "Remove Language", MessageBoxButtons.YesNo );
            if ( result == DialogResult.No )
            {
                return;
            }

            // remove the language from the project, if it exists.
            int indexOf = -1;
            for ( int i = 0; i < this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Count; ++i )
            {
                if ( this.subtitleProjectComponent.ProjectData.LanguageDataCollection[i].Language == this.languageToolStripComboBox.SelectedItem as string )
                {
                    indexOf = i;
                    break;
                }
            }

            if ( indexOf < this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Count )
            {
                this.subtitleProjectComponent.ProjectData.LanguageDataCollection.RemoveAt( indexOf );
            }

            m_currentLanguage = null;   // clear this so we don't try to save the language's data

            // remove the language from the combo box
            int selectedIndex = this.languageToolStripComboBox.SelectedIndex;
            this.languageToolStripComboBox.Items.RemoveAt( this.languageToolStripComboBox.SelectedIndex );

            // try to find another language to select
            while ( (selectedIndex >= 0) && (selectedIndex >= this.languageToolStripComboBox.Items.Count) )
            {
                --selectedIndex;
            }

            this.languageToolStripComboBox.SelectedIndex = selectedIndex;

            languageToolStripComboBox_SelectedIndexChanged( sender, e );

            m_subtitleProjectModified = true;
            this.subtitleProjectComponent.IsModified = this.IsModified;
        }

        private void languageToolStripComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            // save the data from the previously selected language
            PopulateProjectDataFromUI();

            this.removeLanguageToolStripButton.Enabled = this.languageToolStripComboBox.SelectedIndex != -1;

            EnableEditing( this.languageToolStripComboBox.SelectedIndex != -1 );

            // load the data for the newly selected language
            PopulateUIFromProjectData();
        }
        #endregion

        #region Output RichTextBox
        private void RichTextBoxSearchReplaceDialog_FindCurrentRichTextBox( object sender, rageSearchReplaceRichTextBoxEventArgs e )
        {
            if ( this.outputRichTextBox.Focused )
            {
                e.RichTextBox = this.outputRichTextBox;
            }
        }

        private void outputRichTextBoxContextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            this.outputRichTextBoxCopyToolStripMenuItem.Enabled = this.outputRichTextBox.SelectedText.Length > 0;
        }

        private void outputRichTextBoxFindToolStripMenuItem_Click( object sender, EventArgs e )
        {
            // provide a default location, if none has been provided
            if ( !this.scriptControl.RichTextBoxSearchReplaceDialog.LocationSet )
            {
                Utility.CenterLocation( this, this.scriptControl.RichTextBoxSearchReplaceDialog );
                this.scriptControl.RichTextBoxSearchReplaceDialog.LocationSet = true;
            }

            // Show the find/replace form
            if ( !this.scriptControl.RichTextBoxSearchReplaceDialog.Visible )
            {
                this.scriptControl.RichTextBoxSearchReplaceDialog.Show( this );
            }

            this.scriptControl.RichTextBoxSearchReplaceDialog.Focus();

            this.scriptControl.RichTextBoxSearchReplaceDialog.InitSearchText( this.outputRichTextBox );
        }

        private void outputRichTextBoxCopyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.outputRichTextBox.SelectedText.Length > 0 )
            {
                this.outputRichTextBox.Copy();
            }
        }

        private void outputRichTextBoxSelectAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.outputRichTextBox.SelectAll();
        }

        private void outputRichTextBoxSearchDownToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.scriptControl.RichTextBoxSearchReplaceDialog.SearchAgainDown( this.outputRichTextBox );
        }

        private void outputRichTextBoxSearchUpToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.scriptControl.RichTextBoxSearchReplaceDialog.SearchAgainUp( this.outputRichTextBox );
        }

        private void outputRichTextBoxSearchHighlightedDownToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.scriptControl.RichTextBoxSearchReplaceDialog.SearchHighlightedDown( this.outputRichTextBox );
        }

        private void outputRichTextBoxSearchHighlightedUpToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.scriptControl.RichTextBoxSearchReplaceDialog.SearchHighlightedUp( this.outputRichTextBox );
        }
        #endregion

        #region SubtitleProjectComponent
        private void subtitleProjectComponent_FileExternallyModified( object sender, EventArgs e )
        {
            if ( this.InvokeRequired )
            {
                this.Invoke( new EventHandler( subtitleProjectComponent_FileExternallyModified ), new object[] { sender, e } );
                return;
            }

            DialogResult result = rageMessageBox.ShowQuestion( this,
                "The subtitle file has been externally modified.  Would you like to reload it?", "Subtitle File Externally Modified" );
            if ( result == DialogResult.Yes )
            {
                ReloadProject();
            }
        }

        private void subtitleProjectComponent_ModifiedChanged( object sender, EventArgs e )
        {
            string name = "<new>";
            if ( !String.IsNullOrEmpty( this.subtitleProjectComponent.Filename ) )
            {
                name = Path.GetFileName( this.subtitleProjectComponent.Filename );
            }

            this.Text = String.Format( "{0}{1} - Subtitle Editor", name, this.subtitleProjectComponent.IsModified ? "*" : "" );
        }
        #endregion

        #region SubtitleExporterComponent
        private void subtitleExporterComponent_CheckOutFile( object sender, SourceControlEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                this.Invoke( new SourceControlEventHandler( subtitleExporterComponent_CheckOutFile ), new object[] { sender, e } );
                return;
            }

            try
            {
                rageStatus status;
                if ( !File.Exists( e.Filename ) || !m_sourceControlProviderSettings.IsSourceControlled( e.Filename, out status ) )
                {
                    e.Result = DialogResult.OK;
                    return;
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }

            while ( true )
            {
                // check out the file
                rageStatus status;
                m_sourceControlProviderSettings.CheckOut( e.Filename, out status );
                if ( status.Success() )
                {
                    e.Result = DialogResult.OK;
                    break;
                }

                e.Result = rageMessageBox.ShowExclamation( this, status.ErrorString, "Error Checking Out File",
                    MessageBoxButtons.AbortRetryIgnore );
                if ( e.Result == DialogResult.Retry )
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
        }

        private void subtitleExporterComponent_ExportBegin( object sender, EventArgs e )
        {
            if ( this.InvokeRequired )
            {
                this.Invoke( new EventHandler( subtitleExporterComponent_ExportBegin ), new object[] { sender, e } );
                return;
            }

            this.toolStripStatusLabel.Text = string.Empty;
            this.outputRichTextBox.Clear();

            this.outputDockableWindow.Activate();
        }

        private void subtitleExporterComponent_ExportComplete( object sender, RunWorkerCompletedEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                this.Invoke( new RunWorkerCompletedEventHandler( subtitleExporterComponent_ExportComplete ), new object[] { sender, e } );
                return;
            }

            if ( e.Cancelled )
            {
                this.outputRichTextBox.AppendText( "Cancelled" );
            }
            else if ( (int)e.Result == 0 )
            {
                this.outputRichTextBox.AppendText( "Success!" );
            }
            else if ( e.Error != null )
            {
                this.outputRichTextBox.AppendText( String.Format( "Error:  {0}{1}", e.Error.ToString(), Environment.NewLine ) );
                this.outputRichTextBox.AppendText( String.Format( "Export Failed.  {0} error(s).{1}", (int)e.Result, Environment.NewLine ) );
            }
            else
            {
                this.outputRichTextBox.AppendText( String.Format( "Export Failed.  {0} error(s).{1}", (int)e.Result, Environment.NewLine ) );
            }

            this.toolStripStatusLabel.Text = string.Empty;
            this.toolStripStatusLabel.Tag = null;
        }

        private void subtitleExporterComponent_ExportMessage( object sender, ExportMessageEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                this.Invoke( new ExportMessageEventHandler( subtitleExporterComponent_ExportMessage ), new object[] { sender, e } );
                return;
            }

            this.outputRichTextBox.AppendText( String.Format( "{0}{1}", e.Message, Environment.NewLine ) );
        }

        private void subtitleExporterComponent_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                this.Invoke( new ProgressChangedEventHandler( subtitleExporterComponent_ProgressChanged ), new object[] { sender, e } );
                return;
            }

            string message = this.toolStripStatusLabel.Tag as string;
            if ( e.UserState != null )
            {
                message = e.UserState as string;
                this.toolStripStatusLabel.Tag = message;
            }

            if ( !String.IsNullOrEmpty( message ) )
            {
                this.toolStripStatusLabel.Text = String.Format( "{0}% - {1}", e.ProgressPercentage, message );
            }
            else
            {
                this.toolStripStatusLabel.Text = String.Format( "{0}%", e.ProgressPercentage );
            }
        }
        #endregion

        #region ScriptControl
        private void scriptControl_FileClosed( object sender, EventArgs e )
        {
            this.scriptDockableWindow.Text = this.scriptDockableWindow.TabText = this.scriptControl.Text;

            m_scriptControlModified = true;
            this.subtitleProjectComponent.IsModified = this.IsModified;
        }

        private void scriptControl_FileOpened( object sender, EventArgs e )
        {
            this.scriptDockableWindow.Text = this.scriptDockableWindow.TabText = this.scriptControl.Text;

            m_scriptControlModified = true;
            this.subtitleProjectComponent.IsModified = this.IsModified;
        }

        private void scriptControl_AddLocalizedText( object sender, AddLocalizedTextEventArgs e )
        {
            LocalizedTextData data = new LocalizedTextData();
            data.ID = this.localizedTextEditorControl.NextID();
            data.FontName = this.localizedTextEditorControl.LocalizedTextOptions.DefaultFontOption.Name;
            data.Language = this.localizedTextEditorControl.LocalizedTextOptions.DefaultLanguageOption.Language;
            data.Platform = this.localizedTextEditorControl.LocalizedTextOptions.DefaultPlatformOption.Name;
            data.Region = this.localizedTextEditorControl.LocalizedTextOptions.DefaultRegionOption.Name;
            data.Rtf = e.Text;
            data.Text = e.Text;

            this.localizedTextEditorControl.Add( data );
        }
        #endregion

        #region PreviewControl
        private void previewControl_CurrentTimeChanged( object sender, EventArgs e )
        {
            this.eventEditorControl.CurrentTime = (float)this.previewControl.CurrentTime;

            if ( this.localizedTextEditorControl.TextRenderer == null )
            {
                return;
            }

            foreach ( List<EventControl> eventCtrls in m_textDataHashToEventControl.Values )
            {
                if ( eventCtrls.Count > 0 )
                {
                    SubtitleEventData eventData = eventCtrls[0].Tag as SubtitleEventData;
                    ShowHideBitmap( eventData.LocalizedTextData, eventCtrls );
                }
            }
        }

        private void previewControl_FileClosed( object sender, EventArgs e )
        {
            this.previewTabbedDocument.Text = this.previewTabbedDocument.TabText = this.previewControl.Text;

            m_previewControlModified = true;
            this.subtitleProjectComponent.IsModified = this.IsModified;
        }

        private void previewControl_FileOpened( object sender, EventArgs e )
        {
            this.previewTabbedDocument.Text = this.previewTabbedDocument.TabText = this.previewControl.Text;

            this.eventEditorControl.SetDisplayRange( (float)this.previewControl.StartTime, (float)this.previewControl.EndTime );

            m_previewControlModified = true;
            this.subtitleProjectComponent.IsModified = this.IsModified;
        }

        private void previewControl_EndTimeChanged( object sender, EventArgs e )
        {
            this.eventEditorControl.SetDisplayRange( (float)this.previewControl.StartTime, (float)this.previewControl.EndTime );
        }

        private void previewControl_StartTimeChanged( object sender, EventArgs e )
        {
            this.eventEditorControl.SetDisplayRange( (float)this.previewControl.StartTime, (float)this.previewControl.EndTime );
        }
        #endregion

        #region LocalizedTextEditorControl
        private void localizedTextEditorControlContextMenuStripAddToTimelineToolStripMenuItem_Click( object sender, EventArgs e )
        {
            foreach ( GTLTreeNode treeNode in this.localizedTextEditorControl.TreeList.TreeList.SelectedNodes )
            {
                int index = treeNode.Index;
                if ( index < this.localizedTextEditorControl.TextDataCollection.Count )
                {
                    LocalizedTextData data = this.localizedTextEditorControl.TextDataCollection[index];
                    if ( !AddLocalizedTextDataToTimeline( data ) )
                    {
                        break;
                    }
                }
            }
        }

        private void localizedTextEditorControl_ModifiedChanged( object sender, EventArgs e )
        {
            m_localizedTextEditorControlModified = this.localizedTextEditorControl.IsModified;
            this.subtitleProjectComponent.IsModified = this.IsModified;
        }

        private void localizedTextEditorControl_TextDataAdded( object sender, LocalizedTextDataIndexEventArgs e )
        {
            
        }

		private void localizedTextEditorControl_TextDataPreChanged(object sender, LocalizedTextDataPreChangedEventArgs e)
		{
			switch (e.Member)
			{
				case LocalizedTextData.DataMember.Identifier:
				case LocalizedTextData.DataMember.Font:
				case LocalizedTextData.DataMember.Uppercase:
				case LocalizedTextData.DataMember.OffsetX:
				case LocalizedTextData.DataMember.OffsetY:
				case LocalizedTextData.DataMember.ScaleX:
				case LocalizedTextData.DataMember.ScaleY:
					break;
				case LocalizedTextData.DataMember.Speaker:
					{
						// update the EventEditorControl's track types and names
						string newSpeaker = e.NewStringValue;
						string oldSpeaker = e.PreviousStringValue;
						if (!String.IsNullOrEmpty(newSpeaker))
						{
							if (newSpeaker != oldSpeaker)
							{
								// Speaker changed, so move event from old track to new track, but if there is a clash, allow the user to cancel
								// Find the event in the old track
								TrackControl newTrack = null;
								TrackControl oldTrack = null;
								EventControl oldEventControl = null;
								foreach (TrackControl trackCtrl in this.eventEditorControl.TrackControls)
								{
									if (oldSpeaker == trackCtrl.TypeName)
									{
										// Found old track
										oldTrack = trackCtrl;

										// Get event
										foreach (EventControl eventCtrl in oldTrack.EventControls)
										{
											SubtitleEventData eventData = eventCtrl.Tag as SubtitleEventData;
											if (eventData.LocalizedTextData.ID == e.LocalizedTextData.ID)
											{
												// Found old event control
												oldEventControl = eventCtrl;
												break;
											}
										}
									}
									if (newSpeaker == trackCtrl.TypeName)
									{
										// Found old track
										newTrack = trackCtrl;
									}
								}

								if (newTrack == null)
								{
									// No new track, so no conflict and no need to cancel
									e.Cancelled = false;
									return;
								}

								if (oldTrack == null)
								{
									// No old track, so no times to conflict with and no need to cancel
									e.Cancelled = false;
									return;
								}

								if (oldEventControl == null)
								{
									// No old event control, so no times to conflict with and no need to cancel
									e.Cancelled = false;
									return;
								}

								// Does the new track already have events in that time range?
								List<EventControl> aobExistingEventsInOldEventsTimeRange = new List<EventControl>();
								foreach (EventControl eventCrtl in newTrack.EventControls)
								{
									if (
										((eventCrtl.MaxT < oldEventControl.MaxT) && (eventCrtl.MaxT > oldEventControl.MinT))
										||
										((eventCrtl.MinT < oldEventControl.MaxT) && (eventCrtl.MinT > oldEventControl.MinT))
										)
									{
										// Got a hit!
										aobExistingEventsInOldEventsTimeRange.Add(eventCrtl);
									}
								}

								if (aobExistingEventsInOldEventsTimeRange.Count > 0)
								{
									// Uh-oh, there is a clash, what ever will I do?  Ask the user...
									DialogResult result = rageMessageBox.ShowQuestion(this,
										"Moving event from " + oldSpeaker + " to " + newSpeaker + " causes a clash because " + newSpeaker + " already has subtitles within this time range.\nDelete existing " + newSpeaker + " subtitles?",
										"Subtitle Clash", MessageBoxButtons.OKCancel);

									if (result == DialogResult.OK)
									{
										// OK, so delete later
										e.Cancelled = false;
										return;
									}
									else if (result == DialogResult.Cancel)
									{
										// They clicked cancel, so cancel
										e.Cancelled = true;
										return;
									}
								}
							}
						}
					}
					break;
			}
		}

        private void localizedTextEditorControl_TextDataChanged( object sender, LocalizedTextDataChangedEventArgs e )
        {
            switch ( e.Member )
            {
                case LocalizedTextData.DataMember.Identifier:
                    {
                        // update the SubtitleEventData's LocalizedTextID
                        List<EventControl> eventCtrls;
                        if ( m_textDataHashToEventControl.TryGetValue( e.LocalizedTextData.GetHashCode(), out eventCtrls ) )
                        {
                            SubtitleEventData eventData = eventCtrls[0].Tag as SubtitleEventData;
                            Debug.Assert( eventData.LocalizedTextData == e.LocalizedTextData );
                            eventData.LocalizedTextID = eventData.LocalizedTextData.ID;
                        }
                    }
                    break;
                case LocalizedTextData.DataMember.Font:
                case LocalizedTextData.DataMember.Uppercase:
                case LocalizedTextData.DataMember.OffsetX:
                case LocalizedTextData.DataMember.OffsetY:
                case LocalizedTextData.DataMember.ScaleX:
                case LocalizedTextData.DataMember.ScaleY:
                    {
                        // refresh the bitmap
                        List<EventControl> eventCtrls;
                        if ( m_textDataHashToEventControl.TryGetValue( e.LocalizedTextData.GetHashCode(), out eventCtrls ) )
                        {
                            // Verification
                            foreach ( EventControl eventCtrl in eventCtrls )
                            {
                                SubtitleEventData eventData = eventCtrls[0].Tag as SubtitleEventData;
                                Debug.Assert( eventData.LocalizedTextData == e.LocalizedTextData );
                            }

                            RefreshBitmap( e.LocalizedTextData );
                        }
                    }
                    break;
                case LocalizedTextData.DataMember.String:
                    {
                        // update the EventEditorControl's Text and refresh the bitmap
                        List<EventControl> eventCtrls;
                        if ( m_textDataHashToEventControl.TryGetValue( e.LocalizedTextData.GetHashCode(), out eventCtrls ) )
                        {
                            SubtitleEventData eventData;

                            // Verification
                            foreach ( EventControl eventCtrl in eventCtrls )
                            {
                                eventData = eventCtrls[0].Tag as SubtitleEventData;
                                Debug.Assert( eventData.LocalizedTextData == e.LocalizedTextData );
                            }

                            eventData = eventCtrls[0].Tag as SubtitleEventData;
                            string text = eventData.LocalizedTextData.Text.Replace( "\r", " " );
                            text = text.Replace( "\n", " " );

                            foreach ( EventControl eventCtrl in eventCtrls )
                            {
                                eventCtrl.Text = text;
                                eventCtrl.TypeName = String.Format( "{0:0.00} WPM", CalculateWPM( eventCtrl.Text, eventCtrl.MinT, eventCtrl.MaxT ) );
                            }

                            RefreshBitmap( e.LocalizedTextData );
                        }
                    }
                    break;
                case LocalizedTextData.DataMember.Speaker:
                    {
                        // update the EventEditorControl's track types and names
                        string speaker = e.LocalizedTextData.GetToString( e.Member );
                        if ( !String.IsNullOrEmpty( speaker ) )
                        {
                            string oldSpeaker = e.PreviousStringValue;

                            if ( String.IsNullOrEmpty( oldSpeaker ) )
                            {
                                this.eventEditorControl.AddTrackType( speaker );
                            }
                            else if ( speaker != oldSpeaker )
                            {
								// Speaker changed, so move event from old track to new track
								// Find the event in the old track
								TrackControl newTrack = null;
								TrackControl oldTrack = null;
								EventControl oldEventControl = null;
								foreach (TrackControl trackCtrl in this.eventEditorControl.TrackControls)
								{
									if (oldSpeaker == trackCtrl.TypeName)
									{
										// Found old track
										oldTrack = trackCtrl;

										// Get event
										foreach (EventControl eventCtrl in oldTrack.EventControls)
										{
											SubtitleEventData eventData = eventCtrl.Tag as SubtitleEventData;
											if (eventData.LocalizedTextData.ID == e.LocalizedTextData.ID)
											{
												// Found old event control
												oldEventControl = eventCtrl;
												break;
											}
										}
									}
									if (speaker == trackCtrl.TypeName)
									{
										// Found old track
										newTrack = trackCtrl;
									}
								}

								// Got the old stuff, so create the new stuff
								if (newTrack == null)
								{
									// Create a new track for the new speaker
									newTrack = this.eventEditorControl.CreateTrack(speaker, speaker);
									this.eventEditorControl.AddTrack(newTrack);
								}

								if (oldEventControl == null)
								{
									// No old event control, so no times to conflict and no control to move, so bail
									return;
								}

								// Does the new track already have events in that time range?
								List<EventControl> aobExistingEventingIfOldEventsTimeRange = new List<EventControl>();
								if (oldEventControl != null)
								{
									// Have old event control, so I have possible clashing times
									foreach (EventControl eventCrtl in newTrack.EventControls)
									{
										if (
											((eventCrtl.MaxT < oldEventControl.MaxT) && (eventCrtl.MaxT > oldEventControl.MinT))
											||
											((eventCrtl.MinT < oldEventControl.MaxT) && (eventCrtl.MinT > oldEventControl.MinT))
											)
										{
											// Got a hit!
											aobExistingEventingIfOldEventsTimeRange.Add(eventCrtl);
										}
									}
								}

								// Delete existing events
								foreach (EventControl eventCrtl in aobExistingEventingIfOldEventsTimeRange)
								{
									newTrack.RemoveEvent(eventCrtl);
								}

								// Remove old event from the old track
								oldTrack.RemoveEvent(oldEventControl);

								// Add old event to the new track
								newTrack.AddEvent(oldEventControl);
                            }
                        }
                    }
                    break;
            }
        }

        private void localizedTextEditorControl_TextDataMoved( object sender, LocalizedTextDataMovedEventArgs e )
        {
            
        }

        private void localizedTextEditorControl_TextDataRemove( object sender, LocalizedTextDataCancelEventArgs e )
        {
            bool haveEvents = false;
            foreach ( LocalizedTextData data in e.TextDataCollection )
            {
                int hash = data.GetHashCode();
                if ( m_textDataHashToEventControl.ContainsKey( hash ) )
                {
                    haveEvents = true;
                    break;
                }
            }

            if ( haveEvents )
            {
                DialogResult result = rageMessageBox.ShowWarning( this,
                    "Removing the line(s) will also remove the associated Subtitle(s) from the Timeline.\n\nDo you wish to proceed?", 
                    "Remove Localized Text", MessageBoxButtons.YesNo );
                if ( result == DialogResult.No )
                {
                    e.Cancel = true;
                }
            }
        }

        private void localizedTextEditorControl_TextDataRemoved( object sender, LocalizedTextDataIndexEventArgs e )
        {
            int hash = e.LocalizedTextData.GetHashCode();

            List<EventControl> eventCtrls;
            if ( m_textDataHashToEventControl.TryGetValue( hash, out eventCtrls ) )
            {
                // remove every event that used the Localized Text
                while ( eventCtrls.Count > 0 )
                {
                    this.eventEditorControl.RemoveEvent( eventCtrls[0] );
                    
                    RemoveEventControlFromHash( e.LocalizedTextData, eventCtrls[0] );
                }

                // validate our track names and remove tracks that should no longer exist
                LocalizedTextDataCollection collection = this.localizedTextEditorControl.TextDataCollection;
                List<TrackControl> trackCtrls = this.eventEditorControl.TrackControls;
                foreach ( TrackControl trackCtrl in trackCtrls )
                {
                    bool found = false;
                    foreach ( LocalizedTextData data in collection )
                    {
                        if ( data.Speaker == trackCtrl.TypeName )
                        {
                            found = true;
                            break;
                        }
                    }

                    if ( !found )
                    {
                        this.eventEditorControl.RemoveTrackType( trackCtrl.TypeName );
                        this.eventEditorControl.RemoveTrack( trackCtrl );
                    }
                }
            }           
        }
        #endregion

        #region EventEditorControl
        private void eventEditorControl_EventCreate( object sender, EventCreateOnTrackEventArgs e )
        {
            SubtitleEventData eventData = new SubtitleEventData();
            DialogResult result = EditEventData( eventData, e.TrackControl.Text, true );
            if ( result == DialogResult.OK )
            {
                e.EventControl = AddSubtitleEventData( eventData, e.MinT, e.MaxT, e.TrackControl );
            }
        }

        private void eventEditorControl_EventDelete( object sender, EventOnTrackWithCancelEventArgs e )
        {
            SubtitleEventData eventData = e.EventControl.Tag as SubtitleEventData;
            RemoveEventControlFromHash( eventData.LocalizedTextData, e.EventControl );

            m_eventEditorControlModified = true;
            this.subtitleProjectComponent.IsModified = this.IsModified;
        }

        private void eventEditorControl_EventEdit( object sender, EventOnTrackEventArgs e )
        {
            SubtitleEventData eventData = e.EventControl.Tag as SubtitleEventData;

            LocalizedTextData oldLocalizedTextData = eventData.LocalizedTextData;

            DialogResult result = EditEventData( eventData, e.TrackControl.Text, false );
            if ( result == DialogResult.OK )
            {
                e.EventControl.Text = (eventData.LocalizedTextData != null) ? eventData.LocalizedTextData.Text : string.Empty;

                if ( eventData.LocalizedTextData != oldLocalizedTextData )
                {
                    if ( oldLocalizedTextData != null )
                    {
                        RemoveEventControlFromHash( oldLocalizedTextData, e.EventControl );
                    }

                    if ( eventData.LocalizedTextData != null )
                    {
                        AddEventControlToHash( eventData.LocalizedTextData, e.EventControl );
                    }
                }

                m_eventEditorControlModified = true;
                this.subtitleProjectComponent.IsModified = this.IsModified;
            }
        }

        private void eventEditorControl_EventMoved( object sender, EventOnTrackEventArgs e )
        {
            e.EventControl.TypeName = String.Format( "{0:0.00} WPM", CalculateWPM( e.EventControl.Text, e.EventControl.MinT, e.EventControl.MaxT ) );

            SubtitleEventData data = e.EventControl.Tag as SubtitleEventData;
            data.MinT = e.EventControl.MinT;
            data.MaxT = e.EventControl.MaxT;

            m_eventEditorControlModified = true;
            this.subtitleProjectComponent.IsModified = this.IsModified;
        }

        private void eventEditorControl_EventMoving( object sender, EventOnTrackEventArgs e )
        {
            e.EventControl.TypeName = String.Format( "{0:0.00} WPM", CalculateWPM( e.EventControl.Text, e.EventControl.MinT, e.EventControl.MaxT ) );

            SubtitleEventData data = e.EventControl.Tag as SubtitleEventData;
            data.MinT = e.EventControl.MinT;
            data.MaxT = e.EventControl.MaxT;
        }
        #endregion
        #endregion

        #region Private Functions
        private void LoadLayout()
        {
            // ensure the window handle has been created before setting the window position
            int handle = (int)this.Handle;

            // decide when to upgrade
            if ( Settings.Default.UpgradeNeeded == string.Empty )
            {
                Settings.Default.Upgrade();
            }

            try
            {
                // Form
                Point loc = Settings.Default.WindowLocation;
                Size size = Settings.Default.WindowSize;
                rageImage.AdjustForResolution( Settings.Default.ScreenSize, Screen.GetWorkingArea( this ), ref loc, ref size );

                this.Location = loc;
                this.Size = size;
                this.WindowState = Settings.Default.WindowState;

                this.Refresh();

                EnableEditing( false );

                if ( !String.IsNullOrEmpty( m_startupProjectFilename ) )
                {
                    LoadProject( m_startupProjectFilename );
                }
                else
                {
                    string filename = Settings.Default.SubtitleFile;
                    if ( !String.IsNullOrEmpty( filename ) )
                    {
                        LoadProject( filename );
                    }
                    else
                    {                        
                        filename = Settings.Default.SubtitleOptionsFilename;
                        if ( !String.IsNullOrEmpty( filename ) )
                        {
                            if ( LoadSubtitleOptions( filename ) )
                            {
                                this.subtitleProjectComponent.ProjectData.SubtitleOptionsFilename = m_subtitleOptions.CurrentFilename;
                                
                                m_subtitleProjectModified = false;
                            }
                        }

                        filename = Settings.Default.LocalizedTextOptionsFilename;
                        if ( !String.IsNullOrEmpty( filename ) )
                        {
                            if ( LoadLocalizedTextOptions( filename ) )
                            {
                                this.subtitleProjectComponent.ProjectData.LocalizedTextOptionsFilename = m_localizedTextOptions.CurrentFilename;

                                m_subtitleProjectModified = false;
                            }
                        }

                        this.subtitleProjectComponent.IsModified = this.IsModified;
                    }
                }

                System.Collections.Specialized.StringCollection recentProjects = Settings.Default.RecentProjects;
                if ( recentProjects != null )
                {
                    for ( int i = recentProjects.Count - 1; i >= 0; --i )
                    {
                        AddRecentProject( recentProjects[i] );
                    }
                }

                string appSettings = Settings.Default.AppSettings;
                if ( !String.IsNullOrEmpty( appSettings ) )
                {
                    rageStatus status;
                    m_appSettings.Deserialize( appSettings, out status );
                    if ( !status.Success() )
                    {
                        Console.WriteLine( status.ErrorString );
                        rageMessageBox.ShowExclamation( this, status.ErrorString, "AppSettings Deserialization Error" );
                    }
                    else
                    {
                        this.scriptControl.ScriptControlSettings = m_appSettings.ScriptControlSettings;
                        this.previewControl.PreviewControlSettings = m_appSettings.PreviewControlSettings;
                        m_sourceControlProviderSettings.CopyFrom( m_appSettings.SourceControlProviderSettings );
                    }
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.ToString() );
                rageMessageBox.ShowExclamation( this, e.Message, "Settings Load Error" );
            }
        }

        private void SaveLayout()
        {
            // set this so we know when we can call Upgrade()
            Settings.Default.UpgradeNeeded = "no";

            // Screen resolution
            Settings.Default.ScreenSize = Screen.GetWorkingArea( this ).Size;

            // Form
            Settings.Default.WindowSize = this.Size;
            Settings.Default.WindowLocation = this.Location;
            Settings.Default.WindowState = this.WindowState;

            // UI
            Settings.Default.SubtitleFile = this.subtitleProjectComponent.Filename;
            Settings.Default.LocalizedTextOptionsFilename = m_localizedTextOptions.CurrentFilename;

            System.Collections.Specialized.StringCollection recentProjects = new System.Collections.Specialized.StringCollection();
            foreach ( ToolStripMenuItem item in this.recentProjectsToolStripMenuItem.DropDownItems )
            {
                recentProjects.Add( item.Text );
            }
            Settings.Default.RecentProjects = recentProjects;

            m_appSettings.ScriptControlSettings = this.scriptControl.ScriptControlSettings;
            m_appSettings.PreviewControlSettings = this.previewControl.PreviewControlSettings;
            m_appSettings.SourceControlProviderSettings.CopyFrom( m_sourceControlProviderSettings );

            rageStatus status;
            Settings.Default.AppSettings = m_appSettings.Serialize( out status );
            if ( !status.Success() )
            {
                Console.WriteLine( status.ErrorString );
                rageMessageBox.ShowExclamation( this, status.ErrorString, "AppSettings Serialization Error" );
            }

            // now save it
            int retryCount = 10;
            do
            {
                try
                {
                    Settings.Default.Save();
                    break;
                }
                catch
                {
                    --retryCount;
                    System.Threading.Thread.Sleep( 100 );
                }
            }
            while ( retryCount > 0 );
        }

        private void LoadProject( string filename )
        {
            this.toolStripStatusLabel.Text = String.Format( "Loading {0}...", filename );

            // load the project data
            try
            {
                this.subtitleProjectComponent.LoadProject( filename );
            }
            catch ( Exception e )
            {
                rageMessageBox.ShowExclamation( this, e.Message, "Project File Load Error" );
                RemoveRecentProject( filename );
                return;
            }            

            if ( !String.IsNullOrEmpty( this.subtitleProjectComponent.ProjectData.SubtitleOptionsFilename ) )
            {
                string relativeFilename = this.subtitleProjectComponent.ProjectData.GetRelativePath( this.subtitleProjectComponent.ProjectData.SubtitleOptionsFilename );
                string absoluteFilename = this.subtitleProjectComponent.ProjectData.GetAbsolutePath( relativeFilename );

                if ( !String.IsNullOrEmpty( absoluteFilename ) )
                {
                    LoadSubtitleOptions( absoluteFilename );
                }
            }

            if ( !String.IsNullOrEmpty( this.subtitleProjectComponent.ProjectData.LocalizedTextOptionsFilename ) )
            {
                string relativeFilename = this.subtitleProjectComponent.ProjectData.GetRelativePath( this.subtitleProjectComponent.ProjectData.LocalizedTextOptionsFilename );
                string absoluteFilename = this.subtitleProjectComponent.ProjectData.GetAbsolutePath( relativeFilename );

                if ( !String.IsNullOrEmpty( absoluteFilename ) )
                {
                    LoadLocalizedTextOptions( absoluteFilename );
                }
            }

            // Set the LocalizedTextEditorControl's filename so NextId() uses our project's name.
            this.localizedTextEditorControl.Filename = Path.ChangeExtension( filename, ".txtproj" );

            // load the project's languages into the combo box
            PopulateLanguageComboBoxFromProject();
            
            // populate the UI with the project settings.  This will also try to select a language which will then populate the UI from the project data.
            PopulateUIFromProjectSettings();            

            // reset IsModified
            m_localizedTextEditorControlModified = m_eventEditorControlModified = m_previewControlModified
                = m_scriptControlModified = m_subtitleProjectModified = false;
            this.localizedTextEditorControl.IsModified = false;
            this.subtitleProjectComponent.IsModified = this.IsModified;

            this.toolStripStatusLabel.Text = string.Empty;

            // Set title text
            subtitleProjectComponent_ModifiedChanged( this, EventArgs.Empty );
        }       

        private void ReloadProject()
        {
            string filename = this.subtitleProjectComponent.Filename;

            CloseProject();

            LoadProject( filename );
        }

        private void SaveProject( string filename )
        {
            this.toolStripStatusLabel.Text = String.Format( "Saving {0}...", filename );

            // Set the LocalizedTextEditorControl's filename so NextId() uses our project's name.
            this.localizedTextEditorControl.Filename = Path.ChangeExtension( filename, ".txtproj" );

            this.localizedTextEditorControl.ValidateIdentifiers();

            // save the UI to the project data
            PopulateProjectDataFromUI();

            // save the UI to the project settings
            PopulateProjectSettingsFromUI();

            while ( true )
            {
                // save the project data
                try
                {
                    this.subtitleProjectComponent.SaveProject( filename );
                    break;
                }
                catch ( Exception e )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this,
                        e.Message, "Project File Save Error", MessageBoxButtons.RetryCancel );
                    if ( result == DialogResult.Cancel )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            // reset IsModified
            m_eventEditorControlModified = m_localizedTextEditorControlModified = m_previewControlModified
                = m_scriptControlModified = m_subtitleProjectModified = false;
            this.localizedTextEditorControl.IsModified = false;
            this.subtitleProjectComponent.IsModified = this.IsModified;

            this.toolStripStatusLabel.Text = string.Empty;
        }

        private void CloseProject()
        {
            this.toolStripStatusLabel.Text = "Closing Project...";

            if ( !String.IsNullOrEmpty( this.subtitleProjectComponent.Filename ) )
            {
                AddRecentProject( this.subtitleProjectComponent.Filename );
            }

            // always save the project settings
            PopulateProjectSettingsFromUI();
            this.subtitleProjectComponent.SaveProjectSettings();

            // clear the UI
            Clear();

            // clear the language combo box
            this.languageToolStripComboBox.Items.Clear();

            m_currentLanguage = string.Empty;

            // clear the project data
            this.subtitleProjectComponent.Clear();
            
            // reset the time
            this.previewControl.CurrentTime = 0.0M;

            EnableEditing( false );

            // Reset the LocalizedTextEditorControl's filename so NextId() doesn't use the last project's name.
            this.localizedTextEditorControl.Filename = string.Empty;

            // reset IsModified
            m_eventEditorControlModified = m_localizedTextEditorControlModified = m_previewControlModified
                = m_scriptControlModified = m_subtitleProjectModified = false;
            this.localizedTextEditorControl.IsModified = false;
            this.subtitleProjectComponent.IsModified = this.IsModified;

            this.toolStripStatusLabel.Text = string.Empty;

            // Reset the title text
            subtitleProjectComponent_ModifiedChanged( this, EventArgs.Empty );
        }

        private void PopulateLanguageComboBoxFromProject()
        {
            // clear the combo box
            this.languageToolStripComboBox.Items.Clear();

            m_currentLanguage = string.Empty;

            // populate it
            foreach ( SubtitleLanguageData item in this.subtitleProjectComponent.ProjectData.LanguageDataCollection )
            {
                this.languageToolStripComboBox.Items.Add( item.Language );
            }
        }

        private void PopulateUIFromProjectData()
        {
            bool isEventEditorControlModified = m_eventEditorControlModified;
            bool isLocalizedTextEditorControlModified = m_localizedTextEditorControlModified;
            bool isPreviewContolModified = m_previewControlModified;
            bool isScriptControlModified = m_scriptControlModified;
            bool isSubtitleProjectModified = m_subtitleProjectModified;

            this.subtitleProjectComponent.EnableModifications = false;

            Clear();

            m_currentLanguage = this.languageToolStripComboBox.SelectedItem as string;

            SubtitleLanguageData languageData = this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Find( m_currentLanguage );
            LocalizedTextLanguageOption languageOption = m_localizedTextOptions.LanguageOptionCollection.Find( m_currentLanguage );            

            if ( (languageData != null) && (languageOption != null) )
            {
                // load the language's script
                if ( !String.IsNullOrEmpty( languageData.ScriptFilename ) )
                {
                    string relativeFilename = this.subtitleProjectComponent.ProjectData.GetRelativePath( languageData.ScriptFilename );
                    string absoluteFilename = this.subtitleProjectComponent.ProjectData.GetAbsolutePath( relativeFilename );
                    
                    try
                    {
                        this.scriptControl.LoadFile( absoluteFilename );

                        this.scriptDockableWindow.Text = this.scriptDockableWindow.TabText = this.scriptControl.Text;
                    }
                    catch ( Exception e )
                    {
                        rageMessageBox.ShowExclamation( this, e.Message, "Script File Load Error" );
                    }
                }

                // load the language's media file
                if ( !String.IsNullOrEmpty( languageData.MediaFilename ) )
                {
                    string relativeFilename = this.subtitleProjectComponent.ProjectData.GetRelativePath( languageData.MediaFilename );
                    string absoluteFilename = this.subtitleProjectComponent.ProjectData.GetAbsolutePath( relativeFilename );

					if (!String.IsNullOrEmpty(absoluteFilename) && !File.Exists(absoluteFilename))
					{
						// Media file doesn't exist, so don't try to load it
						rageMessageBox.ShowWarning(this,
							"Unable to load media file " + absoluteFilename.Replace('\\', '/') + " as file does not exist.\nYou can continue to work but will not be able to preview until an existing media file is associated with this scene.",
							"Media File does not exist");
						languageData.MediaFilename = null;
					}
					else
					{
						try
						{
							this.previewControl.LoadFile(absoluteFilename);

							this.previewTabbedDocument.Text = this.previewTabbedDocument.TabText = this.previewControl.Text;
						}
						catch (Exception e)
						{
							rageMessageBox.ShowExclamation(this, e.Message, "Media File Load Error");
						}
					}
                }

                // populate the LocalizedTextEditorControl
                this.localizedTextEditorControl.Add( languageData.TextDataCollection );
                this.localizedTextEditorControl.LocalizedTextOptions = GenerateLocalizedTextOptionsForLanguage( languageOption );
                this.localizedTextEditorControl.IsModified = false;
                this.localizedTextEditorControl.ClearUndoRedoStack();


				this.eventEditorControl.SetDisplayRange((float)this.previewControl.StartTime, (float)this.previewControl.EndTime);
				this.eventEditorControl.SetDisplayView((float)this.previewControl.StartTime, (float)this.previewControl.EndTime);

                // populate the EventEditorControl
                foreach ( LocalizedTextData textData in languageData.TextDataCollection )
                {
                    // add the track type
                    this.eventEditorControl.AddTrackType( textData.Speaker );
                }

                foreach ( SubtitleTrackData trackData in languageData.TrackDataCollection )
                {
                    // create the track
                    TrackControl trackCtrl = this.eventEditorControl.CreateTrack( trackData.Speaker, trackData.Speaker );
                    if ( trackCtrl != null )
                    {
                        // add the events
                        foreach ( SubtitleEventData eventData in trackData.EventDataCollection )
                        {
                            LocalizedTextData textData = languageData.FindTextData( eventData.LocalizedTextID );
                            if ( textData != null )
                            {
                                eventData.LocalizedTextData = textData;

                                EventControl eventCtrl = trackCtrl.CreateEvent( textData.Text, trackData.Speaker, 
                                    eventData.MinT, eventData.MaxT, -1.0f );
                                if ( eventCtrl != null )
                                {
                                    eventCtrl.Tag = eventData;
                                    eventCtrl.TypeName = String.Format( "{0:0.00} WPM", CalculateWPM( eventCtrl.Text, eventCtrl.MinT, eventCtrl.MaxT ) );

                                    trackCtrl.AddEvent( eventCtrl );

                                    AddEventControlToHash( textData, eventCtrl );
                                }
                            }
                        }

                        this.eventEditorControl.AddTrack( trackCtrl );
                    }
                }
            }

            m_eventEditorControlModified = isEventEditorControlModified;
            m_localizedTextEditorControlModified = isLocalizedTextEditorControlModified;
            m_previewControlModified = isPreviewContolModified;
            m_scriptControlModified = isScriptControlModified;
            m_subtitleProjectModified = isSubtitleProjectModified;
            this.subtitleProjectComponent.EnableModifications = true;
        }

        private void PopulateProjectDataFromUI()
        {
            if ( String.IsNullOrEmpty( m_currentLanguage ) )
            {
                return;
            }

            // populate the language data
            SubtitleLanguageData languageData = this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Find( m_currentLanguage );
            if ( languageData == null )
            {
                languageData = new SubtitleLanguageData( m_currentLanguage );
                this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Add( languageData );
            }

            // save the script and media info
            languageData.DisplayMax = this.eventEditorControl.TimeLine.DisplayMax;
            languageData.DisplayMin = this.eventEditorControl.TimeLine.DisplayMin;
            languageData.MediaFilename = this.previewControl.Filename;
            languageData.PhaseMax = this.eventEditorControl.TimeLine.PhaseMax;
            languageData.PhaseMin = this.eventEditorControl.TimeLine.PhaseMin;
            languageData.ScriptFilename = this.scriptControl.Filename;
            
            // save the LocalizedTextData
            languageData.TextDataCollection.Clear();
            languageData.TextDataCollection.AddRange( this.localizedTextEditorControl.TextDataCollection );

            // save the event and track data
            languageData.TrackDataCollection.Clear();

            List<TrackControl> trackCtrls = this.eventEditorControl.TrackControls;
            foreach ( TrackControl trackCtrl in trackCtrls )
            {
                SubtitleTrackData trackData = new SubtitleTrackData();
                trackData.Speaker = trackCtrl.Text;
                
                List<EventControl> eventCtrls = trackCtrl.EventControls;
                foreach ( EventControl eventCtrl in eventCtrls )
                {
                    trackData.EventDataCollection.Add( eventCtrl.Tag as SubtitleEventData );
                }

                languageData.TrackDataCollection.Add( trackData );
            }          
        }

        private void PopulateUIFromProjectSettings()
        {
            // Make the project's source control settings our own
            m_sourceControlProviderSettings.CopyFrom( this.subtitleProjectComponent.ProjectSettings.SourceControlProviderSettings );

            // read the project settings to the UI
            this.previewControl.EndTime = this.subtitleProjectComponent.ProjectSettings.EndTime;
            this.previewControl.StartTime = this.subtitleProjectComponent.ProjectSettings.StartTime;
            this.previewControl.CurrentTime = this.subtitleProjectComponent.ProjectSettings.CurrentTime;

            this.localizedTextEditorControl.LocalizedTextEditorControlSettings
                = this.subtitleProjectComponent.ProjectSettings.LocalizedTextEditorControlSettings;
            this.eventEditorControl.EventEditorControlSettings = this.subtitleProjectComponent.ProjectSettings.EventEditorControlSettings;

            // try to select a current language
            int indexOf = -1;
            if ( !String.IsNullOrEmpty( this.subtitleProjectComponent.ProjectSettings.CurrentLanguage ) )
            {
                indexOf = this.languageToolStripComboBox.Items.IndexOf( this.subtitleProjectComponent.ProjectSettings.CurrentLanguage );
            }

            if ( indexOf != -1 )
            {
                this.languageToolStripComboBox.SelectedIndex = indexOf;
            }
            else if ( this.languageToolStripComboBox.Items.Count > 0 )
            {
                this.languageToolStripComboBox.SelectedIndex = 0;
            }
        }

        private void PopulateProjectSettingsFromUI()
        {
            // save the current source control settings to the project settings
            this.subtitleProjectComponent.ProjectSettings.SourceControlProviderSettings.CopyFrom( m_sourceControlProviderSettings );

            // save the UI to the project settings
            this.subtitleProjectComponent.ProjectSettings.CurrentLanguage = this.languageToolStripComboBox.SelectedItem as string;
            this.subtitleProjectComponent.ProjectSettings.CurrentTime = this.previewControl.CurrentTime;
            this.subtitleProjectComponent.ProjectSettings.StartTime = this.previewControl.StartTime;
            this.subtitleProjectComponent.ProjectSettings.EndTime = this.previewControl.EndTime;

            this.subtitleProjectComponent.ProjectSettings.LocalizedTextEditorControlSettings
                = this.localizedTextEditorControl.LocalizedTextEditorControlSettings;
            this.subtitleProjectComponent.ProjectSettings.EventEditorControlSettings = this.eventEditorControl.EventEditorControlSettings;
        }

        private void Clear()
        {                                   
            this.scriptControl.CloseFile();
            this.scriptDockableWindow.Text = this.scriptDockableWindow.TabText = this.scriptControl.Text;
            
            this.previewControl.ClearBitmaps();
            this.previewControl.CloseFile();
            this.previewTabbedDocument.Text = this.previewTabbedDocument.TabText = this.previewControl.Text;
            
            this.localizedTextEditorControl.Clear();
            this.eventEditorControl.Clear();

            this.m_textDataHashToEventControl.Clear();

            foreach ( Bitmap bmp in m_textDataHashToBitmap.Values )
            {
                bmp.Dispose();
            }

            m_textDataHashToBitmap.Clear();
        }

        private void ImportLocalizedText( string filename )
        {
            this.toolStripStatusLabel.Text = String.Format( "Importing {0}...", filename );

            LocalizedTextProjectData project = new LocalizedTextProjectData();

            while ( true )
            {
                rageStatus status;
                project.LoadFile( filename, out status );
                if ( !status.Success() )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, status.ErrorString, 
                        "Error Loading Localized Text Project", MessageBoxButtons.RetryCancel );
                    if ( result == DialogResult.Cancel )
                    {
                        this.toolStripStatusLabel.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    break;
                }
            }

            // save current state
            PopulateProjectDataFromUI();

            LocalizedTextDataConflictResolution resolution = LocalizedTextDataConflictResolution.KeepOriginal;
            bool applyAll = false;

            // FIXME: What to do with this list?
            LocalizedTextDataCollection textDataNoLanguageCollection = new LocalizedTextDataCollection();

            foreach ( LocalizedTextData textData in project.TextDataCollection )
            {
                if ( String.IsNullOrEmpty( textData.Language ) )
                {
                    // only import items assigned to a language
                    textDataNoLanguageCollection.Add( textData );
                    continue;
                }

                // only import the allowed languages
                if ( m_localizedTextOptions.LanguageOptionCollection.Find( textData.Language ) == null )
                {
                    continue;
                }

                SubtitleLanguageData languageData = this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Find( textData.Language );
                if ( languageData == null )
                {
                    languageData = new SubtitleLanguageData( textData.Language );
                    this.subtitleProjectComponent.ProjectData.LanguageDataCollection.Add( languageData );
                }

                DialogResult result = ResolveTextDataConflict( languageData, textData, ref resolution, ref applyAll );
                if ( result != DialogResult.OK )
                {
                    break;
                }
            }

            this.toolStripStatusLabel.Text = string.Empty;

            // update for any changes
            PopulateUIFromProjectData();
        }

        private void ExportLocalizedText( string filename )
        {
            this.toolStripStatusLabel.Text = String.Format( "Exporting {0}...", filename );

            PopulateProjectDataFromUI();

            LocalizedTextProjectData project = new LocalizedTextProjectData();
            project.LocalizedTextOptionsFilename = m_localizedTextOptions.CurrentFilename;

            // collect the text data for all languages
            foreach ( SubtitleLanguageData language in this.subtitleProjectComponent.ProjectData.LanguageDataCollection )
            {
                project.TextDataCollection.AddRange( language.TextDataCollection );
            }

            while ( true )
            {
                rageStatus status;

                if ( File.Exists( filename ) && m_sourceControlProviderSettings.IsSourceControlled( filename, out status ) )
                {
                    // check out the file
                    m_sourceControlProviderSettings.CheckOut( filename, out status );
                    if ( !status.Success() )
                    {
                        DialogResult result = rageMessageBox.ShowExclamation( this, status.ErrorString,
                            "Error Checking Out Localized Text File", MessageBoxButtons.RetryCancel );
                        if ( result == DialogResult.Cancel )
                        {
                            this.toolStripStatusLabel.Text = string.Empty;
                            return;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }

                // save the file
                project.SaveFile( filename, out status );
                if ( !status.Success() )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, status.ErrorString, 
                        "Error Saving Localized Text Project", MessageBoxButtons.RetryCancel );
                    if ( result == DialogResult.Cancel )
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    break;
                }
            }

            this.toolStripStatusLabel.Text = string.Empty;
        }

        private bool LoadSubtitleOptions( string filename )
        {
            this.toolStripStatusLabel.Text = String.Format( "Loading {0}...", filename );

            bool success = true;
            while ( true )
            {
                // load the settings file
                rageStatus status;
                m_subtitleOptions.LoadFile( filename, out status );
                if ( !status.Success() )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, status.ErrorString,
                        "Error Loading Subtitle Options File", MessageBoxButtons.RetryCancel );
                    if ( result == DialogResult.Cancel )
                    {
                        success = false;
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    break;
                }
            }

            this.toolStripStatusLabel.Text = string.Empty;

            return success;
        }

        private bool LoadLocalizedTextOptions( string filename )
        {
            this.toolStripStatusLabel.Text = String.Format( "Loading {0}...", filename );

            bool success = true;
            while ( true )
            {
                // load the settings file
                rageStatus status;
                m_localizedTextOptions.LoadFile( filename, out status );
                if ( !status.Success() )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, status.ErrorString, 
                        "Error Loading Localized Text Options File", MessageBoxButtons.RetryCancel );
                    if ( result == DialogResult.Cancel )
                    {
                        success = false;
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    InitializeFromLocalizedTextOptions();
                    break;
                }
            }

            this.toolStripStatusLabel.Text = string.Empty;

            return success;
        }

        private void InitializeFromLocalizedTextOptions()
        {
            if ( this.subtitleProjectComponent.ProjectData.LocalizedTextOptionsFilename != m_localizedTextOptions.CurrentFilename )
            {
                this.subtitleProjectComponent.ProjectData.LocalizedTextOptionsFilename = m_localizedTextOptions.CurrentFilename;

                m_subtitleProjectModified = true;
                this.subtitleProjectComponent.IsModified = this.IsModified;
            }

            // Only do this if we have a different Type
            m_localizedTextOptions.UnloadTextExporterPlugin();
            m_localizedTextOptions.LoadTextExporterPlugin();
            
            // Only do this if we have a different Type
            m_localizedTextOptions.UnloadTextRendererPlugin();
            m_localizedTextOptions.LoadTextRendererPlugin();
            
            this.localizedTextEditorControl.TextRenderer = m_localizedTextOptions.CreateTextRenderer();                

            if ( this.languageToolStripComboBox.SelectedItem != null )
            {
                LocalizedTextLanguageOption languageOption
                    = m_localizedTextOptions.LanguageOptionCollection.Find( this.languageToolStripComboBox.SelectedItem as string );
                if ( languageOption != null )
                {
                    this.localizedTextEditorControl.LocalizedTextOptions = GenerateLocalizedTextOptionsForLanguage( languageOption );
                }
            }

            // Always rebuild all of the bitmaps in case we changed a font or the screen resolution or something.
            foreach ( List<EventControl> eventCtrls in m_textDataHashToEventControl.Values )
            {
                SubtitleEventData eventData = eventCtrls[0].Tag as SubtitleEventData;
                RemoveBitmap( eventData.LocalizedTextData );
                AddBitmap( eventData.LocalizedTextData );
            }

            this.previewControl.Invalidate( true );
        }

        private void EnableEditing( bool enable )
        {            
            this.scriptControl.Enabled = enable;
            this.previewControl.Enabled = enable;
            this.localizedTextEditorControl.Enabled = enable;
            this.eventEditorControl.Enabled = enable;
            this.saveAsToolStripMenuItem.Enabled = enable;
            this.saveToolStripButton.Enabled = enable;
            this.saveToolStripMenuItem.Enabled = enable;
            this.importLocalizedTextToolStripMenuItem.Enabled = enable;
            this.exportLocalizedTextToolStripMenuItem.Enabled = enable;
            this.exportToolStripMenuItem.Enabled = enable;
        }

        private LocalizedTextOptions GenerateLocalizedTextOptionsForLanguage( LocalizedTextLanguageOption option )
        {
            LocalizedTextOptions settings = m_localizedTextOptions.Clone() as LocalizedTextOptions;

            settings.LanguageOptionCollection.Clear();
            settings.LanguageOptionCollection.Add( option );
            settings.DefaultLanguageOptionIndex = 0;
            
            return settings;
        }

        private DialogResult EditEventData( SubtitleEventData eventData, string speaker, bool newEvent )
        {
            LocalizedTextDataCollection trackTextDataCollection = new LocalizedTextDataCollection();

            // build a list of all text data that match the speaker and have an ID
            LocalizedTextDataCollection collection = this.localizedTextEditorControl.TextDataCollection;
            foreach ( LocalizedTextData item in collection )
            {
                if ( !String.IsNullOrEmpty( item.ID ) && (item.Speaker == speaker) )
                {
                    trackTextDataCollection.Add( item );
                }
            }

            if ( trackTextDataCollection.Count == 0 )
            {
                return DialogResult.Cancel;
            }

            // create and show the edit dialog
            EditEventDialog dialog = new EditEventDialog();            
            dialog.EventData = eventData;
            dialog.HelpNamespace = this.helpProvider.HelpNamespace;
            dialog.TextDataCollection = trackTextDataCollection;
            dialog.Text = newEvent ? "Add Event" : "Edit Event";

            List<string> transitionTypes = new List<string>();
            foreach ( SubtitleTransitionOption transtionOption in m_subtitleOptions.TransitionOptionCollection )
            {
                transitionTypes.Add( transtionOption.Name );
            }

            dialog.TransitionInTypes = transitionTypes;
            dialog.TransitionOutTypes = transitionTypes;

            return dialog.ShowDialog( this );
        }

        private DialogResult ResolveTextDataConflict( SubtitleLanguageData languageData, LocalizedTextData textData, 
            ref LocalizedTextDataConflictResolution resolution, ref bool applyAll )
        {
            LocalizedTextData originalTextData = languageData.TextDataCollection.Find( textData.Language, textData.ID );
            if ( originalTextData != null )
            {
                if ( applyAll )
                {
                    if ( resolution == LocalizedTextDataConflictResolution.ReplaceOriginal )
                    {
                        originalTextData.CopyFrom( textData );
                    }
                }
                else
                {
                    ResolveLocalizedTextConflictDialog dialog = new ResolveLocalizedTextConflictDialog();
                    dialog.HelpNamespace = this.helpProvider.HelpNamespace;

                    LocalizedTextLanguageOption languageOption 
                        = m_localizedTextOptions.LanguageOptionCollection.Find( this.languageToolStripComboBox.SelectedItem as string );
                    if ( languageOption != null )
                    {
                        dialog.LocalizedTextOptions = GenerateLocalizedTextOptionsForLanguage( languageOption );
                    }

                    dialog.NewTextData = textData;
                    dialog.OriginalTextData = originalTextData;

                    DialogResult result = dialog.ShowDialog( this );
                    if ( result == DialogResult.OK )
                    {                                                
                        if ( dialog.ApplyDecisionToAllRemainingItems )
                        {
                            resolution = dialog.LocalizedTextDataConflictResolution;
                            applyAll = true;
                        }

                        switch ( dialog.LocalizedTextDataConflictResolution )
                        {
                            case LocalizedTextDataConflictResolution.EditNewItem:
                                {
                                    textData.CopyFrom( dialog.NewTextData );

                                    // check against collisions again
                                    return ResolveTextDataConflict( languageData, textData, ref resolution, ref applyAll );
                                }
                            case LocalizedTextDataConflictResolution.KeepOriginal:
                                {
                                    // do nothing
                                }
                                break;
                            case LocalizedTextDataConflictResolution.ReplaceOriginal:
                                {
                                    originalTextData.CopyFrom( textData );
                                }
                                break;
                        }
                    }
                    else
                    {
                        return result;
                    }
                }
            }
            else
            {
                languageData.TextDataCollection.Add( textData );
            }

            return DialogResult.OK;
        }

        private bool AddLocalizedTextDataToTimeline( LocalizedTextData data )
        {
            SubtitleEventData eventData = new SubtitleEventData();
            eventData.LocalizedTextID = data.ID;
            eventData.LocalizedTextData = data;

            List<TrackControl> trackCtrls = this.eventEditorControl.TrackControls;
            foreach ( TrackControl trackCtrl in trackCtrls )
            {
                if ( trackCtrl.TypeName == data.Speaker )
                {
                    float minT = 0.0f;
                    float maxT = 0.01f;

                    List<EventControl> eventCtrls = trackCtrl.EventControls;
                    if ( eventCtrls.Count > 0 )
                    {
                        EventControl lastEventCtrl = eventCtrls[eventCtrls.Count - 1];
                        minT += lastEventCtrl.MaxT + 0.01f;
                        maxT += lastEventCtrl.MaxT + 0.01f;
                        if ( maxT > 1.0f )
                        {
                            maxT = 1.0f;
                        }

                        if ( minT >= maxT )
                        {
                            rageMessageBox.ShowExclamation( this.localizedTextEditorControl.TreeList, String.Format(
                                "There is not enough room for the Subtitle Event at the end of track '{0}'.\nPlease make room and try again.",
                                trackCtrl.TypeName ), "Add Localized Text To Timeline" );
                            return false;
                        }
                    }

                    EventControl eventCtrl = AddSubtitleEventData( eventData, minT, maxT, trackCtrl );
                    if ( eventCtrl != null )
                    {
                        this.eventEditorControl.AddEvent( eventCtrl, trackCtrl );
                    }

                    break;
                }
            }

            return true;
        }

        private EventControl AddSubtitleEventData( SubtitleEventData eventData, float minT, float maxT, TrackControl trackCtrl )
        {
            string text = (eventData.LocalizedTextData != null) ? eventData.LocalizedTextData.Text : string.Empty;
            text = text.Replace( "\r", " " );
            text = text.Replace( "\n", " " );

            string typeName = String.Format( "{0:0.00} WPM", CalculateWPM( text, minT, maxT ) );

            EventControl eventCtrl = this.eventEditorControl.CreateEvent( text, typeName, minT, maxT, -1.0f, trackCtrl );
            if ( eventCtrl != null )
            {
                eventData.MaxT = minT;
                eventData.MinT = maxT;
                eventCtrl.Tag = eventData;

                AddEventControlToHash( eventData.LocalizedTextData, eventCtrl );
            }

            m_eventEditorControlModified = true;
            this.subtitleProjectComponent.IsModified = this.IsModified;

            return eventCtrl;
        }

        private float CalculateWPM( string text, float minT, float maxT )
        {
            // recalculate WPM
            string[] split = text.Split( new char[] { ' ', '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries );

            float start = this.eventEditorControl.TimeLine.PhaseToDisplay( minT );
            float end = this.eventEditorControl.TimeLine.PhaseToDisplay( maxT );
            float timeInMinutes = (end - start) / 60.0f;

            return split.Length / timeInMinutes;
        }

        private void AddEventControlToHash( LocalizedTextData data, EventControl eventCtrl )
        {
            if ( data != null )
            {
                int hash = data.GetHashCode();

                // keep track of which EventControl(s) maps to which LocalizedTextData
                List<EventControl> eventCtrls;
                if ( !m_textDataHashToEventControl.TryGetValue( hash, out eventCtrls ) )
                {
                    eventCtrls = new List<EventControl>();
                    m_textDataHashToEventControl.Add( hash, eventCtrls );
                    
                    AddBitmap( data );
                }

                eventCtrls.Add( eventCtrl );

                ShowHideBitmap( data, eventCtrls );
            }
        }

        private void RemoveEventControlFromHash( LocalizedTextData data, EventControl eventCtrl )
        {
            int hash = data.GetHashCode();

            // remove the EventControl from the map
            List<EventControl> eventCtrls;
            if ( m_textDataHashToEventControl.TryGetValue( hash, out eventCtrls ) )
            {
                eventCtrls.Remove( eventCtrl );
                
                if ( eventCtrls.Count == 0 )
                {
                    m_textDataHashToEventControl.Remove( hash );
                    
                    RemoveBitmap( data );
                }

                ShowHideBitmap( data, eventCtrls );
            }
        }

        private void AddBitmap( LocalizedTextData data )
        {
            // Create the bitmap and add it to the PreviewControl
            Bitmap bmp = null;
            this.localizedTextEditorControl.TextRenderer.Render( data, this.localizedTextEditorControl.TextDataCollection,
                this.localizedTextEditorControl.LocalizedTextOptions, ref bmp );
            if ( bmp != null )
            {
                m_textDataHashToBitmap.Add( data.GetHashCode(), bmp );

                this.previewControl.AddBitmap( bmp );
            }
        }

        private void RemoveBitmap( LocalizedTextData data )
        {
            int hash = data.GetHashCode();

            // remove the Bitmap from the hash
            Bitmap bmp;
            if ( m_textDataHashToBitmap.TryGetValue( hash, out bmp ) )
            {
                this.previewControl.RemoveBitmap( bmp );

                m_textDataHashToBitmap.Remove( hash );
                bmp.Dispose();
            }
        }

        private void ShowHideBitmap( LocalizedTextData data, List<EventControl> eventCtrls )
        {
            bool show = false;
            foreach ( EventControl eventCtrl in eventCtrls )
            {
                float start = this.eventEditorControl.TimeLine.PhaseToDisplay( eventCtrl.MinT );
                float end = this.eventEditorControl.TimeLine.PhaseToDisplay( eventCtrl.MaxT );

                if ( (this.eventEditorControl.CurrentTime >= start) && (this.eventEditorControl.CurrentTime <= end) )
                {
                    show = true;
                    break;
                }
            }

            int hash = data.GetHashCode();
            Bitmap bmp = null;
            m_textDataHashToBitmap.TryGetValue( hash, out bmp );

            if ( show )
            {
                if ( bmp != null )
                {
                    this.previewControl.ShowBitmap( bmp );
                }
            }
            else if ( bmp != null )
            {
                this.previewControl.HideBitmap( bmp );
            }
        }

        private void RefreshBitmap( LocalizedTextData data )
        {
            int hash = data.GetHashCode();
            Bitmap bmp = null;
            m_textDataHashToBitmap.TryGetValue( hash, out bmp );

            this.localizedTextEditorControl.TextRenderer.Render( data, this.localizedTextEditorControl.TextDataCollection,
                this.localizedTextEditorControl.LocalizedTextOptions, ref bmp );
            if ( bmp != null )
            {
                this.previewControl.RefreshBitmap( bmp );
            }
        }

        private void AddRecentProject( string filename )
        {
            if ( !File.Exists( filename ) )
            {
                return;
            }

            string uppercaseFilename = filename.ToUpper();

            int indexOf = -1;
            for ( int i = 0; i < this.recentProjectsToolStripMenuItem.DropDownItems.Count; ++i )
            {
                if ( this.recentProjectsToolStripMenuItem.DropDownItems[i].Text.ToUpper() == uppercaseFilename )
                {
                    indexOf = i;
                    break;
                }
            }

            ToolStripItem item = null;
            if ( indexOf == -1 )
            {
                // Create a new menu item
                item = new ToolStripMenuItem( filename );
                item.Click += new EventHandler( recentProjectToolStripMenuItem_Click );                
            }
            else
            {
                // Remove the menu item from its current location
                item = this.recentProjectsToolStripMenuItem.DropDownItems[indexOf];
                this.recentProjectsToolStripMenuItem.DropDownItems.RemoveAt( indexOf );
            }

            // Insert at the top
            this.recentProjectsToolStripMenuItem.DropDownItems.Insert( 0, item );

            // Keep track of no more than 10
            if ( this.recentProjectsToolStripMenuItem.DropDownItems.Count > 10 )
            {
                this.recentProjectsToolStripMenuItem.DropDownItems.RemoveAt( 10 );
            }
        }

        private void RemoveRecentProject( string filename )
        {
            string uppercaseFilename = filename.ToUpper();

            for ( int i = 0; i < this.recentProjectsToolStripMenuItem.DropDownItems.Count; ++i )
            {
                if ( this.recentProjectsToolStripMenuItem.DropDownItems[i].Text.ToUpper() == uppercaseFilename )
                {
                    this.recentProjectsToolStripMenuItem.DropDownItems.RemoveAt( i );
                    break;
                }
            }
        }
        #endregion
    }
}