using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using LocalizedTextControls;
using SubtitleEditorCore;

namespace SubtitleEditor
{
    public partial class EditEventDialog : Form
    {
        public EditEventDialog()
        {
            InitializeComponent();
        }

        #region Variables
        private SubtitleEventData m_eventData;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;
            }
        }

        public SubtitleEventData EventData
        {
            get
            {
                return m_eventData;
            }
            set
            {
                m_eventData = value;

                if ( m_eventData != null )
                {
                    if ( m_eventData.LocalizedTextData != null )
                    {
                        this.localizedTextComboBox.SelectedIndex = this.localizedTextComboBox.Items.IndexOf( m_eventData.LocalizedTextData );
                    }

                    this.transitionInCheckBox.Checked = m_eventData.TransitionIn.Enabled;
                    this.transitionInComboBox.SelectedIndex = this.transitionInComboBox.Items.IndexOf( m_eventData.TransitionIn.TypeName );

                    try
                    {
                        this.transitionInNumericUpDown.Value = (decimal)m_eventData.TransitionIn.Duration;
                    }
                    catch
                    {
                        this.transitionInNumericUpDown.Value = 0.0M;
                    }

                    this.transitionOutCheckBox.Checked = m_eventData.TransitionOut.Enabled;
                    this.transitionOutComboBox.SelectedIndex = this.transitionOutComboBox.Items.IndexOf( m_eventData.TransitionOut.TypeName );

                    try
                    {
                        this.transitionOutNumericUpDown.Value = (decimal)m_eventData.TransitionOut.Duration;
                    }
                    catch
                    {
                        this.transitionOutNumericUpDown.Value = 0.0M;
                    }
                }
            }
        }

        public LocalizedTextDataCollection TextDataCollection
        {
            set
            {
                this.localizedTextComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( LocalizedTextData textData in value )
                    {
                        this.localizedTextComboBox.Items.Add( textData );
                    }

                    if ( (this.EventData != null) && (this.EventData.LocalizedTextData != null) )
                    {
                        this.localizedTextComboBox.SelectedIndex = this.localizedTextComboBox.Items.IndexOf( this.EventData.LocalizedTextData );
                    }
                }
            }
        }

        public List<string> TransitionInTypes
        {
            set
            {
                this.transitionInComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string transitionInType in value )
                    {
                        this.transitionInComboBox.Items.Add( transitionInType );
                    }

                    if ( this.EventData != null )
                    {
                        this.transitionInComboBox.SelectedIndex = this.transitionInComboBox.Items.IndexOf( this.EventData.TransitionIn.TypeName );
                    }
                }
            }
        }

        public List<string> TransitionOutTypes
        {
            set
            {
                this.transitionOutComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string transitionOutType in value )
                    {
                        this.transitionOutComboBox.Items.Add( transitionOutType );
                    }

                    if ( this.EventData != null )
                    {
                        this.transitionOutComboBox.SelectedIndex = this.transitionOutComboBox.Items.IndexOf( this.EventData.TransitionOut.TypeName );
                    }
                }
            }
        }
        #endregion

        #region Event Handlers
        private void transitionInCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.transitionInComboBox.Enabled = this.transitionInLabel.Enabled = this.transitionInNumericUpDown.Enabled
                = this.transitionInCheckBox.Checked;
        }

        private void transitionOutCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.transitionOutComboBox.Enabled = this.transitionOutLabel.Enabled = this.transitionOutNumericUpDown.Enabled
                = this.transitionOutCheckBox.Checked;
        }

        private void okButton_Click( object sender, EventArgs e )
        {
            if ( this.EventData != null )
            {
                this.EventData.LocalizedTextData = this.localizedTextComboBox.SelectedItem as LocalizedTextData;
                if ( this.EventData.LocalizedTextData != null )
                {
                    this.EventData.LocalizedTextID = this.EventData.LocalizedTextData.ID;
                }

                this.EventData.TransitionIn.Enabled = this.transitionInCheckBox.Checked;

                try
                {
                    this.EventData.TransitionIn.Duration = (float)this.transitionInNumericUpDown.Value;
                }
                catch
                {
                    this.EventData.TransitionIn.Duration = 0.0f;
                }

                this.EventData.TransitionIn.TypeName = this.transitionInComboBox.SelectedItem as string;

                this.EventData.TransitionOut.Enabled = this.transitionOutCheckBox.Checked;

                try
                {
                    this.EventData.TransitionOut.Duration = (float)this.transitionOutNumericUpDown.Value;
                }
                catch
                {
                    this.EventData.TransitionOut.Duration = 0.0f;
                }

                this.EventData.TransitionOut.TypeName = this.transitionOutComboBox.SelectedItem as string;
            }
        }
        #endregion
    }
}