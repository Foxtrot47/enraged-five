namespace SubtitleEditor
{
    partial class ResolveLocalizedTextConflictDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            LocalizedTextControls.DefaultLocalizedTextRenderer defaultLocalizedTextRenderer1 = new LocalizedTextControls.DefaultLocalizedTextRenderer();
            LocalizedTextControls.DefaultLocalizedTextRenderer defaultLocalizedTextRenderer2 = new LocalizedTextControls.DefaultLocalizedTextRenderer();
            this.instructionsLabel = new System.Windows.Forms.Label();
            this.applyAllCheckBox = new System.Windows.Forms.CheckBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.originalItemLabel = new System.Windows.Forms.Label();
            this.newItemLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.replaceOriginalRadioButton = new System.Windows.Forms.RadioButton();
            this.keepOriginalRadioButton = new System.Windows.Forms.RadioButton();
            this.editNewItemRadioButton = new System.Windows.Forms.RadioButton();
            this.newItemLocalizedTextEditorControl = new LocalizedTextControls.LocalizedTextEditorControl();
            this.originalItemLocalizedTextEditorControl = new LocalizedTextControls.LocalizedTextEditorControl();
            this.localizedTextEditorDialogComponent = new LocalizedTextControls.LocalizedTextEditorDialogComponent( this.components );
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // instructionsLabel
            // 
            this.instructionsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.instructionsLabel.AutoSize = true;
            this.instructionsLabel.Location = new System.Drawing.Point( 12, 9 );
            this.instructionsLabel.Name = "instructionsLabel";
            this.instructionsLabel.Size = new System.Drawing.Size( 828, 13 );
            this.instructionsLabel.TabIndex = 0;
            this.instructionsLabel.Text = "An item you are importing has the same ID and Language as an existing item.  You " +
                "may edit either item and accept them both, or select one of them to use instead " +
                "of the other.";
            // 
            // applyAllCheckBox
            // 
            this.applyAllCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applyAllCheckBox.AutoSize = true;
            this.applyAllCheckBox.Location = new System.Drawing.Point( 12, 284 );
            this.applyAllCheckBox.Name = "applyAllCheckBox";
            this.applyAllCheckBox.Size = new System.Drawing.Size( 216, 17 );
            this.applyAllCheckBox.TabIndex = 1;
            this.applyAllCheckBox.Text = "Apply this decision to all remaining items.";
            this.applyAllCheckBox.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 763, 280 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // originalItemLabel
            // 
            this.originalItemLabel.AutoSize = true;
            this.originalItemLabel.Location = new System.Drawing.Point( 9, 94 );
            this.originalItemLabel.Name = "originalItemLabel";
            this.originalItemLabel.Size = new System.Drawing.Size( 68, 13 );
            this.originalItemLabel.TabIndex = 6;
            this.originalItemLabel.Text = "Original Item:";
            // 
            // newItemLabel
            // 
            this.newItemLabel.AutoSize = true;
            this.newItemLabel.Location = new System.Drawing.Point( 12, 184 );
            this.newItemLabel.Name = "newItemLabel";
            this.newItemLabel.Size = new System.Drawing.Size( 55, 13 );
            this.newItemLabel.TabIndex = 8;
            this.newItemLabel.Text = "New Item:";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point( 682, 280 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 9;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // groupBox
            // 
            this.groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox.Controls.Add( this.replaceOriginalRadioButton );
            this.groupBox.Controls.Add( this.keepOriginalRadioButton );
            this.groupBox.Controls.Add( this.editNewItemRadioButton );
            this.groupBox.Location = new System.Drawing.Point( 12, 25 );
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size( 826, 49 );
            this.groupBox.TabIndex = 10;
            this.groupBox.TabStop = false;
            // 
            // replaceOriginalRadioButton
            // 
            this.replaceOriginalRadioButton.AutoSize = true;
            this.replaceOriginalRadioButton.Location = new System.Drawing.Point( 250, 19 );
            this.replaceOriginalRadioButton.Name = "replaceOriginalRadioButton";
            this.replaceOriginalRadioButton.Size = new System.Drawing.Size( 225, 17 );
            this.replaceOriginalRadioButton.TabIndex = 2;
            this.replaceOriginalRadioButton.Text = "Replace the original item with the new one";
            this.replaceOriginalRadioButton.UseVisualStyleBackColor = true;
            this.replaceOriginalRadioButton.CheckedChanged += new System.EventHandler( this.replaceOriginalRadioButton_CheckedChanged );
            // 
            // keepOriginalRadioButton
            // 
            this.keepOriginalRadioButton.AutoSize = true;
            this.keepOriginalRadioButton.Checked = true;
            this.keepOriginalRadioButton.Location = new System.Drawing.Point( 118, 19 );
            this.keepOriginalRadioButton.Name = "keepOriginalRadioButton";
            this.keepOriginalRadioButton.Size = new System.Drawing.Size( 126, 17 );
            this.keepOriginalRadioButton.TabIndex = 1;
            this.keepOriginalRadioButton.TabStop = true;
            this.keepOriginalRadioButton.Text = "Keep the original item";
            this.keepOriginalRadioButton.UseVisualStyleBackColor = true;
            this.keepOriginalRadioButton.CheckedChanged += new System.EventHandler( this.keepOriginalRadioButton_CheckedChanged );
            // 
            // editNewItemRadioButton
            // 
            this.editNewItemRadioButton.AutoSize = true;
            this.editNewItemRadioButton.Location = new System.Drawing.Point( 6, 19 );
            this.editNewItemRadioButton.Name = "editNewItemRadioButton";
            this.editNewItemRadioButton.Size = new System.Drawing.Size( 106, 17 );
            this.editNewItemRadioButton.TabIndex = 0;
            this.editNewItemRadioButton.Text = "Edit the new item";
            this.editNewItemRadioButton.UseVisualStyleBackColor = true;
            this.editNewItemRadioButton.CheckedChanged += new System.EventHandler( this.editNewItemRadioButton_CheckedChanged );
            // 
            // newItemLocalizedTextEditorControl
            // 
            this.newItemLocalizedTextEditorControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.newItemLocalizedTextEditorControl.CanAdd = false;
            this.newItemLocalizedTextEditorControl.CanMove = false;
            this.newItemLocalizedTextEditorControl.CanRemove = false;
            this.newItemLocalizedTextEditorControl.Enabled = false;
            this.newItemLocalizedTextEditorControl.Location = new System.Drawing.Point( 12, 200 );
            this.newItemLocalizedTextEditorControl.Name = "newItemLocalizedTextEditorControl";
            this.newItemLocalizedTextEditorControl.Size = new System.Drawing.Size( 828, 58 );
            this.newItemLocalizedTextEditorControl.TabIndex = 7;
            this.newItemLocalizedTextEditorControl.TextRenderer = defaultLocalizedTextRenderer1;
            this.newItemLocalizedTextEditorControl.TextDataChanged += new LocalizedTextControls.LocalizedTextDataChangedEventHandler( this.newItemLocalizedTextEditorControl_TextDataChanged );
            this.newItemLocalizedTextEditorControl.Enter += new System.EventHandler( this.newItemLocalizedTextEditorControl_Enter );
            // 
            // originalItemLocalizedTextEditorControl
            // 
            this.originalItemLocalizedTextEditorControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.originalItemLocalizedTextEditorControl.Enabled = false;
            this.originalItemLocalizedTextEditorControl.Location = new System.Drawing.Point( 12, 110 );
            this.originalItemLocalizedTextEditorControl.Name = "originalItemLocalizedTextEditorControl";
            this.originalItemLocalizedTextEditorControl.Size = new System.Drawing.Size( 828, 58 );
            this.originalItemLocalizedTextEditorControl.TabIndex = 5;
            this.originalItemLocalizedTextEditorControl.TextRenderer = defaultLocalizedTextRenderer2;
            this.originalItemLocalizedTextEditorControl.TextDataChanged += new LocalizedTextControls.LocalizedTextDataChangedEventHandler( this.originalItemLocalizedTextEditorControl_TextDataChanged );
            this.originalItemLocalizedTextEditorControl.Enter += new System.EventHandler( this.originalItemLocalizedTextEditorControl_Enter );
            // 
            // localizedTextEditorDialogComponent
            // 
            this.localizedTextEditorDialogComponent.LocalizedTextEditorControl = null;
            // 
            // ResolveLocalizedTextConflictDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 850, 315 );
            this.Controls.Add( this.groupBox );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.newItemLabel );
            this.Controls.Add( this.newItemLocalizedTextEditorControl );
            this.Controls.Add( this.originalItemLabel );
            this.Controls.Add( this.originalItemLocalizedTextEditorControl );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.applyAllCheckBox );
            this.Controls.Add( this.instructionsLabel );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ResolveLocalizedTextConflictDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import Localized Text Collision";
            this.groupBox.ResumeLayout( false );
            this.groupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label instructionsLabel;
        private System.Windows.Forms.CheckBox applyAllCheckBox;
        private System.Windows.Forms.Button cancelButton;
        private LocalizedTextControls.LocalizedTextEditorControl originalItemLocalizedTextEditorControl;
        private System.Windows.Forms.Label originalItemLabel;
        private LocalizedTextControls.LocalizedTextEditorControl newItemLocalizedTextEditorControl;
        private System.Windows.Forms.Label newItemLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.RadioButton replaceOriginalRadioButton;
        private System.Windows.Forms.RadioButton keepOriginalRadioButton;
        private System.Windows.Forms.RadioButton editNewItemRadioButton;
        private LocalizedTextControls.LocalizedTextEditorDialogComponent localizedTextEditorDialogComponent;
        private System.Windows.Forms.HelpProvider helpProvider;
    }
}