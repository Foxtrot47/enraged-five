using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using LocalizedTextControls;

namespace SubtitleEditor
{
    public partial class ResolveLocalizedTextConflictDialog : Form
    {
        public ResolveLocalizedTextConflictDialog()
        {
            InitializeComponent();
        }

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;

                this.originalItemLocalizedTextEditorControl.HelpProvider = this.helpProvider;
                this.newItemLocalizedTextEditorControl.HelpProvider = this.helpProvider;
            }
        }

        /// <summary>
        /// No clones.
        /// </summary>
        public LocalizedTextData OriginalTextData
        {
            get
            {
                return this.originalItemLocalizedTextEditorControl.TextDataCollection[0];
            }
            set
            {
                this.originalItemLocalizedTextEditorControl.Clear();
                if ( value != null )
                {
                    this.originalItemLocalizedTextEditorControl.Add( value );
                }
            }
        }

        /// <summary>
        /// Set makes a clone.  Returns that clone.
        /// </summary>
        public LocalizedTextData NewTextData
        {
            get
            {
                return this.newItemLocalizedTextEditorControl.TextDataCollection[0];
            }
            set
            {
                this.newItemLocalizedTextEditorControl.Clear();
                if ( value != null )
                {
                    this.newItemLocalizedTextEditorControl.Add( value.Clone() as LocalizedTextData );
                }
            }
        }

        public LocalizedTextOptions LocalizedTextOptions
        {
            set
            {
                if ( value != null )
                {
                    this.originalItemLocalizedTextEditorControl.LocalizedTextOptions = value;
                    this.newItemLocalizedTextEditorControl.LocalizedTextOptions = value;
                }
            }
        }

        public LocalizedTextDataConflictResolution LocalizedTextDataConflictResolution
        {
            get
            {
                if ( this.editNewItemRadioButton.Checked )
                {
                    return LocalizedTextDataConflictResolution.EditNewItem;
                }
                else if ( this.replaceOriginalRadioButton.Checked )
                {
                    return LocalizedTextDataConflictResolution.ReplaceOriginal;
                }
                else
                {
                    return LocalizedTextDataConflictResolution.KeepOriginal;
                }
            }
        }

        public bool ApplyDecisionToAllRemainingItems
        {
            get
            {
                return (this.LocalizedTextDataConflictResolution == LocalizedTextDataConflictResolution.EditNewItem) 
                    ? false : this.applyAllCheckBox.Checked;
            }
        }
        #endregion

        #region Event Handlers
        private void editNewItemRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            EnableControls();
        }

        private void keepOriginalRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            EnableControls();
        }

        private void replaceOriginalRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            EnableControls();
        }

        private void originalItemLocalizedTextEditorControl_TextDataChanged( object sender, LocalizedTextDataChangedEventArgs e )
        {
            if ( (e.Member == LocalizedTextData.DataMember.Identifier) || (e.Member == LocalizedTextData.DataMember.Language) )
            {
                EnableControls();
            }
        }

        private void newItemLocalizedTextEditorControl_TextDataChanged( object sender, LocalizedTextDataChangedEventArgs e )
        {
            if ( (e.Member == LocalizedTextData.DataMember.Identifier) || (e.Member == LocalizedTextData.DataMember.Language) )
            {
                EnableControls();
            }
        }

        private void originalItemLocalizedTextEditorControl_Enter( object sender, EventArgs e )
        {
            if ( !this.localizedTextEditorDialogComponent.DialogOpen )
            {
                this.localizedTextEditorDialogComponent.LocalizedTextEditorControl = this.originalItemLocalizedTextEditorControl;
            }
        }

        private void newItemLocalizedTextEditorControl_Enter( object sender, EventArgs e )
        {
            if ( !this.localizedTextEditorDialogComponent.DialogOpen )
            {
                this.localizedTextEditorDialogComponent.LocalizedTextEditorControl = this.newItemLocalizedTextEditorControl;
            }
        }
        #endregion

        #region Private Functions
        private void EnableControls()
        {
            this.newItemLocalizedTextEditorControl.Enabled = this.editNewItemRadioButton.Checked;
            this.applyAllCheckBox.Enabled = !this.editNewItemRadioButton.Checked;

            if ( this.editNewItemRadioButton.Checked )
            {
                this.okButton.Enabled = this.OriginalTextData.ID != this.NewTextData.ID;
            }
            else
            {
                this.okButton.Enabled = true;
            }
        }
        #endregion
    }

    public enum LocalizedTextDataConflictResolution
    {
        EditNewItem,
        KeepOriginal,
        ReplaceOriginal
    }
}