using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using RSG.Base;
using RSG.Base.Forms;
using RSG.Base.IO;

using SubtitleEditor.Properties;

namespace SubtitleEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main( string[] args )
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler( Application_ThreadException );

            rageCommandLineItem projectItem = new rageCommandLineItem( string.Empty, 1, false,
                "filename", "The filename of the subtitle project to load at startup." );
            rageCommandLineItem adminItem = new rageCommandLineItem( "admin", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[true|false]", "Starts the Editor in administration mode, allowing the user to access the Localized Text Options and Subtitle Options editing dialogs." );

            rageCommandLineParser parser = new rageCommandLineParser();
            parser.AddItem( projectItem );
            parser.AddItem( adminItem );

            if ( !parser.Parse( args ) )
            {
                Console.WriteLine( parser.Usage );
                return -1;
            }

            bool admin = false;
            if ( adminItem.WasSet )
            {
                string val = (adminItem.Value as string).ToUpper();
                if ( (val == "FALSE") || (val == "F") || (val == "0") )
                {
                    admin = false;
                }
                else
                {
                    admin = true;
                }
            }

            try
            {
                if ( projectItem.WasSet )
                {
                    Application.Run( new Form1( projectItem.Value as string, admin ) );
                }
                else
                {
                    Application.Run( new Form1( admin ) );
                }

                return 0;
            }
            catch ( System.Exception e )
            {
                Application_ThreadException( null, new System.Threading.ThreadExceptionEventArgs( e ) );

                return 1;
            }
        }

        static void Application_ThreadException( object sender, System.Threading.ThreadExceptionEventArgs e )
        {
            List<string> attachments = new List<string>();

            string userConfigFilename = rageFileUtilities.GetRageApplicationUserConfigFilename( "SubtitleEditor.exe" );
            if ( File.Exists( userConfigFilename ) )
            {
                attachments.Add( userConfigFilename );
            }

            UnhandledExceptionDialog eForm = new UnhandledExceptionDialog( "Subtitle Editor", attachments,
                Settings.Default.ExceptionToEmailAddress, Settings.Default.ExceptionFromEmailAddress, e.Exception );
            eForm.ShowDialog();

            Settings.Default.ExceptionFromEmailAddress = eForm.YourEmailAddress;

            Form1.ExitingAfterException = true;

            try
            {
                Application.Exit();
            }
            catch ( Exception ex )
            {
                Console.WriteLine( "Application.Exit() failed: " + ex.Message );
                Application.Exit();
            }
        }
    }
}