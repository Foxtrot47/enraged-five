using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

using LocalizedTextControls;
using RSG.Base;
using RSG.Base.Command;
using SubtitleEditorCore;

namespace SubtitleExporter
{
    class Program
    {
        static int Main( string[] args )
        {
            rageCommandLineItem projectItem = new rageCommandLineItem( string.Empty, 1, true, 
                "filename", "The filename of the subtitle project to export." );
            rageCommandLineItem cutSubOutputPathItem = new rageCommandLineItem( "cutsubPath", 1, false, 
                "directory", "Override the project's 'scene.cutsub' output path.  Default:  %RAGE_ASSET_ROOT%\\tune\\cutscene" );
            rageCommandLineItem textOutputPathItem = new rageCommandLineItem( "txtPath", 1, false,
                "directory", "Override the project's 'stringtable.txt' output path.  Default:  project directory." );
            rageCommandLineItem strtblPathItem = new rageCommandLineItem( "strtblPath", 1, false,
                "directory", "Override the project's 'stringtable.strtbl' output path.  Default:  project directory." );
            rageCommandLineItem makeStrtblItem = new rageCommandLineItem( "makeStrtbl", rageCommandLineItem.NumValues.NoneOrOne, false, 
                "[true|false]", "Override the project's make string table setting." );
            rageCommandLineItem logFileItem = new rageCommandLineItem( "log", rageCommandLineItem.NumValues.NoneOrOne, false, 
                "[filename]", "The log file to write the output to.  Default:  log.txt" );

            rageCommandLineParser parser = new rageCommandLineParser();
            parser.AddItem( projectItem );
            parser.AddItem( cutSubOutputPathItem );
            parser.AddItem( textOutputPathItem );
            parser.AddItem( strtblPathItem );
            parser.AddItem( makeStrtblItem );
            parser.AddItem( logFileItem );

            if ( !parser.Parse( args ) )
            {
                Console.WriteLine( parser.Usage );
                return -1;
            }

            if ( logFileItem.WasSet )
            {
                OpenLog( logFileItem.Value as string );
            }
            
            // load the subtitle project
            SubtitleProjectComponent projectComponent = new SubtitleProjectComponent();
            try
            {
                projectComponent.LoadProject( projectItem.Value as string );
            }
            catch ( Exception e )
            {
                WriteOutput( e.ToString() );
                CloseLog( projectItem.Value as string );
                return 1;
            }

            // load the subtitle options
            SubtitleOptions subtitleOptions = new SubtitleOptions();
            string filename = projectComponent.ProjectData.SubtitleOptionsFilename;
            if ( !String.IsNullOrEmpty( filename ) )
            {
                rageStatus status;
                subtitleOptions.LoadFile( filename, out status );
                if ( !status.Success() )
                {
                    WriteOutput( status.ErrorString );
                    CloseLog( projectItem.Value as string );
                    return 1;
                }
            }

            // load the localized text options
            LocalizedTextOptions textOptions = new LocalizedTextOptions();
            filename = projectComponent.ProjectData.LocalizedTextOptionsFilename;
            if ( !String.IsNullOrEmpty( filename ) )
            {
                rageStatus status;
                textOptions.LoadFile( filename, out status );
                if ( !status.Success() )
                {
                    WriteOutput( status.ErrorString );
                    CloseLog( projectItem.Value as string );
                    return 1;
                }
            }

            textOptions.UnloadTextExporterPlugin();
            textOptions.LoadTextExporterPlugin();

            textOptions.UnloadTextRendererPlugin();
            textOptions.LoadTextRendererPlugin();

            if ( cutSubOutputPathItem.WasSet )
            {
                projectComponent.ProjectData.ExporterSettings.CutsubExportPath = cutSubOutputPathItem.Value as string;
            }

            if ( textOutputPathItem.WasSet )
            {
                projectComponent.ProjectData.ExporterSettings.TextExporterSettings.StringTableTextOutputPath = textOutputPathItem.Value as string;
            }

            if ( strtblPathItem.WasSet )
            {
                projectComponent.ProjectData.ExporterSettings.TextExporterSettings.StringTableOutputPath = strtblPathItem.Value as string;
            }

            if ( makeStrtblItem.WasSet )
            {
                string val = (makeStrtblItem.Value as string).ToUpper();
                if ( (val == "FALSE") || (val == "F") || (val == "0") )
                {
                    projectComponent.ProjectData.ExporterSettings.TextExporterSettings.MakeStringTable = false;
                }
                else
                {
                    projectComponent.ProjectData.ExporterSettings.TextExporterSettings.MakeStringTable = true;
                }
            }

            // create the exporter component
            SubtitleExporterComponent exporterComponent = new SubtitleExporterComponent();
            exporterComponent.CheckOutFile += new SourceControlEventHandler( exporterComponent_CheckOutFile );
            exporterComponent.ExportComplete += new System.ComponentModel.RunWorkerCompletedEventHandler( exporterComponent_ExportComplete );
            exporterComponent.ExportMessage += new ExportMessageEventHandler( exporterComponent_ExportMessage );
            exporterComponent.LocalizedTextOptions = textOptions;
            exporterComponent.SubtitleOptions = subtitleOptions;
            exporterComponent.SubtitleProjectData = projectComponent.ProjectData;

            // do the export
            exporterComponent.Export();

            // wait till finished
            while ( !sm_finished )
            {
                Thread.Sleep( 10 );
            }

            CloseLog( projectItem.Value as string );
            return sm_exitCode;
        }

        #region Variables
        static private string sm_tempLogFilename = null;
        static private string sm_logFilename = null;
        static private bool sm_finished = false;
        static private int sm_exitCode = 0;
        static private TextWriter sm_writer = null;
        #endregion

        #region Event Handlers
        static void exporterComponent_CheckOutFile( object sender, SourceControlEventArgs e )
        {
            // no file checkout, just make sure it is writable
            if ( File.Exists( e.Filename ) )
            {
                try
                {
                    FileAttributes attributes = File.GetAttributes( e.Filename );
                    if ( (attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                    {
                        File.SetAttributes( e.Filename, File.GetAttributes( e.Filename ) & (~FileAttributes.ReadOnly) );
                    }
                }
                catch ( Exception ex )
                {
                    WriteOutput( String.Format( "Error trying to make '{0}' writable: {1}", e.Filename, ex.Message ) );
                }
            }
        }

        static void exporterComponent_ExportComplete( object sender, System.ComponentModel.RunWorkerCompletedEventArgs e )
        {
            if ( e.Cancelled )
            {
                WriteOutput( "Cancelled" );
                sm_exitCode = -1;
            }
            else if ( (int)e.Result == 0 )
            {
                WriteOutput( "Success!" );
                sm_exitCode = 0;
            }
            else if ( e.Error != null )
            {
                WriteOutput( String.Format( "Error:  {0}", e.Error.ToString() ) );
                WriteOutput( String.Format( "Export Failed.  {0} error(s).", (int)e.Result ) );
                sm_exitCode = 1;
            }
            else
            {
                WriteOutput( String.Format( "Export Failed.  {0} error(s).", (int)e.Result ) );
                sm_exitCode = 1;
            }

            sm_finished = true;
        }

        static void exporterComponent_ExportMessage( object sender, ExportMessageEventArgs e )
        {
            WriteOutput( e.Message );
        }
        #endregion

        #region Private Functions
        private static void OpenLog( string filename )
        {
            try
            {
                sm_logFilename = filename;
                sm_tempLogFilename = Path.GetTempFileName();

                sm_writer = new StreamWriter( sm_tempLogFilename, false );
            }
            catch ( Exception e )
            {
                if ( sm_writer != null )
                {
                    sm_writer.Close();
                    sm_writer = null;
                }

                WriteOutput( String.Format( "Error: Unable to create the temporary log file '{0}'.", sm_tempLogFilename ) );
                WriteOutput( e.ToString() );
            }
        }

        private static void CloseLog( string projectFilename )
        {
            if ( sm_writer != null )
            {
                sm_writer.Close();
                sm_writer = null;

                if ( String.IsNullOrEmpty( sm_logFilename ) )
                {
                    sm_logFilename = Path.Combine( Path.GetDirectoryName( projectFilename ), "log.txt" );
                }

                try
                {
                    File.Copy( sm_tempLogFilename, sm_logFilename, true );
                    File.Delete( sm_tempLogFilename );
                }
                catch ( Exception e )
                {
                    WriteOutput( e.ToString() );
                }
            }
        }

        private static void WriteOutput( string message )
        {
            Console.WriteLine( message );

            if ( sm_writer != null )
            {
                sm_writer.WriteLine( message );
            }
        }
        #endregion
    }
}
