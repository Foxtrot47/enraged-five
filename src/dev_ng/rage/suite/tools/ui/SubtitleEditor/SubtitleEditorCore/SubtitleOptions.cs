using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Serialization;

namespace SubtitleEditorCore
{
    /// <summary>
    /// Holds the options that determine the available Transitions.
    /// </summary>
    public class SubtitleOptions : rageSerializableObject
    {
        public SubtitleOptions()
        {

        }

        #region Variables
        private SubtitleTransitionOptionCollection m_transitionOptionCollection = new SubtitleTransitionOptionCollection();

        private int m_defaultTransitionOptionIndex = -1;
        #endregion

        #region Properties
        /// <summary>
        /// The collection of LocalizedTextFontOptions.
        /// </summary>
        [Description( "The collection of LocalizedTextFontOptions." )]
        public SubtitleTransitionOptionCollection TransitionOptionCollection
        {
            get
            {
                return m_transitionOptionCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_transitionOptionCollection = value;
                }
                else
                {
                    m_transitionOptionCollection.Reset();
                }
            }
        }

        /// <summary>
        /// The index of the default SubtitleTransitionOption in TransitionOptionCollection.
        /// </summary>
        [DefaultValue( -1 ), Description( "The index of the default SubtitleTransitionOption in TransitionOptionCollection." )]
        public int DefaultTransitionOptionIndex
        {
            get
            {
                return m_defaultTransitionOptionIndex;
            }
            set
            {
                m_defaultTransitionOptionIndex = value;
            }
        }

        [XmlIgnore, ReadOnly( true ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public SubtitleTransitionOption DefaultLanguageOption
        {
            get
            {
                if ( (this.DefaultTransitionOptionIndex >= 0) && (this.DefaultTransitionOptionIndex < this.TransitionOptionCollection.Count) )
                {
                    return this.TransitionOptionCollection[this.DefaultTransitionOptionIndex];
                }
                else
                {
                    return new SubtitleTransitionOption();
                }
            }
            set
            {
                if ( value != null )
                {
                    this.DefaultTransitionOptionIndex = this.TransitionOptionCollection.IndexOf( value );
                }
                else
                {
                    this.DefaultTransitionOptionIndex = -1;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleOptions )
            {
                return Equals( obj as SubtitleOptions );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            SubtitleOptions options = new SubtitleOptions();
            options.CopyFrom( this );
            return options;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleOptions )
            {
                CopyFrom( other as SubtitleOptions );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.DefaultTransitionOptionIndex = -1;
            this.TransitionOptionCollection.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleOptions other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.DefaultTransitionOptionIndex = other.DefaultTransitionOptionIndex;
            this.TransitionOptionCollection.CopyFrom( other.TransitionOptionCollection );
        }

        public bool Equals( SubtitleOptions other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return (this.DefaultTransitionOptionIndex == other.DefaultTransitionOptionIndex)
                && this.TransitionOptionCollection.Equals( other.TransitionOptionCollection );
        }
        #endregion
    }
}
