using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using EventEditorControls;
using LocalizedTextControls;
using RSG.Base.Serialization;
using RSG.Base.SCM;

namespace SubtitleEditorCore
{
    /// <summary>
    /// Holds the per-user collection of project settings such as source control settings.
    /// </summary>
    public class SubtitleProjectSettings : rageSerializableObject
    {
        public SubtitleProjectSettings()
        {

        }

        #region Variables
        private string m_currentLanguage = string.Empty;

        private decimal m_currentTime = 0.0M;
        private decimal m_startTime = 0.0M;
        private decimal m_endTime = 0.0M;

        private LocalizedTextEditorControlSettings m_localizedTextEditorControlSettings = new LocalizedTextEditorControlSettings();
        private EventEditorControlSettings m_eventEditorControlSettings = new EventEditorControlSettings();

        private rageSourceControlProviderSettings m_sourceControlProviderSettings = new rageSourceControlProviderSettings();
        #endregion

        #region Properties
        /// <summary>
        /// The current language that is selected.
        /// </summary>
        [DefaultValue( "" ), Description( "The current language that is selected." )]
        public string CurrentLanguage
        {
            get
            {
                return m_currentLanguage;
            }
            set
            {
                if ( value != null )
                {
                    m_currentLanguage = value;
                }
                else
                {
                    m_currentLanguage = string.Empty;
                }
            }
        }

        /// <summary>
        /// The current time of the PreviewControl.
        /// </summary>
        [DefaultValue( 0 ), Description( "The current time of the PreviewControl." )]
        public decimal CurrentTime
        {
            get
            {
                return m_currentTime;
            }
            set
            {
                m_currentTime = value;
                if ( m_currentTime < 0.0M )
                {
                    m_currentTime = 0.0M;
                }
            }
        }

        /// <summary>
        /// The playback start time of the PreviewControl.
        /// </summary>
        [DefaultValue( 0 ), Description( "The playback start time of the PreviewControl." )]
        public decimal StartTime
        {
            get
            {
                return m_startTime;
            }
            set
            {
                m_startTime = value;
                if ( m_startTime < 0.0M )
                {
                    m_startTime = 0.0M;
                }
            }
        }

        /// <summary>
        /// The playback end time of the PreviewControl.
        /// </summary>
        [DefaultValue( 0 ), Description( "The playback end time of the PreviewControl." )]
        public decimal EndTime
        {
            get
            {
                return m_endTime;
            }
            set
            {
                m_endTime = value;
                if ( m_endTime < 0.0M )
                {
                    m_endTime = 0.0M;
                }
            }
        }

        /// <summary>
        /// The settings for the LocalizedTextEditorControl.
        /// </summary>
        [Description( "The settings for the LocalizedTextEditorControl." )]
        public LocalizedTextEditorControlSettings LocalizedTextEditorControlSettings
        {
            get
            {
                return m_localizedTextEditorControlSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_localizedTextEditorControlSettings = value;
                }
                else
                {
                    m_localizedTextEditorControlSettings.Reset();
                }
            }
        }

        /// <summary>
        /// The settings for the EventEditorControl.
        /// </summary>
        [Description( "The settings for the EventEditorControl." )]
        public EventEditorControlSettings EventEditorControlSettings
        {
            get
            {
                return m_eventEditorControlSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_eventEditorControlSettings = value;
                }
                else
                {
                    m_eventEditorControlSettings.Reset();
                }
            }
        }

        /// <summary>
        /// The source control settings for the Subtitle Project.
        /// </summary>
        [Description( "The source control settings for the Subtitle Project." )]
        public rageSourceControlProviderSettings SourceControlProviderSettings
        {
            get
            {
                return m_sourceControlProviderSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_sourceControlProviderSettings = value;
                }
                else
                {
                    m_sourceControlProviderSettings.Reset();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleProjectSettings )
            {
                return Equals( obj as SubtitleProjectSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            SubtitleProjectSettings settings = new SubtitleProjectSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleProjectSettings )
            {
                CopyFrom( other as SubtitleProjectSettings );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.CurrentLanguage = string.Empty;
            this.CurrentTime = 0.0M;
            this.EndTime = 0.0M;
            this.EventEditorControlSettings.Reset();
            this.LocalizedTextEditorControlSettings.Reset();
            this.SourceControlProviderSettings.Reset();
            this.StartTime = 0.0M;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleProjectSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.CurrentLanguage = other.CurrentLanguage;
            this.CurrentTime = other.CurrentTime;
            this.EndTime = other.EndTime;
            this.EventEditorControlSettings.CopyFrom( other.EventEditorControlSettings );
            this.LocalizedTextEditorControlSettings.CopyFrom( other.LocalizedTextEditorControlSettings );
            this.SourceControlProviderSettings.CopyFrom( other.SourceControlProviderSettings );
            this.StartTime = other.StartTime;
        }

        public bool Equals( SubtitleProjectSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return (this.CurrentLanguage == other.CurrentLanguage)
                && (this.CurrentTime == other.CurrentTime)
                && (this.EndTime == other.EndTime)
                && (this.StartTime == other.StartTime)
                && this.EventEditorControlSettings.Equals( other.EventEditorControlSettings )
                && this.LocalizedTextEditorControlSettings.Equals( other.LocalizedTextEditorControlSettings )
                && this.SourceControlProviderSettings.Equals( other.SourceControlProviderSettings );
        }
        #endregion
    }
}
