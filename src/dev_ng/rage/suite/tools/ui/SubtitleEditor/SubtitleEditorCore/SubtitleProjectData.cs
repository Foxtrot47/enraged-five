using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Windows.Forms;

using LocalizedTextControls;
using RSG.Base.Serialization;

namespace SubtitleEditorCore
{
    /// <summary>
    /// Holds all of the LocalizedTextData items and timelines for all languages.
    /// </summary>
    public class SubtitleProjectData : rageSerializableObject
    {
        public SubtitleProjectData()
        {
            Reset();
        }

        #region Variables
        private string m_subtitleOptionsFilename = string.Empty;
        private string m_localizedTextOptionsFilename = string.Empty;
        private SubtitleLanguageDataCollection m_languageDataCollection = new SubtitleLanguageDataCollection();
        private SubtitleExporterSettings m_exporterSettings = new SubtitleExporterSettings();
        #endregion

        #region Properties
        /// <summary>
        /// The name of the game-specific SubtitleOptions file to use for this Subtitle Project.
        /// </summary>
        [DefaultValue( "" ), Description( "The name of the game-specific SubtitleOptions file to use for this Subtitle Project." )]
        public string SubtitleOptionsFilename
        {
            get
            {
                return m_subtitleOptionsFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_subtitleOptionsFilename = value;
                }
                else
                {
                    m_subtitleOptionsFilename = string.Empty;
                }
            }
        }
        
        /// <summary>
        /// The name of the game-specific LocalizedTextOptions file to use for this Subtitle Project.
        /// </summary>
        [DefaultValue( "" ), Description( "The name of the game-specific LocalizedTextOptions file to use for this Subtitle Project." )]
        public string LocalizedTextOptionsFilename
        {
            get
            {
                return m_localizedTextOptionsFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_localizedTextOptionsFilename = value;
                }
                else
                {
                    m_localizedTextOptionsFilename = string.Empty;
                }
            }
        }        

        /// <summary>
        /// The collection of SubtitleLanguageData items.
        /// </summary>
        [Description( "The collection of SubtitleLanguageData items." )]
        public SubtitleLanguageDataCollection LanguageDataCollection
        {
            get
            {
                return m_languageDataCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_languageDataCollection = value;
                }
            }
        }

        /// <summary>
        /// The collection of settings that specify how to export the subtitles.
        /// </summary>
        [Description( "The collection of settings that specify how to export the subtitles." )]
        public SubtitleExporterSettings ExporterSettings
        {
            get
            {
                return m_exporterSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_exporterSettings = value;
                }
                else
                {
                    m_exporterSettings.Reset();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleProjectData )
            {
                return Equals( obj as SubtitleProjectData );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            SubtitleProjectData settings = new SubtitleProjectData();
            settings.CopyFrom( this );
            return settings;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleProjectData )
            {
                CopyFrom( other as SubtitleProjectData );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.ExporterSettings.Reset();
            this.LanguageDataCollection.Reset();
            this.LocalizedTextOptionsFilename = string.Empty;
            this.SubtitleOptionsFilename = string.Empty;

            string filename = Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), "LocalizedTextOptions.txtproj.options" );
            if ( File.Exists( filename ) )
            {
                this.LocalizedTextOptionsFilename = filename;
            }
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleProjectData other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.ExporterSettings.CopyFrom( other.ExporterSettings );
            this.LanguageDataCollection.CopyFrom( other.LanguageDataCollection );
            this.LocalizedTextOptionsFilename = other.LocalizedTextOptionsFilename;
            this.SubtitleOptionsFilename = other.SubtitleOptionsFilename;
        }

        public bool Equals( SubtitleProjectData other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return (this.LocalizedTextOptionsFilename == other.LocalizedTextOptionsFilename)
                && (this.SubtitleOptionsFilename == other.SubtitleOptionsFilename)
                && this.ExporterSettings.Equals( other.ExporterSettings )
                && this.LanguageDataCollection.Equals( other.LanguageDataCollection );
        }
        #endregion
    }
}
