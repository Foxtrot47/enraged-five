namespace SubtitleEditorCore
{
    partial class SubtitleExporterSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cutSubGroupBox = new System.Windows.Forms.GroupBox();
            this.browseCutSubOutputPathButton = new System.Windows.Forms.Button();
            this.cutSubOutputPathTextBox = new System.Windows.Forms.TextBox();
            this.cutSubOutputPathLabel = new System.Windows.Forms.Label();
            this.localizedTextGroupBox = new System.Windows.Forms.GroupBox();
            this.localizedTextExporterSettingsControl = new LocalizedTextControls.LocalizedTextExporterSettingsControl();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.cutSubGroupBox.SuspendLayout();
            this.localizedTextGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // cutSubGroupBox
            // 
            this.cutSubGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cutSubGroupBox.Controls.Add( this.browseCutSubOutputPathButton );
            this.cutSubGroupBox.Controls.Add( this.cutSubOutputPathTextBox );
            this.cutSubGroupBox.Controls.Add( this.cutSubOutputPathLabel );
            this.cutSubGroupBox.Location = new System.Drawing.Point( 3, 150 );
            this.cutSubGroupBox.Name = "cutSubGroupBox";
            this.cutSubGroupBox.Size = new System.Drawing.Size( 674, 49 );
            this.cutSubGroupBox.TabIndex = 0;
            this.cutSubGroupBox.TabStop = false;
            this.cutSubGroupBox.Text = "Subtitle Export";
            // 
            // browseCutSubOutputPathButton
            // 
            this.browseCutSubOutputPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseCutSubOutputPathButton.Location = new System.Drawing.Point( 643, 17 );
            this.browseCutSubOutputPathButton.Name = "browseCutSubOutputPathButton";
            this.browseCutSubOutputPathButton.Size = new System.Drawing.Size( 25, 23 );
            this.browseCutSubOutputPathButton.TabIndex = 2;
            this.browseCutSubOutputPathButton.Text = "...";
            this.browseCutSubOutputPathButton.UseVisualStyleBackColor = true;
            this.browseCutSubOutputPathButton.Click += new System.EventHandler( this.browseCutSubOutputPathButton_Click );
            // 
            // cutSubOutputPathTextBox
            // 
            this.cutSubOutputPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cutSubOutputPathTextBox.Location = new System.Drawing.Point( 76, 19 );
            this.cutSubOutputPathTextBox.Name = "cutSubOutputPathTextBox";
            this.cutSubOutputPathTextBox.Size = new System.Drawing.Size( 561, 20 );
            this.cutSubOutputPathTextBox.TabIndex = 1;
            this.cutSubOutputPathTextBox.TextChanged += new System.EventHandler( this.cutSubOutputPathTextBox_TextChanged );
            // 
            // cutSubOutputPathLabel
            // 
            this.cutSubOutputPathLabel.AutoSize = true;
            this.cutSubOutputPathLabel.Location = new System.Drawing.Point( 6, 22 );
            this.cutSubOutputPathLabel.Name = "cutSubOutputPathLabel";
            this.cutSubOutputPathLabel.Size = new System.Drawing.Size( 64, 13 );
            this.cutSubOutputPathLabel.TabIndex = 0;
            this.cutSubOutputPathLabel.Text = "Output Path";
            // 
            // localizedTextGroupBox
            // 
            this.localizedTextGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.localizedTextGroupBox.Controls.Add( this.localizedTextExporterSettingsControl );
            this.localizedTextGroupBox.Location = new System.Drawing.Point( 3, 3 );
            this.localizedTextGroupBox.Name = "localizedTextGroupBox";
            this.localizedTextGroupBox.Size = new System.Drawing.Size( 674, 141 );
            this.localizedTextGroupBox.TabIndex = 1;
            this.localizedTextGroupBox.TabStop = false;
            this.localizedTextGroupBox.Text = "Localized Text Export";
            // 
            // localizedTextExporterSettingsControl
            // 
            this.localizedTextExporterSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.localizedTextExporterSettingsControl.Location = new System.Drawing.Point( 3, 16 );
            this.localizedTextExporterSettingsControl.Name = "localizedTextExporterSettingsControl";
            this.localizedTextExporterSettingsControl.Size = new System.Drawing.Size( 668, 122 );
            this.localizedTextExporterSettingsControl.TabIndex = 0;
            this.localizedTextExporterSettingsControl.Changed += new System.EventHandler( this.localizedTextExporterSettingsControl_Changed );
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // SubtitleExporterSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.localizedTextGroupBox );
            this.Controls.Add( this.cutSubGroupBox );
            this.Name = "SubtitleExporterSettingsControl";
            this.Size = new System.Drawing.Size( 680, 203 );
            this.cutSubGroupBox.ResumeLayout( false );
            this.cutSubGroupBox.PerformLayout();
            this.localizedTextGroupBox.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.GroupBox cutSubGroupBox;
        private System.Windows.Forms.Button browseCutSubOutputPathButton;
        private System.Windows.Forms.TextBox cutSubOutputPathTextBox;
        private System.Windows.Forms.Label cutSubOutputPathLabel;
        private System.Windows.Forms.GroupBox localizedTextGroupBox;
        private LocalizedTextControls.LocalizedTextExporterSettingsControl localizedTextExporterSettingsControl;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}
