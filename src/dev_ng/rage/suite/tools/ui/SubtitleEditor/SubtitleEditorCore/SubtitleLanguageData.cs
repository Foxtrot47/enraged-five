using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using LocalizedTextControls;

using RSG.Base.Serialization;

namespace SubtitleEditorCore
{
    /// <summary>
    /// Holds the LocalizedTextData and timeline data for a particular language
    /// </summary>
    [Serializable]
    public class SubtitleLanguageData : IRageClonableObject
    {
        public SubtitleLanguageData()
        {

        }

        public SubtitleLanguageData( string language )
        {
            this.Language = language;
        }

        #region Variables
        private string m_language = string.Empty;
        private string m_scriptFilename = string.Empty;
        private string m_mediaFilename = string.Empty;

        private float m_phaseMin = 0.0f;
        private float m_phaseMax = 1.0f;
        private float m_displayMin = 0.0f;
        private float m_displayMax = 100.0f;

        private LocalizedTextDataCollection m_textDataCollection = new LocalizedTextDataCollection();
        private SubtitleTrackDataCollection m_trackDataCollection = new SubtitleTrackDataCollection();
        #endregion

        #region Properties
        /// <summary>
        /// The language.
        /// </summary>
        [DefaultValue( "" ), Description( "The language." )]
        public string Language
        {
            get
            {
                return m_language;
            }
            set
            {
                if ( value != null )
                {
                    m_language = value;
                }
                else
                {
                    m_language = string.Empty;
                }
            }
        }

        /// <summary>
        /// The name of the script filename for this language that is loaded into the ScriptControl.
        /// </summary>
        [DefaultValue( "" ), Description( "The name of the script filename for this language that is loaded into the ScriptControl." )]        
        public string ScriptFilename
        {
            get
            {
                return m_scriptFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_scriptFilename = value;
                }
                else
                {
                    m_scriptFilename = string.Empty;
                }
            }
        }

        /// <summary>
        /// The name of the media filename for this language that is loaded into the PreviewControl.
        /// </summary>
        [DefaultValue( "" ), Description( "The name of the media filename for this language that is loaded into the PreviewControl." )]
        public string MediaFilename
        {
            get
            {
                return m_mediaFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_mediaFilename = value;
                }
                else
                {
                    m_mediaFilename = string.Empty;
                }
            }
        }

        /// <summary>
        /// The minimum phase value for the media file's entire timeline.
        /// </summary>
        [DefaultValue( 0 ), Description( "The minimum phase value for the media file's entire timeline." )]
        public float PhaseMin
        {
            get
            {
                return m_phaseMin;
            }
            set
            {
                m_phaseMin = value;
            }
        }

        /// <summary>
        /// The maximum phase value for the media file's entire timeline.
        /// </summary>
        [DefaultValue( 1 ), Description( "The maximum phase value for the media file's entire timeline." )]
        public float PhaseMax
        {
            get
            {
                return m_phaseMax;
            }
            set
            {
                m_phaseMax = value;
            }
        }

        /// <summary>
        /// The minimum displayed value (frames, seconds, whatever) for the media file's entire timeline.
        /// </summary>
        [DefaultValue( 0 ), Description( "The minimum displayed value (frames, seconds, whatever) for the media file's entire timeline." )]
        public float DisplayMin
        {
            get
            {
                return m_displayMin;
            }
            set
            {
                m_displayMin = value;
            }
        }

        /// <summary>
        /// The maximum displayed value (frames, seconds, whatever) for the media file's entire timeline.
        /// </summary>
        [DefaultValue( 100 ), Description( "The maximum displayed value (frames, seconds, whatever) for the media file's entire timeline." )]
        public float DisplayMax
        {
            get
            {
                return m_displayMax;
            }
            set
            {
                m_displayMax = value;
            }
        }

        /// <summary>
        /// The collection of LocalizedTextData items.
        /// </summary>
        [Description( "The collection of LocalizedTextData items." )]
        public LocalizedTextDataCollection TextDataCollection
        {
            get
            {
                return m_textDataCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_textDataCollection = value;
                }
                else
                {
                    m_textDataCollection.Clear();
                }
            }
        }

        /// <summary>
        /// The collection of SubtitleTrackData items.
        /// </summary>
        [Description( "he collection of SubtitleTrackData items." )]
        public SubtitleTrackDataCollection TrackDataCollection
        {
            get
            {
                return m_trackDataCollection;
            }
            set
            {                
                if ( value != null )
                {
                    m_trackDataCollection = value;
                }
                else
                {
                    m_trackDataCollection.Clear();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleLanguageData )
            {
                return Equals( obj as SubtitleLanguageData );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Language;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            SubtitleLanguageData data = new SubtitleLanguageData();
            data.CopyFrom( this );
            return data;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleLanguageData )
            {
                CopyFrom( other as SubtitleLanguageData );
            }
        }

        public void Reset()
        {
            this.DisplayMax = 100.0f;
            this.DisplayMin = 0.0f;
            this.Language = string.Empty;
            this.MediaFilename = string.Empty;
            this.PhaseMax = 1.0f;
            this.PhaseMin = 0.0f;
            this.ScriptFilename = string.Empty;
            this.TextDataCollection.Reset();
            this.TrackDataCollection.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleLanguageData other )
        {
            if ( other == null )
            {
                return;
            }

            this.DisplayMax = other.DisplayMax;
            this.DisplayMin = other.DisplayMin;
            this.Language = other.Language;
            this.MediaFilename = other.MediaFilename;
            this.PhaseMax = other.PhaseMax;
            this.PhaseMin = other.PhaseMin;
            this.ScriptFilename = other.ScriptFilename;
            this.TextDataCollection.CopyFrom( other.TextDataCollection );
            this.TrackDataCollection.CopyFrom( other.TrackDataCollection );
        }

        public bool Equals( SubtitleLanguageData other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.DisplayMax == other.DisplayMax)
                && (this.DisplayMin == other.DisplayMin)
                && (this.Language == other.Language)
                && (this.MediaFilename == other.MediaFilename)
                && (this.PhaseMax == other.PhaseMax)
                && (this.PhaseMin == other.PhaseMin)
                && (this.ScriptFilename == other.ScriptFilename)
                && this.TextDataCollection.Equals( other.TextDataCollection )
                && this.TrackDataCollection.Equals( other.TrackDataCollection );
        }

        public LocalizedTextData FindTextData( string id )
        {
            foreach ( LocalizedTextData textData in this.TextDataCollection )
            {
                if ( textData.ID == id )
                {
                    return textData;
                }
            }

            return null;
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of SubtitleLanguageData items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class SubtitleLanguageDataCollection : rageSerializableList<SubtitleLanguageData>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            SubtitleLanguageDataCollection collection = new SubtitleLanguageDataCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion

        #region Public Functions
        public SubtitleLanguageData Find( string language )
        {
            string uppercaseLanguage = language.ToUpper();
            foreach ( SubtitleLanguageData data in this )
            {
                if ( data.Language.ToUpper() == uppercaseLanguage )
                {
                    return data;
                }
            }

            return null;
        }
        #endregion
    }
}