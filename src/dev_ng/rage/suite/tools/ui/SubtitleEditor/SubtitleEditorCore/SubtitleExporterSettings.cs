using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

using LocalizedTextControls;
using RSG.Base.ConfigParser;
using RSG.Base.Serialization;
namespace SubtitleEditorCore
{
    public class SubtitleExporterSettings : IRageClonableObject
    {
        public SubtitleExporterSettings()
        {
            Reset();
        }

        #region Variables
        private string m_cutsubExportPath = string.Empty;
        private LocalizedTextExporterSettings m_textExporterSettings = new LocalizedTextExporterSettings();
        #endregion

        #region Properties
        /// <summary>
        /// The path where the .cutsub file should be saved.  It will have the same name as the SubtitleProjectData's filename.
        /// </summary>
        [Description( "The path where the .cutsub file should be saved.  It will have the same name as the SubtitleProjectData's filename." )]        
        public string CutsubExportPath
        {
            get
            {
                return m_cutsubExportPath;
            }
            set
            {
                if ( value != null )
                {
                    m_cutsubExportPath = value;
                }
                else
                {
                    m_cutsubExportPath = string.Empty;
                }
            }
        }

        public LocalizedTextExporterSettings TextExporterSettings
        {
            get
            {
                return m_textExporterSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_textExporterSettings = value;
                }
                else
                {
                    m_textExporterSettings.Reset();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleExporterSettings )
            {
                return Equals( obj as SubtitleExporterSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            SubtitleExporterSettings settings = new SubtitleExporterSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleExporterSettings )
            {
                CopyFrom( other as SubtitleExporterSettings );
            }
        }

        public void Reset()
        {            
            ConfigExportView exportView = new ConfigExportView();
            String cutsceneTunePath = exportView.GetTunePath();
            if (String.IsNullOrEmpty(cutsceneTunePath) == false)
                this.CutsubExportPath = cutsceneTunePath.Replace('/', '\\');
            else
                this.CutsubExportPath = "";

            this.TextExporterSettings.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleExporterSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            this.CutsubExportPath = other.CutsubExportPath;
            this.TextExporterSettings.CopyFrom( other.TextExporterSettings );
        }

        public bool Equals( SubtitleExporterSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.CutsubExportPath == other.CutsubExportPath)
                && this.TextExporterSettings.Equals( other.TextExporterSettings );
        }
        #endregion
    }
}
