using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using System.Windows.Forms;

using EventEditorControls;
using LocalizedTextControls;
using RSG.Base;

namespace SubtitleEditorCore
{
    public partial class SubtitleExporterComponent : Component
    {
        public SubtitleExporterComponent()
        {
            InitializeComponent();
        }

        public SubtitleExporterComponent( IContainer container )
        {
            container.Add( this );

            InitializeComponent();
        }

        #region Constants
        // These constants are the values as found in rage/suite/src/cutfile/cutfevent.h
        private const int CUTSCENE_LOAD_SUBTITLES_EVENT = 12;
        private const int CUTSCENE_SHOW_SUBTITLE_EVENT = 30;
        private const int CUTSCENE_HIDE_SUBTITLE_EVENT = 31;

        private string[] c_languages = new string[]{ 	
            "ENGLISH",
	        "SPANISH",
	        "FRENCH",
	        "GERMAN",
	        "ITALIAN",
	        "PORTUGESE",
	        "JAPANESE",
	        "CHINESE-TRADITIONAL",
	        "CHINESE-SIMPLIFIED",
	        "KOREAN",
	        "NORWEGIAN"
        };
        #endregion

        #region Variables
        private TimeLineComponent m_timeLineComponent = new TimeLineComponent();
        private SubtitleProjectData m_projectData = new SubtitleProjectData();
        private SubtitleOptions m_options = new SubtitleOptions();

        private int m_numProgressSteps = 0;
        private int m_currentProgressStep = 0;
        #endregion

        #region Properties
        [Category( "Data" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TimeLineComponent TimeLineComponent
        {
            get
            {
                return m_timeLineComponent;
            }
            set
            {
                if ( value != null )
                {
                    m_timeLineComponent = value;
                }
            }
        }

        [Category( "Data" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public SubtitleProjectData SubtitleProjectData
        {
            get
            {
                return m_projectData;
            }
            set
            {
                if ( value != null )
                {
                    m_projectData = value;
                }
                else
                {
                    m_projectData.Reset();
                }
            }
        }

        [Category( "Data" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public SubtitleOptions SubtitleOptions
        {
            get
            {
                return m_options;
            }
            set
            {
                if ( value != null )
                {
                    m_options = value;
                }
                else
                {
                    m_options.Reset();
                }
            }
        }

        [Category( "Data" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextOptions LocalizedTextOptions
        {
            get
            {
                return this.localizedTextExporterComponent.LocalizedTextOptions;
            }
            set
            {
                this.localizedTextExporterComponent.LocalizedTextOptions = value;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsBusy
        {
            get
            {
                return this.backgroundWorker.IsBusy;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the export begins.
        /// </summary>
        [Description( "Occurs when the export begins." ), Category( "Action" )]
        public event EventHandler ExportBegin;

        /// <summary>
        /// Occurs when the progress of the export is updated.
        /// </summary>
        [Description( "Occurs when the progress of the export is updated." ), Category( "Action" )]
        public event ProgressChangedEventHandler ProgressChanged;

        /// <summary>
        /// Occurs when the export wants to display a message.
        /// </summary>
        [Description( "Occurs when the export wants to display a message." ), Category( "Action" )]
        public event ExportMessageEventHandler ExportMessage;

        /// <summary>
        /// Occurs when the export finishes.
        /// </summary>
        [Description( "Occurs when the export finishes." ), Category( "Action" )]
        public event RunWorkerCompletedEventHandler ExportComplete;

        /// <summary>
        /// Dispatched when a file should be checked out from source control.
        /// </summary>
        [Description( "Dispatched when a file should be checked out from source control." ), Category( "Action" )]
        public event SourceControlEventHandler CheckOutFile;
        #endregion

        #region Event Dispatchers
        protected void OnExportBegin()
        {
            if ( this.ExportBegin != null )
            {
                this.ExportBegin( this, EventArgs.Empty );
            }
        }

        protected void OnProgressChanged( ProgressChangedEventArgs e )
        {
            if ( this.ProgressChanged != null )
            {
                this.ProgressChanged( this, e );
            }
        }

        protected void OnExportMessage( ExportMessageEventArgs e )
        {
            if ( this.ExportMessage != null )
            {
                this.ExportMessage( this, e );
            }
        }

        protected void OnExportComplete( RunWorkerCompletedEventArgs e )
        {
            if ( this.ExportComplete != null )
            {
                this.ExportComplete( this, e );
            }
        }

        protected void OnCheckOutFile( SourceControlEventArgs e )
        {
            if ( this.CheckOutFile != null )
            {
                this.CheckOutFile( this, e );
            }
        }
        #endregion

        #region Event Handlers
        private void localizedTextExporterComponent_CheckOutFile( object sender, SourceControlEventArgs e )
        {
            OnCheckOutFile( e );
        }

        private void localizedTextExporterComponent_ExportBegin( object sender, EventArgs e )
        {
            OnExportBegin();
        }

        private void localizedTextExporterComponent_ExportComplete( object sender, RunWorkerCompletedEventArgs e )
        {
            // daisy-chain together
            if ( (int)e.Result == 0 )
            {
                ++m_currentProgressStep;

                this.backgroundWorker.RunWorkerAsync();
            }
            else
            {
                OnExportComplete( e );
            }
        }

        private void localizedTextExporterComponent_ExportMessage( object sender, ExportMessageEventArgs e )
        {
            ReportMessage( e.Message );
        }

        private void localizedTextExporterComponent_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            backgroundWorker_ProgressChanged( sender, e );
        }

        private void backgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            string projectName = Path.GetFileNameWithoutExtension( this.SubtitleProjectData.CurrentFilename );

            string cutsubFilename = Path.Combine( this.SubtitleProjectData.ExporterSettings.CutsubExportPath.Replace( "/", "\\" ), projectName );
            cutsubFilename += ".cutsub";
            cutsubFilename = Path.Combine( Path.GetDirectoryName( this.SubtitleProjectData.CurrentFilename ), cutsubFilename );
            cutsubFilename = this.SubtitleProjectData.GetRelativePath( cutsubFilename );
            cutsubFilename = this.SubtitleProjectData.GetAbsolutePath( cutsubFilename );

            e.Result = ExportCutsub( cutsubFilename );
        }

        private void backgroundWorker_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            OnProgressChanged( e );
        }

        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            OnExportComplete( e );
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Begins exporting the given project asynchronously.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="settings"
        /// <returns></returns>
        public void Export()
        {
            if ( this.backgroundWorker.IsBusy )
            {
                return;
            }

            m_numProgressSteps = 2;
            m_currentProgressStep = 0;

            // copy our Subtitle Project data and settings into the LocalizedTextExporterComponent
            this.localizedTextExporterComponent.LocalizedTextProjectData.ExporterSettings 
                = this.SubtitleProjectData.ExporterSettings.TextExporterSettings;
            this.localizedTextExporterComponent.LocalizedTextProjectData.LocalizedTextOptionsFilename 
                = this.SubtitleProjectData.LocalizedTextOptionsFilename;

            this.localizedTextExporterComponent.LocalizedTextProjectData.TextDataCollection.Reset();
            foreach ( SubtitleLanguageData languageData in this.SubtitleProjectData.LanguageDataCollection )
            {
                this.localizedTextExporterComponent.LocalizedTextProjectData.TextDataCollection.AddRange( languageData.TextDataCollection );
            }

            this.localizedTextExporterComponent.LocalizedTextProjectData.CurrentFilename 
                = this.SubtitleProjectData.CurrentFilename;
            this.localizedTextExporterComponent.LocalizedTextProjectData.OriginalFileLocation 
                = this.SubtitleProjectData.OriginalFileLocation;

            // start the export
            this.localizedTextExporterComponent.Export();
        }

        public void Cancel()
        {
            if ( this.localizedTextExporterComponent.IsBusy )
            {
                this.localizedTextExporterComponent.Cancel();
            }
            else if ( this.IsBusy )
            {
                this.backgroundWorker.CancelAsync();
            }
        }
        #endregion

        #region Private Functions
        private void ReportProgress( int percent, string message )
        {
            float range = 100.0f / (float)m_numProgressSteps;
            float batchProgress = ((float)percent / 100.0f) * range;
            if ( m_currentProgressStep > 0 )
            {
                batchProgress += (m_currentProgressStep * range);
            }

            if ( batchProgress < 0.0f )
            {
                batchProgress = 0.0f;
            }
            else if ( batchProgress > 100.0f )
            {
                batchProgress = 100.0f;
            }

            this.backgroundWorker.ReportProgress( (int)batchProgress, message );
        }

        private void ReportMessage( string message )
        {
            OnExportMessage( new ExportMessageEventArgs( message ) );
        }

        private int ExportCutsub( string cutsubFilename )
        {
            string message = String.Format( "Exporting {0}...", cutsubFilename );
            ReportProgress( 0, message );
            ReportMessage( message );

            string directory = Path.GetDirectoryName( cutsubFilename );
            if ( !Directory.Exists( directory ) )
            {
                Directory.CreateDirectory( directory );
            }

            if ( File.Exists( cutsubFilename ) )
            {
                SourceControlEventArgs e = new SourceControlEventArgs( cutsubFilename );
                OnCheckOutFile( e );
                if ( e.Result == DialogResult.Abort )
                {
                    return 1;
                }
            }            

            int numSteps = this.SubtitleProjectData.LanguageDataCollection.Count + 4;
            int currentStep = 1;

            ReportMessage( "Collecting .cutsub data..." );

            // collect the data
            List<cutfObject> objectList = new List<cutfObject>();
            Dictionary<string, cutfSubtitleObject> subtitleObjectsBySpeaker = new Dictionary<string, cutfSubtitleObject>();
            List<cutfNameEventArgs> eventArgsList = new List<cutfNameEventArgs>();
            List<cutfObjectIdEvent> loadEventList = new List<cutfObjectIdEvent>();
            List<cutfObjectIdEvent> eventList = new List<cutfObjectIdEvent>();

            ReportMessage( "Creating cutfAssetManagerObject..." );

            cutfAssetManagerObject assetMgrObj = new cutfAssetManagerObject( objectList.Count );
            objectList.Add( assetMgrObj );

            cutfNameEventArgs loadSubtitlesEventArgs 
                = new cutfNameEventArgs( Path.GetFileNameWithoutExtension( this.SubtitleProjectData.CurrentFilename ) );
            eventArgsList.Add( loadSubtitlesEventArgs );

            cutfObjectIdEvent loadSubtitlesEvent = new cutfObjectIdEvent( assetMgrObj.ObjectId, 0.0f, CUTSCENE_LOAD_SUBTITLES_EVENT, 0 );
            loadEventList.Add( loadSubtitlesEvent );

            foreach ( SubtitleLanguageData languageData in this.SubtitleProjectData.LanguageDataCollection )
            {
                ReportMessage( String.Format( "Processing {0} language...", languageData.Language ) );

                this.TimeLineComponent.SetPhase( languageData.PhaseMin, languageData.PhaseMax );
                this.TimeLineComponent.SetDisplay( languageData.DisplayMin, languageData.DisplayMax );

                // find the language id
                string language = languageData.Language.ToUpper();
                int languageId = -1;
                for ( int i = 0; i < c_languages.Length; ++i )
                {
                    if ( language == c_languages[i] )
                    {
                        languageId = i;
                        break;
                    }
                }

                // go through each track
                foreach ( SubtitleTrackData trackData in languageData.TrackDataCollection )
                {
                    // create a subtitle object for each speaker, if we haven't already
                    cutfSubtitleObject subtitleObject;
                    if ( !subtitleObjectsBySpeaker.TryGetValue( trackData.Speaker, out subtitleObject ) )
                    {
                        ReportMessage( String.Format( "Creating cutfSubtitleObject {0}...", trackData.Speaker ) );

                        subtitleObject = new cutfSubtitleObject( objectList.Count, trackData.Speaker );
                        objectList.Add( subtitleObject );
                        subtitleObjectsBySpeaker.Add( trackData.Speaker, subtitleObject );
                    }

                    ReportMessage( String.Format( "Creating events and event args for cutfSubtitleObject '{0}'...", trackData.Speaker ) );

                    // go through each event on the track.  create the event args and the events
                    foreach ( SubtitleEventData eventData in trackData.EventDataCollection )
                    {
                        float startTime = this.TimeLineComponent.PhaseToDisplay( eventData.MinT );
                        int transitionIn = -1;
                        if ( eventData.TransitionIn.Enabled )
                        {
                            transitionIn = GetTransitionId( eventData.TransitionIn.TypeName );
                        }

                        float endTime = this.TimeLineComponent.PhaseToDisplay( eventData.MaxT );
                        int transitionOut = -1;
                        if ( eventData.TransitionOut.Enabled )
                        {
                            transitionOut = GetTransitionId( eventData.TransitionOut.TypeName ); 
                            endTime -= eventData.TransitionOut.Duration;
                        }

                        cutfSubtitleEventArgs eventArgs = new cutfSubtitleEventArgs( eventData.LocalizedTextID, languageId,
                            transitionIn, eventData.TransitionIn.Duration, transitionOut, eventData.TransitionOut.Duration );
                        eventArgsList.Add( eventArgs );

                        cutfObjectIdEvent startEvent 
                            = new cutfObjectIdEvent( subtitleObject.ObjectId, startTime, CUTSCENE_SHOW_SUBTITLE_EVENT, eventArgsList.Count - 1 );
                        eventList.Add( startEvent );

                        cutfObjectIdEvent endEvent
                            = new cutfObjectIdEvent( subtitleObject.ObjectId, endTime, CUTSCENE_HIDE_SUBTITLE_EVENT, eventArgsList.Count - 1 );
                        eventList.Add( endEvent );
                    }

                    ReportMessage( String.Format( "Found {0} subtitles.", trackData.EventDataCollection.Count ) );
                }

                ReportProgress( (int)(((float)currentStep / (float)numSteps) * 100.0f), null );
                ++currentStep;
            }

            XmlTextWriter writer = null;
            try
            {
                writer = new XmlTextWriter( cutsubFilename, Encoding.UTF8 );
                writer.Formatting = Formatting.Indented;

                writer.WriteStartDocument();
                {
                    writer.WriteStartElement( "rage__cutfCutsceneFile2" );
                    writer.WriteAttributeString( "v", "1.0" );
                    {
                        ReportMessage( String.Format( "Writing {0} objects...", objectList.Count ) );

                        writer.WriteStartElement( "pCutsceneObjects" );
                        {
                            foreach ( cutfObject obj in objectList )
                            {
                                obj.WriteXml( writer );
                            }
                        }
                        writer.WriteEndElement();

                        ReportProgress( (int)(((float)currentStep / (float)numSteps) * 100.0f), null );
                        ++currentStep;

                        ReportMessage( String.Format( "Writing {0} event args...", eventArgsList.Count ) );

                        writer.WriteStartElement( "pCutsceneEventArgsList" );
                        {
                            foreach ( cutfNameEventArgs eventArgs in eventArgsList )
                            {
                                eventArgs.WriteXml( writer );
                            }
                        }
                        writer.WriteEndElement();

                        ReportProgress( (int)(((float)currentStep / (float)numSteps) * 100.0f), null );
                        ++currentStep;

                        ReportMessage( String.Format( "Writing {0} load events...", loadEventList.Count ) );

                        writer.WriteStartElement( "pCutsceneLoadEventList" );
                        {
                            foreach ( cutfObjectIdEvent evt in loadEventList )
                            {
                                evt.WriteXml( writer );
                            }
                        }
                        writer.WriteEndElement();

                        ReportProgress( (int)(((float)currentStep / (float)numSteps) * 100.0f), null );
                        ++currentStep;

                        ReportMessage( String.Format( "Writing {0} events...", eventList.Count ) );

                        writer.WriteStartElement( "pCutsceneEventList" );
                        {
                            foreach ( cutfObjectIdEvent evt in eventList )
                            {
                                evt.WriteXml( writer );
                            }
                        }
                        writer.WriteEndElement();

                        ReportProgress( (int)(((float)currentStep / (float)numSteps) * 100.0f), null );
                        ++currentStep;
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndDocument();
            }
            catch ( Exception ex )
            {
                ReportMessage( ex.ToString() );
                return 1;
            }
            finally
            {
                if ( writer != null )
                {
                    writer.Close();
                }
            }

            message = String.Format( "Exported {0}.", cutsubFilename );
            ReportProgress( 100, message );
            ++m_currentProgressStep;

            ReportMessage( message );

            return 0;
        }

        private int GetTransitionId( string typeName )
        {
            string uppercaseTypeName = typeName.ToUpper();
            for ( int i = 0; i < this.SubtitleOptions.TransitionOptionCollection.Count; ++i )
            {
                if ( uppercaseTypeName == this.SubtitleOptions.TransitionOptionCollection[i].Name.ToUpper() )
                {
                    return i;
                }
            }

            return -1;
        }
        #endregion

        #region Helper Classes
        internal abstract class cutfObject
        {
            public cutfObject( int objectId )
            {
                m_objectId = objectId;
            }

            #region Variables
            protected int m_objectId;
            #endregion

            #region Properties
            public int ObjectId
            {
                get
                {
                    return m_objectId;
                }
            }
            #endregion

            #region Public Functions
            public abstract void WriteXml( XmlTextWriter writer );
            #endregion
        }

        internal class cutfAssetManagerObject : cutfObject
        {
            public cutfAssetManagerObject( int objectId )
                : base( objectId )
            {

            }

            #region Public Functions
            public override void WriteXml( XmlTextWriter writer )
            {
                writer.WriteStartElement( "Item" );
                writer.WriteAttributeString( "type", "rage__cutfAssetManagerObject" );
                {
                    writer.WriteStartElement( "iObjectId" );
                    writer.WriteAttributeString( "value", m_objectId.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "attributeList" );
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            #endregion
        }

        /// <summary>
        /// As found in rage/suite/src/cutfile/cutfobject.h
        /// </summary>
        internal class cutfSubtitleObject : cutfObject
        {
            public cutfSubtitleObject( int objectId, string name )
                : base( objectId )
            {
                m_name = name;
            }

            #region Variables
            private string m_name;
            #endregion

            #region Public Functions
            public override void WriteXml( XmlTextWriter writer )
            {
                writer.WriteStartElement( "Item" );
                writer.WriteAttributeString( "type", "rage__cutfSubtitleObject" );
                {
                    writer.WriteStartElement( "iObjectId" );
                    writer.WriteAttributeString( "value", m_objectId.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "attributeList" );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "cName" );
                    {
                        writer.WriteString( m_name );
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            #endregion
        }

        /// <summary>
        /// As found in rage/suite/src/cutfile/cutfeventargs.h
        /// </summary>
        internal class cutfNameEventArgs
        {
            public cutfNameEventArgs( string name )
            {
                m_name = name;
            }

            #region Variables
            protected string m_name;
            #endregion

            #region Public Functions
            public virtual void WriteXml( XmlTextWriter writer )
            {
                writer.WriteStartElement( "Item" );
                writer.WriteAttributeString( "type", "rage__cutfNameEventArgs" );
                {
                    writer.WriteStartElement( "attributeList" );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "cName" );
                    {
                        writer.WriteString( m_name );
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            #endregion
        }

        /// <summary>
        /// As found in rage/suite/src/cutfile/cutfeventargs.h
        /// </summary>
        internal class cutfSubtitleEventArgs : cutfNameEventArgs
        {
            public cutfSubtitleEventArgs( string identifier, int languageId )
                : base( identifier )
            {
                m_languageId = languageId;
            }

            public cutfSubtitleEventArgs( string identifier, int languageId, 
                int transitionIn, float transitionInDuration, int transitionOut, float transitionOutDuration )
                : this( identifier, languageId )
            {
                m_transitionIn = transitionIn;
                m_transitionInDuration = transitionInDuration;
                m_transitionOut = transitionOut;
                m_transitionOutDuration = transitionOutDuration;
            }

            #region Variables
            private int m_languageId;
            private int m_transitionIn = -1;
            private float m_transitionInDuration = 0.0f;
            private int m_transitionOut = -1;
            private float m_transitionOutDuration = 0.0f;
            #endregion

            #region Public Functions
            public override void WriteXml( XmlTextWriter writer )
            {
                writer.WriteStartElement( "Item" );
                writer.WriteAttributeString( "type", "rage__cutfSubtitleEventArgs" );
                {
                    writer.WriteStartElement( "attributeList" );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "cName" );
                    {
                        writer.WriteString( m_name );
                    }
                    writer.WriteEndElement();

                    writer.WriteStartElement( "iLanguageID" );
                    writer.WriteAttributeString( "value", m_languageId.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "iTransitionIn" );
                    writer.WriteAttributeString( "value", m_transitionIn.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "fTransitionInDuration" );
                    writer.WriteAttributeString( "value", m_transitionInDuration.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "iTransitionOut" );
                    writer.WriteAttributeString( "value", m_transitionOut.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "fTransitionOutDuration" );
                    writer.WriteAttributeString( "value", m_transitionOutDuration.ToString() );
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            #endregion
        }

        /// <summary>
        /// As found in rage/suite/src/cutfile/cutfevent.h
        /// </summary>
        internal class cutfObjectIdEvent
        {
            public cutfObjectIdEvent( int objectId, float time, int eventId, int eventArgsIndex )
            {
                m_objectId = objectId;
                m_time = time;
                m_eventId = eventId;
                m_eventArgsIndex = eventArgsIndex;
            }

            #region Variables
            private int m_objectId;
            private float m_time;
            private int m_eventId;
            private int m_eventArgsIndex;
            #endregion

            #region Public Functions
            public void WriteXml( XmlTextWriter writer )
            {
                writer.WriteStartElement( "Item" );
                writer.WriteAttributeString( "type", "rage__cutfObjectIdEvent" );
                {
                    writer.WriteStartElement( "fTime" );
                    writer.WriteAttributeString( "value", m_time.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "iEventId" );
                    writer.WriteAttributeString( "value", m_eventId.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "pEventArgs" );
                    writer.WriteAttributeString( "ref", m_eventArgsIndex.ToString() );
                    writer.WriteEndElement();

                    writer.WriteStartElement( "iObjectId" );
                    writer.WriteAttributeString( "value", m_objectId.ToString() );
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            #endregion
        }
        #endregion
    }
}
