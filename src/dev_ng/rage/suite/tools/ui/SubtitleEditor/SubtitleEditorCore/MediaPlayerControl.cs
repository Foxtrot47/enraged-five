using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Microsoft.Win32;
using DirectShowLib;

namespace SubtitleEditorCore
{
    public partial class MediaPlayerControl : UserControl
    {
        public MediaPlayerControl()
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }

            CloseFile();

            base.Dispose( disposing );
        }

        #region Internal Structures
        internal class BitmapOverlay
        {
            public BitmapOverlay( int hash, Bitmap bmp )
            {
                this.Hash = hash;
                this.Bitmap = bmp;
                this.Point = new Point( 0, 0 );
            }

            public BitmapOverlay( int hash, Bitmap bmp, Point p )
            {
                this.Hash = hash;
                this.Bitmap = bmp;
                this.Point = p;
            }

            public int Hash = 0;
            public Bitmap Bitmap = null;
            public Point Point = Point.Empty;
            public bool Show = false;
        }
        #endregion

        #region Enums
        internal enum PlayState
        {
            Stopped,
            Paused,
            Running,
            Init
        };

        internal enum MediaType
        {
            Audio,
            Video
        }
        #endregion

        #region Constants
        private const int c_WMGraphNotify = 0x0400 + 13;
        private const int c_volumeFull = 0;
        private const int c_volumeSilence = -10000;
        #endregion

        #region Variables
        private IGraphBuilder m_graphBuilder = null;
        private IMediaControl m_mediaControl = null;
        private IMediaEventEx m_mediaEventEx = null;
        private IVideoWindow m_videoWindow = null;
        private IBasicAudio m_basicAudio = null;
        private IBasicVideo m_basicVideo = null;
        private IMediaSeeking m_mediaSeeking = null;
        private IVideoFrameStep m_frameStep = null;

        private IFilterGraph2 m_filterGraph = null;
        private IBaseFilter m_vmr9 = null;
        private IVMRWindowlessControl9 m_windowlessCtrl = null;
        private MediaPlayerCompositor m_compositor = null;

        private string m_filename = string.Empty;
        private bool m_isAudio = false;
        private bool m_haveAudio = true;
        private bool m_isMuted = false;
        private bool m_isFullScreen = false;
        private int m_currentVolume = c_volumeFull;
        private PlayState m_currentState = PlayState.Init;
        private double m_currentPlaybackRate = 1.0;

        private IntPtr m_hDrain = IntPtr.Zero;

        private Size m_resolution = Size.Empty;

        private long m_startPosition = 0;
        private long m_stopPosition = 0;

        private bool m_loop = false;
        private bool m_displayTimecode = true;
        private bool m_displaySafeZone = true;
        private bool m_canRewind = false;
        private bool m_canStepBackward = false;
        private bool m_canPlayBackwards = false;
        private bool m_canStepForward = false;
        private bool m_canFastForward = false;

        private decimal m_lastTime = 0.0M;        

#if DEBUG
        private DsROTEntry m_rot = null;
#endif

        private Mutex m_bitmapMutex = new Mutex();
        private List<BitmapOverlay> m_bitmaps = new List<BitmapOverlay>();

        private decimal m_externalTime = 0.0M;
        #endregion

        #region Events
        /// <summary>
        /// Event dispatched when the current playback time changes.
        /// </summary>
        [Category( "Action" ), Description( "Event dispatched when the current playback time changes." )]
        public event EventHandler CurrentTimeChanged;
        #endregion

        #region Event Dispatchers
        protected void OnCurrentTimeChanged()
        {
            if ( this.CurrentTimeChanged != null )
            {
                this.CurrentTimeChanged( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Returns true when a media file is loaded.
        /// </summary>
        [Description( "Returns true when a media file is loaded." )]
        public bool IsLoaded
        {
            get
            {
                return m_currentState != PlayState.Init;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media is paused.
        /// </summary>
        [Description( "Returns true when the currently loaded media is paused." )]
        public bool IsPaused
        {
            get
            {
                return m_currentState == PlayState.Paused;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media is playing.
        /// </summary>
        [Description( "Returns true when the currently loaded media is playing." )]
        public bool IsRunning
        {
            get
            {
                return m_currentState == PlayState.Running;
            }
        }

        /// <summary>
        /// Retrieves the name of the currently loaded media file.
        /// </summary>
        [Browsable( false ), Description( "Retrieves the name of the currently loaded media file." )]
        public string Filename
        {
            get
            {
                return m_filename;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file is a video.
        /// </summary>
        [Description( "Returns true when the currently loaded media file is a video." )]
        public bool IsVideo
        {
            get
            {
                return this.IsLoaded && !this.IsAudio;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file is audio-only.
        /// </summary>
        [Description( "Returns true when the currently loaded media file is audio-only." )]
        public bool IsAudio
        {
            get
            {
                return this.IsLoaded && m_isAudio;
            }
        }

        /// <summary>
        /// Returns the resolution of the currently loaded video file.
        /// </summary>
        [Description( "Returns the resolution of the currently loaded video file." )]
        public Size Resolution
        {
            get
            {
                return m_resolution;
            }
        }

        /// <summary>
        /// Gets and sets the volume.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( 100.0f ), Description( "Gets and sets the volume." )]
        public float Volume
        {
            get
            {
                return 100.0f + ((float)m_currentVolume / 10.0f);
            }
            set
            {
                int volume = (int)((10.0f * value) - 1000.0f);
                if ( m_currentVolume != volume )
                {
                    m_currentVolume = volume;

                    if ( this.IsLoaded && (m_basicAudio != null) && m_haveAudio && !this.IsMuted )
                    {
                        int hr = m_basicAudio.put_Volume( m_currentVolume );
                    }
                }
            }
        }

        /// <summary>
        /// Mutes and unmutes the currently loaded media file.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( false ), Description( "Mutes and unmutes the currently loaded media file." )]
        public bool IsMuted
        {
            get
            {
                return m_isMuted;
            }
            set
            {
                if ( m_isMuted != value )
                {
                    m_isMuted = value;

                    if ( this.IsLoaded && (m_basicAudio != null) && m_haveAudio )
                    {
                        int volume = m_currentVolume;
                        if ( m_isMuted )
                        {
                            volume = c_volumeSilence;
                        }

                        // Set new volume
                        int hr = m_basicAudio.put_Volume( volume );
                    }
                }
            }
        }

        /// <summary>
        /// Indicates whether or not to loop the media file.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( false ), Description( "Indicates whether or not to loop the media file." )]
        public bool Loop
        {
            get
            {
                return m_loop;
            }
            set
            {
                m_loop = value;
            }
        }

        /// <summary>
        /// Shows or hides the timecode displayed in the upper left corner of the video window.
        /// </summary>
        [Category( "Appearance" ), DefaultValue( true ), Description( "Shows or hides the timecode displayed in the upper left corner of the video window." )]
        public bool DisplayTimecode
        {
            get
            {
                return m_displayTimecode;
            }
            set
            {
                if ( m_displayTimecode != value )
                {
                    m_displayTimecode = value;

                    // invalidate just the top line where the timecode may be displaying
                    this.renderingPanel.Invalidate( new Rectangle( 0, 0, this.renderingPanel.Width, this.renderingPanel.Font.Height ) );
                }

                if ( m_compositor != null )
                {
                    m_compositor.DisplayTimecode = m_displayTimecode;
                }
            }
        }

        /// <summary>
        /// Shows or hides the safe zone rectangle that is displayed over the video window.
        /// </summary>
        [Category( "Appearance" ), DefaultValue( true ), Description( "Shows or hides the safe zone rectangle that is displayed over the video window." )]
        public bool DisplaySafeZone
        {
            get
            {
                return m_displaySafeZone;
            }
            set
            {
                if ( m_displaySafeZone != value )
                {
                    m_displaySafeZone = value;

                    this.renderingPanel.Invalidate();
                }

                if ( m_compositor != null )
                {
                    m_compositor.DisplaySafeZone = m_displaySafeZone;
                }
            }
        }

        /// <summary>
        /// Gets or sets whether the video is displayed full screen (currently non-functional).
        /// </summary>
        [Category( "Appearance" ), DefaultValue( false ), Description( "Gets or sets whether the video is displayed full screen (currently non-functional)." )]
        public bool IsFullScreen
        {
            get
            {
                return m_isFullScreen;
            }
            set
            {
                if ( (m_isFullScreen != value) && this.IsLoaded && !this.IsAudio && (m_windowlessCtrl != null) )
                {
                    m_isFullScreen = value;

                    if ( m_isFullScreen )
                    {
                        // FIXME
                    }
                    else
                    {
                        // FIXME
                    }
                }
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file supports rewind.
        /// </summary>
        [Category( "Behavior" ), Description( "Returns true when the currently loaded media file supports rewind." )]
        public bool CanRewind
        {
            get
            {
                if ( this.IsLoaded )
                {
                    return m_canRewind;
                }

                return false;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file supports stepping backward.
        /// </summary>
        [Category( "Behavior" ), Description( "Returns true when the currently loaded media file supports stepping backward." )]
        public bool CanStepBackward
        {
            get
            {
                if ( this.IsLoaded )
                {
                    return m_canStepBackward;
                }

                return false;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file supports playing in reverse.
        /// </summary>
        [Category( "Behavior" ), Description( "Returns true when the currently loaded media file supports playing in reverse." )]
        public bool CanPlayBackwards
        {
            get
            {
                if ( this.IsLoaded )
                {
                    return m_canPlayBackwards;
                }

                return false;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file supports pausing (should always return true).
        /// </summary>
        [Category( "Behavior" ), Description( "Returns true when the currently loaded media file supports pausing (should always return true)." )]
        public bool CanPause
        {
            get
            {
                return this.IsLoaded;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file supports playing forwards (should always return true).
        /// </summary>
        [Category( "Behavior" ), Description( "Returns true when the currently loaded media file supports playing forwards (should always return true)." )]
        public bool CanPlayForwards
        {
            get
            {
                return this.IsLoaded;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file supports stepping forward.
        /// </summary>
        [Category( "Behavior" ), Description( "Returns true when the currently loaded media file supports stepping forward." )]
        public bool CanStepForward
        {
            get
            {
                if ( this.IsLoaded )
                {
                    return m_canStepForward;
                }

                return false;
            }
        }

        /// <summary>
        /// Returns true when the currently loaded media file supports fast forward.
        /// </summary>
        [Category( "Behavior" ), Description( "Returns true when the currently loaded media file supports fast forward." )]
        public bool CanFastForward
        {
            get
            {
                if ( this.IsLoaded )
                {
                    return m_canFastForward;
                }

                return false;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public decimal StartTime
        {
            get
            {
                return (decimal)m_startPosition / 10000000.0M;
            }
            set
            {
                long val = (long)(value * 10000000.0M);
                if ( m_startPosition != val )
                {
                    m_startPosition = val;

                    if ( (value < this.CurrentTime) && (m_mediaControl != null) && (m_mediaSeeking != null) )
                    {
                        int hr = m_mediaControl.Pause();

                        hr = m_mediaSeeking.SetPositions( new DsLong( m_startPosition ), AMSeekingSeekingFlags.AbsolutePositioning,
                            null, AMSeekingSeekingFlags.NoPositioning );

                        if ( m_currentState == PlayState.Running )
                        {
                            hr = m_mediaControl.Run();
                        }
                        else
                        {
                            // Display the first frame to indicate the reset condition
                            hr = m_mediaControl.Pause();
                            this.renderingPanel.Invalidate();
                        }
                    }
                }
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public decimal EndTime
        {
            get
            {
                return (decimal)m_stopPosition / 10000000.0M;
            }
            set
            {
                long val = (long)(value * 10000000.0M);
                if ( m_stopPosition != val )
                {
                    m_stopPosition = val;

                    if ( this.IsLoaded && (m_mediaControl != null) && (m_mediaSeeking != null ) )
                    {
                        int hr = m_mediaControl.Pause();

                        hr = m_mediaSeeking.SetPositions( null, AMSeekingSeekingFlags.NoPositioning,
                            new DsLong( m_stopPosition ), AMSeekingSeekingFlags.AbsolutePositioning );

                        if ( m_currentState == PlayState.Running )
                        {
                            hr = m_mediaControl.Run();
                        }
                        else
                        {
                            // Display the first frame to indicate the reset condition
                            hr = m_mediaControl.Pause();
                            this.renderingPanel.Invalidate();
                        }
                    }
                }
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public decimal CurrentTime
        {
            get
            {
                if ( this.IsLoaded && (m_mediaSeeking != null) )
                {
                    long currentPosition;
                    int hr = m_mediaSeeking.GetCurrentPosition( out currentPosition );
                    if ( hr == 0 )
                    {
                        return (decimal)currentPosition / 10000000.0M;
                    }
                }

                return m_externalTime;
            }
            set
            {
                if ( this.IsLoaded && (m_mediaControl != null) && (m_mediaSeeking != null) )
                {
                    long val = (long)(value * 10000000.0M);

                    int hr = m_mediaControl.Pause();

                    hr = m_mediaSeeking.SetPositions( new DsLong( val ), AMSeekingSeekingFlags.AbsolutePositioning,
                        null, AMSeekingSeekingFlags.NoPositioning );
                    
                    if ( m_currentState == PlayState.Running )
                    {
                        hr = m_mediaControl.Run();
                    }
                    else
                    {
                        // Display the first frame to indicate the reset condition
                        hr = m_mediaControl.Pause();
                        this.renderingPanel.Invalidate();
                    }
                }
                else
                {
                    m_externalTime = value;

                    // invalidate just the top line where the timecode may be displaying
                    this.renderingPanel.Invalidate( new Rectangle( 0, 0, this.renderingPanel.Width, this.renderingPanel.Font.Height ) );
                }
            }
        }
        #endregion

        #region Event Handlers
        private void SystemEvents_DisplaySettingsChanged( object sender, EventArgs e )
        {
            if ( (m_windowlessCtrl != null) && !this.IsAudio )
            {
                int hr = m_windowlessCtrl.DisplayModeChanged();
            }
        }

        private void renderingPanel_ClientSizeChanged( object sender, EventArgs e )
        {
            if ( this.IsVideo )
            {
                MoveVideoWindow();
            }
            else
            {
                this.renderingPanel.Invalidate();
            }
        }

        private void renderingPanel_Move( object sender, EventArgs e )
        {
            if ( this.IsVideo )
            {
                MoveVideoWindow();
            }
            else
            {
                this.renderingPanel.Invalidate();
            }
        }

        private void renderingPanel_Paint( object sender, PaintEventArgs e )
        {
            if ( this.IsVideo )
            {
                if ( (m_windowlessCtrl != null) && !this.IsAudio )
                {
                    IntPtr hdc = e.Graphics.GetHdc();
                    int hr = m_windowlessCtrl.RepaintVideo( this.renderingPanel.Handle, hdc );
                    e.Graphics.ReleaseHdc( hdc );
                }
            }
            else
            {
                if ( this.DisplaySafeZone )
                {
                    int width = this.renderingPanel.Width * 4 / 5;
                    int height = this.renderingPanel.Height * 4 / 5;
                    int x = (this.renderingPanel.Width - width) / 2;
                    int y = (this.renderingPanel.Height - height) / 2;

                    e.Graphics.DrawRectangle( Pens.Green, x, y, width, height );
                }

                if ( this.DisplayTimecode )
                {
                    TimeSpan span = TimeSpan.FromSeconds( (double)this.CurrentTime );
                    e.Graphics.DrawString( span.ToString(), this.renderingPanel.Font, Brushes.White, 0.0f, 0.0f );
                }

                m_bitmapMutex.WaitOne();

                if ( m_bitmaps.Count > 0 )
                {
                    Bitmap bmp = null;
                    Graphics g = null;
                    try
                    {
                        Size screenResolution = m_bitmaps[0].Bitmap.Size;
                        bmp = new Bitmap( screenResolution.Width, screenResolution.Height, PixelFormat.Format32bppArgb );

                        g = Graphics.FromImage( bmp );

                        // Clear the bitmap with complete transparency
                        g.Clear( Color.Transparent );

                        // add each of the overlays
                        foreach ( BitmapOverlay overlay in m_bitmaps )
                        {
                            if ( overlay.Show )
                            {
                                g.DrawImage( overlay.Bitmap, overlay.Point );
                            }
                        }

                        // cleanup
                        g.Dispose();
                        g = null;

                        e.Graphics.DrawImage( bmp, this.renderingPanel.ClientRectangle );

                        bmp.Dispose();
                        bmp = null;
                    }
                    catch
                    {
                        if ( g != null )
                        {
                            g.Dispose();
                        }

                        if ( bmp != null )
                        {
                            bmp.Dispose();
                        }
                    }
                }

                m_bitmapMutex.ReleaseMutex();
            }
        }

        private void renderingPanel_Resize( object sender, EventArgs e )
        {
            if ( this.IsVideo )
            {
                MoveVideoWindow();
            }
            else
            {
                this.renderingPanel.Invalidate();
            }
        }

        private void MediaPlayerControl_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Escape )
            {
                this.IsFullScreen = false;
                
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void timer_Tick( object sender, EventArgs e )
        {
            decimal time = this.CurrentTime;
            if ( time != m_lastTime )
            {
                m_lastTime = time;

                if ( this.IsAudio )
                {
                    // invalidate just the top line where the timecode may be displaying
                    this.renderingPanel.Invalidate( new Rectangle( 0, 0, this.renderingPanel.Width, this.renderingPanel.Font.Height ) );
                }

                OnCurrentTimeChanged();
            }
        }
        #endregion

        #region Public Functions
        public void LoadFile( string filename)
        {
            CloseFile();

            try
            {
                // Reset status variables
                m_currentState = PlayState.Stopped;
                m_currentVolume = c_volumeFull;

                // Start playing the media file
                PlayMovieInWindow( filename );
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.ToString() );

                CloseFile();
            }
        }

        public void CloseFile()
        {
            int hr = 0;

            // Stop media playback
            if ( m_mediaControl != null )
            {
                hr = m_mediaControl.Stop();
            }            

            // Free DirectShow interfaces
            CloseInterfaces();
            
            // Clear global flags
            m_currentState = PlayState.Stopped;
            m_isAudio = true;
            m_isFullScreen = false;

            this.renderingPanel.Invalidate();

            m_filename = string.Empty;

            // No current media state
            m_currentState = PlayState.Init;
        }

        public void Rewind()
        {
            if ( !this.IsLoaded )
            {
                return;
            }

            InitPositions();

            // Go back to beginning
            if ( m_mediaSeeking != null )
            {
                Stop();

                m_currentState = PlayState.Paused;

                // FIXME: see if we can do a true rewind
            }
        }

        public void StepBackward()
        {
            if ( !this.IsLoaded )
            {
                return;
            }

            InitPositions();

            Pause();

            this.CurrentTime -= 1.0M / 30.0M;
        }

        public void PlayBackwards()
        {
            if ( !this.IsLoaded )
            {
                return;
            }

            InitPositions();

            /* FIXME
            SetRate( -1.0f );

            if ( m_currentState == PlayState.Paused )
            {
                // Run the graph to play the media file
                int hr = m_mediaControl.Run();
                DsError.ThrowExceptionForHR( hr );

                m_currentState = PlayState.Running;
            }
            */
        }

        public void Pause()
        {
            if ( !this.IsLoaded )
            {
                return;
            }

            if ( m_currentState == PlayState.Running )
            {
                if ( m_mediaControl.Pause() >= 0 )
                {
                    m_currentState = PlayState.Paused;
                }
            }
        }

        public void Stop()
        {
            if ( !this.IsLoaded )
            {
                return;
            }

            int hr = 0;
            DsLong pos = new DsLong( 0 );

            if ( (m_mediaControl == null) || (m_mediaSeeking == null) )
            {
                return;
            }

            // Stop and reset position to beginning
            if ( (m_currentState == PlayState.Paused) || (m_currentState == PlayState.Running) )
            {
                hr = m_mediaControl.Stop();
                m_currentState = PlayState.Stopped;

                // Seek to the beginning
                hr = m_mediaSeeking.SetPositions( new DsLong( m_startPosition ), AMSeekingSeekingFlags.AbsolutePositioning, 
                    new DsLong( m_stopPosition ), AMSeekingSeekingFlags.NoPositioning );

                // Display the first frame to indicate the reset condition
                hr = m_mediaControl.Pause();
            }
        }

        public void PlayForwards()
        {
            if ( !this.IsLoaded )
            {
                return;
            }

            InitPositions();

            SetRate( 1.0f );

            if ( m_currentState == PlayState.Paused )
            {
                // Run the graph to play the media file
                int hr = m_mediaControl.Run();
                DsError.ThrowExceptionForHR( hr );

                m_currentState = PlayState.Running;
            }
        }

        public void StepForward()
        {
            if ( !this.IsLoaded )
            {
                return;
            }

            InitPositions();

            StepOneFrame();
        }

        public void FastForward()
        {
            if ( !this.IsLoaded )
            {
                return;
            }

            InitPositions();

            if ( m_currentState == PlayState.Running )
            {
                double rate = m_currentPlaybackRate;
                if ( (m_currentPlaybackRate > -1.0f) && (m_currentPlaybackRate < 1.0f) )
                {
                    if ( m_currentPlaybackRate != 0.25 )
                    {
                        rate = 0.25;
                    }
                    else
                    {
                        rate = 0.5;
                    }
                }
                else
                {
                    if ( m_currentPlaybackRate == 2.0f )
                    {
                        rate = 3.0f;
                    }
                    else
                    {
                        rate = 2.0f;
                    }
                }

                if ( rate != m_currentPlaybackRate )
                {
                    SetRate( rate );
                }
            }
            else if ( m_currentState == PlayState.Paused )
            {
                double rate = m_currentPlaybackRate;
                if ( m_currentPlaybackRate != 0.25 )
                {
                    rate = 0.25;
                }
                else
                {
                    rate = 0.5;
                }

                if ( rate != m_currentPlaybackRate )
                {
                    SetRate( rate );
                }

                // Run the graph to play the media file
                int hr = m_mediaControl.Run();
                DsError.ThrowExceptionForHR( hr );

                m_currentState = PlayState.Running;
            }
        }

        public void ToggleFullScreen()
        {
            this.IsFullScreen = !this.IsFullScreen;
        }

        public void AddBitmap( Bitmap bmp )
        {
            m_bitmapMutex.WaitOne();

            int hash = bmp.GetHashCode();
            bool found = false;
            foreach ( BitmapOverlay overlay in m_bitmaps )
            {
                if ( overlay.Hash == hash )
                {
                    found = true;
                    break;
                }
            }
            
            if ( !found )
            {
                m_bitmaps.Add( new BitmapOverlay( hash, bmp ) );
            }

            if ( m_compositor != null )
            {
                m_compositor.AddBitmap( bmp );
            }

            m_bitmapMutex.ReleaseMutex();
        }

        public void AddBitmap( Bitmap bmp, Point p )
        {
            m_bitmapMutex.WaitOne();

            int hash = bmp.GetHashCode();
            bool found = false;
            foreach ( BitmapOverlay overlay in m_bitmaps )
            {
                if ( overlay.Hash == hash )
                {
                    found = true;
                    break;
                }
            }

            if ( !found )
            {
                m_bitmaps.Add( new BitmapOverlay( hash, bmp, p ) );
            }

            if ( m_compositor != null )
            {
                m_compositor.AddBitmap( bmp, p );
            }

            m_bitmapMutex.ReleaseMutex();
        }

        public void RemoveBitmap( Bitmap bmp )
        {
            m_bitmapMutex.WaitOne();

            int hash = bmp.GetHashCode();
            int indexOf = -1;
            for ( int i = 0; i < m_bitmaps.Count; ++i )
            {
                if ( m_bitmaps[i].Hash == hash )
                {
                    indexOf = i;
                    break;
                }
            }

            if ( indexOf != -1 )
            {
                m_bitmaps.RemoveAt( indexOf );
            }

            if ( m_compositor != null )
            {
                m_compositor.RemoveBitmap( bmp );
            }

            m_bitmapMutex.ReleaseMutex();
        }

        public void RefreshBitmap( Bitmap bmp )
        {
            m_bitmapMutex.WaitOne();

            if ( !this.IsVideo )
            {
                this.renderingPanel.Invalidate();
            }

            if ( m_compositor != null )
            {
                m_compositor.RefreshBitmap( bmp );
            }

            m_bitmapMutex.ReleaseMutex();
        }

        public void ShowBitmap( Bitmap bmp )
        {
            m_bitmapMutex.WaitOne();

            int hash = bmp.GetHashCode();
            foreach ( BitmapOverlay overlay in m_bitmaps )
            {
                if ( overlay.Hash == hash )
                {
                    if ( !overlay.Show )
                    {
                        overlay.Show = true;

                        if ( !this.IsVideo )
                        {
                            this.renderingPanel.Invalidate();
                        }
                    }

                    break;
                }
            }

            if ( m_compositor != null )
            {
                m_compositor.ShowBitmap( bmp );
            }

            m_bitmapMutex.ReleaseMutex();
        }

        public void HideBitmap( Bitmap bmp )
        {
            m_bitmapMutex.WaitOne();

            int hash = bmp.GetHashCode();
            foreach ( BitmapOverlay overlay in m_bitmaps )
            {
                if ( overlay.Hash == hash )
                {
                    if ( overlay.Show )
                    {
                        overlay.Show = false;

                        if ( !this.IsVideo )
                        {
                            this.renderingPanel.Invalidate();
                        }
                    }

                    break;
                }
            }

            if ( m_compositor != null )
            {
                m_compositor.HideBitmap( bmp );
            }

            m_bitmapMutex.ReleaseMutex();
        }

        public void ClearBitmaps()
        {
            m_bitmapMutex.WaitOne();

            m_bitmaps.Clear();

            if ( m_compositor != null )
            {
                m_compositor.ClearBitmaps();
            }

            m_bitmapMutex.ReleaseMutex();
        }
        #endregion

        #region Private Functions
        private void PlayMovieInWindow( string filename )
        {
            int hr = 0;

            if ( String.IsNullOrEmpty( filename ) )
            {
                return;
            }

            m_filename = filename;
            m_startPosition = 0;
            m_stopPosition = 0;

            m_filterGraph = (IFilterGraph2)(new FilterGraph());
            m_graphBuilder = (IGraphBuilder)m_filterGraph;

            try
            {
                m_vmr9 = (IBaseFilter)(new VideoMixingRenderer9());

                ConfigureVMR9InWindowlessMode();

                hr = m_filterGraph.AddFilter( m_vmr9, "Video Mixing Renderer 9" );
                DsError.ThrowExceptionForHR( hr );
            }
            catch ( Exception e )
            {
                // This might happen if we load an audio file.
                Console.WriteLine( e.ToString() );
            }

            // Have the graph builder construct its the appropriate graph automatically
            hr = m_filterGraph.RenderFile( filename, null );
            DsError.ThrowExceptionForHR( hr );

            // QueryInterface for DirectShow interfaces
            m_mediaControl = (IMediaControl)m_graphBuilder;
            m_mediaEventEx = (IMediaEventEx)m_graphBuilder;
            m_mediaSeeking = (IMediaSeeking)m_graphBuilder;

            // Query for video interfaces, which may not be relevant for audio files
            m_videoWindow = m_graphBuilder as IVideoWindow;
            m_basicVideo = m_graphBuilder as IBasicVideo;

            // Query for audio interfaces, which may not be relevant for video-only files
            m_basicAudio = m_graphBuilder as IBasicAudio;

            if ( m_mediaSeeking != null )
            {
                // retrieve the start and stop positions
                hr = m_mediaSeeking.GetPositions( out m_startPosition, out m_stopPosition );

                AMSeekingSeekingCapabilities capabilities = AMSeekingSeekingCapabilities.CanSeekBackwards;
                hr = m_mediaSeeking.CheckCapabilities( ref capabilities );
                m_canRewind = hr == 0;

                capabilities |= AMSeekingSeekingCapabilities.CanPlayBackwards;
                hr = m_mediaSeeking.CheckCapabilities( ref capabilities );
                m_canPlayBackwards = hr == 0;

                capabilities |= AMSeekingSeekingCapabilities.CanSeekForwards;
                hr = m_mediaSeeking.CheckCapabilities( ref capabilities );
                m_canFastForward = hr == 0;
            }

            // Is this an audio-only file (no video component)?
            CheckVisibility();

            // Have the graph signal event via window callbacks for performance
            hr = m_mediaEventEx.SetNotifyWindow( this.Handle, c_WMGraphNotify, IntPtr.Zero );
            DsError.ThrowExceptionForHR( hr );

            AddHandlers();

            if ( !this.IsAudio )
            {
                int ipWidth, ipHeight, ipARWidth, ipARHeight;
                m_windowlessCtrl.GetNativeVideoSize( out ipWidth, out ipHeight, out ipARWidth, out ipARHeight );
                m_resolution = new Size( ipWidth, ipHeight );

                GetFrameStepInterface();

                m_bitmapMutex.WaitOne();

                // Add existing overlays, and turn them on, if needed
                foreach ( BitmapOverlay overlay in m_bitmaps )
                {
                    AddBitmap( overlay.Bitmap, overlay.Point );

                    if ( overlay.Show )
                    {
                        ShowBitmap( overlay.Bitmap );
                    }
                }

                m_bitmapMutex.ReleaseMutex();
            }
            else
            {
                m_canStepForward = false;
                m_canStepBackward = false;
            }

            if ( m_basicAudio != null )
            {
                int volume;
                hr = m_basicAudio.get_Volume( out volume );
                m_haveAudio = hr == 0;

                if ( m_haveAudio )
                {
                    volume = m_currentVolume;
                    if ( this.IsMuted )
                    {
                        volume = c_volumeSilence;
                    }

                    // Set new volume
                    hr = m_basicAudio.put_Volume( volume );
                }
            }
            else
            {
                m_haveAudio = false;
            }

            // Complete window initialization
            m_isFullScreen = false;
            m_currentPlaybackRate = 1.0;

#if DEBUG
            m_rot = new DsROTEntry( m_graphBuilder );
#endif
            m_currentState = PlayState.Stopped;
        }

        private void ConfigureVMR9InWindowlessMode()
        {
            int hr = 0;

            IVMRFilterConfig9 filterConfig = (IVMRFilterConfig9)m_vmr9;

            // Must be called before calling SetImageCompositor
            hr = filterConfig.SetNumberOfStreams( 1 );
            DsError.ThrowExceptionForHR( hr );

            // Create an instance of the Compositor
            m_compositor = new MediaPlayerCompositor();
            m_compositor.DisplayTimecode = this.DisplayTimecode;
            m_compositor.DisplaySafeZone = this.DisplaySafeZone;

            // Configure the filter with the Compositor
            hr = filterConfig.SetImageCompositor( m_compositor );
            DsError.ThrowExceptionForHR( hr );

            // Change VMR9 mode to Windowless
            hr = filterConfig.SetRenderingMode( VMR9Mode.Windowless );
            DsError.ThrowExceptionForHR( hr );

            m_windowlessCtrl = (IVMRWindowlessControl9)m_vmr9;

            // Set rendering window
            hr = m_windowlessCtrl.SetVideoClippingWindow( this.renderingPanel.Handle );
            DsError.ThrowExceptionForHR( hr );

            // Set Aspect-Ratio
            hr = m_windowlessCtrl.SetAspectRatioMode( VMR9AspectRatioMode.LetterBox );
            DsError.ThrowExceptionForHR( hr );

            // Call the resize handler to configure the output size
            MoveVideoWindow();
        }

        private void InitPositions()
        {
            if ( m_currentState == PlayState.Stopped )
            {
                // Seek to the beginning
                int hr = m_mediaSeeking.SetPositions( new DsLong( m_startPosition ), AMSeekingSeekingFlags.AbsolutePositioning,
                    new DsLong( m_stopPosition ), AMSeekingSeekingFlags.NoPositioning );

                // Display the first frame to indicate the reset condition
                hr = m_mediaControl.Pause();
                this.renderingPanel.Invalidate();

                m_currentState = PlayState.Paused;
            }
        }

        private void MoveVideoWindow()
        {
            if ( m_windowlessCtrl != null )
            {
                int hr = m_windowlessCtrl.SetVideoPosition( null, DsRect.FromRectangle( this.renderingPanel.ClientRectangle ) );
            }
        }

        private void CheckVisibility()
        {
            if ( (m_windowlessCtrl == null) || (m_basicVideo == null) )
            {
                // Audio-only files have no video interfaces.  This might also
                // be a file whose video component uses an unknown video codec.
                m_isAudio = true;
                return;
            }
            else
            {
                // Clear the global flag
                m_isAudio = false;
            }

            int width, height, arWidth, arHeight;
            int hr = m_windowlessCtrl.GetNativeVideoSize( out width, out height, out arWidth, out arHeight );
            DsError.ThrowExceptionForHR( hr );

            if ( (width == 0) || (height == 0) )
            {
                m_isAudio = true;
            }
        }

        /// <summary>
        /// Some video renderers support stepping media frame by frame with the
        /// IVideoFrameStep interface.  See the interface documentation for more
        /// details on frame stepping.
        /// </summary>
        /// <returns></returns>
        private bool GetFrameStepInterface()
        {
            int hr = 0;

            IVideoFrameStep frameStepTest = null;

            // Get the frame step interface, if supported
            frameStepTest = (IVideoFrameStep)m_graphBuilder;

            // Check if this decoder can step
            hr = frameStepTest.CanStep( 0, null );
            if ( hr == 0 )
            {
                m_frameStep = frameStepTest;

                m_canStepForward = true;
                m_canStepBackward = true;

                return true;
            }
            else
            {
                // BUG 1560263 found by husakm (thanks)...
                // Marshal.ReleaseComObject(frameStepTest);
                m_frameStep = null;

                m_canStepForward = false;
                m_canStepBackward = false;

                return false;
            }
        }

        private void CloseInterfaces()
        {
            int hr = 0;

            try
            {
                lock ( this )
                {
                    if ( m_mediaControl != null )
                    {
                        m_mediaControl.Stop();
                    }

                    if ( m_mediaEventEx != null )
                    {
                        hr = m_mediaEventEx.SetNotifyWindow( IntPtr.Zero, 0, IntPtr.Zero );
                        DsError.ThrowExceptionForHR( hr );
                    }

#if DEBUG
                    if ( m_rot != null )
                    {
                        m_rot.Dispose();
                        m_rot = null;
                    }
#endif
                    RemoveHandlers();

                    // Release and zero DirectShow interfaces
                    if ( m_mediaEventEx != null )
                    {
                        m_mediaEventEx = null;
                    }

                    if ( m_mediaSeeking != null )
                    {
                        m_mediaSeeking = null;
                    }

                    if ( m_mediaControl != null )
                    {
                        m_mediaControl = null;
                    }

                    if ( m_basicAudio != null )
                    {
                        m_basicAudio = null;
                    }

                    if ( m_basicVideo != null )
                    {
                        m_basicVideo = null;
                    }

                    if ( m_videoWindow != null )
                    {
                        m_videoWindow = null;
                    }

                    if ( m_frameStep != null )
                    {
                        m_frameStep = null;
                    }

                    if ( m_graphBuilder != null )
                    {
                        m_graphBuilder = null;
                    }

                    if ( m_compositor != null )
                    {
                        m_compositor.Dispose();
                        m_compositor = null;
                    }

                    if ( m_vmr9 != null )
                    {
                        Marshal.ReleaseComObject( m_vmr9 );
                        m_vmr9 = null;
                        m_windowlessCtrl = null;
                    }

                    if ( m_filterGraph != null )
                    {
                        Marshal.ReleaseComObject( m_filterGraph );
                        m_filterGraph = null;
                    }

                    GC.Collect();
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.ToString() );
            }
        }

        private void AddHandlers()
        {
            if ( this.IsVideo )
            {
                SystemEvents.DisplaySettingsChanged += new EventHandler( SystemEvents_DisplaySettingsChanged ); // for WM_DISPLAYCHANGE
            }
        }

        private void RemoveHandlers()
        {           
            if ( this.IsVideo )
            {
                SystemEvents.DisplaySettingsChanged -= new EventHandler( SystemEvents_DisplaySettingsChanged );
            }
        }

        private int StepOneFrame()
        {
            int hr = 0;

            // If the Frame Stepping interface exists, use it to step one frame
            if ( m_frameStep != null )
            {
                // The graph must be paused for frame stepping to work
                if ( m_currentState != PlayState.Paused )
                {
                    Pause();
                }

                // Step the requested number of frames, if supported
                hr = m_frameStep.Step( 1, null );
            }

            return hr;
        }

        private int StepFrames( int nFramesToStep )
        {
            int hr = 0;

            // If the Frame Stepping interface exists, use it to step frames
            if ( m_frameStep != null )
            {
                // The renderer may not support frame stepping for more than one
                // frame at a time, so check for support.  S_OK indicates that the
                // renderer can step nFramesToStep successfully.
                hr = m_frameStep.CanStep( nFramesToStep, null );
                if ( hr == 0 )
                {
                    // The graph must be paused for frame stepping to work
                    if ( m_currentState != PlayState.Paused )
                    {
                        Pause();
                    }

                    // Step the requested number of frames, if supported
                    hr = m_frameStep.Step( nFramesToStep, null );
                }
            }

            return hr;
        }

        private int ModifyRate( double dRateAdjust )
        {
            int hr = 0;
            double dRate;

            // If the IMediaPosition interface exists, use it to set rate
            if ( (m_mediaSeeking != null) && (dRateAdjust != 0.0) )
            {
                hr = m_mediaSeeking.GetRate( out dRate );
                if ( hr == 0 )
                {
                    // Add current rate to adjustment value
                    double dNewRate = dRate + dRateAdjust;
                    hr = m_mediaSeeking.SetRate( dNewRate );

                    // Save global rate
                    if ( hr == 0 )
                    {
                        m_currentPlaybackRate = dNewRate;
                    }
                }
            }

            return hr;
        }

        private int SetRate( double rate )
        {
            int hr = 0;

            // If the IMediaPosition interface exists, use it to set rate
            if ( m_mediaSeeking != null )
            {
                hr = m_mediaSeeking.SetRate( rate );
                if ( hr >= 0 )
                {
                    m_currentPlaybackRate = rate;
                }
            }

            return hr;
        }

        private void HandleGraphEvent()
        {
            int hr = 0;
            EventCode evCode;
            IntPtr evParam1, evParam2;

            // Make sure that we don't access the media event interface
            // after it has already been released.
            if ( m_mediaEventEx == null )
            {
                return;
            }

            // Process all queued events
            while ( m_mediaEventEx.GetEvent( out evCode, out evParam1, out evParam2, 0 ) == 0 )
            {
                // Free memory associated with callback, since we're not using it
                hr = m_mediaEventEx.FreeEventParams( evCode, evParam1, evParam2 );

                // If this is the end of the clip, reset to beginning
                if ( evCode == EventCode.Complete )
                {
                    Pause();

                    this.CurrentTime = (decimal)m_startPosition / 10000000.0M;

                    if ( m_loop )
                    {
                        PlayForwards();
                    }
                }
            }
        }

        protected override void WndProc( ref Message m )
        {
            switch ( m.Msg )
            {
                case c_WMGraphNotify:
                    {
                        HandleGraphEvent();
                        break;
                    }
            }

            // Pass this message to the video window for notification of system changes
            if ( m_videoWindow != null )
            {
                m_videoWindow.NotifyOwnerMessage( m.HWnd, m.Msg, m.WParam, m.LParam );
            }

            base.WndProc( ref m );
        }
        #endregion
    }
}
