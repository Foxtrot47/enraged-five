using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using LocalizedTextControls;
using RSG.Base.Serialization;

namespace SubtitleEditorCore
{
    /// <summary>
    /// Holds all of the data associated with a LocalizedTextData event on the Timeline
    /// </summary>
    [Serializable]
    public class SubtitleEventData : IRageClonableObject
    {
        public SubtitleEventData()
        {
            
        }

        #region Variables
        private string m_localizedTextID = string.Empty;
        private float m_minT = 0.0f;
        private float m_maxT = 0.0f;
        private SubtitleTransitionData m_transitionIn = new SubtitleTransitionData();
        private SubtitleTransitionData m_transitionOut = new SubtitleTransitionData();

        private LocalizedTextData m_localizedTextData = null;
        #endregion

        #region Properties
        /// <summary>
        /// The ID of the LocalizedTextData that is associated with this Subtitle Event.
        /// </summary>
        [DefaultValue( "" ), Description( "The ID of the LocalizedTextData that is associated with this Subtitle Event." )]
        public string LocalizedTextID
        {
            get
            {
                return m_localizedTextID;
            }
            set
            {
                if ( value != null )
                {
                    m_localizedTextID = value;
                }
                else
                {
                    m_localizedTextID = string.Empty;
                }
            }
        }

        [XmlIgnore, ReadOnly( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextData LocalizedTextData
        {
            get
            {
                return m_localizedTextData;
            }
            set
            {
                m_localizedTextData = value;
            }
        }

        /// <summary>
        /// The start phase of this Subtitle Event.
        /// </summary>
        [DefaultValue( 0.0f ), Description( "The start phase of this Subtitle Event." )]
        public float MinT
        {
            get
            {
                return m_minT;
            }
            set
            {
                m_minT = value;
            }
        }

        /// <summary>
        /// The end phase of this Subtitle Event.
        /// </summary>
        [DefaultValue( 0.0f ), Description( "The end phase of this Subtitle Event." )]
        public float MaxT
        {
            get
            {
                return m_maxT;
            }
            set
            {
                m_maxT = value;
            }
        }

        /// <summary>
        /// The Transition to apply at the beginning.
        /// </summary>
        [Description( "The Transition to apply at the beginning." )]
        public SubtitleTransitionData TransitionIn
        {
            get
            {
                return m_transitionIn;
            }
            set
            {
                if ( value != null )
                {
                    m_transitionIn = value;
                }
                else
                {
                    m_transitionIn.Reset();
                }
            }
        }

        /// <summary>
        /// The Transition to apply at the end.
        /// </summary>
        [Description( "The Transition to apply at the end." )]
        public SubtitleTransitionData TransitionOut
        {
            get
            {
                return m_transitionOut;
            }
            set
            {
                if ( value != null )
                {
                    m_transitionOut = value;
                }
                else
                {
                    m_transitionOut.Reset();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleEventData )
            {
                return Equals( obj as SubtitleEventData );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format( "{0}: ({1:0.00}->{2:0.00})", this.LocalizedTextID, this.MinT, this.MaxT );
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            SubtitleEventData data = new SubtitleEventData();
            data.CopyFrom( this );
            return data;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleEventData )
            {
                CopyFrom( other as SubtitleEventData );
            }
        }

        public void Reset()
        {
            this.LocalizedTextData = null;
            this.LocalizedTextID = string.Empty;
            this.MaxT = 0.0f;
            this.MinT = 0.0f;
            this.TransitionIn.Reset();
            this.TransitionOut.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleEventData other )
        {
            if ( other == null )
            {
                return;
            }

            this.LocalizedTextID = other.LocalizedTextID;
            this.MaxT = other.MaxT;
            this.MinT = other.MinT;
            this.TransitionIn.CopyFrom( other.TransitionIn );
            this.TransitionOut.CopyFrom( other.TransitionOut );
        }

        public bool Equals( SubtitleEventData other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.LocalizedTextID == other.LocalizedTextID)
                && (this.MaxT == other.MaxT)
                && (this.MinT == other.MinT)
                && this.TransitionIn.Equals( other.TransitionIn )
                && this.TransitionOut.Equals( other.TransitionOut );
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of SubtitleEventData items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class SubtitleEventDataCollection : rageSerializableList<SubtitleEventData>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            SubtitleEventDataCollection collection = new SubtitleEventDataCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion

        #region Public Functions
        public SubtitleEventData Find( string id )
        {
            foreach ( SubtitleEventData data in this )
            {
                if ( data.LocalizedTextID == id )
                {
                    return data;
                }
            }

            return null;
        }
        #endregion
    }

    [Serializable]
    public class SubtitleTransitionData : IRageClonableObject
    {
        public SubtitleTransitionData()
        {
            
        }

        #region Variables
        private bool m_enabled = false;
        private float m_duration = 0.5f;
        private string m_typeName = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// Enable this transition.
        /// </summary>
        [DefaultValue( false ), Description( "Enable this transition." )]
        public bool Enabled
        {
            get
            {
                return m_enabled;
            }
            set
            {
                m_enabled = value;
            }
        }

        /// <summary>
        /// The length of the transition.
        /// </summary>
        [DefaultValue( 0.5f ), Description( "The length of the transition." )]
        public float Duration
        {
            get
            {
                return m_duration;
            }
            set
            {
                m_duration = value;
            }
        }

        /// <summary>
        /// The game-specific type of transition to perform.
        /// </summary>
        [DefaultValue( "" ), Description( "The game-specific type of transition to perform." )]
        public string TypeName
        {
            get
            {
                return m_typeName;
            }
            set
            {
                if ( value != null )
                {
                    m_typeName = value;
                }
                else
                {
                    m_typeName = string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleTransitionData )
            {
                return Equals( obj as SubtitleTransitionData );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.TypeName;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            SubtitleTransitionData data = new SubtitleTransitionData();
            data.CopyFrom( this );
            return data;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleTransitionData )
            {
                CopyFrom( other as SubtitleTransitionData );
            }
        }

        public void Reset()
        {
            this.Duration = 0.5f;
            this.Enabled = false;
            this.TypeName = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleTransitionData other )
        {
            if ( other == null )
            {
                return;
            }

            this.Duration = other.Duration;
            this.Enabled = other.Enabled;
            this.TypeName = other.TypeName;
        }

        public bool Equals( SubtitleTransitionData other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.Duration == other.Duration)
                && (this.Enabled == other.Enabled)
                && (this.TypeName == other.TypeName);
        }
        #endregion
    }
}
