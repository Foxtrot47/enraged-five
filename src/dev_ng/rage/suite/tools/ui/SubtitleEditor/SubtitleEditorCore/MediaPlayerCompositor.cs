using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

using Microsoft.DirectX.Direct3D;
using Direct3D = Microsoft.DirectX.Direct3D;
using DirectShowLib;
using System.Drawing.Imaging;


namespace SubtitleEditorCore
{
    public class MediaPlayerCompositor : IVMRImageCompositor9, IDisposable
    {
        public MediaPlayerCompositor()
        {
            Device.IsUsingEventHandlers = false;
        }

        #region Internal Structures
        internal class TextureOverlay
        {
            public TextureOverlay( int hash, Bitmap bmp )
            {
                this.Hash = hash;
                this.Bitmap = bmp;
                this.Point = new Point( 0, 0 );
            }

            public TextureOverlay( int hash, Bitmap bmp, Point p )
            {
                this.Hash = hash;
                this.Bitmap = bmp;
                this.Point = p;
            }

            public int Hash = 0;
            public Bitmap Bitmap = null;
            public Point Point = Point.Empty;
            public Texture Texture = null;
            public bool Show = false;
        }
        #endregion

        #region Variables
        private IntPtr m_unmanagedDevice = IntPtr.Zero;
        private Device m_device = null;
        private Sprite m_timecodeSprite = null;
        private Sprite m_overlaySprite = null;
        private Direct3D.Font m_d3dFont = null;
        private System.Drawing.Font m_gdiFont = null;
        private bool m_displayTimecode = true;
        private bool m_displaySafeZone = true;

        private List<TextureOverlay> m_textures = new List<TextureOverlay>();
        private Mutex m_texturesMutex = new Mutex();

        private Texture m_safeZoneTexture = null;
        #endregion

        #region Public Functions
        public void AddBitmap( Bitmap bmp )
        {
            m_texturesMutex.WaitOne();

            int hash = bmp.GetHashCode();
            bool found = false;
            foreach ( TextureOverlay overlay in m_textures )
            {
                if ( overlay.Hash == hash )
                {
                    found = true;
                    break;
                }
            }

            if ( !found )
            {
				m_textures.Add(new TextureOverlay(hash, bmp));
			}

            m_texturesMutex.ReleaseMutex();
        }

        public void AddBitmap( Bitmap bmp, Point p )
        {
            m_texturesMutex.WaitOne();

            int hash = bmp.GetHashCode();
            bool found = false;
            foreach ( TextureOverlay overlay in m_textures )
            {
                if ( overlay.Hash == hash )
                {
                    found = true;
                    break;
                }
            }

            if ( !found )
            {
                m_textures.Add( new TextureOverlay( hash, bmp, p ) );
            }

            m_texturesMutex.ReleaseMutex();
        }

        public void RemoveBitmap( Bitmap bmp )
        {
            m_texturesMutex.WaitOne();

            int hash = bmp.GetHashCode();
            int indexOf = -1;
            for ( int i = 0; i < m_textures.Count; ++i )
            {
                if ( m_textures[i].Hash == hash )
                {
                    indexOf = i;
                    break;
                }
            }

            if ( indexOf != -1 )
            {
                if ( m_textures[indexOf].Texture != null )
                {
                    m_textures[indexOf].Texture.Dispose();
                }

                m_textures.RemoveAt( indexOf );
            }

            m_texturesMutex.ReleaseMutex();
        }

        public void RefreshBitmap( Bitmap bmp )
        {
            m_texturesMutex.WaitOne();

            int hash = bmp.GetHashCode();
            foreach ( TextureOverlay overlay in m_textures )
            {
                if ( overlay.Hash == hash )
                {
                    if ( overlay.Texture != null )
                    {
                        overlay.Texture.Dispose();
                        overlay.Texture = null;
                    }

                    break;
                }
            }

            m_texturesMutex.ReleaseMutex();
        }

        public void ShowBitmap( Bitmap bmp )
        {
            m_texturesMutex.WaitOne();

            int hash = bmp.GetHashCode();
            foreach ( TextureOverlay overlay in m_textures )
            {
                if ( overlay.Hash == hash )
                {
                    overlay.Show = true;
                    break;
                }
            }

            m_texturesMutex.ReleaseMutex();
        }

        public void HideBitmap( Bitmap bmp )
        {
            m_texturesMutex.WaitOne();

            int hash = bmp.GetHashCode();
            foreach ( TextureOverlay overlay in m_textures )
            {
                if ( overlay.Hash == hash )
                {
                    overlay.Show = false;
                    break;
                }
            }

            m_texturesMutex.ReleaseMutex();
        }

        public void ClearBitmaps()
        {
            m_texturesMutex.WaitOne();

            foreach ( TextureOverlay overlay in m_textures )
            {
                if ( overlay.Texture != null )
                {
                    overlay.Texture.Dispose();
                }
            }

            m_textures.Clear();

            m_texturesMutex.ReleaseMutex();
        }
        #endregion

        #region Properties
        public bool DisplayTimecode
        {
            get
            {
                return m_displayTimecode;
            }
            set
            {
                m_displayTimecode = value;
            }
        }

        public bool DisplaySafeZone
        {
            get
            {
                return m_displaySafeZone;
            }
            set
            {
                m_displaySafeZone = value;
            }
        }
        #endregion

        #region IVMRImageCompositor9 Interface
        public int CompositeImage( IntPtr pD3DDevice, IntPtr pddsRenderTarget, AMMediaType pmtRenderTarget, 
            long rtStart, long rtEnd, int dwClrBkGnd, VMR9VideoStreamInfo[] pVideoStreamInfo, int cStreams )
        {
            try
            {
                // Just in case the filter call CompositeImage before InitCompositionDevice (this sometime occure)
                if ( m_unmanagedDevice != pD3DDevice )
                {
                    SetManagedDevice( pD3DDevice );
                }

                // Create a managed Direct3D surface (the Render Target) from the unmanaged pointer.
                // The constructor don't call IUnknown.AddRef but the "destructor" seem to call IUnknown.Release
                // Direct3D seem to be happier with that according to the DirectX log
                Marshal.AddRef( pddsRenderTarget );
                Surface renderTarget = new Surface( pddsRenderTarget );
                SurfaceDescription renderTargetDesc = renderTarget.Description;
                Rectangle renderTargetRect = new Rectangle( 0, 0, renderTargetDesc.Width, renderTargetDesc.Height );

                // Same thing for the first video surface
                // WARNING : This Compositor sample only use the video provided to the first pin.
                Marshal.AddRef( pVideoStreamInfo[0].pddsVideoSurface );
                Surface surface = new Surface( pVideoStreamInfo[0].pddsVideoSurface );
                SurfaceDescription surfaceDesc = surface.Description;
                Rectangle surfaceRect = new Rectangle( 0, 0, surfaceDesc.Width, surfaceDesc.Height );

                // Get the current time (to write it over the video later)
                TimeSpan timeStart = TimeSpan.FromTicks( rtStart );

                // Set the m_device's render target (this doesn't seem to be needed)
                m_device.SetRenderTarget( 0, renderTarget );

                // Copy the whole video surface into the render target
                // it's a de facto surface cleaning...
                m_device.StretchRectangle( surface, surfaceRect, renderTarget, renderTargetRect, TextureFilter.None );

                // sprits's methods need to be called between m_device.BeginScene and m_device.EndScene
                m_device.BeginScene();

                // Init the sprite engine for AlphaBlending operations
                m_timecodeSprite.Begin( SpriteFlags.AlphaBlend );
                m_overlaySprite.Begin( SpriteFlags.AlphaBlend );

                if ( this.DisplayTimecode )
                {
                    // Write the current video time (using the sprite)...
                    m_d3dFont.DrawText( m_timecodeSprite, timeStart.ToString(), Point.Empty, Color.White );                    
                }

                if ( this.DisplaySafeZone )
                {
                    if ( m_safeZoneTexture == null )
                    {
                        int width = renderTargetRect.Width * 4 / 5;
                        int height = renderTargetRect.Height * 4 / 5;
                        int x = (renderTargetRect.Width - width) / 2;
                        int y = (renderTargetRect.Height - height) / 2;

                        Bitmap bmp = new Bitmap( renderTargetRect.Width, renderTargetRect.Height, PixelFormat.Format32bppArgb );

                        Graphics g = Graphics.FromImage( bmp );

                        // Clear the bitmap with complete transparency
                        g.Clear( Color.Transparent );

                        g.DrawRectangle( Pens.Green, x, y, width, height );

                        g.Dispose();

                        m_safeZoneTexture = Texture.FromBitmap( m_device, bmp, Usage.None, Pool.Managed );
                        bmp.Dispose();
                    }

                    m_overlaySprite.Draw2D( m_safeZoneTexture, Rectangle.Empty, Rectangle.Empty, Point.Empty, -1 );
                }

                m_texturesMutex.WaitOne();

                foreach ( TextureOverlay overlay in m_textures )
                {
                    if ( overlay.Texture == null )
                    {
                        overlay.Texture = Texture.FromBitmap( m_device, overlay.Bitmap, Usage.None, Pool.Managed );
                    }

                    if ( overlay.Show )
                    {
                        m_overlaySprite.Draw2D( overlay.Texture, Rectangle.Empty, surfaceRect, Point.Empty, -1 );
                    }
                }

                m_texturesMutex.ReleaseMutex();

                // End the spite engine (drawings take place here)
                m_timecodeSprite.End();
                m_overlaySprite.End();

                // End the screen. 
                m_device.EndScene();

                // No Present required because the rendering is on a render target... 

                // Dispose the managed surface
                surface.Dispose();
                surface = null;

                // and the managed render target
                renderTarget.Dispose();
                renderTarget = null;
            }
            catch ( Exception e )
            {
                Debug.WriteLine( e.ToString() );
            }

            // return a success to the filter
            return 0;
        }

        public int InitCompositionDevice( IntPtr pD3DDevice )
        {
            try
            {
                // Init the compositor with this unmanaged m_device
                if ( m_unmanagedDevice != pD3DDevice )
                {
                    SetManagedDevice( pD3DDevice );
                }
            }
            catch ( Exception e )
            {
                Debug.WriteLine( e.ToString() );
            }

            // return a success to the filter
            return 0;
        }

        public int SetStreamMediaType( int dwStrmID, AMMediaType pmt, bool fTexture )
        {
            // This method is called many times with pmt == null
            if ( pmt == null )
            {
                return 0;
            }

            // This sample don't use this method... but return a success
            return 0;
        }

        public int TermCompositionDevice( IntPtr pD3DDevice )
        {
            try
            {
                // Free the resources each time this method is called
                m_unmanagedDevice = IntPtr.Zero;
                FreeResources();
            }
            catch ( Exception e )
            {
                Debug.WriteLine( e.ToString() );
            }

            // return a success to the filter
            return 0;
        }
        #endregion

        #region IDisposable Interface
        public void Dispose()
        {
            // free resources
            FreeResources();
        }
        #endregion

        #region Private Functions
        private void FreeResources()
        {
            if ( m_safeZoneTexture != null )
            {
                m_safeZoneTexture.Dispose();
                m_safeZoneTexture = null;
            }

            if ( m_d3dFont != null )
            {
                m_d3dFont.Dispose();
                m_d3dFont = null;
            }

            if ( m_timecodeSprite != null )
            {
                m_timecodeSprite.Dispose();
                m_timecodeSprite = null;
            }

            if ( m_overlaySprite != null )
            {
                m_overlaySprite.Dispose();
                m_overlaySprite = null;
            }

            if ( m_device != null )
            {
                m_device.Dispose();
                m_device = null;
            }

            if ( m_gdiFont != null )
            {
                m_gdiFont.Dispose();
                m_gdiFont = null;
            }
        }

		// This function is called whenever the monitor it is running on changes
		private void SetManagedDevice(IntPtr m_unmanagedDevice)
        {
            // Start by freeing everything
            FreeResources();

			// This function is called whenever the monitor it is running on changes, so I need to regen all the texture overlays again
			m_texturesMutex.WaitOne();
			foreach (TextureOverlay overlay in m_textures)
			{
				if(overlay.Texture != null)
				{
					overlay.Texture.Dispose();
					overlay.Texture = null;
				}
			}
			m_texturesMutex.ReleaseMutex();

            // Create a managed Device from the unmanaged pointer
            // The constructor don't call IUnknown.AddRef but the "destructor" seem to call IUnknown.Release
            // Direct3D seem to be happier with that according to the DirectX log
            Marshal.AddRef( m_unmanagedDevice );
            this.m_unmanagedDevice = m_unmanagedDevice;
            m_device = new Device( m_unmanagedDevice );

            // Create helper objects to draw over the video
            m_timecodeSprite = new Sprite( m_device );
            m_overlaySprite = new Sprite( m_device );
            m_gdiFont = new System.Drawing.Font( "Tahoma", 15 );
            m_d3dFont = new Direct3D.Font( m_device, m_gdiFont );
        }
        #endregion
    }
}
