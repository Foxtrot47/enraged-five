using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Forms;
using RSG.Base.Serialization;

namespace SubtitleEditorCore
{
    /// <summary>
    /// Holds all of the per-user ScriptControl settings.
    /// </summary>
    public class ScriptControlSettings : rageSerializableObject
    {
        public ScriptControlSettings()
        {
            Reset();
        }

        #region Variables
        private rageRichTextBoxSearchReplaceSettings m_searchReplaceSettings = new rageRichTextBoxSearchReplaceSettings();
        #endregion

        #region Properties
        /// <summary>
        /// The settings for the RichTextBoxSearchReplaceDialog.
        /// </summary>
        [Description( "The settings for the RichTextBoxSearchReplaceDialog." )]
        public rageRichTextBoxSearchReplaceSettings SearchReplaceSettings
        {
            get
            {
                return m_searchReplaceSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_searchReplaceSettings = value;
                }
                else
                {
                    m_searchReplaceSettings.Reset();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is ScriptControlSettings )
            {
                return Equals( obj as ScriptControlSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            ScriptControlSettings settings = new ScriptControlSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is ScriptControlSettings )
            {
                CopyFrom( other as ScriptControlSettings );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.SearchReplaceSettings.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( ScriptControlSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.SearchReplaceSettings.CopyFrom( other.SearchReplaceSettings );
        }

        public bool Equals( ScriptControlSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return this.SearchReplaceSettings.Equals( other.SearchReplaceSettings );
        }
        #endregion
    }
}
