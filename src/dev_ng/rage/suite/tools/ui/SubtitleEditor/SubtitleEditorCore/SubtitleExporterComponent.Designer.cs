namespace SubtitleEditorCore
{
    partial class SubtitleExporterComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.localizedTextExporterComponent = new LocalizedTextControls.LocalizedTextExporterComponent( this.components );
            this.localizedTextProjectComponent = new LocalizedTextControls.LocalizedTextProjectComponent( this.components );
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler( this.backgroundWorker_DoWork );
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler( this.backgroundWorker_RunWorkerCompleted );
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler( this.backgroundWorker_ProgressChanged );
            // 
            // localizedTextExporterComponent
            // 
            this.localizedTextExporterComponent.ExportBegin += new System.EventHandler( this.localizedTextExporterComponent_ExportBegin );
            this.localizedTextExporterComponent.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler( this.localizedTextExporterComponent_ProgressChanged );
            this.localizedTextExporterComponent.ExportComplete += new System.ComponentModel.RunWorkerCompletedEventHandler( this.localizedTextExporterComponent_ExportComplete );
            this.localizedTextExporterComponent.ExportMessage += new LocalizedTextControls.ExportMessageEventHandler( this.localizedTextExporterComponent_ExportMessage );
            this.localizedTextExporterComponent.CheckOutFile += new LocalizedTextControls.SourceControlEventHandler( this.localizedTextExporterComponent_CheckOutFile );
            // 
            // localizedTextProjectComponent
            // 
            this.localizedTextProjectComponent.FileWatcherEnabled = false;

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private LocalizedTextControls.LocalizedTextExporterComponent localizedTextExporterComponent;
        private LocalizedTextControls.LocalizedTextProjectComponent localizedTextProjectComponent;
    }
}
