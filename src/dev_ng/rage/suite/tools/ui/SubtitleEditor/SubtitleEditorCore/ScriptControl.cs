using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

using LocalizedTextControls;
using RSG.Base.Forms;

namespace SubtitleEditorCore
{
    public partial class ScriptControl : UserControl
    {
        public ScriptControl()
        {
            InitializeComponent();

            m_searchReplaceDialog.FindCurrentRichTextBox += new rageSearchReplaceRichTextBoxEventHandler( searchReplaceDialog_FindCurrentRichTextBox );
            m_searchReplaceDialog.CanReplace = false;
            m_searchReplaceDialog.CanFindAll = false;
            m_searchReplaceDialog.CanReplaceAll = false;
        }

        #region Variables
        private string m_filename = string.Empty;

        private RichTextBoxSearchReplaceDialog m_searchReplaceDialog = new RichTextBoxSearchReplaceDialog();
        #endregion

        #region Properties
        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                if ( m_filename != value )
                {
                    if ( value != null )
                    {
                        m_filename = value;
                    }
                    else
                    {
                        m_filename = string.Empty;
                    }

                    if ( !String.IsNullOrEmpty( this.Filename ) )
                    {
                        this.Text = String.Format( "{0} - Script", this.Filename );                        
                    }
                    else
                    {
                        this.Text = "Script";
                    }
                }
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public ScriptControlSettings ScriptControlSettings
        {
            get
            {
                ScriptControlSettings s = new ScriptControlSettings();
                m_searchReplaceDialog.SetSettingsFromUI( s.SearchReplaceSettings );
                return s;
            }
            set
            {
                if ( value != null )
                {
                    m_searchReplaceDialog.SetUIFromSettings( value.SearchReplaceSettings );
                }
                else
                {
                    this.ScriptControlSettings = new ScriptControlSettings();
                }
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public RichTextBoxSearchReplaceDialog RichTextBoxSearchReplaceDialog
        {
            get
            {
                return m_searchReplaceDialog;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a script file is opened.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when a media file is opened." )]
        public event EventHandler FileOpened;

        /// <summary>
        /// Occurs when a media file is closed.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when a media file is closed." )]
        public event EventHandler FileClosed;

        /// <summary>
        /// Event that is dispatched when the user wants to add a LocalizedTextData item to a LocalizedTextEditorControl.
        /// </summary>
        [Category( "Action" ), Description( "Event that is dispatched when the user wants to add a LocalizedTextData item to a LocalizedTextEditorControl." )]
        public event AddLocalizedTextEventHandler AddLocalizedText;
        #endregion

        #region Event Dispatchers
        protected void OnFileOpened()
        {
            if ( this.FileOpened != null )
            {
                this.FileOpened( this, EventArgs.Empty );
            }
        }

        protected void OnFileClosed()
        {
            if ( this.FileClosed != null )
            {
                this.FileClosed( this, EventArgs.Empty );
            }
        }

        protected void OnNewLocalizedText( AddLocalizedTextEventArgs e )
        {
            if ( this.AddLocalizedText != null )
            {
                this.AddLocalizedText( this, e );
            }
        }
        #endregion

        #region Event Handlers
        private void richTextBox_SelectionChanged( object sender, EventArgs e )
        {
            bool enable = this.richTextBox.SelectedText.Length > 0;
            this.copyToolStripMenuItem.Enabled = enable;
        }

        #region ContextMenuStrip
        private void contextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            this.closeScriptToolStripMenuItem.Enabled = !String.IsNullOrEmpty( this.Filename );
            this.newLocalizedTextToolStripMenuItem.Enabled = this.richTextBox.SelectedText.Length > 0;
            this.copyToolStripMenuItem.Enabled = this.richTextBox.SelectedText.Length > 0;
        }

        private void openScriptToolStripMenuItem_Click( object sender, EventArgs e )
        {
            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                try
                {
                    LoadFile( this.openFileDialog.FileName );
                }
                catch ( Exception ex )
                {
                    rageMessageBox.ShowExclamation( this.FindForm(), ex.Message, "Script File Load Error" );
                    return;
                }

                OnFileOpened();
            }
        }

        private void closeScriptToolStripMenuItem_Click( object sender, EventArgs e )
        {
            CloseFile();

            OnFileClosed();
        }

        private void newLocalizedTextToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.richTextBox.SelectedText.Length > 0 )
            {
                OnNewLocalizedText( new AddLocalizedTextEventArgs( this.richTextBox.SelectedText ) );
            }
        }

        private void copyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.richTextBox.SelectedText.Length > 0 )
            {
                this.richTextBox.Copy();
            }
        }

        private void findToolStripMenuItem_Click( object sender, EventArgs e )
        {
            // provide a default location, if none has been provided
            if ( !m_searchReplaceDialog.LocationSet )
            {
                Utility.CenterLocation( FindForm(), m_searchReplaceDialog );
                m_searchReplaceDialog.LocationSet = true;
            }

            // Show the find/replace form
            if ( !m_searchReplaceDialog.Visible )
            {
                m_searchReplaceDialog.Show( FindForm() );
            }

            m_searchReplaceDialog.Focus();

            m_searchReplaceDialog.InitSearchText( this.richTextBox );
        }

        private void searchAgainDownToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_searchReplaceDialog.SearchAgainDown( this.richTextBox );
        }

        private void searchAgainUpToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_searchReplaceDialog.SearchAgainUp( this.richTextBox );
        }

        private void searchHighlightedDownToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_searchReplaceDialog.SearchHighlightedDown( this.richTextBox );
        }

        private void searchHighllightedUpToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_searchReplaceDialog.SearchHighlightedUp( this.richTextBox );
        }
        #endregion

        private void searchReplaceDialog_FindCurrentRichTextBox( object sender, rageSearchReplaceRichTextBoxEventArgs e )
        {
            if ( this.richTextBox.Focused )
            {
                e.RichTextBox = this.richTextBox;
            }
        }
        #endregion

        #region Public Functions
        public void LoadFile( string filename )
        {
            CloseFile();

            try
            {
                // try to load as rtf first
                this.richTextBox.LoadFile( filename );
            }
            catch 
            {
                // now try plain text
                this.richTextBox.LoadFile( filename, RichTextBoxStreamType.PlainText );
            }

            this.Filename = filename;

            this.closeScriptToolStripMenuItem.Enabled = true;

            AddFileWatcher();
        }

        public void CloseFile()
        {
            RemoveFileWatcher();

            this.richTextBox.Clear();
            this.Filename = string.Empty;

            this.closeScriptToolStripMenuItem.Enabled = false;
        }
        #endregion

        #region Private Functions
        private void AddFileWatcher()
        {
            if ( !String.IsNullOrEmpty( this.Filename ) )
            {
                try
                {
                    if ( !File.Exists( this.Filename ) )
                    {
                        return;
                    }
                }
                catch
                {
                    return;
                }

                this.fileSystemWatcher.Path = Path.GetDirectoryName( this.Filename );
                this.fileSystemWatcher.Filter = Path.GetFileName( this.Filename );
                this.fileSystemWatcher.EnableRaisingEvents = true;
            }
        }

        private void RemoveFileWatcher()
        {
            if ( this.fileSystemWatcher.EnableRaisingEvents )
            {
                this.fileSystemWatcher.EnableRaisingEvents = false;
                this.fileSystemWatcher.Filter = string.Empty;
            }
        }

        private void PromptToReload()
        {
            this.fileSystemWatcher.EnableRaisingEvents = false;

            DialogResult result = rageMessageBox.ShowQuestion( this, "The script file has been externally modified.  Would you like to reload it?",
                "Script File Externally Modified" );
            if ( result == DialogResult.Yes )
            {
                LoadFile( this.Filename );
            }

            this.fileSystemWatcher.EnableRaisingEvents = true;
        }

        private void fileSystemWatcher_Changed( object sender, FileSystemEventArgs e )
        {
            PromptToReload();
        }

        private void fileSystemWatcher_Renamed( object source, RenamedEventArgs e )
        {
            if ( e.FullPath.ToLower() == this.Filename.ToLower() )
            {
                PromptToReload();
            }
        }        
        #endregion
    }

    public class AddLocalizedTextEventArgs : EventArgs
    {
        public AddLocalizedTextEventArgs( string text )
        {
            m_text = text;
        }

        #region Variables
        private string m_text;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return m_text;
            }
        }
        #endregion
    }

    public delegate void AddLocalizedTextEventHandler( object sender, AddLocalizedTextEventArgs e );
}
