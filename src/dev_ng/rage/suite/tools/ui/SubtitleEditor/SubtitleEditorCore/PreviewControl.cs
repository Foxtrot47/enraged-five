using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Media;
using System.Text;
using System.Windows.Forms;

using LocalizedTextControls;

using RSG.Base;
using RSG.Base.Forms;

namespace SubtitleEditorCore
{
    public partial class PreviewControl : UserControl
    {
        public PreviewControl()
        {
            InitializeComponent();

            UpdatePreviewPanelSizeAndLocation();
        }

        #region Enums
        private enum PlayState
        {
            Stop,
            Rewind,
            StepBackward,
            PlayBackwards,
            Pause,
            PlayForwards,
            StepForward,
            FastForward
        }
        #endregion

        #region Variables
        private Size c_defaultResolution = new Size( 1280, 720 );

        private Size m_resolution = new Size( 1280, 720 );
        private string m_filename = string.Empty;

        private bool m_wasRunningWhenValueChanged = false;

        private DateTime m_lastNoMediaUpdateTime = DateTime.MinValue;
        private PlayState m_noMediaPlayState = PlayState.Stop;
        private decimal m_noMediaPlayRate = 1.0M;
        #endregion

        #region Properties
        /// <summary>
        /// Gets and sets the volume.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( 100.0f ), Description( "Gets and sets the volume." )]
        public float Volume
        {
            get
            {
                return this.mediaPlayerControl.Volume;
            }
            set
            {
                this.mediaPlayerControl.Volume = value;
            }
        }

        /// <summary>
        /// Mutes and unmutes the currently loaded media file.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( false ), Description( "Mutes and unmutes the currently loaded media file." )]
        public bool IsMuted
        {
            get
            {
                return this.mediaPlayerControl.IsMuted;
            }
            set
            {
                this.mediaPlayerControl.IsMuted = value;
            }
        }

        /// <summary>
        /// Indicates whether or not to loop the media file.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( false ), Description( "Indicates whether or not to loop the media file." )]
        public bool Loop
        {
            get
            {
                return this.mediaPlayerControl.Loop;
            }
            set
            {
                this.mediaPlayerControl.Loop = value;
            }
        }

        /// <summary>
        /// Shows or hides the timecode displayed in the upper left corner of the video window.
        /// </summary>
        [Category( "Appearance" ), DefaultValue( true ), Description( "Shows or hides the timecode displayed in the upper left corner of the video window." )]
        public bool DisplayTimecode
        {
            get
            {
                return this.mediaPlayerControl.DisplayTimecode;
            }
            set
            {
                this.mediaPlayerControl.DisplayTimecode = value;
            }
        }

        /// <summary>
        /// Shows or hides the safe zone rectangle that is displayed over the video window.
        /// </summary>
        [Category( "Appearance" ), DefaultValue( true ), Description( "Shows or hides the safe zone rectangle that is displayed over the video window." )]
        public bool DisplaySafeZone
        {
            get
            {
                return this.mediaPlayerControl.DisplaySafeZone;
            }
            set
            {
                this.mediaPlayerControl.DisplaySafeZone = value;
            }
        }

        /// <summary>
        /// Gets or sets whether the video is displayed full screen (currently non-functional).
        /// </summary>
        [Category( "Appearance" ), DefaultValue( false ), Description( "Gets or sets whether the video is displayed full screen (currently non-functional)." )]
        public bool IsFullscreen
        {
            get
            {
                return this.mediaPlayerControl.IsFullScreen;
            }
            set
            {
                this.mediaPlayerControl.IsFullScreen = value;
            }
        }

        /// <summary>
        /// Returns the resolution of the currently loaded video file.
        /// </summary>
        [Category( "Appearance" ), Description( "Returns the resolution of the currently loaded video file." )]
        public Size Resolution
        {
            get
            {
                return this.mediaPlayerControl.Resolution;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public decimal CurrentTime
        {
            get
            {
                return this.playbackControl.Value;
            }
            set
            {
                this.playbackControl.Value = value;
                this.mediaPlayerControl.CurrentTime = value;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public decimal StartTime
        {
            get
            {
                return this.playbackControl.StartFrame;
            }
            set
            {
                this.playbackControl.StartFrame = value;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public decimal EndTime
        {
            get
            {
                return this.playbackControl.EndFrame;
            }
            set
            {
                this.playbackControl.EndFrame = value;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string Filename
        {
            get
            {
                return this.mediaPlayerControl.Filename;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public PreviewControlSettings PreviewControlSettings
        {
            get
            {
                PreviewControlSettings settings = new PreviewControlSettings();
                settings.DisplaySafeZone = this.DisplaySafeZone;
                settings.DisplayTimecode = this.DisplayTimecode;
                settings.IsMuted = this.IsMuted;
                settings.Loop = this.Loop;
                settings.Volume = this.Volume;
                return settings;
            }
            set
            {
                if ( value != null )
                {
                    this.DisplaySafeZone = value.DisplaySafeZone;
                    this.DisplayTimecode = value.DisplayTimecode;
                    this.IsMuted = value.IsMuted;
                    this.Loop = value.Loop;
                    this.Volume = value.Volume;
                }
                else
                {
                    this.PreviewControlSettings = new PreviewControlSettings();
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the current playback time is changed.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when the current playback time is changed." )]
        public event EventHandler CurrentTimeChanged;

        /// <summary>
        /// Occurs when a media file is opened.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when a media file is opened." )]
        public event EventHandler FileOpened;

        /// <summary>
        /// Occurs when a media file is closed.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when a media file is closed." )]
        public event EventHandler FileClosed;

        /// <summary>
        /// Occurs when the playback start time is changed.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when the playback start time is changed." )]
        public event EventHandler StartTimeChanged
        {
            add
            {
                this.playbackControl.StartFrameChanged += value;
            }
            remove
            {
                this.playbackControl.StartFrameChanged -= value;
            }
        }

        /// <summary>
        /// Occurs when the playback end time is changed.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when the playback end time is changed." )]
        public event EventHandler EndTimeChanged
        {
            add
            {
                this.playbackControl.EndFrameChanged += value;
            }
            remove
            {
                this.playbackControl.EndFrameChanged -= value;
            }
        }
        #endregion

        #region Event Dispatchers
        protected void OnCurrentTimeChanged()
        {
            if ( this.CurrentTimeChanged != null )
            {
                this.CurrentTimeChanged( this, EventArgs.Empty );
            }
        }

        protected void OnFileOpened()
        {
            if ( this.FileOpened != null )
            {
                this.FileOpened( this, EventArgs.Empty );
            }
        }

        protected void OnFileClosed()
        {
            if ( this.FileClosed != null )
            {
                this.FileClosed( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void noMediaUpdateTimer_Tick( object sender, EventArgs e )
        {
            DateTime currentTime = DateTime.Now;

            TimeSpan interval = currentTime - m_lastNoMediaUpdateTime;

            try
            {
                decimal newValue = this.playbackControl.Value + ((decimal)interval.TotalSeconds * m_noMediaPlayRate);
                if ( newValue < this.playbackControl.PlaybackStart )
                {
                    newValue = this.playbackControl.PlaybackStart;

                    if ( this.mediaPlayerControl.Loop )
                    {
                        this.playbackControl.Value = this.playbackControl.PlaybackEnd;
                    }
                    else
                    {
                        Pause();

                        this.playbackControl.Value = this.playbackControl.PlaybackStart;
                    }
                }
                else if ( newValue > this.playbackControl.PlaybackEnd )
                {
                    if ( this.mediaPlayerControl.Loop )
                    {
                        this.playbackControl.Value = this.playbackControl.PlaybackStart;
                    }
                    else
                    {
                        Pause();

                        this.playbackControl.Value = this.playbackControl.PlaybackStart;
                    }
                }
                else
                {
                    this.playbackControl.Value = newValue;
                }

                this.mediaPlayerControl.CurrentTime = this.playbackControl.Value;
                                
                OnCurrentTimeChanged();
                
                m_lastNoMediaUpdateTime = currentTime;
            }
            catch
            {

            }
        }

        private void panel_ClientSizeChanged( object sender, EventArgs e )
        {
            UpdatePreviewPanelSizeAndLocation();
        }

        #region ContextMenuStrip
        private void contextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            this.closeAudioVideoFileToolStripMenuItem.Enabled = this.mediaPlayerControl.IsLoaded;

            this.muteToolStripMenuItem.Checked = this.mediaPlayerControl.IsMuted;

            this.fullScreenToolStripMenuItem.Checked = this.mediaPlayerControl.IsFullScreen;
        }

        private void openAudioVideoFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                try
                {
                    LoadFile( this.openFileDialog.FileName );                    
                }
                catch ( Exception ex )
                {
                    rageMessageBox.ShowExclamation( this.FindForm(), ex.Message, "Sound File Load Error" );
                    return;
                }

                OnFileOpened();
            }
        }

        private void closeAudioVideoFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            CloseFile();

            OnFileClosed();
        }

        private void loopToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.mediaPlayerControl.Loop = this.loopToolStripMenuItem.Checked;
        }

        private void displayTimecodeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.DisplayTimecode = this.displayTimecodeToolStripMenuItem.Checked;
        }

        private void displaySafeZoneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.DisplaySafeZone = this.displaySafeZoneToolStripMenuItem.Checked;
        }

        private void playForwardsPauseToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.mediaPlayerControl.IsRunning )
            {
                Pause();
            }
            else
            {
                PlayForwards();
            }
        }

        private void playBackwardsPauseToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.mediaPlayerControl.IsRunning )
            {
                Pause();
            }
            else
            {
                PlayBackwards();
            }
        }

        private void rewindToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Rewind();
        }

        private void fastForwardToolStripMenuItem_Click( object sender, EventArgs e )
        {
            FastForward();
        }

        private void stepBackwardToolStripMenuItem_Click( object sender, EventArgs e )
        {
            StepBackward();
        }

        private void stepForwardToolStripMenuItem_Click( object sender, EventArgs e )
        {
            StepForward();
        }

        private void volumeUpToolStripMenuItem_Click( object sender, EventArgs e )
        {
            float vol = this.mediaPlayerControl.Volume + 10.0f;
            if ( vol > 100.0f )
            {
                vol = 100.0f;
            }

            this.mediaPlayerControl.Volume = vol;
        }

        private void volumeDownToolStripMenuItem_Click( object sender, EventArgs e )
        {
            float vol = this.mediaPlayerControl.Volume - 10.0f;
            if ( vol < 0.0f )
            {
                vol = 0.0f;
            }

            this.mediaPlayerControl.Volume = vol;
        }

        private void muteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.mediaPlayerControl.IsMuted = this.muteToolStripMenuItem.Checked;
        }

        private void fullScreenToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.mediaPlayerControl.IsFullScreen = true;
        }
        #endregion

        #region PlaybackControl
        private void playbackControl_PlaybackEndChanged( object sender, EventArgs e )
        {
            this.mediaPlayerControl.EndTime = this.playbackControl.PlaybackEnd;
        }

        private void playbackControl_PlaybackStartChanged( object sender, EventArgs e )
        {
            this.mediaPlayerControl.StartTime = this.playbackControl.PlaybackStart;
        }

        private void playbackControl_ValueChanging( object sender, EventArgs e )
        {
            if ( !m_wasRunningWhenValueChanged )
            {                
                if ( this.mediaPlayerControl.IsLoaded )
                {
                    m_wasRunningWhenValueChanged = this.mediaPlayerControl.IsRunning;
                    this.mediaPlayerControl.Pause();
                }
                else
                {
                    m_wasRunningWhenValueChanged = (m_noMediaPlayState != PlayState.Pause) && (m_noMediaPlayState != PlayState.Stop);
                    this.noMediaUpdateTimer.Enabled = false;
                }
            }

            OnCurrentTimeChanged();
        }

        private void playbackControl_ValueChanged( object sender, EventArgs e )
        {
            this.mediaPlayerControl.CurrentTime = this.playbackControl.Value;

            if ( m_wasRunningWhenValueChanged )
            {
                if ( this.mediaPlayerControl.IsLoaded )
                {
                    this.mediaPlayerControl.PlayForwards();
                }
                else
                {
                    m_lastNoMediaUpdateTime = DateTime.Now;
                    this.noMediaUpdateTimer.Enabled = true;
                }

                m_wasRunningWhenValueChanged = false;
            }

            OnCurrentTimeChanged();
        }

        private void playbackControl_FastForwardOrGoToEndButtonClick( object sender, EventArgs e )
        {
            FastForward();
        }

        private void playbackControl_GoToNextOrStepForwardButtonClick( object sender, EventArgs e )
        {
            StepForward();
        }

        private void playbackControl_GoToPreviousOrStepBackwardButtonClick( object sender, EventArgs e )
        {
            StepBackward();
        }

        private void playbackControl_PauseButtonClick( object sender, EventArgs e )
        {
            Pause();
        }

        private void playbackControl_PlayBackwardsButtonClick( object sender, EventArgs e )
        {
            PlayBackwards();
        }

        private void playbackControl_PlayForwardsButtonClick( object sender, EventArgs e )
        {
            PlayForwards();
        }

        private void playbackControl_RewindOrGoToStartButtonClick( object sender, EventArgs e )
        {
            Rewind();
        }
        #endregion

        #region MediaPlayerControl
        private void mediaPlayerControl_CurrentTimeChanged( object sender, EventArgs e )
        {            
            this.playbackControl.Value = this.mediaPlayerControl.CurrentTime;

            OnCurrentTimeChanged();
        }
        #endregion
        #endregion

        #region Public Functions
        public void LoadFile( string filename )
        {
            CloseFile();

            if ( !String.IsNullOrEmpty( filename ) )
            {
                this.mediaPlayerControl.LoadFile( filename );

                this.noMediaUpdateTimer.Enabled = false;

                this.Text = String.Format( "{0} - Preview", this.mediaPlayerControl.Filename );

                this.playbackControl.CanFastForwardOrGoToEnd = this.fastForwardToolStripMenuItem.Enabled = this.mediaPlayerControl.CanFastForward;
                this.playbackControl.CanRewindOrGoToStart = this.rewindToolStripMenuItem.Enabled = this.mediaPlayerControl.CanRewind;
                this.playbackControl.CanGoToNextOrStepForward = this.stepForwardToolStripMenuItem.Enabled = this.mediaPlayerControl.CanStepForward;
                this.playbackControl.CanGoToPreviousOrStepBackward = this.stepBackwardToolStripMenuItem.Enabled = this.mediaPlayerControl.CanStepBackward;
                this.playbackControl.CanPause = this.mediaPlayerControl.CanPause;
                this.playbackControl.CanPlayBackwards = this.playBackwardsPauseToolStripMenuItem.Enabled = this.mediaPlayerControl.CanPlayBackwards;
                this.playbackControl.CanPlayForwards = this.playForwardsPauseToolStripMenuItem.Enabled = this.mediaPlayerControl.CanPlayForwards;

                this.playbackControl.MaximumFrame = this.mediaPlayerControl.EndTime;
                this.playbackControl.MinimumFrame = this.mediaPlayerControl.StartTime;
                this.playbackControl.EndFrame = this.playbackControl.MaximumFrame;
                this.playbackControl.StartFrame = this.playbackControl.MinimumFrame;
                this.playbackControl.PlaybackEnd = this.playbackControl.EndFrame;
                this.playbackControl.PlaybackStart = this.playbackControl.StartFrame;

                this.playbackControl.Value = 0.0M;

                OnCurrentTimeChanged();

                UpdatePreviewPanelSizeAndLocation();
            }
        }

        public void CloseFile()
        {
            this.mediaPlayerControl.CloseFile();

            this.playbackControl.CanFastForwardOrGoToEnd = true;
            this.playbackControl.CanRewindOrGoToStart = true;
            this.playbackControl.CanGoToNextOrStepForward = true;
            this.playbackControl.CanGoToPreviousOrStepBackward = true;
            this.playbackControl.CanPause = true;
            this.playbackControl.CanPlayBackwards = true;
            this.playbackControl.CanPlayForwards = true;

            m_wasRunningWhenValueChanged = false;

            m_noMediaPlayState = PlayState.Stop;

            this.Text = "Preview";
        }

        public void Rewind()
        {
            if ( this.mediaPlayerControl.IsLoaded )
            {
                this.mediaPlayerControl.Rewind();
            }
            else if ( this.playbackControl.Value > this.playbackControl.PlaybackStart )
            {
                if ( m_noMediaPlayRate == -2.0M )
                {
                    m_noMediaPlayRate = -3.0M;
                }
                else
                {
                    m_noMediaPlayRate = -2.0M;
                }

                m_noMediaPlayState = PlayState.Rewind;
                m_lastNoMediaUpdateTime = DateTime.Now;
                this.noMediaUpdateTimer.Enabled = true;
            }
        }

        public void StepBackward()
        {
            if ( this.mediaPlayerControl.IsLoaded )
            {
                this.mediaPlayerControl.StepBackward();
            }
            else if ( this.playbackControl.Value > this.playbackControl.PlaybackStart )
            {
                m_noMediaPlayState = PlayState.Pause;
                this.noMediaUpdateTimer.Enabled = false;

                this.playbackControl.Value -= (1.0M / 30.0M);

                OnCurrentTimeChanged();
            }
        }

        public void PlayBackwards()
        {
            if ( this.mediaPlayerControl.IsLoaded )
            {
                this.mediaPlayerControl.PlayBackwards();
            }
            else if ( this.playbackControl.Value > this.playbackControl.PlaybackStart )
            {
                m_noMediaPlayRate = -1.0M;
                m_noMediaPlayState = PlayState.PlayBackwards;
                m_lastNoMediaUpdateTime = DateTime.Now;
                this.noMediaUpdateTimer.Enabled = true;
            }
        }

        public void Pause()
        {
            if ( this.mediaPlayerControl.IsLoaded )
            {
                this.mediaPlayerControl.Pause();
            }
            else 
            {
                m_noMediaPlayRate = 0.0M;
                m_noMediaPlayState = PlayState.Pause;
                this.noMediaUpdateTimer.Enabled = false;
            }
        }

        public void PlayForwards()
        {
            if ( this.mediaPlayerControl.IsLoaded )
            {
                this.mediaPlayerControl.PlayForwards();
            }
            else if ( this.playbackControl.Value < this.playbackControl.PlaybackEnd )
            {
                m_noMediaPlayRate = 1.0M;
                m_noMediaPlayState = PlayState.PlayForwards;
                m_lastNoMediaUpdateTime = DateTime.Now;
                this.noMediaUpdateTimer.Enabled = true;
            }
        }

        public void StepForward()
        {
            if ( this.mediaPlayerControl.IsLoaded )
            {
                this.mediaPlayerControl.StepForward();
            }
            else if ( this.playbackControl.Value < this.playbackControl.PlaybackEnd )
            {
                m_noMediaPlayState = PlayState.Pause;
                this.noMediaUpdateTimer.Enabled = false;

                this.playbackControl.Value += (1.0M / 30.0M);

                OnCurrentTimeChanged();
            }
        }

        public void FastForward()
        {
            if ( this.mediaPlayerControl.IsLoaded )
            {
                this.mediaPlayerControl.FastForward();
            }
            else if ( this.playbackControl.Value < this.playbackControl.PlaybackEnd )
            {
                if ( m_noMediaPlayRate == 2.0M )
                {
                    m_noMediaPlayRate = 3.0M;
                }
                else
                {
                    m_noMediaPlayRate = 2.0M;
                }

                m_noMediaPlayState = PlayState.FastForward;
                m_lastNoMediaUpdateTime = DateTime.Now;
                this.noMediaUpdateTimer.Enabled = true;
            }
        }

        public void AddBitmap( Bitmap bmp )
        {
            this.mediaPlayerControl.AddBitmap( bmp );
        }

        public void RemoveBitmap( Bitmap bmp )
        {
            this.mediaPlayerControl.RemoveBitmap( bmp );
        }

        public void RefreshBitmap( Bitmap bmp )
        {
            this.mediaPlayerControl.RefreshBitmap( bmp );
        }

        public void ShowBitmap( Bitmap bmp )
        {
            this.mediaPlayerControl.ShowBitmap( bmp );
        }

        public void HideBitmap( Bitmap bmp )
        {
            this.mediaPlayerControl.HideBitmap( bmp );
        }

        public void ClearBitmaps()
        {
            this.mediaPlayerControl.ClearBitmaps();
        }
        #endregion

        #region Private Functions
        private void UpdatePreviewPanelSizeAndLocation()
        {
            // set the preview panel's size and location
            try
            {
                Size resolution = this.Resolution;
                if ( resolution == Size.Empty )
                {
                    resolution = c_defaultResolution;
                }

                Size parentSize = this.panel.ClientSize;

                // Based on resolution enum, determine which aspect ratio to use
                float aspectRatio = (float)resolution.Width / (float)resolution.Height;

                // Maximize width and height while maintaining its aspect ratio
                int width = Convert.ToInt32( parentSize.Height * aspectRatio );
                int height = Convert.ToInt32( parentSize.Width / aspectRatio );
                if ( (parentSize.Width > parentSize.Height) && (width <= parentSize.Width) )
                {
                    height = parentSize.Height;
                }
                else
                {
                    width = parentSize.Width;
                }

                this.mediaPlayerControl.Size = new Size( width, height );

                // Position the window in the center of the dock window
                int x = (parentSize.Width / 2) - (this.mediaPlayerControl.Width / 2);
                int y = (parentSize.Height / 2) - (this.mediaPlayerControl.Height / 2);
                this.mediaPlayerControl.Location = new Point( x, y );
            }
            catch
            {

            }
        }
        #endregion
    }
}
