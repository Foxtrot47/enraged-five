namespace SubtitleEditorCore
{
    partial class MediaPlayerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer( this.components );
            this.renderingPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler( this.timer_Tick );
            // 
            // renderingPanel
            // 
            this.renderingPanel.BackColor = System.Drawing.Color.Black;
            this.renderingPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderingPanel.Location = new System.Drawing.Point( 0, 0 );
            this.renderingPanel.Name = "renderingPanel";
            this.renderingPanel.Size = new System.Drawing.Size( 150, 150 );
            this.renderingPanel.TabIndex = 0;
            this.renderingPanel.Paint += new System.Windows.Forms.PaintEventHandler( this.renderingPanel_Paint );
            this.renderingPanel.Resize += new System.EventHandler( this.renderingPanel_Resize );
            this.renderingPanel.Move += new System.EventHandler( this.renderingPanel_Move );
            this.renderingPanel.ClientSizeChanged += new System.EventHandler( this.renderingPanel_ClientSizeChanged );
            // 
            // MediaPlayerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.renderingPanel );
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "MediaPlayerControl";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler( this.MediaPlayerControl_KeyDown );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Panel renderingPanel;
    }
}
