using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Serialization;

namespace SubtitleEditorCore
{
    /// <summary>
    /// A structure that holds all of the data associated with a transition that can be added to a SubtitleEventData item.
    /// </summary>
    [Serializable]
    public class SubtitleTransitionOption : IRageClonableObject
    {
        public SubtitleTransitionOption()
        {

        }

        #region Variables
        private string m_name = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The name of the transition.
        /// </summary>
        [DefaultValue( "" ), Description( "The name of the transition." ), Category( "Required" )]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleTransitionOption )
            {
                return Equals( obj as SubtitleTransitionOption );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            SubtitleTransitionOption data = new SubtitleTransitionOption();
            data.CopyFrom( this );
            return data;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleTransitionOption )
            {
                CopyFrom( other as SubtitleTransitionOption );
            }
        }

        public void Reset()
        {
            this.Name = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleTransitionOption other )
        {
            if ( other == null )
            {
                return;
            }

            this.Name = other.Name;
        }

        public bool Equals( SubtitleTransitionOption other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.Name == other.Name);
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of SubtitleTransitionOption items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class SubtitleTransitionOptionCollection : rageSerializableList<SubtitleTransitionOption>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            SubtitleTransitionOptionCollection collection = new SubtitleTransitionOptionCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion
    }
}
