using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Serialization;

namespace SubtitleEditorCore
{
    /// <summary>
    /// Holds all of the per-user settings for the PreviewControl
    /// </summary>
    public class PreviewControlSettings : rageSerializableObject
    {
        public PreviewControlSettings()
        {
            Reset();
        }

        #region Variables
        private float m_volume = 100.0f;
        private bool m_isMuted = false;
        private bool m_loop = false;
        private bool m_displayTimecode = true;
        private bool m_displaySafeZone = true;
        #endregion

        #region Properties
        /// <summary>
        /// The volume.
        /// </summary>
        [DefaultValue( 100 ), Description( "The volume." )]
        public float Volume
        {
            get
            {
                return m_volume;
            }
            set
            {
                m_volume = value;
            }
        }

        /// <summary>
        /// If the media file's audio is muted.
        /// </summary>
        [DefaultValue( false ), Description( "If the media file's audio is muted." )]
        public bool IsMuted
        {
            get
            {
                return m_isMuted;
            }
            set
            {
                m_isMuted = value;
            }
        }

        /// <summary>
        /// Whether or not to loop the media file.
        /// </summary>
        [DefaultValue( false ), Description( "Whether or not to loop the media file." )]
        public bool Loop
        {
            get
            {
                return m_loop;
            }
            set
            {
                m_loop = value;
            }
        }

        /// <summary>
        /// Whether or not to display the timecode in the upper left corner of the video window.
        /// </summary>
        [DefaultValue( true ), Description( "Whether or not to display the timecode in the upper left corner of the video window." )]
        public bool DisplayTimecode
        {
            get
            {
                return m_displayTimecode;
            }
            set
            {
                m_displayTimecode = value;
            }
        }

        /// <summary>
        /// Whether or not to display the safe zone rectangle over the video window.
        /// </summary>
        [DefaultValue( true ), Description( "Whether or not to display the safe zone rectangle over the video window." )]
        public bool DisplaySafeZone
        {
            get
            {
                return m_displaySafeZone;
            }
            set
            {
                m_displaySafeZone = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is PreviewControlSettings )
            {
                return Equals( obj as PreviewControlSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            PreviewControlSettings settings = new PreviewControlSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is PreviewControlSettings )
            {
                CopyFrom( other as PreviewControlSettings );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.DisplaySafeZone = true;
            this.DisplayTimecode = true;
            this.IsMuted = false;
            this.Loop = false;
            this.Volume = 100.0f;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( PreviewControlSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.DisplaySafeZone = other.DisplaySafeZone;
            this.DisplayTimecode = other.DisplayTimecode;
            this.IsMuted = other.IsMuted;
            this.Loop = other.Loop;
            this.Volume = other.Volume;
        }

        public bool Equals( PreviewControlSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return (this.DisplaySafeZone == other.DisplaySafeZone)
                && (this.DisplayTimecode == other.DisplayTimecode)
                && (this.IsMuted == other.IsMuted)
                && (this.Loop == other.Loop)
                && (this.Volume == other.Volume);
        }
        #endregion
    }
}
