using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using LocalizedTextControls;
using RSG.Base.Command;
using RSG.Base.Forms;

namespace SubtitleEditorCore
{
    public partial class SubtitleOptionsDialog : Form
    {
        public SubtitleOptionsDialog()
        {
            InitializeComponent();

            this.subtitleTransitionsListBoxSettingsControl.EditType = typeof( SubtitleTransitionOption );
        }

        #region Variables
        private bool m_dispatchEvents = true;
        private SubtitleOptions m_originalSubtitleOptions = null;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;

                this.subtitleTransitionsListBoxSettingsControl.HelpProvider = this.helpProvider;
            }
        }

        public SubtitleOptions SubtitleOptions
        {
            get
            {
                SubtitleOptions options = new SubtitleOptions();

                // get the transitions
                foreach ( SubtitleTransitionOption option in this.subtitleTransitionsListBoxSettingsControl.Items )
                {
                    options.TransitionOptionCollection.Add( option );
                }

                options.DefaultTransitionOptionIndex = this.subtitleTransitionsListBoxSettingsControl.DefaultItemIndex;

                // preserve the file locations from the original options
                options.CurrentFilename = m_originalSubtitleOptions.CurrentFilename;
                options.OriginalFileLocation = m_originalSubtitleOptions.OriginalFileLocation;

                return options;
            }
            set
            {
                if ( value != null )
                {
                    m_dispatchEvents = false;

                    // load the transitions
                    List<ICloneable> options = new List<ICloneable>();
                    foreach ( SubtitleTransitionOption option in value.TransitionOptionCollection )
                    {
                        options.Add( option );
                    }
                    this.subtitleTransitionsListBoxSettingsControl.Items = options;

                    this.subtitleTransitionsListBoxSettingsControl.DefaultItemIndex = value.DefaultTransitionOptionIndex;

                    m_dispatchEvents = true;
                }

                m_originalSubtitleOptions = value;
            }
        }
        #endregion

        #region Events
        public event SourceControlEventHandler CheckOutFile;
        public event EventHandler Changed;
        #endregion

        #region Event Dispatchers
        protected void OnChanged()
        {
            if ( m_dispatchEvents && (this.Changed != null) )
            {
                this.Changed( this, EventArgs.Empty );
            }
        }

        protected void OnCheckOutFile( SourceControlEventArgs e )
        {
            if ( m_dispatchEvents && (this.CheckOutFile != null) )
            {
                this.CheckOutFile( this, e );
            }
        }
        #endregion

        #region Event Handlers
        private void generalImportButton_Click( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowWarning( this,
                "Importing a Subtitle Transition Options file will overwrite your current options,\n" +
                "but you can still revert to your original options by clicking the Cancel Button.\n\nDo you wish to proceed?",
                "Import Subtitle Transition Options File", MessageBoxButtons.YesNo );
            if ( result == DialogResult.No )
            {
                return;
            }

            LoadSubtitleOptionsFile();
        }

        private void subtitleTransitionsListBoxSettingsControl_Changed( object sender, EventArgs e )
        {
            OnChanged();
        }

        private void okButton_Click( object sender, EventArgs e )
        {
            SubtitleOptions options = this.SubtitleOptions;
            if ( options.Equals( m_originalSubtitleOptions ) )
            {
                this.DialogResult = DialogResult.OK;
                return;
            }

            string msg = String.IsNullOrEmpty( options.CurrentFilename ) ? "You've never saved a Subtitle Transition Options file.  Would you like to do that now?" : "The Localized Text Options has changed.  Would you like to save the file?";

            // optional save
            DialogResult result = rageMessageBox.ShowQuestion( this, msg,
                "Subtitle Transition Options", MessageBoxButtons.YesNoCancel );
            if ( result == DialogResult.No )
            {
                this.DialogResult = DialogResult.OK;
                return;
            }
            else if ( result == DialogResult.Cancel )
            {
                return;
            }

            if ( SaveSubtitleOptionsFile() )
            {
                this.DialogResult = DialogResult.OK;
            }
        }
        #endregion

        #region Private Functions
        private bool SaveSubtitleOptionsFile()
        {
            this.saveFileDialog.Filter = "Subtitle Options Files (*.subproj.options)|*.subproj.options";
            this.saveFileDialog.Title = "Save Subtitle Transition Options File As";

            if ( !String.IsNullOrEmpty( m_originalSubtitleOptions.CurrentFilename ) )
            {
                this.saveFileDialog.InitialDirectory = Path.GetDirectoryName( m_originalSubtitleOptions.CurrentFilename );
                this.saveFileDialog.FileName = Path.GetFileName( m_originalSubtitleOptions.CurrentFilename );
            }
            else
            {
                this.saveFileDialog.InitialDirectory = Path.GetDirectoryName( Application.ExecutablePath );
                this.saveFileDialog.FileName = Path.GetFileName( "SubtitleOptions.subproj.options" );
            }

            DialogResult result = this.saveFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                SourceControlEventArgs e = new SourceControlEventArgs( this.saveFileDialog.FileName );
                OnCheckOutFile( e );
                if ( e.Result == DialogResult.Abort )
                {
                    return false;
                }

                SubtitleOptions options = this.SubtitleOptions;
                while ( true )
                {
                    rageStatus status;
                    options.SaveFile( this.saveFileDialog.FileName, out status );
                    if ( status.Success() )
                    {
                        m_originalSubtitleOptions = options; // this is now our original options
                        break;
                    }
                    else
                    {
                        result = rageMessageBox.ShowExclamation( this, status.ErrorString, "Export Subtitle Transition Options Error",
                            MessageBoxButtons.RetryCancel );
                        if ( result == DialogResult.Cancel )
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private void LoadSubtitleOptionsFile()
        {
            this.openFileDialog.Filter = "Subtitle Options Files (*.subproj.options)|*.subproj.options";
            this.openFileDialog.Title = "Open Subtitle Options File";

            if ( !String.IsNullOrEmpty( m_originalSubtitleOptions.CurrentFilename ) )
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( m_originalSubtitleOptions.CurrentFilename );
                this.openFileDialog.FileName = Path.GetFileName( m_originalSubtitleOptions.CurrentFilename );
            }
            else
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( Application.ExecutablePath );
                this.openFileDialog.FileName = "SubtitleOptions.subproj.options";
            }

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result != DialogResult.OK )
            {
                return;
            }

            SubtitleOptions options = new SubtitleOptions();
            while ( true )
            {
                rageStatus status;
                options.LoadFile( this.openFileDialog.FileName, out status );
                if ( status.Success() )
                {
                    // this is now our current and our original options
                    this.SubtitleOptions = options;
                    OnChanged();
                    break;
                }
                else
                {
                    result = rageMessageBox.ShowExclamation( this, status.ErrorString, "Import Subtitle Transition Options Error",
                        MessageBoxButtons.RetryCancel );
                    if ( result == DialogResult.Cancel )
                    {
                        break;
                    }
                }
            }
        }
        #endregion
    }
}