namespace SubtitleEditorCore
{
    partial class SubtitleOptionsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.generalTabPage = new System.Windows.Forms.TabPage();
            this.generalImportButton = new System.Windows.Forms.Button();
            this.transitionsTabPage = new System.Windows.Forms.TabPage();
            this.subtitleTransitionsListBoxSettingsControl = new LocalizedTextControls.ListBoxSettingsControl();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.tabControl.SuspendLayout();
            this.generalTabPage.SuspendLayout();
            this.transitionsTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point( 362, 362 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler( this.okButton_Click );
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 443, 362 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add( this.generalTabPage );
            this.tabControl.Controls.Add( this.transitionsTabPage );
            this.tabControl.Location = new System.Drawing.Point( 12, 12 );
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size( 506, 344 );
            this.tabControl.TabIndex = 3;
            // 
            // generalTabPage
            // 
            this.generalTabPage.Controls.Add( this.generalImportButton );
            this.generalTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.generalTabPage.Name = "generalTabPage";
            this.generalTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.generalTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.generalTabPage.TabIndex = 0;
            this.generalTabPage.Text = "General";
            this.generalTabPage.UseVisualStyleBackColor = true;
            // 
            // generalImportButton
            // 
            this.generalImportButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.generalImportButton, "Import a .subproj.options file." );
            this.generalImportButton.Location = new System.Drawing.Point( 6, 6 );
            this.generalImportButton.Name = "generalImportButton";
            this.helpProvider.SetShowHelp( this.generalImportButton, true );
            this.generalImportButton.Size = new System.Drawing.Size( 486, 23 );
            this.generalImportButton.TabIndex = 0;
            this.generalImportButton.Text = "Import";
            this.generalImportButton.UseVisualStyleBackColor = true;
            this.generalImportButton.Click += new System.EventHandler( this.generalImportButton_Click );
            // 
            // transitionsTabPage
            // 
            this.transitionsTabPage.Controls.Add( this.subtitleTransitionsListBoxSettingsControl );
            this.transitionsTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.transitionsTabPage.Name = "transitionsTabPage";
            this.transitionsTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.transitionsTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.transitionsTabPage.TabIndex = 1;
            this.transitionsTabPage.Text = "Transitions";
            this.transitionsTabPage.UseVisualStyleBackColor = true;
            // 
            // subtitleTransitionsListBoxSettingsControl
            // 
            this.subtitleTransitionsListBoxSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subtitleTransitionsListBoxSettingsControl.Location = new System.Drawing.Point( 3, 3 );
            this.subtitleTransitionsListBoxSettingsControl.Name = "subtitleTransitionsListBoxSettingsControl";
            this.subtitleTransitionsListBoxSettingsControl.Size = new System.Drawing.Size( 492, 312 );
            this.subtitleTransitionsListBoxSettingsControl.TabIndex = 0;
            this.subtitleTransitionsListBoxSettingsControl.Changed += new System.EventHandler( this.subtitleTransitionsListBoxSettingsControl_Changed );
            // 
            // openFileDialog
            // 
            this.openFileDialog.RestoreDirectory = true;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.RestoreDirectory = true;
            // 
            // SubtitleOptionsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 530, 397 );
            this.Controls.Add( this.tabControl );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider.SetHelpKeyword( this, "SubtitleOptionsDialog.html" );
            this.helpProvider.SetHelpNavigator( this, System.Windows.Forms.HelpNavigator.Topic );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size( 536, 422 );
            this.Name = "SubtitleOptionsDialog";
            this.helpProvider.SetShowHelp( this, true );
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Subtitle Options";
            this.tabControl.ResumeLayout( false );
            this.generalTabPage.ResumeLayout( false );
            this.transitionsTabPage.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage generalTabPage;
        private System.Windows.Forms.TabPage transitionsTabPage;
        private LocalizedTextControls.ListBoxSettingsControl subtitleTransitionsListBoxSettingsControl;
        private System.Windows.Forms.Button generalImportButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.HelpProvider helpProvider;
    }
}