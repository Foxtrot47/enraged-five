namespace SubtitleEditorCore
{
    partial class PreviewControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.playbackControl = new PlaybackControls.PlaybackControl();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.openAudioVideoFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAudioVideoFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayTimecodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displaySafeZoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.playForwardsPauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playBackwardsPauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rewindToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fastForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepBackwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.volumeUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.volumeDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.muteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.fullScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel = new System.Windows.Forms.Panel();
            this.mediaPlayerControl = new SubtitleEditorCore.MediaPlayerControl();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.noMediaUpdateTimer = new System.Windows.Forms.Timer( this.components );
            this.contextMenuStrip.SuspendLayout();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // playbackControl
            // 
            this.playbackControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackControl.ContextMenuStrip = this.contextMenuStrip;
            this.playbackControl.EndFrame = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackControl.EndFrameEnabled = false;
            this.playbackControl.Location = new System.Drawing.Point( 3, 327 );
            this.playbackControl.MaximumFrame = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackControl.MinimumFrame = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.Name = "playbackControl";
            this.playbackControl.PlaybackEnd = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackControl.PlaybackStart = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.Size = new System.Drawing.Size( 920, 70 );
            this.playbackControl.StartFrame = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.StartFrameEnabled = false;
            this.playbackControl.TabIndex = 0;
            this.playbackControl.Value = new decimal( new int[] {
            0,
            0,
            0,
            65536} );
            this.playbackControl.RewindOrGoToStartButtonClick += new System.EventHandler( this.playbackControl_RewindOrGoToStartButtonClick );
            this.playbackControl.PlaybackStartChanged += new System.EventHandler( this.playbackControl_PlaybackStartChanged );
            this.playbackControl.PlaybackEndChanged += new System.EventHandler( this.playbackControl_PlaybackEndChanged );
            this.playbackControl.GoToPreviousOrStepBackwardButtonClick += new System.EventHandler( this.playbackControl_GoToPreviousOrStepBackwardButtonClick );
            this.playbackControl.ValueChanged += new System.EventHandler( this.playbackControl_ValueChanged );
            this.playbackControl.PlayForwardsButtonClick += new System.EventHandler( this.playbackControl_PlayForwardsButtonClick );
            this.playbackControl.GoToNextOrStepForwardButtonClick += new System.EventHandler( this.playbackControl_GoToNextOrStepForwardButtonClick );
            this.playbackControl.PlayBackwardsButtonClick += new System.EventHandler( this.playbackControl_PlayBackwardsButtonClick );
            this.playbackControl.FastForwardOrGoToEndButtonClick += new System.EventHandler( this.playbackControl_FastForwardOrGoToEndButtonClick );
            this.playbackControl.ValueChanging += new System.EventHandler( this.playbackControl_ValueChanging );
            this.playbackControl.PauseButtonClick += new System.EventHandler( this.playbackControl_PauseButtonClick );
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.openAudioVideoFileToolStripMenuItem,
            this.closeAudioVideoFileToolStripMenuItem,
            this.toolStripSeparator1,
            this.loopToolStripMenuItem,
            this.displayTimecodeToolStripMenuItem,
            this.displaySafeZoneToolStripMenuItem,
            this.toolStripSeparator2,
            this.playForwardsPauseToolStripMenuItem,
            this.playBackwardsPauseToolStripMenuItem,
            this.rewindToolStripMenuItem,
            this.fastForwardToolStripMenuItem,
            this.stepBackwardToolStripMenuItem,
            this.stepForwardToolStripMenuItem,
            this.toolStripSeparator3,
            this.volumeUpToolStripMenuItem,
            this.volumeDownToolStripMenuItem,
            this.muteToolStripMenuItem,
            this.toolStripSeparator4,
            this.fullScreenToolStripMenuItem} );
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size( 261, 358 );
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler( this.contextMenuStrip_Opening );
            // 
            // openAudioVideoFileToolStripMenuItem
            // 
            this.openAudioVideoFileToolStripMenuItem.Image = global::SubtitleEditorCore.Properties.Resources.openHS;
            this.openAudioVideoFileToolStripMenuItem.Name = "openAudioVideoFileToolStripMenuItem";
            this.openAudioVideoFileToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.openAudioVideoFileToolStripMenuItem.Text = "&Open Audio/Video File";
            this.openAudioVideoFileToolStripMenuItem.Click += new System.EventHandler( this.openAudioVideoFileToolStripMenuItem_Click );
            // 
            // closeAudioVideoFileToolStripMenuItem
            // 
            this.closeAudioVideoFileToolStripMenuItem.Image = global::SubtitleEditorCore.Properties.Resources.DeleteHS;
            this.closeAudioVideoFileToolStripMenuItem.Name = "closeAudioVideoFileToolStripMenuItem";
            this.closeAudioVideoFileToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.closeAudioVideoFileToolStripMenuItem.Text = "Close Audio/Video File";
            this.closeAudioVideoFileToolStripMenuItem.Click += new System.EventHandler( this.closeAudioVideoFileToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 257, 6 );
            // 
            // loopToolStripMenuItem
            // 
            this.loopToolStripMenuItem.CheckOnClick = true;
            this.loopToolStripMenuItem.Name = "loopToolStripMenuItem";
            this.loopToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.loopToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.loopToolStripMenuItem.Text = "&Loop";
            this.loopToolStripMenuItem.Click += new System.EventHandler( this.loopToolStripMenuItem_Click );
            // 
            // displayTimecodeToolStripMenuItem
            // 
            this.displayTimecodeToolStripMenuItem.Checked = true;
            this.displayTimecodeToolStripMenuItem.CheckOnClick = true;
            this.displayTimecodeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.displayTimecodeToolStripMenuItem.Name = "displayTimecodeToolStripMenuItem";
            this.displayTimecodeToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.displayTimecodeToolStripMenuItem.Text = "&Display Timecode";
            this.displayTimecodeToolStripMenuItem.Click += new System.EventHandler( this.displayTimecodeToolStripMenuItem_Click );
            // 
            // displaySafeZoneToolStripMenuItem
            // 
            this.displaySafeZoneToolStripMenuItem.Checked = true;
            this.displaySafeZoneToolStripMenuItem.CheckOnClick = true;
            this.displaySafeZoneToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.displaySafeZoneToolStripMenuItem.Name = "displaySafeZoneToolStripMenuItem";
            this.displaySafeZoneToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.displaySafeZoneToolStripMenuItem.Text = "Display Safe &Zone";
            this.displaySafeZoneToolStripMenuItem.Click += new System.EventHandler( this.displaySafeZoneToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 257, 6 );
            // 
            // playForwardsPauseToolStripMenuItem
            // 
            this.playForwardsPauseToolStripMenuItem.Name = "playForwardsPauseToolStripMenuItem";
            this.playForwardsPauseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.playForwardsPauseToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.playForwardsPauseToolStripMenuItem.Text = "&Play Forwards/Pause";
            this.playForwardsPauseToolStripMenuItem.Click += new System.EventHandler( this.playForwardsPauseToolStripMenuItem_Click );
            // 
            // playBackwardsPauseToolStripMenuItem
            // 
            this.playBackwardsPauseToolStripMenuItem.Name = "playBackwardsPauseToolStripMenuItem";
            this.playBackwardsPauseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.P)));
            this.playBackwardsPauseToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.playBackwardsPauseToolStripMenuItem.Text = "Play &Backwards/Pause";
            this.playBackwardsPauseToolStripMenuItem.Click += new System.EventHandler( this.playBackwardsPauseToolStripMenuItem_Click );
            // 
            // rewindToolStripMenuItem
            // 
            this.rewindToolStripMenuItem.Name = "rewindToolStripMenuItem";
            this.rewindToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.B)));
            this.rewindToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.rewindToolStripMenuItem.Text = "&Rewind";
            this.rewindToolStripMenuItem.Click += new System.EventHandler( this.rewindToolStripMenuItem_Click );
            // 
            // fastForwardToolStripMenuItem
            // 
            this.fastForwardToolStripMenuItem.Name = "fastForwardToolStripMenuItem";
            this.fastForwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.F)));
            this.fastForwardToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.fastForwardToolStripMenuItem.Text = "F&ast Forward";
            this.fastForwardToolStripMenuItem.Click += new System.EventHandler( this.fastForwardToolStripMenuItem_Click );
            // 
            // stepBackwardToolStripMenuItem
            // 
            this.stepBackwardToolStripMenuItem.Name = "stepBackwardToolStripMenuItem";
            this.stepBackwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.stepBackwardToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.stepBackwardToolStripMenuItem.Text = "S&tep Backward";
            this.stepBackwardToolStripMenuItem.Click += new System.EventHandler( this.stepBackwardToolStripMenuItem_Click );
            // 
            // stepForwardToolStripMenuItem
            // 
            this.stepForwardToolStripMenuItem.Name = "stepForwardToolStripMenuItem";
            this.stepForwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.stepForwardToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.stepForwardToolStripMenuItem.Text = "&Step Forward";
            this.stepForwardToolStripMenuItem.Click += new System.EventHandler( this.stepForwardToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 257, 6 );
            // 
            // volumeUpToolStripMenuItem
            // 
            this.volumeUpToolStripMenuItem.Name = "volumeUpToolStripMenuItem";
            this.volumeUpToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.volumeUpToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.volumeUpToolStripMenuItem.Text = "Volume &Up";
            this.volumeUpToolStripMenuItem.Click += new System.EventHandler( this.volumeUpToolStripMenuItem_Click );
            // 
            // volumeDownToolStripMenuItem
            // 
            this.volumeDownToolStripMenuItem.Name = "volumeDownToolStripMenuItem";
            this.volumeDownToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.volumeDownToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.volumeDownToolStripMenuItem.Text = "Volume &Down";
            this.volumeDownToolStripMenuItem.Click += new System.EventHandler( this.volumeDownToolStripMenuItem_Click );
            // 
            // muteToolStripMenuItem
            // 
            this.muteToolStripMenuItem.CheckOnClick = true;
            this.muteToolStripMenuItem.Name = "muteToolStripMenuItem";
            this.muteToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.muteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.muteToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.muteToolStripMenuItem.Text = "&Mute";
            this.muteToolStripMenuItem.Click += new System.EventHandler( this.muteToolStripMenuItem_Click );
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 257, 6 );
            // 
            // fullScreenToolStripMenuItem
            // 
            this.fullScreenToolStripMenuItem.Enabled = false;
            this.fullScreenToolStripMenuItem.Name = "fullScreenToolStripMenuItem";
            this.fullScreenToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.fullScreenToolStripMenuItem.Size = new System.Drawing.Size( 260, 22 );
            this.fullScreenToolStripMenuItem.Text = "&Full Screen";
            this.fullScreenToolStripMenuItem.Click += new System.EventHandler( this.fullScreenToolStripMenuItem_Click );
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.ContextMenuStrip = this.contextMenuStrip;
            this.panel.Controls.Add( this.mediaPlayerControl );
            this.panel.Location = new System.Drawing.Point( 0, 0 );
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size( 926, 329 );
            this.panel.TabIndex = 1;
            this.panel.ClientSizeChanged += new System.EventHandler( this.panel_ClientSizeChanged );
            // 
            // mediaPlayerControl
            // 
            this.mediaPlayerControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mediaPlayerControl.BackColor = System.Drawing.Color.Black;
            this.mediaPlayerControl.ForeColor = System.Drawing.Color.White;
            this.mediaPlayerControl.Location = new System.Drawing.Point( 2, 2 );
            this.mediaPlayerControl.Name = "mediaPlayerControl";
            this.mediaPlayerControl.Size = new System.Drawing.Size( 922, 329 );
            this.mediaPlayerControl.TabIndex = 0;
            this.mediaPlayerControl.CurrentTimeChanged += new System.EventHandler( this.mediaPlayerControl_CurrentTimeChanged );
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Media files (all types)|*.mpeg;*.mpg;*.wma;*.wmv;*.avi;*.wav;*.au;*.aif;*.aiff;*." +
                "mp3";
            this.openFileDialog.Title = "Open Sound File";
            // 
            // noMediaUpdateTimer
            // 
            this.noMediaUpdateTimer.Interval = 10;
            this.noMediaUpdateTimer.Tick += new System.EventHandler( this.noMediaUpdateTimer_Tick );
            // 
            // PreviewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.panel );
            this.Controls.Add( this.playbackControl );
            this.Name = "PreviewControl";
            this.Size = new System.Drawing.Size( 926, 400 );
            this.contextMenuStrip.ResumeLayout( false );
            this.panel.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private PlaybackControls.PlaybackControl playbackControl;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openAudioVideoFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAudioVideoFileToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private MediaPlayerControl mediaPlayerControl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem muteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loopToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Timer noMediaUpdateTimer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem fullScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playForwardsPauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playBackwardsPauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rewindToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fastForwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stepBackwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stepForwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem volumeUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem volumeDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem displayTimecodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displaySafeZoneToolStripMenuItem;
    }
}
