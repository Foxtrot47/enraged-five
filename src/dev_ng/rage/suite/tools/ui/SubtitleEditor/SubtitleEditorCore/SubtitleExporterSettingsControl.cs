using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;

namespace SubtitleEditorCore
{
    public partial class SubtitleExporterSettingsControl : UserControl
    {
        public SubtitleExporterSettingsControl()
        {
            InitializeComponent();
        }

        #region Variables
        private bool m_dispatchEvents = true;
        #endregion

        #region Properties
        public HelpProvider HelpProvider
        {
            set
            {
                if ( value != null )
                {
                    string cutSubHelp = "The directory where the .cutsub file will be saved.";
                    value.SetShowHelp( this.cutSubOutputPathLabel, true );
                    value.SetShowHelp( this.cutSubOutputPathTextBox, true );
                    value.SetHelpString( this.cutSubOutputPathLabel, cutSubHelp );
                    value.SetHelpString( this.cutSubOutputPathTextBox, cutSubHelp );

                    value.SetShowHelp( this.browseCutSubOutputPathButton, true );
                    value.SetHelpString( this.browseCutSubOutputPathButton, "Browse for the directory where the .cutsub file will be saved." );

                    this.localizedTextExporterSettingsControl.HelpProvider = value;
                }
            }
        }
        /// <summary>
        /// The settings to edit and were created/edited.
        /// </summary>
        [Category( "Data" ), Description( "The settings to edit and were created/edited." ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public SubtitleExporterSettings SubtitleExporterSettings
        {
            get
            {
                SubtitleExporterSettings settings = new SubtitleExporterSettings();

                settings.CutsubExportPath = this.cutSubOutputPathTextBox.Text.Trim();
                settings.TextExporterSettings = this.localizedTextExporterSettingsControl.LocalizedTextExporterSettings;

                return settings;
            }
            set
            {
                if ( value != null )
                {
                    this.cutSubOutputPathTextBox.Text = value.CutsubExportPath;
                    this.localizedTextExporterSettingsControl.LocalizedTextExporterSettings = value.TextExporterSettings;
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when an edit is made.
        /// </summary>
        [Category( "Property Changed" ), Description( "Occurs when an edit is made." )]
        public event EventHandler Changed;
        #endregion

        #region Event Dispatchers
        protected void OnChanged()
        {
            if ( m_dispatchEvents && (this.Changed != null) )
            {
                this.Changed( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void localizedTextExporterSettingsControl_Changed( object sender, EventArgs e )
        {
            OnChanged();
        }

        private void cutSubOutputPathTextBox_TextChanged( object sender, EventArgs e )
        {
            OnChanged();
        }

        private void browseCutSubOutputPathButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Select the folder where 'scene.cutsub' will be saved.";

            string cutSubOutputPath = this.cutSubOutputPathTextBox.Text.Trim();
            if ( Directory.Exists( cutSubOutputPath ) )
            {
                this.folderBrowserDialog.SelectedPath = cutSubOutputPath;
            }

            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.cutSubOutputPathTextBox.Text = this.folderBrowserDialog.SelectedPath;

                OnChanged();
            }
        }
        #endregion

        #region Public Functions
        public void Validate( out rageStatus status )
        {
            this.localizedTextExporterSettingsControl.Validate( out status );
        }
        #endregion
    }
}
