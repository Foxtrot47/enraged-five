namespace SubtitleEditorCore
{
    partial class ScriptControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.openScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.newLocalizedTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAgainDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAgainUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchHighlightedDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchHighllightedUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.fileSystemWatcher = new System.IO.FileSystemWatcher();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox
            // 
            this.richTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.richTextBox.ContextMenuStrip = this.contextMenuStrip;
            this.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox.HideSelection = false;
            this.richTextBox.Location = new System.Drawing.Point( 0, 0 );
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.ShortcutsEnabled = false;
            this.richTextBox.Size = new System.Drawing.Size( 258, 245 );
            this.richTextBox.TabIndex = 1;
            this.richTextBox.Text = "";
            this.richTextBox.SelectionChanged += new System.EventHandler( this.richTextBox_SelectionChanged );
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.openScriptToolStripMenuItem,
            this.closeScriptToolStripMenuItem,
            this.toolStripSeparator2,
            this.findToolStripMenuItem,
            this.toolStripSeparator1,
            this.newLocalizedTextToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.searchAgainDownToolStripMenuItem,
            this.searchAgainUpToolStripMenuItem,
            this.searchHighlightedDownToolStripMenuItem,
            this.searchHighllightedUpToolStripMenuItem} );
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size( 267, 214 );
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler( this.contextMenuStrip_Opening );
            // 
            // openScriptToolStripMenuItem
            // 
            this.openScriptToolStripMenuItem.Image = global::SubtitleEditorCore.Properties.Resources.openHS;
            this.openScriptToolStripMenuItem.Name = "openScriptToolStripMenuItem";
            this.openScriptToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.openScriptToolStripMenuItem.Text = "&Open Script";
            this.openScriptToolStripMenuItem.Click += new System.EventHandler( this.openScriptToolStripMenuItem_Click );
            // 
            // closeScriptToolStripMenuItem
            // 
            this.closeScriptToolStripMenuItem.Image = global::SubtitleEditorCore.Properties.Resources.DeleteHS;
            this.closeScriptToolStripMenuItem.Name = "closeScriptToolStripMenuItem";
            this.closeScriptToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.closeScriptToolStripMenuItem.Text = "Close Script";
            this.closeScriptToolStripMenuItem.Click += new System.EventHandler( this.closeScriptToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 263, 6 );
            // 
            // findToolStripMenuItem
            // 
            this.findToolStripMenuItem.Image = global::SubtitleEditorCore.Properties.Resources.FindHS;
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            this.findToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.findToolStripMenuItem.Text = "&Find";
            this.findToolStripMenuItem.Click += new System.EventHandler( this.findToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 263, 6 );
            // 
            // newLocalizedTextToolStripMenuItem
            // 
            this.newLocalizedTextToolStripMenuItem.Image = global::SubtitleEditorCore.Properties.Resources.NewCardHS;
            this.newLocalizedTextToolStripMenuItem.Name = "newLocalizedTextToolStripMenuItem";
            this.newLocalizedTextToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Insert;
            this.newLocalizedTextToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.newLocalizedTextToolStripMenuItem.Text = "&New Localized Text";
            this.newLocalizedTextToolStripMenuItem.Click += new System.EventHandler( this.newLocalizedTextToolStripMenuItem_Click );
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = global::SubtitleEditorCore.Properties.Resources.CopyHS;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler( this.copyToolStripMenuItem_Click );
            // 
            // searchAgainDownToolStripMenuItem
            // 
            this.searchAgainDownToolStripMenuItem.Name = "searchAgainDownToolStripMenuItem";
            this.searchAgainDownToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.searchAgainDownToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.searchAgainDownToolStripMenuItem.Text = "Search Again Down";
            this.searchAgainDownToolStripMenuItem.Visible = false;
            this.searchAgainDownToolStripMenuItem.Click += new System.EventHandler( this.searchAgainDownToolStripMenuItem_Click );
            // 
            // searchAgainUpToolStripMenuItem
            // 
            this.searchAgainUpToolStripMenuItem.Name = "searchAgainUpToolStripMenuItem";
            this.searchAgainUpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this.searchAgainUpToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.searchAgainUpToolStripMenuItem.Text = "Search Again Up";
            this.searchAgainUpToolStripMenuItem.Visible = false;
            this.searchAgainUpToolStripMenuItem.Click += new System.EventHandler( this.searchAgainUpToolStripMenuItem_Click );
            // 
            // searchHighlightedDownToolStripMenuItem
            // 
            this.searchHighlightedDownToolStripMenuItem.Name = "searchHighlightedDownToolStripMenuItem";
            this.searchHighlightedDownToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F3)));
            this.searchHighlightedDownToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.searchHighlightedDownToolStripMenuItem.Text = "Search Highlighted Down";
            this.searchHighlightedDownToolStripMenuItem.Visible = false;
            this.searchHighlightedDownToolStripMenuItem.Click += new System.EventHandler( this.searchHighlightedDownToolStripMenuItem_Click );
            // 
            // searchHighllightedUpToolStripMenuItem
            // 
            this.searchHighllightedUpToolStripMenuItem.Name = "searchHighllightedUpToolStripMenuItem";
            this.searchHighllightedUpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.F3)));
            this.searchHighllightedUpToolStripMenuItem.Size = new System.Drawing.Size( 266, 22 );
            this.searchHighllightedUpToolStripMenuItem.Text = "Search Highllighted Up";
            this.searchHighllightedUpToolStripMenuItem.Visible = false;
            this.searchHighllightedUpToolStripMenuItem.Click += new System.EventHandler( this.searchHighllightedUpToolStripMenuItem_Click );
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Text Files (*.txt;*.rtf)|*.txt;*.rtf|All files (*.*)|*.*";
            this.openFileDialog.Title = "Open Script";
            // 
            // fileSystemWatcher
            // 
            this.fileSystemWatcher.EnableRaisingEvents = true;
            this.fileSystemWatcher.SynchronizingObject = this;
            this.fileSystemWatcher.Renamed += new System.IO.RenamedEventHandler( this.fileSystemWatcher_Renamed );
            this.fileSystemWatcher.Changed += new System.IO.FileSystemEventHandler( this.fileSystemWatcher_Changed );
            // 
            // ScriptControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.richTextBox );
            this.Name = "ScriptControl";
            this.Size = new System.Drawing.Size( 258, 245 );
            this.contextMenuStrip.ResumeLayout( false );
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem newLocalizedTextToolStripMenuItem;
        private System.IO.FileSystemWatcher fileSystemWatcher;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAgainDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAgainUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchHighlightedDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchHighllightedUpToolStripMenuItem;
    }
}
