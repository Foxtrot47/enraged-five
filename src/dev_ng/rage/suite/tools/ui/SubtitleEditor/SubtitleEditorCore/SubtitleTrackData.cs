using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Serialization;

namespace SubtitleEditorCore
{
    /// <summary>
    /// Holds the data associated with a track on the timeline
    /// </summary>
    [Serializable]
    public class SubtitleTrackData : IRageClonableObject
    {
        public SubtitleTrackData()
        {

        }

        #region Variables
        private string m_speaker = string.Empty;
        private SubtitleEventDataCollection m_eventDataCollection = new SubtitleEventDataCollection();
        #endregion

        #region Properties
        /// <summary>
        /// The name of the track, or who is speaking.
        /// </summary>
        [DefaultValue( "" ), Description( "The name of the track, or who is speaking." )]
        public string Speaker
        {
            get
            {
                return m_speaker;
            }
            set
            {
                m_speaker = value;
            }
        }

        /// <summary>
        /// The collection of SubtitleEventData items.
        /// </summary>
        [Description( "The collection of SubtitleEventData items." )]
        public SubtitleEventDataCollection EventDataCollection
        {
            get
            {
                return m_eventDataCollection;
            }
            set
            {                
                if ( value != null )
                {
                    m_eventDataCollection = value;
                }
                else
                {
                    m_eventDataCollection.Clear();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is SubtitleTrackData )
            {
                return Equals( obj as SubtitleTrackData );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Speaker;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            SubtitleTrackData data = new SubtitleTrackData();
            data.CopyFrom( this );
            return data;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is SubtitleTrackData )
            {
                CopyFrom( other as SubtitleTrackData );
            }
        }

        public void Reset()
        {
            this.EventDataCollection.Reset();
            this.Speaker = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( SubtitleTrackData other )
        {
            if ( other == null )
            {
                return;
            }

            this.EventDataCollection.CopyFrom( other.EventDataCollection );
            this.Speaker = other.Speaker;
        }

        public bool Equals( SubtitleTrackData other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.Speaker == other.Speaker)
                && this.EventDataCollection.Equals( other.EventDataCollection );
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of SubtitleTrackData items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class SubtitleTrackDataCollection : rageSerializableList<SubtitleTrackData>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            SubtitleTrackDataCollection collection = new SubtitleTrackDataCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion

        #region Public Functions
        public SubtitleTrackData Find( string speaker )
        {
            foreach ( SubtitleTrackData item in this )
            {
                if ( item.Speaker == speaker )
                {
                    return item;
                }
            }

            return null;
        }
        #endregion
    }
}
