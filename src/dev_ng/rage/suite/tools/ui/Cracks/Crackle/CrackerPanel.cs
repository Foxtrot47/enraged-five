using System;
using System.Drawing;
using System.Windows.Forms;

namespace Crackle
{
    class CrackerPanel : Panel
    {
        protected override Point ScrollToControl(Control activeControl)
        {
            return AutoScrollPosition;
        }

        public void CrackerPanel_KeyDown(object sender, KeyEventArgs e, float zoom)
        {            
            //int scaledOffset = (int)((float)offset * (zoom));
            float scaledOffset = offset * zoom;

            if ( e.Modifiers == Keys.Control )
            {
                scaledOffset *= 2;
            }

            switch (e.KeyCode)
            {
                case Keys.Left:
                    AutoScrollPosition = new Point(-AutoScrollPosition.X - (int)scaledOffset, -AutoScrollPosition.Y);
                    break;
                case Keys.Right:
                    AutoScrollPosition = new Point(-AutoScrollPosition.X + (int)scaledOffset, -AutoScrollPosition.Y);
                    break;
                case Keys.Up:
                    AutoScrollPosition = new Point(-AutoScrollPosition.X, -AutoScrollPosition.Y - (int)scaledOffset);
                    break;
                case Keys.Down:
                    AutoScrollPosition = new Point(-AutoScrollPosition.X, -AutoScrollPosition.Y + (int)scaledOffset);
                    break;                
            }
        }

        public void CrackerPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                location = MousePosition;
                ignoreMove = false;
            }
        }

        public void CrackerPanel_MouseMove(object sender, MouseEventArgs e, float zoom, float zoomStep)
        {
            if (ignoreMove)
                return;

            Point offset = new Point(MousePosition.X - location.X, MousePosition.Y - location.Y);

            offset.X = (int)(offset.X / (zoom * zoomStep));
            offset.Y = (int)(offset.Y / (zoom * zoomStep));

            if (offset.X != 0 || offset.Y != 0)
            {
                AutoScrollPosition = new Point(-AutoScrollPosition.X + Math.Abs(offset.X) * offset.X, -AutoScrollPosition.Y + Math.Abs(offset.Y) * offset.Y);
                location = MousePosition;
            }
        }

        public void CrackerPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
                ignoreMove = true;
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
        }

        private Point location = new Point();
        private int offset = 8;        
        private bool ignoreMove = true;
    }
}
