using System;
using System.Drawing;
using System.Windows.Forms;

namespace Crackle
{
    public partial class CracksForm : Form
    {
        public CracksForm()
        {
            //Initialize the window's controls
            InitializeComponent();

            //Set OnOpen event
            cracker.OnOpen += new Cracks.OpenEventHandler(this.Open);

            //Set OnDone event
            cracker.OnDone += new EventHandler(this.Done);
            cracker.OnUndone += new Cracks.UndoneEventHandler(this.Undone);
            cracker.OnSplitable += new Cracks.SplitableEventHandler(this.Splitable);

            //cracker.StoreDecomposition(storeDecompositionToolStripMenuItem.Checked);
            cracker.ShowDecomposition(showDecompositionToolStripMenuItem.Checked);

            showCenterToolStripMenuItem.Checked = cracker.CenterPointVisible;

            Invalidate(true);
        }

        private void Open(object sender, Cracks.OpenEventArgs e)
        {
            //storeDecompositionToolStripMenuItem.Enabled = true;
            showDecompositionToolStripMenuItem.Enabled = true;
            showStatsToolStripMenuItem.Enabled = true;
            //storeDecompositionToolStripMenuItem.Checked = e.Convex;
            //showDecompositionToolStripMenuItem.Checked = e.Convex;
            showCenterToolStripMenuItem.Enabled = false;
            weldSelectionToolStripMenuItem.Enabled = false;
            toggleBackgroundToolStripMenuItem.Enabled = false;
        }

        private void Done(object sender, EventArgs e)
        {
            //Enable save and undo, disable redo
            saveToolStripMenuItem.Enabled = !cracker.Saved();
            redoToolStripMenuItem.Enabled = false;
            undoToolStripMenuItem.Enabled = true;
        }

        private void Undone(object sender, Cracks.UndoneEventArgs e)
        {
            //Enable save and undo, disable redo
            saveToolStripMenuItem.Enabled = !cracker.Saved();
            redoToolStripMenuItem.Enabled = true;
            undoToolStripMenuItem.Enabled = e.Undo;
        }

        private void Splitable(object sender, Cracks.SplitableEventArgs e)
        {
            //Enable splitable
            splitPieceToolStripMenuItem.Enabled = e.Splitable;
        }

        private void cracker_KeyDown(object sender, KeyEventArgs e)
        {
            // forward to parent container            
            panel.CrackerPanel_KeyDown(sender, e, cracker.Zoom);

            switch (e.KeyCode)
            {
            case Keys.Oemplus:
                if (zoomInToolStripMenuItem.Enabled)
                    zoomInToolStripMenuItem_Click(this, new EventArgs());
                break;
            case Keys.OemMinus:
                if (zoomOutToolStripMenuItem.Enabled)
                    zoomOutToolStripMenuItem_Click(this, new EventArgs());
                break;
            case Keys.C:
                if (showCenterToolStripMenuItem.Enabled)
                    showCenterToolStripMenuItem_Click(this, new EventArgs());
                break;
            case Keys.B:
                if (toggleBackgroundToolStripMenuItem.Enabled)
                    toggleBackgroundToolStripMenuItem_Click(this, new EventArgs());
                break;
            case Keys.S:
                if(splitPieceToolStripMenuItem.Enabled)
                    splitPieceToolStripMenuItem_Click(this, new EventArgs());
                break;
            case Keys.W:
                if (weldSelectionToolStripMenuItem.Enabled)
                    cracker.WeldVertices();
                break;
            }
        }

        private void cracker_MouseDown(object sender, MouseEventArgs e)
        {
            // forward to parent container
            panel.CrackerPanel_MouseDown(sender, e);
        }

        private void cracker_MouseMove(object sender, MouseEventArgs e)
        {
            // forward to parent container
            panel.CrackerPanel_MouseMove(sender, e, cracker.Zoom, cracker.ZoomStep);
        }

        private void cracker_MouseUp(object sender, MouseEventArgs e)
        {
            // forward to parent container
            panel.CrackerPanel_MouseUp(sender, e);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Request that the user specify a filename
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image Files (*.ACT;*.BMP;*.CUT;*.DCX;*.DDS;*.GIF;*.ICO;*.JPG;*.LBM;*.LIF;*.MDL;*.PAL;*.PCD;*.PCX;*.PIC;*.PNG;*.PNM;*.PSD;*.PSP;*.RAW;*.SGI;*.TGA;*.TIF;*.WAL)|*.ACT;*.BMP;*.CUT;*.DCX;*.DDS;*.GIF;*.ICO;*.JPG;*.LBM;*.LIF;*.MDL;*.PAL;*.PCD;*.PCX;*.PIC;*.PNG;*.PNM;*.PSD;*.PSP;*.RAW;*.SGI;*.TGA;*.TIF;*.WAL";
            ofd.Filter += "|ACT files (*.ACT)|*.ACT";
            ofd.Filter += "|BMP files (*.BMP)|*.BMP";
            ofd.Filter += "|CUT files (*.CUT)|*.CUT";
            ofd.Filter += "|DCX files (*.DCX)|*.DCX";
            ofd.Filter += "|DDS files (*.DDS)|*.DDS";
            ofd.Filter += "|GIF files (*.GIF)|*.GIF";
            ofd.Filter += "|ICO files (*.ICO)|*.ICO";
            ofd.Filter += "|JPG files (*.JPG)|*.JPG";
            ofd.Filter += "|LBM files (*.LBM)|*.LBM";
            ofd.Filter += "|LIF files (*.LIF)|*.LIF";
            ofd.Filter += "|MDL files (*.MDL)|*.MDL";
            ofd.Filter += "|PAL files (*.PAL)|*.PAL";
            ofd.Filter += "|PCD files (*.PCD)|*.PCD";
            ofd.Filter += "|PCX files (*.PCX)|*.PCX";
            ofd.Filter += "|PIC files (*.PIC)|*.PIC";
            ofd.Filter += "|PNG files (*.PNG)|*.PNG";
            ofd.Filter += "|PNM files (*.PNM)|*.PNM";
            ofd.Filter += "|PSD files (*.PSD)|*.PSD";
            ofd.Filter += "|PSP files (*.PSP)|*.PSP";
            ofd.Filter += "|RAW files (*.RAW)|*.RAW";
            ofd.Filter += "|SGI files (*.SGI)|*.SGI";
            ofd.Filter += "|TGA files (*.TGA)|*.TGA";
            ofd.Filter += "|TIF files (*.TIF)|*.TIF";
            ofd.Filter += "|WAL files (*.WAL)|*.WAL";
            ofd.Filter += "|All files (*.*)|*.*";
            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            //Attempt to open the specified file
            if (!cracker.OpenReferenceFiles(ofd.FileName))
                return;
            cracker.Focus();

            //Add the filename to the window title
            int backslash = ofd.FileName.LastIndexOf('\\');
            Text = backslash == -1 ? ofd.FileName : ofd.FileName.Substring(1 + backslash);
            int dot = Text.LastIndexOf('.');
            if (dot != -1)
                Text = Text.Substring(0, dot);
            Text += " - Crackle";

            //Disable save, undo and redo
            saveToolStripMenuItem.Enabled = !cracker.Saved();
            undoToolStripMenuItem.Enabled = false;
            redoToolStripMenuItem.Enabled = false;

            saveReferenceLinesStripMenuItem.Enabled = true;

            zoomInToolStripMenuItem.Enabled = true;
            zoomOutToolStripMenuItem.Enabled = true;

            showCenterToolStripMenuItem.Enabled = true;
            toggleBackgroundToolStripMenuItem.Enabled = true;
            weldSelectionToolStripMenuItem.Enabled = true;

            Invalidate(true);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Inform the cracker control it should attempt to save
            if (!cracker.Save())
                return;

            //Disable save
            saveToolStripMenuItem.Enabled = !cracker.Saved();
        }

        //private void storeDecompositionToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    //convexToolStripMenuItem.Checked = !convexToolStripMenuItem.Checked;
        //    if (!saveToolStripMenuItem.Enabled)
        //        saveToolStripMenuItem.Enabled = true;
        //    else
        //        saveToolStripMenuItem.Enabled = !cracker.Saved();
        //    cracker.StoreDecomposition(storeDecompositionToolStripMenuItem.Checked);
        //}

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Close the window
            this.Close();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Disable undo if we've reached the end
            if (!cracker.Undo())
                undoToolStripMenuItem.Enabled = false;

            //Enable save and redo
            saveToolStripMenuItem.Enabled = !cracker.Saved();
            redoToolStripMenuItem.Enabled = true;
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Disable redo if we've reached the beginning
            if (!cracker.Redo())
                redoToolStripMenuItem.Enabled = false;

            //Enable save and undo
            saveToolStripMenuItem.Enabled = !cracker.Saved();
            undoToolStripMenuItem.Enabled = true;
        }

        private void showDecompositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cracker.ShowDecomposition(showDecompositionToolStripMenuItem.Checked);
        }

        private void zoomInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //cracker.Zoom = 2 * cracker.Zoom;
            cracker.Zoom = cracker.Zoom + cracker.ZoomStep;
        }

        private void zoomOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //cracker.Zoom = cracker.Zoom / 2;
            cracker.Zoom = cracker.Zoom - cracker.ZoomStep;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Show the about box
            new AboutBox().ShowDialog(this);
        }

        private void CracksForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Allow the cracker control to prevent the window from closing
            e.Cancel = !cracker.Exit();
        }

        private void showCenterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cracker.CenterPointVisible = !cracker.CenterPointVisible;
            showCenterToolStripMenuItem.Checked = cracker.CenterPointVisible;

            cracker.Invalidate(true);
        }

        private void saveReferenceLinesStripMenuItem_Click(object sender, EventArgs e)
        {
            cracker.Save();
        }

        private void toggleBackgroundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toggleBackgroundToolStripMenuItem.Checked = !toggleBackgroundToolStripMenuItem.Checked;
            cracker.BackgroundVisible = toggleBackgroundToolStripMenuItem.Checked;
        }

        private void cracker_KeyUp(object sender, KeyEventArgs e)
        {
        }

        private void splitPieceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cracker.SplitPiece();
        }

        private void weldSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cracker.WeldVertices();
        }

        private void showStatsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showStatsToolStripMenuItem.Checked = !showStatsToolStripMenuItem.Checked;
            cracker.ShowStats(showStatsToolStripMenuItem.Checked);

        }
    }
}
