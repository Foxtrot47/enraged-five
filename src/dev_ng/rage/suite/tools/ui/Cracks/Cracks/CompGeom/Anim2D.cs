using System;

namespace CompGeom
{
    public abstract class Anim2D
    {
        // default constructor
        protected internal System.Drawing.Graphics animGraphics = null;

        public virtual void register(System.Drawing.Graphics g)
        {
            animGraphics = g;
        }
        public virtual bool animating()
        {
            return animGraphics != null;
        }
        public virtual void draw(System.Drawing.Graphics g)
        {
            draw(g, false);
        }

        public abstract void draw(System.Drawing.Graphics g, bool label);
    }
}
