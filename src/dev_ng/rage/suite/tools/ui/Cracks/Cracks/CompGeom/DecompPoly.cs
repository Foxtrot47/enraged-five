using System;
using System.Collections.Generic;

namespace CompGeom
{
    sealed public class DecompPoly : Anim2D
    {
        private int After
        {
            set
            {
                // i reflex
                assert(reflex(value), "non reflex i in setAfter(" + value + ")");
                subD.setWeight(value, value + 1, 0);
                if (visible(value, value + 2))
                    subD.init(value, value + 2, 0, value + 1, value + 1);
            }
        }

        private int Before
        {
            set
            {
                // i reflex
                assert(reflex(value), "non reflex i in setAfter(" + value + ")");
                subD.setWeight(value - 1, value, 0);
                if (visible(value - 2, value))
                    subD.init(value - 2, value, 0, value - 1, value - 1);
            }
        }

        private PointList sp; // the polygon
        private int n; // number of vertices
        private SubDecomp subD; // the subproblems in  n x r space

        private int reflexFirst; // for reflexIter
        private int[] reflexNext_Renamed_Field;
        private bool[] reflexFlag;

        internal const int DELAY = 100; // time for animation

        public DecompPoly(PointList pl)
        {
            sp = pl;
            n = sp.number();
        }

        public void init()
        {
            initReflex();
            subD = new SubDecomp(reflexFlag);
            initVisibility();
            initSubproblems();
        }

        public bool reflex(int i)
        {
            return reflexFlag[i];
        }

        private void initReflex()
        {
            reflexFlag = new bool[n];
            reflexNext_Renamed_Field = new int[n];

            int wrap = 0; /* find reflex vertices */
            reflexFlag[wrap] = true; // by convention
            for (int i = n - 1; i > 0; i--)
            {
                reflexFlag[i] = Point.right(sp.p(i - 1), sp.p(i), sp.p(wrap));
                wrap = i;
            }

            reflexFirst = n; // for reflexIter
            for (int i = n - 1; i >= 0; i--)
            {
                reflexNext_Renamed_Field[i] = reflexFirst;
                if (reflex(i))
                {
                    reflexFirst = i;
                }
            }
        }

        /* a cheap iterator through reflex vertices; each vertex knows the
        index of the next reflex vertex. */
        public int reflexIter()
        {
            return reflexFirst;
        }

        public int reflexIter(int i)
        {
            // start w/ i or 1st reflex after...
            if (i <= 0)
                return reflexFirst;
            if (i > reflexNext_Renamed_Field.Length)
                return reflexNext_Renamed_Field.Length;
            return reflexNext_Renamed_Field[i - 1];
        }

        public int reflexNext(int i)
        {
            return reflexNext_Renamed_Field[i];
        }

        public const int INFINITY = 100000;
        public const int BAD = 999990;
        public const int NONE = 0;

        public bool visible(int i, int j)
        {
            return subD.weight(i, j) < BAD;
        }

        public void initVisibility()
        {
            // initReflex() first
            if (animating())
                SupportClass.GraphicsManager.manager.SetColor(animGraphics, System.Drawing.Color.Yellow);
            VisPoly vp = new VisPoly(sp);
            for (int i = reflexIter(); i < n; i = reflexNext(i))
            {
                vp.build(i);
                while (!vp.empty())
                {
                    int j = vp.popVisible() % n;
                    if (j < i)
                        subD.setWeight(j, i, INFINITY);
                    else
                        subD.setWeight(i, j, INFINITY);
                    if (animating())
                        animGraphics.DrawLine(SupportClass.GraphicsManager.manager.GetPen(animGraphics), sp.p(i).x(), sp.p(i).y(), sp.p(j).x(), sp.p(j).y());
                }
            }
            if (animating())
            {
                SupportClass.GraphicsManager.manager.SetColor(animGraphics, System.Drawing.Color.Blue);
                try
                {
                    //UPGRADE_TODO: Method 'java.lang.Thread.sleep' was converted to 'System.Threading.Thread.Sleep' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javalangThreadsleep_long'"
                    System.Threading.Thread.Sleep(new System.TimeSpan((System.Int64)10000 * DELAY));
                }
                catch (System.Threading.ThreadInterruptedException)
                {
                }
            }
        }

        public void initSubproblems()
        {
            // initVisibility first
            int i;

            i = reflexIter();
            if (i == 0)
            {
                After = i; i = reflexNext(i);
            }
            if (i == 1)
            {
                subD.setWeight(0, 1, 0); After = i; i = reflexNext(i);
            }
            while (i < n - 2)
            {
                Before = i; After = i; i = reflexNext(i);
            }
            if (i == n - 2)
            {
                Before = i; subD.setWeight(i, i + 1, 0); i = reflexNext(i);
            }
            if (i == n - 1)
            {
                Before = i;
            }
        }

        public void initPairs(int i, int k)
        {
            subD.init(i, k);
        }

        public int guard;

        public void recoverSolution(int i, int k)
        {
            // make the bottom of the stack determines the solution used
            //
            int j;
            if (guard-- < 0)
            {
                System.Console.Out.WriteLine("Can't recover" + i + "," + k); return;
            }
            if (k - i <= 1)
                return;
            PairDeque pair = subD.pairs(i, k);
            if (reflex(i))
            {
                j = pair.bB();
                recoverSolution(j, k); // use jk, so no constraints from pair
                if (j - i > 1)
                {
                    if (pair.aB() != pair.bB())
                    {
                        // if we aren't using ij here
                        PairDeque pd = subD.pairs(i, j); // i still reflex, so fix bottom to match pairs.aB()
                        pd.restore();
                        while ((!pd.emptyB()) && pair.aB() != pd.aB())
                            pd.popB();
                        assert(!pd.emptyB(), "emptied pd " + i + "," + j + "," + k + " " + pair.ToString());
                    }
                    recoverSolution(i, j);
                }
            }
            else
            {
                j = pair.aB();
                recoverSolution(i, j); // use ij, so no constraints from pair
                if (k - j > 1)
                {
                    if (pair.aB() != pair.bB())
                    {
                        PairDeque pd = subD.pairs(j, k);
                        pd.restore();
                        while ((!pd.empty()) && pair.bB() != pd.bB())
                            pd.popB();
                        assert(!pd.empty(), "emptied pd " + i + "," + j + "," + k + " " + pair.ToString());
                    }
                    recoverSolution(j, k);
                }
            }
        }

        public void typeA(int i, int j, int k)
        {
            /* i reflex; use jk */
            //    System.out.print("\nA "+i+","+j+","+k+":");
            //    assert(reflex(i), "non reflex i in typeA("+i+","+j+","+k+")");
            //    assert(k-i > 1, "too small in typeA("+i+","+j+","+k+")");
            if (!visible(i, j))
                return;
            int top = j;
            int w = subD.weight(i, j);
            if (k - j > 1)
            {
                if (!visible(j, k))
                    return;
                w += subD.weight(j, k) + 1;
            }
            if (j - i > 1)
            {
                // check if must use ij, too.
                PairDeque pair = subD.pairs(i, j);
                if (!Point.left(sp.p(k), sp.p(j), sp.p(pair.bB())))
                {
                    while (pair.more1B() && !Point.left(sp.p(k), sp.p(j), sp.p(pair.underbB())))
                        pair.popB();
                    if ((!pair.emptyB()) && !Point.right(sp.p(k), sp.p(i), sp.p(pair.aB())))
                        top = pair.aB();
                    else
                        w++; // yes, use ij. top = j already
                }
                else
                    w++; // yes, use ij. top = j already
            }
            update(i, k, w, top, j);
        }

        public void typeB(int i, int j, int k)
        {
            /* k reflex, i not. */
            //    System.out.print("\nB "+i+","+j+","+k+":");
            if (!visible(j, k))
                return;
            int top = j;
            int w = subD.weight(j, k);
            if (j - i > 1)
            {
                if (!visible(i, j))
                    return;
                w += subD.weight(i, j) + 1;
            }
            if (k - j > 1)
            {
                // check if must use jk, too.
                PairDeque pair = subD.pairs(j, k);
                if (!Point.right(sp.p(i), sp.p(j), sp.p(pair.aF())))
                {
                    while (pair.more1() && !Point.right(sp.p(i), sp.p(j), sp.p(pair.underaF())))
                        pair.pop();
                    if ((!pair.empty()) && !Point.left(sp.p(i), sp.p(k), sp.p(pair.bF())))
                        top = pair.bF();
                    else
                        w++; // yes, use jk. top=j already
                }
                else
                    w++; // yes, use jk. top=j already
            }
            update(i, k, w, j, top);
        }

        /* We have a new solution for subprob a,b with weight w, using
        i,j.  If it is better than previous solutions, we update. 
        We assume that a<b and i < j.
        */
        public void update(int a, int b, int w, int i, int j)
        {
            //    System.out.print("update("+a+","+b+" w:"+w+" "+i+","+j+")");
            int ow = subD.weight(a, b);
            if (w <= ow)
            {
                PairDeque pair = subD.pairs(a, b);
                if (w < ow)
                {
                    pair.flush(); subD.setWeight(a, b, w);
                }
                pair.pushNarrow(i, j);
            }
        }

        public void assert(bool flag, System.String s)
        {
            if (!flag)
                System.Console.Out.WriteLine("ASSERT FAIL: " + s);
        }

        private void drawDiagonal(System.Drawing.Graphics g, bool label, int i, int k)
        {
            g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), sp.p(i).x(), sp.p(i).y(), sp.p(k).x(), sp.p(k).y());
            if (label && (reflex(i) || reflex(k)))
            {
                //UPGRADE_TODO: Method 'java.awt.Graphics.drawString' was converted to 'System.Drawing.Graphics.DrawString' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtGraphicsdrawString_javalangString_int_int'"
                g.DrawString(System.Convert.ToString(subD.weight(i, k)), SupportClass.GraphicsManager.manager.GetFont(g), SupportClass.GraphicsManager.manager.GetBrush(g), (sp.p(i).x() + sp.p(k).x()) / 2, (sp.p(i).y() + sp.p(k).y()) / 2 - SupportClass.GraphicsManager.manager.GetFont(g).GetHeight());
            }
        }

        private void drawHelper(System.Drawing.Graphics g, bool label, int i, int k)
        {
            int j;
            bool ijreal = true, jkreal = true;
            if (k - i <= 1)
                return;
            PairDeque pair = subD.pairs(i, k);
            if (reflex(i))
            {
                j = pair.bB(); ijreal = (pair.aB() == pair.bB());
            }
            else
            {
                j = pair.aB(); jkreal = (pair.bB() == pair.aB());
            }

            if (ijreal)
                drawDiagonal(g, label, i, j);
            else if (label)
            {
                SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Orange);
                drawDiagonal(g, label, i, j);
                SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Blue);
            }

            if (jkreal)
                drawDiagonal(g, label, j, k);
            else if (label)
            {
                SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Orange);
                drawDiagonal(g, label, j, k);
                SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Blue);
            }

            if (guard-- < 0)
            {
                System.Console.Out.WriteLine("Infinite Loop drawing" + i + "," + k);
                return;
            }
            drawHelper(g, label, i, j);
            drawHelper(g, label, j, k);
        }

        public override void draw(System.Drawing.Graphics g, bool label)
        {
            System.Drawing.Color shapeColor = System.Drawing.Color.Blue;
            System.Drawing.Color polyColor = System.Drawing.Color.LightBlue;
            sp.drawPolygon(g, sp.number(), shapeColor, polyColor);
            if (animating())
            {
                for (int i = reflexIter(); i < sp.number(); i = reflexNext(i))
                    sp.p(i).drawCirc(animGraphics, 5);
            }
            guard = 3 * n;
            drawDiagonal(g, label, 0, sp.number() - 1);
            drawHelper(g, label, 0, sp.number() - 1);
        }

        public void decompose(List<List<int>> lists, int list, int i, int k)
        {
            int j;
            bool ijreal = true, jkreal = true;
            if (k - i <= 1)
                return;
            PairDeque pair = subD.pairs(i, k);
            if (reflex(i))
            {
                j = pair.bB(); ijreal = (pair.aB() == pair.bB());
            }
            else
            {
                j = pair.aB(); jkreal = (pair.bB() == pair.aB());
            }

            int index = 0;
            for (; index != lists[list].Count; ++index)
            {
                if (j < lists[list][index])
                    break;
            }
            lists[list].Insert(index, j);
            int ijlist = list, jklist = list;

            if (ijreal)
            {
                //If the distance between i and j is greater than 1, then ij needs a new list.
                if (j - i > 1)
                {
                    ijlist = lists.Count;
                    lists.Add(new List<int>());
                    lists[ijlist].Add(i);
                    lists[ijlist].Add(j);
                }
            }

            if (jkreal)
            {
                /*
                 * We need a new list when the distance between consecutive indices is greater than 1.
                 * If ijreal then we need the new list for ij.
                 * If jkreal then we need the new list for jk.
                 */
                //If the distance between j and k is greater than 1, then jk needs a new list.
                if (k - j > 1)
                {
                    jklist = lists.Count;
                    lists.Add(new List<int>());
                    lists[jklist].Add(j);
                    lists[jklist].Add(k);
                }
            }

            if (guard-- < 0)
            {
                System.Console.Out.WriteLine("Infinite Loop decomposing" + i + "," + k);
                return;
            }
            decompose(lists, ijlist, i, j);
            decompose(lists, jklist, j, k);
        }

        public List<List<int>> decompose()
        {
            guard = 3 * n;
            List<List<int>> lists = new List<List<int>>();
            lists.Add(new List<int>());
            lists[0].Add(0);
            lists[0].Add(sp.number() - 1);
            decompose(lists, 0, 0, sp.number() - 1);
            return lists;
        }

        public override System.String ToString()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append(sp.number() + ": " + sp.ToString());
            return s.ToString();
        }
    }

    /* This class stores weights and stacks for all subproblems for the 
	decomposition by dynamic programming.  
	It uses indirect addressing on the second index to make all the
	reflex vertices come first so it needs to allocate only O(nr) space.
	*/
    sealed class SubDecomp
    {
        private int[][] wt;
        private PairDeque[][] pd;
        private int[] rx; // indirect index so reflex come first

        internal SubDecomp(bool[] reflex)
        {
            int n = reflex.Length, r = 0;

            rx = new int[n];

            for (int i = 0; i < n; i++)
                if (reflex[i])
                    rx[i] = r++;

            int j = r;
            for (int i = 0; i < n; i++)
                if (!reflex[i])
                    rx[i] = j++;

            wt = new int[n][];
            pd = new PairDeque[n][];
            for (int i = 0; i < n; i++)
            {
                wt[i] = new int[reflex[i] ? n : r];
                for (j = 0; j < wt[i].Length; j++)
                    wt[i][j] = DecompPoly.BAD;
                pd[i] = new PairDeque[wt[i].Length];
            }
        }

        public void setWeight(int i, int j, int w)
        {
            wt[i][rx[j]] = w;
        }

        public int weight(int i, int j)
        {
            return wt[i][rx[j]];
        }

        public PairDeque pairs(int i, int j)
        {
            return pd[i][rx[j]];
        }

        public PairDeque init(int i, int j)
        {
            return pd[i][rx[j]] = new PairDeque();
        }

        public void init(int i, int j, int w, int a, int b)
        {
            setWeight(i, j, w); init(i, j).push(a, b);
        }
    }
}
