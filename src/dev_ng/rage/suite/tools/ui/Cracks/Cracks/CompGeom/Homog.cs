using System;

namespace CompGeom
{
    public sealed class Homog
    {
        internal double w, x_Renamed_Field, y_Renamed_Field;

        public Homog(double pw, double px, double py)
        {
            w = pw; x_Renamed_Field = px; y_Renamed_Field = py;
        }

        public Homog(Point p) : this(1.0, p.x_Renamed_Field, p.y_Renamed_Field)
        {
        }

        public Homog(int px, int py) : this(1.0, px, -py)
        {
        }

        //UPGRADE_NOTE: ref keyword was added to struct-type parameters. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1303'"
        public Homog(ref System.Drawing.Point p) : this(p.X, p.Y)
        {
        }

        //UPGRADE_NOTE: Final was removed from the declaration of 'INFINITY '. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1003'"
        public static readonly Homog INFINITY = new Homog(1.0, 0.0, 0.0);

        public Homog assign(double pw, double px, double py)
        {
            w = pw; x_Renamed_Field = px; y_Renamed_Field = py; return this;
        }

        internal int x()
        {
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            return (int)(x_Renamed_Field / w);
        } // drawing needs these

        internal int y()
        {
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            return (int)((-y_Renamed_Field) / w);
        }

        public Homog add(Homog p)
        {
            x_Renamed_Field += p.x_Renamed_Field; y_Renamed_Field += p.y_Renamed_Field; return this;
        }

        public Homog sub(Homog p)
        {
            x_Renamed_Field -= p.x_Renamed_Field; y_Renamed_Field -= p.y_Renamed_Field; return this;
        }

        public Homog neg()
        {
            w = -w; x_Renamed_Field = -x_Renamed_Field; y_Renamed_Field = -y_Renamed_Field; return this;
        }

        public Homog mul(double m)
        {
            w *= m; x_Renamed_Field *= m; y_Renamed_Field *= m; return this;
        }

        public Homog div(double m)
        {
            w /= m; x_Renamed_Field /= m; y_Renamed_Field /= m; return this;
        }

        public Homog normalize()
        {
            return div(len());
        }

        public double len2()
        {
            return x_Renamed_Field * x_Renamed_Field + y_Renamed_Field * y_Renamed_Field;
        }

        public double len()
        {
            return System.Math.Sqrt(this.len2());
        }

        public Homog perp()
        {
            double tmp = -y_Renamed_Field; y_Renamed_Field = x_Renamed_Field; x_Renamed_Field = tmp; return this;
        }

        public double dot(Point p)
        {
            return w + x_Renamed_Field * p.x_Renamed_Field + y_Renamed_Field * p.y_Renamed_Field;
        }

        public double dot(Homog p)
        {
            return w * p.w + x_Renamed_Field * p.x_Renamed_Field + y_Renamed_Field * p.y_Renamed_Field;
        }

        public double perpdot(Homog p)
        {
            return x_Renamed_Field * p.y_Renamed_Field - y_Renamed_Field * p.x_Renamed_Field;
        }

        public double dotperp(Homog p)
        {
            return (-x_Renamed_Field) * p.y_Renamed_Field + y_Renamed_Field * p.x_Renamed_Field;
        }

        public bool equals(Homog q)
        {
            return (q.w * x_Renamed_Field == w * q.x_Renamed_Field) && (q.w * y_Renamed_Field == w * q.y_Renamed_Field);
        }

        public bool left(Point p)
        {
            return dot(p) > 0;
        }

        public bool right(Point p)
        {
            return dot(p) < 0;
        }

        public static double det(Homog p, Homog q, Homog r)
        {
            return p.w * q.perpdot(r) - q.w * p.perpdot(r) + r.w * p.perpdot(q);
        }

        public static bool ccw(Homog p, Homog q, Homog r)
        {
            return det(p, q, r) > 0;
        }

        public static bool cw(Homog p, Homog q, Homog r)
        {
            return det(p, q, r) < 0;
        }

        public System.Drawing.Point toScreen()
        {
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            return new System.Drawing.Point((int)(x_Renamed_Field / w), (int)((-y_Renamed_Field) / w));
        }

        public Point toPoint()
        {
            return new Point(x_Renamed_Field / w, y_Renamed_Field / w);
        }

        public Homog meet(Homog p)
        {
            return new Homog(x_Renamed_Field * p.y_Renamed_Field - y_Renamed_Field * p.x_Renamed_Field, p.w * y_Renamed_Field - w * p.y_Renamed_Field, w * p.x_Renamed_Field - p.w * x_Renamed_Field);
        }

        public Homog meet(Point p)
        {
            return new Homog(x_Renamed_Field * p.y_Renamed_Field - y_Renamed_Field * p.x_Renamed_Field, y_Renamed_Field - w * p.y_Renamed_Field, w * p.x_Renamed_Field - x_Renamed_Field);
        }

        internal static Homog bisect(Homog l, Homog m)
        {
            System.Console.Out.WriteLine("bisect " + l + m);
            Homog ip = l.meet(m);
            System.Console.Out.WriteLine("ip> " + ip + ip.toPoint());
            if (ip.w == 0)
            {
                /* parallel segments, must be opposite or colinear */
                m.w = (m.w + l.w) / 2.0;
                //UPGRADE_TODO: Method 'java.io.PrintStream.println' was converted to 'System.Console.Out.WriteLine' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaioPrintStreamprintln_javalangObject'"
                System.Console.Out.WriteLine(m);
                return m;
            }
            else
            {
                m.normalize().add(l.normalize()); /* midpt at infinity */
                m.w = 0.0;
                l = m.meet(ip); /* meet mid point with ip. */
                System.Console.Out.WriteLine("m " + m + "  l " + l + "bisect done");
                return l;
            }
        }

        public override System.String ToString()
        {
            return " (" + w + "; " + x_Renamed_Field + ", " + y_Renamed_Field + ")  ";
        }

        public void draw(System.Drawing.Graphics g)
        {
            toPoint().draw(g);
        }

        public void drawLine(System.Drawing.Graphics g)
        {
            //UPGRADE_TODO: The equivalent in .NET for method 'java.awt.Graphics.getClipRect' may return a different value. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1043'"
            System.Drawing.Rectangle r = System.Drawing.Rectangle.Truncate(g.ClipBounds);
            System.Console.Out.WriteLine("\n drawline" + this);
            if (r.Height * System.Math.Abs(x_Renamed_Field) > r.Width * System.Math.Abs(y_Renamed_Field))
            {
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), (int)((w - r.Y * y_Renamed_Field) / (-x_Renamed_Field)), r.Y, (int)((w - (r.Y + r.Height) * y_Renamed_Field) / (-x_Renamed_Field)), r.Y + r.Height);
            }
            else
            {
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), r.X, (int)((w + r.X * x_Renamed_Field) / y_Renamed_Field), r.X + r.Width, (int)((w + (r.X + r.Width) * x_Renamed_Field) / y_Renamed_Field));
            }
        }
    }
}
