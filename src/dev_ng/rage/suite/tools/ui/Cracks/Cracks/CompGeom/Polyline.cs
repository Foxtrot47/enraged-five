using System;

namespace CompGeom
{
    public class Polyline : PointList
    {
        public Polyline(int n) : base(n)
        {
        }

        public Polyline(PointList pl) : base(pl)
        {
        }

        public override void draw(System.Drawing.Graphics g, bool label)
        {
            base.draw(g, label);
            drawPolyline(g, number());
        }
    }
}
