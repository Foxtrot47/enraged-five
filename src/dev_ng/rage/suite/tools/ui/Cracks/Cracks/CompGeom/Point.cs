using System;
namespace CompGeom
{
    public class Point
    {
        protected internal double x_Renamed_Field, y_Renamed_Field;

        public Point(double px, double py)
        {
            x_Renamed_Field = px; y_Renamed_Field = py;
        }

        public Point(int px, int py)
        {
            x_Renamed_Field = px; y_Renamed_Field = -py;
        }

        public Point(Point p) : this(p.x_Renamed_Field, p.y_Renamed_Field)
        {
        }

        //UPGRADE_NOTE: ref keyword was added to struct-type parameters. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1303'"
        public Point(ref System.Drawing.Point p) : this(p.X, p.Y)
        {
        }

        public Point(Homog p) : this(p.x_Renamed_Field / p.w, p.y_Renamed_Field / p.w)
        {
        }

        /* drawing needs these */
        protected internal int x()
        {
            return (int)x_Renamed_Field;
        }

        protected internal int y()
        {
            return (int)(-y_Renamed_Field);
        }

        public virtual Point add(Point p)
        {
            x_Renamed_Field += p.x_Renamed_Field; y_Renamed_Field += p.y_Renamed_Field; return this;
        }

        public virtual Point sub(Point p)
        {
            x_Renamed_Field -= p.x_Renamed_Field; y_Renamed_Field -= p.y_Renamed_Field; return this;
        }

        public virtual Point neg()
        {
            x_Renamed_Field = -x_Renamed_Field; y_Renamed_Field = -y_Renamed_Field; return this;
        }

        public virtual Point mul(double m)
        {
            x_Renamed_Field *= m; y_Renamed_Field *= m; return this;
        }

        public virtual Point div(double m)
        {
            x_Renamed_Field /= m; y_Renamed_Field /= m; return this;
        }

        public double len2()
        {
            return x_Renamed_Field * x_Renamed_Field + y_Renamed_Field * y_Renamed_Field;
        }

        public double len()
        {
            if (stats_Renamed_Field)
                sqrts++;
            return System.Math.Sqrt(len2());
        }

        public double dist2(Point p)
        {
            double dx = x_Renamed_Field - p.x_Renamed_Field, dy = y_Renamed_Field - p.y_Renamed_Field;
            return dx * dx + dy * dy;
        }

        public double dist(Point p)
        {
            if (stats_Renamed_Field)
                sqrts++;
            return System.Math.Sqrt(dist2(p));
        }

        public virtual Point normalize()
        {
            return div(len());
        }

        static public Point add(Point p, Point q)
        {
            return new Point(p.x_Renamed_Field + q.x_Renamed_Field, p.y_Renamed_Field + q.y_Renamed_Field);
        }

        static public Point sub(Point p, Point q)
        {
            return new Point(p.x_Renamed_Field - q.x_Renamed_Field, p.y_Renamed_Field - q.y_Renamed_Field);
        }

        static public Point lincomb(Point p, double beta, Point q)
        {
            return new Point(p.x_Renamed_Field + beta * q.x_Renamed_Field, p.y_Renamed_Field + beta * q.y_Renamed_Field);
        }

        static public Point lincomb(double alpha, Point p, double beta, Point q)
        {
            return new Point(alpha * p.x_Renamed_Field + beta * q.x_Renamed_Field, alpha * p.y_Renamed_Field + beta * q.y_Renamed_Field);
        }

        public virtual Point perp()
        {
            double tmp = -y_Renamed_Field; y_Renamed_Field = x_Renamed_Field; x_Renamed_Field = tmp; return this;
        }

        public virtual double dot(Point p)
        {
            return x_Renamed_Field * p.x_Renamed_Field + y_Renamed_Field * p.y_Renamed_Field;
        }

        public virtual double perpdot(Point p)
        {
            return x_Renamed_Field * p.y_Renamed_Field - y_Renamed_Field * p.x_Renamed_Field;
        }

        public virtual double dotperp(Point p)
        {
            return (-x_Renamed_Field) * p.y_Renamed_Field + y_Renamed_Field * p.x_Renamed_Field;
        }

        /// <summary>operate on vector from p to q </summary>
        public virtual double dot(Point p, Point q)
        {
            return x_Renamed_Field * (q.x_Renamed_Field - p.x_Renamed_Field) + y_Renamed_Field * (q.y_Renamed_Field - p.y_Renamed_Field);
        }

        public virtual double perpdot(Point p, Point q)
        {
            return x_Renamed_Field * (q.y_Renamed_Field - p.y_Renamed_Field) - y_Renamed_Field * (q.x_Renamed_Field - p.x_Renamed_Field);
        }

        public virtual double dotperp(Point p, Point q)
        {
            return y_Renamed_Field * (q.x_Renamed_Field - p.x_Renamed_Field) - x_Renamed_Field * (q.y_Renamed_Field - p.y_Renamed_Field);
        }

        public virtual Homog meet(Point p)
        {
            return new Homog(x_Renamed_Field * p.y_Renamed_Field - y_Renamed_Field * p.x_Renamed_Field, y_Renamed_Field - p.y_Renamed_Field, p.x_Renamed_Field - x_Renamed_Field);
        }

        public bool equals(Point q)
        {
            return (x_Renamed_Field == q.x_Renamed_Field) && (y_Renamed_Field == q.y_Renamed_Field);
        }

        public virtual bool less(Point q)
        {
            /* true if p.x < q.x, break ties on y */
            double diff = x_Renamed_Field - q.x_Renamed_Field;
            if (stats_Renamed_Field)
                comparisons++;
            return (diff < 0) || ((diff == 0) && (y_Renamed_Field < q.y_Renamed_Field));
        }

        public virtual bool greater(Point q)
        {
            /* true if p.y < q.y */
            double diff = x_Renamed_Field - q.x_Renamed_Field;
            if (stats_Renamed_Field)
                comparisons++;
            return (diff > 0) || ((diff == 0) && (y_Renamed_Field > q.y_Renamed_Field));
        }

        public virtual bool y_less(Point q)
        {
            /* true if p.y < q.y, break ties on x */
            double diff = y_Renamed_Field - q.y_Renamed_Field;
            if (stats_Renamed_Field)
                comparisons++;
            return (diff < 0) || ((diff == 0) && (x_Renamed_Field < q.x_Renamed_Field));
        }

        static public bool segmentIntersect(Point a, Point b, Point c, Point d)
        {
            double abc = det3(a, b, c);
            double abd = det3(a, b, d);
            if (abc * abd > 0)
                return false;
            if (ON(abc) && ON(abd))
                /* all four collinear! */
                return (dot(c, a, b) <= 0.0) || (dot(d, a, b) <= 0.0) || (dot(a, c, d) <= 0.0);
            return det3(c, d, a) * det3(c, d, b) <= 0;
        }

        static private double det3(Point p, Point q, Point r)
        {
            if (stats_Renamed_Field)
                sidednesstests++;
            return (q.x_Renamed_Field - p.x_Renamed_Field) * (r.y_Renamed_Field - p.y_Renamed_Field) - (q.y_Renamed_Field - p.y_Renamed_Field) * (r.x_Renamed_Field - p.x_Renamed_Field);
        }

        static private bool LEFT(double test)
        {
            return test > 0.0;
        }

        static private bool ON(double test)
        {
            return test == 0.0;
        }

        static private bool RIGHT(double test)
        {
            return test < 0.0;
        }

        static private double dot(Point o, Point p, Point q)
        {
            if (stats_Renamed_Field)
                onlinetests++;
            return (p.x_Renamed_Field - o.x_Renamed_Field) * (q.x_Renamed_Field - o.x_Renamed_Field) + (p.y_Renamed_Field - o.y_Renamed_Field) * (q.y_Renamed_Field - o.y_Renamed_Field);
        }

        static public bool leftORinside(Point p, Point q, Point r)
        {
            double test = det3(p, q, r);
            return (LEFT(test) || (ON(test) && dot(r, p, q) < 0.0));
        }

        static public bool leftORonseg(Point p, Point q, Point r)
        {
            double test = det3(p, q, r);
            return (LEFT(test) || (ON(test) && dot(r, p, q) <= 0.0));
        }

        static public bool right(Point p, Point q, Point r)
        {
            return RIGHT(det3(p, q, r));
        }

        static public bool left(Point p, Point q, Point r)
        {
            return LEFT(det3(p, q, r));
        }

        static public bool colinear(Point p, Point q, Point r)
        {
            return ON(det3(p, q, r));
        }

        private const bool stats_Renamed_Field = true;
        //private static long elapsed;
        private static long comparisons;
        private static long sidednesstests;
        private static long onlinetests;
        private static long sqrts;
        private static long starttime;

        public static System.String stats()
        {
            long elapsed = (System.DateTime.Now.Ticks - 621355968000000000) / 10000 - starttime;
            return "\nElapsed Time (ms)" + System.Convert.ToString(elapsed) + "\n     Comparisons " + comparisons + "\n Sidedness Tests " + sidednesstests + "\n    Online Tests " + onlinetests + "\n    Square roots " + sqrts;
        }

        public static void reset()
        {
            comparisons = sidednesstests = onlinetests = sqrts = 0;
            starttime = (System.DateTime.Now.Ticks - 621355968000000000) / 10000;
        }

        public override System.String ToString()
        {
            return " (" + x_Renamed_Field + ", " + y_Renamed_Field + ")  ";
        }

        //UPGRADE_NOTE: ref keyword was added to struct-type parameters. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1303'"
        public virtual System.Drawing.Point toJavaPoint(ref System.Drawing.Point p)
        {
            p.X = x(); p.Y = y(); return p;
        }

        public void drawVector(System.Drawing.Graphics g, Point orig)
        {
            orig.drawDot(g);
            g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), orig.x(), orig.y(), orig.x() + x(), orig.y() + y());
        }

        public void drawCirc(System.Drawing.Graphics g, int rad)
        {
            g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), x() - rad, y() - rad, rad * 2, rad * 2);
        }

        public void drawDot(System.Drawing.Graphics g, int rad)
        {
            g.FillEllipse(SupportClass.GraphicsManager.manager.GetPaint(g), x() - rad, y() - rad, rad * 2, rad * 2);
        }

        public void drawSquare(System.Drawing.Graphics g, int rad)
        {
            g.FillRectangle(SupportClass.GraphicsManager.manager.GetPaint(g), x() - rad, y() - rad, rad * 2, rad * 2);
        }

        public void drawBox(System.Drawing.Graphics g, int rad)
        {
            g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), x() - rad, y() - rad, rad * 2, rad * 2);
        }

        private const int RADIUS = 2;
        public void drawCirc(System.Drawing.Graphics g)
        {
            drawCirc(g, RADIUS);
        }

        public void drawDot(System.Drawing.Graphics g)
        {
            drawDot(g, RADIUS);
        }

        public void drawSquare(System.Drawing.Graphics g)
        {
            drawSquare(g, RADIUS);
        }

        public void drawBox(System.Drawing.Graphics g)
        {
            drawBox(g, RADIUS);
        }

        public void draw(System.Drawing.Graphics g)
        {
            drawCirc(g, RADIUS);
        }

        public void draw(System.Drawing.Graphics g, bool label)
        {
            draw(g);
            if (label)
            {
                //UPGRADE_TODO: Method 'java.awt.Graphics.drawString' was converted to 'System.Drawing.Graphics.DrawString' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtGraphicsdrawString_javalangString_int_int'"
                g.DrawString(ToString(), SupportClass.GraphicsManager.manager.GetFont(g), SupportClass.GraphicsManager.manager.GetBrush(g), x(), y() - SupportClass.GraphicsManager.manager.GetFont(g).GetHeight());
            }
        }
    }
}
