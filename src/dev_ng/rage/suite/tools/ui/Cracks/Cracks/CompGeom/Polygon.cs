using System;

namespace CompGeom
{
    public class Polygon : PointList
    {
        public Polygon(int n) : base(n)
        {
        }

        public Polygon(PointList pl) : base(pl)
        {
        }

        public override void draw(System.Drawing.Graphics g, bool label)
        {
            base.draw(g, label);
            drawPolygon(g, number());
        }
    }
}
