using System;
namespace CompGeom
{
	
	public class TriangPoly:Anim2D
	{
		private PointList sp;
		private int[][] ind;
		private double[][] wt;
		
		public const double INFINITY = 1.0e30;
		public const int NONE = 0;
		
		public TriangPoly(PointList pl)
		{
			sp = pl;
			wt = new double[sp.number()][];
			for (int i = 0; i < sp.number(); i++)
			{
				wt[i] = new double[sp.number()];
			}
			ind = new int[sp.number()][];
			for (int i2 = 0; i2 < sp.number(); i2++)
			{
				ind[i2] = new int[sp.number()];
			}
			wt = new double[sp.number()][];
			for (int i3 = 0; i3 < sp.number(); i3++)
			{
				wt[i3] = new double[sp.number()];
			}
		}
		
		public virtual void  setTriangle(int i, int j, int k)
		{
			/** i<j<k */
			ind[i][k] = j;
			if (animating() && (j != NONE))
			{
				//      animGraphics.drawLine(sp.p(i).x(), sp.p(i).y(), sp.p(j).x(), sp.p(j).y());
				//      animGraphics.drawLine(sp.p(j).x(), sp.p(j).y(), sp.p(k).x(), sp.p(k).y());
				animGraphics.DrawLine(SupportClass.GraphicsManager.manager.GetPen(animGraphics), sp.p(i).x(), sp.p(i).y(), sp.p(k).x(), sp.p(k).y());
			}
		}
		
		public virtual void  drawSegs(int i, int j, int ii, int jj)
		{
			if (animating())
			{
				animGraphics.DrawLine(SupportClass.GraphicsManager.manager.GetPen(animGraphics), sp.p(i).x(), sp.p(i).y(), sp.p(j).x(), sp.p(j).y());
				animGraphics.DrawLine(SupportClass.GraphicsManager.manager.GetPen(animGraphics), sp.p(ii).x(), sp.p(ii).y(), sp.p(jj).x(), sp.p(jj).y());
			}
		}
		public virtual void  setWeight(int i, int l, double w)
		{
			wt[i][l] = w;
		}
		public virtual double weight(int i, int l)
		{
			return wt[i][l];
		}
		
		private void  drawhelper(System.Drawing.Graphics g, bool label, int i, int j)
		{
			if (label)
			{
				//UPGRADE_TODO: Method 'java.awt.Graphics.drawString' was converted to 'System.Drawing.Graphics.DrawString' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtGraphicsdrawString_javalangString_int_int'"
				g.DrawString(wt[i][j - i].ToString(), SupportClass.GraphicsManager.manager.GetFont(g), SupportClass.GraphicsManager.manager.GetBrush(g), (sp.p(i).x() + sp.p(j).x()) / 2, (sp.p(i).y() + sp.p(j).y()) / 2 - SupportClass.GraphicsManager.manager.GetFont(g).GetHeight());
			}
			g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), sp.p(i).x(), sp.p(i).y(), sp.p(j).x(), sp.p(j).y());
			if (ind[i][j] != NONE)
			{
				drawhelper(g, label, i, ind[i][j]);
				drawhelper(g, label, ind[i][j], j);
			}
		}
		
		public override void  draw(System.Drawing.Graphics g, bool label)
		{
			//    sp.draw(g, label);
			if (ind[0][sp.number() - 1] != NONE)
				drawhelper(g, label, 0, sp.number() - 1);
		}
	}
}