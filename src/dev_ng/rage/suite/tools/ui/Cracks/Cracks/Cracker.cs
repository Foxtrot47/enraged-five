using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Cracks
{
    public partial class Cracker : UserControl
    {
        public Cracker()
        {
            //Initialize the control's components
            InitializeComponent();

            //Add event handlers for input and painting            
            MouseWheel += new MouseEventHandler(Cracker_MouseWheel);

        }

        public bool OpenReferenceFiles(string filename)
        {
            //Attempt to load the specified bitmap
            Bitmap bitmap = DevIL.DevIL.LoadBitmap(filename);
            if (bitmap == null)
                return false;

            //Give the user the opportunity to save
            if (!Exit())
                return false;

            //Forget old data and adopt the new bitmap
            Close();
            BackgroundImage = bitmap;
            originalImageSize = bitmap.Size;

            //Derive a text filename from the bitmap filename
            int dot = filename.LastIndexOf('.');
            file = (dot == -1 ? filename : filename.Substring(0, dot)) + ".txt";

            OpenConvex(file);

            if (OnOpen != null)
                OnOpen(this, new OpenEventArgs(store));

            //Enable the control and resize it to fit the bitmap
            ClientSize = bitmap.Size;
            Enabled = true;

            Invalidate(true);
            return true;
        }

        private bool OpenConvex(string file)
        {
            try
            {
                //Attempt to open the text file
                using (TextReader tr = new StreamReader(file))
                {
                    //Ignore the number of cracks
                    string line = tr.ReadLine();
                    while ((line = tr.ReadLine()) != null)
                    {
                        //Create a new crack and read the number of vertices
                        Crack crack = new Crack();
                        int count = int.Parse(line.Trim('\t'));
                        for (int index = 0; index != count; ++index)
                        {
                            //Read the location of this vertex
                            line = tr.ReadLine();
                            //int x = (int)Math.Round(float.Parse(line.Substring(0, line.IndexOf('\t'))) * (BackgroundImage.Size.Width - 1) / 511);
                            //int y = (int)Math.Round(float.Parse(line.Substring(1 + line.IndexOf('\t'))) * (BackgroundImage.Size.Height - 1) / 511);
                            float x = float.Parse(line.Substring(0, line.IndexOf('\t'))) * (BackgroundImage.Size.Width - 1) / 511;
                            float y = float.Parse(line.Substring(1 + line.IndexOf('\t'))) * (BackgroundImage.Size.Height - 1) / 511;

                            //Search for an existing vertex at the location
                            Vertex v = null;
                            if (cracks.Count != 0)
                            {
                                for (int i = 0, j = 0; ; )
                                {
                                    //Determine if vertices remain in this crack
                                    if (j != cracks[i].Count)
                                    {
                                        //Determine if this vertex is at the location of interest
                                        if (cracks[i][j].X == x && cracks[i][j].Y == y)
                                        {
                                            //Adopt the vertex
                                            v = cracks[i][j];
                                            break;
                                        }
                                        //Proceed to the next vertex
                                        ++j;
                                        continue;
                                    }
                                    //Proceed to the first vertex in the next crack, if there is one
                                    j = 0;
                                    if (++i == cracks.Count)
                                        break;
                                }
                            }
                            //If there is no vertex at this location, create a new one
                            if (v == null)
                                v = new Vertex(x, y);

                            //Add the vertex to the crack
                            v.AddTo(crack);
                        }

                        //Read the number of convex polygons
                        line = tr.ReadLine();
                        int polygons = int.Parse(line.Trim('\t'));
                        for (int polygon = 0; polygon != polygons; ++polygon)
                        {
                            //Read the number of indices
                            line = tr.ReadLine();
                            int indices = int.Parse(line.Trim('\t'));
                            for (int index = 0; index != indices; ++index)
                                tr.ReadLine();
                        }

                        //Store the crack
                        if (!Counterclockwise(crack))
                            crack.Reverse();
                        crack.AddTo(cracks);
                    }
                }
                return true;
            }
            catch
            {
                cracks = new Cracks();
                return false;
            }
        }

        public event OpenEventHandler OnOpen;

        public bool Save()
        {
            if (SaveConvex())
            {
                SaveReferenceLines();
            }
            else
            {
                return false;
            }

            return true;
        }

        private bool SaveConvex()
        {
            try
            {
                //Decompose the polygons
                List<List<List<int>>> polygons = Decompose();

                //Attempt to open the text file
                using (TextWriter tw = new StreamWriter(file))
                {
                    //Write the number of cracks
                    tw.WriteLine(cracks.Count.ToString());
                    for (int i = 0; i != cracks.Count; ++i)
                    {
                        Crack crack = cracks[i];

                        //Write the number of vertices
                        int count = crack.Count;
                        tw.WriteLine(count.ToString());
                        for (int index = count; index != 0; )
                        {
                            //Write the location of this vertex
                            Vertex v = crack[--index];
                            //tw.WriteLine(vertex.location.X.ToString() + "\t" + vertex.location.Y.ToString());
                            tw.WriteLine(((float)v.X * 511 / (BackgroundImage.Size.Width - 1)).ToString() + "\t" + ((float)v.Y * 511 / (BackgroundImage.Size.Height - 1)).ToString());
                        }

                        //Write the number of convex polygons decomposing this crack
                        List<List<int>> complex = polygons[i];
                        tw.WriteLine(complex.Count.ToString());
                        foreach (List<int> convex in complex)
                        {
                            //Write the number of indices in this polygon
                            count = convex.Count;
                            tw.WriteLine(count.ToString());
                            for (int index = count; index != 0; )
                            {
                                //Write the index of this vertex
                                tw.WriteLine((crack.Count - convex[--index] - 1).ToString());
                            }
                        }
                    }

                    //Indicate that the current action is saved
                    actions.Save();
                }
                return true;
            }
            catch
            {
                MessageBox.Show(this, "Failed saving \"" + file + "\".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Saved()
        {
            //Determine whether the current action is saved
            return actions.Saved();
        }

        public bool Exit()
        {
            //Determine whether the current action is saved
            if (!Saved())
            {
                //Ask the user to save
                DialogResult dr = MessageBox.Show(file + " has changed.\r\n\r\nDo you want to save the changes?", "Crackle", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                switch (dr)
                {
                case DialogResult.Yes:
                    return Save();
                case DialogResult.Cancel:
                    return false;
                }
            }
            return true;
        }

        public void Close()
        {
            file = null;

            actions = new Actions();
            cracks = new Cracks();

            BackgroundImage = null;

            crack = null;

            vertex = null;
            multiSelectionList.Clear();
            if (OnSplitable != null)
                OnSplitable(this, new SplitableEventArgs(false));
            Invalidate(true);

            Zoom = 1.0F;
        }

        public bool Redo()
        {
            Invalidate(true);
            return actions.Redo(this);
        }

        public bool Undo()
        {
            Invalidate(true);
            return actions.Undo(this);
        }

        public event EventHandler OnDone;
        public event UndoneEventHandler OnUndone;
        public event SplitableEventHandler OnSplitable;

        public void Do(Action action)
        {
            //Add an action and signal its addition
            actions.Add(action);
            if (OnDone != null)
                OnDone(this, new EventArgs());
        }

        public void StoreDecomposition(bool s)
        {
            store = s;
        }

        public void ShowDecomposition(bool s)
        {
            show = s;
            Invalidate(true);
        }

        public void ShowStats(bool s)
        {
            showStats = s;
        }

        public float Zoom
        {
            get
            {
                return zoom;
            }

            set
            {
                zoom = value;

                if (zoom < ZoomMin)
                {
                    zoom = ZoomMin;
                }

                if (zoom > ZoomMax)
                {
                    zoom = ZoomMax;
                }

                if (BackgroundImage != null)
                {
                    ClientSize = new Size((int)Math.Round(BackgroundImage.Width * zoom), (int)Math.Round(BackgroundImage.Height * zoom));
                    BackgroundImageLayout = ImageLayout.Stretch;
                    Invalidate(true);
                }
            }
        }

        public PointF AddZoom(PointF p)
        {
            //return new Point((int)Math.Round(p.X * Zoom), (int)Math.Round(p.Y * Zoom));
            return new PointF(p.X * Zoom, p.Y * Zoom);
        }

        public PointF RemoveZoom(PointF p)
        {
            //return new Point((int)Math.Round(p.X / Zoom), (int)Math.Round(p.Y / Zoom));
            return new PointF(p.X / Zoom, p.Y / Zoom);
        }

        protected override bool IsInputKey(Keys keyData)
        {
            return true;
        }

        private Crack SelectCrack(PointF location)
        {
            if (crack != null && crack.Contains(location))
                return crack;
            foreach (Crack c in cracks)
            {
                if (c == crack)
                    continue;
                if (c.Contains(location))
                    return c;
            }
            return null;
        }

        private Vertex SelectVertex(PointF location)
        {
            //Find the vertex nearest this location
            Vertex vert = null;
            //int square = Vertex.radius * Vertex.radius;
            float square = Vertex.radius * Vertex.radius / Zoom / Zoom;
            foreach (Crack c in cracks)
            {
                foreach (Vertex v in c)
                {
                    //Ignore vertices in the current crack
                    if (mode == Mode.insert && crack.Contains(v))
                        continue;

                    //Squared distance is the dot product of the difference with itself
                    //int x = location.X - v.X, y = location.Y - v.Y;
                    //int s = x * x + y * y;
                    float x = location.X - v.X, y = location.Y - v.Y;
                    float s = x * x + y * y;
                    if (s <= square)
                    {
                        //Record the nearest vertex and its squared distance
                        square = s;
                        vert = v;
                    }
                }
            }
            //Indicate whether a vertex was found
            return vert;
        }
        private List<Vertex> SelectVertices(PointF start, PointF end)
        {
            //Find the vertex nearest this location
            List<Vertex> verts = new List<Vertex>();
            //int square = Vertex.radius * Vertex.radius;
            float square = Vertex.radius * Vertex.radius / Zoom / Zoom;
            foreach (Crack c in cracks)
            {
                foreach (Vertex v in c)
                {
                    //Ignore vertices in the current crack
                    if (mode == Mode.insert && crack.Contains(v))
                        continue;
//                     System.Console.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}\n", v.X, v.Y, start.X, start.Y, end.X, end.Y);
                    if(v.X>start.X && v.Y>start.Y && v.X<end.X && v.Y<end.Y)
                    {
                        verts.Add(v);
                    }
                }
            }
            //Indicate whether a vertex was found
            return verts;
        }

        private void Cracker_MouseWheel(object sender, MouseEventArgs e)
        {
            if (ModifierKeys == Keys.Shift)
            {
                if (multiSelectionList.Count > 0
                    || null != vertex)
                {
                    ScaleSelectedVertices ssv = new ScaleSelectedVertices(multiSelectionList);

                    int halfWidth = BackgroundImage.Width / 2;
                    int halfHeight = BackgroundImage.Height / 2;

                    Point center = new Point(halfWidth, halfHeight);

                    float scale = 1.05f;
                    if (e.Delta < 0)
                        scale = 1.0f / scale;

                    bool doSsv = true;
                    for (int i = 0; i < multiSelectionList.Count; ++i)
                    {
                        Vertex v = multiSelectionList[i];

                        PointF newPos = new PointF(v.X - center.X, v.Y - center.Y);

                        newPos.X *= scale;
                        newPos.Y *= scale;

                        newPos.X += center.X;
                        newPos.Y += center.Y;

                        if (false == (newPos.X >= 0.0F && newPos.X < (float)(BackgroundImage.Width - 1))
                            || false == (newPos.Y >= 0.0F && newPos.Y < (float)(BackgroundImage.Height - 1)))
                        {
                            List<Vertex> multiSelectionListBackup = new List<Vertex>(multiSelectionList);
                            ssv.Undo(this);
                            multiSelectionList = multiSelectionListBackup;
                            doSsv = false;
                            break;
                        }
                        else
                        {
                            // the new point is good, set the values
                            v.Location = newPos;
                        }
                    }

                    if (doSsv)
                    {
                        Do(ssv);

                        // rebuild all bounds
                        foreach (Crack crack in cracks)
                        {
                            crack.Unbound();
                        }

                        Invalidate(true);
                    }
                }
            }
            else
            {
                if (e.Delta < 0)
                {
                    Zoom = Zoom - ZoomStep;
                }
                else
                {
                    Zoom = Zoom + ZoomStep;
                }
            }
        }

        public void WeldVertices()
        {
            if (multiSelectionList.Count > 0
               || null != vertex)
            {
                WeldSelectedVertices wsv = new WeldSelectedVertices(multiSelectionList);

                Vertex v = multiSelectionList[0];
                PointF center = new PointF(v.X, v.Y);
                for (int i = 1; i < multiSelectionList.Count; ++i)
                {
                    v = multiSelectionList[i];
                    center = center + new SizeF(v.X, v.Y);
                }
                center.X /= multiSelectionList.Count;
                center.Y /= multiSelectionList.Count;

                bool doWsv = true;
                Vertex newVert = new Vertex(center);
                for (int i = 0; i < multiSelectionList.Count; ++i)
                {
                    v = multiSelectionList[i];
                    foreach(Crack c in v)
                    {
                        newVert.ReplaceIn(c, v);
                    }
                }

                if (doWsv)
                {
                    Do(wsv);

                    // rebuild all bounds
                    foreach (Crack crack in cracks)
                    {
                        crack.Unbound();
                    }

                    Invalidate(true);
                }
            }
        }

        private static void DrawLine(Graphics graphics, Pen pen, PointF first, PointF second)
        {
            graphics.DrawLine(pen, first.X, first.Y, second.X, second.Y);
        }

        private void InvalidatePoint(PointF first)
        {
            InvalidateRectangle(first.X, first.Y, first.X, first.Y);
        }

        private void InvalidateLine(PointF first, PointF second)
        {
            InvalidateRectangle(
                Math.Min(first.X, second.X),
                Math.Min(first.Y, second.Y),
                Math.Max(first.X, second.X),
                Math.Max(first.Y, second.Y)
            );
        }

        private void InvalidateTriangle(PointF first, PointF second, PointF third)
        {
            InvalidateRectangle(
                Math.Min(first.X, Math.Min(second.X, third.X)),
                Math.Min(first.Y, Math.Min(second.Y, third.Y)),
                Math.Max(first.X, Math.Max(second.X, third.X)),
                Math.Max(first.Y, Math.Max(second.Y, third.Y))
            );
        }

        private void InvalidateRectangle(float left, float top, float right, float bottom)
        {
            Invalidate(
                new Rectangle(
                    (int)Math.Round(Zoom * left - Vertex.radius),
                    (int)Math.Round(Zoom * top - Vertex.radius),
                    (int)Math.Round(Zoom * (right - left) + 2 * Vertex.radius + 1),
                    (int)Math.Round(Zoom * (bottom - top) + 2 * Vertex.radius + 1)
                ),
                true
            );
        }

        private void InvalidateBounds(Crack crack)
        {
            RectangleF rectangle = crack.Bounds;

            float left = (rectangle.Left + Vertex.radius) * Zoom - Vertex.radius;
            float top = (rectangle.Top + Vertex.radius) * Zoom - Vertex.radius;

            Invalidate(
                new Rectangle(
                    (int)Math.Round(left),
                    (int)Math.Round(top),
                    (int)Math.Round((rectangle.Right - Vertex.radius) * Zoom + Vertex.radius - left),
                    (int)Math.Round((rectangle.Bottom - Vertex.radius) * Zoom + Vertex.radius - top)
                ),
                true
            );
        }

        private static bool Counterclockwise(Crack crack)
        {
            //Determine the winding of this crack
            //int twiceArea = 0;
            float twiceArea = 0;
            int count = crack.Count;
            for (int next = 0, last = count - 1; next != count; last = next++)
            {
                //Add the cross product of this vertex and the next
                Vertex first = crack[last];
                Vertex second = crack[next];
                twiceArea += first.X * second.Y - first.Y * second.X;
            }
            return twiceArea < 0;
        }

        private List<List<List<int>>> Decompose()
        {
            List<List<List<int>>> polygons = new List<List<List<int>>>();
            foreach (Crack crack in cracks)
            {
                CompGeom.PointList pl = PointListFrom(crack, 1);
                CompGeom.DecompPoly dp = Compute(pl);
                List<List<int>> complex = dp.decompose();
                polygons.Add(complex);
                foreach (List<int> convex in complex)
                {
                    for (int i = 0; i != convex.Count; ++i)
                    {
                        int j = (i + 1) % convex.Count;
                        int k = (j + 1) % convex.Count;
                        Vertex a = crack[convex[i]];
                        Vertex b = crack[convex[j]];
                        Vertex c = crack[convex[k]];
                        PointF u = a.Location - new SizeF(b.Location);
                        PointF v = c.Location - new SizeF(b.Location);
                        //int cross = u.X * v.Y - u.Y * v.X;
                        float cross = u.X * v.Y - u.Y * v.X;
                        if (cross < 0)
                            throw new Exception("Nonconvex polygon detected.");
                    }
                }
            }
            return polygons;
        }

        private static CompGeom.PointList PointListFrom(Crack crack, float zoom)
        {
            //Wind this counter-clockwise crack
            CompGeom.PointList pl = new CompGeom.PointList(crack.Count);
            foreach (Vertex v in crack)
                pl.add(new CompGeom.Point(v.X * zoom, -v.Y * zoom));
            return pl;
        }

        //http://www.cs.unc.edu/~snoeyink/demos/convdecomp/MCDDemo.html
        private static CompGeom.DecompPoly Compute(CompGeom.PointList pl)
        {
            //int type;
            int i, k, n = pl.number();
            CompGeom.DecompPoly dp = new CompGeom.DecompPoly(pl);
            //animate(dp);
            dp.init();

            for (int l = 3; l < n; l++)
            {
                for (i = dp.reflexIter(); i + l < n; i = dp.reflexNext(i))
                    if (dp.visible(i, k = i + l))
                    {
                        dp.initPairs(i, k);
                        if (dp.reflex(k))
                            for (int j = i + 1; j < k; j++)
                                dp.typeA(i, j, k);
                        else
                        {
                            for (int j = dp.reflexIter(i + 1); j < k - 1; j = dp.reflexNext(j))
                                dp.typeA(i, j, k);
                            dp.typeA(i, k - 1, k); // do this, reflex or not.
                        }
                    }

                for (k = dp.reflexIter(l); k < n; k = dp.reflexNext(k))
                    if ((!dp.reflex(i = k - l)) && dp.visible(i, k))
                    {
                        dp.initPairs(i, k);
                        dp.typeB(i, i + 1, k); // do this, reflex or not.
                        for (int j = dp.reflexIter(i + 2); j < k; j = dp.reflexNext(j))
                            dp.typeB(i, j, k);
                    }
            }
            dp.guard = 3 * n;
            dp.recoverSolution(0, n - 1);
            return dp;
        }

        private class Vertex : IEnumerable
        {
            public PointF Location
            {
                get
                {
                    return location;
                }

                set
                {
                    location = value;
                    foreach (Crack crack in this)
                        crack.Unbound();
                }
            }

            public float X
            {
                get
                {
                    return location.X;
                }
            }

            public float Y
            {
                get
                {
                    return location.Y;
                }
            }

            public Vertex(float x, float y)
            {
                location = new PointF(x, y);
            }

            public Vertex(PointF point)
            {
                location = point;
            }

            public void SetX(float x)
            {
                location.X = x;
            }

            public void SetY(float y)
            {
                location.Y = y;
            }

            ~Vertex()
            {
                for (int count = containers.Count; containers.Count != 0; )
                {
                    RemoveFrom(containers[--count]);
                }
            }

            public Crack GetContainer(int index)
            {
                return containers[index];
            }

            public int ContainerCount
            {
                get
                {
                    return containers.Count;
                }
            }

            public void DrawNormal(Graphics graphics, float zoom)
            {
                Draw(graphics, normalBrush, zoom);
            }

            public void DrawHighlight(Graphics graphics, float zoom)
            {
                Draw(graphics, highlightBrush, zoom);
            }

            public void DrawUsingThisBrush(Graphics graphics, Brush brush, float zoom)
            {
                Draw(graphics, brush, zoom);
            }

            public void Draw(Graphics graphics, Brush brush, float zoom)
            {
                PointF point = new PointF(location.X * zoom - radius, location.Y * zoom - radius);
                graphics.FillEllipse(brush, point.X, point.Y, 2 * radius, 2 * radius);
                graphics.DrawEllipse(pen, point.X, point.Y, 2 * radius, 2 * radius);
            }

            public bool AddTo(Crack crack)
            {
                if (!crack.Add(this))
                    return false;
                containers.Add(crack);
                return true;
            }

            public bool AddTo(Crack crack, int index)
            {
                if (!crack.Add(this, index))
                    return false;
                containers.Add(crack);
                return true;
            }

            public bool ReplaceIn(Crack crack, Vertex oldVert)
            {
                if (!crack.Replace(oldVert, this))
                    return false;
                if(!containers.Contains(crack))
                    containers.Add(crack);
                return true;
            }

            public void RemoveFrom(Crack crack)
            {
                if (containers.Remove(crack))
                    crack.Remove(this);
            }

            public bool IsContainedIn(Crack container)
            {
                return containers.Contains(container);
            }

            public void Move(PointF e, PointF bgSize, bool dragging, float Zoom, Cracker c)
            {
                foreach (Crack crack in this)
                    c.InvalidateBounds(crack);
                //Point location = new Point(Math.Max(0, Math.Min(e.X, ClientSize.Width - 1)), Math.Max(0, Math.Min(e.Y, ClientSize.Height - 1)));
                //                Point location = RemoveZoom(new Point(Math.Max(0, Math.Min(e.X, BackgroundImage.Width - 1)), Math.Max(0, Math.Min(e.Y, BackgroundImage.Height - 1))));
                PointF location = new PointF(e.X, e.Y);
                location = new PointF(Math.Max(0, Math.Min(location.X, bgSize.X - 1)), Math.Max(0, Math.Min(location.Y, bgSize.Y - 1)));
                if (!dragging)
                {
                    //Determine if this location is far enough away
                    //int x = location.X - vertex.X;
                    //int y = location.Y - vertex.Y;
                    float x = location.X - this.X;
                    float y = location.Y - this.Y;
                    if (x * x + y * y < Vertex.radius * Vertex.radius / Zoom / Zoom)
                        return;

                    //Start dragging
                    c.Do(new MoveVertex(this));
                    dragging = true;
                }
                //Attempt to snap to a nearby vertex
                this.Location = location;
                if (c.mode == Mode.insert)
                {
                    Vertex selection = c.SelectVertex(location);
                    if (selection != null)
                        this.Location = selection.Location;
                }
                foreach (Crack crack in this)
                    c.InvalidateBounds(crack);
            }

            public IEnumerator GetEnumerator()
            {
                return new Enumerator(this);
            }

            public class Enumerator : IEnumerator
            {
                public Enumerator(Vertex v)
                {
                    vertexList = v;
                }

                public object Current
                {
                    get
                    {
                        return vertexList.containers[index];
                    }
                }

                public void Reset()
                {
                    index = -1;
                }

                public bool MoveNext()
                {
                    if (index == vertexList.containers.Count - 1)
                        return false;
                    ++index;
                    return true;
                }

                private Vertex vertexList;
                private int index = -1;
            }

            private PointF location;
            private List<Crack> containers = new List<Crack>();

            public static float radius = 4.0F;

            public static Pen pen = new Pen(Color.Black, 1);
            public static Brush normalBrush = new SolidBrush(Color.FromArgb(255, 0, 255, 0));
            public static Brush highlightBrush = new SolidBrush(Color.FromArgb(255, 255, 255, 0));
        }

        private class Crack : IEnumerable
        {
            ~Crack()
            {
                for (int count = vertices.Count; count != 0; )
                    vertices[--count].RemoveFrom(this);
            }

            public RectangleF Bounds
            {
                get
                {
                    if (!bounded)
                        Bound();
                    return bounds;
                }
            }

            public int Count
            {
                get
                {
                    return vertices.Count;
                }
            }

            public Vertex this[int index]
            {
                get
                {
                    return vertices[index];
                }
            }

            public void DrawClosed(Graphics graphics, float zoom, Pen pen, bool drawVertices)
            {
                if (null == pen)
                {
                    DrawClosed(graphics, closedPen, zoom, drawVertices);
                }
                else
                {
                    DrawClosed(graphics, pen, zoom, drawVertices);
                }
            }

            public void DrawClosed(Graphics graphics, Pen pen, float zoom, bool drawVertices)
            {
                if (!bounded)
                    Bound();
                //if (graphics.ClipBounds.Right < bounds.Left || bounds.Right < graphics.ClipBounds.Left || graphics.ClipBounds.Bottom < bounds.Top || bounds.Bottom < graphics.ClipBounds.Top)
                if (graphics.ClipBounds.Right / zoom < bounds.Left || bounds.Right < graphics.ClipBounds.Left / zoom || graphics.ClipBounds.Bottom / zoom < bounds.Top || bounds.Bottom < graphics.ClipBounds.Top / zoom)
                    return;

                if (vertices.Count != 0)
                {
                    //Draw all the lines and vertices in this crack
                    for (int i = 0; i != vertices.Count; ++i)
                    {
                        Vertex v = vertices[i];
                        //DrawLine(graphics, pen, vertex.Location, vertices[(i + 1) % vertices.Count].Location);
                        Vertex next = vertices[(i + 1) % vertices.Count];
                        //DrawLine(graphics, pen, new Point((int)Math.Round(v.X * zoom), (int)Math.Round(v.Y * zoom)), new Point((int)Math.Round(next.X * zoom), (int)Math.Round(next.Y * zoom)));
                        DrawLine(graphics, pen, new PointF(v.X * zoom, v.Y * zoom), new PointF(next.X * zoom, next.Y * zoom));

                        if (true == drawVertices)
                        {
                            v.DrawNormal(graphics, zoom);
                        }
                    }

                    if (true == drawVertices)
                    {
                        //Redraw the first vertex
                        vertices[0].DrawNormal(graphics, zoom);
                    }
                }
            }

            public void DrawOpen(Graphics graphics, float zoom)
            {
                if (vertices.Count != 0)
                {
                    //Draw the line that would close this crack
                    //DrawLine(graphics, closingPen, vertices[(vertices.Count - 2 + vertices.Count) % vertices.Count].Location, vertices[0].Location);
                    Vertex v = vertices[(vertices.Count - 2 + vertices.Count) % vertices.Count];
                    Vertex next = vertices[0];
                    //DrawLine(graphics, closingPen, new Point((int)Math.Round(v.X * zoom), (int)Math.Round(v.Y * zoom)), new Point((int)Math.Round(next.X * zoom), (int)Math.Round(next.Y * zoom)));
                    DrawLine(graphics, closingPen, new PointF(v.X * zoom, v.Y * zoom), new PointF(next.X * zoom, next.Y * zoom));

                    //Draw all but the last line and vertex in this crack
                    int count = vertices.Count - 1;
                    for (int i = 0; i != count; ++i)
                    {
                        v = vertices[i];
                        //DrawLine(graphics, openPen, vertex.Location, vertices[(i + 1) % vertices.Count].Location);
                        next = vertices[(i + 1) % vertices.Count];
                        //DrawLine(graphics, openPen, new Point((int)Math.Round(v.X * zoom), (int)Math.Round(v.Y * zoom)), new Point((int)Math.Round(next.X * zoom), (int)Math.Round(next.Y * zoom)));
                        DrawLine(graphics, openPen, new PointF(v.X * zoom, v.Y * zoom), new PointF(next.X * zoom, next.Y * zoom));
                        v.DrawNormal(graphics, zoom);
                    }

                    //Draw the last vertex
                    vertices[vertices.Count - 1].DrawNormal(graphics, zoom);
                }
            }

            public bool AddTo(Cracks cracks)
            {
                return cracks.Add(this);
            }

            public void RemoveFrom(Cracks cracks)
            {
                cracks.Remove(this);
            }

            public bool Add(Vertex v)
            {
                if (vertices.Contains(v))
                    return false;
                vertices.Add(v);
                bounded = false;
                v.AddTo(this);
                return true;
            }

            public bool Add(Vertex v, int index)
            {
                if (vertices.Contains(v))
                    return false;
                vertices.Insert(index, v);
                bounded = false;
                v.AddTo(this, index);
                return true;
            }

            public void Remove(Vertex v)
            {
                if (vertices.Remove(v))
                {
                    v.RemoveFrom(this);
                    bounded = false;
                }
            }

            public bool Replace(Vertex oldOne, Vertex newOne)
            {
                if (!vertices.Contains(oldOne))
                {
                    vertices.Add(newOne);
                }
                else
                {
                    int index = vertices.IndexOf(oldOne);
                    vertices[index] = newOne;
                }
                return true;
            }

            public void Reverse()
            {
                vertices.Reverse();
            }

            public bool Insert(Vertex first, Vertex v, Vertex second)
            {
                //Verify that this crack contains the first and second vertices
                int i = vertices.IndexOf(first);
                if (i == -1)
                    return false;
                int j = vertices.IndexOf(second);
                if (j == -1)
                    return false;

                //If the first precedes the second, add the vertex before the second
                if ((i + 1) % vertices.Count == j)
                {
                    v.AddTo(this, j);
                    return true;
                }

                //If the second precedes the first, add the vertex before the first
                if ((i - 1 + vertices.Count) % vertices.Count == j)
                {
                    v.AddTo(this, i);
                    return true;
                }

                //The first and second are not adjacent
                return false;
            }

            public bool Contains(Vertex v)
            {
                return vertices.Contains(v);
            }

            public int IndexOf(Vertex v)
            {
                return vertices.IndexOf(v);
            }

            public IEnumerator GetEnumerator()
            {
                return new Enumerator(this);
            }

            public class Enumerator : IEnumerator
            {
                public Enumerator(Crack c)
                {
                    crack = c;
                }

                public object Current
                {
                    get
                    {
                        return crack[index];
                    }
                }

                public void Reset()
                {
                    index = -1;
                }

                public bool MoveNext()
                {
                    if (index == crack.Count - 1)
                        return false;
                    ++index;
                    return true;
                }

                private Crack crack;
                private int index = -1;
            }

            //http://www.visibone.com/inpoly/
            public bool Contains(PointF location)
            {
                int npoints = Count;
                if (npoints < 3)
                    return false;
                bool inside = false;
                //int xold = vertices[npoints - 1].X;
                //int yold = vertices[npoints - 1].Y;
                float xold = vertices[npoints - 1].X;
                float yold = vertices[npoints - 1].Y;
                for (int i = 0; i < npoints; ++i)
                {
                    //int x1, y1;
                    //int x2, y2;
                    //int xnew = vertices[i].X;
                    //int ynew = vertices[i].Y;
                    float x1, y1;
                    float x2, y2;
                    float xnew = vertices[i].X;
                    float ynew = vertices[i].Y;

                    if (xnew > xold)
                    {
                        x1 = xold;
                        x2 = xnew;
                        y1 = yold;
                        y2 = ynew;
                    }
                    else
                    {
                        x1 = xnew;
                        x2 = xold;
                        y1 = ynew;
                        y2 = yold;
                    }

                    if ((xnew < location.X) == (location.X <= xold)          /* edge "open" at one end */
                        && (location.Y - y1) * (x2 - x1)
                        < (y2 - y1) * (location.X - x1))
                    {
                        inside = !inside;
                    }

                    xold = xnew;
                    yold = ynew;
                }
                return inside;
            }

            public void Unbound()
            {
                bounded = false;
            }

            private void Bound()
            {
                //bounds = new Rectangle(int.MaxValue, int.MaxValue, 1, 1);
                bounds = new RectangleF(float.MaxValue, float.MaxValue, -float.MaxValue, -float.MaxValue);
                foreach (Vertex v in vertices)
                {
                    //int x = Math.Min(v.X - Vertex.radius, bounds.Left);
                    //int y = Math.Min(v.Y - Vertex.radius, bounds.Top);
                    float x = Math.Min(v.X - Vertex.radius, bounds.Left);
                    float y = Math.Min(v.Y - Vertex.radius, bounds.Top);
                    bounds = new RectangleF(
                        x,
                        y,
                        1 + Math.Max(v.X + Vertex.radius, bounds.Right) - x,
                        1 + Math.Max(v.Y + Vertex.radius, bounds.Bottom) - y
                    );
                }
                bounded = true;
            }

            private List<Vertex> vertices = new List<Vertex>();
            private RectangleF bounds;
            private bool bounded = false;

            public static int thickness = 2;

            public static Pen closedPen = new Pen(Color.FromArgb(255, 0, 0, 255), thickness);
            public static Pen openPen = new Pen(Color.FromArgb(255, 255, 0, 0), thickness);
            public static Pen closingPen = new Pen(Color.FromArgb(255, 255, 0, 255), thickness);
        }

        private class Cracks
        {
            public int Count
            {
                get
                {
                    return cracks.Count;
                }
            }

            public int PointCount
            {
                get
                {
                    int totalPoints = 0;
                    foreach (Crack crack in cracks)
                    {
                        totalPoints += crack.Count;
                    }
                    return totalPoints;
                }
            }

            public Crack this[int index]
            {
                get
                {
                    return cracks[index];
                }
            }

            public void Draw(Graphics graphics, bool decompose, float zoom, bool drawVertices, Pen pen)
            {
                foreach (Crack crack in cracks)
                {
                    if (decompose)
                    {
                        try
                        {
                            Compute(PointListFrom(crack, zoom)).draw(graphics);
                        }
                        catch
                        {
                        }
                    }
                    crack.DrawClosed(graphics, zoom, pen, drawVertices);
                }
            }

            public bool Add(Crack crack)
            {
                if (cracks.Contains(crack))
                    return false;
                cracks.Add(crack);
                return true;
            }

            public void Remove(Crack crack)
            {
                cracks.Remove(crack);
            }

            public IEnumerator GetEnumerator()
            {
                return new Enumerator(this);
            }

            public class Enumerator : IEnumerator
            {
                public Enumerator(Cracks c)
                {
                    cracks = c;
                }

                public object Current
                {
                    get
                    {
                        return cracks[index];
                    }
                }

                public void Reset()
                {
                    index = -1;
                }

                public bool MoveNext()
                {
                    if (index == cracks.Count - 1)
                        return false;
                    ++index;
                    return true;
                }

                private Cracks cracks;
                private int index = -1;
            }

            private List<Crack> cracks = new List<Crack>();
        }

        private enum Mode
        {
            select,
            insert
        }

        //private abstract class Action
        public abstract class Action
        {
            public abstract void Redo(Cracker cracker);
            public abstract void Undo(Cracker cracker);
        }

        private class StartCrack : Action
        {
            public StartCrack(Crack c)
            {
                crack = c;
            }

            public override void Redo(Cracker cracker)
            {
                //Switch to insertion mode
                cracker.mode = Mode.insert;

                //Activate this crack and its second vertex
                cracker.crack = crack;
                cracker.vertex = crack[1];
                //cracker.vertex.Location = cracker.PointToClient(MousePosition);
                cracker.vertex.Location = cracker.RemoveZoom(cracker.PointToClient(MousePosition));

                cracker.multiSelectionList.Clear();
                if (cracker.OnSplitable != null)
                    cracker.OnSplitable(this, new SplitableEventArgs(false));
                cracker.Invalidate(true);
            }

            public override void Undo(Cracker cracker)
            {
                //Deactivate the current crack
                cracker.crack = null;
                cracker.vertex = null;

                //Switch to selection mode
                cracker.mode = Mode.select;

                cracker.multiSelectionList.Clear();
                if (cracker.OnSplitable != null)
                    cracker.OnSplitable(this, new SplitableEventArgs(false));
                cracker.Invalidate(true);
            }

            private Crack crack;
        }

        private class AddVertex : Action
        {
            public AddVertex(Vertex v)
            {
                vertex = v;
            }

            public override void Redo(Cracker cracker)
            {
                //Insert this vertex immediately before the final vertex in the current crack
                vertex.AddTo(cracker.crack, cracker.crack.Count - 1);
            }

            public override void Undo(Cracker cracker)
            {
                //Remove this vertex from the current crack
                vertex.RemoveFrom(cracker.crack);
            }

            private Vertex vertex;
        }

        private class FinishCrack : Action
        {
            public FinishCrack(Crack c)
            {
                crack = c;
                ccw = Counterclockwise(c);
                if (!ccw)
                    c.Reverse();
            }

            public override void Redo(Cracker cracker)
            {
                if (!ccw)
                    crack.Reverse();

                //Switch to selection mode
                cracker.mode = Mode.select;

                //Remove the current vertex from this crack and forget them
                cracker.vertex.RemoveFrom(crack);
                cracker.vertex = null;
                cracker.crack = null;

                //Store this crack if it is a polygon
                if (crack.Count > 2)
                    crack.AddTo(cracker.cracks);

                cracker.multiSelectionList.Clear();
                if (cracker.OnSplitable != null)
                    cracker.OnSplitable(this, new SplitableEventArgs(false));
                cracker.Invalidate(true);
            }

            public override void Undo(Cracker cracker)
            {
                if (!ccw)
                    crack.Reverse();

                //Remove this crack and activate it
                crack.RemoveFrom(cracker.cracks);
                cracker.crack = crack;

                //Add a new vertex to the end of this crack
                //cracker.vertex = new Vertex(cracker.PointToClient(MousePosition));                
                cracker.vertex = new Vertex(cracker.RemoveZoom(cracker.PointToClient(MousePosition)));
                cracker.vertex.AddTo(cracker.crack);

                //Switch to insertion mode
                cracker.mode = Mode.insert;

                cracker.ignoreMove = false;
                cracker.dragging = true;
            }

            private Crack crack;
            private bool ccw;
        }

        private class MoveVertex : Action
        {
            public MoveVertex(Vertex v)
            {
                vertex = v;
                start = vertex.Location;
            }

            public override void Redo(Cracker cracker)
            {
                vertex.Location = end;
            }

            public override void Undo(Cracker cracker)
            {
                end = vertex.Location;
                vertex.Location = start;
            }

            private Vertex vertex;
            private PointF start;
            private PointF end;
        }

        private abstract class VertexContainers : Action
        {
            public VertexContainers(Vertex v)
            {
                vertex = v;

                //Record this vertex's containers
                foreach (Crack crack in vertex)
                    containers.Add(new Container(crack, crack.IndexOf(vertex)));
            }

            protected Vertex vertex;
            protected struct Container
            {
                public Crack crack;
                public int index;

                public Container(Crack c, int i)
                {
                    crack = c;
                    index = i;
                }
            }
            protected List<Container> containers = new List<Container>();
        }

        private class DeleteVertex : VertexContainers
        {
            public DeleteVertex(Vertex v)
                : base(v)
            {
            }

            public override void Redo(Cracker cracker)
            {
                foreach (Container container in containers)
                {
                    //Remove this vertex from its containers
                    vertex.RemoveFrom(container.crack);

                    //Forget the containers if they are no longer polygons
                    if (container.crack.Count < 3)
                        container.crack.RemoveFrom(cracker.cracks);
                }

                //Deactivate this vertex if it was active
                if (vertex == cracker.vertex)
                    cracker.vertex = null;
            }

            public override void Undo(Cracker cracker)
            {
                foreach (Container container in containers)
                {
                    //Add this vertex to its containers
                    vertex.AddTo(container.crack, container.index);

                    //Store the container if it has become a polygon
                    if (container.crack.Count == 3)
                        container.crack.AddTo(cracker.cracks);
                }
            }
        }

        private class InsertVertex : VertexContainers
        {
            public InsertVertex(Vertex v)
                : base(v)
            {
            }

            public override void Redo(Cracker cracker)
            {
                //Add this vertex to its containers
                foreach (Container container in containers)
                    vertex.AddTo(container.crack, container.index);
            }

            public override void Undo(Cracker cracker)
            {
                //Remove this vertex from its containers
                foreach (Container container in containers)
                    vertex.RemoveFrom(container.crack);

                //Deactivate this vertex if it was active
                if (vertex == cracker.vertex)
                    cracker.vertex = null;
            }
        }

        private class DeleteCrack : Action
        {
            public DeleteCrack(Crack c)
            {
                crack = c;
            }

            public override void Redo(Cracker cracker)
            {
                crack.RemoveFrom(cracker.cracks);
                cracker.crack = null;
                //cracker.crack = cracker.SelectCrack(cracker.PointToClient(MousePosition));
                cracker.crack = cracker.SelectCrack(cracker.RemoveZoom(cracker.PointToClient(MousePosition)));
            }

            public override void Undo(Cracker cracker)
            {
                crack.AddTo(cracker.cracks);
                //cracker.crack = cracker.SelectCrack(cracker.PointToClient(MousePosition));
                cracker.crack = cracker.SelectCrack(cracker.RemoveZoom(cracker.PointToClient(MousePosition)));
            }

            private Crack crack;
        }

        private class SplitCrack : Action
        {
            public SplitCrack(Crack crack0, int index0, Crack crack)
            {
                this.crack0 = crack0;
                this.index0 = index0;
                this.crack = crack;
            }

            public override void Redo(Cracker cracker)
            {
                cracker.InvalidateBounds(crack0);
                for (int begin = 1, end = crack.Count - 1; begin != end; ++begin)
                    crack[begin].RemoveFrom(crack0);
                crack.AddTo(cracker.cracks);
            }

            public override void Undo(Cracker cracker)
            {
                int index = index0;
                foreach (Vertex vertex in crack)
                    vertex.AddTo(crack0, index++);
                crack.RemoveFrom(cracker.cracks);
                cracker.InvalidateBounds(crack0);
                cracker.multiSelectionList.Clear();
                if (cracker.OnSplitable != null)
                    cracker.OnSplitable(this, new SplitableEventArgs(false));
            }

            private Crack crack0;
            private Crack crack;
            private int index0;
        }

        private class ScaleSelectedVertices : Action
        {
            public ScaleSelectedVertices(List<Vertex> multiSelectionList)
            {
                foreach (Vertex vertex in multiSelectionList)
                {
                    this.multiSelectionList.Add(vertex);
                    locationList.Add(vertex.Location);
                }
            }

            public override void Redo(Cracker cracker)
            {
                Swap();
            }

            public override void Undo(Cracker cracker)
            {
                Swap();
                cracker.multiSelectionList.Clear();
                if (cracker.OnSplitable != null)
                    cracker.OnSplitable(this, new SplitableEventArgs(false));
            }

            private void Swap()
            {
                for (int index = 0; index != multiSelectionList.Count; ++index)
                {
                    Vertex vertex = multiSelectionList[index];
                    PointF location = vertex.Location;
                    vertex.Location = locationList[index];
                    locationList[index] = location;
                }
            }

            private List<Vertex> multiSelectionList = new List<Vertex>();
            private List<PointF> locationList = new List<PointF>();
        }

        private class WeldSelectedVertices : Action
        {
            public WeldSelectedVertices(List<Vertex> multiSelectionList)
            {
                foreach (Vertex vertex in multiSelectionList)
                {
                    this.multiSelectionList.Add(vertex);
                    locationList.Add(vertex.Location);
                }
            }

            public override void Redo(Cracker cracker)
            {
                Swap();
            }

            public override void Undo(Cracker cracker)
            {
                Swap();
                cracker.multiSelectionList.Clear();
                if (cracker.OnSplitable != null)
                    cracker.OnSplitable(this, new SplitableEventArgs(false));
            }

            private void Swap()
            {
                for (int index = 0; index != multiSelectionList.Count; ++index)
                {
                    Vertex vertex = multiSelectionList[index];
                    PointF location = vertex.Location;
                    vertex.Location = locationList[index];
                    locationList[index] = location;
                }
            }

            private List<Vertex> multiSelectionList = new List<Vertex>();
            private List<PointF> locationList = new List<PointF>();
        }

        private class Actions
        {
            public int Index
            {
                get
                {
                    return index - 1;
                }
            }

            public void Add(Action action)
            {
                //Forget actions following this index
                actions.RemoveRange(index, actions.Count - index);

                //Forget the save index if it no longer exists
                if (save > actions.Count)
                    save = -1;

                //Add the action and update the index
                actions.Add(action);
                index = actions.Count;
            }

            public void Save()
            {
                save = index;
            }

            public bool Saved()
            {
                return save == index;
            }

            public bool Redo(Cracker cracker)
            {
                if (index == actions.Count)
                    return false;
                actions[index++].Redo(cracker);
                return index != actions.Count;
            }

            public bool Undo(Cracker cracker)
            {
                if (index == 0)
                    return false;
                actions[--index].Undo(cracker);
                return index != 0;
            }

            public List<Action> actions = new List<Action>();
            private int index = 0;
            private int save = 0;
        }

        private string file = null;

        private Actions actions = new Actions();
        private Cracks cracks = new Cracks();

        private Crack crack = null;
        private Vertex vertex = null;

        private Mode mode = Mode.select;

        private float zoom = 1;

        private bool ignoreClick = false;
        private bool ignoreMove = false;
        private bool dragging = false;
        private bool dragSelecting = false;
        private List<Vertex> selectionSave = new List<Vertex>();
        private PointF dragSelectionStart = new PointF();
        private PointF dragSelectionEnd = new PointF();

        private bool store = false;
        private bool show = false;
        private bool showStats = false;

        private void Cracker_KeyDown(object sender, KeyEventArgs e)
        {
            if (mode == Mode.select)
            {
                switch (e.KeyCode)
                {
                case Keys.Delete:
                    {
                        //Delete the current vertex, if there is one
                        if (vertex != null)
                        {
                            Do(new DeleteVertex(vertex));

                            //Remove the current vertex from its containers
                            for (int count = vertex.ContainerCount; count != 0; )
                            {
                                Crack c = vertex.GetContainer(--count);
                                vertex.RemoveFrom(c);
                                //Invalidate(c.Bounds);
                                InvalidateBounds(c);

                                //Remove this crack if it's not a polygon
                                if (c.Count < 3)
                                    c.RemoveFrom(cracks);
                            }

                            //Forget the vertex
                            vertex = null;
                            //crack = SelectCrack(PointToClient(MousePosition));
                            crack = SelectCrack(RemoveZoom(PointToClient(MousePosition)));
                            if (crack != null)
                                //Invalidate(crack.Bounds);
                                InvalidateBounds(crack);
                            ignoreMove = false;
                        }
                        else
                        {
                            //Delete the current crack, if there is one
                            if (crack != null)
                            {
                                Do(new DeleteCrack(crack));
                                crack.RemoveFrom(cracks);
                                //Invalidate(crack.Bounds);
                                InvalidateBounds(crack);
                                crack = null;
                            }
                        }

                        multiSelectionList.Clear();
                        if (OnSplitable != null)
                            OnSplitable(this, new SplitableEventArgs(false));
                        Invalidate(true);

                        break;
                    }
                case Keys.Insert:
                    {
                        Point location = PointToClient(MousePosition);
                        Vertex first = null;
                        Vertex second = null;
                        Vertex v = null;
                        foreach (Crack crack in cracks)
                        {
                            //If there is a vertex, attempt to insert it in this crack between first and second
                            if (v != null)
                            {
                                crack.Insert(first, v, second);
                                //Invalidate(crack.Bounds);
                                InvalidateBounds(crack);
                            }
                            else
                            {
                                for (int index = 0; index != crack.Count; ++index)
                                {
                                    //Find when this location is nearest the line through this vertex and the next
                                    first = crack[index];
                                    second = crack[(index + 1) % crack.Count];
                                    float p0x = first.Location.X * Zoom;
                                    float p0y = first.Location.Y * Zoom;
                                    float p1x = second.Location.X * Zoom;
                                    float p1y = second.Location.Y * Zoom;
                                    float x = p1x - p0x;
                                    float y = p1y - p0y;
                                    float t = ((location.X - p0x) * x + (location.Y - p0y) * y) / (x * x + y * y);

                                    //Continue if the time is beyond the ends of the segment
                                    if (!(0 < t && t < 1))
                                        continue;

                                    //Find the distance between this location and the nearest point on the segment
                                    x = p0x + t * x;
                                    y = p0y + t * y;
                                    //Point point = new Point((int)Math.Round(x / Zoom), (int)Math.Round(y / Zoom));
                                    PointF point = new PointF(x / Zoom, y / Zoom);
                                    x -= location.X;
                                    y -= location.Y;

                                    //Determine if the distance is short enough
                                    if (x * x + y * y < Vertex.radius * Vertex.radius)
                                    {
                                        //Create a vertex at the nearest point, and insert it in the current crack
                                        v = new Vertex(point);
                                        crack.Insert(first, v, second);
                                        InvalidateBounds(crack);
                                        break;
                                    }
                                }
                            }
                        }

                        //If there is a new vertex, record its creation
                        if (v != null)
                            Do(new InsertVertex(v));

                        multiSelectionList.Clear();
                        if (OnSplitable != null)
                            OnSplitable(this, new SplitableEventArgs(false));
                        Invalidate(true);
                    }
                    break;
                }
            }
        }

        private void Cracker_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
                return;
            ignoreClick = false;
            ignoreMove = false;
            switch (MouseButtons & (MouseButtons.Left | MouseButtons.Right))
            {
            case MouseButtons.Left:
                dragging = false;
                dragSelecting = false;
                dragSelectionStart = RemoveZoom(new Point(e.X, e.Y));
                dragSelectionEnd = RemoveZoom(new Point(e.X, e.Y));
                switch (mode)
                {
                case Mode.insert:
                    dragging = true;
                    break;
                case Mode.select:
                    //Unselect current vertex, if any
                    if (vertex != null)
                    {
                        if (multiSelectionList.Count == 0
                            && ModifierKeys == Keys.Control)
                        {
                            multiSelectionList.Add(vertex);
                        }

                        InvalidatePoint(vertex.Location);
                    }

                    //Attempt to select a vertex
                    //vertex = SelectVertex(new Point(e.X, e.Y));
                    vertex = SelectVertex(RemoveZoom(new Point(e.X, e.Y)));
                    if (vertex != null)
                    {
                        InvalidatePoint(vertex.Location);

                        //Unselect current crack, if any
                        if (crack != null)
                        {
                            //Invalidate(crack.Bounds);
                            InvalidateBounds(crack);
                            crack = null;
                        }

                        if (ModifierKeys == Keys.Control)
                        {
                            if (false == multiSelectionList.Contains(vertex))
                            {
                                multiSelectionList.Add(vertex);
                                if (OnSplitable != null)
                                    OnSplitable(this, new SplitableEventArgs(multiSelectionList.Count == 2));
                            }
                            else
                            {
                                multiSelectionList.Remove(vertex);
                                if (OnSplitable != null)
                                    OnSplitable(this, new SplitableEventArgs(multiSelectionList.Count == 2));
                            }

                        }
                        else
                        {
                            multiSelectionList.Clear();
                            if (OnSplitable != null)
                                OnSplitable(this, new SplitableEventArgs(false));
                            Invalidate(true);
                        }
                    }
                    else
                    {
                        selectionSave.Clear();
                        if (ModifierKeys != Keys.Control)
                            multiSelectionList.Clear();
                        else
                        {
                            Vertex[] verts = new Vertex[multiSelectionList.Count];
                            multiSelectionList.CopyTo(verts);
                            selectionSave.AddRange(verts);
                        }
                        if (OnSplitable != null)
                            OnSplitable(this, new SplitableEventArgs(false));
                        Invalidate(true);
                        dragSelecting = true;
                    }
                    break;
                }
                break;
            case MouseButtons.Right:
                dragging = false;
                switch (mode)
                {
                case Mode.insert:
                    multiSelectionList.Clear();
                    if (OnSplitable != null)
                        OnSplitable(this, new SplitableEventArgs(false));
                    Invalidate(true);

                    //Move the current vertex to the next
                    ignoreMove = true;
                    Vertex last = crack[(2 * crack.Count - 2) % crack.Count];
                    Vertex next = crack[0];
                    InvalidateTriangle(last.Location, vertex.Location, next.Location);
                    vertex.Location = next.Location;
                    break;
                case Mode.select:
                    multiSelectionList.Clear();
                    if (OnSplitable != null)
                        OnSplitable(this, new SplitableEventArgs(false));
                    Invalidate(true);

                    if (crack != null)
                        //Invalidate(crack.Bounds);
                        InvalidateBounds(crack);
                    crack = new Crack();
                    //Point location = new Point(e.X, e.Y);
                    PointF location = RemoveZoom(new PointF(e.X, e.Y));

                    //If possible, select a vertex at which to start the crack
                    Vertex selection = SelectVertex(location);
                    if (selection != null)
                    {
                        vertex = selection;
                    }
                    else
                    {
                        dragging = true;
                        if (vertex != null)
                        {
                            //Start the crack at the selected vertex
                            vertex.AddTo(crack);
                            InvalidateLine(vertex.Location, location);
                        }

                        //Create a new vertex for the crack
                        vertex = new Vertex(location);
                    }

                    //Add the selected vertex to the crack
                    vertex.AddTo(crack);
                    InvalidatePoint(vertex.Location);
                    break;
                }
                break;
            default:
                ignoreClick = true;
                switch (mode)
                {
                case Mode.insert:
                    if (crack != null)
                        //Invalidate(crack.Bounds);
                        InvalidateBounds(crack);
                    break;
                case Mode.select:
                    if (dragging)
                    {
                        //Parent.Parent;
                        UndoneEventArgs uea = new UndoneEventArgs(Undo());
                        if (OnUndone != null)
                            OnUndone(this, uea);
                        dragging = false;
                    }
                    vertex = null;
                    crack = null;
                    break;
                }
                break;
            }
        }

        private void Cracker_MouseMove(object sender, MouseEventArgs e)
        {
            Invalidate(true); //@HACK - Figure out why rendering doesn't work right after a piece is split

            if (CanDoActionHere())
            {
                this.Cursor = Cursors.UpArrow;
            }
            else
            {
                this.Cursor = Cursors.Cross;
            }

            if (!ContainsFocus || ignoreMove)
                return;
            if (vertex != null)
            {
                vertex.Move(RemoveZoom(e.Location), new PointF(BackgroundImage.Width, BackgroundImage.Height), dragging, Zoom, this);
            }
            else if (dragSelecting)
            {
                dragSelectionEnd = RemoveZoom(e.Location);
            }
//             else if (multiSelectionList.Count > 0)
//             {
//                 foreach (Vertex v in multiSelectionList)
//                 {
//                     v.Move(RemoveZoom(e.Location), new PointF(BackgroundImage.Width, BackgroundImage.Height), dragging, Zoom, this);
//                 }
//             }
            else
            {
                //Crack c = SelectCrack(PointToClient(MousePosition));
                Crack c = SelectCrack(RemoveZoom(PointToClient(MousePosition)));
                if (crack != c)
                {
                    if (crack != null)
                        //Invalidate(crack.Bounds);
                        InvalidateBounds(crack);
                    if (c != null)
                        //Invalidate(c.Bounds);
                        InvalidateBounds(c);
                    crack = c;
                }
            }
        }

        private void Cracker_MouseUp(object sender, MouseEventArgs e)
        {
            if (ignoreClick)
                return;
            switch (e.Button)
            {
            case MouseButtons.Left:
                switch (mode)
                {
                case Mode.insert:
                    dragging = true;
                    Vertex last = crack[(2 * crack.Count - 2) % crack.Count];
                    Vertex next = crack[0];
                    InvalidateTriangle(last.Location, vertex.Location, next.Location);

                    //Attempt to snap to a nearby vertex
                    //Point location = new Point(e.X, e.Y);
                    PointF location = RemoveZoom(new PointF(e.X, e.Y));
                    Vertex selection = SelectVertex(location);
                    if (selection != null)
                    {
                        vertex.RemoveFrom(crack);
                        (vertex = selection).AddTo(crack);
                    }

                    //Add vertex to crack
                    Do(new AddVertex(vertex));
                    vertex = new Vertex(location);
                    vertex.AddTo(crack);
                    break;
                case Mode.select:
                    ignoreMove = vertex != null;
                    break;
                }
                break;
            case MouseButtons.Right:
                switch (mode)
                {
                case Mode.insert:
                    //Close the crack
                    Do(new FinishCrack(crack));
                    vertex.RemoveFrom(crack);

                    //Store the crack, if it's a polygon
                    if (crack.Count > 2)
                        crack.AddTo(cracks);

                    //Switch to select mode
                    vertex = null;
                    ignoreMove = false;
                    mode = Mode.select;
                    //Invalidate(crack.Bounds);
                    InvalidateBounds(crack);
                    break;
                case Mode.select:
                    //Switch to insert mode
                    mode = Mode.insert;
                    dragging = true;
                    //Point location = new Point(e.X, e.Y);
                    PointF location = RemoveZoom(new PointF(e.X, e.Y));

                    //Start a crack
                    if (crack.Count == 1)
                    {
                        vertex = new Vertex(location);
                        vertex.AddTo(crack);
                    }
                    Do(new StartCrack(crack));
                    InvalidatePoint(vertex.Location);
                    break;
                }
                break;
            }
            dragSelecting = false;
        }

        public static Brush centerPointBrush = new SolidBrush(Color.Fuchsia);
        private static System.Drawing.Font statsFont = new System.Drawing.Font("Microsoft Sans Serif", 20.00F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
        
        private void Cracker_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.ScaleTransform(Zoom, Zoom);

            if (false == backgroundVisible)
            {
                SolidBrush brush = new SolidBrush(Color.Black);
                e.Graphics.FillRectangle(brush, 0, 0, this.Width, this.Height);
            }

            //Draw the cracks            
            cracks.Draw(e.Graphics, show, zoom, true, null);

            int totalPolygons = 0;
            try
            {
                List<List<List<int>>> polygons = Decompose();
                for (int i = 0; i != cracks.Count; ++i)
                {
                    totalPolygons += polygons[i].Count;
                }
            }
            catch
            {
            }
            if (showStats)
            {
                e.Graphics.DrawString("Total Shapes: " + cracks.Count, statsFont, new SolidBrush(Color.FromArgb(255, 255, 0, 0)), new Point(10, 10));
                e.Graphics.DrawString("Total Polygon: " + totalPolygons, statsFont, new SolidBrush(Color.FromArgb(255, 255, 0, 0)), new Point(10, 40));
                e.Graphics.DrawString("Total Points: " + cracks.PointCount, statsFont, new SolidBrush(Color.FromArgb(255, 255, 0, 0)), new Point(10, 70));
            }

            //Draw the current crack
            if (crack != null)
            {
                switch (mode)
                {
                case Mode.insert:
                    crack.DrawOpen(e.Graphics, zoom);
                    break;
                case Mode.select:
                    if (vertex == null)
                        crack.DrawClosed(e.Graphics, new Pen(System.Drawing.Color.Yellow, Crack.thickness), zoom, true);
                    break;
                }
            }

            //Highlight the selected vertex
            if (vertex != null)
                vertex.DrawHighlight(e.Graphics, zoom);

            if (multiSelectionList.Count > 0)
            {
                SolidBrush b = new SolidBrush(Color.BlueViolet);
                for (int i = multiSelectionList.Count; i > 0; --i)
                {
                    multiSelectionList[i - 1].Draw(e.Graphics, b, zoom);
                }
            }

            if (centerPointVisible)
            {
                centerPoint.SetX(BackgroundImage.Width / 2);
                centerPoint.SetY(BackgroundImage.Height / 2);

                centerPoint.DrawUsingThisBrush(e.Graphics, centerPointBrush, zoom);
            }

            if (dragSelecting)
            {
                Pen pen = new Pen(Color.DarkGray);
                PointF start = new PointF(dragSelectionStart.X,dragSelectionStart.Y);
                PointF end = new PointF(dragSelectionEnd.X, dragSelectionEnd.Y);
                if (start.X > end.X)
                {
                    float oldStart = start.X;
                    start.X = end.X;
                    end.X = oldStart;
                }
                if (start.Y > dragSelectionEnd.Y)
                {
                    float oldStart = start.Y;
                    start.Y = end.Y;
                    end.Y = oldStart;
                }
                e.Graphics.DrawRectangle(pen, start.X, start.Y, end.X - start.X, end.Y - start.Y);
                List<Vertex> selectedVerts = SelectVertices(start, end);
                if (selectedVerts.Count > 0)
                {
                    multiSelectionList.Clear();
                    if (ModifierKeys == Keys.Control)
                    {
                        multiSelectionList.AddRange(selectionSave.ToArray());
                        multiSelectionList.AddRange(selectedVerts.ToArray());
                    }
                    else
                    {
                        multiSelectionList.AddRange(selectedVerts.ToArray());
                    }
                    if (OnSplitable != null)
                        OnSplitable(this, new SplitableEventArgs(multiSelectionList.Count == 2));
                    Invalidate(true);
                }
            }
        }

        private float zoomStep;
        /// <remarks>Get or set the zoom increment/decrement factor (must be greater than 0.0, Examples: 0.1=%10, 1.0=%100, 2.0=%200, etc.)</remarks>
        /// <value>0.1</value>
        public float ZoomStep
        {
            get
            {
                return zoomStep;
            }
            set
            {
                // TODO - ADD RANGE EXCEPTION
                zoomStep = value;
            }
        }

        private float zoomMin;
        /// <remarks>Minimum zoom scale (must be greater than 0.0)</remarks>
        /// <value>0.1</value>
        public float ZoomMin
        {
            get
            {
                return zoomMin;
            }
            set
            {
                // TODO - ADD RANGE EXCEPTION
                zoomMin = value;
            }
        }

        private float zoomMax;
        /// <remarks>Maximum zoom scale (don't set too high)</remarks>
        /// <value>5.0</value>
        public float ZoomMax
        {
            get
            {
                return zoomMax;
            }
            set
            {
                // TODO - ADD RANGE EXCEPTION
                zoomMax = value;
            }
        }

        bool centerPointVisible;
        /// <remarks>Toggle the center point of the reference image</remarks>
        /// <value>false</value>
        public bool CenterPointVisible
        {
            get
            {
                return centerPointVisible;
            }
            set
            {
                centerPointVisible = value;
            }
        }

        private Vertex centerPoint = new Vertex(0, 0);
        private Size originalImageSize = new Size(0, 0);

        public void SaveReferenceLines()
        {
            Image tmpImage = BackgroundImage;
            BackgroundImage = null;

            Bitmap bmp = new Bitmap(originalImageSize.Width, originalImageSize.Height);
            Graphics g = Graphics.FromImage(bmp);
            SolidBrush brush = new SolidBrush(Color.Black);

            g.FillRectangle(brush, 0, 0, bmp.Width, bmp.Height);
            Pen pen = new Pen(Color.White);
            cracks.Draw(g, false, 1.0f, false, pen);

            String filename = "";
            int dot = file.LastIndexOf('.');
            filename = file.Substring(0, dot) + ".bmp";
            try
            {
                // This may fail if the BMP file is not checked out
                bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Bmp);
            }
            catch (System.Exception)
            {
                // Let the user know in case he forgot to check out
                MessageBox.Show(this, "Failed saving \"" + filename + "\". Did you check out this file?", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            BackgroundImage = tmpImage;
        }

        private bool backgroundVisible;
        /// <value>true</value>
        public bool BackgroundVisible
        {
            get
            {
                return backgroundVisible;
            }
            set
            {
                backgroundVisible = value;
                Invalidate(true);
            }
        }

        private List<Vertex> multiSelectionList = new List<Vertex>();

        /// <summary>
        /// Indicates whether the mouse cursor is over a line or vertex.  If over a line, you can insert a vertex.  If you over a vertex, you can select it.
        /// </summary>
        /// <remarks>Indicates whether the mouse cursor is over a line or vertex.  If over a line, you can insert a vertex.  If you over a vertex, you can select it.</remarks>
        /// <returns>bool</returns>
        public bool CanDoActionHere()
        {
            Point location = PointToClient(MousePosition);
            Vertex first = null;
            Vertex second = null;
            Vertex v = null;
            foreach (Crack crack in cracks)
            {
                //If there is a vertex, attempt to insert it in this crack between first and second
                if (v != null)
                {
                    return true;
                }
                else
                {
                    for (int index = 0; index != crack.Count; ++index)
                    {
                        //Find when this location is nearest the line through this vertex and the next
                        first = crack[index];
                        second = crack[(index + 1) % crack.Count];
                        float p0x = first.Location.X * Zoom;
                        float p0y = first.Location.Y * Zoom;
                        float p1x = second.Location.X * Zoom;
                        float p1y = second.Location.Y * Zoom;
                        float x = p1x - p0x;
                        float y = p1y - p0y;
                        float t = ((location.X - p0x) * x + (location.Y - p0y) * y) / (x * x + y * y);

                        //Continue if the time is beyond the ends of the segment
                        if (!(0 < t && t < 1))
                            continue;

                        //Find the distance between this location and the nearest point on the segment
                        x = p0x + t * x;
                        y = p0y + t * y;
                        //Point point = new Point((int)Math.Round(x / Zoom), (int)Math.Round(y / Zoom));
                        PointF point = new PointF(x / Zoom, y / Zoom);
                        x -= location.X;
                        y -= location.Y;

                        //Determine if the distance is short enough
                        if (x * x + y * y < Vertex.radius * Vertex.radius)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }


        /// <summary>
        /// If two and only two vertices are selected, then this will connect them creating two pieces from one piece.
        /// </summary>
        /// <remarks>Splits a piece in two.</remarks>
        /// <returns>bool</returns>
        public bool SplitPiece()
        {
            if (multiSelectionList.Count != 2)
                return false;
            Vertex vertex0 = multiSelectionList[0];
            Vertex vertex1 = multiSelectionList[1];
            foreach (Crack crack0 in multiSelectionList[0])
            {
                foreach (Crack crack1 in multiSelectionList[1])
                {
                    if (crack0 == crack1)
                    {
                        int index0 = crack0.IndexOf(vertex0);
                        int index1 = crack0.IndexOf(vertex1);
                        if (index1 < index0)
                        {
                            Vertex vertex = vertex0;
                            int index = index0;
                            vertex0 = vertex1;
                            index0 = index1;
                            vertex1 = vertex;
                            index1 = index;
                        }
                        if (index1 - index0 == 1)
                            continue;
                        InvalidateBounds(crack0);
                        Crack crack = new Crack();
                        Do(new SplitCrack(crack0, index0, crack));
                        vertex0.AddTo(crack);
                        while (crack0[index0 + 1] != vertex1)
                        {
                            Vertex vertex = crack0[index0 + 1];
                            vertex.RemoveFrom(crack0);
                            vertex.AddTo(crack);
                        }
                        vertex1.AddTo(crack);
                        crack.AddTo(cracks);
                        return true;
                    }
                }
            }
            return false;
        }

        public void DeselectAll()
        {
            //Deactivate the current crack
            this.crack = null;
            this.vertex = null;

            //Vertex v = this.vertex;
            //this.vertex = null;
            //InvalidatePoint(new PointF(v.X, v.Y));
            //v = null;
            Invalidate(true);
        }
    }

    public delegate void OpenEventHandler(object sender, OpenEventArgs e);

    public class OpenEventArgs : EventArgs
    {
        public OpenEventArgs(bool c)
        {
            convex = c;
        }

        public bool Convex
        {
            get
            {
                return convex;
            }
        }

        private bool convex;
    }

    public delegate void UndoneEventHandler(object sender, UndoneEventArgs e);

    public class UndoneEventArgs : EventArgs
    {
        public UndoneEventArgs(bool u)
        {
            undo = u;
        }

        public bool Undo
        {
            get
            {
                return undo;
            }
        }

        private bool undo;
    }

    public delegate void SplitableEventHandler(object sender, SplitableEventArgs e);

    public class SplitableEventArgs : EventArgs
    {
        public SplitableEventArgs(bool splitable)
        {
            this.splitable = splitable;
        }

        public bool Splitable
        {
            get
            {
                return splitable;
            }
        }

        private bool splitable;
    }
}
