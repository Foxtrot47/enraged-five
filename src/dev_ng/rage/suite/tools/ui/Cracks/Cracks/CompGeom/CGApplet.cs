using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
namespace CompGeom
{
	
	//UPGRADE_TODO: Class 'java.applet.Applet' was converted to 'System.Windows.Forms.UserControl' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaappletApplet'"
	[Serializable]
	public abstract class CGApplet:System.Windows.Forms.UserControl
	{
		public CGApplet()
		{
			InitBlock();
		}
		private void  InitBlock()
		{
			System.Windows.Forms.Button temp_Button2;
			temp_Button2 = new System.Windows.Forms.Button();
			temp_Button2.Text = "Clear All";
			clear_button = temp_Button2;
			System.Windows.Forms.Button temp_Button4;
			temp_Button4 = new System.Windows.Forms.Button();
			temp_Button4.Text = "Delete Last";
			delete_button = temp_Button4;
			System.Windows.Forms.Button temp_Button6;
			temp_Button6 = new System.Windows.Forms.Button();
			temp_Button6.Text = "Animate";
			anim_button = temp_Button6;
			System.Windows.Forms.Button temp_Button8;
			temp_Button8 = new System.Windows.Forms.Button();
			temp_Button8.Text = "Detail";
			detail_button = temp_Button8;
			System.Windows.Forms.Button temp_Button10;
			temp_Button10 = new System.Windows.Forms.Button();
			temp_Button10.Text = "Random";
			random_button = temp_Button10;
			System.Windows.Forms.Button temp_Button12;
			temp_Button12 = new System.Windows.Forms.Button();
			temp_Button12.Text = "Update";
			update_button = temp_Button12;
			this.Load += new System.EventHandler(this.CGApplet_StartEventHandler);
			this.Disposed += new System.EventHandler(this.CGApplet_StopEventHandler);
		}
		public bool isActiveVar = true;
		
		internal static PointList pts = new PointList(100);
		internal static Anim2D result = null;
		internal static bool detail = false;
		internal static bool animating_Renamed_Field = false;
		
		//UPGRADE_NOTE: The initialization of  'clear_button' was moved to method 'InitBlock'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1005'"
		private System.Windows.Forms.Button clear_button;
		//UPGRADE_NOTE: The initialization of  'delete_button' was moved to method 'InitBlock'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1005'"
		private System.Windows.Forms.Button delete_button;
		//UPGRADE_NOTE: The initialization of  'anim_button' was moved to method 'InitBlock'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1005'"
		private System.Windows.Forms.Button anim_button;
		//UPGRADE_NOTE: The initialization of  'detail_button' was moved to method 'InitBlock'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1005'"
		private System.Windows.Forms.Button detail_button;
		//UPGRADE_NOTE: The initialization of  'random_button' was moved to method 'InitBlock'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1005'"
		private System.Windows.Forms.Button random_button;
		//UPGRADE_NOTE: The initialization of  'update_button' was moved to method 'InitBlock'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1005'"
		private System.Windows.Forms.Button update_button;
		
		public bool animating()
		{
			return animating_Renamed_Field;
		}
		
		//UPGRADE_TODO: Commented code was moved to the 'InitializeComponent' method. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1228'"
		public void  init()
		{
			InitializeComponent();
			//UPGRADE_ISSUE: Method 'java.awt.Container.setLayout' was not converted. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1000_javaawtContainersetLayout_javaawtLayoutManager'"
			//UPGRADE_ISSUE: Constructor 'java.awt.BorderLayout.BorderLayout' was not converted. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1000_javaawtBorderLayout'"
			/*
			setLayout(new BorderLayout());*/
			
			/*
			BackColor = System.Drawing.Color.White;*/
			
			/*
			ForeColor = System.Drawing.Color.Black;*/
			
			//UPGRADE_ISSUE: Class hierarchy differences between 'java.awt.Panel' and 'System.Windows.Forms.Panel' may cause compilation errors. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1186'"
			System.Windows.Forms.Panel p = new System.Windows.Forms.Panel();
			addbuttons(p);
			//UPGRADE_TODO: Method 'java.awt.Container.add' was converted to 'System.Windows.Forms.ContainerControl.Controls.Add' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtContaineradd_javalangString_javaawtComponent'"
			//UPGRADE_TODO: The following statement was not moved to InitializeComponent. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1307'"
			Controls.Add(p);
		}
		
		//UPGRADE_ISSUE: Class hierarchy differences between 'java.awt.Panel' and 'System.Windows.Forms.Panel' may cause compilation errors. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1186'"
		internal virtual void  addbuttons(System.Windows.Forms.Panel p)
		{
			//UPGRADE_TODO: Method 'java.awt.Container.add' was converted to 'System.Windows.Forms.ContainerControl.Controls.Add' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtContaineradd_javaawtComponent'"
			p.Controls.Add(clear_button);
			//UPGRADE_TODO: Method 'java.awt.Container.add' was converted to 'System.Windows.Forms.ContainerControl.Controls.Add' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtContaineradd_javaawtComponent'"
			p.Controls.Add(delete_button);
			//UPGRADE_TODO: Method 'java.awt.Container.add' was converted to 'System.Windows.Forms.ContainerControl.Controls.Add' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtContaineradd_javaawtComponent'"
			p.Controls.Add(detail_button);
			//UPGRADE_TODO: Method 'java.awt.Container.add' was converted to 'System.Windows.Forms.ContainerControl.Controls.Add' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtContaineradd_javaawtComponent'"
			p.Controls.Add(anim_button);
			//UPGRADE_TODO: Method 'java.awt.Container.add' was converted to 'System.Windows.Forms.ContainerControl.Controls.Add' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtContaineradd_javaawtComponent'"
			p.Controls.Add(random_button);
			//UPGRADE_TODO: Method 'java.awt.Container.add' was converted to 'System.Windows.Forms.ContainerControl.Controls.Add' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtContaineradd_javaawtComponent'"
			p.Controls.Add(update_button);
		}
		
		internal virtual bool handlebuttons(System.Object target)
		{
			if (target == clear_button)
			{
				pts.removeAll(); result = null;
			}
			else if (target == delete_button)
			{
				pts.deleteLast(); result = null;
			}
			else if (target == detail_button)
				detail = !detail;
			else if (target == anim_button)
			{
				animating_Renamed_Field = !animating_Renamed_Field;
			}
			else if (target == random_button)
			{
				pts.random(30, 350, 350); result = null;
			}
			else if (target == update_button)
			{
				if (!pts.Empty)
				{
					CompGeom.Point.reset();
					result = compute(pts);
					System.Console.Out.WriteLine(CompGeom.Point.stats());
				}
			}
			else
				return false;
			SupportClass.GraphicsManager.manager.GetGraphics(this);
			Refresh();
			return true;
		}
		
		//UPGRADE_NOTE: The equivalent of method 'java.awt.Component.action' is not an override method. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1143'"
		//UPGRADE_ISSUE: Class 'java.awt.Event' was not converted. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1000_javaawtEvent'"
		public bool action(Event e, System.Object obj)
		{
			//UPGRADE_ISSUE: Field 'java.awt.Event.target' was not converted. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1000_javaawtEvent'"
			if (e.target is System.Windows.Forms.Button)
			{
				//UPGRADE_ISSUE: Field 'java.awt.Event.target' was not converted. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1000_javaawtEvent'"
				return handlebuttons(e.target);
			}
			return true;
		}
		
		//UPGRADE_NOTE: The equivalent of method 'java.awt.Component.mouseDown' is not an override method. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1143'"
		//UPGRADE_ISSUE: Class 'java.awt.Event' was not converted. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1000_javaawtEvent'"
		public bool mouseDown(Event e, int x, int y)
		{
			System.Drawing.Graphics g = SupportClass.GraphicsManager.manager.GetGraphics(this);
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			g.FillEllipse(SupportClass.GraphicsManager.manager.GetPaint(g), x - 3, y - 3, 6, 6);
			pts.add(x, y);
			result = null;
			//UPGRADE_TODO: Method 'java.awt.Component.repaint' was converted to 'System.Windows.Forms.Control.Refresh' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtComponentrepaint'"
			Refresh();
			return true;
		}
		
		public virtual void  animate(Anim2D pa)
		{
			if (animating())
				pa.register(SupportClass.GraphicsManager.manager.GetGraphics(this));
		}
		
		public System.String GetUserControlInfo()
		{
			return "Computational Geometry demonstration applet\n" + "Jack Snoeyink\nversion 1.0 January 1996";
		}
		
		static public void  makeStandalone(System.String title, PointIOApplet ap, int width, int height)
		{
			//UPGRADE_TODO: Class 'java.awt.Frame' was converted to 'System.Windows.Forms.Form' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtFrame'"
			System.Windows.Forms.Form temp_frame;
			temp_frame = new System.Windows.Forms.Form();
			temp_frame.Text = title;
			System.Windows.Forms.Form f = temp_frame;
			ap.init();
			ap.start();
			//UPGRADE_TODO: Method 'java.awt.Container.add' was converted to 'System.Windows.Forms.ContainerControl.Controls.Add' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtContaineradd_javalangString_javaawtComponent'"
			f.Controls.Add(ap);
			f.Size = new System.Drawing.Size(width, height);
			//UPGRADE_TODO: 'System.Windows.Forms.Application.Run' must be called to start a main form. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1135'"
			f.Show();
		}
		//Example of use in derived class:
		//public static void main(String args[]);
		// { makeStandalone("Jarvis march", new Jarvis(), 350, 350); }
		
		protected override void  OnPaint(System.Windows.Forms.PaintEventArgs g_EventArg)
		{
			System.Drawing.Graphics g = null;
			if (g_EventArg != null)
				g = g_EventArg.Graphics;
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Red);
			pts.draw(g, detail);
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Blue);
			if (result != null)
				result.draw(g, detail);
		}
		
		public abstract Anim2D compute(PointList pl);
		public void  ResizeControl(System.Drawing.Size p)
		{
			this.Width = p.Width;
			this.Height = p.Height;
		}
		public void  ResizeControl(int p2, int p3)
		{
			this.Width = p2;
			this.Height = p3;
		}
		public System.String[][] GetParameterInfo()
		{
			return null;
		}
		public System.String  TempDocumentBaseVar = "";
		public virtual System.Uri DocumentBase
		{
			get
			{
				if (TempDocumentBaseVar == "")
					return new System.Uri("http://127.0.0.1");
				else
					return new System.Uri(TempDocumentBaseVar);
			}
			
		}
		public System.Drawing.Image getImage(System.Uri p4)
		{
			Bitmap TemporalyBitmap = new Bitmap(p4.AbsolutePath);
			return (Image) TemporalyBitmap;
		}
		public System.Drawing.Image getImage(System.Uri p5, System.String p6)
		{
			Bitmap TemporalyBitmap = new Bitmap(p5.AbsolutePath + p6);
			return (Image) TemporalyBitmap;
		}
		public virtual System.Boolean isActive()
		{
			return isActiveVar;
		}
		public virtual void  start()
		{
			isActiveVar = true;
		}
		public virtual void  stop()
		{
			isActiveVar = false;
		}
		private void  CGApplet_StartEventHandler(System.Object sender, System.EventArgs e)
		{
			init();
			start();
		}
		private void  CGApplet_StopEventHandler(System.Object sender, System.EventArgs e)
		{
			stop();
		}
		public virtual String getParameter(System.String paramName)
		{
			return null;
		}
		#region Windows Form Designer generated code
		private void  InitializeComponent()
		{
			this.SuspendLayout();
			this.BackColor = Color.LightGray;
			BackColor = System.Drawing.Color.White;
			ForeColor = System.Drawing.Color.Black;
			this.ResumeLayout(false);
		}
		#endregion
	}
}