namespace Cracks
{
    partial class Cracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Cracker
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Cursor = System.Windows.Forms.Cursors.Cross;
            this.DoubleBuffered = true;
            this.Name = "Cracker";
            this.Size = new System.Drawing.Size(512, 512);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Cracker_Paint);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Cracker_MouseMove);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Cracker_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Cracker_MouseUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cracker_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
