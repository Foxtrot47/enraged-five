using System;

namespace CompGeom
{
	/// <summary>Implements a linear time algorithm for the visiblity polygon of
	/// sp[0] in a given simple polyline. 
	/// </summary>

    public class VisPoly : Anim2D
    {
        private PointList sp; // simple polygon
        private Point org; // origin of visibility polygon
        private int[] v; // stack holds indices of visibility polygon
        private int[] vtype; // types of vp vertices
        private int vp; // stack pointer to top element

        public VisPoly(PointList pl)
        {
            int n = pl.number();
            sp = pl;
            v = new int[n];
            vtype = new int[n];
            vp = -1;
        }


        /// <summary>Build the visibility polygon for a given vertex </summary>

        public virtual void build(int orgIndex)
        {
            Homog edgej; // during the loops, this is the line p(j-1)->p(j)
            org = sp.p(orgIndex);
            vp = -1;
            int j = orgIndex;
            push(j++, RWALL); // org & p[1] on VP
            do
            {
                // loop always pushes pj and increments j.
                push(j++, RWALL);
                if (j >= sp.number() + orgIndex)
                    return; // we are done.
                edgej = p(j - 1).meet(p(j));
                if (!edgej.right(org))
                    continue; // easiest case: add edge to VP.
                // else pj backtracks, we must determine where
                if (!edgej.left(p(j - 2)))
                {
                    // pj is above last VP edge
                    j = exitRBay(j, top(), Homog.INFINITY);
                    push(j++, RLID); continue; // exits bay; push next two
                }

                saveLid(); // else pj below top edge; becomes lid or pops
                do
                {
                    // pj hides some of VP; break loop when can push pj.
                    //System.out.print("do j:"+j+" lid:"+LLidIdx+" "+RLidIdx+toString());
                    if (!Point.right(org, top(), p(j)))
                    {
                        // saved lid ok so far...
                        if (Point.right(p(j), p(j + 1), org))
                            j++;
                        // continue to hide
                        else if (!edgej.right(p(j + 1)))
                        {
                            // or turns up into bay
                            j = exitLBay(j, p(j), p(LLidIdx).meet(p(LLidIdx - 1))) + 1;
                        }
                        else
                        {
                            // or turns down; put saved lid back & add new VP edge 
                            restoreLid(); push(j++, LWALL); break;
                        }
                        edgej = p(j - 1).meet(p(j)); // loop continues with new j; update edgej
                    }
                    // lid is no longer visible
                    else if (!edgej.left(top()))
                    {
                        // entered RBay, must get out
                        assert((RLidIdx != NOTSAVED), "no RLid saved " + LLidIdx + RLidIdx + ToString());
                        j = exitRBay(j, top(), edgej.neg()); // exits bay;
                        push(j++, RLID); break;
                    }
                    // found new visible lid to add to VP.
                    else
                        saveLid(); // save new lid from VP; continue to hide VP.
                }
                while (true);
                //System.out.print("exit j:"+j+" lid:"+LLidIdx+" "+RLidIdx+toString());
            }
            while (j < sp.number() + orgIndex); // don't push origin again.
        }

        public static void assert(bool flag, System.String s)
        {
            if (!flag)
                System.Console.Out.WriteLine("ASSERT FAIL: " + s);
        }

        public virtual bool empty()
        {
            return vp < 0;
        }

        public virtual int popVisible()
        {
            while ((vtype[vp] == RLID) || (vtype[vp] == LLID))
                vp--;
            return v[vp--];
        }

        /// <summary>helper functions </summary>

        private Point p(int j)
        {
            return sp.pn(j);
        }

        private Point top()
        {
            return p(v[vp]);
        }

        private Point ntop()
        {
            return p(v[vp - 1]);
        }

        private void push(int idx, int t)
        {
            v[++vp] = idx; vtype[vp] = t;
        }

        /// <summary>exit a bay: proceed from j, j++, .. until exiting the bay defined
        /// to the right (or left for exitLBay) of the line from org through
        /// point bot to line lid.  Return j such that (j,j+1) forms new lid
        /// of this bay.  Assumes that pl.p(j) is not left (right) of the line
        /// org->bot.  
        /// </summary>
        internal virtual int exitRBay(int j, Point bot, Homog lid)
        {
            int wn = 0; // winding number
            Homog mouth = org.meet(bot);
            bool lastLeft, currLeft = false;
            while (++j < 3 * sp.number())
            {
                lastLeft = currLeft;
                currLeft = mouth.left(p(j));
                if ((currLeft != lastLeft) && (Point.left(p(j - 1), p(j), org) == currLeft))
                {
                    if (!currLeft)
                        wn--;
                    else if (wn++ == 0)
                    {
                        // on 0->1 transitions, check window
                        Homog edge = p(j - 1).meet(p(j));
                        if (edge.left(bot) && !Homog.cw(mouth, edge, lid))
                            return j - 1; // j exits window!
                    }
                }
            }

            System.Console.Out.WriteLine("ERROR: We never exited RBay " + bot + lid + wn + "\n" + ToString());
            return j;
        }

        internal virtual int exitLBay(int j, Point bot, Homog lid)
        {
            int wn = 0; // winding number
            Homog mouth = org.meet(bot);
            bool lastRight, currRight = false; // called with !right(org,bot,pj)
            while (++j < 3 * sp.number())
            {
                lastRight = currRight;
                currRight = mouth.right(p(j));
                if ((currRight != lastRight) && (Point.right(p(j - 1), p(j), org) == currRight))
                {
                    if (!currRight)
                        wn++;
                    else if (wn-- == 0)
                    {
                        // on 0->-1 transitions, check window
                        Homog edge = p(j - 1).meet(p(j));
                        if (edge.right(bot) && !Homog.cw(mouth, edge, lid))
                            return j - 1; // j exits window!
                    }
                }
            }

            System.Console.Out.WriteLine("ERROR: We never exited LBay " + bot + lid + wn + "\n" + ToString());
            return j;
        }

        /// <summary>polygon vertex types:    LLID-------------------RLID
        /// |            |
        /// |            |
        /// --------LWALL        RWALL---------
        /// </summary>
        internal const int RLID = 0;
        internal const int LLID = 1;
        internal const int RWALL = 2;
        internal const int LWALL = 3;

        //UPGRADE_NOTE: Final was removed from the declaration of 'vtypeLUT '. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1003'"
        internal static readonly System.Drawing.Color[] vtypeLUT = new System.Drawing.Color[] { System.Drawing.Color.Yellow, System.Drawing.Color.Black, System.Drawing.Color.Green, System.Drawing.Color.Blue };
        //UPGRADE_NOTE: Final was removed from the declaration of 'vtypeString'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1003'"
        internal static readonly System.String[] vtypeString = new System.String[] { "RL ", "LL ", "RW ", "LW " };

        /// <summary>Proceedures to keep the lid above the current vertex on top of the stack, 
        /// leaving the top() as a vertex of the VP that is also a vertex of sp.
        /// These use global status variables to keep the code  for build() cleaner.
        /// </summary>
        internal const int NOTSAVED = -1; // flag for when we don't have a RLidIdx
        private int LLidIdx, RLidIdx;
        private void saveLid()
        {
            //System.out.print("saveLid " + toString());
            if (vtype[vp] == LWALL)
                vp--; // for LWALL, lid is previous two
            LLidIdx = v[vp--];
            if (vtype[vp] == RLID)
                RLidIdx = v[vp--];
            // if not RLID, just leave on top().
            else
                RLidIdx = NOTSAVED;
        }

        private void restoreLid()
        {
            //System.out.print("restoreLid "+LLidIdx+","+RLidIdx+ toString());
            if (RLidIdx != NOTSAVED)
                push(RLidIdx, RLID);
            push(LLidIdx, LLID);
        }

        public override void draw(System.Drawing.Graphics g, bool label)
        {
            sp.drawPolygon(g, sp.number(), System.Drawing.Color.Red, System.Drawing.Color.Red);
            if (empty())
                return;

            SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
            org.drawDot(g, 5);
            Point last = p(v[vp]);
            for (int i = 0; i <= vp; i++)
            {
                SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Blue);
                if (vtype[i] == RLID)
                {
                    Homog q = org.meet(last).meet(p(v[i]).meet(p(v[i + 1])));
                    g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), last.x(), last.y(), q.x(), q.y());
                }
                else if (vtype[i] == LWALL)
                {
                    Homog q = org.meet(p(v[i])).meet(p(v[i - 2]).meet(p(v[i - 1])));
                    g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), q.x(), q.y(), p(v[i]).x(), p(v[i]).y());
                }
                else
                    g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), last.x(), last.y(), p(v[i]).x(), p(v[i]).y());
                last = p(v[i]);
                SupportClass.GraphicsManager.manager.SetColor(g, vtypeLUT[vtype[i]]);
                last.drawDot(g);
            }
        }

        public override System.String ToString()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append("VP " + vp + ": ");
            for (int i = 0; i <= vp; i++)
                s.Append(v[i] + vtypeString[vtype[i]]);
            s.Append("\nVP:");
            for (int i = 0; i <= vp; i++)
            {
                s.Append(p(v[i]).ToString());
            }
            s.Append("\nSP:");
            s.Append(sp.ToString());
            s.Append("\n");
            return s.ToString();
        }
    }
}
