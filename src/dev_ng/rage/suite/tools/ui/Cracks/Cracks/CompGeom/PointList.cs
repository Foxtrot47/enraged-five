using System;

namespace CompGeom
{
    public class PointList : Anim2D
    {
        public virtual bool Empty
        {
            get
            {
                return counter <= 0;
            }
        }

        public PointList(int n)
        {
            counter = 0;
            p_Renamed_Field = new Point[n];
        }

        public PointList() : this(100)
        {
        }

        /// <summary>copy array/share points constructor </summary>
        public PointList(PointList pl) : this(pl.counter)
        {
            counter = pl.counter;
            for (int i = 0; i < counter; i++)
                p_Renamed_Field[i] = pl.p(i); /* share points */
        }

        private Point[] p_Renamed_Field;
        private int counter;

        public Point p(int i)
        {
            return p_Renamed_Field[i];
        }

        public Point pn(int i)
        {
            return p_Renamed_Field[i % number()];
        }

        public int number()
        {
            return counter;
        }

        protected internal void setnumber(int c)
        {
            counter = c;
        }

        public void truncate(int c)
        {
            if (c > p_Renamed_Field.Length)
                throw new System.IndexOutOfRangeException("PointList too short for truncation");
            else
                setnumber(c);
        }

        protected internal virtual void dr()
        {
            if (animating())
                draw(animGraphics, true);
        }

        public virtual void add(int x, int y)
        {
            p_Renamed_Field[counter] = new Point(x, y);
            if ((counter <= 0) || (!p_Renamed_Field[counter - 1].equals(p_Renamed_Field[counter])))
                counter++;
            dr();
        }

        public virtual void add(Point pt)
        {
            p_Renamed_Field[counter++] = pt; dr();
        }

        public void swap(int i, int j)
        {
            Point tmp = p_Renamed_Field[i]; p_Renamed_Field[i] = p_Renamed_Field[j]; p_Renamed_Field[j] = tmp; dr();
        }

        public virtual Point last()
        {
            return p_Renamed_Field[counter - 1];
        }

        public virtual Point nlast()
        {
            return p_Renamed_Field[counter - 2];
        }

        public virtual Point deleteLast()
        {
            if (!Empty)
                counter--;
            return p_Renamed_Field[counter];
        }

        public virtual int delete(int i)
        {
            swap(i, counter - 1); deleteLast(); return i;
        }

        public virtual void removeAll()
        {
            counter = 0;
        }

        public virtual PointList random(int n, double width, double height)
        {
            System.Random rnd = new System.Random();
            counter = n;
            if (p_Renamed_Field.Length < n)
                p_Renamed_Field = new Point[n];
            for (int i = 0; i < n; i++)
                p_Renamed_Field[i] = new Point(rnd.NextDouble() * (width - 5), (-rnd.NextDouble()) * (height - 15));
            return this;
        }

        public int leftmost()
        {
            int min = 0;
            for (int i = 1; i < number(); i++)
                /* Find leftmost point */
                if (p(i).less(p(min)))
                    min = i;
            if (animating())
                p(min).drawCirc(animGraphics, 5);
            dr();
            return min;
        }

        public int extreme(Point dir)
        {
            return extreme(dir, 0, number());
        }

        public int extreme(Point dir, int m, int limit)
        {
            double di, dm = p(m).dot(dir);
            for (int i = m + 1; i < limit; i++)
            {
                /* Find leftmost point */
                di = p(i).dot(dir);
                if ((di > dm) || ((di == dm) && (dir.perpdot(p(m), p(i)) > 0)))
                {
                    dm = di; m = i;
                }
            }
            if (animating())
            {
                dr();
                p(m).drawCirc(animGraphics, 5);
                dir.drawVector(animGraphics, p(m));
            }
            return m;
        }

        public virtual void insertionsort()
        {
            int j;
            Point tmp;
            for (int i = 1; i < number(); i++)
            {
                tmp = p(i);
                if (tmp.less(p(0)))
                {
                    for (j = i - 1; j >= 0; j--)
                        p_Renamed_Field[j + 1] = p(j);
                    p_Renamed_Field[0] = tmp;
                }
                else
                {
                    j = i - 1;
                    while (tmp.less(p(j)))
                    {
                        p_Renamed_Field[j + 1] = p(j); j--;
                    }
                    p_Renamed_Field[j + 1] = tmp;
                }
            }
            dr();
        }

        /* Quick Sort implementation
        */
        public virtual void quicksort()
        {
            quickSort(0, number() - 1);
        }

        private void quickSort(int left, int right)
        {
            int leftIndex = left;
            int rightIndex = right;
            Point partionElement;
            if (right > left)
            {

                /* Arbitrarily establishing partition element as the midpoint of
                * the array.
                */
                partionElement = p((left + right) / 2);

                // loop through the array until indices cross 
                while (leftIndex <= rightIndex)
                {
                    /* find the first element that is greater than or equal to 
                    * the partionElement starting from the leftIndex.
                    */
                    while ((leftIndex < right) && p(leftIndex).less(partionElement))
                        ++leftIndex;

                    /* find an element that is smaller than or equal to 
                    * the partionElement starting from the rightIndex.
                    */
                    while ((rightIndex > left) && partionElement.less(p(rightIndex)))
                        --rightIndex;

                    // if the indexes have not crossed, swap
                    if (leftIndex <= rightIndex)
                    {
                        swap(leftIndex, rightIndex);
                        ++leftIndex;
                        --rightIndex;
                    }
                }

                /* If the right index has not reached the left side of array
                * must now sort the left partition.
                */
                if (left < rightIndex)
                    quickSort(left, rightIndex);

                /* If the left index has not reached the right side of array
                * must now sort the right partition.
                */
                if (leftIndex < right)
                    quickSort(leftIndex, right);
            }
            dr();
        }

        public void drawPolyline(System.Drawing.Graphics g, int n)
        {
            if (n > 1)
                for (int i = 1; i < n; i++)
                    g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), p(i - 1).x(), p(i - 1).y(), p(i).x(), p(i).y());
        }

        public void drawPolygon(System.Drawing.Graphics g, int n, System.Drawing.Color shapeColor, System.Drawing.Color polyColor)
        {
            if (n > 2)
            {
                SupportClass.GraphicsManager.manager.SetColor(g, shapeColor);
                g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), p(n - 1).x(), p(n - 1).y(), p(0).x(), p(0).y());
            }

            SupportClass.GraphicsManager.manager.SetColor(g, polyColor);
            drawPolyline(g, n);
        }

        public override void draw(System.Drawing.Graphics g, bool label)
        {
            SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Red);
            for (int i = 0; i < number(); i++)
                g.FillEllipse(SupportClass.GraphicsManager.manager.GetPaint(g), p(i).x() - 2, p(i).y() - 2, 4, 4);
            if (label)
                for (int i = 0; i < number(); i++)
                {
                    //UPGRADE_TODO: Method 'java.awt.Graphics.drawString' was converted to 'System.Drawing.Graphics.DrawString' which has a different behavior. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1073_javaawtGraphicsdrawString_javalangString_int_int'"
                    g.DrawString(System.Convert.ToString(i), SupportClass.GraphicsManager.manager.GetFont(g), SupportClass.GraphicsManager.manager.GetBrush(g), p(i).x(), p(i).y() - SupportClass.GraphicsManager.manager.GetFont(g).GetHeight());
                }
        }

        public override System.String ToString()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            for (int i = 0; i < number(); i++)
            {
                s.Append(p(i).ToString());
            }
            return s.ToString();
        }
    }
}
