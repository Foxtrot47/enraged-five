﻿using System;

namespace Rockstar.MoVE.Services
{
    public interface IConfigMetadata
    {
        string Section { get; }
        Type Type { get; }
    }
}