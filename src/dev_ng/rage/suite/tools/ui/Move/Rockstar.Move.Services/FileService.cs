﻿using System;
using System.ComponentModel.Composition;
using System.IO;

namespace Rockstar.MoVE.Services
{
    [Export(typeof(IFileService))]
    internal class FileService : IFileService
    {
        static class Const 
        { 
            public static string Untilted = "Untitled";
            public static string Extension = ".mtf";
        }

        static internal readonly string ApplicationFolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), Resources.ApplicationFolderName);

        public TextReader CreateStreamReader(string filename)
        {
            string path = Path.Combine(ApplicationFolderPath, filename);
            return new StreamReader(path);
        }

        public TextWriter CreateStreamWriter(string filename)
        {
            return new StreamWriter(Path.Combine(ApplicationFolderPath, filename));
        }

        public bool Exists(string filename)
        {
            return File.Exists(Path.Combine(ApplicationFolderPath, filename));
        }

        public string GetApplicationFolderPath()
        {
            return ApplicationFolderPath;
        }

        public string FindUntiltedFilename()
        {
            string filename = String.Concat(Const.Untilted, Const.Extension); ;

            int i = 1;
            while (Exists(filename))
            {
                filename = String.Concat(String.Concat(Const.Untilted, i), Const.Extension);
                ++i;
            }

            return filename;
        }
    }
}