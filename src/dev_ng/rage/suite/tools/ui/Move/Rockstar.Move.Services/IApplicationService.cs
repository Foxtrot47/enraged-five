﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Input;

using Move.Utils;

using AvalonDock;

namespace Rockstar.MoVE.Services
{
    using KeyDownEventHandler = EventHandler<SimpleEventArgs<KeyDownMessage>>;

    public delegate void CurrentDocumentChangedHandler();
    public delegate void NewCommandExecutedHandler();
    public delegate void SaveCommandExecutedHandler();
    public delegate void SaveAsCommandExecutedHandler(string filename);
    public delegate void SaveSelectionCommandExecutedHandler(string filename);
    public delegate object OpenCommandExecutedHandler(string filename);
    public delegate void CloseCommandExecutedHandler();
    public delegate void ExportCommandExecutedHandler(string filename, string nodemapPath, bool build, bool prototype, bool preview);
    public delegate void RebuildAllCommandExecutedHandler(string[] filenames, bool build);
    public delegate void SaveDirtyDocumentsHandler();
    public delegate void SaveDirtyClipsHandler();
    public delegate List<string> FindDirtyDocumentsHandler();
    public delegate List<string> FindDirtyClipsHandler();
    public delegate void OpenSaveAllCommandExecutedHandler();
    public delegate void ExportAllCommandExecutedHandler(bool buildRpf);

    public class KeyDownMessage 
    {
    }

    public interface IApplicationService
    {
        event CurrentDocumentChangedHandler CurrentDocumentChanged;
        DocumentContent CurrentDocument { get; set; }

        event NewCommandExecutedHandler NewCommandExecuted;
        void ExecuteNewCommand();

        event SaveCommandExecutedHandler SaveCommandExecuted;
        void ExecuteSaveCommand();
        bool ExecuteSaveCommandWithResult();

        event SaveAsCommandExecutedHandler SaveAsCommandExecuted;
        void ExecuteSaveAsCommand();

        event SaveSelectionCommandExecutedHandler SaveSelectionCommandExecuted;
        void ExecuteSaveSelectionCommand();

        event OpenCommandExecutedHandler OpenCommandExecuted;
        void ExecuteOpenCommand();

        event CloseCommandExecutedHandler CloseCommandExecuted;
        void ExecuteCloseCommand();

        void OpenFile(string filename);

        object OpenDatabase(string filename);	// rsTODO: Do this through the file service instead?

        event ExportCommandExecutedHandler ExportCommandExecuted;
        void ExecuteExportCommand(bool build, bool prototype, bool preview);

        event RebuildAllCommandExecutedHandler RebuildAllCommandExecuted;
        void ExecuteRebuildAllCommand(bool build);

        event SaveDirtyDocumentsHandler SaveDirtyDocumentsExecuted;
        void SaveDirtyDocuments();

        event SaveDirtyClipsHandler SaveDirtyClipsExecuted;
        void SaveDirtyClips();

        event FindDirtyDocumentsHandler FindDirtyDocumentsExecuted;
        List<string> FindDirtyDocuments();

        event FindDirtyClipsHandler FindDirtyClipsExecuted;
        List<string> FindDirtyClips();

        void LoadRecentFiles(string recentFilesPath);
        void SaveRecentFiles();
        StringCollection GetRecentFiles();

        void LoadFavoriteDirectories(string favoriteDirectoriesPath);
        void SaveFavoriteDirectories();

        void RegisterKeyDownHandler(Key key, KeyDownEventHandler handler);
        void UnregisterKeyDownHandler(Key key, KeyDownEventHandler handler);
        void RegisterKeyGestureDownHandler(KeyGesture key, KeyDownEventHandler handler);
        void UnregisterKeyGestureDownHandler(KeyGesture key, KeyDownEventHandler handler);
        void OnKeyDown(KeyEventArgs e);

        event OpenSaveAllCommandExecutedHandler OpenSaveAllCommandExecuted;
        void OpenSaveAllNetworks();

        event ExportAllCommandExecutedHandler ExportAllCommandExecuted;
        void ExportAllNetworks(bool buildRpf);
    }
}
