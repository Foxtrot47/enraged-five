﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Windows.Input;
using System.Xml.Serialization;
using System.IO;

using Microsoft.Win32;

using AvalonDock;

using Rockstar.MoVE.Framework;
using Move.Utils;

namespace Rockstar.MoVE.Services
{
    using KeyDownEventHandler = EventHandler<SimpleEventArgs<KeyDownMessage>>;
    using RSG.Base.Configuration;

    [Export(typeof(IApplicationService))]
    internal class ApplicationService : IApplicationService
    {
        private string OpenFilePath = null;
        private string ExportFilePath = null;

        private const int NumberOfRecentFiles = 5;

        private static StringCollection recentFiles = new StringCollection();

        private static StringCollection RecentFiles
        {
            get { return recentFiles; }
        }

        private string RecentFilesPath = null;

        private string ExportInitialDirectory = null;
        private string NodemapInitialDirectory = null;

        public void LoadRecentFiles(string recentFilesPath)
        {
            RecentFilesPath = recentFilesPath;

            if (File.Exists(RecentFilesPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(StringCollection));
                using (TextReader reader = new StreamReader(RecentFilesPath))
                {
                    recentFiles = (StringCollection)serializer.Deserialize(reader);
                }

                // Remove files from the Recent Files list that no longer exist.
                for (int i = 0; i < recentFiles.Count; ++i)
                {
                    if (!File.Exists(recentFiles[i]))
                        recentFiles.RemoveAt(i);
                }

                while (recentFiles.Count > NumberOfRecentFiles)
                    recentFiles.RemoveAt(NumberOfRecentFiles);
            }
        }

        public void SaveRecentFiles()
        {
            if (!Directory.Exists(FileService.ApplicationFolderPath))
                Directory.CreateDirectory(FileService.ApplicationFolderPath);

            XmlSerializer serializer = new XmlSerializer(typeof(StringCollection));
            using (TextWriter writer = new StreamWriter(RecentFilesPath))
            {
                serializer.Serialize(writer, recentFiles);
            }
        }

        public StringCollection GetRecentFiles()
        {
            return RecentFiles;
        }

        private string FavoriteDirectoriesPath = null;

        public void LoadFavoriteDirectories(string favoriteDirectoriesPath)
        {
            //  Store the path to the favorite directories XML file
            FavoriteDirectoriesPath = favoriteDirectoriesPath;

            if (File.Exists(FavoriteDirectoriesPath))
            {
                //  Read the favorite directories from an XML file
                StringCollection dirs = new StringCollection();
                XmlSerializer serializer = new XmlSerializer(typeof(StringCollection));
                using (TextReader reader = new StreamReader(FavoriteDirectoriesPath))
                {
                    dirs = (StringCollection)serializer.Deserialize(reader);
                }

                //  Set the favorite directories back up
                OpenFilePath = dirs[0];
                ExportFilePath = dirs[1];
            }
        }

        public void SaveFavoriteDirectories()
        {
            //  Create the application folder if we dont already have one
            if (!Directory.Exists(FileService.ApplicationFolderPath))
                Directory.CreateDirectory(FileService.ApplicationFolderPath);
            //  Create a list of strings to contain the favorite directories
            StringCollection dirs = new StringCollection();
            dirs.Add(OpenFilePath);
            dirs.Add(ExportFilePath);
            //  Write out the directories to a XML file
            XmlSerializer serializer = new XmlSerializer(typeof(StringCollection));
            using (TextWriter writer = new StreamWriter(FavoriteDirectoriesPath))
            {
                serializer.Serialize(writer, dirs);
            }
        }

        public event CurrentDocumentChangedHandler CurrentDocumentChanged;

        DocumentContent _CurrentDocument = null;
        public DocumentContent CurrentDocument
        {
            get
            {
                return _CurrentDocument;
            }
            set
            {
                if (value != _CurrentDocument)
                {
                    _CurrentDocument = value;
                    IPersistentContent content = _CurrentDocument as IPersistentContent;
                    if (content != null)
                    {
                        if (!String.IsNullOrEmpty(content.FullyQualifiedFilename))
                        {
                            //  Remove any duplicates
                            recentFiles.Remove(content.FullyQualifiedFilename);
                            //  Add the current document to the recent files list
                            recentFiles.Insert(0, content.FullyQualifiedFilename);
                        }
                    }

                    if (CurrentDocumentChanged != null)
                    {
                        CurrentDocumentChanged();
                    }
                }
            }
        }

        public event NewCommandExecutedHandler NewCommandExecuted;
        public void ExecuteNewCommand()
        {
            if (NewCommandExecuted != null)
            {
                NewCommandExecuted();
            }
        }

        public event SaveCommandExecutedHandler SaveCommandExecuted;
        public void ExecuteSaveCommand()
        {
            IPersistentContent content = CurrentDocument as IPersistentContent;
            if (content != null)
            {
                if (String.IsNullOrEmpty(content.FullyQualifiedFilename))
                {
                    SaveFileDialog dlg = new SaveFileDialog();
                    dlg.InitialDirectory = FileService.ApplicationFolderPath;
                    dlg.Filter = "MoVE Files (*.mxtf)|*.mxtf|All Files (*.*)|*.*";
                    dlg.Title = "Save";
                    dlg.DefaultExt = ".mxtf";
                    dlg.ShowDialog();

                    if (!String.IsNullOrEmpty(dlg.FileName))
                    {
                        if (SaveAsCommandExecuted != null)
                        {
                            SaveAsCommandExecuted(dlg.FileName);
                        }
                    }
                }
                else
                {
                    if (SaveCommandExecuted != null)
                    {
                        SaveCommandExecuted();
                    }
                }
            }
        }

        public bool ExecuteSaveCommandWithResult()
        {
            IPersistentContent content = CurrentDocument as IPersistentContent;
            if (content != null)
            {
                if (String.IsNullOrEmpty(content.FullyQualifiedFilename))
                {
                    SaveFileDialog dlg = new SaveFileDialog();
                    dlg.InitialDirectory = FileService.ApplicationFolderPath;
                    dlg.Filter = "MoVE Files (*.mxtf)|*.mxtf|All Files (*.*)|*.*";
                    dlg.Title = "Save";
                    dlg.DefaultExt = ".mxtf";
                    dlg.ShowDialog();

                    if (!String.IsNullOrEmpty(dlg.FileName))
                    {
                        if (SaveAsCommandExecuted != null)
                        {
                            SaveAsCommandExecuted(dlg.FileName);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (SaveCommandExecuted != null)
                    {
                        SaveCommandExecuted();
                    }
                }

                return true;
            }

            return false;
        }

        public event SaveAsCommandExecutedHandler SaveAsCommandExecuted;
        public void ExecuteSaveAsCommand()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = FileService.ApplicationFolderPath;
            dlg.Filter = "MoVE Files (*.mxtf)|*.mxtf|All Files (*.*)|*.*";
            dlg.Title = "Save As";
            dlg.DefaultExt = ".mxtf";
            dlg.ShowDialog();

            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                if (SaveAsCommandExecuted != null)
                {
                    SaveAsCommandExecuted(dlg.FileName);
                }
            }
        }

        public event SaveSelectionCommandExecutedHandler SaveSelectionCommandExecuted;
        public void ExecuteSaveSelectionCommand()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = FileService.ApplicationFolderPath;
            dlg.Filter = "MoVE Files (*.mxtf)|*.mxtf|All Files (*.*)|*.*";
            dlg.Title = "Save As";
            dlg.DefaultExt = ".mxtf";
            dlg.ShowDialog();

            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                if (SaveSelectionCommandExecuted != null)
                {
                    SaveSelectionCommandExecuted(dlg.FileName);
                }
            }
        }

        public event OpenCommandExecutedHandler OpenCommandExecuted;
        public void ExecuteOpenCommand()
        {
            OpenFileDialog dlg = new OpenFileDialog();

            if (OpenFilePath == null)
            {
                OpenFilePath = FileService.ApplicationFolderPath;
            }

            dlg.InitialDirectory = OpenFilePath;
            dlg.Filter = "MoVE Files (*.mxtf)|*.mxtf|(*.mtf)|*.mtf|All Files (*.*)|*.*";
            dlg.Title = "Open";
            dlg.ShowDialog();

            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                if (OpenCommandExecuted != null)
                {
                    OpenFilePath = dlg.FileName.Substring(0, dlg.FileName.LastIndexOf('\\'));
                    OpenCommandExecuted(dlg.FileName);
                }   
            }
        }

        public event CloseCommandExecutedHandler CloseCommandExecuted;
        public void ExecuteCloseCommand()
        {
            CloseCommandExecuted();
        }

        public void OpenFile(string filename)
        {
            if (!String.IsNullOrEmpty(filename))
            {
                if (OpenCommandExecuted != null)
                {
                    OpenCommandExecuted(filename);
                }
            }
        }

        // rsTODO: Do this through the File service instead?
        public object OpenDatabase(string filename)
        {
            if (OpenCommandExecuted != null)
            {
                return OpenCommandExecuted(filename);
            }
            return null;
        }

        public event ExportCommandExecutedHandler ExportCommandExecuted;
        public void ExecuteExportCommand(bool build, bool prototype, bool preview)
        {
            if (ExportCommandExecuted == null)
            {
                return;
            }

            IConfig config = ConfigFactory.CreateConfig();
            string nodeMapDirectory = ContentConfig.NodeMapExportPath;
            nodeMapDirectory = Path.Combine(nodeMapDirectory, "anim", "move", "nodemaps");
            if (string.IsNullOrEmpty(ExportInitialDirectory))
            {
                ExportInitialDirectory = Path.Combine(config.Project.DefaultBranch.Export, "anim", "move_networks");
            }
            if (string.IsNullOrEmpty(NodemapInitialDirectory))
            {
                NodemapInitialDirectory = nodeMapDirectory;
            }

            IPersistentContent content = CurrentDocument as IPersistentContent;
            if (content != null && !string.IsNullOrEmpty(content.FullyQualifiedFilename))
            {
                Microsoft.Win32.SaveFileDialog dlg = new SaveFileDialog();
                string name = Path.GetFileName(content.FullyQualifiedFilename);
                
                dlg.FileName = Path.ChangeExtension(name, "imvf");
                dlg.Filter = "Moves Intermediate File Format (*.imvf)|*.imvf";
                dlg.CustomPlaces.Add(new FileDialogCustomPlace(Path.Combine(config.Project.DefaultBranch.Export, "anim", "move_networks")));
                dlg.InitialDirectory = ExportInitialDirectory;

                if ((bool)dlg.ShowDialog())
                {
                    string exportPath = dlg.FileName;

                    Microsoft.Win32.SaveFileDialog dlg2 = new SaveFileDialog();

                    string filename = Path.GetFileNameWithoutExtension(exportPath);

                    string nodemapFileName = filename + "_nodemap";

                    dlg2.FileName = Path.ChangeExtension(nodemapFileName, "xml");
                    dlg2.Filter = "Xml files (*.xml)|*.xml";
                    dlg2.CustomPlaces.Add(new FileDialogCustomPlace(nodeMapDirectory));
                    dlg2.InitialDirectory = NodemapInitialDirectory;

                    if ((bool)dlg2.ShowDialog())
                    {
                        string nodemapPath = dlg2.FileName;
                        ExportCommandExecuted(exportPath, nodemapPath, build, prototype, preview);

                        if(File.Exists(exportPath))
                        {
                            ExportInitialDirectory = Path.GetDirectoryName(exportPath);
                        }
                        if (File.Exists(nodemapPath))
                        {
                            NodemapInitialDirectory = Path.GetDirectoryName(nodemapPath);
                        }
                    }
                }
            }
        }

        public event RebuildAllCommandExecutedHandler RebuildAllCommandExecuted;
        public void ExecuteRebuildAllCommand(bool build)
        {
            if (RebuildAllCommandExecuted == null)
            {
                return;
            }

            IConfig config = ConfigFactory.CreateConfig();
            string directory = Path.Combine(config.Project.DefaultBranch.Assets, "anim", "move", "networks");
            if (!Directory.Exists(directory))
            {
                return;
            }

            RebuildAllCommandExecuted(Directory.GetFiles(directory, "*.mxtf", SearchOption.AllDirectories), build);
        }

        private bool PromptUserForExportPathname(out string exportPathname)
        {
            exportPathname = "";
            SaveFileDialog dlg = new SaveFileDialog();
            IConfig config = ConfigFactory.CreateConfig();

            dlg.InitialDirectory = Path.Combine(config.Project.DefaultBranch.Export, "anim", "move_networks");
            dlg.Filter = "MoVE Intermedate Files (*.imvf)|*.imvf|All Files (*.*)|*.*";
            dlg.Title = "Export";
            dlg.DefaultExt = ".imvf";

            IPersistentContent content = CurrentDocument as IPersistentContent;
            if (content != null)
            {
                if (!String.IsNullOrEmpty(content.FullyQualifiedFilename))
                {
                    string fileName = content.FullyQualifiedFilename.Substring(content.FullyQualifiedFilename.LastIndexOf('\\') + 1);
                    fileName = fileName.Substring(0, fileName.LastIndexOf("."));
                    dlg.FileName = fileName;
                }
            }

            if (dlg.ShowDialog() == true && !String.IsNullOrEmpty(dlg.FileName))
            {
                exportPathname = dlg.FileName;
                return true;
            }

            return false;
        }

        public event SaveDirtyDocumentsHandler SaveDirtyDocumentsExecuted;
        public void SaveDirtyDocuments()
        {
            if (SaveDirtyDocumentsExecuted != null)
            {
                SaveDirtyDocumentsExecuted();
            }
        }

        public event SaveDirtyClipsHandler SaveDirtyClipsExecuted;
        public void SaveDirtyClips()
        {
            if (SaveDirtyClipsExecuted != null)
            {
                SaveDirtyClipsExecuted();
            }
        }

        public event FindDirtyDocumentsHandler FindDirtyDocumentsExecuted;
        public List<string> FindDirtyDocuments()
        {
            if (FindDirtyDocumentsExecuted != null)
            {
                return FindDirtyDocumentsExecuted();
            }

            return null;
        }

        public event FindDirtyClipsHandler FindDirtyClipsExecuted;
        public List<string> FindDirtyClips()
        {
            if (FindDirtyClipsExecuted != null)
            {
                return FindDirtyClipsExecuted();
            }

            return null;
        }


        Dictionary<Key, List<KeyDownEventHandler>> _KeyDownEventHandlers = new Dictionary<Key, List<KeyDownEventHandler>>();
        Dictionary<KeyGesture, List<KeyDownEventHandler>> _KeyGestureDownEventHandlers = new Dictionary<KeyGesture, List<KeyDownEventHandler>>();
        
        public bool TryGetMatch(KeyEventArgs e, out List<KeyDownEventHandler> handlers)
        {
            handlers = new List<KeyDownEventHandler>();
            foreach (KeyValuePair<KeyGesture, List<KeyDownEventHandler>> pair in _KeyGestureDownEventHandlers)
            {
                
                if(pair.Key.Matches(null, e))
                {
                    handlers = pair.Value;
                    return true;
                }
            }
            return false;
        }
        
        public void OnKeyDown(KeyEventArgs e)
        {
            List<KeyDownEventHandler> handlers = null;
            if (_KeyDownEventHandlers.TryGetValue(e.Key, out handlers))
            {
                foreach (var handler in handlers)
                {
                    handler.Raise(this, new KeyDownMessage());
                }
            }
            else if (TryGetMatch(e, out handlers))
            {
                foreach (var handler in handlers)
                {
                    handler.Raise(this, new KeyDownMessage());
                }
            }
        }

        public void RegisterKeyDownHandler(Key key, KeyDownEventHandler handler)
        {
            List<KeyDownEventHandler> handlers = null;
            if (_KeyDownEventHandlers.TryGetValue(key, out handlers))
            {
                if (!handlers.Contains(handler))
                {
                    handlers.Add(handler);
                }
            }
            else
            {
                handlers = new List<KeyDownEventHandler>();
                handlers.Add(handler);
                _KeyDownEventHandlers.Add(key, handlers);
            }
        }

        public void UnregisterKeyDownHandler(Key key, KeyDownEventHandler handler)
        {
            List<KeyDownEventHandler> handlers = null;
            if (_KeyDownEventHandlers.TryGetValue(key, out handlers))
            {
                handlers.Remove(handler);
            }
        }

        public void RegisterKeyGestureDownHandler(KeyGesture key, KeyDownEventHandler handler)
        {
            List<KeyDownEventHandler> handlers = null;
            if (_KeyGestureDownEventHandlers.TryGetValue(key, out handlers))
            {
                if (!handlers.Contains(handler))
                {
                    handlers.Add(handler);
                }
            }
            else
            {
                handlers = new List<KeyDownEventHandler>();
                handlers.Add(handler);
                _KeyGestureDownEventHandlers.Add(key, handlers);
            }
        }

        public void UnregisterKeyGestureDownHandler(KeyGesture key, KeyDownEventHandler handler)
        {
            List<KeyDownEventHandler> handlers = null;
            if (_KeyGestureDownEventHandlers.TryGetValue(key, out handlers))
            {
                _KeyGestureDownEventHandlers.Remove(key);
            }
        }

        public event OpenSaveAllCommandExecutedHandler OpenSaveAllCommandExecuted;
        public void OpenSaveAllNetworks()
        {
            if (OpenSaveAllCommandExecuted != null)
            {
                OpenSaveAllCommandExecuted();
            }
        }

        public event ExportAllCommandExecutedHandler ExportAllCommandExecuted;
        public void ExportAllNetworks(bool buildRpf)
        {
            if (ExportAllCommandExecuted != null)
            {
                ExportAllCommandExecuted(buildRpf);
            }
        }
    }
}
