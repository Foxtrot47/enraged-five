﻿using System;
using System.Configuration;

namespace Rockstar.MoVE.Services
{
    public abstract class IConfiguration
    {
        public abstract void Inject(ConfigurationSection section);

        public virtual void OnInitialized()
        {
        }
    }
}