﻿using System;
using System.Xml;

namespace Rockstar.MoVE.Services
{
    public class NodeMap
    {
        #region Events
        #endregion // Events

        #region Properties
        static XmlDocument NodeNameMap;
        static bool IsInit = false;
        static XmlElement RootElement;
        static String Filename;
        #endregion // Properties

        #region Static Methods
        static public void Init( String sFilename )
        {
            if (!IsInit)
            {
                NodeNameMap = new XmlDocument();
                NodeNameMap.AppendChild(NodeNameMap.CreateProcessingInstruction("xml", "version=\"1.0\" encoding = \"Windows-1252\""));
                RootElement = NodeNameMap.CreateElement("root");
                NodeNameMap.AppendChild(RootElement);
                Filename = sFilename;
                IsInit = true;
            }
        }

        static public void AddNode(String sName, String sCrc)
        {
            if (IsInit)
            {
                XmlElement elem = NodeNameMap.CreateElement("node");
                elem.SetAttribute("name", sName);
                elem.SetAttribute("crc", sCrc);
                RootElement.AppendChild(elem);
                IsInit = true;
            }
        }

        static public void Shutdown()
        {
            NodeNameMap.Save(Filename);
            IsInit = false;
        }
        #endregion // Static Methods

    }
}
