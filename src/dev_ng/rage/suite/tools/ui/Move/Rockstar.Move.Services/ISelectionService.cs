﻿using System;
using System.Collections;

namespace Rockstar.MoVE.Services
{
    public delegate void CurrentSelectionChangedHandler();

    public interface ISelectionService
    {
        void InsertSelectedItem(object item);
        void RemoveSelectedItem(object item);

        bool IsSelected(object item);
        
        void Clear();

        object CurrentSelection { get; set; }
        event CurrentSelectionChangedHandler CurrentSelectionChanged;
        event CurrentSelectionChangedHandler CurrentSelectionChanging;
    }
}
