﻿using System;

namespace Rockstar.MoVE.Services
{
    public delegate void CurrentActiveChangingHandler();
    public delegate void CurrentActiveChangedHandler();
    public delegate void CurrentActiveViewChangedHandler();
    public delegate void CurrentRootChangingHandler();
    public delegate void CurrentRootChangedHandler();

    public interface IDataModelService
    {
        object CurrentActive { get; set; }
        event CurrentActiveChangingHandler CurrentActiveChanging;
        event CurrentActiveChangedHandler CurrentActiveChanged;
        event CurrentActiveViewChangedHandler CurrentActiveViewChanged;

        object CurrentRoot { get; set; }
        event CurrentRootChangingHandler CurrentRootChanging;
        event CurrentRootChangedHandler CurrentRootChanged;

        void InvalidateActiveView();
    }
}