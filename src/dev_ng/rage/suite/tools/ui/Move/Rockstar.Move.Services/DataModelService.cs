﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Services
{
    [Export(typeof(IDataModelService))]
    internal class DataModelService : IDataModelService
    {
        public event CurrentActiveChangingHandler CurrentActiveChanging;
        public event CurrentActiveChangedHandler CurrentActiveChanged;
        
        object currentActive;

        public object CurrentActive
        {
            get
            {
                return currentActive;
            }
            set
            {
                if (value != currentActive)
                {
                    if (CurrentActiveChanging != null)
                    {
                        CurrentActiveChanging();
                    }

                    currentActive = value;

                    if (CurrentActiveChanged != null)
                    {
                        CurrentActiveChanged();
                    }
                }
            }
        }
        public event CurrentActiveViewChangedHandler CurrentActiveViewChanged;

        public event CurrentRootChangingHandler CurrentRootChanging;
        public event CurrentRootChangedHandler CurrentRootChanged;

        object currentRoot;

        public object CurrentRoot
        {
            get
            {
                return currentRoot;
            }
            set
            {
                if (value != currentRoot)
                {
                    if (CurrentRootChanging != null)
                    {
                        CurrentRootChanging();
                    }

                    currentRoot = value;

                    if (CurrentRootChanged != null)
                    {
                        CurrentRootChanged();
                    }
                }
            }
        }

        public void InvalidateActiveView()
        {
            if (CurrentActiveViewChanged != null)
            {
                CurrentActiveViewChanged();
            }
        }
    }
}