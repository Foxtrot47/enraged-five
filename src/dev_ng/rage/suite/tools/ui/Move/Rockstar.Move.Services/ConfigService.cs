﻿using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;

using AvalonDock;

namespace Rockstar.MoVE.Services
{
    [Export(typeof(IConfigService))]
    internal class ConfigService : IConfigService
    {
        [ImportMany("Config")]
        Lazy<IConfiguration, IConfigMetadata>[] _Configs = null;

        Configuration _Config = null;

        public void Load()
        {
            string applicationName = Environment.GetCommandLineArgs()[0];
            if (!applicationName.Contains(".exe"))
            {
                applicationName = String.Concat(applicationName, ".exe");
            }

            string exePath = Path.Combine(Environment.CurrentDirectory, applicationName);

            _Config = ConfigurationManager.OpenExeConfiguration(exePath);

            foreach (var config in _Configs)
            {
                ConfigurationSection section = null;
                if (_Config.Sections[config.Metadata.Section] == null)
                {
                    section = (ConfigurationSection)Activator.CreateInstance(config.Metadata.Type);
                    section.SectionInformation.ForceSave = true;
                    _Config.Sections.Add(config.Metadata.Section, section);
                    _Config.Save(ConfigurationSaveMode.Modified);

                    ConfigurationManager.RefreshSection(config.Metadata.Section);
                }

                config.Value.Inject(_Config.GetSection(config.Metadata.Section));
            }
        }

        public void Save()
        {
            _Config.Save(ConfigurationSaveMode.Full);
        }
    }
}