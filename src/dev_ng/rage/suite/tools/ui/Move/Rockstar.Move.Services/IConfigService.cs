﻿using System;

namespace Rockstar.MoVE.Services
{
    public interface IConfigService
    {
        void Load();
        void Save();
    }
}