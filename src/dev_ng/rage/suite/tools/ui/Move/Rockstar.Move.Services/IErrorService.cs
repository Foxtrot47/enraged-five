﻿using System;
using System.Collections.ObjectModel;

namespace Rockstar.MoVE.Services
{
    public interface IErrorService
    {
        ReadOnlyObservableCollection<string> Errors { get; }

        void Log(string description);
    }
}