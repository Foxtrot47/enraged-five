﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Services
{
    [MetadataAttribute]
    public class ConfigMetadataAttribute : Attribute
    {
        public string Section { get; private set; }
        public Type Type { get; private set; }

        public ConfigMetadataAttribute(string section, Type type)
        {
            Section = section;
            Type = type;
        }
    }
}