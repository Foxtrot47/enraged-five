﻿using System;
using System.ComponentModel.Composition;

using Move.Utils;

namespace Rockstar.MoVE.Services
{
    [Export(typeof(IRuntimeCommunicationService))]
    public class RuntimeCommunicationService : IRuntimeCommunicationService
    {
        public event EventHandler<SimpleEventArgs<IDebugSession>> StartDebugging;
        public event EventHandler<SimpleEventArgs<IDebugSession>> StopDebugging;

        public void OnStartDebugging(object sender, IDebugSession session)
        {
            StartDebugging.Raise(sender, session);
        }

        public void OnStopDebugging(object sender, IDebugSession session)
        {
            StopDebugging.Raise(sender, session);
        }
    }
}