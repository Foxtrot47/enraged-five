﻿using System;

using Move.Utils;

namespace Rockstar.MoVE.Services
{
    public interface IDebugSession
    {
        // you need to downcast this to something useful... Rage.Move.Core.Client.DebugSession probably
    }

    public interface IRuntimeCommunicationService
    {
        event EventHandler<SimpleEventArgs<IDebugSession>> StartDebugging;
        event EventHandler<SimpleEventArgs<IDebugSession>> StopDebugging;

        void OnStartDebugging(object sender, IDebugSession session);
        void OnStopDebugging(object sender, IDebugSession session);

    }
}
