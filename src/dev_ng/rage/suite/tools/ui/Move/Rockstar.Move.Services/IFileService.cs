﻿using System;
using System.IO;


namespace Rockstar.MoVE.Services
{
    public interface IFileService
    {
        TextReader CreateStreamReader(string path);
        TextWriter CreateStreamWriter(string path);

        bool Exists(string path);

        string GetApplicationFolderPath();
        string FindUntiltedFilename();
    }
}