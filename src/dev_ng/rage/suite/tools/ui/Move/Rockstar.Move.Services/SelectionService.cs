﻿using System;
using System.Collections;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Services
{
    [Export(typeof(ISelectionService))]
    public class SelectionService : ISelectionService
    {
        public void InsertSelectedItem(object item)
        {
            if (this.SelectedItems.Contains(item))
                return;

            SelectedItems.Add(item);
        }
        
        public void RemoveSelectedItem(object item)
        {
            if (!this.SelectedItems.Contains(item))
                return;

            SelectedItems.Remove(item);
        }

        public bool IsSelected(object item)
        {
            return SelectedItems.Contains(item);
        }

        public void Clear()
        {
            CurrentSelection = null;
            SelectedItems.Clear();
        }

        ArrayList SelectedItems = new ArrayList();

        public event CurrentSelectionChangedHandler CurrentSelectionChanging;
        public event CurrentSelectionChangedHandler CurrentSelectionChanged;

        object currentSelection;

        public object CurrentSelection
        {
            get
            {
                return currentSelection;
            }
            set
            {
                if (value != currentSelection)
                {
                    if (CurrentSelectionChanging != null)
                    {
                        CurrentSelectionChanging();
                    }
                    currentSelection = value;
                    if (CurrentSelectionChanged != null)
                    {
                        CurrentSelectionChanged();
                    }
                }
            }
        }
    }
}