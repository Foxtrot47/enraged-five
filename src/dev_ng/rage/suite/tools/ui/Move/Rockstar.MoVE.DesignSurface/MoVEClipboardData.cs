﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

using Move.Utils;
using New.Move.Core;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

namespace Rockstar.MoVE.Design
{
    [Serializable]
    public class MoVEClipData
    {
        public NoResetObservableCollection<ISelectable> m_data = new NoResetObservableCollection<ISelectable>();

        public MoVEClipData()
        {

        }

        public MoVEClipData(SerializationInfo info, StreamingContext context)
        {
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("m_data", m_data);
        }
    }

    sealed class ClipDataDeserializationBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type typeToDeserialize = null;

            // For each assemblyName/typeName that you want to deserialize to
            // a different type, set typeToDeserialize to the desired type.
            String assemVer1 = Assembly.GetExecutingAssembly().FullName;
            String typeVer1 = "MoVEClipData";

            if (assemblyName == assemVer1 && typeName == typeVer1)
            {
                // To use a type from a different assembly version, 
                // change the version number.
                // To do this, uncomment the following line of code.
                // assemblyName = assemblyName.Replace("1.0.0.0", "2.0.0.0");

                // To use a different type from the same assembly, 
                // change the type name.
                typeName = "MoVEClipData";
            }

            // The following line of code returns the type.
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}",
                typeName, assemblyName));

            return typeToDeserialize;
        }
    }
    
    public class MoVEClipboardData
    {
        NoResetObservableCollection<ISelectable> m_clipboardData = new NoResetObservableCollection<ISelectable>();

        /// <summary>
        /// Constructor
        /// </summary>
        public MoVEClipboardData()
        {

        }

        /// <summary>
        /// Clear the collection of data
        /// </summary>
        public void Clear()
        {
            m_clipboardData.Clear();
        }

        /// <summary>
        /// Add an item to the collection of data
        /// </summary>
        /// <param name="item">The item to add to the list</param>
        public void Add(ISelectable item)
        {
            m_clipboardData.Add(item);
        }

        /// <summary>
        /// Get the collection of data
        /// </summary>
        /// <returns>The collection of data</returns>
        public NoResetObservableCollection<ISelectable> GetData()
        {
            return m_clipboardData;
        }

        /// <summary>
        /// Send the collection of data to the shared clipboard
        /// </summary>
        public void SendDataToClipboard()
        {
            MoVEClipData data = new MoVEClipData();
            data.m_data = m_clipboardData;

            //  Create a memorystream
            MemoryStream ms = new MemoryStream();
            try
            {
                //  Create a binary formatter to serialize the data to the memorystream
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, data);
                Clipboard.SetData("MoVEClipData", ms.GetBuffer());
            }
            catch (SerializationException e)
            {
                MessageBox.Show("Failed to serialize because: " + e.Message);
                throw;
            }
            finally
            {
                ms.Close();
            }
        }

        /// <summary>
        /// Get the collection of data from the shared clipboard
        /// </summary>
        /// <returns></returns>
        public bool GetDataFromClipboard()
        {
            bool result = false;

            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Binder = new ClipDataDeserializationBinder();

            MemoryStream ms = new MemoryStream(Clipboard.GetData("MoVEClipData") as byte[]);
            try
            {
                MoVEClipData obj1 = (MoVEClipData)formatter.Deserialize(ms);
                m_clipboardData = obj1.m_data;
                result = true;
            }
            catch (System.Exception e)
            {
                MessageBox.Show("Failed to deserialize because: " + e.Message);
                throw;
            }
            finally
            {
                ms.Close();
            }

            return result;
        }
    }
}
