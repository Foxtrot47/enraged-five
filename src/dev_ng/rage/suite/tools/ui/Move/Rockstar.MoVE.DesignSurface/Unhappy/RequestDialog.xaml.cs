﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rage.Move;
using New.Move.Core;

using AvalonDock;


namespace Rockstar.MoVE.Design
{
    public partial class RequestDialog : Window
    {
        public RequestDialog()
        {
            InitializeComponent();
        }

        Database Database
        {
            get
            {
                //DocumentContent doc = App.Window.DocumentHost.SelectedItem as DocumentContent;
                //return doc.Tag as Database;
                return null;
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            Requests.ItemsSource = Database.Requests;
            Requests.DisplayMemberPath = "Name";
        }

        public void DoModal()
        {
            ShowDialog();
        }

        void AddButton_Click(object sender, RoutedEventArgs e)
        {
            RequestEditDialog dialog = new RequestEditDialog();
            dialog.DoModal();
        }

        void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            Request selected = Requests.SelectedItem as Request;
            Database.Requests.Remove(selected);
        }

        void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
