﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

using New.Move.Core;

using Rockstar.MoVE.Services;

namespace Rockstar.MoVE.Design
{
    // i think there's probably a better way to do this by MEFing different document types together... but I don't have time at the moment
    // this would probably make more sense being part of the main window too
    public partial class NewDocumentDialog : Window
    {
        enum Result
        {
            Ok,
            Cancel,
        }

        public NewDocumentDialog()
        {
            InitializeComponent();
        }

        public ITransitionalDesc DoModal()
        {
            ShowDialog();

            return Desc;
        }

        internal string FullyQualifiedFilename { get; private set; }

        ITransitionalDesc Desc = null;
        Result State = Result.Cancel;

        void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                ListBoxItem item = e.AddedItems[0] as ListBoxItem;
                if ((string)item.Content== "State Machine")
                {
                    Desc = StateMachine.Desc;
                }
                else if ((string)item.Content == "Motion Tree")
                {
                    Desc = Motiontree.Desc;
                }
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (State == Result.Cancel)
            {
                Desc = null;
            }

            base.OnClosing(e);
        }

        void OK_Click(object sender, RoutedEventArgs e)
        {
            State = Result.Ok;
            Close();
        }

        void Cancel_Click(object sender, RoutedEventArgs e)
        {
            State = Result.Cancel;
            Close();
        }
    }
}
