﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public partial class DiagramAnchorRealOutput : DiagramAnchor
    {
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            HitRectangle.Width = this.ActualWidth + 8.0;

            base.OnRenderSizeChanged(sizeInfo);
        }

        public DiagramAnchorRealOutput(string name, IAnchor source)
            : base(name, source)
        {
            InitializeComponent();
        }

        public override Point? Sender
        {
            get { return new Point?(Point.TranslatePoint(new Point(0, 0), GetDiagram(this))); }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                Point position = Point.TranslatePoint(new Point(0, 0), diagram);
                Start = new Point?(position);
                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        DiagramAdorner adorner = new DiagramAdorner(diagram, Start.Value, this);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

        public override IDiagramAnchor HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is DiagramAnchorRealInput || hitobj is DiagramAnchorRealSource)
                        return hitobj as DiagramAnchor;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }
    }
}
