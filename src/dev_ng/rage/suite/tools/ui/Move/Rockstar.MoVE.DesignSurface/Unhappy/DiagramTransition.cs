﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using New.Move.Core;

using ITransition = New.Move.Core.ITransition;

namespace Rockstar.MoVE.Design
{
    public class DiagramTransition : Control, INotifyPropertyChanged
    {
        public DiagramTransition(DiagramState source, DiagramState dest, Diagram diagram, Transition transition, Point start, Point midpoint, Point end)
        {
            Transition = transition;
            Transition.PropertyChanged += new PropertyChangedEventHandler(Transition_PropertyChanged);
            _Diagram = diagram;

            Start = start;
            Mid = midpoint;
            End = end;

            Loaded += new RoutedEventHandler(DiagramTransition_Loaded);
            Unloaded += new RoutedEventHandler(DiagramTransition_Unloaded);

            Source = source;
            Source.Loaded += new RoutedEventHandler(DiagramState_Loaded);

            Dest = dest;
            Dest.Loaded += new RoutedEventHandler(DiagramState_Loaded);

            SelectionBehaviour.SetIsSelectionEnabled(this, true);

            Tag = transition;

            InitialiseStrokeArray();
        }

        public void Cleanup()
        {
            Transition.PropertyChanged -= new PropertyChangedEventHandler(Transition_PropertyChanged);

            Transition.IsSelectedChanged -=
                new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);

            Source.Loaded -= new RoutedEventHandler(DiagramState_Loaded);
            Source.PositionChanged -= new RoutedEventHandler(DiagramState_PositionChanged);

            Dest.Loaded -= new RoutedEventHandler(DiagramState_Loaded);
            Dest.PositionChanged -= new RoutedEventHandler(DiagramState_PositionChanged);
        }

        void Transition_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Source" || e.PropertyName == "Dest")
            {
                _Diagram.InvalidateTransitions();
            }
            else if (e.PropertyName == "Content.Enabled.Value")
            {
                InitialiseStrokeArray();
            }
        }

        void InitialiseStrokeArray()
        {
            if (Transition.Content.Enabled.Value)
            {
                StrokeDashArray = null;
            }
            else
            {
                StrokeDashArray = new DoubleCollection();
                StrokeDashArray.Add(3.0f);
                StrokeDashArray.Add(6.0f);
            }
        }

        
        public Point Start { get; private set; }
        public Point Mid { get; private set; }
        public Point End { get; private set; }

        // Drag data
        bool _IsBeingDraggedFromStart;
        public bool IsBeingDraggedFromStart
        {
            get { return _IsBeingDraggedFromStart; }
            set
            {
                _IsBeingDraggedFromStart = value;
                UpdateGeometry();
            }
        }

        Point _DragStart;
        public Point DragStart 
        {
            get { return _DragStart; }
            set
            {
                _DragStart = value;
                UpdateGeometry();
            }
        }

        bool _IsBeingDraggedFromEnd;
        public bool IsBeingDraggedFromEnd
        {
            get { return _IsBeingDraggedFromEnd; }
            set
            {
                _IsBeingDraggedFromEnd = value;
                UpdateGeometry();
            }
        }

        Point _DragEnd;
        public Point DragEnd
        {
            get { return _DragEnd; }
            set
            {
                _DragEnd = value;
                UpdateGeometry();
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            ContextMenu = new ContextMenu();

            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete Transition";
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);
            ContextMenu.Items.Add(deleteItem);

            SelectionBehaviour.SetIsSelected(this, IsSelected);

            Transition.IsSelectedChanged +=
                new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);
        }

        void DiagramTransition_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        void DiagramTransition_Unloaded(object sender, RoutedEventArgs e)
        {
            Transition.PropertyChanged -= new PropertyChangedEventHandler(Transition_PropertyChanged);

            Transition.IsSelectedChanged -=
                new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);

            Source.Loaded -= new RoutedEventHandler(DiagramState_Loaded);
            Source.PositionChanged -= new RoutedEventHandler(DiagramState_PositionChanged);

            Dest.Loaded -= new RoutedEventHandler(DiagramState_Loaded);
            Dest.PositionChanged -= new RoutedEventHandler(DiagramState_PositionChanged);

            Transition.Tag = null;
        }

        void Transition_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            switch (e.Action)
            {
                case SelectionChangedAction.Add:
                    SetValue(SelectionBehaviour.IsSelectedProperty, true);
                    break;
                case SelectionChangedAction.Remove:
                    SetValue(SelectionBehaviour.IsSelectedProperty, false);
                    break;
            }
        }

        void DiagramState_PositionChanged(object sender, RoutedEventArgs e)
        {
            UpdateGeometry();
        }

        void DiagramState_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateGeometry();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Transition.Dispose();
        }

        public Guid Id { get; private set; }

        public Transition Transition { get; private set; }

        Diagram _Diagram;

        DoubleCollection _StrokeDashArray;
        public DoubleCollection StrokeDashArray
        {
            get
            {
                return _StrokeDashArray;
            }
            set
            {
                if (_StrokeDashArray != value)
                {
                    _StrokeDashArray = value;
                    OnPropertyChanged("StrokeDashArray");
                }
            }
        }

        PathGeometry _Geometry;
        public PathGeometry Geometry
        {
            get
            {
                return _Geometry;
            }
            set
            {
                if (_Geometry != value)
                {
                    _Geometry = value;
                    OnPropertyChanged("Geometry");
                    UpdatePosition();
                }
            }
        }

        DiagramState _Source;
        public DiagramState Source
        {
            get
            {
                return _Source;
            }
            set
            {
                if (_Source != value)
                {
                    _Source = value;
                    UpdateGeometry();
                }
            }
        }

        DiagramState _Dest;
        public DiagramState Dest
        {
            get
            {
                return _Dest;
            }
            set
            {
                if (_Dest != value)
                {
                    _Dest = value;
                    UpdateGeometry();
                }
            }
        }

        Point _SourcePosition;
        public Point SourcePosition
        {
            get
            {
                return _SourcePosition;
            }
            set
            {
                if (_SourcePosition != value)
                {
                    _SourcePosition = value;
                    OnPropertyChanged("SourcePosition");
                }
            }
        }

        double _SourceAngle;
        public double SourceAngle
        {
            get
            {
                return _SourceAngle;
            }
            set
            {
                if (_SourceAngle != value)
                {
                    _SourceAngle = value;
                    OnPropertyChanged("SourceAngle");
                }
            }
        }

        Point _DestPosition;
        public Point DestPosition
        {
            get
            {
                return _DestPosition;
            }
            set
            {
                if (_DestPosition != value)
                {
                    _DestPosition = value;
                    OnPropertyChanged("DestPosition");
                }
            }
        }

        double _DestAngle;
        public double DestAngle
        {
            get
            {
                return _DestAngle;
            }
            set
            {
                if (_DestAngle != value)
                {
                    _DestAngle = value;
                    OnPropertyChanged("DestAngle");
                }
            }
        }

        public bool IsSelected
        {
            get
            {
                return _Diagram.Database.SelectedItems.Contains(Transition);
            }
        }

        public Point OffsetStart { get; set; }
        public Point OffsetMid { get; set; }
        public Point OffsetEnd { get; set; }

        void UpdateGeometry()
        {
            if (_Source != null && _Dest != null)
            {
                if (IsBeingDraggedFromStart)
                {
                    Point lineStart = OffsetEnd;
                    Point lineEnd = OffsetStart + (DragStart - Start);
                    PathSegmentCollection lines = new PathSegmentCollection();
                    lines.Add(new LineSegment(lineEnd, true));
                    PathFigure pathFigure = new PathFigure(lineStart, lines, true);
                    Geometry = new PathGeometry(new PathFigure[] { pathFigure });
                }
                else if (IsBeingDraggedFromEnd)
                {
                    Point lineStart = OffsetEnd + (DragEnd - End);
                    Point lineEnd = OffsetStart;
                    PathSegmentCollection lines = new PathSegmentCollection();
                    lines.Add(new LineSegment(lineEnd, true));
                    PathFigure pathFigure = new PathFigure(lineStart, lines, true);
                    Geometry = new PathGeometry(new PathFigure[] { pathFigure });
                }
                else
                {
                    Point start = new Point();
                    Point mid = Mid;
                    Point end = new Point();

                    if (End.X < Start.X)
                    {
                        start.X = Start.X - End.X;
                        mid.X -= End.X;
                    }
                    else
                    {
                        end.X = End.X - Start.X;
                        mid.X -= Start.X;
                    }

                    if (End.Y < Start.Y)
                    {
                        start.Y = Start.Y - End.Y;
                        mid.Y -= End.Y;
                    }
                    else
                    {
                        end.Y = End.Y - Start.Y;
                        mid.Y -= Start.Y;
                    }

                    OffsetStart = start;
                    OffsetMid = mid;
                    OffsetEnd = end;

                    Point[] cp1, cp2;
                    Point[] points = new Point[] { start, mid, end };
                    BezierSpline.GetCurveControlPoints(points, out cp1, out cp2);

                    PathSegmentCollection lines = new PathSegmentCollection();
                    for (int i = 0; i < cp1.Length; ++i)
                    {
                        lines.Add(new BezierSegment(cp1[i], cp2[i], points[i + 1], true));
                    }
                    PathFigure f = new PathFigure(points[0], lines, false);
                    Geometry = new PathGeometry(new PathFigure[] { f });
                }
            }
        }

        void UpdatePosition()
        {
            if (Geometry.Bounds.Width > 0 || Geometry.Bounds.Height > 0)
            {
                Point start, stangent;
                Geometry.GetPointAtFractionLength(0, out start, out stangent);

                Point end, etangent;
                Geometry.GetPointAtFractionLength(1, out end, out etangent);

                SourceAngle = Math.Atan2(-stangent.Y, -stangent.X) * (180 / Math.PI);
                DestAngle = Math.Atan2(etangent.Y, etangent.X) * (180 / Math.PI);

                start.Offset(-stangent.X * 5, -stangent.Y * 5);
                end.Offset(etangent.X * 5, etangent.Y * 5);

                SourcePosition = start;
                DestPosition = end - new Vector(etangent.X * 8.0, etangent.Y * 8.0);
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
