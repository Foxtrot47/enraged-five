﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public partial class DiagramAnchorClipSource : DiagramAnchor, IClipAnchor
    {
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            HitRectangle.Width = this.ActualWidth + 8.0;

            base.OnRenderSizeChanged(sizeInfo);
        }

        public static readonly DependencyProperty InputProperty =
            DependencyProperty.Register("Input", typeof(PropertyInput), typeof(DiagramAnchorClipSource));

        public static readonly DependencyProperty ResultEnabledProperty =
            DependencyProperty.Register("ResultEnabled", typeof(bool), typeof(DiagramAnchorClipSource));

        internal PropertyInput Input
        {
            get { return (PropertyInput)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        internal bool ResultEnabled
        {
            get { return (bool)GetValue(ResultEnabledProperty); }
            set { SetValue(ResultEnabledProperty, value); }
        }

        public DiagramAnchorClipSource(string name, Property property) : base(name, property)
        {
            InitializeComponent();

            MouseEnter += new MouseEventHandler(
                delegate(object sender, MouseEventArgs e)
                {
                    if (ResultEnabled)
                    {
                        HighlightEnabled = true;
                    }
                });

            MouseLeave += new MouseEventHandler(
                delegate(object sender, MouseEventArgs e)
                {
                    if (ResultEnabled)
                    {
                        HighlightEnabled = false;
                    }
                });
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            SetBinding(DiagramAnchorClipSource.InputProperty, 
                new Binding("Input") { Source = Anchor });

            SetBinding(DiagramAnchorClipSource.ResultEnabledProperty, 
                new Binding("OutputEnabled") { Source = Anchor });
        }

        public override Point? Sender
        {
            get
            {
                return new Point?(ResultPoint.TranslatePoint(new Point(0, 0), GetDiagram(this)));
            }
        }

        public override Point? Receiver
        {
            get 
            { 
                return new Point?(SourcePoint.TranslatePoint(new Point(0, 0), GetDiagram(this))); 
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                Point position = ResultPoint.TranslatePoint(new Point(0, 0), diagram);
                Start = new Point?(position);
                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                Start = null;
            }

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        DiagramAdorner adorner = new DiagramAdorner(diagram, Start.Value, this);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

        public override IDiagramAnchor HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitObj = diagram.InputHitTest(point) as DependencyObject;
                while (hitObj != null && hitObj.GetType() != typeof(Diagram) && hitObj != this)
                {
                    if (hitObj is DiagramAnchorClipSource ||
                            (hitObj is DiagramTunnelParameter && (hitObj as DiagramTunnelParameter).Parameter == Parameter.Clip) || hitObj is IClipAnchor)
                    {
                        return hitObj as IDiagramAnchor;
                    }

                    hitObj = VisualTreeHelper.GetParent(hitObj);
                }
            }

            return null;
        }
    }
}
