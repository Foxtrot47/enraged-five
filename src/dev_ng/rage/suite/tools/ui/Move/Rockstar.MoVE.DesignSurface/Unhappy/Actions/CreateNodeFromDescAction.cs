﻿using System.Windows;

using New.Move.Core;
using Move.Core.Restorable;

namespace Rockstar.MoVE.Design
{
    internal class CreateNodeFromDescAction : IExecuteAction
    {
        static class Const
        {
            public static readonly Point Offset = new Point(2048, 2048);
        }

        internal CreateNodeFromDescAction(Database database, IDesc desc, Point position)
        {
            _Database = database;
            _Desc = desc;
            _Position = position;
        }

        Database _Database;
        IDesc _Desc;

        Point _Position;

        public void Execute()
        {
            INode node = null;
            if (_Desc is ITransitionalDesc)
            {
                node = _Database.Create((ITransitionalDesc)_Desc, Const.Offset);
            }
            else
            {
                node = _Database.Create(_Desc);
            }
            node.Position = _Position;

            _Database.AppendChild(_Database.Active, node);
        }
    }
}