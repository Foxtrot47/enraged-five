﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using AvalonDock;

using New.Move.Core;
using Move.Core;
using MoVE.Core;

using Rockstar.MoVE.Services;

namespace Rockstar.MoVE.Design
{
    public class Diagram : Canvas
    {
        public static class Const
        {
            public static readonly int Size = 4096;
        }

        public Diagram()
        {
            AllowDrop = true;

            Loaded += new RoutedEventHandler(DiagramCanvas_Loaded);
            Unloaded += new RoutedEventHandler(DiagramCanvas_Unloaded);

            _NodeDragController = new NodeDragController(this);
            _TransitionEndDragController = new TransitionEndDragController(this);
        }

        bool _Updated = false;

        internal DocumentContent Document { get; set; }

        public EventHandler<EventArgs> DatabaseChanged;
        void OnDatabaseChanged()
        {
            if (DatabaseChanged != null)
                DatabaseChanged(_Database, EventArgs.Empty);
        }

        internal IDataModelService DataModelService { get; set; }

        Database _Database;
        internal Database Database
        {
            get
            {
                return _Database;
            }
            set
            {
                if (_Database != null)
                {
                    ActiveChanging();
                }

                _Database = value;

                if (_Database != null)
                {
                    ActiveChanged();
                }
            }
        }

        double _Scale = 1.0;
        public double Scale
        {
            get { return _Scale; }
            set
            {
                if (_Scale != value)
                {
                    _Scale = value;
                    LayoutTransform = new ScaleTransform(_Scale, _Scale);
                }
            }
        }

        Rect _SelectedNodeBounds = Rect.Empty;
        public Rect SelectedNodeBounds
        {
            get { return _SelectedNodeBounds; }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
        }

        public Point RelativePosition(Point position)
        {
            IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();

            if (children.Count().Equals(0))
                return new Point(0, 0);

            Point topleft = new Point(double.MaxValue, double.MaxValue);

            foreach (Control child in children)
            {
                topleft.X = Math.Min(topleft.X, Canvas.GetLeft(child));
                topleft.Y = Math.Min(topleft.Y, Canvas.GetTop(child));
            }

            return new Point(topleft.X + position.X, topleft.Y + position.Y);
        }

        public Size Size
        {
            get
            {
                IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();

                Size size = new Size(0, 0);

                foreach (IDiagramElement child in children)
                {
                    double width = child.Position.X + child.DesiredSize.Width;

                    if (size.Width < width)
                        size.Width = width;

                    double height = child.Position.Y + child.DesiredSize.Height;

                    if (size.Height < height)
                        size.Height = height;
                }

                return size;
            }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
             Size size = new Size(Double.PositiveInfinity, Double.PositiveInfinity);

             foreach (UIElement element in this.InternalChildren)
                 element.Measure(size);
           
            return MeasureItems();
        }

        private Size MeasureItems()
        {
            IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();
            if (children.Count().Equals(0))
                return new Size();

            Rect bounds = new Rect(new Point(Double.MaxValue, Double.MaxValue), new Size());

            foreach (Control child in children)
            {
                Point location = new Point(Canvas.GetLeft(child), Canvas.GetTop(child));

                bounds.X = Math.Min(bounds.X, location.X);
                bounds.Y = Math.Min(bounds.Y, location.Y);
            }

            Point point = new Point(-bounds.X, -bounds.Y);

            foreach (Control child in children)
            {
                Point location = new Point(Canvas.GetLeft(child) + point.X, Canvas.GetTop(child) + point.Y);

                bounds.Width = Math.Max(bounds.Width, location.X + child.DesiredSize.Width);
                bounds.Height = Math.Max(bounds.Height, location.Y + child.DesiredSize.Height);
            }

            //return new Size(bounds.Width, bounds.Height);
            return new Size(Const.Size, Const.Size);
        }
        
        protected override Size ArrangeOverride(Size finalSize)
        {
            IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();
            if (children.Count().Equals(0))
            {
                return new Size(Const.Size, Const.Size);
                //return new Size();
            }

            Rect bounds = new Rect(new Point(Double.MaxValue, Double.MaxValue), new Size());

            foreach (Control child in children)
            {
                Point location = new Point(Canvas.GetLeft(child), Canvas.GetTop(child));

                bounds.X = Math.Min(bounds.X, location.X);
                bounds.Y = Math.Min(bounds.Y, location.Y);
            }

            // create an absolute location
            Point point = new Point(0, 0);
            //Point point = new Point(-bounds.X, -bounds.Y);

            foreach (Control child in children)
            {
                Point location = new Point(Canvas.GetLeft(child) + point.X, Canvas.GetTop(child) + point.Y);

                Rect rect = new Rect(location, child.DesiredSize);
                child.Arrange(rect);

                bounds.Width = Math.Max(bounds.Width, location.X + child.DesiredSize.Width);
                bounds.Height = Math.Max(bounds.Height, location.Y + child.DesiredSize.Height);
            }

            foreach (DiagramConnection connection in InternalChildren.OfType<DiagramConnection>())
            {
                Point start = connection.Sender.Sender.Value;
                Point end = connection.Receiver.Receiver.Value;

                Rect rect = new Rect(start, end);
                connection.Arrange(rect);
            }

            foreach (DiagramTransition transition in InternalChildren.OfType<DiagramTransition>())
            {
                /*
                Point source0 = transition.Source.TopLeft;
                Point source1 = transition.Source.BottomRight;

                Point p = transition.Source.Center;

                Point dest0 = transition.Dest.TopLeft;
                Point dest1 = transition.Dest.BottomRight;

                Point dest = new Point(0, 0);

                double vx = p.X;
                if (vx < dest0.X) vx = dest0.X;
                if (vx > dest1.X) vx = dest1.X;
                dest.X = vx;

                double vy = p.Y;
                if (vy < dest0.Y) vy = dest0.Y;
                if (vy > dest1.Y) vy = dest1.Y;
                dest.Y = vy;

                Point source = new Point(0, 0);
                
                double vx1 = dest.X;
                if (vx1 < source0.X) vx1 = source0.X;
                if (vx1 > source1.X) vx1 = source1.X;
                source.X = vx1;

                double vy1 = dest.Y;
                if (vy1 < source0.Y) vy1 = source0.Y;
                if (vy1 > source1.Y) vy1 = source1.Y;
                source.Y = vy1;
                */
                Rect rect = new Rect(transition.Start, transition.End);
                transition.Arrange(rect);
            }
            
            //return new Size(bounds.Width, bounds.Height);
            return new Size(Const.Size, Const.Size);
        }

        UserControl GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is DiagramViewer))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as UserControl;
        }

        public void DocumentContent_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(DragObject)))
            {
                DragObject drag = e.Data.GetData(typeof(DragObject)) as DragObject;
                _Database.Execute(new CreateNodeFromDescAction(_Database, drag.Desc, e.GetPosition(this)));
            }
        }

        public void DiagramViewer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ITransitional active = _Database.Active;
            ITransitional parent = (ITransitional)_Database.Find(active.Parent);

            if (parent != null)
                _Database.Active = parent;
        }

        internal void ActiveChanging()
        {
            _Database.Active.RemoveHandler(new NotifyCollectionChangedEventHandler(Children_CollectionChanged));

            object curr = DataModelService.CurrentActive;
            if (curr is Motiontree)
            {
                ((Motiontree)curr).Connections.CollectionChanged -= new NotifyCollectionChangedEventHandler(Connections_CollectionChanged);
            }

            if (curr is IAutomaton)
            {
                ((IAutomaton)curr).Transitions.CollectionChanged -= new NotifyCollectionChangedEventHandler(Transitions_CollectionChanged);
            }
        }

        internal void ActiveChanged()
        {
            UpdateDiagram();

            _Database.Active.AddHandler(new NotifyCollectionChangedEventHandler(Children_CollectionChanged));

            object curr = DataModelService.CurrentActive;

            if (curr is Motiontree)
            {
                ((Motiontree)curr).Connections.CollectionChanged += new NotifyCollectionChangedEventHandler(Connections_CollectionChanged);
            }

            if (curr is IAutomaton)
            {
                ((IAutomaton)curr).Transitions.CollectionChanged += new NotifyCollectionChangedEventHandler(Transitions_CollectionChanged);
            }
        }

        void Children_CollectionChanged(object obj, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (INode item in e.OldItems)
                {
                    RemoveChild(item);
                }
            }

            if (e.NewItems != null)
            {
                foreach (INode item in e.NewItems)
                {
                    InsertChild(item);
                }
            }
        }

        void Connections_CollectionChanged(object obj, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (New.Move.Core.Connection item in e.OldItems)
                {
                    RemoveConnection(item);
                }
            }

            if (e.NewItems != null)
            {
                foreach (New.Move.Core.Connection item in e.NewItems)
                {
                    InsertConnection(item);
                }
            }
        }

        void Transitions_CollectionChanged(object obj, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Transition item in e.NewItems)
                    {
                        MarkForInsert(item);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Transition item in e.OldItems)
                    {
                        MarkForRemoval(item);
                    }
                    break;
            }

            UpdateDirtyTransitions();
        }

        public void DocumentHost_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DocumentPane host = sender as DocumentPane;
            if (!_Updated && host.SelectedItem == Document)
            {
                UpdateDiagram();
                e.Handled = true;
            }
        }

        bool _Loaded = false;

        void DiagramCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_Loaded)
            {
                SelectionBehaviour.Selected += new EventHandler<SelectedEventArgs>(SelectionBehaviour_Selected);

                _Loaded = true;
            }

            if (!_Updated)
            {
                UpdateDiagram();
            }
        }

        void DiagramCanvas_Unloaded(object sender, RoutedEventArgs e)
        {
            SelectionBehaviour.Selected -= new EventHandler<SelectedEventArgs>(SelectionBehaviour_Selected);

            Children.Clear();
            _Updated = false;

            _Loaded = false;
        }

        void UpdateDiagram()
        {
            _TransitionSetList.Clear();
            Children.Clear();

            ITransitional active = (ITransitional)DataModelService.CurrentActive;

            foreach (INode child in active.Children)
            {
                InsertChild(child);
            }

            var mt = active as Motiontree;
            if (mt != null)
            {
                foreach (New.Move.Core.Connection connection in mt.Connections)
                {
                    InsertConnection(connection);
                }
            }

            var automaton = active as IAutomaton;
            if (automaton != null)
            {
                foreach (Transition transition in automaton.Transitions)
                {
                    //InsertTransition(transition);
                    MarkForInsert(transition);
                }
            }

            UpdateDirtyTransitions();

            _Updated = true;
        }

        void InsertChild(INode item)
        {
            UIElement newControl = null;

            if (item is Output)
            {
                newControl = new DiagramLogical(item as Output);
            }
            else if (item is Input)
            {
                newControl = new DiagramLogical(item as Input);
            }
            else if (item is Reference)
            {
                newControl = new DiagramReference(item as Reference);
            }
            else if (item is Network)
            {
                newControl = new DiagramNetwork(item as Network);
            }
            else if (item is LogicParent)
            {
                newControl = new DiagramLogicalParent(item as LogicParent);
            }
            else if (item is ITransitional)
            {
                DiagramState newDiagramState = new DiagramState(item as ITransitional);
                newDiagramState.PositionChanged += new RoutedEventHandler(DiagramState_PositionChanged);
                newControl = newDiagramState;
            }
            else if (item is SignalBase)
            {
                newControl = new DiagramSignal(item as SignalBase);
            }
            else if (item is TunnelParameter)
            {
                newControl = new DiagramTunnelParameter(item as TunnelParameter);
            }
            else if (item is TunnelEvent)
            {
                newControl = new DiagramTunnelEvent(item as TunnelEvent);
            }
            else if (item is ILogic)
            {
                newControl = new DiagramLogical(item as ILogic);
            }

            if (newControl != null)
            {
                _NodeDragController.AddNode(newControl as DiagramNode);
                Children.Add(newControl);
            }
        }

        void RemoveChild(INode item)
        {
            UIElement[] children = new UIElement[InternalChildren.Count];
            InternalChildren.CopyTo(children, 0);

            foreach (DiagramNode node in children.OfType<DiagramNode>())
            {
                if (node.Source == item)
                {
                    _NodeDragController.RemoveNode(node);
                    Children.Remove(node);
                }
            }
        }

        void InsertConnection(New.Move.Core.Connection item)
        {
            IDiagramAnchor sender = item.Source.Tag as IDiagramAnchor;
            IDiagramAnchor receiver = item.Dest.Tag as IDiagramAnchor;
            
            if (sender != null && receiver != null)
            {
                DiagramConnection connection = new DiagramConnection(sender, receiver, item);
                item.Tag = connection;
                Canvas.SetZIndex(connection, Children.Count);
                Children.Add(connection);
            }
        }

        void RemoveConnection(New.Move.Core.Connection item)
        {
            UIElement[] children = new UIElement[InternalChildren.Count];
            InternalChildren.CopyTo(children, 0);

            foreach (DiagramConnection connection in children.OfType<DiagramConnection>())
            {
                if (connection.Connection == item)
                {
                    Children.Remove(connection);
                    connection.Tag = null;
                }
            }
        }

        class TransitionSet
        {
            internal ITransitional T1;
            internal ITransitional T2;

            internal List<Transition> Transitions = new List<Transition>();

            internal bool IsDirty = true;
        }

        List<TransitionSet> _TransitionSetList = new List<TransitionSet>();
        
        static class TransitionHelper
        {
            static void ClearDirty(List<TransitionSet> sets)
            {
                foreach (TransitionSet set in sets)
                {
                    set.IsDirty = false;
                }
            }

            static bool TryFind(ITransitional t1, ITransitional t2, List<TransitionSet> items, out TransitionSet set)
            {
                foreach (TransitionSet item in items)
                {
                    if ((item.T1 == t1 && item.T2 == t2) || (item.T1 == t2 && item.T2 == t1))
                    {
                        set = item;
                        return true;
                    }
                }

                set = null;
                return false;
            }

            static internal void TryAdd(Transition transition, List<TransitionSet> items)
            {
                TransitionSet set = null;
                if (TryFind(transition.Source, transition.Dest, items, out set))
                {
                    if (!set.Transitions.Contains(transition))
                    {
                        set.Transitions.Add(transition);
                        set.IsDirty = true;
                    }
                }
                else
                {
                    List<Transition> transitions = new List<Transition>();
                    transitions.Add(transition);
                    items.Add(new TransitionSet() { T1 = transition.Source, T2 = transition.Dest, Transitions = transitions });
                }
            }

            static internal void TryRemove(Transition transition, List<TransitionSet> items)
            {
                TransitionSet set = null;
                if (TryFind(transition.Source, transition.Dest, items, out set))
                {
                    if (set.Transitions.Contains(transition))
                    {
                        set.Transitions.Remove(transition);
                        set.IsDirty = true;
                    }
                }
            }

            internal class Node
            {
                internal Node(Point position, Point[] springs)
                {
                    Position = position;
                    Springs = springs;
                    Velocity = new Vector();
                }

                internal Point[] Springs { get; private set; }

                internal Point Position { get; set; }
                internal Vector Velocity { get; set; }
            }
        }

        void MarkForInsert(Transition item)
        {
            TransitionHelper.TryAdd(item, _TransitionSetList);
        }

        void MarkForRemoval(Transition item)
        {
            TransitionHelper.TryRemove(item, _TransitionSetList);
        }

        Point[] GeneratePoints(Point midpoint, Vector direction, int count)
        {
            Point[] points = new Point[count];
            for (int i = 0; i < count; ++i)
            {
                points[i] = midpoint;
                midpoint += direction;
            }

            return points;
        }

        internal void UpdatePositionOfSelectedItems(double x, double y)
        {
            foreach (IDiagramElement element in Children.OfType<IDiagramElement>())
            {
                if (_SelectionService.IsSelected(element))
                {
                    element.Position = new Point(element.Position.X + x, element.Position.Y + y);
                }
            }
        }

        Vector GetRepulsiveForce(TransitionHelper.Node n0, TransitionHelper.Node n1)
        {
            double k = 8.9876 * Math.Pow(10.0, 9.0);

            Vector offset = n0.Position - n1.Position;
            double lengthsqrd = offset.LengthSquared;
            offset.Normalize();
            return k * ((0.008 * 0.008) / lengthsqrd) * offset;
        }

        Vector GetAttractionForce(TransitionHelper.Node node, Point spring)
        {
            Vector x = spring - node.Position;
            return 50.0 * x;
        }

        Point[] Scatter(Point midpoint, Vector direction, int count)
        {
            Point[] points = GeneratePoints(midpoint, direction, count);

            TransitionHelper.Node[] nodes = new TransitionHelper.Node[points.Length];
            for (int i = 0; i < points.Length; ++i)
            {
                nodes[i] = new TransitionHelper.Node(points[i], new Point[] { midpoint });
            }

            for (;;)
            {
                double kineticEnergy = 0;
                foreach (TransitionHelper.Node node in nodes)
                {
                    Vector force = new Vector();

                    foreach (TransitionHelper.Node other in nodes)
                    {
                        if (other != node)
                        {
                            force += GetRepulsiveForce(node, other);
                        }
                    }

                    foreach (Point spring in node.Springs)
                    {
                        force += GetAttractionForce(node, spring); ;
                    }

                    double offset = force * direction;
                    force = direction * offset;

                    node.Velocity = (node.Velocity + 0.1 * force) * 0.1;
                    node.Position = node.Position + 0.1 * node.Velocity;
                    kineticEnergy = kineticEnergy + 50.0 * (node.Velocity.Length);
                }

                if (kineticEnergy < 0.0001)
                {
                    break;
                }
            }

            Point[] result = new Point[points.Length];
            for (int i = 0; i < points.Length; ++i)
            {
                result[i] = nodes[i].Position;
            }

            return result;
        }

        void DiagramState_PositionChanged(object sender, RoutedEventArgs e)
        {
            DiagramState node = sender as DiagramState;
            foreach (TransitionSet set in _TransitionSetList)
            {
                if (set.T1 == (ITransitional)node.Source || 
                    set.T2 == (ITransitional)node.Source)
                {
                    set.IsDirty = true;
                }
            }

            UpdateDirtyTransitions();
        }

        internal void SelectAll()
        {
            _SelectionService.Clear();

            IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();
            foreach (IDiagramElement element in children)
            {
                _SelectionService.InsertSelectedItem(element);
            }
        }

        internal void SelectChild(IDiagramElement element)
        {
            if (element == null)
                return;

            _SelectionService.InsertSelectedItem(element);
        }

        internal void UnselectChild(IDiagramElement element)
        {
            if (element == null)
                return;

            _SelectionService.RemoveSelectedItem(element);
        }

        private static bool IsGoodDouble(double d)
        {
            return !double.IsNaN(d) && !double.IsInfinity(d);
        }

        private static bool IsGoodVector(Vector v)
        {
            return IsGoodDouble(v.X) && IsGoodDouble(v.Y);
        }

        public void InvalidateTransitions()
        {
            _TransitionSetList.Clear();

            ITransitional active = (ITransitional)DataModelService.CurrentActive;
            var automaton = active as IAutomaton;
            if (automaton != null)
            {
                foreach (Transition transition in automaton.Transitions)
                {
                    MarkForInsert(transition);
                }
            }

            UpdateDirtyTransitions();
        }

        void UpdateDirtyTransitions()
        {
            var dirtyTransitionSets = _TransitionSetList.Where(transSet => transSet.IsDirty);

            // Remove all of the DiragamTransition objects from the Children collection of they belong to a dirty set
            foreach (TransitionSet dirtyTransitionSet in dirtyTransitionSets)
            {
                IEnumerable<DiagramTransition> transitions = Children.OfType<DiagramTransition>();
                var copyOfTransitions = new List<DiagramTransition>(transitions);

                foreach (DiagramTransition transition in copyOfTransitions)
                {
                    if ((transition.Transition.Source == dirtyTransitionSet.T1 && transition.Transition.Dest == dirtyTransitionSet.T2) ||
                        (transition.Transition.Source == dirtyTransitionSet.T2 && transition.Transition.Dest == dirtyTransitionSet.T1))
                    {
                        transition.Cleanup();// This is a hack, but the whole job is real mess.  Nodes and connections don't have these issues :-(
                        Children.Remove(transition);
                        _TransitionEndDragController.RemoveTransition(transition);
                    }
                }
            }

            foreach (TransitionSet dirtyTransitionSet in dirtyTransitionSets)
            {
                IDiagramState t1 = dirtyTransitionSet.T1.Tag as IDiagramState;
                IDiagramState t2 = dirtyTransitionSet.T2.Tag as IDiagramState;
                Point p0 = t1.Center;
                Point p1 = t2.Center;

                if (p0.X == p1.X && p0.Y == p1.Y)
                {
                    continue;
                }

                Vector direction = p1 - p0;
                Point midpoint = new Point(p0.X + direction.X / 2.0, p0.Y + direction.Y / 2.0);

                Point source = new Point();
                double vx1 = midpoint.X;
                if (vx1 < t1.TopLeft.X) vx1 = t1.TopLeft.X;
                if (vx1 > t1.BottomRight.X) vx1 = t1.BottomRight.X;
                source.X = vx1;

                double vy1 = midpoint.Y;
                if (vy1 < t1.TopLeft.Y) vy1 = t1.TopLeft.Y;
                if (vy1 > t1.BottomRight.Y) vy1 = t1.BottomRight.Y;
                source.Y = vy1;

                Point dest = new Point();
                double vx = midpoint.X;
                if (vx < t2.TopLeft.X) vx = t2.TopLeft.X;
                if (vx > t2.BottomRight.X) vx = t2.BottomRight.X;
                dest.X = vx;

                double vy = midpoint.Y;
                if (vy < t2.TopLeft.Y) vy = t2.TopLeft.Y;
                if (vy > t2.BottomRight.Y) vy = t2.BottomRight.Y;
                dest.Y = vy;

                direction = dest - source;
                
                midpoint = new Point(source.X + direction.X / 2.0, source.Y + direction.Y / 2.0);

                Vector perpendicular = new Vector(-direction.Y, direction.X);
                perpendicular.Normalize();
                if (IsGoodVector(perpendicular))
                {
                    int count = dirtyTransitionSet.Transitions.Count;
                    Point[] scatter = Scatter(midpoint, perpendicular, count);

                    for (int i = 0; i < count; ++i)
                    {
                        Transition t = dirtyTransitionSet.Transitions[i];
                        if (t.Source.Tag as IDiagramState == t1)
                        {
                            InsertTransition(dirtyTransitionSet.Transitions[i], source, scatter[i], dest);
                        }
                        else
                        {
                            InsertTransition(dirtyTransitionSet.Transitions[i], dest, scatter[i], source);
                        }
                    }

                    dirtyTransitionSet.IsDirty = false;
                }
            }
        }

        // TODO: if one of the transitions changes position, i need to invalidate all transitions in the set
        void InsertTransition(Transition item, Point start, Point midpoint, Point end)
        {
            DiagramState source = item.Source.Tag as DiagramState;
            DiagramState dest = item.Dest.Tag as DiagramState;

            DiagramTransition transition = new DiagramTransition(source, dest, this, item, start, midpoint, end);
            item.Tag = transition;
            Canvas.SetZIndex(transition, Children.Count);
            Children.Add(transition);
            _TransitionEndDragController.AddTransition(transition);
        }

        Point TranslateWithOffset(Point point)
        {
            if (Children.Count != 0)
            {
                Point offset = new Point(Double.MaxValue, Double.MaxValue);

                foreach (UIElement element in Children)
                {
                    if (Canvas.GetLeft(element) < offset.X)
                        offset.X = Canvas.GetLeft(element);
                    if (Canvas.GetTop(element) < offset.Y)
                        offset.Y = Canvas.GetTop(element);
                }

                return new Point(point.X + offset.X, point.Y + offset.Y);
            }

            return point;
        }

        ISelectionService _SelectionService = null;
        public ISelectionService SelectionService
        {
            get { return _SelectionService; }
        }

        internal void InjectService(ISelectionService service)
        {
            _SelectionService = service;
            _NodeDragController.InjectService(service);
        }

        internal void SetActiveSelection()
        {
            _SelectionService.CurrentSelection = _Database.SelectedItems.FirstOrDefault();
        }

        internal void UnSelectItems()
        {
            _SelectionService.CurrentSelection = null;
        }

        void SelectionBehaviour_Selected(object sender, SelectedEventArgs e)
        {
            // Deselect everything if control is not held
            if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
            {
                ISelectable[] items = new ISelectable[_Database.SelectedItems.Count];
                _Database.SelectedItems.CopyTo(items, 0);
                foreach (ISelectable item in items)
                {
                    if (item != e.SelectedItem)
                    {
                        _Database.SelectedItems.Remove(item);
                    }
                }
            }

            // If the item isn't currently selected
            if (!_Database.SelectedItems.Contains(e.SelectedItem))
            {
                // Add it to the selection and tell the selection service about it
                _SelectionService.CurrentSelection = e.SelectedItem;

                _Database.SelectedItems.Add(e.SelectedItem);   
            }
        }

        public void ApplicationCommandDelete_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            ISelectable[] selectedItems = new ISelectable[_Database.SelectedItems.Count];
            _Database.SelectedItems.CopyTo(selectedItems, 0);

            foreach (ISelectable selectedItem in selectedItems)
            {
                if (selectedItem is Output || selectedItem is Input)
                    continue;

                selectedItem.Dispose();
            }

            _Database.SelectedItems.Clear();
        }

        public void ApplicationCommandDelete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _Database.SelectedItems.Count != 0;
            //e.Handled = true;
        }

        private NodeDragController _NodeDragController;
        private TransitionEndDragController _TransitionEndDragController;
    }
}