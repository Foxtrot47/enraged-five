﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public class DiagramAdorner : Adorner
    {
        public DiagramAdorner(Diagram canvas, Point source, IDiagramAnchor item)
            : base(canvas)
        {
            Source = source;

            Pen = new Pen(Brushes.LightSlateGray, 1);
            Pen.LineJoin = PenLineJoin.Round;
            Cursor = Cursors.Cross;

            Canvas = canvas;
            Item = item;

        }

        PathGeometry Geometry { get; set; }
        Point Source { get; set; }
        Pen Pen { get; set; }

        protected Diagram Canvas { get; private set; }
        protected IDiagramAnchor Item { get; private set; }

        IDiagramAnchor _Hit = null;
        IDiagramAnchor Hit 
        {
            get
            {
                return _Hit;
            }
            set
            {
                if (_Hit != null)
                {
                    _Hit.HighlightEnabled = false;
                }

                _Hit = value;

                if (_Hit != null)
                {
                    _Hit.HighlightEnabled = true;
                }
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!IsMouseCaptured)
                    CaptureMouse();

                Hit = Item.HitTest(e.GetPosition(this));

                Geometry = Calculate(e.GetPosition(this));

                InvalidateVisual();
            }
            else
            {
                if (IsMouseCaptured)
                    ReleaseMouseCapture();
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            ILogic logic = Item.Anchor.Parent;
            Motiontree parent = logic.Database.Find(logic.Parent) as Motiontree;

            if (Hit != null)
            {
                Hit.HighlightEnabled = false;
                parent.Connections.Add(new Connection(Item.Anchor, Hit.Anchor));
            }

            if (IsMouseCaptured)
                ReleaseMouseCapture();

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(Canvas);
            if (layer != null)
                layer.Remove(this);
        }

        protected override void OnRender(DrawingContext context)
        {
            base.OnRender(context);

            context.DrawGeometry(null, Pen, Geometry);
            context.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));
        }

        PathGeometry Calculate(Point position)
        {
            PathGeometry geometry = new PathGeometry();

            PathFigure figure = new PathFigure();
            figure.StartPoint = Source;

            PointCollection points = new PointCollection();
            points.Add(new Point(((position.X - Source.X) / 2) + Source.X, Source.Y));
            points.Add(new Point(((position.X - Source.X) / 2) + Source.X, position.Y));
            points.Add(position);

            figure.Segments.Add(new PolyBezierSegment(points, true));
            geometry.Figures.Add(figure);

            return geometry;
        }
    }
}