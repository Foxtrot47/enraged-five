﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

using New.Move.Core;
using Rockstar.MoVE.Services;

namespace Rockstar.MoVE.Design
{
    internal class TransitionEndDragController
    {
        private enum TransitionPart
        {
            Start,
            Middle,
            End
        }

        internal TransitionEndDragController(Diagram diagram)
        {
            diagram_ = diagram;
            transitions_ = new List<DiagramTransition>();
        }

        internal void AddTransition(DiagramTransition transition)
        {
            transitions_.Add(transition);
            transition.MouseDown += new MouseButtonEventHandler(OnMouseDownOverTransition);
            transition.MouseUp += new MouseButtonEventHandler(OnMouseUpOverTransition);
        }

        internal void RemoveTransition(DiagramTransition transition)
        {
            transitions_.Remove(transition);
            transition.MouseDown -= new MouseButtonEventHandler(OnMouseDownOverTransition);
            transition.MouseUp -= new MouseButtonEventHandler(OnMouseUpOverTransition);
        }

        void OnMouseDownOverTransition(object sender, MouseButtonEventArgs e)
        {
            DiagramTransition diagramTransition = (DiagramTransition)sender;
            Point mouseDownPosition = e.GetPosition(diagram_);

            TransitionPart partBelowMouse = DeterminePartAtPosition(mouseDownPosition, diagramTransition);
            if (partBelowMouse == TransitionPart.Start)
            {
                diagramTransition.IsBeingDraggedFromStart = true;
                diagramTransition.DragStart = mouseDownPosition;

                diagramTransition.CaptureMouse();
                diagramTransition.MouseMove += new MouseEventHandler(OnMouseMoveOverTransition);
            }
            else if (partBelowMouse == TransitionPart.End)
            {
                diagramTransition.IsBeingDraggedFromEnd = true;
                diagramTransition.DragEnd = mouseDownPosition;

                diagramTransition.CaptureMouse();
                diagramTransition.MouseMove += new MouseEventHandler(OnMouseMoveOverTransition);
            }
        }

        void OnMouseMoveOverTransition(object sender, MouseEventArgs e)
        {
            DiagramTransition diagramTransition = (DiagramTransition)sender;

            Point mousePosition = e.GetPosition(diagram_);

            if (diagramTransition.IsBeingDraggedFromStart)
            {
                diagramTransition.DragStart = mousePosition;
            }
            else if (diagramTransition.IsBeingDraggedFromEnd)
            {
                diagramTransition.DragEnd = mousePosition;
            }
        }

        void OnMouseUpOverTransition(object sender, MouseButtonEventArgs e)
        {
            DiagramTransition diagramTransition = (DiagramTransition)sender;

            diagramTransition.MouseMove -= new MouseEventHandler(OnMouseMoveOverTransition);
            diagramTransition.ReleaseMouseCapture();

            Point mousePosition = e.GetPosition(diagram_);

            DiagramState diagramStateUnderMouse = GetDiagramStateAtPosition(mousePosition);

            if (diagramStateUnderMouse != null)
            {
                Transition transition = (Transition)diagramTransition.Tag;
                ITransitional transitionalUnderMouse = (ITransitional)diagramStateUnderMouse.Tag;

                if (diagramTransition.IsBeingDraggedFromStart)
                {
                    if ((transitionalUnderMouse != transition.Source) && (transitionalUnderMouse != transition.Dest))
                    {
                        transition.Source.TransitionOrder.Remove(transition);
                        transition.Source = transitionalUnderMouse;
                        transitionalUnderMouse.TransitionOrder.Add(transition);
                    }
                }
                else if (diagramTransition.IsBeingDraggedFromEnd)
                {
                    if ((transitionalUnderMouse != transition.Source) && (transitionalUnderMouse != transition.Dest))
                    {
                        transition.Dest = transitionalUnderMouse;
                    }
                }
            }

            diagramTransition.IsBeingDraggedFromStart = false;
            diagramTransition.IsBeingDraggedFromEnd = false;            
        }

        private TransitionPart DeterminePartAtPosition(Point position, DiagramTransition diagramTransition)
        {
            double mouseDownToStart = (position - diagramTransition.Start).Length;
            double mouseDownToMid = (position - diagramTransition.Mid).Length;
            double mouseDownToEnd = (position - diagramTransition.End).Length;

            if (mouseDownToStart < mouseDownToEnd && mouseDownToStart < mouseDownToMid)
                return TransitionPart.Start;
            else if (mouseDownToEnd < mouseDownToStart && mouseDownToEnd < mouseDownToMid)
                return TransitionPart.End;

            return TransitionPart.Middle;
        }

        private DiagramState GetDiagramStateAtPosition(Point position)
        {
            foreach (var child in diagram_.Children)
            {
                if (child is DiagramState)
                {
                    DiagramState diagramState = (DiagramState)child;
                    if (position.X >= diagramState.Left &&
                        position.X <= diagramState.Left + diagramState.ActualWidth &&
                        position.Y >= diagramState.Top &&
                        position.Y <= diagramState.Top + diagramState.ActualHeight)
                    {
                        return diagramState;
                    }
                }
            }

            return null;
        }

        private Diagram diagram_;// required to access database.

        private List<DiagramTransition> transitions_;
    }
}
