﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MoVE.Core;
using Rockstar.MoVE.Design;

namespace Rockstar.MoVE.Design
{
    public partial class DiagramNetwork : DiagramNode
    {
        public DiagramNetwork(Network source)
            : base(source)
        {
            InitializeComponent();

            Result.Children.Clear();

            DiagramAnchorResultTransform result = new DiagramAnchorResultTransform("Result", source.Result);
            source.Result.Tag = result;
            result.Container = this;
            Result.Children.Add(result);
        }

        public ImageSource Icon
        {
            get
            {
                Assembly iconAssembly = Assembly.GetAssembly(typeof(Rockstar.MoVE.Framework.DescriptorGroup));

                BitmapImage bitmapImage = new BitmapImage();
                string iconName = "";

                switch (Type)
                {
                    case "Network": { iconName = "MotionTree"; } break;
//                     default: { Console.WriteLine("***** Type not found: " + Type + " *****"); } break;
                }

                try
                {
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = iconAssembly.GetManifestResourceStream("Rockstar.MoVE.Framework.Media." + iconName + ".png");
                    bitmapImage.EndInit();

                    return bitmapImage;
                }
                catch
                {
                    return null;                	
                }
            }
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            MenuItem deleteItem = new MenuItem() { Header = "Delete node" };
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);

            contextMenu.Items.Add(deleteItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;
            contextMenu.Items.Clear();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }
    }
}
