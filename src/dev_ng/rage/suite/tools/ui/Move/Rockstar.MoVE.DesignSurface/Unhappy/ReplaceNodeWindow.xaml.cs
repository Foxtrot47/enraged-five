﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    /// <summary>
    /// Interaction logic for ReplaceNodeWindow.xaml
    /// </summary>
    public partial class ReplaceNodeWindow : Window
    {
        public ReplaceNodeWindow(ITransitional nodeToBeReplaced, IEnumerable<ITransitional> replacementNodes)
        {
            InitializeComponent();

            textBlockDeclaration.Text = string.Format(textBlockDeclaration.Text, nodeToBeReplaced.ToString());

            foreach (var replacementNode in replacementNodes)
                listBoxNodes.Items.Add(replacementNode);
        }

        public ITransitional ReplacementSelected
        {
            get { return (ITransitional)listBoxNodes.SelectedItem; }
        }

        private void OnOKClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void OnCancelClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            buttonOK.IsEnabled = listBoxNodes.SelectedItems.Count == 1;
        }
    }
}
