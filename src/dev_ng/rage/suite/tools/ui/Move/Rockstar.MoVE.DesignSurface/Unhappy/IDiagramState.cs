﻿using System.Windows;

namespace Rockstar.MoVE.Design
{
    public interface IDiagramState
    {
        Point Center { get; }
        Point TopCenter { get; }
        Point TopRight { get; }
        Point TopLeft { get; }
        Point BottomCenter { get; }
        Point BottomRight { get; }
        Point LeftCenter { get; }
        Point RightCenter { get; }
    }
}