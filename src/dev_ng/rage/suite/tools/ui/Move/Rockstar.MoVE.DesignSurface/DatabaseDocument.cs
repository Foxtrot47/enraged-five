﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using AvalonDock;

using New.Move.Core;

using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Services;

using Move.Utils;
using Rockstar.MoVE.Net.Connectivity;
using System.Reflection;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using System.Xml;
using System.Text;
using Rockstar.MoVE.Framework.DataModel.Prototypes;
using RSG.AnimClipEditor.ViewModel;
using System.Diagnostics;

namespace Rockstar.MoVE.Design
{
    internal class DatabaseDocument : DocumentContent, IPersistentContent
    {
        static class Const
        {
            public static readonly Point Offset = new Point(2048, 2048);
        }

        static DatabaseDocument()
        {
            try
            {
                CheckOutFiles();
            }
            catch (Exception ex)
            {
                LogFactory.ApplicationLog.Error("Exception in the static DatabaseDocument constructor");
            }
        }

        private static void CheckOutFiles()
        {
            string metaDir1 = ContentConfig.MetadataDirectory + "\\definitions\\tools\\animation\\...";
            string metaDir2 = ContentConfig.MetadataDirectory + "\\definitions\\rage\\framework\\src\\fwanimation\\...";
            string metaDir3 = ContentConfig.Config.Project.DefaultBranch.Common + "\\data\\anim\\clip_sets\\...";
            string metaDir4 = ContentConfig.Config.Project.DefaultBranch.Export + "\\anim\\expressions\\...";

            string metaFile1 = ContentConfig.ToolsConfigDirectory + "\\config\\anim\\clip_tag_definitions.xml";
            string metaFile2 = ContentConfig.ToolsConfigDirectory + "\\config\\anim\\clip_property_definitions.xml";
            string metaFile3 = Path.Combine(ContentConfig.AssetsDirectory, "anim", "filters", "filters.xml");
            string metaFile4 = ContentConfig.ExportedFiltersFilename;

            string[] syncLocations = new string[] { metaDir1, metaDir2, metaDir3, metaDir4, metaFile1, metaFile2, metaFile3, metaFile4 };
            string[] syncDirectories = new string[] { metaDir1, metaDir2, metaDir3, metaDir4 };

            CheckOutFiles(syncLocations, syncDirectories);

            //Fallback hack
            if (!File.Exists(metaFile4))
            {
                ContentConfig.ExportedFiltersFilenameOverride = "x:\\gta5\\assets_ng\\export\\anim\\filters.zip";
                metaFile4 = ContentConfig.ExportedFiltersFilename;
                syncLocations = new string[] { metaFile4 };
                syncDirectories = new string[] {};
                CheckOutFiles(syncLocations, syncDirectories);
            }
        }

        private static void CheckOutFiles(string[] syncLocations, string[] syncDirectories)
        {
            foreach (string syncDirectory in syncDirectories)
            {
                //  Create the directory if it doesn't already exist
                if (!Directory.Exists(syncDirectory))
                {
                    Directory.CreateDirectory(syncDirectory);
                }
            }

            P4 pfConnection = new P4();
            string currentDirectory = Directory.GetCurrentDirectory();
            try
            {
                pfConnection.Connect();
                foreach (string syncLocation in syncLocations)
                {
                    string dirName = Path.GetDirectoryName(syncLocation);
                    if (!Directory.Exists(dirName))
                    {
                        Directory.CreateDirectory(dirName);
                    }

                    Directory.SetCurrentDirectory(dirName);
                    pfConnection.Run("sync", syncLocation);
                }
            }
            catch
            { }
            finally
            {
                pfConnection.Disconnect();
                Directory.SetCurrentDirectory(currentDirectory);
            }
        }

        public DatabaseDocument(IDataModel model, IApplicationService appService, IDataModelService dataModelService, ITransitionalDesc desc)
        {
            Viewer = new DiagramViewer(dataModelService, model);
            Viewer.Diagram.DataModelService = dataModelService;

            ApplicationService = appService;

            FKeyGest = new KeyGesture(Key.F, ModifierKeys.Control);
            AKeyGest = new KeyGesture(Key.A, ModifierKeys.Control);
            XKeyGest = new KeyGesture(Key.X, ModifierKeys.Control);
            CKeyGest = new KeyGesture(Key.C, ModifierKeys.Control);
            VKeyGest = new KeyGesture(Key.V, ModifierKeys.Control);
            DelKeyGest = new KeyGesture(Key.Delete, ModifierKeys.Control);
            ApplicationService.RegisterKeyGestureDownHandler(FKeyGest, FocusDiagramViewer);
            ApplicationService.RegisterKeyGestureDownHandler(AKeyGest, SelectAllDiagramElements);
            ApplicationService.RegisterKeyGestureDownHandler(XKeyGest, Cut);
            ApplicationService.RegisterKeyGestureDownHandler(CKeyGest, Copy);
            ApplicationService.RegisterKeyGestureDownHandler(VKeyGest, Paste);
            ApplicationService.RegisterKeyGestureDownHandler(DelKeyGest, Delete);

            Database = new Database(desc, model, dataModelService, Const.Offset, new DatabaseNodeFactory());
            if (desc is MotiontreeDesc)
            {
                Title = "New Motion Tree";
            }
            else
            {
                Title = "New State Machine";
            }
            Database.FullyQualifiedFilenameChanged += new RoutedEventHandler(FullyQualifiedFilenameChanged);
            Database.DirtyChanged += new RoutedEventHandler(DirtyChanged);
            Viewer.Diagram.Database = Database;

            DataModelService = dataModelService;

            AllowDrop = true;
            Drop += new DragEventHandler(Viewer.Diagram.DocumentContent_Drop);

            PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
        }

        KeyGesture FKeyGest { get; set; }
        KeyGesture AKeyGest { get; set; }
        KeyGesture XKeyGest { get; set; }
        KeyGesture CKeyGest { get; set; }
        KeyGesture VKeyGest { get; set; }
        KeyGesture DelKeyGest { get; set; }

        internal DatabaseDocument(IDataModel model, IApplicationService appService, IDataModelService dataModelService, string filename)
        {
            Database = new Database(model, dataModelService, Const.Offset, new DatabaseNodeFactory());
            Database.Load(filename);

            Viewer = new DiagramViewer(dataModelService, model);
            Viewer.Diagram.DataModelService = dataModelService;

            ApplicationService = appService;

            FKeyGest = new KeyGesture(Key.F, ModifierKeys.Control);
            AKeyGest = new KeyGesture(Key.A, ModifierKeys.Control);
            XKeyGest = new KeyGesture(Key.X, ModifierKeys.Control);
            CKeyGest = new KeyGesture(Key.C, ModifierKeys.Control);
            VKeyGest = new KeyGesture(Key.V, ModifierKeys.Control);
            DelKeyGest = new KeyGesture(Key.Delete, ModifierKeys.Control);
            ApplicationService.RegisterKeyGestureDownHandler(FKeyGest, FocusDiagramViewer);
            ApplicationService.RegisterKeyGestureDownHandler(AKeyGest, SelectAllDiagramElements);
            ApplicationService.RegisterKeyGestureDownHandler(XKeyGest, Cut);
            ApplicationService.RegisterKeyGestureDownHandler(CKeyGest, Copy);
            ApplicationService.RegisterKeyGestureDownHandler(VKeyGest, Paste);
            ApplicationService.RegisterKeyGestureDownHandler(DelKeyGest, Delete);

            Viewer.Zoom = Database.Active.Zoom;
            Viewer.ScrollTo(Database.Active.ScrollOffset);
            
            Title = Path.GetFileNameWithoutExtension(Database.FullyQualifiedFilename);
            if (Database.IsDirty == true)
            {
                Database.FullyQualifiedFilename = string.Empty;
            }
            Database.FullyQualifiedFilenameChanged += new RoutedEventHandler(FullyQualifiedFilenameChanged);
            Database.DirtyChanged += new RoutedEventHandler(DirtyChanged);
            Viewer.Diagram.Database = Database;

            DataModelService = dataModelService;

            AllowDrop = true;
            Drop += new DragEventHandler(Viewer.Diagram.DocumentContent_Drop);

            PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
        }

        internal void CentreOnDiagramNode(DiagramNode diagramNode)
        {
            if (IsActiveDocument)
            {
                Viewer.CentreOnDiagramNode(diagramNode);
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            List<ClipPrototype> clips = new List<ClipPrototype>();
            foreach (INode node in Database.Nodes)
            {
                ClipPrototype clip = node as ClipPrototype;
                if (clip == null)
                {
                    continue;
                }

                if (clip.Clip.Source == New.Move.Core.ClipSource.LocalFile)
                {
                    if (clip.ClipViewModel != null && clip.ClipViewModel.DirtyState)
                    {
                        clips.Add(clip);
                    }
                }
            }

            if (clips.Count > 0)
            {
                foreach (ClipPrototype prototype in clips)
                {
                    MessageBoxResult result = MessageBox.Show(String.Format("Save changes to {0}?", Path.GetFileName(prototype.Clip.Filename)),
                        "Save?", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);

                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            if (!prototype.ClipViewModel.SaveClip())
                            {
                                e.Cancel = true;
                            }
                            break;
                        case MessageBoxResult.No:
                            // do nothing...
                            break;
                        case MessageBoxResult.Cancel:
                            e.Cancel = true;
                            break;
                    }
                }
            }

            if (Database.IsDirty && !e.Cancel)
            {
                MessageBoxResult result = MessageBox.Show(String.Format("Save changes to {0}?", Path.GetFileName(FullyQualifiedFilename)),
                    "Save?", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        if (!this.ApplicationService.ExecuteSaveCommandWithResult())
                        {
                            e.Cancel = true;
                        }
                        break;
                    case MessageBoxResult.No:
                        // do nothing...
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }

            if (e.Cancel == false)
            {
                //  Clear the database elements
                Database.Messages.Clear();
                Database.Signals.Clear();
                Database.Events.Clear();
                Database.Requests.Clear();
                Database.Flags.Clear();

                Database.Nodes.Clear();

                Database.VariableClipSets.Clear();

                Database.SelectedItems.Clear();

                Database.Root.Children.Clear();

                //  Clear the selected node, if there is one
                Viewer.Diagram.UnSelectItems();
            }

            base.OnClosing(e);
        }

        protected override void OnClosed()
        {
            ApplicationService.UnregisterKeyGestureDownHandler(FKeyGest, FocusDiagramViewer);
            ApplicationService.UnregisterKeyGestureDownHandler(AKeyGest, SelectAllDiagramElements);
            ApplicationService.UnregisterKeyGestureDownHandler(XKeyGest, Cut);
            ApplicationService.UnregisterKeyGestureDownHandler(CKeyGest, Copy);
            ApplicationService.UnregisterKeyGestureDownHandler(VKeyGest, Paste);
            ApplicationService.UnregisterKeyGestureDownHandler(DelKeyGest, Delete);
            base.OnClosed();
        }

        internal void InjectService(ISelectionService service)
        {
            Viewer.Diagram.InjectService(service);
        }

        void SelectAllDiagramElements(object sender, SimpleEventArgs<KeyDownMessage> e)
        {
            if (IsActiveDocument)
            {
                Viewer.Diagram.SelectAll();
            }
        }

        void FocusDiagramViewer(object sender, SimpleEventArgs<KeyDownMessage> e)
        {
            this.FocusViewer();
        }

        internal void FocusViewer()
        {
            if (IsActiveDocument)
            {
                Viewer.Focus();
            }
        }

        void ActiveChanging()
        {
            if (IsActiveDocument)
            {
                Viewer.Diagram.ActiveChanging();
            }
        }

        void ActiveChanged()
        {
            if (IsActiveDocument)
            {
                Viewer.Diagram.ActiveChanged();
            }
        }

        void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // LPXO: This isn't safe as the event handler is not correctly removed when the tab is closed so we keep stacking event handlers and end up
            // updating the diagram over and over.  FIX
            
            if ((String.Compare(e.PropertyName, "IsActiveDocument") == 0))// || (String.Compare(e.PropertyName, "IsActiveContent") == 0))
            {
                if (IsActiveDocument)
                {
                    DataModelService.CurrentActiveChanging += new CurrentActiveChangingHandler(ActiveChanging);
                    DataModelService.CurrentActiveChanged += new CurrentActiveChangedHandler(ActiveChanged);
                    Viewer.Diagram.SetActiveSelection();
                }
                else
                {
                    DataModelService.CurrentActiveChanging -= new CurrentActiveChangingHandler(ActiveChanging);
                    DataModelService.CurrentActiveChanged -= new CurrentActiveChangedHandler(ActiveChanged);
                }
            }
             
        }

        IApplicationService ApplicationService { get; set; }
        IDataModelService DataModelService { get; set; }

        DiagramViewer Viewer
        {
            get
            {
                return Content as DiagramViewer;
            }
            set
            {
                Content = value;
            }
        }

        internal Database Database
        {
            get
            {
                return Tag as Database;
            }
            set
            {
                Tag = value;
            }
        }

        public string FullyQualifiedFilename
        {
            get
            {
                return Database.FullyQualifiedFilename;
            }
        }

        void FullyQualifiedFilenameChanged(object sender, RoutedEventArgs e)
        {
            string filename = Path.GetFileNameWithoutExtension(Database.FullyQualifiedFilename);
            Title = Database.IsDirty ? String.Concat(filename, "*") : filename.TrimEnd('*');
        }

        void DirtyChanged(object sender, RoutedEventArgs e)
        {
            Title = Database.IsDirty ? String.Concat(Title, "*") : Title.TrimEnd('*');
        }

        public bool IsDirty
        {
            get
            {
                return Database.IsDirty;
            }
        }

        public void Save()
        {
            if (Rockstar.MoVE.Components.Properties.Settings.Default.AutoBackup)
            {
                string tempPath = Path.Combine(ContentConfig.ToolsTempDirectory, "MoVE", "Backup");
                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }

                string backupName = string.Format("Backup_{0}.mxtf", this.Title);
                string backupPath = Path.Combine(tempPath, backupName);
                if (File.Exists(backupPath))
                {
                    FileAttributes attributes = File.GetAttributes(backupPath);
                    if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        // Make the file RW
                        attributes = attributes & ~FileAttributes.ReadOnly;
                        File.SetAttributes(backupPath, attributes);
                    } 
                }

                this.Database.SaveXmlFormat(Path.Combine(tempPath, backupName));
            }

            Database.Save();
        }

        public void Save(string filename)
        {
            if (Rockstar.MoVE.Components.Properties.Settings.Default.AutoBackup)
            {
                string tempPath = Path.Combine(ContentConfig.ToolsTempDirectory, "MoVE", "Backup");
                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }

                string backupName = string.Format("Backup_{0}.mxtf", this.Title);
                string backupPath = Path.Combine(tempPath, backupName);
                if (File.Exists(backupPath))
                {
                    FileAttributes attributes = File.GetAttributes(backupPath);
                    if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        // Make the file RW
                        attributes = attributes & ~FileAttributes.ReadOnly;
                        File.SetAttributes(backupPath, attributes);
                    }
                }

                this.Database.SaveXmlFormat(Path.Combine(tempPath, backupName));
            }

            string path = Path.GetDirectoryName(filename);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            Database.Save(filename);
        }

        public void SaveSelection(string filename)
        {
            if (this.Database.SelectedItems.OfType<INode>().Count() == 0)
            {
                MessageBox.Show(
                "Unable to save the selection when that selection doesn't contain any nodes.", "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            }

            string path = Path.GetDirectoryName(filename);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (this.Database.Active is Motiontree)
            {
                this.Database.SaveXmlFormatSelected(filename);
            }
            else if (this.Database.Active is StateMachine)
            {
                this.Database.SaveXmlFormatSelected(filename);
            }

            MessageBox.Show(
                "Unable to save the selection inside a node that isn't either a Motion Tree or a State Machine.", "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
        }

        public void Export(string imvfPathname, string nodemapPathname, bool build, bool prototype, bool preview)
        {
            Database.MakeNodeNamesUnique();

            DatabaseValidator validator = new DatabaseValidator(Database);
            LogFactory.ApplicationLog.Message("Validating network...");
            validator.Validate();

            if (validator.HasErrors)
            {
                LogFactory.ApplicationLog.Message("Network is invalid.  Export aborted.");
                string msg = "The current network is invalid and could not be exported. The target file has not been overwritten. Check the log for more details";
                MessageBox.Show(msg, "Network Validation Failed");
                return;
            }
            else if (validator.HasWarnings)
            {
                LogFactory.ApplicationLog.Message(
                    "Network is valid, but some issues were detected.  See warnings above.");
            }
            else
            {
                LogFactory.ApplicationLog.Message("Network is valid.");
            }

            // Build target file pathnames
            string filename = Path.GetFileNameWithoutExtension(imvfPathname);
            if (string.IsNullOrEmpty(nodemapPathname))
            {
                string nodeMapDirectory = ContentConfig.NodeMapExportPath;
                nodemapPathname = Path.Combine(nodeMapDirectory, "anim", "move", "nodemaps", filename + "_nodemap.xml");
            }
            var scFilenames = new string[] { imvfPathname, nodemapPathname };
            foreach (string file in scFilenames)
            {
                string dirName = Path.GetDirectoryName(file);
                //  Create the directory if it does not exist
                if (!Directory.Exists(dirName))
                {
                    Directory.CreateDirectory(dirName);
                }
            }

            bool canContinue = true;
            var targetPathnames = new string[] { imvfPathname, nodemapPathname };
            List<string> readOnlyList = new List<string>();
            if (Rockstar.MoVE.Components.Properties.Settings.Default.PerforceIntegration)
            {
                // Make sure all files are readonly and if in source control checked out.
                P4 pfConnection = new P4();
                string currentDirectory = Directory.GetCurrentDirectory();
                string changelistDescription = "Automatic MoVE changelist for " + filename + ".imvf\n";
                try
                {
                    pfConnection.Connect();
                    int changeListNumber = -1;
                    // Open the file for edit in an existing changelist if one is available
                    P4API.P4RecordSet previousChangeLists = pfConnection.Run("changelists", "-l", "-s", "pending", "-u", pfConnection.User, "-c", pfConnection.Client);
                    foreach (P4API.P4Record record in previousChangeLists.Records)
                    {
                        if (record.Fields.ContainsKey("desc"))
                        {
                            String description = record.Fields["desc"];
                            if (description == changelistDescription)
                            {
                                if (changeListNumber == -1)
                                {
                                    int.TryParse(record.Fields["change"], out changeListNumber);
                                }
                            }
                        }
                    }

                    if (changeListNumber == -1)
                    {
                        P4API.P4PendingChangelist changeList = pfConnection.CreatePendingChangelist("Automatic MoVE changelist for " + filename + ".mrf");
                        changeListNumber = changeList.Number;
                    }

                    foreach (string file in scFilenames)
                    {
                        try
                        {
                            string dirName = Path.GetDirectoryName(file);
                            Directory.SetCurrentDirectory(dirName);

                            pfConnection.Run("sync", file);
                            pfConnection.Run("edit", "-c", changeListNumber.ToString(), file);
                            pfConnection.Run("reopen", "-c", changeListNumber.ToString(), file);
                            pfConnection.Run("add", "-c", changeListNumber.ToString(), file);
                        }
                        catch
                        {
                            LogFactory.ApplicationLog.Warning("Perforce error occurred while checking files out.");
                        }
                    }

                    // Clear up any previous and now empty created changelists
                    P4API.P4RecordSet currentChangeLists = pfConnection.Run("changelists", "-l", "-s", "pending", "-u", pfConnection.User, "-c", pfConnection.Client);
                    foreach (P4API.P4Record record in currentChangeLists.Records)
                    {
                        if (record.Fields.ContainsKey("desc"))
                        {
                            String description = record.Fields["desc"];
                            if (description == changelistDescription)
                            {
                                if (record.Fields.ContainsKey("change"))
                                {
                                    pfConnection.Run("change", "-d", record.Fields["change"]);
                                }
                            }
                        }
                    }
                }
                catch
                { }
                finally
                {
                    pfConnection.Disconnect();
                    Directory.SetCurrentDirectory(currentDirectory);

                }
            }

            // check that none of the target files are read-only
            foreach (string targetPathname in targetPathnames)
            {
                if (File.Exists(targetPathname))
                {
                    var targetFileInfo = new FileInfo(targetPathname);
                    if (targetFileInfo.Attributes.HasFlag(FileAttributes.ReadOnly))
                    {
                        readOnlyList.Add(targetPathname);
                    }
                }
            }

            // Error if any of the target files are read only
            if (readOnlyList.Count > 0)
            {
                var errorMessage = new System.Text.StringBuilder();
                errorMessage.AppendLine("Unable to export network as the following target files are read-only:");
                errorMessage.AppendLine();
                foreach (string readOnlyTargetPathname in readOnlyList)
                {
                    errorMessage.AppendLine(string.Format("'{0}'", readOnlyTargetPathname));
                }

                MessageBox.Show(errorMessage.ToString(), "Read-only target files encountered");
                LogFactory.ApplicationLog.Error(errorMessage.ToString());
                canContinue = false;
            }

            if (!canContinue)
            {
                return;
            }

            // Build XML
            var xmlSerializer = new DatabaseXMLSerializer();
            try
            {
                xmlSerializer.Export(imvfPathname, Database, nodemapPathname);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format("Unexpected error during export as IMVF to '{0}'.  Check the log for more details.", imvfPathname), 
                    "Unexpected error during export");
                LogFactory.ApplicationLog.Error("Unexpected error during export as IMVF to '{0}'.  Details follow.", imvfPathname);
                LogFactory.ApplicationLog.Message(ex.ToString());
                return;
            }

            LogFactory.ApplicationLog.Message("Successfully exported network as IMVF to '{0}'", imvfPathname);
            LogFactory.ApplicationLog.Message("Nodemap saved to {0}", nodemapPathname);
            string convertExe = Path.Combine(ContentConfig.Config.ToolsRoot, "ironlib", "lib", "RSG.Pipeline.Convert.exe");
            string branch = "--branch " + ContentConfig.Config.Project.DefaultBranch.Name;
            if (build)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = convertExe;
                startInfo.Arguments = string.Format("{0} {1}", branch, imvfPathname);
                startInfo.UseShellExecute = false;
                System.Diagnostics.Process buildProcess = System.Diagnostics.Process.Start(startInfo);
                buildProcess.WaitForExit();
            }
            else if (prototype || preview)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = convertExe;
                startInfo.Arguments = string.Format("{0} --preview {1}", branch, imvfPathname);
                startInfo.UseShellExecute = false;
                Process buildProcess = Process.Start(startInfo);
                buildProcess.WaitForExit();

                string mrfFilename = Path.Combine(ContentConfig.Config.Project.DefaultBranch.Preview, Path.GetFileNameWithoutExtension(imvfPathname) + ".mrf");
                if (prototype)
                {
                    List<string> clipSets;
                    ContentConfig.LoadClipSets(out clipSets);
                    clipSets.Insert(0, "No clip set");
                    var animSetsPopup = new Rockstar.MoVE.Components.AnimSetsPrompt();
                    animSetsPopup.DataContext = clipSets;
                    bool? dialogResult = animSetsPopup.ShowDialog();
                    if (dialogResult == true)
                    {
                        ConnectionManager.RunNetwork(Database, animSetsPopup.AnimSet, mrfFilename);
                    }
                }
            }

            if (validator.HasWarnings)
            {
                MessageBox.Show("The export was successful, but there were some issues detected during validation.  Check the log for more details", "Export Warnings");
            }
        }

        public bool Export(string imvf, string nodemap, bool build)
        {
            string filename = Path.GetFileNameWithoutExtension(imvf);
            var targetPathnames = new string[] { imvf, nodemap };

            foreach (string targetPathname in targetPathnames)
            {
                if (File.Exists(targetPathname))
                {
                    var targetFileInfo = new FileInfo(targetPathname);
                    if (targetFileInfo.Attributes.HasFlag(FileAttributes.ReadOnly))
                    {
                        LogFactory.ApplicationLog.Error("Unable to export the file '{0}' due to the file '{1}' being read-only", filename, targetPathname);
                        return false;
                    }
                }
            }
            
            try
            {
                DatabaseXMLSerializer xmlSerializer = new DatabaseXMLSerializer();
                xmlSerializer.Export(imvf, this.Database, nodemap);
            }
            catch (Exception ex)
            {
                LogFactory.ApplicationLog.Exception(ex, "Unexpected error during export as IMVF to '{0}'.  Details follow.", imvf);
                return false;
            }

            LogFactory.ApplicationLog.Message("Successfully exported network as IMVF to '{0}'", imvf);
            if (build)
            {
                string convertExe = Path.Combine(ContentConfig.Config.ToolsRoot, "ironlib", "lib", "RSG.Pipeline.Convert.exe");
                string branch = "--branch " + ContentConfig.Config.Project.DefaultBranch.Name;

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = convertExe;
                startInfo.Arguments = string.Format("{0} {1}", branch, imvf);
                startInfo.UseShellExecute = false;
                Process buildProcess = Process.Start(startInfo);
                buildProcess.WaitForExit();
            }

            return true;
        }

        public static bool PerforceCheckout(IEnumerable<string> filenames, string desctription)
        {
            P4 p4 = new P4();
            string currentDirectory = Directory.GetCurrentDirectory();
            try
            {
                p4.Connect();
                int changeListNumber = -1;
                P4API.P4RecordSet previousChangeLists = p4.Run("changelists", "-l", "-s", "pending", "-u", p4.User, "-c", p4.Client);
                foreach (P4API.P4Record record in previousChangeLists.Records)
                {
                    if (!record.Fields.ContainsKey("desc"))
                    {
                        continue;
                    }

                    String description = record.Fields["desc"];
                    if (string.CompareOrdinal(description, desctription) != 0)
                    {
                        continue;
                    }

                    if (!int.TryParse(record.Fields["change"], out changeListNumber))
                    {
                        changeListNumber = -1;
                    }

                    break;
                }

                if (changeListNumber == -1)
                {
                    P4API.P4PendingChangelist changeList = p4.CreatePendingChangelist(desctription);
                    changeListNumber = changeList.Number;
                }

                foreach (string filename in filenames)
                {
                    try
                    {
                        string dirName = Path.GetDirectoryName(filename);
                        Directory.SetCurrentDirectory(dirName);

                        p4.Run("sync", filename);
                        p4.Run("edit", "-c", changeListNumber.ToString(), filename);
                        p4.Run("reopen", "-c", changeListNumber.ToString(), filename);
                        p4.Run("add", "-c", changeListNumber.ToString(), filename);
                    }
                    catch
                    {
                        LogFactory.ApplicationLog.Warning("Perforce error occurred while checking '{0}' out.", filename);
                    }
                }
            }
            catch (Exception ex)
            {
                LogFactory.ApplicationLog.Exception(ex, "Unhandled perforce exception");
                return false;
            }
            finally
            {
                p4.Disconnect();
                Directory.SetCurrentDirectory(currentDirectory);
            }

            return true;
        }

        NoResetObservableCollection<ISelectable> clipboard = new NoResetObservableCollection<ISelectable>();
        bool cutting = false;
        void Cut(object sender, SimpleEventArgs<KeyDownMessage> e)
        {
            cutting = true;
            this.Copy(sender, e);
            cutting = false;
            List<ISelectable> selection = new List<ISelectable>(this.Database.SelectedItems);
            foreach (ISelectable selectable in selection)
            {
                if (selectable is Output || selectable is Input)
                {
                    continue;
                }

                INode selectedNode = selectable as INode;
                if (selectedNode != null)
                {
                    selectedNode.Dispose();
                }
            }

            this.Database.SelectedItems.Clear();
        }

        void Copy(object sender, SimpleEventArgs<KeyDownMessage> e)
        {
            string output = Path.Combine(Path.GetTempPath(), "tmp_MoVE_copy.xml");
            ITransitional active = this.Database.Active;
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(output, settings))
            {
                writer.WriteStartDocument();

                string name = null;
                if (active is StateMachine)
                {
                    name = "StateMachine";
                }
                else if (active is Motiontree)
                {
                    name = "MotionTree";
                }
                else if (active is LogicLeafSM)
                {
                    name = "LogicLeafSM";
                }
                else if (active is LogicInlineSM)
                {
                    name = "LogicInlineSM";
                }

                if (name == null)
                {
                    writer.WriteStartElement("Dummy");
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    return;
                }

                writer.WriteStartElement(name);
                writer.WriteAttributeString("Id", active.Id.ToString());
                writer.WriteAttributeString("Cutting", cutting.ToString());

                double minimumX = double.MaxValue;
                double minimumY = double.MaxValue;
                foreach (ISelectable selectable in this.Database.SelectedItems)
                {
                    INode node = selectable as INode;
                    if (node == null)
                    {
                        continue;
                    }

                    minimumX = Math.Min(minimumX, node.Position.X);
                    minimumY = Math.Min(minimumY, node.Position.Y);
                }

                minimumX += 30.0;
                minimumY += 10.0;
                writer.WriteAttributeString("minimumX", minimumX.ToString());
                writer.WriteAttributeString("minimumY", minimumY.ToString());

                active.Copy(writer, this.Database.SelectedItems);
                writer.WriteEndElement();

                writer.WriteEndDocument();
            }
        }

        void Paste(object sender, SimpleEventArgs<KeyDownMessage> e)
        {
            string output = Path.Combine(Path.GetTempPath(), "tmp_MoVE_copy.xml");
            if (!File.Exists(output))
            {
                return;
            }

            ITransitional active = this.Database.Active;
            using (XmlReader reader = XmlReader.Create(output))
            {
                reader.MoveToContent();
                if (!ValidatePaste(active, reader.Name))
                {
                    return;
                }

                Guid id = Guid.Parse(reader.GetAttribute("Id"));
                bool isCutting = bool.Parse(reader.GetAttribute("Cutting"));

                bool updatePosition = id != active.Id;
                Point offset = new Point(5.0, 5.0);
                if (updatePosition)
                {
                    Point pastePosition = Mouse.GetPosition((this.Content as DiagramViewer).Diagram);
                    pastePosition = new Point(pastePosition.X + offset.X, pastePosition.Y + offset.Y);
                    double minimumX = double.Parse(reader.GetAttribute("minimumX"));
                    double minimumY = double.Parse(reader.GetAttribute("minimumY"));
                    offset = new Point(pastePosition.X - minimumX, pastePosition.Y - minimumY);
                }

                active.Paste(reader, offset, isCutting);
                List<INode> added = new List<INode>();
                foreach (INode node in this.GetAllNodesFromRoot(active))
                {
                    if (this.Database.Nodes.Contains(node))
                    {
                        continue;
                    }

                    added.Add(node);
                    this.Database.Nodes.Add(node);
                }

                if (!isCutting)
                {
                    foreach (INode node in added)
                    {
                        if (!(node is SignalBase))
                        {
                            this.Database.MakeNodeNameUnique(node);
                        }
                    }
                }
            }

            this.Database.SelectedItems.Clear();
        }

        private bool ValidatePaste(ITransitional pasteLocation, string copyType)
        {
            if (pasteLocation is Motiontree)
            {
                if (copyType != "MotionTree")
                {
                    MessageBox.Show("Unable to paste copied items here. The copied items are only supported in a state machine.");
                    return false;
                }
            }

            if (copyType == "MotionTree" && !(pasteLocation is Motiontree))
            {
                MessageBox.Show("Unable to paste copied items here. The copied items are only supported in a motion tree.");
                return false;
            }

            if (copyType == "LogicInlineSM" && !(pasteLocation is LogicInlineSM))
            {
                MessageBox.Show("Unable to paste copied items here. The copied items are only supported inside a inline state machine.");
                return false;
            }

            return true;
        }

        private IEnumerable<INode> GetAllNodesFromRoot(INode root)
        {
            if (root == null)
            {
                yield break;
            }

            yield return root;
            ITransitional transitional = root as ITransitional;
            if (transitional == null)
            {
                yield break;
            }

            foreach (INode child in transitional.Children)
            {
                foreach (INode node in this.GetAllNodesFromRoot(child))
                {
                    yield return node;
                }
            }
        }

        private void ResolvePaste(INode clone, bool append)
        {
            Database.InitFromNode(clone);
            if (append)
            {
                ITransitional parent = Database.Find(clone.Parent) as ITransitional;
                if (parent != null)
                    Database.AppendChild(parent as ITransitional, clone);
            }

            if (clone is LogicParent)
            {
                foreach (INode child in (clone as LogicParent).Children)
                {
                    if (child is Motiontree)
                        child.Parent = clone.Id;

                    ResolvePaste(child, false);
                }
            }
            else if (clone is Motiontree)
            {
                INode parentNode = this.Database.Find(clone.Parent);
                List<Input> removeList = new List<Input>();
                foreach (INode child in (clone as Motiontree).Children)
                {
                    Input inputChild = child as Input;
                    if (parentNode is LogicLeafSM && inputChild != null)
                    {
                        // Cannot have inputs when the motion tree is in a normal state machine
                        removeList.Add(inputChild);
                    }

                    if (child is Motiontree)
                    {
                        child.Parent = clone.Id;
                    }

                    ResolvePaste(child, false);
                }

                foreach (Input input in removeList)
                {
                    (clone as Motiontree).Remove(input);
                }
            }
            else if (clone is StateMachine)
            {
                if ((clone as StateMachine).Children != null)
                {
                    foreach (INode child in (clone as StateMachine).Children)
                    {
                        if (child is Motiontree)
                            child.Parent = clone.Id;

                        ResolvePaste(child, false);
                    }
                }
            }
        }

        void Delete(object sender, SimpleEventArgs<KeyDownMessage> e)
        {
            foreach (ISelectable selectedItem in Database.SelectedItems)
            {
                if (selectedItem is Output || selectedItem is Input)
                    continue;

                INode selectedNode = (INode)selectedItem;
                selectedNode.Dispose();
            }
        }

        private void CollectionAnchors(object node, object clone, ref Dictionary<Guid, IAnchor> anchors)
        {
            System.Type nodeType = node.GetType();
            System.Reflection.PropertyInfo[] nodeProperties = nodeType.GetProperties();

            System.Type cloneType = clone.GetType();
            System.Reflection.PropertyInfo[] cloneProperties = cloneType.GetProperties();

            for (int i = 0; i < nodeProperties.Length; i++)
            {
                if (i > cloneProperties.Length - 1)
                    break;

                System.Reflection.PropertyInfo nodeProp = nodeProperties[i];
                if (nodeProp == null)
                    continue;

                ParameterInfo[] parameters = nodeProp.GetIndexParameters();
                if (parameters.Length >= 1)
                    continue;

                System.Reflection.PropertyInfo cloneProp = null;
                foreach (PropertyInfo info in cloneProperties)
                {
                    if (info.Name == nodeProperties[i].Name)
                    {
                        cloneProp = info;
                        break;
                    }
                }

                if (cloneProp == null)
                    continue;

                object nodeValue = nodeProp.GetValue(node, null);
                object cloneValue = cloneProp.GetValue(clone, null);

                if (nodeValue is IAnchor)
                {
                    if (!anchors.ContainsKey((nodeValue as IAnchor).Id))
                        anchors.Add((nodeValue as IAnchor).Id, cloneValue as IAnchor);
                }
                else if (nodeValue is ILogicPropertyArray)
                {
                    List<IProperty> nodeChildren = new List<IProperty>();
                    List<IProperty> cloneChildren = new List<IProperty>();
                    foreach (var item in (nodeValue as ILogicPropertyArray).Children)
                    {
                        nodeChildren.Add(item);
                    }
                    foreach (var item in (cloneValue as ILogicPropertyArray).Children)
                    {
                        cloneChildren.Add(item);
                    }
                    for (int j = 0; j < nodeChildren.Count; j++)
                    {
                        CollectionAnchors(nodeChildren[j], cloneChildren[j], ref anchors);
                    }
                }
                else if (nodeValue is IProperty)
                {
                    CollectionAnchors(nodeValue, cloneValue, ref anchors);
                }
            }
        }
    }
}