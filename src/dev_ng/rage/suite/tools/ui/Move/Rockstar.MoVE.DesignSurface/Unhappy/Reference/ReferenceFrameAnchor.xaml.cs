﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;
using Rockstar.MoVE.Design;

namespace Rockstar.MoVE.Design
{
    public partial class ReferenceFrameAnchor : DiagramAnchor, IFrameAnchor
    {
        public ReferenceFrameAnchor(ReferenceSignal signal)
            : base(signal.Name, signal)
        {
            InitializeComponent();

            Anchor.Tag = null;
            Unloaded += new RoutedEventHandler(ReferenceFrameAnchor_Unloaded);
        }

        void ReferenceFrameAnchor_Unloaded(object sender, RoutedEventArgs e)
        {
            if (Anchor != null)
            {
                Anchor.Tag = null;
            }
        }

        public override Point? Receiver
        {
            get { return new Point?(Point.TranslatePoint(new Point(0, 0), GetDiagram(this))); }
        }
    }
}
