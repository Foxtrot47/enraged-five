﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

using Rockstar.MoVE.Design;

namespace Rockstar.MoVE.Design
{
    public class DefaultMarkerAdorner : Adorner
    {
        static class Const
        {
            public static double AnimationTime = 0.2;
        }

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(Point), typeof(DefaultMarkerAdorner));

        public DefaultMarkerAdorner(Diagram diagram, DefaultMarker item, Point position)
            : base(diagram)
        {
            _Diagram = diagram;
            _Item = item;
            Position = position;
            _Start = position;

            _Brush = Brushes.LightSlateGray.Clone();
            _Brush.Opacity = 0.5f;
        }

        Diagram _Diagram;
        DefaultMarker _Item;
        Point _Start;
        Brush _Brush;

        DiagramState _Hit { get; set; }

        Point Position
        {
            get { return (Point)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            base.OnPreviewMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!IsMouseCaptured)
                    CaptureMouse();

                Position = e.GetPosition(_Diagram);
                _Hit = HitTest(e.GetPosition(this));

                InvalidateVisual();

                e.Handled = true;
            }
            else
            {
                if (IsMouseCaptured)
                    ReleaseMouseCapture();
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            if (_Hit != null)
            {
                IAutomaton parent = _Hit.Source.Database.Find(_Hit.Source.Parent) as IAutomaton;
                parent.Default = _Hit.Source as ITransitional;

                AdornerLayer layer = AdornerLayer.GetAdornerLayer(_Diagram);
                if (layer != null)
                    layer.Remove(this);
            }
            else
            {
                PointAnimation anim = new PointAnimation();
                anim.From = e.GetPosition(_Diagram);
                anim.To = _Start;
                anim.Duration = new Duration(TimeSpan.FromSeconds(Const.AnimationTime));
                anim.CurrentTimeInvalidated += new EventHandler(PointAnimation_CurrentTimeInvalidated);
                anim.Completed += new EventHandler(PointAnimation_Completed);

                BeginAnimation(DefaultMarkerAdorner.PositionProperty, anim);
            }

            if (IsMouseCaptured)
                ReleaseMouseCapture();
        }

        void PointAnimation_CurrentTimeInvalidated(object sender, EventArgs e)
        {
            InvalidateVisual();
        }

        void PointAnimation_Completed(object sender, EventArgs e)
        {
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(_Diagram);
            if (layer != null)
                layer.Remove(this);
        }

        protected override void OnRender(DrawingContext context)
        {
            base.OnRender(context);

            Geometry geometry = Geometry.Parse("M4,5 9,5 9,3 4,3 M9,0 17,4 9,8 Z").Clone();
            geometry.Transform = new TranslateTransform(Position.X, Position.Y - 4);

            context.DrawEllipse(_Brush, null, Position, 4, 4);
            context.DrawGeometry(_Brush, null, geometry);
        }

        DiagramState HitTest(Point point)
        {
            DependencyObject hitobj = _Diagram.InputHitTest(point) as DependencyObject;
            while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != _Item)
            {
                if (hitobj is DiagramState)
                    return hitobj as DiagramState;

                hitobj = VisualTreeHelper.GetParent(hitobj);
            }

            return null;
        }
    }

    public class DefaultMarker : UserControl
    {
        public DefaultMarker()
        {
        }

        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseLeftButtonDown(e);
            if (!IsMouseCaptured)
                CaptureMouse();

            e.Handled = true;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && IsMouseCaptured)
            {
                try
                {
                    Diagram diagram = GetDiagram(this);

                    ReleaseMouseCapture();

                    DefaultMarkerAdorner adorner = new DefaultMarkerAdorner(diagram, this, e.GetPosition(diagram));

                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        layer.Add(adorner);
                    }

                    e.Handled = true;
                }
                catch
                {
                }
            }

            base.OnMouseMove(e);
        }

        Diagram GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }
    }

    public partial class DiagramState : DiagramNode, IDiagramState
    {
        public static readonly DependencyProperty IsDefaultProperty =
            DependencyProperty.Register("IsDefault", typeof(bool), typeof(DiagramState));

        public ImageSource Icon
        {
            get
            {
                Assembly iconAssembly = Assembly.GetAssembly(typeof(Rockstar.MoVE.Framework.DescriptorGroup));

                BitmapImage bitmapImage = new BitmapImage();
                string iconName = "";

                switch (Type)
                {
                    case "Motiontree": { iconName = "MotionTree"; } break;
                    case "StateMachine": { iconName = "StateMachine"; } break;
//                     default: { Console.WriteLine("***** Type not found: " + Type + " *****"); } break;
                }

                try
                {
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = iconAssembly.GetManifestResourceStream("Rockstar.MoVE.Framework.Media." + iconName + ".png");
                    bitmapImage.EndInit();

                    return bitmapImage;
                }
                catch
                {
                    return null;                	
                }
            }
        }

        public DiagramState(ITransitional source)
            : base(source)
        {
            InitializeComponent();

            source.DefaultChanged += new EventHandler<DefaultChangedEventArgs>(Source_DefaultChanged);

            Loaded += new RoutedEventHandler(DiagramSource_Loaded);
        }

        void DiagramSource_Loaded(object sender, RoutedEventArgs e)
        {
            OnPositionChanged();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            IAutomaton parent =  Source.Database.Find(Source.Parent) as IAutomaton;
            IsDefault = (parent != null && parent.Default == Source);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Rectangle RectanglePart = (Rectangle)GetTemplateChild("PART_Rectangle");
            if (RectanglePart != null)
            {
                RectanglePart.MouseLeftButtonDown += new MouseButtonEventHandler(RectanglePart_MouseLeftButtonDown);
            }
        }

        DiagramViewer _Diagram = null;
        Point? _Start = null;

        public bool IsDefault 
        {
            get { return (bool)GetValue(IsDefaultProperty); }
            set { SetValue(IsDefaultProperty, value); }
        }
      
        void Source_DefaultChanged(object sender, DefaultChangedEventArgs e)
        {
            IsDefault = e.IsDefault;
            InvalidateMeasure();
        }

        protected override void OnPreviewMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDoubleClick(e);

            if (e.ChangedButton == MouseButton.Left)
            {
                DiagramViewer viewer = GetDiagram(this);
                Diagram diagram = viewer.Diagram;

                Database database = diagram.Database;
                database.Active = Source as ITransitional;

                e.Handled = true;
            }
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete node";
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);
            contextMenu.Items.Add(deleteItem);

            MenuItem defaultState = new MenuItem();
            defaultState.Header = "Set as default state";
            defaultState.Click += new RoutedEventHandler(DefaultState_Click);
            contextMenu.Items.Add(defaultState);

            MenuItem replaceNode = new MenuItem();
            replaceNode.Header = "Replace node...";
            replaceNode.Click += new RoutedEventHandler(OnReplaceNode);
            contextMenu.Items.Add(replaceNode);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu context = (ContextMenu)sender;
            context.Items.Clear();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }

        void DefaultState_Click(object sender, RoutedEventArgs e)
        {
            IAutomaton parent = Source.Database.Find(Source.Parent) as IAutomaton;
            parent.Default = Source as ITransitional;
        }

        void OnReplaceNode(object sender, RoutedEventArgs e)
        {
            IAutomaton parentAutomaton = (IAutomaton)Source.Database.Find(Source.Parent);
            ITransitional parentTransitional = (ITransitional)Source.Database.Find(Source.Parent);
            ITransitional nodeToBeReplaced = (ITransitional)Source;

            // First we need to determine if the selected node is tied into the state machine
            if (parentAutomaton.Transitions.Where(t => (t.Source == nodeToBeReplaced || t.Dest == nodeToBeReplaced)).Count() == 0)
            {
                MessageBox.Show(
                    string.Format("The node '{0}' cannot be replaced as it doesn't take part in any of the transitions in this state machine.", 
                    "Node unsuitable for replacement"));
                return;
            }

            // Determine the nodes that could possibly replace the selected node
            var potentialReplacements = new List<ITransitional>();
            foreach (var childNode in parentTransitional.Children)
            {
                if (childNode != nodeToBeReplaced &&
                    parentAutomaton.Transitions.Where(t => (t.Source == childNode || t.Dest == childNode)).Count() == 0)
                {
                    potentialReplacements.Add((ITransitional)childNode);
                }
            }

            if( potentialReplacements.Count == 0 )
            {
                MessageBox.Show(
                    string.Format("The node '{0}' cannot be replaced as there are no nodes that can take its place.  Replacement nodes must not be involved in any of the transitions in this state machine.",
                    "No nodes suitable for replacement"));
                return;
            }

            var replaceNodeWindow = new ReplaceNodeWindow(nodeToBeReplaced, potentialReplacements);
            if (replaceNodeWindow.ShowDialog() == true)
            {
                ITransitional replacementNode = replaceNodeWindow.ReplacementSelected;

                // Transfer transition order collection
                foreach (var transitionOrder in nodeToBeReplaced.TransitionOrder)
                    replacementNode.TransitionOrder.Add(transitionOrder);
                nodeToBeReplaced.TransitionOrder.Clear();

                // Update the transitions themselves
                foreach (var transition in parentAutomaton.Transitions)
                {
                    if (transition.Source == nodeToBeReplaced)
                        transition.Source = replacementNode;
                    else if (transition.Dest == nodeToBeReplaced)
                        transition.Dest = replacementNode;
                }
            }
        }

        void RectanglePart_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            MouseButtonEventArgs args = e as MouseButtonEventArgs;

            _Diagram = GetDiagram(this);
            if (_Diagram != null)
            {
                _Start = new Point?(args.GetPosition(_Diagram));
                e.Handled = true;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                _Start = null;

            if (_Start.HasValue)
            {
                DiagramTransitionAdorner adorner = new DiagramTransitionAdorner(_Diagram, this, _Start.Value);
                if (adorner != null)
                {
                    AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(_Diagram);
                    if (adornerLayer != null)
                    {
                        adornerLayer.Add(adorner);
                        e.Handled = true;
                    }
                }
            }

            base.OnMouseMove(e);
        }

        Size OffsetDesiredSize
        {
            get
            {
                return DesiredSize;
            }
        }

        public Point Center
        {
            get 
            {
                return new Point(RelativePosition.X + (OffsetDesiredSize.Width / 2), RelativePosition.Y + (OffsetDesiredSize.Height / 2)); 
            }
        }

        public Point TopCenter
        {
            get 
            {
                return new Point(RelativePosition.X + (OffsetDesiredSize.Width / 2), RelativePosition.Y); 

            }
        }

        public Point TopRight
        {
            get 
            {
                return new Point(RelativePosition.X + OffsetDesiredSize.Width, RelativePosition.Y); 
            }
        }

        public Point TopLeft
        {
            get 
            {
                Grid PART_Grid = (Grid)GetTemplateChild("PART_Grid");
                if (PART_Grid != null)
                {
                    return TranslatePoint(new Point(PART_Grid.ColumnDefinitions[0].ActualWidth,
                            PART_Grid.RowDefinitions[0].ActualHeight), GetCanvas(this));
                }

                return new Point(0, 0);
            }
        }

        public Point BottomCenter
        {
            get 
            {
                return new Point(RelativePosition.X + (OffsetDesiredSize.Width / 2), RelativePosition.Y + OffsetDesiredSize.Height); 
            }
        }

        public Point BottomRight
        {
            get
            {
                Point offset = TranslatePoint(new Point(0,0), GetCanvas(this));
                return new Point(offset.X + DesiredSize.Width, offset.Y + DesiredSize.Height);
            }
        }

        public Point LeftCenter
        {
            get 
            {
                return new Point(RelativePosition.X, RelativePosition.Y + (OffsetDesiredSize.Height / 2)); 
            }
        }

        public Point RightCenter
        {
            get 
            {
                return new Point(RelativePosition.X + OffsetDesiredSize.Width, RelativePosition.Y + (OffsetDesiredSize.Height / 2)); 
            }
        }

        Point RelativePosition
        {
            get
            {
                Grid PART_Grid = (Grid)GetTemplateChild("PART_Grid");
                if (PART_Grid != null)
                {
                    return TranslatePoint(new Point(PART_Grid.ColumnDefinitions[0].ActualWidth / 2,
                            PART_Grid.RowDefinitions[0].ActualHeight / 2), GetCanvas(this));
                }
                else
                {
                    return new Point(0, 0);
                }
            }
        }

        public Size OffsetSize
        {
            get
            {
                Grid PART_Grid = (Grid)GetTemplateChild("PART_Grid");
                if (PART_Grid != null)
                {
                    return new Size(PART_Grid.ColumnDefinitions[0].ActualWidth / 2,
                        PART_Grid.RowDefinitions[0].ActualHeight / 2);
                }
                else
                {
                    return new Size(0, 0);
                }
            }
        }
    }
}
