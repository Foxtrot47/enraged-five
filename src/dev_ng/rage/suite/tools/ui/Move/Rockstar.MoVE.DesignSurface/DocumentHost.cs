﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AvalonDock;

using RSG.Base.Logging;

// shouldn't have to rely on components to use this...
using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Services;

using New.Move.Core;
using RSG.Base.Configuration;
using Rockstar.MoVE.Framework.DataModel.Prototypes;

namespace Rockstar.MoVE.Design
{
    [Export("DocumentHost", typeof(DocumentPane))]
    public class DocumentHost : DocumentPane
    {
        public DocumentHost()
        {
        }

        [Import(typeof(IApplicationService))]
        IApplicationService _ApplicationService = null;

        [Import(typeof(IFileService))]
        IFileService _FileService = null;

        [Import(typeof(IDataModelService))]
        IDataModelService _DataModelService = null;

        [Import(typeof(ISelectionService))]
        ISelectionService _SelectionService = null;

        [Import("DataModel", typeof(IDataModel))]
        IDataModel _DataModel = null;

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            _ApplicationService.NewCommandExecuted += new NewCommandExecutedHandler(ApplicationService_NewCommandExecuted);
            _ApplicationService.SaveCommandExecuted += new SaveCommandExecutedHandler(ApplicationService_SaveCommandExecuted);
            _ApplicationService.SaveAsCommandExecuted += new SaveAsCommandExecutedHandler(ApplicationService_SaveAsCommandExecuted);
            _ApplicationService.SaveSelectionCommandExecuted += new SaveSelectionCommandExecutedHandler(ApplicationService_SaveSelectionCommandExecuted);
            _ApplicationService.OpenCommandExecuted += new OpenCommandExecutedHandler(ApplicationService_OpenCommandExecuted);
            _ApplicationService.CloseCommandExecuted += new CloseCommandExecutedHandler(ApplicationService_CloseCommandExecuted);
            _ApplicationService.ExportCommandExecuted += new ExportCommandExecutedHandler(ApplicationService_ExportCommandExecuted);
            _ApplicationService.SaveDirtyDocumentsExecuted += new SaveDirtyDocumentsHandler(ApplicationService_SaveDirtyDocuments);
            _ApplicationService.SaveDirtyClipsExecuted += new SaveDirtyClipsHandler(ApplicationService_SaveDirtyClips);
            _ApplicationService.FindDirtyDocumentsExecuted += new FindDirtyDocumentsHandler(ApplicationService_FindDirtyDocuments);
            _ApplicationService.FindDirtyClipsExecuted += new FindDirtyClipsHandler(ApplicationService_FindDirtyClips);
            _ApplicationService.RebuildAllCommandExecuted += new RebuildAllCommandExecutedHandler(ApplicationService_RebuildAllCommandExecuted);
            _ApplicationService.OpenSaveAllCommandExecuted += OpenSaveAllCommandExecuted;
            _ApplicationService.ExportAllCommandExecuted += ExportAllCommandExecuted;
        }

        private void ExportAllCommandExecuted(bool buildRpf)
        {
            IConfig config = ConfigFactory.CreateConfig();
            string directory = Path.Combine(config.Project.DefaultBranch.Assets, "anim", "move", "networks");
            if (!Directory.Exists(directory))
            {
                return;
            }

            string[] filenames = Directory.GetFiles(directory, "*.mxtf", SearchOption.AllDirectories);
            Dictionary<string, string> exportPairs = new Dictionary<string, string>();
            string nodeMapDirectory = Path.Combine(config.Project.DefaultBranch.Build, "common", "non_final", "anim", "move", "nodemaps");
            string exportDirectory = Path.Combine(config.Project.DefaultBranch.Export, "anim", "move_networks");
            foreach (string filename in filenames)
            {
                string name = Path.GetFileNameWithoutExtension(filename);
                string nodeMap = Path.Combine(nodeMapDirectory, name + "_nodemap.xml");
                string exportPathname = Path.Combine(exportDirectory, name + ".imvf");
                exportPairs.Add(exportPathname, nodeMap);
            }

            int i = 0;
            List<Tuple<DatabaseDocument, string, string>> data = new List<Tuple<DatabaseDocument, string, string>>();
            List<DatabaseDocument> errors = new List<DatabaseDocument>();
            foreach (KeyValuePair<string, string> exportPair in exportPairs)
            {
                DatabaseDocument doc = new DatabaseDocument(this._DataModel, this._ApplicationService, this._DataModelService, filenames[i++]);
                doc.Database.MakeNodeNamesUnique();
                DatabaseValidator validator = new DatabaseValidator(doc.Database);
                LogFactory.ApplicationLog.Message("Validating network...");
                validator.Validate();
                if (validator.HasErrors)
                {
                    LogFactory.ApplicationLog.Message("Network is invalid.  Export aborted.");
                    string msg = "The current network is invalid and could not be exported. The target file has not been overwritten. Check the log for more details";
                    MessageBox.Show(msg, "Network Validation Failed");
                    continue;
                }
                else if (validator.HasWarnings)
                {
                    LogFactory.ApplicationLog.Message(
                        "Network is valid, but some issues were detected.  See warnings above.");
                }
                else
                {
                    LogFactory.ApplicationLog.Message("Network is valid.");
                }

                data.Add(new Tuple<DatabaseDocument, string, string>(doc, exportPair.Key, exportPair.Value));
            }

            if (errors.Count > 0)
            {
                string msg = string.Format("Unable to export {0} of the networks due to errors that can be found in the log. Do you wish to continue and export just the valid networks?", errors.Count);
                MessageBoxResult result = MessageBox.Show(msg, "Network Errors", MessageBoxButton.YesNo, MessageBoxImage.Error);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }

            string description = "Automatic MoVE changelist for batch export\n";
            string[] scFilenames = new string[]
            { 
                Path.Combine(nodeMapDirectory, "*_nodemap.xml"),
                Path.Combine(exportDirectory, "*.imvf")
            };

            DatabaseDocument.PerforceCheckout(scFilenames, description);
            foreach (Tuple<DatabaseDocument, string, string> exportData in data)
            {
                exportData.Item1.Export(exportData.Item2, exportData.Item3, false);
            }

            if (buildRpf)
            {
                string rbFilename = Path.Combine(config.ToolsRoot, "ironlib", "util", "data_convert_file.rb");
                string irExe = Path.Combine(config.ToolsBin, "ironruby", "bin", "ir.exe");
                string prompt = Path.Combine(config.ToolsRoot, "ironlib", "prompt.bat");

                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.FileName = prompt;
                startInfo.Arguments = string.Format("{0} {1} {2}", irExe, rbFilename, exportDirectory);
                startInfo.UseShellExecute = false;
                System.Diagnostics.Process buildProcess = System.Diagnostics.Process.Start(startInfo);
                buildProcess.WaitForExit();
            }
        }

        private void OpenSaveAllCommandExecuted()
        {
            string description = "Automatic MoVE changelist for batch save\n";

            IConfig config = ConfigFactory.CreateConfig();
            string directory = Path.Combine(config.Project.DefaultBranch.Assets, "anim", "move", "networks");
            if (!Directory.Exists(directory))
            {
                return;
            }
            
            List<string> filenames = new List<string>(Directory.GetFiles(directory, "*.mxtf", SearchOption.AllDirectories));
            DatabaseDocument.PerforceCheckout(Enumerable.Repeat(Path.Combine(directory, "*.mxtf"), 1), description);
            foreach (string filename in filenames)
            {
                DatabaseDocument doc = new DatabaseDocument(this._DataModel, this._ApplicationService, this._DataModelService, filename);
                doc.Save();
            }
        }

        void ApplicationService_SaveSelectionCommandExecuted(string filename)
        {
            IPersistentContent content = SelectedItem as IPersistentContent;
            if (content != null)
            {
                try
                {
                    content.SaveSelection(filename);
                }
                catch (SaveFailedException e)
                {
                    MessageBox.Show(e.Message, "Save Selection Failed", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        void ApplicationService_NewCommandExecuted()
        {
            NewDocumentDialog dialog = new NewDocumentDialog();
            ITransitionalDesc desc = dialog.DoModal();

            if (desc != null)
            {
                // LPXO: Tabs dont work.  Dont allow multiple tabs to be open until it is fixed.
                for (int i = 0; i < Items.Count; i++)
                    (Items.GetItemAt(i) as DatabaseDocument).Close();

                if (Items.Count != 0)
                {
                    return;
                }

                DatabaseDocument document = new DatabaseDocument(_DataModel, _ApplicationService, _DataModelService, desc);
                document.InjectService(_SelectionService);
                Items.Add(document);
                SelectedItem = document;
                document.FocusViewer();
            }
        }

        void ApplicationService_SaveCommandExecuted()
        {
            IPersistentContent content = SelectedItem as IPersistentContent;
            if (content != null)
            {
                try
                {
                    content.Save();
                }
                catch (SaveFailedException e)
                {
                    MessageBox.Show(e.Message, "Save Failed", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        void ApplicationService_SaveAsCommandExecuted(string filename)
        {
            IPersistentContent content = SelectedItem as IPersistentContent;
            if (content != null)
            {
                try
                {
                    content.Save(filename);
                }
                catch (SaveFailedException e)
                {
                    MessageBox.Show(e.Message, "Save As Failed", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        Database ApplicationService_OpenCommandExecuted(string filename)
        {
            try
            {
                // LPXO: Tabs dont work.  Dont allow multiple tabs to be open until it is fixed.
                for (int i = 0; i < Items.Count; i++)
                    (Items.GetItemAt(i) as DatabaseDocument).Close();


                DatabaseDocument document = new DatabaseDocument(_DataModel, _ApplicationService, _DataModelService, filename);
                document.InjectService(_SelectionService);
                Items.Add(document);
                SelectedItem = document;

                // Check for known integrity issues caused by former bugs
                var dbIntegrityChecker = new DatabaseIntegrityChecker();
                dbIntegrityChecker.CheckDatabase(document.Database);

                if (dbIntegrityChecker.FoundIssues)
                {
                    foreach (string issueDescription in dbIntegrityChecker.IssueDescriptions)
                    {
                        LogFactory.ApplicationLog.Warning(issueDescription);
                    }

                    MessageBox.Show(
                        "The file was loaded successfully, but there are some issues with the integrity of the network.  Check the log for more details.  "
                        + "To fix these problems, select the Check Database option from the Tools menu.", 
                        "Network Integrity Warnings");
                }

                return document.Database;
            }
            catch (LoadFailedException e)
            {
                MessageBox.Show(e.Message, "Load Failed", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return null;
            }
        }

        void ApplicationService_CloseCommandExecuted()
        {
            //  There should only be one document open while tabs aren't working, 
            //  but go through the list anyway, just in case
            for (int i = 0; i < Items.Count; i++)
            {
                (Items.GetItemAt(i) as DatabaseDocument).Close();
            }
        }

        void ApplicationService_RebuildAllCommandExecuted(string[] filenames, bool build)
        {
            IConfig config = ConfigFactory.CreateConfig();
            for (int i = 0; i < filenames.Length - 1; i++)
            {
                DatabaseDocument doc = new DatabaseDocument(this._DataModel, this._ApplicationService, this._DataModelService, filenames[i]);
                string name = Path.GetFileNameWithoutExtension(filenames[i]);
                filenames[i] = Path.Combine(config.Project.DefaultBranch.Export, "anim", "move_networks", name + ".imvf");
                doc.Export(filenames[i], null, false, false, false);
            }

            for (int i = filenames.Length - 1; i < filenames.Length; i++)
            {
                DatabaseDocument doc = new DatabaseDocument(this._DataModel, this._ApplicationService, this._DataModelService, filenames[i]);
                string name = Path.GetFileNameWithoutExtension(filenames[i]);
                filenames[i] = Path.Combine(config.Project.DefaultBranch.Export, "anim", "move_networks", name + ".imvf");
                doc.Export(filenames[i], null, build, false, false);
            }
        }

        void ApplicationService_ExportCommandExecuted(string filename, string nodemapPath, bool build, bool prototype, bool preview)
        {
            DatabaseDocument doc = SelectedItem as DatabaseDocument;
            if (doc != null)
            {
                doc.Export(filename, nodemapPath, build, prototype, preview);
            }
        }

        void ApplicationService_SaveDirtyDocuments()
        {
            foreach (IPersistentContent content in Items)
            {
                if (content.IsDirty)
                {
                    content.Save();
                }
            }
        }

        List<string> ApplicationService_FindDirtyDocuments()
        {
            List<string> docs = new List<string>();
            foreach (IPersistentContent content in Items)
            {
                if (content.IsDirty)
                {
                    docs.Add(Path.GetFileName(content.FullyQualifiedFilename));
                }
            }

            return docs;
        }

        void ApplicationService_SaveDirtyClips()
        {
            DatabaseDocument doc = SelectedItem as DatabaseDocument;
            if (doc != null && doc.Database != null)
            {
                Database database = doc.Database;
                foreach (INode node in database.Nodes)
                {
                    ClipPrototype clip = node as ClipPrototype;
                    if (clip == null || clip.ClipViewModel == null)
                    {
                        continue;
                    }

                    if (clip.Clip.Source == New.Move.Core.ClipSource.LocalFile)
                    {
                        if (clip.ClipViewModel.DirtyState)
                        {
                            clip.ClipViewModel.SaveClip();
                        }
                    }
                }
            }
        }

        List<string> ApplicationService_FindDirtyClips()
        {
            List<string> clips = new List<string>();
            DatabaseDocument doc = SelectedItem as DatabaseDocument;
            if (doc != null && doc.Database != null)
            {
                Database database = doc.Database;
                foreach (INode node in database.Nodes)
                {
                    ClipPrototype clip = node as ClipPrototype;
                    if (clip == null || clip.ClipViewModel == null)
                    {
                        continue;
                    }

                    if (clip.Clip.Source == New.Move.Core.ClipSource.LocalFile)
                    {
                        if (clip.ClipViewModel.DirtyState)
                        {
                            clips.Add(Path.GetFileName(clip.Clip.Filename));
                        }
                    }
                }
            }

            return clips;
        }
    }
}
