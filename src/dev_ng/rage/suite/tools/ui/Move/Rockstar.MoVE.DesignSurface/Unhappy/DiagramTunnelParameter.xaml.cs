﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

using Rockstar.MoVE.Design;

namespace Rockstar.MoVE.Design
{
    public partial class DiagramTunnelParameter : DiagramNode, IDiagramAnchor
    {
        public static readonly DependencyProperty ParameterProperty =
            DependencyProperty.Register("Parameter", typeof(Parameter), typeof(DiagramTunnelParameter));

        public static readonly DependencyProperty HighlightEnabledProperty =
            DependencyProperty.Register("HighlightEnabled", typeof(bool), typeof(DiagramTunnelParameter));

        internal Parameter Parameter
        {
            get { return (Parameter)GetValue(ParameterProperty); }
            set { SetValue(ParameterProperty, value); }
        }

        public bool HighlightEnabled
        {
            get { return (bool)GetValue(HighlightEnabledProperty); }
            set { SetValue(HighlightEnabledProperty, value); }
        }

        public DiagramTunnelParameter(TunnelParameter source)
            : base(source)
        {
            InitializeComponent();
            Anchor = source.ParameterAnchor;
            Anchor.Tag = this;

            Database db = source.Database;

            Parameters.ItemsSource = db.Signals;
            Parameters.DisplayMemberPath = "Name";

            Parameters.SetBinding(ComboBox.SelectedItemProperty,
                new Binding("Parameter") { Source = source });

            Container = this;

            Unloaded += new RoutedEventHandler(DiagramTunnelParameter_Unloaded);
        }

        void DiagramTunnelParameter_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(Parameters, ComboBox.SelectedItemProperty);
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete";
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);

            contextMenu.Items.Add(deleteItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;
            contextMenu.Items.Clear();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }

        protected Diagram GetDiagramCanvas(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }

        void Grapple_MouseDown(object sender, RoutedEventArgs e)
        {
            Diagram diagram = GetDiagramCanvas(this);
            if (diagram != null)
            {
            }
        }

        void Parameters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (Signal parameter in e.AddedItems)
            {
                Parameter = parameter.GetParameter();
            }
        }

        public IDiagramAnchor HitTest(Point point)
        {
            return null;
        }

        public DiagramNode Container { get; set; }
        public IAnchor Anchor { get; private set; }

        public Point? Sender { get { return null; } }
        public Point? Receiver
        {
            get
            {
                return new Point?(PART_Source.TranslatePoint(new Point(0, 0), GetDiagramCanvas(this)));
            }
        }
    }
}
