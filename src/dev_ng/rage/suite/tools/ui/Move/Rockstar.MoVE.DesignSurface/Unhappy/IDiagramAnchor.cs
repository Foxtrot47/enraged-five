﻿using System.Windows;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public interface IDiagramAnchor
    {
        IDiagramAnchor HitTest(Point point);
        IAnchor Anchor { get; }

        Point? Sender { get; }
        Point? Receiver { get; }

        DiagramNode Container { get; set; }

        event RoutedEventHandler Loaded;

        bool HighlightEnabled { get; set; }
    }
}