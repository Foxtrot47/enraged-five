﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Collections;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public interface IDiagramElement
    {
        System.Windows.Point Position { get; set; }
        System.Windows.Size DesiredSize { get; }
    }

    [Serializable]
    public abstract class DiagramNode : UserControl, IDiagramElement, ICloneable
    {
        protected DiagramNode(INode source)
        {
            DataContext = this;

            Tag = source;
            source.Tag = this;

            Binding nameBinding = new Binding("Name");
            nameBinding.Source = source;
            nameBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            SetBinding(DiagramNode.LabelProperty, nameBinding);

            Binding additionalHeaderTextBinding = new Binding("AdditionalText");
            additionalHeaderTextBinding.Source = source;
            additionalHeaderTextBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            SetBinding(DiagramNode.AdditionalHeaderTextProperty, additionalHeaderTextBinding);

            Binding typeBinding = new Binding("Type");
            typeBinding.Source = source;
            SetBinding(DiagramNode.TypeProperty, typeBinding);

            Binding enabledBinding = new Binding("Enabled");
            enabledBinding.Source = source;
            enabledBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            SetBinding(DiagramNode.EnabledProperty, enabledBinding);

            Loaded += new RoutedEventHandler(DiagramNode_Loaded);
            Unloaded += new RoutedEventHandler(DiagramNode_Unloaded);
        }

        public INode Source 
        { 
            get 
            { 
                return Tag as INode; 
            }
            protected set
            {
                Tag = value;
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            Source.IsSelectedChanged +=
                new EventHandler<IsSelectedChangedEventArgs>(Source_IsSelectedChanged);

            Source.IsEngagedChanged +=
                new EventHandler<IsEngagedChangedEventArgs>(Source_IsEngagedChanged);
        }

        void DiagramNode_Loaded(object sender, RoutedEventArgs e)
        {
        }

        void DiagramNode_Unloaded(object sender, RoutedEventArgs e)
        {
            if (Source != null)
            {
                Source.IsSelectedChanged -=
                    new EventHandler<IsSelectedChangedEventArgs>(Source_IsSelectedChanged);

                Source.IsEngagedChanged -=
                    new EventHandler<IsEngagedChangedEventArgs>(Source_IsEngagedChanged);

                // LPXO: This was being called prematurely.  In some situations, diagram nodes were being unloaded when files were opening
                // resulting in an exception.
                //Source.Tag = null;
            }

            BindingOperations.ClearBinding(this, DiagramNode.LabelProperty);
        }
        
        void Source_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            switch (e.Action)
            {
                case SelectionChangedAction.Add:
                    SetValue(SelectionBehaviour.IsSelectedProperty, true);
                    break;
                case SelectionChangedAction.Remove:
                    SetValue(SelectionBehaviour.IsSelectedProperty, false);
                    break;
            }
        }
        
        void Source_IsEngagedChanged(object sender, IsEngagedChangedEventArgs e)
        {
            Engaged = e.Action == EngagedChangedAction.Add;
        }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(DiagramNode));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public static readonly DependencyProperty AdditionalHeaderTextProperty =
            DependencyProperty.Register("AdditionalHeaderText", typeof(string), typeof(DiagramNode));

        public string AdditionalHeaderText
        {
            get { return (string)GetValue(AdditionalHeaderTextProperty); }
            set { SetValue(AdditionalHeaderTextProperty, value); }
        }

        public static readonly DependencyProperty TypeProperty =
            DependencyProperty.Register("Type", typeof(string), typeof(DiagramNode));

        public string Type
        {
            get { return (string)GetValue(TypeProperty); }
        }

        public static readonly DependencyProperty EnabledProperty =
            DependencyProperty.Register("Enabled", typeof(bool), typeof(DiagramNode));

        public bool Enabled
        {
            get { return (bool)GetValue(EnabledProperty); }
            set { SetValue(EnabledProperty, value); }
        }

        public static readonly DependencyProperty EngagedProperty =
          DependencyProperty.Register("Engaged", typeof(bool), typeof(DiagramNode), new FrameworkPropertyMetadata(false));

        public bool Engaged
        {
            get { return (bool)GetValue(EngagedProperty); }
            set 
            {
                Action a = () => SetValue(EngagedProperty, value);

                if(this.Dispatcher.CheckAccess()) {
                    a();
                }
                else {
                    this.Dispatcher.Invoke(a);
                }
            }
        }

        public Point Position
        {
            get
            {
                return Source.Position;
            }
            set
            {
                if (Source.Position != value)
                {
                    Source.Position = value;
                    OnPositionChanged();
                }
            }
        }

        public Point Centre
        {
            get { return new Point(Position.X + (ActualWidth * 0.5), Position.Y + (ActualHeight * 0.5)); }
        }

        public double Top
        {
            get
            {
                return Source.Position.Y;
            }
            set
            {
                if (Source.Position.Y != value)
                {
                    Source.Position = new Point(Source.Position.X, value);
                    OnPositionChanged();
                }
            }
        }

        public double Left
        {
            get
            {
                return Source.Position.X;
            }
            set
            {
                if (Source.Position.X != value)
                {
                    Source.Position = new Point(value, Source.Position.Y);
                    OnPositionChanged();
                }
            }
        }

        public Guid Id
        {
            get
            {
                return Source.Id;
            }
        }

        public bool IsSelected
        {
            get
            {
                return Source.Database.SelectedItems.Contains(Source);
            }
        }

        public event RoutedEventHandler PositionChanged;
        protected void OnPositionChanged()
        {
            if (PositionChanged != null)
                PositionChanged(this, new RoutedEventArgs());
        }

        protected Diagram GetCanvas(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }

        protected DiagramViewer GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is DiagramViewer))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as DiagramViewer;
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public virtual DiagramNode Clone()
        {
            return (DiagramNode)this.MemberwiseClone();
        }
    }
}