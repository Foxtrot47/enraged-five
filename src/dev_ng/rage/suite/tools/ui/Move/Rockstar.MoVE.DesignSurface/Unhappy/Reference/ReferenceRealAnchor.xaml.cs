﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public partial class ReferenceRealAnchor : DiagramAnchor, IRealAnchor
    {

        public static readonly DependencyProperty InputProperty =
            DependencyProperty.Register("Input", typeof(PropertyInput), typeof(ReferenceRealAnchor));

        public static readonly DependencyProperty ResultEnabledProperty =
            DependencyProperty.Register("ResultEnabled", typeof(bool), typeof(ReferenceRealAnchor));

        internal PropertyInput Input
        {
            get { return (PropertyInput)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        internal bool ResultEnabled
        {
            get { return (bool)GetValue(ResultEnabledProperty); }
            set { SetValue(ResultEnabledProperty, value); }
        }

        public ReferenceRealAnchor(ReferenceSignal signal)
            : base(signal.Name, signal)
        {
            InitializeComponent();

            Anchor.Tag = this;
            Unloaded += new RoutedEventHandler(ReferenceRealAnchor_Unloaded);
        }

        void ReferenceRealAnchor_Unloaded(object sender, RoutedEventArgs e)
        {
            if (Anchor != null)
            {
                Anchor.Tag = null;
            }
        }

        public override Point? Receiver
        {
            get { return new Point?(SourcePoint.TranslatePoint(new Point(0, 0), GetDiagram(this))); }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            if ((Anchor as ReferenceSignal).AssocProperty != null)
            {
                SetBinding(ReferenceRealAnchor.InputProperty,
                    new Binding("Input") { Source = (Anchor as ReferenceSignal).AssocProperty });

                SetBinding(ReferenceRealAnchor.ResultEnabledProperty,
                    new Binding("OutputEnabled") { Source = (Anchor as ReferenceSignal).AssocProperty });
            }
        }
    }
}
