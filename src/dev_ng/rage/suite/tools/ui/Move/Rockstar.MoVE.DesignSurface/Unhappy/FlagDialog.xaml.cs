﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rage.Move;
using New.Move.Core;

using AvalonDock;

namespace Rockstar.MoVE.Design
{
    public partial class FlagDialog : Window
    {
        public FlagDialog()
        {
            InitializeComponent();
        }

        Database Database
        {
            get
            {
                //DocumentContent doc = App.Window.DocumentHost.SelectedItem as DocumentContent;
                //return doc.Tag as Database;
                return null;
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            Flags.ItemsSource = Database.Flags;
            Flags.DisplayMemberPath = "Name";
        }

        public void DoModal()
        {
            ShowDialog();
        }

        void AddButton_Click(object sender, RoutedEventArgs e)
        {
            FlagEditDialog dialog = new FlagEditDialog();
            dialog.DoModal();
        }

        void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            Flag selected = Flags.SelectedItem as Flag;
            Database.Flags.Remove(selected);
        }

        void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
