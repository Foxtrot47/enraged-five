﻿using System.Xml;
using MoVE.Core;
using New.Move.Core;
using Rockstar.MoVE.Framework.DataModel.Prototypes;

namespace Rockstar.MoVE.Design
{

    public class DatabaseNodeFactory : DatabaseXmlFactory
    {
        #region Methods
        /// <summary>
        /// Create a new <see cref="INode"/> object based on the specified reader.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as a data provider for the new node object.
        /// </param>
        /// <param name="database">
        /// The database the new node exists in.
        /// </param>
        /// <param name="parent">
        /// The identifier of the parent to the new node.
        /// </param>
        /// <returns>
        /// A new node that is represented by the specified reader.
        /// </returns>
        public override INode CreateNode(XmlReader reader, Database database, ITransitional parent)
        {
            if (string.CompareOrdinal(reader.Name, "motiontree") == 0)
            {
                return new Motiontree(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "statemachine") == 0)
            {
                return new StateMachine(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "logicleaf") == 0)
            {
                return new LogicLeafSM(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "logicinline") == 0)
            {
                return new LogicInlineSM(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "output") == 0)
            {
                return new Output(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "input") == 0)
            {
                return new Input(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "invalid") == 0)
            {
                return new InvalidPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "network") == 0)
            {
                return new Network(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "multipleoperation") == 0)
            {
                return new MultiplyOperatorPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "addoperation") == 0)
            {
                return new AddOperatorPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "realsignal") == 0)
            {
                return new Real(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "animationsignal") == 0)
            {
                return new Animation(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "booleansignal") == 0)
            {
                return new Boolean(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "clipsignal") == 0)
            {
                return new Clip(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "expressionsignal") == 0)
            {
                return new Expression(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "filtersignal") == 0)
            {
                return new Filter(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "framesignal") == 0)
            {
                return new Frame(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "hashstringsignal") == 0)
            {
                return new HashString(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "intsignal") == 0)
            {
                return new Int(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "nodeparametersignal") == 0)
            {
                return new NodeSignalParameter(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "parameterizedmotionsignal") == 0)
            {
                return new ParameterizedMotion(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "tagsignal") == 0)
            {
                return new Tag(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "Curve") == 0)
            {
                return new CurvePrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Extrapolate") == 0)
            {
                return new ExtrapolatePrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Filter") == 0)
            {
                return new FilterPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Identity") == 0)
            {
                return new IdentityPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Expression") == 0)
            {
                return new ExpressionPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "JointLimit") == 0)
            {
                return new JointLimitPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Frame") == 0)
            {
                return new FramePrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Pose") == 0)
            {
                return new PosePrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Capture") == 0)
            {
                return new CapturePrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Animation") == 0)
            {
                return new AnimationPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "ParameterizedMotion") == 0)
            {
                return new ParameterizedMotionPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Clip") == 0)
            {
                return new ClipPrototype(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "Add2") == 0)
            {
                return new AddPrototype(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "AddN") == 0)
            {
                return new NWayAddPrototype(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "Blend2") == 0)
            {
                return new BlendPrototype(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "BlendN") == 0)
            {
                return new NWayBlendPrototype(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "Merge2") == 0)
            {
                return new MergePrototype(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "MergeN") == 0)
            {
                return new NWayMergePrototype(reader, database, parent);
            }
            else if (string.CompareOrdinal(reader.Name, "Reference") == 0)
            {
                return new Reference(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "TunnelEvent") == 0)
            {
                return new TunnelEvent(reader, database, parent.Id);
            }
            else if (string.CompareOrdinal(reader.Name, "TunnelParameter") == 0)
            {
                return new TunnelParameter(reader, database, parent.Id);
            }

            
            
            reader.Skip();
            return null;
        }

        /// <summary>
        /// Create a new <see cref="TransitionBase"/> object based on the specified reader.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as a data provider for the new transition
        /// base object.
        /// </param>
        /// <param name="database">
        /// The database the new transition exists in.
        /// </param>
        /// <returns>
        /// A new transition base that is represented by the specified reader.
        /// </returns>
        public override TransitionBase CreateTransitionBase(XmlReader reader, Database database)
        {
            if (reader.Name == "base")
            {
                return new Rockstar.MoVE.Framework.Transition(reader, database);
            }

            reader.Skip();
            return null;
        }

        /// <summary>
        /// Create a new <see cref="ICondition"/> object based on the specified reader.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as a data provider for the new condition object.
        /// </param>
        /// <param name="database">
        /// The database the new condition exists in.
        /// </param>
        /// <returns>
        /// A new transition condition that is represented by the specified reader.
        /// </returns>
        public override ICondition CreateTransitionCondition(XmlReader reader, Database database)
        {
            if (string.CompareOrdinal(reader.Name, "request") == 0)
            {
                return new OnRequestCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "flag") == 0)
            {
                return new OnFlagCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "tag") == 0)
            {
                return new OnTagCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "event") == 0)
            {
                return new AtEventCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "greaterthan") == 0)
            {
                return new GreaterThanCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "greaterthanequal") == 0)
            {
                return new GreaterThanEqualCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "lessthan") == 0)
            {
                return new LessThanCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "lessthanequal") == 0)
            {
                return new LessThanEqualCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "inrange") == 0)
            {
                return new InRangeCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "notinrange") == 0)
            {
                return new NotInRangeCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "lifetimegreaterthan") == 0)
            {
                return new LifetimeGreaterThanCondition(reader);
            }
            else if (string.CompareOrdinal(reader.Name, "lifetimelessthan") == 0)
            {
                return new LifetimeLessThanCondition(reader);
            }
            else if (string.CompareOrdinal(reader.Name, "boolequal") == 0)
            {
                return new BoolEqualCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "int") == 0)
            {
                return new IntCondition(reader, database);
            }
            else if (string.CompareOrdinal(reader.Name, "string") == 0)
            {
                return new StringCondition(reader, database);
            }

            reader.Skip();
            return null;
        }
        #endregion
    }
}
