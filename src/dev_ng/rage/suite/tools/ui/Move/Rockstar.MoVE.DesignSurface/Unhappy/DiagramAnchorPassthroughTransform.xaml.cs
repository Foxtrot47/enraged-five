﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public partial class DiagramAnchorPassthroughTransform : DiagramAnchor, ITransformAnchor
    {
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            HitRectangle.Width = this.ActualWidth + 18.0;

            base.OnRenderSizeChanged(sizeInfo);
        }

        public DiagramAnchorPassthroughTransform(string name, IAnchor source) : base(name, source)
        {
            InitializeComponent();

            MouseEnter += new MouseEventHandler(
                delegate(object sender, MouseEventArgs e)
                {
                    HighlightEnabled = true;
                });

            MouseLeave += new MouseEventHandler(
                delegate(object sender, MouseEventArgs e)
                {
                    HighlightEnabled = false;
                });
        }

        public override Point? Sender
        {
            get { return new Point?(SenderPoint.TranslatePoint(new Point(0, 0), GetDiagram(this))); }
        }

        public override Point? Receiver
        {
            get { return new Point?(ReceiverPoint.TranslatePoint(new Point(0, 0), GetDiagram(this))); }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                Point position = SenderPoint.TranslatePoint(new Point(0, 0), diagram);
                Start = new Point?(position);
                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        DiagramAdorner adorner = new DiagramAdorner(diagram, Start.Value, this);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

        public override IDiagramAnchor HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is DiagramAnchorSourceTransform || hitobj is DiagramAnchorPassthroughTransform ||
                            (hitobj is DiagramTunnelParameter && (hitobj as DiagramTunnelParameter).Parameter == Parameter.Node) || hitobj is ITransformAnchor)
                    {
                        return hitobj as IDiagramAnchor;
                    }

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        void ReceiverGrapple_MouseDown(object sender, RoutedEventArgs e)
        {
            try
            {
                Motiontree parent = Anchor.Parent.Database.Find(Anchor.Parent.Parent) as Motiontree;

                // TODO: store a reference to connections in the anchor
                New.Move.Core.Connection connection = parent.Connections.FirstOrDefault(
                    delegate(New.Move.Core.Connection c) { return c.Dest == Anchor; });

                DiagramConnection tag = connection.Tag as DiagramConnection;

                parent.Connections.Remove(connection);

                if (tag.Sender.Sender.HasValue)
                {
                    Diagram diagram = GetDiagram(this);
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        Adorner adorner = new DiagramAdorner(diagram, tag.Sender.Sender.Value, tag.Sender);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
