﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using AvalonDock;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Services;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Navigator", WindowDockRegion.Left)]
    public partial class Navigator : DockableContent
    {
        public Navigator()
        {
            InitializeComponent();
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _ApplicationService = value;
                _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
            }
        }

        IApplicationService _ApplicationService;

        [Import(typeof(IDataModelService))]
        IDataModelService DataModelService
        {
            set
            {
                _DataModelService = value;
                _DataModelService.CurrentActiveChanged += new CurrentActiveChangedHandler(CurrentActiveChanged);
                _DataModelService.CurrentRootChanged += new CurrentRootChangedHandler(CurrentRootChanged);
            }
        }

        ISelectionService _SelectionService = null;

        [Import(typeof(ISelectionService))]
        ISelectionService SelectionService
        {
            set
            {
                _SelectionService = value;
            }
        }

        IDataModelService _DataModelService;
        INode lastNodeMouseDownOn_;

        void CurrentDocumentChanged()
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            RootTreeViewItem.ItemsSource = database.Root.Children;
        }

        void CurrentActiveChanged()
        {
            ITransitional active = _DataModelService.CurrentActive as ITransitional;
            string path = active.FromHierarchy(RootTreeViewItem);
            TreeView.SetSelectedItem(path);
        }

        void CurrentRootChanged()
        {
            if (_ApplicationService.CurrentDocument == null)
                return;

            if (_ApplicationService.CurrentDocument.Tag == null)
                return;

            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            RootTreeViewItem.ItemsSource = database.Root.Children;
        }

        private void OnLeafMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Look up the node from the control
            FrameworkElement controlClicked = (FrameworkElement)sender;
            INode selectedNode = (INode)controlClicked.DataContext;

            lastNodeMouseDownOn_ = selectedNode;
        }

        private void OnLeafMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Look up the node from the control
            FrameworkElement controlClicked = (FrameworkElement)sender;
            INode selectedNode = (INode)controlClicked.DataContext;

            if (selectedNode != lastNodeMouseDownOn_)
                return;

            // select this node if it's present on the canvas
            Database database = (Database)_ApplicationService.CurrentDocument.Tag;
            ITransitional selectedNodeParent = (ITransitional)database.Find(selectedNode.Parent);
            if (database.Active == selectedNodeParent)
            {
                database.SelectedItems.Clear();
                database.SelectedItems.Add(selectedNode);
            }
        }

        private void OnLeafDoubleClicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Look up the node from the control
            FrameworkElement controlClicked = (FrameworkElement)sender;
            INode selectedNode = (INode)controlClicked.DataContext;

            // Set the current node's parent as the active transitional
            Database database = (Database)_ApplicationService.CurrentDocument.Tag;
            ITransitional selectedNodeParent = (ITransitional)database.Find(selectedNode.Parent);
            database.Active = selectedNodeParent;

            // Focus on the selected node
            DatabaseDocument databaseDocument = (DatabaseDocument)_ApplicationService.CurrentDocument;
            DiagramNode diagramNode = (DiagramNode)selectedNode.Tag;
            databaseDocument.CentreOnDiagramNode(diagramNode);

            // Select the node
            database.SelectedItems.Clear();
            database.SelectedItems.Add(selectedNode);
        }
    }

    internal static class NavigatorExtensions
    {
        internal static string FromHierarchy(this ITransitional leaf, TreeViewItem rootItem)
        {
            Database database = leaf.Database;

            Stack<INode> items = new Stack<INode>();
            items.Push(leaf);

            INode parent = database.Find(leaf.Parent);

            while (parent != null)
            {
                items.Push(parent);
                parent = database.Find(parent.Parent);
            }

            if (items.Count != 0)
            {
                INode root = items.Pop();
                string str = rootItem.ToString();

                while (items.Count != 0)
                {
                    INode item = items.Pop();
                    str = Path.Combine(str, item.ToString());
                }

                Console.WriteLine(str);
                return str;
            }

            return String.Empty;
        }
    }
}
