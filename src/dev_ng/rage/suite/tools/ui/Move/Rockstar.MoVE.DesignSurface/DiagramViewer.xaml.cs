﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

using New.Move.Core;
using Move.Utils;

using Rockstar.MoVE.Services;
using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Framework.DataModel.Prototypes;

using RSG.Base.Logging;

using System.IO;


namespace Rockstar.MoVE.Design
{
    public partial class DiagramViewer : UserControl
    {
        private static class Const
        {
            public static double PanMargin = 50;
            public static double ZoomDelta = 0.1;
            public static double Zoom = 1.5;
        }

        public static Point sRightClipPt = new Point(0.0, 0.0);

        public double Zoom
        {
            get
            {
                return ZoomSlider.Value;
            }
            set
            {
                if (value >= ZoomSlider.Minimum && value <= ZoomSlider.Maximum)
                {
                    Diagram.Scale = value;
                    ZoomSlider.Value = value;
                    //Console.WriteLine("Zoom: {0}", value);
                    UpdateScrollSize();
                    UpdateDatabaseZoom();
                }
                else
                {
                    //LPXO: Not sure why we'd want to do this but ive left this commented out for the time being to
                    // fix user complaints of zoom looping
                    
                    //SetZoomOnActive(Const.Zoom);
                    //Diagram.Scale = Const.Zoom;
                    //ZoomSlider.Value = Const.Zoom;
                    //UpdateScrollSize();
                }
            }
        }

        internal void ScrollTo(Point position)
        {
            ScrollViewer.ScrollToHorizontalOffset(position.X);
            ScrollViewer.ScrollToVerticalOffset(position.Y);
        }

        Point _ScrollStartPoint;
        Point _ScrollStartOffset;

        public DiagramViewer(IDataModelService dataModelService, IDataModel dataModel)
        {
            InitializeComponent();

            _DataModelService = dataModelService;
            _DataModel = dataModel;

            Zoom = 1.5;

            Diagram.DatabaseChanged += new EventHandler<EventArgs>(Diagram_DatabaseChanged);
        }

        bool _Loaded = false;

        protected override void OnInitialized(EventArgs e)
        {
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete,
                Diagram.ApplicationCommandDelete_Execute, Diagram.ApplicationCommandDelete_CanExecute));
            Diagram.Loaded += new RoutedEventHandler(Diagram_Loaded);
            Diagram.Unloaded += new RoutedEventHandler(Diagram_Unloaded);

            Diagram.SizeChanged += new SizeChangedEventHandler(Diagram_SizeChanged);

            ZoomSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(ZoomSlider_ValueChanged);
            ZoomSlider.MouseDoubleClick += new MouseButtonEventHandler(ZoomSlider_MouseDoubleClick);

            SizeChanged += new SizeChangedEventHandler(Diagram_SizeChanged); 
            ScrollViewer.ScrollChanged += new ScrollChangedEventHandler(ScrollViewer_ScrollChanged);
            
            base.OnInitialized(e);
        }

        IDataModelService _DataModelService = null;
        IDataModel _DataModel = null;

        void Diagram_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_Loaded)
            {
                _DataModelService.CurrentActiveChanged += new CurrentActiveChangedHandler(Database_ActiveChanged);
                _DataModelService.CurrentRootChanged += new CurrentRootChangedHandler(Database_RootChanged);

                ITransitional active = (ITransitional)_DataModelService.CurrentActive;
                ScrollViewer.ScrollToHorizontalOffset(active.ScrollOffset.X + ScrollViewer.ViewportWidth);
                ScrollViewer.ScrollToVerticalOffset(active.ScrollOffset.Y + ScrollViewer.ViewportHeight);

                PathRoot.ItemsSource = Diagram.Database.Root.Children;

                Stack<INode> path = new Stack<INode>();

                INode node = Diagram.Database.Active;
                while (node != Diagram.Database.Root)
                {
                    path.Push(node);
                    node = Diagram.Database.Find(node.Parent);
                }

                if (path.Count != 0)
                {
                    INode curr = path.Pop();
                    Path.AddTrail(PathRoot, curr);

                    while (path.Count != 0)
                    {
                        INode last = curr;
                        curr = path.Pop();
                        Path.AddTrail(last, curr);
                    }
                }

                DependencyPropertyDescriptor desc =
                    DependencyPropertyDescriptor.FromProperty(Breadcrumb.SelectedItemProperty, typeof(Breadcrumb));
                desc.AddValueChanged(Path, Path_SelectedItemChanged);

                _Loaded = true;
            }

            UpdateScrollSize();
            this.Focus();
        }

        void Diagram_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_Loaded)
            {
                _Loaded = false;

                _DataModelService.CurrentActiveChanged -= new CurrentActiveChangedHandler(Database_ActiveChanged);
                _DataModelService.CurrentRootChanged -= new CurrentRootChangedHandler(Database_RootChanged);
            }
        }

        new internal void Focus()
        {
            IEnumerable<IDiagramElement> children = Diagram.Children.OfType<IDiagramElement>();

            Point min = new Point(Double.PositiveInfinity, Double.PositiveInfinity);
            Point max = new Point(Double.NegativeInfinity, Double.NegativeInfinity);
            int numChildren = 0;

            foreach (IDiagramElement child in children)
            {
                numChildren++;

                if (child.Position.X < min.X)
                    min.X = child.Position.X;
                if (child.Position.Y < min.Y)
                    min.Y = child.Position.Y;

                double width = child.Position.X + child.DesiredSize.Width;
                if (max.X < width)
                    max.X = width;
                double height = child.Position.Y + child.DesiredSize.Height;
                if (max.Y < height)
                    max.Y = height;
            }

            if (numChildren <= 0)
            {
                return;
            }

            Rect bounds = new Rect(min, max);

            ITransitional active = (ITransitional)_DataModelService.CurrentActive;
            
            double horizSet = bounds.TopLeft.X + bounds.Width / 2;
            double vertSet = bounds.TopLeft.Y + bounds.Height / 2;
            
            double horizFinal = (horizSet / Diagram.Const.Size) * ScrollViewer.ScrollableWidth;
            double vertFinal = (vertSet / Diagram.Const.Size) * ScrollViewer.ScrollableHeight;
            ScrollViewer.ScrollToHorizontalOffset(horizFinal);
            ScrollViewer.ScrollToVerticalOffset(vertFinal);
        }

        internal void CentreOnDiagramNode(DiagramNode diagramNode)
        {
            _SuppressScrollView = false;

            double newScrollX = (diagramNode.Centre.X / Diagram.Const.Size) * ScrollViewer.ScrollableWidth;
            double newScrollY = (diagramNode.Centre.Y / Diagram.Const.Size) * ScrollViewer.ScrollableHeight;
            ScrollViewer.ScrollToHorizontalOffset(newScrollX);
            ScrollViewer.ScrollToVerticalOffset(newScrollY);
        }

        public ObservableCollection<BreadcrumbItem> PathItems = new ObservableCollection<BreadcrumbItem>();

        void Diagram_DatabaseChanged(object sender, EventArgs e)
        {
            Database db = sender as Database;
            ITransitional active = db.Active;

            Zoom = active.Zoom;

            ScrollViewer.ScrollToHorizontalOffset(active.ScrollOffset.X);
            ScrollViewer.ScrollToVerticalOffset(active.ScrollOffset.Y);
        }

        void Path_SelectedItemChanged(object sender, EventArgs e)
        {
            if (Database.IsLoading)
                return;

            Breadcrumb path = sender as Breadcrumb;
            if (Diagram.Database != null)
            {
                if (path.SelectedItem is ITransitional)
                {
                    Diagram.Database.Active = path.SelectedItem as ITransitional;
                }
                else if (path.SelectedItem == PathRoot)
                {
                    Diagram.Database.Active = Diagram.Database.Root;
                }
                else if (path.SelectedItem is ILogic)
                {
                    CentreOnLogicEntity(path.SelectedItem as ILogic);
                }
            }
        }

        void CentreOnLogicEntity(ILogic logicEntity)
        {
            DiagramNode diagramNode = logicEntity.Tag as DiagramNode;
            double newScrollX = (diagramNode.Centre.X / Diagram.Const.Size) * ScrollViewer.ScrollableWidth;
            double newScrollY = (diagramNode.Centre.Y / Diagram.Const.Size) * ScrollViewer.ScrollableHeight;
            ScrollViewer.ScrollToHorizontalOffset(newScrollX);
            ScrollViewer.ScrollToVerticalOffset(newScrollY);
        }

        void Diagram_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateScrollSize();
        }

        void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Zoom = e.NewValue;
        }

        void ZoomSlider_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.Zoom = 1.0;
        }

        void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_SuppressScrollView)
            {
                _SuppressScrollView = false;
                return;
            }

            double horizontalOffset = 0;
            double verticalOffset = 0;

            if (e.ExtentWidthChange != 0 && e.ExtentWidthChange != e.ExtentWidth)
            {
                // Keep centered horizontally.
                double percent = e.ExtentWidthChange / (e.ExtentWidth - e.ExtentWidthChange);
                double middle = e.HorizontalOffset + (e.ViewportWidth / 2);
                horizontalOffset = middle * percent;
                ScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset + horizontalOffset);
            }

            if (e.ExtentHeightChange != 0 && e.ExtentHeightChange != e.ExtentHeight)
            {
                // Keep centered vertically.
                double percent = e.ExtentHeightChange / (e.ExtentHeight - e.ExtentHeightChange);
                double middle = e.VerticalOffset + (e.ViewportHeight / 2);
                verticalOffset = middle * percent;
                ScrollViewer.ScrollToVerticalOffset(e.VerticalOffset + verticalOffset);
            }

            ITransitional active = (ITransitional)_DataModelService.CurrentActive;

            if (_Loaded)
            {
                // Get the new top-left coordinates in world space.
                double topLeftX = ScrollViewer.ContentHorizontalOffset / this.Zoom;
                double topLeftY = ScrollViewer.ContentVerticalOffset / this.Zoom;
                //Console.WriteLine("{0}, {1}", topLeftX, topLeftY);

                active.ScrollOffset = new Point((e.HorizontalOffset + horizontalOffset) - ScrollViewer.ViewportWidth,
                                                (e.VerticalOffset + verticalOffset) - ScrollViewer.ViewportHeight);

                //Console.WriteLine("Scroll Offset: X {0}, Y {1}", active.ScrollOffset.X, active.ScrollOffset.Y);
 
                ScrollViewer.ScrollToHorizontalOffset(active.ScrollOffset.X + ScrollViewer.ViewportWidth);
                ScrollViewer.ScrollToVerticalOffset(active.ScrollOffset.Y + ScrollViewer.ViewportHeight);
            }
        }

        bool _SuppressScrollView = false;

        void Database_ActiveChanged()
        {
            _SuppressScrollView = true;

            object sender = _DataModelService.CurrentActive;
            ITransitional active = sender as ITransitional;

            Diagram.UnSelectItems();

            if (active.Zoom <= 0.0)
            {
                this.Zoom = 1.0;
            }
            else
            {
                this.Zoom = active.Zoom;
            }

            ScrollViewer.ScrollToHorizontalOffset(active.ScrollOffset.X + ScrollViewer.ViewportWidth);
            ScrollViewer.ScrollToVerticalOffset(active.ScrollOffset.Y + ScrollViewer.ViewportHeight);

            // Update breadcrumb
            ITransitional parent = active.Database.Find(active.Parent) as ITransitional;
            if (parent != null)
            {
                Path.AddTrail(parent, active);
            }
            else
            {
                Path.GoTo(PathRoot);
            }
        }

        void Database_RootChanged()
        {
            PathRoot.ItemsSource = Diagram.Database.Root.Children;// kick the breadcrumb
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu context = (ContextMenu)sender;

            context.Items.Clear();

            MenuItem createParent = new MenuItem();
            createParent.Header = "Create parent state machine...";
            createParent.Click += new RoutedEventHandler(CreateParent_Click);

            context.Items.Add(createParent);

            if (Diagram.Database.Active.IsAutomaton)
            {
                if (Diagram.Database.Active.Type != "State machine")
                {
                    MenuItem createParentMotion = new MenuItem();
                    createParentMotion.Header = "Create parent motion tree...";
                    createParentMotion.Click += new RoutedEventHandler(CreateParentMotion_Click);

                    context.Items.Add(createParentMotion);
                }
            }
            else
            {
                MenuItem createReference = new MenuItem();
                createReference.Header = "Create reference...";
                createReference.Click += new RoutedEventHandler(CreateReference_Click);

                context.Items.Add(createReference);
            }
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu context = (ContextMenu)sender;
            context.Items.Clear();
        }

        void CreateParent_Click(object sender, RoutedEventArgs e)
        {
            // Here we are inserting a new state machine in between the current transitional and its parent
            ITransitional currentTransitional = Diagram.Database.Active;
            Point formerCurrentTransitionalPosition = currentTransitional.Position;
            currentTransitional.Position = new Point(2048, 2048);
            ITransitional newParentStateMachine = currentTransitional.Database.Create(new StateMachineDesc()) as ITransitional;
            newParentStateMachine.ScrollOffset = new Point(2048, 2048);

            if (currentTransitional.Parent == Guid.Empty)// insert the new state machine at the root of the db
            {
                newParentStateMachine.Add(currentTransitional);

                Diagram.Database.Active = newParentStateMachine;
                Diagram.Database.Root = newParentStateMachine;
            }
            else// insert the new state machine above the current level as a child of its current parent
            {
                newParentStateMachine.Position = formerCurrentTransitionalPosition;// Put the new state machine where the node it's replacing was

                // Update the transition order collections.
                foreach (var transition in currentTransitional.TransitionOrder)
                {
                    newParentStateMachine.TransitionOrder.Add(transition);
                }
                currentTransitional.TransitionOrder.Clear();

                var formerCurrentTranstionalParentNode = (ITransitional)Diagram.Database.Find(currentTransitional.Parent);
                var formerCurrentTranstionalParentNodeAsAutomaton = (IAutomaton)formerCurrentTranstionalParentNode;

                // Update all of the transitions themselves
                foreach (var transition in formerCurrentTranstionalParentNodeAsAutomaton.Transitions)
                {
                    if (transition.Source == currentTransitional)
                    {
                        transition.Source = newParentStateMachine;
                    }

                    if (transition.Dest == currentTransitional)
                    {
                        transition.Dest = newParentStateMachine;
                    }
                }

                bool transitionalBeingReplacedIsDefault = (formerCurrentTranstionalParentNodeAsAutomaton.Default == currentTransitional);

                // Add the new state machine as a child of its new parent
                formerCurrentTranstionalParentNode.Remove(currentTransitional);
                formerCurrentTranstionalParentNode.Add(newParentStateMachine);
                newParentStateMachine.Add(currentTransitional);

                if (transitionalBeingReplacedIsDefault)
                    formerCurrentTranstionalParentNodeAsAutomaton.Default = newParentStateMachine;

                Diagram.Database.Active = newParentStateMachine;
            }
        }

        void CreateParentMotion_Click(object sender, RoutedEventArgs e)
        {
            StateMachine currentTransitional = Diagram.Database.Active as StateMachine;
            if (currentTransitional == null)
                return;

            Database database = currentTransitional.Database;
            if (database == null)
                return;

            //  Create a new motion tree for the new parent
            Motiontree newParentMotionTree = database.Create(new MotiontreeDesc()) as Motiontree;
            newParentMotionTree.Name = "Inserted Motion Tree";
            newParentMotionTree.ScrollOffset = new Point(2048, 2048);
            newParentMotionTree.Children[0].Position = new Point(100 + 2048, 2048 - 200);
            database.MakeNodeNameUnique(newParentMotionTree);

            //  Create a new LogicLeafSM
            LogicLeafSM newStateMachine = currentTransitional.Database.Create(new LogicLeafSMDesc()) as LogicLeafSM;
            newStateMachine.Name = "Inserted State Machine";
            newStateMachine.ScrollOffset = currentTransitional.ScrollOffset;
            database.MakeNodeNameUnique(newStateMachine);

            // Add the state machine to the motion tree and create a connection.
            newStateMachine.Position = new Point(2048 - 100, 2048 - 200);
            newParentMotionTree.Add(newStateMachine);
            newParentMotionTree.Connections.Add(new Connection(newStateMachine.Result, (newParentMotionTree.Children[0] as Output).OutputTransform));

            foreach (New.Move.Core.Transition t in currentTransitional.Transitions)
            {
                newStateMachine.Transitions.Add(t);
            }

            //  Copy the children across
            ITransitional defaultTransitional = null;
            foreach (INode child in currentTransitional.Children)
            {
                newStateMachine.Add(child);
                if (child is ITransitional)
                {
                    if (currentTransitional.Default == child)
                        defaultTransitional = child as ITransitional;
                }
            }
            newStateMachine.Default = defaultTransitional;

            //  Remove the original state machine children and transitions
            currentTransitional.Children.Clear();
            currentTransitional.Transitions.Clear();
            currentTransitional.Default = newParentMotionTree;
            currentTransitional.Add(newParentMotionTree);
            currentTransitional.ScrollOffset = new Point(2048, 2048);
            currentTransitional.Children[0].Position = new Point(100 + 2048, 2048 - 200);

            Diagram.Database.Active = newParentMotionTree;
            Diagram.Database.Active = newStateMachine;
            this.Focus();

            //  Dispose of the original state machine
            //currentTransitional.Dispose();


            //if (currentTransitional.Parent == Guid.Empty)// insert the new motion tree at the root of the db
            //{
            //    newParentMotionTree.Add(newStateMachine);

            //    Diagram.Database.Active = newParentMotionTree;
            //    Diagram.Database.Root = newParentMotionTree;
            //}
            //else// insert the new motion tree above the current level as a child if its current parent
            //{
            //    newParentMotionTree.Position = currentTransitional.Position;// Put the new motion tree where the node it's replacing was

            //    // Add the new motion tree as a child of its new parent
            //    var formerCurrentTranstionalParentNode = (ITransitional)Diagram.Database.Find(currentTransitional.Parent);
            //    if (formerCurrentTranstionalParentNode.Type == "StateMachine")
            //    {
            //        StateMachine parentStateMachine = formerCurrentTranstionalParentNode as StateMachine;
            //        foreach (New.Move.Core.Transition t in parentStateMachine.Transitions)
            //        {
            //            if (t.Dest == currentTransitional)
            //            {
            //                t.Dest = newParentMotionTree;
            //            }
            //            if (t.Source == currentTransitional)
            //            {
            //                t.Source = newParentMotionTree;
            //            }
            //        }
            //    }
            //    else if (formerCurrentTranstionalParentNode.Type == "State machine")
            //    {
            //        LogicLeafSM parentStateMachine = formerCurrentTranstionalParentNode as LogicLeafSM;
            //        foreach (New.Move.Core.Transition t in parentStateMachine.Transitions)
            //        {
            //            if (t.Dest == currentTransitional)
            //            {
            //                t.Dest = newParentMotionTree;
            //            }
            //            if (t.Source == currentTransitional)
            //            {
            //                t.Source = newParentMotionTree;
            //            }
            //        }
            //    }
            //    //  Copy transition orders too
            //    newParentMotionTree.TransitionOrder = currentTransitional.TransitionOrder;
            //    //  And enter and exit events
            //    newParentMotionTree.EnterEvent = currentTransitional.EnterEvent;
            //    newParentMotionTree.ExitEvent = currentTransitional.ExitEvent;
            //    //  And name the new motion tree the same as the state machine it is parenting
            //    newParentMotionTree.Name = currentTransitional.Name;

            //    formerCurrentTranstionalParentNode.Remove(currentTransitional);
            //    formerCurrentTranstionalParentNode.Add(newParentMotionTree);
            //    newParentMotionTree.Add(newStateMachine);

            //    Diagram.Database.Active = newParentMotionTree;
            //}

        }

        void CreateReference_Click(object sender, RoutedEventArgs e)
        {
            ITransitional active = Diagram.Database.Active;
            DatabaseXmlFactory factory = active.Database.XmlFactory;
            if (factory == null)
            {
                factory = new DatabaseNodeFactory();
            }

            ILogic reference = active.Database.CreateReference(new ReferenceDesc(), factory);
            reference.Position = sRightClipPt;
            active.Add(reference);
        }
        
        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            base.OnPreviewMouseWheel(e);

            if (0 < e.Delta)
            {
                Zoom += Const.ZoomDelta;
            }
            else
            {
                Zoom -= Const.ZoomDelta;
            }

            e.Handled = true;
        }

        Point? _OriginalPosition = null;

        Point _RawBoxStartPosition;
        Point? _BoxStartPosition = null;
        bool _IsDoingABoxSelection = false;
        Rect _SelectionBox;

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    //  Make sure the element we clicked is the ScrollViewer, before capturing the mouse
                    IInputElement element = this.InputHitTest(e.GetPosition(this));
                    bool ctrlHeld = Keyboard.Modifiers.HasFlag(ModifierKeys.Control);
                    if (element is System.Windows.Controls.ScrollViewer && !ctrlHeld)
                    {
                        if (ScrollViewer.IsMouseOver && !Diagram.IsMouseOver)
                        {
                            _ScrollStartPoint = e.GetPosition(this);
                            _ScrollStartOffset.X = ScrollViewer.HorizontalOffset;
                            _ScrollStartOffset.Y = ScrollViewer.VerticalOffset;

                            Cursor = (ScrollViewer.ExtentWidth > ScrollViewer.ViewportWidth) ||
                                (ScrollViewer.ExtentHeight > ScrollViewer.ViewportHeight) ?
                                Cursors.ScrollAll : Cursors.Arrow;

                            CaptureMouse();

                            Diagram.Database.SelectedItems.Clear();
                            Diagram.UnSelectItems();
                        }
                    }
                    else if (element is System.Windows.Controls.ScrollViewer && ctrlHeld)
                    {
                        // We can start a box selection
                        _IsDoingABoxSelection = true;

                        _RawBoxStartPosition = e.GetPosition(this);
                        Point pos = e.GetPosition(this);
                        Point offset = new Point(this.ScrollViewer.HorizontalOffset / this.Zoom, this.ScrollViewer.VerticalOffset / this.Zoom);
                        this._BoxStartPosition = new Point((pos.X / this.Zoom) + offset.X, (pos.Y / this.Zoom) + offset.Y);
                        this._SelectionBox = new Rect(_RawBoxStartPosition, _RawBoxStartPosition);

                        CaptureMouse();

                        Diagram.Database.SelectedItems.Clear();
                        Diagram.UnSelectItems();
                    }
                    break;
                case MouseButton.Middle:
                    if (ScrollViewer.IsMouseOver && !Diagram.IsMouseOver)
                    {
                        _OriginalPosition = new Point?(e.GetPosition(this));

                        CaptureMouse();
                    }
                    break;
                case MouseButton.Right:
                    sRightClipPt = e.GetPosition(Diagram);
                    break;
            }
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            if (IsMouseCaptured)
            {
                Cursor = Cursors.Arrow;
                ReleaseMouseCapture();

                if (_IsDoingABoxSelection)
                {
                    if (this.Diagram.Database.SelectedItems.Count == 1)
                    {
                        this.Diagram.SelectionService.CurrentSelection = this.Diagram.Database.SelectedItems[0];
                    }
                    else
                    {
                        this.Diagram.SelectionService.CurrentSelection = null;
                    }
                }
                _IsDoingABoxSelection = false;
                _OriginalPosition = null;
                _BoxStartPosition = null;
                this.InvalidateVisual();
            }
            base.OnPreviewMouseUp(e);
        }

        private List<object> previousBoxSelection = new List<object>();
        private object previousActiveBoxSelection;
        private List<object> selectionOrder = new List<object>();

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            if (IsMouseCaptured && !this._IsDoingABoxSelection)
            {
                if (!_OriginalPosition.HasValue)
                {
                    Point point = e.GetPosition(this);

                    Point delta = new Point(
                        (point.X > _ScrollStartPoint.X) ? -(point.X - _ScrollStartPoint.X) : (_ScrollStartPoint.X - point.X),
                        (point.Y > _ScrollStartPoint.Y) ? -(point.Y - _ScrollStartPoint.Y) : (_ScrollStartPoint.Y - point.Y));

                    Point offset = new Point(_ScrollStartOffset.X + delta.X, _ScrollStartOffset.Y + delta.Y);

                    ScrollViewer.ScrollToHorizontalOffset(offset.X);
                    ScrollViewer.ScrollToVerticalOffset(offset.Y);

                    Diagram.Database.Active.ScrollOffset = new Point(offset.X - ScrollViewer.ViewportWidth, offset.Y - ScrollViewer.ViewportHeight); ;
                }
                else
                {
                    Point pos = e.GetPosition(this);
                    Point original = _OriginalPosition.Value;
                    double horizontalChange = pos.X - original.X;
                    double verticalChange = pos.Y - original.Y;

                    Diagram.UpdatePositionOfSelectedItems(horizontalChange, verticalChange);
                }
            }
            else if (IsMouseCaptured && this._IsDoingABoxSelection && _BoxStartPosition.HasValue)
            {
                Point pos = e.GetPosition(this);
                this._SelectionBox = new Rect(_RawBoxStartPosition, pos);
                Point offset = new Point(this.ScrollViewer.HorizontalOffset / this.Zoom, this.ScrollViewer.VerticalOffset / this.Zoom);
                pos = new Point((pos.X / this.Zoom) + offset.X, (pos.Y / this.Zoom) + offset.Y);

                Point max = new Point(_BoxStartPosition.Value.X > pos.X ? _BoxStartPosition.Value.X : pos.X,
                        _BoxStartPosition.Value.Y > pos.Y ? _BoxStartPosition.Value.Y : pos.Y);
                Point min = new Point(_BoxStartPosition.Value.X > pos.X ? pos.X : _BoxStartPosition.Value.X,
                    _BoxStartPosition.Value.Y > pos.Y ? pos.Y : _BoxStartPosition.Value.Y);

                Diagram.Database.SelectedItems.Clear();
                List<object> currentBoxSelection = new List<object>();
                foreach (object child in this.Diagram.Children)
                {
                    UIElement uiElement = child as UIElement;
                    double offsetFix = 23.0;
                    if (child is DiagramNode)
                    {
                        IDiagramElement element = child as IDiagramElement;
                        Point elementPos = element.Position;
                        elementPos.Y = elementPos.Y + offsetFix;
                        if (IsPointInBox(min, max, new Point(elementPos.X, elementPos.Y)))
                        {
                            SelectionBehaviour.SetIsSelected(uiElement, true);
                            this.Diagram.SelectChild(child as DiagramNode);
                            this.Diagram.Database.SelectedItems.Add((child as DiagramNode).Source);
                            currentBoxSelection.Add((child as DiagramNode).Source);
                            e.Handled = true;
                        }
                        else if (IsPointInBox(min, max, new Point(elementPos.X + uiElement.RenderSize.Width, elementPos.Y)))
                        {
                            SelectionBehaviour.SetIsSelected(uiElement, true);
                            this.Diagram.SelectChild(child as DiagramNode);
                            this.Diagram.Database.SelectedItems.Add((child as DiagramNode).Source);
                            currentBoxSelection.Add((child as DiagramNode).Source);
                            e.Handled = true;
                        }
                        else if (IsPointInBox(min, max, new Point(elementPos.X + uiElement.RenderSize.Width, elementPos.Y + uiElement.RenderSize.Height)))
                        {
                            SelectionBehaviour.SetIsSelected(uiElement, true);
                            this.Diagram.SelectChild(child as DiagramNode);
                            this.Diagram.Database.SelectedItems.Add((child as DiagramNode).Source);
                            currentBoxSelection.Add((child as DiagramNode).Source);
                            e.Handled = true;
                        }
                        else if (IsPointInBox(min, max, new Point(elementPos.X, elementPos.Y + uiElement.RenderSize.Height)))
                        {
                            SelectionBehaviour.SetIsSelected(uiElement, true);
                            this.Diagram.SelectChild(child as DiagramNode);
                            this.Diagram.Database.SelectedItems.Add((child as DiagramNode).Source);
                            currentBoxSelection.Add((child as DiagramNode).Source);
                            e.Handled = true;
                        }
                    }
                    else if (child is DiagramConnection)
                    {
                        DiagramConnection connection = (child as DiagramConnection);
                        Point? sender = null;
                        Point? receiver = null;
                        if (connection.Sender != null && connection.Sender.Anchor != null && connection.Sender.Anchor.Tag is DiagramAnchor)
                        {
                            sender = (connection.Sender.Anchor.Tag as DiagramAnchor).Sender;
                            if (sender != null)
                                sender = new Point(sender.Value.X, sender.Value.Y + offsetFix);
                        }
                        else if (connection.Sender != null && connection.Sender.Anchor != null && connection.Sender.Anchor.Tag is DiagramSignal)
                        {
                            sender = (connection.Sender.Anchor.Tag as DiagramSignal).Sender;
                            if (sender != null)
                                sender = new Point(sender.Value.X, sender.Value.Y + offsetFix);
                        }
                        if (connection.Receiver != null && connection.Receiver.Anchor != null && connection.Receiver.Anchor.Tag is DiagramAnchor)
                        {
                            receiver = (connection.Receiver.Anchor.Tag as DiagramAnchor).Receiver;
                            if (receiver != null)
                                receiver = new Point(receiver.Value.X, receiver.Value.Y + offsetFix);
                        }
                        if ((sender == null || !sender.HasValue) && (receiver == null || !receiver.HasValue))
                            continue;

                        if (sender != null && sender.HasValue)
                        {
                            if (IsPointInBox(min, max, sender.Value))
                            {
                                SelectionBehaviour.SetIsSelected(uiElement, true);
                                this.Diagram.SelectionService.InsertSelectedItem((child as DiagramConnection).Connection);
                                this.Diagram.Database.SelectedItems.Add((child as DiagramConnection).Connection);
                                currentBoxSelection.Add((child as DiagramConnection).Connection);
                                e.Handled = true;
                                continue;
                            }
                        }
                        if (receiver != null && receiver.HasValue)
                        {
                            if (IsPointInBox(min, max, receiver.Value))
                            {
                                SelectionBehaviour.SetIsSelected(uiElement, true);
                                e.Handled = true;
                                continue;
                            }
                        }
                        SelectionBehaviour.SetIsSelected(uiElement, false);
                        this.Diagram.SelectionService.RemoveSelectedItem((child as DiagramConnection).Connection);
                        this.Diagram.Database.SelectedItems.Remove((child as DiagramConnection).Connection);
                    }
                    else if (child is DiagramTransition)
                    {
                        DiagramTransition transition = (child as DiagramTransition);
                        Point start = transition.Start;
                        Point end = transition.End;
                        if (IsPointInBox(min, max, start))
                        {
                            SelectionBehaviour.SetIsSelected(uiElement, true);
                            this.Diagram.SelectionService.InsertSelectedItem((child as DiagramTransition).Transition);
                            this.Diagram.Database.SelectedItems.Add((child as DiagramTransition).Transition);
                            currentBoxSelection.Add((child as DiagramTransition).Transition);
                            e.Handled = true;
                            continue;
                        }
                        if (IsPointInBox(min, max, end))
                        {
                            SelectionBehaviour.SetIsSelected(uiElement, true);
                            this.Diagram.SelectionService.InsertSelectedItem((child as DiagramTransition).Transition);
                            this.Diagram.Database.SelectedItems.Add((child as DiagramTransition).Transition);
                            currentBoxSelection.Add((child as DiagramTransition).Transition);
                            e.Handled = true;
                            continue;
                        }
                        SelectionBehaviour.SetIsSelected(uiElement, false);
                        this.Diagram.SelectionService.RemoveSelectedItem((child as DiagramTransition).Transition);
                        this.Diagram.Database.SelectedItems.Remove((child as DiagramTransition).Transition);
                    }
                }
                this.InvalidateVisual();

                if (currentBoxSelection.Count > 0)
                {
                    if (currentBoxSelection.Count > previousBoxSelection.Count)
                    {
                        // Something(s) has been selected
                        foreach (object selection in currentBoxSelection)
                        {
                            if (!previousBoxSelection.Contains(selection))
                            {
                                selectionOrder.Add(selection);
                                previousActiveBoxSelection = selection;
                                this.Diagram.SelectionService.CurrentSelection = selection;
                            }
                        }
                    }
                    else if (currentBoxSelection.Count < previousBoxSelection.Count)
                    {
                        // something has been de-selected.
                        foreach (object previousSelection in previousBoxSelection)
                        {
                            if (!currentBoxSelection.Contains(previousSelection))
                            {
                                selectionOrder.Remove(previousSelection);
                            }
                        }
                        if (this.selectionOrder.Count > 0)
                            this.Diagram.SelectionService.CurrentSelection = this.selectionOrder[this.selectionOrder.Count - 1];
                        else
                            this.Diagram.SelectionService.CurrentSelection = null;

                    }
                    else if (currentBoxSelection.Count == previousBoxSelection.Count)
                    {
                        //this.Diagram.SelectionService.CurrentSelection = previousActiveBoxSelection;
                    }
                }
                else
                {
                    previousActiveBoxSelection = null;
                    selectionOrder.Clear();
                    this.Diagram.UnSelectItems();
                }

                previousBoxSelection = currentBoxSelection;
            }

            base.OnPreviewMouseMove(e);
        }

        private bool IsPointInBox(Point min, Point max, Point point)
        {
            if (point.X < min.X || point.X > max.X)
                return false;
            if (point.Y < min.Y || point.Y > max.Y)
                return false;

            return true;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (this._IsDoingABoxSelection)
            {
                drawingContext.DrawRectangle(Brushes.Transparent, new Pen(Brushes.Black, 1.5), _SelectionBox);
            }
        }

        protected override void OnPreviewDrop(DragEventArgs e)
        {
            try
            {
                Array objList = (Array)e.Data.GetData(DataFormats.FileDrop);
                if (objList != null)
                {
                    Point offset = new Point(0.0, 0.0);
                    foreach (object obj in objList)
                    {
                        string s = obj.ToString();
                        INode node = null;
                        bool associationFound = false;
                        foreach (IFileAssociatedDesc desc in _DataModel.MotionTreeDescriptors.OfType<IFileAssociatedDesc>())
                        {
                            string[] fileExtensions = desc.FileExtension.Split(';');
                            foreach(string fileExtension in fileExtensions)
                            {
                                if(fileExtension == System.IO.Path.GetExtension(s))
                                {
                                    node = Diagram.Database.Create(desc as IDesc);
                                    node.Position = new Point(e.GetPosition(Diagram).X + offset.X, e.GetPosition(Diagram).Y + offset.Y);
                                    offset.X += 10.0;
                                    offset.Y += 10.0;

                                    Diagram.Database.AppendChild(Diagram.Database.Active, node);

                                    if (node is IFileAssociated)
                                        (node as IFileAssociated).SetAssociatedFilename(s);

                                    associationFound = true;
                                }
                            }
                            
                        }
                        if(!associationFound)
                            LogFactory.ApplicationLog.Message("No association found for \"{0}\".  It may not be registered.", s);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                LogFactory.ApplicationLog.Error("Error in DragDrop function: {0}", ex.Message);
            }
        }

        void UpdateScrollSize()
        {
            if (ActualWidth == 0 || ActualHeight == 0)
                return;

            Size diagramSize = new Size(Diagram.ActualWidth * Zoom, Diagram.ActualHeight * Zoom);

            Grid.Width = diagramSize.Width;
            Grid.Height = diagramSize.Height;

            //Console.WriteLine("ScrollSize: Width {0}, Height {1}", Grid.Width, Grid.Height); 
        }

        void UpdateDatabaseZoom()
        {
            if (Diagram.Database != null)
            {
                //Console.WriteLine("Update Database Zoom {0} on {1}", ZoomSlider.Value, Diagram.Database.Active.Name);
                Diagram.Database.Active.Zoom = ZoomSlider.Value;
            }
        }

        void SetZoomOnActive(double zoom)
        {
            if (Diagram.Database != null)
            {
                Diagram.Database.Active.Zoom = zoom;
            }
        }

        void CollectionView_Filter(object sender, FilterEventArgs e)
        {
            if (e.Item is IAutomaton)
                e.Accepted = true;

            e.Accepted = false;
        }

        void BackButton_Click(object sender, RoutedEventArgs e)
        {
            ITransitional active = Diagram.Database.Active;
            ITransitional parent = (ITransitional)Diagram.Database.Find(active.Parent);

            if (parent != null)
                Diagram.Database.Active = parent;
        }
    }
}
