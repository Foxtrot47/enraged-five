﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

using New.Move.Core;
using Rockstar.MoVE.Services;

namespace Rockstar.MoVE.Design
{
    internal class NodeDragController
    {
        internal NodeDragController(Diagram diagram)
        {
            diagram_ = diagram;
            nodes_ = new List<DiagramNode>();
        }

        internal void AddNode(DiagramNode node)
        {
            node.MouseLeftButtonDown += new MouseButtonEventHandler(OnDiagramNodeMouseLeftButtonDown);
            node.MouseLeftButtonUp += new MouseButtonEventHandler(OnDiagramNodeMouseLeftButtonUp);
            nodes_.Add(node);
        }

        internal void RemoveNode(DiagramNode node)
        {
            node.MouseLeftButtonUp -= new MouseButtonEventHandler(OnDiagramNodeMouseLeftButtonUp);
            node.MouseLeftButtonDown -= new MouseButtonEventHandler(OnDiagramNodeMouseLeftButtonDown);
            nodes_.Remove(node);
        }

        internal void InjectService(ISelectionService service)
        {
            selectionService_ = service;
        }

        private void OnDiagramNodeMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DiagramNode control = (DiagramNode)sender;
            ISelectable clickedItem = (ISelectable)control.Tag;

            nodesMovedThisDrag_ = false;
            attemptDeselectionOnLeftMouseUp_ = diagram_.Database.SelectedItems.Contains(clickedItem);

            // Deselect everything if control is not held
            if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
            {
                diagram_.Database.SelectedItems.Clear();
            }

            // add it to the selection if it wasn't there
            if (!diagram_.Database.SelectedItems.Contains(clickedItem))
            {
                if(selectionService_ != null)
                    selectionService_.CurrentSelection = clickedItem;
                diagram_.Database.SelectedItems.Add(clickedItem);
            }

            DragBehaviour.SetIsDragging(control, true);

            Point nodeDragStartPosition = e.GetPosition(control as IInputElement);
            DragBehaviour.SetOriginalX(control, nodeDragStartPosition.X);
            DragBehaviour.SetOriginalY(control, nodeDragStartPosition.Y);

            control.CaptureMouse();
            control.MouseMove += new MouseEventHandler(OnDiagramNodeMouseMove);
        }

        private void OnDiagramNodeMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DiagramNode control = (DiagramNode)sender;

            //deselect the node if it was 'clicked' rather than dragged
            if (!nodesMovedThisDrag_ && attemptDeselectionOnLeftMouseUp_ && Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
            {
                ISelectable clickedItem = (ISelectable)control.Tag;
                diagram_.Database.SelectedItems.Remove(clickedItem);
            }

            control.ClearValue(DragBehaviour.OriginalXPropertyKey);
            control.ClearValue(DragBehaviour.OriginalYPropertyKey);

            control.MouseMove -= new MouseEventHandler(OnDiagramNodeMouseMove);
            control.ReleaseMouseCapture();
        }

        private void OnDiagramNodeMouseMove(object sender, MouseEventArgs e)
        {
            DiagramNode draggedControl = (DiagramNode)sender;

            Point nodeDragEndPosition = e.GetPosition(draggedControl as IInputElement);
            double dx = nodeDragEndPosition.X - DragBehaviour.GetOriginalX(draggedControl);
            double dy = nodeDragEndPosition.Y - DragBehaviour.GetOriginalY(draggedControl);

            if (dx != 0 || dy != 0)
                nodesMovedThisDrag_ = true;

            foreach (ISelectable selectedItem in diagram_.Database.SelectedItems)
            {
                if (selectedItem.Tag is DiagramNode)
                {
                    DiagramNode control = (DiagramNode)selectedItem.Tag;

                    DragBehaviour.SetX(control, DragBehaviour.GetX(control) + dx);
                    DragBehaviour.SetY(control, DragBehaviour.GetY(control) + dy);
                }
            }
        }

        private Diagram diagram_;// required to access database.
        private ISelectionService selectionService_;

        private List<DiagramNode> nodes_;

        // State associated with each particular node move operation
        private bool nodesMovedThisDrag_;
        private bool attemptDeselectionOnLeftMouseUp_;
    }
}
