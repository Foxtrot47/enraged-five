﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public class SelectedEventArgs : EventArgs
    {
        public SelectedEventArgs(ISelectable selectedItem)
        {
            SelectedItem = selectedItem;
        }

        public ISelectable SelectedItem { get; private set; }
    }

    public static class SelectionBehaviour
    {
        public static readonly DependencyProperty IsSelectionEnabledProperty =
            DependencyProperty.RegisterAttached("IsSelectionEnabled", typeof(bool), typeof(SelectionBehaviour), new UIPropertyMetadata(false, OnIsSelectionPropertyChanged));
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.RegisterAttached("IsSelected", typeof(bool), typeof(SelectionBehaviour), new UIPropertyMetadata(false));

        static public EventHandler<SelectedEventArgs> Selected;
        static void OnSelected(FrameworkElement element, ISelectable selectedItem)
        {
            if (Selected != null)
                Selected(element, new SelectedEventArgs(selectedItem));
        }

        public static bool GetIsSelectionEnabled(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsSelectionEnabledProperty);
        }

        public static void SetIsSelectionEnabled(DependencyObject obj, bool value)
        {
            obj.SetValue(IsSelectionEnabledProperty, value);
        }

        public static bool GetIsSelected(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsSelectedProperty);
        }

        public static void SetIsSelected(DependencyObject obj, bool value)
        {
            obj.SetValue(IsSelectedProperty, value);
        }

        static void OnIsSelectionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var element = obj as FrameworkElement;
            FrameworkContentElement contentElement = null;
            if (element == null)
            {
                contentElement = obj as FrameworkContentElement;
                if (contentElement == null)
                    return;
            }

            if (e.NewValue is bool == false)
                return;

            if ((bool)e.NewValue)
            {
                if (element != null)
                {
                    element.MouseLeftButtonDown += Element_MouseLeftButtonDown;
                }
                else
                {
                    contentElement.MouseLeftButtonDown += Element_MouseLeftButtonDown;
                }
            }
            else 
            {
                if (element != null)
                {
                    element.MouseLeftButtonDown -= Element_MouseLeftButtonDown;
                }
                else
                {
                    contentElement.MouseLeftButtonDown -= Element_MouseLeftButtonDown;
                }
            }
        }

        static void Element_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!(sender is DiagramNode))// Node selection and drag is handled by NodeDragController
            {
                var obj = sender as FrameworkElement;
                ISelectable selectableObject = (ISelectable)obj.Tag;
                OnSelected(obj, selectableObject);
            }
        }
    }
}