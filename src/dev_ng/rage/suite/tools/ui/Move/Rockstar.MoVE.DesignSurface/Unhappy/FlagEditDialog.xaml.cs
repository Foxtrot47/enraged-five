﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;
using Rage.Move;

using AvalonDock;

namespace Rockstar.MoVE.Design
{
    public partial class FlagEditDialog : Window
    {
        public FlagEditDialog()
        {
            InitializeComponent();
        }

        Database Database
        {
            get
            {
                //DocumentContent doc = App.Window.DocumentHost.SelectedItem as DocumentContent;
                //return doc.Tag as Database;
                return null;
            }
        }

        public void DoModal()
        {
            ShowDialog();
        }

        void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(Flag.Text))
                Database.Flags.Add(new Flag(Flag.Text));

            Close();
        }

        void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
