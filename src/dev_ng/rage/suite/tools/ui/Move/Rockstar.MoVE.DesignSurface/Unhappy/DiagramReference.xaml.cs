﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;
using Rockstar.MoVE.Design;

using Rage.Move.Core;

namespace Rockstar.MoVE.Design
{
    class ReferenceResponse : ICommandResponse
    {
        public ReferenceResponse(Reference reference)
        {
            _Reference = reference;
        }

        public string Filename
        {
            get
            {
                return _Reference.Filename.Value;
            }
            set
            {
                _Reference.Filename.Value = value;
            }
        }

        Reference _Reference;
    }

    delegate DiagramAnchor SignalDelegate(ReferenceSignal refence);

    public partial class DiagramReference : DiagramNode
    {
        public DiagramReference(Reference source)
            : base(source)
        {
            InitializeComponent();

            DiagramAnchorResultTransform anchor = new DiagramAnchorResultTransform("Reference", source.Source);
            source.Source.Tag = anchor;
            anchor.Container = this;

            Anchors.Children.Add(anchor);

            AddAnchors(source);

            source.ReferenceSignals.CollectionChanged += new NotifyCollectionChangedEventHandler(ReferenceSignals_CollectionChanged);
            source.ReferenceRequests.CollectionChanged += new NotifyCollectionChangedEventHandler(ReferenceRequests_CollectionChanged);
            source.ReferenceFlags.CollectionChanged += new NotifyCollectionChangedEventHandler(ReferenceFlags_CollectionChanged);

            Unloaded += new RoutedEventHandler(DiagramReference_Unloaded);
        }

        void AddAnchors(Reference source)
        {
            if (source.ReferencedDatabase != null)
            {
                foreach (ReferenceSignal signal in source.ReferenceSignals)
                {
                    DiagramAnchor anchor = _SignalLookup[signal.GetType()](signal);
                    anchor.Container = this;
                    Anchors.Children.Add(anchor);
                }
            }
        }

        void DiagramReference_Unloaded(object sender, RoutedEventArgs e)
        {
            if (Source != null)
            {
                (Source as Reference).ReferenceSignals.CollectionChanged -= new NotifyCollectionChangedEventHandler(ReferenceSignals_CollectionChanged);
                (Source as Reference).ReferenceRequests.CollectionChanged -= new NotifyCollectionChangedEventHandler(ReferenceRequests_CollectionChanged);
                (Source as Reference).ReferenceFlags.CollectionChanged -= new NotifyCollectionChangedEventHandler(ReferenceFlags_CollectionChanged);
            }
        }

        void ReferenceSignals_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ReferenceSignal signal in e.NewItems)
                    {
                        //if (signal.HasInputAnchor)
                        {
                            DiagramAnchor anchor = _SignalLookup[signal.GetType()](signal);
                            anchor.Container = this;
//                             Anchors.Children.Insert(e.NewStartingIndex + 1, anchor);
                            Anchors.Children.Add(anchor);
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    Motiontree parent = Source.Database.Find(Source.Parent) as Motiontree;

                    foreach (ReferenceSignal signal in e.OldItems)
                    {
                        foreach (DiagramAnchor anchor in Anchors.Children)
                        {
                            if (signal == anchor.Anchor)
                            {
                                Anchors.Children.Remove(anchor);
                                break;
                            }
                        }
                    }
                    break;
            }
        }

        void ReferenceRequests_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            
        }

        void ReferenceFlags_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            
        }

        static Dictionary<Type, SignalDelegate> _SignalLookup = new Dictionary<Type, SignalDelegate>
        {
            {
                typeof(AnimationReference), delegate(ReferenceSignal signal)
                {
                    ReferenceAnimationAnchor anchor = new ReferenceAnimationAnchor(signal);
                    return anchor;
                } 
            },
            {
                typeof(BooleanReference), delegate(ReferenceSignal signal)
                {
                    ReferenceBooleanAnchor anchor = new ReferenceBooleanAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(ClipReference), delegate(ReferenceSignal signal)
                {
                    ReferenceClipAnchor anchor = new ReferenceClipAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(ExpressionReference), delegate(ReferenceSignal signal)
                {
                    ReferenceExpressionAnchor anchor = new ReferenceExpressionAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(FilterReference), delegate(ReferenceSignal signal)
                {
                    ReferenceFilterAnchor anchor = new ReferenceFilterAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(FrameReference), delegate(ReferenceSignal signal)
                {
                    ReferenceFrameAnchor anchor = new ReferenceFrameAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(HashStringReference), delegate(ReferenceSignal signal)
                {
                    ReferenceFrameAnchor anchor = new ReferenceFrameAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(IntReference), delegate(ReferenceSignal signal)
                {
                    ReferenceNodeAnchor anchor = new ReferenceNodeAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(NodeReference), delegate(ReferenceSignal signal)
                {
                    ReferenceNodeAnchor anchor = new ReferenceNodeAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(ParameterizedMotionReference), delegate(ReferenceSignal signal)
                {
                    ReferenceParameterizedMotionAnchor anchor = new ReferenceParameterizedMotionAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(RealReference), delegate(ReferenceSignal signal)
                {
                    ReferenceRealAnchor anchor = new ReferenceRealAnchor(signal);
                    return anchor;
                }
            },
            {
                typeof(TagReference), delegate(ReferenceSignal signal)
                {
                    ReferenceRealAnchor anchor = new ReferenceRealAnchor(signal);
                    return anchor;
                }
            },
        };

        protected override void OnPreviewMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDoubleClick(e);

            if (e.ChangedButton == MouseButton.Left)
            {
                Reference reference = Source as Reference;
                if (reference.ReferencedDatabase != null)
                {
                    NavigationCommands.GoToPage.Execute(reference.ReferencedDatabase, this);
                }

                e.Handled = true;
            }
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete node";
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);

            contextMenu.Items.Add(deleteItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;
            contextMenu.Items.Clear();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }
    }
}
