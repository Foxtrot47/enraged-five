﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using New.Move.Core;

namespace Rockstar.MoVE.Design
{
    public class DiagramConnection : Control, INotifyPropertyChanged
    {
        public static readonly DependencyProperty IsProxyProperty =
            DependencyProperty.Register("IsProxy", typeof(bool), typeof(DiagramConnection));

        public bool IsProxy
        {
            get { return (bool)GetValue(IsProxyProperty); }
            set { SetValue(IsProxyProperty, value); }
        }

        public DiagramConnection(IDiagramAnchor sender, IDiagramAnchor receiver, Connection connection)
        {
            Id = Guid.NewGuid();

            Unloaded += new RoutedEventHandler(DiagramConnection_Unloaded);

            try {
                DiagramNode parent = sender.Container;
                parent.PositionChanged += new RoutedEventHandler(DiagramNode_PositionChanged);
                parent.SizeChanged += new SizeChangedEventHandler(DiagramNode_SizeChanged);
            }
            catch { }

            Sender = sender;
            Sender.Loaded += new RoutedEventHandler(DiagramAnchor_Loaded);

            try
            {
                DiagramNode parent = receiver.Container;
                parent.PositionChanged += new RoutedEventHandler(DiagramNode_PositionChanged);
                parent.SizeChanged += new SizeChangedEventHandler(DiagramNode_SizeChanged);
            }
            catch { }

            SelectionBehaviour.SetIsSelectionEnabled(this, true);

            Receiver = receiver;
            Receiver.Loaded += new RoutedEventHandler(DiagramAnchor_Loaded);

            Connection = connection;
            Tag = connection;

            Connection.IsSelectedChanged +=
                new EventHandler<IsSelectedChangedEventArgs>(Connection_IsSelectedChanged);

            IsProxy = connection.IsProxy;

            Connection.IsProxyChanged += new EventHandler<ConnectionProxyEventArgs>(Connection_IsProxyChanged);
        }

        void Connection_IsProxyChanged(object sender, ConnectionProxyEventArgs e)
        {
            IsProxy = e.IsProxy;
        }

        void DiagramConnection_Unloaded(object sender, RoutedEventArgs e)
        {
            Connection.IsSelectedChanged -=
                new EventHandler<IsSelectedChangedEventArgs>(Connection_IsSelectedChanged);

            Sender.Loaded -= new RoutedEventHandler(DiagramAnchor_Loaded);
            try
            {
                DiagramNode parent = Sender.Container;
                parent.PositionChanged -= new RoutedEventHandler(DiagramNode_PositionChanged);
                parent.SizeChanged -= new SizeChangedEventHandler(DiagramNode_SizeChanged);

            }
            catch { }

            Receiver.Loaded -= new RoutedEventHandler(DiagramAnchor_Loaded);
            try
            {
                DiagramNode parent = Receiver.Container;
                parent.PositionChanged -= new RoutedEventHandler(DiagramNode_PositionChanged);
                parent.SizeChanged -= new SizeChangedEventHandler(DiagramNode_SizeChanged);

            }
            catch { }

            Connection.IsProxyChanged -= new EventHandler<ConnectionProxyEventArgs>(Connection_IsProxyChanged);

            Connection.Tag = null;
        }

        void Connection_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            switch (e.Action)
            {
                case SelectionChangedAction.Add:
                    SetValue(SelectionBehaviour.IsSelectedProperty, true);
                    break;
                case SelectionChangedAction.Remove:
                    SetValue(SelectionBehaviour.IsSelectedProperty, false);
                    break;
            }
        }

        void DiagramAnchor_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateGeometry();
        }

        public Guid Id { get; private set; }

        public Connection Connection { get; private set; }

        PathGeometry _Geometry;
        public PathGeometry Geometry
        {
            get { return _Geometry; }
            set
            {
                if (_Geometry != value)
                {
                    _Geometry = value;
                    OnPropertyChanged("Geometry");
                }
            }
        }

        IDiagramAnchor _Sender;
        public IDiagramAnchor Sender
        {
            get { return _Sender; }
            set
            {
                if (_Sender != value)
                {
                    _Sender = value;
                    UpdateGeometry();
                }
            }
        }

        IDiagramAnchor _Receiver;
        public IDiagramAnchor Receiver
        {
            get { return _Receiver; }
            set
            {
                if (_Receiver != value)
                {
                    _Receiver = value;
                    UpdateGeometry();
                }
            }
        }

        void DiagramNode_PositionChanged(object sender, EventArgs e)
        {
            UpdateGeometry();
        }

        void DiagramNode_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateGeometry();
        }

        void UpdateGeometry()
        {
            if (_Sender != null && _Sender.Sender.HasValue && _Receiver != null && _Receiver.Receiver.HasValue)
            {
                PathGeometry geometry = new PathGeometry();

                Point start = new Point(0, 0);

                Point end = new Point(
                    _Receiver.Receiver.Value.X - _Sender.Sender.Value.X,
                    _Receiver.Receiver.Value.Y - _Sender.Sender.Value.Y);

                if (_Receiver.Receiver.Value.Y < _Sender.Sender.Value.Y)
                {
                    double delta = _Sender.Sender.Value.Y - _Receiver.Receiver.Value.Y;

                    start.Y += delta;
                    end.Y += delta;
                }

                if (_Sender.Sender.Value.X < _Receiver.Receiver.Value.X)
                {
                    PathFigure figure = new PathFigure();
                    figure.StartPoint = start;

                    PointCollection points = new PointCollection();
                    points.Add(new Point(end.X / 2, start.Y));
                    points.Add(new Point(end.X / 2, end.Y));
                    points.Add(end);

                    figure.Segments.Add(new PolyBezierSegment(points, true));
                    geometry.Figures.Add(figure);
                }
                else
                {
                    double delta = _Sender.Sender.Value.X - _Receiver.Receiver.Value.X;

                    start.X += delta;
                    end.X += delta;

                    Point[] points = new Point[4];
                    points[0] = start;

                    if (end.Y < start.Y)
                    {    
                        points[1] = new Point(start.X, 3 * ((start.Y - end.Y) / 4));
                        points[2] = new Point(end.X, (start.Y - end.Y) / 4);
                    }
                    else
                    {
                        points[1] = new Point(start.X, (end.Y - start.Y) / 4);
                        points[2] = new Point(end.X, 3 * ((end.Y - start.Y) / 4));
                    }

                    points[3] = end;

                    Point[] cp1, cp2;
                    BezierSpline.GetCurveControlPoints(points, out cp1, out cp2);

                    PathSegmentCollection curves = new PathSegmentCollection();
                    for (int i = 0; i < cp1.Length; ++i)
                    {
                        curves.Add(new BezierSegment(cp1[i], cp2[i], points[i + 1], true));
                    }

                    PathFigure f = new PathFigure(points[0], curves, false);
                    geometry = new PathGeometry(new PathFigure[] { f });
                }

                Geometry = geometry;
            }
        }

        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}