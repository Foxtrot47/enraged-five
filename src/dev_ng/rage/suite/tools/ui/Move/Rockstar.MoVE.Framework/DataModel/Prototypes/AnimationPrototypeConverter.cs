﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(AnimationPrototype), typeof(PropertyCollection))]
    internal class AnimationPrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            AnimationPrototype prototype = value as AnimationPrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Animation"] = prototype.Animation;
            properties[Resources.Phase] = prototype.Phase;
            properties[Resources.Rate] = prototype.Rate;
            properties[Resources.Delta] = prototype.Delta;
            properties[Resources.Looped] = prototype.Looped;
            properties["Absolute"] = prototype.Absolute;

            PropertyCategory events = new PropertyCategory();
            collection[Resources.Events] = events;

            events[Resources.Looped] = prototype.AnimationLooped;
            events[Resources.Ended] = prototype.AnimationEnded;

            return collection;
        }
    }
}