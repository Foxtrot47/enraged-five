﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(FramePrototype), typeof(PropertyCollection))]
    internal class FramePrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            FramePrototype prototype = value as FramePrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Frame"] = prototype.Frame;

            return collection;
        }
    }
}