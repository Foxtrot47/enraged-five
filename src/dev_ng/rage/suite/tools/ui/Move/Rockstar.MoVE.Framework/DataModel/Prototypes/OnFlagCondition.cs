﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml;
using New.Move.Core;
using RSG.Base.Logging;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    /// <summary>
    /// Represents a condition that can be placed onto a transition that is valid when a
    /// associated flag is set.
    /// </summary>
    [Serializable]
    public class OnFlagCondition : ConditionBase, ISerializable
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Flag"/> property.
        /// </summary>
        private const string FlagSerialisationTag = "Flag";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="NotSet"/> property.
        /// </summary>
        private const string NotSetSerialisationTag = "NotSet";

        /// <summary>
        /// The private field used for the <see cref="Event"/> property.
        /// </summary>
        private FlagConditionProperty flagProperty;

        /// <summary>
        /// The private field used for the <see cref="NotSet"/> property.
        /// </summary>
        private BooleanConditionProperty notSet;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OnFlagCondition"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the condition.
        /// </param>
        public OnFlagCondition(string name)
            : base(name)
        {
            this.flagProperty = new FlagConditionProperty();
            this.notSet = false;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OnFlagCondition"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public OnFlagCondition(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.notSet = new BooleanConditionProperty();
            this.flagProperty = new FlagConditionProperty();
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case FlagSerialisationTag:
                        this.flagProperty = (FlagConditionProperty)entry.Value;
                        break;
                    case NotSetSerialisationTag:
                        this.notSet = (BooleanConditionProperty)entry.Value;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OnFlagCondition"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="database">
        /// The root database the condition belongs to.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public OnFlagCondition(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Deserialise(reader, database);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the flag property for this condition.
        /// </summary>
        public FlagConditionProperty Flag
        {
            get { return this.flagProperty; }
        }

        /// <summary>
        /// Gets the not set property for this condition.
        /// </summary>
        public BooleanConditionProperty NotSet
        {
            get { return this.notSet; }
        }

        /// <summary>
        /// Gets the type descriptor that describes the condition.
        /// </summary>
        public override Type Descriptor
        {
            get { return typeof(OnFlagDescriptor); }
        }

        /// <summary>
        /// Gets the xml export type name for this condition.
        /// </summary>
        public override string XMLExportTypeName
        {
            get { return "OnFlag"; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.GetSerializationData(info, context);

            info.AddValue(FlagSerialisationTag, this.Flag);
            info.AddValue(NotSetSerialisationTag, this.NotSet);
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <param name="parent">
        /// The parent for the cloned object.
        /// </param>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        public override ICondition Clone(Condition parent)
        {
            OnFlagCondition clone = new OnFlagCondition(this.Name);
            clone.notSet = new BooleanConditionProperty(this.NotSet);
            clone.flagProperty = new FlagConditionProperty(this.Flag);
            clone.NotSet.Parent = parent;
            clone.Flag.Parent = parent;
            return clone;
        }

        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        public override void ExportToXML(PargenXmlNode parentNode)
        {
            parentNode.AppendTextElementNode("Parameter", this.Flag.Value.Name);
            parentNode.AppendValueElementNode(
                "NotSet", "value", this.NotSet.Value ? "true" : "false");
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("flag");
            writer.WriteAttributeString(ConditionBase.NameSerialisationTag, this.Name);

            if (this.NotSet != null)
            {
                writer.WriteAttributeString(
                    NotSetSerialisationTag,
                    this.NotSet.Value.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                writer.WriteAttributeString(NotSetSerialisationTag, "false");
            }

            if (this.Flag != null && this.Flag.Value != null)
            {
                writer.WriteAttributeString(
                    FlagSerialisationTag,
                    this.Flag.Value.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="database">
        /// The root database this condition belongs to.
        /// </param>
        private void Deserialise(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute(ConditionBase.NameSerialisationTag);
            this.notSet = bool.Parse(reader.GetAttribute(NotSetSerialisationTag));

            string flagId = reader.GetAttribute(FlagSerialisationTag);
            this.flagProperty = new FlagConditionProperty();
            if (flagId != null)
            {
                this.Flag.Value = database.GetFlag(Guid.ParseExact(flagId, "D"));
                if (this.Flag.Value == null)
                {
                    string flagName = reader.GetAttribute("FlagName");
                    this.Flag.Value = database.GetFlag(flagName);
                    if (this.Flag.Value == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to flag with id '{0}'. This needs fixing before a export is allowed.", flagId);
                    }
                }
            }

            reader.Skip();
        }
        #endregion
    } // New.Move.Core.OnFlagCondition {Interface}
} // New.Move.Core {Namespace}
