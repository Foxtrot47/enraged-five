﻿using System;

namespace Rockstar.MoVE.Framework
{
    public interface IDescriptorMetadata
    {
        int Index { get; }
    }
}