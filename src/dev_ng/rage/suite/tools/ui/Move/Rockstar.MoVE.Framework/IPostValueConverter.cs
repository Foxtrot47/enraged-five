﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rockstar.MoVE.Framework
{
    public interface IPostValueConverter
    {
        void PostConvert(object value, object newValue);
    }
}
