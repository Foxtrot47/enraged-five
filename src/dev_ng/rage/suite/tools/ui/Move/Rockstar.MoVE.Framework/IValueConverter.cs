﻿using System;

namespace Rockstar.MoVE.Framework
{
    public interface IValueConverter
    {
        object Convert(object value);
    }
}