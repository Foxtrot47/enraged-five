﻿using System;
using System.ComponentModel.Composition;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Converters
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(StateMachine), typeof(PropertyCollection))]
    internal class StateMachineConverter : IValueConverter
    {
        public object Convert(object value)
        {
            StateMachine prototype = value as StateMachine;

            PropertyCategory properties = new PropertyCategory();
            PropertyCollection collection = new PropertyCollection();
            PropertyCategory transitions = new PropertyCategory();
            collection["Properties"] = properties;
            collection["Transitions"] = transitions;

            transitions["Order"] = prototype.TransitionOrder;

            PropertyCategory events = new PropertyCategory();
            collection[Resources.Events] = events;

            events["Enter"] = prototype.EnterEvent;
            events["Exit"] = prototype.ExitEvent;
            properties["Defer Block Update"] = prototype.DeferBlockUpdate;

            return collection;
        }
    }
}