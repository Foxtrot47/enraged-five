﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using Move.Core.Restorable;

using RSG.ManagedRage.ClipAnimation;
using System.Globalization;
using System.Windows;
using RSG.AnimClipEditor.ViewModel;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    public class ClipPrototype : Logic, IFileAssociated
    {
        public ClipLogicalProperty Clip
        {
            get;
            set;
        }

        public RealLogicalProperty Phase
        {
            get;
            private set;
        }

        public RealLogicalProperty Rate
        {
            get;
            private set;
        }

        public RealLogicalProperty Delta
        {
            get;
            private set;
        }

        public BooleanLogicalProperty Looped
        {
            get;
            private set;
        }

        public BooleanLogicalProperty ZeroWeightSync
        {
            get;
            private set;
        }

        public LogicalEventProperty ClipLooped
        {
            get;
            private set;
        }

        public LogicalEventProperty ClipEnded
        {
            get;
            private set;
        }

        public ClipPropertyLogicalPropertyArray ClipProperties
        {
            get;
            private set;
        }

        public TagEventToSignalLogicalPropertyArray TagEventToSignalProperties
        {
            get;
            private set;
        }

        public ResultTransform Result
        {
            get;
            private set;
        }

        public MClip ManagedClip
        {
            get;
            private set;
        }

        public ClipViewModel ClipViewModel { get; set; }

        public SyncEvent Value { get; set; }
        public string SubType { get; set; }

        public void SetAssociatedFilename(string filename)
        {
            Clip.Input = PropertyInput.Filename;
            Clip.Filename = filename;
            Clip.Source = ClipSource.LocalFile;
            Name = System.IO.Path.GetFileNameWithoutExtension(filename);
        }

        public ClipPrototype(IDesc desc)
            : base(desc)
        {
            Clip = new ClipLogicalProperty(this);
            Phase = 0.0f;
            Rate = 1.0f;
            Delta = 0.0f;
            Looped = true;
            ZeroWeightSync = false;
            
            ClipLooped = new LogicalEventProperty();
            ClipEnded = new LogicalEventProperty();

            ClipProperties = new ClipPropertyLogicalPropertyArray(this);
            TagEventToSignalProperties = new TagEventToSignalLogicalPropertyArray(this);
           
            Result = new ResultTransform(this);

            InitManagedClip();
        }

        bool _ManagedClipInit = false;
        public void InitManagedClip()
        {
            if (!_ManagedClipInit)
            {
                MClip.Init();

                if (File.Exists(Clip.Filename))
                {
                    if (Path.GetExtension(Clip.Filename) == ".clipxml")
                    {
                        ManagedClip = new MClip(MClip.ClipType.XML);
                    }
                    else if (Path.GetExtension(Clip.Filename) == ".clip")
                    {
                        ManagedClip = new MClip(MClip.ClipType.Normal);
                    }

                    ManagedClip.Load(Clip.Filename);
                }
                else
                {
                    ManagedClip = new MClip(MClip.ClipType.Normal);
                }

                Clip.PropertyChanged += new PropertyChangedEventHandler(ClipFileName_Changed);
                _ManagedClipInit = true;
            }
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "ClipNode");

            itemNode.AppendTextElementNode("Name", Name);

            Clip.ExportToXML(itemNode.AppendChild("Clip"), motiontreeParent, hashedNames);
            Phase.ExportToXML(itemNode.AppendChild("Phase"), motiontreeParent);
            Rate.ExportToXML(itemNode.AppendChild("Rate"), motiontreeParent);
            Delta.ExportToXML(itemNode.AppendChild("Delta"), motiontreeParent);
            Looped.ExportToXML(itemNode.AppendChild("Looped"), motiontreeParent);
            ZeroWeightSync.ExportToXML(itemNode.AppendChild("ZeroWeightSync"), motiontreeParent);
            ClipLooped.ExportToXML(itemNode.AppendChild("ClipLooped"), motiontreeParent);
            ClipEnded.ExportToXML(itemNode.AppendChild("ClipEnded"), motiontreeParent);

            var clipEventsNode = itemNode.AppendChild("TagEvents");
            foreach (var clipEventToSignalProperty in TagEventToSignalProperties.Items)
            {
                var clipEventItemNode = clipEventsNode.AppendChild("Item");
                clipEventItemNode.AppendTextElementNode("TagEvent", clipEventToSignalProperty.TagEventName);
                clipEventItemNode.AppendTextElementNode("Parameter", clipEventToSignalProperty.SignalName);
            }
        }

        public delegate void VariableClipSetChangedDelegate(VariableClipSet oldClipSet);

        protected override void OnLoadCompleteImpl()
        {
            foreach (var clipEventToSignalProperty in TagEventToSignalProperties.Items)
            {
                clipEventToSignalProperty.UpdateSelectionLists();
            }

            if (Clip != null && Clip.VariableClipSet != null)
            {
                foreach (VariableClipSet clipset in Database.VariableClipSets)
                {
                    if (clipset == Clip.VariableClipSet)
                    {
                        clipset.SetUsed(true);
                    }
                }
            }

            Clip.VariableClipSetChanged = new ClipLogicalProperty.VariableClipSetChangedDelegate(VariableClipSetChanged);
        }

        protected override void OnSelectionChanged(bool selected)
        {
            if (this.ClipViewModel == null || this.Clip.Source != ClipSource.LocalFile)
            {
                return;
            }

            if (selected)
            {

            }
            else
            {
                //this.ClipViewModel.Disconnect(null);
            }
        }

        public void VariableClipSetChanged(VariableClipSet oldClipSet)
        {
            if (oldClipSet != Clip.VariableClipSet)
            {
                foreach (VariableClipSet clipset in Database.VariableClipSets)
                {
                    if (clipset == Clip.VariableClipSet)
                    {
                        clipset.SetUsed(true);
                    }
                    else if (clipset == oldClipSet)
                    {
                        clipset.SetUsed(false);
                    }
                }
            }
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new ClipPrototype(new ClipDescriptor());
            clone.AdditionalText = this.AdditionalText;
            clone.Database = this.Database;
            clone.Enabled = this.Enabled;
            clone.Name = this.Name;
            clone.Parent = this.Parent;
            clone.Position = this.Position;

            (clone as ClipPrototype).TagEventToSignalProperties.Parent = (clone as ClipPrototype);
            (clone as ClipPrototype).ClipProperties.Parent = (clone as ClipPrototype);
            (clone as ClipPrototype).Phase = new RealLogicalProperty((clone as ClipPrototype), Phase);
            (clone as ClipPrototype).Rate = new RealLogicalProperty((clone as ClipPrototype), Rate);
            (clone as ClipPrototype).Delta = new RealLogicalProperty((clone as ClipPrototype), Delta);
            (clone as ClipPrototype).Looped = new BooleanLogicalProperty(Looped);
            (clone as ClipPrototype).Looped.Parent = clone;
            (clone as ClipPrototype).ZeroWeightSync = new BooleanLogicalProperty(ZeroWeightSync);
            (clone as ClipPrototype).ZeroWeightSync.Parent = clone;
            (clone as ClipPrototype).ClipLooped = new LogicalEventProperty(ClipLooped);
            (clone as ClipPrototype).ClipEnded = new LogicalEventProperty(ClipEnded);

            (clone as ClipPrototype).Clip.PropertyChanged -= new PropertyChangedEventHandler(ClipFileName_Changed);
            (clone as ClipPrototype).Clip = new ClipLogicalProperty(Clip);
            (clone as ClipPrototype).Clip.Parent = clone;
            (clone as ClipPrototype).Clip.PropertyChanged += new PropertyChangedEventHandler(ClipFileName_Changed);
            (clone as ClipPrototype).ClipFileName_Changed(Clip, new PropertyChangedEventArgs("Filename"));

            (clone as ClipPrototype).SubType = this.SubType;

            foreach (ClipPropertyLogicalProperty item in this.ClipProperties.Items)
            {
                (clone as ClipPrototype).ClipProperties.Items.Add(new ClipPropertyLogicalProperty(item));
            }

            foreach (TagEventToSignalLogicalProperty item in this.TagEventToSignalProperties.Items)
            {
                TagEventToSignalLogicalProperty newItem = new TagEventToSignalLogicalProperty(item);
                (clone as ClipPrototype).TagEventToSignalProperties.Items.Add(newItem);
            }

            (clone as ClipPrototype).OnLoadCompleteImpl();

            return (ISelectable)clone;
        }

        #region Events
        void ClipFileName_Changed(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Filename")
            {
                if (Clip.Filename != null && File.Exists(Clip.Filename))
                {
                    if (ManagedClip == null)
                    {
                        if (Path.GetExtension(Clip.Filename) == ".clipxml")
                        {
                            ManagedClip = new MClip(MClip.ClipType.XML);
                        }
                        else if (Path.GetExtension(Clip.Filename) == ".clip")
                        {
                            ManagedClip = new MClip(MClip.ClipType.Normal);
                        }
                    }
                    
                    ManagedClip.Load(Clip.Filename);
                }
                else
                {
                    ManagedClip.Unload();
                }
            }
            else if (e.PropertyName == "Source")
            {
                FireClipSourceChangedEvent();
            }
        }

        public event EventHandler ClipSourceChanged;
        public void FireClipSourceChangedEvent()
        {
            if (this.ClipSourceChanged != null)
                this.ClipSourceChanged(this, EventArgs.Empty);
        }
        #endregion // Events

        /// <summary>
        /// Initialises a new instance of the <see cref="ClipPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public ClipPrototype(XmlReader reader, Database database, Guid parent)
            : this(new ClipDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Clip");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Clip != null)
            {
                writer.WriteStartElement("Clip");
                this.Clip.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Phase != null)
            {
                writer.WriteStartElement("Phase");
                this.Phase.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Rate != null)
            {
                writer.WriteStartElement("Rate");
                this.Rate.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Delta != null)
            {
                writer.WriteStartElement("Delta");
                this.Delta.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Looped != null)
            {
                writer.WriteStartElement("Looped");
                this.Looped.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ZeroWeightSync != null)
            {
                writer.WriteStartElement("ZeroWeightSync");
                this.ZeroWeightSync.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ClipLooped != null)
            {
                writer.WriteStartElement("ClipLooped");
                this.ClipLooped.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ClipEnded != null)
            {
                writer.WriteStartElement("ClipEnded");
                this.ClipEnded.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ClipProperties != null)
            {
                writer.WriteStartElement("ClipProperties");
                foreach (ClipPropertyLogicalProperty item in this.ClipProperties.Items)
                {
                    writer.WriteStartElement("ClipPropertyLogicalProperty");
                    item.Serialise(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }

            if (this.TagEventToSignalProperties != null)
            {
                writer.WriteStartElement("TagEventToSignalProperties");
                foreach (TagEventToSignalLogicalProperty item in this.TagEventToSignalProperties.Items)
                {
                    writer.WriteStartElement("TagEventToSignalLogicalProperty");
                    item.Serialise(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            
            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Clip") == 0)
                {
                    this.Clip = new ClipLogicalProperty(reader, this.Database, this);
                    this.InitManagedClip();
                    this.Clip.PropertyChanged += new PropertyChangedEventHandler(ClipFileName_Changed);
                    this.ClipFileName_Changed(Clip, new PropertyChangedEventArgs("Filename"));
                }
                else if (string.CompareOrdinal(reader.Name, "Phase") == 0)
                {
                    this.Phase = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Rate") == 0)
                {
                    this.Rate = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Delta") == 0)
                {
                    this.Delta = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Looped") == 0)
                {
                    this.Looped = new BooleanLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "ZeroWeightSync") == 0)
                {
                    this.ZeroWeightSync = new BooleanLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "ClipLooped") == 0)
                {
                    this.ClipLooped = new LogicalEventProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "ClipEnded") == 0)
                {
                    this.ClipEnded = new LogicalEventProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "ClipProperties") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            this.ClipProperties.Items.Add(new ClipPropertyLogicalProperty(reader, this.Database, this));
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, "TagEventToSignalProperties") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            this.TagEventToSignalProperties.Items.Add(new TagEventToSignalLogicalProperty(reader, this));
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new ResultTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Clip.Id);
            ids.Add(this.ClipEnded.Id);
            ids.Add(this.ClipLooped.Id);
            ids.Add(this.Delta.Id);
            ids.Add(this.Looped.Id);
            ids.Add(this.Phase.Id);
            ids.Add(this.Rate.Id);
            ids.Add(this.Result.Id);
            ids.Add(this.ClipProperties.Id);
            foreach (ClipPropertyLogicalProperty item in this.ClipProperties.Items)
            {
                ids.AddRange(item.GetAllGuids());
            }

            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Clip.Id = New.Move.Core.Database.GetId(map, this.Clip.Id);
            this.ClipEnded.Id = New.Move.Core.Database.GetId(map, this.ClipEnded.Id);
            this.ClipLooped.Id = New.Move.Core.Database.GetId(map, this.ClipLooped.Id);
            this.Delta.Id = New.Move.Core.Database.GetId(map, this.Delta.Id);
            this.Looped.Id = New.Move.Core.Database.GetId(map, this.Looped.Id);
            this.Phase.Id = New.Move.Core.Database.GetId(map, this.Phase.Id);
            this.Rate.Id = New.Move.Core.Database.GetId(map, this.Rate.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
            foreach (ClipPropertyLogicalProperty item in this.ClipProperties.Items)
            {
                item.ResetIds(map);
            }
        }
    }
}
