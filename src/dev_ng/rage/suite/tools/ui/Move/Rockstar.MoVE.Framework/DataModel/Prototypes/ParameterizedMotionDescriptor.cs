﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("MotionTreeDescriptor", typeof(IDesc)), DescriptorMetadata((int)DescriptorGroup.Other)]
    public class ParameterizedMotionDescriptor : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Parameterized Motion";
            public static int Id = 17;
            public static string Help = "Supplies parameterized motion data";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(ParameterizedMotionPrototype); } }

        public ParameterizedMotionDescriptor()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource =
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Rockstar.MoVE.Framework.Media.ParameterizedMotion.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new ParameterizedMotionPrototype(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}