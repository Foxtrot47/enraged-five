﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(NWayAddPrototype), typeof(PropertyCollection))]
    internal class NWayAddPrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            NWayAddPrototype prototype = value as NWayAddPrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Inputs"] = prototype.Inputs;
            properties["Filter"] = prototype.Filter;
            properties["Sync"] = prototype.Sync;
            properties["Zero As Dest"] = prototype.ZeroAsDest;

            return collection;
        }
    }
}