﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Xml;

using New.Move.Core;
using Move.Core;
using Move.Utils;

using RSG.TrackViewer.Data;
using RSG.TrackViewer.ViewModel;
using RSG.TrackViewer;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Serializable]
    public class CurvePrototype : Logic, IOperator
    {
        public RealInputAnchor Input { get; private set; }

        public CurvePrototype(IDesc desc)
            : base(desc)
        {
            Input = new RealInputAnchor(this);
            Output = new RealOutputAnchor(this);
            TrackGroup = new TrackGroup("Curve");

            Track track = new Track("Output"); 
            TrackPoint start = new FloatTrackPoint(0.0, 0.0);

            start.IsLockedX = true;
            track.AddPoint(start);

            TrackPoint end = new FloatTrackPoint(1.0, 0.0);

            end.IsLockedX = true;
            track.AddPoint(end);

            TrackGroup.Add(track);

            MinimumInputValue = 0.0f;
            MaximumInputValue = 1.0f;
            MinimumOutputValue = 0.0f;
            MaximumOutputValue = 1.0f;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CurvePrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public CurvePrototype(XmlReader reader, Database database, Guid parent)
            : this(new CurveDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Curves.Clear();
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        public CurvePrototype(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            //  Set the description up
            this.Desc = new CurveDescriptor();
            //  Set the data here, the rest is handled by the base class
            this.Input = (RealInputAnchor)info.GetValue("Input", typeof(RealInputAnchor));
            this.Output = (RealOutputAnchor)info.GetValue("Output", typeof(RealOutputAnchor));
            this.TrackGroup = (TrackGroup)info.GetValue("TrackGroup", typeof(TrackGroup));
            this.Curves = (CurveCollection)info.GetValue("Curves", typeof(CurveCollection));
            this.Tag = (object)info.GetValue("Tag", typeof(object));
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Curve");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Input != null)
            {
                writer.WriteStartElement("Input");
                this.Input.Serialise(writer);                
                writer.WriteEndElement();
            }

            if (this.Output != null)
            {
                writer.WriteStartElement("Output");
                this.Output.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.TrackGroup != null)
            {
                this.TrackGroup.Serialise(writer);
            }

            writer.WriteStartElement("InputClamps");
            writer.WriteAttributeString(
                "min", this.MinimumInputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "max", this.MaximumInputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteEndElement();

            writer.WriteStartElement("OutputClamps");
            writer.WriteAttributeString(
                "min", this.MinimumOutputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "max", this.MaximumOutputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteEndElement();

            if (this.Curves != null)
            {
                writer.WriteStartElement("Curves");
                foreach (Curve curve in this.Curves)
                {
                    curve.Serialise(writer);
                }
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Input") == 0)
                {
                    this.Input = new RealInputAnchor(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Output") == 0)
                {
                    this.Output = new RealOutputAnchor(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "TrackGroup") == 0)
                {
                    this.TrackGroup = new TrackGroup(reader);
                }
                else if (string.CompareOrdinal(reader.Name, "InputClamps") == 0)
                {
                    this.MinimumInputValue = double.Parse(reader.GetAttribute("min"));
                    this.MaximumInputValue = double.Parse(reader.GetAttribute("max"));
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "OutputClamps") == 0)
                {
                    this.MinimumOutputValue = double.Parse(reader.GetAttribute("min"));
                    this.MaximumOutputValue = double.Parse(reader.GetAttribute("max"));
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Curves") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            this.Curves.Add(new Curve(reader));
                            reader.Skip();
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Read();
                }
            }


            reader.Skip();
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //  Set the data for this class
            info.AddValue("Input", this.Input);
            info.AddValue("Output", this.Output);
            info.AddValue("TrackGroup", this.TrackGroup);
            info.AddValue("Curves", this.Curves);
            info.AddValue("Tag", this.Tag);
            //  The rest is set in the base class
            base.GetObjectData(info, context);
        }

        public List<IAnchor> Inputs
        {
            get
            {
                List<IAnchor> inputs = new List<IAnchor>();
                inputs.Add(Input);

                return inputs;
            }
            set
            {
                Inputs.Clear();
                foreach(IAnchor input in value)
                {
                    Inputs.Add(input);
                }

            }
        }

        double _MinimumInputValue;
        public double MinimumInputValue
        {
            get
            {
                return _MinimumInputValue;
            }
            set
            {
                foreach (Track track in TrackGroup.Tracks)
                {
                    track.ClampMinimum = value;
                }

                _MinimumInputValue = value;
            }
        }

        double _MaximumInputValue;
        public double MaximumInputValue 
        {
            get
            {
                return _MaximumInputValue;
            }
            set
            {
                foreach (Track track in TrackGroup.Tracks)
                {
                    track.ClampMaximum = value;
                }

                _MaximumInputValue = value;
            }
        }

        double _MinimumOutputValue;
        public double MinimumOutputValue 
        {
            get
            {
                return _MinimumOutputValue;
            }
            set
            {
                foreach (Track track in TrackGroup.Tracks)
                {
                    track.OutputMinimum = value;
                }

                _MinimumOutputValue = value;
            }
        }

        double _MaximumOutputValue;
        public double MaximumOutputValue
        {
            get
            {
                return _MaximumOutputValue;
            }
            set
            {
                foreach (Track track in TrackGroup.Tracks)
                {
                    track.OutputMaximum = value;
                }

                _MaximumOutputValue = value;
            }
        }

        CurveCollection _Curves = new CurveCollection();
        public CurveCollection Curves
        {
            get
            {
                return _Curves;
            }
            set
            {
                if (_Curves == null)
                {
                    _Curves = new CurveCollection();
                }
                _Curves.Clear();
                foreach (Curve curve in value)
                {
                    _Curves.Add(curve);
                }
            }
        }

        /// <summary>
        /// A single object containg all of the data that will be rendered on the data layer
        /// </summary>
        public TrackGroup TrackGroup
        {
            get
            {
                return _TrackGroup;
            }
            set
            {
                if (value != _TrackGroup)
                    _TrackGroup = value;
            }
        }
        TrackGroup _TrackGroup;

        public RealOutputAnchor Output { get; private set; }

        public IOperatorItem Item { get { return new TrackStackItem(this.TrackGroup.Tracks[0]); } set { } }

        static Track ConvertCurveToTrack(Curve curve)
        {
            Track track = new Track(curve.Name);
            int idx = 0;
            foreach (CurveSegment cs in curve.Points)
            {
                TrackPoint tp = new FloatTrackPoint(cs.X, cs.Y);
                if (idx == 0 || idx == curve.Points.Length - 1)
                    tp.IsLockedX = true;
                track.AddPoint(tp);
                idx++;
            }
            return track;
        }

        public override void GetCustomData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("MinimumInputValue", _MinimumInputValue);
            info.AddValue("MaximumInputValue", _MaximumInputValue);
            info.AddValue("MinimumOutputValue", _MinimumOutputValue);
            info.AddValue("MaximumOutputValue", _MaximumOutputValue);
            info.AddValue("TrackGroup", TrackGroup);
        }

        public override void SetCustomData(SerializationInfo info, StreamingContext context)
        {
            ObservableCollection<Curve> tempCurves = (ObservableCollection<Curve>)info.GetValue("Curves", typeof(ObservableCollection<Curve>));
            _Curves = new CurveCollection(tempCurves);
            try
            {
                TrackGroup = (TrackGroup)info.GetValue("TrackGroup", typeof(TrackGroup));
                foreach (Track t in TrackGroup.Tracks)
                {
                    t.IsActive = true;
                }
            }
            catch (Exception)
            {
                TrackGroup = new TrackGroup("Curve");
                foreach (Curve c in _Curves)
                {
                    TrackGroup.Add(ConvertCurveToTrack(c));
                }
            }
            MinimumInputValue = info.GetDouble("MinimumInputValue");
            MaximumInputValue = info.GetDouble("MaximumInputValue");
            MinimumOutputValue = info.GetDouble("MinimumOutputValue");
            MaximumOutputValue = info.GetDouble("MaximumOutputValue");
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new CurvePrototype(new CurveDescriptor());
            CurvePrototype highClone = (CurvePrototype)base.Clone(clone);

            highClone.Input = new RealInputAnchor(highClone);
            highClone.Output = new RealOutputAnchor(highClone);

            if (this.TrackGroup != null)
            {
                highClone.TrackGroup = new TrackGroup(this.TrackGroup.Name);
                highClone.TrackGroup.RenderLines = this.TrackGroup.RenderLines;
                highClone.TrackGroup.Enabled = this.TrackGroup.Enabled;
                foreach (Track track in this.TrackGroup.Tracks)
                {
                    highClone.TrackGroup.Tracks.Add(new Track(track));
                }
            }

            if (this.Curves != null)
            {
                ObservableCollection<Curve> curves = new ObservableCollection<Curve>();
                foreach (Curve curve in this.Curves)
                {
                    curves.Add(new Curve(curve));
                }
                highClone.Curves = new CurveCollection(curves);
                highClone.Curves.Name = this.Curves.Name;
            }

            highClone.Tag = null;

            return (ISelectable)highClone;
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Input.Id);
            ids.Add(this.Output.Id);

            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Input.Id = New.Move.Core.Database.GetId(map, this.Input.Id);
            this.Output.Id = New.Move.Core.Database.GetId(map, this.Output.Id);
        }

        #region Events
        public void Range_Changed(object sender, EventArgs e)
        {
            WindowedTimeLine timeline = sender as WindowedTimeLine;
            if (timeline.OperationalAxis == Axis.X)
            {
                this.MaximumOutputValue = timeline.DisplayMax;
                this.MinimumOutputValue = timeline.DisplayMin;
            }
            else
            {
                this.MaximumInputValue = timeline.DisplayMax;
                this.MinimumInputValue = timeline.DisplayMin;
            }
        }
        #endregion
    }
}