﻿using System;
using System.Collections.Generic;
using System.Xml;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    /// <summary>
    /// Represents a add node that adds two real inputs and produces a single output.
    /// </summary>
    public class AddOperatorPrototype : Logic, IOperator
    {

        public RealOutputAnchor Result { get; private set; }
        public RealInputAnchor Input0 { get; private set; }
        public RealInputAnchor Input1 { get; private set; }


        public AddOperatorPrototype(IDesc desc)
            : base(desc)
        {
            Result = new RealOutputAnchor(this);
            Input0 = new RealInputAnchor(this);
            Input1 = new RealInputAnchor(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="AddOperatorPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public AddOperatorPrototype(XmlReader reader, Database database, Guid parent)
            : this(new AddOperatorDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        public List<IAnchor> Inputs
        {
            get
            {
                List<IAnchor> inputs = new List<IAnchor>();
                inputs.Add(Input0);
                inputs.Add(Input1);

                return inputs;
            }
        }

        public IOperatorItem Item { get { return new AddOperatorStackItem(); } }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new AddOperatorPrototype(new AddOperatorDescriptor());
            AddOperatorPrototype highClone = (AddOperatorPrototype)base.Clone(clone);
            
            highClone.Result = new RealOutputAnchor(highClone);
            highClone.Input0 = new RealInputAnchor(highClone);
            highClone.Input1 = new RealInputAnchor(highClone);
            highClone.Tag = null;

            return (ISelectable)highClone;
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("addoperation");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }
            if (this.Input0 != null)
            {
                writer.WriteStartElement("Input0");
                this.Input0.Serialise(writer);
                writer.WriteEndElement();
            }
            if (this.Input1 != null)
            {
                writer.WriteStartElement("Input1");
                this.Input1.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new RealOutputAnchor(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Input0") == 0)
                {
                    this.Input0 = new RealInputAnchor(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Input1") == 0)
                {
                    this.Input1 = new RealInputAnchor(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Input0.Id);
            ids.Add(this.Input1.Id);
            ids.Add(this.Result.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Input0.Id = New.Move.Core.Database.GetId(map, this.Input0.Id);
            this.Input1.Id = New.Move.Core.Database.GetId(map, this.Input1.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
        }

    }
}