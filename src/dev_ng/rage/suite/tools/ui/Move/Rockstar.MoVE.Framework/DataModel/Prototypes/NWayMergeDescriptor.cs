﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("MotionTreeDescriptor", typeof(IDesc)), DescriptorMetadata((int)DescriptorGroup.MergeN)]
    public class NWayMergeDescriptor : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Merge N";
            public static int Id = 26;
            public static string Help = "Merge N";
            public static string Group = "Blend";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(NWayMergePrototype); } }

        public NWayMergeDescriptor()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource =
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Rockstar.MoVE.Framework.Media.NWayMerge.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new NWayMergePrototype(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}