﻿using System;

namespace Rockstar.MoVE.Framework
{
    public interface IValueConverterMetadata
    {
        Type ConvertFrom { get; }
        Type ConvertTo { get; }
    }
}