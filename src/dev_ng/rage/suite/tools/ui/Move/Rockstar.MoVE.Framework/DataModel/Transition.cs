﻿using System;
using System.Runtime.Serialization;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using System.Xml;
using System.Globalization;
using System.Collections.Generic;

namespace Rockstar.MoVE.Framework
{
    [Serializable]
    public class Transition : TransitionBase, ISerializable
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        public const string IdSerializationTag = "Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Source"/> property.
        /// </summary>
        public const string SourceSerializationTag = "Source";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Dest"/> property.
        /// </summary>
        public const string DestSerializationTag = "Dest";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Parent"/> property.
        /// </summary>
        public const string ParentSerializationTag = "Parent";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Enabled"/> property.
        /// </summary>
        public const string EnabledSerializationTag = "Enabled";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Modifier"/> property.
        /// </summary>
        public const string ModifierSerializationTag = "Modifier";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Duration"/> property.
        /// </summary>
        public const string DurationSerializationTag = "Duration";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Sync"/> property.
        /// </summary>
        public const string SyncSerializationTag = "Sync";

        /// <summary>
        /// The tag that is used to serialise/deserialise the
        /// <see cref="FilterTemplate"/> property.
        /// </summary>
        public const string FilterTemplateSerializationTag = "FilterTemplate";

        /// <summary>
        /// The tag that is used to serialise/deserialise the
        /// <see cref="BlockUpdateOnTransition"/> property.
        /// </summary>
        public const string BlockUpdateOnTransitionSerializationTag = "BlockUpdateOnTransition";

        /// <summary>
        /// The tag that is used to serialise/deserialise the
        /// <see cref="TransitionWeightOutput"/> property.
        /// </summary>
        public const string TransitionWeightOutputSerializationTag = "TransitionWeightOutput";

        /// <summary>
        /// The tag that is used to serialise/deserialise the
        /// <see cref="Transitional"/> property.
        /// </summary>
        public const string TransitionalSerializationTag = "Transitional";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Immutable"/> property.
        /// </summary>
        public const string ImmutableSerializationTag = "Immutable";

        /// <summary>
        /// The id for the source object that needs resolving.
        /// </summary>
        private string resolveSource;

        /// <summary>
        /// The if for the destination object that needs resolving.
        /// </summary>
        private string resolveDestination;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the 
        /// <see cref="Rockstar.MoVE.Framework.Transition"/> class.
        /// </summary>
        /// <param name="source">
        /// Represents the source object this transition will have.
        /// </param>
        /// <param name="destination">
        /// Represents the destination object this transition will have.
        /// </param>
        public Transition(ITransitional source, ITransitional sink)
            : base(source, sink)
        {
            Enabled = new BoolTransitionProperty(true);
            Modifier = New.Move.Core.Modifier.Linear;
            Duration = 0.25;
            Sync = new SyncConditionProperty();
            FilterTemplate = new FilterLogicalProperty();
            BlockUpdateOnTransition = new BoolTransitionProperty();
            TransitionWeightOutput = new RealSignalOutputProperty();
            Transitional = new BoolTransitionProperty(true);
            Immutable = new BoolTransitionProperty();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Rockstar.MoVE.Framework.Transition"/>
        /// class as a copy of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        private Transition(Transition other)
            : base(other.Source, other.Dest)
        {
            Enabled = new BoolTransitionProperty(other.Enabled);
            Modifier = new ModifierTransitionProperty(other.Modifier);
            Duration = new RealTransitionProperty(other.Duration);
            Sync = new SyncConditionProperty(other.Sync);
            FilterTemplate = new FilterLogicalProperty(other.FilterTemplate);
            BlockUpdateOnTransition = new BoolTransitionProperty(other.BlockUpdateOnTransition);
            TransitionWeightOutput = new RealSignalOutputProperty(other.TransitionWeightOutput);
            Transitional = new BoolTransitionProperty(other.Transitional);
            Immutable = new BoolTransitionProperty(other.Immutable);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Rockstar.MoVE.Framework.Transition"/>
        /// class using the specified serialisation info and streaming context as
        /// data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public Transition(SerializationInfo info, StreamingContext context)
        {
            Id = (Guid)info.GetValue(IdSerializationTag, typeof(Guid));
            Source = (ITransitional)info.GetValue(SourceSerializationTag, typeof(ITransitional));
            Dest = (ITransitional)info.GetValue(DestSerializationTag, typeof(ITransitional));
            Parent = (IAutomaton)info.GetValue(ParentSerializationTag, typeof(IAutomaton));

            Modifier = (ModifierTransitionProperty)info.GetValue(ModifierSerializationTag, typeof(ModifierTransitionProperty));
            Duration = (RealTransitionProperty)info.GetValue(DurationSerializationTag, typeof(RealTransitionProperty));
            try
            {
                Sync = (SyncConditionProperty)info.GetValue(SyncSerializationTag, typeof(SyncConditionProperty));
                FilterTemplate = (FilterLogicalProperty)info.GetValue(FilterTemplateSerializationTag, typeof(FilterLogicalProperty));
            }
            catch (Exception)
            {
                FilterTemplate = new FilterLogicalProperty();
            }

            try
            {
                BlockUpdateOnTransition = (BoolTransitionProperty)info.GetValue(BlockUpdateOnTransitionSerializationTag, typeof(BoolTransitionProperty));
            }
            catch (SerializationException)
            {
                BlockUpdateOnTransition = new BoolTransitionProperty();
            }

            try
            {
                TransitionWeightOutput = (RealSignalOutputProperty)info.GetValue(TransitionWeightOutputSerializationTag, typeof(RealSignalOutputProperty));
            }
            catch (SerializationException)
            {
                TransitionWeightOutput = new RealSignalOutputProperty();
            }

            try
            {
                Enabled = (BoolTransitionProperty)info.GetValue(EnabledSerializationTag, typeof(BoolTransitionProperty));
            }
            catch (SerializationException)
            {
                Enabled = new BoolTransitionProperty(true);
            }

            try
            {
                Transitional = (BoolTransitionProperty)info.GetValue(TransitionalSerializationTag, typeof(BoolTransitionProperty));
            }
            catch (SerializationException)
            {
                Transitional = new BoolTransitionProperty(false);
            }

            try
            {
                Immutable = (BoolTransitionProperty)info.GetValue(ImmutableSerializationTag, typeof(BoolTransitionProperty));
            }
            catch (SerializationException)
            {
                Immutable = new BoolTransitionProperty(false);
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Transition"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="database">
        /// The root database the transition belongs to.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public Transition(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Deserialise(reader, database);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the boolean property that represents whether or not this transition
        /// is currently enabled.
        /// </summary>
        public override BoolTransitionProperty Enabled
        {
            get;
            protected set;
        }

        /// <summary>
        /// Get the type of modifier for this transition.
        /// </summary>
        public override ModifierTransitionProperty Modifier
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the duration value for this transition.
        /// </summary>
        public override RealTransitionProperty Duration
        {
            get;
            protected set;
        }

        /// <summary>
        /// Get the type of syncing this transition does.
        /// </summary>
        public override SyncConditionProperty Sync
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the filter template for this transition.
        /// </summary>
        public override FilterLogicalProperty FilterTemplate
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the boolean property that represents whether or not this transition
        /// blocks any updating going on while the transition is taking place.
        /// </summary>
        public override BoolTransitionProperty BlockUpdateOnTransition
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the weight value that is applied to this transition.
        /// </summary>
        public override RealSignalOutputProperty TransitionWeightOutput
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the boolean property that represents whether or not this transition
        /// is currently set to a transitional.
        /// </summary>
        public override BoolTransitionProperty Transitional
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the boolean property that represents whether or not this transition
        /// is currently set to immutable.
        /// </summary>
        public override BoolTransitionProperty Immutable
        {
            get;
            protected set;
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredTransition);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(IdSerializationTag, Id);
            info.AddValue(SourceSerializationTag, Source);
            info.AddValue(DestSerializationTag, Dest);
            info.AddValue(ParentSerializationTag, Parent);

            info.AddValue(EnabledSerializationTag, Enabled);
            info.AddValue(ModifierSerializationTag, Modifier);
            info.AddValue(DurationSerializationTag, Duration);
            info.AddValue(SyncSerializationTag, Sync);
            info.AddValue(FilterTemplateSerializationTag, FilterTemplate);
            info.AddValue(BlockUpdateOnTransitionSerializationTag, BlockUpdateOnTransition);
            info.AddValue(TransitionWeightOutputSerializationTag, TransitionWeightOutput);
            info.AddValue(TransitionalSerializationTag, Transitional);
            info.AddValue(ImmutableSerializationTag, Immutable);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            writer.WriteStartElement("base");

            writer.WriteAttributeString(
                ImmutableSerializationTag,
                this.Immutable.Value ? "true" : "false");

            writer.WriteAttributeString(
                TransitionalSerializationTag,
                this.Transitional.Value ? "true" : "false");

            writer.WriteAttributeString(
                EnabledSerializationTag,
                this.Enabled.Value ? "true" : "false");

            writer.WriteAttributeString(
                BlockUpdateOnTransitionSerializationTag,
                this.BlockUpdateOnTransition.Value ? "true" : "false");

            if (this.Dest != null)
            {
                writer.WriteAttributeString(
                    "Destination", this.Dest.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            if (this.Source != null)
            {
                writer.WriteAttributeString(
                    "Source", this.Source.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            writer.WriteStartElement("modifier");
            this.Modifier.Serialise(writer);
            writer.WriteEndElement();

            writer.WriteStartElement("duration");
            this.Duration.Serialise(writer);
            writer.WriteEndElement();

            writer.WriteStartElement("sync");
            this.Sync.Serialise(writer);
            writer.WriteEndElement();

            writer.WriteStartElement("filter");
            this.FilterTemplate.Serialise(writer);
            writer.WriteEndElement();

            writer.WriteStartElement("weight");
            this.TransitionWeightOutput.Serialise(writer);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        public override ISelectable Clone()
        {
            return new Transition(this);
        }

        public override void ResolvePoints(Database database, ITransitional parentSource, ITransitional parentDest)
        {
            if (!string.IsNullOrEmpty(this.resolveSource))
            {
                this.Source = database.Find(Guid.Parse(this.resolveSource)) as ITransitional;
                if (this.Source == null)
                {
                    this.Source = parentSource;
                }
            }

            if (!string.IsNullOrEmpty(this.resolveDestination))
            {
                this.Dest = database.Find(Guid.Parse(this.resolveDestination)) as ITransitional;
                if (this.Dest == null)
                {
                    this.Dest = parentDest;
                }
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="database">
        /// The database this transition belongs to.
        /// </param>
        private void Deserialise(XmlReader reader, Database database)
        {
            this.Immutable = bool.Parse(reader.GetAttribute(ImmutableSerializationTag));
            this.Immutable.Parent = this;
            this.Transitional = bool.Parse(reader.GetAttribute(TransitionalSerializationTag));
            this.Transitional.Parent = this;
            this.Enabled = bool.Parse(reader.GetAttribute(EnabledSerializationTag));
            this.Enabled.Parent = this;
            this.BlockUpdateOnTransition = bool.Parse(reader.GetAttribute(BlockUpdateOnTransitionSerializationTag));
            this.BlockUpdateOnTransition.Parent = this;

            this.resolveSource = reader.GetAttribute("Source");
            this.resolveDestination = reader.GetAttribute("Destination");

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "modifier") == 0)
                {
                    this.Modifier = new ModifierTransitionProperty(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "duration") == 0)
                {
                    this.Duration = new RealTransitionProperty(reader, database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "sync") == 0)
                {
                    this.Sync = new SyncConditionProperty(reader, null);
                }
                else if (string.CompareOrdinal(reader.Name, "filter") == 0)
                {
                    this.FilterTemplate = new FilterLogicalProperty(reader, database, null);
                }
                else if (string.CompareOrdinal(reader.Name, "weight") == 0)
                {
                    this.TransitionWeightOutput = new RealSignalOutputProperty(reader, database, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
            if (this.Dest != null)
            {
                Guid newDestId = Database.GetId(map, this.Dest.Id);
                ITransitional newDest = this.Parent.Database.Find(newDestId) as ITransitional;
                if (newDest != null)
                {
                    this.Dest = newDest;
                }
            }

            if (this.Source != null)
            {
                Guid newSourceId = Database.GetId(map, this.Source.Id);
                ITransitional newDSource = this.Parent.Database.Find(newSourceId) as ITransitional;
                if (newDSource != null)
                {
                    this.Dest = newDSource;
                }
            }

            this.Duration.Id = Database.GetId(map, this.Duration.Id);
            this.Sync.Id = Database.GetId(map, this.Dest.Id);
            this.TransitionWeightOutput.Id = Database.GetId(map, this.TransitionWeightOutput.Id);
            this.Modifier.Id = Database.GetId(map, this.Modifier.Id);
            this.FilterTemplate.Id = Database.GetId(map, this.FilterTemplate.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            if (this.Dest != null)
            {
                ids.Add(this.Dest.Id);
            }

            ids.Add(this.Duration.Id);
            if (this.Source != null)
            {
                ids.Add(this.Source.Id);
            }

            ids.Add(this.Sync.Id);
            ids.Add(this.TransitionWeightOutput.Id);
            ids.Add(this.Modifier.Id);
            ids.Add(this.FilterTemplate.Id);

            return ids;
        }
        #endregion
    }

    public class RegisteredTransition: ITransitionDesc
    {
        public RegisteredTransition()
        {
        }

        static class Const
        {
            public static string Name = "Transit";
            public static int Id = 0;
            public static string Help = "Performs a transitional blend between two states over a given period of time";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }

        public ITransition Create(ITransitional source, ITransitional sink)
        {
            return new Transition(source, sink);
        }
    }
}
