﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Rockstar.MoVE.Framework
{
    public class PropertyCollection : IEnumerable
    {
        Dictionary<string, object> _Properties = new Dictionary<string, object>();

        public object this[string name]
        {
            get
            {
                if (_Properties.ContainsKey(name))
                {
                    return _Properties[name];
                }

                return null;
            }
            set
            {
                _Properties[name] = value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Properties.GetEnumerator();
        }

        public IEnumerator Items 
        {
            get
            {
                return _Properties.GetEnumerator();
            }
        }
    }
}