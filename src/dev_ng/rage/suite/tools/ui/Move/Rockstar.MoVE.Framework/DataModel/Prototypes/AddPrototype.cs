﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Serializable]
    public class AddPrototype : Logic, ISerializable
    {
        static class ParameterOffset
        {
            public const uint Filter = 0;
            public const uint Weight = 1;
            public const uint Sync = 2;
            public const uint Output = ushort.MaxValue;
        }

        FilterLogicalProperty _Filter;
        public FilterLogicalProperty Filter
        {
            get { return _Filter; }
            private set 
            {
                if (_Filter != null)
                {
                    _Filter.PropertyChanged -= new System.ComponentModel.PropertyChangedEventHandler(OnFilterPropertyChanged);
                }
                _Filter = value;
                _Filter.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(OnFilterPropertyChanged);
                UpdateAdditionalText();
            }
        }

        [ParameterExport(ParameterOffset.Weight)]
        public RealLogicalProperty Weight
        {
            get;
            private set;
        }

        [ParameterExport(ParameterOffset.Output)]
        public ResultTransform Result
        {
            get;
            private set;
        }

        [ParameterExport(ParameterOffset.Sync)]
        public SyncConditionProperty Sync
        {
            get;
            private set;
        }
        
        public BoolTransitionProperty Source0Immutable
        {
            get;
            private set;
        }

        public BoolTransitionProperty Source1Immutable
        {
            get;
            private set;
        }

        public SourceInfluenceLogicalProperty Source0Influence { get; private set; }
        public SourceInfluenceLogicalProperty Source1Influence { get; private set; }

        public SourceTransform Source0 { get; private set; }
        public SourceTransform Source1 { get; private set; }

        public AddPrototype(IDesc desc)
            : base(desc)
        {
            Filter = new FilterLogicalProperty();
            Source0Immutable = new BoolTransitionProperty();
            Source1Immutable = new BoolTransitionProperty();
            Source0Influence = new SourceInfluenceLogicalProperty();
            Source1Influence = new SourceInfluenceLogicalProperty();
            Weight = 0.0f;

            Result = new ResultTransform(this);
            Source0 = new SourceTransform(this);
            Source1 = new SourceTransform(this);

            Sync = new SyncConditionProperty();
        }

        void OnFilterPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Input" || e.PropertyName == "FilterTemplate")
            {
                UpdateAdditionalText();
            }
        }

        void UpdateAdditionalText()
        {
            if (Filter.Input == PropertyInput.Value && !string.IsNullOrEmpty(Filter.FilterName))
            {
                AdditionalText = string.Format("({0})", Filter.FilterName);
            }
            else
            {
                AdditionalText = "";
            }
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "AddNode");
            itemNode.AppendTextElementNode("Name", Name);
            Filter.ExportToXML(itemNode.AppendChild("Filter"), motiontreeParent);
            Weight.ExportToXML(itemNode.AppendChild("Weight"), motiontreeParent);

            itemNode.AppendValueElementNode("Source0Immutable", "value", Source0Immutable.Value.ToString().ToLower());
            itemNode.AppendValueElementNode("Source1Immutable", "value", Source1Immutable.Value.ToString().ToLower());

            Sync.ExportToXML(itemNode.AppendChild("Synchronizer"));

            // Lookup the source node names
            foreach (var connection in motiontreeParent.Connections)
            {
                if (connection.Dest == Source0)
                    itemNode.AppendTextElementNode("Child0", connection.Source.Parent.Name);
                else if (connection.Dest == Source1)
                    itemNode.AppendTextElementNode("Child1", connection.Source.Parent.Name);
            }

            Source0Influence.ExportToXML(itemNode, "Source0InfluenceFlag");
            Source1Influence.ExportToXML(itemNode, "Source1InfluenceFlag");
        }
        
        public override ISelectable Clone(Logic clone)
        {
            clone = new AddPrototype(new AddDescriptor());
            AddPrototype highClone = (AddPrototype)base.Clone(clone);
            
            highClone.Filter = new FilterLogicalProperty(this.Filter);
            highClone.Weight = new RealLogicalProperty(this, this.Weight);
            highClone.Sync = new SyncConditionProperty(this.Sync);
            highClone.Source0Immutable = new BoolTransitionProperty(this.Source0Immutable);
            highClone.Source1Immutable = new BoolTransitionProperty(this.Source1Immutable);
            highClone.Source0Influence = new SourceInfluenceLogicalProperty(Source0Influence);
            highClone.Source1Influence = new SourceInfluenceLogicalProperty(Source1Influence);

            highClone.Result = new ResultTransform(highClone);
            highClone.Source0 = new SourceTransform(highClone);
            highClone.Source1 = new SourceTransform(highClone);

            highClone.Sync = new SyncConditionProperty(Sync);
            highClone.Tag = null;

            return (ISelectable)highClone;
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="AddPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public AddPrototype(XmlReader reader, Database database, ITransitional parent)
            : this(new AddDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent != null ? parent.Id : Guid.Empty;
            this.Database = database;
            this.Deserialise(reader, parent);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Add2");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Filter != null)
            {
                writer.WriteStartElement("Filter");
                this.Filter.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Weight != null)
            {
                writer.WriteStartElement("Weight");
                this.Weight.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Sync != null)
            {
                writer.WriteStartElement("Sync");
                this.Sync.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source0Immutable != null)
            {
                writer.WriteStartElement("Source0Immutable");
                this.Source0Immutable.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source1Immutable != null)
            {
                writer.WriteStartElement("Source1Immutable");
                this.Source1Immutable.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source0Influence != null)
            {
                writer.WriteStartElement("Source0Influence");
                this.Source0Influence.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source1Influence != null)
            {
                writer.WriteStartElement("Source1Influence");
                this.Source1Influence.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source0 != null)
            {
                writer.WriteStartElement("Source0");
                this.Source0.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source1 != null)
            {
                writer.WriteStartElement("Source1");
                this.Source1.Serialise(writer);
                writer.WriteEndElement();
            }
            
            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, ITransitional parent)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Filter") == 0)
                {
                    this.Filter = new FilterLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Weight") == 0)
                {
                    this.Weight = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Sync") == 0)
                {
                    this.Sync = new SyncConditionProperty(reader, null);
                }
                else if (string.CompareOrdinal(reader.Name, "Transitional") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Immutable") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Source0Immutable") == 0)
                {
                    this.Source0Immutable = new BoolTransitionProperty(reader, this.Database, null);
                }
                else if (string.CompareOrdinal(reader.Name, "Source1Immutable") == 0)
                {
                    this.Source1Immutable = new BoolTransitionProperty(reader, this.Database, null);
                }
                else if (string.CompareOrdinal(reader.Name, "Source0Influence") == 0)
                {
                    this.Source0Influence = new SourceInfluenceLogicalProperty(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Source1Influence") == 0)
                {
                    this.Source1Influence = new SourceInfluenceLogicalProperty(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Source0") == 0)
                {
                    this.Source0 = new SourceTransform(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Source1") == 0)
                {
                    this.Source1 = new SourceTransform(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new ResultTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Filter.Id);
            ids.Add(this.Result.Id);
            ids.Add(this.Source0.Id);
            ids.Add(this.Source0Immutable.Id);
            ids.Add(this.Source0Influence.Id);
            ids.Add(this.Source1.Id);
            ids.Add(this.Source1Immutable.Id);
            ids.Add(this.Source1Influence.Id);
            ids.Add(this.Sync.Id);
            ids.Add(this.Weight.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Filter.Id = New.Move.Core.Database.GetId(map, this.Filter.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
            this.Source0.Id = New.Move.Core.Database.GetId(map, this.Source0.Id);
            this.Source0Immutable.Id = New.Move.Core.Database.GetId(map, this.Source0Immutable.Id);
            this.Source0Influence.Id = New.Move.Core.Database.GetId(map, this.Source0Influence.Id);
            this.Source1.Id = New.Move.Core.Database.GetId(map, this.Source1.Id);
            this.Source1Immutable.Id = New.Move.Core.Database.GetId(map, this.Source1Immutable.Id);
            this.Source1Influence.Id = New.Move.Core.Database.GetId(map, this.Source1Influence.Id);
            this.Sync.Id = New.Move.Core.Database.GetId(map, this.Sync.Id);
            this.Weight.Id = New.Move.Core.Database.GetId(map, this.Weight.Id);
        }
    }
}