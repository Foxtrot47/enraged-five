﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Serializable]
    public class FilterPrototype : Logic
    {
        static class ParameterOffset
        {
            public const uint Filter = 0;
            public const uint Output = 1;
        }

        [ParameterExport(ParameterOffset.Filter)]
        public FilterLogicalProperty Filter { get; private set; }

        [ParameterExport(ParameterOffset.Output)]
        public PassthroughTransform Result { get; private set; }

        public FilterPrototype(IDesc desc)
            : base(desc)
        {
            Filter = new FilterLogicalProperty();

            Result = new PassthroughTransform(this);
        }

        public FilterPrototype(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            //  Set the description up
            this.Desc = new FilterDescriptor();
            //  Set the data here, the rest is handled by the base class
            this.Filter = (FilterLogicalProperty)info.GetValue("Filter", typeof(FilterLogicalProperty));
            this.Result = (PassthroughTransform)info.GetValue("Result", typeof(PassthroughTransform));
            this.Tag = (object)info.GetValue("Tag", typeof(object));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //  Set the data for this class
            info.AddValue("Filter", this.Filter);
            info.AddValue("Result", this.Result);
            info.AddValue("Tag", this.Tag);
            //  The rest is set in the base class
            base.GetObjectData(info, context);
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "FilterNode");

            itemNode.AppendTextElementNode("Name", Name);

            foreach (Connection connection in motiontreeParent.Connections)
            {
                if (connection.Dest == Result)
                {
                    itemNode.AppendTextElementNode("Source", connection.Source.Parent.Name);
                    break;
                }
            }

            Filter.ExportToXML(itemNode.AppendChild("Filter"), motiontreeParent);
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new FilterPrototype(new FilterDescriptor());
            FilterPrototype highClone = (FilterPrototype)base.Clone(clone);

            highClone.Id = Guid.NewGuid();

            highClone.Filter = new FilterLogicalProperty(Filter);
            highClone.Filter.Parent = this;

            highClone.Result = new PassthroughTransform(highClone);
            highClone.Tag = null;

            return (ISelectable)highClone;
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public FilterPrototype(XmlReader reader, Database database, Guid parent)
            : this(new FilterDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Filter");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Filter != null)
            {
                writer.WriteStartElement("Filter");
                this.Filter.Serialise(writer);
                writer.WriteEndElement();
            }
            
            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Filter") == 0)
                {
                    this.Filter = new FilterLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new PassthroughTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Filter.Id);
            ids.Add(this.Result.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Filter.Id = New.Move.Core.Database.GetId(map, this.Filter.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
        }
    }
}