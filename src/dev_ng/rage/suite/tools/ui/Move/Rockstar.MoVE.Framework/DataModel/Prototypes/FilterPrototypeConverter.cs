﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(FilterPrototype), typeof(PropertyCollection))]
    internal class FilterPrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            FilterPrototype prototype = value as FilterPrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Filter"] = prototype.Filter;

            return collection;
        }
    }
}