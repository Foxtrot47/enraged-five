﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Serializable]
    public class CapturePrototype : Logic
    {
        static class Const
        {
            public const uint FrameParameterExportOffset = 0;
            public const uint OwnerParameterExportOffset = 1;
            public const uint OutputParameterExportOffset = ushort.MaxValue;
        }

        [ParameterExport(Const.FrameParameterExportOffset)]
        public FrameLogicalProperty Frame
        {
            get;
            private set;
        }
        
        [ParameterExport(Const.OutputParameterExportOffset)]
        public PassthroughTransform Result
        {
            get;
            private set;
        }

        public CapturePrototype(IDesc desc)
            : base(desc)
        {
            Frame = new FrameLogicalProperty();

            Result = new PassthroughTransform(this);
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "CaptureNode");

            itemNode.AppendTextElementNode("Name", Name);

            foreach (Connection connection in motiontreeParent.Connections)
            {
                if (connection.Dest == Result)
                {
                    itemNode.AppendTextElementNode("Source", connection.Source.Parent.Name);
                    break;
                }
            }

            Frame.ExportToXML(itemNode.AppendChild("Frame"), motiontreeParent);
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new CapturePrototype(new CaptureDescriptor());
            clone.AdditionalText = this.AdditionalText;
            clone.Database = this.Database;
            clone.Enabled = this.Enabled;
            clone.Name = this.Name;
            clone.Parent = this.Parent;
            clone.Position = this.Position;

            (clone as CapturePrototype).Frame = new FrameLogicalProperty(this.Frame);
            return (ISelectable)clone;
        }
                      
        /// <summary>
        /// Initialises a new instance of the <see cref="CapturePrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public CapturePrototype(XmlReader reader, Database database, Guid parent)
            : this(new CaptureDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Capture");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Frame != null)
            {
                writer.WriteStartElement("Frame");
                this.Frame.Serialise(writer);
                writer.WriteEndElement();
            }
            
            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Frame") == 0)
                {
                    this.Frame = new FrameLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Owner") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new PassthroughTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Frame.Id);
            ids.Add(this.Result.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Frame.Id = New.Move.Core.Database.GetId(map, this.Frame.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
        }
    }
}