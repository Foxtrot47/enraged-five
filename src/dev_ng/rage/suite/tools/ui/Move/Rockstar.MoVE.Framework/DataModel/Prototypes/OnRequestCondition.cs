﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml;
using New.Move.Core;
using RSG.Base.Logging;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    /// <summary>
    /// Represents a condition that can be placed onto a transition that is valid when a
    /// associated request is set.
    /// </summary>
    [Serializable]
    public class OnRequestCondition : ConditionBase, ISerializable
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Request"/> property.
        /// </summary>
        private const string RequestSerialisationTag = "Request";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="NotSet"/> property.
        /// </summary>
        private const string NotSetSerialisationTag = "NotSet";

        /// <summary>
        /// The private field used for the <see cref="Event"/> property.
        /// </summary>
        private RequestConditionProperty requestProperty;

        /// <summary>
        /// The private field used for the <see cref="NotSet"/> property.
        /// </summary>
        private BooleanConditionProperty notSet;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OnRequestCondition"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the condition.
        /// </param>
        public OnRequestCondition(string name)
            : base(name)
        {
            this.requestProperty = new RequestConditionProperty();
            this.notSet = false;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OnRequestCondition"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public OnRequestCondition(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.notSet = new BooleanConditionProperty();
            this.requestProperty = new RequestConditionProperty();
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case RequestSerialisationTag:
                        this.requestProperty = (RequestConditionProperty)entry.Value;
                        break;
                    case NotSetSerialisationTag:
                        this.notSet = (BooleanConditionProperty)entry.Value;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OnRequestCondition"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="database">
        /// The root database the condition belongs to.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public OnRequestCondition(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Deserialise(reader, database);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the request property for this condition.
        /// </summary>
        public RequestConditionProperty Request
        {
            get { return this.requestProperty; }
        }

        /// <summary>
        /// Gets the not set property for this condition.
        /// </summary>
        public BooleanConditionProperty NotSet
        {
            get { return this.notSet; }
        }

        /// <summary>
        /// Gets the type descriptor that describes the condition.
        /// </summary>
        public override Type Descriptor
        {
            get { return typeof(OnRequestDescriptor); }
        }

        /// <summary>
        /// Gets the xml export type name for this condition.
        /// </summary>
        public override string XMLExportTypeName
        {
            get { return "OnRequest"; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.GetSerializationData(info, context);

            info.AddValue(RequestSerialisationTag, this.Request);
            info.AddValue(NotSetSerialisationTag, this.NotSet);
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <param name="parent">
        /// The parent for the cloned object.
        /// </param>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        public override ICondition Clone(Condition parent)
        {
            OnRequestCondition clone = new OnRequestCondition(this.Name);
            clone.notSet = new BooleanConditionProperty(this.NotSet);
            clone.requestProperty = new RequestConditionProperty(this.Request);
            clone.NotSet.Parent = parent;
            clone.Request.Parent = parent;
            return clone;
        }

        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        public override void ExportToXML(PargenXmlNode parentNode)
        {
            parentNode.AppendTextElementNode("Parameter", this.Request.Value.Name);
            parentNode.AppendValueElementNode(
                "NotSet", "value", this.NotSet.Value ? "true" : "false");
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("request");
            writer.WriteAttributeString(ConditionBase.NameSerialisationTag, this.Name);

            if (this.NotSet != null)
            {
                writer.WriteAttributeString(
                    NotSetSerialisationTag,
                    this.NotSet.Value.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                writer.WriteAttributeString(NotSetSerialisationTag, "false");
            }

            if (this.Request != null && this.Request.Value != null)
            {
                writer.WriteAttributeString(
                    RequestSerialisationTag,
                    this.Request.Value.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("RequestName", this.Request.Value.Name);
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="database">
        /// The root database this condition belongs to.
        /// </param>
        private void Deserialise(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute(ConditionBase.NameSerialisationTag);
            this.notSet = bool.Parse(reader.GetAttribute(NotSetSerialisationTag));

            string requestId = reader.GetAttribute(RequestSerialisationTag);
            this.requestProperty = new RequestConditionProperty();
            if (requestId != null)
            {
                this.Request.Value = database.GetRequest(Guid.ParseExact(requestId, "D"));
                if (this.Request.Value == null)
                {
                    string requestName = reader.GetAttribute("RequestName");
                    this.Request.Value = database.GetRequest(requestName);
                    if (this.Request.Value == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to request with id '{0}'. This needs fixing before a export is allowed.", requestId);
                    }
                }
            }

            reader.Skip();
        }
        #endregion
    } // Rockstar.MoVE.Framework.DataModel.Prototypes.OnRequestCondition {Class}
} // Rockstar.MoVE.Framework.DataModel.Prototypes {Namespace}
