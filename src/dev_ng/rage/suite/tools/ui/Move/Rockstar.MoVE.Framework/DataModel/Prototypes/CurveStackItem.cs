﻿using System.Windows;
using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    public class CurveStackItem : IOperatorItem
    {
        public CurveStackItem(CurvePrototype curve)
        {
            Curve = curve;
        }

        CurvePrototype Curve;

        struct Segment
        {
            public CurveSegment Start;
            public CurveSegment End;
        }

        public void GetExportData(ChunkObject info, ChunkObject parent, ChunkContext context)
        {
            Curve curve = Curve.Curves[0];

            //we just have a list of points here. we need the proper data
            Segment[] segments = new Segment[curve.Points.Length-1];

            for (int i = 1; i < curve.Points.Length; ++i)
            {
                segments[i - 1].Start = curve.Points[i - 1];
                segments[i - 1].End = curve.Points[i];
            }

            info.AppendUInt32(5);
            ChunkObject.LabelId label = ChunkObject.ReserveLabel();
            info.AppendOffset(label);
            info.AppendLabel(label);

            info.AppendFloat((float)curve.ClampMinimum);
            info.AppendFloat((float)curve.ClampMaximum);
            info.AppendInt32(segments.Length);
            label = ChunkObject.ReserveLabel();
            info.AppendOffset(label);
            info.AppendLabel(label);

            foreach (Segment segment in segments)
            {
                info.AppendUInt32(0); // Type: kCurveLinear
                info.AppendFloat((float)segment.End.X); // Key
                double m = (segment.End.Y - segment.Start.Y) / (segment.End.X - segment.Start.X);
                double b = segment.End.Y - m * segment.End.X;
                info.AppendFloat((float)m);
                info.AppendFloat((float)b);
            }
        }
    }
}