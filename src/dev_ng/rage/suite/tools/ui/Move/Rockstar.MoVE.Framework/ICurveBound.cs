﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Rockstar.MoVE.Framework
{
    public interface ICurveBound
    {
        CurveCollection Curves { get; set; }
    }
}
