﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("MotionTreeDescriptor", typeof(IDesc)), DescriptorMetadata((int)DescriptorGroup.AddN)]
    public class NWayAddDescriptor : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Add N";
            public static int Id = 22;
            public static string Help = "Add N";
            public static string Group = "Blend";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _paletteIcon; } }

        public Type ConstructType { get { return typeof(NWayAddPrototype); } }

        public NWayAddDescriptor()
        {
            try
            {
                // Load the palette icon embedded resource
                _paletteIcon = new BitmapImage();
                _paletteIcon.BeginInit();
                _paletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Rockstar.MoVE.Framework.Media.NWayAdd.png");
                _paletteIcon.EndInit();
            }
            catch
            {
                _paletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new NWayAddPrototype(this);
        }

        protected BitmapImage _paletteIcon = null;
    }
}