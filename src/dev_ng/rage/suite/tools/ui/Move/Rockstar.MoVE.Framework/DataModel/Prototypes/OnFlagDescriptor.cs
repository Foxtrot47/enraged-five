﻿using System.ComponentModel.Composition;
using New.Move.Core;
using System;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ConditionDescriptor", typeof(IConditionDesc)), DescriptorMetadata((int)DescriptorGroup.Other)]
    [Serializable]
    public class OnFlagDescriptor : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "On Flag";
            public static int ListPosition = 1;
        }

        public string Name { get { return Const.Name; } }
        public int ListPosition { get { return Const.ListPosition; } }

        public ICondition Create()
        {
            return new OnFlagCondition(Const.Name);
        }
    }
}