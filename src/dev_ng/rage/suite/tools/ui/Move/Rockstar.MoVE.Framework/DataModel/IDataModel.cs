﻿using System;
using System.Collections.ObjectModel;

using New.Move.Core;

namespace Rockstar.MoVE.Framework
{
    public interface IDataModel : ICoreDataModel
    {
        IValueConverter FindConverter(Type from, Type to);

        /* UNCOMMENT WHEN ICoreDataModel IS GONE
        ObservableCollection<IDesc> MotionTreeDescriptors { get; }
        ObservableCollection<IDesc> StateMachineDescriptors { get; }

        ObservableCollection<IConditionDesc> ConditionDescriptors { get; }
         */
    }
}