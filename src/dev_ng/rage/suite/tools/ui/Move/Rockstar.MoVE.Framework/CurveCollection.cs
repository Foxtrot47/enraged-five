﻿using System;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;

using New.Move.Core;

namespace Rockstar.MoVE.Framework
{
    [Serializable]
    public class CurveCollection : ObservableCollection<Curve>, IProperty//, ISerializable
    {   
        
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public CurveCollection(ILogic parent)
        {
        }

        public CurveCollection()
        {
            Name = "CHANGE ME";
            Id = Guid.NewGuid();
        }

        public CurveCollection(ObservableCollection<Curve> curves)
        {
            Name = "CHANGE ME";
            Id = Guid.NewGuid();
            foreach(Curve curve in curves)
            {
                this.Add(curve);
            }
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        new public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }
        
        public CurveCollection(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(Const.Name, typeof(string));
            Id = (Guid)info.GetValue(Const.Id, typeof(Guid));

            int count = (int)info.GetValue(Const.Count, typeof(int));
            for (int index = 0; index < count; ++index)
            {
                this.Add((Curve)info.GetValue(index.ToString(), typeof(Curve)));
            }

            //_Items.CollectionChanged += new NotifyCollectionChangedEventHandler(Items_CollectionChanged);
        }

        public object Clone()
        {
            CurveCollection clone = new CurveCollection();
            clone.Name = this.Name;

            foreach (Curve curve in this.Items)
            {
                this.Add(new Curve(curve));
            }
            return clone;
        }
           /*
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Count, Count);
            for (int index = 0; index < Count; ++index)
            {
                info.AddValue(index.ToString(), this[index]);
            }
        }
         * */
    }
}

