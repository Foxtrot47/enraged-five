﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(ParameterizedMotionPrototype), typeof(PropertyCollection))]
    internal class ParameterizedMotionPrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            ParameterizedMotionPrototype prototype = value as ParameterizedMotionPrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Parameterized Motion"] = prototype.PM;
            properties[Resources.Phase] = prototype.Phase;
            properties[Resources.Rate] = prototype.Rate;
            properties[Resources.Delta] = prototype.Delta;

            return collection;
        }
    }
}