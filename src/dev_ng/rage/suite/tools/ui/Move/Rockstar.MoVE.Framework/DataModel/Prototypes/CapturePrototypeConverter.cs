﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(CapturePrototype), typeof(PropertyCollection))]
    internal class CapturePrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            CapturePrototype prototype = value as CapturePrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Frame"] = prototype.Frame;

            return collection;
        }
    }
}