﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

using RSG.TrackViewer.Data;
using RSG.TrackViewer;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Serializable]
    public class NWayBlendPrototype : Logic
    {
        const int MAX_COLORS = 10;
        static Color[] sColors = new Color[MAX_COLORS] { Colors.Red, Colors.Green, Colors.Blue, Colors.Pink, Colors.AntiqueWhite,
                                                                    Colors.Black, Colors.Brown, Colors.Cornsilk, Colors.Orange, Colors.Olive};
        static class ParameterOffset
        {
            public const uint Filter = 1;
            public const uint Weight = 2;
            public const uint InputFilter = 3;
            public const uint Sync = 4;
            public const uint ZeroAsDest = 5;
            public const uint Curves = 6;
            public const uint Output = ushort.MaxValue;
        }

        [ParameterExport(new uint[] { ParameterOffset.Weight, ParameterOffset.InputFilter })]
        public WeightFilteredTransformArray Inputs { get; private set; }

        FilterLogicalProperty _Filter;
        public FilterLogicalProperty Filter
        {
            get { return _Filter; }
            private set
            {
                if (_Filter != null)
                {
                    _Filter.PropertyChanged -= new System.ComponentModel.PropertyChangedEventHandler(OnFilterPropertyChanged);
                }
                _Filter = value;
                _Filter.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(OnFilterPropertyChanged);
                UpdateAdditionalText();
            }
        }

        [ParameterExport(ParameterOffset.Output)]
        public ResultTransform Result { get; private set; }

        [ParameterExport(ParameterOffset.Sync)]
        public SyncConditionProperty Sync
        {
            get;
            private set;
        }

        [ParameterExport(ParameterOffset.ZeroAsDest)]
        public BooleanLogicalProperty ZeroAsDest
        {
            get;
            private set;
        }

        public double MinimumInputValue
        {
            get
            {
                return _MinimumInputValue;
            }
            set
            {
                foreach (Track track in TrackGroup.Tracks)
                {
                    track.ClampMinimum = value;
                }

                _MinimumInputValue = value;
            }
        }
        double _MinimumInputValue;

        public double MaximumInputValue
        {
            get
            {
                return _MaximumInputValue;
            }
            set
            {
                foreach (Track track in TrackGroup.Tracks)
                {
                    track.ClampMaximum = value;
                }

                _MaximumInputValue = value;
            }
        }
        double _MaximumInputValue;


        public double MinimumOutputValue
        {
            get
            {
                return _MinimumOutputValue;
            }
            set
            {
                // push a range change down to each of the curves
                _MinimumOutputValue = value;
            }
        }
        double _MinimumOutputValue;

        public double MaximumOutputValue
        {
            get
            {
                return _MaximumOutputValue;
            }
            set
            {
                // push a range change down to each of the curves
                _MaximumOutputValue = value;
            }
        }
        double _MaximumOutputValue;

        /// <summary>
        /// A single object containg all of the data that will be rendered on the data layer
        /// </summary>
        public TrackGroup TrackGroup
        {
            get
            {
                return _TrackGroup;
            }
            set
            {
                if (value != _TrackGroup)
                    _TrackGroup = value;
            }
        }
        TrackGroup _TrackGroup;
        

        public void Inputs_ChildrenChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (WeightFilteredTransform item in e.OldItems)
                {
                    TrackGroup.Remove(item.OutTrack);
                }
            }

            if (e.NewItems != null)
            {
                foreach (WeightFilteredTransform item in e.NewItems)
                {
                    if (item.OutTrack != null)
                    {
                        TrackGroup.Add(item.OutTrack);
                    }
                }
            }

            int i = 0;
            foreach (Track track in TrackGroup.Tracks)
            {
                track.Name = "Track " + i.ToString();
                i++;
            }
            TrackGroup.FireTracksChanged();
        }

        public override void SetCustomData(SerializationInfo info, StreamingContext context) 
        {
            Inputs.AddHandler(new NotifyCollectionChangedEventHandler(Inputs_ChildrenChanged));
            TrackGroup = new TrackGroup("NWayBlend");
            foreach (WeightFilteredTransform wft in Inputs.Items)
            {
                if(wft.OutTrack != null)
                    TrackGroup.Add(wft.OutTrack);
            }

            try
            {
                MinimumInputValue = info.GetDouble("MinimumInputValue");
                MaximumInputValue = info.GetDouble("MaximumInputValue");
                MinimumOutputValue = info.GetDouble("MinimumOutputValue");
                MaximumOutputValue = info.GetDouble("MaximumOutputValue");
            }
            catch (Exception)
            {
                MinimumInputValue = 0.0;
                MaximumInputValue = 1.0;
                MinimumOutputValue = 0.0;
                MaximumOutputValue = 1.0;
            } 
        }

        public override void GetCustomData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("MinimumInputValue", _MinimumInputValue);
            info.AddValue("MaximumInputValue", _MaximumInputValue);
            info.AddValue("MinimumOutputValue", _MinimumOutputValue);
            info.AddValue("MaximumOutputValue", _MaximumOutputValue);
        }

        public NWayBlendPrototype(IDesc desc)
            : base(desc)
        {
            Inputs = new WeightFilteredTransformArray();
            Inputs.Parent = this;
            Filter = new FilterLogicalProperty();

            Result = new ResultTransform(this);
            Sync = new SyncConditionProperty();
            ZeroAsDest = false;

            Inputs.AddHandler(new NotifyCollectionChangedEventHandler(Inputs_ChildrenChanged));
            
            TrackGroup = new TrackGroup("NWayBlend");

            MinimumInputValue = 0.0f;
            MaximumInputValue = 1.0f;
            MinimumOutputValue = 0.0f;
            MaximumOutputValue = 1.0f;
        }

        public NWayBlendPrototype(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            //  Set the description up
            this.Desc = new NWayBlendDescriptor();
            //  Set the data here, the rest is handled in the base class
            this.AdditionalText = (string)info.GetValue("AdditionalText", typeof(string));
            this.Filter = (FilterLogicalProperty)info.GetValue("Filter", typeof(FilterLogicalProperty));
            
            this.TrackGroup = (TrackGroup)info.GetValue("TrackGroup", typeof(TrackGroup));

            this.MaximumInputValue = (double)info.GetValue("MaximumInputValue", typeof(double));
            this.MaximumOutputValue = (double)info.GetValue("MaximumOutputValue", typeof(double));
            this.MinimumInputValue = (double)info.GetValue("MinimumInputValue", typeof(double));
            this.MinimumOutputValue = (double)info.GetValue("MinimumOutputValue", typeof(double));

            this.Result = (ResultTransform)info.GetValue("Result", typeof(ResultTransform));
            this.Sync = (SyncConditionProperty)info.GetValue("Sync", typeof(SyncConditionProperty));
            this.Tag = (object)info.GetValue("Tag", typeof(object));
            this.ZeroAsDest = (BooleanLogicalProperty)info.GetValue("ZeroAsDest", typeof(BooleanLogicalProperty));

            this.Inputs = (WeightFilteredTransformArray)info.GetValue("Inputs", typeof(WeightFilteredTransformArray));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //  Set the data for this class
            info.AddValue("AdditionalText", this.AdditionalText);
            info.AddValue("Filter", this.Filter);
            
            info.AddValue("TrackGroup", this.TrackGroup);

            info.AddValue("MaximumInputValue", this.MaximumInputValue);
            info.AddValue("MaximumOutputValue", this.MaximumOutputValue);
            info.AddValue("MinimumInputValue", this.MinimumInputValue);
            info.AddValue("MinimumOutputValue", this.MinimumOutputValue);

            info.AddValue("Result", this.Result);
            info.AddValue("Sync", this.Sync);
            info.AddValue("Tag", this.Tag);
            info.AddValue("ZeroAsDest", this.ZeroAsDest);

            info.AddValue("Inputs", this.Inputs);

            //  The rest is set in the base class
            base.GetObjectData(info, context);
        }

        void OnFilterPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Input" || e.PropertyName == "FilterTemplate")
            {
                UpdateAdditionalText();
            }
        }

        void UpdateAdditionalText()
        {
            if (Filter.Input == PropertyInput.Value && !string.IsNullOrEmpty(Filter.FilterName))
            {
                AdditionalText = string.Format("({0})", Filter.FilterName);
            }
            else
            {
                AdditionalText = "";
            }
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "BlendNwayNode");
            itemNode.AppendTextElementNode("Name", Name);
            Filter.ExportToXML(itemNode.AppendChild("Filter"), motiontreeParent);
            ZeroAsDest.ExportToXML(itemNode.AppendChild("ZeroDestination"), motiontreeParent);

            Sync.ExportToXML(itemNode.AppendChild("Synchronizer"));

            var inputsNode = itemNode.AppendChild("Children");
            foreach (WeightFilteredTransform item in Inputs.Items)
            {
                var inputItemNode = inputsNode.AppendChild("Item");

                foreach (var connection in motiontreeParent.Connections)
                {
                    if (connection.Dest == item.Input)
                        inputItemNode.AppendTextElementNode("Input", connection.Source.Parent.Name);
                }

                item.Weight.ExportToXML(inputItemNode.AppendChild("Weight"), motiontreeParent);
                item.Filter.ExportToXML(inputItemNode.AppendChild("InputFilter"), motiontreeParent);
                inputItemNode.AppendValueElementNode("Immutable", "value", item.Immutable.Value.ToString().ToLower());
            }
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new NWayBlendPrototype(new NWayBlendDescriptor());
            NWayBlendPrototype highClone = (NWayBlendPrototype)clone;
            highClone.AdditionalText = this.AdditionalText;
            highClone.Database = this.Database;
            highClone.Enabled = this.Enabled;
            highClone.Filter = new FilterLogicalProperty(this.Filter);
            highClone.Filter.Parent = this;

            highClone.MaximumInputValue = this.MaximumInputValue;
            highClone.MaximumOutputValue = this.MaximumOutputValue;
            highClone.MinimumInputValue = this.MinimumInputValue;
            highClone.MinimumOutputValue = this.MinimumOutputValue;
            highClone.Name = this.Name;
            highClone.Position = this.Position;
            highClone.Result = new ResultTransform(highClone);
            highClone.Sync = new SyncConditionProperty(this.Sync);
            highClone.Tag = null;
            highClone.ZeroAsDest = new BooleanLogicalProperty(this.ZeroAsDest);

            //if (this.TrackGroup != null && this.TrackGroup.Tracks != null)
            //{
            //    foreach (Track track in this.TrackGroup.Tracks)
            //    {
            //        highClone.TrackGroup.Add(new Track(track));
            //    }
            //}

            foreach (WeightFilteredTransform transform in this.Inputs.Items)
            {
                WeightFilteredTransform item = new WeightFilteredTransform(transform);
                highClone.Inputs.Items.Add(item);
                item.Parent = highClone;
                item.Filter.Parent = highClone;
                item.Input.Parent = highClone;
                item.Weight.Parent = highClone;
            }

            return (ISelectable)highClone;
        }

        #region Events
        public void Range_Changed(object sender, EventArgs e)
        {
            WindowedTimeLine timeline = sender as WindowedTimeLine;
            if (timeline.OperationalAxis == Axis.X)
            {
                this.MaximumOutputValue = timeline.DisplayMax;
                this.MinimumOutputValue = timeline.DisplayMin;
            }
            else
            {
                this.MaximumInputValue = timeline.DisplayMax;
                this.MinimumInputValue = timeline.DisplayMin;
            }
        }
        #endregion
        
        /// <summary>
        /// Initialises a new instance of the <see cref="NWayBlendPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public NWayBlendPrototype(XmlReader reader, Database database, ITransitional parent)
            : this(new NWayBlendDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Parent = parent != null ? parent.Id : Guid.Empty;
            this.Database = database;
            this.Deserialise(reader, parent);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("BlendN");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "InputMin", this.MinimumInputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "InputMax", this.MaximumInputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "OutputMin", this.MinimumOutputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "OutputMax", this.MaximumOutputValue.ToString(CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Filter != null)
            {
                writer.WriteStartElement("Filter");
                this.Filter.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Sync != null)
            {
                writer.WriteStartElement("Sync");
                this.Sync.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ZeroAsDest != null)
            {
                writer.WriteStartElement("ZeroAsDest");
                this.ZeroAsDest.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Inputs != null)
            {
                writer.WriteStartElement("Inputs");
                foreach (WeightFilteredTransform item in this.Inputs.Children)
                {
                    writer.WriteStartElement("WeightFilteredTransform");
                    item.Serialise(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            
            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, ITransitional parent)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.MaximumInputValue = double.Parse(
                reader.GetAttribute("InputMax"), CultureInfo.InvariantCulture);
            this.MinimumInputValue = double.Parse(
                reader.GetAttribute("InputMin"), CultureInfo.InvariantCulture);
            this.MaximumOutputValue = double.Parse(
                reader.GetAttribute("OutputMax"), CultureInfo.InvariantCulture);
            this.MinimumOutputValue = double.Parse(
                reader.GetAttribute("OutputMin"), CultureInfo.InvariantCulture);

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Filter") == 0)
                {
                    this.Filter = new FilterLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Sync") == 0)
                {
                    this.Sync = new SyncConditionProperty(reader, null);
                }
                else if (string.CompareOrdinal(reader.Name, "Transitional") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Immutable") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "ZeroAsDest") == 0)
                {
                    this.ZeroAsDest = new BooleanLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "TrackGroup") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Inputs") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            this.Inputs.Items.Add(new WeightFilteredTransform(reader, this.Database, this));
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new ResultTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Filter.Id = New.Move.Core.Database.GetId(map, this.Filter.Id);
            this.Inputs.Id = New.Move.Core.Database.GetId(map, this.Inputs.Id);
            foreach (WeightFilteredTransform transform in this.Inputs.Items)
            {
                transform.ResetIds(map);
            }

            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
            this.Sync.Id = New.Move.Core.Database.GetId(map, this.Sync.Id);
            this.ZeroAsDest.Id = New.Move.Core.Database.GetId(map, this.ZeroAsDest.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Filter.Id);
            ids.Add(this.Inputs.Id);
            foreach (WeightFilteredTransform transform in this.Inputs.Items)
            {
                ids.AddRange(transform.GetAllGuids());
            }

            ids.Add(Result.Id);
            ids.Add(Sync.Id);
            ids.Add(ZeroAsDest.Id);
            return ids;
        }
    }
}