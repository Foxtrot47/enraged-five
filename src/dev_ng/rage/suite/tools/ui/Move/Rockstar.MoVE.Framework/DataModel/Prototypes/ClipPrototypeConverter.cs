﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(ClipPrototype), typeof(PropertyCollection))]
    internal class ClipPrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            ClipPrototype prototype = value as ClipPrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties[Resources.Clip] = prototype.Clip;
            properties[Resources.Phase] = prototype.Phase;
            properties[Resources.Rate] = prototype.Rate;
            properties[Resources.Delta] = prototype.Delta;
            properties[Resources.Looped] = prototype.Looped;
            properties[Resources.ZeroWeightSync] = prototype.ZeroWeightSync;
           
            PropertyCategory events = new PropertyCategory();
            collection[Resources.Events] = events;

            events[Resources.Looped] = prototype.ClipLooped;
            events[Resources.Ended] = prototype.ClipEnded;

            PropertyCategory clipproperties = new PropertyCategory();
            collection[Resources.ClipProperties] = clipproperties;

            clipproperties[Resources.ClipProperties] = prototype.ClipProperties;

            PropertyCategory clipEventToSignalProperties = new PropertyCategory();
            collection["Clip Tags"] = clipEventToSignalProperties;
            clipEventToSignalProperties["Clip Tags"] = prototype.TagEventToSignalProperties;

            return collection;
        }
    }
}