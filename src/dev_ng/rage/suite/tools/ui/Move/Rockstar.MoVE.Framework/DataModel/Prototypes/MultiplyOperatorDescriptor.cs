﻿using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("MotionTreeDescriptor", typeof(IDesc)), DescriptorMetadata((int)DescriptorGroup.Operand)]
    public class MultiplyOperatorDescriptor : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Multiply";
            public static int Id = 3;
            public static string Help = "Multiply operation";
            public static string Group = "Operator";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _paletteIcon; } }

        public Type ConstructType { get { return typeof(MultiplyOperatorPrototype); } }

        public MultiplyOperatorDescriptor()
        {
            try
            {
                // Load the palette icon embedded resource
                _paletteIcon = new BitmapImage();
                _paletteIcon.BeginInit();
                _paletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(
                    "Rockstar.MoVE.Framework.Media.MultiplyOperator.png");
                _paletteIcon.EndInit();
            }
            catch
            {
                _paletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new MultiplyOperatorPrototype(this);
        }

        protected BitmapImage _paletteIcon = null;
    }
}