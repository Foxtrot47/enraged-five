﻿using System.Xml;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    public class MultiplyOperatorStackItem : IOperatorItem
    {
        public void ExportToXML(PargenXmlNode parentNode)
        {
            parentNode.AppendValueElementNode("Item", "type", "MultiplyOperator");
        }
    }
}