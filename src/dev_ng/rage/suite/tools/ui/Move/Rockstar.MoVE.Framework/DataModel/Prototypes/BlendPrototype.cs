﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using RSG.TrackViewer.Data;
using RSG.TrackViewer;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    /// <summary>
    /// Represents a node that blends two source inputs and produces a single output.
    /// </summary>
    [Serializable]
    public class BlendPrototype : Logic, ILooseOperator
    {
        static class ParameterOffset
        {
            public const uint Filter = 0;
            public const uint Weight = 1;
            public const uint Sync = 2;
            public const uint Output = ushort.MaxValue;
        }

        /// <summary>
        /// A single object containg all of the data that will be rendered on the data layer
        /// </summary>
        public TrackGroup TrackGroup
        {
            get
            {
                return _TrackGroup;
            }
            set
            {
                if (value != _TrackGroup)
                    _TrackGroup = value;
            }
        }
        TrackGroup _TrackGroup;

        public double MinimumInputValue
        {
            get
            {
                return _MinimumInputValue;
            }
            set
            {
                foreach (Track track in TrackGroup.Tracks)
                {
                    track.ClampMinimum = value;
                }

                _MinimumInputValue = value;
            }
        }
        double _MinimumInputValue;

        public double MaximumInputValue
        {
            get
            {
                return _MaximumInputValue;
            }
            set
            {
                foreach (Track track in TrackGroup.Tracks)
                {
                    track.ClampMaximum = value;
                }

                _MaximumInputValue = value;
            }
        }
        double _MaximumInputValue;


        public double MinimumOutputValue
        {
            get
            {
                return _MinimumOutputValue;
            }
            set
            {
                // push a range change down to each of the curves
                _MinimumOutputValue = value;
            }
        }
        double _MinimumOutputValue;

        public double MaximumOutputValue
        {
            get
            {
                return _MaximumOutputValue;
            }
            set
            {
                // push a range change down to each of the curves
                _MaximumOutputValue = value;
            }
        }
        double _MaximumOutputValue;

        FilterLogicalProperty _Filter;
        public FilterLogicalProperty Filter
        {
            get { return _Filter; }
            private set
            {
                if (_Filter != null)
                {
                    _Filter.PropertyChanged -= new System.ComponentModel.PropertyChangedEventHandler(OnFilterPropertyChanged);
                }
                _Filter = value;
                _Filter.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(OnFilterPropertyChanged);
                UpdateAdditionalText();
            }
        }

        [ParameterExport(ParameterOffset.Weight)]
        public RealLogicalProperty Weight { get; private set; }

        public BoolTransitionProperty Source0Immutable
        {
            get;
            private set;
        }

        public BoolTransitionProperty Source1Immutable
        {
            get;
            private set;
        }

        public SourceInfluenceLogicalProperty Source0Influence { get; private set; }
        public SourceInfluenceLogicalProperty Source1Influence { get; private set; }

        public BoolTransitionProperty MergeBlend
        {
            get;
            private set;
        }

        public Track OutTrack
        {
            get
            {
                return _OutTrack;
            }
            set
            {
                if (_OutTrack != value)
                {
                    _OutTrack = value;
                }
            }
        }
        Track _OutTrack;

        public IOperatorItem Item { get { return new TrackStackItem(_OutTrack); } set { } }

        [ParameterExport(ParameterOffset.Sync)]
        public SyncConditionProperty Sync
        {
            get;
            private set;
        }

        [ParameterExport(ParameterOffset.Output)]
        public ResultTransform Result { get; private set; }
        public SourceTransform Source0 { get; private set; }
        public SourceTransform Source1 { get; private set; }

        public BlendPrototype(IDesc desc)
            : base(desc)
        {
            Filter = new FilterLogicalProperty();
            Source0Immutable = new BoolTransitionProperty();
            Source1Immutable = new BoolTransitionProperty();
            Source0Influence = new SourceInfluenceLogicalProperty();
            Source1Influence = new SourceInfluenceLogicalProperty();
            MergeBlend = new BoolTransitionProperty();
            Weight = 0.0f;
            Weight.Parent = this;
            Weight.PropertyChanged += Weight_PropertyChanged;

            Result = new ResultTransform(this);
            Source0 = new SourceTransform(this);
            Source1 = new SourceTransform(this);

            Sync = new SyncConditionProperty();

            _OutTrack = new Track("Weight");
            _OutTrack.IsActive = false;
            TrackPoint start = new FloatTrackPoint(0.0, 0.0);
            start.IsLockedX = true;
            TrackPoint end = new FloatTrackPoint(1.0, 0.0);
            end.IsLockedX = true;
            _OutTrack.AddPoint(start);
            _OutTrack.AddPoint(end);

            TrackGroup = new TrackGroup("TwoWayBlend");
            TrackGroup.Add(_OutTrack);

            MinimumInputValue = 0.0f;
            MaximumInputValue = 1.0f;
            MinimumOutputValue = 0.0f;
            MaximumOutputValue = 1.0f;
        }

        public BlendPrototype(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            //  Set the description up
            this.Desc = new BlendDescriptor();
            //  Set data here, the rest is handled by the base class
            this.Filter = (FilterLogicalProperty)info.GetValue("Filter", typeof(FilterLogicalProperty));
            this.Weight = (RealLogicalProperty)info.GetValue("Weight", typeof(RealLogicalProperty));
            this.Weight.PropertyChanged += Weight_PropertyChanged;

            this.Result = (ResultTransform)info.GetValue("Result", typeof(ResultTransform));
            this.Source0 = (SourceTransform)info.GetValue("Source0", typeof(SourceTransform));
            this.Source1 = (SourceTransform)info.GetValue("Source1", typeof(SourceTransform));

            this.OutTrack = (Track)info.GetValue("OutTrack", typeof(Track));
            this.TrackGroup = (TrackGroup)info.GetValue("TrackGroup", typeof(TrackGroup));

            this.MinimumInputValue = (double)info.GetValue("MinimumInputValue", typeof(double));
            this.MaximumInputValue = (double)info.GetValue("MaximumInputValue", typeof(double));
            this.MinimumOutputValue = (double)info.GetValue("MinimumOutputValue", typeof(double));
            this.MaximumOutputValue = (double)info.GetValue("MaximumOutputValue", typeof(double));

            this.Sync = (SyncConditionProperty)info.GetValue("Sync", typeof(SyncConditionProperty));
            this.Tag = (object)info.GetValue("Tag", typeof(object));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //  Set the data here
            info.AddValue("Filter", this.Filter);
            this.Weight.PropertyChanged -= Weight_PropertyChanged;
            info.AddValue("Weight", this.Weight);
            this.Weight.PropertyChanged += Weight_PropertyChanged;

            info.AddValue("Result", this.Result);
            info.AddValue("Source0", this.Source0);
            info.AddValue("Source1", this.Source1);

            info.AddValue("OutTrack", this.OutTrack);
            info.AddValue("TrackGroup", this.TrackGroup);

            info.AddValue("MinimumInputValue", this.MinimumInputValue);
            info.AddValue("MaximumInputValue", this.MaximumInputValue);
            info.AddValue("MinimumOutputValue", this.MinimumOutputValue);
            info.AddValue("MaximumOutputValue", this.MaximumOutputValue);

            info.AddValue("Sync", this.Sync);
            info.AddValue("Tag", this.Tag);
            //  The rest is set in the base class
            base.GetObjectData(info, context);
        }

        void Weight_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Input")
            {
                switch (this.Weight.Input)
                {
                    case PropertyInput.Curve:
                        _OutTrack.IsActive = true;
                        break;
                    default:
                        _OutTrack.IsActive = false;
                        break;
                }
            }
        }

        void OnFilterPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Input" || e.PropertyName == "FilterTemplate")
            {
                UpdateAdditionalText();
            }
        }

        void UpdateAdditionalText()
        {
            if (Filter.Input == PropertyInput.Value && !string.IsNullOrEmpty(Filter.FilterName))
            {
                AdditionalText = string.Format("({0})", Filter.FilterName);
            }
            else
            {
                AdditionalText = "";
            }
        }

        public override void SetCustomData(SerializationInfo info, StreamingContext context)
        {
            Weight.PropertyChanged += Weight_PropertyChanged;
            Weight.Parent = this;
            TrackGroup = new TrackGroup("TwoWayBlend");

            try
            {
                _OutTrack = (Track)info.GetValue("OutTrack", typeof(Track));
                if (_OutTrack == null)
                {
                    _OutTrack = new Track("Weight");
                    _OutTrack.IsActive = false;
                    TrackPoint start = new FloatTrackPoint(0.0, 0.0);
                    start.IsLockedX = true;
                    TrackPoint end = new FloatTrackPoint(1.0, 0.0);
                    end.IsLockedX = true;
                    _OutTrack.AddPoint(start);
                    _OutTrack.AddPoint(end);
                }
                TrackGroup.Add(_OutTrack);

                MinimumInputValue = info.GetDouble("MinimumInputValue");
                MaximumInputValue = info.GetDouble("MaximumInputValue");
                MinimumOutputValue = info.GetDouble("MinimumOutputValue");
                MaximumOutputValue = info.GetDouble("MaximumOutputValue");
            }
            catch (Exception)
            {
                if (_OutTrack == null)
                {
                    _OutTrack = new Track("Weight");
                    _OutTrack.IsActive = false;
                    TrackPoint start = new FloatTrackPoint(0.0, 0.0);
                    start.IsLockedX = true;
                    TrackPoint end = new FloatTrackPoint(1.0, 0.0);
                    end.IsLockedX = true;
                    _OutTrack.AddPoint(start);
                    _OutTrack.AddPoint(end);
                }
                TrackGroup.Add(_OutTrack);

                MinimumInputValue = 0.0;
                MaximumInputValue = 1.0;
                MinimumOutputValue = 0.0;
                MaximumOutputValue = 1.0;
            }
        }

        public override void GetCustomData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("MinimumInputValue", _MinimumInputValue);
            info.AddValue("MaximumInputValue", _MaximumInputValue);
            info.AddValue("MinimumOutputValue", _MinimumOutputValue);
            info.AddValue("MaximumOutputValue", _MaximumOutputValue);
            info.AddValue("OutTrack", _OutTrack);
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "BlendNode");
            itemNode.AppendTextElementNode("Name", Name);
            Filter.ExportToXML(itemNode.AppendChild("Filter"), motiontreeParent);
            Weight.ExportToXML(itemNode.AppendChild("Weight"), motiontreeParent);

            itemNode.AppendValueElementNode("Source0Immutable", "value", Source0Immutable.Value.ToString().ToLower());
            itemNode.AppendValueElementNode("Source1Immutable", "value", Source1Immutable.Value.ToString().ToLower());
            itemNode.AppendValueElementNode("MergeBlend", "value", this.MergeBlend.Value.ToString().ToLower());
            
            Sync.ExportToXML(itemNode.AppendChild("Synchronizer"));

            // Lookup the source node names
            foreach (var connection in motiontreeParent.Connections)
            {
                if( connection.Dest == Source0 )
                    itemNode.AppendTextElementNode("Child0", connection.Source.Parent.Name);
                else if (connection.Dest == Source1)
                    itemNode.AppendTextElementNode("Child1", connection.Source.Parent.Name);
            }

            Source0Influence.ExportToXML(itemNode, "Source0InfluenceFlag");
            Source1Influence.ExportToXML(itemNode, "Source1InfluenceFlag");
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new BlendPrototype(new BlendDescriptor());
            BlendPrototype highClone = (BlendPrototype)base.Clone(clone);

            highClone.Filter = new FilterLogicalProperty(Filter);
            highClone.Weight.PropertyChanged -= Weight_PropertyChanged;
            highClone.Weight = new RealLogicalProperty(this, Weight);
            highClone.Weight.PropertyChanged += Weight_PropertyChanged;

            highClone.Source0Immutable = new BoolTransitionProperty(this.Source0Immutable);
            highClone.Source1Immutable = new BoolTransitionProperty(this.Source1Immutable);
            highClone.Source0Influence = new SourceInfluenceLogicalProperty(Source0Influence);
            highClone.Source1Influence = new SourceInfluenceLogicalProperty(Source1Influence);
            highClone.MergeBlend = new BoolTransitionProperty(this.MergeBlend);

            highClone.Result = new ResultTransform(highClone);
            highClone.Source0 = new SourceTransform(highClone);
            highClone.Source1 = new SourceTransform(highClone);

            highClone.MinimumInputValue = MinimumInputValue;
            highClone.MaximumInputValue = MaximumInputValue;
            highClone.MinimumOutputValue = MinimumOutputValue;
            highClone.MaximumOutputValue = MaximumOutputValue;

            highClone.OutTrack = new Track(this.OutTrack);
            highClone.TrackGroup = new TrackGroup("TwoWayBlend");
            highClone.TrackGroup.Tracks.Add(_OutTrack);


            highClone.Sync = new SyncConditionProperty(Sync);
            highClone.Tag = null;

            return (ISelectable)highClone;
        }

        #region Events
        public void Range_Changed(object sender, EventArgs e)
        {
            WindowedTimeLine timeline = sender as WindowedTimeLine;
            if (timeline.OperationalAxis == Axis.X)
            {
                this.MaximumOutputValue = timeline.DisplayMax;
                this.MinimumOutputValue = timeline.DisplayMin;
            }
            else
            {
                this.MaximumInputValue = timeline.DisplayMax;
                this.MinimumInputValue = timeline.DisplayMin;
            }
        }
        #endregion

        /// <summary>
        /// Initialises a new instance of the <see cref="BlendPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public BlendPrototype(XmlReader reader, Database database, ITransitional parent)
            : this(new BlendDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Weight.PropertyChanged -= Weight_PropertyChanged;

            this.Parent = parent != null ? parent.Id : Guid.Empty;
            this.Database = database;
            this.Deserialise(reader, parent);
            this.Weight.PropertyChanged += Weight_PropertyChanged;
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Blend2");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "InputMin", this.MinimumInputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "InputMax", this.MaximumInputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "OutputMin", this.MinimumOutputValue.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "OutputMax", this.MaximumOutputValue.ToString(CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Filter != null)
            {
                writer.WriteStartElement("Filter");
                this.Filter.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Weight != null)
            {
                writer.WriteStartElement("Weight");
                this.Weight.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Sync != null)
            {
                writer.WriteStartElement("Sync");
                this.Sync.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source0Immutable != null)
            {
                writer.WriteStartElement("Source0Immutable");
                this.Source0Immutable.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source1Immutable != null)
            {
                writer.WriteStartElement("Source1Immutable");
                this.Source1Immutable.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source0Influence != null)
            {
                writer.WriteStartElement("Source0Influence");
                this.Source0Influence.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source1Influence != null)
            {
                writer.WriteStartElement("Source1Influence");
                this.Source1Influence.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.MergeBlend != null)
            {
                writer.WriteStartElement("MergeBlend");
                this.MergeBlend.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source0 != null)
            {
                writer.WriteStartElement("Source0");
                this.Source0.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source1 != null)
            {
                writer.WriteStartElement("Source1");
                this.Source1.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.TrackGroup != null)
            {
                this.TrackGroup.Serialise(writer);
            }
            
            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, ITransitional parent)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.MaximumInputValue = double.Parse(
                reader.GetAttribute("InputMax"), CultureInfo.InvariantCulture);
            this.MinimumInputValue = double.Parse(
                reader.GetAttribute("InputMin"), CultureInfo.InvariantCulture);
            this.MaximumOutputValue = double.Parse(
                reader.GetAttribute("OutputMax"), CultureInfo.InvariantCulture);
            this.MinimumOutputValue = double.Parse(
                reader.GetAttribute("OutputMin"), CultureInfo.InvariantCulture);
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Filter") == 0)
                {
                    this.Filter = new FilterLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Weight") == 0)
                {
                    this.Weight = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Sync") == 0)
                {
                    this.Sync = new SyncConditionProperty(reader, null);
                }
                else if (string.CompareOrdinal(reader.Name, "Transitional") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Immutable") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "MergeBlend") == 0)
                {
                    this.MergeBlend = new BoolTransitionProperty(reader, this.Database, null);
                }
                else if (string.CompareOrdinal(reader.Name, "Source0Immutable") == 0)
                {
                    this.Source0Immutable = new BoolTransitionProperty(reader, this.Database, null);
                }
                else if (string.CompareOrdinal(reader.Name, "Source1Immutable") == 0)
                {
                    this.Source1Immutable = new BoolTransitionProperty(reader, this.Database, null);
                }
                else if (string.CompareOrdinal(reader.Name, "Source0Influence") == 0)
                {
                    this.Source0Influence = new SourceInfluenceLogicalProperty(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Source1Influence") == 0)
                {
                    this.Source1Influence = new SourceInfluenceLogicalProperty(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Source0") == 0)
                {
                    this.Source0 = new SourceTransform(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Source1") == 0)
                {
                    this.Source1 = new SourceTransform(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Track") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "TrackGroup") == 0)
                {
                    this.TrackGroup = new TrackGroup(reader);
                    if (this.TrackGroup.Tracks.Count >= 1)
                    {
                        this.OutTrack = this.TrackGroup.Tracks[0];
                    }
                    else
                    {
                        this.OutTrack = new Track("Weight");
                        this.OutTrack.IsActive = false;
                        TrackPoint start = new FloatTrackPoint(0.0, 0.0);
                        start.IsLockedX = true;
                        TrackPoint end = new FloatTrackPoint(1.0, 0.0);
                        end.IsLockedX = true;
                        this.OutTrack.AddPoint(start);
                        this.OutTrack.AddPoint(end);
                        this.TrackGroup.Add(this.OutTrack);
                    }
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new ResultTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Filter.Id);
            ids.Add(this.MergeBlend.Id);
            ids.Add(this.Result.Id);
            ids.Add(this.Source0.Id);
            ids.Add(this.Source0Immutable.Id);
            ids.Add(this.Source0Influence.Id);
            ids.Add(this.Source1.Id);
            ids.Add(this.Source1Immutable.Id);
            ids.Add(this.Source1Influence.Id);
            ids.Add(this.Sync.Id);
            ids.Add(this.Weight.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Filter.Id = New.Move.Core.Database.GetId(map, this.Filter.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
            this.Source0.Id = New.Move.Core.Database.GetId(map, this.Source0.Id);
            this.Source0Immutable.Id = New.Move.Core.Database.GetId(map, this.Source0Immutable.Id);
            this.Source0Influence.Id = New.Move.Core.Database.GetId(map, this.Source0Influence.Id);
            this.Source1.Id = New.Move.Core.Database.GetId(map, this.Source1.Id);
            this.Source1Immutable.Id = New.Move.Core.Database.GetId(map, this.Source1Immutable.Id);
            this.Source1Influence.Id = New.Move.Core.Database.GetId(map, this.Source1Influence.Id);
            this.Sync.Id = New.Move.Core.Database.GetId(map, this.Sync.Id);
            this.Weight.Id = New.Move.Core.Database.GetId(map, this.Weight.Id);
        }
    }
}