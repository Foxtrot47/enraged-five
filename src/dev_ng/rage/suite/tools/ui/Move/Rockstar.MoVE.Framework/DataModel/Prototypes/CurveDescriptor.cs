﻿using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("MotionTreeDescriptor", typeof(IDesc)), DescriptorMetadata((int)DescriptorGroup.Other)]
    public class CurveDescriptor : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Curve";
            public static int Id = 5;
            public static string Help = "Curve operation";
            public static string Group = "Operator";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _Icon; } }

        public Type ConstructType { get { return typeof(CurvePrototype); } }

        public CurveDescriptor()
        {
            try
            {
                _Icon = new BitmapImage();
                _Icon.BeginInit();
                _Icon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(
                    "Rockstar.MoVE.Framework.Media.Curve.png");
                _Icon.EndInit();
            }
            catch (Exception)
            {
                _Icon = null;
            }
        }

        public INode Create(Database database)
        {
            return new CurvePrototype(this);
        }

        BitmapImage _Icon = null;

    }
}