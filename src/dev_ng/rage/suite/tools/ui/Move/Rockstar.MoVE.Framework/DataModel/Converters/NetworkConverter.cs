﻿using System;
using System.ComponentModel.Composition;

using New.Move.Core;
using MoVE.Core;

namespace Rockstar.MoVE.Framework.DataModel.Converters
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(Network), typeof(PropertyCollection))]
    internal class NetworkConverter : IValueConverter
    {
        public object Convert(object value)
        {
            Network prototype = (Network)value;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Parameter Name"] = prototype.ParameterName;

            return collection;
        }
    }
}