﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MoVE.Core;
using New.Move.Core;
using RSG.Base.Logging;
using Rockstar.MoVE.Framework.DataModel.Prototypes;

using RSG.AnimationMetadata;

namespace Rockstar.MoVE.Framework
{
    public class DatabaseValidator
    {
        public DatabaseValidator(Database database)
        {
            database_ = database;
        }

        public bool HasErrors { get { return errors_.Count > 0; } }
        public bool HasWarnings { get { return warnings_.Count > 0; } }

        public void Validate()
        {
            ValidateNodeNameUniqueness();

            ValidateControlParameters(database_.Signals);

            foreach (SignalBase signalBase in database_.Nodes.OfType<SignalBase>())
                ValidateSignalBase(signalBase);
            foreach (AddOperatorPrototype addOperatorNode in database_.Nodes.OfType <AddOperatorPrototype>())
                ValidateAddOperatorNode(addOperatorNode);
            foreach (AddPrototype add2Node in database_.Nodes.OfType<AddPrototype>())
                ValidateAdd2Node(add2Node);
            foreach (NWayAddPrototype addNNode in database_.Nodes.OfType<NWayAddPrototype>())
                ValidateAddNNode(addNNode);
            foreach (AnimationPrototype animationNode in database_.Nodes.OfType<AnimationPrototype>())
                ValidateAnimationNode(animationNode);
            foreach (BlendPrototype blend2Node in database_.Nodes.OfType<BlendPrototype>())
                ValidateBlend2Node(blend2Node);
            foreach (NWayBlendPrototype blendNNode in database_.Nodes.OfType<NWayBlendPrototype>())
                ValidateBlendNNode(blendNNode);
            foreach (CapturePrototype captureNode in database_.Nodes.OfType<CapturePrototype>())
                ValidateCaptureNode(captureNode);
            foreach (ClipPrototype clipNode in database_.Nodes.OfType<ClipPrototype>())
                ValidateClipNode(clipNode);
            foreach (CurvePrototype curveNode in database_.Nodes.OfType<CurvePrototype>())
                ValidateCurveNode(curveNode);
            foreach (ExpressionPrototype expressionNode in database_.Nodes.OfType<ExpressionPrototype>())
                ValidateExpressionNode(expressionNode);
            foreach (ExtrapolatePrototype extrapolateNode in database_.Nodes.OfType<ExtrapolatePrototype>())
                ValidateExtrapolateNode(extrapolateNode);
            foreach (FilterPrototype filterNode in database_.Nodes.OfType<FilterPrototype>())
                ValidateFilterNode(filterNode);
            foreach (FramePrototype frameNode in database_.Nodes.OfType<FramePrototype>())
                ValidateFrameNode(frameNode);
            foreach (IdentityPrototype identityNode in database_.Nodes.OfType<IdentityPrototype>())
                ValidateIdentityNode(identityNode);
            foreach (Input inputtNode in database_.Nodes.OfType<Input>())
                ValidateInputNode(inputtNode);
            foreach (InvalidPrototype invalidNode in database_.Nodes.OfType<InvalidPrototype>())
                ValidateInvalidNode(invalidNode);
            foreach (JointLimitPrototype jointLimitNode in database_.Nodes.OfType<JointLimitPrototype>())
                ValidateJointLimitNode(jointLimitNode);
            foreach (MergePrototype merge2Node in database_.Nodes.OfType<MergePrototype>())
                ValidateMerge2Node(merge2Node);
            foreach (NWayMergePrototype mergeNNode in database_.Nodes.OfType<NWayMergePrototype>())
                ValidateMergeNNode(mergeNNode);
            foreach (LogicInlineSM logicInlineSMNode in database_.Nodes.OfType<LogicInlineSM>())
                ValidateLogicInlineSMNode(logicInlineSMNode);
            foreach (LogicLeafSM logicLeafSMNode in database_.Nodes.OfType<LogicLeafSM>())
                ValidateLogicLeafSMNode(logicLeafSMNode);
            foreach (Motiontree motiontreeNode in database_.Nodes.OfType<Motiontree>())
                ValidateMotiontreeNode(motiontreeNode);
            foreach (MultiplyOperatorPrototype multiplyOperatorNode in database_.Nodes.OfType<MultiplyOperatorPrototype>())
                ValidateMultiplyOperatorNode(multiplyOperatorNode);
            foreach (Network networkNode in database_.Nodes.OfType<Network>())
                ValidateNetworkNode(networkNode);
            foreach (Output outputNode in database_.Nodes.OfType<Output>())
                ValidateOutputNode(outputNode);
            foreach (ParameterizedMotionPrototype pmNode in database_.Nodes.OfType<ParameterizedMotionPrototype>())
                ValidateParameterizedMotionNode(pmNode);
            foreach (PosePrototype poseNode in database_.Nodes.OfType<PosePrototype>())
                ValidatePoseNode(poseNode);
            foreach (StateMachine stateMachineNode in database_.Nodes.OfType<StateMachine>())
                ValidateStateMachineNode(stateMachineNode);
        }

        private void ValidateNodeNameUniqueness()
        {
            IEnumerable<INode> query =
                from child in database_.Nodes
                where (child.GetType() != typeof(Output) &&
                       child.GetType().BaseType != typeof(SignalBase) &&
                       child.GetType() != typeof(TunnelParameter) &&
                       child.GetType() != typeof(TunnelEvent) &&
                       !(child is IOperator))
                select child;

            // Build a map of node name to all matching hierarchical node names
            var nodeNameToHierarchicalNamesMap = new Dictionary<string, List<string>>();
            foreach (INode node in query)
            {
                string nameLowerCase = node.Name.ToLower();// hashing is case insensitive, so names have to be unique on a case independent basis.
                if (!nodeNameToHierarchicalNamesMap.ContainsKey(nameLowerCase))// new entry
                {
                    nodeNameToHierarchicalNamesMap.Add(nameLowerCase, new List<string>());
                }

                string hierarchicalNodeName = Database.GetHierarchicalNodeName(database_, node);
                nodeNameToHierarchicalNamesMap[nameLowerCase].Add(hierarchicalNodeName);
            }

            // If we have more than one entry in any of the hierarchical name lists, we have a problem
            foreach (var nodeNameToHierarchicalNamesPair in nodeNameToHierarchicalNamesMap)
            {
                List<string> hierarchicalNames = nodeNameToHierarchicalNamesPair.Value;
                if (hierarchicalNames.Count > 1)
                {
                    StringBuilder errorBuilder = new StringBuilder();
                    errorBuilder.AppendFormat("There {0} nodes with the name '{1}'.  Node names must be unique across the network.  The nodes are at: ",
                        hierarchicalNames.Count, nodeNameToHierarchicalNamesPair.Key);
                    for(int hierarchicalNameIndex = 0 ; hierarchicalNameIndex < hierarchicalNames.Count ; ++hierarchicalNameIndex)
                    {
                        errorBuilder.AppendFormat("'{0}'", hierarchicalNames[hierarchicalNameIndex]);
                        if (hierarchicalNameIndex < hierarchicalNames.Count - 1)
                        {
                            errorBuilder.Append(", ");
                        }
                    }

                    LogError(errorBuilder.ToString());
                }
            }
        }

        private void ValidateControlParameters(IList<Signal> signals)
        {
            var controlParameterNameCountMap = new Dictionary<string, int>();

            foreach (Signal controlParameter in signals)
            {
                string controlParameterName = controlParameter.Name.ToLower();
                if (!controlParameterNameCountMap.ContainsKey(controlParameterName))
                {
                    controlParameterNameCountMap.Add(controlParameterName, 0);
                }

                ++controlParameterNameCountMap[controlParameterName];
            }

            foreach (var controlParameterNameCountPair in controlParameterNameCountMap)
            {
                if (controlParameterNameCountPair.Value > 1)
                {
                    LogError("There are {0} control parameters with the name '{1}'.  Control parameter names must be unique across the network.",
                        controlParameterNameCountPair.Value, controlParameterNameCountPair.Key);
                }
            }
        }

        private void ValidateSignalBase(SignalBase signalBase)
        {
            if (signalBase.Signal == null)
            {
                LogError("Unable to export a signal node with a ghost reference to a control parameter");
            }
        }

        private void ValidateAddOperatorNode(AddOperatorPrototype addOperatorNode)
        {
            ValidateOneIncomingConnection(addOperatorNode, "Add operator", addOperatorNode.Input0, "Input0");
            ValidateOneIncomingConnection(addOperatorNode, "Add operator", addOperatorNode.Input1, "Input1");

            ValidateOneOutgoingConnection(addOperatorNode, "Add operator", addOperatorNode.Result, "Result");
        }

        private void ValidateAdd2Node(AddPrototype add2Node)
        {
            ValidateOneIncomingConnection(add2Node, "Add 2", add2Node.Source0, "Source0");
            ValidateOneIncomingConnection(add2Node, "Add 2", add2Node.Source1, "Source1");
            if (add2Node.Weight.Input == PropertyInput.Curve)
                ReportInvalidCurveInputError(add2Node, "Add 2", add2Node.Weight, "Weight");
            else if (add2Node.Weight.Input == PropertyInput.Real)
                ValidateOneIncomingConnection(add2Node, "Add 2", add2Node.Weight, "Weight");
            if (add2Node.Filter.Input == PropertyInput.Filter)
                ValidateOneIncomingConnection(add2Node, "Add 2", add2Node.Filter, "Filter");
            else if (add2Node.Filter.Input == PropertyInput.Value)
                ValidateFilterValue(add2Node, "Add 2", add2Node.Filter, "Filter");

            ValidateOneOutgoingConnection(add2Node, "Add 2", add2Node.Result, "Result");
        }

        private void ValidateAddNNode(NWayAddPrototype addNNode)
        {
            for (int inputIndex = 0; inputIndex < addNNode.Inputs.Items.Count; ++inputIndex)
            {
                var item = addNNode.Inputs.Items[inputIndex];
                ValidateOneIncomingConnection(addNNode, "Add N", item.Input, string.Format("Source[{0}]", inputIndex));
                if (item.Weight.Input == PropertyInput.Real || item.Weight.Input == PropertyInput.Curve)
                    ValidateOneIncomingConnection(addNNode, "Add N", item.Weight, string.Format("Weight[{0}]", inputIndex));
                if (item.Filter.Input == PropertyInput.Filter)
                    ValidateOneIncomingConnection(addNNode, "Add N", item.Filter, string.Format("Filter[{0}]", inputIndex));
                else if (item.Filter.Input == PropertyInput.Value)
                    ValidateFilterValue(addNNode, "Add N", item.Filter, string.Format("Filter[{0}]", inputIndex));
            }

            if (addNNode.Filter.Input == PropertyInput.Filter)
                ValidateOneIncomingConnection(addNNode, "Add N", addNNode.Filter, "Filter");
            else if (addNNode.Filter.Input == PropertyInput.Value)
                ValidateFilterValue(addNNode, "Add N", addNNode.Filter, "Filter");
            if (addNNode.ZeroAsDest.Input == PropertyInput.Boolean)
                ValidateOneIncomingConnection(addNNode, "Add N", addNNode.ZeroAsDest, "ZeroAsDest");

            ValidateOneOutgoingConnection(addNNode, "Add N", addNNode.Result, "Result");

            ValidateTrackGroup(addNNode, "Add N", addNNode.TrackGroup);
        }

        private void ValidateAnimationNode(AnimationPrototype animationNode)
        {
            if (animationNode.Animation.Input == PropertyInput.Animation)
                ValidateOneIncomingConnection(animationNode, "Animation", animationNode.Animation, "Animation");
            if (animationNode.Phase.Input == PropertyInput.Real || animationNode.Phase.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(animationNode, "Animation", animationNode.Phase, "Phase");
            if (animationNode.Rate.Input == PropertyInput.Real || animationNode.Rate.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(animationNode, "Animation", animationNode.Rate, "Rate");
            if (animationNode.Delta.Input == PropertyInput.Real || animationNode.Delta.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(animationNode, "Animation", animationNode.Delta, "Delta");
            if (animationNode.Looped.Input == PropertyInput.Boolean)
                ValidateOneIncomingConnection(animationNode, "Animation", animationNode.Looped, "Looped");
            if (animationNode.Absolute.Input == PropertyInput.Boolean)
                ValidateOneIncomingConnection(animationNode, "Animation", animationNode.Absolute, "Absolute");
 
            ValidateOneOutgoingConnection(animationNode, "Animation", animationNode.Result, "Result");
        }

        private void ValidateBlend2Node(BlendPrototype blend2Node)
        {
            ValidateOneIncomingConnection(blend2Node, "Blend 2", blend2Node.Source0, "Source0");
            ValidateOneIncomingConnection(blend2Node, "Blend 2", blend2Node.Source1, "Source1");
            if (blend2Node.Weight.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(blend2Node, "Blend 2", blend2Node.Weight, "Weight");
                //ReportInvalidCurveInputError(blend2Node, "Blend 2", blend2Node.Weight, "Weight");
            else if (blend2Node.Weight.Input == PropertyInput.Real)
                ValidateOneIncomingConnection(blend2Node, "Blend 2", blend2Node.Weight, "Weight");
            if (blend2Node.Filter.Input == PropertyInput.Filter)
                ValidateOneIncomingConnection(blend2Node, "Blend 2", blend2Node.Filter, "Filter");
            else if (blend2Node.Filter.Input == PropertyInput.Value)
                ValidateFilterValue(blend2Node, "Blend 2", blend2Node.Filter, "Filter");

            ValidateOneOutgoingConnection(blend2Node, "Blend 2", blend2Node.Result, "Result");
        }

        private void ValidateBlendNNode(NWayBlendPrototype blendNNode)
        {
            for (int inputIndex = 0; inputIndex < blendNNode.Inputs.Items.Count; ++inputIndex )
            {
                var item = blendNNode.Inputs.Items[inputIndex];
                ValidateOneIncomingConnection(blendNNode, "Blend N", item.Input, string.Format("Source[{0}]", inputIndex));
                if (item.Weight.Input == PropertyInput.Real || item.Weight.Input == PropertyInput.Curve)
                    ValidateOneIncomingConnection(blendNNode, "Blend N", item.Weight, string.Format("Weight[{0}]", inputIndex));
                if (item.Filter.Input == PropertyInput.Filter)
                    ValidateOneIncomingConnection(blendNNode, "Blend N", item.Filter, string.Format("Filter[{0}]", inputIndex));
                else if (item.Filter.Input == PropertyInput.Value)
                    ValidateFilterValue(blendNNode, "Blend N", item.Filter, string.Format("Filter[{0}]", inputIndex));
            }

            if (blendNNode.Filter.Input == PropertyInput.Filter)
                ValidateOneIncomingConnection(blendNNode, "Blend N", blendNNode.Filter, "Filter");
            else if (blendNNode.Filter.Input == PropertyInput.Value)
                ValidateFilterValue(blendNNode, "Blend N", blendNNode.Filter, "Filter");
            if (blendNNode.ZeroAsDest.Input == PropertyInput.Boolean)
                ValidateOneIncomingConnection(blendNNode, "Blend N", blendNNode.ZeroAsDest, "ZeroAsDest");

            ValidateOneOutgoingConnection(blendNNode, "Blend N", blendNNode.Result, "Result");

            ValidateTrackGroup(blendNNode, "Blend N", blendNNode.TrackGroup);
        }

        private void ValidateCaptureNode(CapturePrototype captureNode)
        {
            ValidateOneIncomingConnection(captureNode, "Capture", captureNode.Result, "Result");
            if (captureNode.Frame.Input == PropertyInput.Frame)
                ValidateOneIncomingConnection(captureNode, "Frame", captureNode.Frame, "Frame");

            ValidateOneOutgoingConnection(captureNode, "Capture", captureNode.Result, "Result");
        }

        private void ValidateClipNode(ClipPrototype clipNode)
        {
            if (clipNode.Clip.Input == PropertyInput.Filename)
            {
                if (clipNode.Clip.Source == ClipSource.LocalFile)
                {
                    if (string.IsNullOrWhiteSpace(clipNode.Clip.Filename) || !System.IO.Path.IsPathRooted(clipNode.Clip.Filename))
                    {
                        LogError("Clip node '{0}' must reference a valid absolute clip path.", Database.GetHierarchicalNodeName(database_, clipNode), clipNode.Clip.Filename);
                    }
                    else
                    {
                        LogWarning("Clip node '{0}' references a local file at '{1}'.  This should only be the case when prototyping.",
                            Database.GetHierarchicalNodeName(database_, clipNode), clipNode.Clip.Filename);
                    }
                }
                else if (clipNode.Clip.Source == ClipSource.ClipDictionary)
                {
                    if (string.IsNullOrWhiteSpace(clipNode.Clip.ClipDictionary))
                    {
                        LogError("Clip node '{0}' must reference a valid clip dictionary.", Database.GetHierarchicalNodeName(database_, clipNode));
                    }
                    else if (string.IsNullOrWhiteSpace(clipNode.Clip.ClipDictionaryClip))
                    {
                        LogError("Clip node '{0}' must reference a valid clip from the dictionary '{1}'.",
                            Database.GetHierarchicalNodeName(database_, clipNode), clipNode.Clip.ClipDictionary);
                    }
                }
                else if (clipNode.Clip.Source == ClipSource.ClipSet)
                {
                    if (string.IsNullOrWhiteSpace(clipNode.Clip.AbsoluteClipSet))
                    {
                        LogError("Clip node '{0}' must reference a valid absolute clip set.", Database.GetHierarchicalNodeName(database_, clipNode));
                    }
                    else if (string.IsNullOrWhiteSpace(clipNode.Clip.AbsoluteClipSetClip))
                    {
                        LogError("Clip node '{0}' must reference a valid clip from the absolute clip set '{1}'.",
                            Database.GetHierarchicalNodeName(database_, clipNode), clipNode.Clip.AbsoluteClipSet);
                    }
                }
                else if (clipNode.Clip.Source == ClipSource.VariableClipSet)
                {
                    string variableClipSetName = "Default";
                    if (clipNode.Clip.VariableClipSet != null)
                        variableClipSetName = clipNode.Clip.VariableClipSet.Name;

                    if (string.IsNullOrWhiteSpace(clipNode.Clip.VariableClipSetClip))
                    {
                        LogError("Clip node '{0}' must reference a valid clip from the variable clip set '{1}'.",
                            Database.GetHierarchicalNodeName(database_, clipNode), variableClipSetName);
                    }
                }
            }

            if (clipNode.Clip.Input == PropertyInput.Clip)
                ValidateOneIncomingConnection(clipNode, "Clip", clipNode.Clip, "Clip");
            if (clipNode.Phase.Input == PropertyInput.Real || clipNode.Phase.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(clipNode, "Clip", clipNode.Phase, "Phase");
            if (clipNode.Rate.Input == PropertyInput.Real || clipNode.Rate.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(clipNode, "Clip", clipNode.Rate, "Rate");
            if (clipNode.Delta.Input == PropertyInput.Real || clipNode.Delta.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(clipNode, "Clip", clipNode.Delta, "Delta");
            if (clipNode.Looped.Input == PropertyInput.Boolean)
                ValidateOneIncomingConnection(clipNode, "Clip", clipNode.Looped, "Looped");

            if (clipNode.ZeroWeightSync.Input == PropertyInput.Boolean)
            {
                LogError("Clip node '{0}' has its input type set to 'Boolean', which isn't supported by runtime.", Database.GetHierarchicalNodeName(database_, clipNode));
            }
            if (clipNode.ZeroWeightSync.OutputEnabled)
            {
                LogError("Clip node '{0}' has its output enabled, which isn't supported by runtime.", Database.GetHierarchicalNodeName(database_, clipNode));
            }

            var signalsReferencedByThisClipNode = new Dictionary<string, int>();// Count up the signal references
            for (int clipEventMappingIndex = 0; clipEventMappingIndex < clipNode.TagEventToSignalProperties.Items.Count; ++clipEventMappingIndex)
            {
                var tagEventToSignalProperty = clipNode.TagEventToSignalProperties.Items[clipEventMappingIndex];

                if (tagEventToSignalProperty.TagEventName == TagEventToSignalLogicalProperty.InvalidSelectionText)
                {
                    LogError("Clip node '{0}' has a tag event at index {1} with an invalid tag event name.",
                        Database.GetHierarchicalNodeName(database_, clipNode), clipEventMappingIndex + 1);
                }
                else if (!tagEventToSignalProperty.AvailableEvents.Contains(tagEventToSignalProperty.TagEventName))// Invalid event name
                {
                    LogError("Clip node '{0}' has a tag event at index {1} with an invalid tag event name.",
                        Database.GetHierarchicalNodeName(database_, clipNode), clipEventMappingIndex + 1);
                }
                else if (tagEventToSignalProperty.SignalName == TagEventToSignalLogicalProperty.InvalidSelectionText)
                {
                    LogError("Clip node '{0}' has a tag event at index {1} with an invalid control parameter name.  Are there any signals of the appropriate type?",
                        Database.GetHierarchicalNodeName(database_, clipNode), clipEventMappingIndex + 1);
                }
                else if (!tagEventToSignalProperty.AvailableSignals.Contains(tagEventToSignalProperty.SignalName))// Invalid signal name
                {
                    LogError("Clip node '{0}' has a tag event at index {1} with an invalid control parameter name.  Has the signal name changed?",
                        Database.GetHierarchicalNodeName(database_, clipNode), clipEventMappingIndex + 1);
                }
                else
                {
                    if (!signalsReferencedByThisClipNode.ContainsKey(tagEventToSignalProperty.SignalName))
                    {
                        signalsReferencedByThisClipNode.Add(tagEventToSignalProperty.SignalName, 1);
                    }
                    else
                    {
                        ++signalsReferencedByThisClipNode[tagEventToSignalProperty.SignalName];
                    }
                }
            }

            foreach (var tagSignalMapEntry in signalsReferencedByThisClipNode)
            {
                if (tagSignalMapEntry.Value > 1)
                {
                    LogError("Clip node '{0}' has more than one tag event mapped to the control parameter '{1}'.",
                        Database.GetHierarchicalNodeName(database_, clipNode), tagSignalMapEntry.Key);
                }
            }

            ValidateOneOutgoingConnection(clipNode, "Clip", clipNode.Result, "Result");
        }

        private void ValidateCurveNode(CurvePrototype curveNode)
        {
            ValidateOneIncomingConnection(curveNode, "Curve", curveNode.Input, "Input");

            ValidateAtLeastOneOutgoingConnection(curveNode, "Curve", curveNode.Output, "Output");

            ValidateTrackGroup(curveNode, "Curve", curveNode.TrackGroup);
        }

        private void ValidateExpressionNode(ExpressionPrototype expressionNode)
        {
            ValidateOneIncomingConnection(expressionNode, "Expression", expressionNode.Result, "Result");
            if (expressionNode.Expressions.Input == PropertyInput.Expressions)
                ValidateOneIncomingConnection(expressionNode, "Expression", expressionNode.Expressions, "Expressions");
            if (expressionNode.Weight.Input == PropertyInput.Real || expressionNode.Weight.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(expressionNode, "Expression", expressionNode.Weight, "Weight");

            ValidateOneOutgoingConnection(expressionNode, "Expression", expressionNode.Result, "Result");
        }

        private void ValidateExtrapolateNode(ExtrapolatePrototype extrapolateNode)
        {
            if (extrapolateNode.Damping.Input == PropertyInput.Real || extrapolateNode.Damping.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(extrapolateNode, "Extrapolate", extrapolateNode.Damping, "Damping");
            if (extrapolateNode.DeltaTime.Input == PropertyInput.Real || extrapolateNode.DeltaTime.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(extrapolateNode, "Extrapolate", extrapolateNode.DeltaTime, "DeltaTime");

            ValidateOneOutgoingConnection(extrapolateNode, "Extrapolate", extrapolateNode.Result, "Result");
        }

        private void ValidateFilterNode(FilterPrototype filterNode)
        {
            ValidateOneIncomingConnection(filterNode, "Filter", filterNode.Result, "Result");
            if (filterNode.Filter.Input == PropertyInput.Filter)
                ValidateOneIncomingConnection(filterNode, "Filter", filterNode.Filter, "Filter");
            else if (filterNode.Filter.Input == PropertyInput.Value)
                ValidateFilterValue(filterNode, "Filter", filterNode.Filter, "Filter");

            ValidateOneOutgoingConnection(filterNode, "Filter", filterNode.Result, "Result");
        }

        private void ValidateFrameNode(FramePrototype frameNode)
        {
            if (frameNode.Frame.Input == PropertyInput.Frame)
                ValidateOneIncomingConnection(frameNode, "Frame", frameNode.Frame, "Frame");

            ValidateOneOutgoingConnection(frameNode, "Frame", frameNode.Result, "Result");
        }

        private void ValidateIdentityNode(IdentityPrototype identityNode)
        {
            ValidateOneOutgoingConnection(identityNode, "Identity", identityNode.Result, "Result");
        }

        private void ValidateInputNode(Input inputNode)
        {
            ValidateOneOutgoingConnection(inputNode, "Input", inputNode.InputTransform, "InputTransform");
        }

        private void ValidateInvalidNode(InvalidPrototype invalidNode)
        {
            ValidateOneOutgoingConnection(invalidNode, "Invalid", invalidNode.Result, "Result");
        }

        private void ValidateJointLimitNode(JointLimitPrototype jointLimitNode)
        {
            ValidateOneIncomingConnection(jointLimitNode, "Joint Limit", jointLimitNode.Result, "Result");

            ValidateOneOutgoingConnection(jointLimitNode, "Joint Limit", jointLimitNode.Result, "Result");
        }

        private void ValidateLogicInlineSMNode(LogicInlineSM logicInlineSMNode)
        {
            if (logicInlineSMNode.Children.Count == 0)
                LogError("Logic inline state machine node '{0}' has no child nodes.", Database.GetHierarchicalNodeName(database_, logicInlineSMNode));

            if (logicInlineSMNode.Default == null)
                LogError("Logic inline state machine node '{0}' has no default child.", Database.GetHierarchicalNodeName(database_, logicInlineSMNode));

            ValidateOneIncomingConnection(logicInlineSMNode, "Inline state machine", logicInlineSMNode.Result, "Result");

            ValidateOneOutgoingConnection(logicInlineSMNode, "Inline state machine", logicInlineSMNode.Result, "Result");
        }

        private void ValidateLogicLeafSMNode(LogicLeafSM logicLeafSMNode)
        {
            if (logicLeafSMNode.Children.Count == 0)
                LogError("Logic leaf state machine node '{0}' has no child nodes.", Database.GetHierarchicalNodeName(database_, logicLeafSMNode));

            if (logicLeafSMNode.Default == null)
                LogError("Logic leaf state machine node '{0}' has no default child.", Database.GetHierarchicalNodeName(database_, logicLeafSMNode));

            ValidateOneOutgoingConnection(logicLeafSMNode, "Leaf state machine", logicLeafSMNode.Result, "Result");
        }

        private void ValidateMerge2Node(MergePrototype merge2Node)
        {
            ValidateOneIncomingConnection(merge2Node, "Merge 2", merge2Node.Source0, "Source0");
            ValidateOneIncomingConnection(merge2Node, "Merge 2", merge2Node.Source1, "Source1");
            if (merge2Node.Filter.Input == PropertyInput.Filter)
                ValidateOneIncomingConnection(merge2Node, "Merge 2", merge2Node.Filter, "Filter");
            else if (merge2Node.Filter.Input == PropertyInput.Value)
                ValidateFilterValue(merge2Node, "Merge 2", merge2Node.Filter, "Filter");

            ValidateOneOutgoingConnection(merge2Node, "Merge 2", merge2Node.Result, "Result");
        }

        private void ValidateMergeNNode(NWayMergePrototype mergeNNode)
        {
            for (int inputIndex = 0; inputIndex < mergeNNode.Inputs.Items.Count; ++inputIndex)
            {
                var item = mergeNNode.Inputs.Items[inputIndex];
                ValidateOneIncomingConnection(mergeNNode, "Merge N", item.Input, string.Format("Source[{0}]", inputIndex));
                if (item.Filter.Input == PropertyInput.Filter)
                    ValidateOneIncomingConnection(mergeNNode, "Merge N", item.Filter, string.Format("Filter[{0}]", inputIndex));
                else if (item.Filter.Input == PropertyInput.Value)
                    ValidateFilterValue(mergeNNode, "Merge N", item.Filter, string.Format("Filter[{0}]", inputIndex));
            }

            if (mergeNNode.Filter.Input == PropertyInput.Filter)
                ValidateOneIncomingConnection(mergeNNode, "Merge N", mergeNNode.Filter, "Filter");
            else if (mergeNNode.Filter.Input == PropertyInput.Value)
                ValidateFilterValue(mergeNNode, "Merge N", mergeNNode.Filter, "Filter");
            if (mergeNNode.ZeroAsDest.Input == PropertyInput.Boolean)
                ValidateOneIncomingConnection(mergeNNode, "Merge N", mergeNNode.ZeroAsDest, "ZeroAsDest");

            ValidateOneOutgoingConnection(mergeNNode, "Merge N", mergeNNode.Result, "Result");

            ValidateTrackGroup(mergeNNode, "Merge N", mergeNNode.TrackGroup);
        }

        private void ValidateMotiontreeNode(Motiontree motiontreeNode)
        {
            string name = Database.GetHierarchicalNodeName(database_, motiontreeNode);
            if (motiontreeNode.EnterEvent.Enabled)
            {
                if (motiontreeNode.EnterEvent.SelectedEvent == null)
                    LogError("Motiontree node '{0}' has its entered event set to null.", name);
            }
            if (motiontreeNode.ExitEvent.Enabled)
            {
                if (motiontreeNode.ExitEvent.SelectedEvent == null)
                    LogError("Motiontree node '{0}' has its exited event set to null.", name);
            }

            if (motiontreeNode.TransitionOrder != null)
            {
                int conditionCount = 0;
                foreach (New.Move.Core.Transition transition in motiontreeNode.TransitionOrder)
                {
                    if (transition != null && transition.Conditions != null)
                        conditionCount += transition.Conditions.Count;
                }
                if (conditionCount > 32)
                    LogWarning("Motiontree node '{0}' has more than 32 conditions summed over each transition from the node, this will cause a runtime asset.", name);
            }

            var outputSource = motiontreeNode.FindOutputChild();

            if (outputSource == null)
                LogError("Motiontree node '{0}' has nothing connected to its output node.", name);

            ValidateTransitions(motiontreeNode);
        }

        private void ValidateMultiplyOperatorNode(MultiplyOperatorPrototype multiplyOperatorNode)
        {
            ValidateOneIncomingConnection(multiplyOperatorNode, "Multiply operator", multiplyOperatorNode.Input0, "Input0");
            ValidateOneIncomingConnection(multiplyOperatorNode, "Multiply operator", multiplyOperatorNode.Input1, "Input1");

            ValidateOneOutgoingConnection(multiplyOperatorNode, "Multiply operator", multiplyOperatorNode.Result, "Result");
        }

        private void ValidateNetworkNode(Network networkNode)
        {
            ValidateOneOutgoingConnection(networkNode, "Network", networkNode.Result, "Result");

            if (string.IsNullOrEmpty(networkNode.ParameterName.Value))
            {
                LogError("Network node '{0}' has an empty Parameter Name, which is invalid.", Database.GetHierarchicalNodeName(database_, networkNode));
            }
        }

        private void ValidateOutputNode(Output outputNode)
        {
            ValidateOneIncomingConnection(outputNode, "Output", outputNode.OutputTransform, "OutputTransform");   
        }

        private void ValidateParameterizedMotionNode(ParameterizedMotionPrototype pmNode)
        {
            if( pmNode.PM.Input == PropertyInput.ParameterizedMotion)
                ValidateOneIncomingConnection(pmNode, "Parameterized Motion", pmNode.PM, "Filter");
            if (pmNode.Phase.Input == PropertyInput.Real || pmNode.Phase.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(pmNode, "Parameterized Motion", pmNode.Phase, "Phase");
            if (pmNode.Rate.Input == PropertyInput.Real || pmNode.Rate.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(pmNode, "Parameterized Motion", pmNode.Rate, "Rate");
            if (pmNode.Delta.Input == PropertyInput.Real || pmNode.Delta.Input == PropertyInput.Curve)
                ValidateOneIncomingConnection(pmNode, "Parameterized Motion", pmNode.Delta, "Delta");

            ValidateOneOutgoingConnection(pmNode, "Parameterized Motion", pmNode.Result, "Result");
        }

        private void ValidatePoseNode(PosePrototype poseNode)
        {
            ValidateOneOutgoingConnection(poseNode, "Pose", poseNode.Result, "Result");
        }

        private void ValidateStateMachineNode(StateMachine stateMachineNode)
        {
            if (stateMachineNode.Children.Count == 0)
                LogError("State machine node '{0}' has no child nodes.", Database.GetHierarchicalNodeName(database_, stateMachineNode));

            if (stateMachineNode.Default == null)
                LogError("State machine node '{0}' has no default child.", Database.GetHierarchicalNodeName(database_, stateMachineNode));

            ValidateTransitions(stateMachineNode);
        }

        private void ValidateOneIncomingConnection(INode node, string nodeTypeName, IAnchor destinationAnchor, string anchorName)
        {
            Motiontree motiontreeParent = (Motiontree)database_.Find(node.Parent);

            var incomingConnections = motiontreeParent.Connections.Where(connection => connection.Dest == destinationAnchor);

            if (incomingConnections.Count() == 0)
                LogError("{0} node '{1}' has no incoming connections to anchor '{2}'.", nodeTypeName, Database.GetHierarchicalNodeName(database_, node), anchorName);
            else if (incomingConnections.Count() > 1)
                LogError("{0} node '{1}' has more than one incoming connection to anchor '{2}'.", nodeTypeName, Database.GetHierarchicalNodeName(database_, node), anchorName);
        }

        private void ValidateFilterValue(INode node, string nodeTypeName, FilterLogicalProperty filterProperty, string anchorName)
        {
            if (string.IsNullOrEmpty(filterProperty.FilterDictionary))
            {
                LogError("{0} node '{1}' has no filter dictionary set on property '{2}'.", nodeTypeName, Database.GetHierarchicalNodeName(database_, node), anchorName);
            }
            if (string.IsNullOrEmpty(filterProperty.FilterName))
            {
                LogError("{0} node '{1}' has no filter name set on property '{2}'.", nodeTypeName, Database.GetHierarchicalNodeName(database_, node), anchorName);
            }
        }

        private void ValidateOneOutgoingConnection(INode node, string nodeTypeName, IAnchor sourceAnchor, string anchorName)
        {
            Motiontree motiontreeParent = (Motiontree)database_.Find(node.Parent);
            int connectionCount = 0;
            int proxyConnectionCount = 0;
            foreach (Connection connection in motiontreeParent.Connections)
            {
                if (!object.ReferenceEquals(connection.Source, sourceAnchor))
                {
                    continue;
                }

                connectionCount++;
                if (connection.IsProxy)
                {
                    proxyConnectionCount++;
                }
            }

            if (connectionCount == 0)
            {
                LogError("{0} node '{1}' has no outgoing connections from anchor '{2}'.", nodeTypeName, Database.GetHierarchicalNodeName(database_, node), anchorName);
            }
            else if (connectionCount > 1)
            {
                if (node is Input && connectionCount - proxyConnectionCount == 1)
                {
                    return;
                }

                LogError("{0} node '{1}' has more than one outgoing connection from anchor '{2}'.", nodeTypeName, Database.GetHierarchicalNodeName(database_, node), anchorName);
            }
        }

        private void ValidateAtLeastOneOutgoingConnection(INode node, string nodeTypeName, IAnchor sourceAnchor, string anchorName)
        {
            Motiontree motiontreeParent = (Motiontree)database_.Find(node.Parent);

            var outgoingConnections = motiontreeParent.Connections.Where(connection => connection.Source == sourceAnchor);

            if (outgoingConnections.Count() == 0)
                LogError("{0} node '{1}' has no outgoing connections from anchor '{2}'.", nodeTypeName, Database.GetHierarchicalNodeName(database_, node), anchorName);
        }

        private void ReportInvalidCurveInputError(INode node, string nodeTypeName, IAnchor sourceAnchor, string anchorName)
        {
            LogError("{0} node '{1}' has invalid input type 'Curve' at anchor '{2}'.  " + 
                     "If you have a curve node as input you should select 'Real' as the input type.",
                     nodeTypeName, Database.GetHierarchicalNodeName(database_, node), anchorName);
        }

        private void ValidateTrackGroup(INode node, string nodeTypeName, RSG.TrackViewer.Data.TrackGroup trackGroup)
        {
            foreach (var track in trackGroup.Tracks)
            {
                var pointFrequencyMap = new Dictionary<double, uint>();
                foreach(var point in track.Points)
                {
                    if (pointFrequencyMap.ContainsKey(point.X))
                        pointFrequencyMap[point.X]++;
                    else
                        pointFrequencyMap.Add(point.X, 1);
                }

                foreach (var mapItem in pointFrequencyMap)
                {
                    if (mapItem.Value > 1)
                    {
                        // We have multiple points with the same x value, which leads to degenerate segments
                        LogError("{0} node '{1}' has {2} points with an x value of {3} on track '{4}'.  There should be exactly 1.",
                            nodeTypeName, Database.GetHierarchicalNodeName(database_, node), mapItem.Value, mapItem.Key, track.ShortName);
                    }
                }
            }
        }

        private void ValidateTransitions(ITransitional transitional)
        {
            foreach (var transition in transitional.TransitionOrder)
                ValidateTransition(transitional, transition);
        }

        private void ValidateTransition(ITransitional transitional, New.Move.Core.Transition transition)
        {
            if (transition.Content.FilterTemplate.Input == PropertyInput.Value)
            {
                if (string.IsNullOrEmpty(transition.Content.FilterTemplate.FilterDictionary))
                {
                    LogError("Node '{0}' is the source of a transition '{1}' that has no filter dictionary specified.",
                        Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
                }
                if (string.IsNullOrEmpty(transition.Content.FilterTemplate.FilterName))
                {
                    LogError("Node '{0}' is the source of a transition '{1}' that has no filter name specified.",
                        Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
                }
            }

            if (transition.Content.Duration.Input == PropertyInput.Real)
            {
                if (transition.Content.Duration.SelectedSignal == null)
                {
                    LogError("Node '{0}' is the source of a transition '{1}' that has a Real property current not set to a valid parameter.",
                        Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
                }
            }

            foreach (var condition in transition.Conditions)
            {
                ValidateTransitionCondition(transitional, transition, condition);
            }
        }

        private void ValidateTransitionCondition(ITransitional transitional, New.Move.Core.Transition transition, Condition condition)
        {
            if (condition.Content is AtEventCondition)
                ValidateAtEventConditionContent(transitional, transition, (AtEventCondition)condition.Content);
            else if (condition.Content is BoolEqualCondition)
                ValidateBoolEqualConditionContent(transitional, transition, (BoolEqualCondition)condition.Content);
            else if (condition.Content is IntCondition)
                ValidateIntConditionContent(transitional, transition, (IntCondition)condition.Content);
            else if (condition.Content is StringCondition)
                ValidateHashStringConditionContent(transitional, transition, (StringCondition)condition.Content);
            else if (condition.Content is GreaterThanCondition)
                ValidateGreaterThanConditionContent(transitional, transition, (GreaterThanCondition)condition.Content);
            else if (condition.Content is GreaterThanEqualCondition)
                ValidateGreaterThanEqualConditionContent(transitional, transition, (GreaterThanEqualCondition)condition.Content);
            else if (condition.Content is InRangeCondition)
                ValidateInRangeConditionContent(transitional, transition, (InRangeCondition)condition.Content);
            else if (condition.Content is LessThanCondition)
                ValidateLessThanConditionContent(transitional, transition, (LessThanCondition)condition.Content);
            else if (condition.Content is LessThanEqualCondition)
                ValidateLessThanEqualConditionContent(transitional, transition, (LessThanEqualCondition)condition.Content);
            else if (condition.Content is LifetimeGreaterThanCondition)
                ValidateLifetimeGreaterThanConditionContent(transitional, transition, (LifetimeGreaterThanCondition)condition.Content);
            else if (condition.Content is LifetimeLessThanCondition)
                ValidateLifetimeLessThanConditionContent(transitional, transition, (LifetimeLessThanCondition)condition.Content);
            else if (condition.Content is NotInRangeCondition)
                ValidateNotInRangeConditionContent(transitional, transition, (NotInRangeCondition)condition.Content);
            else if (condition.Content is OnFlagCondition)
                ValidateOnFlagConditionContent(transitional, transition, (OnFlagCondition)condition.Content);
            else if (condition.Content is OnRequestCondition)
                ValidateOnRequestConditionContent(transitional, transition, (OnRequestCondition)condition.Content);
        }

        private void ValidateAtEventConditionContent(ITransitional transitional, New.Move.Core.Transition transition, AtEventCondition condition)
        {
            if (condition.Event.Event == null)
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains an 'At event' condition with no 'Event' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateBoolEqualConditionContent(ITransitional transitional, New.Move.Core.Transition transition, BoolEqualCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateBooleanSignalConditionProperty(transitional, transition, "Bool equal", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains an 'Bool equal' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateIntConditionContent(ITransitional transitional, New.Move.Core.Transition transition, IntCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateIntSignalConditionProperty(transitional, transition, "Int", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains an 'Int' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateHashStringConditionContent(ITransitional transitional, New.Move.Core.Transition transition, StringCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateStringSignalConditionProperty(transitional, transition, "HashString", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains a 'String' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateGreaterThanConditionContent(ITransitional transitional, New.Move.Core.Transition transition, GreaterThanCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateRealSignalConditionProperty(transitional, transition, "Greater than", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains a 'Greater than' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateGreaterThanEqualConditionContent(ITransitional transitional, New.Move.Core.Transition transition, GreaterThanEqualCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateRealSignalConditionProperty(transitional, transition, "Greater than equal", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains a 'Greater than equal' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateInRangeConditionContent(ITransitional transitional, New.Move.Core.Transition transition, InRangeCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateRealSignalConditionProperty(transitional, transition, "In range", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains an 'In range' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateLessThanConditionContent(ITransitional transitional, New.Move.Core.Transition transition, LessThanCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateRealSignalConditionProperty(transitional, transition, "Less than", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains a 'Less than' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateLessThanEqualConditionContent(ITransitional transitional, New.Move.Core.Transition transition, LessThanEqualCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateRealSignalConditionProperty(transitional, transition, "Less than equal", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains a 'Less than equal' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateLifetimeGreaterThanConditionContent(ITransitional transitional, New.Move.Core.Transition transition, LifetimeGreaterThanCondition condition)
        {
            // No validation required
        }

        private void ValidateLifetimeLessThanConditionContent(ITransitional transitional, New.Move.Core.Transition transition, LifetimeLessThanCondition condition)
        {
            // No validation required
        }

        private void ValidateNotInRangeConditionContent(ITransitional transitional, New.Move.Core.Transition transition, NotInRangeCondition condition)
        {
            if (condition.Signal.Value != null)
            {
                ValidateRealSignalConditionProperty(transitional, transition, "Not in range", condition.Signal);
            }
            else
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains an 'Not in range' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateOnFlagConditionContent(ITransitional transitional, New.Move.Core.Transition transition, OnFlagCondition condition)
        {
            if (condition.Flag.Value == null)
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains an 'On flag' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateOnRequestConditionContent(ITransitional transitional, New.Move.Core.Transition transition, OnRequestCondition condition)
        {
            if (condition.Request.Value == null)
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains an 'On request' condition with no 'Signal' property specified.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name);
            }
        }

        private void ValidateBooleanSignalConditionProperty(ITransitional transitional, New.Move.Core.Transition transition, string conditionTypeText, SignalConditionProperty signalConditionProperty)
        {
            ValidateSignalConditionProperty(transitional, transition, conditionTypeText, signalConditionProperty, "Boolean", TagAttributeType.Bool);
        }

        private void ValidateIntSignalConditionProperty(ITransitional transitional, New.Move.Core.Transition transition, string conditionTypeText, SignalConditionProperty signalConditionProperty)
        {
            ValidateSignalConditionProperty(transitional, transition, conditionTypeText, signalConditionProperty, "Int", TagAttributeType.Int);
        }

        private void ValidateStringSignalConditionProperty(ITransitional transitional, New.Move.Core.Transition transition, string conditionTypeText, SignalConditionProperty signalConditionProperty)
        {
            ValidateSignalConditionProperty(transitional, transition, conditionTypeText, signalConditionProperty, "HashString", TagAttributeType.HashString);
        }

        private void ValidateRealSignalConditionProperty(ITransitional transitional, New.Move.Core.Transition transition, string conditionTypeText, SignalConditionProperty signalConditionProperty)
        {
            ValidateSignalConditionProperty(transitional, transition, conditionTypeText, signalConditionProperty, "Real", TagAttributeType.Float);
        }

        private void ValidateSignalConditionProperty(
            ITransitional transitional, 
            New.Move.Core.Transition transition, 
            string conditionTypeText, 
            SignalConditionProperty signalConditionProperty, 
            string propertyTypeText, 
            TagAttributeType tagAttributeType)
        {
            if (signalConditionProperty.Value.Type == "Tag")
            {
                if (!signalConditionProperty.TagTypes.Contains(signalConditionProperty.TagType))
                {
                    LogError("Node '{0}' is the source of a transition '{1}' that contains a '{2}' condition with an invalid 'Tag Type'.",
                        Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name, conditionTypeText);
                }
                else if (!signalConditionProperty.TagAttributes.Contains(signalConditionProperty.TagAttribute))
                {
                    LogError("Node '{0}' is the source of a transition '{1}' that contains a '{2}' condition with an invalid 'Tag Attribute'.",
                        Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name, conditionTypeText);
                }
                else
                {
                    try
                    {
                        ClipPropertyDefinition tagDef = ClipTagDefinitions.PropertyDescriptions[signalConditionProperty.TagType];
                        ClipAttributeDefinition attributeDef = tagDef.AttributeDefs.Where(att => att.Name == signalConditionProperty.TagAttribute).First();
                        if (attributeDef.Type != (long)tagAttributeType)
                        {
                            LogError("Node '{0}' is the source of a transition '{1}' that contains a '{2}' condition with an invalid Tag Attribute type.",
                                Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name, conditionTypeText);
                        }
                    }
                    catch (Exception)
                    {
                        LogError("Node '{0}' is the source of a transition '{1}' that contains a '{2}' condition with an invalid Tag Attribute type.",
                            Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name, conditionTypeText);
                    }
                }
            }
            else if (signalConditionProperty.Value.Type != propertyTypeText)
            {
                LogError("Node '{0}' is the source of a transition '{1}' that contains a '{2}' condition with an invalid 'Signal' property of type '{3}'.",
                    Database.GetHierarchicalNodeName(database_, (INode)transitional), transition.Name, conditionTypeText, signalConditionProperty.Value.Type);
            }
        }

        private void LogError(string format, params object[] args)
        {
            string message = string.Format(format, args);
            errors_.Add(message);
            LogFactory.ApplicationLog.Error(message);
        }

        private void LogWarning(string format, params object[] args)
        {
            string message = string.Format(format, args);
            warnings_.Add(message);
            LogFactory.ApplicationLog.Warning(message);
        }

        private enum TagAttributeType
        {
            Float = 1,
            Int = 2,
            Bool = 3,
            HashString = 12,
        };

        private Database database_;
        private List<string> errors_ = new List<string>();
        private List<string> warnings_ = new List<string>();
    }
}
