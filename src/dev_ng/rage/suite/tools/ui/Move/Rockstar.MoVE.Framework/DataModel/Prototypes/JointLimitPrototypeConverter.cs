﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(JointLimitPrototype), typeof(PropertyCollection))]
    internal class JointLimitPrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            JointLimitPrototype prototype = value as JointLimitPrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            return collection;
        }
    }
}