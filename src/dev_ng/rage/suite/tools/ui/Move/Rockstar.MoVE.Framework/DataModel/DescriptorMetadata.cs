﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework
{
    [MetadataAttribute]
    public class DescriptorMetadataAttribute : Attribute
    {
        public int Index { get; private set; }

        public DescriptorMetadataAttribute(int index)
        {
            Index = index;
        }
    }
}