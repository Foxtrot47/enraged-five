﻿using System;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using System.Linq;

using New.Move.Core;

namespace Rockstar.MoVE.Framework
{
    [Export("DataModel", typeof(IDataModel))]
    internal class FrameworkModel : IDataModel
    {
        [ImportMany("MotionTreeDescriptor")]
        Lazy<IDesc, IDescriptorMetadata>[] _MotionTreeDescriptors = null;

        public ObservableCollection<IDesc> MotionTreeDescriptors 
        {
            get
            {
                ObservableCollection<IDesc> items = new ObservableCollection<IDesc>();
                foreach (var desc in _MotionTreeDescriptors.OrderBy(i => i.Metadata.Index))
                {
                    items.Add(desc.Value);
                }

                return items;
            }
        }
        
        [ImportMany("StateMachineDescriptor")]
        Lazy<IDesc, IDescriptorMetadata>[] _StateMachineDescriptors = null;

        public ObservableCollection<IDesc> StateMachineDescriptors
        {
            get
            {
                ObservableCollection<IDesc> items = new ObservableCollection<IDesc>();
                foreach (var desc in _StateMachineDescriptors.OrderBy(i => i.Metadata.Index))
                {
                    items.Add(desc.Value);
                }

                return items;
            }
        }

        [ImportMany("ConditionDescriptor")]
        Lazy<IConditionDesc, IDescriptorMetadata>[] _ConditionDescriptors = null;

        public ObservableCollection<IConditionDesc> ConditionDescriptors
        {
            get
            {
                ObservableCollection<IConditionDesc> items = new ObservableCollection<IConditionDesc>();
                foreach (var desc in _ConditionDescriptors.OrderBy(i => i.Value.ListPosition))
                {
                    items.Add(desc.Value);
                }

                return items;
            }
        }

        [ImportMany("ValueConverter")]
        Lazy<IValueConverter, IValueConverterMetadata>[] _ValueConverters = null;

        public IValueConverter FindConverter(Type convertFrom, Type convertTo)
        {
            var converter = from item in _ValueConverters
                                where (item.Metadata.ConvertFrom.Equals(convertFrom) 
                                    && item.Metadata.ConvertTo.Equals(convertTo))
                                select new {item.Value};

            foreach (var c in converter)
            {
                return c.Value;
            }

            return null;
        }
    }
}