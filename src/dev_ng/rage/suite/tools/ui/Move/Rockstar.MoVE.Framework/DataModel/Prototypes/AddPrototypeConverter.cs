﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(AddPrototype), typeof(PropertyCollection))]
    internal class AddPrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            AddPrototype prototype = value as AddPrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Weight"] = prototype.Weight;
            properties["Filter"] = prototype.Filter;
            properties["Sync"] = prototype.Sync;
            properties["Source0Influence"] = prototype.Source0Influence;
            properties["Source1Influence"] = prototype.Source1Influence;
            properties["Source0Immutable"] = prototype.Source0Immutable;
            properties["Source1Immutable"] = prototype.Source1Immutable;
        
            return collection;
        }
    }
}