﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(ExpressionPrototype), typeof(PropertyCollection))]
    internal class ExpressionPrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            ExpressionPrototype prototype = value as ExpressionPrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Inputs"] = prototype.Inputs;
            properties["Expressions"] = prototype.Expressions;
            properties["Weight"] = prototype.Weight;

            return collection;
        }
    }
}