﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework
{
    [MetadataAttribute]
    public class ValueConverterMetadataAttribute : Attribute
    {
        public Type ConvertFrom { get; private set; }
        public Type ConvertTo { get; private set; }

        public ValueConverterMetadataAttribute(Type from, Type to)
        {
            ConvertFrom = from;
            ConvertTo = to;
        }
    }
}