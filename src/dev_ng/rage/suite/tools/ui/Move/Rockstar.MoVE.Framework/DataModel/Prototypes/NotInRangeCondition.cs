﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml;
using New.Move.Core;
using RSG.Base.Logging;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    /// <summary>
    /// Represents a condition that can be placed onto a transition that is valid when a
    /// specified real signal has a value outside of a specified range.
    /// </summary>
    [Serializable]
    public class NotInRangeCondition : ConditionBase, ISerializable
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Event"/> property.
        /// </summary>
        private const string SignalSerialisationTag = "Signal";

        /// <summary>
        /// The tag that is used to serialise/deserialise the
        /// <see cref="UpperValue"/> property.
        /// </summary>
        private const string UpperValueSerialisationTag = "UpperValue";

        /// <summary>
        /// The tag that is used to serialise/deserialise the
        /// <see cref="UpperValue"/> property.
        /// </summary>
        private const string LowerValueSerialisationTag = "LowerValue";

        /// <summary>
        /// The private field used for the <see cref="Signal"/> property.
        /// </summary>
        private SignalConditionProperty signal;

        /// <summary>
        /// The private field used for the <see cref="UpperValue"/> property.
        /// </summary>
        private RealConditionProperty upperValue;

        /// <summary>
        /// The private field used for the <see cref="LowerValue"/> property.
        /// </summary>
        private RealConditionProperty lowerValue;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="NotInRangeCondition"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the condition.
        /// </param>
        public NotInRangeCondition(string name)
            : base(name)
        {
            this.signal = new SignalConditionProperty();
            this.upperValue = 0.5;
            this.lowerValue = 0.5;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NotInRangeCondition"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public NotInRangeCondition(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.upperValue = new RealConditionProperty();
            this.lowerValue = new RealConditionProperty();
            this.signal = new SignalConditionProperty();
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case SignalSerialisationTag:
                        this.signal = (SignalConditionProperty)entry.Value;
                        break;
                    case UpperValueSerialisationTag:
                        this.upperValue = (RealConditionProperty)entry.Value;
                        break;
                    case LowerValueSerialisationTag:
                        this.lowerValue = (RealConditionProperty)entry.Value;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NotInRangeCondition"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="database">
        /// The root database the condition belongs to.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public NotInRangeCondition(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Deserialise(reader, database);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the signal property for this condition.
        /// </summary>
        public SignalConditionProperty Signal
        {
            get { return this.signal; }
        }

        /// <summary>
        /// Gets the upper value property for this condition.
        /// </summary>
        public RealConditionProperty UpperValue
        {
            get { return this.upperValue; }
        }

        /// <summary>
        /// Gets the lower value property for this condition.
        /// </summary>
        public RealConditionProperty LowerValue
        {
            get { return this.lowerValue; }
        }

        /// <summary>
        /// Gets the type descriptor that describes the condition.
        /// </summary>
        public override Type Descriptor
        {
            get { return typeof(NotInRangeDescriptor); }
        }

        /// <summary>
        /// Gets the xml export type name for this condition.
        /// </summary>
        public override string XMLExportTypeName
        {
            get { return "NotInRange"; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.GetSerializationData(info, context);

            info.AddValue(SignalSerialisationTag, this.Signal);
            info.AddValue(UpperValueSerialisationTag, this.UpperValue);
            info.AddValue(LowerValueSerialisationTag, this.LowerValue);
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <param name="parent">
        /// The parent for the cloned object.
        /// </param>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        public override ICondition Clone(Condition parent)
        {
            NotInRangeCondition clone = new NotInRangeCondition(this.Name);
            clone.signal = new SignalConditionProperty(this.Signal);
            clone.lowerValue = new RealConditionProperty(this.LowerValue);
            clone.upperValue = new RealConditionProperty(this.UpperValue);
            clone.Signal.Parent = parent;
            clone.LowerValue.Parent = parent;
            clone.UpperValue.Parent = parent;
            return clone;
        }

        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        public override void ExportToXML(PargenXmlNode parentNode)
        {
            parentNode.AppendTextElementNode("Parameter", this.Signal.Value.Name);
            if (this.Signal.Value.Type == "Tag")
            {
                parentNode.AppendTextElementNode("TagType", this.Signal.TagType);
                parentNode.AppendTextElementNode("TagAttribute", this.Signal.TagAttribute);
            }

            parentNode.AppendValueElementNode(
                "Upper", "value", this.UpperValue.Value.ToString());
            parentNode.AppendValueElementNode(
                "Lower", "value", this.LowerValue.Value.ToString());
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("notinrange");
            writer.WriteAttributeString(ConditionBase.NameSerialisationTag, this.Name);

            if (this.LowerValue != null)
            {
                writer.WriteAttributeString(
                    LowerValueSerialisationTag,
                    this.LowerValue.Value.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                writer.WriteAttributeString(LowerValueSerialisationTag, "0.5");
            }

            if (this.UpperValue != null)
            {
                writer.WriteAttributeString(
                    UpperValueSerialisationTag,
                    this.UpperValue.Value.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                writer.WriteAttributeString(UpperValueSerialisationTag, "0.5");
            }

            if (this.Signal != null && this.Signal.Value != null)
            {
                writer.WriteAttributeString(
                    SignalSerialisationTag,
                    this.Signal.Value.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.Signal.Value.Name);

                writer.WriteAttributeString("TagType", this.Signal.TagType);
                writer.WriteAttributeString("TagAttribute", this.Signal.TagAttribute);
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="database">
        /// The root database this condition belongs to.
        /// </param>
        private void Deserialise(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute(ConditionBase.NameSerialisationTag);
            this.lowerValue = double.Parse(reader.GetAttribute(LowerValueSerialisationTag));
            this.upperValue = double.Parse(reader.GetAttribute(UpperValueSerialisationTag));

            string signalId = reader.GetAttribute(SignalSerialisationTag);
            this.signal = new SignalConditionProperty();
            if (!string.IsNullOrEmpty(signalId))
            {
                this.Signal.Value = database.GetSignal(Guid.Parse(signalId));
                if (this.Signal.Value == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.Signal.Value = database.GetSignal(signalName);
                    if (this.Signal.Value == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}'. This needs fixing before a export is allowed.", signalId);
                    }
                }
            }

            this.Signal.TagType = reader.GetAttribute("TagType");
            this.Signal.TagAttribute = reader.GetAttribute("TagAttribute");
            reader.Skip();
        }
        #endregion
    } // New.Move.Core.NotInRangeCondition {Interface}
} // New.Move.Core {Namespace}
