﻿using System.ComponentModel.Composition;
using New.Move.Core;
using System;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ConditionDescriptor", typeof(IConditionDesc)), DescriptorMetadata(3)]
    [Serializable]
    public class GreaterThanEqualDescriptor : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "Greater Than Equal";
            public static int ListPosition = 4;
        }

        public string Name { get { return Const.Name; } }
        public int ListPosition { get { return Const.ListPosition; } }

        public ICondition Create()
        {
            return new GreaterThanEqualCondition(Const.Name);
        }
    }
}