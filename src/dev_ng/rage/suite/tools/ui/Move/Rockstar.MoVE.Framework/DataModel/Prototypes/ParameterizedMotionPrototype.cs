﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Serializable]
    public class ParameterizedMotionPrototype : Logic
    {
        static class ParameterOffset
        {
            public const uint ParameterizedMotion = 0;
            public const uint Phase = 1;
            public const uint Delta = 2;
            public const uint Rate = 3;
            public const uint Parameters = 4;
            public const uint Output = ushort.MaxValue;
        }

        [ParameterExport(ParameterOffset.ParameterizedMotion)]
        public ParameterizedMotionLogicalProperty PM { get; private set; }

        [ParameterExport(ParameterOffset.Phase)]
        public RealLogicalProperty Phase { get; private set; }

        [ParameterExport(ParameterOffset.Rate)]
        public RealLogicalProperty Rate { get; private set; }

        [ParameterExport(ParameterOffset.Delta)]
        public RealLogicalProperty Delta { get; private set; }

        [ParameterExport(ParameterOffset.Parameters)]
        public LogicalPropertyArray<RealLogicalProperty> Parameters { get; private set; }

        [ParameterExport(ParameterOffset.Output)]
        public ResultTransform Result { get; private set; }

        public ParameterizedMotionPrototype(IDesc desc)
            : base(desc)
        {
            PM = "";
            Phase = 1.0f;
            Rate = 1.0f;
            Delta = 0.0f;
            Parameters = new LogicalPropertyArray<RealLogicalProperty>();
            Parameters.Parent = this;

            Result = new ResultTransform(this);
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "ParameterizedMotionNode");

            itemNode.AppendTextElementNode("Name", Name);

            PM.ExportToXML(itemNode.AppendChild("PM"), motiontreeParent);
            Rate.ExportToXML(itemNode.AppendChild("Rate"), motiontreeParent);
            Phase.ExportToXML(itemNode.AppendChild("Phase"), motiontreeParent);
            Delta.ExportToXML(itemNode.AppendChild("Delta"), motiontreeParent);
            var parametersNode = itemNode.AppendChild("Parameters");
            foreach (RealLogicalProperty parameter in Parameters.Items)
            {
                parameter.ExportToXML(parametersNode.AppendChild("Item"), motiontreeParent);
            }
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new ParameterizedMotionPrototype(new ParameterizedMotionDescriptor());
            clone.AdditionalText = this.AdditionalText;
            clone.Database = this.Database;
            clone.Enabled = this.Enabled;
            clone.Name = this.Name;
            clone.Parent = this.Parent;
            clone.Position = this.Position;

            (clone as ParameterizedMotionPrototype).Delta = new RealLogicalProperty(this, this.Delta);
            (clone as ParameterizedMotionPrototype).Phase = new RealLogicalProperty(this, this.Phase);
            (clone as ParameterizedMotionPrototype).PM = new ParameterizedMotionLogicalProperty(this.PM);
            (clone as ParameterizedMotionPrototype).Rate = new RealLogicalProperty(this, this.Rate);

            foreach (RealLogicalProperty p in this.Parameters.Items)
            {
                (clone as ParameterizedMotionPrototype).Parameters.Items.Add(new RealLogicalProperty(this, p));
            }

            return (ISelectable)clone;
        }
                          
        /// <summary>
        /// Initialises a new instance of the <see cref="ParameterizedMotionPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public ParameterizedMotionPrototype(XmlReader reader, Database database, Guid parent)
            : this(new ParameterizedMotionDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("ParameterizedMotion");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.PM != null)
            {
                writer.WriteStartElement("PM");
                this.PM.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Phase != null)
            {
                writer.WriteStartElement("Phase");
                this.Phase.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Rate != null)
            {
                writer.WriteStartElement("Rate");
                this.Rate.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Delta != null)
            {
                writer.WriteStartElement("Delta");
                this.Delta.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Parameters != null)
            {
                writer.WriteStartElement("Parameters");
                foreach (RealLogicalProperty item in this.Parameters.Children)
                {
                    item.Serialise(writer);
                }
                writer.WriteEndElement();
            }
      
            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "PM") == 0)
                {
                    this.PM = new ParameterizedMotionLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Phase") == 0)
                {
                    this.Phase = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Rate") == 0)
                {
                    this.Rate = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Delta") == 0)
                {
                    this.Delta = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Parameters") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            this.Parameters.Items.Add(new RealLogicalProperty(reader, this.Database, this));
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new ResultTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Delta.Id = New.Move.Core.Database.GetId(map, this.Delta.Id);
            foreach (RealLogicalProperty parameter in this.Parameters.Items)
            {
                parameter.Id = New.Move.Core.Database.GetId(map, parameter.Id);
            }

            this.Phase.Id = New.Move.Core.Database.GetId(map, this.Phase.Id);
            this.PM.Id = New.Move.Core.Database.GetId(map, this.PM.Id);
            this.Rate.Id = New.Move.Core.Database.GetId(map, this.Rate.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Delta.Id);
            ids.Add(this.Parameters.Id);
            foreach (RealLogicalProperty parameter in this.Parameters.Items)
            {
                ids.Add(parameter.Id);
            }

            ids.Add(Phase.Id);
            ids.Add(PM.Id);
            ids.Add(Rate.Id);
            ids.Add(Result.Id);
            return ids;
        }        
    }
}