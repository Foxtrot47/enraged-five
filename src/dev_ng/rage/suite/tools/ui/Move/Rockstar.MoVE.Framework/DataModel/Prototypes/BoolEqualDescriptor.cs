﻿using System.ComponentModel.Composition;
using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ConditionDescriptor", typeof(IConditionDesc)), DescriptorMetadata((int)DescriptorGroup.Other)]
    public class BoolEqualDescriptor : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "Bool Equals";
            public static int ListPosition = 11;
        }

        public string Name { get { return Const.Name; } }
        public int ListPosition { get { return Const.ListPosition; } }

        public ICondition Create()
        {
            return new BoolEqualCondition(Const.Name);
        }
    }
}