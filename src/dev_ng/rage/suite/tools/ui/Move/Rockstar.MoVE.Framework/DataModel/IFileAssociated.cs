﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rockstar.MoVE.Framework
{
    public interface IFileAssociated
    {
        void SetAssociatedFilename(string filename);
    }
}
