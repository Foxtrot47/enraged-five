﻿using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using New.Move.Core;
using MoVE.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("MotionTreeDescriptor", typeof(IDesc)), DescriptorMetadata((int)DescriptorGroup.Base)]
    public class LeafDescriptor : ITransitionalDesc
    {
        static class Const
        {
            public static string Name = "State machine";
            public static int Id = 0;
            public static string Help = "State machine";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get; private set; }

        public Type ConstructType { get { return typeof(LogicLeafSM); } }

        public LeafDescriptor()
        {
            try
            {
                BitmapImage icon = new BitmapImage();
                icon.BeginInit();
                icon.StreamSource =
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                    "Rockstar.MoVE.Framework.Media.State0.png");
                icon.EndInit();

                Icon = icon;
            }
            catch
            {
                Icon = null;
            }
        }

        public ITransitional Create(Database database, Point offset)
        {
            return new LogicLeafSM(offset);
        }

        public INode Create(Database database)
        {
            return new LogicLeafSM();
        }
    }
}