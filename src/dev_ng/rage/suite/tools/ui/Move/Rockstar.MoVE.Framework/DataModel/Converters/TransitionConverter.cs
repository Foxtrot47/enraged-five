﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Converters
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(New.Move.Core.Transition), typeof(PropertyCollection))]
    internal class TransitionConverter : IValueConverter
    {
        public object Convert(object value)
        {
            New.Move.Core.Transition prototype = value as New.Move.Core.Transition;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Enabled"] = prototype.Content.Enabled;

            properties["Modifier"] = prototype.Content.Modifier;
            properties["Duration"] = prototype.Content.Duration;

            PropertyCategory conditions = new PropertyCategory();
            collection["Conditions"] = conditions;

            conditions[""] = prototype.Conditions;

            properties["Sync"] = prototype.Content.Sync;
            properties["FilterTemplate"] = prototype.Content.FilterTemplate;

            properties["Block update on transition"] = prototype.Content.BlockUpdateOnTransition;
            properties["Weight output"] = prototype.Content.TransitionWeightOutput;

            properties["Transitional"] = prototype.Content.Transitional;
            properties["Immutable"] = prototype.Content.Immutable;

            return collection;
        }
    }
}