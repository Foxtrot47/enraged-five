﻿using System;
using System.ComponentModel.Composition;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Converters
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(Motiontree), typeof(PropertyCollection))]
    internal class MotionTreeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            Motiontree prototype = value as Motiontree;

            PropertyCategory properties = new PropertyCategory();
            PropertyCollection collection = new PropertyCollection();
            PropertyCategory transitions = new PropertyCategory();
            collection["Properties"] = properties;
            collection["Transitions"] = transitions;
            transitions["Order"] = prototype.TransitionOrder;

            PropertyCategory events = new PropertyCategory();
            collection[Resources.Events] = events;

            events["Enter"] = prototype.EnterEvent;
            events["Exit"] = prototype.ExitEvent;
            properties["Defer Block Update"] = prototype.DeferBlockUpdate;
            return collection;
        }
    }
}