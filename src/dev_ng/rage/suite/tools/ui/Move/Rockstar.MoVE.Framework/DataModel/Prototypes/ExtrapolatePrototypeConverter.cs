﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(ExtrapolatePrototype), typeof(PropertyCollection))]
    internal class ExtrapolatePrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            ExtrapolatePrototype prototype = value as ExtrapolatePrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Damping"] = prototype.Damping;
            properties["Delta Time"] = prototype.DeltaTime;
            properties["Last Frame"] = prototype.LastFrame;
            properties["Delta Frame"] = prototype.DeltaFrame;

            return collection;
        }
    }
}