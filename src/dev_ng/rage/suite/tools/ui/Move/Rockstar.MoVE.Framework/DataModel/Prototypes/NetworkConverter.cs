﻿using System;
using System.ComponentModel.Composition;

using MoVE.Core;

namespace Rockstar.MoVE.Framework.DataModel
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(Network), typeof(PropertyCollection))]
    internal class NetworkConverter : IValueConverter
    {
        public object Convert(object value)
        {
            Network prototype = value as Network;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            
            collection[Resources.Properties] = properties;

            properties["File name"] = prototype.NetworkProp;
            
            return collection;
        }
    }
}
