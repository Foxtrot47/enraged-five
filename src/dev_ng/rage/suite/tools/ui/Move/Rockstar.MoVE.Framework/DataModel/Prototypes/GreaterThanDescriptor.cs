﻿using System.ComponentModel.Composition;
using New.Move.Core;
using System;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ConditionDescriptor", typeof(IConditionDesc)), DescriptorMetadata(2)]
    [Serializable]
    public class GreaterThanDescriptor : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "Greater Than";
            public static int ListPosition = 3;
        }

        public string Name { get { return Const.Name; } }
        public int ListPosition { get { return Const.ListPosition; } }

        public ICondition Create()
        {
            return new GreaterThanCondition(Const.Name);
        }
    }
}