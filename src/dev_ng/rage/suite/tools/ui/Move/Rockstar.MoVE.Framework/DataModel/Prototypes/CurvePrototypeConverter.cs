﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(CurvePrototype), typeof(PropertyCollection))]
    internal class CurvePrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            CurvePrototype prototype = value as CurvePrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            //properties["Outputs"] = prototype.Outputs;

            return collection;
        }
    }
}