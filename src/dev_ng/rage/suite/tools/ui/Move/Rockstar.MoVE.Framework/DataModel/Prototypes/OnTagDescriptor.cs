﻿using System.ComponentModel.Composition;
using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ConditionDescriptor", typeof(IConditionDesc)), DescriptorMetadata((int)DescriptorGroup.Other)]
    public class OnTagDescriptor : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "On Tag";
            public static int ListPosition = 1;
        }

        public string Name { get { return Const.Name; } }
        public int ListPosition { get { return Const.ListPosition; } }

        public ICondition Create()
        {
            return new OnTagCondition(Const.Name);
        }
    }
}