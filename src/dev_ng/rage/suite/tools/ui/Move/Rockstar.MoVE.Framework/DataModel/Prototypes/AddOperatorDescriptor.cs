﻿using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("MotionTreeDescriptor", typeof(IDesc)), DescriptorMetadata((int)DescriptorGroup.Operand)]
    public class AddOperatorDescriptor : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Add";
            public static int Id = 1;
            public static string Help = "Add";
            public static string Group = "Operator";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _paletteIcon; } }

        public Type ConstructType { get { return typeof(AddOperatorPrototype); } }

        public AddOperatorDescriptor()
        {
            try
            {
                // Load the palette icon embedded resource
                _paletteIcon = new BitmapImage();
                _paletteIcon.BeginInit();
                _paletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(
                    "Rockstar.MoVE.Framework.Media.AddOperator.png");
                _paletteIcon.EndInit();
            }
            catch
            {
                _paletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new AddOperatorPrototype(this);
        }

        protected BitmapImage _paletteIcon = null;
    }
}