﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("StateMachineDescriptor", typeof(IDesc)), DescriptorMetadata((int)DescriptorGroup.Base)]
    public class StateMachineDescriptor : ITransitionalDesc
    {
        static class Const
        {
            public static string Name = "StateMachine";
            public static int Id = 1;
            public static string Help = "StateMachine";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get; private set; }

        public Type ConstructType { get { return typeof(StateMachine); } }

        public StateMachineDescriptor()
        {
            try
            {
                BitmapImage icon = new BitmapImage();
                icon.BeginInit();
                icon.StreamSource =
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                    "Rockstar.MoVE.Framework.Media.StateMachine.png");
                icon.EndInit();

                Icon = icon;
            }
            catch
            {
                Icon = null;
            }
        }

        public ITransitional Create(Database database, Point offset)
        {
            return new StateMachine(database, offset);
        }

        public INode Create(Database database)
        {
            return new StateMachine(database);
        }
    }
}