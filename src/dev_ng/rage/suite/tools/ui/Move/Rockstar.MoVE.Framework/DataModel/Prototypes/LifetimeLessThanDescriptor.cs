﻿using System.ComponentModel.Composition;
using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ConditionDescriptor", typeof(IConditionDesc)), DescriptorMetadata((int)DescriptorGroup.Other)]
    public class LifetimeLessThanDescriptor : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "Lifetime Less Than";
            public static int ListPosition = 10;
        }

        public string Name { get { return Const.Name; } }
        public int ListPosition { get { return Const.ListPosition; } }

        public ICondition Create()
        {
            return new LifetimeLessThanCondition(Const.Name);
        }
    }
}