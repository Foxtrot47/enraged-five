﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    public class AnimationPrototype : Logic
    {
        static class Const
        {
            public const uint AnimationParameterExportOffset = 0;
            public const uint PhaseParameterExportOffset = 1;
            public const uint RateParameterExportOffset = 2;
            public const uint DeltaParameterExportOffset = 3;
            public const uint LoopedParameterExportOffset = 4;
            public const uint AbsoluteParameterExportOffset = 5;
            public const uint OutputParameterExportOffset = ushort.MaxValue;

            public const uint LoopedEventExportOffset = 0;
            public const uint EndedEventExportOffset = 1;
        }

        [ParameterExport(Const.AnimationParameterExportOffset)]
        public AnimationLogicalProperty Animation
        {
            get;
            private set;
        }

        [ParameterExport(Const.PhaseParameterExportOffset)]
        public RealLogicalProperty Phase
        {
            get;
            private set;
        }

        [ParameterExport(Const.RateParameterExportOffset)]
        public RealLogicalProperty Rate
        {
            get;
            private set;
        }

        [ParameterExport(Const.DeltaParameterExportOffset)]
        public RealLogicalProperty Delta
        {
            get;
            private set;
        }

        [ParameterExport(Const.LoopedParameterExportOffset)]
        public BooleanLogicalProperty Looped
        {
            get;
            private set;
        }

        [ParameterExport(Const.AbsoluteParameterExportOffset)]
        public BooleanLogicalProperty Absolute
        {
            get;
            private set;
        }

        [EventExport(Const.LoopedEventExportOffset)]
        public LogicalEventProperty AnimationLooped
        {
            get;
            private set;
        }

        [EventExport(Const.EndedEventExportOffset)]
        public LogicalEventProperty AnimationEnded
        {
            get;
            private set;
        }

        [ParameterExport(Const.OutputParameterExportOffset)]
        public ResultTransform Result
        {
            get;
            private set;
        }

        public AnimationPrototype(IDesc desc)
            : base(desc)
        {
            Animation = "";
            Phase = 0.0f;
            Rate = 1.0f;
            Delta = 0.0f;
            Looped = true;
            Absolute = false;

            AnimationLooped = new LogicalEventProperty();
            AnimationEnded = new LogicalEventProperty();

            Result = new ResultTransform(this);
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "AnimationNode");
            itemNode.AppendTextElementNode("Name", Name);
            Animation.ExportToXML(itemNode.AppendChild("Animation"), motiontreeParent);
            Phase.ExportToXML(itemNode.AppendChild("Phase"), motiontreeParent);
            Rate.ExportToXML(itemNode.AppendChild("Rate"), motiontreeParent);
            Delta.ExportToXML(itemNode.AppendChild("Delta"), motiontreeParent);
            Looped.ExportToXML(itemNode.AppendChild("Looped"), motiontreeParent);
            Absolute.ExportToXML(itemNode.AppendChild("Absolute"), motiontreeParent);
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new AnimationPrototype(new AnimationDescriptor());
            AnimationPrototype highClone = (AnimationPrototype)base.Clone(clone);

            highClone.Phase = new RealLogicalProperty(this, Phase);
            highClone.Rate = new RealLogicalProperty(this, Rate);
            highClone.Delta = new RealLogicalProperty(this, Delta);
            highClone.AnimationLooped = new LogicalEventProperty(AnimationLooped);
            highClone.AnimationEnded = new LogicalEventProperty(AnimationEnded);
            highClone.Animation = new AnimationLogicalProperty(Animation);

            highClone.Looped = new BooleanLogicalProperty(Looped);
            highClone.Absolute = new BooleanLogicalProperty(Absolute);

            highClone.Result = new ResultTransform(highClone);
            highClone.Tag = null;

            return (ISelectable)highClone;
        }
                      
        /// <summary>
        /// Initialises a new instance of the <see cref="AnimationPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public AnimationPrototype(XmlReader reader, Database database, Guid parent)
            : this(new AnimationDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Animation");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Animation != null)
            {
                writer.WriteStartElement("Animation");
                this.Animation.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Phase != null)
            {
                writer.WriteStartElement("Phase");
                this.Phase.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Rate != null)
            {
                writer.WriteStartElement("Rate");
                this.Rate.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Delta != null)
            {
                writer.WriteStartElement("Delta");
                this.Delta.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Looped != null)
            {
                writer.WriteStartElement("Looped");
                this.Looped.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Absolute != null)
            {
                writer.WriteStartElement("Absolute");
                this.Absolute.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.AnimationLooped != null)
            {
                writer.WriteStartElement("AnimationLooped");
                this.AnimationLooped.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.AnimationEnded != null)
            {
                writer.WriteStartElement("AnimationEnded");
                this.AnimationEnded.Serialise(writer);
                writer.WriteEndElement();
            }
            
            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Animation") == 0)
                {
                    this.Animation = new AnimationLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Phase") == 0)
                {
                    this.Phase = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Rate") == 0)
                {
                    this.Rate = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Delta") == 0)
                {
                    this.Delta = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Looped") == 0)
                {
                    this.Looped = new BooleanLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "AnimationLooped") == 0)
                {
                    this.AnimationLooped =  new LogicalEventProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "AnimationEnded") == 0)
                {
                    this.AnimationEnded = new LogicalEventProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new ResultTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Absolute.Id);
            ids.Add(this.Animation.Id);
            ids.Add(this.AnimationEnded.Id);
            ids.Add(this.AnimationLooped.Id);
            ids.Add(this.Delta.Id);
            ids.Add(this.Looped.Id);
            ids.Add(this.Phase.Id);
            ids.Add(this.Rate.Id);
            ids.Add(this.Result.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Absolute.Id = New.Move.Core.Database.GetId(map, this.Absolute.Id);
            this.Animation.Id = New.Move.Core.Database.GetId(map, this.Animation.Id);
            this.AnimationEnded.Id = New.Move.Core.Database.GetId(map, this.AnimationEnded.Id);
            this.AnimationLooped.Id = New.Move.Core.Database.GetId(map, this.AnimationLooped.Id);
            this.Looped.Id = New.Move.Core.Database.GetId(map, this.Looped.Id);
            this.Phase.Id = New.Move.Core.Database.GetId(map, this.Phase.Id);
            this.Rate.Id = New.Move.Core.Database.GetId(map, this.Rate.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
        }
    }
}