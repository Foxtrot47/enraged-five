﻿namespace Rockstar.MoVE.Framework
{
    public enum DescriptorGroup
    {
        Base = 0,
        Operand,
        Blend2,
        BlendN,
        Merge2,
        MergeN,
        Add2,
        AddN,
        Other
    }
}