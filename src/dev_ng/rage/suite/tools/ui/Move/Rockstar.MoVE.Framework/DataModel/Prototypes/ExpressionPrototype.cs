﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;
using System.Globalization;
using System.Windows;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Serializable]
    public class ExpressionPrototype : Logic
    {
        static class ParameterOffset
        {
            public const uint Expressions = 0;
            public const uint Weight = 1;
            public const uint InputFilter = 2;
            public const uint Output = ushort.MaxValue;
        }

        [ParameterExport(new uint[] { ParameterOffset.Weight, ParameterOffset.InputFilter })]
        public NamedFloatVariableArray Inputs { get; private set; }

        [ParameterExport(ParameterOffset.Expressions)]
        public ExpressionsLogicalProperty Expressions { get; private set; }

        [ParameterExport(ParameterOffset.Weight)]
        public RealLogicalProperty Weight { get; private set; }

        [ParameterExport(ParameterOffset.Output)]
        public PassthroughTransform Result { get; private set; }

        public ExpressionPrototype(IDesc desc)
            : base(desc)
        {
            Inputs = new NamedFloatVariableArray();
            Inputs.Parent = this;

            Expressions = "";
            Weight = 0.0f;

            Result = new PassthroughTransform(this);
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "ExpressionNode");

            itemNode.AppendTextElementNode("Name", Name);

            foreach (Connection connection in motiontreeParent.Connections)
            {
                if (connection.Dest == Result)
                {
                    itemNode.AppendTextElementNode("Source", connection.Source.Parent.Name);
                    break;
                }
            }

            Expressions.ExportToXML(itemNode.AppendChild("Expressions"), motiontreeParent);
            Weight.ExportToXML(itemNode.AppendChild("Weight"), motiontreeParent);

            var inputsNode = itemNode.AppendChild("Variables");
            foreach (NamedFloatVariable item in Inputs.Items)
            {
                var inputItemNode = inputsNode.AppendChild("Item");

                item.NamedVariable.ExportToXML(inputItemNode, motiontreeParent);
            }
        }

        public ExpressionPrototype(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            //  Set the description up
            this.Desc = new ExpressionDescriptor();
            //  Set the data here, the rest is handled by the base class
            this.Expressions = (ExpressionsLogicalProperty)info.GetValue("Expressions", typeof(ExpressionsLogicalProperty));
            this.Weight = (RealLogicalProperty)info.GetValue("Weight", typeof(RealLogicalProperty));
            this.Result = (PassthroughTransform)info.GetValue("Result", typeof(PassthroughTransform));
            //this.Tag = (object)info.GetValue("Tag", typeof(object));

            this.Inputs = (NamedFloatVariableArray)info.GetValue("Inputs", typeof(NamedFloatVariableArray));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //  Set the data for this class
            info.AddValue("Expressions", this.Expressions);
            info.AddValue("Weight", this.Weight);
            info.AddValue("Result", this.Result);
            //info.AddValue("Tag", this.Tag);

            info.AddValue("Inputs", this.Inputs);

            //  The rest is set in the base class
            base.GetObjectData(info, context);
        }

        public override ISelectable Clone(Logic clone)
        {
            clone = new ExpressionPrototype(new ExpressionDescriptor());
            ExpressionPrototype highClone = (ExpressionPrototype)base.Clone(clone);

            highClone.Expressions = new ExpressionsLogicalProperty(Expressions);
            highClone.Weight = new RealLogicalProperty(this, Weight);
            highClone.Result = new PassthroughTransform(highClone);

            highClone.Tag = null;

            foreach (NamedFloatVariable namedVariable in this.Inputs.Items)
            {
                NamedFloatVariable item = new NamedFloatVariable(namedVariable);
                highClone.Inputs.Items.Add(item);
                item.Parent = highClone;
                item.NamedVariable.Parent = highClone;
            }

            return (ISelectable)highClone;
        }
             
        /// <summary>
        /// Initialises a new instance of the <see cref="ExpressionPrototype"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public ExpressionPrototype(XmlReader reader, Database database, Guid parent)
            : this(new ExpressionDescriptor())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Expression");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Expressions != null)
            {
                writer.WriteStartElement("Expressions");
                this.Expressions.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Weight != null)
            {
                writer.WriteStartElement("Weight");
                this.Weight.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Inputs != null)
            {
                writer.WriteStartElement("Inputs");
                foreach (NamedFloatVariable item in this.Inputs.Children)
                {
                    writer.WriteStartElement("NamedFloatVariable");
                    item.Serialise(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }

            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Expressions") == 0)
                {
                    this.Expressions = new ExpressionsLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Weight") == 0)
                {
                    this.Weight = new RealLogicalProperty(reader, this.Database, this);
                }
                else if (string.CompareOrdinal(reader.Name, "Inputs") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            this.Inputs.Items.Add(new NamedFloatVariable(reader, this.Database, this));
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new PassthroughTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Expressions.Id);
            ids.Add(this.Inputs.Id);
            foreach (NamedFloatVariable input in this.Inputs.Items)
            {
                ids.AddRange(input.GetAllGuids());
            }

            ids.Add(this.Result.Id);
            ids.Add(this.Weight.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Expressions.Id = New.Move.Core.Database.GetId(map, this.Expressions.Id);
            this.Inputs.Id = New.Move.Core.Database.GetId(map, this.Inputs.Id);
            foreach (NamedFloatVariable input in this.Inputs.Items)
            {
                input.ResetIds(map);
            }

            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
            this.Weight.Id = New.Move.Core.Database.GetId(map, this.Weight.Id);
        }
    }
}