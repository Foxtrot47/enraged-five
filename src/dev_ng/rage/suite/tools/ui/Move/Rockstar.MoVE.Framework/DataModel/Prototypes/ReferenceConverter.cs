﻿using System;
using System.ComponentModel.Composition;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(Reference), typeof(PropertyCollection))]
    internal class ReferenceConverter : IValueConverter
    {
        public object Convert(object value)
        {
            Reference prototype = value as Reference;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["File Name"] = prototype.Filename;
            properties["Ref Requests"] = prototype.ReferenceRequestProperties;
            properties["Ref Flags"] = prototype.ReferenceFlagProperties;
            properties["Ref Signals"] = prototype.ReferenceSignalProperties;

            return collection;
        }
    }
}