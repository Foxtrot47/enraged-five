﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(MergePrototype), typeof(PropertyCollection))]
    internal class MergePrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            MergePrototype prototype = value as MergePrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Filter"] = prototype.Filter;
            properties["Sync"] = prototype.Sync;
            properties["Source0Influence"] = prototype.Source0Influence;
            properties["Source1Influence"] = prototype.Source1Influence;
            properties["Source0Immutable"] = prototype.Source0Immutable;
            properties["Source1Immutable"] = prototype.Source1Immutable;

            return collection;
        }
    }
}