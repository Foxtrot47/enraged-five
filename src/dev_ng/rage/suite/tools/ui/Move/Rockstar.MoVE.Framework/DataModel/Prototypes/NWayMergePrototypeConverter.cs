﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(NWayMergePrototype), typeof(PropertyCollection))]
    internal class NWayMergePrototypeConverter : IValueConverter
    {
        public object Convert(object value)
        {
            NWayMergePrototype prototype = value as NWayMergePrototype;

            PropertyCollection collection = new PropertyCollection();
            PropertyCategory properties = new PropertyCategory();
            collection[Resources.Properties] = properties;

            properties["Inputs"] = prototype.Inputs;
            properties["Filter"] = prototype.Filter;
            properties["Sync"] = prototype.Sync;
            properties["Zero As Dest"] = prototype.ZeroAsDest;

            return collection;
        }
    }
}