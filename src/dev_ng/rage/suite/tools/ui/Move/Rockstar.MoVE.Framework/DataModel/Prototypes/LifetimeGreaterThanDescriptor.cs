﻿using System.ComponentModel.Composition;
using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel.Prototypes
{
    [Export("ConditionDescriptor", typeof(IConditionDesc)), DescriptorMetadata(2)]
    public class LifetimeGreaterThanDescriptor : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "Lifetime Greater Than";
            public static int ListPosition = 9;
        }

        public string Name { get { return Const.Name; } }
        public int ListPosition { get { return Const.ListPosition; } }

        public ICondition Create()
        {
            return new LifetimeGreaterThanCondition(Const.Name);
        }
    }
}