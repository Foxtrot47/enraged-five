﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Rockstar.MoVE.Framework;

namespace Rockstar.MoVE.Components
{
    public class CurveCanvas : Canvas
    {
        protected override Size MeasureOverride(Size availableSize)
        {
            double width = 0d;
            double height = 0d;

            foreach (object obj in Children)
            {
                FrameworkElement child = obj as FrameworkElement;
                if (child != null)
                {
                    child.Measure(availableSize);
                    double x = GetTop(child) + child.DesiredSize.Height;
                    if (!double.IsInfinity(x) && !double.IsNaN(x))
                        width = Math.Max(width, x);
                    x = GetLeft(child) + child.DesiredSize.Width;
                    if (!double.IsInfinity(x) && !double.IsNaN(x))
                        height = Math.Max(height, x);
                }
            }

            return new Size(height, width);
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            foreach (UIElement child in InternalChildren)
            {
                ContentPresenter presenter = (ContentPresenter)child;
                if (presenter != null && presenter.Content is RenderableCurve)
                {
                    child.Arrange(new Rect(child.DesiredSize));
                    continue;
                }

                double left = Canvas.GetLeft(child);
                double top = Canvas.GetTop(child);
                Point canvasPoint = ToCanvas(top, left);
                child.Arrange(new Rect(canvasPoint, child.DesiredSize));
            }
            return arrangeSize;
        }

        Point ToCanvas(double lat, double lon)
        {
            return ToCanvas(lat, lon, ActualWidth, ActualHeight);
        }

        public static Point ToCanvas(double lat, double lon, double width, double height)
        {
            double x = lon * width;
            double y = height - lat * height;
            return new Point(x, y);
        }

        public static Point FromCanvas(double lat, double lon, double width, double height)
        {
            double x = lon / width;
            double y = 1.0 - lat / height;
            return new Point(x, y);
        }
    }
}