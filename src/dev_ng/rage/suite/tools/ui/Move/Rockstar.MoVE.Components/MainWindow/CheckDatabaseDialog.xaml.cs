﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using New.Move.Core;

namespace Rockstar.MoVE.Components.MainWindow
{
    /// <summary>
    /// Interaction logic for CheckDatabaseDialog.xaml
    /// </summary>
    public partial class CheckDatabaseDialog : Window
    {
        public CheckDatabaseDialog(Database database)
        {
            InitializeComponent();

            viewModel_ = new CheckDatabaseViewModel(database);
            DataContext = viewModel_;
        }

        private void OnClickFixIssues(object sender, RoutedEventArgs e)
        {
            viewModel_.FixDatabaseIssues();
            Close();
        }

        private void OnClickOK(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private CheckDatabaseViewModel viewModel_;
    }
}
