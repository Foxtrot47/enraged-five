﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Framework.DataModel.Prototypes;

using RSG.TrackViewer;

using RSG.TrackViewer.Data;

namespace Rockstar.MoVE.Components
{
    internal class RenderableCurve : INotifyPropertyChanged
    {
        public RenderableCurve(Curve curve)
        {
            Curve = curve;
            Curve.PropertyChanged += new PropertyChangedEventHandler(Curve_PropertyChanged);
        }

        internal void RemoveHandler()
        {
            Curve.PropertyChanged -= new PropertyChangedEventHandler(Curve_PropertyChanged);
        }

        internal Curve Curve = null;

        public Color Color
        {
            get
            {
                return Curve.Color;
            }
        }

        public bool Visible
        {
            get
            {
                return Curve.Visible;
            }
        }

        void Curve_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Color")
            {
                NotifyPropertyChanged("Color");
            }
            else
            if (e.PropertyName == "Visible")
            {
                NotifyPropertyChanged("Visible");
            }
        }

        public void CalculateGeometry(double width, double height)
        {
            PathSegmentCollection segments = new PathSegmentCollection();
            for (int i = 1; i < Curve.Points.Length; ++i)
            {
                Point point = CurveCanvas.ToCanvas(Curve.Points[i].Y, Curve.Points[i].X, width, height);
                segments.Add(new LineSegment(point, true));
            }
            Point start = CurveCanvas.ToCanvas(Curve.Points[0].Y, Curve.Points[0].X, width, height);
            PathFigure figure = new PathFigure(start, segments, false);
            Geometry = new PathGeometry(new PathFigure[] { figure });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        PathGeometry _Geometry = null;
        public PathGeometry Geometry
        {
            get
            {
                return _Geometry;
            }
            private set
            {
                _Geometry = value;
                NotifyPropertyChanged("Geometry");
            }
        }
    }

    public partial class CurvePrototypeEditor : UserControl
    {
        public CurvePrototypeEditor()
        {
            InitializeComponent();
            Items = new ObservableCollection<object>();
            Items.CollectionChanged += new NotifyCollectionChangedEventHandler(Items_CollectionChanged);

            DataContextChanged += new DependencyPropertyChangedEventHandler(OnDataContextChanged);

            Unloaded += new RoutedEventHandler(OnUnloaded);

            ItemsControl.MouseDown += new MouseButtonEventHandler(ItemsControl_MouseDown);

            ItemsControl.ItemsSource = Items;
            InvalidateVisual();
        }

        void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

            ICurveBound prototype = (ICurveBound)DataContext;
            if (prototype != null)
            {
                prototype.Curves.CollectionChanged -= new NotifyCollectionChangedEventHandler(Curves_CollectionChanged);
            }
            DataContext = e.NewValue;
            prototype = (ICurveBound)DataContext;
            if (prototype != null)
            {
                prototype.Curves.CollectionChanged += new NotifyCollectionChangedEventHandler(Curves_CollectionChanged);
                foreach (Curve curve in prototype.Curves)
                {
                    Curves.Add(new RenderableCurve(curve));
                }
            }
        }

        void OnUnloaded(object sender, RoutedEventArgs e)
        {
            foreach (RenderableCurve curve in Curves)
            {
                curve.RemoveHandler();
            }

            DataContext = null;
        }

        void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (CurveSegment point in e.NewItems.OfType<CurveSegment>())
                {
                    point.PropertyChanged += new PropertyChangedEventHandler(
                        delegate(object obj, PropertyChangedEventArgs args)
                        {
                            if (args.PropertyName == "Position")
                            {
                                InvalidateVisual();
                            }
                        }
                    );
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                InvalidateVisual();
            }
        }

        void ItemsControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    {
                        Point position = e.GetPosition(ItemsControl);
                        HitTestResult result = VisualTreeHelper.HitTest(ItemsControl, position);
                        if (result != null && result.VisualHit is FrameworkElement)
                        {
                            if ((result.VisualHit as FrameworkElement).DataContext is RenderableCurve)
                            {
                                RenderableCurve path = (RenderableCurve)((result.VisualHit as FrameworkElement).DataContext);
                                if (path != null)
                                {
                                    Point t = CurveCanvas.FromCanvas(position.Y, position.X, ItemsControl.ActualWidth, ItemsControl.ActualHeight);
                                    CurveSegment point = path.Curve.Add(t);
                                    int idx = 0;
                                    foreach (CurveSegment p in Items.OfType<CurveSegment>())
                                    {
                                        if (point.X < p.X)
                                        {
                                            break;
                                        }
                                        ++idx;
                                    }

                                    Items.Insert(idx, point);
                                    InvalidateVisual();
                                }
                            }
                        }
                    }
                    break;
                case MouseButton.Right:
                    {
                        Point position = e.GetPosition(ItemsControl);
                        HitTestResult result = VisualTreeHelper.HitTest(ItemsControl, position);
                        if (result != null && result.VisualHit is FrameworkElement)
                        {
                            object context = (result.VisualHit as FrameworkElement).DataContext;
                            if (context is CurveSegment)
                            {
                                CurveSegment point = (CurveSegment)context;
                                Curve path = point.Curve;
                                path.Remove(point);
                                Items.Remove(point);
                                InvalidateVisual();
                            }
                        }
                    }
                    break;
            }

            base.OnMouseDown(e);
        }

        void Curves_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (Curve curve in e.NewItems)
                {
                    Curves.Add(new RenderableCurve(curve));
                }
                InvalidateVisual();
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Curve curve in e.OldItems)
                {
                    RenderableCurve frc = Curves.Find(delegate(RenderableCurve rc) { return rc.Curve == curve; });
                    Curves.Remove(frc);
                    List<object> delList = new List<object>();
                    RenderableCurve parentCurve = null;
                    foreach (RenderableCurve item in Items.OfType<RenderableCurve>())
                    {
                        if(frc == item)
                        {
                            parentCurve = frc;
                            foreach (CurveSegment cs in (item as RenderableCurve).Curve.Points)
                                delList.Add(cs as object);
                        }
                    }
                    foreach (object obj in delList)
                        Items.Remove(obj);
                    Items.Remove(parentCurve as object);
                }
                InvalidateVisual();
            }
        }

        public ObservableCollection<object> Items { get; set; }
        List<RenderableCurve> Curves = new List<RenderableCurve>(); 

        protected override void OnRender(DrawingContext drawingContext)
        {  
            foreach (RenderableCurve path in Curves)
            {
                if (!Items.Contains(path))
                {
                    Items.Add(path);
                    foreach (CurveSegment p in path.Curve.Points)
                    {
                        Items.Add(p);
                    }
                }

                path.CalculateGeometry(ItemsControl.ActualWidth, ItemsControl.ActualHeight);
            }

            base.OnRender(drawingContext);
        }

        void Add_Click(object sender, RoutedEventArgs e)
        {
            CurvePrototype prototype = (CurvePrototype)DataContext;
            prototype.TrackGroup.Tracks.Add(new Track("Output00"));
        }

        void Remove_Click(object sender, RoutedEventArgs e)
        {
            if (CurveItems.SelectedIndex > 0)
            {
                CurvePrototype prototype = (CurvePrototype)DataContext;
                prototype.TrackGroup.Tracks.RemoveAt(CurveItems.SelectedIndex);
            }
        }
    }



}
