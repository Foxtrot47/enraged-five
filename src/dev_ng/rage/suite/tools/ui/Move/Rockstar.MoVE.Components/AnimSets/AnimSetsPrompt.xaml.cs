﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rockstar.MoVE.Components
{
    /// <summary>
    /// Interaction logic for AnimSets.xaml
    /// </summary>
    public partial class AnimSetsPrompt : Window
    {
        #region Constructors
        public AnimSetsPrompt()
        {
            InitializeComponent();
            cboAnimSet.SelectedIndex = 0;
            AnimSet = null;
        }
        #endregion // Constructors

        #region Properties
        public string AnimSet { get; set; }
        #endregion // Properties

        #region Events
        void OK_Click(object sender, RoutedEventArgs e)
        {
            AnimSet = cboAnimSet.Text;
            this.DialogResult = true;
        }

        void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        #endregion // Events
    }
}
