﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace Rockstar.MoVE.Components.CurveEditor
{
    public class CurveSegment : INotifyPropertyChanged
    {
        public CurveSegment(Point position)
        {
            Position = position;
        }

        public double X
        {
            get
            {
                return Position.X;
            }
            set
            {
                if (Position.X != value)
                {
                    Position = new Point(value, Position.Y);
                    NotifyPropertyChanged("X");
                    NotifyPropertyChanged("Position");
                }
            }
        }

        public double Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                if (Position.Y != value)
                {
                    Position = new Point(Position.X, value);
                    NotifyPropertyChanged("Y");
                    NotifyPropertyChanged("Position");
                }
            }
        }

        Point Position { get; set; }

        Color _Color;
        public Color Color
        {
            get
            {
                return _Color;
            }
            set
            {
                if (_Color != value)
                {
                    _Color = value;
                    NotifyPropertyChanged("Color");
                }
            }
        }

        bool _Visible = true;
        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                if (_Visible != value)
                {
                    _Visible = value;
                    NotifyPropertyChanged("Visible");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        internal Curve Curve = null;
    }
}