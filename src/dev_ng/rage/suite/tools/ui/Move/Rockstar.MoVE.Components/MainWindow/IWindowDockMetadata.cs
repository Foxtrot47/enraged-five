﻿using System;

using AvalonDock;

namespace Rockstar.MoVE.Components.MainWindow
{
    public interface IWindowDockMetadata
    {
        string Name { get; }
        WindowDockRegion Region { get; }
        WindowDockState State { get; }
    }
}