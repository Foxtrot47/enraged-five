﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Rockstar.MoVE.Components.UniqueRename
{
    /// <summary>
    /// Interaction logic for UniqueNameDialog.xaml
    /// </summary>
    public partial class UniqueNameDialog : Window, INotifyPropertyChanged
    {
        public UniqueNameDialog()
        {
            InitializeComponent();
        }

        public bool IgnoreCase
        {
            get;
            set;
        }

        public List<string> NameConflicts
        {
            get;
            set;
        }

        public bool CanClickOk
        {
            get { return canClickOk; }
            set
            {
                canClickOk = value;
                Notify("CanClickOk");
            }
        }
        private bool canClickOk;

        public string NewName
        {
            get { return newName; }
            set
            {
                bool setFocus = false;
                if (newName == null)
                    setFocus = true;

                newName = value;
                Notify("NewName");

                if (!ValidateName())
                {
                    CanClickOk = false;
                    TextInput.BorderBrush = Brushes.Red;
                }
                else
                {
                    CanClickOk = true;
                    TextInput.BorderBrush = Brushes.Black;
                }

                if (setFocus)
                {
                    this.TextInput.Focus();
                    this.TextInput.TextChanged += TextInput_TextChanged;
                }
            }
        }

        void TextInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.TextInput.SelectAll();
            this.TextInput.TextChanged -= TextInput_TextChanged;
        }
        private string newName;
    
        private bool ValidateName()
        {
            if (string.IsNullOrWhiteSpace(NewName))
            {
                TextInput.ToolTip = "Name cannot be empty.";
                return false;
            }

            if (this.NameConflicts != null)
            {
                foreach (string conflict in NameConflicts)
                {
                    if (string.Compare(conflict, NewName, IgnoreCase) == 0)
                    {
                        TextInput.ToolTip = "Name cannot be equal to a existing name.";
                        return false;
                    }
                }
            }

            TextInput.ToolTip = null;
            return true;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler  PropertyChanged;
        public void Notify(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
