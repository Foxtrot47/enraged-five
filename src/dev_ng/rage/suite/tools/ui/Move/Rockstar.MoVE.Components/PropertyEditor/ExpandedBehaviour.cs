﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Rockstar.MoVE.Components.PropertyEditor
{
    public static class ExpandedBehaviour
    {
        public static readonly DependencyProperty IsExpandedEnabledProperty =
            DependencyProperty.RegisterAttached("IsExpandedEnabled", typeof(bool), typeof(ExpandedBehaviour), new UIPropertyMetadata(false, OnIsExpandedEnabledPropertyChanged));
        public static readonly DependencyProperty IsExpandedProperty =
            DependencyProperty.RegisterAttached("IsExpanded", typeof(bool), typeof(ExpandedBehaviour), new UIPropertyMetadata(false, OnIsExpandedPropertyChanged));

        public static bool GetIsExpandedEnabled(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsExpandedEnabledProperty);
        }

        public static void SetIsExpandedEnabled(DependencyObject obj, bool value)
        {
            obj.SetValue(IsExpandedEnabledProperty, value);
        }

        public static bool GetIsExpanded(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsExpandedProperty);
        }

        public static void SetIsExpanded(DependencyObject obj, bool value)
        {
            obj.SetValue(IsExpandedProperty, value);
        }

        static void OnIsExpandedEnabledPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var element = obj as FrameworkElement;
            FrameworkContentElement contentElement = null;
            if (element == null)
            {
                contentElement = obj as FrameworkContentElement;
                if (contentElement == null)
                    return;
            }

            if (e.NewValue is bool == false)
            {
                return;
            }
            
            if ((bool)e.NewValue)
            {
                /*
                if (element != null)
                {
                    element.MouseLeftButtonDown += Element_MouseLeftButtonDown;
                }
                else
                {
                    contentElement.MouseLeftButtonDown += Element_MouseLeftButtonDown;
                }
                 */
            }
                /*
            else
            {
                if (element != null)
                {
                    element.MouseLeftButtonDown -= Element_MouseLeftButtonDown;
                }
                else
                {
                    contentElement.MouseLeftButtonDown -= Element_MouseLeftButtonDown;
                }
            }
                 */
        }

        static void OnIsExpandedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {

        }
    }
}