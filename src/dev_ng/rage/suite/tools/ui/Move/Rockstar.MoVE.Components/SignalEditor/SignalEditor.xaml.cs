﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using AvalonDock;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Services;

using Move.Core;
using New.Move.Core;

namespace Rockstar.MoVE.Components.SignalEditor
{
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Control Parameter Editor", WindowDockRegion.Right, WindowDockState.Hidden)]
    public partial class SignalEditor : DockableContent
    {
        public SignalEditor()
        {
            InitializeComponent();
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _ApplicationService = value;
                _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
            }
        }

        IApplicationService _ApplicationService;

        void CurrentDocumentChanged()
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            DataContext = database;
            ListView.ItemsSource = database.Signals;
        }

        void AddSignal_Click(object sender, RoutedEventArgs e)
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            if (database != null)
            {
                Signal signal = null;
                ComboBoxItem item = (ComboBoxItem)SelectionComboBox.SelectedItem;
                if (item != null && !String.IsNullOrEmpty(NameTextBox.Text))
                {
                    string selection = (string)item.Content;
                    if (selection == "Real")
                    {
                        signal = new RealSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Animation")
                    {
                        signal = new AnimationSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Clip")
                    {
                        signal = new ClipSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Expression")
                    {
                        signal = new ExpressionSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Filter")
                    {
                        signal = new FilterSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Frame")
                    {
                        signal = new FrameSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Parameterized Motion")
                    {
                        signal = new ParameterizedMotionSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Node")
                    {
                        signal = new NodeSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Boolean")
                    {
                        signal = new BooleanSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Int")
                    {
                        signal = new IntSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "HashString")
                    {
                        signal = new HashStringSignal() { Name = NameTextBox.Text };
                    }
                    else if (selection == "Tag")
                    {
                        signal = new TagSignal() { Name = NameTextBox.Text };
                    }

                    bool found = false;

                    foreach (Signal s in database.Signals)
                    {
                        if (s.Name == signal.Name && s.GetType() == signal.GetType())
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        MessageBox.Show("A parameter with that name and type already exists!");
                    }
                    else
                    {
                        database.Signals.Add(signal);
                    }
                }
            }
        }


        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Trace.WriteLine("Moved");
            if (!(sender is FrameworkElement) || (!(e.Source is Grid) && !(e.Source is ContentPresenter) && !(e.Source is TextBlock)))
                return;

            Signal signal = (sender as FrameworkElement).DataContext as Signal;
            Database db = DataContext as Database;
            if (signal == null || db == null)
                return;

            System.Diagnostics.Trace.WriteLine("Valid");
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                System.Diagnostics.Trace.Write("Left Mouse Down");
                DragObject drag = new DragObject(signal.Desc);
                DragDrop.DoDragDrop(ListView, drag, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }

        private void Grid_DragOver(object sender, DragEventArgs e)
        {
            if (!(sender is FrameworkElement))
                return;

            Signal signal = (sender as FrameworkElement).DataContext as Signal;
            Database db = DataContext as Database;
            if (signal == null || db == null)
                return;

            e.Effects = DragDropEffects.None;

            // If the DataObject contains description data, extract it.
            if (e.Data.GetDataPresent("Move.Core.DragObject"))
            {
                Move.Core.DragObject data = (Move.Core.DragObject)e.Data.GetData("Move.Core.DragObject");
                INode node = data.Desc.Create(DataContext as Database);
                if (node is New.Move.Core.SignalBase)
                {
                    Signal draggedSignal = (node as New.Move.Core.SignalBase).Signal;
                    if (draggedSignal != signal)
                    {
                        e.Effects = DragDropEffects.Move;
                    }
                }
            }
            e.Handled = true;
        }

        private void Grid_Drop(object sender, DragEventArgs e)
        {
            if (!(sender is FrameworkElement))
                return;

            Signal signal = (sender as FrameworkElement).DataContext as Signal;
            Database db = DataContext as Database;
            if (signal == null || db == null)
                return;

            e.Effects = DragDropEffects.None;

            // If the DataObject contains description data, extract it.
            if (e.Data.GetDataPresent("Move.Core.DragObject"))
            {
                Move.Core.DragObject data = (Move.Core.DragObject)e.Data.GetData("Move.Core.DragObject");
                INode node = data.Desc.Create(DataContext as Database);
                if (node is New.Move.Core.SignalBase)
                {
                    Signal draggedSignal = (node as New.Move.Core.SignalBase).Signal;
                    if (draggedSignal != signal)
                    {
                        int currentIndex = db.Signals.IndexOf(draggedSignal);
                        int newIndex = db.Signals.IndexOf(signal);
                        if (currentIndex != newIndex)
                            db.Signals.Move(currentIndex, newIndex);
                    }
                }
            }
            e.Handled = true;
        }
        
        void Remove_Click(object sender, RoutedEventArgs e)
        {
            Signal signal = (Signal)(sender as Button).DataContext;
            Database db = DataContext as Database;
            db.Signals.Remove(signal);
        }

        void MoveUp_Click(object sender, RoutedEventArgs e)
        {
            Signal signal = (Signal)(sender as Button).DataContext;
            Database db = DataContext as Database;
            int currentIndex = db.Signals.IndexOf(signal);
            if (currentIndex > 0)
            {
                db.Signals.Move(currentIndex, currentIndex - 1);
            }
        }

        void MoveDown_Click(object sender, RoutedEventArgs e)
        {
            Signal signal = (Signal)(sender as Button).DataContext;
            Database db = DataContext as Database;
            int currentIndex = db.Signals.IndexOf(signal);
            if (currentIndex + 1 < db.Signals.Count)
            {
                db.Signals.Move(currentIndex, currentIndex + 1);
            }
        }

        static Signal GetObjectDataFromPoint(ItemsControl source, Point point)
        {
            UIElement element = source.InputHitTest(point) as UIElement;
            if (element != null)
            {
                object data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue && !(element is Button))
                {
                    data = source.ItemContainerGenerator.ItemFromContainer(element);
                    if (data == DependencyProperty.UnsetValue)
                        element = VisualTreeHelper.GetParent(element) as UIElement;

                    if (element == source)
                        return null;
                }

                if (data != DependencyProperty.UnsetValue)
                    return data as Signal;
            }

            return null;
        }

        private void Slider_PreviewMouseMove(object sender, MouseEventArgs e)
        {

        }

        private void Remame_Click(object sender, RoutedEventArgs e)
        {
            Signal signal = (Signal)(sender as Button).DataContext;
            Database db = DataContext as Database;
            List<string> nameConflicts = new List<string>();
            if (db != null)
            {
                foreach (Signal s in db.Signals)
                {
                    if (s.GetType() == signal.GetType())
                        nameConflicts.Add(s.Name);
                }
            }
            nameConflicts.Remove(signal.Name);

            UniqueRename.UniqueNameDialog dialog = new UniqueRename.UniqueNameDialog();
            dialog.Title = "Rename Control Parameter";
            dialog.NewName = signal.Name;
            dialog.NameConflicts = nameConflicts;
            dialog.Owner = Application.Current.MainWindow;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if ((bool)dialog.ShowDialog())
            {
                signal.Name = dialog.NewName;
            }
        }

        private void CommandBinding_DeleteExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.ListView == null || this.ListView.SelectedItems == null
                || this.ListView.SelectedItems.Count == 0)
                return;
            
            Database db = DataContext as Database;
            if (db == null)
                return;

            List<Signal> removeList = (from s in this.ListView.SelectedItems.OfType<Signal>()
                                       select s).ToList();

            foreach (Signal signal in removeList)
            {
                db.Signals.Remove(signal);
            }
        }
    }
}
