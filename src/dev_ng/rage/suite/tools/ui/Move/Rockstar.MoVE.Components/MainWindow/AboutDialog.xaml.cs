﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Rockstar.MoVE.Components.MainWindow
{
    public partial class AboutDialog : Window
    {
        public string VersionString
        {
            get
            {
                return "Version " + New.Move.Core.DatabaseExportSerializer.GetVersionString() + 
                    "\n" + New.Move.Core.DatabaseExportSerializer.GetVersionSubString();
            }
        }

        public AboutDialog()
        {
            InitializeComponent();
            //  Set data context to this so I can use binding on VersionString
            this.DataContext = this;
        }

        public void DoModal()
        {
            ShowDialog();
        }

        void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
