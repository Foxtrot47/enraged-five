﻿using System;

namespace Rockstar.MoVE.Components.MainWindow
{
    public enum WindowDockRegion : uint
    {
        Document = 0,
        Left = 1,
        Right = 2,
        Bottom = 3,
    }
}