﻿using System;
using System.Globalization;
using System.Windows.Data;

using AvalonDock;

namespace Rockstar.MoVE.Components.MainWindow
{
    [ValueConversion(typeof(DockableContentState), typeof(bool))]
    public class DockableContentStateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DockableContentState state = (DockableContentState)value;
            
            switch (state)
            {
                case DockableContentState.Docked:
                    // fallthrough
                case DockableContentState.AutoHide:
                    // fallthrough
                case DockableContentState.DockableWindow:
                    // fallthrough
                case DockableContentState.FloatingWindow:
                    // fallthrough
                case DockableContentState.Document:
                    return true;
                case DockableContentState.Hidden:
                    return false;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool boolean = (bool)value;
            if (boolean != true)
            {
                return DockableContentState.Hidden;
            }
            else
            {
                return DockableContentState.DockableWindow;
            }
        }
    }
}