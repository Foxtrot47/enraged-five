﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using AvalonDock;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Services;

using Move.Core;
using New.Move.Core;

namespace Rockstar.MoVE.Components.EventEditor
{
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Event Editor", WindowDockRegion.Right, WindowDockState.Hidden)]
    public partial class EventEditor : DockableContent
    {
        public EventEditor()
        {
            InitializeComponent();
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _ApplicationService = value;
                _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
            }
        }

        IApplicationService _ApplicationService;
        
        void CurrentDocumentChanged()
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            DataContext = database;
            ListView.ItemsSource = database.Events;
        }

        void Event_Click(object sender, RoutedEventArgs e)
        {
            LogicEvent evt = (LogicEvent)(sender as Button).DataContext;
            evt.FireEvent();
        }

        void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            if (database != null)
            {
                if (!String.IsNullOrEmpty(NameTextBox.Text))
                {
                    LogicEvent evt = new LogicEvent(NameTextBox.Text);

                    bool found = false;

                    foreach (LogicEvent le in database.Events)
                    {
                        if (le.Name == evt.Name)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        MessageBox.Show("An event with that name already exists!");
                    }
                    else
                    {
                        database.Events.Add(evt);
                    }
                }
            }
        }

        void MoveUp_Click(object sender, RoutedEventArgs e)
        {
            LogicEvent evt = (LogicEvent)(sender as Button).DataContext;
            Database db = DataContext as Database;
            int currentIndex = db.Events.IndexOf(evt);
            if (currentIndex > 0)
            {
                db.Events.Move(currentIndex, currentIndex - 1);
            }
        }

        void MoveDown_Click(object sender, RoutedEventArgs e)
        {
            LogicEvent evt = (LogicEvent)(sender as Button).DataContext;
            Database db = DataContext as Database;
            int currentIndex = db.Events.IndexOf(evt);
            if (currentIndex + 1 < db.Events.Count)
            {
                db.Events.Move(currentIndex, currentIndex + 1);
            }
        }

        void Remove_Click(object sender, RoutedEventArgs e)
        {
            LogicEvent evt = (LogicEvent)(sender as Button).DataContext;
            Database db = DataContext as Database;
            db.Events.Remove(evt);
        }

        private void Remame_Click(object sender, RoutedEventArgs e)
        {
            LogicEvent evt = (LogicEvent)(sender as Button).DataContext;
            Database db = DataContext as Database;
            List<string> nameConflicts = new List<string>();
            if (db != null)
            {
                foreach (LogicEvent le in db.Events)
                {
                    nameConflicts.Add(le.Name);
                }
            }
            nameConflicts.Remove(evt.Name);

            UniqueRename.UniqueNameDialog dialog = new UniqueRename.UniqueNameDialog();
            dialog.Title = "Rename Event";
            dialog.NewName = evt.Name;
            dialog.NameConflicts = nameConflicts;
            dialog.Owner = Application.Current.MainWindow;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if ((bool)dialog.ShowDialog())
            {
                evt.Name = dialog.NewName;
            }
        }
    }
}
