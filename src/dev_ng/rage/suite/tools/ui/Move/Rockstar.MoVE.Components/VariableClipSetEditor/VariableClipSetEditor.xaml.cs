﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using AvalonDock;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Services;

using Move.Core;
using New.Move.Core;

namespace Rockstar.MoVE.Components.VariableClipSetEditor
{
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Variable ClipSet Editor", WindowDockRegion.Right, WindowDockState.Hidden)]
    public partial class VariableClipSetEditor : DockableContent
    {
        public VariableClipSetEditor()
        {
            InitializeComponent();
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _ApplicationService = value;
                _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
            }
        }

        IApplicationService _ApplicationService;

        void CurrentDocumentChanged()
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            DataContext = database;
            ListView.ItemsSource = database.VariableClipSets;
        }

        void VariableClipSet_Click(object sender, RoutedEventArgs e)
        {
            VariableClipSet clipSet = (VariableClipSet)(sender as Button).DataContext;
        }

        void AddVariableClipSet_Click(object sender, RoutedEventArgs e)
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            if (database != null)
            {
                if (!String.IsNullOrEmpty(NameTextBox.Text))
                {
                    VariableClipSet clipSet = new VariableClipSet(NameTextBox.Text);

                    bool found = false;

                    foreach (VariableClipSet v in database.VariableClipSets)
                    {
                        if (v.Name == clipSet.Name)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        MessageBox.Show("A variable clipset with that name already exists!");
                    }
                    else
                    {
                        database.VariableClipSets.Add(clipSet);
                    }
                }
            }
        }

        void Remove_Click(object sender, RoutedEventArgs e)
        {
            VariableClipSet clipSet = (VariableClipSet)(sender as Button).DataContext;
            Database db = DataContext as Database;
            db.VariableClipSets.Remove(clipSet);
        }

        private void MoveUp_Click(object sender, RoutedEventArgs e)
        {
            VariableClipSet flag = (VariableClipSet)(sender as Button).DataContext;
            Database db = DataContext as Database;
            if (flag == null || db == null)
                return;
            int currentIndex = db.VariableClipSets.IndexOf(flag);
            if (currentIndex <= 0)
                return;

            db.VariableClipSets.Move(currentIndex, currentIndex - 1);
        }

        private void MoveDown_Click(object sender, RoutedEventArgs e)
        {
            VariableClipSet flag = (VariableClipSet)(sender as Button).DataContext;
            Database db = DataContext as Database;
            if (flag == null || db == null)
                return;
            int currentIndex = db.VariableClipSets.IndexOf(flag);
            if (currentIndex == -1 || currentIndex + 1 >= db.VariableClipSets.Count)
                return;

            db.VariableClipSets.Move(currentIndex, currentIndex + 1);
        }

        private void Remame_Click(object sender, RoutedEventArgs e)
        {
            VariableClipSet flag = (VariableClipSet)(sender as Button).DataContext;
            Database db = DataContext as Database;
            List<string> nameConflicts = new List<string>();
            if (db != null)
            {
                foreach (VariableClipSet s in db.VariableClipSets)
                {
                    nameConflicts.Add(s.Name);
                }
            }
            nameConflicts.Remove(flag.Name);

            UniqueRename.UniqueNameDialog dialog = new UniqueRename.UniqueNameDialog();
            dialog.Title = "Rename Variable Clip Set";
            dialog.NewName = flag.Name;
            dialog.NameConflicts = nameConflicts;
            dialog.Owner = Application.Current.MainWindow;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if ((bool)dialog.ShowDialog())
            {
                flag.Name = dialog.NewName;
            }
        }
    }
}
