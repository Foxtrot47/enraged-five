﻿using System;

namespace Rockstar.MoVE.Components.MainWindow
{
    public enum WindowDockState : uint
    {
        Docked = 0,
        Hidden = 1,
        Floating = 2,
    }
}