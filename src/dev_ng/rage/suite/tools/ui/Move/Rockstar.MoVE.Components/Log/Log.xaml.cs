﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ComponentModel.Composition;
using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Services;

using RSG.Base.Logging;

using AvalonDock;

namespace Rockstar.MoVE.Components.Log
{
    /// <summary>
    /// Interaction logic for Log.xaml
    /// </summary>
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Log", WindowDockRegion.Bottom)]
    public partial class Log : DockableContent
    {
        //  Message prefixes and colours
        private const String m_InfoPrefix = "Info: ";
        private const String m_ErrorPrefix = "Error: ";
        private const String m_WarningPrefix = "Warning: ";
        private const String m_DebugPrefix = "Debug: ";
        private SolidColorBrush m_DefaultColour = Brushes.White;
        private SolidColorBrush m_InfoColour = Brushes.SkyBlue;
        private SolidColorBrush m_ErrorColour = Brushes.Red;
        private SolidColorBrush m_WarningColour = Brushes.Yellow;
        private SolidColorBrush m_DebugColour = Brushes.SkyBlue;
        //  FlowDocument to display in RichTextBox
        FlowDocument m_FlowDocument;

        public Log()
        {
            InitializeComponent();
            //  Add our own even handlers for the log messages
            LogFactory.ApplicationLog.LogMessage += this.LogMessage;
            LogFactory.ApplicationLog.LogError += this.LogError;
            LogFactory.ApplicationLog.LogToolException += this.LogError;
            LogFactory.ApplicationLog.LogWarning += this.LogWarning;
            LogFactory.ApplicationLog.LogDebug += this.LogDebug;
            //  Create the FlowDocument to be displayed in the RichTextBox
            m_FlowDocument = new FlowDocument();
            richTB.Document = m_FlowDocument;
            //  Clear log even handler
            (Application.Current.MainWindow as Rockstar.MoVE.Components.MainWindow.MainWindow).LogNeedsClearing += new EventHandler(Log_LogNeedsClearing);
        }

        void Log_LogNeedsClearing(object sender, EventArgs e)
        {
            //  Clear the FlowDocument blocks
            m_FlowDocument.Blocks.Clear();
        }

        private void LogMessage(Object sender, LogMessageEventArgs e)
        {
            AddToLog(m_InfoPrefix, m_InfoColour, e.Message);
        }

        private void LogError(Object sender, LogMessageEventArgs e)
        {
            AddToLog(m_ErrorPrefix, m_ErrorColour, e.Message);
        }

        private void LogWarning(Object sender, LogMessageEventArgs e)
        {
            AddToLog(m_WarningPrefix, m_WarningColour, e.Message);
        }

        private void LogDebug(Object sender, LogMessageEventArgs e)
        {
#if DEBUG
            AddToLog(m_DebugPrefix, m_DebugColour, e.Message);
#endif  //  DEBUG
        }

        private void AddToLog(string prefix, SolidColorBrush colour, string msg)
        {
            //  Create a new paragraph for this message
            Paragraph paragraph = new Paragraph();
            //  Add the date and time
            paragraph.Inlines.Add(DateTime.Now.ToString() + " ");
            //  Add a run of text for the prefix, set to the correct colour
            Run r = new Run(prefix + "\t");
            r.Foreground = colour;
            paragraph.Inlines.Add(new Bold(r));
            //  Add the rest of the actual message
            paragraph.Inlines.Add(msg + Environment.NewLine);
            //  Add the paragraph to the document blocks
            m_FlowDocument.Blocks.Add(paragraph);

            //  Auto scroll to bottom
            richTB.ScrollToEnd();
        }

        ISelectionService _SelectionService = null;

        [Import(typeof(ISelectionService))]
        ISelectionService SelectionService
        {
            set
            {
                _SelectionService = value;
                _SelectionService.CurrentSelectionChanged += new CurrentSelectionChangedHandler(SelectionService_CurrentSelectionChanged);
            }
        }

        [Import("DataModel")]
        IDataModel DataModel { get; set; }

        void SelectionService_CurrentSelectionChanged()
        {
            
        }
    }
}
