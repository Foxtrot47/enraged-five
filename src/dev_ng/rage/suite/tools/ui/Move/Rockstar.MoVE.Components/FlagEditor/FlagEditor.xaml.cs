﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using AvalonDock;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Services;

using Move.Core;
using New.Move.Core;

namespace Rockstar.MoVE.Components.FlagEditor
{
    /*
    //[Export("WindowDock", typeof(Pane)), WindowDockMetadata("Flag Editor", 0)]
    internal class FlagEditorPane : DockablePane
    {
        public FlagEditorPane()
        {
            _Content = new FlagEditor();
            Items.Add(_Content);
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _Content.InjectService(value);
            }
        }

        FlagEditor _Content;
    }
     */

    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Flag Editor", WindowDockRegion.Right, WindowDockState.Hidden)]
    public partial class FlagEditor : DockableContent
    {
        public FlagEditor()
        {
            InitializeComponent();
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _ApplicationService = value;
                _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
            }
        }

        IApplicationService _ApplicationService;

        /*
        internal void InjectService(IApplicationService service)
        {
            _ApplicationService = service;
            _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
        }
         */

        void CurrentDocumentChanged()
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            DataContext = database;
            ListView.ItemsSource = database.Flags;
        }

        void AddFlag_Click(object sender, RoutedEventArgs e)
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            if (database != null)
            {
                if (!String.IsNullOrEmpty(NameTextBox.Text))
                {
                    Flag flag = new Flag(NameTextBox.Text);

                    bool found = false;

                    foreach (Flag f in database.Flags)
                    {
                        if (f.Name == flag.Name)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        MessageBox.Show("A flag with that name already exists!");
                    }
                    else
                    {
                        database.Flags.Add(flag);
                    }

                    
                }
            }
        }

        void Remove_Click(object sender, RoutedEventArgs e)
        {
            Flag flag = (Flag)(sender as Button).DataContext;
            Database db = DataContext as Database;
            db.Flags.Remove(flag);
        }

        private void MoveUp_Click(object sender, RoutedEventArgs e)
        {
            Flag flag = (Flag)(sender as Button).DataContext;
            Database db = DataContext as Database;
            if (flag == null || db == null)
                return;
            int currentIndex = db.Flags.IndexOf(flag);
            if (currentIndex <= 0)
                return;

            db.Flags.Move(currentIndex, currentIndex - 1);
        }

        private void MoveDown_Click(object sender, RoutedEventArgs e)
        {
            Flag flag = (Flag)(sender as Button).DataContext;
            Database db = DataContext as Database;
            if (flag == null || db == null)
                return;
            int currentIndex = db.Flags.IndexOf(flag);
            if (currentIndex == -1 || currentIndex + 1 >= db.Flags.Count)
                return;

            db.Flags.Move(currentIndex, currentIndex + 1);
        }

        private void Remame_Click(object sender, RoutedEventArgs e)
        {
            Flag flag = (Flag)(sender as Button).DataContext;
            Database db = DataContext as Database;
            List<string> nameConflicts = new List<string>();
            if (db != null)
            {
                foreach (Flag f in db.Flags)
                {
                    nameConflicts.Add(f.Name);
                }
            }
            nameConflicts.Remove(flag.Name);

            UniqueRename.UniqueNameDialog dialog = new UniqueRename.UniqueNameDialog();
            dialog.Title = "Rename Flag";
            dialog.NewName = flag.Name;
            dialog.NameConflicts = nameConflicts;
            dialog.Owner = Application.Current.MainWindow;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if ((bool)dialog.ShowDialog())
            {
                flag.Name = dialog.NewName;
            }
        }
    }
}
