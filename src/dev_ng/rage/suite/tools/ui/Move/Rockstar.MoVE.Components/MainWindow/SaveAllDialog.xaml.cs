﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Rockstar.MoVE.Components.MainWindow
{
    internal enum SaveAllResult
    {
        Yes,
        No,
        Cancel,
    }

    public partial class SaveAllDialog : Window
    {
        public SaveAllDialog(IEnumerable<string> items)
        {
            InitializeComponent();
            Items.ItemsSource = items;
        }

        internal SaveAllResult DoModal()
        {
            Result = SaveAllResult.Cancel;
            ShowDialog();
            return Result;
        }

        internal SaveAllResult Result { get; private set; }

        void Yes_Click(object sender, RoutedEventArgs e)
        {
            Result = SaveAllResult.Yes;
            Close();
        }

        void No_Click(object sender, RoutedEventArgs e)
        {
            Result = SaveAllResult.No;
            Close();
        }

        void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
