﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Framework.DataModel.Prototypes;

using RSG.AnimClipEditor.View;
using RSG.PlaybackControl;
using RSG.AnimClipEditor;
using RSG.AnimClipEditor.DataModel;
using RSG.AnimClipEditor.ViewModel;

namespace Rockstar.MoVE.Components
{
    public partial class ClipPrototypeEditor : UserControl
    {
        public ClipPrototypeEditor()
        {
            InitializeComponent();
        }
    }

    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(ClipPrototype), typeof(Control))]
    internal class ClipPrototypeEditorConverter : IValueConverter, IPostValueConverter
    {
        ClipEditor currentClipEditor;
        private bool reConnectClip = false;

        public object Convert(object value)
        {
            ClipPrototype prototype = value as ClipPrototype;
            prototype.ClipSourceChanged += new EventHandler(prototype_ClipSourceChanged);
            prototype.InitManagedClip();

            if (currentClipEditor == null)
            {
                currentClipEditor = new ClipEditor(prototype.ManagedClip);
                prototype.ClipViewModel = currentClipEditor.DataContext as ClipViewModel;
            }
            else
            {
                if (prototype.ClipViewModel == null)
                {
                    prototype.ClipViewModel = new ClipViewModel(new ClipModel(prototype.ManagedClip));
                }

                if (reConnectClip)
                {
                    prototype.ClipViewModel.PushRAGValues();
                }

                currentClipEditor.DataContext = prototype.ClipViewModel;
            }

            if (prototype.Clip.Source != New.Move.Core.ClipSource.LocalFile)
            {
                this.currentClipEditor.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.currentClipEditor.Visibility = Visibility.Visible;
            }

            return currentClipEditor;
        }

        public void PostConvert(object value, object newValue)
        {
            ClipPrototype prototype = value as ClipPrototype;
            prototype.ClipSourceChanged -= new EventHandler(prototype_ClipSourceChanged);

            if (currentClipEditor == null)
                return;

            object dataContext = currentClipEditor.DataContext;
            if (!(dataContext is RSG.AnimClipEditor.ViewModel.ClipViewModel))
                return;

            this.reConnectClip = false;
            if (newValue is ClipPrototype)
            {
                ClipViewModel viewModel = dataContext as RSG.AnimClipEditor.ViewModel.ClipViewModel;
                this.reConnectClip = viewModel.IsConnected;
                viewModel.Dispose(false);
            }
            else
            {
                (dataContext as RSG.AnimClipEditor.ViewModel.ClipViewModel).Dispose(false);
            }
        }

        private object ClipViewModel(ClipViewModel clipViewModel)
        {
            throw new NotImplementedException();
        }

        void prototype_ClipSourceChanged(object sender, EventArgs e)
        {
            ClipPrototype prototype = sender as ClipPrototype;
            if (prototype.Clip.Source != New.Move.Core.ClipSource.LocalFile)
            {
                if (this.currentClipEditor != null)
                    this.currentClipEditor.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (this.currentClipEditor != null)
                {
                    this.currentClipEditor.Visibility = Visibility.Visible;
                }
            }
        }
    }
}
