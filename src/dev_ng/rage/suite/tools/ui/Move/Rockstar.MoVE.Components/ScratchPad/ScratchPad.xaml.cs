﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Services;

using AvalonDock;

namespace Rockstar.MoVE.Components.ScratchPad
{
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Contextual Controls", WindowDockRegion.Bottom)]
    public partial class ScratchPad : DockableContent
    {
        public ScratchPad()
        {
            InitializeComponent();
        }

        ISelectionService _SelectionService = null;

        [Import(typeof(ISelectionService))]
        ISelectionService SelectionService
        {
            set
            {
                _SelectionService = value;
                _SelectionService.CurrentSelectionChanged += CurrentSelectionChanged;
            }
        }

        [Import("DataModel")]
        IDataModel DataModel { get; set; }

        private void CurrentSelectionChanged()
        {
            if (DataContext != null)
            {
                IValueConverter converter = DataModel.FindConverter(DataContext.GetType(), typeof(Control));
                if (converter != null && converter is IPostValueConverter)
                {
                    (converter as IPostValueConverter).PostConvert(DataContext, _SelectionService.CurrentSelection);
                }
            }

            DataContext = _SelectionService.CurrentSelection;
            Content = null;

            if (DataContext != null)
            {
                IValueConverter converter = DataModel.FindConverter(DataContext.GetType(), typeof(Control));
                if (converter != null)
                {
                    Content = converter.Convert(DataContext);
                }
            }
        }
    }
}
