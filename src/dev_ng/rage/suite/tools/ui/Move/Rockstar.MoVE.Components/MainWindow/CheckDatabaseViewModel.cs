﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using New.Move.Core;

namespace Rockstar.MoVE.Components.MainWindow
{
    internal class CheckDatabaseViewModel : INotifyPropertyChanged
    {
        internal CheckDatabaseViewModel(Database database)
        {
            database_ = database;
            databaseIntegrityChecker_ = new DatabaseIntegrityChecker();
            databaseIntegrityChecker_.CheckDatabase(database_);

            DatabaseCanBeFixed = databaseIntegrityChecker_.FoundIssues;

            if (databaseIntegrityChecker_.FoundIssues)
            {
                StringBuilder databaseCheckResultsBuilder = new StringBuilder();
                databaseCheckResultsBuilder.AppendLine(
                    "Problems were detected with the database in the current network and are listed below.  Click the 'Fix Issues' button below to automatically fix these problems.");
                databaseCheckResultsBuilder.AppendLine();
                foreach (string warning in databaseIntegrityChecker_.IssueDescriptions)
                {
                    databaseCheckResultsBuilder.AppendLine(warning);
                }

                DatabaseCheckResults = databaseCheckResultsBuilder.ToString();
            }
            else
            {
                DatabaseCheckResults = "There are no problems with this network's database.";
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public void FixDatabaseIssues()
        {
            databaseIntegrityChecker_.FixDatabase(database_);
        }

        private bool databaseCanBeFixed_;
        public bool DatabaseCanBeFixed
        {
            get { return databaseCanBeFixed_; }
            private set 
            {
                databaseCanBeFixed_ = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("DatabaseCanBeFixed"));
            }
        }

        private string databaseCheckResults_;
        public string DatabaseCheckResults
        {
            get { return databaseCheckResults_; }
            private set 
            { 
                databaseCheckResults_ = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("DatabaseCheckResults"));
            }
        }

        private Database database_;
        private DatabaseIntegrityChecker databaseIntegrityChecker_;
    }
}
