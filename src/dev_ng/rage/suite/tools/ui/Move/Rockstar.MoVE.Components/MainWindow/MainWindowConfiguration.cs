﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Configuration;

using Rockstar.MoVE.Services;

namespace Rockstar.MoVE.Components.MainWindow
{
    public class MainWindowConfigurationSection : ConfigurationSection 
    {
        public MainWindowConfigurationSection()
        {
            Width = 960;
            Height = 768;
        }

        [ConfigurationProperty("Width")]
        public double Width 
        {
            get
            {
                return (double)this["Width"];
            }
            set
            {
                this["Width"] = value;
            }
        }

        [ConfigurationProperty("Height")]
        public double Height
        {
            get
            {
                return (double)this["Height"];
            }
            set
            {
                this["Height"] = value;
            }
        }
    }

    [Export("Config", typeof(IConfiguration)), Export(typeof(MainWindowConfiguration)), 
        PartCreationPolicy(CreationPolicy.Shared),
        ConfigMetadata("MainWindow", typeof(MainWindowConfigurationSection))]
    internal class MainWindowConfiguration : IConfiguration, INotifyPropertyChanged
    {
        public MainWindowConfiguration()
        {
        }

        MainWindowConfigurationSection _Section = null;

        public override void OnInitialized()
        {
            Width = 960;
            Height = 768;
        }

        public override void Inject(ConfigurationSection section)
        {
            _Section = (MainWindowConfigurationSection)section;
            OnPropertyChanged("Width");
            OnPropertyChanged("Height");
        }

        public double Width 
        {
            get
            {
                if (_Section != null)
                {
                    return _Section.Width;
                }

                return 0;
            }
            set
            {
                if (_Section != null && _Section.Width != value)
                {
                    _Section.Width = value;
                    OnPropertyChanged("Width");
                }
            }
        }

        public double Height
        {
            get
            {
                if (_Section != null)
                {
                    return _Section.Height;
                }

                return 0;
            }
            set
            {
                if (_Section != null && _Section.Height != value)
                {
                    _Section.Height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}