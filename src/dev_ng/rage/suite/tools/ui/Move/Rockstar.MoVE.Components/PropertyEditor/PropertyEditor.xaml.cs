﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

using AvalonDock;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Framework.DataModel;
using Rockstar.MoVE.Framework.DataModel.Prototypes;
using Rockstar.MoVE.Services;

using RelativeSource = System.Windows.Data.RelativeSource;

using ConditionCollection = New.Move.Core.ConditionCollection;
using Condition = New.Move.Core.Condition;
using Transition = New.Move.Core.Transition;

using New.Move.Core;

namespace Rockstar.MoVE.Components.PropertyEditor
{
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Property Editor", WindowDockRegion.Right)]
    public partial class PropertyEditor : DockableContent
    {
        public PropertyEditor()
        {
            InitializeComponent();
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _ApplicationService = value;
                _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
            }
        }

        [Import(typeof(ISelectionService))]
        ISelectionService SelectionService
        {
            set
            {
                _SelectionService = value;
                _SelectionService.CurrentSelectionChanged += new CurrentSelectionChangedHandler(CurrentSelectionChanged);
            }
        }

        [Import("DataModel")]
        IDataModel DataModel
        {
            set
            {
                _DataModel = value;
                Conditions = _DataModel.ConditionDescriptors;
            }
        }

        IApplicationService _ApplicationService = null;
        ISelectionService _SelectionService = null;
 
        void CurrentDocumentChanged()
        {
            if (Database != null)
            {
                Database.Signals.CollectionChanged -= new System.Collections.Specialized.NotifyCollectionChangedEventHandler(OnSignalsCollectionChanged);
            }

            Database = _ApplicationService.CurrentDocument.Tag as Database;
            Database.Signals.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(OnSignalsCollectionChanged);
            ResetTypedSignalContainers();
        }

        private void ResetTypedSignalContainers()
        {
            TypedParameterContainerMap_ = new Dictionary<string, ObservableCollection<Signal>>();

            RegisterTypedSignalContainer("Animation");
            RegisterTypedSignalContainer("Boolean");
            RegisterTypedSignalContainer("Clip");
            RegisterTypedSignalContainer("Expressions");
            RegisterTypedSignalContainer("Filter");
            RegisterTypedSignalContainer("Frame");
            RegisterTypedSignalContainer("Node");
            RegisterTypedSignalContainer("Parameterized Motion");
            RegisterTypedSignalContainer("Real");
            RegisterTypedSignalContainer("Int");
            RegisterTypedSignalContainer("HashString");
            RegisterTypedSignalContainer("Reference");
            RegisterTypedSignalContainer("Tag");
        }

        private void RegisterTypedSignalContainer(string signalType)
        {
            var typedSignalContainer = new ObservableCollection<Signal>();
            foreach (var typedSignal in Database.Signals.Where(signal => signal.Type == signalType))
            {
                typedSignalContainer.Add(typedSignal);
            }

            TypedParameterContainerMap_.Add(signalType, typedSignalContainer);
        }

        void OnSignalsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Move)
            {
                string type = string.Empty;
                int oldIndex = -1;
                Signal oldItem = null;
                if (e.OldItems != null)
                {
                    foreach (Signal oldSignal in e.OldItems)
                    {
                        type = oldSignal.Type;
                        oldItem = oldSignal;
                        oldIndex = TypedParameterContainerMap_[type].IndexOf(oldSignal);
                        break;
                    }
                }

                if (!TypedParameterContainerMap_.ContainsKey(type) || oldItem == null || oldIndex == -1)
                    return;

                int newIndex = 0;
                bool found = false;
                foreach (var signal in Database.Signals)
                {
                    if (signal == oldItem)
                    {
                        found = true;
                        break;
                    }
                    if (signal.Type == type)
                    {
                        newIndex++;
                    }
                }
                if (oldIndex == newIndex || found == false)
                    return;

                TypedParameterContainerMap_[type].Move(oldIndex, newIndex);
            }
            else
            {
                if (e.NewItems != null)
                {
                    foreach (Signal newSignal in e.NewItems)
                    {
                        TypedParameterContainerMap_[newSignal.Type].Add(newSignal);
                    }
                }
                if (e.OldItems != null)
                {
                    foreach (Signal oldSignal in e.OldItems)
                    {
                        TypedParameterContainerMap_[oldSignal.Type].Remove(oldSignal);
                    }
                }
            }
        }

        IDataModel _DataModel = null;

        public ObservableCollection<IConditionDesc> Conditions { get; private set; }
        public Database Database { get; private set; }

        private Dictionary<string, ObservableCollection<Signal>> TypedParameterContainerMap_;// Observable signal collections, keyed on type

        public ObservableCollection<Signal> TagParameters
        {
            get { return TypedParameterContainerMap_["Tag"]; }
        }

        public ObservableCollection<Signal> AnimationParameters
        {
            get { return TypedParameterContainerMap_["Animation"]; }
        }

        public ObservableCollection<Signal> BooleanParameters
        {
            get { return TypedParameterContainerMap_["Boolean"]; }
        }

        public ObservableCollection<Signal> ClipParameters
        {
            get { return TypedParameterContainerMap_["Clip"]; }
        }

        public ObservableCollection<Signal> ExpressionsParameters
        {
            get { return TypedParameterContainerMap_["Expressions"]; }
        }

        public ObservableCollection<Signal> FilterParameters
        {
            get { return TypedParameterContainerMap_["Filter"]; }
        }

        public ObservableCollection<Signal> FrameParameters
        {
            get { return TypedParameterContainerMap_["Frame"]; }
        }

        public ObservableCollection<Signal> NodeParameters
        {
            get { return TypedParameterContainerMap_["Node"]; }
        }

        public ObservableCollection<Signal> ParameterizedMotionParameters
        {
            get { return TypedParameterContainerMap_["Parameterized Motion"]; }
        }

        public ObservableCollection<Signal> RealParameters
        {
            get { return TypedParameterContainerMap_["Real"]; }
        }

        public ObservableCollection<Signal> ReferenceParameters
        {
            get { return TypedParameterContainerMap_["Reference"]; }
        }

        public ObservableCollection<Signal> Parameters
        {
            get
            {
                return Database.Signals;
            }
        }

        public ObservableCollection<LogicEvent> Events
        {
            get
            {
                return Database.Events;
            }
        }

        public ObservableCollection<Flag> Flags
        {
            get
            {
                return Database.Flags;
            }
        }

        public ObservableCollection<Request> Requests
        {
            get
            {
                return Database.Requests;
            }
        }

        public ObservableCollection<VariableClipSet> VariableClipSets
        {
            get
            {
                return Database.VariableClipSets;
            }
        }

        void CurrentSelectionChanged()
        {
            DataContext = _SelectionService.CurrentSelection;
            Items.ItemsSource = null;

            if (DataContext != null)
            {
                IValueConverter converter = _DataModel.FindConverter(DataContext.GetType(), typeof(PropertyCollection));
                if (converter != null)
                {
                    Items.ItemsSource = (PropertyCollection)converter.Convert(DataContext);                    
                }
            }
        }

        void ContentPresenter_ClosePopup(object sender, RoutedEventArgs e)
        {
            ContentPresenter content = (ContentPresenter)sender;
            content.Tag = false;
        }

        void CreateCondition_Click(object sender, RoutedEventArgs e)
        {
            ConditionCollection conditions = (ConditionCollection)((sender as Button).DataContext);

            ObservableCollection<IConditionDesc> descs = _DataModel.ConditionDescriptors;
            if (descs.Count != 0)
            {
                conditions.Add(new Condition(descs[0], null));
            }
        }

        void RemoveCondition_Click(object sender, RoutedEventArgs e)
        {
            ConditionCollection conditions = (ConditionCollection)((sender as Button).Tag);
            Condition condition = (Condition)(sender as Button).DataContext;

            if (condition != null)
            {
                ICondition content = condition.Content;
                Type type = content.GetType();
                BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;

                PropertyInfo[] properties = type.GetProperties(flags);
                foreach (PropertyInfo pi in properties)
                {
                    if (pi.PropertyType != typeof(SignalConditionProperty))
                        continue;

                    object propertyValue = pi.GetValue(content, null);
                    if (!(propertyValue is SignalConditionProperty))
                        continue;

                    Signal signal = (propertyValue as SignalConditionProperty).Value;

                    if (signal != null)
                    {
                        signal.SetUsed(false);
                    }
                }
            }

            conditions.Remove(condition);
        }

        void OnBrowseFilenameLogicalProperty(object sender, RoutedEventArgs e)
        {
            FilenameLogicalProperty filenameLogicalProperty = (FilenameLogicalProperty)(sender as FrameworkElement).DataContext;

            var openFileDialog = new System.Windows.Forms.OpenFileDialog();
            openFileDialog.Title = "Browse for MoVE network file";
            openFileDialog.Filter = "MoVE network (*.mtf)|*.mtf";
            openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(filenameLogicalProperty.Value);
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filenameLogicalProperty.Value = openFileDialog.FileName;
            }
        }

        void OnBrowseLocalClip(object sender, RoutedEventArgs e)
        {
            ClipLogicalProperty clipProperty = (ClipLogicalProperty)(sender as FrameworkElement).DataContext;

            var openFileDialog = new System.Windows.Forms.OpenFileDialog();
            openFileDialog.Title = "Browse for clip file";
            openFileDialog.Filter = "Animation clip (*.clip)|*.clip";
            if (!string.IsNullOrWhiteSpace(clipProperty.Filename))
                openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(clipProperty.Filename);
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                clipProperty.Filename = openFileDialog.FileName;
            }
        }

        void MoveTransitionUp_Click(object sender, RoutedEventArgs e)
        {
            TransitionCollection collection = (TransitionCollection)(sender as Button).DataContext;
            Transition transition = (Transition)(sender as Button).Tag;
            if (transition != null)
            {
                int index = collection.IndexOf(transition);
                if (index != 0)
                {
                    collection.Move(index, index - 1);
                }
            }
        }

        void MoveTransitionDown_Click(object sender, RoutedEventArgs e)
        {
            TransitionCollection collection = (TransitionCollection)(sender as Button).DataContext;
            Transition transition = (Transition)(sender as Button).Tag;
            if (transition != null)
            {
                int index = collection.IndexOf(transition);
                if (index != collection.Count - 1)
                {
                    collection.Move(index, index + 1);
                }
            }
        }

        void AddArrayItem_Click(object sender, RoutedEventArgs e)
        {
            ILogicPropertyArray array = (ILogicPropertyArray)(sender as Button).DataContext;
            string arrayParent = array.Parent.Type;
            if (arrayParent == "Blend N" || arrayParent == "Merge N" || arrayParent == "Add N")
            {
                WeightFilteredTransformArray transformArray = array as WeightFilteredTransformArray;
                int counter = transformArray.Items.Count;

                if (counter > 7)
                {
                    return;
                }
            }
            else if (arrayParent == "Expression")
            {
                NamedFloatVariableArray variableArray = array as NamedFloatVariableArray;
                int counter = variableArray.Items.Count;

                if (counter > 11)
                {
                    return;
                }
            }

            array.Add();
        }

        void AddNewTagEventToSignalProperty(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            TagEventToSignalLogicalPropertyArray array = (TagEventToSignalLogicalPropertyArray)element.DataContext;
            array.Add();
            TagEventToSignalLogicalProperty newProperty = (TagEventToSignalLogicalProperty)array.Children.Last();
            newProperty.UpdateSelectionLists();
        }

        void RemoveArrayItem_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            ILogicPropertyArray array = (ILogicPropertyArray)element.Tag;

            IProperty property = (IProperty)element.DataContext;
            array.Remove(property);
        }

        private void OnClipInputChecked(object sender, RoutedEventArgs e)
        {
            ClipLogicalProperty clipProperty = (ClipLogicalProperty)(sender as FrameworkElement).DataContext;
            if( clipProperty.Input == PropertyInput.Filename )
                clipProperty.Source = clipProperty.Source;
        }

        private void OnRemoveTagEventToSignalProperty(object sender, RoutedEventArgs e)
        {
            FrameworkElement frameworkElement = (FrameworkElement)sender;
            TagEventToSignalLogicalProperty eventToSignalProperty = (TagEventToSignalLogicalProperty)frameworkElement.DataContext;
            ClipPrototype clipNode = (ClipPrototype)eventToSignalProperty.Parent;
            eventToSignalProperty.OnRemovedFromParent();
            clipNode.TagEventToSignalProperties.Remove(eventToSignalProperty);
        }
    }
}
