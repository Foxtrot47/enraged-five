﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Services;

using New.Move.Core;
using Move.Core;

namespace Rockstar.MoVE.Components.ItemPalette
{
    [Export("WindowView", typeof(UserControl)), WindowViewMetadata(1)]
    public partial class PalettePanel : UserControl
    {
        [Import(typeof(IApplicationService))]
        IApplicationService _ApplicationService = null;

        [Import(typeof(IDataModelService))]
        IDataModelService _DataModelService = null;

        [Import("DataModel")]
        IDataModel _DataModel = null;

        public PalettePanel()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(PalettePanel_Loaded);
        }

        bool _Loaded = false;
        IDesc _Desc = null;

        void PalettePanel_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_Loaded)
            {
                _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(ApplicationService_CurrentDocumentChanged);
                _DataModelService.CurrentActiveChanged += new CurrentActiveChangedHandler(DataModelService_CurrentActiveChanged);
                _Loaded = true;
            }
        }

        ObservableCollection<IDesc> _Items = new ObservableCollection<IDesc>();
        public ObservableCollection<IDesc> Items
        {
            get
            {
                return _Items;
            }
        }

        void ApplicationService_CurrentDocumentChanged()
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            ITransitionalDesc descriptor = database.Active.Descriptor;
            UpdateItems(database.Active);
        }

        void DataModelService_CurrentActiveChanged()
        {
            ITransitional active = _DataModelService.CurrentActive as ITransitional;
            UpdateItems(active);
        }

        void UpdateItems(ITransitional active)
        {
            _Items.Clear();

            if (active is Motiontree)
            {
                foreach (IDesc desc in _DataModel.MotionTreeDescriptors)
                {
                    _Items.Add(desc);
                }
            }
            else if (active is IAutomaton)
            {
                foreach (IDesc desc in _DataModel.StateMachineDescriptors)
                {
                    _Items.Add(desc);
                }
            }
        }

        void Item_MouseDown(object sender, MouseButtonEventArgs e)
        {
            UIElement element = sender as UIElement;
            if (element != null)
            {
                object data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue)
                {
                    data = ItemsControl.ItemContainerGenerator.ItemFromContainer(element);
                    if (data == DependencyProperty.UnsetValue)
                    {
                        element = VisualTreeHelper.GetParent(element) as UIElement;
                    }

                    if (element == ItemsControl)
                        return;
                }

                if (data != DependencyProperty.UnsetValue)
                {
                    _Desc = data as IDesc;
                }
            }
        }

        void ItemsControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                _Desc = null;
            }

            if (_Desc != null)
            {
                DragObject dragObj = new DragObject(_Desc);
                DragDrop.DoDragDrop(this, dragObj, DragDropEffects.Copy);

                e.Handled = true;
            }
        }
    }
}
