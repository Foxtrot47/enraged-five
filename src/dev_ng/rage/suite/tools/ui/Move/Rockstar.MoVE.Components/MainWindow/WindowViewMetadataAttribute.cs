﻿using System;
using System.ComponentModel.Composition;

namespace Rockstar.MoVE.Components.MainWindow
{
    [MetadataAttribute]
    public class WindowViewMetadataAttribute : Attribute
    {
        public int Index { get; private set; }

        public WindowViewMetadataAttribute(int index)
        {
            Index = index;
        }
    }
}