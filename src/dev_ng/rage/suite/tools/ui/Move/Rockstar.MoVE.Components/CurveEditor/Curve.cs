﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Rockstar.MoVE.Components.CurveEditor
{
    public class Curve : INotifyPropertyChanged
    {
        public Curve(string name)
        {
            _Color = Color.FromRgb(0, 255, 255);
            Name = name;

            Points = new CurveSegment[2];
            Points[0] = new CurveSegment(new Point(0, 0)) { Curve = this, Color = Color };
            Points[1] = new CurveSegment(new Point(1, 0)) { Curve = this, Color = Color };
        }

        public string Name { get; set; }
        Color _Color;
        public Color Color
        {
            get
            {
                return _Color;
            }
            set
            {
                if (_Color != value)
                {
                    _Color = value;
                    foreach (CurveSegment point in Points)
                    {
                        point.Color = value;
                    }
                    NotifyPropertyChanged("Color");
                }
            }
        }

        bool _Visible = true;
        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                if (_Visible != value)
                {
                    _Visible = value;
                    foreach (CurveSegment point in Points)
                    {
                        point.Visible = value;
                    }
                    NotifyPropertyChanged("Visible");
                }
            }
        }

        public CurveSegment Add(Point point)
        {
            CurveSegment[] points = new CurveSegment[Points.Length + 1];
            int idx = 0;
            foreach (CurveSegment p in Points)
            {
                if (p.X < point.X)
                {
                    points[idx] = p;
                    ++idx;
                }
                else
                {
                    break;
                }
            }

            CurveSegment ret = new CurveSegment(point) { Curve = this, Color = _Color };
            points[idx] = ret;

            for (int i = idx + 1; i < points.Length; ++i)
            {
                points[i] = Points[idx];
                ++idx;
            }

            Points = points;

            return ret;
        }

        public void Remove(CurveSegment point)
        {
            CurveSegment[] points = new CurveSegment[Points.Length - 1];

            int j = 0;
            for (int i = 0; i < Points.Length; ++i)
            {
                if (Points[i] != point)
                {
                    points[j] = Points[i];
                    ++j;
                }
            }

            Points = points;
        }

        public void CalculateGeometry(double width, double height)
        {
            PathSegmentCollection segments = new PathSegmentCollection();
            for (int i = 1; i < Points.Length; ++i)
            {
                Point point = CurveCanvas.ToCanvas(Points[i].Y, Points[i].X, width, height);
                segments.Add(new LineSegment(point, true));
            }
            Point start = CurveCanvas.ToCanvas(Points[0].Y, Points[0].X, width, height);
            PathFigure figure = new PathFigure(start, segments, false);
            Geometry = new PathGeometry(new PathFigure[] { figure });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public CurveSegment[] Points { get; private set; }
        PathGeometry _Geometry = null;
        public PathGeometry Geometry
        {
            get
            {
                return _Geometry;
            }
            private set
            {
                _Geometry = value;
                NotifyPropertyChanged("Geometry");
            }
        }
    }
}