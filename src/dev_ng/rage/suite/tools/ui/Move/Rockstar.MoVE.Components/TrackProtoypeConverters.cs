﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows.Controls;

using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Framework.DataModel.Prototypes;

using RSG.TrackViewer;
using RSG.TrackViewer.ViewModel;
using RSG.Base;

namespace Rockstar.MoVE.Components
{
    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(NWayBlendPrototype), typeof(Control))]
    internal class NWayBlendCurvePrototypeEditorConverter : IValueConverter, ICurveValueConverter
    {
        public object Convert(object value)
        {
            NWayBlendPrototype prototype = value as NWayBlendPrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.HorizTimeline.DisplayMax = prototype.MaximumOutputValue;
            (control as TrackViewer).TM.HorizTimeline.DisplayMin = prototype.MinimumOutputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMax = prototype.MaximumInputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMin = prototype.MinimumInputValue;
            (control as TrackViewer).TM.VertTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.TrackGroupCollectionViewModel.TrackGroupViewModels.Add(new TrackGroupViewModel(prototype.TrackGroup));

            return control;
        }

        public void PostConvert(object value, object newValue)
        {
            NWayBlendPrototype prototype = value as NWayBlendPrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.VertTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
        }
    }

    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(BlendPrototype), typeof(Control))]
    internal class BlendCurvePrototypeEditorConverter : IValueConverter, ICurveValueConverter
    {
        public object Convert(object value)
        {
            BlendPrototype prototype = value as BlendPrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.HorizTimeline.DisplayMax = prototype.MaximumOutputValue;
            (control as TrackViewer).TM.HorizTimeline.DisplayMin = prototype.MinimumOutputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMax = prototype.MaximumInputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMin = prototype.MinimumInputValue;
            (control as TrackViewer).TM.VertTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.TrackGroupCollectionViewModel.TrackGroupViewModels.Add(new TrackGroupViewModel(prototype.TrackGroup));

            return control;
        }

        public void PostConvert(object value, object newValue)
        {
            BlendPrototype prototype = value as BlendPrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.VertTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
        }
    }

    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(NWayMergePrototype), typeof(Control))]
    internal class NWayMergeCurvePrototypeEditorConverter : IValueConverter, ICurveValueConverter
    {
        public object Convert(object value)
        {
            NWayMergePrototype prototype = value as NWayMergePrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.HorizTimeline.DisplayMax = prototype.MaximumOutputValue;
            (control as TrackViewer).TM.HorizTimeline.DisplayMin = prototype.MinimumOutputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMax = prototype.MaximumInputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMin = prototype.MinimumInputValue;
            (control as TrackViewer).TM.VertTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.TrackGroupCollectionViewModel.TrackGroupViewModels.Add(new TrackGroupViewModel(prototype.TrackGroup));

            return control;
        }

        public void PostConvert(object value, object newValue)
        {
            NWayMergePrototype prototype = value as NWayMergePrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.VertTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
        }
    }

    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(NWayAddPrototype), typeof(Control))]
    internal class NWayAddCurvePrototypeEditorConverter : IValueConverter, ICurveValueConverter
    {
        public object Convert(object value)
        {
            NWayAddPrototype prototype = value as NWayAddPrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.HorizTimeline.DisplayMax = prototype.MaximumOutputValue;
            (control as TrackViewer).TM.HorizTimeline.DisplayMin = prototype.MinimumOutputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMax = prototype.MaximumInputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMin = prototype.MinimumInputValue;
            (control as TrackViewer).TM.VertTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.TrackGroupCollectionViewModel.TrackGroupViewModels.Add(new TrackGroupViewModel(prototype.TrackGroup));

            return control;
        }

        public void PostConvert(object value, object newValue)
        {
            NWayAddPrototype prototype = value as NWayAddPrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.VertTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
        }

    }

    [Export("ValueConverter", typeof(IValueConverter)), ValueConverterMetadata(typeof(CurvePrototype), typeof(Control))]
    internal class CurvePrototypeEditorConverter : IValueConverter, ICurveValueConverter
    {
        public object Convert(object value)
        {
            CurvePrototype prototype = value as CurvePrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.HorizTimeline.DisplayMax = prototype.MaximumOutputValue;
            (control as TrackViewer).TM.HorizTimeline.DisplayMin = prototype.MinimumOutputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMax = prototype.MaximumInputValue;
            (control as TrackViewer).TM.VertTimeline.DisplayMin = prototype.MinimumInputValue;
            (control as TrackViewer).TM.VertTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged += new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.TrackGroupCollectionViewModel.TrackGroupViewModels.Add(new TrackGroupViewModel(prototype.TrackGroup));

            return control;
        }

        public void PostConvert(object value, object newValue)
        {
            CurvePrototype prototype = value as CurvePrototype;
            Control control = new TrackViewer();

            (control as TrackViewer).TM.VertTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
            (control as TrackViewer).TM.HorizTimeline.RangeChanged -= new EventHandler(prototype.Range_Changed);
        }
    }
}
