﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Services;

using AvalonDock;

namespace Rockstar.MoVE.Components.CurveEditor
{
    //[Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Curve Editor", WindowDockRegion.Bottom)]
    public partial class CurveEditor : DockableContent
    {
        public ObservableCollection<object> Items { get; set; }
        ObservableCollection<Curve> Paths { get; set; }

        public static readonly DependencyProperty MinInputProperty
            = DependencyProperty.Register("MinInput", typeof(double), typeof(CurveEditor)
            , new FrameworkPropertyMetadata(0.0
                , FrameworkPropertyMetadataOptions.AffectsMeasure
                | FrameworkPropertyMetadataOptions.AffectsRender));

        public double MinInput
        {
            get { return (double)GetValue(MinInputProperty); }
            set { SetValue(MinInputProperty, value); }
        }

        public static readonly DependencyProperty MaxInputProperty
            = DependencyProperty.Register("MaxInput", typeof(double), typeof(CurveEditor)
            , new FrameworkPropertyMetadata(1.0
                , FrameworkPropertyMetadataOptions.AffectsMeasure
                | FrameworkPropertyMetadataOptions.AffectsRender));

        public double MaxInput
        {
            get { return (double)GetValue(MaxInputProperty); }
            set { SetValue(MaxInputProperty, value); }
        }

        public static readonly DependencyProperty MinOutputProperty
            = DependencyProperty.Register("MinOutput", typeof(double), typeof(CurveEditor)
            , new FrameworkPropertyMetadata(0.0
                , FrameworkPropertyMetadataOptions.AffectsMeasure
                | FrameworkPropertyMetadataOptions.AffectsRender));

        public double MinOutput
        {
            get { return (double)GetValue(MinOutputProperty); }
            set { SetValue(MinOutputProperty, value); }
        }

        public static readonly DependencyProperty MaxOutputProperty
            = DependencyProperty.Register("MaxOutput", typeof(double), typeof(CurveEditor)
            , new FrameworkPropertyMetadata(1.0
                , FrameworkPropertyMetadataOptions.AffectsMeasure
                | FrameworkPropertyMetadataOptions.AffectsRender));

        public double MaxOutput
        {
            get { return (double)GetValue(MaxOutputProperty); }
            set { SetValue(MaxOutputProperty, value); }
        }

        public double[] ConverterInput
        {
            get
            {
                return new double[2] { MinInput, ItemsControl.ActualWidth };
            }
        }

        public double[] ConverterOutput
        {
            get
            {
                return new double[2] { MinOutput, ItemsControl.ActualHeight };
            }
        }

        ISelectionService _SelectionService = null;

        [Import(typeof(ISelectionService))]
        ISelectionService SelectionService
        {
            set
            {
                _SelectionService = value;
                _SelectionService.CurrentSelectionChanged += new CurrentSelectionChangedHandler(SelectionService_CurrentSelectionChanged);
            }
        }

        [Import("DataModel")]
        IDataModel DataModel { get; set; }
        
        void SelectionService_CurrentSelectionChanged()
        {
            DataContext = _SelectionService.CurrentSelection;

            if (DataContext != null)
            {
                //IValueConverter converter = DataModel.FindConverter(DataContext.GetType(), typeof(CurveCollection));
                //if (converter != null)
                {
                    // add selected curves
                    //Items.ItemsSource = (PropertyCollection)converter.Convert(DataContext);
                }
            }
            else
            {
                // remove all curves
            }
        }

        public CurveEditor()
        {
            InitializeComponent();
            Items = new ObservableCollection<object>();
            Paths = new ObservableCollection<Curve>();
            Paths.Add(new Curve("input0"));
            Items.CollectionChanged += new NotifyCollectionChangedEventHandler(Items_CollectionChanged);

            ItemsControl.MouseDown += new MouseButtonEventHandler(ItemsControl_MouseDown);

            ItemsControl.ItemsSource = Items;
            ItemsList.ItemsSource = Paths;
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            foreach (Curve path in Paths)
            {
                if (!Items.Contains(path))
                {
                    Items.Add(path);
                    foreach (CurveSegment p in path.Points)
                    {
                        Items.Add(p);
                    }
                }

                path.CalculateGeometry(ItemsControl.ActualWidth, ItemsControl.ActualHeight);
            }

            base.OnRender(drawingContext);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
        }

        void ItemsControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    {
                        Point position = e.GetPosition(ItemsControl);
                        HitTestResult result = VisualTreeHelper.HitTest(ItemsControl, position);
                        if (result != null && result.VisualHit is FrameworkElement)
                        {
                            Curve path = (Curve)((result.VisualHit as FrameworkElement).DataContext);
                            if (path != null)
                            {
                                Point t = CurveCanvas.FromCanvas(position.Y, position.X, ItemsControl.ActualWidth, ItemsControl.ActualHeight);
                                CurveSegment point = path.Add(t);
                                int idx = 0;
                                foreach (CurveSegment p in Items.OfType<CurveSegment>())
                                {
                                    if (point.X < p.X)
                                    {
                                        break;
                                    }
                                    ++idx;
                                }

                                Items.Insert(idx, point);
                                InvalidateVisual();
                            }
                        }
                    }
                    break;
                case MouseButton.Right:
                    {
                        Point position = e.GetPosition(ItemsControl);
                        HitTestResult result = VisualTreeHelper.HitTest(ItemsControl, position);
                        if (result != null && result.VisualHit is FrameworkElement)
                        {
                            object context = (result.VisualHit as FrameworkElement).DataContext;
                            if (context is CurveSegment)
                            {
                                CurveSegment point = (CurveSegment)context;
                                Curve path = point.Curve;
                                path.Remove(point);
                                Items.Remove(point);
                                InvalidateVisual();
                            }
                        }
                    }
                    break;
            }

            base.OnMouseDown(e);
        }

        void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (CurveSegment point in e.NewItems.OfType<CurveSegment>())
                {
                    point.PropertyChanged += new PropertyChangedEventHandler(
                        delegate(object obj, PropertyChangedEventArgs args)
                        {
                            if (args.PropertyName == "Position")
                            {
                                InvalidateVisual();
                            }
                        }
                    );
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                InvalidateVisual();
            }
        }

        void Button_Click(object sender, RoutedEventArgs e)
        {
            Paths.Add(new Curve("input_again"));
            InvalidateVisual();
        }

        void Remove_Click(object sender, RoutedEventArgs e)
        {
            Curve path = (Curve)(sender as FrameworkElement).DataContext;
            Paths.Remove(path);
            Items.Remove(path);
            foreach (CurveSegment point in path.Points)
            {
                //ContentPresenter cp = (ContentPresenter)ItemsControl.ItemContainerGenerator.ContainerFromItem(point);
                Items.Remove(point);
            }
        }
    }
}

namespace Rockstar.MoVE.Components
{
    public class ItemStyleSelector : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {
            ContentPresenter cp = container as ContentPresenter;
            if (item is RenderableCurve)
            {
                return cp.FindResource("PathStyle") as Style;
            }
            else if (item is CurveSegment)
            {
                return cp.FindResource("PointStyle") as Style;
            }
            
            return base.SelectStyle(item, container);
        }
    }
}
