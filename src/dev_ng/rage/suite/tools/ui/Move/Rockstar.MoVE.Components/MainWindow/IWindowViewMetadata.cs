﻿using System;

namespace Rockstar.MoVE.Components.MainWindow
{
    public interface IWindowViewMetadata
    {
        int Index { get; }
    }
}