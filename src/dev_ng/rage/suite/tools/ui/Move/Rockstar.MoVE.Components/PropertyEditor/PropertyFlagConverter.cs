﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using New.Move.Core;

namespace Rockstar.MoVE.Components.PropertyEditor
{
    [ValueConversion(typeof(New.Move.Core.PropertyInput), typeof(IEnumerable<PropertyInput>))]
    public class PropertyFlagConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<PropertyInput> ret = new List<PropertyInput>();
            PropertyInput flags = (PropertyInput)value;
            foreach (PropertyInput pi in Enum.GetValues(typeof(PropertyInput)))
            {
                if ((flags & pi) != 0)
                {
                    ret.Add(pi);
                }
            }

            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}