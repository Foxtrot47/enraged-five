﻿using System;
using System.ComponentModel.Composition;

using AvalonDock;

namespace Rockstar.MoVE.Components.MainWindow
{
    [MetadataAttribute]
    public class WindowDockMetadataAttribute : Attribute
    {
        public string Name { get; private set; }
        public WindowDockRegion Region { get; private set; }
        public WindowDockState State { get; private set; }

        public WindowDockMetadataAttribute(string name, WindowDockRegion region, WindowDockState state)
        {
            Name = name;
            Region = region;
            State = state;
        }

        public WindowDockMetadataAttribute(string name, WindowDockRegion region)
        {
            Name = name;
            Region = region;
            State = WindowDockState.Docked;
        }
    }
}