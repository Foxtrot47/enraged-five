﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

using AvalonDock;

using Rockstar.MoVE.Services;
using Rockstar.MoVE.Components.Properties;

namespace Rockstar.MoVE.Components.MainWindow
{
    [Export("MainWindow", typeof(Window))]
    public partial class MainWindow : Window
    {
        public event EventHandler AllViewsLoaded;

        [ImportMany("WindowView")]
        Lazy<UserControl, IWindowViewMetadata>[] _Views = null;

        [ImportMany("WindowDock")]
        Lazy<DockableContent, IWindowDockMetadata>[] _Docks = null;

        [Import(typeof(IApplicationService))]
        IApplicationService _ApplicationService = null;

        [Import(typeof(IFileService))]
        IFileService _FileService = null;

        [Import(typeof(MainWindowConfiguration))]
        MainWindowConfiguration Config 
        { 
            set 
            {
                SetBinding(Window.WidthProperty, new Binding("Width") { Source = value, Mode = BindingMode.TwoWay });
                SetBinding(Window.HeightProperty, new Binding("Height") { Source = value, Mode = BindingMode.TwoWay });
            } 
        }

        [Import("DocumentHost")]
        public DocumentPane DocumentHost
        {
            set
            {
                Document.Children.Insert(0, value);
            }
        }

        private string DefaultLayout = null;

        private string LastLayout = null;

        private string ActiveSavedLayout = null;

        public class SavedLayout
        {
            #region Events
            public event EventHandler Activated;
            #endregion

            #region Fields
            private string m_layout = null;
            private string m_name = null;
            private bool m_active = false;
            private MenuItem m_item;
            private bool CanUnchecked = false;
            #endregion

            #region Properties
            public string Name
            {
                get { return m_name; }
            }

            public string Layout
            {
                get { return m_layout; }
                set { m_layout = value; }
            }

            public bool Active
            {
                get { return m_active; }
                set
                {
                    m_active = value;
                    if (m_item != null)
                    {
                        CanUnchecked = true;
                        this.m_item.IsChecked = m_active;
                        CanUnchecked = false;
                    }
                }
            }

            public MenuItem Item
            {
                get { return m_item; }
                set
                {
                    if (m_item != null)
                    {
                        m_item.Checked -= m_item_CheckedChanged;
                        m_item.Unchecked -= m_item_CheckedChanged;
                    }

                    m_item = value;
                    if (m_item != null)
                    {
                        m_item.Checked += m_item_CheckedChanged;
                        m_item.Unchecked += m_item_CheckedChanged;
                    }
                }
            }
            #endregion

            #region Constructor
            public SavedLayout(string layout, string name)
            {
                m_layout = layout;
                m_name = name;
            }

            void m_item_CheckedChanged(object sender, RoutedEventArgs e)
            {
                if (!Item.IsChecked && !CanUnchecked)
                {
                    Item.IsChecked = true;
                    return;
                }

                this.Active = Item.IsChecked;

                if (this.Active)
                {
                    if (this.Activated != null)
                        this.Activated(this, EventArgs.Empty);
                }
            }
            #endregion
        }

        public List<SavedLayout> SavedLayouts = new List<SavedLayout>();

        public MainWindow()
        {
            UpgradeSettings();

            InitializeComponent();
            this.Title = "MoVE " + Convert.ToInt32((int)New.Move.Core.DatabaseExportSerializer.Version.Major) + "." + Convert.ToInt32((int)New.Move.Core.DatabaseExportSerializer.Version.Minor) + "." + Convert.ToInt32((int)New.Move.Core.DatabaseExportSerializer.Version.Patch) + "." + Convert.ToInt32((int)New.Move.Core.DatabaseExportSerializer.Version.IsDev);
            this.StateChanged += new EventHandler(OnStateChanged);
            if (Properties.Settings.Default.Maximised)
                this.WindowState = System.Windows.WindowState.Maximized;
            else
                this.WindowState = System.Windows.WindowState.Normal;

            this.PerforceIntegrationItem.IsChecked = Settings.Default.PerforceIntegration;
        }

        /// <summary>
        /// Upgrade our settings from the previous version.
        /// </summary>
        private void UpgradeSettings()
        {
            try
            {
                if (Rockstar.MoVE.Components.Properties.Settings.Default.NeedsUpdating == true)
                {
                    Rockstar.MoVE.Components.Properties.Settings.Default.Upgrade();
                    Rockstar.MoVE.Components.Properties.Settings.Default.NeedsUpdating = false;
                    Rockstar.MoVE.Components.Properties.Settings.Default.Save();
                }
            }
            catch (System.Configuration.ConfigurationException)
            {
                Rockstar.MoVE.Components.Properties.Settings.Default.Reset();
            }
            catch (Exception)
            {
                Rockstar.MoVE.Components.Properties.Settings.Default.Reset();
            }
        }

        void OnStateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
                Properties.Settings.Default.Maximised = true;
            else if (this.WindowState == System.Windows.WindowState.Normal)
                Properties.Settings.Default.Maximised = false;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            DockingManagerControl.ActiveDocumentChanged += new EventHandler(DockingManagerControl_ActiveDocumentChanged);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            //  Save recent files before closing
            SaveRecentFiles();
            //  Save favorite directories before closing
            SaveFavoriteDirectories();

            base.OnClosing(e);

            List<string> docs = _ApplicationService.FindDirtyDocuments();
            if (docs == null)
            {
                docs = new List<string>();
            }

            List<string> clips = _ApplicationService.FindDirtyClips();
            if (clips == null)
            {
                clips = new List<string>();
            }

            if (docs.Count + clips.Count != 0)
            {
                SaveAllDialog dialog = new SaveAllDialog(docs.Concat(clips));
                SaveAllResult result = dialog.DoModal();

                switch (result)
                {
                    case SaveAllResult.Yes:
                        {
                            _ApplicationService.SaveDirtyDocuments();
                            _ApplicationService.SaveDirtyClips();
                        }
                        break;
                    case SaveAllResult.No:
                        {
                        }
                        break;
                    case SaveAllResult.Cancel:
                        {
                            e.Cancel = true;
                            return;
                        }
                }
            }

            SaveLayout();
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Load all the layouts that have been saved including last layout and default layout.
            if (_FileService.Exists("Layout.xml"))
            {
                try
                {
                    using (TextReader reader = _FileService.CreateStreamReader("Layout.xml"))
                    {
                        using (System.Xml.XmlReader xml = System.Xml.XmlReader.Create(reader))
                        {
                            while (xml.MoveToContent() != System.Xml.XmlNodeType.None)
                            {
                                if (xml.IsStartElement())
                                {
                                    if (xml.Name == "Layouts")
                                    {
                                    }
                                    else if (xml.Name == "Default")
                                    {
                                        this.DefaultLayout = xml.ReadInnerXml();
                                    }
                                    else if (xml.Name == "LayoutOnClose")
                                    {
                                        this.LastLayout = xml.ReadInnerXml();
                                    }
                                    else if (xml.Name == "ActiveLayoutOnClose")
                                    {
                                        this.ActiveSavedLayout = xml.GetAttribute("name");
                                    }
                                    else
                                    {
                                        string name = xml.Name.Replace('_', ' ');
                                        if (!string.IsNullOrWhiteSpace(name))
                                        {
                                            SavedLayout newLayout = new SavedLayout(xml.ReadInnerXml(), name);
                                            newLayout.Activated += Layout_Activated;
                                            SavedLayouts.Add(newLayout);
                                            if (newLayout.Name == ActiveSavedLayout)
                                            {
                                                newLayout.Active = true;
                                                this.OverwriteCurrent.IsEnabled = true;
                                            }
                                        }
                                        else
                                            xml.Skip();
                                    }
                                }

                                xml.Read();
                            }
                        }
                    }
                }
                catch
                { }
            }
            if (DefaultLayout == null)
            {
                DefaultLayout =
                "<DockingManager>" +
                    "<ResizingPanel ResizeWidth=\"0\" ResizeHeight=\"*\" Orientation=\"Horizontal\">" +
                        "<ResizingPanel ResizeWidth=\"220\" ResizeHeight=\"*\" Orientation=\"Horizontal\">" +
                            "<DockablePane ResizeWidth=\"*\" ResizeHeight=\"*\" Anchor=\"Left\">" +
                                "<DockableContent Name=\"NavigatorView\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                            "</DockablePane>" +
                        "</ResizingPanel>" +
                        "<ResizingPanel ResizeWidth=\"*\" ResizeHeight=\"*\" Orientation=\"Vertical\">" +
                            "<DocumentPanePlaceHolder />" +
                            "<DockablePane ResizeWidth=\"*\" ResizeHeight=\"*\" Anchor=\"Bottom\">" +
                                "<DockableContent Name=\"ScratchPad_\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                                "<DockableContent Name=\"LogView\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                                "<DockableContent Name=\"PlaybackViewer_\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                            "</DockablePane>" +
                        "</ResizingPanel>" +
                        "<ResizingPanel ResizeWidth=\"220\" ResizeHeight=\"*\" Orientation=\"Horizontal\">" +
                            "<DockablePane ResizeWidth=\"*\" ResizeHeight=\"*\" Anchor=\"Right\">" +
                                "<DockableContent Name=\"Editor\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                                "<DockableContent Name=\"FlagView\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                                "<DockableContent Name=\"VariableClipSetView\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                                "<DockableContent Name=\"SignalView\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                                "<DockableContent Name=\"RequestView\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                                "<DockableContent Name=\"EventView\" AutoHide=\"false\" EffectiveSize=\"0,0\" />" +
                            "</DockablePane>" +
                        "</ResizingPanel>" +
                    "</ResizingPanel>" +
                    "<Hidden />" +
                    "<Windows />" +
                "</DockingManager>";
            }
            if (LastLayout == null)
            {
                LastLayout = DefaultLayout;
            }

            foreach (var dock in _Docks)
            {
                if (dock.Value is DockableContent)
                {
                    DockableContent content = dock.Value as DockableContent;
                    MenuItem item = new MenuItem() { Header = dock.Metadata.Name, IsCheckable = true };
                    item.SetBinding(MenuItem.IsCheckedProperty, new Binding("State") { Source = content, Mode = BindingMode.OneWay, Converter = new DockableContentStateConverter() });
                    item.Click += new RoutedEventHandler(WindowsMenuItem_Click);
                    item.Tag = content;

                    WindowsMenu.Items.Add(item);
                }
                
            }
            
            foreach (var view in _Views.OrderBy(i => i.Metadata.Index))
            {
                Layout.Children.Add(view.Value);
                view.Value.Loaded += new RoutedEventHandler(ViewLoaded);
            }

            foreach (var dock in _Docks)
            {
                switch (dock.Metadata.Region)
                {
                    case WindowDockRegion.Left:
                        LeftRegion.Items.Add(dock.Value);
                        break;
                    case WindowDockRegion.Right:
                        RightRegion.Items.Add(dock.Value);
                        break;
                    case WindowDockRegion.Bottom:
                        BottomRegion.Items.Add(dock.Value);
                        break;
                }

                switch (dock.Metadata.State)
                {
                    case WindowDockState.Hidden:
                        // For some reason this hides the entire pane...
                        //DockingManagerControl.Hide(dock.Value);
                        break;
                }
            }
        }

        private int viewsLoaded = 0;
        void ViewLoaded(object sender, RoutedEventArgs e)
        {
            viewsLoaded++;
            if (viewsLoaded == _Views.Count())
                if (this.AllViewsLoaded != null)
                    this.AllViewsLoaded(this, EventArgs.Empty);
        }

        void WindowsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            DockableContent content = item.Tag as DockableContent;

            if (item.IsChecked)
            {
                DockingManagerControl.Show(content, DockableContentState.DockableWindow);
            }
            else
            {
                DockingManagerControl.Hide(content);
            }
        }

        void DockingManager_Loaded(object sender, RoutedEventArgs e)
        {
            LoadLayout();
        }

        void ApplicationCommand_New(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteNewCommand();
        }

        void ApplicationCommand_Save(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteSaveCommand();
        }

        void ApplicationCommand_SaveAs(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteSaveAsCommand();
        }

        void ApplicationCommand_Open(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteOpenCommand();
        }

        void ApplicationCommand_Close(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteCloseCommand();
        }

        void ApplicationCommand_Exit(object sender, RoutedEventArgs e)
        {
            Close();
        }

        void ApplicationCommand_Undo(object sender, RoutedEventArgs e)
        {
        }

        void ApplicationCommand_Redo(object sender, RoutedEventArgs e)
        {
        }

        private void ExportClicked(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteExportCommand(false, false, false);
        }

        private void ExportAndBuildClicked(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteExportCommand(true, false, false);
        }

        private void ExportToPreviewClicked(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteExportCommand(false, false, true);
        }

        private void ExportAndPrototypeClicked(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteExportCommand(false, true, false);
        }

        private void ExportAllClicked(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteRebuildAllCommand(false);
        }

        private void RebuildAllClicked(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteRebuildAllCommand(true);
        }

        void OpenRecentMenu_Click(object sender, RoutedEventArgs e)
        {
            //  Get the menu item that was clicked
            MenuItem item = (MenuItem)sender;
            //  Get the filename contained in the header
            string headerStr = item.Header.ToString();
            //  Just want the filename, not the index as well
            headerStr = headerStr.Substring(headerStr.IndexOf(' '));
            //  Strip any leading spaces
            headerStr = headerStr.Trim();

            _ApplicationService.OpenFile(headerStr);
        }

        void About_Click(object sender, RoutedEventArgs e)
        {
            AboutDialog dialog = new AboutDialog();
            dialog.DoModal();
        }

        private void CheckDatabase_Click(object sender, RoutedEventArgs e)
        {
            if (_ApplicationService.CurrentDocument == null)
            {
                MessageBox.Show("It's not possible to check the database unless a network is loaded.", "No Network Loaded");
                return;
            }

            CheckDatabaseDialog dialog = new CheckDatabaseDialog((New.Move.Core.Database)_ApplicationService.CurrentDocument.Tag);
            dialog.ShowDialog();
        }

        public event EventHandler LogNeedsClearing;

        private void ClearLog_Click(object sender, RoutedEventArgs e)
        {
            if (LogNeedsClearing != null)
                LogNeedsClearing(null, null);
        }

        void DockingManagerControl_ActiveDocumentChanged(object sender, EventArgs e)
        {
            _ApplicationService.CurrentDocument = sender as DocumentContent;
            //  Update recent files menu items
            UpdateRecentFilesMenu();
            UpdateWindowTitle();
        }

        void LoadLayout()
        {
            try
            {
                using (TextReader stream = new StringReader(LastLayout))
                {
                    DockingManagerControl.RestoreLayout(stream);
                }
            }
            catch
            {
                using (TextReader stream = new StringReader(DefaultLayout))
                {
                    DockingManagerControl.RestoreLayout(stream);
                }
            }
        }

        void SaveLayout()
        {
            using (TextWriter writer = _FileService.CreateStreamWriter("Layout.xml"))
            {
                using (System.Xml.XmlWriter xml = System.Xml.XmlWriter.Create(writer))
                {
                    xml.WriteStartDocument();
                    xml.WriteWhitespace("\n");
                    xml.WriteStartElement("Layouts");
                    xml.WriteWhitespace("\n");

                    xml.WriteStartElement("Default");

                    using (TextReader reader = new StringReader(DefaultLayout))
                    {
                        using (System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(reader))
                        {
                            xml.WriteNode(xmlReader, true);
                        }
                    }

                    xml.WriteEndElement();
                    xml.WriteWhitespace("\n");

                    xml.WriteStartElement("LayoutOnClose");
                    xml.WriteWhitespace("\n");
                    DockingManagerControl.SaveLayout(xml);
                    xml.WriteWhitespace("\n");
                    xml.WriteEndElement();
                    xml.WriteWhitespace("\n");

                    foreach (SavedLayout layout in this.SavedLayouts)
                    {
                        if (layout.Active)
                        {
                            xml.WriteStartElement("ActiveLayoutOnClose");
                            xml.WriteAttributeString("name", layout.Name);
                            xml.WriteEndElement();
                            xml.WriteWhitespace("\n");
                            break;
                        }
                    }

                    foreach (SavedLayout layout in this.SavedLayouts)
                    {
                        xml.WriteStartElement(layout.Name.Replace(' ', '_'));

                        using (TextReader reader = new StringReader(layout.Layout))
                        {
                            using (System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(reader))
                            {
                                xml.WriteNode(xmlReader, true);
                            }
                        }

                        xml.WriteEndElement();
                        xml.WriteWhitespace("\n");
                    }

                    xml.WriteEndElement();
                    xml.WriteEndDocument();
                }

                writer.Flush();
                writer.Close();
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.IsDown)
            {
                _ApplicationService.OnKeyDown(e);
            }

            base.OnKeyDown(e);
        }

        public void LoadRecentFiles(string recentFilesPath)
        {
            _ApplicationService.LoadRecentFiles(recentFilesPath);
        }

        public void SaveRecentFiles()
        {
            _ApplicationService.SaveRecentFiles();
        }

        public void LoadFavoriteDirectories(string favoriteDirectoriesPath)
        {
            _ApplicationService.LoadFavoriteDirectories(favoriteDirectoriesPath);
        }

        public void SaveFavoriteDirectories()
        {
            _ApplicationService.SaveFavoriteDirectories();
        }

        public StringCollection GetRecentFiles()
        {
            return _ApplicationService.GetRecentFiles();
        }

        public void UpdateWindowTitle()
        {
            StringCollection recentFiles = GetRecentFiles();
            if (recentFiles.Count > 0)
            {
                this.Title = "MoVE " + Convert.ToInt32((int)New.Move.Core.DatabaseExportSerializer.Version.Major) + "." + Convert.ToInt32((int)New.Move.Core.DatabaseExportSerializer.Version.Minor) + "." + Convert.ToInt32((int)New.Move.Core.DatabaseExportSerializer.Version.Patch) + "." + Convert.ToInt32((int)New.Move.Core.DatabaseExportSerializer.Version.IsDev);
                this.Title = this.Title + "     " + recentFiles[0];
            }
        }

        public void UpdateRecentFilesMenu()
        {
            //  Clear current menu items
            OpenRecentMenu.Items.Clear();
            //  Add recent items again
            StringCollection recentFiles = GetRecentFiles();
            for (int i = 0; i < recentFiles.Count; i++)
            {
                var recentMenuItem = new MenuItem();
                recentMenuItem.Header = (i + 1) + ". " + recentFiles[i];
                recentMenuItem.Click += new RoutedEventHandler(OpenRecentMenu_Click);
                OpenRecentMenu.Items.Add(recentMenuItem);
            }
        }

        public void OpenFirstRecentFile(string fileToLoad)
        {
            //  Load in the specified file if it's not null
            if (fileToLoad != null)
            {
                _ApplicationService.OpenFile(fileToLoad);
            }

            //  Comment this back in if it's ever wanted.  Opens the last file that was worked on.
            /*
            StringCollection recentFiles = GetRecentFiles();

            if (recentFiles.Count > 0)
            {
                _ApplicationService.OpenFile(recentFiles[0]);
            }
            */
        }

        MenuItem DeleteItem = null;

        private void LoadLayoutItem_Loaded(object sender, RoutedEventArgs e)
        {
            MenuItem parent = sender as MenuItem;
            if (parent == null)
                return;
            parent.Items.Clear();

            // Create all the items
            bool deleteValid = false;
            foreach (SavedLayout layout in this.SavedLayouts)
            {
                MenuItem item = new MenuItem();
                item.Header = layout.Name;
                item.IsCheckable = true;
                item.IsChecked = false;
                item.StaysOpenOnClick = true;
                if (layout.Active)
                {
                    item.IsChecked = true;
                    deleteValid = true;
                }

                parent.Items.Add(item);
                layout.Item = item;
            }
            if (this.SavedLayouts.Count > 0)
                parent.Items.Add(new Separator());

            if (DeleteItem == null)
            {
                DeleteItem = new MenuItem();
                DeleteItem.Header = "Delete Current Layout";
                DeleteItem.Click += new RoutedEventHandler(delete_Click);
                DeleteItem.StaysOpenOnClick = true;
            }
            DeleteItem.IsEnabled = deleteValid;
            parent.Items.Add(DeleteItem);
        }

        void delete_Click(object sender, RoutedEventArgs e)
        {
            SavedLayout remove = null;
            this.OverwriteCurrent.IsEnabled = false;
            foreach (SavedLayout saved in SavedLayouts)
            {
                if (saved.Active)
                {
                    remove = saved;
                    break;
                }
            }
            if (remove == null)
                return;

            SavedLayouts.Remove(remove);

            if (remove.Item == null || !this.LoadLayoutItem.Items.Contains(remove.Item))
                return;

            int index = this.LoadLayoutItem.Items.IndexOf(remove.Item);
            this.LoadLayoutItem.Items.Remove(remove.Item);

            if (this.LoadLayoutItem.Items.Count == 2)
            {
                this.LoadLayoutItem.Items.RemoveAt(0);
                this.DeleteItem.IsEnabled = false;
            }
            else
            {
                var newActive = this.LoadLayoutItem.Items[index];
                if (newActive == null)
                    return;
                if (newActive is Separator)
                    newActive = this.LoadLayoutItem.Items[this.LoadLayoutItem.Items.Count - 3];

                if (!(newActive is MenuItem))
                    return;

                (newActive as MenuItem).IsChecked = true;
            }
        }

        private void OverwriteCurrent_Click(object sender, RoutedEventArgs e)
        {
            foreach (SavedLayout layout in this.SavedLayouts)
            {
                if (layout.Active)
                {
                    System.Text.StringBuilder layoutString = new System.Text.StringBuilder();
                    using (TextWriter writer = new StringWriter(layoutString))
                        DockingManagerControl.SaveLayout(writer);

                    layout.Layout = layoutString.ToString();
                    break;
                }
            }
        }

        private void SaveCurrentAs_Click(object sender, RoutedEventArgs e)
        {
            string name = "Custom Layout ";
            int index = 0;
            string uniqueName = name + index.ToString();
            while (!IsLayoutNameUnique(uniqueName))
            {
                index++;
                uniqueName = name + index.ToString();
            }

            SaveCurrentAs_Click(uniqueName);
        }

        private void SaveCurrentAs_Click(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                System.Text.StringBuilder layoutString = new System.Text.StringBuilder();
                using (TextWriter writer = new StringWriter(layoutString))
                    DockingManagerControl.SaveLayout(writer);

                SavedLayout newLayout = new SavedLayout(layoutString.ToString(), name);
                newLayout.Activated += Layout_Activated;
                SavedLayouts.Add(newLayout);

                this.ActiveSavedLayout = name;
                newLayout.Active = true;
                foreach (SavedLayout layout in this.SavedLayouts)
                {
                    if (layout == newLayout)
                        continue;

                    layout.Active = false;
                }
                OverwriteCurrent.IsEnabled = true;
            }
        }

        private bool IsLayoutNameUnique(string name)
        {
            foreach (SavedLayout saved in SavedLayouts)
            {
                if (saved.Name == name)
                    return false;
            }
            return true;
        }

        private void UseDefault_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (TextReader stream = new StringReader(DefaultLayout))
                {
                    DockingManagerControl.RestoreLayout(stream);
                }
                foreach (SavedLayout layout in this.SavedLayouts)
                {
                    layout.Active = false;
                }
                this.OverwriteCurrent.IsEnabled = false;
            }
            catch
            {
                MessageBox.Show("Error Loading Layout.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PerforceIntegrationClicked(object sender, RoutedEventArgs e)
        {
            Settings.Default.PerforceIntegration = !Settings.Default.PerforceIntegration;
            Settings.Default.Save();
        }

        void Layout_Activated(object sender, EventArgs e)
        {
            this.ActiveSavedLayout = (sender as SavedLayout).Name;
            foreach (SavedLayout layout in this.SavedLayouts)
            {
                if (layout == sender)
                {
                    try
                    {
                        using (TextReader stream = new StringReader(layout.Layout))
                        {
                            DockingManagerControl.RestoreLayout(stream);
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Error Loading Layout.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    continue;
                }

                layout.Active = false;
            }
            OverwriteCurrent.IsEnabled = true;
        }

        private void AutoBackupClicked(object sender, RoutedEventArgs e)
        {
            Settings.Default.AutoBackup = !Settings.Default.AutoBackup;
            Settings.Default.Save();
        }

        private void SaveSelection_Click(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExecuteSaveSelectionCommand();
        }

        private void OpenSaveAllNetworks_Click(object sender, RoutedEventArgs e)
        {
            _ApplicationService.OpenSaveAllNetworks();
        }

        private void ExportAllNetworks_Click(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExportAllNetworks(false);
        }

        private void ExportAllNetworksAndBuildRpf_Click(object sender, RoutedEventArgs e)
        {
            _ApplicationService.ExportAllNetworks(true);
        }
    }
}
