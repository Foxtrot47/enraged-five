﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using AvalonDock;

using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Services;

using Move.Core;
using New.Move.Core;

namespace Rockstar.MoVE.Components.RequestEditor
{
    /*
    //[Export("WindowDock", typeof(Pane)), WindowDockMetadata("Request Editor", 0)]
    internal class RequestEditorPane : DockablePane
    {
        public RequestEditorPane()
        {
            _Content = new RequestEditor();
            Items.Add(_Content);
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _Content.InjectService(value);
            }
        }

        RequestEditor _Content;
    }
     */

    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Request Editor", WindowDockRegion.Right, WindowDockState.Hidden)]
    public partial class RequestEditor : DockableContent
    {
        public RequestEditor()
        {
            InitializeComponent();
        }

        [Import(typeof(IApplicationService))]
        IApplicationService ApplicationService
        {
            set
            {
                _ApplicationService = value;
                _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
            }
        }

        IApplicationService _ApplicationService;

        /*
        internal void InjectService(IApplicationService service)
        {
            _ApplicationService = service;
            _ApplicationService.CurrentDocumentChanged += new CurrentDocumentChangedHandler(CurrentDocumentChanged);
        }
         */

        void CurrentDocumentChanged()
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            DataContext = database;
            ListView.ItemsSource = database.Requests;
        }

        void Request_Click(object sender, RoutedEventArgs e)
        {
            Request request = (Request)(sender as Button).DataContext;
            request.FireRequest(sender, e);
        }

        void AddRequest_Click(object sender, RoutedEventArgs e)
        {
            Database database = _ApplicationService.CurrentDocument.Tag as Database;
            if (database != null)
            {
                if (!String.IsNullOrEmpty(NameTextBox.Text))
                {
                    Request request = new Request(NameTextBox.Text);

                    bool found = false;

                    foreach (Request r in database.Requests)
                    {
                        if (r.Name == request.Name)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        MessageBox.Show("A request with that name already exists!");
                    }
                    else
                    {
                        database.Requests.Add(request);
                    }
                }
            }
        }

        void Remove_Click(object sender, RoutedEventArgs e)
        {
            Request request = (Request)(sender as Button).DataContext;
            Database db = DataContext as Database;
            db.Requests.Remove(request);
        }

        private void MoveUp_Click(object sender, RoutedEventArgs e)
        {
            Request flag = (Request)(sender as Button).DataContext;
            Database db = DataContext as Database;
            if (flag == null || db == null)
                return;
            int currentIndex = db.Requests.IndexOf(flag);
            if (currentIndex <= 0)
                return;

            db.Requests.Move(currentIndex, currentIndex - 1);
        }

        private void MoveDown_Click(object sender, RoutedEventArgs e)
        {
            Request flag = (Request)(sender as Button).DataContext;
            Database db = DataContext as Database;
            if (flag == null || db == null)
                return;
            int currentIndex = db.Requests.IndexOf(flag);
            if (currentIndex == -1 || currentIndex + 1 >= db.Requests.Count)
                return;

            db.Requests.Move(currentIndex, currentIndex + 1);
        }

        private void Remame_Click(object sender, RoutedEventArgs e)
        {
            Request request = (Request)(sender as Button).DataContext;
            Database db = DataContext as Database;
            List<string> nameConflicts = new List<string>();
            if (db != null)
            {
                foreach (Request s in db.Requests)
                {
                    nameConflicts.Add(s.Name);
                }
            }
            nameConflicts.Remove(request.Name);

            UniqueRename.UniqueNameDialog dialog = new UniqueRename.UniqueNameDialog();
            dialog.Title = "Rename Variable Clip Set";
            dialog.NewName = request.Name;
            dialog.NameConflicts = nameConflicts;
            dialog.Owner = Application.Current.MainWindow;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if ((bool)dialog.ShowDialog())
            {
                request.Name = dialog.NewName;
            }
        }
    }
}
