require 'Move.Utils'
require 'MockServerCommon'
require 'PresentationCore'
require 'PresentationFramework'
require 'PP'

#Should come up with a better way to deal with these rather than have them hardcoded in
#the simulation scripts.  After saving an mtf file from MoVE these ids can be pulled out
#by going to "File->Move Debug Tools->Dump Network Ids" and dumping out the network information
NetworkA_Id = System::Guid.new(System::String.new("e298630a88064480b49ffbc2fda63cdd"))
NetworkB_Id = System::Guid.new(System::String.new("e25bd015d0184f6ca9276a301e6aa29c"))


=begin
Choose from:
basic
basicWithSignals
basicWithSignalsAndSubsignals
overlappingStates
childStates
overlapTransitions
overlapTransitions2
overlapTransitions3
startInTheMiddle
randomStatesLoopForever
multipleParents (doesn't work)
sharedGrandparent (doesn't work)
fourStates
bigDaddy
=end

# Global variables
$link = nil         # the network link to use (once one has been established)
$fixedStep = 0.0    # a fixed amount of time to add at the end of each network instruction
$singleStepping = false
$simulationName = "randomStatesLoopForever"
$realTime = true
$realTimeFactor = 1
$actionLimit = 4000
$frameRate = 1.0 / 15.0
$rng = System::Random.new();

$flags = [false, true, false]
$reqs = [false, false, false]
$signals = {}
$tempSignals = {}

class RandomWalkVar
    def initialize()
        @hiddenVal = 0
        @stepAmt = 0.2
    end
    
    def step()
        @hiddenVal += ($rng.Next() - 0.5) * @stepAmt;
        @hiddenVal = 0 if @hiddenVal < 0
        @hiddenVal = 1 if @hiddenVal > 1
        return @hiddenVal > 0.5
    end
end

def doInitialize()
	#Setup two sample networks for testing with
	niA = MockRuntimeServer::SimulationNetworkInstance.new()
    niA.InstanceId = 0
	niA.Id = NetworkA_Id
	niA.Version = 1
	niA.Crc = 100
	niA.Filename = "T:\\rage\\assets\\sample_move\\randomStateNetwork.mtf"
    niA.Description = "MainCharacter"
	
	niB = MockRuntimeServer::SimulationNetworkInstance.new()
    niB.InstanceId = 1
	niB.Id = NetworkB_Id
	niB.Version = 1
	niB.Crc = 100
	niB.Filename = "T:\\rage\\assets\\networkB1.mtf"
    niB.Description = "SomeNPC"
	
    niC = MockRuntimeServer::SimulationNetworkInstance.new()
    niC.InstanceId = 2
    niC.Id = NetworkB_Id
    niC.Version = 1
    niC.Crc = 100
    niC.Filename = "T:\\rage\\assets\\networkB1.mtf"
    niC.Description = "SomeOtherNPC"
    
	server.Context.NetworkInstances.Clear()
	
	server.Context.NetworkInstances.Add(niA.InstanceId, niA)
	server.Context.NetworkInstances.Add(niB.InstanceId, niB)
    server.Context.NetworkInstances.Add(niC.InstanceId, niC)
    
end

# display a timestamped string
def disp(str)
    time = $link.elapsedTime()
    server.DisplayMessage(("%6.3f" % time) + "s: #{str}")
end

def pobj(obj)
    disp(obj.pretty_inspect())
end

def checkpoint 
    if($singleStepping)
        instruction = yield
        disp "> " + instruction
        result = System::Windows::MessageBox.Show("Break: #{instruction}\nYes = Step, No = Run, Cancel = Cancel", "Breakpoint", System::Windows::MessageBoxButton.YesNoCancel)
        if (result == System::Windows::MessageBoxResult.No)
            $singleStepping = false
        elsif (result == System::Windows::MessageBoxResult.Cancel)
            exit(0)
        end
    end
end

# format for signals is sigId => (type, value, parent, writer, parentInstance)
def sendSignals(sigHash)
    return if sigHash.length == 0

    $link.startSignalSet()
    sigHash.each_pair do |sigId, sigData|
        sigType, sigVal, parent, writer, parentInstance = *sigData
        parent = 0 if parent.nil?
        writer = 0 if writer.nil?
        parentInstance = 0 if parentInstance.nil?
        case sigType
        when :Float
            $link.addSignalReal(sigId, sigVal, writer, parent, parentInstance)
        when :Bool
            $link.addSignalBool(sigId, sigVal, writer, parent, parentInstance)
        when :Anim
            $link.addSignalAnim(sigId, sigVal, writer, parent, parentInstance)
        when :Clip
            $link.addSignalClip(sigId, sigVal, writer, parent, parentInstance)
        when :Expression
            $link.addSignalExpression(sigId, sigVal, writer, parent, parentInstance)
        when :Filter
            $link.addSignalFilter(sigId, sigVal, writer, parent, parentInstance)
        when :Frame
            $link.addSignalFrame(sigId, sigVal, writer, parent, parentInstance)
        when :PM
            $link.addSignalPM(sigId, sigVal, writer, parent, parentInstance)
        when :Node
            $link.addSignalNode(sigId, sigVal, writer, parent, parentInstance)
        else
        end
    end
    $link.endSignalSet()
end

# format for signals is (sigId, type, value, parent, writer, parentInstance)
def sendSignalList(sigList)
    return if sigList.length == 0
    
    $link.startSignalSet()
    sigList.each do |sigData|
        sigId, sigType, sigVal, parent, writer, parentInstance = *sigData
        parent = 0 if parent.nil?
        writer = 0 if writer.nil?
        parentInstance = 0 if parent.nil?
        case sigType
        when :Float
            $link.addSignalReal(sigId, sigVal, writer, parent, parentInstance)
        when :Bool
            $link.addSignalBool(sigId, sigVal, writer, parent, parentInstance)
        when :Anim
            $link.addSignalAnim(sigId, sigVal, writer, parent, parentInstance)
        when :Clip
            $link.addSignalClip(sigId, sigVal, writer, parent, parentInstance)
        when :Expression
            $link.addSignalExpression(sigId, sigVal, writer, parent, parentInstance)
        when :Filter
            $link.addSignalFilter(sigId, sigVal, writer, parent, parentInstance)
        when :Frame
            $link.addSignalFrame(sigId, sigVal, writer, parent, parentInstance)
        when :PM
            $link.addSignalPM(sigId, sigVal, writer, parent, parentInstance)
        when :Node
            $link.addSignalNode(sigId, sigVal, writer, parent, parentInstance)
        end
    end
    $link.endSignalSet()
end

def wait(time)
    time *= $realTimeFactor
    while(time > 0.001) 
        thisTime = time
        if (thisTime > $frameRate) then 
            thisTime = time;
        end
        
        dt = 0.0
        if($realTime)
            sleep(thisTime )
            dt = -1.0
        else 
            dt = $frameRate
        end
        
        statebits = [0]
        
        $flags.each_with_index do |flag, idx|
            iflag = flag ? 1 : 0
            statebits[0] = statebits[0] | (iflag << idx)
        end
        
        $reqs.each_with_index do |req, idx|
            ireq = req ? 1 : 0
            statebits[0] = statebits[0] | (ireq << (idx + $flags.length))
        end
        
        #disp "  wait #{time}"
        
        $link.sendUpdateRoot(dt, statebits);
        
        sendSignals($signals)

        time -= $frameRate;
    end
end


########################################################################################################
########################################################################################################
## States
########################################################################################################
########################################################################################################

$stateInsts = {
:A => [1, 0],
:B => [2, 0],
:C => [3, 0],
:D => [4, 0],

# child states
:Aa => [10, 1],
:Ab => [11, 1],
:Ac => [12, 1],
:Ba => [20, 2],
:Bb => [21, 2],
:Bc => [22, 2],
:Ca => [30, 3],
:Cb => [31, 3],
:Cc => [32, 3],
:Da => [40, 4],
:Db => [41, 4],
:Dc => [42, 4],

# grandchild states
:Aax => [100, 10],
:Aay => [101, 10],
:Aaz => [102, 10],
:Aaw => [103, 10],
:Aar => [104, 10],
:Aas => [105, 10],
:Aat => [106, 10],
:Aau => [107, 10],
:Aav => [108, 10],
:Abx => [110, 11],
:Aby => [111, 11],
:Abz => [112, 11],
:Abw => [113, 11],
:Acx => [120, 12],
:Acy => [121, 12],
:Acz => [122, 12],
:Acw => [123, 12],

:Bax => [200, 20],
:Bay => [201, 20],
:Baz => [202, 20],
:Baw => [203, 20],
:Bbx => [210, 21],
:Bby => [211, 21],
:Bbz => [212, 21],
:Bbw => [213, 21],
:Bcx => [220, 22],
:Bcy => [221, 22],
:Bcz => [222, 22],
:Bcw => [223, 22],

:Cax => [300, 30],
:Cay => [301, 30],
:Caz => [302, 30],
:Caw => [303, 30],
:Cbx => [310, 31],
:Cby => [311, 31],
:Cbz => [312, 31],
:Cbw => [313, 31],
:Ccx => [320, 32],
:Ccy => [321, 32],
:Ccz => [322, 32],
:Ccw => [323, 32],

# shared children. A_S, A_T, A_U, and B_S, B_T, B_U are the same S T U states with different parents
:A_S => [9900, 1],
:A_T => [9901, 1],
:A_U => [9902, 1],
:B_S => [9900, 2],
:B_T => [9901, 2],
:B_U => [9902, 2],
:C_S => [9900, 3],
:C_T => [9901, 3],
:C_U => [9902, 3],

# shared grandchildren. Ditto - same shared states but different parents 
:Aa_S => [9900, 10],
:Aa_T => [9901, 10],
:Aa_U => [9902, 10],
:Ab_S => [9900, 11],
:Ab_T => [9901, 11],
:Ab_U => [9902, 11],
:Ba_S => [9900, 20],
:Ba_T => [9901, 20],
:Ba_U => [9902, 20],
:Bb_S => [9900, 21],
:Bb_T => [9901, 21],
:Bb_U => [9902, 21],
         
# unshared children of shared states

:Ss => [9910, 9900],
:St => [9911, 9900],
:Su => [9912, 9900],
:Ts => [9920, 9901],
:Tt => [9921, 9901],
:Tu => [9922, 9901],

}


########################################################################################################
########################################################################################################
## Network instructions
########################################################################################################
########################################################################################################


# Sleep for time ms
def zzz(time)
    checkpoint {"zzz #{time}"}
    wait(time)
end

# Start states
def sta(*states)
    $signals = {}
    $tempSignals = {}
    checkpoint {"sta #{states.join(',')}"}
    fullStates = []
    (states.length / 2).times do |i|
        childAndParent = $stateInsts[states[i * 2]] + [states[i * 2 + 1]]
        fullStates.push childAndParent
    end
    $link.sendStartStateSet(fullStates)
end

# stop states
def stp(*states)
    checkpoint {"stp #{states.join(',')}"}
    fullStates = []
    (states.length / 2).times do |i|
        childAndParent = $stateInsts[states[i * 2]] + [states[i * 2 + 1]]
        fullStates.push childAndParent
    end
    wait($fixedStep);
    $link.sendStopStateSet(fullStates)
end

# begin transition
def btx(fromS, toS, parentInst, via, otherStates = [])
    checkpoint {"btx #{fromS} #{toS} #{via} [#{otherStates.join(',')}]"}
    from = $stateInsts[fromS]
    to = $stateInsts[toS]
    fullStates = []
    (otherStates.length / 2).times do |i|
        childAndParent = $stateInsts[otherStates[i * 2]] + [otherStates[i * 2 + 1]]
        fullStates.push childAndParent
    end
    wait($fixedStep);
    $link.sendStartTransition(from[0], to[0], parentInst, via, fullStates)
end

# end transition
def etx(fromS, toS, parentInst, via, otherStates = [])
    checkpoint {"ext #{fromS} #{toS} #{via} [#{otherStates.join(',')}]"}
    from = $stateInsts[fromS]
    to = $stateInsts[toS]
    fullStates = []
    (otherStates.length / 2).times do |i|
        childAndParent = $stateInsts[otherStates[i * 2]] + [otherStates[i * 2 + 1]]
        fullStates.push childAndParent
    end
    wait($fixedStep);
    $link.sendStopTransition(from[0], to[0], parentInst, via, fullStates)
end

# transition with wait
def trn(fromS, toS, parentInst, via, sleepyTime)
    checkpoint {"trn #{fromS} #{toS} #{via}"}
    from = $stateInsts[fromS]
    to = $stateInsts[toS]
    wait(sleepyTime)
    $link.sendStartTransition(from[0], to[0], parentInst, via, [])
    wait($fixedStep);
    $link.sendStopTransition(from[0], to[0], parentInst, via, [])
end

def gfl(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:Float, value, parent, writer, parentInstance]
    end
end

def gbo(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:Bool, value, parent, writer, parentInstance]
    end
end

def gan(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:Anim, value, parent, writer, parentInstance]
    end
end

def gcl(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:Clip, value, parent, writer, parentInstance]
    end
end

def gex(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:Expression, value, parent, writer, parentInstance]
    end
end

def gft(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:Filter, value, parent, writer, parentInstance]
    end
end

def gfr(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:Frame, value, parent, writer, parentInstance]
    end
end

def gpm(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:PM, value, parent, writer, parentInstance]
    end
end

def gno(signal, value, parent = 0, writer = 0, parentInstance = 0)
    if (value == :Stop) then
        $signals.delete(signal)
    else
        $signals[signal] = [:Node, value, parent, writer, parentInstance]
    end
end

def sig(signals)
    checkpoint {"sig #{signals}"}
    sendSignalList(signals)
end

def cls()
    $signals = {}
end

def trnList(states, parentInst, time)
    0.step((states.length-3), 2) do |i|
        trn states[i], states[i+2], parentInst, states[i+1], time
    end
end


########################################################################################################
########################################################################################################
## Simulation scripts
########################################################################################################
########################################################################################################

def basic()
    $fixedStep = 2.0
    
    disp "Starting a basic simulation"
    sta :A, 1
    zzz 1
    trn :A, :B, 1, 1, 1.0
    zzz 0.5
    $flags[0] = true
    zzz 0.5
    trn :B, :C, 1, 1, 1.0
    zzz 0.1
    $flags[0] = false
    zzz 0.9
    trn :C, :D, 1, 1, 1.0
    zzz 0.5
    $flags[0] = true
    zzz 0.5
    trn :D, :B, 1, 1, 1.0
    zzz 1
    stp :B, 1
end

def basicWithSignals()
    $fixedStep = 2.0
    
    Movestick_X = 0
    Movestick_Y = 1
    World_Temperature = 2
    Is_Falling = 3
    Force_Facial_Animation = 4
    Facial_Anim = 5
    
    disp "Starting a basic simulation"
    sta :A, 1
    gfl Movestick_X, 0
    gfl Movestick_Y, 1
    gbo Is_Falling, false
    zzz 0.5
    zzz 0.3
    gbo Is_Falling, true
    zzz 0.2
    trn :A, :B, 1, 1, 1.0
    zzz 0.5
    gfl Movestick_X, -0.5
    gbo Is_Falling, false
    $flags[0] = true
    zzz 0.5
    trn :B, :C, 1, 1, 1.0
    zzz 0.1
    gfl Movestick_Y, 0.5
    gfl Movestick_X, 0.5
    $flags[0] = false
    zzz 0.9
    gbo Is_Falling, true
    trn :C, :D, 1, 1, 1.0
    zzz 0.5
    gfl Movestick_X, 0
    $flags[0] = true
    zzz 0.5
    gbo Is_Falling, false
    gfl Movestick_Y, 0
    trn :D, :B, 1, 1, 1.0
    zzz 1
    stp :B, 1
end    

def basicWithSignalsAndSubsignals()
    $fixedStep = 2.0
    
    Movestick_X = 0
    Movestick_Y = 1
    World_Temperature = 2
    Is_Falling = 3
    Force_Facial_Animation = 4
    Facial_Anim = 5
    Hand_Override = 6
    Anim_Rate_Expr = 7
    
    disp "Starting a basic simulation"
    sta :A, 1                    # in state A
    gfl Movestick_X, 0
    gfl Movestick_Y, 1
    gbo Is_Falling, false
    zzz 0.8
    gan Facial_Anim, "Look_Scared"
    gbo Is_Falling, true
    gcl Hand_Override, "Grasp_Wildly"
    zzz 0.2
    btx :A, :B, 1, 1
    
    stateBId = $stateInsts[:B][0]
    
    sig [[World_Temperature, :Float, 0.0, stateBId, 0, 1]]
    $fixedStep = 0
    10.times do |i|
        zzz $frameRate
        sig [[World_Temperature, :Float, 0.1 * i, stateBId, 0, 1]]
    end
    10.times do 
        zzz $frameRate
        sig [[World_Temperature, :Float, 1.0, stateBId, 0, 1]]
    end
    zzz $frameRate
    sig [[World_Temperature, :Float, 1.0, stateBId, 0, 1]]
    etx :A, :B, 1, 1
    30.times do
        zzz $frameRate
        sig [[World_Temperature, :Float, 3.0, stateBId, 0, 1]]
    end
    gfl Movestick_X, -0.5
    gbo Is_Falling, false
    gan Facial_Anim, "Look_Relieved"
    gcl Hand_Override, :Stop
    10.times do
        zzz $frameRate
        sig [[World_Temperature, :Float, 3.0, stateBId, 0, 1]]
    end
    $flags[0] = true
    15.times do
        zzz $frameRate
        sig [[World_Temperature, :Float, 3.0, stateBId, 0, 1]]
    end
    btx :B, :C, 1, 1          # in state C
    30.times do |i|
        zzz $frameRate
        sig [[World_Temperature, :Float, 3.0 - (i * 0.05), stateBId, 0, 1]]
    end
    gan Facial_Anim, "Big_Smile"
    zzz $frameRate
    etx :B, :C, 1, 1
    $fixedStep = 2.0
    gfl Movestick_Y, 0.5
    gfl Movestick_X, 0.5
    $flags[0] = false
    zzz 0.4
    gex Anim_Rate_Expr, "= ({1} * {2}) + 0.5"
    zzz 0.5
    gbo Is_Falling, true
    gan Facial_Anim, "Look_Angry"
    gcl Hand_Override, "Shake_Fist"
    trn :C, :D, 1, 1, 1.0          # in state D
    zzz 0.2
    gex Anim_Rate_Expr, :Stop
    zzz 0.3
    gfl Movestick_X, 0
    $flags[0] = true
    zzz 0.5
    gbo Is_Falling, false
    gan Facial_Anim, "Look_Relieved"
    gcl Hand_Override, :Stop
    gfl Movestick_Y, 0
    trn :D, :B, 1, 1, 1.0          # in state B
    gan Facial_Anim, "Look_Tired"
    zzz 1
    stp :B, 1
end    

def overlappingStates()
    disp "Simulating network A"
    sta :A, 1
    zzz 1.0
    2.times do |i|
        disp "iter #{i}"
        trn :A, :B, 1, 1, 1.0
        zzz 3.0
        trn :B, :C, 1, 1, 1.0
        zzz 3.0
        trn :C, :A, 1, 1, 1.0
        zzz 1.0
    end
    btx :A, :B, 1, 1
    zzz 1.0
    btx :B, :C, 1, 1
    zzz 1.0
    btx :C, :A, 1, 1
    zzz 3.0
    etx :A, :B, 1, 1
    zzz 1.0
    etx :B, :C, 1, 1
    zzz 1.0
    etx :C, :A, 1, 1
    zzz 3.0
    2.times do |i|
        disp "iter #{i}"
        trn :A, :B, 1, 1, 1.0
        zzz 3.0
        trn :B, :C, 1, 1, 1.0
        zzz 3.0
        trn :C, :A, 1, 1, 1.0
        zzz 1.0
    end
    stp :A, 1
end

def childStates()
    $fixedStep = 0.5;
    disp "Simulating network A"
    disp "state A"
    sta :A, 1, :Aa, 2
    trnList [:Aa, 1, :Ab, 1, :Ac], 2, 1.0
    disp "going to B"
    btx :A,:B,1, 1,      [:Ba, 3]
    etx :A,:B,1, 1,      [:Ac, 2]
    disp "in B"
    zzz 2
    trnList [:Ba, 1, :Bb], 3, 1.0
    zzz 2
    disp "going to C"
    btx :B,:C,1,1,         [:Ca, 4]
    etx :B,:C,1,1,         [:Bb, 3]
    disp "in C"
    trnList [:Ca, 1, :Cb, 1, :Cc, 1, :Ca], 4, 1.0
    disp "going to A"
    btx :C,:A,1,1,         [:Aa, 5]
    etx :C,:A,1,1,         [:Ca, 4]
    disp "in A"
    zzz 1
    disp "going to C"
    btx :A,:C,1,1,         [:Ca, 6]
    etx :A,:C,1,1,         [:Aa, 5]
    trnList [:Ca, 1, :Cc, 1, :Cb], 6, 1.0
    disp "going to A"
    btx :C,:A,1,1,         [:Aa, 7]
    zzz 0.5
    etx :C,:A,1,1,         [:Cb, 6]
    zzz 3
    stp :A, 1, :Aa, 7
    disp "done"
end

def overlapTransitions()
    $fixedStep = 0.3
    disp "Overlapping transitions"
    sta :A, 1
    btx :A, :B, 1, 0
    btx :B, :A, 1, 0
    btx :A, :C, 1, 0
    etx :A, :B, 1, 0
    btx :C, :A, 1, 0
    etx :B, :A, 1, 0
    etx :A, :C, 1, 0
    etx :C, :A, 1, 0
    zzz 1
    btx :A, :B, 1, 0
    btx :B, :C, 1, 0
    btx :C, :A, 1, 0
    etx :A, :B, 1, 0
    etx :B, :C, 1, 0
    etx :C, :A, 1, 0
    zzz 1
    btx :A, :B, 1, 0
    btx :B, :A, 1, 0
    btx :A, :B, 1, 1
    btx :B, :A, 1, 1
    etx :B, :A, 1, 0
    etx :A, :B, 1, 1
    etx :A, :B, 1, 0
    etx :B, :A, 1, 1
    zzz 1
    stp :A, 1
    disp "done"
end

def overlapTransitions2()
    $fixedStep = 0.2
    disp "Overlapping transitions 2"
    
    Walk = :A
    Run = :B
    Left = :Ab
    Mid = :Aa
    Right = :Ac
    
    sta Walk, 1, Mid, 2 
    btx Mid, Left, 2, 0
    btx Walk, Run, 1, 0
    btx Left, Mid, 2, 0
    btx Mid, Right, 2, 0
    etx Mid, Left, 2, 0
    btx Run, Walk, 1, 0, [Mid, 3]
    btx Mid, Right, 3, 0
    btx Right, Mid, 2, 0
    btx Right, Mid, 3, 0
    btx Mid, Left, 2, 0
    btx Mid, Left, 3, 0
    etx Walk, Run, 1, 0, [Left, 2, Mid, 2, Right, 2, Mid, 2, Left, 2]
    btx Left, Mid, 3, 0
    etx Run, Walk, 1, 0
    etx Mid, Right, 3, 0
    etx Right, Mid, 3, 0
    etx Mid, Left, 3, 0
    etx Left, Mid, 3, 0
    stp Walk, 1, Mid, 3
    
    disp "done"
end

def overlapTransitions3() # i think this is a stripped down version of the error case in #2
    $fixedStep = 0.2
    disp "Overlapping transitions 3"
    
    Walk = :A
    Run = :B
    Left = :Ab
    Mid = :Aa
    Right = :Ac
    
    sta Walk, 1, Mid, 2
    btx Mid, Left, 2, 0        # 'old' thread
    btx Walk, Run, 1, 0
    btx Run, Walk, 1, 0, [Mid, 3]  # problem starts here, there's a new mid on a new thread
    btx Mid, Left, 3, 0        # 'new' thread takes a frame to transition to the state the old thread was already in
    btx Left, Mid, 2, 0        # 'old' thread goes back to mid
    btx Left, Mid, 3, 0        # 'new' thread goes back to mid
    etx Walk, Run, 1, 0, [Mid, 2, Left, 2, Mid, 2] # which 2 mids stop here? there are 4 active. It's not the oldest two
    # clean up
    etx Run, Walk, 1, 0
    etx Mid, Left, 3, 0
    etx Left, Mid, 3, 0
    stp Walk, 1, Mid, 3
end

def randDifferent( oldVal, list )
    items = list.length
    loop {
        i = $rng.Next(items)
        return list[i] unless list[i] == oldVal
    }
end

$childStates = {
    nil => [:A, :B, :C],
    :A => [:Aa, :Ab, :Ac],
    :B => [:Ba, :Bb, :Bc],
    :C => [:Ca, :Cb, :Cc],
    :Aa => [:Aax, :Aay, :Aaz, :Aaw, :Aar, :Aas, :Aat, :Aau, :Aav],
    :Ab => [:Abx, :Aby, :Abz, :Abw],
    :Ac => [:Acx, :Acy, :Acz, :Acw],
    :Ba => [:Bax, :Bay, :Baz, :Baw],
    :Bb => [:Bbx, :Bby, :Bbz, :Bbw],
    :Bc => [:Bcx, :Bcy, :Bcz, :Bcw],
    :Ca => [:Cax, :Cay, :Caz, :Caw],
    :Cb => [:Cbx, :Cby, :Cbz, :Cbw],
    :Cc => [:Ccx, :Ccy, :Ccz, :Ccw],
}

def remapStates(newStateIds)

    oldIdToNameMap = {}
    $stateInsts.each_pair do |stateName, defn|
      oldIdToNameMap[defn[0]] = stateName
    end
    
    newInstMap = {}
    $stateInsts.each_pair do |stateName, defn|
        newDefn = [0,0]
        newDefn[0] = newStateIds[stateName] || 0 
        newDefn[1] = newStateIds[oldIdToNameMap[defn[1]]] || 0
        newInstMap[stateName] = newDefn
    end
    
    $stateInsts = newInstMap
end

def remapRandomStatesTest()
    stateIds = {
    :A=>1,
    :B=>2,
    :C=>4,
    :Aa=>3,
    :Ac=>5,
    :Ab=>6,
    :Aax=>8,
    :Aay=>10,
    :Aaz=>12,
    :Aaw=>14,
    :Aar=>16,
    :Aas=>18,
    :Aat=>20,
    :Aau=>22,
    :Aav=>24,
    :Abx=>26,
    :Aby=>28,
    :Abz=>30,
    :Abw=>32,
    :Acx=>34,
    :Acz=>36,
    :Acw=>38,
    :Acy=>40,
    :Ba=>41,
    :Bc=>42,
    :Bb=>43,
    :Baz=>45,
    :Baw=>47,
    :Bay=>49,
    :Bax=>51,
    :Bbx=>53,
    :Bbw=>55,
    :Bbz=>57,
    :Bby=>59,
    :Bcx=>61,
    :Bcz=>63,
    :Bcy=>65,
    :Bcw=>67,
    :Ca=>68,
    :Cc=>69,
    :Cb=>70,
    :Cax=>72,
    :Caw=>74,
    :Caz=>76,
    :Cay=>78,
    :Cbx=>80,
    :Cbw=>82,
    :Cbz=>84,
    :Cby=>86,
    :Ccy=>87,
    :Ccz=>88,
    :Ccx=>89,
    :Ccw=>90
    }
    
    remapStates(stateIds)
    
    $stateInsts.each_pair do |state, info|
        disp "#{state} [#{info[0]}, #{info[1]}]"
    end
end

def startInTheMiddle()
    $fixedStep = 1.0;
    
    etx :Aaz, :Aax, 5, 1
    trnList [:Aax, 1, :Aay, 1, :Aaz], 5, 1.0
    btx :A, :B, 1, 1, [:Ba, 6, :Bax, 7]
    etx :A, :B, 1, 1, [:Aa, 2, :Aaz, 5]
    trnList [:Bax, 1, :Baz, 1, :Bay], 7, 1.0
    stp [:B, 1, :Ba, 6, :Bay, 7]
end

def randomizeFlags()
    switchFlag = 100
    pulseRequest = 40
    cancelRequest = 500
    
    $flags.length.times do |i|
        if ($rng.Next(1000) < switchFlag)
            $flags[i] = !$flags[i];
        end
    end
    
    $reqs.length.times do |i|
        if ($rng.Next(1000) < pulseRequest)
            $reqs[i] = true
        end
        if ($rng.Next(1000) < cancelRequest)
            $reqs[i] = false
        end
    end
end

def randomStatesLoopForever()
    $fixedStep = 0.3;

    remapRandomStatesTest();
    
    switchParent = 50
    switchChild = 40
    switchGrand = 200
    
    # pick a random parent, child, and grandchild to start with
    currParent = :A
    currChild = $childStates[currParent][0]
    currGrand = $childStates[currChild][0]
    
    currRootInst = 1
    currParentInst = 2
    currChildInst = 3
    nextInst = 4
    
    actions = 1
    
    sta currParent, currRootInst, currChild, currParentInst, currGrand, currChildInst

    if $actionLimit > 0 then
        disp "Limiting actions to #{$actionLimit}"
    end
    
    loop do
        if ($rng.Next(1000) < switchParent) then
           newParent = randDifferent(currParent, $childStates[nil])
           newParentInst = nextInst; nextInst += 1
           newChild = $childStates[newParent][0]
           newChildInst = nextInst; nextInst += 1
           newGrand = $childStates[newChild][0]
         
           
           btx currParent, newParent, currRootInst, 1, [newChild, newParentInst, newGrand, newChildInst]
           sleeps = $rng.Next(4)
           sleeps.times do 
              randomizeFlags()
              zzz $fixedStep
           end
           etx currParent, newParent, currRootInst, 1, [currChild, currParentInst, currGrand, currChildInst]
           
           currParent, currChild, currGrand = newParent, newChild, newGrand
           currParentInst, currChildInst = newParentInst, newChildInst
           actions += 1
           
        elsif ($rng.Next(1000) < switchChild) then
            newChild = randDifferent(currChild, $childStates[currParent])
            newChildInst = nextInst; nextInst += 1
            newGrand = $childStates[newChild][0]
            
            btx currChild, newChild, currParentInst, 1, [newGrand, newChildInst]
            sleeps = $rng.Next(4)
            sleeps.times do 
               randomizeFlags()
               zzz $fixedStep
            end
            etx currChild, newChild, currParentInst, 1, [currGrand, currChildInst]
            
            currChild, currGrand, currChildInst = newChild, newGrand, newChildInst
            actions += 1

        elsif ($rng.Next(1000) < switchGrand) then
            newGrand = randDifferent(currGrand, $childStates[currChild])
            
            btx currGrand, newGrand, currChildInst, 1
            sleeps = $rng.Next(4)
            sleeps.times do 
               randomizeFlags()
               zzz $fixedStep
            end
            etx currGrand, newGrand, currChildInst, 1
            
            currGrand = newGrand
            actions += 1
            
        end
        
        randomizeFlags()
        zzz $fixedStep
        
        if actions % 100 == 0 then
            disp "#{actions} actions"
        end
        if $actionLimit > 0 && actions >= $actionLimit then
            break
        end
    end
end

def multipleParents()
    $fixedStep = 1.0
    disp "S and T as children of Aa"
    sta :A, :Aa, :Aa_S
    trn :Aa_S, :Aa_T, 1, 0.5
    trn :Aa_T, :Aa_S, 1, 0.5
    btx :Aa, :Ab, 1
    etx :Aa, :Ab, 1,    [:Aa_S]
    trn :Ab, :Ac, 1, 0.5
    btx :A, :B, 1,      [:Ba]
    etx :A, :B, 1,      [:Ac]
    zzz 1
    disp "S and T as children of Bb"
    btx :Ba, :Bb, 1,    [:Bb_S]
    etx :Ba, :Bb, 1
    trn :Bb_S, :Bb_T, 1, 0.5
    trn :Bb_T, :Bb_U, 1, 0.5
    trn :Bb_U, :Bb_S, 1, 0.5
    btx :B, :C, 1
    etx :B, :C, 1,      [:Bb, :Bb_S]
    zzz 1
    btx :C, :A, 1,      [:Aa, :Aa_S]
    etx :C, :A, 1
    trn :Aa_S, :Aa_T, 1, 0.5
    trn :Aa_T, :Aa_S, 1, 0.5
    stp :A, :Aa, :Aa_S
end


def sharedGrandparent()
    $fixedStep = 1.0
    disp "S and T as children of Aa"
    sta :A, :Aa, :Aa_S
    btx :Aa_S, :Aa_T, 1,    [:Ts]
    etx :Aa_S, :Aa_T, 1
    trn :Ts, :Tt, 1, 0.5
    trn :Tt, :Tu, 1, 0.5
    btx :Aa_T, :Aa_S, 1
    etx :Aa_T, :Aa_S, 1,         [:Tu]
    btx :Aa, :Ab, 1
    etx :Aa, :Ab, 1,    [:Aa_S]
    trn :Ab, :Ac, 1, 0.5
    btx :A, :B, 1,      [:Ba]
    etx :A, :B, 1,      [:Ac]
    zzz 1
    disp "S and T as children of Bb"
    btx :Ba, :Bb, 1,    [:Bb_S]
    etx :Ba, :Bb, 1
    btx :Bb_S, :Bb_T, 1,        [:Ts]
    etx :Bb_S, :Bb_T, 1
    trn :Ts, :Tu, 1, 0.5
    trn :Tu, :Tt, 1, 0.5
    trn :Tt, :Tu, 1, 0.5
    btx :Bb_T, :Bb_U, 1
    etx :Bb_T, :Bb_U, 1,         [:Tu]
    trn :Bb_U, :Bb_S, 1, 0.5
    btx :B, :C, 1
    etx :B, :C, 1,      [:Bb, :Bb_S]
    zzz 1
    btx :C, :A, 1,      [:Aa, :Aa_S]
    etx :C, :A, 1
    trn :Aa_S, :Aa_T, 1, 0.5
    trn :Aa_T, :Aa_S, 1, 0.5
    stp :A, :Aa, :Aa_S
end

def fourStates()
    $fixedStep = 1.0
    sta :A, 1
    trnList [:A, 1, :B, 1, :C, 1, :D, 1, :A], 1, 1.0
    stp :A, 1
end

def bigDaddy()
    # I'm cheating here and 'reusing' instance IDs.
    # this would actually happen in practice too so not entirely cheating
    $fixedStep = 1.0

    disp 'A'
    sta :A, 1, :Aa, 10, :Aax, 100

    
    50.times do |i|
    
        disp "Iter #{i}"

        trnList [:Aax, 1, :Aay, 1, :Aaz, 1, :Aay], 100, 1.0
        
        btx :Aa, :Ac, 10, 1, [:Acx, 101]
        etx :Aa, :Ac, 10, 1, [:Aay, 100]
        
        trnList [:Acx, 1, :Acz, 1, :Acx, 1, :Acz, 1, :Acy], 101, 1.0
        
        btx :Ac, :Ab, 10, 1, [:Abx, 100]
        etx :Ac, :Ab, 10, 1, [:Acy, 101]

        trnList [:Abx, 1, :Aby, 1, :Abz], 100, 1.0
       
        btx :Ab, :Aa, 10, 1, [:Aax, 101]
        etx :Ab, :Aa, 10, 1, [:Abz, 100]
        
        trn :Aax, :Aaz, 101, 1, 1.0
        
        disp 'B'
        
        btx :A, :B, 1, 1, [:Ba, 11, :Bax, 100]
        #sneaky, beginning one trans while another is still in progress
        btx :Bax, :Bay, 100, 1
        etx :A, :B, 1, 1, [:Aa, 10, :Aaz, 101]
        etx :Bax, :Bay, 100, 1
        
        trnList [:Bay, 1, :Bax, 1, :Bay, 1, :Baz], 100, 1.0
        
        btx :Baz, :Bax, 100, 1
        
        btx :Ba, :Bb, 11, 1, [:Bbx, 101]
        # again sneaky, ending a transition while the parent's is in progress
        etx :Baz, :Bax, 100, 1
        etx :Ba, :Bb, 11, 1, [:Bax, 100]
        
        trnList [:Bbx, 1, :Bby, 1, :Bbz, 1, :Bby], 101, 1.0
        
        # double sneaky, one transition ends, one begins
        btx :Bb, :Bc, 11, 1, [:Bcx, 100]
        btx :Bby, :Bbz, 101, 1
        btx :Bcx, :Bcy, 100, 1
        etx :Bcx, :Bcy, 100, 1
        etx :Bby, :Bbz, 101, 1
        etx :Bb, :Bc, 11, 1, [:Bbz, 101]
        
        trn :Bcy, :Bcz, 100, 1, 1.0
        zzz 2
        
        disp 'C'
        btx :B, :C, 1, 1, [:Ca, 10, :Cax, 101]
        etx :B, :C, 1, 1, [:Bc, 11, :Bcz, 100]
        
        trnList [:Cax, 1, :Cay, 1, :Caz, 1, :Cay], 101, 1.0
        
        btx :Ca, :Cb, 10, 1, [:Cbx, 100]
        etx :Ca, :Cb, 10, 1, [:Cay, 101]
        
        trnList [:Cbx, 1, :Cby, 1, :Cbz, 1, :Cbx], 100, 1.0
        
        btx :Cb, :Cc, 10, 1, [:Ccx, 101]
        etx :Cb, :Cc, 10, 1, [:Cbx, 100]
        
        trnList [:Ccx, 1, :Ccz, 1, :Ccx, 1, :Ccz, 1, :Ccy], 101, 1.0
        
        btx :Cc, :Cb, 10, 1, [:Cbx, 100]
        etx :Cc, :Cb, 10, 1, [:Ccy, 101]
        
        disp 'D'
        
        btx :C, :D, 1, 1, [:Da, 11]
        etx :C, :D, 1, 1, [:Cb, 10, :Cbx, 100]

        trnList [:Da, 1, :Db, 1, :Dc, 1, :Da], 11, 1.0
        
        disp 'B'
        
        btx :D, :B, 1, 1, [:Ba, 10, :Bax, 100]
        etx :D, :B, 1, 1, [:Da, 11]
        
        trnList [:Bax, 1, :Baz, 1, :Bax, 1, :Bay], 100, 1.0
        
        btx :Ba, :Bc, 10, 1, [:Bcx, 101]
        etx :Ba, :Bc, 10, 1, [:Bay, 100]

        trnList [:Bcx, 1, :Bcy, 1, :Bcz, 1, :Bcx], 101, 1.0
        
        disp 'C'
        
        btx :B, :C, 1, 1, [:Ca, 11, :Cax, 100]
        etx :B, :C, 1, 1, [:Bc, 10, :Bcx, 101]

        trnList [:Cax, 1, :Cay, 1, :Caz, 1, :Cax], 100, 1.0
        
        btx :Ca, :Cb, 11, 1, [:Cbx, 101]
        etx :Ca, :Cb, 11, 1, [:Cax, 100]
        
        btx :Cb, :Ca, 11, 1, [:Cax, 100]
        etx :Cb, :Ca, 11, 1, [:Cbx, 101]
        
        trnList [:Cax, 1, :Cay, 1, :Caz, 1, :Cax], 100, 1.0
        
        disp 'A'
        
        btx :C, :A, 1, 1, [:Aa, 10, :Aax, 101]
        etx :C, :A, 1, 1, [:Ca, 11, :Cax, 100]
        
        trnList [:Aax, 1, :Aaz, 1, :Aay, 1, :Aaz], 101, 1.0
        
        btx :Aa, :Ac, 10, 1, [:Acx, 99]
        etx :Aa, :Ac, 10, 1, [:Aaz, 101]
        
        trnList [:Acx, 1, :Acy, 1, :Acz], 99, 1.0
        
        btx :Ac, :Aa, 10, 1, [:Aax, 100]
        btx :Acz, :Acx, 99, 1
        btx :Aax, :Aay, 100, 1
        etx :Acz, :Acx, 99, 1
        etx :Ac, :Aa, 10, 1, [:Acx, 99]
        etx :Aax, :Aay, 100, 1
        
        trn :Aay, :Aax, 100, 1, 1.0
    end

    stp :A, 1, :Aa, 10, :Aax, 100
end

server.DisplayMessage("SIM: #{$simulationName}");

if ($singleStepping)
    server.DisplayMessage("!!! Single Stepping Enabled !!!")
end

if ($realTime)
    server.DisplayMessage("!!! Running in real time !!!")
end

def doSimulation( id, respId, name )
    $link = Link.new(id, server)
    
    $link.sendStartDebugging(respId.to_i, 1, 3, 0, 3, 3);
    eval $simulationName + "()"
end
