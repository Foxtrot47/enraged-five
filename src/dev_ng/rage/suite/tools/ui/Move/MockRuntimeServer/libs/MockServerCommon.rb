require 'Move.Utils'

#These need to stay in sync with the NetworkMessageId enum defined
#in Move.Core/Message.cs
MSG_START_DEBUGGING = 102
MSG_START_TRANSITION = 105
MSG_STOP_TRANSITION = 106
MSG_START_STATE_SET = 107
MSG_STOP_STATE_SET = 108
MSG_UPDATE_TREE = 109
MSG_SIGNAL_SET = 110

#Helper function to read the network guid from a dump of an mtf file
def readNetworkIdFromFile( fileName )
	begin
		file = File.new( fileName, "r" )
			strId = file.gets
			return System::Guid.new(System::String.new(strId))
		file.close
	rescue => err
		puts "Exception: #{err}"
		return nil
	end
end

#Helper function to generate a network instance from a dump of an mtf file
def createNetworkInstanceFromFile( fileName )

	networkId = readNetworkIdFromFile( fileName )
	
	if networkId.nil?
		return nil
	end

	network = MockRuntimeServer::SimulationNetworkInstance.new()
	network.Id = networkId
	network.Version = 1
	network.Crc = 100
	network.Filename = File.dirname(fileName) + "\\" + File.basename(fileName, File.extname(fileName)) + ".mtf"
	
	return network
end


class Link 
    @@launchTime = System::DateTime.Now
    
    attr_accessor :elapsedTime

    def initialize(networkId, server)
        @networkId = networkId.to_i()
        @server = server
        @useActualTime = false
        @updatePass = 0
        @lastTime = System::DateTime.Now
    end

    # states is a list of [child, parent, parentInstance] triplets
    def sendStartTransition(from, to, parentInst, via, states)
        startMessage( MSG_START_TRANSITION )
        @bb.WriteUInt16( from )
        @bb.WriteUInt16( to )
        @bb.WriteUInt32( parentInst )
        @bb.WriteUInt16( via )
        @bb.WriteUInt8(states.length)
        states.each do |s|
            @bb.WriteUInt16(s[0])
            @bb.WriteUInt16(s[1])
            @bb.WriteUInt32(s[2])
        end
        endMessage
    end

    # states is a list of [child, parent, parentInstance] triplets
    def sendStopTransition(from, to, parentInst, via, states)
        startMessage( MSG_STOP_TRANSITION )
        @bb.WriteUInt16( from )
        @bb.WriteUInt16( to )
        @bb.WriteUInt32( parentInst )
        @bb.WriteUInt16( via )
        @bb.WriteUInt8(states.length)
        states.each { |state| 
            @bb.WriteUInt16(state[0])
            @bb.WriteUInt32(state[2])
        }
        endMessage
    end
    
    # states is a list of [child, parent, parentInstance] triplets
    def sendStartStateSet(states)
        startMessage( MSG_START_STATE_SET )
        @bb.WriteUInt8(states.length)
        states.each do |s|
            @bb.WriteUInt16(s[0])
            @bb.WriteUInt16(s[1])
            @bb.WriteUInt32(s[2])
        end
        endMessage
    end
    
    # states is a list of [child, parent, parentInstance] triplets
    def sendStopStateSet(states)
        startMessage( MSG_STOP_STATE_SET )
        @bb.WriteUInt8(states.length)
        states.each { |state| 
            @bb.WriteUInt16(state[0]) 
            @bb.WriteUInt32(state[2])
        }
        endMessage
    end
    
    # dt = -1 means use wall clock time
    def sendUpdateRoot(dt, statebits)
        newTime = System::DateTime.Now
        if dt < 0 then
            dt = newTime - @lastTime
        end
        @lastTime = newTime
        @elapsedTime += dt
        
        @updatePass += 1

        startMessage( MSG_UPDATE_TREE )
        @bb.WriteFloat(dt)
        @bb.WriteUInt16(statebits.length)
        statebits.each { |flag| @bb.WriteUInt32(flag) }
        endMessage
    end
    
    def sendSignalSet(id, signals)
        startMessage(MSG_SIGNAL_SET)
        @bb.WriteUInt16(signals.length)
        signals.each_pair do |key,value| 
            @bb.WriteUInt32(key);
            # todo: more stuff goes here
        end
    end

    def sendStartDebugging(respId, statebitWords, flags, flagOffset, requests, reqOffset)
        @bb = Rage::Move::Utils::ByteBuffer.new(255)
        @bb.BeginTcpBuffer()
        @bb.WriteUInt32(MSG_START_DEBUGGING)
        @bb.WriteInt32(respId)
        @bb.WriteUInt32( @networkId )
        @bb.WriteUInt32(@updatePass)
        @bb.WriteBool(true)
        @bb.WriteUInt16(statebitWords)
        @bb.WriteUInt16(flags)
        @bb.WriteUInt16(flagOffset)
        @bb.WriteUInt16(requests)
        @bb.WriteUInt16(reqOffset)
        @elapsedTime = System::DateTime.Now - @@launchTime;
        @bb.WriteFloat(@elapsedTime)
        endMessage()
        
        @lastAnim = ""
        @lastClip = ""
        @lastExpr = ""
        @lastFilter = ""
        @lastFrame = ""
        @lastPM = ""
    end
        
    def startMessage(messageId)
        @bb = Rage::Move::Utils::ByteBuffer.new(255)
        @bb.BeginTcpBuffer()
        @bb.WriteUInt32(messageId)
        @bb.WriteInt32(0)
        @bb.WriteUInt32( @networkId )
        @bb.WriteUInt32(@updatePass)
    end
    
    def endMessage()
        @bb.EndTcpBuffer()
        @server.BroadcastMessage(@bb)
    end
    
    def startSignalSet()
        @sigBuf = Rage::Move::Utils::ByteBuffer.new(255)
        @sigBuf.BeginTcpBuffer()
        @sigBuf.WriteUInt32(MSG_SIGNAL_SET)
        @sigBuf.WriteInt32(0)
        @sigBuf.WriteUInt32( @networkId )
        @sigBuf.WriteUInt32(@updatePass)
        @sigCountPos = @sigBuf.Length
        @sigCount = 0
        @sigBuf.WriteUInt16(@sigCount)
    end
    
    def addSignalReal(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        @sigBuf.WriteUInt32(sigId)
        @sigBuf.WriteUInt16(writer)
        @sigBuf.WriteUInt16(parent)
        @sigBuf.WriteUInt32(parentInstance)
        @sigBuf.WriteUInt8(8) # type
        @sigBuf.WriteFloat(value)
        @sigCount += 1
    end
    
    def addSignalBool(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        @sigBuf.WriteUInt32(sigId)
        @sigBuf.WriteUInt16(writer)
        @sigBuf.WriteUInt16(parent)
        @sigBuf.WriteUInt32(parentInstance)
        @sigBuf.WriteUInt8(value ? 17 : 16) # type
        @sigCount += 1
    end
    
    def addSignalAnim(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        @sigBuf.WriteUInt32(sigId)
        @sigBuf.WriteUInt16(writer)
        @sigBuf.WriteUInt16(parent)
        @sigBuf.WriteUInt32(parentInstance)
        if (value != @lastAnim)
            @sigBuf.WriteUInt8(24) # type
            @sigBuf.WriteString(value)
        else
            @sigBuf.WriteUInt8(25)
        end
        @lastAnim = value
        @sigCount += 1
    end
    
    def addSignalClip(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        @sigBuf.WriteUInt32(sigId)
        @sigBuf.WriteUInt16(writer)
        @sigBuf.WriteUInt16(parent)
        @sigBuf.WriteUInt32(parentInstance)
        if (value != @lastClip)
            @sigBuf.WriteUInt8(32) # type
            @sigBuf.WriteString(value)
        else
            @sigBuf.WriteUInt8(33)
        end
        @lastClip = value
        @sigCount += 1
    end
    
    def addSignalExpression(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        @sigBuf.WriteUInt32(sigId)
        @sigBuf.WriteUInt16(writer)
        @sigBuf.WriteUInt16(parent)
        @sigBuf.WriteUInt32(parentInstance)
        if (value != @lastExpr)
            @sigBuf.WriteUInt8(40) # type
            @sigBuf.WriteString(value)
        else
            @sigBuf.WriteUInt8(41)
        end
        @lastExpr = value
        @sigCount += 1
    end
    
    def addSignalFilter(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        @sigBuf.WriteUInt32(sigId)
        @sigBuf.WriteUInt16(writer)
        @sigBuf.WriteUInt16(parent)
        @sigBuf.WriteUInt32(parentInstance)
        if (value != @lastFilter)
            @sigBuf.WriteUInt8(48) # type
            @sigBuf.WriteString(value)
        else
            @sigBuf.WriteUInt8(49)
        end
        @lastFilter = value
        @sigCount += 1
    end
    
    def addSignalFrame(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        # not implemented yet
    end
    
    def addSignalPM(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        @sigBuf.WriteUInt32(sigId)
        @sigBuf.WriteUInt16(writer)
        @sigBuf.WriteUInt16(parent)
        @sigBuf.WriteUInt32(parentInstance)
        if (value != @lastPM)
            @sigBuf.WriteUInt8(64) # type
            @sigBuf.WriteString(value)
        else
            @sigBuf.WriteUInt8(65)
        end
        @lastPM = value
        @sigCount += 1
    end
    
    def addSignalNode(sigId, value, writer = 0, parent = 0, parentInstance = 0)
        @sigBuf.WriteUInt32(sigId)
        @sigBuf.WriteUInt16(writer)
        @sigBuf.WriteUInt16(parent)
        @sigBuf.WriteUInt32(parentInstance)
        @sigBuf.WriteUInt8(72)
        @sigBuf.WriteUInt16(value)
        @sigCount += 1
    end
    
    def endSignalSet()
        if (@sigCount > 0)
            @sigBuf.OverwriteUInt16(@sigCountPos, @sigCount)
            @sigBuf.EndTcpBuffer()
            @server.BroadcastMessage(@sigBuf)
        end
        @sigCount = 0
        @sigCountPos = -1
        @sigBuf = nil
    end    
end
