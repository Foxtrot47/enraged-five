﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace MockRuntimeServer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string ServerScript { get; set; }

        public static string ServerScriptSimulation {get; set;}

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if(e.Args.Length > 0)
            {
                ServerScript = e.Args[0];
            }
            if (e.Args.Length > 1)
            {
                ServerScriptSimulation = e.Args[1];
            }
           
        }
    }
}
