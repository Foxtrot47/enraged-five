﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MockRuntimeServer
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        MoveServer _server = new MoveServer(16000);

        public Window1()
        {
            InitializeComponent();
            
            if(App.ServerScript != null && App.ServerScript.Length > 0)
                _scriptPathTextBox.Text =  App.ServerScript;

            if (App.ServerScriptSimulation != null && App.ServerScriptSimulation.Length > 0)
            {
                _simulationTextBox.Text = App.ServerScriptSimulation;
            }

            _server.DisplayMessageEvent += new MoveServer.DisplayMessageHandler(Server_OnDisplayMessage);
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            _server.Start(_scriptPathTextBox.Text, _simulationTextBox.Text);
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            _server.Stop();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            _serverMessageTextBox.Text = String.Empty;
        }

        private void PickScriptButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".rb";
            dlg.Filter = "Ruby Scripts (*.rb)|*.rb";

            Nullable<bool> result = dlg.ShowDialog();
            if(result == true)
            {
                _scriptPathTextBox.Text = dlg.FileName;
            }
        }

        private delegate void AppendServerMessageCallback(string message);
      
        private void AppendServerMessage(string message)
        {
            _serverMessageTextBox.AppendText(message);
            _serverMessageTextBox.ScrollToLine(_serverMessageTextBox.LineCount - 1);
        }

        private void Server_OnDisplayMessage(object sender, DisplayMessageEventArgs e)
        {
            if(_serverMessageTextBox.Dispatcher.CheckAccess())
            {
                AppendServerMessage(e.Message);
            }
            else
            {
                _serverMessageTextBox.Dispatcher.Invoke(new AppendServerMessageCallback(AppendServerMessage), new object[] { e.Message });
            }
           
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            _serverMessageTextBox.Text = String.Empty;
            _server.Reset();
        }

        private void ResetAndReloadButton_Click(object sender, RoutedEventArgs e)
        {
            _serverMessageTextBox.Text = String.Empty;
            _server.ResetAndReload(_scriptPathTextBox.Text, _simulationTextBox.Text);
        }
    }
}
