﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Windows.Threading;

using IronRuby;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;

using Rage.Move.Utils;

namespace MockRuntimeServer
{
    public class DisplayMessageEventArgs
    {
        public DisplayMessageEventArgs(string msg)
        {
            Message = msg;
        }
        public string Message { get; set; }
    }

    public class SimulationNetworkInstance
    {
        public SimulationNetworkInstance()
        {
        }

        public uint InstanceId { get; set; }
        public Guid Id { get; set; }
        public int Version { get; set; }
        public int Crc { get; set; }
        public string Filename { get; set; }
        public string Description { get; set; }
    }

    public class SimulationContext
    {
        Dictionary<uint, SimulationNetworkInstance> _networkInstances = new Dictionary<uint, SimulationNetworkInstance>();

        public SimulationContext()
        {
            Version = 1;
        }

        public uint Version { get; set; }

        public Dictionary<uint, SimulationNetworkInstance> NetworkInstances
        {
            get { return _networkInstances; }
        }
    }

    internal class NetworkMessageIds
    {
        public const uint MSG_SYSINFO = 1;

        public const uint MSG_REQUEST_NETWORK_INSTANCES = 2;
        public const uint MSG_NETWORK_INSTANCES = 3;

        public const uint LINK_MSG_HANDLER_START = 100;

        public const uint MSG_REQUEST_START_DEBUGGING = 101;
        public const uint MSG_START_DEBUGGING = 102;

        public const uint MSG_REQUEST_STOP_DEBUGGING = 103;
        public const uint MSG_STOP_DEBUGGING = 104;
    }

    public class MoveServer
    {
        private class ConnectionInfo
        {
            public Socket Socket;
            public byte[] Buffer;
        }

        private List<ConnectionInfo> _connections =
            new List<ConnectionInfo>();

        private Socket _serverSocket;
        private int _port;
        private bool _listening = false;

        private ScriptEngine _engine;
        private ScriptScope _scope;
        private Thread _scriptThread = null;

        private SimulationContext _context = new SimulationContext();

        private string _simulationName;

        public delegate void DisplayMessageHandler(object sender, DisplayMessageEventArgs e);
        public event DisplayMessageHandler DisplayMessageEvent;

        public MoveServer(int port) 
        { 
            _port = port;

            InitializeScripting();
        }

        public SimulationContext Context
        {
            get { return _context; }
        }

        protected void InitializeScripting()
        {
            _engine = IronRuby.Ruby.CreateEngine();

            //Add the current assembly to the script engine
            _engine.Runtime.LoadAssembly(Assembly.GetExecutingAssembly());

            _scope = _engine.CreateScope();
            _scope.SetVariable("server", this);
        }

        public void Stop()
        {
            if (_scriptThread != null)
            {
                _scriptThread.Abort();
            }
            if (_engine != null)
            {
                _engine.Runtime.Shutdown();
            }

            if(_serverSocket != null)
            {
                _serverSocket.Close();
                if (_serverSocket.Connected)
                {
                    foreach (ConnectionInfo ci in _connections)
                    {
                        ci.Socket.Close();
                    }

                    lock (_connections) _connections.Clear();
                }
            }
            _serverSocket = null;
            _listening = false;
            DisplayMessage("Server stopped\n");
        }

        public void Start(string scriptPath, string simulationName)
        {
            _simulationName = simulationName;
            if (_listening)
            {
                DisplayMessage("Server already started.\n");
            }
            else
            {
                if(!LoadScript(scriptPath))
                {
                    DisplayMessage("Startup failed...\n");
                }
                else
                {
                    if(RunScriptInitialize())
                    {
                        SetupServerSocket();

                        _serverSocket.BeginAccept(
                            new AsyncCallback(AcceptCallback), _serverSocket);

                        _listening = true;

                        DisplayMessage("Server started, waiting for connection...\n");
                    }
                }
            }
        }

        public void Reset()
        {
            Stop();
            InitializeScripting();
        }

        public void ResetAndReload(string scriptPath, string simulationName)
        {
            Reset();
            Start(scriptPath, simulationName);
        }

        public void DisplayMessage(string message)
        {
            if (DisplayMessageEvent != null)
            {
                if (!message.EndsWith("\n"))
                    message += "\n";
                DisplayMessageEvent(this, new DisplayMessageEventArgs(message));
            }
        }

        private void SetupServerSocket()
        {
            _serverSocket = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint myEndPoint = new IPEndPoint(IPAddress.Any, _port);
            _serverSocket.Bind(myEndPoint);
            _serverSocket.Listen((int)SocketOptionName.MaxConnections);
        }

        private void AcceptCallback(IAsyncResult result)
        {
            ConnectionInfo connection = new ConnectionInfo();
            try
            {
                Socket s = (Socket)result.AsyncState;
                connection.Socket = s.EndAccept(result);
                connection.Buffer = new byte[1024];
                
                lock (_connections)
                {
                    _connections.Add(connection);
                }

                //Start receive and a new accept
                connection.Socket.BeginReceive(connection.Buffer, 0,
                    connection.Buffer.Length, SocketFlags.None,
                    new AsyncCallback(ReceiveCallback), connection);

                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), result.AsyncState);

                //Ping back the system information
                SendSystemInformation();

                DisplayMessage("Connection established.\n");
            }
            catch (SocketException exc)
            {
                CloseConnection(connection);
                DisplayMessage("SOCKET EXCEPTION : " + exc.SocketErrorCode);
            }
            catch (ObjectDisposedException)
            {
                //The listening socket was closed
            }
            catch (Exception exc)
            {
                CloseConnection(connection);
                DisplayMessage("EXCEPTION : " + exc);
            }
        }

        private void DisplayRubyException(System.Exception ex)
        {
            // Look to see if there's an ironruby exception
            foreach (System.Collections.DictionaryEntry de in ex.Data)
            {
                var rubyExcep = de.Value as IronRuby.Runtime.RubyExceptionData;
                if (rubyExcep != null)
                {
                    foreach (var item in rubyExcep.Backtrace)
                    {
                        string s = item.ToString();
                        if (!s.Contains(".rb"))
                        {
                            break;
                        }
                        DisplayMessage(item.ToString());
                    }
                }
            }
            DisplayMessage(ex.ToString());
        }

        private bool LoadScript(string scriptPath)
        {
            try
            {
                ScriptSource source = _engine.CreateScriptSourceFromFile(scriptPath);
                object result = source.Execute(_scope);
                if (result != null)
                {
                    DisplayMessage(result.ToString());
                }
                return true;
            }
            catch (Microsoft.Scripting.SyntaxErrorException ex)
            {
                DisplayMessage(String.Format("{0}({1}) : {2} : {3}", ex.SourcePath, ex.Line, ex.Severity.ToString(), ex.Message));
                DisplayMessage(String.Format("{0}({1}) : from : {2}", ex.SourcePath, ex.Line, ex.GetCodeLine()));
                DisplayMessage(ex.ToString());
            }
            catch (System.Exception ex)
            {
                DisplayRubyException(ex);
            }

            return false;
        }

        private bool RunScriptInitialize()
        {
            try
            {
                ScriptSource source = _engine.CreateScriptSourceFromString("doInitialize()");
                object result = source.Execute(_scope);
                return true;
            }
            catch (System.Exception ex)
            {
                DisplayRubyException(ex);
            }

            return false;
        }

        public class SimulationThreadArgs
        {
            public uint InstanceId;
            public int ResponderId;
            public string SimulationName;
        }

        private void SimulationStart(object state)
        {
            SimulationThreadArgs args = state as SimulationThreadArgs;
            try
            {
                string scriptString = String.Format("doSimulation(\"{0}\", \"{1}\", \"{2}\")", args.InstanceId.ToString(), args.ResponderId.ToString(), args.SimulationName);
                ScriptSource source = _engine.CreateScriptSourceFromString(scriptString);
                object result = source.Execute(_scope);
                if (result != null)
                {
                    DisplayMessage(result.ToString());
                }
            }
            catch (System.Exception ex)
            {
                DisplayRubyException(ex);
            }
        }

        public void RunSimulation(uint instanceId, int responderId, string simName)
        {
            if(_context.NetworkInstances.ContainsKey(instanceId))
            {
                if (_scriptThread != null)
                {
                    while (_scriptThread.ThreadState != ThreadState.Stopped)
                    {
                        Thread.Sleep(100);
                    }
                }

                SimulationThreadArgs args = new SimulationThreadArgs();
                args.InstanceId = instanceId;
                args.SimulationName = simName;
                args.ResponderId = responderId;

                _scriptThread = new Thread(SimulationStart);
                _scriptThread.Name = "ScriptSimulationThread";
                _scriptThread.IsBackground = true;
                _scriptThread.Start(args);
            }
        }

        private void ReceiveCallback(IAsyncResult result)
        {
            ConnectionInfo connection = (ConnectionInfo)result.AsyncState;
            try
            {
                int bytesRead = connection.Socket.EndReceive(result);
                if (0 != bytesRead)
                {
                    ProcessIncomingMessage(connection.Buffer);

                    connection.Socket.BeginReceive(connection.Buffer, 0,
                        connection.Buffer.Length, SocketFlags.None,
                        new AsyncCallback(ReceiveCallback), connection);
                }
                else
                {
                    //The client disconnected
                    DisplayMessage("Client disconnected...\n");
                    CloseConnection(connection);
                }
            }
            catch (SocketException exc)
            {
                if (exc.SocketErrorCode == SocketError.ConnectionReset)
                {
                    DisplayMessage("Client disconnected...\n");
                }
                CloseConnection(connection);
                Console.WriteLine("Socket exception: " + exc.SocketErrorCode);
            }
            catch (Exception exc)
            {
                CloseConnection(connection);
                Console.WriteLine("Exception: " + exc);
            }
        }

        private void CloseConnection(ConnectionInfo ci)
        {
            ci.Socket.Close();
            lock (_connections) _connections.Remove(ci);
        }

        public void BroadcastMessage(ByteBuffer bb)
        {
            lock (_connections)
            {
                foreach (ConnectionInfo ci in _connections)
                {
                    SocketError error;
                    try
                    {
                        if(ci.Socket.Connected)
                        {
                            int bytesSent = ci.Socket.Send(bb.Storage, 0, bb.Length, SocketFlags.None, out error);
                            if (error != SocketError.Success)
                            {
                                Console.Write("Socket Error : {0}", error);
                            }
                        }
                    }
                    catch (SocketException ex)
                    {
                        DisplayMessage(String.Format("Socket Exception {0}", ex.Message));
                    }  
                }
            }
        }

        private void SendSystemInformation()
        {
            ByteBuffer bb = new ByteBuffer(128);
            bb.BeginTcpBuffer();
            bb.WriteUInt32(NetworkMessageIds.MSG_SYSINFO);
            bb.WriteUInt32(Context.Version);
            bb.EndTcpBuffer();
            BroadcastMessage(bb);
        }

        private void ProcessIncomingMessage(byte[] buffer)
        {
            ByteBuffer bb = new ByteBuffer((uint)buffer.Length);
            Buffer.BlockCopy(buffer, 0, bb.Storage, 0, buffer.Length);

            uint packetSize = bb.ReadUInt32();
            uint packetType = bb.ReadUInt32();
           
            switch (packetType)
            {   
                case NetworkMessageIds.MSG_REQUEST_NETWORK_INSTANCES:
                    {
                        int responderId = bb.ReadInt32();
                        ByteBuffer rbb = new ByteBuffer(2048);
                        rbb.BeginTcpBuffer();
                        rbb.WriteUInt32(NetworkMessageIds.MSG_NETWORK_INSTANCES);
                        rbb.WriteInt32(responderId);
                        rbb.WriteInt32(Context.NetworkInstances.Count);
                        foreach (KeyValuePair<uint, SimulationNetworkInstance> item in _context.NetworkInstances)
                        {
                            SimulationNetworkInstance ni = item.Value;
                            rbb.WriteUInt32(ni.InstanceId);
                            rbb.WriteGuid(ni.Id);
                            rbb.WriteInt32(ni.Version);
                            rbb.WriteInt32(ni.Crc);
                            rbb.WriteString(ni.Filename);
                            rbb.WriteString(ni.Description);
                        }
                        rbb.EndTcpBuffer();
                        BroadcastMessage(rbb);
                        DisplayMessage("Received MSG_REQUEST_NETWORK_INSTANCES\n");
                        break;
                    }
                case NetworkMessageIds.MSG_REQUEST_START_DEBUGGING:
                    {
                        int responderId = bb.ReadInt32();
                        uint instanceId = bb.ReadUInt32();
                        //ByteBuffer rbb = new ByteBuffer(2048);
                        //rbb.BeginTcpBuffer();
                        //rbb.WriteUInt32(NetworkMessageIds.MSG_START_DEBUGGING);
                        //rbb.WriteInt32(responderId);
                        //rbb.WriteGuid(networkId);
                        //rbb.WriteFloat(0.0f);
                        //rbb.WriteBool(true);
                        //rbb.EndTcpBuffer();
                        //BroadcastMessage(rbb);
                        //DisplayMessage("Received MSG_REQUEST_START_DEBUGGING\n");

                        //Start the simulation thread
                        RunSimulation(instanceId, responderId, _simulationName);

                        break;
                    }
                //case NetworkMessageIds.MSG_REQUEST_NETWORK_INSTANCE_DATA:
                //    {
                //        uint responseType = bb.ReadUInt32();
                //        uint networkId = bb.ReadUInt32();

                //        SimulationNetworkInstance sni = 
                //            (from networkInstance in Context.NetworkInstances where networkInstance.Id == networkId select networkInstance).FirstOrDefault();

                //        ByteBuffer rbb = new ByteBuffer(128);
                //        rbb.BeginTcpBuffer();
                //        rbb.WriteUInt32(responseType);
                //        if(sni != null)
                //        {
                //            rbb.WriteInt32(sni.Version);
                //            rbb.WriteString(sni.Filename);
                //        }
                //        else
                //        {
                //            rbb.WriteUInt32(0);
                //            rbb.WriteString("unknown");
                //        }
                //        rbb.EndTcpBuffer();
                //        BroadcastMessage(rbb);
                //        DisplayMessage("Received MSG_REQUEST_NETWORK_INSTANCE_DATA\n");
                //        break;
                //    }
                //case NetworkMessageIds.MSG_REQUEST_NETWORK_ACTIVE_STATES:
                //    {
                //        uint responseType = bb.ReadUInt32();
                //        ByteBuffer rbb = new ByteBuffer(128);
                //        rbb.BeginTcpBuffer();
                //        rbb.WriteUInt32(responseType);
                //        rbb.WriteUInt32(1);
                //        rbb.WriteInt32(Context.StartState);
                //        rbb.EndTcpBuffer();
                //        BroadcastMessage(rbb);
                //        DisplayMessage("Received MSG_REQUEST_NETWORK_ACTIVE_STATES\n");  
                //        break;
                //    }
                default:
                    DisplayMessage(String.Format("Received packet [{0}, {1}]", packetSize, packetType));
                    break;
            }
        }
    }
}
