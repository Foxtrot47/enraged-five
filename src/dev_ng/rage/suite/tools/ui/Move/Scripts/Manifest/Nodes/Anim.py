import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(node, stream, serializer):
	filename = node.GetAttribute("File")
	loop = node.GetAttribute("Loop")

	#stream.AppendString(filename.Value, 128)
	#append an int here for a potential string pointer...
	stream.AppendUInt32(0);
	stream.AppendBool(loop.Value)
	
	source = node.Parent.GetSourceOf(node.GetAnchor("Phase", 0))
	signal = node.Parent.GetControlSignalOf(node.GetAnchor("Phase", 0))
	animation = node.Parent.GetControlSignalOf(node.GetAnchor("Anim", 0))
	if source != None: 
		stream.AppendInt32(1)
	elif signal != None:
		stream.AppendInt32(1)
	else:
		sink = node.Parent.GetSinkOf(node.GetAnchor("Phase", 1))
		if sink != None:
			stream.AppendInt32(2)
		elif animation != None:
			stream.AppendInt32(3)
		else:
			stream.AppendInt32(0)
	
def Validate():
	return True
		
def Initialize(node):
	node.Anchors.Add(TransformResult("Result"))
	node.Anchors.Add(AnimationSource("Anim"))
	node.Anchors.Add(SignalSource("Phase"))
	node.Anchors.Add(SignalSource("Rate"))
	node.Anchors.Add(SignalSource("Time"))
	node.Anchors.Add(SignalSource("Delta"))
	node.Anchors.Add(SignalSource("Loop"))
	node.Anchors.Add(SignalSource("Absolute"))
	node.Anchors.Add(SignalResult("Phase"))
	node.Anchors.Add(MessageResult("Looped"))
	node.Anchors.Add(MessageResult("Ended"))
	
	node.Attributes.Add(StringAttribute("File"))
	node.Attributes.Add(BoolAttribute("Loop", True))
		
node = RegisterNode('Anim', 3)
node.Help = 'Supplies animation data'
node.Group = 'Utilities'
node.Image = 'Anim.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize

