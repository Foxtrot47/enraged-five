#require 'mscorlib, Version=2.0.0.0, Culture=neutral, 
#	PublicKeyToken=b77a5c561934e089, processorArchitecture=x86'
#require 'System, Version=2.0.0.0, Culture=neutral, 
#	PublicKeyToken=b77a5c561934e089, processorArchitecture=MSIL'
require 'WindowsBase, Version=3.0.0.0, 
	Culture=neutral, PublicKeyToken=31bf3856ad364e35'
	
#C:\soft\rage\suite\tools\Move\Move\bin\Debug>..\..\..\ironruby\bin\ir build_network.rb
	
require 'Rage.Move.Core'

Animation = Rage::Move::Core::Dg::ControlSignalAnimation
Clip = Rage::Move::Core::Dg::ControlSignalClip
Collapsability = Rage::Move::Core::Collapsability
Connection = Rage::Move::Core::Dg::SignalConnection
Expression = Rage::Move::Core::Dg::ControlSignalExpression
Filter = Rage::Move::Core::Dg::ControlSignalFilter
Frame = Rage::Move::Core::Dg::ControlSignalFrame	
ParameterizedMotion = Rage::Move::Core::Dg::ControlSignalParameterizedMotion
Point = System::Windows::Point
Reference = Rage::Move::Core::Dg::Reference
Runtime = Rage::Move::Core::Runtime	
Single = Rage::Move::Core::Dg::ControlSignalFloat
Switch = Rage::Move::Core::Dg::ControlSignalBool
	
$mixer_channel = "C:\\Documents and Settings\\darchard\\My Documents\\RAGE.MOVE\\mixer_channel.motion"
$additive_channel = "C:\\Documents and Settings\\darchard\\My Documents\\RAGE.MOVE\\additive_channel.motion"
	
def create(type)
	reg = Runtime::instance.find_registered_node type
	reg.create
end	
	
def connect(source, anchor)
	Runtime::database.control_signals.add source
	connection = Connection.new source.guid, anchor.guid
	$root.connections.add connection
end	
	
$inc_pos = Point.new 250.0, 50.0
$inc_offset = 100.0	

def get_position()
	pos = $inc_pos
	$inc_pos = Point.new $inc_pos.x, $inc_pos.y + $inc_offset
	pos
end
	
runtime = Rage::Move::Core::Runtime.new
Runtime::database.clear
Runtime::database.root = Runtime::database.create_motion_tree
Runtime::database.active = Runtime::database.root

$root = Runtime::database.root

# mixer mover	
mover_blendn = create "BlendN"
mover_blendn.position = Point.new 750.0, 500.0
mover_blendn.name = "Mixer Mover"
$root.children.add mover_blendn

frame = Frame.new "MIXER_MOVER_FRAME"
Runtime::database.control_signals.add frame

mover_blendn.arrays.first.create mover_blendn.anchors
connect Single.new("MIXER_MOVER_WEIGHT_0"), 
	mover_blendn.get_anchors("Weight").last
		
frame0 = create "Frame"
frame0.position = Point.new 575, 276
$root.children.add frame0

connection = Connection.new frame.guid, 
	frame0.get_anchor("Frame").guid
$root.connections.add connection

connection = Connection.new	frame0.get_anchor("Result").guid, 
	mover_blendn.get_anchors("Source").last.guid
$root.connections.add connection	

def dump_mixer(prefix, mixer)
	(1..21).each do |index|
		reference = Reference.new $mixer_channel
		reference.position = get_position
		$root.children.add reference
		
		idx = index.to_s
		
		connect Animation.new(prefix + "_ANIMATION_" + idx), 
			reference.get_anchor("Animation")
		connect Expression.new(prefix + "_EXPRESSION_" + idx),
			reference.get_anchor("Expr")
		connect Filter.new(prefix + "_FILTER_" + idx),
			reference.get_anchor("Filter")
		connect Single.new(prefix + "_PHASE_" + idx),
			reference.get_anchor("Phase")
		connect Single.new(prefix + "_RATE_" + idx),
			reference.get_anchor("Rate")
		connect Single.new(prefix + "_TIME_" + idx),
			reference.get_anchor("Time")
		connect Single.new(prefix + "_DELTA_" + idx),
			reference.get_anchor("Delta")
		connect Switch.new(prefix + "_LOOPING_" + idx),
			reference.get_anchor("Looping")
		connect Switch.new(prefix + "_ABSOLUTE_" + idx),
			reference.get_anchor("Absolute")
		connect Switch.new(prefix + "_EXPR_SELECT_" + idx),
			reference.get_anchor("Activate_Expr")
		connect Switch.new(prefix + "_FILTER_SELECT_" + idx),
			reference.get_anchor("Activate_Filter")
		
		mixer.arrays.first.create mixer.anchors
		
		connection = Connection.new reference.get_anchor("Result").guid, 
			mixer.get_anchors("Source").last.guid
		$root.connections.add connection
		connect Single.new(prefix + "_WEIGHT_" + idx), 
			mixer.get_anchors("Weight").last
	end
end

dump_mixer("MIXER_MOVER", mover_blendn)
	
# mixer
mixer_blendn = create "BlendN"
mixer_blendn.name = "Mixer"
mixer_blendn.position = Point.new 750.0, 2500.0
$root.children.add mixer_blendn

rd_extrap = create "Extrapolate"
rd_extrap.position = Point.new 465, 2100
$root.children.add rd_extrap
rd_filter = create "Filter"
rd_filter.position = Point.new 635, 2325
$root.children.add rd_filter

connect Filter.new("RAGDOLL_FILTER"), rd_filter.get_anchor("Filter")

connection = Connection.new rd_extrap.get_anchor("Result").guid,
	rd_filter.get_anchor("Result").guid
$root.connections.add connection	

connect Single.new("RAGDOLL_DAMPING"),
	rd_extrap.get_anchor("Damping")
connect Single.new("RAGDOLL_DELTA_TIME"),
	rd_extrap.get_anchor("Delta Time")
connect Frame.new("RAGDOLL_LAST_FRAME"),
	rd_extrap.get_anchor("Last Frame")
connect Switch.new("RAGDOLL_OWN_LAST"),
	rd_extrap.get_anchor("Own Last Frame")
connect Frame.new("RAGDOLL_DELTA_FRAME"),
	rd_extrap.get_anchor("Delta Frame")
connect Switch.new("RAGDOLL_OWN_DELTA"),
	rd_extrap.get_anchor("Own Delta Frame")

mixer_blendn.arrays.first.create mixer_blendn.anchors
connect Single.new("RAGDOLL_WEIGHT"),
	mixer_blendn.get_anchors("Weight").last

connection = Connection.new rd_filter.get_anchor("Result").guid,
	mixer_blendn.get_anchors("Source").last.guid
$root.connections.add connection	
	
dump_mixer("MIXER", mixer_blendn)

#other inputs
mover_filter = Filter.new "MOVER_FILTER"
Runtime::database.control_signals.add mover_filter
mover_filter2 = Filter.new "MOVER_FILTER2"
Runtime::database.control_signals.add mover_filter2
lod_filter = Filter.new "LOD_FILTER"
Runtime::database.control_signals.add lod_filter
port_upper = Expression.new "PORT_UPPER_BODY_FIXUP"
Runtime::database.control_signals.add port_upper
blend_weight = Single.new "BLEND_WEIGHT"
Runtime::database.control_signals.add blend_weight
add_weight = Single.new "ADD_WEIGHT"
Runtime::database.control_signals.add add_weight

# additive mixer	
add_mixer = create "AddN"
add_mixer.name = "Additive Mixer"
add_mixer.position = Point.new 750.0, 4500.0
$root.children.add add_mixer

(0..21).each do |index|
	reference = Reference.new $additive_channel
	reference.position = get_position
	$root.children.add reference
	
	idx = index.to_s
	
	connect Animation.new("ADDITIVE_MIXER_ANIMATION_" + idx),
		reference.get_anchor("Animation")
	connect Filter.new("ADDITIVE_MIXER_FILTER_" + idx),
		reference.get_anchor("Filter")
	connect Single.new("ADDITIVE_MIXER_PHASE_" + idx),
		reference.get_anchor("Phase")
	connect Single.new("ADDITIVE_MIXER_RATE_" + idx),
		reference.get_anchor("Rate")
	connect Single.new("ADDITIVE_MIXER_TIME_" + idx),
		reference.get_anchor("Time")
	connect Single.new("ADDITIVE_MIXER_DELTA_" + idx),
		reference.get_anchor("Delta")
	connect Switch.new("ADDITIVE_MIXER_LOOPING_" + idx),
		reference.get_anchor("Looping")
	connect Switch.new("ADDITIVE_MIXER_ABSOLUTE_" + idx),
		reference.get_anchor("Absolute")
	connect Switch.new("ADDITIVE_MIXER_FILTER_SELECT_" + idx),
		reference.get_anchor("Active_Filter")
		
	add_mixer.arrays.first.create add_mixer.anchors
	
	connection = Connection.new reference.get_anchor("Result").guid, 
		add_mixer.get_anchors("Source").last.guid
	$root.connections.add connection
	connect Single.new("ADDITIVE_MIXER_WEIGHT_" + idx), 
		add_mixer.get_anchors("Weight").last
end	
	
filter = create "Filter"
filter.position = Point.new 1000, 1000
$root.children.add filter

connection = Connection.new mover_filter2.guid,
	filter.get_anchor("Filter").guid
$root.connections.add connection

connection = Connection.new mover_blendn.get_anchor("Result").guid,
	filter.get_anchor("Result").guid
$root.connections.add connection

expr = create "Expr"
expr.position = Point.new 1175, 2854
$root.children.add expr

connection = Connection.new port_upper.guid,
	expr.get_anchor("Expression").guid
$root.connections.add connection

connection = Connection.new mixer_blendn.get_anchor("Result").guid,
	expr.get_anchor("Result").guid
$root.connections.add connection

add2 = create "Add2"
add2.position = Point.new 1510, 3160
$root.children.add add2

connection = Connection.new add_weight.guid,
	add2.get_anchor("Weight").guid
$root.connections.add connection

connection = Connection.new expr.get_anchor("Result").guid,
	add2.get_anchor("Source0").guid
$root.connections.add connection

connection = Connection.new add_mixer.get_anchor("Result").guid,
	add2.get_anchor("Source1").guid
$root.connections.add connection

blend2 = create "Blend2"
blend2.position = Point.new 1930, 2595
$root.children.add blend2

connection = Connection.new mover_filter.guid,
	blend2.get_anchor("Filter").guid
$root.connections.add connection
connection = Connection.new blend_weight.guid,
	blend2.get_anchor("Weight").guid
$root.connections.add connection

connection = Connection.new filter.get_anchor("Result").guid,
	blend2.get_anchor("Source0").guid
$root.connections.add connection

connection = Connection.new add2.get_anchor("Result").guid,
	blend2.get_anchor("Source1").guid
$root.connections.add connection

lod = create "Filter"
lod.position = Point.new 2535, 3055
lod.name = "Lod Filter"
$root.children.add lod

connection = Connection.new lod_filter.guid, 
	lod.get_anchor("Filter").guid
$root.connections.add connection
	
connection = Connection.new blend2.get_anchor("Result").guid,
	lod.get_anchor("Result").guid
$root.connections.add connection	
	
$root.output_position = Point.new 2870, 3025

connection = Connection.new lod.get_anchor("Result").guid,
		$root.get_anchor("Result").guid
$root.connections.add connection
	
Runtime::database.save 'C:\\soft\\rage\\suite\\tools\\Move\\Move\\bin\\Debug\\jimmy.mte'
