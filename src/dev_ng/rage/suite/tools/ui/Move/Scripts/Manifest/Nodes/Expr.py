import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(node, stream, serializer):
	filename = node.GetAttribute("File")
	#stream.AppendString(filename.Value, 128)
	#append an int here for a potential string pointer...
	stream.AppendUInt32(0);
	
	source = serializer.GetSourceOf(node, "Result")
	
	#anchor = node.GetAnchor("Result")
	#parent = node.Parent
	#ource = parent.GetSourceOf(anchor)
	
	stream.AppendOffset(serializer.GetLabel(source))
	
def Validate():
	return True

def Initialize(node):
	node.Anchors.Add(TransformPassthrough("Result"))
	node.Anchors.Add(ExpressionSource("Expression"))
	node.Attributes.Add(StringAttribute("File"))
	
node = RegisterNode("Expr", 8)
node.Help = 'Adds expression data'
node.Group = 'Utilities'
node.Image = 'Expr.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize	

	