import clr;
clr.AddReferenceByName("Rage.Move.Core")

#from System import *
from Rage.Move.Core.Dg import *

def Serialize(transition, stream):
	duration = transition.GetAttribute("Duration")
	modifier = transition.GetAttribute("Modifier")
	
	stream.AppendFloat(duration.Value)
	stream.AppendInt32(modifier.Modifier)
	
def Validate():
	return True

def Intitialize(transition):
	transition.Attributes.Add(ModifierAttribute("Modifier", Modifier.Linear))
	transition.Attributes.Add(FloatAttribute("Duration", 0.5))
	
transition = RegisterTransition('Transit', 0)
transition.Help = "Performs a transitional blend between two states over a given period of time"
transition.Serialize = Serialize
transition.Validate = Validate
transition.Initialize = Intitialize
