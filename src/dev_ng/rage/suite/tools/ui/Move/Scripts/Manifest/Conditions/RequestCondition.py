import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(condition, stream):
	request = condition.GetAttribute("Request")
	notset = condition.GetAttribute("Not Set")
	
	stream.AppendInt32(request.Value.GetIndex())
	stream.AppendBool(notset.Value)
	
def Validate():
	return True

def Initialize(condition):
	condition.Attributes.Add(RequestAttribute("Request"))
	condition.Attributes.Add(BoolAttribute("Not Set", False))
	
condition = RegisterCondition("Request Condition", 5)
condition.Serialize = Serialize
condition.Validate = Validate
condition.Initialize = Initialize	
	