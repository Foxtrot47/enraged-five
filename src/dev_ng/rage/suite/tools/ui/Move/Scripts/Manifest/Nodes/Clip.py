import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(node, stream, serializer):
	filename = node.GetAttribute("File")
	loop = node.GetAttribute("Loop")

	#stream.AppendString(filename.Value, 128)
	#append an int here for a potential string pointer...
	stream.AppendUInt32(0);
	stream.AppendBool(loop.Value)

def Validate():
	return True
	
def Initialize(node):	
	node.Anchors.Add(TransformResult("Result"))
	node.Anchors.Add(ClipSource("Clip"))
	node.Anchors.Add(SignalSource("Phase"))
	node.Anchors.Add(SignalSource("Rate"))
	node.Anchors.Add(SignalSource("Time"))
	node.Anchors.Add(SignalSource("Delta"))
	
	node.Attributes.Add(StringAttribute("File"))
	node.Attributes.Add(BoolAttribute("Loop", True))
	
node = RegisterNode("Clip", 6)
node.Help = 'Supplies animation clip data'
node.Group = 'Utilities'
node.Image = 'Clip.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize	
	