import clr
clr.AddReferenceByName("Rage.Move.Core")

def Serialize(node, stream, serializer):
	return
	
def Validate():
	return True
	
def Initialize(node):
	node.Anchors.Add(TransformResult("Result"))
	node.Anchors.Add(SignalSource("Damping"))
	node.Anchors.Add(FrameSource("LastFrame"))
	node.Anchors.Add(FrameSource("DeltaFrame"))
	node.Anchors.Add(SignalSource("OwnLast"))
	node.Anchors.Add(SignalSource("OwnDelta"))
	return
	
node = RegisterNode("Extrapolate", 13)
node.Help = 'Extrapolate from last frame'
node.Group = 'Utils'
node.Image = 'Extrapolate.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize
