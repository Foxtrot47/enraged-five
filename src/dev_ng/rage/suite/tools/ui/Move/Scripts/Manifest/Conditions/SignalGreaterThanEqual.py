import clr
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(condition, stream):
	signal = condition.Get("Signal")
	value = condition.Get("Trigger Value")

	stream.AppendData(signal)
	stream.AppendData(value)

def Validate():
	return True

def Initialize(condition):
	condition.Attributes.Add(SignalAttribute("Signal"))
	condition.Attributes.Add(FloatAttribute("Trigger Value", 0.5))
	
condition = RegisterCondition("Signal Greater Than Equal", 12)
condition.Serialize = Serialize
condition.Validate = Validate
condition.Initialize = Initialize
	