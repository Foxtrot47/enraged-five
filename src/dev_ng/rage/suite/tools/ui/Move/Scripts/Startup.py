import clr

clr.AddReference("mscorlib")
clr.AddReference("System")
clr.AddReference("System.Core")
clr.AddReference("System.Data")
clr.AddReference("WindowsBase")
clr.AddReference("PresentationCore")
clr.AddReference("PresentationFramework")
clr.AddReferenceByName("Rage.Move")
clr.AddReferenceByName("Rage.Move.Core")
clr.AddReferenceByName("Rage.Move.Utils")

from System.Diagnostics import *
from System.IO import *
from System.Windows import *

from Rage.Move.Core import *
from Rage.Move.Core.Dag import *

	