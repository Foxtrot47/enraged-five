import clr
clr.AddReferenceByName("Rage.Move.Core")

def Serialize(node, stream, serializer):
	inputs = node.GetAnchors("Source")
	stream.AppendInt32(inputs.Count)
	
	weights = node.GetAnchors("Weight")
	stream.AppendInt32(weights.Count)
	
	for input in inputs:
		source = node.Parent.GetSourceOf(input)
		stream.AppendOffset(serializer.GetLabel(source))
	
	for weight in weights:
		source = node.Parent.GetControlSignalOf(weight)
		index = Runtime.Database.ControlSignals.IndexOf(source)
		stream.AppendInt32(index)
	
def Validate():
	return True
	
def Initialize(node):
	node.Arrays.Add(ArrayPair("Input", TransformSourceArray("Source"), SignalSourceArray("Weight")))
	node.Anchors.Add(TransformResult("Result"))
	
node = RegisterNode("BlendN", 12)
node.Help = 'Blend N inputs together'
node.Group = 'Blends'
node.Image = 'BlendN.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize