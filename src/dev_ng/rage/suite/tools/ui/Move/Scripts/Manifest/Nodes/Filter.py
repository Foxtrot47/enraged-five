import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(node, stream, serializer):
	#anchor = node.GetAnchor("Result")
	#parent = node.Parent
	#source = parent.GetSourceOf(anchor)
	
	source = serializer.GetSourceOf(node, "Result")
	
	stream.AppendOffset(serializer.GetLabel(source))
	
	filter = node.GetAttribute("Filter")
	
	stream.AppendFloat(filter.Value.Root)
	stream.AppendFloat(filter.Value.RightFoot)
	stream.AppendFloat(filter.Value.LeftFoot)
	stream.AppendFloat(filter.Value.RightKnee)
	stream.AppendFloat(filter.Value.LeftKnee)
	stream.AppendFloat(filter.Value.RightHip)
	stream.AppendFloat(filter.Value.LeftHip)
	stream.AppendFloat(filter.Value.Pelvis)
	stream.AppendFloat(filter.Value.Spine)
	stream.AppendFloat(filter.Value.Neck)
	stream.AppendFloat(filter.Value.Head)
	stream.AppendFloat(filter.Value.Face)
	stream.AppendFloat(filter.Value.RightHand)
	stream.AppendFloat(filter.Value.LeftHand)
	stream.AppendFloat(filter.Value.RightElbow)
	stream.AppendFloat(filter.Value.LeftElbow)
	stream.AppendFloat(filter.Value.RightShoulder)
	stream.AppendFloat(filter.Value.LeftShoulder)
	stream.AppendFloat(filter.Value.Mover)
	
def Validate():
	return True
	
def Initialize(node):
	node.Anchors.Add(TransformPassthrough("Result"))
	node.Anchors.Add(FilterSource("Filter"))
	
	node.Attributes.Add(FilterAttribute("Filter"))
	
node = RegisterNode("Filter", 5)
node.Help = 'Filters an animation source'
node.Group = 'Utilities'
node.Image = 'Filter.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize		
	