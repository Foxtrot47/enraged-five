import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(transition, stream):
	duration = transition.Get("Duration")
	modifier = transition.Get("Modifier")
	event = transition.Get("Event")
	
	stream.AppendData(duration)
	stream.AppendData(modifier)
	stream.AppendData(event)
	
def Validate():
	return True

def Initialize(transition):
	transition.Attributes.Add(ModifierAttribute("Modifier", Modifier.Linear))
	transition.Attributes.Add(FloatAttribute("Duration", 0.5))
	transition.Attributes.Add(IntAttribute("Event", 0))
	
transition = RegisterTransition('TransitAtEvent', 1)
transition.Help = "Performs a transitional blend at an event between two states over a given period of time"
transition.Serialize = Serialize
transition.Validate = Validate
transition.Initialize = Initialize	
	
#Runtime.Instance.RegisterTransition("TransitAtEvent",
#	{
#		'help' : "Performs a transitional blend at an event between two states over a given period of time",
#		'id' : 1,
#		
#		'attributes' : 
#		[
#			{ 'name' : "Modifier", 'type' : "Modifier", 'value' : "Linear" },
#			{ 'name' : "Duration", 'type' : "System.Single", 'value' : 0.5, 'min' : 0.0 },
#			{ 'name' : "Event", 'type' : "System.UInt32", 'value' : 0, 'min' : 0 }
#		],
#		
#		'serialize' : Serialize,
#		'validate' : Validate
#	}
#)