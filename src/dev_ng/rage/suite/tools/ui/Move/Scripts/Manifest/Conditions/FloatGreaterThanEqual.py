import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(condition, stream):
	parameter = condition.Get("Control Parameter")
	value = condition.Get("Trigger Value")
	
	stream.AppendData(parameter)
	stream.AppendData(value)
	
def Validate():
	return True

def Initialize(condition):
	condition.Attributes.Add(ControlSignalAttribute("Control Signal"))
	condition.Attributes.Add(FloatAttribute("Trigger Value", 0.5))
	
condition = RegisterCondition("Float Greater Than Equal", 4)
condition.Serialize = Serialize
condition.Validate = Validate
condition.Initialize = Initialize	
	