import clr
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(condition, stream):
	signal = condition.GetAttribute("Control Signal")
	value = condition.GetAttribute("Value")
	
	#todo: where does this data source come from
	# IAttribute
	index = Runtime.Instance.GetDataSource(signal)
	
	#index = Runtime.Database.ControlSignals.IndexOf(signal.Value)
	stream.AppendInt32(index)
	stream.AppendInt32(value.Value)
	
def Validate():
	return True
	
def Initialize(condition):
	condition.Attributes.Add(ControlSignalAttribute("Control Signal"))
	condition.Attributes.Add(BoolAttribute("Value", False))
	
condition = RegisterCondition("Bool Equals", 14)
condition.Serialize = Serialize
condition.Validate = Validate
condition.Initialize = Initialize