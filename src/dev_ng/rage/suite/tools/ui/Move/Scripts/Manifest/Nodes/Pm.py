import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(node, stream, serializer):
	filename = node.GetAttribute("File")
	#stream.AppendString(filename.Value, 128)
	#append an int here for a potential string pointer...
	stream.AppendUInt32(0);

	stream.AppendInt32(0) #input.Count
	
	#input = node.GetDataArray('Input')
	#stream.AppendData(input.Count)
	
	#for child in range(input.Count):
	#	parameter = input.Children[child].Get()
	#	stream.AppendData(parameter)
		
def Validate():
	return True
	
def Initialize(node):
	node.Anchors.Add(TransformResult("Result"))
	node.Anchors.Add(ParameterizedMotionSource("Pm"))
	# need a better way for 'arrays' of data
	
	node.Attributes.Add(StringAttribute("File"))
	
node = RegisterNode("Pm", 7)
node.Help = 'Supplies parameterized motion data'
node.Group = 'Utilities'
node.Image = 'Pm.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize		

