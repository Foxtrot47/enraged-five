import clr
clr.AddReferenceByName("Rage.Move.Core")

def Serialize(node, stream, serializer):
	inputs = node.GetAnchors("Source")
	stream.AppendInt32(inputs.Count)

	for input in inputs:
		source = node.Parent.GetSourceOf(input)
		stream.AppendOffset(serializer.GetLabel(source))
	
	weights = node.GetAnchors("Weight")
	
	for weight in weights:
		source = node.Parent.GetControlSignalOf(weight)
		index = Runtime.Database.ControlSignals.IndexOf(source)
		stream.AppendInt32(index)
	
def Validate():
	return True
	
def Initialize(node):
	node.Arrays.Add(ArrayPair("Input", TransformSourceArray("Source"), SignalSourceArray("Weight")))
	node.Anchors.Add(TransformResult("Result"))
	
node = RegisterNode("AddN", 11)
node.Help = 'Add N inputs together'
node.Group = 'Blends'
node.Image = 'AddN.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize