import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(condition, stream):
	event = condition.Get("Event")
	
	stream.AppendData(event)
	
def Validate():
	return True

def Initialize(condition):
	condition.Attributes.Add(IntAttribute("Event"))
	
condition = RegisterCondition("At Event", 7)
condition.Serialize = Serialize
condition.Validate = Validate
condition.Initialize = Initialize	
	