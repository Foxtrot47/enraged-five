import clr
clr.AddReferenceByName("Rage.Move.Core")

def Serialize(node, stream, serializer):
	anchor0 = node.GetAnchor("Source0")
	source0 = node.Parent.GetSourceOf(anchor0)
	
	stream.AppendOffset(serializer.GetLabel(source0))
	
	anchor1 = node.GetAnchor("Source1")
	source1 = node.Parent.GetSourceOf(anchor1)
	
	stream.AppendOffset(serializer.GetLabel(source1))

	anchor = node.GetAnchor("Weight")
	
	weight = node.Parent.GetControlSignalOf(anchor)

	index = Runtime.Database.ControlSignals.IndexOf(weight)
	stream.AppendInt32(index)
	
def Validate():
	return True
	
def Initialize(node):
	node.Anchors.Add(TransformSource("Source0"))
	node.Anchors.Add(TransformSource("Source1"))
	node.Anchors.Add(TransformResult("Result"))
	node.Anchors.Add(FilterSource("Filter"))
	node.Anchors.Add(SignalSource("Weight"))
	
node = RegisterNode("Add2", 10)
node.Help = 'Add 2 animations together'
node.Group = 'Blends'
node.Image = 'Add.svg'
node.Serialize = Serialize
node.Validate = Validate
node.Initialize = Initialize