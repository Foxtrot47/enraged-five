import clr;
clr.AddReferenceByName("Rage.Move.Core")

from Rage.Move.Core.Dg import *

def Serialize(condition, stream):
	parameter = condition.Get("Control Parameter")
	upper = condition.Get("Upper Value")
	lower = condition.Get("Lower Value")
	
	stream.AppendData(parameter)
	stream.AppendData(upper)
	stream.AppendData(lower)
	
def Validate():
	return True

def Initialize(condition):
	condition.Attributes.Add(ControlSignalAttribute("Control Signal"))
	condition.Attributes.Add(FloatAttribute("Upper Value", 0.5))
	condition.Attributes.Add(FloatAttribute("Lower Value", 0.5))
	
condition = RegisterCondition("Float Not In Range", 6)
condition.Serialize = Serialize
condition.Validate = Validate
condition.Initialize = Initialize	
	