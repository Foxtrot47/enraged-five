﻿using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.ConstrainedExecution;
using System;
using System.Security;

namespace Rockstar.MoVE.Interop.WindowsCodecs
{
    internal class MILDLLNames
    {
        internal const string WindowsCodecs = "WindowsCodecs.dll";
    }

    public enum WICDecodeOptions
    {
        WICDecodeMetadataCacheOnDemand = 0x00000000,
    }

    public enum DitherType
    {
        DitherTypeNone = 0,
    }

    public enum WICPaletteType
    {
        WICPaletteTypeCustom = 0,
    };

    public enum WICBitmapTransformOptions
    {
        WICBitmapTransformFlipVertical = 0x10
    }

    public struct Int32Rect
    {
        public Int32 x;
        public Int32 y;
        public Int32 width;
        public Int32 height;
    }

    public static class WICCodec
    {
        // Please make sure that this version is in-sync with that found in:
        public const int WINCODEC_SDK_VERSION = 0x0236;

        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "WICCreateImagingFactory_Proxy")]
        public static extern int CreateImagingFactory(UInt32 SDKVersion, out IntPtr ppICodecFactory);
    }

    public static class WICImagingFactory
    {
        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICImagingFactory_CreateDecoderFromStream_Proxy")]
        public static extern int /*HRESULT*/ CreateDecoderFromStream(IntPtr pICodecFactory, IStream pIStream, ref Guid guidVendor, WICDecodeOptions metadataFlags, out IntPtr /* IWICBitmapDecoder */ ppIDecode);

        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICImagingFactory_CreateFormatConverter_Proxy")]
        public static extern int /* HRESULT */ CreateFormatConverter(IntPtr pICodecFactory, out IntPtr /* IMILFormatConverter */ ppFormatConverter);

        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICImagingFactory_CreateBitmapFlipRotator_Proxy")]
        public static extern int /* HRESULT */ CreateBitmapFlipRotator(IntPtr pICodecFactory, out IntPtr /* IWICBitmapFlipRotator */ ppBitmapFlipRotator);
    }

    public class WICBitmapSource
    {
        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICBitmapSource_GetSize_Proxy")]
        public static extern int /* HRESULT */ GetSize(IntPtr /* IWICBitmapSource */ THIS_PTR, out UInt32 puiWidth, out UInt32 puiHeight);

        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICBitmapSource_CopyPixels_Proxy")]
        public static extern int /* HRESULT */ CopyPixels(IntPtr /* IWICBitmapSource */ THIS_PTR, /* ref System.Windows.Int32Rect prc,*/ ref Int32Rect prc, UInt32 cbStride, UInt32 cbPixelsSize, IntPtr /* BYTE* */ pvPixels);
    }

    public static class WICFormatConverter
    {
        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICFormatConverter_Initialize_Proxy")]
        public static extern int /* HRESULT */ Initialize(IntPtr THIS_PTR, IntPtr /* IMILBitmapSource */ pISource, ref Guid dstPixelFormatGuid, DitherType dither, IntPtr /* IWICPalette */ pIPalette, double /*float*/ alphaThresholdPercent, WICPaletteType paletteTranslate);
    }

    public static class WICBitmapFlipRotator
    {
        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICBitmapFlipRotator_Initialize_Proxy")]
        public static extern int /* HRESULT */ Initialize(IntPtr THIS_PTR, IntPtr /* IMILBitmapSource */ pISource, WICBitmapTransformOptions options);
    }

    public class WICBitmapDecoder
    {
        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICBitmapDecoder_GetFrameCount_Proxy")]
        public static extern int /* HRESULT */ GetFrameCount(IntPtr THIS_PTR, out uint pFrameCount);

        [DllImport(MILDLLNames.WindowsCodecs, EntryPoint = "IWICBitmapDecoder_GetFrame_Proxy")]
        public static extern int /* HRESULT */ GetFrame(IntPtr /* IWICBitmapDecoder */ THIS_PTR, UInt32 index, out IntPtr /* IWICBitmapFrameDecode */ ppIFrameDecode);
    }

    public static class Helper
    {
        [SecurityCritical]
        public static void ReleaseInterface(ref IntPtr ptr)
        {
            if (ptr != IntPtr.Zero)
            {
                Marshal.Release(ptr);
                ptr = IntPtr.Zero;
            }
        }
    }

    public static class WICPixelFormatGUIDs
    {
        public static readonly Guid WICPixelFormat32bppBGRA = new Guid(0x6fddc324, 0x4e03, 0x4bfe, 0xb1, 0x85, 0x3d, 0x77, 0x76, 0x8d, 0xc9, 0x0f);
    }
}