﻿using System;
using System.Collections.Generic;

using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    public class ClipSignalWidget : SignalWidget
    {
        #region Constructors
        public ClipSignalWidget(Signal signal) : base(signal) { }
        #endregion // Constructors

        #region Properties

        #endregion // Properties

        #region Public Methods
        public override void Init(WidgetClient client)
        {
            if (!client.WidgetExists(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Signal.Name))
            {
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_NAME, Signal.Name);
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_TYPE, TypeName);
                client.PressButton(RagWidgetUtils.RAG_WIDGET_MOVE_CREATE_WIDGETS);
            }
        }
         
        public override void OnSignalPropertyChanged(string propertyName)
        {
            string rootPath = string.Format("Animation/Create MoVE network/{0}/", Signal.Name);

            ConnectionManager.RunActionAgainstClient(
                client =>
                {
                    if (propertyName == "SelectedSource")
                    {
                        string newSource = (this.Signal as ClipSignal).SelectedSource;
                        client.WriteStringWidget(rootPath + "Source", newSource);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "SelectedAbsoluateClipSet")
                    {
                        string newValue = (this.Signal as ClipSignal).SelectedAbsoluateClipSet;
                        client.WriteStringWidget(rootPath + "Absolute Clipset:", newValue);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "SelectedAbsoluateClip")
                    {
                        string newValue = (this.Signal as ClipSignal).SelectedAbsoluateClip;
                        client.WriteStringWidget(rootPath + "Clip Name:", newValue);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "SelectedClipDictionary")
                    {
                        string newValue = (this.Signal as ClipSignal).SelectedClipDictionary;
                        client.WriteStringWidget(rootPath + "Clip Dictionary:", newValue);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "SelectedDictionaryClip")
                    {
                        string newValue = (this.Signal as ClipSignal).SelectedDictionaryClip;
                        client.WriteStringWidget(rootPath + "Clip Name:", newValue);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "VariableClipSet")
                    {
                        string newValue = (this.Signal as ClipSignal).VariableClipSet;
                        client.WriteStringWidget(rootPath + "Variable Clipset:", newValue);
                        client.SendSyncCommand();

                        var test = client.ReadStringWidget(rootPath + "Variable Clipset:");
                    }
                    else if (propertyName == "VariableClipName")
                    {
                        string newValue = (this.Signal as ClipSignal).VariableClipName;
                        client.WriteStringWidget(rootPath + "Clip Name:", newValue);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "ClipFilePath")
                    {
                        string newValue = (this.Signal as ClipSignal).ClipFilePath;
                        client.WriteStringWidget(rootPath + "File Path:", newValue);
                        client.SendSyncCommand();
                    }
                });
        }
        #endregion // Public Methods
    }
}
