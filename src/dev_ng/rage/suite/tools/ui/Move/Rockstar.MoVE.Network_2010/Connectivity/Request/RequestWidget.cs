﻿using System;
using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    public class RequestWidget
    {

        #region Constructors
        public RequestWidget(Request request)
        {
            Request = request;
            this.Request.RequestFired += new EventHandler(this.Request_Fired);
        }
        #endregion // Constructors

        #region Constants
        const string TYPE_NAME = "Request";
        #endregion

        #region Properties
        public Request Request { get; set; }
        #endregion

        #region Public Methods
        public void Init(WidgetClient client)
        {
            if (!client.WidgetExists(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Request.Name))
            {
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_NAME, Request.Name);
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_TYPE, TYPE_NAME);
                client.PressButton(RagWidgetUtils.RAG_WIDGET_MOVE_CREATE_WIDGETS);
            }
        }
        
        public void Update()
        {
            ConnectionManager.RunActionAgainstClient(
                client =>
                {
                    client.WriteBoolWidget(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Request.Name, true);
                    client.SendSyncCommand();
                });
        }

        public void Shutdown()
        {
            this.Request.RequestFired -= new EventHandler(this.Request_Fired);
        }
        #endregion // Public Methods

        #region Events
        public void Request_Fired(object sender, EventArgs e)
        {
            this.Update();
        }
        #endregion // Events
    }
}
