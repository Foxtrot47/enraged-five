﻿using System;
using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    public class LogicEventWidget
    {
        #region Constructors
        public LogicEventWidget(LogicEvent logicEvent)
        {
            LogicEvent = logicEvent;
            this.LogicEvent.LogicEventFired += new EventHandler(this.Event_PropertyChanged);
        }
        #endregion // Constructors

        #region Constants
        const string TYPE_NAME = "Event";
        #endregion

        #region Properties
        public LogicEvent LogicEvent { get; set; }
        #endregion

        #region Public Methods
        public void Init(WidgetClient client)
        {
            if (!client.WidgetExists(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + LogicEvent.Name))
            {
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_NAME, LogicEvent.Name);
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_TYPE, TYPE_NAME);
                client.PressButton(RagWidgetUtils.RAG_WIDGET_MOVE_CREATE_WIDGETS);
            }
        }
        
        public void Update()
        {
            ConnectionManager.RunActionAgainstClient(
                client =>
                {
                    client.WriteBoolWidget(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + LogicEvent.Name, true);
                    client.SendSyncCommand();
                });
        }

        public void Shutdown()
        {
            this.LogicEvent.LogicEventFired -= new EventHandler(this.Event_PropertyChanged);
        }
        #endregion // Public Methods

        #region Events
        public void Event_PropertyChanged(object sender, EventArgs e)
        {
            this.Update();
        }
        #endregion // Events
    }
}
