﻿using System.ComponentModel;
using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    public class FlagWidget
    {
        #region Constructors
        public FlagWidget(Flag flag)
        {
            Flag = flag;
            this.Flag.PropertyChanged += new PropertyChangedEventHandler(this.Flag_PropertyChanged);
        }
        #endregion // Constructors
        
        #region Constants
        const string TYPE_NAME = "Flag";
        #endregion

        #region Properties
        public Flag Flag { get; set; }
        #endregion

        #region Public Methods
        public void Init(WidgetClient client)
        {
            if (!client.WidgetExists(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Flag.Name))
            {
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_NAME, Flag.Name);
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_TYPE, TYPE_NAME);
                client.PressButton(RagWidgetUtils.RAG_WIDGET_MOVE_CREATE_WIDGETS);
            }
        }
        
        public void Update()
        {
            ConnectionManager.RunActionAgainstClient(
                client =>
                {
                    client.WriteBoolWidget(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Flag.Name, Flag.Value);
                    client.SendSyncCommand();
                });
        }

        public void Shutdown()
        {
            this.Flag.PropertyChanged -= new PropertyChangedEventHandler(this.Flag_PropertyChanged);
        }
        #endregion // Public Methods

        #region Events
        public void Flag_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                this.Update();
            }
        }
        #endregion // Events

    }
}
