﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rockstar.MoVE.Net.Utils
{
    public static class RagWidgetUtils
    {
        #region Constants
        public const string RAG_WIDGET_ANIM_ROOT = "Animation/";
        public const string RAG_WIDGET_ANIM_TOGGLE_BANK = RAG_WIDGET_ANIM_ROOT + "Toggle Animation bank";

        public const string RAG_WIDGET_MOVE_ROOT = RAG_WIDGET_ANIM_ROOT + "Create MoVE network/";
        public const string RAG_WIDGET_MOVE_REMOVE_CURRENT_NETWORK = RAG_WIDGET_MOVE_ROOT + "Remove current preview network";
        public const string RAG_WIDGET_MOVE_DIRECTORY = RAG_WIDGET_MOVE_ROOT + "Directory:";
        public const string RAG_WIDGET_MOVE_RESCAN = RAG_WIDGET_MOVE_ROOT + "rescan";
        public const string RAG_WIDGET_MOVE_SELECT_FILE = RAG_WIDGET_MOVE_ROOT + "Select network file";
        public const string RAG_WIDGET_MOVE_ANIM_SET = RAG_WIDGET_MOVE_ROOT + "Anim set";
        public const string RAG_WIDGET_MOVE_LOAD_NETWORK = RAG_WIDGET_MOVE_ROOT + "Load selected network";

        public const string RAG_WIDGET_MOVE_HOOKUP_PARAM = RAG_WIDGET_MOVE_ROOT + "Hookup parameter/";
        
        public const string RAG_WIDGET_MOVE_PARAM_NAME = RAG_WIDGET_MOVE_HOOKUP_PARAM + "Parameter name";
        public const string RAG_WIDGET_MOVE_PARAM_TYPE = RAG_WIDGET_MOVE_HOOKUP_PARAM + "Type";
        public const string RAG_WIDGET_MOVE_CREATE_WIDGETS = RAG_WIDGET_MOVE_HOOKUP_PARAM + "Create widgets";
        public const string RAG_WIDGET_MOVE_CLEAR_ALL_WIDGETS = RAG_WIDGET_MOVE_HOOKUP_PARAM + "Clear all widgets";

        public const string RAG_WIDGET_MOVE_WIDGET_OPTIONS = RAG_WIDGET_MOVE_HOOKUP_PARAM + "Widget options/";
        public const string RAG_WIDGET_MOVE_FLOAT_MIN = RAG_WIDGET_MOVE_WIDGET_OPTIONS + "Min value";
        public const string RAG_WIDGET_MOVE_FLOAT_MAX = RAG_WIDGET_MOVE_WIDGET_OPTIONS + "Max value";
        #endregion

    }
}
