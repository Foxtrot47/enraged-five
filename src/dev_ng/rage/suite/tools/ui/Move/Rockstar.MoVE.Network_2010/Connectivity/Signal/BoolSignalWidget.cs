﻿using System;
using System.Collections.Generic;

using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    public class BoolSignalWidget : SignalWidget
    {
        #region Constructors
        public BoolSignalWidget(Signal signal) : base(signal) { }
        #endregion // Constructors

        #region Properties
        
        #endregion // Properties

        #region Public Methods
        public override void Init(WidgetClient client)
        {
            if (!client.WidgetExists(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Signal.Name))
            {
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_NAME, Signal.Name);
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_TYPE, TypeName);
                client.PressButton(RagWidgetUtils.RAG_WIDGET_MOVE_CREATE_WIDGETS);
            }
        }

        public override void OnSignalPropertyChanged(string propertyName)
        {
            if (propertyName != "Value")
                return;

            BooleanSignal boolSignal = this.Signal as BooleanSignal;

            ConnectionManager.RunActionAgainstClient(
                client =>
                {
                    client.WriteBoolWidget(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + boolSignal.Name, boolSignal.Value);
                    client.SendSyncCommand();
                });
        }
        #endregion // Public Methods
    }
}
