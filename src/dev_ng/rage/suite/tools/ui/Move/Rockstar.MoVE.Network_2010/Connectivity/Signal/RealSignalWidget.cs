﻿using System;
using System.Collections.Generic;

using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    public class RealSignalWidget : SignalWidget
    {
        #region Constructors
        public RealSignalWidget(Signal signal) : base(signal) { }
        #endregion // Constructors

        #region Properties
        
        #endregion // Properties

        #region Public Methods
        public override void Init(WidgetClient client)
        {
            if (!client.WidgetExists(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Signal.Name))
            {
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_NAME, Signal.Name);
                client.WriteFloatWidget(RagWidgetUtils.RAG_WIDGET_MOVE_FLOAT_MIN, 0.0f);
                client.WriteFloatWidget(RagWidgetUtils.RAG_WIDGET_MOVE_FLOAT_MAX, 1.0f);
                client.PressButton(RagWidgetUtils.RAG_WIDGET_MOVE_CREATE_WIDGETS);
            }
        }

        public static bool InitialiseWidgetType(WidgetClient client)
        {
            string typename = sm_SignalTypeMap[Parameter.Real];
            client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_TYPE, typename);
            return WaitForExistence(RagWidgetUtils.RAG_WIDGET_MOVE_FLOAT_MIN, 5000, client);
        }

        public override void OnSignalPropertyChanged(string propertyName)
        {
            if (propertyName != "Value")
                return;

            RealSignal realSignal = Signal as RealSignal;


            ConnectionManager.RunActionAgainstClient(
                client =>
                {
                    client.WriteFloatWidget(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + realSignal.Name, realSignal.Value);
                    client.SendSyncCommand();
                });
        }
        #endregion // Public Methods
    }
}
