﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    public abstract class SignalWidget
    {

        #region Constructors
        public SignalWidget(Signal signal)
        {
            Signal = signal;
            this.Signal.PropertyChanged += new PropertyChangedEventHandler( this.Signal_PropertyChanged );
        }
        #endregion

        #region Fields
        protected static Dictionary<Parameter, string> sm_SignalTypeMap = new Dictionary<Parameter, string>()
        {
            { Parameter.None, "None" },
            { Parameter.Animation, "Animation" },
            { Parameter.Boolean, "Bool" },
            { Parameter.Clip, "Clip" },
            { Parameter.Expressions, "Expressions" },
            { Parameter.Filter, "Filter" },
            { Parameter.Frame, "Frame" },
            { Parameter.ParameterizedMotion, "ParameterizedMotion" },
            { Parameter.Real, "Real" },
            { Parameter.Reference, "Reference" },
            { Parameter.Node, "Node" },
        };
        #endregion

        #region Properties
        public Signal Signal { get; set; }
        public string TypeName
        {
            get
            {
                return sm_SignalTypeMap[Signal.GetParameter()];
            }
        }
        #endregion

        #region Public Methods 
        public abstract void Init(WidgetClient client);
        public abstract void OnSignalPropertyChanged(string propertyName);
        public void Shutdown()
        {
            this.Signal.PropertyChanged -= new PropertyChangedEventHandler(this.Signal_PropertyChanged);
        }

        protected static bool WaitForExistence(string widget, int timeout, WidgetClient client)
        {
            Stopwatch sw = Stopwatch.StartNew();
            while (sw.ElapsedMilliseconds < timeout)
            {
                if (client.WidgetExists(widget))
                {
                    return true;
                }

                Thread.Sleep(100);
            }

            return false;
        }
        #endregion // Public Methods

        #region Events
        public void Signal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnSignalPropertyChanged(e.PropertyName);
        }
        #endregion // Events
    }
}
