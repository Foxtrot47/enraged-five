﻿using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    public class FilterSignalWidget : SignalWidget
    {
        #region Constructors
        public FilterSignalWidget(Signal signal) : base(signal) { }
        #endregion // Constructors

        #region Properties

        #endregion // Properties

        #region Public Methods
        public override void Init(WidgetClient client)
        {
            if (!client.WidgetExists(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Signal.Name))
            {
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_NAME, Signal.Name);
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_TYPE, TypeName);
                client.PressButton(RagWidgetUtils.RAG_WIDGET_MOVE_CREATE_WIDGETS);
            }
        }

        public override void OnSignalPropertyChanged(string propertyName)
        {
            string rootPath = string.Format("Animation/Create MoVE network/{0}/", Signal.Name);

            if (propertyName == "SelectedFilter")
            {
                string newFilter = (this.Signal as FilterSignal).SelectedFilter;

                ConnectionManager.RunActionAgainstClient(
                    client =>
                    {
                        client.WriteStringWidget(rootPath + "Filter Name:", newFilter);
                        client.SendSyncCommand();
                    });
            }
        }
        #endregion // Public Methods
    }
}
