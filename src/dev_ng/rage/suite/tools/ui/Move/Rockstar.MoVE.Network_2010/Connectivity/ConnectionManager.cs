﻿using System;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Collections.Specialized;
using System.ServiceModel;


using RSG.Base.Network;
using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using Move.Utils;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Clients;
using RSG.Base.Logging;

namespace Rockstar.MoVE.Net.Connectivity
{
    /// <summary>
    /// 
    /// </summary>
    public static class ConnectionManager
    {
        #region Fields
        private static GameConnection _gameConnection;

        static private List<LogicEventWidget> m_RegEventWidgets = new List<LogicEventWidget>();
        static private List<FlagWidget> m_RegFlagWidgets = new List<FlagWidget>();
        static private List<RequestWidget> m_RegRequestWidgets = new List<RequestWidget>();
        static private List<SignalWidget> m_RegSignalWidgets = new List<SignalWidget>();
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Gets a value that indicating whether this wrapper is currently connected to a
        /// running instance of Rag.
        /// </summary>
        internal static bool IsConnected
        {
            get { return _gameConnection != null; }
        }
        #endregion

        #region Public Methods
        private static bool Init()
        {
            try
            {
                IEnumerable<GameConnection> connections = RagClientFactory.GetGameConnections();
                _gameConnection = connections.FirstOrDefault(connection => connection.DefaultConnection);
            }
            catch (Exception)
            {
                _gameConnection = null;
            }

            return IsConnected;
        }

        private static void InitAll(string filename, WidgetClient client)
        {
            foreach (SignalWidget signalWidget in m_RegSignalWidgets)
            {
                signalWidget.Init(client);
            }
            foreach (LogicEventWidget logicEvent in m_RegEventWidgets)
            {
                logicEvent.Init(client);
            }
            foreach (FlagWidget flag in m_RegFlagWidgets)
            {
                flag.Init(client);
            }
            foreach (RequestWidget request in m_RegRequestWidgets)
            {
                request.Init(client);
            }
        }

        private static void RegisterEvent( LogicEvent logicEvent )
        {
            m_RegEventWidgets.Add( new LogicEventWidget( logicEvent ) );
        }

        private static void RegisterFlag(Flag flag)
        {
            m_RegFlagWidgets.Add( new FlagWidget( flag ) );
        }

        private static void RegisterRequest(Request request)
        {
            m_RegRequestWidgets.Add( new RequestWidget( request ) );
        }

        private static void RegisterSignals(NoResetObservableCollection<Signal> signals, WidgetClient client)
        {
            List<Signal> realSignals = new List<Signal>();
            List<Signal> boolSignals = new List<Signal>();
            List<Signal> clipSignals = new List<Signal>();
            List<Signal> expressionSignals = new List<Signal>();
            foreach (Signal signal in signals)
            {
                switch (signal.GetParameter())
                {
                    case Parameter.Real:
                        realSignals.Add(signal);
                        break;
                    case Parameter.Boolean:
                        boolSignals.Add(signal);
                        break;
                    case Parameter.Clip:
                        clipSignals.Add(signal);
                        break;
                    case Parameter.Expressions:
                        expressionSignals.Add(signal);
                        break;
                    case Parameter.Filter:
                    case Parameter.Animation:
                    case Parameter.Frame:
                    case Parameter.Node:
                    case Parameter.ParameterizedMotion:
                    case Parameter.Reference:
                    case Parameter.None:
                        break;
                }
            }

            if (RealSignalWidget.InitialiseWidgetType(client))
            {
                foreach (Signal signal in realSignals)
                {
                    m_RegSignalWidgets.Add(new RealSignalWidget(signal));
                }
            }

            foreach (Signal signal in boolSignals)
            {
                m_RegSignalWidgets.Add(new BoolSignalWidget(signal));
            }

            foreach (Signal signal in clipSignals)
            {
                m_RegSignalWidgets.Add(new ClipSignalWidget(signal));
            }

            foreach (Signal signal in expressionSignals)
            {
                m_RegSignalWidgets.Add(new ExpressionSignalWidget(signal));
            }
        }

        private static void RegisterAll(Database database, WidgetClient client)
        {
            RegisterSignals(database.Signals, client);
            foreach (LogicEvent logicEvent in database.Events)
            {
                RegisterEvent(logicEvent);
            }
            foreach (Flag flag in database.Flags)
            {
                RegisterFlag(flag);
            }
            foreach (Request request in database.Requests)
            {
                RegisterRequest(request);
            }
        }

        private static void DeregisterAll()
        {
            if (!IsConnected)
            {
                return;
            }

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                client.WriteBoolWidget( "MoVE/Create MoVE network/Hookup parameter/Clear all widgets", true );
            }
            catch (TimeoutException e)
            {
                client.Abort();
                _gameConnection = null;
            }
            catch (CommunicationException e)
            {
                client.Abort();
                _gameConnection = null;
            }
            finally
            {
                // TODO: Fix this logic as Close() might throw an exception.
                client.Close();
            }

            foreach (LogicEventWidget eventWidget in m_RegEventWidgets)
            {
                eventWidget.Shutdown();
            }
            m_RegEventWidgets.Clear();

            foreach (FlagWidget flagWidget in m_RegFlagWidgets)
            {
                flagWidget.Shutdown();
            }
            m_RegFlagWidgets.Clear();

            foreach (RequestWidget requestWidget in m_RegRequestWidgets)
            {
                requestWidget.Shutdown();
            }
            m_RegRequestWidgets.Clear();

            foreach (SignalWidget signalWidget in m_RegSignalWidgets)
            {
                signalWidget.Shutdown();
            }
            m_RegSignalWidgets.Clear();
        }

        public static void RunNetwork(Database database, string animSet, string filename )
        {
            DeregisterAll();
            Init();

            if (animSet != null && IsConnected)
            {
                WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
                try
                {
                    RegisterAll(database, client);

                    if (!client.WidgetExists("MoVE/Create MoVE network/Directory:"))
                    {
                        client.PressButton("Animation/Toggle Animation bank");
                        client.SendSyncCommand();
                    }

                    // Lets make sure the file is located in the preview folder assets:/anim/move/networks/preview.

                	client.PressButton("Animation/Create MoVE network/rescan");

                    //string networkFile = Path.Combine(previewFolder, Path.GetFileName(filename));
                    //string networkXmlFile = Path.Combine(previewFolder, Path.GetFileName(filename) + ".xml");
                    //if (Path.GetDirectoryName(filename).ToLower() != previewFolder.ToLower())
                    //{
                    //    File.Copy(filename, networkFile, true);
                    //}

                    client.PressButton("MoVE/Create MoVE network/Remove current preview network");
                    client.SendSyncCommand();

                    client.PressButton("MoVE/Create MoVE network/rescan");
                    client.SendSyncCommand();

                    client.WriteStringWidget("MoVE/Create MoVE network/Select network file", Path.GetFileName(filename));
                    client.SendSyncCommand();

                    client.WriteStringWidget("MoVE/Create MoVE network/ClipSet", animSet);
                    client.SendSyncCommand();

                    client.PressButton("MoVE/Create MoVE network/Load selected network");
                    client.SendSyncCommand();

                    InitAll(Path.GetFileNameWithoutExtension(filename), client);
                }
                catch (TimeoutException e)
                {
                    LogFactory.ApplicationLog.ToolException(e, "RunNetwork operation timed out.");
                    _gameConnection = null;
                }
                catch (CommunicationException e)
                {
                    LogFactory.ApplicationLog.ToolException(e, "RunNetwork communication problem.");
                    _gameConnection = null;
                }
                finally
                {
                    if (client.State == CommunicationState.Faulted)
                    {
                        client.Abort();
                    }
                    else
                    {
                        client.Close();
                    }
                }
            }
        }

        public static void RunActionAgainstClient(Action<WidgetClient> action)
        {
            if (!IsConnected)
            {
                return;
            }

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                action.Invoke(client);
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "RunActionAgainstClient operation timed out.");
                _gameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "RunActionAgainstClient communication problem.");
                _gameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
        }
        #endregion // Public Methods
    }
}
