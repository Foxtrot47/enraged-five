﻿using System;
using System.Collections.Generic;

using New.Move.Core;
using Rockstar.MoVE.Net.Utils;
using RSG.Rag.Clients;

namespace Rockstar.MoVE.Net.Connectivity
{
    /// <summary>
    /// </summary>
    public class ExpressionSignalWidget : SignalWidget
    {
        #region Constructors
        public ExpressionSignalWidget(Signal signal) : base(signal) { }
        #endregion // Constructors

        #region Properties

        #endregion // Properties

        #region Public Methods
        public override void Init(WidgetClient client)
        {
            if (!client.WidgetExists(RagWidgetUtils.RAG_WIDGET_MOVE_ROOT + Signal.Name))
            {
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_NAME, Signal.Name);
                client.WriteStringWidget(RagWidgetUtils.RAG_WIDGET_MOVE_PARAM_TYPE, TypeName);
                client.PressButton(RagWidgetUtils.RAG_WIDGET_MOVE_CREATE_WIDGETS);
            }
        }

        public override void OnSignalPropertyChanged(string propertyName)
        {
            string rootPath = string.Format("Animation/Create MoVE network/{0}/", Signal.Name);

            ConnectionManager.RunActionAgainstClient(
                client =>
                {
                    if (propertyName == "SelectedSource")
                    {
                        string newSource = (this.Signal as ExpressionSignal).SelectedSource;
                        client.WriteStringWidget(rootPath + "Source", (this.Signal as ExpressionSignal).SelectedSource);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "FilePath")
                    {
                        string newValue = (this.Signal as ExpressionSignal).FilePath;
                        client.WriteStringWidget(rootPath + "File Path:", newValue);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "SelectedExpressionDictionary")
                    {
                        string newValue = (this.Signal as ExpressionSignal).SelectedExpressionDictionary;
                        client.WriteStringWidget(rootPath + "Expression Dictionary:", newValue);
                        client.SendSyncCommand();
                    }
                    else if (propertyName == "SelectedExpression")
                    {
                        string newValue = (this.Signal as ExpressionSignal).SelectedExpression;
                        client.WriteStringWidget(rootPath + "Expressions Name:", newValue);
                        client.SendSyncCommand();
                    }
                });
        }
        #endregion // Public Methods
    }
}
