﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class AnimationReference : ReferenceSignal, ISerializable
    {
        public AnimationReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public AnimationReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override Parameter GetParameter()
        {
            return Parameter.Animation;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}