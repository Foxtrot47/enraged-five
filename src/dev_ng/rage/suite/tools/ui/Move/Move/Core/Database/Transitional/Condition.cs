﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Core.Restorable;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class ConditionCollection : NoResetObservableCollection<Condition>
    {
    }

    [Serializable]
    public class Condition : RestorableObject, INotifyPropertyChanged
    {
        public Condition(IConditionDesc descriptor, Transition parent)
        {
            RestorablePropertyDescriptor desc =
                RestorablePropertyDescriptor.FromProperty(Condition.DescriptorProperty, typeof(Condition));
            desc.AddValueChanged(this, Descriptor_ValueChanged);

            desc = RestorablePropertyDescriptor.FromProperty(Condition.ContentProperty, typeof(Condition));
            desc.AddValueChanged(this, Content_ValueChanged);

            Descriptor = descriptor;
            Parent = parent;
            FlagWhenPassed = false;
        }

        public Condition(XmlReader reader, Transition parent, Database database)
        {
            RestorablePropertyDescriptor desc =
                RestorablePropertyDescriptor.FromProperty(Condition.DescriptorProperty, typeof(Condition));
            desc.AddValueChanged(this, Descriptor_ValueChanged);

            desc = RestorablePropertyDescriptor.FromProperty(Condition.ContentProperty, typeof(Condition));
            desc.AddValueChanged(this, Content_ValueChanged);

            Parent = parent;
            FlagWhenPassed = false;
            ICondition condition = database.XmlFactory.CreateTransitionCondition(reader, database);
            foreach (var description in database.DataModel.ConditionDescriptors)
            {
                if (description.GetType() == condition.Descriptor)
                {
                    this.Descriptor = description;
                    break;
                }
            }

            this.SetValue(ContentProperty, condition);
        }

        public Condition(Condition other)
        {
            RestorablePropertyDescriptor desc =
                RestorablePropertyDescriptor.FromProperty(Condition.DescriptorProperty, typeof(Condition));
            desc.AddValueChanged(this, Descriptor_ValueChanged);

            desc = RestorablePropertyDescriptor.FromProperty(Condition.ContentProperty, typeof(Condition));
            desc.AddValueChanged(this, Content_ValueChanged);

            Descriptor = other.Descriptor;
            Parent = other.Parent;
            FlagWhenPassed = other.FlagWhenPassed;

            ICondition content = other.Content != null ? other.Content.Clone(this) : null;
            SetValue(ContentProperty, content);
        }

        internal static readonly RestorableProperty DescriptorProperty =
            RestorableProperty.Register("Descriptor", typeof(IConditionDesc), typeof(Condition));

        public IConditionDesc Descriptor
        {
            get { return (IConditionDesc)GetValue(DescriptorProperty); }
            set { SetValue(DescriptorProperty, value); }
        }

        internal void Descriptor_ValueChanged(object sender, EventArgs e)
        {
            Content = Descriptor.Create();

            PropertyInfo[] properties = Content.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                if (pi != null && pi.PropertyType.GetInterface("IConditionProperty") != null)
                {
                    IConditionProperty property = (IConditionProperty)pi.GetValue(Content, null);
                    property.Parent = this;
                }
            }

            OnPropertyChanged("Descriptor");
        }

        internal static readonly RestorableProperty ContentProperty =
            RestorableProperty.Register("Content", typeof(ICondition), typeof(Condition));

        public ICondition Content 
        { 
            get { return (ICondition)GetValue(ContentProperty); }
            internal set 
            {
                if (Content != null && value != null)
                {
                    TransferEquivalentConditionProperties(Content, value);
                }

                SetValue(ContentProperty, value);
            }
        }

        private void TransferEquivalentConditionProperties(ICondition source, ICondition target)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (target == null)
                throw new ArgumentNullException("target");

            bool sourceHasLowerValue = false;
            double sourceLowerValue = 0;
            bool sourceHasSignal = false;
            Signal sourceSignal = null;
            bool sourceHasTriggerValue = false;
            double sourceTriggerValue = 0;
            bool sourceHasUpperValue = false;
            double sourceUpperValue = 0;

            PropertyInfo[] sourcePropertyInfoArray = source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var propertyInfo in sourcePropertyInfoArray)
            {
                if (propertyInfo.Name == "LowerValue")
                {
                    sourceHasLowerValue = true;
                    RealConditionProperty lowerValueProperty = (RealConditionProperty)propertyInfo.GetValue(source, null);
                    sourceLowerValue = lowerValueProperty.Value;
                }
                else if (propertyInfo.Name == "Signal")
                {
                    sourceHasSignal = true;
                    SignalConditionProperty signalProperty = (SignalConditionProperty)propertyInfo.GetValue(source, null);
                    sourceSignal = signalProperty.Value;
                }
                else if (propertyInfo.Name == "TriggerValue")
                {
                    sourceHasTriggerValue = true;
                    RealConditionProperty triggerValueProperty = (RealConditionProperty)propertyInfo.GetValue(source, null);
                    sourceTriggerValue = triggerValueProperty.Value;
                }
                else if (propertyInfo.Name == "UpperValue")
                {
                    sourceHasUpperValue = true;
                    RealConditionProperty upperValueProperty = (RealConditionProperty)propertyInfo.GetValue(source, null);
                    sourceUpperValue = upperValueProperty.Value;
                }
            }

            PropertyInfo[] targetPropertyInfoArray = target.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var propertyInfo in targetPropertyInfoArray)
            {
                if (sourceHasLowerValue && propertyInfo.Name == "LowerValue")
                {
                    RealConditionProperty lowerValueProperty = (RealConditionProperty)propertyInfo.GetValue(target, null);
                    lowerValueProperty.Value = sourceLowerValue;
                }
                else if (sourceHasSignal && propertyInfo.Name == "Signal")
                {
                    SignalConditionProperty signalProperty = (SignalConditionProperty)propertyInfo.GetValue(target, null);
                    if (sourceSignal != null)
                    {
                        sourceSignal.SetUsed(false);
                    }
                    signalProperty.Value = sourceSignal;
                }
                else if (sourceHasTriggerValue && propertyInfo.Name == "TriggerValue")
                {
                    RealConditionProperty triggerValueProperty = (RealConditionProperty)propertyInfo.GetValue(target, null);
                    triggerValueProperty.Value = sourceTriggerValue;
                }
                else if (sourceHasUpperValue && propertyInfo.Name == "UpperValue")
                {
                    RealConditionProperty upperValueProperty = (RealConditionProperty)propertyInfo.GetValue(target, null);
                    upperValueProperty.Value = sourceUpperValue;
                }
            }
        }

        internal void Content_ValueChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("Content");
        }

        public bool FlagWhenPassed { get; set; }

        private static readonly RestorableProperty ParentProperty =
            RestorableProperty.Register("Parent", typeof(Transition), typeof(Condition));

        public Transition Parent 
        {
            get { return (Transition)GetValue(ParentProperty); }
            internal set { SetValue(ParentProperty, value); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void Dispose()
        {
            Parent.Conditions.Remove(this);
        }

        public void ExportToXML(PargenXmlNode parentNode)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", Content.XMLExportTypeName);

            // Removing FlagWhenPassed from export (cannot remove from class as the binary would mess up).
            //itemNode.AppendValueElementNode("FlagWhenPassed", "value", FlagWhenPassed.ToString().ToLower());

            Content.ExportToXML(itemNode);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        /// <param name="database">
        /// The root database that this condition belongs to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            if (this.Content != null)
            {
                this.Content.Serialise(writer);
            }
        }
    }
}