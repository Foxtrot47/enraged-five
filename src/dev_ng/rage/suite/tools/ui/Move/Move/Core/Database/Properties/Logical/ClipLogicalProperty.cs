﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;
using Rockstar.MoVE.Framework.DataModel;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    public enum ClipSource
    {
        VariableClipSet = 0,
        ClipDictionary = 1,
        ClipSet = 2,
        LocalFile = 3,
    }

    [Serializable]
    public class ClipLogicalProperty : Property, ISerializable, IExternalFileLink
    {
        public ClipLogicalProperty()
        {
            Flags = PropertyInput.Clip | PropertyInput.Filename | PropertyInput.Invalid;
            Input = PropertyInput.Filename;
            Source = ClipSource.VariableClipSet;
            VariableClipSet = null;
            VariableClipSetClip = "";

            Id = Guid.NewGuid();

            ReadLookupData();

            Connections = new NoResetObservableCollection<Connection>();
        }

        public ClipLogicalProperty(ClipLogicalProperty other)
        {
            Flags = other.Flags;
            Input = other.Input;
            Source = other.Source;
            Filename = other.Filename;
            ClipDictionary = other.ClipDictionary;
            ClipDictionaryClip = other.ClipDictionaryClip;
            AbsoluteClipSet = other.AbsoluteClipSet;
            AbsoluteClipSetClip = other.AbsoluteClipSetClip;
            VariableClipSet = other.VariableClipSet;
            VariableClipSetClip = other.VariableClipSetClip;
            this._OutputEnabled = other._OutputEnabled;
            _SelectedEvent = other.SelectedEvent;

            Id = Guid.NewGuid();

            ReadLookupData();

            Connections = new NoResetObservableCollection<Connection>();
        }

        public ClipLogicalProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        private void ReadLookupData()
        {
            ClipArchiveReader archiveReader = new ClipArchiveReader();
            ClipDictionaries = archiveReader.ReadClipDictionaryNames();

            ClipSetReader clipSetReader = new ClipSetReader();
            AbsoluteClipSets = clipSetReader.ReadClipSetNames();
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public object Tag { get; set; }

        public PropertyInput Flags { get; private set; }
        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }

        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            if (_Input != PropertyInput.Clip)
            {
                from.RemoveConnectionsFromAnchor(this);
            }
        }

        public ILogic Parent { get; set; }

        #region Clip data
        ClipSource _Source;
        public ClipSource Source
        {
            get { return _Source; }
            set
            {
                _Source = value;
                OnPropertyChanged("Source");
            }
        }

        // Local file
        private string filename_;
        public string Filename 
        {
            get { return filename_; }
            set
            {
                filename_ = value;
                OnPropertyChanged("Filename");
            }
        }

        // Clip dictionary
        private string clipDictionary_;
        public string ClipDictionary
        {
            get { return clipDictionary_; }
            set 
            { 
                clipDictionary_ = value;
                OnPropertyChanged("ClipDictionary");

                if (clipDictionary_ != null)
                {
                    ClipArchiveReader archiveReader = new ClipArchiveReader();
                    ClipDictionaryClips = archiveReader.ReadClipNamesFromDictionary(clipDictionary_);
                    if (ClipDictionaryClips.Count() > 0)
                        ClipDictionaryClip = ClipDictionaryClips.First();
                }
            }
        }

        private string clipDictionaryClip_;
        public string ClipDictionaryClip
        {
            get { return clipDictionaryClip_; }
            set
            {
                clipDictionaryClip_ = value;
                OnPropertyChanged("ClipDictionaryClip");
            }
        }

        // Absolute clip set
        public string AbsoluteClipSet { get; set; }
        public string AbsoluteClipSetClip { get; set; }

        public delegate void VariableClipSetChangedDelegate(VariableClipSet oldClipSet);
        public VariableClipSetChangedDelegate VariableClipSetChanged;
        // Variable clip set
        private VariableClipSet variableClipSet_;
        public VariableClipSet VariableClipSet
        {
            get { return variableClipSet_; }
            set
            {
                VariableClipSet oldClipSet = variableClipSet_;
                variableClipSet_ = value;
                if (VariableClipSetChanged != null)
                {
                    VariableClipSetChanged(oldClipSet);
                }
            }
        }
        public string VariableClipSetClip { get; set; }

        #endregion//Clip data

        #region Clip lookup data

        public IEnumerable<string> ClipDictionaries { get; set; }

        private IEnumerable<string> clipDictionaryClips_;
        public IEnumerable<string> ClipDictionaryClips
        {
            get { return clipDictionaryClips_; }
            set
            {
                if (clipDictionaryClips_ != value)
                {
                    clipDictionaryClips_ = value;
                    OnPropertyChanged("ClipDictionaryClips");
                }
            }
        }

        public IEnumerable<string> AbsoluteClipSets { get; set; }

        #endregion// Clip lookup data

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Input = "Input";
            public static string OutputEnabled = "OutputEnabled";
            public static string Flags = "Flags";
            public static string Filename = "Filename";
            public static string SelectedEvent = "SelectedEvent";
            public static string Source = "Source";
            public static string ClipDictionary = "ClipDictionary";
            public static string ClipDictionaryClip = "ClipDictionaryClip";
            public static string AbsoluteClipSet = "AbsoluteClipSet";
            public static string AbsoluteClipSetClip = "AbsoluteClipSetClip";
            public static string VariableClipSet = "VariableClipSet";
            public static string VariableClipSetClip = "VariableClipSetClip";
        }

        public ClipLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));

            _Input = (PropertyInput)info.GetValue(SerializationTag.Input, typeof(PropertyInput));
            try
            {
                _OutputEnabled = info.GetBoolean(SerializationTag.OutputEnabled);
                _SelectedEvent = (Signal)info.GetValue(SerializationTag.SelectedEvent, typeof(Signal));
            }
            catch (SerializationException)
            {
            }

            Flags = (PropertyInput)info.GetValue(SerializationTag.Flags, typeof(PropertyInput));

            ReadLookupData();

            switch (_Input)
            {
                case PropertyInput.Filename:
                    {
                        Filename = (string)info.GetValue(SerializationTag.Filename, typeof(string));

                        try
                        {
                            Source = (ClipSource)info.GetValue(SerializationTag.Source, typeof(ClipSource));

                            ClipDictionary = info.GetString(SerializationTag.ClipDictionary);
                            if (ClipDictionary != null)
                            {
                                ClipArchiveReader archiveReader = new ClipArchiveReader();
                                ClipDictionaryClips = archiveReader.ReadClipNamesFromDictionary(ClipDictionary);
                                ClipDictionaryClip = info.GetString(SerializationTag.ClipDictionaryClip);
                            }

                            AbsoluteClipSet = info.GetString(SerializationTag.AbsoluteClipSet);
                            AbsoluteClipSetClip = info.GetString(SerializationTag.AbsoluteClipSetClip);

                            VariableClipSet = (VariableClipSet)info.GetValue(SerializationTag.VariableClipSet, typeof(VariableClipSet));
                            VariableClipSetClip = info.GetString(SerializationTag.VariableClipSetClip);
                        }
                        catch (SerializationException)
                        {
                            // This is a clip that was built when the game was still parsing filenames at runtime so
                            // we need to figure out what the intention was
                            if (System.IO.Path.IsPathRooted(Filename))
                            {
                                Source = ClipSource.LocalFile;
                            }
                            else if (Filename.StartsWith("%"))// hardcoded clip set
                            {
                                string[] tokens = Filename.Split(new char[] { '\\', '/' });
                                if (tokens.Length == 2)
                                {
                                    AbsoluteClipSet = tokens[0].Substring(1);// miss off the leading '@'
                                    AbsoluteClipSetClip = tokens[1];
                                    Source = ClipSource.ClipSet;
                                }
                                else
                                {
                                    Source = ClipSource.LocalFile;
                                }
                            }
                            else if ( !Filename.Contains('\\') && !Filename.Contains('/') )// variable clip set
                            {
                                VariableClipSet = null;
                                VariableClipSetClip = Filename;
                                Source = ClipSource.VariableClipSet;
                            }
                            else// clip dictionary
                            {
                                string[] tokens = Filename.Split(new char[] { '\\', '/' });
                                if (tokens.Length == 2)
                                {
                                    ClipDictionary = tokens[0];
                                    ClipDictionaryClip = tokens[1];
                                    Source = ClipSource.ClipDictionary;
                                }
                                else
                                {
                                    Source = ClipSource.LocalFile;
                                }
                            }
                        }
                        break;
                    }
            }    
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);

            info.AddValue(SerializationTag.Input, _Input);
            info.AddValue(SerializationTag.OutputEnabled, _OutputEnabled);
            info.AddValue(SerializationTag.Flags, Flags);
            info.AddValue(SerializationTag.SelectedEvent, _SelectedEvent);

            switch (_Input)
            {
                case PropertyInput.Filename:
                    {
                        info.AddValue(SerializationTag.Filename, Filename);
                        info.AddValue(SerializationTag.Source, Source);
                        info.AddValue(SerializationTag.ClipDictionary, ClipDictionary);
                        info.AddValue(SerializationTag.ClipDictionaryClip, ClipDictionaryClip);
                        info.AddValue(SerializationTag.AbsoluteClipSet, AbsoluteClipSet);
                        info.AddValue(SerializationTag.AbsoluteClipSetClip, AbsoluteClipSetClip);
                        info.AddValue(SerializationTag.VariableClipSet, VariableClipSet);
                        info.AddValue(SerializationTag.VariableClipSetClip, VariableClipSetClip);
                        break;
                    }
            }
        }

        public void ExportToXML(PargenXmlNode parentNode, Motiontree motiontreeParent, ICollection<string> hashedNames)
        {
            switch (Input)
            {
                case PropertyInput.Clip:
                    parentNode.AppendTextElementNode("Type", "kClipAttributeParameter");
                    foreach (Connection connection in motiontreeParent.Connections)
                    {
                        if (connection.Dest == this)
                        {
                            parentNode.AppendTextElementNode("Parameter", connection.Source.Parent.Name);
                            break;
                        }
                    }
                    break;
                case PropertyInput.Filename:
                    parentNode.AppendTextElementNode("Type", "kClipAttributeFilename");
                    if (Source == ClipSource.VariableClipSet)
                    {
                        parentNode.AppendTextElementNode("Context", "kClipContextVariableClipSet");

                        if (VariableClipSet != null)
                        {
                            parentNode.AppendTextElementNode("BankName", VariableClipSet.Name);
                            hashedNames.Add(VariableClipSet.Name);
                            if (!string.IsNullOrWhiteSpace(this.VariableClipSetClip))
                            {
                                hashedNames.Add(VariableClipSetClip);
                            }
                        }
                        else
                        {
                            parentNode.AppendTextElementNode("BankName", "Default");
                            hashedNames.Add("Default");
                            if (!string.IsNullOrWhiteSpace(this.VariableClipSetClip))
                            {
                                hashedNames.Add(VariableClipSetClip);
                            }
                        }

                        parentNode.AppendTextElementNode("ClipName", VariableClipSetClip);
                    }
                    else if (Source == ClipSource.ClipDictionary)
                    {
                        parentNode.AppendTextElementNode("Context", "kClipContextClipDictionary");

                        parentNode.AppendTextElementNode("BankName", ClipDictionary);
                        hashedNames.Add(ClipDictionary);

                        parentNode.AppendTextElementNode("ClipName", ClipDictionaryClip);
                        hashedNames.Add(ClipDictionaryClip);
                    }
                    else if (Source == ClipSource.ClipSet)
                    {
                        parentNode.AppendTextElementNode("Context", "kClipContextAbsoluteClipSet");

                        parentNode.AppendTextElementNode("BankName", AbsoluteClipSet);
                        hashedNames.Add(AbsoluteClipSet);

                        parentNode.AppendTextElementNode("ClipName", AbsoluteClipSetClip);
                        hashedNames.Add(AbsoluteClipSetClip);
                    }
                    else if (Source == ClipSource.LocalFile)
                    {
                        parentNode.AppendTextElementNode("Context", "kClipContextLocalFile");

                        parentNode.AppendTextElementNode("ClipName", Filename);
                        hashedNames.Add(Filename);
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                    break;
                case PropertyInput.Value:
                    parentNode.AppendTextElementNode("Type", "kClipAttributeIgnored");
                    break;
            }
        }
        
        public object Clone()
        {
            return new ClipLogicalProperty(this);
        }
               
        /// <summary>
        /// Initialises a new instance of the <see cref="ClipLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public ClipLogicalProperty(XmlReader reader, Database database, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Input = (PropertyInput)uint.Parse(reader.GetAttribute("Input"), CultureInfo.InvariantCulture);
            this.OutputEnabled = bool.Parse(reader.GetAttribute("OutputEnabled"));

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedEvent = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedEvent == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedEvent = database.GetSignal(signalName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}' inside the '{1}' node. This needs fixing before a export is allowed.", signalId, parent.Name);
                    }
                }
            }

            if (this.Input == PropertyInput.Filename)
            {
                this.Filename = reader.GetAttribute("Filename");
                this.Source = (ClipSource)uint.Parse(reader.GetAttribute("Source"), CultureInfo.InvariantCulture);
                this.ClipDictionary = reader.GetAttribute("ClipDictionary");
                if (ClipDictionary != null)
                {
                    ClipArchiveReader archiveReader = new ClipArchiveReader();
                    ClipDictionaryClips = archiveReader.ReadClipNamesFromDictionary(ClipDictionary);
                    this.ClipDictionaryClip = reader.GetAttribute("ClipDictionaryClip").ToLower();
                }

                this.AbsoluteClipSet = reader.GetAttribute("AbsoluteClipSet");
                this.AbsoluteClipSetClip = reader.GetAttribute("AbsoluteClipSetClip");
                this.VariableClipSetClip = reader.GetAttribute("VariableClipSetClip");

                string clipSetId = reader.GetAttribute("ClipSet");
                if (!string.IsNullOrEmpty(clipSetId))
                {
                    this.VariableClipSet = database.GetVariableClipSet(Guid.ParseExact(clipSetId, "D"));
                    if (this.VariableClipSet == null)
                    {
                        string clipSetName = reader.GetAttribute("ClipSetName");
                        this.VariableClipSet = database.GetVariableClipSet(clipSetName);
                        if (this.VariableClipSet == null)
                        {
                            LogFactory.ApplicationLog.Warning("Unable to find a reference to variable clip set with id '{0}'. This needs fixing before a export is allowed.", clipSetId);
                        }
                    }
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Input", ((uint)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("OutputEnabled", this.OutputEnabled.ToString(CultureInfo.InvariantCulture));

            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString("Signal", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedEvent.Name);
            }

            if (this.Input == PropertyInput.Filename)
            {
                writer.WriteAttributeString("Filename", this.Filename);
                writer.WriteAttributeString("Source", ((uint)this.Source).ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString("ClipDictionary", this.ClipDictionary);
                writer.WriteAttributeString("ClipDictionaryClip", this.ClipDictionaryClip);
                writer.WriteAttributeString("AbsoluteClipSet", this.AbsoluteClipSet);
                writer.WriteAttributeString("AbsoluteClipSetClip", this.AbsoluteClipSetClip);
                writer.WriteAttributeString("VariableClipSetClip", this.VariableClipSetClip);

                if (this.VariableClipSet != null)
                {
                    writer.WriteAttributeString("ClipSet", this.VariableClipSet.Id.ToString("D", CultureInfo.InvariantCulture));
                    writer.WriteAttributeString("ClipSetName", this.VariableClipSet.Name);
                }
            }
        }
    }
}