﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    public class PargenXmlNode
    {
        public static PargenXmlNode CreateRootNode(string version, string encoding, string rootNodeName)
        {
            var document = new XmlDocument();
            var declaration = document.CreateXmlDeclaration(version, encoding, null);
            document.AppendChild(declaration);
            var rootNode = document.AppendChild(document.CreateElement(rootNodeName));

            return new PargenXmlNode(document, rootNode);
        }

        public void SaveDocument(string pathname)
        {
            document_.Save(pathname);
        }

        public PargenXmlNode AppendChild(string childNodeName)
        {
            var childNode = node_.AppendChild(
                document_.CreateElement(childNodeName));

            return new PargenXmlNode(document_, childNode);
        }

        public PargenXmlNode AppendValueElementNode(string childNodeName, string valueKey, string value)
        {
            var childElement = document_.CreateElement(childNodeName);
            childElement.SetAttribute(valueKey, value);
            var childNode = node_.AppendChild(childElement);

            return new PargenXmlNode(document_, childNode);
        }

        public void AppendTextElementNode(string childNodeName, string text)
        {
            var childNode = node_.AppendChild(
                document_.CreateElement(childNodeName));
            childNode.InnerText = text;
        }

        private PargenXmlNode(XmlDocument document, XmlNode node)
        {
            document_ = document;
            node_ = node;
        }

        private XmlDocument document_;
        private XmlNode node_;
    }
}
