﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class Int : SignalBase, ISerializable
    {
        public Int(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public Int(IntSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Int"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Int(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "intsignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new Int(new IntSignal());
            (clone as Int).AdditionalText = this.AdditionalText;
            (clone as Int).Database = this.Database;
            (clone as Int).Enabled = this.Enabled;
            (clone as Int).Name = this.Name;
            (clone as Int).Position = this.Position;
            (clone as Int).Signal = this.Signal;

            return (ISelectable)clone;
        }
    }
}