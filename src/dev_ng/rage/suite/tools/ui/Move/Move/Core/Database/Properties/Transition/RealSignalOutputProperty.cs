﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class RealSignalOutputProperty : ITransitionProperty, ISerializable
    {
        #region Contructors

        public RealSignalOutputProperty()
        {
            Id = Guid.NewGuid();
        }

        public RealSignalOutputProperty(RealSignalOutputProperty other)
        {
            this.Enabled = other.Enabled;
            this.Name = other.Name;
            this.Parent = other.Parent;
            if (other.SelectedSignal != null && other.SelectedSignal is ICloneable)
            {
                this.SelectedSignal = other.SelectedSignal;
            }

            Id = Guid.NewGuid();
        }
        #endregion // Constructors

        #region Properties
        public string Name { get; set; }
        public Guid Id { get; set; }

        public ITransition Parent { get; set; }

        public bool Enabled { get; set; }

        public Signal SelectedSignal
        {
            get { return _SelectedSignal; }
            set
            {
                if (_SelectedSignal != value)
                {
                    _SelectedSignal = value;
                    OnPropertyChanged("SelectedSignal");
                }
            }
        }
        Signal _SelectedSignal;
        #endregion // Properties

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }
        #endregion // Events

        #region Serialisation
        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Enabled = "Enabled";
            public static string SelectedSignal = "SelectedSignal";
        }

        public RealSignalOutputProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ITransition)info.GetValue(SerializationTag.Parent, typeof(ITransition));
            Enabled = info.GetBoolean(SerializationTag.Enabled);
            SelectedSignal = (Signal)info.GetValue(SerializationTag.SelectedSignal, typeof(Signal));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Enabled, Enabled);
            info.AddValue(SerializationTag.SelectedSignal, SelectedSignal);
        }
        #endregion // Serialisation

        public object Clone()
        {
            return new RealSignalOutputProperty(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RealSignalOutputProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public RealSignalOutputProperty(XmlReader reader, Database database, ITransition parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Deserialise(reader, database);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Enabled", this.Enabled.ToString());

            if (this.SelectedSignal != null)
            {
                writer.WriteAttributeString("Signal", this.SelectedSignal.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedSignal.Name);
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                return;
            }


            this.Enabled = bool.Parse(reader.GetAttribute("Enabled"));

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedSignal = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedSignal == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedSignal = database.GetSignal(signalName);
                    if (this.SelectedSignal == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}'. This needs fixing before a export is allowed.", signalId);
                    }
                }
            }

            reader.Skip();
        }
    }
}
