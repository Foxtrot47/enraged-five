﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rage.Move.Utils
{
    public static class FileComparison
    {
        public static bool FileStreamContentsAreIdentical(System.IO.FileStream fileStream1, System.IO.FileStream fileStream2)
        {
            if (fileStream1.Length != fileStream2.Length)
                return false;

            while (fileStream1.Position != fileStream1.Length)
            {
                long numBytesRemaining = fileStream1.Length - fileStream1.Position;
                int bytesToRead = (int)Math.Min(numBytesRemaining, bufferSize_);
                byte[] dataBlock1 = new byte[bytesToRead];
                byte[] dataBlock2 = new byte[bytesToRead];
                fileStream1.Read(dataBlock1, 0, bytesToRead);
                fileStream2.Read(dataBlock2, 0, bytesToRead);

                for (int index = 0; index < bytesToRead; ++index)
                {
                    if (dataBlock1[index] != dataBlock2[index])
                        return false;
                }
            }

            return true;
        }

        private static long bufferSize_ = 32 * 1024;
    }
}
