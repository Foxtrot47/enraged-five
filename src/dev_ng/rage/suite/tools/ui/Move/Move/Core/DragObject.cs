﻿using New.Move.Core;

namespace Move.Core
{
    public class DragObject
    {
        public DragObject(IDesc desc)
        {
            Desc = desc;
        }

        public IDesc Desc { get; private set; }
    }
}