﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace New.Move.Core
{
    class OrphanedNodeIssue : IDatabaseIssue
    {
        internal OrphanedNodeIssue(INode node, string hierarchicalNodeName)
        {
            node_ = node;
            hierarchicalNodeName_ = hierarchicalNodeName;
        }

        #region IDatabaseIssue Members

        public string Description
        {
            get 
            {
                return string.Format("Node '{0}' references an invalid parent node and is this orphaned from the rest of the database.", hierarchicalNodeName_);
            }
        }

        public void Fix(Database database)
        {
            if (database.Nodes.Contains(node_))// As removes are recursive this may have been fixed by a previous IDatabaseIssue.Fix()
            {
                RemoveNodeRecursive(database, node_);
            }
        }

        #endregion

        private void RemoveNodeRecursive(Database database, INode node)
        {
            if (node is ITransitional)
            {
                ITransitional transitional = (ITransitional)node;
                INode[] childNodes = database.Nodes.Where(n => (n.Parent != Guid.Empty && n.Parent == node.Id )).ToArray();// We can't use the corrupted node's child list as that may be comprimised
                foreach (var childNode in childNodes)
                {
                    RemoveNodeRecursive(database, childNode);
                }
            }

            database.Nodes.Remove(node);
        }

        private INode node_;
        private string hierarchicalNodeName_;
    }
}
