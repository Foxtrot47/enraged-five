﻿using System;

namespace New.Move.Core
{
    public class IsSelectedChangedEventArgs : EventArgs
    {
        public IsSelectedChangedEventArgs(SelectionChangedAction action, int count)
        {
            Action = action;
            Count = count;
        }

        public SelectionChangedAction Action { get; private set; }
        public int Count { get; private set; }
    }
}