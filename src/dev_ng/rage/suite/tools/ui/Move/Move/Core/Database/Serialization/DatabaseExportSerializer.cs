using System;
using System.Collections.Generic;
using System.IO;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    public class DatabaseExportSerializer
    {
        public enum Version
        {
            Major = 1,
            Minor = 9,
            Patch = 14,
            IsDev = 0,
        }

        public static string GetVersionString()
        {
            string version = (int)(Version.Major) + "." +
                                (int)(Version.Minor) + "." +
                                (int)(Version.Patch) + "." +
                                (int)(Version.IsDev);

            return version;
        }

        public static string GetVersionSubString()
        {
            return "ALPHA (Work in Progress)";
        }
    }
}
