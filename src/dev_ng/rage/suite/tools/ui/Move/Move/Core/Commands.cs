﻿using System;

namespace Rage.Move.Core.Commands
{
    public abstract class Command
    {
        abstract public void Redo();
        abstract public void Undo();
        abstract public void Do();
    }

    public class Invoker
    {
        private Command[] commands;
        private int top = 1;
        private int pos = 1;
        private int bottom = 0;

        public Invoker(int capacity)
        {
            if (capacity < 1)
                throw new ArgumentOutOfRangeException("Capacity needs to be atleast 1");

            commands = new Command[capacity + 1];
        }

        public void Execute(Command command)
        {
            command.Do();
            this.Push(command);
        }

        public void Redo()
        {
            if (pos != top)
            {
                ++pos;
                if (pos >= commands.Length)
                    pos -= commands.Length;

                Command command = commands[pos];
                command.Redo();
            }
        }

        public void Undo()
        {
            Command command = this.Peek();
            command.Undo();

            this.Pop();
        }

        private bool IsFull
        {
            get { return top == bottom; }
        }

        private int Count
        {
            get
            {
                int count = pos - bottom - 1;
                if (count < 0)
                    count += commands.Length;
                return count;
            }
        }

        private int Capacity
        {
            get { return commands.Length - 1; }
        }

        private void Pop()
        {
            if (Count > 0)
            {
                --pos;
                if (pos < 0)
                    pos += commands.Length;
            }
            else
            {
                throw new InvalidOperationException("Cannot pop from an expty stack");
            }
        }

        private void Push(Command command)
        {
            if (IsFull)
            {
                bottom++;
                if (bottom >= commands.Length)
                    bottom -= commands.Length;
            }

            ++pos;
            if (pos >= commands.Length)
                pos -= commands.Length;

            if (pos != top + 1)
            {
                if (pos < top)
                {
                    for (int i = pos + 1; i < top; ++i)
                        commands[i] = default(Command);
                }
                else
                if (top < pos)
                {
                    for (int i = pos + 1; i < commands.Length; ++i)
                        commands[i] = default(Command);
                    for (int i = 0; i <= top; ++i)
                        commands[i] = default(Command);
                }
            }

            top = pos;
            
            commands[pos] = command;
        }

        private Command Peek()
        {
            return commands[pos];
        }

        public void Clear()
        {
            if (Count > 0)
            {
                for (int i = 0; i < commands.Length; ++i)
                {
                    commands[i] = default(Command);
                }
                top = 1;
                pos = 1;
                bottom = 0;
            }
        }

    }
}
