﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class LogicalTunnelEventProperty : LogicalEventProperty, INotifyPropertyChanged, ISerializable
    {
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.LogicalTunnelEventProperty"/> class using the specified
        /// System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> parameter is null.
        /// </exception>
        public LogicalTunnelEventProperty(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Enabled = bool.Parse(reader.GetAttribute("Enabled"));
            string eventId = reader.GetAttribute("Event");
            if (!string.IsNullOrEmpty(eventId))
            {
                this.SelectedEvent = database.GetEvent(Guid.ParseExact(eventId, "D"));
                if (this.SelectedEvent == null)
                {
                    string eventName = reader.GetAttribute("EventName");
                    this.SelectedEvent = database.GetEvent(eventName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to event with id '{0}'. This needs fixing before a export is allowed.", eventId);
                    }
                }
            }

            reader.Skip();
        }

        new public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public LogicalTunnelEventProperty()
            : base()
        {  
        }

        public LogicalTunnelEventProperty(LogicalTunnelEventProperty other)
            : base()
        {
            this.Enabled = other.Enabled;
            this.Events = other.Events;
            this.SelectedEvent = other.SelectedEvent;
        }

        LogicEvent _SelectedEvent;
        new public LogicEvent SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != null)
                {
                    _SelectedEvent.SetUsed(false);
                }
                _SelectedEvent = value;
                if (_SelectedEvent != null)
                {
                    _SelectedEvent.SetUsed(true);
                }
            }
        }

        public NoResetObservableCollection<LogicEvent> Events { get; set; }

        bool _Enabled = false;
        new public bool Enabled
        {
            get { return _Enabled; }
            set
            {
                if (_Enabled != value)
                {
                    _Enabled = value;
                    OnPropertyChanged("Enabled");
                }
            }
        }

        new static class SerializationTag
        {
            public static string Enabled = "Enabled";
            public static string SelectedEvent = "SelectedEvent";
           
        }

        public LogicalTunnelEventProperty(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            try
            {
                Enabled = info.GetBoolean(SerializationTag.Enabled);
                SelectedEvent = (LogicEvent)info.GetValue(SerializationTag.SelectedEvent, typeof(LogicEvent));
            }
            catch (Exception)
            {
            
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(LogicalEventProperty.SerializationTag.Name, Name);
            info.AddValue(LogicalEventProperty.SerializationTag.Id, Id);
            info.AddValue(LogicalEventProperty.SerializationTag.Parent, Parent);
            
            info.AddValue(SerializationTag.Enabled, _Enabled);
            info.AddValue(SerializationTag.SelectedEvent, SelectedEvent);
        }

        public void ExportToXML(PargenXmlNode parentNode)
        {
            if (Enabled && SelectedEvent != null)
            {
                parentNode.AppendValueElementNode("Enabled", "value", "true");
                parentNode.AppendTextElementNode("Id", SelectedEvent.Name);
            }
            else
            {
                parentNode.AppendValueElementNode("Enabled", "value", "false");
            }   
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        internal void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Enabled", this.Enabled.ToString(CultureInfo.InvariantCulture));

            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString("Event", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("EventName", this.SelectedEvent.Name);
            }
        }
    }
}
