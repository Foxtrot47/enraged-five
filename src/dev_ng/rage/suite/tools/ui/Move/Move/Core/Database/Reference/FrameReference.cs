﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class FrameReference : ReferenceSignal, ISerializable
    {
        public FrameReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public FrameReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}