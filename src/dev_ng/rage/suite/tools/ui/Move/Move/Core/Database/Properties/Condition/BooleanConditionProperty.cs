﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;

namespace New.Move.Core
{
    [Serializable]
    public class BooleanConditionProperty : IConditionProperty, ISerializable
    {
        public static implicit operator BooleanConditionProperty(bool boolean)
        {
            BooleanConditionProperty property = new BooleanConditionProperty();
            property.Value = boolean;

            return property;
        }

        public BooleanConditionProperty()
        {
            Id = Guid.NewGuid();
        }

        public BooleanConditionProperty(BooleanConditionProperty other)
        {
            Id = Guid.NewGuid();
            this.Name = other.Name;
            this.Value = other.Value;
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        public bool Value { get; set; }
        public Condition Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag 
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Value = "Value";
            public static string Parent = "Parent";
        }

        public BooleanConditionProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Value = (bool)info.GetValue(SerializationTag.Value, typeof(bool));
            Parent = (Condition)info.GetValue(SerializationTag.Parent, typeof(Condition));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Value, Value);
            info.AddValue(SerializationTag.Parent, Parent);
        }

        public object Clone()
        {
            return new BooleanConditionProperty(this);
        }
                     
        /// <summary>
        /// Initialises a new instance of the <see cref="BooleanConditionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public BooleanConditionProperty(XmlReader reader, Database database, Condition parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Value = bool.Parse(reader.GetAttribute("Value"));
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Value", this.Value.ToString(CultureInfo.InvariantCulture));
        }
    }
}