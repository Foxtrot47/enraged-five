﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Xml;

using Move.Core;
using Move.Utils;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    public interface ILogicalEvent
    {
        object Tag { get; set; }
    }

    [Serializable]
    public class LogicalEventProperty : ILogicalEvent, IAnchor, INotifyPropertyChanged, ISerializable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public LogicalEventProperty()
        {
            Id = Guid.NewGuid();
        }

        public LogicalEventProperty(LogicalEventProperty other)
        {
            _Enabled = other._Enabled;
            Id = Guid.NewGuid();
            this.SelectedEvent = other.SelectedEvent;
        }

        public const int EventCount = 2;

        public string Name { get; set; }
        public Guid Id { get; set; }

        public ILogic Parent { get; set; }

        public object Tag { get; set; }

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            from.RemoveConnectionsFromAnchor(this);
        }

        bool _Enabled = false;
        public bool Enabled
        {
            get { return _Enabled; }
            set
            {
                if (_Enabled != value)
                {
                    _Enabled = value;
                    OnPropertyChanged("Enabled");
                }
            }
        }

        public LogicEvent SelectedEvent { get; set; }
        
        static protected class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Enabled = "Enabled";
            public static string SelectedEvent = "SelectedEvent";
        }

        public LogicalEventProperty(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(SerializationTag.Name);
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
            _Enabled = info.GetBoolean(SerializationTag.Enabled);
            try
            {
                SelectedEvent = (LogicEvent)info.GetValue(SerializationTag.SelectedEvent, typeof(LogicEvent));
            }
            catch (Exception)
            {
            } 
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Enabled, _Enabled);
            info.AddValue(SerializationTag.SelectedEvent, SelectedEvent);

        }

        public void ExportToXML(PargenXmlNode parentNode, Motiontree motiontreeParent)
        {
            parentNode.AppendValueElementNode("Enabled", "value", Enabled ? "true" : "false");

            if (Enabled)
            {
                if (SelectedEvent != null)
                    parentNode.AppendTextElementNode("Id", SelectedEvent.Name);
                else
                {
                    // Is it connected to a tunnel event?
                    foreach (Connection connection in motiontreeParent.Connections)
                    {
                        if (connection.Source == this && connection.Dest != null && connection.Dest.Parent != null && connection.Dest.Parent is TunnelEvent)
                        {
                            TunnelEvent tunnelEvent = (TunnelEvent)connection.Dest.Parent;
                            if( tunnelEvent.Event != null )
                                parentNode.AppendTextElementNode("Id", tunnelEvent.Event.Name);
                        }
                    }
                }
            }
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.LogicalEventProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public LogicalEventProperty(XmlReader reader, Database database, ILogic parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Enabled = bool.Parse(reader.GetAttribute("Enabled"));

            string eventId = reader.GetAttribute("Event");
            if (!string.IsNullOrEmpty(eventId))
            {
                this.SelectedEvent = database.GetEvent(Guid.ParseExact(eventId, "D"));
                if (this.SelectedEvent == null)
                {
                    string eventName = reader.GetAttribute("EventName");
                    this.SelectedEvent = database.GetEvent(eventName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to event with id '{0}' inside the '{1}' node. This needs fixing before a export is allowed.", eventId, parent.Name);
                    }
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Enabled", this.Enabled.ToString(CultureInfo.InvariantCulture));

            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString("Event", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("EventName", this.SelectedEvent.Name);
            }
        }
    }
}