﻿using Move.Utils;

namespace New.Move.Core
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Windows;
    using System.Xml;

    /// <summary>
    /// When implemented represents a node within a database that has children plus a
    /// transition order which defines the order of priority that transitions are executed in
    /// that are attached to this node.
    /// </summary>
    public interface ITransitional : INode
    {
        #region Events
        /// <summary>
        /// Occurs when this transitional node becomes or stops becoming the default
        /// transitional node for its parent.
        /// </summary>
        event EventHandler<DefaultChangedEventArgs> DefaultChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this node is a automaton node.
        /// </summary>
        bool IsAutomaton { get; }

        /// <summary>
        /// Gets the collection of child nodes for this node.
        /// </summary>
        NoResetObservableCollection<INode> Children { get; }

        /// <summary>
        /// Gets the <see cref="New.Move.Core.ITransitionalDesc"/> object that describes
        /// the creation of this object.
        /// </summary>
        ITransitionalDesc Descriptor { get; }

        /// <summary>
        /// Gets or sets the offset of the scroll bar that the diagram for this node has.
        /// </summary>
        Point ScrollOffset { get; set; }

        /// <summary>
        /// Gets or sets the current zoom factor that the diagram for this node has.
        /// </summary>
        double Zoom { get; set; }

        /// <summary>
        /// Gets the collection defining the transition order for this node.
        /// </summary>
        TransitionCollection TransitionOrder { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Adds the specified node to this transitional nodes child collection.
        /// </summary>
        /// <param name="node">
        /// The node to add.
        /// </param>
        void Add(INode node);

        /// <summary>
        /// Removes the specified node from this transitional nodes child collection.
        /// </summary>
        /// <param name="node">
        /// The node to remove.
        /// </param>
        void Remove(INode node);

        /// <summary>
        /// Called when this transitional node becomes or stops becoming the default
        /// transitional node for its parent.
        /// </summary>
        /// <param name="isDefault">
        /// A value indicating whether this node is the default transitional node or not.
        /// </param>
        void OnDefaultChanged(bool isDefault);

        /// <summary>
        /// Adds this given handler to the child collection changed event.
        /// </summary>
        /// <param name="handler">
        /// The handler to add to the children changed event.
        /// </param>
        void AddHandler(NotifyCollectionChangedEventHandler handler);

        /// <summary>
        /// Removes this given handler to the child collection changed event.
        /// </summary>
        /// <param name="handler">
        /// The handler to remove to the children changed event.
        /// </param>
        void RemoveHandler(NotifyCollectionChangedEventHandler handler);

        /// <summary>
        /// Copies this item and the speciifed child items by serialising them out to the
        /// specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the specified items get serialised out to.
        /// </param>
        /// <param name="items">
        /// The child items that should be serialised to the specified writer.
        /// </param>
        void Copy(XmlWriter writer, IEnumerable<ISelectable> items);

        /// <summary>
        /// Pastes the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The reader containing the data to paste.
        /// </param>
        /// <param name="offset">
        /// A offset point that determines the position offset for the child objects.
        /// </param>
        void Paste(XmlReader reader, Point offset, bool cutting);
        #endregion
    } // New.Move.Core.ITransitional {Class}
} // New.Move.Core {Namespace}
