﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Move.Core.Restorable
{
    public abstract class RestorableObjectManager : RestorableObject, IActionManager
    {
        protected RestorableObjectManager()
        {
            PushHandler(PropertyChanged);
        }

        void PropertyChanged(object sender, RestorablePropertyChangedEventArgs e)
        {
            Push(e.Action);
        }

        private Stack<EventHandler<RestorablePropertyChangedEventArgs>> PropertyChangedStack =
            new Stack<EventHandler<RestorablePropertyChangedEventArgs>>();

        public void PushHandler(EventHandler<RestorablePropertyChangedEventArgs> handler)
        {
            PropertyChangedStack.Push(handler);
            RestorablePropertyChanged = handler;
        }

        public void PopHandler()
        {
            PropertyChangedStack.Pop();
            RestorablePropertyChanged = PropertyChangedStack.Peek();
        }

        public abstract void Push(IRestorableAction action);
    }
}