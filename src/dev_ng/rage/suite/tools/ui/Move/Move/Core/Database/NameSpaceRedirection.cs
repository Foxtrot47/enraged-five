﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Reflection;

namespace New.Move.Core
{
    public class NameSpaceRedirection : SerializationBinder
    {

        public override Type BindToType(string assemblyName, string typeName)
        {
            string oldNameSpace = "RSG.Base";
            string newNameSpace = "RSG.TrackViewer";


            if (typeName.Contains(string.Concat(oldNameSpace, ".TrackPoint")))
            {

                typeName = typeName.Replace(string.Concat(oldNameSpace, ".TrackPoint"), string.Concat(newNameSpace, ".Data.TrackPoint"));
                typeName = typeName.Replace(oldNameSpace, newNameSpace);
                assemblyName = assemblyName.Replace(oldNameSpace, newNameSpace);
            }
            if (typeName.Contains(string.Concat(oldNameSpace, ".FloatTrackPoint")))
            {
                typeName = typeName.Replace(string.Concat(oldNameSpace, ".FloatTrackPoint"), string.Concat(newNameSpace, ".Data.FloatTrackPoint"));
                typeName = typeName.Replace(oldNameSpace, newNameSpace);
                assemblyName = assemblyName.Replace(oldNameSpace, newNameSpace);
            }
            else if (typeName.Contains(string.Concat(oldNameSpace, ".TrackGroup")))
            {
                typeName = typeName.Replace(string.Concat(oldNameSpace, ".TrackGroup"), string.Concat(newNameSpace, ".Data.TrackGroup"));
                typeName = typeName.Replace(oldNameSpace, newNameSpace);
                assemblyName = assemblyName.Replace(oldNameSpace, newNameSpace);
            }
            else if (typeName.Contains(string.Concat(oldNameSpace, ".Track")))
            {
                typeName = typeName.Replace(string.Concat(oldNameSpace, ".Track"), string.Concat(newNameSpace, ".Data.Track"));
                typeName = typeName.Replace(oldNameSpace, newNameSpace);
                assemblyName = assemblyName.Replace(oldNameSpace, newNameSpace);
            }
            
            return Type.GetType(String.Format("{0}, {1}", typeName, assemblyName), true);
            
        }
    }
}
