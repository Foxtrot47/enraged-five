﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using Move.Core.Restorable;
using Move.Utils;
using System.Xml;

namespace New.Move.Core
{
    /// <summary>
    /// Provides a abstract base class for all transitional logical parents.
    /// </summary>
    public abstract class LogicParent :
        RestorableObject, ILogic, IAutomaton, ITransitional, ITransitionalParent
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Children"/> property.
        /// </summary>
        public const string ChildrenSerializationTag = "Children";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Transitions"/>
        /// property.
        /// </summary>
        public const string TransitionsSerializationTag = "Transitions";

        /// <summary>
        /// The private field for the <see cref="AdditionalText"/> property.
        /// </summary>
        private string additionalText;

        /// <summary>
        /// The private field for the <see cref="Children"/> property.
        /// </summary>
        private NoResetObservableCollection<INode> children;

        /// <summary>
        /// The private field for the <see cref="Database"/> property.
        /// </summary>
        private Database database;

        /// <summary>
        /// The private field for the <see cref="DefaultTransitional"/> property.
        /// </summary>
        private ITransitional defaultTransitional;

        /// <summary>
        /// The private field for the <see cref="Enabled"/> property.
        /// </summary>
        private bool enabled = true;

        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field for the <see cref="Parent"/> property.
        /// </summary>
        private Guid parent;

        /// <summary>
        /// The private field for the <see cref="Position"/> property.
        /// </summary>
        private Point position;

        /// <summary>
        /// The private field for the <see cref="ScrollOffset"/> property.
        /// </summary>
        private Point scrolloffset;

        /// <summary>
        /// The private field for the <see cref="Tag"/> property.
        /// </summary>
        private object tag;

        /// <summary>
        /// The private field for the <see cref="TransitionOrder"/> property.
        /// </summary>
        private TransitionCollection transitionOrder;

        /// <summary>
        /// The private field for the <see cref="Transitions"/> property.
        /// </summary>
        private TransitionCollection transitions;

        /// <summary>
        /// The private field for the <see cref="Zoom"/> property.
        /// </summary>
        private double zoom;

        /// <summary>
        /// A list of transition ids that need resolving on load to fill in the
        /// transition order.
        /// </summary>
        protected List<Guid> transitionOrderResolve = new List<Guid>();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.LogicParent"/> class.
        /// </summary>
        public LogicParent()
        {
            this.Id = Guid.NewGuid();
            this.Children = new NoResetObservableCollection<INode>();
            this.Transitions = new TransitionCollection();
            this.Transitions.CollectionChanged += Transitions_CollectionChanged;
            this.TransitionOrder = new TransitionCollection();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.LogicParent"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public LogicParent(SerializationInfo info, StreamingContext context)
        {
            // This is never called by any of its children...

            Id = (Guid)info.GetValue("Id", typeof(Guid));

            Transitions.CollectionChanged += Transitions_CollectionChanged;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the default transitional node changes for this state machine.
        /// </summary>
        public event EventHandler<DefaultChangedEventArgs> DefaultChanged;

        /// <summary>
        /// Occurs when this nodes engaged state has changed.
        /// </summary>
        public event EventHandler<IsEngagedChangedEventArgs> IsEngagedChanged;

        /// <summary>
        /// Occurs when the selection state of this transition has changed.
        /// </summary>
        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets any additional text associated with this logic parent.
        /// </summary>
        public string AdditionalText
        {
            get { return this.additionalText; }
            set { this.additionalText = value; }
        }

        /// <summary>
        /// Gets the collection of child nodes currently in this state machine.
        /// </summary>
        public NoResetObservableCollection<INode> Children
        {
            get { return children; }
            protected set { children = value; }
        }

        /// <summary>
        /// Gets or sets the current database this logic parent belongs to.
        /// </summary>
        public Database Database
        {
            get { return this.database; }
            set { this.database = value; ; }
        }

        /// <summary>
        /// Gets or sets the transitional object that is a child of this logic parent and
        /// is set to the default transitional object. 
        /// </summary>
        public ITransitional Default
        {
            get
            {
                return defaultTransitional;
            }

            set
            {
                if (object.ReferenceEquals(value, this.defaultTransitional))
                    return;

                if (this.defaultTransitional != null)
                    this.defaultTransitional.OnDefaultChanged(false);

                defaultTransitional = value;

                if (value != null)
                    value.OnDefaultChanged(true);
            }
        }

        /// <summary>
        /// Gets the transitional descriptor for this logic parent.
        /// </summary>
        public abstract ITransitionalDesc Descriptor { get; }

        /// <summary>
        /// Gets the descriptor that describes this logic parent.
        /// </summary>
        public IDesc Desc
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this logic parent is currently enabled.
        /// </summary>
        public bool Enabled
        {
            get { return this.enabled; }
            set { this.enabled = value; }
        }

        /// <summary>
        /// Gets or sets the unique global identifier for this logic parent.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; ; }
        }

        /// <summary>
        /// Gets a value indicating whether this logic parent is a automaton.
        /// </summary>
        public bool IsAutomaton
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets the name for this logic parent.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the identifier that references the parent for this logic parent.
        /// </summary>
        public Guid Parent
        {
            get { return this.parent; }
            set { this.parent = value; ; }
        }

        /// <summary>
        /// Gets or sets the current position for this logic parent.
        /// </summary>
        public Point Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        /// <summary>
        /// Gets or sets the current scroll offset for this logic parent.
        /// </summary>
        public Point ScrollOffset
        {
            get { return this.scrolloffset; }
            set { this.scrolloffset = value; }
        }

        /// <summary>
        /// Gets or sets the object tag that can be set to the UI component representing
        /// this logic parent.
        /// </summary>
        public object Tag
        {
            get { return this.tag; }
            set { this.tag = value; }
        }

        /// <summary>
        /// Gets the collection of transitions that are currently in this logic parent.
        /// </summary>
        public TransitionCollection Transitions
        {
            get { return transitions; }
            set { transitions = value; }
        }

        /// <summary>
        /// Gets or sets the collection of transitions in order for this logic parent.
        /// </summary>
        public TransitionCollection TransitionOrder
        {
            get { return transitionOrder; }
            set { transitionOrder = value; }
        }

        /// <summary>
        /// Gets the string type for this motion tree.
        /// </summary>
        public string Type
        {
            get { return Desc.Name; }
        }

        /// <summary>
        /// Gets or sets the current zoom factor for this logic parent.
        /// </summary>
        public double Zoom
        {
            get { return this.zoom; }
            set { this.zoom = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a node to this logical parents child collection.
        /// </summary>
        /// <param name="node">
        /// The node to add to this logic parent.
        /// </param>
        public void Add(INode node)
        {
            Debug.Assert(node is ITransitional);
            //  When cutting and pasting between instances of MoVE then the database
            //  of the node needs to be set to the current database.
            //Debug.Assert(node.Database == Database);
            node.Database = Database;

            ITransitional transitional = node as ITransitional;
            if (transitional == null || this.Children.Contains(transitional))
                return;

            transitional.Parent = this.Id;
            this.Children.Add(transitional);

            if (this.Default == null)
                this.Default = transitional;
        }

        /// <summary>
        /// Adds this given handler to the child collection changed event.
        /// </summary>
        /// <param name="handler">
        /// The handler to add to the children changed event.
        /// </param>
        public void AddHandler(NotifyCollectionChangedEventHandler handler)
        {
            Children.CollectionChanged += handler;
        }

        /// <summary>
        /// Disposes of this current instance.
        /// </summary>
        public void Dispose()
        {
            if (this.Parent != Guid.Empty && this.Database != null)
            {
                ITransitional parent = Database.Find(Parent) as ITransitional;
                if (parent != null)
                    parent.Remove(this);
            }

            if (this.Database != null)
                this.Database.Nodes.Remove(this);

            if (this.Children == null)
                return;

            INode[] childArray = new INode[this.Children.Count];
            this.Children.CopyTo(childArray, 0);
            foreach (INode child in childArray)
            {
                child.Dispose();
            }

            this.Children.Clear();
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            // This is never called by any of its children...

            info.AddValue("Id", Id);
            info.AddValue("Database", Database);
        }

        /// <summary>
        /// Gets called after this node has been loaded and is added to a database.
        /// </summary>
        public void OnLoadComplete()
        {
            foreach (Guid id in this.transitionOrderResolve)
            {
                foreach (ITransitionalParent parent in this.Database.Nodes.OfType<ITransitionalParent>())
                {
                    bool found = false;
                    foreach (Transition transition in parent.Transitions)
                    {
                        if (id == transition.Id)
                        {
                            this.TransitionOrder.Add(transition);
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Called when this transitional node becomes or stops being the default
        /// transitional node for its parent.
        /// </summary>
        /// <param name="isDefault">
        /// A value indicating whether this node is the default transitional node or not.
        /// </param>
        public void OnDefaultChanged(bool isDefault)
        {
            EventHandler<DefaultChangedEventArgs> handler = this.DefaultChanged;
            if (handler == null)
                return;

            this.DefaultChanged(this, new DefaultChangedEventArgs(isDefault));
        }

        /// <summary>
        /// Called when this nodes engaged state has changed.
        /// </summary>
        /// <param name="action">
        /// The action associated with the change.
        /// </param>
        public void OnEngagedChanged(EngagedChangedAction action)
        {
            EventHandler<IsEngagedChangedEventArgs> handler = this.IsEngagedChanged;
            if (handler == null)
                return;

            this.IsEngagedChanged(this, new IsEngagedChangedEventArgs(action));
        }

        /// <summary>
        /// Fires the selection changed event.
        /// </summary>
        /// <param name="action">
        /// Defines whether this transition has been added to the selection or removed from
        /// the selection.
        /// </param>
        /// <param name="count">
        /// The total number of items that are currently selected.
        /// </param>
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            EventHandler<IsSelectedChangedEventArgs> handler = this.IsSelectedChanged;
            if (handler == null)
                return;

            this.IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
        }

        /// <summary>
        /// Removes this given handler to the child collection changed event.
        /// </summary>
        /// <param name="handler">
        /// The handler to remove to the children changed event.
        /// </param>
        public void RemoveHandler(NotifyCollectionChangedEventHandler handler)
        {
            Children.CollectionChanged -= handler;
        }

        /// <summary>
        /// Removes a node from this state machines child collection.
        /// </summary>
        /// <param name="node">
        /// The node to remove from this static machine.
        /// </param>
        public void Remove(INode node)
        {
            Debug.Assert(node is ITransitional);
            //  When cutting and pasting between instances of MoVE then the database
            //  of the node needs to be set to the current database.
            //Debug.Assert(node.Database == Database);
            node.Database = Database;

            ITransitional transitional = node as ITransitional;
            if (transitional == null || !this.Children.Contains(transitional))
                return;

            bool changeDefault = false;
            if (object.ReferenceEquals(this.Default, transitional))
                changeDefault = true;

            transitional.Parent = Guid.Empty;
            this.Children.Remove(transitional);

            List<Transition> invalidTransitions = new List<Transition>();
            foreach (Transition transition in this.Transitions)
            {
                if (object.ReferenceEquals(transition.Source, transitional))
                {
                    invalidTransitions.Add(transition);
                }
                else if (object.ReferenceEquals(transition.Dest, transitional))
                {
                    invalidTransitions.Add(transition);
                }
            }
            foreach (Transition transition in invalidTransitions)
                this.Transitions.Remove(transition);

            if (changeDefault)
                this.Default = this.Children.FirstOrDefault() as ITransitional;
        }

        /// <summary>
        /// Gets the string representation of this instance.
        /// </summary>
        /// <returns>
        /// The string representation of this instance.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        /// <param name="hashedNames">
        /// A collection of hashed names to use during the export process.
        /// </param>
        public abstract void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames);

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        public virtual object Clone()
        {
            return null;
        }

        /// <summary>
        /// Gets the transition inside this state machine that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the transition to retrieve.
        /// </param>
        /// <returns>
        /// The transition with the specified id if found; otherwise false.
        /// </returns>
        public Transition GetTransition(Guid id)
        {
            foreach (Transition transition in this.Transitions)
            {
                if (transition.Id == id)
                {
                    return transition;
                }
            }

            return null;
        }

        /// <summary>
        /// Get called whenever the transition in the state machine change. Makes sure that
        /// the order is kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The observable collection that changed.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        protected void Transitions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this.children == null || this.transitionOrder == null)
                return;

            if (e.Action == NotifyCollectionChangedAction.Remove && e.OldItems != null)
            {
                foreach (Transition transition in e.OldItems.Cast<Transition>())
                {
                    if (!this.children.Contains(transition.Source))
                        continue;

                    transition.Source.TransitionOrder.Remove(transition);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems != null)
            {
                foreach (Transition transition in e.NewItems.Cast<Transition>())
                {
                    if (!this.children.Contains(transition.Source))
                        continue;

                    transition.Source.TransitionOrder.Add(transition);
                }
            }
        } 

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public abstract void Serialise(XmlWriter writer);

        /// <summary>
        /// Copies this item and the speciifed child items by serialising them out to the
        /// specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the specified items get serialised out to.
        /// </param>
        /// <param name="items">
        /// The child items that should be serialised to the specified writer.
        /// </param>
        public virtual void Copy(XmlWriter writer, IEnumerable<ISelectable> items)
        {
            writer.WriteStartElement(TransitionsSerializationTag);
            foreach (Transition transistion in this.Transitions)
            {
                if (!System.Linq.Enumerable.Contains(items, transistion))
                {
                    continue;
                }

                transistion.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteStartElement(ChildrenSerializationTag);
            foreach (INode node in this.Children)
            {
                if (!System.Linq.Enumerable.Contains(items, node))
                {
                    continue;
                }

                node.Serialise(writer);
            }

            writer.WriteEndElement();

            writer.WriteStartElement("Ordering");
            foreach (INode node in this.Children)
            {
                if (!items.Contains(node))
                {
                    continue;
                }

                ITransitional transitional = node as ITransitional;
                if (transitional != null)
                {
                    writer.WriteStartElement("Order");
                    writer.WriteAttributeString("Id", node.Id.ToString("D"));

                    foreach (Transition transition in transitional.TransitionOrder)
                    {
                        if (!items.Contains(transition))
                        {
                            continue;
                        }

                        writer.WriteStartElement("Transition");
                        writer.WriteAttributeString("Id", transition.Id.ToString("D"));
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Pastes the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The reader containing the data to paste.
        /// </param>
        /// <param name="offset">
        /// A offset point that determines the position offset for the child objects.
        /// </param>
        public virtual void Paste(XmlReader reader, Point offset, bool cutting)
        {

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            List<INode> children = new List<INode>();
            List<INode> newTransitionals = new List<INode>();
            List<Transition> transitions = new List<Transition>();
            Dictionary<Transition, Guid> sources = new Dictionary<Transition, Guid>();
            Dictionary<Transition, Guid> destinations = new Dictionary<Transition, Guid>();
            Dictionary<Guid, List<Guid>> orderings = new Dictionary<Guid, List<Guid>>();
            Dictionary<Guid, INode> map = new Dictionary<Guid, INode>();
            foreach (INode child in this.Children)
            {
                map.Add(child.Id, child);
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals(TransitionsSerializationTag, reader.Name))
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            string sourceId = reader.GetAttribute("Source");
                            string destId = reader.GetAttribute("Destination");
                            Transition newTransition = new Transition(reader, this.Database, this);
                            transitions.Add(newTransition);
                            if (sourceId != null)
                            {
                                sources.Add(newTransition, Guid.Parse(sourceId));
                            }

                            if (destId != null)
                            {
                                destinations.Add(newTransition, Guid.Parse(destId));
                            }
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (String.Equals(ChildrenSerializationTag, reader.Name))
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            INode newNode = this.Database.XmlFactory.CreateNode(reader, this.Database, this);
                            newNode.Position = new Point(newNode.Position.X + offset.X, newNode.Position.Y + offset.Y);
                            if (!cutting)
                            {
                                this.Database.MakeNodeNameUnique(newNode);
                            }

                            this.Database.Nodes.Add(newNode);
                            ITransitional newTransitional = newNode as ITransitional;
                            if (newTransitional != null)
                            {
                                newTransitionals.Add(newTransitional);
                                newTransitional.TransitionOrder.Clear();
                            }

                            if (newNode != null)
                            {
                                children.Add(newNode);
                                map[newNode.Id] = newNode;
                            }
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (String.Equals("Ordering", reader.Name))
                {
                    reader.ReadStartElement();
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        if (String.Equals("Order", reader.Name))
                        {
                            string idString = reader.GetAttribute("Id");
                            List<Guid> orders = new List<Guid>();
                            orderings.Add(Guid.ParseExact(idString, "D"), orders);
                            if (reader.IsEmptyElement)
                            {
                                reader.Skip();
                                continue;
                            }

                            reader.ReadStartElement();
                            while (reader.MoveToContent() == XmlNodeType.Element)
                            {
                                if (String.Equals("Transition", reader.Name))
                                {
                                    string transitionIdString = reader.GetAttribute("Id");
                                    orders.Add(Guid.ParseExact(transitionIdString, "D"));
                                }

                                reader.Skip();
                            }

                            if (reader.MoveToContent() == XmlNodeType.EndElement)
                            {
                                reader.ReadEndElement();
                            }
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }

                    reader.Skip();
                }
                else
                {
                    reader.Skip();
                }
            }

            foreach (KeyValuePair<Transition, Guid> kvp in sources)
            {
                INode source = null;
                if (map.TryGetValue(kvp.Value, out source))
                {
                    kvp.Key.Content.Source = source as ITransitional;
                    kvp.Key.Source = source as ITransitional;
                }
            }

            foreach (KeyValuePair<Transition, Guid> kvp in destinations)
            {
                INode destination = null;
                if (map.TryGetValue(kvp.Value, out destination))
                {
                    kvp.Key.Content.Dest = destination as ITransitional;
                    kvp.Key.Dest = destination as ITransitional;
                }
            }

            List<Guid> ids = new List<Guid>();
            foreach (INode child in children)
            {
                ids.AddRange(child.GetAllGuids());
            }

            foreach (Transition transition in transitions)
            {
                ids.AddRange(transition.GetAllGuids());
            }

            Dictionary<Guid, Guid> idMap = new Dictionary<Guid, Guid>();
            foreach (Guid id in ids)
            {
                if (id == Guid.Empty)
                {
                    continue;
                }

                if (idMap.ContainsKey(id))
                {
                    continue;
                }

                idMap.Add(id, Guid.NewGuid());
            }

            foreach (INode child in children)
            {
                child.Parent = this.Id;
                child.ResetIds(idMap);
                this.Children.Add(child);
            }

            foreach (Transition transition in transitions)
            {
                if (transition.Dest == null || transition.Content.Dest == null)
                {
                    continue;
                }

                if (transition.Source == null || transition.Content.Source == null)
                {
                    continue;
                }

                transition.ResetIds(idMap);
                this.Transitions.Add(transition);
            }

            foreach (ITransitional newTransitional in newTransitionals)
            {
                newTransitional.TransitionOrder.Clear();
                List<Guid> realOrder = null;
                Guid previousId = newTransitional.Id;
                foreach (KeyValuePair<Guid, Guid> kvp in idMap)
                {
                    if (kvp.Value == newTransitional.Id)
                    {
                        previousId = kvp.Key;
                        break;
                    }
                }

                if (!orderings.TryGetValue(previousId, out realOrder))
                {
                    continue;
                }

                foreach (Guid transitionId in realOrder)
                {
                    Guid newId = transitionId;
                    if (!idMap.TryGetValue(transitionId, out newId))
                    {
                        newId = transitionId;
                    }

                    Transition transition = this.GetTransition(newId);
                    if (transition != null)
                    {
                        newTransitional.TransitionOrder.Add(transition);
                    }
                }
            }

            if (this.Default == null)
            {
                if (this.Children.Count > 0 && this.Children[0] is ITransitional)
                {
                    this.Default = this.Children[0] as ITransitional;
                }
            }
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public abstract void ResetIds(Dictionary<Guid, Guid> map);

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public abstract List<Guid> GetAllGuids();
        #endregion
    }
}