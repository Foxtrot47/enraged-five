﻿using System;
using System.Xml;
using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    /// <summary>
    /// When implemented defines a single condition that can be placed onto a transition
    /// inside a state machine.
    /// </summary>
    public interface ICondition
    {
        #region Properties
        /// <summary>
        /// Gets the name given to this condition.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the type of the class that describes the creation of this condition.
        /// </summary>
        Type Descriptor { get; }

        /// <summary>
        /// Gets the xml node name this condition should have when serialised to a .imvf file.
        /// </summary>
        string XMLExportTypeName { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        /// <param name="hashedNames">
        /// A collection of hashed names to use during the export process.
        /// </param>
        void ExportToXML(PargenXmlNode parentNode);

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        ICondition Clone(Condition parent);

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        void Serialise(XmlWriter writer);
        #endregion
    } // New.Move.Core.ICondition {Interface}
} // New.Move.Core {Namespace}
