﻿namespace New.Move.Core
{
    public enum Modifier
    {
        Linear = 0,
        EaseInOut,
        EaseOut,
        EaseIn,
        Step
    }
}