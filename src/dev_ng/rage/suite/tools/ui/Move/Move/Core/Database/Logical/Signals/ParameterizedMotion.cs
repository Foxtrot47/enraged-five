﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class ParameterizedMotion : SignalBase, ISerializable
    {
        public ParameterizedMotion(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public ParameterizedMotion(ParameterizedMotionSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ParameterizedMotion"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public ParameterizedMotion(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "parameterizedmotionsignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new ParameterizedMotion(new ParameterizedMotionSignal());
            (clone as ParameterizedMotion).AdditionalText = this.AdditionalText;
            (clone as ParameterizedMotion).Database = this.Database;
            (clone as ParameterizedMotion).Enabled = this.Enabled;
            (clone as ParameterizedMotion).Name = this.Name;
            (clone as ParameterizedMotion).Position = this.Position;
            (clone as ParameterizedMotion).Signal = this.Signal;

            return (ISelectable)clone;
        }

    }
}