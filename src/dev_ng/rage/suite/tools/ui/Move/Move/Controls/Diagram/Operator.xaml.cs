﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

using Rage.Move.Core.Dag;
using Rage.Move.Core.Dg;
using Rage.Move.Core;
using Rage.Move.Utils;

namespace Rage.Move
{
    public class OperatorSourceItem : SourceItemBase
    {
        public OperatorSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }
    }

    public class OperatorResultItem : AnchorResultItem
    {
        public OperatorResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is OperatorSourceItem || hitobj is SignalPassthroughItem || hitobj is SignalSourceItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public partial class Operator : UserControl, IMeasureable, ISelectable, IConnectable
    {
        public static readonly RoutedEvent PositionChangedEvent = EventManager.RegisterRoutedEvent(
            "PositionChanged", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(Operator));

        public event EventHandler ChildrenLoaded;
        public void OnChildrenLoaded()
        {
            if (ChildrenLoaded != null)
                ChildrenLoaded(this, EventArgs.Empty);
        }

        public event ConnectionOverEventHandler ConnectionEnter;
        public void OnConnectionEnter(IAnchorSender sender)
        {
            if (ConnectionEnter != null)
                ConnectionEnter(sender);
        }

        public event ConnectionOverEventHandler ConnectionExit;
        public void OnConnectionExit(IAnchorSender sender)
        {
            if (ConnectionExit != null)
                ConnectionExit(sender);
        }

        int _Count;
        public int Count
        {
            get { return _Count; }
            set
            {
                _Count = value;

                Panel PART_Anchors = (Panel)GetTemplateChild("PART_Anchors");
                if (PART_Anchors != null)
                {
                    if (PART_Anchors.Children.Count == _Count)
                        OnChildrenLoaded();
                }
            }
        }

        Point _Position;
        public Point Position
        {
            get { return _Position; }
            set
            {
                if (_Position != value)
                {
                    _Position = value;
                    if (PositionChanged != null)
                        PositionChanged(_Position);
                }
            }
        }

        public Guid Id { get; private set; }

        Hashtable _Anchors = new Hashtable();
        public Hashtable Anchors { get { return _Anchors; } }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(Operator));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public Operator(Guid id)
        {
            InitializeComponent();
            DataContext = this;
            Id = id;

            AddHandler(PositionChangedEvent, new RoutedEventHandler(OnPositionChanged), true);
        }

        void OnPositionChanged(object sender, RoutedEventArgs e)
        {
            Position = new Point(Canvas.GetLeft(this), Canvas.GetTop(this));
        }

        public event PositionChangedEventHandler PositionChanged = null;

        public static readonly DependencyProperty SelectedProperty =
            DependencyProperty.Register("Selected", typeof(bool), typeof(Operator), new FrameworkPropertyMetadata(false));

        public bool Selected
        {
            get { return (bool)GetValue(SelectedProperty); }
            set { SetValue(SelectedProperty, value); }
        }

        public Point Center
        {
            get { return new Point(Position.X + (DesiredSize.Width / 2), Position.Y + (DesiredSize.Height / 2)); }
        }

        public Point TopCenter
        {
            get { return new Point(Position.X + (DesiredSize.Width / 2), Position.Y); }
        }

        public Point TopRight
        {
            get { return new Point(Position.X + DesiredSize.Width, Position.Y); }
        }

        public Point TopLeft
        {
            get { return new Point(Position.X, Position.Y); }
        }

        public Point BottomCenter
        {
            get { return new Point(Position.X + (DesiredSize.Width / 2), Position.Y + DesiredSize.Height); }
        }

        public Point LeftCenter
        {
            get { return new Point(Position.X, Position.Y + (DesiredSize.Height / 2)); }
        }

        public Point RightCenter
        {
            get { return new Point(Position.X + DesiredSize.Width, Position.Y + (DesiredSize.Height / 2)); }
        }

        public double Left
        {
            get { return Position.X; }
            set { Position = new Point(value, Position.Y); }
        }

        public double Top
        {
            get { return Position.Y; }
            set { Position = new Point(Position.X, value); }
        }

        protected event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void Attach(IEnumerable items)
        {
            StackPanel PART_Anchors = (StackPanel)GetTemplateChild("PART_Anchors");
            PART_Anchors.Children.Clear();
            _Anchors.Clear();

            foreach (IAnchor anchor in items)
            {
                AnchorItem item = anchor.Create(this);
                item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++Count; });
                PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                PART_Anchors.Children.Add(item);

                _Anchors.Add(item.Id, item);
            }
        }
    }
}
