﻿using System;
using System.Windows.Media;

namespace New.Move.Core
{
    public class RealDesc : IDesc
    {
        public string Name { get { return null; } }
        public int Id { get { return 0; } }
        public string Help { get { return null; } }
        public string Group { get { return null; } }

        public ImageSource Icon { get { return null; } }

        public Type ConstructType { get { return typeof(Real); } }

        RealSignal _Signal;

        public RealDesc(RealSignal signal)
        {
            _Signal = signal;
        }

        public INode Create(Database database)
        {
            return new Real(_Signal);
        }
    }
}