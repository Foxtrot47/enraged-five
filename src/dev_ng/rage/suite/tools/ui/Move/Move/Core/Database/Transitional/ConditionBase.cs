﻿using System;
using System.Runtime.Serialization;
using System.Xml;
using Move.Core.Restorable;

namespace New.Move.Core
{
    /// <summary>
    /// Provides a base class for a condition that can be placed upon a transition.
    /// </summary>
    public abstract class ConditionBase : RestorableObject, ICondition
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        protected const string NameSerialisationTag = "Name";

        /// <summary>
        /// The private field for the <see cref="Name"/> property.
        /// </summary>
        private string name;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ConditionBase"/> class.
        /// </summary>
        public ConditionBase()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ConditionBase"/> class
        /// with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the condition.
        /// </param>
        public ConditionBase(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ConditionBase"/> class
        /// as a copy of the specified instance.
        /// </summary>
        /// <param name="name">
        /// The instance to copy.
        /// </param>
        public ConditionBase(ConditionBase other)
        {
            this.name = other.name;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ConditionBase"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public ConditionBase(SerializationInfo info, StreamingContext context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case NameSerialisationTag:
                        this.name = (string)entry.Value;
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the name this message can be referenced by.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets the type descriptor that describes the condition.
        /// </summary>
        public abstract Type Descriptor { get; }

        /// <summary>
        /// Gets the xml export type name for this condition.
        /// </summary>
        public abstract string XMLExportTypeName { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        public abstract void ExportToXML(PargenXmlNode parentNode);

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        public abstract ICondition Clone(Condition parent);

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public abstract void Serialise(XmlWriter writer);

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialize this instance.
        /// </summary>
        /// <param name="info">
        /// The serialization info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        protected void GetSerializationData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerialisationTag, Name);
        }
        #endregion
    } // New.Move.Core.ConditionBase {Class}
} // New.Move.Core {Namespace}
