﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

using Rage.Move.Core;
using Rage.Move.Core.Dag;
using Rage.Move.Core.Dg;
using Rage.Move.Utils;

using Condition = Rage.Move.Core.Dg.ConditionBase;

namespace Rage.Move
{
    public class FilterItem : UserControl
    {
        public FilterItem()
        {
            Loaded += new RoutedEventHandler(FilterItem_Loaded);
        }

        public Slider Value
        {
            get { return (Slider)GetTemplateChild("PART_Slider"); }
        }

        void FilterItem_Loaded(object sender, RoutedEventArgs e)
        {
            Button PART_Button = (Button)GetTemplateChild("PART_Button");
            PART_Button.Click += new RoutedEventHandler(Button_Click);
            
            Slider PART_Slider = (Slider)GetTemplateChild("PART_Slider");
            PART_Slider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(Slider_ValueChanged);
            PART_Slider.Visibility = Visibility.Hidden;

            PART_Button.Background = CalculateColor(PART_Slider.Value);
        }

        void Button_Click(object sender, RoutedEventArgs e)
        {
            Slider PART_Slider = (Slider)GetTemplateChild("PART_Slider");
            if (PART_Slider.Visibility == Visibility.Hidden)
            {
                Canvas.SetZIndex(this, 99);
                PART_Slider.Visibility = Visibility.Visible;
            }
            else
            {
                Canvas.SetZIndex(this, 0);
                PART_Slider.Visibility = Visibility.Hidden;
            }
        }

        void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider PART_Slider = (Slider)GetTemplateChild("PART_Slider");
            Button PART_Button = (Button)GetTemplateChild("PART_Button");

            PART_Button.Background = CalculateColor(PART_Slider.Value);
        }

        Brush CalculateColor(double value)
        {
            Color color = new Color();
            color.R = 0;
            color.G = (byte)(255.0f * value);
            color.B = 0;
            color.A = 255;

            return new SolidColorBrush(color);
        }
    }

    public class ConditionItem : UserControl 
    {
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(ConditionItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public ConditionItem(string name, FrameworkElement parent)
        {
            Template = (ControlTemplate)parent.FindResource("ConditionItemTemplate");
            DataContext = this;
            Label = name;
        }

        public StackPanel Properties { get; private set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Properties = (StackPanel)GetTemplateChild("PART_Properties");
        }
    }

    public abstract class AttributeItem : UserControl
    {
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(AttributeItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public AttributeItem(string name)
        {
            DataContext = this;
            Label = name;
        }
    }

    public abstract class PropertyItem : UserControl
    {
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(PropertyItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public PropertyItem(string name)
        {
            DataContext = this;
            Label = name;
        }
    }
    /*
    public class PropertyItem_AnimationAsAnchor : PropertyItem
    {
        public PropertyItem_AnimationAsAnchor(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_AnimationAsAnchor_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup = (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            selection.Content = ((ListBoxItem)type.SelectedItem).Content;
            System.Windows.Controls.Primitives.Popup popup = (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }

        void Value_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Anchor Value");
        }

        void Id_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Anchor Id");
        }

        void Signal_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Anchor Signal");
        }
    }
     */

    public class PropertyItem_Animation : PropertyItem
    {
        public PropertyItem_Animation(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_Animation_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup = (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );

            //Source = Rage.Move.Core.Dg.Properties.AnimationDataType.Filename;
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.AnimationDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup = (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }
        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.AnimationDataType), typeof(PropertyItem_Animation));

        public Rage.Move.Core.Dg.Properties.AnimationDataType Source 
        { 
            get { return (Rage.Move.Core.Dg.Properties.AnimationDataType)GetValue(SourceProperty); } 
            set { SetValue(SourceProperty, value); } 
        }
         */
    }

    public class BoolAttributeItem : AttributeItem
    {
        public BoolAttributeItem(string name, FrameworkElement parent)
            : base(name) 
        {
            Template = (ControlTemplate)parent.FindResource("BoolAttributeItemTemplate");
        }

        public CheckBox Value
        {
            get { return (CheckBox)GetTemplateChild("PART_Value"); }
        }
    }

    public class PropertyItem_Boolean : PropertyItem
    {
        public PropertyItem_Boolean(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_Boolean_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup = (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );
        }

        public CheckBox Value
        {
            get { return (CheckBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.BooleanDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup = (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }
        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.BooleanDataType), typeof(PropertyItem_Boolean));

        public Rage.Move.Core.Dg.Properties.BooleanDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.BooleanDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class PropertyItem_Clip : PropertyItem
    {
        public PropertyItem_Clip(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_Clip_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup =
                        (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.ClipDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup =
                (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.ClipDataType), typeof(PropertyItem_Clip));

        public Rage.Move.Core.Dg.Properties.ClipDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.ClipDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }


    public class ControlSignalAttributeItem : AttributeItem
    {
        public ControlSignalAttributeItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("ControlSignalAttributeItemTemplate");
        }

        public ComboBox Value
        {
            get { return (ComboBox)GetTemplateChild("PART_Value"); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            New.Move.Core.Database database = Runtime.Database;

            Value.ItemsSource = database.Signals;
            Value.DisplayMemberPath = "Name";
        }
    }

    public class MessageAttributeItem : AttributeItem
    {
        public MessageAttributeItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("MessageAttributeItemTemplate");
        }

        public ComboBox Value
        {
            get { return (ComboBox)GetTemplateChild("PART_Value"); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            New.Move.Core.Database database = Runtime.Database;

            Value.ItemsSource = database.Messages;
            Value.DisplayMemberPath = "Name";
        }
    }


    public class IntAttributeItem : AttributeItem
    {
        public IntAttributeItem(string name, FrameworkElement parent)
            : base(name) 
        {
            Template = (ControlTemplate)parent.FindResource("IntAttributeItemTemplate");
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }
    }

    public class FilterAttributeItem : AttributeItem
    {
        public FilterAttributeItem(string name, FrameworkElement parent)
            : base(name) 
        {
            Template = (ControlTemplate)parent.FindResource("FilterAttributeItemTemplate");
        }

        public void SetBinding(object source, string item)
        {
            FilterItem part = (FilterItem)GetTemplateChild("PART_" + item);
            
            Binding binding = new Binding(item);
            binding.Source = source;
            part.Value.SetBinding(Slider.ValueProperty, binding);
        }
    }

    public class PropertyItem_Expressions : PropertyItem
    {
        public PropertyItem_Expressions(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_Expressions_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup =
                        (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.ExpressionsDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup =
                (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.ExpressionsDataType), typeof(PropertyItem_Expressions));

        public Rage.Move.Core.Dg.Properties.ExpressionsDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.ExpressionsDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class PropertyItem_Filter : PropertyItem
    {
        public PropertyItem_Filter(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_Filter_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup = (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.FilterDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup =
                (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }
        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.FilterDataType), typeof(PropertyItem_Filter));

        public Rage.Move.Core.Dg.Properties.FilterDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.FilterDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class PropertyItem_Frame : PropertyItem
    {
        public PropertyItem_Frame(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_Frame_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup =
                        (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.FrameDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup =
                (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }
        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.FrameDataType), typeof(PropertyItem_Frame));

        public Rage.Move.Core.Dg.Properties.FrameDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.FrameDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class FloatAttributeItem : AttributeItem
    {
        public FloatAttributeItem(string name, FrameworkElement parent)
            : base(name) 
        {
            Template = (ControlTemplate)parent.FindResource("FloatAttributeItemTemplate");
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }
    }

    public class PropertyItem_ParameterizedMotion : PropertyItem
    {
        public PropertyItem_ParameterizedMotion(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_ParameterizedMotion_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup =
                        (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.ParameterizedMotionDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup =
                (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }
        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.ParameterizedMotionDataType), typeof(PropertyItem_ParameterizedMotion));

        public Rage.Move.Core.Dg.Properties.ParameterizedMotionDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.ParameterizedMotionDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class PropertyItem_Real : PropertyItem
    {
        public PropertyItem_Real(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_Real_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup = (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);
                }
            );
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.RealDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup = 
                (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }
        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.RealDataType), typeof(PropertyItem_Real));

        public Rage.Move.Core.Dg.Properties.RealDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.RealDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class PropertyItem_RealArray : PropertyItem
    {
        public PropertyItem_RealArray(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_RealArray_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button create = (Button)GetTemplateChild("PART_CreateButton");
                    create.Click += new RoutedEventHandler(CreateButton_Click);
                }
            );
        }

        public event RoutedEventHandler CreateClicked;
        void OnCreateClicked()
        {
            if (CreateClicked != null)
                CreateClicked(this, new RoutedEventArgs());
        }

        void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            OnCreateClicked();
        }

        StackPanel _Children = null;
        public UIElementCollection Children { get { return _Children.Children; }}

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _Children = (StackPanel)GetTemplateChild("PART_Children");
        }
    }

    public class PropertyItem_RealArrayItem : PropertyItem
    {
        public PropertyItem_RealArrayItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_RealArrayItem_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup =
                        (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);

                    Button remove = (Button)GetTemplateChild("PART_RemoveButton");
                    remove.Click += new RoutedEventHandler(RemoveButton_Click);
                }
            );
        }

        public event RoutedEventHandler RemoveClicked;
        void OnRemoveClicked()
        {
            if (RemoveClicked != null)
                RemoveClicked(this, new RoutedEventArgs());
        }

        void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            OnRemoveClicked();
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            //Source = (Rage.Move.Core.Dg.Properties.RealDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup =
                (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.RealDataType), typeof(PropertyItem_RealArrayItem));

        public Rage.Move.Core.Dg.Properties.RealDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.RealDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class PropertyItem_WeightArray : PropertyItem
    {
        public PropertyItem_WeightArray(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_WeightArray_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button create = (Button)GetTemplateChild("PART_CreateButton");
                    create.Click += new RoutedEventHandler(CreateButton_Click);
                }
            );
        }

        public event RoutedEventHandler CreateClicked;
        void OnCreateClicked()
        {
            if (CreateClicked != null)
                CreateClicked(this, new RoutedEventArgs());
        }

        void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            OnCreateClicked();
        }

        StackPanel _Children = null;
        public UIElementCollection Children { get { return _Children.Children; } }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _Children = (StackPanel)GetTemplateChild("PART_Children");
        }
    }

    public class PropertyItem_SourceArray : PropertyItem
    {
        public PropertyItem_SourceArray(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_SourceArray_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button create = (Button)GetTemplateChild("PART_CreateButton");
                    create.Click += new RoutedEventHandler(CreateButton_Click);
                }
            );
        }

        public event RoutedEventHandler CreateClicked;
        void OnCreateClicked()
        {
            if (CreateClicked != null)
                CreateClicked(this, new RoutedEventArgs());
        }

        void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            OnCreateClicked();
        }

        StackPanel _Children = null;
        public UIElementCollection Children { get { return _Children.Children; } }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _Children = (StackPanel)GetTemplateChild("PART_Children");
        }
    }

    public class PropertyItem_SourceArrayItem : PropertyItem
    {
        public PropertyItem_SourceArrayItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_SourceArrayItem_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button remove = (Button)GetTemplateChild("PART_RemoveButton");
                    remove.Click += new RoutedEventHandler(RemoveButton_Click);
                }
            );
        }

        public event RoutedEventHandler RemoveClicked;
        void OnRemoveClicked()
        {
            if (RemoveClicked != null)
                RemoveClicked(this, new RoutedEventArgs());
        }

        void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            OnRemoveClicked();
        }
    }

    /*
    public class PropertyItem_WeightArrayItem : PropertyItem
    {
        public PropertyItem_WeightArrayItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("PropertyItem_WeightArrayItem_Template");
            Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Button selection = (Button)GetTemplateChild("Selection");
                    System.Windows.Controls.Primitives.Popup popup =
                        (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
                    selection.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs ea)
                        {
                            popup.IsOpen = true;
                        }
                    );

                    ListBox type = (ListBox)GetTemplateChild("Type");
                    type.SelectionChanged += new SelectionChangedEventHandler(Type_SelectionChanged);

                    Button remove = (Button)GetTemplateChild("PART_RemoveButton");
                    remove.Click += new RoutedEventHandler(RemoveButton_Click);
                }
            );
        }

        public event RoutedEventHandler RemoveClicked;
        void OnRemoveClicked()
        {
            if (RemoveClicked != null)
                RemoveClicked(this, new RoutedEventArgs());
        }

        void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            OnRemoveClicked();
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }

        void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button selection = (Button)GetTemplateChild("Selection");
            ListBox type = (ListBox)GetTemplateChild("Type");
            Source = (Rage.Move.Core.Dg.Properties.WeightDataType)type.SelectedItem;
            System.Windows.Controls.Primitives.Popup popup =
                (System.Windows.Controls.Primitives.Popup)GetTemplateChild("Popup1");
            popup.IsOpen = false;
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.WeightDataType), typeof(PropertyItem_WeightArrayItem));

        public Rage.Move.Core.Dg.Properties.WeightDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.WeightDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
    }
    */
    public class ModifierAttributeItem : AttributeItem
    {
        public ModifierAttributeItem(string name, FrameworkElement parent)
            : base(name) 
        {
            Template = (ControlTemplate)parent.FindResource("ModifierAttributeItemTemplate");
        }

        public ComboBox Value
        {
            get { return (ComboBox)GetTemplateChild("PART_Value"); }
        }
    }

    public class RequestAttributeItem : AttributeItem
    {
        public RequestAttributeItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("RequestAttributeItemTemplate");
        }

        public ComboBox Value
        {
            get { return (ComboBox)GetTemplateChild("PART_Value"); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Value.ItemsSource = Runtime.Database.Requests;
            Value.DisplayMemberPath = "Name";
        }
    }

    public class FlagAttributeItem : AttributeItem 
    {
        public FlagAttributeItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("FlagAttributeItemTemplate");
        }

        public ComboBox Value
        {
            get { return (ComboBox)GetTemplateChild("PART_Value"); }
        }

        public override void  OnApplyTemplate()
        {
 	         base.OnApplyTemplate();

            Value.ItemsSource = Runtime.Database.Flags;
            Value.DisplayMemberPath = "Name";
        }
    }

    public class SignalAttributeItem : AttributeItem
    {
        public SignalAttributeItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("SignalAttributeItemTemplate");
        }

        public ComboBox Value
        {
            get { return (ComboBox)GetTemplateChild("PART_Value"); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            New.Move.Core.ITransitional active = Runtime.Database.Active;
            /*
            if (active.Parent != null)
            {
                IEnumerable<SignalSource> signals = active.Parent.Anchors.OfType<SignalSource>();
                if (signals.Count() != 0)
                {
                    Value.ItemsSource = signals;
                    Value.DisplayMemberPath = "Name";
                }
            }
            else
            if (active.LogicalParent != null)
            {
                IEnumerable<ISignal> signals = active.LogicalParent.Anchors.OfType<ISignal>();
                if (signals.Count() != 0)
                {
                    Value.ItemsSource = signals;
                    Value.DisplayMemberPath = "Name";
                }
            }
             */
        }
    }

    public class StringAttributeItem : AttributeItem
    {
        public StringAttributeItem(string name, FrameworkElement parent)
            : base(name) 
        {
            Template = (ControlTemplate)parent.FindResource("StringAttributeItemTemplate");
        }

        public TextBox Value
        {
            get { return (TextBox)GetTemplateChild("PART_Value"); }
        }
    }

    public class MessageItem : AttributeItem
    {
        public MessageItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("MessageItemTemplate");
        }

        public CheckBox Value
        {
            get { return (CheckBox)GetTemplateChild("PART_Value"); }
        }
    }

    public static class Attribute
    {
        public static int CreateProperties(this LogicNode source, Attributes parent)
        {
            int count = 0;
            System.Reflection.PropertyInfo[] properties = source.GetType().GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                if (property.PropertyType.GetInterface("Property") != null)
                {
                    Rage.Move.Core.Dg.Properties.Property prop = (Rage.Move.Core.Dg.Properties.Property)property.GetValue(source, null);
                    PropertyItem item = prop.Create(parent, property.Name);
                    parent.Properties.Children.Add(item);

                    ++count;
                }
            }

            return count;
        }

        public static PropertyItem Create(this Rage.Move.Core.Dg.Properties.Property source, FrameworkElement parent, string name)
        {
            /*
            if (source is Rage.Move.Core.Dg.Properties.Animation)
            {
                PropertyItem_Animation instance = new PropertyItem_Animation(name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Filename");
                        binding.Source = source;
                        instance.Value.SetBinding(TextBox.TextProperty, binding);

                        DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                            PropertyItem_Animation.SourceProperty, typeof(PropertyItem_Animation));
                        if (desc != null)
                        {
                            desc.AddValueChanged(instance, delegate
                            {
                                Rage.Move.Core.Dg.Properties.Animation property = 
                                    (Rage.Move.Core.Dg.Properties.Animation)source;
                                property.Source = instance.Source;
                            });
                        }

                        Binding bind = new Binding("Source");
                        bind.Source = source;
                        instance.SetBinding(PropertyItem_Animation.SourceProperty, bind);
                    }
                );

                return instance;
            }
            else

            if (source is Rage.Move.Core.Dg.Properties.Boolean)
            {
                PropertyItem_Boolean instance = new PropertyItem_Boolean(name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(CheckBox.IsCheckedProperty, binding);

                        DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                            PropertyItem_Boolean.SourceProperty, typeof(PropertyItem_Boolean));
                        if (desc != null)
                        {
                            desc.AddValueChanged(instance, delegate
                            {
                                Rage.Move.Core.Dg.Properties.Boolean property =
                                    (Rage.Move.Core.Dg.Properties.Boolean)source;
                                property.Source = instance.Source;
                            });
                        }

                        Binding bind = new Binding("Source");
                        bind.Source = source;
                        instance.SetBinding(PropertyItem_Boolean.SourceProperty, bind);
                    }
                );

                return instance;
            }
            else 
            
            if (source is Rage.Move.Core.Dg.Properties.Clip)
            {
                PropertyItem_Clip instance = new PropertyItem_Clip(name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Filename");
                        binding.Source = source;
                        instance.Value.SetBinding(TextBox.TextProperty, binding);

                        DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                            PropertyItem_Clip.SourceProperty, typeof(PropertyItem_Clip));
                        if (desc != null)
                        {
                            desc.AddValueChanged(instance, delegate
                            {
                                Rage.Move.Core.Dg.Properties.Clip property =
                                    (Rage.Move.Core.Dg.Properties.Clip)source;
                                property.Source = instance.Source;
                            });
                        }

                        Binding bind = new Binding("Source");
                        bind.Source = source;
                        instance.SetBinding(PropertyItem_Clip.SourceProperty, bind);
                    }
                );

                return instance;
            }
            else 
            if (source is Rage.Move.Core.Dg.Properties.Expressions)
            {
                PropertyItem_Expressions instance = new PropertyItem_Expressions(name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Filename");
                        binding.Source = source;
                        instance.Value.SetBinding(TextBox.TextProperty, binding);

                        DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                            PropertyItem_Expressions.SourceProperty, typeof(PropertyItem_Expressions));
                        if (desc != null)
                        {
                            desc.AddValueChanged(instance, delegate
                            {
                                Rage.Move.Core.Dg.Properties.Expressions property =
                                    (Rage.Move.Core.Dg.Properties.Expressions)source;
                                property.Source = instance.Source;
                            });
                        }

                        Binding bind = new Binding("Source");
                        bind.Source = source;
                        instance.SetBinding(PropertyItem_Expressions.SourceProperty, bind);
                    }
                );

                return instance;
            }
            else 
            if (source is Rage.Move.Core.Dg.Properties.Filter)
            {
                PropertyItem_Filter instance = new PropertyItem_Filter(name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        //Binding binding = new Binding("Value");
                        //binding.Source = source;
                        //instance.Value.SetBinding(,binding);

                        DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                            PropertyItem_Filter.SourceProperty, typeof(PropertyItem_Filter));
                        if (desc != null)
                        {
                            desc.AddValueChanged(instance, delegate
                            {
                                Rage.Move.Core.Dg.Properties.Filter property =
                                    (Rage.Move.Core.Dg.Properties.Filter)source;
                                property.Source = instance.Source;
                            });
                        }

                        Binding bind = new Binding("Source");
                        bind.Source = source;
                        instance.SetBinding(PropertyItem_Filter.SourceProperty, bind);
                    }
                );

                return instance;
            }
            else 
            if (source is Rage.Move.Core.Dg.Properties.Frame)
            {
                PropertyItem_Frame instance = new PropertyItem_Frame(name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        //Binding stuff...

                        DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                            PropertyItem_Frame.SourceProperty, typeof(PropertyItem_Frame));
                        if (desc != null)
                        {
                            desc.AddValueChanged(instance, delegate
                            {
                                Rage.Move.Core.Dg.Properties.Frame property =
                                    (Rage.Move.Core.Dg.Properties.Frame)source;
                                property.Source = instance.Source;
                            });
                        }

                        Binding bind = new Binding("Source");
                        bind.Source = source;
                        instance.SetBinding(PropertyItem_Frame.SourceProperty, bind);
                    }
                );

                return instance;
            }
             * */
            /*
        else if (source is Rage.Move.Core.Dg.Properties.ParameterizedMotion)
        {
            PropertyItem_ParameterizedMotion instance = new PropertyItem_ParameterizedMotion(name, parent);
            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Binding binding = new Binding("Filename");
                    binding.Source = source;
                    instance.Value.SetBinding(TextBox.TextProperty, binding);

                    DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                        PropertyItem_ParameterizedMotion.SourceProperty, typeof(PropertyItem_ParameterizedMotion));
                    if (desc != null)
                    {
                        desc.AddValueChanged(instance, delegate
                        {
                            Rage.Move.Core.Dg.Properties.ParameterizedMotion property =
                                (Rage.Move.Core.Dg.Properties.ParameterizedMotion)source;
                            property.Source = instance.Source;
                        });
                    }

                    Binding bind = new Binding("Source");
                    bind.Source = source;
                    instance.SetBinding(PropertyItem_ParameterizedMotion.SourceProperty, bind);
                }
            );

            return instance;
        }
                 
        else
        if (source is Rage.Move.Core.Dg.Properties.Real)
        {
            PropertyItem_Real instance = new PropertyItem_Real(name, parent);
            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    Binding binding = new Binding("Value");
                    binding.Source = source;
                    instance.Value.SetBinding(TextBox.TextProperty, binding);

                    DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                        PropertyItem_Real.SourceProperty, typeof(PropertyItem_Real));
                    if (desc != null)
                    {
                        desc.AddValueChanged(instance, delegate
                        {
                            Rage.Move.Core.Dg.Properties.Real property =
                                (Rage.Move.Core.Dg.Properties.Real)source;
                            property.Source = instance.Source;
                        });
                    }

                    Binding bind = new Binding("Source");
                    bind.Source = source;
                    instance.SetBinding(PropertyItem_Real.SourceProperty, bind);
                }
            );

            return instance;
        }
        else if (source is Rage.Move.Core.Dg.Properties.RealArray)
        {
            PropertyItem_RealArray instance = new PropertyItem_RealArray(name, parent);
            Rage.Move.Core.Dg.Properties.RealArray array =
                (Rage.Move.Core.Dg.Properties.RealArray)source;
                
            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    foreach (Rage.Move.Core.Dg.Properties.Real real in array.Items)
                    {
                        Rage.Move.Core.Dg.Properties.Real value = real;
                        PropertyItem_RealArrayItem item = new PropertyItem_RealArrayItem("Real", parent);
                        item.Loaded += new RoutedEventHandler(
                            delegate(object obj, RoutedEventArgs rea)
                            {
                                Binding binding = new Binding("Value");
                                binding.Source = value;
                                item.Value.SetBinding(TextBox.TextProperty, binding);

                                DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                                    PropertyItem_RealArrayItem.SourceProperty, typeof(PropertyItem_RealArrayItem));
                                if (desc != null)
                                {
                                    desc.AddValueChanged(item, delegate
                                    {
                                        Rage.Move.Core.Dg.Properties.Real property =
                                            (Rage.Move.Core.Dg.Properties.Real)value;
                                        property.Source = item.Source;
                                    });
                                }

                                Binding bind = new Binding("Source");
                                bind.Source = value;
                                item.SetBinding(PropertyItem_RealArrayItem.SourceProperty, bind);
                            }
                        );

                        item.RemoveClicked += new RoutedEventHandler(
                            delegate(object obj, RoutedEventArgs rea)
                            {
                                array.Items.Remove(value);
                                instance.Children.Remove(item);
                            }
                        );
                        instance.Children.Add(item);
                    }
                }
            );

            array.Items.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e)
                {
                    if (e.NewItems != null)
                    {
                        foreach (Rage.Move.Core.Dg.Properties.Real real in e.NewItems)
                        {
                            Rage.Move.Core.Dg.Properties.Real value = real;

                            PropertyItem_RealArrayItem item = new PropertyItem_RealArrayItem("Real", parent);
                            item.Loaded += new RoutedEventHandler(
                                delegate(object obj, RoutedEventArgs rea)
                                {
                                    Binding binding = new Binding("Value");
                                    binding.Source = value;
                                    item.Value.SetBinding(TextBox.TextProperty, binding);

                                    DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                                        PropertyItem_RealArrayItem.SourceProperty, typeof(PropertyItem_RealArrayItem));
                                    if (desc != null)
                                    {
                                        desc.AddValueChanged(item, delegate
                                        {
                                            Rage.Move.Core.Dg.Properties.Real property =
                                                (Rage.Move.Core.Dg.Properties.Real)value;
                                            property.Source = item.Source;
                                        });
                                    }

                                    Binding bind = new Binding("Source");
                                    bind.Source = value;
                                    item.SetBinding(PropertyItem_RealArrayItem.SourceProperty, bind);
                                }
                            );

                            item.RemoveClicked += new RoutedEventHandler(
                                delegate(object obj, RoutedEventArgs rea)
                                {
                                    array.Items.Remove(real);
                                    instance.Children.Remove(item);
                                }
                            );

                            instance.Children.Add(item);
                        }
                    }
                }
            );

            instance.CreateClicked += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    array.Items.Add(new Rage.Move.Core.Dg.Properties.Real());
                }
            );

            return instance;
        }
             * */
            if (source is Rage.Move.Core.Dg.Properties.SourceArray)
            {
                PropertyItem_SourceArray instance = new PropertyItem_SourceArray(name, parent);
                Rage.Move.Core.Dg.Properties.SourceArray array =
                    (Rage.Move.Core.Dg.Properties.SourceArray)source;

                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        foreach (SourceTransform transform in array.Items)
                        {
                            SourceTransform value = transform;
                            PropertyItem_SourceArrayItem item = new PropertyItem_SourceArrayItem("Input", parent);

                            item.RemoveClicked += new RoutedEventHandler(
                                delegate(object obj, RoutedEventArgs rea)
                                {
                                    array.Items.Remove(value);
                                    instance.Children.Remove(item);
                                }
                            );
                            instance.Children.Add(item);
                        }

                    }
                );

                array.Items.CollectionChanged += new NotifyCollectionChangedEventHandler(
                    delegate(object sender, NotifyCollectionChangedEventArgs e)
                    {
                        if (e.NewItems != null)
                        {
                            foreach (SourceTransform transform in e.NewItems)
                            {
                                SourceTransform value = transform;

                                PropertyItem_SourceArrayItem item = new PropertyItem_SourceArrayItem("Input", parent);

                                item.RemoveClicked += new RoutedEventHandler(
                                    delegate(object obj, RoutedEventArgs rea)
                                    {
                                        array.Items.Remove(transform);
                                        instance.Children.Remove(item);
                                    }
                                );
                                instance.Children.Add(item);
                            }
                        }
                    }
                );

                instance.CreateClicked += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        array.Items.Add(new SourceTransform(null));
                    }
                );

                return instance;
            }
                /*
            else if (source is Rage.Move.Core.Dg.Properties.WeightArray)
            {
                PropertyItem_WeightArray instance = new PropertyItem_WeightArray(name, parent);
                Rage.Move.Core.Dg.Properties.WeightArray array =
                    (Rage.Move.Core.Dg.Properties.WeightArray)source;

                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        foreach (Rage.Move.Core.Dg.Properties.Weight weight in array.Items)
                        {
                            Rage.Move.Core.Dg.Properties.Weight value = weight;
                            PropertyItem_RealArrayItem item = new PropertyItem_RealArrayItem("Weight", parent);
                            item.Loaded += new RoutedEventHandler(
                                delegate(object obj, RoutedEventArgs rea)
                                {
                                    Binding binding = new Binding("Value");
                                    binding.Source = value;
                                    item.Value.SetBinding(TextBox.TextProperty, binding);

                                    DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                                        PropertyItem_RealArrayItem.SourceProperty, typeof(PropertyItem_RealArrayItem));
                                    if (desc != null)
                                    {
                                        desc.AddValueChanged(item, delegate
                                        {
                                            Rage.Move.Core.Dg.Properties.Weight property =
                                                (Rage.Move.Core.Dg.Properties.Weight)value;
                                            property.Source = item.Source;
                                        });
                                    }

                                    Binding bind = new Binding("Source");
                                    bind.Source = value;
                                    item.SetBinding(PropertyItem_RealArrayItem.SourceProperty, bind);
                                }
                            );

                            item.RemoveClicked += new RoutedEventHandler(
                                delegate(object obj, RoutedEventArgs rea)
                                {
                                    array.Items.Remove(value);
                                    instance.Children.Remove(item);
                                }
                            );
                            instance.Children.Add(item);
                        }
                    }
                );

                array.Items.CollectionChanged += new NotifyCollectionChangedEventHandler(
                    delegate(object sender, NotifyCollectionChangedEventArgs e)
                    {
                        if (e.NewItems != null)
                        {
                            foreach (Rage.Move.Core.Dg.Properties.Weight weight in e.NewItems)
                            {
                                Rage.Move.Core.Dg.Properties.Weight value = weight;

                                PropertyItem_RealArrayItem item = new PropertyItem_RealArrayItem("Weight", parent);
                                item.Loaded += new RoutedEventHandler(
                                    delegate(object obj, RoutedEventArgs rea)
                                    {
                                        Binding binding = new Binding("Value");
                                        binding.Source = value;
                                        item.Value.SetBinding(TextBox.TextProperty, binding);

                                        DependencyPropertyDescriptor desc = DependencyPropertyDescriptor.FromProperty(
                                            PropertyItem_RealArrayItem.SourceProperty, typeof(PropertyItem_RealArrayItem));
                                        if (desc != null)
                                        {
                                            desc.AddValueChanged(item, delegate
                                            {
                                                Rage.Move.Core.Dg.Properties.Weight property =
                                                    (Rage.Move.Core.Dg.Properties.Weight)value;
                                                property.Source = item.Source;
                                            });
                                        }

                                        Binding bind = new Binding("Source");
                                        bind.Source = value;
                                        item.SetBinding(PropertyItem_RealArrayItem.SourceProperty, bind);
                                    }
                                );

                                item.RemoveClicked += new RoutedEventHandler(
                                    delegate(object obj, RoutedEventArgs rea)
                                    {
                                        array.Items.Remove(weight);
                                        instance.Children.Remove(item);
                                    }
                                );

                                instance.Children.Add(item);
                            }
                        }
                    }
                );

                instance.CreateClicked += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        array.Items.Add(new Rage.Move.Core.Dg.Properties.Weight());
                    }
                );

                return instance;
            }
                 */

            return null;
        }

        public static AttributeItem Create(this IAttribute source, FrameworkElement parent)
        {
            if (source is BoolAttribute)
            {
                BoolAttributeItem instance = new BoolAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(CheckBox.IsCheckedProperty, binding);
                    }
                );

                return instance;
            }
            else
            if (source is ControlSignalAttribute)
            {
                ControlSignalAttributeItem instance = new ControlSignalAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(ComboBox.SelectedItemProperty, binding);

                        IAnchor value = source.Value as IAnchor;
                        if (value != null)
                        {
                            int index = 0;
                            foreach (IAnchor item in instance.Value.ItemsSource.OfType<IAnchor>())
                            {
                                if (item.Id == value.Id)
                                {
                                    instance.Value.SelectedIndex = index;
                                    break;
                                }

                                ++index;
                            }
                        }
                    }
                );

                return instance;
            }
            else 
            if (source is MessageAttribute)
            {
                MessageAttributeItem instance = new MessageAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(ComboBox.SelectedItemProperty, binding);

                        Message value = source.Value as Message;
                        if (value != null)
                        {
                            int index = 0;
                            foreach (Message message in instance.Value.ItemsSource.OfType<Message>())
                            {
                                if (message.Id == value.Id)
                                {
                                    instance.Value.SelectedIndex = index;
                                    break;
                                }

                                ++index;
                            }
                        }
                    }
                );

                return instance;
            }
            else
            if (source is IntAttribute)
            {
                IntAttributeItem instance = new IntAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(TextBox.TextProperty, binding);
                    }
                );

                return instance;
            }
            else
            if (source is FilterAttribute)
            {
                FilterAttributeItem instance = new FilterAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        instance.SetBinding(source.Value, "Root");
                        instance.SetBinding(source.Value, "LeftFoot");
                        instance.SetBinding(source.Value, "RightFoot");
                        instance.SetBinding(source.Value, "LeftKnee");
                        instance.SetBinding(source.Value, "RightKnee");
                        instance.SetBinding(source.Value, "LeftHip");
                        instance.SetBinding(source.Value, "RightHip");
                        instance.SetBinding(source.Value, "Pelvis");
                        instance.SetBinding(source.Value, "Spine");
                        instance.SetBinding(source.Value, "Neck");
                        instance.SetBinding(source.Value, "Head");
                        instance.SetBinding(source.Value, "Face");
                        instance.SetBinding(source.Value, "LeftHand");
                        instance.SetBinding(source.Value, "RightHand");
                        instance.SetBinding(source.Value, "LeftElbow");
                        instance.SetBinding(source.Value, "RightElbow");
                        instance.SetBinding(source.Value, "LeftShoulder");
                        instance.SetBinding(source.Value, "RightShoulder");
                        instance.SetBinding(source.Value, "Mover");
                    }
                );
                
                return instance;
            }
            else
            if (source is FloatAttribute)
            {
                FloatAttributeItem instance = new FloatAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(TextBox.TextProperty, binding);
                    }
                );

                return instance;
            }
            else
            if (source is ModifierAttribute)
            {
                ModifierAttributeItem instance = new ModifierAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(ComboBox.SelectedItemProperty, binding);
                    }
                );

                return instance;
            }
            else
            if (source is RequestAttribute)
            {
                RequestAttributeItem instance = new RequestAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(ComboBox.SelectedItemProperty, binding);
                    }
                );

                return instance;
            }
            else 
            if (source is FlagAttribute)
            {
                FlagAttributeItem instance = new FlagAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(ComboBox.SelectedItemProperty, binding);
                    }
                );

                return instance;
            }
            else
            if (source is SignalAttribute)
            {
                SignalAttributeItem instance = new SignalAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(ComboBox.SelectedItemProperty, binding);
                    }
                );

                return instance;
            }
            else
            if (source is StringAttribute)
            {
                StringAttributeItem instance = new StringAttributeItem(source.Name, parent);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Binding binding = new Binding("Value");
                        binding.Source = source;
                        instance.Value.SetBinding(TextBox.TextProperty, binding);
                    }
                );

                return instance;
            }

            return null;
        }

        public static MessageItem Create(this IMessage source, FrameworkElement parent)
        {
            Message message = (Message)source;

            MessageItem item = new MessageItem(source.Name, parent);
            return item;
        }

        public static ConditionItem Create(this ICondition source, FrameworkElement parent)
        {
            Condition condition = (Condition)source;

            ConditionItem item = new ConditionItem(source.Name, parent);
            item.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    foreach (IAttribute attribute in condition.Attributes)
                        item.Properties.Children.Add(attribute.Create(parent));
                }
            );
            
            return item;
        }
    }

    public abstract class ControlItem : UserControl
    {
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(ControlItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public ControlItem(string name)
        {
            DataContext = this;
            Label = name;
        }
    }

    public class SignalControlItem : ControlItem
    {
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(float), typeof(SignalControlItem), new FrameworkPropertyMetadata(ValueProperty_Changed));

        public static readonly DependencyProperty MinProperty =
            DependencyProperty.Register("Min", typeof(float), typeof(SignalControlItem));

        public static readonly DependencyProperty MaxProperty =
            DependencyProperty.Register("Max", typeof(float), typeof(SignalControlItem));

        public SignalControlItem(string name, float min, float max, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("SignalControlItemTemplate");

            Min = min;
            Max = max;

            AllowUpdate = true;

            //Runtime.Instance.Link.RequestControlParameterValue(name, OnResponse);
        }

        public float Value 
        {
            get { return (float)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public float Min
        {
            get { return (float)GetValue(MinProperty); }
            set { SetValue(MinProperty, value); }
        }

        public float Max
        {
            get { return (float)GetValue(MaxProperty); }
            set { SetValue(MaxProperty, value); }
        }

        bool AllowUpdate { get; set; }

        void OnResponse(ByteBuffer buffer)
        {
            AllowUpdate = false;
            Value = buffer.ReadFloat();
            AllowUpdate = true;
        }

        static void ValueProperty_Changed(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            SignalControlItem item = (SignalControlItem)sender;
            //if (item.AllowUpdate)
              //  Runtime.Instance.Link.SendControlParameterValue(item.Label, item.Value);
        }
    }

    public class SwitchControlItem : ControlItem
    {
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(bool), typeof(SwitchControlItem), new FrameworkPropertyMetadata(ValueProperty_Changed));

        public SwitchControlItem(string name, FrameworkElement parent)
            : base(name)
        {
            Template = (ControlTemplate)parent.FindResource("SwitchControlItemTemplate");

            AllowUpdate = true;

            //Runtime.Instance.Link.RequestControlParameterValue(name, OnResponse);
        }

        public bool Value
        {
            get { return (bool)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        bool AllowUpdate { get; set; }

        void OnResponse(ByteBuffer buffer)
        {
            AllowUpdate = false;
            Value = (bool)TypeDescriptor.GetConverter(typeof(uint)).ConvertTo(buffer.ReadUInt32(), typeof(bool));
            AllowUpdate = true;
        }

        static void ValueProperty_Changed(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            SwitchControlItem item = (SwitchControlItem)sender;
            //if (item.AllowUpdate)
             //   Runtime.Instance.Link.SendControlParameterValue(item.Label, item.Value);
        }
    }

    public class RequestItem : UserControl
    {
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(RequestItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public RequestItem(string name, FrameworkElement parent)
        {
            DataContext = this;
            Label = name;

            Template = (ControlTemplate)parent.FindResource("RequestItemTemplate");
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }
    }

    public partial class Attributes : UserControl
    {
        public Attributes()
        {
            InitializeComponent();

            PropertiesBorder.Visibility = Visibility.Collapsed;
            MessagesBorder.Visibility = Visibility.Collapsed;
            ConditionsBorder.Visibility = Visibility.Collapsed;
            WeightsBorder.Visibility = Visibility.Collapsed;
            ControlBorder.Visibility = Visibility.Collapsed;
            RequestsBorder.Visibility = Visibility.Collapsed;

            Selection.Instance.SelectionChanging += new SelectionChangingEventHandler(Instance_SelectionChanging);
            Selection.Instance.SelectionChanged += new SelectionEventHandler(Instance_SelectionChanged);
            Selection.Instance.SelectionCleared += new SelectionClearedEventHandler(Instance_SelectionCleared);
        }

        public string Label { get; set; }

        public void Client_OnConnected(Client client)
        {
            /*
            Controls.Children.Clear();

            foreach (ControlSignal signal in Runtime.Database.ControlSignals)
            {
                if (signal is ControlSignalFloat)
                {
                    ControlSignalFloat csf = (ControlSignalFloat)signal;
                    Controls.Children.Add(new SignalControlItem(csf.Name, csf.Min, csf.Max, this));
                }
                else
                if (signal is ControlSignalBool)
                {
                    Controls.Children.Add(new SwitchControlItem(signal.Name, this));
                }
            }

            ControlBorder.Visibility = Visibility.Visible;

            Requests.Children.Clear();

            foreach (Request request in Runtime.Database.Requests)
                Requests.Children.Add(new RequestItem(request.Name, this));

            RequestsBorder.Visibility = Visibility.Visible;
             */
        }

        public void Client_OnDisconnected(Client client)
        {
            RequestsBorder.Visibility = Visibility.Collapsed;
            ControlBorder.Visibility = Visibility.Collapsed;
        }

        void Instance_SelectionChanging(List<ISelectable> selected)
        {
            New.Move.Core.ITransitional active = Runtime.Database.Active;

            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                //MotionTree_SelectionChanging(selected, active as New.Move.Core.Motiontree);
            }
            else if (Runtime.Database.Active is New.Move.Core.StateMachine)
            {
                //StateMachine_SelectionChanging(selected, active as New.Move.Core.StateMachine);
            }
        }

        public class Selected
        {
            public Selected() { }

            public TextChangedEventHandler TextChanged;
        }

        Selected _Selected { get; set; }

        void MotionTree_SelectionChanging(List<ISelectable> selected, New.Move.Core.Motiontree active)
        {
            if (_Selected != null && _Selected.TextChanged != null)
                PART_Name.TextChanged -= _Selected.TextChanged;
        }

        void StateMachine_SelectionChanging(List<ISelectable> selected, New.Move.Core.StateMachine active)
        {
            if (_Selected != null)
                PART_Name.TextChanged -= _Selected.TextChanged;

            foreach (Rage.Move.Core.Dg.Transition transition in active.Transitions)
            {
                try
                {
                    ISelectable selection = selected.Find(delegate(ISelectable s) { return s.Id == transition.Content.Id; });
                    if (selection != null)
                    {
                        transition.Conditions.CollectionChanged -= new NotifyCollectionChangedEventHandler(Transition_CollectionChanged);
                    }
                }
                catch (Exception) { }

            }
        }

        void Transition_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (ICondition condition in e.NewItems)
                Conditions.Children.Add(condition.Create(this));
        }

        LogicNodeBase GetSelectedLogic(ISelectable selection)
        {
            /*
            if (Runtime.Database.Active is MotionTree)
            {
                MotionTree active = (MotionTree)Runtime.Database.Active;
                foreach (LogicNodeBase child in active.Children)
                {
                    if (child.Id == selection.Id)
                        return child;
                }
            }
             */

            return null;
        }

        OpNode GetSelectedOperator(ISelectable selection)
        {
            /*
            if (Runtime.Database.Active is MotionTree)
            {
                MotionTree active = (MotionTree)Runtime.Database.Active;
                foreach (OpNode opnode in active.Operators)
                {
                    if (opnode.Id == selection.Id)
                        return opnode;
                }
            }
            */
            return null;
        }

        New.Move.Core.ITransitional GetSelectedTransitional(ISelectable selection)
        {
            if (Runtime.Database.Active is New.Move.Core.StateMachine)
            {
                New.Move.Core.ITransitional active = Runtime.Database.Active;
                foreach (New.Move.Core.ITransitional child in active.Children)
                {
                    if (child.Id == selection.Id)
                        return child;
                }
            }

            return null;
        }

        void Instance_SelectionChanged(ISelectable selection)
        {
            Properties.Children.Clear();
            PropertiesBorder.Visibility = Visibility.Collapsed;
            Messages.Children.Clear();
            MessagesBorder.Visibility = Visibility.Collapsed;
            ConditionsBorder.Visibility = Visibility.Collapsed;

            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                LogicNodeBase node = GetSelectedLogic(selection);

                if (node != null && node is LogicNode)
                {
                    LogicNode logic = (LogicNode)node;
                    PART_Name.IsEnabled = true;
                    PART_Name.Text = node.Name;

                    _Selected = new Selected();
                    _Selected.TextChanged = new TextChangedEventHandler(
                        delegate(object sender, TextChangedEventArgs e) { node.Name = PART_Name.Text; });

                    PART_Name.TextChanged += _Selected.TextChanged;

                    if (logic.Attributes.Count != 0)
                        PropertiesBorder.Visibility = Visibility.Visible;

                    if (logic.Messages.Count != 0)
                        MessagesBorder.Visibility = Visibility.Visible;

                    foreach (IAttribute attribute in logic.Attributes)
                        Properties.Children.Add(attribute.Create(this));

                    if (logic.CreateProperties(this) != 0)
                        PropertiesBorder.Visibility = Visibility.Visible;

                    foreach (IMessage message in logic.Messages)
                        Messages.Children.Add(message.Create(this));
                }
/*
                else 
                if (node != null && node is LogicStateNode)
                {
                    LogicStateNode state = (LogicStateNode)node;
                    PART_Name.IsEnabled = true;
                    PART_Name.Text = state.Name;

                    _Selected = new Selected();
                    _Selected.TextChanged = new TextChangedEventHandler(
                        delegate(object sender, TextChangedEventArgs e) { node.Name = PART_Name.Text; });

                    PART_Name.TextChanged += _Selected.TextChanged;
                }
 */
/*
                else 
                if (node != null && node is Reference)
                {
                    Reference reference = (Reference)node;
                    PART_Name.IsEnabled = true;
                    PART_Name.Text = reference.Name;

                    _Selected = new Selected();
                    _Selected.TextChanged = new TextChangedEventHandler(
                        delegate(object sender, TextChangedEventArgs e) { node.Name = PART_Name.Text; });

                    PART_Name.TextChanged += _Selected.TextChanged;
                }
 */
                else
                {
                    OpNode opnode = GetSelectedOperator(selection);
                    if (opnode != null)
                    {
                        PART_Name.IsEnabled = false;
                        PART_Name.Text = opnode.Name;

                        if (opnode.Attributes.Count != 0)
                            PropertiesBorder.Visibility = Visibility.Visible;

                        foreach (IAttribute attribute in opnode.Attributes)
                            Properties.Children.Add(attribute.Create(this));
                    }
                    else
                    {
                        PART_Name.Text = "<No Name>";
                        PART_Name.IsEnabled = false;
                    }
                }
               
                return;
            }

            if (Runtime.Database.Active is New.Move.Core.StateMachine)
            {
                New.Move.Core.ITransitional node = GetSelectedTransitional(selection);

                if (node != null)
                {
                    PART_Name.Text = node.Name;

                    _Selected = new Selected();
                    _Selected.TextChanged = new TextChangedEventHandler(
                        delegate(object sender, TextChangedEventArgs e) { node.Name = PART_Name.Text; });

                    PART_Name.TextChanged += _Selected.TextChanged;
                }
                else
                {
                    PART_Name.Text = "<No Name>";
                    PART_Name.IsEnabled = false;
                }
            }

            /*
            ConditionSelect selector = MainWindow.Instance.ConditionSelectControl;
            selector.Clear();

            if (Runtime.Database.Active is New.Move.Core.StateMachine)
            {
                New.Move.Core.StateMachine active = (New.Move.Core.StateMachine)Runtime.Database.Active;
                foreach (TransitionBase transition in active.Transitions)
                {
                    if (transition.Id == selection.Id)
                    {
                        selector.ConditionSelected += new ConditionSelectedEventHandler(
                            delegate(IRegisterCondition_Ex registered) { transition.Conditions.Add(registered.Create()); }
                        );

                        PropertiesBorder.Visibility = Visibility.Visible;
                        MessagesBorder.Visibility = Visibility.Visible;
                        ConditionsBorder.Visibility = Visibility.Visible;

                        transition.Conditions.CollectionChanged += new NotifyCollectionChangedEventHandler(Transition_CollectionChanged);

                        foreach (IAttribute attribute in transition.Attributes)
                            Properties.Children.Add(attribute.Create(this));

                        Conditions.Children.Clear();

                        foreach (ICondition condition in transition.Conditions)
                            Conditions.Children.Add(condition.Create(this));

                        return;
                    }
                }
            }
             */
        }

        void Instance_SelectionCleared()
        {
            PropertiesBorder.Visibility = Visibility.Collapsed;
            MessagesBorder.Visibility = Visibility.Collapsed;
            ConditionsBorder.Visibility = Visibility.Collapsed;

            if (_Selected != null)
                PART_Name.TextChanged -= _Selected.TextChanged;
            PART_Name.Text = "<No Name>";
            PART_Name.IsEnabled = false;

            Conditions.Children.Clear();
        }
    }
}
