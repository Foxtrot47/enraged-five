﻿using System;
using System.Collections.Generic;

namespace New.Move.Core
{
    internal class CIStringEqualityComparer : IEqualityComparer<string>
    {
        #region IEqualityComparer<string> Members

        public bool Equals(string x, string y)
        {
            return (x.ToLower() == y.ToLower());
        }

        public int GetHashCode(string obj)
        {
            return obj.ToLower().GetHashCode();
        }

        #endregion
    }
}
