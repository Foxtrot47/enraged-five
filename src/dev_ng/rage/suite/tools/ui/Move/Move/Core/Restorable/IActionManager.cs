﻿using System;

namespace Move.Core.Restorable
{
    internal interface IActionManager
    {
        void PushHandler(EventHandler<RestorablePropertyChangedEventArgs> handler);
        void PopHandler();

        void Push(IRestorableAction action);
    }
}