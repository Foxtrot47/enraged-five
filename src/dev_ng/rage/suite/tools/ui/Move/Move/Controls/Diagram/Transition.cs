﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

using Rage.Move.Utils;

namespace Rage.Move
{
    public class Transition : Control, INotifyPropertyChanged, ISelectable
    {
        public static readonly DependencyProperty SelectedProperty =
            DependencyProperty.Register("Selected", typeof(bool), typeof(Transition), new FrameworkPropertyMetadata(false));

        public bool Selected
        {
            get { return (bool)GetValue(SelectedProperty); }
            set { SetValue(SelectedProperty, value); }
        }

        public Transition(Guid id, DiagramState source, DiagramState sink)
        {
            Id = id;
            Source = source;
            Sink = sink;

            base.Unloaded += new RoutedEventHandler(Transition_Unloaded);
        }

        public Guid Id { get; private set; }
        Adorner Adorner { get; set; }
        AdornerLayer Layer { get; set; }

        PathGeometry _Geometry;
        public PathGeometry Geometry
        {
            get { return _Geometry; }
            set
            {
                if (_Geometry != value )
                {
                    _Geometry = value;
                    UpdatePosition();
                    OnPropertyChanged("Geometry");
                }
            }
        }

        Point _SourcePosition;
        public Point SourcePosition
        {
            get { return _SourcePosition; }
            set
            {
                if (_SourcePosition != value)
                {
                    _SourcePosition = value;
                    OnPropertyChanged("SourcePosition");
                }
            }
        }

        Point _SinkPosition;
        public Point SinkPosition
        {
            get { return _SinkPosition; }
            set
            {
                if (_SinkPosition != value)
                {
                    _SinkPosition = value;
                    OnPropertyChanged("SinkPosition");
                }
            }
        }

        double _SourceAngle;
        public double SourceAngle
        {
            get { return _SourceAngle; }
            set 
            {
                if (_SourceAngle != value)
                {
                    _SourceAngle = value;
                    OnPropertyChanged("SourceAngle");
                }
            }
        }

        double _SinkAngle;
        public double SinkAngle
        {
            get { return _SinkAngle; }
            set
            {
                if (_SinkAngle != value)
                {
                    _SinkAngle = value;
                    OnPropertyChanged("SinkAngle");
                }
            }
        }

        DiagramState _Source;
        public DiagramState Source
        {
            get { return _Source; }
            set
            {
                if (_Source != value)
                {
                    if (_Source != null)
                        _Source.PropertyChanged -= new PropertyChangedEventHandler(DiagramState_PropertyChanged);

                    _Source = value;

                    if (_Source != null)
                        _Source.PropertyChanged += new PropertyChangedEventHandler(DiagramState_PropertyChanged);

                    UpdateGeometry();
                }
            }
        }

        DiagramState _Sink;
        public DiagramState Sink
        {
            get { return _Sink; }
            set
            {
                if (_Sink != value)
                {
                    if (_Sink != null)
                        _Sink.PropertyChanged -= new PropertyChangedEventHandler(DiagramState_PropertyChanged);

                    _Sink = value;

                    if (_Sink != null)
                        _Sink.PropertyChanged += new PropertyChangedEventHandler(DiagramState_PropertyChanged);

                    UpdateGeometry();
                }
            }
        }

        void UpdateGeometry()
        {
            if (_Source != null && _Sink != null)
            {
                PathGeometry geometry = new PathGeometry();

                Point start = new Point(0, 0);

                Point source = _Source.BottomCenter;
                Point sink = _Sink.TopCenter;

                Point end = new Point(sink.X - source.X, sink.Y - source.Y);

                if (sink.Y < source.Y)
                {
                    double delta = source.Y - sink.Y;

                    start.Y += delta;
                    end.Y += delta;
                }

                if (sink.X < source.X)
                {
                    double delta = source.X - sink.X;

                    start.X += delta;
                    end.X += delta;
                }

                PathFigure figure = new PathFigure();
                figure.StartPoint = start;
                figure.Segments.Add(new PolyLineSegment(new PointCollection() { end }, true));
                geometry.Figures.Add(figure);

                Geometry = geometry;
            }
        }

        void UpdatePosition()
        {
            Point start, stangent;
            Geometry.GetPointAtFractionLength(0, out start, out stangent);

            Point end, etangent;
            Geometry.GetPointAtFractionLength(1, out end, out etangent);

            SourceAngle = Math.Atan2(-stangent.Y, -stangent.X) * (180 / Math.PI);
            SinkAngle = Math.Atan2(etangent.Y, etangent.X) * (180 / Math.PI);

            start.Offset(-stangent.X * 5, -stangent.Y * 5);
            end.Offset(etangent.X * 5, etangent.Y * 5);

            SourcePosition = start;
            SinkPosition = end;
        }

        void DiagramState_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Position"))
                UpdateGeometry();
        }

        void Transition_Unloaded(object sender, RoutedEventArgs e)
        {
            if (Adorner != null)
            {
                if (Layer != null)
                {
                    Layer.Remove(Adorner);
                    Adorner = null;
                }
            }
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
 	        base.OnPreviewMouseDown(e);

            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                if ((Keyboard.Modifiers & (ModifierKeys.Shift | ModifierKeys.Control)) != ModifierKeys.None)
                {
                    if (Selected)
                        diagram.Selection.Remove(this);
                    else
                        diagram.Selection.Add(this);
                }
                else
                if (!Selected)
                {
                    diagram.Selection.Select(this);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        Diagram GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }
    }

    // TransitionAdorner
}
