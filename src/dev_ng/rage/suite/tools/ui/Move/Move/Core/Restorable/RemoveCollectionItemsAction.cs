﻿using System.Collections;
using System.Collections.Specialized;

namespace Move.Core.Restorable
{
    internal sealed class RemoveCollectionItemsAction : IRestorableAction
    {
        public void Undo()
        {
            using (new WithoutCollectionChanged(List, CollectionChanged))
            {
                foreach (object arg in OldItems)
                {
                    List.Add(arg);
                }
            }
        }

        public void Redo()
        {
            using (new WithoutCollectionChanged(List, CollectionChanged))
            {
                foreach (object arg in OldItems)
                {
                    List.Remove(arg);
                }
            }
        }

        internal NotifyCollectionChangedEventHandler CollectionChanged { get; set; }
        public IList OldItems { get; set; }
        public int OldStartingIndex { get; set; }

        public IList List { get; set; }
    }
}