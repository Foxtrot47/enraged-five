﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Remoting.Services;
using System.Text;

namespace Rage.Move
{
    internal sealed class SetAction
    {
        internal string PropertyName { get; set; }
        internal object NewValue { get; set; }
        internal object OldValue { get; set; }
    }

    [Undoable]
    public abstract class Undoable : ContextBoundObject, IUndoable
    {
        public void Undo() { }
        public void Redo() { }
        public void Flush() { }
    }

    public sealed class UndoableProperty : IContextProperty, IContributeObjectSink
    {
        public IMessageSink GetObjectSink(MarshalByRefObject obj, IMessageSink next)
        {
            return new UndoableSink(obj, next);
        }

        public string Name { get { return "Undoable Property"; } }

        public bool IsNewContextOK(Context context)
        {
            return true;
        }

        public void Freeze(Context context)
        {
        }
    }

    internal sealed class UndoableAttribute : Attribute, IContextAttribute
    {
        public void GetPropertiesForNewContext(IConstructionCallMessage msg)
        {
            IContextProperty prop = new UndoableProperty();
            msg.ContextProperties.Add(prop);
        }

        public bool IsContextOK(Context context, IConstructionCallMessage msg)
        {
            if (context.GetProperty("Undoable Property") != null)
                return true;

            return false;
        }
    }

    internal sealed class UndoableSink : IMessageSink
    {
        IMessageSink _NextSink;
        object _Target;
        Type _TargetType;
        ArrayList _Actions;
        int _Index;
        int _Count;

        public IMessageSink NextSink { get { return _NextSink; } }

        private void Add(SetAction action)
        {
            if (_Actions.Count <= _Index)
                _Actions.Add(action);
            else 
                _Actions[_Index] = action;

            ++_Index;

            for (int i = _Index; i < _Actions.Count; ++i)
                _Actions[i] = null;
        }

        private bool CanUndo { get { return _Index > 0; } }
        public bool CanRedo { get { return _Count > 0 && _Actions.Count > _Index && _Actions[_Index] != null; } }

        public IMessage SyncProcessMessage(IMessage msg)
        {
            IMethodCallMessage mcm = msg as IMethodCallMessage;
            IMethodReturnMessage mrm = null;
            if (mcm != null)
            {
                if (mcm.MethodName.StartsWith("set_"))
                {
                    string propname = mcm.MethodName.Substring(4);
                    SetAction action = new SetAction();
                    action.PropertyName = propname;
                    action.NewValue = mcm.InArgs[0];

                    PropertyInfo pi = _TargetType.GetProperty(propname, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    action.OldValue = pi.GetValue(_Target, new object[0]);

                    Add(action);
                }

                if (mcm.MethodName == "Undo")
                {
                    if (CanUndo)
                    {
                        --_Count;
                        SetAction action = (SetAction)_Actions[_Index];
                        PropertyInfo pi = _TargetType.GetProperty(action.PropertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                        pi.SetValue(_Target, action.NewValue, new object[0]);
                        ++_Index;
                    }

                    mrm = new ReturnMessage(null, mcm);
                    return mrm;
                }

                if (mcm.MethodName == "Redo")
                {
                    if (CanRedo)
                    {
                        --_Count;
                        SetAction action = (SetAction)_Actions[_Index];
                        PropertyInfo pi = _TargetType.GetProperty(action.PropertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                        pi.SetValue(_Target, action.NewValue, new object[0]);
                        ++_Index; ; ;
                    }

                    mrm = new ReturnMessage(null, mcm);
                    return mrm;
                }
            }

            if (mcm.MethodName == "Flush")
            {
                _Actions.Clear();
                _Count = 0;
                _Index = 0;

                mrm = new ReturnMessage(null, mcm);
                return mrm;
            }

            return _NextSink.SyncProcessMessage(msg);
        }

        public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink reply)
        {
            return _NextSink.AsyncProcessMessage(msg, reply);
        }

        public UndoableSink(MarshalByRefObject target, IMessageSink next)
        {
            _Target = (Undoable)target;
            _TargetType = _Target.GetType();
            _NextSink = next;
            _Actions = new ArrayList();
            _Index = 0;
        }
    }

    internal interface IUndoable
    {
        void Undo();
        void Redo();
        void Flush();
    }
}