﻿using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Rockstar.MoVE.Services;

namespace New.Move.Core
{
    public class DatabaseReference : Database, ISerializable
    {
        public DatabaseReference(DatabaseXmlFactory xmlFactory)
            : base(xmlFactory)
        {
            RefCount = 0;
        }

        public DatabaseReference(ICoreDataModel dataModel, IDataModelService dataModelService, DatabaseXmlFactory xmlFactory)
            : base(dataModel, dataModelService, xmlFactory)
        {
            RefCount = 0;
        }

        public int RefCount { get; set; }

        new public void Load(string filename)
        {
            this.FullyQualifiedFilename = filename;
            Load();
        }

        new public void Load()
        {
            try
            {
                if (String.IsNullOrEmpty(FullyQualifiedFilename))
                    FullyQualifiedFilename = Database.DefaultFullyQualifiedFilename;

                BinaryFormatter formatter = new BinaryFormatter();

                SurrogateSelector selector = new SurrogateSelector();

                AddSurrogates(selector);

                formatter.SurrogateSelector = selector;
                formatter.SurrogateSelector.ChainSelector(new LogicSurrogateSelector(DataModel));
                formatter.SurrogateSelector.ChainSelector(new ConditionSurrogateSelector(DataModel));

                using (FileStream stream = new FileStream(FullyQualifiedFilename, FileMode.Open, FileAccess.Read))
                {
                    Database database = (Database)formatter.Deserialize(stream);
                    Signals = database.Signals;
                    Requests = database.Requests;
                    Flags = database.Flags;
                    Nodes = database.Nodes;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                string filename = FullyQualifiedFilename;
                FullyQualifiedFilename = String.Empty;
                //throw new LoadFailedException(String.Format("Loading {0} failed!", filename), e);
            }
        }

        static class SerializationTag
        {
            public static string Database = "Database";
            public static string RefCount = "RefCount";
        }
 
    }
}