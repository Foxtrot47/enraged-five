﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class ParameterizedMotionReference : ReferenceSignal, ISerializable
    {
        public ParameterizedMotionReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public ParameterizedMotionReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override Parameter GetParameter()
        {
            return Parameter.ParameterizedMotion;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}