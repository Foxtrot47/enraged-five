﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class Expression : SignalBase, ISerializable
    {
        public Expression(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public Expression(ExpressionSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Expression"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Expression(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "expressionsignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new Expression(new ExpressionSignal());
            (clone as Expression).AdditionalText = this.AdditionalText;
            (clone as Expression).Database = this.Database;
            (clone as Expression).Enabled = this.Enabled;
            (clone as Expression).Name = this.Name;
            (clone as Expression).Position = this.Position;
            (clone as Expression).Signal = this.Signal;

            return (ISelectable)clone;
        }
    }
}