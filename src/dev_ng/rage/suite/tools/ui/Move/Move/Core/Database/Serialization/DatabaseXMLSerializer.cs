﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

using Move.Core;
using Move.Utils;
using RSG.Base.Logging;

namespace New.Move.Core
{
    public class DatabaseXMLSerializer
    {
        public void Export(string pathname, Database db, string nodemapPathname)
        {
            PargenXmlNode rootNode = PargenXmlNode.CreateRootNode("1.0", "UTF-8", "Network");
            var hashedNames = new CIStringSet();
            db.ExportToXML(rootNode, hashedNames);
            rootNode.SaveDocument(pathname);

            PargenXmlNode nodemapRootNode = PargenXmlNode.CreateRootNode("1.0", "UTF-8", "NodeMap");
            var debugNamesNode = nodemapRootNode.AppendChild("debugNames");
            var orderedHashedNamesQuery = from name in hashedNames
                                          orderby name
                                          select name;
            foreach (string hashedName in orderedHashedNamesQuery)
            {
                debugNamesNode.AppendTextElementNode("names", hashedName);
            }
            try
            {
                nodemapRootNode.SaveDocument(nodemapPathname);
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
                LogFactory.ApplicationLog.Warning("Node map failed to export to {0} because of an exception", nodemapPathname);
                LogFactory.ApplicationLog.Message(e.ToString());
            }
        }
    }
}
