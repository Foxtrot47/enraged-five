﻿using System.Collections;
using System.Collections.Specialized;

namespace Move.Core.Restorable
{
    internal sealed class MoveCollectionItemsAction : IRestorableAction
    {
        public void Undo()
        {
            Move(NewStartingIndex, OldStartingIndex);
        }

        public void Redo()
        {
            Move(OldStartingIndex, NewStartingIndex);
        }

        void Move(int oldIndex, int newIndex)
        {
            using (new WithoutCollectionChanged(List, CollectionChanged))
            {
                foreach (object item in Items)
                {
                    object temp = List[oldIndex];

                    List.RemoveAt(oldIndex);
                    List.Insert(newIndex, temp);
                }
            }
        }

        internal NotifyCollectionChangedEventHandler CollectionChanged { get; set; }
        public IList Items { get; set; }
        public int NewStartingIndex { get; set; }
        public int OldStartingIndex { get; set; }

        public IList List { get; set; }
    }
}