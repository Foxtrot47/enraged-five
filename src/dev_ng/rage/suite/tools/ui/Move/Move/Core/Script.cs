﻿using System;
using System.IO;
using IronPython.Runtime;
using IronPython.Runtime.Calls;
using IronPython.Runtime.Types;
using IronPython.Runtime.Operations;
using IronPython.Hosting;
using IronPython.Modules;
using Microsoft.Scripting;

namespace Rage.Move.Core
{
    public class ScriptEngine
    {
        private Microsoft.Scripting.Hosting.ScriptEngine engine = null;
        private Microsoft.Scripting.Hosting.ScriptScope scope = null;

        public ScriptEngine()
        {
            engine = Microsoft.Scripting.Hosting.ScriptRuntime.Create().GetEngine("py");
            scope = engine.CreateScope();

            ((IronPython.PythonEngineOptions)engine.Options).DivisionOptions =
                IronPython.PythonDivisionOptions.New;

            Microsoft.Scripting.Hosting.ScriptSource api =
                engine.CreateScriptSourceFromFile("./Scripts/Startup.py");

            try
            {
                api.Execute(scope);
            }
            catch (Microsoft.Scripting.SyntaxErrorException e)
            {
                Console.WriteLine(e.Line + " : " + e.Column + " : " + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            // nodes
            Enumerate("./Scripts/Manifest/Nodes");

            // transitions
            Enumerate("./Scripts/Manifest/Transitions");

            // conditions
            Enumerate("./Scripts/Manifest/Conditions");
        }

        public void Execute(string code)
        {
            Microsoft.Scripting.Hosting.ScriptSource source = engine.CreateScriptSourceFromString(code,
                SourceCodeKind.Statements);

            try
            {
                source.Execute(scope);
            }
            catch (Microsoft.Scripting.SyntaxErrorException e)
            {
                Console.WriteLine(e.Line + " : " + e.Column + " : " + e.Message);
            }
            catch (InvalidProgramException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        void Enumerate(string path)
        {
            DirectoryInfo info = new DirectoryInfo(path);
            FileInfo[] files = info.GetFiles("*.py");
            foreach (FileInfo file in files)
            {
                Microsoft.Scripting.Hosting.ScriptSource source = 
                    engine.CreateScriptSourceFromFile(Path.Combine(file.DirectoryName, file.Name));

                try
                {
                    source.Execute(scope);
                }
                catch (Microsoft.Scripting.SyntaxErrorException e)
                {
                    Console.WriteLine(e.Line + " : " + e.Column + " : " + e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
