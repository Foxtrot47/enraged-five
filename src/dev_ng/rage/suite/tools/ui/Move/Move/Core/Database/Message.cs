﻿using System;
using System.Runtime.Serialization;
using Move.Core.Restorable;

namespace New.Move.Core
{
    /// <summary>
    /// Represents a single message in a MoVE network.
    /// </summary>
    [Serializable]
    public class Message : RestorableObject, ISerializable
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        private const string NameSerialisationTag = @"Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        private const string IdSerialisationTag = @"Id";
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Message"/> class with
        /// the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the message.
        /// </param>
        public Message(string name)
        {
            this.name = name;
            this.id = Guid.NewGuid();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Message"/> class as a
        /// copy of the specified instance.
        /// </summary>
        /// <param name="name">
        /// The instance to copy.
        /// </param>
        public Message(Message other)
        {
            this.name = other.name;
            this.id = Guid.NewGuid();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Message"/> class with
        /// the specified name and identifier.
        /// </summary>
        /// <param name="name">
        /// The name of the message.
        /// </param>
        /// <param name="id">
        /// The identifier this message will have.
        /// </param>
        public Message(Guid id, string name)
        {
            this.name = name;
            this.id = id;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Message"/> class using
        /// the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public Message(SerializationInfo info, StreamingContext context)
        {
            this.name = (string)info.GetValue(NameSerialisationTag, typeof(string));
            this.id = (Guid)info.GetValue(IdSerialisationTag, typeof(Guid));
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the name this message can be referenced by.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the unique global identifier for this message.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Create a new flag reference, that references a flag that is equal to this.
        /// </summary>
        /// <returns>
        /// A new flag reference that references this flag.
        /// </returns>
        public ReferenceFlag CreateReference()
        {
            return new ReferenceFlag(this.id, this.name);
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialize this instance.
        /// </summary>
        /// <param name="info">
        /// The serialization info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerialisationTag, Name);
            info.AddValue(IdSerialisationTag, Id);
        }
        #endregion
    }
}