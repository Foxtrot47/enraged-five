﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;

using Rage.Move.Core.Dag;
using Rage.Move.Utils.Serialization;

namespace Rage.Move.Core.Dg
{
    public interface ISourceReference
    {
        Guid Reference { get; set; }
    }

    public interface IResultReference
    {
        Guid Reference { get; set; }
    }

    [Serializable]
    public class SignalSourceReference : SignalSource, ISourceReference
    {
        public SignalSourceReference(ControlSignalFloat source)
            : base(source.Name) 
        {
            Reference = source.Id;
        }

        public SignalSourceReference(ControlSignalBool source)
            : base(source.Name)
        {
            Reference = source.Id;
        }

        public SignalSourceReference()
            : base(null) { }

        [XmlAttribute]
        public Guid Reference { get; set; }
    }

    [Serializable]
    public class AnimationSourceReference : AnimationSource, ISourceReference
    {
        public AnimationSourceReference(ControlSignalAnimation source)
            : base(source.Name)
        {
            Reference = source.Id;
        }

        public AnimationSourceReference()
            : base(null) { }

        [XmlAttribute]
        public Guid Reference { get; set; }
    }

    [Serializable]
    public class ClipSourceReference : ClipSource, ISourceReference
    {
        public ClipSourceReference(ControlSignalClip source)
            : base(source.Name)
        {
            Reference = source.Id;
        }

        public ClipSourceReference()
            : base(null) { }

        [XmlAttribute]
        public Guid Reference { get; set; }
    }

    [Serializable]
    public class ExpressionSourceReference : ExpressionSource, ISourceReference
    {
        public ExpressionSourceReference(ControlSignalExpression source)
            : base(source.Name)
        {
            Reference = source.Id;
        }

        public ExpressionSourceReference()
            : base(null) { }

        [XmlAttribute]
        public Guid Reference { get; set; }
    }

    [Serializable]
    public class FilterSourceReference : FilterSource, ISourceReference
    {
        public FilterSourceReference(ControlSignalFilter source)
            : base(source.Name)
        {
            Reference = source.Id;
        }

        public FilterSourceReference()
            : base(null) { }

        [XmlAttribute]
        public Guid Reference { get; set; }
    }

    [Serializable]
    public class FrameSourceReference : FrameSource, ISourceReference
    {
        public FrameSourceReference(ControlSignalFrame source)
            : base(source.Name)
        {
            Reference = source.Id;
        }

        public FrameSourceReference()
            : base(null) { }

        [XmlAttribute]
        public Guid Reference { get; set; }
    }

    [Serializable]
    public class ParameterizedMotionSourceReference : ParameterizedMotionSource, ISourceReference
    {
        public ParameterizedMotionSourceReference(ControlSignalParameterizedMotion source)
            : base(source.Name)
        {
            Reference = source.Id;
        }

        public ParameterizedMotionSourceReference()
            : base(null) { }

        [XmlAttribute]
        public Guid Reference { get; set; }
    }

    [Serializable]
    public class TransformResultReference : TransformResult, IResultReference
    {
        public TransformResultReference(TransformResult result)
            : base(result.Name)
        {
            Reference = result.Id;
        }

        public TransformResultReference()
            : base(null) { }

        [XmlAttribute]
        public Guid Reference { get; set; }
    }

#if NO_REFERENCE
    [Serializable]
    public class Reference : LogicNodeBase
    {
        public Reference(string filename)
            : base("Reference")
        {
            Load(filename);

            foreach (ControlSignal signal in Database.ControlSignals)
            {
                if (signal is ControlSignalFloat)
                    Anchors.Add(new SignalSourceReference(signal as ControlSignalFloat));
                else 
                if (signal is ControlSignalBool)
                    Anchors.Add(new SignalSourceReference(signal as ControlSignalBool));
                else 
                if (signal is ControlSignalAnimation)
                    Anchors.Add(new AnimationSourceReference(signal as ControlSignalAnimation));
                else 
                if (signal is ControlSignalClip)
                    Anchors.Add(new ClipSourceReference(signal as ControlSignalClip));
                else 
                if (signal is ControlSignalExpression)
                    Anchors.Add(new ExpressionSourceReference(signal as ControlSignalExpression));
                else 
                if (signal is ControlSignalFilter)
                    Anchors.Add(new FilterSourceReference(signal as ControlSignalFilter));
                else
                if (signal is ControlSignalFrame)
                    Anchors.Add(new FrameSourceReference(signal as ControlSignalFrame));
                else 
                if (signal is ControlSignalParameterizedMotion)
                    Anchors.Add(new ParameterizedMotionSourceReference(signal as ControlSignalParameterizedMotion));
            }

            if (Database.Root is MotionTree)
            {
                IEnumerable<TransformResult> results =
                    (Database.Root as MotionTree).Anchors.OfType<TransformResult>();
                Anchors.Add(new TransformResultReference(results.First()));
            }
            else
            if (Database.Root is StateMachine)
            {
                IEnumerable<TransformResult> results =
                    (Database.Root as StateMachine).Anchors.OfType<TransformResult>();
                Anchors.Add(new TransformResultReference(results.First()));
            }

            Unid = Uniquifier16.Create();
        }

        public Reference()
            : base(null) { }

        [XmlTypeConverter(typeof(Uniquifier16Converter))]
        public Uniquifier16 Unid { get; set; }

        //[XmlIgnore]
        public override ObservableCollection<IAnchor> Anchors { get { return _Anchors; } set { _Anchors = value; } }

        //[XmlIgnore]
        //public Database Database { get; private set; }

        public string Filename 
        { 
            get { return Database.FullyQualifiedFilename; }
            set { Load(value); } 
        }

        void Load(string filename)
        {
            Database = new Database();
            Database.FullyQualifiedFilename = filename;

            Database.Load();
            /*
            foreach (ControlSignal signal in Database.ControlSignals)
            {
                AnchorSource source = null;

                if (signal is ControlSignalFloat)
                    source = new SignalSource(signal.Name);
                else 
                if (signal is ControlSignalBool)
                    source = new SignalSource(signal.Name);
                else 
                if (signal is ControlSignalAnimation)
                    source = new AnimationSource(signal.Name);
                else 
                if (signal is ControlSignalClip)
                    source = new ClipSource(signal.Name);
                else 
                if (signal is ControlSignalExpression)
                    source = new ExpressionSource(signal.Name);
                else 
                if (signal is ControlSignalFilter)
                    source = new FilterSource(signal.Name);
                else 
                if (signal is ControlSignalParameterizedMotion)
                    source = new ParameterizedMotionSource(signal.Name);

                // TODO: this is here so we can export, but it throws off connections if you have more than one reference!!!
                //source.Id = signal.Id;
                Anchors.Add(source);
            }
           

            TransformResult result = new TransformResult("Result");
            if (Database.Root is MotionTree)
            {
                IEnumerable<TransformResult> results = 
                    (Database.Root as MotionTree).Anchors.OfType<TransformResult>();
                //result.Id = results.First().Id;
            }
            else
            if (Database.Root is StateMachine)
            {
                IEnumerable<TransformResult> results =
                    (Database.Root as StateMachine).Anchors.OfType<TransformResult>();
                //result.Id = results.First().Id;
            }

            Anchors.Add(result);
              * */
            if (Database.Root is MotionTree)
                (Database.Root as MotionTree).Deactivate += new ActivatedEventHandler(
                    delegate() { Runtime.Database.Active = Parent; });
            else
            if (Database.Root is StateMachine)
                (Database.Root as StateMachine).Deactivate += new ActivatedEventHandler(
                    delegate() { Runtime.Database.Active = Parent; });

            Runtime.Instance.References.Add(Database.Id, this);
        }

        public void OnActivate()
        {
            Database.Root.OnActivate();
        }
    }
#endif
}