﻿using System;
using System.Collections;
using Move.Utils;

namespace New.Move.Core
{
    /// <summary>
    /// Represents a collection of <see cref="New.Move.Core.Transition"/> instances.
    /// </summary>
    [Serializable]
    public class TransitionCollection : NoResetObservableCollection<Transition>
    {
        #region Properties
        /// <summary>
        /// Gets a iterator around all the transitions in this collection.
        /// </summary>
        public IEnumerator Enumerator
        {
            get { return GetEnumerator(); }
        } 
        #endregion
    } // New.Move.Core.TransitionCollection
} // New.Move.Core
