﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Move.Utils;
using RSG.Base.Configuration;

namespace Move.Core
{
    internal class ClipArchiveReader
    {
        internal ClipArchiveReader()
        {
        }

        // Cache any data read from the zip to speed things up a little
        private static IEnumerable<string> _cachedClipDictionaryNames = null;
        private static Dictionary<string, IEnumerable<string>> _cachedClipNames = new Dictionary<string, IEnumerable<string>>();

        internal IEnumerable<string> ReadClipDictionaryNames()
        {
            if (_cachedClipDictionaryNames != null)
            {
                return _cachedClipDictionaryNames;
            }

            try
            {
                IConfig config = Move.Utils.ContentConfig.Config;
                IBranch branch = config.Project.DefaultBranch;
                List<string> directories = new List<string>();
                directories.Add(Path.Combine(branch.Export, "anim", "ingame"));
                foreach (IProject project in config.DLCProjects.Values)
                {
                    branch = project.DefaultBranch;
                    directories.Add(Path.Combine(branch.Export, "anim", "ingame"));
                }

                string pattern = "*.icd.zip";
                SearchOption option = SearchOption.AllDirectories;
                List<string> clipDictionaries = new List<string>();
                foreach (string directory in directories)
                {
                    if (!Directory.Exists(directory))
                    {
                        continue;
                    }

                    foreach (string file in Directory.GetFiles(directory, pattern, option))
                    {
                        string filename = Path.GetFileName(file);
                        clipDictionaries.Add(filename.Replace(".icd.zip", ""));
                    }
                }

                _cachedClipDictionaryNames = clipDictionaries;
                return _cachedClipDictionaryNames;
            }
            catch (Exception) 
            {
                return new string[] { };
            }
        }

        internal IEnumerable<string> ReadClipNamesFromDictionary(string dictionaryName)
        {
            if (_cachedClipNames.ContainsKey(dictionaryName))
            {
                return _cachedClipNames[dictionaryName];
            }

            try
            {
                IConfig config = Move.Utils.ContentConfig.Config;
                IBranch branch = config.Project.DefaultBranch;
                List<string> directories = new List<string>();
                directories.Add(Path.Combine(branch.Export, "anim", "ingame"));
                foreach (IProject project in config.DLCProjects.Values)
                {
                    branch = project.DefaultBranch;
                    directories.Add(Path.Combine(branch.Export, "anim", "ingame"));
                }

                string pattern = "*" + dictionaryName + ".icd.zip";
                string dictionaryFilename = dictionaryName + ".icd.zip";
                SearchOption option = SearchOption.AllDirectories;
                string clipDictionaryPath = null;
                foreach (string directory in directories)
                {
                    if (!Directory.Exists(directory))
                    {
                        continue;
                    }

                    foreach (string file in Directory.GetFiles(directory, pattern, option))
                    {
                        string filename = Path.GetFileName(file);
                        if (filename == dictionaryFilename)
                        {
                            clipDictionaryPath = file;
                            break;
                        }
                    }

                    if (clipDictionaryPath != null)
                    {
                        break;
                    }
                }

                if (clipDictionaryPath == null || !File.Exists(clipDictionaryPath))
                {
                    return new string[] { };
                }

                List<string> clips = new List<string>();
                using (var clipsArchive = Ionic.Zip.ZipFile.Read(clipDictionaryPath))
                {
                    foreach (var entry in clipsArchive.Entries)
                    {
                        if (entry.FileName.EndsWith(".clip"))
                        {
                            clips.Add(entry.FileName.Replace(".clip", "").ToLower());
                        }
                    }
                }

                _cachedClipNames.Add(dictionaryName, clips.ToArray());
                return _cachedClipNames[dictionaryName];
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }
    }
}
