﻿using System;

namespace Rage.Move.Utils
{
    public interface ISelectable
    {
        Guid Id { get; }
        bool Selected { get; set; }
    }
}
