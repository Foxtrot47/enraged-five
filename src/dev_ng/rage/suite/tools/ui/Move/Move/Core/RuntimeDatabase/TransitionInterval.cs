﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Move.Core
{
    public class TransitionInterval : Interval
    {
        public ushort TransId;

        public StateInterval StateFrom;
        public StateInterval StateTo;

        public TransitionInterval(ushort transId, TimeSpan startTime, TimeSpan endTime)
        {
            TransId = transId;
            StartTime = startTime;
            EndTime = endTime;
        }

        protected TransitionInterval()
        {
        }

        public static TransitionInterval CreateWithKnownStart(ushort transId, TimeSpan time)
        {
            TransitionInterval s = new TransitionInterval();
            s.TransId = transId;
            s.StartTime = time;
            return s;
        }

        public static TransitionInterval CreateWithKnownEnd(ushort transId, TimeSpan time)
        {
            TransitionInterval s = new TransitionInterval();
            s.TransId = transId;
            s.EndTime = time;
            return s;
        }

        public override string ToString()
        {
            return String.Format("#{1}: {2}", TransId, base.ToString());
        }
    }
}
