﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using Rage.Move.Core;
using Rage.Move.Core.Dg;

namespace Rage.Move
{
    public abstract class PaletteItem : UserControl
    {
        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(ImageSource), typeof(PaletteItem), new FrameworkPropertyMetadata(null));

        public ImageSource Icon
        {
            get { return (ImageSource)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public PaletteItem(FrameworkElement parent)
        {
            Template = Template = (ControlTemplate)parent.FindResource("PaletteItemTemplate");

            //_Id = id;
        }

        protected Point? _DragStart = null;
        //protected int _Id;

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);
            _DragStart = new Point?(e.GetPosition(this));
        }
    }

    public class PaletteTransitionalItem : PaletteItem
    {
        public PaletteTransitionalItem(New.Move.Core.ITransitionalDesc desc, FrameworkElement parent)
            : base(parent)  {
                Desc = desc;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton != MouseButtonState.Pressed)
                _DragStart = null;

            if (_DragStart.HasValue)
            {
                DragTransitionalObject drag = new DragTransitionalObject();
                drag.Desc = Desc;

                DragDrop.DoDragDrop(this, drag, DragDropEffects.Copy);

                e.Handled = true;
            }
        }

        private New.Move.Core.ITransitionalDesc Desc { get; set; }
    }

    public class PaletteNodeItem : PaletteItem
    {
        public PaletteNodeItem(New.Move.Core.ILogicDesc desc, FrameworkElement parent)
            : base(parent)  {
                Desc = desc;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton != MouseButtonState.Pressed)
                _DragStart = null;

            if (_DragStart.HasValue)
            {
                DragNodeObject drag = new DragNodeObject();
                drag.Desc = Desc;

                DragDrop.DoDragDrop(this, drag, DragDropEffects.Copy);

                e.Handled = true;
            }
        }

        private New.Move.Core.ILogicDesc Desc { get; set; }
    }

    public class PaletteOperatorItem : PaletteItem
    {
        public PaletteOperatorItem(FrameworkElement parent)
            : base(parent) {}

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton != MouseButtonState.Pressed)
                _DragStart = null;

            if (_DragStart.HasValue)
            {
                DragOperatorObject drag = new DragOperatorObject();
                //drag.Id = _Id;

                DragDrop.DoDragDrop(this, drag, DragDropEffects.Copy);

                e.Handled = true;
            }
        }
    }

    public class DragOperatorObject 
    {
        public int Id { get; set; }
    };

    public class DragTransitionalObject
    {
        public New.Move.Core.ITransitionalDesc Desc { get; set; }
    }

    public class DragNodeObject
    {
        public New.Move.Core.ILogicDesc Desc { get; set; }
    }

    public partial class Palette : UserControl
    {
        public Palette()
        {
            InitializeComponent();

            Items.Children.Clear();
        }

        public void Database_ActiveChanged(New.Move.Core.ITransitional active)
        {
            Items.Children.Clear();

            if (active is New.Move.Core.StateMachine) SetItems(active as New.Move.Core.StateMachine);
            else
            if (active is New.Move.Core.Motiontree) SetItems(active as New.Move.Core.Motiontree);
            /*
            OperatorItems.Children.Clear();
            if (active is New.Move.Core.Motiontree)
            {
                SetOperatorItems(active as New.Move.Core.Motiontree);
            }
             */
        }

        void SetItems(New.Move.Core.StateMachine active)
        {
            foreach (New.Move.Core.ITransitionalDesc registered in Runtime.Instance.TransitionalDescriptions)
            {
                PaletteTransitionalItem item = new PaletteTransitionalItem(registered, this);
                item.Icon = registered.Icon;
                item.ToolTip = registered.Help;
                Items.Children.Add(item);
            }
        }

        void SetItems(New.Move.Core.Motiontree active)
        {
            foreach (New.Move.Core.ILogicDesc registered in Runtime.Instance.LogicDescriptions)
            {
                PaletteNodeItem item = new PaletteNodeItem(registered, this);
                item.Icon = registered.Icon;
                item.ToolTip = registered.Help;
                Items.Children.Add(item);
            }
        }

        /*
        void SetOperatorItems(New.Move.Core.Motiontree active)
        {
            foreach (IRegisterOperator_Ex registered in Runtime.Instance.RegisteredOperators)
            {
                PaletteOperatorItem item = new PaletteOperatorItem(this);
                item.Icon = registered.PaletteIcon;
                item.ToolTip = registered.Help;
                OperatorItems.Children.Add(item);
            }
        }
         */
    }
}
