﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Xml.Serialization;
using System.IO;
using System.Globalization;
using System.Reflection;

using Move;
using Rage.Move.Core;

using New.Move.Core;
using Rage.Move.Utils;

using MoVE.Utils;
using Move.Utils;

using AvalonDock;

using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Services;
using RSG.Base.Logging.Universal;

namespace Rage.Move
{
    public class DocumentCreatedEventArgs : RoutedEventArgs
    {
        public DocumentCreatedEventArgs(DocumentPane host)
        {
            Host = host;
        }

        public DocumentPane Host { get; set; }
    }

    public partial class App : Application
    {
        private bool _ContentLoaded;

        [Import("MainWindow")]
        public new Window MainWindow
        {
            get { return base.MainWindow; }
            set 
            { 
                base.MainWindow = value; 
                base.MainWindow.Loaded += new RoutedEventHandler(MainWindow_Loaded);
            }
        }

        [Import(typeof(IConfigService))]
        IConfigService _ConfigService = null;

        [Import(typeof(IRuntimeCommunicationService))]
        IRuntimeCommunicationService _RuntimeCommunicationService = null;

        private string m_fileToLoadOnStart = null;  //  The stored filename to load on start up
        public void SetFileToLoadOnStart(string fileToLoad)
        {
            //  Store the filename for later use
            m_fileToLoadOnStart = fileToLoad;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            AppMain.CloseSplashScreen();
        }

        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent()
        {
            if (_ContentLoaded)
            {
                return;
            }
            _ContentLoaded = true;

            Startup += new StartupEventHandler(OnStartup);
            //StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);
            ShutdownMode = ShutdownMode.OnMainWindowClose;
            
            // Uncomment this to enable exception reporting
            DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(Application_DispatcherUnhandledException);

            Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("Skins\\Black\\BlackResources.xaml", UriKind.Relative) });
            Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("Skins\\ExpressionDark.xaml", UriKind.Relative) });
        }

        void OnStartup(object sender, StartupEventArgs e)
        {
        }

        void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ExceptionViewModel viewModel = new ExceptionViewModel(e.Exception);
            ExceptionViewer view = new ExceptionViewer();
            view.DataContext = viewModel;

            view.ShowDialog();

            e.Handled = true;
        }

        CompositionContainer _Container;

        Runtime _Runtime = new Runtime();

        public static App Instance { get { return (App)Application.Current; } }
        
        public Runtime Runtime { get { return _Runtime; } }

        private readonly static string RecentFilesFilePath =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            Path.Combine(Runtime.ApplicationFolderName, "RecentFiles.xml"));

        private readonly static string FavoriteDirectoriesFilePath =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            Path.Combine(Runtime.ApplicationFolderName, "FavoriteDirectories.xml"));

        private readonly static string RuntimeTargetsFilePath =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            Path.Combine(Runtime.ApplicationFolderName, "RuntimeTargets.xml"));

        private static readonly Client _Client = new Client();
        private static Link _Link = null;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            if (Compose())
            {
                _ConfigService.Load();

                MainWindow.Show();
            }
            else
            {
                Shutdown();
            }

            var mainWindow = (Rockstar.MoVE.Components.MainWindow.MainWindow)MainWindow;
            mainWindow.LoadRecentFiles(RecentFilesFilePath);
            mainWindow.UpdateRecentFilesMenu();
            mainWindow.LoadFavoriteDirectories(FavoriteDirectoriesFilePath);
            mainWindow.AllViewsLoaded += new EventHandler(mainWindow_Loaded);
 
            // Forward any StartDebugging or StopDebugging messages on to the communication service
            App.Client.StartDebugging += (sender, args) => _RuntimeCommunicationService.OnStartDebugging(sender, args.Payload);
            App.Client.StopDebugging += (sender, args) => _RuntimeCommunicationService.OnStopDebugging(sender, args.Payload);

            // Make double-click's in any textboxes select all
            EventManager.RegisterClassHandler(typeof(TextBox),
                TextBox.MouseDoubleClickEvent,
                new RoutedEventHandler(TextBox_OnDoubleClickEvent));
        }

        private void TextBox_OnDoubleClickEvent(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }

        void mainWindow_Loaded(object sender, EventArgs e)
        {
            //  Attempt to load first recent file now, or load any file requested at startup
            ((Rockstar.MoVE.Components.MainWindow.MainWindow)MainWindow).OpenFirstRecentFile(m_fileToLoadOnStart);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            DispatcherUnhandledException -= new DispatcherUnhandledExceptionEventHandler(Application_DispatcherUnhandledException);

            try
            {
                _ConfigService.Save();
                if (_Container != null)
                {
                    _Container.Dispose();
                }
            }
            catch (System.Configuration.ConfigurationErrorsException ex)
            {
                System.Diagnostics.Debug.WriteLine("Unexpected error encountered while saving application configuration.");
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
            finally
            {
                base.OnExit(e);
            }
        }

        bool Compose()
        {
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));

            // this should come from the config file
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Rockstar.MoVE.Components.MainWindow.MainWindow).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Rockstar.MoVE.Design.DocumentHost).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Rockstar.MoVE.Framework.IDataModel).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(New.Move.Core.Database).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Rockstar.MoVE.Services.IDataModelService).Assembly));

            // check the Plugins directory (recursively) for DLLs
            //var pluginDir = new DirectoryInfo(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins"));
            //catalog.Catalogs.Add(new DirectoryCatalog(pluginDir.FullName));

            //System.IO.DirectoryInfo[] dirs = pluginDir.GetDirectories("*", SearchOption.AllDirectories);
            //foreach(var dir in dirs)
            //{
            //    catalog.Catalogs.Add(new DirectoryCatalog(dir.FullName));
            //}

            _Container = new CompositionContainer(catalog);

            CompositionBatch batch = new CompositionBatch();
            batch.AddPart(this);

            try
            {
                _Container.Compose(batch);
            }
            catch (CompositionException ex)
            {
                MessageBox.Show(ex.ToString());
                Shutdown(1);

                return false;
            }

            return true;
        }

        public static Client Client
        {
            get { return _Client; }
        }

        public static Link Link
        {
            get { return _Link; }
        }

        public static TimeSpan GetAnimationDuration(double milliseconds)
        {
            return TimeSpan.FromMilliseconds(
                Keyboard.IsKeyDown(Key.F12) ?
                milliseconds * 5 : milliseconds);
        }

        void App_StartDebugging(object sender, SimpleEventArgs<IDebugSession> e)
        {
            _RuntimeCommunicationService.OnStartDebugging(this, e.Payload);
        }

        void App_StopDebugging(object sender, SimpleEventArgs<IDebugSession> e)
        {
            _RuntimeCommunicationService.OnStopDebugging(this, e.Payload);
        }
    }
}
