﻿using System;

namespace New.Move.Core
{
    public interface IExternalFileLink
    {
        string Filename { get; set; }
    }
}
