﻿using System;
using System.Runtime.Serialization;

namespace Rockstar.MoVE.Framework
{
    public class SaveFailedException : ApplicationException
    {
        public SaveFailedException()
        {
        }

        public SaveFailedException(string message)
            : base(message)
        {
        }

        public SaveFailedException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected SaveFailedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}