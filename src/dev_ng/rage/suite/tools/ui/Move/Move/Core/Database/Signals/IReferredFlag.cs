﻿namespace New.Move.Core
{
    public interface IReferredFlag
    {
        ReferenceFlag CreateReference();
    }
}