﻿using System;

namespace Rage.Move.Utils
{
    public interface IGroupable
    {
        Guid Id
        {
            get;
        }

        Guid Parent
        {
            get;
            set;
        }

        bool IsGroup
        {
            get;
            set;
        }
    }
}
