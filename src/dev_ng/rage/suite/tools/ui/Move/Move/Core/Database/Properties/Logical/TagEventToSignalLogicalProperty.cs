﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

using RSG.AnimationMetadata;

using Move.Core;
using Move.Utils;
using System.Xml;
using System.Globalization;

namespace New.Move.Core
{
    [Serializable]
    public class TagEventToSignalLogicalProperty : Property, ISerializable
    {
        public static string InvalidSelectionText { get { return "<none>"; } }

        public TagEventToSignalLogicalProperty()
        {
            Id = Guid.NewGuid();
            TagEventName = InvalidSelectionText;
            SignalName = InvalidSelectionText;
        }

        public TagEventToSignalLogicalProperty(TagEventToSignalLogicalProperty other)
        {
            Parent = other.Parent;
            TagEventName = other.TagEventName;
            SignalName = other.SignalName;
            SelectedEvent = other.SelectedEvent;
            _OutputEnabled = other._OutputEnabled;

            UpdateAvailableTagEvents();
            UpdateAvailableSignals();
            Id = Guid.NewGuid();
        }

        public TagEventToSignalLogicalProperty(ILogic parent)
            : this()
        {
            this.parent = parent;
        }

        public void UpdateSelectionLists()
        {
            UpdateAvailableTagEvents();
            UpdateAvailableSignals();

            Parent.Database.Signals.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(OnSignalsCollectionChanged);
            Parent.Database.SignalNameChanged += new Action(OnSignalNameChanged);
        }

        public void OnRemovedFromParent()
        {
            Parent.Database.Signals.CollectionChanged -= new System.Collections.Specialized.NotifyCollectionChangedEventHandler(OnSignalsCollectionChanged);
            Parent.Database.SignalNameChanged -= new Action(OnSignalNameChanged);
        }

        void OnSignalsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateAvailableSignals();
        }

        void OnSignalNameChanged()
        {
            UpdateAvailableSignals();
        }

        #region IAnchor overrides
        public object Tag { get; set; }

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            from.RemoveConnectionsFromAnchor(this);
        }
        #endregion

        #region IProperty overrides
        public string Name { get; set; }        

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }
        #endregion

        #region Property overrides
        public PropertyInput Flags { get; private set; }
        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }


        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        public IEnumerable<string> AvailableEvents { get { return availableEvents_; } }

        public string TagEventName
        {
            get { return tagEventName_; }
            set
            {
                if (tagEventName_ != value)
                {
                    tagEventName_ = value;
                    OnPropertyChanged("TagEventName");
                }
            }
        }

        public IEnumerable<string> AvailableSignals { get { return availableSignals_; } }

        public string SignalName
        {
            get { return signalName_; }
            set
            {
                if (signalName_ != value)
                {
                    signalName_ = value;
                    OnPropertyChanged("SignalName");
                }
            }
        }

        public Guid Id { get; private set; }

        private ILogic parent;
        public ILogic Parent
        {
            get { return this.parent;}
            set { this.parent = value; }
        }

        static class SerializationTag
        {
            public static string TagEventName = "TagEventName";
            public static string SignalName = "SignalName";
            public static string Id = "Id";
            public static string Parent = "Parent";
        }

        public TagEventToSignalLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
            TagEventName = (string)info.GetValue(SerializationTag.TagEventName, typeof(string));
            SignalName = (string)info.GetValue(SerializationTag.SignalName, typeof(string));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.TagEventName, TagEventName);
            info.AddValue(SerializationTag.SignalName, SignalName);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
        }

        private void UpdateAvailableTagEvents()
        {
            var unsortedEventNames = new List<string>();

            foreach (string eventName in ClipTagDefinitions.PropertyDescriptions.Keys)
            {
                if (ClipTagDefinitions.PropertyDescriptions[eventName].AttributeDefs.Count > 0)
                {
                    unsortedEventNames.Add(eventName);// Only add if there are attributes
                }
            }

            availableEvents_ = unsortedEventNames.OrderBy(eventName => eventName).ToArray();

            OnPropertyChanged("AvailableEvents");
        }

        private void UpdateAvailableSignals()
        {
            if (Parent == null || Parent.Database == null)
                return;

            availableSignals_ = new List<string>();

            foreach (var signal in Parent.Database.Signals.OfType<TagSignal>())
            {
                availableSignals_.Add(signal.Name);
            }

            if (availableSignals_.Count == 0)// Didn't find any valid signals so add the invalid selection text
            {
                availableSignals_.Add(InvalidSelectionText);
                SignalName = InvalidSelectionText;
            }

            OnPropertyChanged("AvailableSignals");
        }

        public object Clone()
        {
            return new TagEventToSignalLogicalProperty(this);
        }

        private string tagEventName_;
        private string signalName_;
        private string[] availableEvents_;
        private List<string> availableSignals_;
  
        /// <summary>
        /// Initialises a new instance of the <see cref="TagEventToSignalLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public TagEventToSignalLogicalProperty(XmlReader reader, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Signal", this.SignalName);
            writer.WriteAttributeString("Tag", this.TagEventName);
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.SignalName = reader.GetAttribute("Signal");
            this.TagEventName = reader.GetAttribute("Tag");
            reader.Skip();
        }
    }
}
