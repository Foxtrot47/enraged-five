﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

using RSG.AnimationMetadata;
using System.Globalization;
using System.Xml;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class TagConditionProperty : IConditionProperty, ISerializable
    {
        public TagConditionProperty()
        {
            Id = Guid.NewGuid();
        }

        public TagConditionProperty(TagConditionProperty other)
        {
            Id = Guid.NewGuid();
            this.Name = other.Name;
            this._Value = other.Value;
        }

        public object Clone()
        {
            return new TagConditionProperty(this);
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        private Signal _Value;
        public Signal Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                OnPropertyChanged("Value");
            }
        }

        public Condition Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Value = "Value";
            public static string Parent = "Parent";
            public static string TagType = "TagType";
            public static string TagAttribute = "TagAttribute";
        }

        public TagConditionProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Value = (Signal)info.GetValue(SerializationTag.Value, typeof(Signal));
            Parent = (Condition)info.GetValue(SerializationTag.Parent, typeof(Condition));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Value, Value);
            info.AddValue(SerializationTag.Parent, Parent);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.StringLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public TagConditionProperty(XmlReader reader, Database database, Condition parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.Value = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.Value == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.Value = database.GetSignal(signalName);
                    if (this.Value == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}'. This needs fixing before a export is allowed.", signalId);
                    }
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Value != null)
            {
                writer.WriteAttributeString("Signal", this.Value.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.Value.Name);
            }
        }
    }
}