﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Rage.Move.Core;

namespace Rage.Move
{
    public delegate void LayerItemEnableChangedEventHandler();

    public class LayerMenuItem : MenuItem
    {
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(SolidColorBrush), typeof(LayerMenuItem));

        public SolidColorBrush Color
        {
            get { return (SolidColorBrush)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(LayerMenuItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public LayerMenuItem()
        {
        }
    }

    public class LayerItem : UserControl
    {
        public static DependencyProperty EnabledProperty =
            DependencyProperty.Register("Enabled", typeof(bool), typeof(LayerItem), new FrameworkPropertyMetadata(EnabledProperty_Changed));

        public bool Enabled
        {
            get { return (bool)GetValue(EnabledProperty); }
            set { SetValue(EnabledProperty, value); }
        }

        public static DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(SolidColorBrush), typeof(LayerItem));

        public SolidColorBrush Color
        {
            get { return (SolidColorBrush)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(LayerItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public LayerItem(Layers.Layer layer)
        {
            DataContext = this;
            Layer = layer;

            Enabled = Layer.Enabled;
            Color = new SolidColorBrush(Layer.Color);
            Label = Layer.Name;
        }

        static void EnabledProperty_Changed(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            LayerItem item = (LayerItem)sender;
            item.Layer.Enabled = item.Enabled;

            if (item.EnableChanged != null)
                item.EnableChanged();
        }

        public Layers.Layer Layer { get; set; }

        internal LayerItemEnableChangedEventHandler EnableChanged;
    }
    
    public partial class Layer : UserControl
    {
        public Layer()
        {
            InitializeComponent();
        }

        void CreateLayer_Click(object sender, RoutedEventArgs e)
        {
            Layers.Layer layer = Runtime.Instance.Layers.Create();

            LayerItem item = new LayerItem(layer);
            item.Template = (ControlTemplate)FindResource("LayerItemTemplate");

            item.EnableChanged += new LayerItemEnableChangedEventHandler(
                delegate() { DiagramViewer.Instance.Diagram.OnDiagramChanged(); });

            Items.Children.Add(item);
        }

        void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            LayerItem item = (LayerItem)(sender as Button).DataContext;
            Items.Children.Remove(item);

            item.Layer.Remove();
        }
    }
}
