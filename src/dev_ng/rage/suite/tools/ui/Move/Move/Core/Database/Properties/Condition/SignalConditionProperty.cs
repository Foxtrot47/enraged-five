﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

using RSG.AnimationMetadata;
using System.Xml;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class SignalConditionProperty : IConditionProperty, ISerializable
    {
        public SignalConditionProperty()
        {
            Id = Guid.NewGuid();
        }

        public SignalConditionProperty(SignalConditionProperty other)
        {
            Id = Guid.NewGuid();
            this.Name = other.Name;
            this._SignalIsTagSignal = other._SignalIsTagSignal;
            this._TagAttribute = other._TagAttribute;
            this._TagType = other._TagType;

            if (other._TagAttributes != null)
            {
                this._TagAttributes = new List<string>();
                foreach (string attribute in other._TagAttributes)
                    this._TagAttributes.Add(attribute);
            }
            if (other._TagTypes != null)
            {
                this._TagTypes = new List<string>();
                foreach (string type in other._TagTypes)
                    this._TagTypes.Add(type);
            }

            this._Value = other.Value;
        }

        public object Clone()
        {
            return new SignalConditionProperty(this);
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        private Signal _Value;
        public Signal Value 
        {
            get { return _Value; }
            set
            {
                if (_Value != null)
                {
                    _Value.SetUsed(false);
                }
                _Value = value;
                if (_Value != null)
                {
                    _Value.SetUsed(true);
                }
                OnPropertyChanged("Value");

                UpdateTagParameterTypes();
            }
        }

        public Condition Parent { get; set; }

        // support for tag parameters
        private bool _SignalIsTagSignal;
        public bool SignalIsTagSignal
        {
            get { return _SignalIsTagSignal; }
            set
            {
                _SignalIsTagSignal = value;
                OnPropertyChanged("SignalIsTagSignal");
            }
        }

        private List<string> _TagTypes;
        public IEnumerable<string> TagTypes
        {
            get { return _TagTypes; }
        }

        private string _TagType;
        public string TagType
        {
            get { return _TagType; }
            set
            {
                _TagType = value;
                OnPropertyChanged("TagType");
                UpdateTagAttributes();
            }
        }

        private List<string> _TagAttributes;
        public IEnumerable<string> TagAttributes
        {
            get { return _TagAttributes; }
        }

        private string _TagAttribute;
        public string TagAttribute
        {
            get { return _TagAttribute; }
            set
            {
                _TagAttribute = value;
                OnPropertyChanged("TagAttribute");
            }
        }

        private void UpdateTagParameterTypes()
        {
            if (Value != null && Value.Type == "Tag")
            {
                SignalIsTagSignal = true;

                var unsortedTagTypes = new List<string>();
                foreach (string eventName in ClipTagDefinitions.PropertyDescriptions.Keys)
                {
                    if (ClipTagDefinitions.PropertyDescriptions[eventName].AttributeDefs.Count > 0)
                    {
                        unsortedTagTypes.Add(eventName);// Only add if there are attributes
                    }
                }

                _TagTypes = unsortedTagTypes.OrderBy(tagType => tagType).ToList();
                OnPropertyChanged("TagTypes");
            }
            else
            {
                SignalIsTagSignal = false;
                _TagTypes = new List<string>();
                OnPropertyChanged("TagTypes");
            }
        }

        private void UpdateTagAttributes()
        {
            _TagAttributes = new List<string>();

            if (_TagTypes != null && _TagTypes.Contains(TagType))
            {
                foreach (var clipAtt in ClipTagDefinitions.PropertyDescriptions[TagType].AttributeDefs)
                {
                    _TagAttributes.Add(clipAtt.Name);
                }
            }

            OnPropertyChanged("TagAttributes");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag 
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Value = "Value";
            public static string Parent = "Parent";
            public static string TagType = "TagType";
            public static string TagAttribute = "TagAttribute";
        }

        public SignalConditionProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Value = (Signal)info.GetValue(SerializationTag.Value, typeof(Signal));
            Parent = (Condition)info.GetValue(SerializationTag.Parent, typeof(Condition));

            try
            {
                TagType = info.GetString(SerializationTag.TagType);
                TagAttribute = info.GetString(SerializationTag.TagAttribute);
            }
            catch (SerializationException)
            {
                TagType = "";
                TagAttribute = "";
            }
            
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Value, Value);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.TagType, TagType);
            info.AddValue(SerializationTag.TagAttribute, TagAttribute);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SignalConditionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public SignalConditionProperty(XmlReader reader, Database database, Condition parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.Value = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.Value == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.Value = database.GetSignal(signalName);
                    if (this.Value == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}'. This needs fixing before a export is allowed.", signalId);
                    }
                }
            }

            this.TagType = reader.GetAttribute("TagType");
            this.TagAttribute = reader.GetAttribute("TagAttribute");
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Value != null)
            {
                writer.WriteAttributeString("Signal", this.Value.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.Value.Name);
            }

            writer.WriteAttributeString("TagType", this.TagType);
            writer.WriteAttributeString("TagAttribute", this.TagAttribute);
        }
    }
}