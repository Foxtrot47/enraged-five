﻿using System;
using System.Windows;

namespace Rage.Move.Core.Dg
{
#if go_away
    public interface ILogicNode
    {
        string Name { get; set; }
        Guid Id { get; }

        string Type { get; set; }
        Point Position { get; set; }
        Guid Layer { get; set; }

        MotionTree Parent { get; set; }
    }
#endif
}
