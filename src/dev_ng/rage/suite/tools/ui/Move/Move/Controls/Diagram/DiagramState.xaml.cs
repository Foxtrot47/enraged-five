﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rage.Move.Core;
using Rage.Move.Core.Dg;
using Rage.Move.Core.Dag;
using Rage.Move.Utils;

namespace Rage.Move
{
    public class CathodeAdorner : Adorner
    {
        public CathodeAdorner(Diagram diagram, Cathode item, Point source)
            : base(diagram)
        {
            Source = source;

            Pen = new Pen(Brushes.LightSlateGray, 2);
            Pen.LineJoin = PenLineJoin.Round;
            Cursor = Cursors.Cross;

            Diagram = diagram;
            Item = item;
        }

        PathGeometry Geometry { get; set; }
        Point Source { get; set; }
        Pen Pen { get; set; }

        Diagram Diagram { get; set; }
        Cathode Item { get; set; }

        DiagramState Hit { get; set; }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!IsMouseCaptured)
                    CaptureMouse();

                Hit = HitTest(e.GetPosition(this));

                Geometry = Calculate(e.GetPosition(this));

                InvalidateVisual();
            }
            else
            {
                if (IsMouseCaptured)
                    ReleaseMouseCapture();
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (IsMouseCaptured)
                ReleaseMouseCapture();

            if (Hit != null)
            {
                ContextMenu context = new ContextMenu();
                foreach (IRegisterTransition_Ex registered in Runtime.Instance.TransitionDescriptors)
                {
                    MenuItem item = new MenuItem();
                    item.Header = registered.Name;
                    item.CommandParameter = registered;
                    item.Click += new RoutedEventHandler(Transition_Click);

                    context.Items.Add(item);
                }

                context.IsOpen = true;

                context.Closed += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs args) 
                    {
                        AdornerLayer layer = AdornerLayer.GetAdornerLayer(Diagram);
                        if (layer != null)
                            layer.Remove(this);
                    }
                );
            }
            else
            {
                AdornerLayer layer = AdornerLayer.GetAdornerLayer(Diagram);
                if (layer != null)
                    layer.Remove(this);
            }
            
            //Diagram.Logic.OnTransition(Item, Hit);
        }

        protected override void OnRender(DrawingContext context)
        {
            base.OnRender(context);

            context.DrawGeometry(null, Pen, Geometry);
            context.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));
        }

        DiagramState HitTest(Point point)
        {
            DependencyObject obj = Item as DependencyObject;
            while (obj != null && !(obj is DiagramState))
                obj = VisualTreeHelper.GetParent(obj);

            DiagramState parent = obj as DiagramState;
            if (parent != null)
            {
                DependencyObject hitobj = Diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != parent)
                {
                    if (hitobj is DiagramState)
                        return hitobj as DiagramState;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        PathGeometry Calculate(Point position)
        {
            PathGeometry geometry = new PathGeometry();

            PathFigure figure = new PathFigure();
            figure.StartPoint = Source;

            figure.Segments.Add(new PolyLineSegment(new PointCollection() { position }, true));
            geometry.Figures.Add(figure);

            return geometry;
        }

        void Transition_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            IRegisterTransition_Ex registered = (IRegisterTransition_Ex)item.CommandParameter;

            DependencyObject obj = Item as DependencyObject;
            while (obj != null && !(obj is DiagramState))
                obj = VisualTreeHelper.GetParent(obj);

            DiagramState parent = obj as DiagramState;

            //Diagram.Logic.OnTransition(registered.Create(parent.Id, Hit.Id));
        }
    }

    public class Cathode : UserControl, INotifyPropertyChanged
    {
        public Cathode()
        {
        }

        Point? Start = null;

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                Start = new Point?(e.GetPosition(diagram));
                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        CathodeAdorner adorner = new CathodeAdorner(diagram, this, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

        Diagram GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }

    public class StartMarkerAdorner : Adorner
    {
        public StartMarkerAdorner(Diagram diagram, StartMarker item, Point position)
            : base(diagram)
        {
            Diagram = diagram;
            Item = item;
            Position = position;

            Brush = Brushes.LightSlateGray.Clone();
            Brush.Opacity = 0.5f;
        }

        Diagram Diagram { get; set; }
        StartMarker Item { get; set; }
        Point Position { get; set; }
        Brush Brush { get; set; }

        DiagramState Hit { get; set; }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!IsMouseCaptured)
                    CaptureMouse();

                Position = e.GetPosition(Diagram);
                Hit = HitTest(e.GetPosition(this));

                InvalidateVisual();
            }
            else
            {
                if (IsMouseCaptured)
                    ReleaseMouseCapture();
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            //base.OnMouseUp(e);

            /*
            if (Hit != null)
                (Runtime.Database.Active as New.Move.Core.StateMachine).Default = Hit.Id;
             */

            if (IsMouseCaptured)
                ReleaseMouseCapture();

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(Diagram);
            if (layer != null)
                layer.Remove(this);
        }

        protected override void OnRender(DrawingContext context)
        {
            base.OnRender(context);

            Geometry geometry = Geometry.Parse("M9,0 17,4 9,8 Z").Clone();
            geometry.Transform = new TranslateTransform(Position.X, Position.Y - 4);

            context.DrawEllipse(Brush, null, Position, 4, 4);
            context.DrawGeometry(Brush, null, geometry);
        }

        DiagramState HitTest(Point point)
        {
            DependencyObject hitobj = Diagram.InputHitTest(point) as DependencyObject;
            while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != Item)
            {
                if (hitobj is DiagramState)
                    return hitobj as DiagramState;

                hitobj = VisualTreeHelper.GetParent(hitobj);
            }

            return null;
        }
    }

    public class StartMarker : UserControl, IMeasureable
    {
        public StartMarker()
        {
             Template = (ControlTemplate)App.Current.FindResource("StartMarkerTemplate");
        }

        public Point Position { get; set; }

        DiagramState _Target;
        public DiagramState Target 
        {
            get { return _Target; }
            set
            {
                if (_Target != value)
                {
                    if (_Target != null)
                        _Target.PropertyChanged -= new PropertyChangedEventHandler(Target_PropertyChanged);

                    _Target = value;

                    if (_Target != null)
                        _Target.PropertyChanged += new PropertyChangedEventHandler(Target_PropertyChanged);

                    UpdatePosition();
                }
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        StartMarkerAdorner adorner = new StartMarkerAdorner(diagram, this, e.GetPosition(diagram));
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

        void UpdatePosition()
        {
            Position = new Point(Canvas.GetLeft(_Target) - DesiredSize.Width, Canvas.GetTop(_Target) - DesiredSize.Height / 2);
            Canvas.SetLeft(this, Position.X);
            Canvas.SetTop(this, Position.Y);
        }

        void Target_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Position"))
                UpdatePosition();
        }

        Diagram GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }

    public partial class DiagramState : UserControl, IMeasureable, ISelectable, IConnectable
    {
        private static class Const
        {
            public static double OpacityFiltered = 0.15;
            public static double OpacityNormal = 1.0;
            public static double AnimationDuration = 300;
        }

        public static readonly RoutedEvent PositionChangedEvent = EventManager.RegisterRoutedEvent(
           "PositionChanged", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(DiagramState));

        Point _Position;
        public Point Position
        {
            get { return _Position; }
            set
            {
                _Position = value;
                if (PositionChanged != null) 
                    PositionChanged(_Position);
                OnPropertyChanged("Position");
            }
        }

        public event ActivatedEventHandler Activated = null;
        public event PositionChangedEventHandler PositionChanged = null;

        bool filtered;

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(DiagramState));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public DiagramState(Guid id)
        {
            InitializeComponent();

            DataContext = this;
            Id = id;

            AddHandler(PositionChangedEvent, new RoutedEventHandler(OnPositionChanged), true);
        }

        public Guid Id { get; private set; }

        public event EventHandler ChildrenLoaded;
        public void OnChildrenLoaded()
        {
            if (ChildrenLoaded != null)
                ChildrenLoaded(this, EventArgs.Empty);
        }

        public event ConnectionOverEventHandler ConnectionEnter;
        public void OnConnectionEnter(IAnchorSender sender)
        {
            if (ConnectionEnter != null)
                ConnectionEnter(sender);
        }

        public event ConnectionOverEventHandler ConnectionExit;
        public void OnConnectionExit(IAnchorSender sender)
        {
            if (ConnectionExit != null)
                ConnectionExit(sender);
        }

        void OnPositionChanged(object sender, RoutedEventArgs e)
        {
            Position = new Point(Canvas.GetLeft(this), Canvas.GetTop(this));
        }

        int _Count;
        public int Count
        {
            get { return _Count; }
            set
            {
                _Count = value;

                Panel PART_Anchors = (Panel)GetTemplateChild("PART_Anchors");
                if (PART_Anchors != null)
                {
                    if (PART_Anchors.Children.Count == _Count)
                        OnChildrenLoaded();
                }
            }
        }

        public bool IsFiltered
        {
            get { return this.filtered; }
            set
            {
                if (this.filtered != value)
                {
                    this.filtered = value;
                    double newOpacity = this.filtered ? Const.OpacityFiltered : Const.OpacityNormal;
                    this.BeginAnimation(DiagramNode.OpacityProperty,
                        new DoubleAnimation(this.Opacity, newOpacity,
                            App.GetAnimationDuration(Const.AnimationDuration)));
                }
            }
        }

        public bool Selected
        {
            get { return (bool)GetValue(SelectedProperty); }
            set { SetValue(SelectedProperty, value); }
        }

        public static readonly DependencyProperty SelectedProperty =
            DependencyProperty.Register("Selected",
                                        typeof(bool),
                                        typeof(DiagramState),
                                        new FrameworkPropertyMetadata(false));

        public bool IsDragConnectionOver
        {
            get
            {
                return (bool)GetValue(IsDragConnectionOverProperty);
            }
            set
            {
                SetValue(IsDragConnectionOverProperty, value);
            }
        }

        public static readonly DependencyProperty IsDragConnectionOverProperty =
            DependencyProperty.Register("IsDragConnectionOver",
                                        typeof(bool),
                                        typeof(DiagramState),
                                        new FrameworkPropertyMetadata(false));

        public double Scale { get; set; }
        
        
        public Point Center
        {
            get { return new Point(RelativePosition.X + (DesiredSize.Width / 2), RelativePosition.Y + (DesiredSize.Height / 2)); }
        }

        public Point TopCenter
        {
            get { return new Point(RelativePosition.X + (DesiredSize.Width / 2), RelativePosition.Y); }
        }

        public Point TopRight
        {
            get { return new Point(RelativePosition.X + DesiredSize.Width, RelativePosition.Y); }
        }

        public Point TopLeft
        {
            get { return new Point(RelativePosition.X, RelativePosition.Y); }
        }

        public Point BottomCenter
        {
            get { return new Point(RelativePosition.X + (DesiredSize.Width / 2), RelativePosition.Y + DesiredSize.Height); }
        }

        public Point LeftCenter
        {
            get { return new Point(RelativePosition.X, RelativePosition.Y + (DesiredSize.Height / 2)); }
        }

        public Point RightCenter
        {
            get { return new Point(RelativePosition.X + DesiredSize.Width, RelativePosition.Y + (DesiredSize.Height / 2)); }
        }

        public double Left
        {
            get { return Position.X; }
            set { Position = new Point(value, Position.Y); }
        }

        public double Top
        {
            get { return Position.Y; }
            set { Position = new Point(Position.X, value); }
        }

        Point RelativePosition
        {
            get 
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                    return TranslatePoint(new Point(0,0), diagram);

                return Position; 
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected override void OnPreviewMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDoubleClick(e);

            if (e.ChangedButton == MouseButton.Left)
            {
                if (Activated != null)
                    Activated();

                e.Handled = true;
            }
        }

        public void Attach(ObservableCollection<IAnchor> items)
        {
            /*
            StackPanel PART_Anchors = (StackPanel)GetTemplateChild("PART_Anchors");
            PART_Anchors.Children.Clear();

            foreach (IAnchor anchor in items)
            {
                if (anchor is ITransformAnchor)
                    continue;

                AnchorItem item = anchor.Create(this);
                item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++Count; });
                PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                PART_Anchors.Children.Add(item);
            }

            if (PART_Anchors.Children.Count == 0)
                OnChildrenLoaded();
             */
        }

        void MessageItem_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow window = Application.Current.MainWindow as MainWindow;
            //window.ShowAddStateInputMessage(this.State);
        }

        void SignalItem_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow window = Application.Current.MainWindow as MainWindow;
            //window.ShowAddStateInputMessage(this.State);
        }

        public object GetChild(string name)
        {
            return GetTemplateChild(name);
        }

        Diagram GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }
    }
}
