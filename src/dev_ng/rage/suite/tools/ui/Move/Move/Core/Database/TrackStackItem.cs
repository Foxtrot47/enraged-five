﻿using System.Windows;
using System.Xml;

using New.Move.Core;
using Move.Core;
using Move.Utils;

using RSG.TrackViewer.Data;

namespace Rockstar.MoVE.Framework.DataModel
{
    public class TrackStackItem : IOperatorItem
    {
        public TrackStackItem(Track track)
        {
            Track = track;
        }

        Track Track;

        struct Segment
        {
            public TrackPoint Start;
            public TrackPoint End;
        }

        public void ExportToXML(PargenXmlNode parentNode)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "CurveOperator");

            itemNode.AppendValueElementNode("MinClamp", "value", Track.ClampMinimum.ToString());
            itemNode.AppendValueElementNode("MaxClamp", "value", Track.ClampMaximum.ToString());

            var keyframesNode = itemNode.AppendChild("Keyframes");

            //we just have a list of points here. we need the proper data
            Segment[] segments = new Segment[Track.Points.Count - 1];

            for (int i = 1; i < Track.Points.Count; ++i)
            {
                segments[i - 1].Start = Track.Points[i - 1];
                segments[i - 1].End = Track.Points[i];
            }

            foreach (Segment segment in segments)
            {
                var keyframeItemNode = keyframesNode.AppendChild("Item");

                keyframeItemNode.AppendValueElementNode("Key", "value", segment.End.X.ToString());

                double m = (segment.End.Y - segment.Start.Y) / (segment.End.X - segment.Start.X);
                keyframeItemNode.AppendValueElementNode("M", "value", m.ToString());

                double b = segment.End.Y - m * segment.End.X;
                keyframeItemNode.AppendValueElementNode("B", "value", b.ToString());
            }
        }
    }
}