﻿using System;
using System.Runtime.Serialization;
using System.Xml;

namespace New.Move.Core
{
    [Serializable]
    public class RealInputAnchor : SourceAnchor, ISerializable
    {
        public RealInputAnchor(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        RealInputAnchor()
            : base(null) { }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
        }

        public RealInputAnchor(SerializationInfo info, StreamingContext context)
            : this()
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RealInputAnchor"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public RealInputAnchor(XmlReader reader, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            reader.Skip();
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}