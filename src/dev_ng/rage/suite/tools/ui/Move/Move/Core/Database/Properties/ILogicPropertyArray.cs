﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace New.Move.Core
{
    public interface ILogicPropertyArray : ILogicProperty, ILogicContainerProperty
    {
        IEnumerable<IProperty> Children { get; }

        void AddHandler(NotifyCollectionChangedEventHandler handler);
        void RemoveHandler(NotifyCollectionChangedEventHandler handler);

        void Remove(IProperty property);
        void Add();

        bool Contains(IProperty property, out IProperty at);
        int IndexOf(IProperty property);
    }
}