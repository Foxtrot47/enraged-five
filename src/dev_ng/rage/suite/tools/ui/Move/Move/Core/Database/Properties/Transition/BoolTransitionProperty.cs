﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;

namespace New.Move.Core
{
    [Serializable]
    public class BoolTransitionProperty : ITransitionProperty, ISerializable
    {
        public static implicit operator BoolTransitionProperty(bool initialValue)
        {
            BoolTransitionProperty property = new BoolTransitionProperty(initialValue);

            return property;
        }

        public BoolTransitionProperty()
        {
            Id = Guid.NewGuid();
        }

        public BoolTransitionProperty(BoolTransitionProperty other)
        {
            this.Name = other.Name;
            this.Parent = other.Parent;
            this.Value = other.Value;
            Id = Guid.NewGuid();
        }


        public BoolTransitionProperty(bool initialValue) : this()
        {
            Value = initialValue;
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public ITransition Parent { get; set; }

        bool _Value;
        public bool Value 
        {
            get { return _Value; }
            set
            {
                _Value = value;
                OnPropertyChanged("Value");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Value = "Value";
        }

        public BoolTransitionProperty(SerializationInfo info, StreamingContext context)
        {
            try
            {
                Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            }
            catch
            {
                Name = null;
            }

            try
            {
                Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            }
            catch
            {
                Id = Guid.NewGuid();
            }

            try
            {
                Parent = (ITransition)info.GetValue(SerializationTag.Parent, typeof(ITransition));
            }
            catch
            {
                Parent = null;
            }

            try
            {
                Value = (bool)info.GetValue(SerializationTag.Value, typeof(bool));
            }
            catch
            {
                Value = false;
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Value, Value);
        }

        public object Clone()
        {
            return new BoolTransitionProperty(this);
        }
                        
        /// <summary>
        /// Initialises a new instance of the <see cref="BoolTransitionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public BoolTransitionProperty(XmlReader reader, Database database, ITransition parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Value = bool.Parse(reader.GetAttribute("Value"));
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "Value", this.Value.ToString(CultureInfo.InvariantCulture));
        }
    }
}