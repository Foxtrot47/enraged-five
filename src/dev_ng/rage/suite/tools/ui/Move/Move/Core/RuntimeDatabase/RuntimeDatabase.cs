﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;

using Move.Utils;
using Rage.Move.Core;
using New.Move.Core;

namespace Move.Core
{
    public class RuntimeDatabaseNode
    {
        public RuntimeDatabaseNode(NetworkMessageBase msg)
        {
            Message = msg;
        }

        public NetworkMessageBase Message { get; protected set; }

        List<NetworkMessageBase> _Children = new List<NetworkMessageBase>();

    }


    public class BoolInterval : Interval
    {
        public enum StateEnum
        {
            BI_False,
            BI_True,
            BI_Unknown,
        }

        public BoolInterval() {}
        public BoolInterval(bool b, TimeSpan start, TimeSpan stop)
            : base(start, stop)
        {
            Data = b ? StateEnum.BI_True : StateEnum.BI_False;
        }
        public BoolInterval(TimeSpan start, TimeSpan stop) : base(start, stop) { } // creates an interval with an unknown value

        public StateEnum Data = StateEnum.BI_Unknown;

        public bool IsUnknown {
            get { return Data == StateEnum.BI_Unknown; }
        }
    }

    public class StringInterval : Interval
    {
        public StringInterval() {}
        public StringInterval(string s, TimeSpan start, TimeSpan stop)
            : base(start, stop)
        {
            Data = s;
        }
        public StringInterval(TimeSpan start, TimeSpan stop) : base(start, stop) { } // creates an interval with an unknown value

        public string Data = null;

        public bool IsUnknown {
            get { return Data == null; }
        }
    }

    public class FloatInterval : Interval
    {
        public FloatInterval() {}
        public FloatInterval(float f, TimeSpan start, TimeSpan stop)
            : base(start, stop)
        {
            Data = f;
        }
        public FloatInterval(TimeSpan start, TimeSpan stop) : base(start, stop) { } // creates an interval with an unknown value

        public float Data = float.NaN;

        public bool IsUnknown {
            get { return float.IsNaN(Data); }
        }
    }

    public static class SearchHelpers
    {
        // Works more like the C++ lower_bound function, always returns a positive value
        // If func tests for <= x instead of < x it becomes the C++ upper_bound function
        // "lessThanVal" can be any predicate function, provided that list.Select(pred) 
        // would look like [true*, false*]. I.e. a series of trues followed by a series of falses.
        // Looked at this way, BinarySearch2 return the index of the first false.
        public static int BinarySearch2<T>(this IList<T> list, Func<T, bool> lessThanVal)
        {
            int index = 0;
            int first = 0;
            int last = list.Count;
            int count = last - first;
            int step;
            while(count > 0)
            {
                step = count / 2;
                index = first + step;
                if (lessThanVal(list[index]))
                {
                    index++;
                    first = index;
                    count -= step + 1;
                }
                else
                {
                    count = step;
                }
            }
            return first;
        }

        // Extension method for searching an interval list
        // Given an interval inter, returns the indices in Values that represent the interval that contains 
        // inter.StartTime (or the next interval, if none contain StartTime), and the interval that contains
        // inter.EndTime, or the previous interval if none contain EndTime.
        public static void FindIndicesForInterval<T> (this IList<T> list, Interval inter, out int startIndex, out int stopIndex) where T : Interval
        {
            startIndex = list.BinarySearch2((x) => (x.EndTime < inter.StartTime));
            stopIndex = list.BinarySearch2((x) => (x.StartTime <= inter.EndTime));
        }
    }

    public class SignalHistory
    {
        // A SignalHistory.Signal is an ordered list of non-overlapping Intervals. Each of these intervals
        // represents a signal's value for some duration of time.
        public abstract class Signal
        {
            public New.Move.Core.Parameter Type;
            public uint Id;
            public ushort Writer;
            public abstract void AddData(TimeSpan currTime, SignalSetMessage.SignalData data, RuntimeDatabase db);
            public abstract void BumpEndTime(TimeSpan newEnd);

        }

        public class BoolSignal : Signal
        {
            public override void AddData(TimeSpan currTime, SignalSetMessage.SignalData data, RuntimeDatabase db)
            {
                Debug.Assert(Type == data.Type, "Signals type changed");
                Debug.Assert(Writer == data.Writer, "Don't support multiple writers within one parent yet");
                
                // A few options here:
                // If currTime = the previous stopTime, AND the values are the same, continue the existing interval
                // If the values changed, create a new interval
                // If currTime != the previous stop time, create a new interval

                var lastInter = Values[Values.Count - 1];
                if (currTime == lastInter.EndTime)
                {
                    lastInter.EndTime = currTime;
                    if (data.Value.Bool == true)
                    {
                        if (lastInter.Data == BoolInterval.StateEnum.BI_True)
                        {
                            // values match, no need to add a new interval. Re-add it to the runtimeDBs open list
                            db._OpenSignals.Add(this);
                            return;
                        }
                    }
                    else
                    {
                        if (lastInter.Data == BoolInterval.StateEnum.BI_False)
                        {
                            // values match, no need to add a new interval. Re-add it to the runtimeDBs open list
                            db._OpenSignals.Add(this);
                            return;
                        }
                    }
                }

                // create a new interval
                var currInter = new BoolInterval(data.Value.Bool, currTime, currTime);
                Values.Add(currInter);
                db._OpenSignals.Add(this);
            }

            public override void BumpEndTime(TimeSpan newEnd)
            {
                Values[Values.Count - 1].EndTime = newEnd;
            }

            public List<BoolInterval> Values = new List<BoolInterval>();

            // Generates a list of signal intervals within a query interval. The generated intervals
            // will cover at least the query interval and possibly more. Also any gaps
            // will be filled by an N/A with the appropriate start and stop time values.
            public IEnumerable<BoolInterval> GenerateCoveringIntervals(Interval query)
            {
                int startIndex, stopIndex;
                Values.FindIndicesForInterval(query, out startIndex, out stopIndex);

                if (query.StartTime < Values[startIndex].StartTime)
                {
                    yield return new BoolInterval(startIndex > 0 ? Values[startIndex - 1].EndTime : query.StartTime, Values[startIndex].StartTime);
                }
                for(int i = startIndex; i < stopIndex; i++)
                {
                    yield return Values[i];
                    if (i < stopIndex-1 && Values[i].EndTime != Values[i+1].StartTime)
                    {
                        yield return new BoolInterval(Values[i].EndTime, Values[i + 1].StartTime);
                    }
                }
                if (query.EndTime > Values[stopIndex-1].EndTime)
                {
                    yield return new BoolInterval(Values[stopIndex - 1].EndTime, stopIndex < Values.Count ? Values[stopIndex].StartTime : query.EndTime);
                }
            }

            public BoolInterval.StateEnum ValueAtTime(TimeSpan t)
            {
                int index = Values.BinarySearch2((x) => (x.EndTime < t));
                if (index < Values.Count && Values[index].Contains(t))
                {
                    return Values[index].Data;
                }
                return BoolInterval.StateEnum.BI_Unknown;
            }
        }

        public class FloatSignal : Signal
        {
            public override void AddData(TimeSpan currTime, SignalSetMessage.SignalData data, RuntimeDatabase db)
            {
                Debug.Assert(Type == data.Type, "Signals type changed");
                Debug.Assert(Writer == data.Writer, "Don't support multiple writers within one parent yet");

                var lastInter = Values[Values.Count - 1];
                if (currTime == lastInter.EndTime)
                {
                    if (data.Value.Float == lastInter.Data)
                    {
                        // values match, no need to add a new interval. Re-add it to the runtimeDBs open list
                        db._OpenSignals.Add(this);
                        return;
                    }
                }

                var currInter = new FloatInterval(data.Value.Float, currTime, currTime);
                Values.Add(currInter);
                db._OpenSignals.Add(this);

                Min = Math.Min(data.Value.Float, Min);
                Max = Math.Max(data.Value.Float, Max);
            }

            public override void BumpEndTime(TimeSpan newEnd)
            {
                Values[Values.Count - 1].EndTime = newEnd;
            }

            public List<FloatInterval> Values = new List<FloatInterval>();

            public float Min = float.MaxValue;
            public float Max = float.MinValue;

            // Generates a list of signal intervals within a query interval. The generated intervals
            // will cover at least the query interval and possibly more. Also any gaps
            // will be filled by an N/A with the appropriate start and stop time values.
            public IEnumerable<FloatInterval> GenerateCoveringIntervals(Interval query)
            {
                int startIndex, stopIndex;
                Values.FindIndicesForInterval(query, out startIndex, out stopIndex);

                if (query.StartTime < Values[startIndex].StartTime)
                {
                    yield return new FloatInterval(startIndex > 0 ? Values[startIndex - 1].EndTime : query.StartTime, Values[startIndex].StartTime);
                }
                for (int i = startIndex; i < stopIndex; i++)
                {
                    yield return Values[i];
                    if (i < stopIndex - 1 && Values[i].EndTime != Values[i + 1].StartTime)
                    {
                        yield return new FloatInterval(Values[i].EndTime, Values[i + 1].StartTime);
                    }
                }
                if (query.EndTime > Values[stopIndex - 1].EndTime)
                {
                    yield return new FloatInterval(Values[stopIndex - 1].EndTime, stopIndex < Values.Count ? Values[stopIndex].StartTime : query.EndTime);
                }
            }

            public float ValueAtTime(TimeSpan t)
            {
                int index = Values.BinarySearch2((x) => (x.EndTime < t));
                if (index < Values.Count && Values[index].Contains(t))
                {
                    return Values[index].Data;
                }
                return float.NaN;
            }
        }

        public class StringSignal : Signal
        {
            public override void AddData(TimeSpan currTime, SignalSetMessage.SignalData data, RuntimeDatabase db)
            {
                Debug.Assert(Type == data.Type, "Signals type changed");
                Debug.Assert(Writer == data.Writer, "Don't support multiple writers within one parent yet");

                var lastInter = Values[Values.Count - 1];
                if (currTime == lastInter.EndTime)
                {
                    if (data.String == lastInter.Data)
                    {
                        // values match, no need to add a new interval. Re-add it to the runtimeDBs open list
                        db._OpenSignals.Add(this);
                        return;
                    }
                    else
                    {
                        IntervalClosed.Raise(this, lastInter);
                    }
                }

                var currInter = new StringInterval(data.String, currTime, currTime);
                Values.Add(currInter);
                IntervalAdded.Raise(this, currInter);
                db._OpenSignals.Add(this);
            }

            public override void BumpEndTime(TimeSpan newEnd)
            {
                Values[Values.Count - 1].EndTime = newEnd;
                IntervalExtended.Raise(this, Values[Values.Count - 1]);
            }

            public List<StringInterval> Values = new List<StringInterval>();

            public event EventHandler<SimpleEventArgs<StringInterval>> IntervalAdded;
            public event EventHandler<SimpleEventArgs<StringInterval>> IntervalExtended;
            public event EventHandler<SimpleEventArgs<StringInterval>> IntervalClosed;

            // Generates a list of signal intervals within a query interval. The generated intervals
            // will cover at least the query interval and possibly more. Also any gaps
            // will be filled by an N/A with the appropriate start and stop time values.
            public IEnumerable<StringInterval> GenerateCoveringIntervals(Interval query)
            {
                int startIndex, stopIndex;
                Values.FindIndicesForInterval(query, out startIndex, out stopIndex);

                if (query.StartTime < Values[startIndex].StartTime)
                {
                    yield return new StringInterval(startIndex > 0 ? Values[startIndex - 1].EndTime : query.StartTime, Values[startIndex].StartTime);
                }
                for (int i = startIndex; i < stopIndex; i++)
                {
                    yield return Values[i];
                    if (i < stopIndex - 1 && Values[i].EndTime != Values[i + 1].StartTime)
                    {
                        yield return new StringInterval(Values[i].EndTime, Values[i + 1].StartTime);
                    }
                }
                if (query.EndTime > Values[stopIndex - 1].EndTime)
                {
                    yield return new StringInterval(Values[stopIndex - 1].EndTime, stopIndex < Values.Count ? Values[stopIndex].StartTime : query.EndTime);
                }
            }

            public string ValueAtTime(TimeSpan t)
            {
                int index = Values.BinarySearch2((x) => (x.EndTime < t));
                if (index < Values.Count && Values[index].Contains(t))
                {
                    return Values[index].Data;
                }
                return null;
            }
        }

        public void Add(SignalSetMessage msg, SignalSetMessage.SignalData data, RuntimeDatabase db) 
        {
            Signal sig = FindSignal(data.Id);
            Debug.Assert(msg.UpdatePass == db.FrameTicks[db.FrameTicks.Count - 1].UpdatePass);
            var time = db.FrameTicks[db.FrameTicks.Count - 1].WallTime;
            if (sig == null)
            {
                sig = CreateSignal(time, data, db);
                Signals.Add(sig);
            }
            else
            {
                sig.AddData(time, data, db);
            }
        }

        public List<Signal> Signals = new List<Signal>();

        public Signal FindSignal(uint id)
        {
            foreach(var sig in Signals)
            {
                if (sig.Id == id)
                {
                    return sig;
                }
            }
            return null;
        }

        Signal CreateSignal(TimeSpan time, SignalSetMessage.SignalData data, RuntimeDatabase db)
        {
            Signal sig = null;
            switch(data.Type)
            {
                case Parameter.Real:
                    {
                        var floatSig = new FloatSignal();
                        FloatInterval firstInterval = new FloatInterval(data.Value.Float, time, time);
                        floatSig.Values.Add(firstInterval);
                        sig = floatSig;
                    }
                    break;
                case Parameter.Boolean:
                    {
                        var boolSig = new BoolSignal();
                        BoolInterval firstInterval = new BoolInterval(data.Value.Bool, time, time);
                        boolSig.Values.Add(firstInterval);
                        sig = boolSig;
                    }
                    break;
                case Parameter.Animation:
                case Parameter.Clip:
                case Parameter.Expressions:
                case Parameter.Filter:
                case Parameter.Frame:
                case Parameter.ParameterizedMotion:
                    {
                        var stringSig = new StringSignal();
                        StringInterval firstInterval = new StringInterval(data.String, time, time);
                        stringSig.Values.Add(firstInterval);
                        sig = stringSig;
                    }
                    break;
                default:
                    throw new NotImplementedException(String.Format("Can't add signals of type {0} to a signal set yet", data.Type));
            }
            sig.Id = data.Id;
            sig.Type = data.Type;
            sig.Writer = data.Writer;
            db._OpenSignals.Add(sig);
            return sig;
        }

    }


    // What kind of queries do we need to do on the runtime db?
    // Not counting signals, what do we need for states and transitions?
    // For the timeline view, need to query for state activations (and transitions) 
    // between two times. 
    // Also need to filter it to only show active states.
    // To hide and show child states do we need to query for each state node
    // what activations it had?

    public class RuntimeDatabase
    {

        public RuntimeDatabase(StartDebuggingNetworkMessage msg)
        {
            NumStatebitWords = msg.StatebitWords;
            NumFlags = msg.NumFlags;
            FlagOffset = msg.FlagOffset;
            NumRequests = msg.NumRequests;
            RequestOffset = msg.RequestOffset;
        }

        public void Reset()
        {
            Disconnect();

            _Nodes.Clear();
            _OpenStateSet.Clear();
            _StateSet.Clear();
            _OpenTransitionSet.Clear();
            _TransitionSet.Clear();
            _StateDag.Reset();
            FrameTicks.Clear();
            GlobalSignalHistory = new SignalHistory();
            _OpenSignals.Clear();
            StartTime = TimeSpan.MinValue;
            LatestTime = TimeSpan.MinValue;
        }

        public void ConnectTo(Database db, Link link)
        {
            if (link == null)
            {
                Console.WriteLine("No database link for db {0}", db);
                return;
            }

            link.StartTransition += HandleStartTransition;
            link.StopTransition += HandleStopTransition;
            link.StartStateSet += HandleStartStateSet;
            link.StopStateSet += HandleStopStateSet;
            link.UpdateRoot += HandleUpdateRoot;
            link.SignalSet += HandleSignalSet;

            _Database = db;
            _Link = link;
        }

        public void Disconnect()
        {
            if (_Link == null)
            {
                return;
            }
            _Link.StartTransition -= HandleStartTransition;
            _Link.StopTransition -= HandleStopTransition;
            _Link.StartStateSet -= HandleStartStateSet;
            _Link.StopStateSet -= HandleStopStateSet;
            _Link.UpdateRoot -= HandleUpdateRoot;
            _Link.SignalSet -= HandleSignalSet;
            _Database = null;
            _Link = null;
        }


        protected void StartStateList(IEnumerable<RuntimeStateInstanceAndParent> states, TimeSpan time)
        {
            foreach (var nodeId in states)
            {
                StateInterval stateInter = StateInterval.CreateWithKnownStart(nodeId.JustInstance(), time);
                stateInter.StartTime = time;
                _OpenStateSet.Add(stateInter);
                _StateSet.Add(stateInter);
                StateDagNode child = _StateDag.Add(nodeId.State, nodeId.Parent);
                if (child != null)
                {
                    StateDagChanged.Raise(this, new StateDagEventArgs(_StateDag, child));
                }
                StateIntervalAdded.Raise(this, stateInter);
            }
        }

        protected void StopStateList(IEnumerable<RuntimeStateInstance> states, TimeSpan time)
        {
            foreach (var nodeId in states)
            {
                int? stateIndex = FindFirstOpenStateIndex(nodeId, false);
                if (stateIndex != null)
                {
                    var stateInter = _OpenStateSet[(int)stateIndex];
                    stateInter.StartedTransOutTime = stateInter.EndTime = time;
                    _OpenStateSet.RemoveAt((int)stateIndex);
                    StateIntervalChanged.Raise(this, stateInter);

                    if (stateInter.TransitionOut != null) // if we were transitioning somewhere, stop the transition too
                    {
                        stateInter.TransitionOut.EndTime = time;
                        _OpenTransitionSet.Remove(stateInter.TransitionOut);
                        TransitionIntervalChanged.Raise(this, stateInter.TransitionOut);
                    }
                }
                else
                {
                    Debug.Assert(false, String.Format("Couldn't find an open state to close. Node: {0}", nodeId));
                    var stateInter = StateInterval.CreateWithKnownEnd(nodeId, time);
                    _StateSet.Add(stateInter);
                    StateDagNode child = _StateDag.Add(nodeId.State, RuntimeStateId.RootState);
                    if (child != null)
                    {
                        StateDagChanged.Raise(this, new StateDagEventArgs(_StateDag, child));
                    }
                    StateIntervalAdded.Raise(this, stateInter);
                }
            }
        }


        protected void HandleStartTransition(object sender, SimpleEventArgs<LinkNetworkMessageBase> e)
        {
            lock (LockObject)
            {

                _Nodes.Add(e.Payload);

                StartTransitionMessage msg = e.Payload as StartTransitionMessage;

                Debug.Assert(msg.UpdatePass == FrameTicks[FrameTicks.Count - 1].UpdatePass, String.Format("Message UpdatePass number ({0}) didn't match the most recent UpdateRoot pass number ({1})", msg.UpdatePass, FrameTicks[FrameTicks.Count - 1].UpdatePass));

                // Start the 'to' StateInterval
                StateInterval stateInterTo = StateInterval.CreateWithKnownStart(msg.To, LatestTime);
                _OpenStateSet.Add(stateInterTo);
                _StateSet.Add(stateInterTo);

                // Start a new transition interval
                TransitionInterval transInter = TransitionInterval.CreateWithKnownStart(msg.Via, LatestTime);
                _OpenTransitionSet.Add(transInter);
                _TransitionSet.Add(transInter);

                // Find the 'from' state interval
                StateInterval stateInterFrom = FindFirstOpenStateInterval(msg.From, true);
                Debug.Assert(stateInterFrom != null, "Couldn't find the state we're transitioning from");

                // Connect the intervals.
                if (stateInterFrom != null)
                {
                    stateInterFrom.StartedTransOutTime = LatestTime;
                    stateInterFrom.TransitionOut = transInter;
                }
                transInter.StateFrom = stateInterFrom;
                transInter.StateTo = stateInterTo;
                stateInterTo.TransitionIn = transInter;

                StateDagNode updatedDag = _StateDag.AddSibling(msg.From.State, msg.To.State);

                if (updatedDag != null)
                {
                    StateDagChanged.Raise(this, new StateDagEventArgs(_StateDag, updatedDag));
                }
                // Call all the callbacks
                StateIntervalAdded.Raise(this, stateInterTo);

                if (stateInterFrom != null)
                {
                    StateIntervalChanged.Raise(this, stateInterFrom);
                }

                TransitionIntervalAdded.Raise(this, transInter);

                // Start up any descendant states
                StartStateList(msg.Descendants, LatestTime);
            }
        }

        protected void HandleStopTransition(object sender, SimpleEventArgs<LinkNetworkMessageBase> e)
        {
            lock (LockObject)
            {
                _Nodes.Add(e.Payload);

                StopTransitionMessage msg = e.Payload as StopTransitionMessage;

                // Process the transition
                // NOTE: There is an implicit assumption here (well, explicit now that I write this) that the first - i.e. oldest - 
                // transition with the same from, to, and via data is really the one we want to close. That is, any transition A->B via trans 1
                // will take the same amount of time to complete, so if we see an A->B via 1 that finished, it must have been 
                // the first one that started. If we ever have variable length transitions we'll need to revisit this - maybe pass 
                // transition GUIDs or something along with each start and stop message.
                int? transIndex = FindFirstOpenTransitionIndex(msg.From, msg.To, msg.Via);
                TransitionInterval transInter = null;

                bool addedTrans = false;

                if (transIndex != null)
                {
                    transInter = _OpenTransitionSet[(int)transIndex];
                    transInter.EndTime = LatestTime;
                    _OpenTransitionSet.RemoveAt((int)transIndex);
                }
                else
                {
                    transInter = TransitionInterval.CreateWithKnownEnd(msg.Via, LatestTime);
                    _TransitionSet.Add(transInter);
                    addedTrans = true;
                }

                StateInterval stateInter = transInter.StateFrom;

                // Process the state we've transitioned from
                if (stateInter == null)
                {
                    Debug.Assert(false, "Couldn't find a state interval from the transition interval");
                    stateInter = FindFirstOpenStateInterval(msg.From, true);                
                }
                if (stateInter != null)
                {
                    Debug.Assert(stateInter.TransitionOut != null && stateInter.TransitionOut.StateFrom != null && stateInter.TransitionOut.StateFrom.StateId == msg.From, "Inconsistent Begin/End transition messages");
                    Debug.Assert(stateInter.TransitionOut != null && stateInter.TransitionOut.StateTo != null && stateInter.TransitionOut.StateTo.StateId == msg.To, "Inconsistent Begin/End transition messages");
                    stateInter.EndTime = LatestTime;
                    _OpenStateSet.Remove(stateInter);
                    StateIntervalChanged.Raise(this, stateInter);
                }
                else
                {
                    // we found a stop without a start, add it to the closed set but
                    // mark it NO_START
                    stateInter = StateInterval.CreateWithKnownEnd(msg.From, LatestTime);
                    _StateSet.Add(stateInter);
                    StateDagNode updatedNode = _StateDag.AddSibling(msg.From.State, msg.To.State);

                    stateInter.TransitionOut = transInter;
                    transInter.StateFrom = stateInter;

                    if (updatedNode != null)
                    {
                        StateDagChanged.Raise(this, new StateDagEventArgs(_StateDag, updatedNode));
                    }
                    StateIntervalAdded.Raise(this, stateInter);
                }

                // Process the state we're transitioning to
                stateInter = transInter.StateTo;
                if (stateInter == null)
                {
                    Debug.Assert(false, "Couldn't find outgoing state interval from transition interval");
                    stateInter = FindFirstOpenStateInterval(msg.To, true);
                }
                Debug.Assert(stateInter != null, "Couldn't find the state we're transitioning to");
                if (stateInter != null)
                {
                    stateInter.FinishedTransInTime = LatestTime;
                    StateIntervalChanged.Raise(this, stateInter);
                }

                // Finally send off the transition message (do this after the state messages, since thats what start transition does)
                if (addedTrans)
                {
                    TransitionIntervalAdded.Raise(this, transInter);
                }
                else
                {
                    TransitionIntervalChanged.Raise(this, transInter);
                }

                // Stop any descendant states
                StopStateList(msg.Descendants, LatestTime);
            }
        }


        protected void HandleStartStateSet(object sender, SimpleEventArgs<LinkNetworkMessageBase> e)
        {
            lock (LockObject)
            {
                _Nodes.Add(e.Payload);

                StartStateSetMessage msg = e.Payload as StartStateSetMessage;
                StartStateList(msg.States, LatestTime);
            }
        }

        protected void HandleStopStateSet(object sender, SimpleEventArgs<LinkNetworkMessageBase> e)
        {
            lock (LockObject)
            {
                _Nodes.Add(e.Payload);

                StopStateSetMessage msg = e.Payload as StopStateSetMessage;
                StopStateList(msg.States, LatestTime);
            }
        }

        protected void HandleUpdateRoot(object sender, SimpleEventArgs<LinkNetworkMessageBase> e)
        {
            lock (LockObject)
            {
                _Nodes.Add(e.Payload);

                UpdateRootMessage msg = e.Payload as UpdateRootMessage;
                TimeSpan dt = TimeSpan.FromSeconds(msg.Dt);
                LatestTime += dt;
                FrameTicks.Add(new TickInfo(msg.UpdatePass, LatestTime));

                // bump the times for all the open signal histories
                foreach(var sig in _OpenSignals)
                {
                    sig.BumpEndTime(LatestTime);
                }
                _OpenSignals.Clear();

                UpdateInfo info = new UpdateInfo();
                Debug.Assert(msg.StatebitWords.Length == NumStatebitWords);

                info.Time = LatestTime;
                info.Statebits = msg.StatebitWords;

                _UpdateInfo.Add(info);

            }
        }

        protected void HandleSignalSet(object sender, SimpleEventArgs<LinkNetworkMessageBase> e)
        {
            lock (LockObject)
            {
                _Nodes.Add(e.Payload);

                SignalSetMessage msg = (SignalSetMessage)e.Payload;

                foreach (var sig in msg.Signals)
                {
                    if (sig.Parent.State == RuntimeStateId.RootState)
                    {
                        GlobalSignalHistory.Add(msg, sig, this);
                    }
                    else
                    {
                        var inter = FindFirstOpenStateInterval(sig.Parent, false);
                        Debug.Assert(inter != null, String.Format("Couldn't find open state interval {0}", sig.Parent));
                        inter.SignalHistory.Add(msg, sig, this);
                    }
                }
            }
        }

        public event EventHandler<SimpleEventArgs<Interval>> StateIntervalChanged;
        public event EventHandler<SimpleEventArgs<Interval>> TransitionIntervalChanged;

        public event EventHandler<SimpleEventArgs<Interval>> StateIntervalAdded;
        public event EventHandler<SimpleEventArgs<Interval>> TransitionIntervalAdded;

        public class StateDagEventArgs : EventArgs
        {
            public StateDagEventArgs(StateDag dag, StateDagNode child)
            {
                StateDag = dag;
                ChildNode = child;
            }
            public StateDag StateDag;
            public StateDagNode ChildNode;
        }

        public event EventHandler<StateDagEventArgs> StateDagChanged;

        protected int? FindFirstOpenStateIndex(RuntimeStateInstance stateId, bool skipTransitioningStates)
        {
            for (int i = 0; i < _OpenStateSet.Count; i++)
            {
                // Check for matching IDs, but also ignore any state interval who has a transition out - that
                // one is essentially 'spoken for' already by an in-progress transition
                if (_OpenStateSet[i].StateId == stateId && (!skipTransitioningStates || _OpenStateSet[i].TransitionOut == null))
                {
                    return i;
                }
            }
            return null;
        }

        protected StateInterval FindFirstOpenStateInterval(RuntimeStateInstance stateId, bool skipTransitioningStates)
        {
            int? i = FindFirstOpenStateIndex(stateId, skipTransitioningStates);
            return i != null ? _OpenStateSet[(int)i] : null;
        }

        protected int? FindFirstOpenTransitionIndex(RuntimeStateInstance fromState, RuntimeStateInstance toState, ushort via)
        {
            for(int i = 0; i < _OpenTransitionSet.Count; i++)
            {
                StateInterval stateFrom = _OpenTransitionSet[i].StateFrom;
                StateInterval stateTo = _OpenTransitionSet[i].StateTo;
                if (stateFrom != null && stateFrom.StateId == fromState &&
                    stateTo != null && stateTo.StateId == toState &&
                    via == _OpenTransitionSet[i].TransId)
                {
                    return i;
                }
            }
            return null;
        }

        protected TransitionInterval FindFirstOpenTransitionInterval(RuntimeStateInstance fromState, RuntimeStateInstance toState, ushort via)
        {
            int? i = FindFirstOpenTransitionIndex(fromState, toState, via);
            return i != null ? _TransitionSet[(int)i] : null;
        }

        // convenience functions for doing queries
        IEnumerable<StateInterval> StatesInInterval(Interval i)
        {
            return StateSet.Where(s => s.Overlaps(i));
        }

        IEnumerable<TransitionInterval> TransitionsInInterval(Interval i)
        {
            return TransitionSet.Where(t => t.Overlaps(i));
        }

        // Use this one for thread safety
        public void GetTimeSpansInSeconds(out double startTime, out double latestTime)
        {
            lock(LockObject)
            {
                startTime = StartTime.TotalSeconds;
                latestTime = LatestTime.TotalSeconds;
            }
        }

        public List<StateInterval> StateSet
        {
            get { return _StateSet; }
        }

        public List<TransitionInterval> TransitionSet
        {
            get { return _TransitionSet; }
        }

        public StateDag StateDag {
            get { return _StateDag; }
        }

        public TimeSpan StartTime { get; protected set; }
        public TimeSpan LatestTime { get; protected set; }

        // Use with caution!
        public object LockObject
        {
            get { return _Lock; }
        }

        Database                _Database;
        Link                    _Link;

        List<LinkNetworkMessageBase> _Nodes = new List<LinkNetworkMessageBase>();

        List<StateInterval> _OpenStateSet = new List<StateInterval>();
        List<StateInterval> _StateSet = new List<StateInterval>();

        List<TransitionInterval> _OpenTransitionSet = new List<TransitionInterval>();
        List<TransitionInterval> _TransitionSet = new List<TransitionInterval>();

        public SignalHistory GlobalSignalHistory = new SignalHistory();

        public struct TickInfo
        {
            public TickInfo(uint updatePass, TimeSpan wallTime)
            {
                UpdatePass = updatePass;
                WallTime = wallTime;
            }
            public uint UpdatePass;
            public TimeSpan WallTime;
        }

        public List<TickInfo> FrameTicks = new List<TickInfo>();

        public List<SignalHistory.Signal> _OpenSignals = new List<SignalHistory.Signal>();

        // These should be constant for every frame
        public int NumStatebitWords { get; protected set; }
        public int NumFlags { get; protected set; }
        public int FlagOffset { get; protected set; }
        public int NumRequests { get; protected set; }
        public int RequestOffset { get; protected set; }

        StateDag _StateDag = new StateDag();

        public class UpdateInfo
        {
            public TimeSpan Time;
            public UInt32[] Statebits;

            public bool GetBit(int i)
            {
                return (Statebits[i >> 5] & (1 << (i & 0x1f))) != 0;
            }
        }

        List<UpdateInfo> _UpdateInfo = new List<UpdateInfo>();

        public int GetStatebitIndexForFlag(int flag)
        {
            return FlagOffset + flag;
        }

        public int GetStatebitIndexForRequest(int request)
        {
            return RequestOffset + request;
        }

        class UpdateInfoSearch : IComparer<UpdateInfo>
        {
            public int Compare(UpdateInfo x, UpdateInfo y) { return x.Time.CompareTo(y.Time);  }
        }

        // Given an interval, finds the frame indices that correspond to the interval. Use these for any frame-based lookups.
        // Frame[start] would be at or before inter.StartTime, and Frame[end] would be at or after inter.EndTime
        public void FindFrameIndicesForInterval(Interval inter, out int start, out int end)
        {
            UpdateInfo startQuery = new UpdateInfo();
            startQuery.Time = inter.StartTime;
            
            UpdateInfo endQuery = new UpdateInfo();
            endQuery.Time = inter.EndTime;

            UpdateInfoSearch comparer = new UpdateInfoSearch();
                
            // Return value for BinarySearch is a little weird:
            // The zero-based index of item in the sorted List, if item is found; otherwise, a negative number 
            // that is the bitwise complement of the index of the next element that is larger than item or, 
            // if there is no larger element, the bitwise complement of Count. 

            start = _UpdateInfo.BinarySearch(startQuery, comparer);

            // Make it the index of the next smaller item, or 0 if we're off the low end of the list
            start = start < 0 ? (~start == _UpdateInfo.Count ? 0 : (~start - 1)) : start;
            start = Math.Max(0, start);

            end = _UpdateInfo.BinarySearch(endQuery, comparer);

            // Make it the index of the next larger item, or Count if we're off the high end of the list
            end = end < 0 ? ~end : end;
            end = Math.Min(end, _UpdateInfo.Count - 1);
        }

        public IEnumerable<BoolInterval> GenerateIntervalsForStatebit(int bit, int startIndex, int endIndex)
        {
            BoolInterval currInterval = new BoolInterval();
            currInterval.Data = _UpdateInfo[startIndex].GetBit(bit) ? BoolInterval.StateEnum.BI_True : BoolInterval.StateEnum.BI_False;
            currInterval.StartTime = _UpdateInfo[startIndex].Time;

            for(int i = startIndex + 1; i < endIndex-1; i++)
            {
                BoolInterval.StateEnum thisFrameData = _UpdateInfo[i].GetBit(bit) ? BoolInterval.StateEnum.BI_True : BoolInterval.StateEnum.BI_False;
                currInterval.EndTime = _UpdateInfo[i].Time; // bump time ahead
                if (currInterval.Data != thisFrameData)
                {
                    yield return currInterval;
                    currInterval = new BoolInterval();
                    currInterval.Data = thisFrameData;
                    currInterval.StartTime = _UpdateInfo[i].Time;
                }
            }
            currInterval.EndTime = _UpdateInfo[endIndex-1].Time;
            yield return currInterval;
        }

        private object _Lock = new object();
    }
}
