﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class FilterReference : ReferenceSignal, ISerializable
    {
        public FilterReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public FilterReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}