﻿namespace New.Move.Core
{
    public interface IReferredRequest
    {
        ReferenceRequest CreateReference();
    }
}