﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class IntReference : ReferenceSignal, ISerializable
    {
        public IntReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public IntReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override Parameter GetParameter()
        {
            return Parameter.Int;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}