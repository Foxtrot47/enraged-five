﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

using Move.Core;
using Move.Utils;
using System.Xml;
using System.Globalization;

namespace New.Move.Core
{
    [Serializable]
    public class FilenameLogicalProperty : ILogicProperty, ISerializable
    {
        public FilenameLogicalProperty()
        {
            Id = Guid.NewGuid();
        }

        public FilenameLogicalProperty(FilenameLogicalProperty other)
        {
            Name = other.Name;
            Value = other.Value;
            Id = Guid.NewGuid();
        }

        public FilenameLogicalProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        string _Value;
        public string Value 
        {
            get { return _Value; }
            set
            {
                if (_Value != value)
                {
                    _Value = value;
                    OnPropertyChanged("Value");
                }
            }
        }

        public ILogic Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Value = "Value";
            public static string Parent = "Parent";
        }

        public FilenameLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Value = (string)info.GetValue(SerializationTag.Value, typeof(string));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Value, Value);
            info.AddValue(SerializationTag.Parent, Parent);
        }

        public object Clone()
        {
            return new FilenameLogicalProperty(this);
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="FilenameLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public FilenameLogicalProperty(XmlReader reader, Database database, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Value = reader.GetAttribute("Value");
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Value", this.Value);
        }
    }
}