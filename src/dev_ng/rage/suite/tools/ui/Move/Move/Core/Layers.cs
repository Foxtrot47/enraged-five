﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace Rage.Move.Core
{
    public interface ILayerable
    {
        Guid Id { get; }
        Guid Layer { get; set; }
    }

    public delegate void EnabledChangedEventHandler(bool enabled);
    public delegate void LayerChangedEventHandler();

    public class Layers
    {
        internal delegate void LayerRemovedEventHandler();

        public class Layer
        {
            internal Layer(string name)
            {
                Name = name;
                Id = Guid.NewGuid();

                Enabled = true;

                Random random = new Random();
                Color = Color.FromRgb(
                    (byte)random.Next(255), 
                    (byte)random.Next(255), 
                    (byte)random.Next(255));
            }

            public void Add(ILayerable item)
            {
                item.Layer = Id;

                if (!_Items.ContainsKey(item.Id))
                    _Items.Add(item.Id, item);
            }

            public void Remove(ILayerable item)
            {
                item.Layer = Guid.Empty;

                if (_Items.ContainsKey(item.Id))
                    _Items.Remove(item.Id);
            }

            public void Remove()
            {
                foreach (ILayerable item in _Items.Values)
                    item.Layer = Guid.Empty;

                if (LayerRemoved != null)
                    LayerRemoved();
            }

            public bool Contains(ILayerable item)
            {
                return _Items.ContainsKey(item.Id);
            }

            Dictionary<Guid, ILayerable> _Items = new Dictionary<Guid, ILayerable>();

            public EnabledChangedEventHandler EnabledChanged;
            internal LayerRemovedEventHandler LayerRemoved;
         
            public string Name { get; set; }
            public Guid Id { get; set; }

            bool _Enabled;
            public bool Enabled 
            { 
                get { return _Enabled; }
                set
                {
                    _Enabled = value;

                    if (EnabledChanged != null)
                        EnabledChanged(_Enabled);
                }
            }
            public Color Color { get; set; }
        }

        public Layers()
        {
        }

        public Layer Create()
        {
            Layer layer = new Layer(String.Format("Layer{0}", _Items.Count + 1));
            layer.LayerRemoved += new LayerRemovedEventHandler(
                delegate() { _Items.Remove(layer); });
                
            _Items.Add(layer);

            return layer;
        }

        public Layer Find(Guid id)
        {
            foreach (Layer layer in _Items)
            {
                if (layer.Id == id)
                    return layer;
            }

            return null;
        }

        ObservableCollection<Layer> _Items = new ObservableCollection<Layer>();
        public ObservableCollection<Layer> Items
        {
            get { return _Items; }
        }
    }
}
