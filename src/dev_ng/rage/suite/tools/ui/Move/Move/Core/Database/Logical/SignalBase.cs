﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml;

using Move.Core;
using Move.Core.Restorable;
using Move.Utils;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    public abstract class SignalBase : RestorableObject, ILogic, ISelectable, ISerializable, INotifyPropertyChanged
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Network"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        protected SignalBase(XmlReader reader, Database database, ITransitional parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent.Id;
            this.Database = database;
            this.Deserialise(reader, parent);
        }

        protected SignalBase(Signal signal)
        {
            Signal = signal;
            Signal.PropertyChanged += new PropertyChangedEventHandler(Signal_PropertyChanged);
            Id = Guid.NewGuid();

            Anchor = new SignalAnchor(this);

            Connections = new NoResetObservableCollection<Connection>();
        }

        public void OnLoadComplete()
        {

        }

        void Signal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name" || e.PropertyName == "Enabled")
            {
                NotifyPropertyChanged(e.PropertyName);
            }
        }

        public override string ToString()
        {
            if (this.Signal == null)
            {
                return base.ToString();
            }

            return Signal.Name;
        }

        public string Name
        {
            get
            {
                if (this.Signal == null)
                {
                    return string.Empty;
                }

                return Signal.Name;
            }
            set 
            { 
                if(value != null && this.Signal != null)
                {
                    Signal.Name = value;
                }
            }
        }

        public string AdditionalText
        {
            get { return ""; }
            set { }
        }

        public string Type
        {
            get
            {
                if (this.Signal == null)
                {
                    return string.Empty;
                }

                return Signal.Type;
            }
            set
            {

            }
        }

        public bool Enabled
        {
            get
            {
                if (this.Signal == null)
                {
                    return false;
                }

                return Signal.Enabled;
            }
            set
            {
                if (this.Signal == null)
                {
                    return;
                }

                Signal.Enabled = value;
            }
        }

        public Point Position { get; set; }

        public Database Database { get; set; }
        public Guid Id { get; set; }
        public Guid Parent { get; set; }

        public IDesc Desc { get; internal set; }

        public object Tag { get; set; }

        public Signal Signal { get; set; }

        public SignalAnchor Anchor { get; set; }

        protected abstract string XmlNodeName { get; }

        NoResetObservableCollection<Connection> _Connections;
        public NoResetObservableCollection<Connection> Connections
        {
            get { return _Connections; }
            set { _Connections = value; }
        }

        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            if (IsSelectedChanged != null)
                IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
        }

        public event EventHandler<IsEngagedChangedEventArgs> IsEngagedChanged;
        public void OnEngagedChanged(EngagedChangedAction action)
        {
            if (IsEngagedChanged != null)
                IsEngagedChanged(this, new IsEngagedChangedEventArgs(action));
        }

        public void Dispose()
        {
            if (Parent != Guid.Empty)
            {
                ITransitional parent = Database.Find(Parent) as ITransitional;
                if (parent != null)
                    parent.Remove(this);
            }

            Database.Nodes.Remove(this);
        }

        static class SerializationTag
        {
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Database = "Database";
            public static string Position = "Position";
            public static string Signal = "Signal";
            public static string Anchor = "Anchor";
            public static string Enabled = "Enabled";
        }

        protected SignalBase(SerializationInfo info, StreamingContext context)
        {
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (Guid)info.GetValue(SerializationTag.Parent, typeof(Guid));
            Database = (Database)info.GetValue(SerializationTag.Database, typeof(Database));

            Position = (Point)info.GetValue(SerializationTag.Position, typeof(Point));
           
            Signal = (Signal)info.GetValue(SerializationTag.Signal, typeof(Signal));
            Signal.PropertyChanged += new PropertyChangedEventHandler(Signal_PropertyChanged);

            Anchor = (SignalAnchor)info.GetValue(SerializationTag.Anchor, typeof(SignalAnchor));

            try
            {
                Enabled = (bool)info.GetValue(SerializationTag.Enabled, typeof(bool));
            }
            catch (SerializationException)
            {
                Enabled = true;
            }
        }                                   

        protected void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Database, Database);

            info.AddValue(SerializationTag.Position, Position);
            
            info.AddValue(SerializationTag.Signal, Signal);
            info.AddValue(SerializationTag.Anchor, Anchor);
            info.AddValue(SerializationTag.Enabled, Enabled);
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }

        public abstract void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames);

        object ICloneable.Clone()
        {
            SignalBase signalBase = null;
            return this.Clone(signalBase);
        }

        
        public virtual ISelectable Clone(SignalBase clone)
        {
            System.Type ot = clone.GetType();
            PropertyInfo[] oproperties = ot.GetProperties();

            System.Type t = this.GetType();
            PropertyInfo[] properties = t.GetProperties();

            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo prop = properties[i];
                PropertyInfo oprop = oproperties[i];

                oprop.SetValue(clone, prop.GetValue(this, null), null);
            }

            return (ISelectable)clone;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement(this.XmlNodeName);
            writer.WriteAttributeString("Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            if (this.Signal != null)
            {
                writer.WriteAttributeString("Signal", this.Signal.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.Signal.Name);
            }

            if (this.Anchor != null)
            {
                writer.WriteAttributeString("Anchor", this.Anchor.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, ITransitional parentNode)
        {
            if (reader == null)
            {
                return;
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.Signal = Database.GetSignal(Guid.Parse(signalId));
                if (this.Signal == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.Signal = Database.GetSignal(signalName);
                    if (this.Signal == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}' inside the '{1}' motion tree. This needs fixing before a export is allowed.", signalId, parentNode.Name);
                    }
                }
            }

            string anchorId = reader.GetAttribute("Anchor");
            if (!string.IsNullOrEmpty(anchorId))
            {
                this.Anchor = new SignalAnchor(this);
                this.Anchor.Id = Guid.Parse(anchorId);
            }
            else
            {
                this.Anchor = new SignalAnchor(this);
            }

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            return ids;
        }
    }
}