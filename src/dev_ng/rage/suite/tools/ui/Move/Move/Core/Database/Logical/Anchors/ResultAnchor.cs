﻿namespace New.Move.Core
{
    public abstract class ResultAnchor : AnchorBase
    {
        public ResultAnchor(string name)
            : base(name) { }

        public ResultAnchor() : base(null) { }
    }
}