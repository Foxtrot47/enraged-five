﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Windows;
using System.Xml;

using Move.Core;
using Move.Core.Restorable;
using Move.Utils;

using Rockstar.MoVE.Services;
using RSG.ManagedRage;

namespace New.Move.Core
{
    /// <summary>
    /// Provides a abstract base class for all of the logical nodes that can exist within
    /// a associated database.
    /// </summary>
    public abstract class Logic : RestorableObject, ILogic, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the
        /// <see cref="AdditionalText"/> property.
        /// </summary>
        protected const string AdditionalTextSerializationTag = "AdditionalText";

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field used for the <see cref="AdditionalText"/> property.
        /// </summary>
        private string additionalText;

        /// <summary>
        /// The private field used for the <see cref="Enabled"/> property.
        /// </summary>
        private bool enabled = true;

        /// <summary>
        /// The private field used for the <see cref="Position"/> property.
        /// </summary>
        private Point position;

        /// <summary>
        /// The private field used for the <see cref="Parent"/> property.
        /// </summary>
        private Guid parent;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialsies a new instance of the <see cref="New.Move.Core.Logic"/> class.
        /// </summary>
        /// <param name="desc">
        /// The descriptive creation object for this instance.
        /// </param>
        protected Logic(IDesc desc)
        {
            Id = Guid.NewGuid();
            Desc = desc;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Logic"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public Logic(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));

            Position = (Point)info.GetValue(SerializationTag.Position, typeof(Point));

            Database = (Database)info.GetValue(SerializationTag.Database, typeof(Database));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (Guid)info.GetValue(SerializationTag.Parent, typeof(Guid));
            Enabled = (bool)info.GetValue(SerializationTag.Enabled, typeof(bool));
        }
        #endregion

        public event EventHandler<IsEngagedChangedEventArgs> IsEngagedChanged;
        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;

        #region Properties
        /// <summary>
        /// Gets or sets the name for this node.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets the additional text attached to this node.
        /// </summary>
        public string AdditionalText
        {
            get
            {
                return additionalText;
            }

            set
            {
                if (additionalText != value)
                {
                    additionalText = value;
                    NotifyPropertyChanged("AdditionalText");
                }
            }
        }

        /// <summary>
        /// Gets the name of the type that this node is representing.
        /// </summary>
        public string Type
        {
            get
            {
                return Desc.Name;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this node is currently enabled.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }

            set
            {
                enabled = value;
                NotifyPropertyChanged("Enabled");
            }
        }

        /// <summary>
        /// Gets or sets the x,y coordinates that this node is located at within the
        /// parent diagram. (KILL ME).
        /// </summary>
        public Point Position
        {
            get
            {
                return position;
            }

            set
            {
                if (position != value)
                {
                    position = value;
                    NotifyPropertyChanged("Position");
                }
            }
        }

        /// <summary>
        /// Gets or sets a reference to the root database this node is contained
        /// within. (KILL ME).
        /// </summary>
        public Database Database { get; set; }

        /// <summary>
        /// Gets or sets the unique identifier for this node.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the identifier to the immediate parent node for this node.
        /// </summary>
        public Guid Parent
        {
            get { return parent; }
            set
            {
                if (parent != value)
                {
                    parent = value;
                    NotifyPropertyChanged("Parent");
                }
            }
        }

        /// <summary>
        /// Gets or sets the description object that describes the creates of this object.
        /// </summary>
        public IDesc Desc { get; set; }

        /// <summary>
        /// Gets or sets the generic tag object for this logic node.
        /// </summary>
        public object Tag { get; set; }
        #endregion




        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Position = "Position";
            public static string Parent = "Parent";
            public static string Database = "Database";
            public static string Enabled = "Enabled";
        }



        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #region Methods
        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public abstract void ResetIds(Dictionary<Guid, Guid> map);

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public abstract List<Guid> GetAllGuids();

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public virtual void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement(this.Type);

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", System.Globalization.CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(System.Globalization.CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(System.Globalization.CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        public void OnLoadComplete()
        {
            OnLoadCompleteImpl();
        }

        public override string ToString()
        {
            return name;
        }

        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            this.OnSelectionChanged(action == SelectionChangedAction.Add);
            if (IsSelectedChanged != null)
                IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
        }

        public void OnEngagedChanged(EngagedChangedAction action)
        {
            if (IsEngagedChanged != null)
                IsEngagedChanged(this, new IsEngagedChangedEventArgs(action));
        }

        public virtual void Dispose()
        {
            if (Parent != Guid.Empty)
            {
                ITransitional parent = Database.Find(Parent) as ITransitional;
                if (parent != null)
                    parent.Remove(this);
            }

            Database.Nodes.Remove(this);
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);

            info.AddValue(SerializationTag.Position, Position);

            info.AddValue(SerializationTag.Database, Database);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Enabled, Enabled);
        }

        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Properties = new RestorableCollection();
        }

        public virtual void GetCustomData(SerializationInfo info, StreamingContext context)
        {
        }

        public virtual void SetCustomData(SerializationInfo info, StreamingContext context)
        {
        }

        public void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            hashedNames.Add(Name);

            ExportToXMLImpl(parentNode, hashedNames);
        }

        public abstract void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames);

        object ICloneable.Clone()
        {
            Logic clone = null;
            ISelectable result = this.Clone(clone);
            return result;
        }

        public virtual ISelectable Clone(Logic clone)
        {
            if (clone == null)
            {
                System.Diagnostics.Debug.Assert(clone != null, "Cloned object is null.");
                return null;
            }

            System.Type ot = clone.GetType();
            PropertyInfo[] oproperties = ot.GetProperties();

            System.Type t = this.GetType();
            PropertyInfo[] properties = t.GetProperties();

            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo prop = properties[i];
                PropertyInfo oprop = oproperties[i];

                if (oprop.CanWrite)
                {
                    object value = prop.GetValue(this, null);
                    if (value is Guid)
                        continue;

                    if (oprop.PropertyType == prop.PropertyType)
                        oprop.SetValue(clone, prop.GetValue(this, null), null);
                }
            }

            return (ISelectable)clone;
        }

        protected virtual void OnLoadCompleteImpl()
        {
        }

        protected virtual void OnSelectionChanged(bool selected)
        {
        }
        #endregion Methods
    }
}