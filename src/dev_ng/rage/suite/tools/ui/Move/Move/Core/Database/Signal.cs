﻿namespace New.Move.Core
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml;

    /// <summary>
    /// Defines the different parameter types supported by a signal.
    /// </summary>
    public enum Parameter
    {
        /// <summary>
        /// The signal represents a parameter of unknown type.
        /// </summary>
        None,

        /// <summary>
        /// The signal represents a animation parameter.
        /// </summary>
        Animation,

        /// <summary>
        /// The signal represents a boolean parameter.
        /// </summary>
        Boolean,

        /// <summary>
        /// The signal represents a clip parameter.
        /// </summary>
        Clip,

        /// <summary>
        /// The signal represents a expression parameter.
        /// </summary>
        Expressions,

        /// <summary>
        /// The signal represents a filter parameter.
        /// </summary>
        Filter,

        /// <summary>
        /// The signal represents a frame parameter.
        /// </summary>
        Frame,

        /// <summary>
        /// The signal represents a parameterized motion parameter.
        /// </summary>
        ParameterizedMotion,

        /// <summary>
        /// The signal represents a real parameter.
        /// </summary>
        Real,

        /// <summary>
        /// The signal represents a reference parameter.
        /// </summary>
        Reference,

        /// <summary>
        /// The signal represents a node parameter.
        /// </summary>
        Node,

        /// <summary>
        /// The signal represents a integer parameter.
        /// </summary>
        Int,

        /// <summary>
        /// The signal represents a hash string parameter.
        /// </summary>
        HashString,

        /// <summary>
        /// The signal represents a tag parameter.
        /// </summary>
        Tag,
    }

    /// <summary>
    /// Represents a control parameter that can be defined for a database and referenced
    /// across it as its own node or as a value for a node parameter.
    /// </summary>
    [Serializable]
    public abstract class Signal :
        SourceAnchor, INotifyPropertyChanged, ISerializable
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        protected const string NameSerializationTag = "Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        protected const string IdSerializationTag = "Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Parent"/> property.
        /// </summary>
        protected const string ParentSerializationTag = "Parent";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Enabled"/> property.
        /// </summary>
        protected const string EnabledSerializationTag = "Enabled";

        /// <summary>
        /// The tag that is used to serialise/deserialise the Value property.
        /// </summary>
        protected const string ValueSerializationTag = "Value";

        /// <summary>
        /// The private field used to the <see cref="UseIcon"/> property.
        /// </summary>
        private int usedCount = 0;

        /// <summary>
        /// The private field used for the <see cref="Enabled"/> property.
        /// </summary>
        private bool enabled = true;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string name = string.Empty;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Signal"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the signal.
        /// </param>
        public Signal(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Signal"/> class.
        /// </summary>
        protected Signal()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Signal"/> class as
        /// a copy of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        protected Signal(Signal other)
            : base(other.Name)
        {
            this.enabled = other.enabled;
            this.usedCount = 0;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Signal"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        protected Signal(SerializationInfo info, StreamingContext context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case NameSerializationTag:
                        this.Name = (string)entry.Value;
                        break;
                    case IdSerializationTag:
                        this.Id = (Guid)entry.Value;
                        break;
                    case ParentSerializationTag:
                        this.Parent = (ILogic)entry.Value;
                        break;
                    case EnabledSerializationTag:
                        this.Enabled = (bool)entry.Value;
                        break;
                    default:
                        Trace.TraceWarning(StringTable.UnrecognisedBinaryData);
                        break;
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the value of a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the <see cref="New.Move.Core.IDesc"/> object that describes the creation of
        /// this object.
        /// </summary>
        public abstract IDesc Desc { get; }

        /// <summary>
        /// Gets the image source used for the icon to display to the user if the signal is
        /// currently not used throughout its owning database.
        /// </summary>
        public ImageSource UsedIcon
        {
            get
            {
                if (this.usedCount > 0)
                {
                    return null;
                }
                else
                {
                    BitmapImage bitmapImage = new BitmapImage();

                    try
                    {
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource =
                            Assembly.GetExecutingAssembly().GetManifestResourceStream(
                            "Move.Core.Images.NotUsed.png");
                        bitmapImage.EndInit();
                        return bitmapImage;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this signal is currently enabled.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
                this.NotifyPropertyChanged("Enabled");
            }
        }

        /// <summary>
        /// Gets or sets the name for this signal. This is the name that is referenced by other
        /// nodes that what to use it as a value for a property.
        /// </summary>
        public new string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.NotifyPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets the type of signal this is.
        /// </summary>
        public string Type
        {
            get
            {
                switch (this.GetParameter())
                {
                    case Parameter.Animation:
                        return "Animation";
                    case Parameter.Boolean:
                        return "Boolean";
                    case Parameter.Clip:
                        return "Clip";
                    case Parameter.Expressions:
                        return "Expressions";
                    case Parameter.Filter:
                        return "Filter";
                    case Parameter.Frame:
                        return "Frame";
                    case Parameter.ParameterizedMotion:
                        return "Parameterized Motion";
                    case Parameter.Real:
                        return "Real";
                    case Parameter.Reference:
                        return "Reference";
                    case Parameter.Node:
                        return "Node";
                    case Parameter.Int:
                        return "Int";
                    case Parameter.HashString:
                        return "HashString";
                    case Parameter.Tag:
                        return "Tag";
                    default:
                        return "Unknown Type";
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Increases or decreases a reference count that is used to either show or find
        /// the "NotUsed" icon to the user.
        /// </summary>
        /// <param name="used">
        /// A value indicating whether to increase or decrease the reference count.
        /// </param>
        public void SetUsed(bool used)
        {
            if (used)
            {
                this.usedCount++;
            }
            else
            {
                this.usedCount--;
            }

            this.NotifyPropertyChanged("UsedIcon");
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public virtual void Serialise(XmlWriter writer)
        {
        }

        /// <summary>
        /// Gets the parameter value for this signal that represents the type it is
        /// representing.
        /// </summary>
        /// <returns>
        /// The parameter value that this signal represents.
        /// </returns>
        public abstract Parameter GetParameter();

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerializationTag, this.Name);
            info.AddValue(IdSerializationTag, this.Id);
            info.AddValue(ParentSerializationTag, this.Parent);
            info.AddValue(EnabledSerializationTag, this.Enabled);
        }

        /// <summary>
        /// Fires the <see cref="PropertyChanged"/> event for this class specifying the name
        /// of the property that has changed.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that changed.
        /// </param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    } // New.Move.Core.Signal {Class}
} // New.Move.Core {Namespace}
