﻿using System.Diagnostics;
using System.Runtime.Serialization;

using Move.Utils;

namespace New.Move.Core
{
    internal sealed class ConditionCollectionSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            Debug.Assert(obj is ConditionCollection);

            ConditionCollection items = obj as ConditionCollection;
            info.AddValue("Count", items.Count);
            for (int index = 0; index < items.Count; ++index)
            {
                info.AddValue(index.ToString(), items[index]);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            ConditionCollection items = new ConditionCollection();
            int count = (int)info.GetValue("Count", typeof(int));
            for (int index = 0; index < count; ++index)
            {
                items.Add((Condition)info.GetValue(index.ToString(), typeof(Condition)));
            }

            return items;
        }
    }
}