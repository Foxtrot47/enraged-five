﻿using System;
using System.Windows.Media;

namespace New.Move.Core
{
    public class TagDesc : IDesc
    {
        public string Name { get { return null; } }
        public int Id { get { return 0; } }
        public string Help { get { return null; } }
        public string Group { get { return null; } }

        public ImageSource Icon { get { return null; } }

        public Type ConstructType { get { return typeof(Frame); } }

        TagSignal _Signal;

        public TagDesc(TagSignal signal)
        {
            _Signal = signal;
        }

        public INode Create(Database database)
        {
            return new Tag(_Signal);
        }
    }
}