﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class ExpressionsLogicalProperty : Property, ISerializable, IExternalFileLink
    {
        private static IEnumerable<string> expressionDictionaries;
        public IEnumerable<string> ExpressionDictionaries { get { return expressionDictionaries; } }
        private IEnumerable<string> expressions_;
        public IEnumerable<string> Expressions
        {
            get { return expressions_; }
            set
            {
                if (expressions_ != value)
                {
                    expressions_ = value;
                    OnPropertyChanged("Expressions");
                }
            }
        }

        public string SelectedExpressionDictionary
        {
            get { return this.selectedExpressionDictionary; }
            set
            {
                this.selectedExpressionDictionary = value;
                OnPropertyChanged("SelectedExpressionDictionary");
                if (selectedExpressionDictionary != null)
                {
                    ExpressionArchiveReader archiveReader = new ExpressionArchiveReader();
                    Expressions = archiveReader.ReadExpressionNamesFromDictionary(selectedExpressionDictionary);
                    OnPropertyChanged("Expressions");
                    if (Expressions.Count() > 0)
                        SelectedExpression = Expressions.First();
                }
            }
        }
        private string selectedExpressionDictionary;

        public string SelectedExpression
        {
            get
            {
                return this.selectedExpression;
            }

            set
            {
                this.selectedExpression = value;
                OnPropertyChanged("SelectedExpression");
            }
        }
        private string selectedExpression;

        public static implicit operator ExpressionsLogicalProperty(string filename)
        {
            ExpressionsLogicalProperty property = new ExpressionsLogicalProperty();
            property._Input = PropertyInput.Filename;
            property.Filename = filename;

            return property;
        }

        public ExpressionsLogicalProperty()
        {
            Flags = PropertyInput.Expressions | PropertyInput.Filename | PropertyInput.Dictionary | PropertyInput.Invalid;
            Input = PropertyInput.Filename;
            Filename = "";

            Id = Guid.NewGuid();

            ReadLookupData();

            Connections = new NoResetObservableCollection<Connection>();
        }

        public ExpressionsLogicalProperty(ExpressionsLogicalProperty other)
        {
            Flags = other.Flags;
            Input = other.Input;
            OutputEnabled = other.OutputEnabled;
            Filename = other.Filename;
            Name = other.Name;
//             Lookup = other.Lookup;
            SelectedEvent = other.SelectedEvent;

            Id = Guid.NewGuid();

            ReadLookupData();

            SelectedExpressionDictionary = other.SelectedExpressionDictionary;
            SelectedExpression = other.SelectedExpression;

            Connections = new NoResetObservableCollection<Connection>();
        }

        public ExpressionsLogicalProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        private void ReadLookupData()
        {
            ExpressionArchiveReader archiveReader = new ExpressionArchiveReader();
            expressionDictionaries = archiveReader.ReadExpressionDictionaryNames();
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public ILogic Parent { get; set; }

        public object Tag { get; set; }

        public PropertyInput Flags { get; private set; }
        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    if (value == PropertyInput.Dictionary)
                    {
                        this.SelectedExpressionDictionary = ExpressionDictionaries.FirstOrDefault();
                    }
                    OnPropertyChanged("Input");
                }
            }
        }

        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        public string Filename { get; set; }
//         public string Lookup { get; set; }

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            if (_Input != PropertyInput.Expressions)
            {
                from.RemoveConnectionsFromAnchor(this);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Input = "Input";
            public static string OutputEnabled = "OutputEnabled";
            public static string Flags = "Flags";
            public static string Filename = "Filename";
//             public static string Lookup = "Lookup";
            public static string SelectedEvent = "SelectedEvent";
            public static string ExpressionDictionary = "ExpressionDictionary";
            public static string Expression = "Expression";
        }

        public ExpressionsLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));

            _Input = (PropertyInput)info.GetValue(SerializationTag.Input, typeof(PropertyInput));
            try
            {
                _OutputEnabled = info.GetBoolean(SerializationTag.OutputEnabled);
                _SelectedEvent = (Signal)info.GetValue(SerializationTag.SelectedEvent, typeof(Signal));
            }
            catch (Exception)
            {
            }

            Flags = PropertyInput.Expressions | PropertyInput.Filename | PropertyInput.Dictionary | PropertyInput.Invalid;

            ReadLookupData();

            switch (_Input)
            {
                case PropertyInput.Filename:
                {
                    Filename = (string)info.GetValue(SerializationTag.Filename, typeof(string));
                }
                break;
                case PropertyInput.Dictionary:
                {
                    //Lookup = (string)info.GetValue(SerializationTag.Lookup, typeof(string));
                    try
                    {
                        SelectedExpressionDictionary = info.GetString(SerializationTag.ExpressionDictionary);
                        if (SelectedExpressionDictionary != null)
                        {
                            ExpressionArchiveReader archiveReader = new ExpressionArchiveReader();
                            Expressions = archiveReader.ReadExpressionNamesFromDictionary(SelectedExpressionDictionary);
                            SelectedExpression = info.GetString(SerializationTag.Expression);
                        }
                    }
                    catch (System.Exception e)
                    {
                        SelectedExpressionDictionary = ExpressionDictionaries.FirstOrDefault();
                        if (SelectedExpressionDictionary != null)
                        {
                            ExpressionArchiveReader archiveReader = new ExpressionArchiveReader();
                            Expressions = archiveReader.ReadExpressionNamesFromDictionary(SelectedExpressionDictionary);
                            SelectedExpression = Expressions.FirstOrDefault();
                        }
                    }
                }
                break;
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);

            info.AddValue(SerializationTag.Input, _Input);
            info.AddValue(SerializationTag.OutputEnabled, _OutputEnabled);
            info.AddValue(SerializationTag.Flags, Flags);
            info.AddValue(SerializationTag.SelectedEvent, _SelectedEvent);

            switch (_Input)
            {
                case PropertyInput.Filename:
                {
                    info.AddValue(SerializationTag.Filename, Filename);
                }
                break;
                case PropertyInput.Dictionary:
                {
//                     info.AddValue(SerializationTag.Lookup, Lookup);
                    info.AddValue(SerializationTag.ExpressionDictionary, SelectedExpressionDictionary);
                    info.AddValue(SerializationTag.Expression, SelectedExpression);
                }
                break;
            }
        }

        public void ExportToXML(PargenXmlNode parentNode, Motiontree motiontreeParent)
        {
            switch (Input)
            {
                case PropertyInput.Expressions:
                {
                    parentNode.AppendTextElementNode("Type", "kExpressionsParameter");
                    foreach (Connection connection in motiontreeParent.Connections)
                    {
                        if (connection.Dest == this)
                        {
                            parentNode.AppendTextElementNode("Parameter", connection.Source.Parent.Name);
                            break;
                        }
                    }
                }
                break;
                case PropertyInput.Filename:
                {
                    parentNode.AppendTextElementNode("Type", "kExpressionsFilename");
                    parentNode.AppendTextElementNode("Filename", Filename);
                }
                break;
                case PropertyInput.Invalid:
                {
                    parentNode.AppendTextElementNode("Type", "kExpressionsIgnored");
                }
                break;
                case PropertyInput.Dictionary:
                {
                    parentNode.AppendTextElementNode("Type", "kExpressionsDictionary");
                    parentNode.AppendTextElementNode("BankName", SelectedExpressionDictionary);
                    parentNode.AppendTextElementNode("ExpressionName", SelectedExpression);
                }
                break;
            }
        }

        public object Clone()
        {
            return new ExpressionsLogicalProperty(this);
        }
                
        /// <summary>
        /// Initialises a new instance of the <see cref="ExpressionsLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public ExpressionsLogicalProperty(XmlReader reader, Database database, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Input = (PropertyInput)uint.Parse(reader.GetAttribute("Input"), CultureInfo.InvariantCulture);
            this.OutputEnabled = bool.Parse(reader.GetAttribute("OutputEnabled"));
            this.Filename = reader.GetAttribute("Filename");
            string dictionary = reader.GetAttribute("Dictionary");
            if (!string.IsNullOrEmpty(dictionary))
            {
                this.SelectedExpressionDictionary = dictionary;
            }

            string expression = reader.GetAttribute("Expression");
            if (!string.IsNullOrEmpty(expression))
            {
                this.SelectedExpression = expression;
            }

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedEvent = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedEvent == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedEvent = database.GetSignal(signalName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}' inside the '{1}' node. This needs fixing before a export is allowed.", signalId, parent.Name);
                    }
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Input", ((uint)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("OutputEnabled", this.OutputEnabled.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Filename", this.Filename);
            writer.WriteAttributeString("Dictionary", this.SelectedExpressionDictionary);
            writer.WriteAttributeString("Expression", this.SelectedExpression);

            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString("Signal", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedEvent.Name);
            }
        }
    }
}