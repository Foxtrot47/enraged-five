﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace New.Move.Core
{
    public class LogicInlineSMDesc : ITransitionalDesc
    {
        static class Const
        {
            public static string Name = "Inline state machine";
            public static int Id = 1;
            public static string Help = "Inline state machine";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get; private set; }

        public Type ConstructType { get { return typeof(LogicInlineSM); } }

        public LogicInlineSMDesc()
        {
            _Children.Add(new StateMachineDesc());
            _Children.Add(new MotiontreeDesc());

            try
            {
                BitmapImage icon = new BitmapImage();
                icon.BeginInit();
                icon.StreamSource =
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Core.Images.PaletteIcon_InlineState.png");
                icon.EndInit();

                Icon = icon;
            }
            catch
            {
                Icon = null;
            }
        }

        List<IDesc> _Children = new List<IDesc>();
        public List<IDesc> Children { get { return _Children; } }

        public ITransitional Create(Database database, Point offset)
        {
            return new LogicInlineSM(offset);
        }

        public INode Create(Database database)
        {
            return new LogicInlineSM();
        }
    }
}