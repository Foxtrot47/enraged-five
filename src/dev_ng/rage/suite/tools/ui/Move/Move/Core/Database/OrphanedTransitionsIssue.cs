﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace New.Move.Core
{
    internal class OrphanedTransitionsIssue : IDatabaseIssue
    {
        internal OrphanedTransitionsIssue(IAutomaton transitionOwner, INode childNode, TransitionCollection childTransitionOrderCollection, string hierarchicalNodeName)
        {
            transitionOwner_ = transitionOwner;
            childNode_ = childNode;
            childTransitionOrderCollection_ = childTransitionOrderCollection;
            hierarchicalNodeName_ = hierarchicalNodeName;
        }

        #region IDatabaseIssue Members

        public string Description
        {
            get 
            {
                return string.Format("Node '{0}' references orphaned transitions, transitions that do not belong to its parent.", hierarchicalNodeName_);
            }
        }

        public void Fix(Database database)
        {
            var orphanedTransitions = new List<Transition>();

            foreach (Transition transition in childTransitionOrderCollection_)
            {
                if (!transitionOwner_.Transitions.Contains(transition))
                {
                    orphanedTransitions.Add(transition);
                }
            }

            foreach (Transition transition in orphanedTransitions)
            {
                childTransitionOrderCollection_.Remove(transition);
            }
        }

        #endregion

        private IAutomaton transitionOwner_;
        private INode childNode_;
        private TransitionCollection childTransitionOrderCollection_;
        private string hierarchicalNodeName_;
    }
}
