﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

using Rage.Move.Utils.Serialization;

namespace Rage.Move.Core.Dg
{
#if def
    // anchor can only ever be an anchor
    public class Anchor
    {
    }

    namespace Properties
    {
        //public delegate void PropertySourceChangedEventHandler(Property property);

       

        

        



       

       

       

       

       

      

        [Serializable]
        public class RealArray : Property, ISerializable
        {
            ObservableCollection<Real> _Items = new ObservableCollection<Real>();

            public string Name { get; set; }
            public Guid Id { get; private set; }

            public PropertyInput Flags { get; private set; }
            PropertyInput _Input;
            public PropertyInput Input
            {
                get
                {
                    return _Input;
                }
                set
                {
                    if (_Input != value)
                    {
                        _Input = value;
                        OnPropertyChanged("Input");
                    }
                }
            }

            public New.Move.Core.ILogic Parent { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

            public RealArray()
            {
                Id = Guid.NewGuid();

                _Items.CollectionChanged += 
                    new NotifyCollectionChangedEventHandler(Items_CollectionChanged);
            }

            public RealArray(New.Move.Core.ILogic parent)
            {
                Parent = parent;
            }

            public ObservableCollection<Real> Items { get { return _Items; } }

            void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                //OnPropertySourceChanged();
            }

            void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
            {
            }

            public object Tag { get; set; }
        }
/*
        public enum WeightDataType
        {
            Weight,
            Signal,
        }
        */

        [Serializable]
        public class Weight : Property, ISerializable
        {
            public static implicit operator Weight(float weight)
            {
                Weight property = new Weight();
                property.Value = weight;

                return property;
            }

            public Weight()
            {
                //Source = RealDataType.Real;
                Value = 0.0f;

                Id = Guid.NewGuid();
                TransformId = Guid.NewGuid();
            }

            public Weight(New.Move.Core.ILogic parent)
            {
                Parent = parent;
            }

            public string Name { get; set; }
            public Guid Id { get; private set; }
            public Guid TransformId { get; private set; }

            public PropertyInput Flags { get; private set; }
            PropertyInput _Input;
            public PropertyInput Input
            {
                get
                {
                    return _Input;
                }
                set
                {
                    if (_Input != value)
                    {
                        _Input = value;
                        OnPropertyChanged("Input");
                    }
                }
            }

            public New.Move.Core.ILogic Parent { get; set; }

            public float Value { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

            void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
            {
            }

            public object Tag { get; set; }
        }

        [Serializable]
        public class WeightArray : Property, ISerializable
        {
            ObservableCollection<Weight> _Items = new ObservableCollection<Weight>();

            public string Name { get; set; }
            public Guid Id { get; private set; }

            public PropertyInput Flags { get; private set; }
            PropertyInput _Input;
            public PropertyInput Input
            {
                get
                {
                    return _Input;
                }
                set
                {
                    if (_Input != value)
                    {
                        _Input = value;
                        OnPropertyChanged("Input");
                    }
                }
            }

            public New.Move.Core.ILogic Parent { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

            public WeightArray()
            {
                Id = Guid.NewGuid();

                _Items.CollectionChanged +=
                    new NotifyCollectionChangedEventHandler(Items_CollectionChanged);
            }

            public WeightArray(New.Move.Core.ILogic parent)
            {
                Parent = parent;
            }

            public ObservableCollection<Weight> Items { get { return _Items; } }

            void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                //OnPropertySourceChanged();
            }

            void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
            {
            }

            public object Tag { get; set; }
        }

        [Serializable]
        public class SourceArray : Property, ISerializable
        {
            ObservableCollection<SourceTransform> _Items = new ObservableCollection<SourceTransform>();

            public string Name { get; set; }
            public Guid Id { get; private set; }

            public PropertyInput Flags { get; private set; }
            PropertyInput _Input;
            public PropertyInput Input
            {
                get
                {
                    return _Input;
                }
                set
                {
                    if (_Input != value)
                    {
                        _Input = value;
                        OnPropertyChanged("Input");
                    }
                }
            }

            public New.Move.Core.ILogic Parent { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

            public SourceArray()
            {
                Id = Guid.NewGuid();

                _Items.CollectionChanged +=
                    new NotifyCollectionChangedEventHandler(Items_CollectionChanged);
            }

            public SourceArray(New.Move.Core.ILogic parent)
            {
                Parent = parent;
            }

            public ObservableCollection<SourceTransform> Items { get { return _Items; } }

            void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                //OnPropertySourceChanged();
            }

            void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
            {
            }

            public object Tag { get; set; }
        }

      

       

      

       
    }
#endif

    /*
    public enum Modifier
    {
        Linear = 0,
        EaseInOut,
        EaseOut,
        EaseIn,
        Step
    }

    public interface IAttribute
    {
        string Name { get; }
        Guid Id { get; }

        object Value { get; set; }
    }

    public abstract class AttributeBase : IAttribute
    {
        public AttributeBase(string name)
        {
            Name = name;
            Id = Guid.NewGuid();
        }

        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public Guid Id { get; set; }

        public virtual object Value { get; set; }
    }

    [Serializable]
    public class BoolAttribute : AttributeBase
    {
        public BoolAttribute(string name, bool value)
            : base(name)
        {
            Value = value;
        }

        public BoolAttribute()
            : base(null) { }

        [XmlElement(typeof(bool))]
        public override object Value { get; set; }
    }

    [Serializable]
    public class ControlSignalAttribute : AttributeBase
    {
        public ControlSignalAttribute(string name)
            : base(name)
        {
            
        }

        public ControlSignalAttribute()
            : base(null) { }

        [XmlElement(typeof(ControlSignal))]
        [XmlTypeConverter(typeof(ControlSignalConverter))]
        public override object Value { get; set; }
    }

    /*
    [Serializable]
    public class MessageAttribute : AttributeBase
    {
        public MessageAttribute(string name)
            : base(name) { }

        public MessageAttribute() : base(null) { }

        [XmlElement(typeof(Message))]
        [XmlTypeConverter(typeof(MessageConverter))]
        public override object Value { get; set; }
    }
    */
    /*
    [Serializable]
    public class IntAttribute : AttributeBase
    {
        public IntAttribute(string name, int value)
            : base(name)
        {
            Value = value;
        }

        public IntAttribute()
            : base(null) { }

        [XmlElement(typeof(int))]
        public override object Value { get; set; }
    }

    [Serializable]
    public class FilterAttribute : AttributeBase
    {
        public FilterAttribute(string name)
            : base(name)
        {
            Value = new Filter();
        }

        public FilterAttribute()
            : base(null) { }

        [XmlElement(typeof(Filter))]
        public override object Value { get; set; }
    }

    [Serializable]
    public class FloatAttribute : AttributeBase
    {
        public FloatAttribute(string name, float value)
            : base(name)
        {
            Value = value;
        }

        public FloatAttribute()
            : base(null) { }

        [XmlElement(Type = typeof(float))]
        public override object Value { get; set; }
    }

    [Serializable]
    public class ModifierAttribute : AttributeBase
    {
        public ModifierAttribute(string name, Modifier modifier)
            : base(name)
        {
            Value = modifier;
        }

        public ModifierAttribute()
            : base(null) { }

        [XmlElement(typeof(Modifier))]
        public override object Value { get; set; }

        [XmlIgnore]
        public int Modifier { get { return Convert.ToInt32(Value); } }
    }
    /*
    [Serializable]
    public class RequestAttribute : AttributeBase
    {
        public RequestAttribute(string name)
            : base(name)
        {
        }

        public RequestAttribute()
            : base(null) { }

        [XmlElement(typeof(Request))]
        [XmlTypeConverter(typeof(RequestConverter))]
        public override object Value { get; set; }
    }
     */
    /*
    [Serializable]
    public class FlagAttribute : AttributeBase 
    {
        public FlagAttribute(string name)
            : base(name)
        {
        }

        public FlagAttribute()
            : base (null) { }

        [XmlElement(typeof(Flag))]
        [XmlTypeConverter(typeof(FlagConverter))]
        public override object Value { get; set; }
    }
    */
    /*
    [Serializable]
    public class SignalAttribute : AttributeBase
    {
        public SignalAttribute(string name)
            : base(name)
        {
        }

        public SignalAttribute()
            : base(null) { }

        [XmlElement(typeof(ISignal))]
        [XmlTypeConverter(typeof(SignalConverter))]
        public override object Value { get; set; }
    }

    [Serializable]
    public class StringAttribute : AttributeBase
    {
        public StringAttribute(string name)
            : base(name)
        {
        }

        public StringAttribute()
            : base(null) { }

        [XmlElement(typeof(string))]
        public override object Value { get; set; }
    }
     */
}
