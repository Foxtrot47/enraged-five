﻿using System;
using System.Reflection;

namespace Move.Core.Restorable
{
    internal sealed class WithoutUnregisteredChanged : IDisposable
    {
        internal WithoutUnregisteredChanged(object obj)
        {
            if (obj is INotifyUnregisteredChanged)
            {
                _Object = obj as INotifyUnregisteredChanged;

                Type type = _Object.GetType();
                FieldInfo fi = null;

                do
                {
                    fi = type.GetField("UnregisteredChanged", BindingFlags.Instance | BindingFlags.NonPublic);
                    type = type.BaseType;
                }
                while (fi == null && type != typeof(object));

                if (fi != null)
                {
                    object o = fi.GetValue(_Object);
                    if (o is EventHandler<UnregisteredChangedEventArgs>)
                    {
                        _Handler = (EventHandler<UnregisteredChangedEventArgs>)o;
                        _Object.UnregisteredChanged -= _Handler;
                    }
                }
            }
        }

        public void Dispose()
        {
            if (_Object != null)
            {
                _Object.UnregisteredChanged += _Handler;
            }
        }

        INotifyUnregisteredChanged _Object;
        EventHandler<UnregisteredChangedEventArgs> _Handler;
    }
}