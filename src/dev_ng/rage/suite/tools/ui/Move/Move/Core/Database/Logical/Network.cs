﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Xml;
using New.Move.Core;

namespace MoVE.Core
{
    /// <summary>
    /// Represents a network node located inside a motion tree.
    /// </summary>
    [Serializable]
    public class Network : Logic
    {
        #region Constructors
        /// <summary>
        /// Initialsies a new instance of the <see cref="New.Move.Core.Network"/> class.
        /// </summary>
        /// <param name="desc">
        /// The descriptive creation object for this instance.
        /// </param>
        public Network(IDesc desc)
            : base(desc)
        {
            this.Result = new ResultTransform(this);
            this.ParameterName = new StringLogicalProperty();
        }
        
        /// <summary>
        /// Initialsies a new instance of the <see cref="New.Move.Core.Network"/> class as a
        /// copy of the specified object.
        /// </summary>
        /// <param name="desc">
        /// The object to copy.
        /// </param>
        public Network(Network other)
            : base(new NetworkDesc())
        {
            this.AdditionalText = other.AdditionalText;
            this.Database = other.Database;
            this.Enabled = other.Enabled;
            this.Id = Guid.NewGuid();
            this.Name = other.Name;
            this.ParameterName = new StringLogicalProperty(other.ParameterName);
            this.Parent = other.Parent;
            this.Position = other.Position;
            this.Result = new ResultTransform(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Network"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Network(XmlReader reader, Database database, Guid parent)
            : this(new NetworkDesc())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the result anchor for this node.
        /// </summary>
        public ResultTransform Result
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the logical string property for this network.
        /// </summary>
        public StringLogicalProperty ParameterName
        {
            get;
            private set;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(Result.Id);
            ids.Add(ParameterName.Id);
            return ids;
        }

        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        /// <param name="hashedNames">
        /// A collection of hashed names to use during the export process.
        /// </param>
        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "SubnetworkNode");
            itemNode.AppendTextElementNode("Name", Name);
            itemNode.AppendTextElementNode("ParameterName", ParameterName.Value);
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        public override ISelectable Clone(Logic clone)
        {
            return new Network(this);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("network");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Result != null)
            {
                writer.WriteStartElement("Result");
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ParameterName != null)
            {
                writer.WriteStartElement("ParameterName");
                this.ParameterName.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Result") == 0)
                {
                    this.Result = new ResultTransform(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, "ParameterName") == 0)
                {
                    this.ParameterName = new StringLogicalProperty(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }
        #endregion Methods
    }
}