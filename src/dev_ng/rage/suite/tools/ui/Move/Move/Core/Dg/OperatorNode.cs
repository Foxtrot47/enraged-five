﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;

namespace Rage.Move.Core.Dg
{
#if dis
    public interface IOpNode
    {
        string Name { get; set; }
        Guid Id { get; }

        string Type { get; set; }
        Point Position { get; set; }

        //MotionTree Parent { get; set; }
    }

    public class OpNodeBase : IOpNode
    {
        public OpNodeBase(string type)
        {
            Name = type;
            Id = Guid.NewGuid();
            Type = type;
        }

        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public Guid Id { get; set; }
        [XmlAttribute]
        public string Type { get; set; }

        public Point Position { get; set; }

        //[XmlIgnore]
        //public MotionTree Parent { get; set; }

        protected ObservableCollection<IAnchor> _Anchors = new ObservableCollection<IAnchor>();
        public virtual ObservableCollection<IAnchor> Anchors 
            { get { return _Anchors; } set { _Anchors = value; } }
        protected ObservableCollection<IAttribute> _Attributes = new ObservableCollection<IAttribute>();
        public virtual ObservableCollection<IAttribute> Attributes
            { get { return _Attributes; } set { _Attributes = value; } }

        public IAnchor GetAnchor(string name, int index)
        {
            IEnumerable<IAnchor> item =
                from anchor in _Anchors where anchor.Name == name select anchor;

            if (item.Count() == 0)
                return null;

            if (item.Count() <= index)
                return null;

            return item.ElementAt(index);
        }

        public IAnchor GetAnchor(string name)
        {
            return GetAnchor(name, 0);
        }

        public IAnchor GetAnchor(Guid id)
        {
            IEnumerable<IAnchor> items = 
                from anchor in _Anchors where anchor.Id == id select anchor;

            if (items.Count() == 0)
                return null;

            return items.ElementAt(0);
        }

        public IAttribute GetAttribute(string name)
        {
            IEnumerable<IAttribute> item =
                from attribute in _Attributes where attribute.Name == name select attribute;

            return item.SingleOrDefault();
        }

        public bool HasAnchor(Guid id)
        {
            IEnumerable<IAnchor> item = 
                from anchor in _Anchors where anchor.Id == id select anchor;

            return item.Count() != 0;
        }

        public void Remove()
        {
            //Parent.Operators.Remove(this);
        }

        public void OnPositionChanged(Point point) { Position = point; }
    }

    [Serializable]
    public class OpNode : OpNodeBase
    {
        public OpNode(string type)
            : base(type) { }

        public OpNode()
            : base(null) { }
    }
#endif
}