﻿using System;
using System.Runtime.Serialization;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel
{
    [Serializable]
    public class WeightFilteredTransformArray : LogicalPropertyArray<WeightFilteredTransform>, ISerializable
    {
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public WeightFilteredTransformArray(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public WeightFilteredTransformArray()
            : base()
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }
        }

        public override object Clone()
        {
            WeightFilteredTransformArray clone = new WeightFilteredTransformArray();
            clone.Name = this.Name;
            clone.Parent = this.Parent;

            foreach (var item in this.Items)
            {
                WeightFilteredTransform newItem = item.Clone() as WeightFilteredTransform;
                clone.Items.Add(newItem);
            }
            return clone;
        }
    }
}