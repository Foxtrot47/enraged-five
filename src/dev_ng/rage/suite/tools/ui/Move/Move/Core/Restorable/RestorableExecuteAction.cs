﻿using System.Collections.Generic;
using System.Linq;

namespace Move.Core.Restorable
{
    public interface IExecuteAction
    {
        void Execute();
    }
}