﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public abstract class SourceAnchor : AnchorBase
    {
        public SourceAnchor(string name)
            : base(name) { }

        protected SourceAnchor() : base(null) { }
    }
}