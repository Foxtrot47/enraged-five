﻿using System;

namespace New.Move.Core
{
    [Flags]
    public enum PropertyInput
    {
        Animation = (1 << 0),
        Boolean = (1 << 1),
        Clip = (1 << 2),
        Dictionary = (1 << 3),
        Expressions = (1 << 4),
        Filename = (1 << 5),
        Filter = (1 << 6),
        Frame = (1 << 7),
        ParameterizedMotion = (1 << 8),
        Real = (1 << 9),
        FilterN = (1 << 10),
        String = (1 << 11),
        Curve = (1 << 12),
        Invalid = (1 << 30),
        Value = (1 << 31)
    }

}