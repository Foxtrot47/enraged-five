﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

using Move.Core;
using Move.Utils;
using System.Xml;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class FilterNLogicalProperty : Property, ISerializable
    {
        public FilterNLogicalProperty()
        {
            Flags = PropertyInput.FilterN | PropertyInput.Invalid;
            Input = PropertyInput.Invalid;

            Id = Guid.NewGuid();

            Connections = new NoResetObservableCollection<Connection>();
        }

        public FilterNLogicalProperty(FilterNLogicalProperty other)
        {
            Flags = other.Flags;
            Input = other.Input;
            OutputEnabled = other.OutputEnabled;
            this.Name = other.Name;
            if (other.SelectedEvent != null && other.SelectedEvent is ICloneable)
                this.SelectedEvent = (other.SelectedEvent as ICloneable).Clone() as Signal;

            Id = Guid.NewGuid();

            Connections = new NoResetObservableCollection<Connection>();
        }

        public FilterNLogicalProperty(ILogic parent)
        {
            Parent = parent;
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        public object Tag { get; set; }

        public PropertyInput Flags { get; private set; }
        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }

        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            if (_Input != PropertyInput.FilterN)
            {
                from.RemoveConnectionsFromAnchor(this);
            }
        }

        public ILogic Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Input = "Input";
            public static string OutputEnabled = "OutputEnabled";
            public static string Flags = "Flags";
            public static string SelectedEvent = "SelectedEvent";
        }

        public FilterNLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(Const.Name, typeof(string));
            Id = (Guid)info.GetValue(Const.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(Const.Parent, typeof(ILogic));

            _Input = (PropertyInput)info.GetValue(Const.Input, typeof(PropertyInput));
            try
            {
                _OutputEnabled = info.GetBoolean(Const.OutputEnabled);
                _SelectedEvent = (Signal)info.GetValue(Const.SelectedEvent, typeof(Signal));
            }
            catch (Exception)
            {
            }
            Flags = (PropertyInput)info.GetValue(Const.Flags, typeof(PropertyInput));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Input, _Input);
            info.AddValue(Const.OutputEnabled, _OutputEnabled);
            info.AddValue(Const.Flags, Flags);
            info.AddValue(Const.SelectedEvent, _SelectedEvent, typeof(Signal));
        }

        public object Clone()
        {
            return new FilterNLogicalProperty(this);
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterNLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public FilterNLogicalProperty(XmlReader reader, Database database, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Input = (PropertyInput)uint.Parse(reader.GetAttribute("Input"), CultureInfo.InvariantCulture);
            this.OutputEnabled = bool.Parse(reader.GetAttribute("OutputEnabled"));

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedEvent = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedEvent == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedEvent = database.GetSignal(signalName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}' inside the '{1}' node. This needs fixing before a export is allowed.", signalId, parent.Name);
                    }
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Input", ((uint)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("OutputEnabled", this.OutputEnabled.ToString(CultureInfo.InvariantCulture));
            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString("Signal", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedEvent.Name);
            }
        }
    }
}