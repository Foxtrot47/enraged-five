﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class ReferenceRequest : Request, ISerializable
    {
        public ReferenceRequest(Guid reference, string name)
            : base(name)
        {
            Reference = reference;
        }

        public Guid Reference { get; protected set; }

        new public ReferenceRequest CreateReference()
        {
            return new ReferenceRequest(Reference, Name);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Reference = "Reference";
        }

        public ReferenceRequest(SerializationInfo info, StreamingContext context)
            //: base(info, context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Reference = (Guid)info.GetValue(SerializationTag.Reference, typeof(Guid));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Reference, Reference);
        }
    }
}