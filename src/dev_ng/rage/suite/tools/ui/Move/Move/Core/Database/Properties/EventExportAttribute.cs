﻿using System;

namespace New.Move.Core
{
    [AttributeUsage(AttributeTargets.Property)]
    public class EventExportAttribute : Attribute
    {
        public EventExportAttribute(uint idx)
        {
            Index = idx;
        }

        public uint Index { get; private set; }
    }
}