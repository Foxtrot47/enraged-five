﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Configuration;

namespace Move.Core
{
    internal class ClipSetReader
    {
        internal ClipSetReader()
        {
        }

        // Cache any data read from the xml to speed things up a little
        private static IEnumerable<string> _cachedClipSetNames;
        private static Dictionary<string, string> _clipsetLocations = new Dictionary<string, string>();
        private static Dictionary<string, List<string>> _cachedClipSetItems = new Dictionary<string,List<string>>();

        internal IEnumerable<string> ReadClipSetNames()
        {
            if (_cachedClipSetNames != null)
            {
                return _cachedClipSetNames;
            }

            var clipSetNames = new List<string>();

            try
            {
                IConfig config = Move.Utils.ContentConfig.Config;
                IBranch branch = config.Project.DefaultBranch;
                List<string> directories = new List<string>();
                directories.Add(Path.Combine(branch.Common, "data", "anim", "clip_sets"));
                foreach (IProject project in config.DLCProjects.Values)
                {
                    branch = project.DefaultBranch;
                    directories.Add(Path.Combine(branch.Common, "data", "anim", "clip_sets"));
                }

                string xmlPattern = "*clip_sets.xml";
                string metaPattern = "*clip_sets.meta";
                SearchOption option = SearchOption.AllDirectories;
                List<string> clipSets = new List<string>();
                foreach (string directory in directories)
                {
                    if (!Directory.Exists(directory))
                    {
                        continue;
                    }

                    List<string> filenames = new List<string>();
                    filenames.AddRange(Directory.GetFiles(directory, xmlPattern, option));
                    filenames.AddRange(Directory.GetFiles(directory, metaPattern, option));
                    foreach (string filename in filenames)
                    {
                        if (!File.Exists(filename))
                        {
                            continue;
                        }

                        XmlDocument doc = new XmlDocument();
                        doc.Load(filename);
                        XmlNodeList clipSetRoot = doc.GetElementsByTagName("clipSets");
                        foreach (XmlNode clipSetsGroup in clipSetRoot)
                        {
                            foreach (XmlNode clipSet in clipSetsGroup.ChildNodes)
                            {
                                XmlAttribute keyAttribute = clipSet.Attributes["key"];
                                clipSets.Add(keyAttribute.Value);
                                _clipsetLocations[keyAttribute.Value] = filename;
                            }
                        }
                    }
                }

                clipSets.Sort();
                _cachedClipSetNames = clipSets;
                return _cachedClipSetNames;
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }

        internal IEnumerable<string> ReadClipSetItems(string clipSetName)
        {
            if (_cachedClipSetItems.ContainsKey(clipSetName))
            {
                return _cachedClipSetItems[clipSetName];
            }

            try
            {
                string filename;
                if (!_clipsetLocations.TryGetValue(clipSetName, out filename))
                {
                    return new string[] { };
                }

                if (!File.Exists(filename))
                {
                    return new string[] { };
                }

                List<string> clipSetItems = new List<string>();
                XmlDocument doc = new XmlDocument();
                doc.Load(filename);
                XmlNodeList clipSetRoot = doc.GetElementsByTagName("clipSets");
                foreach (XmlNode clipSetsGroup in clipSetRoot)
                {
                    foreach (XmlNode clipSet in clipSetsGroup.ChildNodes)
                    {
                        XmlAttribute keyAttribute = clipSet.Attributes["key"];
                        if (keyAttribute.Value != clipSetName)
                        {
                            continue;
                        }

                        foreach (XmlNode clipSetChildNode in clipSet.ChildNodes)
                        {
                            if (clipSetChildNode.Name != "clipItems")
                            {
                                continue;
                            }

                            foreach (XmlNode clipItem in clipSetChildNode.ChildNodes)
                            {
                                XmlAttribute itemKeyAttribute = clipItem.Attributes["key"];
                                clipSetItems.Add(itemKeyAttribute.Value);
                            }

                            break;
                        }

                        break;
                    }

                    break;
                }

                clipSetItems.Sort();
                _cachedClipSetItems[clipSetName] = clipSetItems;
                return clipSetItems;
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }
    }
}
