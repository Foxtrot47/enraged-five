﻿using System.Diagnostics;
using System.Runtime.Serialization;

using Move.Utils;

namespace New.Move.Core
{
    internal sealed class NoResetObservableCollectionSerializationSurrogate<T> : ISerializationSurrogate
    {
        const string ItemsKey = "Items";

        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            Debug.Assert(obj is NoResetObservableCollection<T>);

            NoResetObservableCollection<T> items = obj as NoResetObservableCollection<T>;
            info.AddValue("Count", items.Count);
            for (int index = 0; index < items.Count; ++index)
            {
                info.AddValue(index.ToString(), items[index]);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            NoResetObservableCollection<T> items = new NoResetObservableCollection<T>();
            int count = (int)info.GetValue("Count", typeof(int));
            for (int index = 0; index < count; ++index)
            {
                items.Add((T)info.GetValue(index.ToString(), typeof(T)));
            }

            return items;
        }
    }
}