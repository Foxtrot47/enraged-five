﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows;

using Move.Core.Restorable;

namespace New.Move.Core
{
    public class LogicSurrogateSelector : ISerializationSurrogate, ISurrogateSelector
    {
        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Position = "Position";
            public static string Parent = "Parent";
            public static string Enabled = "Enabled";
            //public static string Database = "Database";
        }

        public LogicSurrogateSelector(ICoreDataModel model)
        {
            DataModel = model;
        }

        ICoreDataModel DataModel { get; set; }

        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            Logic logic = obj as Logic;

            info.AddValue(SerializationTag.Name, logic.Name);
            info.AddValue(SerializationTag.Position, logic.Position);
            info.AddValue(SerializationTag.Id, logic.Id);
            info.AddValue(SerializationTag.Parent, logic.Parent);
            info.AddValue(SerializationTag.Enabled, logic.Enabled);
            //info.AddValue(SerializationTag.Database, logic.Database);

            PropertyInfo[] properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                object value = pi.GetValue(obj, null);
                if (value != null && (value.GetType().GetInterface("IAnchor") != null || value.GetType().GetInterface("IProperty") != null))
                {
                    object property = pi.GetValue(obj, null);
                    info.AddValue(pi.Name, property);
                }
            }

            logic.GetCustomData(info, context);
        }

        private bool IsKnown(Type type)
        {
            return type == typeof(string) || type.IsPrimitive || type.IsSerializable;
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            Logic logic = obj as Logic;

            logic.SetObjectData(info, context);
            RestorableProperty.CreateItems(logic);

            if (logic.GetType() == typeof(Output))
            {
                logic.Desc = new OutputDesc();
            }
            else if (logic.GetType() == typeof(Input))
            {
                logic.Desc = new InputDesc();
            }
            else
            {
                if (logic.Desc == null)
                {
                    logic.Desc = DataModel.MotionTreeDescriptors.FirstOrDefault(
                        delegate(IDesc desc) { return desc.ConstructType == logic.GetType(); });
                }
                /*
                logic.Desc = Motiontree.Desc.Children.Find(
                    delegate(IDesc desc)
                    {
                        return desc.ConstructType == logic.GetType();
                    }
                );
                 */
            }

            logic.Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            logic.Position = (Point)info.GetValue(SerializationTag.Position, typeof(Point));
            logic.Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            logic.Parent = (Guid)info.GetValue(SerializationTag.Parent, typeof(Guid));
            try
            {
                logic.Enabled = (bool)info.GetValue(SerializationTag.Enabled, typeof(bool));
            }
            catch (SerializationException)
            {
                logic.Enabled = true;
            }

            PropertyInfo[] properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                bool anchor = pi.PropertyType.GetInterface("IAnchor") != null;
                bool prop = pi.PropertyType.GetInterface("IProperty") != null;
                if (anchor || prop)
                {
                    try
                    {
                        object value = info.GetValue(pi.Name, pi.PropertyType);

                        if (anchor)
                        {
                            IAnchor typed = value as IAnchor;
                            //typed.Parent = logic;
                        }

                        if (prop)
                        {
                            Property typed = value as Property;
                            //typed.Parent = logic;
                        }

                        pi.SetValue(obj, value, null);
                    }
                    catch (SerializationException)
                    {
                        try
                        {
                            object value = Activator.CreateInstance(pi.PropertyType, new object[] { logic });
                            pi.SetValue(obj, value, null);
                        }
                        catch (MissingMethodException)
                        {
                            object value = Activator.CreateInstance(pi.PropertyType, new object[] {});
                            pi.SetValue(obj, value, null);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            logic.SetCustomData(info, context);

            return null;
        }

        Type GetFirstArgumentOfGenericType(Type type)
        {
            return type.GetGenericArguments()[0];
        }

        bool IsNullableType(Type type)
        {
            if (type.IsGenericType)
                return type.GetGenericTypeDefinition() == typeof(Nullable<>);
            return false;
        }

        ISurrogateSelector _NextSelector;

        public void ChainSelector(ISurrogateSelector selector)
        {
            _NextSelector = selector;
        }

        public ISurrogateSelector GetNextSelector()
        {
            return _NextSelector;
        }

        public ISerializationSurrogate GetSurrogate(Type type, StreamingContext context, out ISurrogateSelector selector)
        {
            selector = this;

            if (type.BaseType == typeof(Logic))
            {
                if (!_Handles.Keys.Contains(type))
                {
                    _Handles.Add(type, FormatterServices.GetSurrogateForCyclicalReference(new LogicSurrogateSelector(DataModel)));
                }

                return _Handles[type];
            }
            if (IsKnown(type))
            {
                return null;
            }
 
            else
            {
                return null;
            }
        }

        Dictionary<Type, ISerializationSurrogate> _Handles = new Dictionary<Type, ISerializationSurrogate>();
    }
}