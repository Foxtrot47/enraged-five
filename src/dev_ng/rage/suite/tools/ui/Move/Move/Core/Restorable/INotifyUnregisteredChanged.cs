﻿using System;

namespace Move.Core.Restorable
{
    internal interface INotifyUnregisteredChanged
    {
        event EventHandler<UnregisteredChangedEventArgs> UnregisteredChanged;
    }
}