﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using Rage.Move.Core;
using Rage.Move.Core.Dag;
using Rage.Move.Core.Dg;
//using Rage.Move.Maths;
using Rage.Move.Utils;

namespace Rage.Move
{
    public delegate void ConnectionEventHandler();
    public delegate void TransitionEventHandler();

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    public class Diagram : Canvas
    {
        private static class Const
        {
            // Duration to pause before displaying new noeds.
            public static double AnimationPauseDuration = 600;

            // Duration for nodes to fade in when the diagram is repopulated.
            public static double NodeFadeInDuration = 500;

            // Duration for the new node animation.
            public static double NewNodeAnimationDuration = 250;

            // Maximum number of nodes a state can have if it's going to be seen one level higher.
            public static int NodeChildCountThreshold = 1;
        }

        public static readonly RoutedEvent DiagramChangedEvent = EventManager.RegisterRoutedEvent(
           "DiagramChanged", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(Diagram));

        public static readonly RoutedEvent ConnectionChangedEvent = EventManager.RegisterRoutedEvent(
            "ConnectionChanged", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(Diagram));

        public static readonly RoutedEvent ConnectionRemovedEvent = EventManager.RegisterRoutedEvent(
            "ConnectionRemoved", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(Diagram));

        List<UIElement> elements = new List<UIElement>();
        Size totalSize = new Size(0, 0);
        double scale = 1.0;
        Rect selectedNodeBounds = Rect.Empty;
        DispatcherTimer animationTimer = new DispatcherTimer();
        Selection selection;
        bool dragging = false;

        public Channels Channels { get; set; }

        public Diagram()
        {
            Logic = new DiagramLogic();
            Logic.ChildrenLoaded += new EventHandler(OnChildrenLoaded);

            //Runtime.Database.ActiveChanged += new ActiveChangedHandler(OnActiveChanged);

            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, OnDelete, CanDelete));

            AddHandler(DiagramChangedEvent, new RoutedEventHandler(OnDiagramChanged), true);
            AddHandler(ConnectionChangedEvent, new RoutedEventHandler(OnConnectionChanged), true);
            AddHandler(ConnectionRemovedEvent, new RoutedEventHandler(OnConnectionRemoved), true);

            this.selection = new Selection(this);
            Channels = new Channels();

            DiagramUpdated += new EventHandler(Channels_DiagramUpdated);

            this.AllowDrop = true;
        }

        public event EventHandler DiagramUpdated;
        private void OnDiagramUpdated()
        {
            if (null != DiagramUpdated)
                DiagramUpdated(this, EventArgs.Empty);
        }

        public event EventHandler DiagramPopulated;
        private void OnDiagramPopulated()
        {
            if (null != DiagramPopulated)
                DiagramPopulated(this, EventArgs.Empty);
        }

        public event ConnectionEventHandler ConnectionCreated;
        public void OnConnection()
        {
            if (ConnectionCreated != null)
                ConnectionCreated();

            OnDiagramChanged();
        }

        public event TransitionEventHandler TransitionCreated;
        public void OnTransition()
        {
            if (TransitionCreated != null)
                TransitionCreated();

            OnDiagramChanged();
        }

        public double Scale
        {
            get { return this.scale; }
            set
            {
                if (this.scale != value)
                {
                    this.scale = value;
                    this.LayoutTransform = new ScaleTransform(scale, scale);
                }
            }
        }

        public Rect SelectedNodeBounds
        {
            get { return selectedNodeBounds; }
        }

        public bool IsDragging
        {
            get { return dragging; }
            private set
            {
                this.dragging = value;
            }
        }

        public DiagramLogic Logic { get; private set; }

        public void OnDiagramChanged()
        {
            if (Runtime.Database.Active is New.Move.Core.Motiontree)
                UpdateMotionTree((New.Move.Core.Motiontree)Runtime.Database.Active);
            else
            if (Runtime.Database.Active is New.Move.Core.StateMachine)
                UpdateStateMachine((New.Move.Core.StateMachine)Runtime.Database.Active);
        }

        void OnActiveChanged(New.Move.Core.ITransitional active)
        {
            Selection.Clear();

            if (active is New.Move.Core.Motiontree)
                UpdateMotionTree((New.Move.Core.Motiontree)active);
            else
            if (active is New.Move.Core.StateMachine)
                UpdateStateMachine((New.Move.Core.StateMachine)active);
        }

        void UpdateMotionTree(New.Move.Core.Motiontree active)
        {
             if (Runtime.Database.Active == null)
                return;

            Clear();

            foreach (New.Move.Core.ILogic child in active.Children)
            {
                /*
                if (child.Layer != Guid.Empty)
                {
                    Layers.Layer layer = Runtime.Instance.Layers.Find(child.Layer);
                    if (layer.Enabled)
                        Children.Add(Logic.Create(child));
                }
                else
                {
                 */
                    Children.Add(Logic.Create(child));
                //}
            }
            /*
            foreach (IOpNode op in active.Operators)
            {
                Children.Add(Logic.Create(op));
            }
             */

            Children.Add(Logic.CreateOutput(active));

            /*
            if (active.HasInputs)
                Children.Add(Logic.CreateInput(active));
             */

            Children.Add(Logic.CreateControlSignals(active));
            Children.Add(Logic.CreateMessages(active));
        }

        void UpdateStateMachine(New.Move.Core.StateMachine active)
        {
            if (Runtime.Database.Active == null)
                return;

            Clear();

            foreach (New.Move.Core.ITransitional child in active.Children)
                Children.Add(Logic.Create(child));

            /*
            if (active.LogicalParent != null)
            {
                if (active.LogicalParent.HasInputs)
                    Children.Add(Logic.CreateLogicInput(active));

                if (Logic.ChildrenHaveOutputs(active))
                    Children.Add(Logic.CreateLogicOutput(active));
            }
            else
            {
                if (active.HasInputs)
                    Children.Add(Logic.CreateInput(active));

                if (Logic.ChildrenHaveOutputs(active))
                    Children.Add(Logic.CreateOutput(active));
            }
            */
            /*
            if (active.ChildCount != 0)
                Children.Add(Logic.CreateStartMarker(this, active));
             */
        }

        // TODO: would be nice if there was a way to just iterate through an input list.
        //  would be handy for combiner-n's as well.
        void OnDelete(object sender, ExecutedRoutedEventArgs e)
        {
           
        }

        void CanDelete(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = 0 < this.Selection.Current.Count;
        }

        protected override void OnInitialized(EventArgs e)
        {
            UpdateDiagram();
            base.OnInitialized(e);
        }
        /*
        protected override int VisualChildrenCount
        {
            get { return Children.OfType<IMeasureable>().Count() + Children.OfType<Connection>().Count(); }
        }

        protected override Visual GetVisualChild(int index)
        {
            return Children[index] as UserControl;
        }
         * */

        public Point RelativePosition(Point position)
        {
            if (Children.OfType<DiagramNode>().Count() == 0 &&
                Children.OfType<Operator>().Count() == 0 &&
                Children.OfType<DiagramState>().Count() == 0)
                return new Point(0, 0);

            Point topleft = new Point(double.MaxValue, double.MaxValue);

            foreach (DiagramNode element in Children.OfType<DiagramNode>())
            {
                topleft.X = Math.Min(topleft.X, Canvas.GetLeft(element));
                topleft.Y = Math.Min(topleft.Y, Canvas.GetTop(element));
            }

            foreach (Operator element in Children.OfType<Operator>())
            {
                topleft.X = Math.Min(topleft.X, Canvas.GetLeft(element));
                topleft.Y = Math.Min(topleft.Y, Canvas.GetTop(element));
            }

            foreach (DiagramState element in Children.OfType<DiagramState>())
            {
                topleft.X = Math.Min(topleft.X, Canvas.GetLeft(element));
                topleft.Y = Math.Min(topleft.Y, Canvas.GetTop(element));
            }

            return new Point(topleft.X + position.X, topleft.Y + position.Y);
        }

        public Size Size
        {
            get
            {
                Size size = new Size(0, 0);

                foreach (DiagramNode element in Children.OfType<DiagramNode>())
                {
                    double width = element.Position.X + element.DesiredSize.Width;

                    if (size.Width < width)
                        size.Width = width;

                    double height = element.Position.Y + element.DesiredSize.Height;

                    if (size.Height < height)
                        size.Height = height;
                }

                foreach (Operator element in Children.OfType<Operator>())
                {
                    double width = element.Position.X + element.DesiredSize.Width;

                    if (size.Width < width)
                        size.Width = width;

                    double height = element.Position.Y + element.DesiredSize.Height;

                    if (size.Height < height)
                        size.Height = height;
                }

                foreach (DiagramState element in Children.OfType<DiagramState>())
                {
                    double width = element.Position.X + element.DesiredSize.Width;

                    if (size.Width < width)
                        size.Width = width;

                    double height = element.Position.Y + element.DesiredSize.Height;

                    if (size.Height < height)
                        size.Height = height;
                }

                return size;
            }
        }

        public Selection Selection
        {
            get { return this.selection; }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            // TODO: this needs to be cleaned up...
             Size size = new Size(double.PositiveInfinity, double.PositiveInfinity);

            foreach (UIElement element in this.InternalChildren)
                element.Measure(size);
           
            return MeasureItems();
        }

        private Size MeasureItems()
        {
            // use the node and groups desired size and position to
            //  figure out what the diagram needs
            var items = Children.OfType<IMeasureable>();
            if (items.Count() == 0) return new Size();

            Rect bounds = new Rect(new Point(Double.MaxValue, Double.MaxValue), new Size());

            foreach (IMeasureable item in items)
            {
                Point location = new Point(
                    Canvas.GetLeft(item as UIElement), 
                    Canvas.GetTop(item as UIElement));

                bounds.X = Math.Min(bounds.X, location.X);
                bounds.Y = Math.Min(bounds.Y, location.Y);
            }

            Point point = new Point(-bounds.X, -bounds.Y);

            foreach (IMeasureable item in items)
            {
                Point location = new Point(
                    Canvas.GetLeft(item as UIElement) + point.X,
                    Canvas.GetTop(item as UIElement) + point.Y);

                bounds.Width = Math.Max(bounds.Width, location.X + item.DesiredSize.Width);
                bounds.Height = Math.Max(bounds.Height, location.Y + item.DesiredSize.Height);
            }
  
            return new Size(bounds.Width, bounds.Height);
        }
        
        protected override Size ArrangeOverride(Size finalSize)
        {
            var items = Children.OfType<IMeasureable>();
            if (items.Count() == 0) return new Size();

            Rect bounds = new Rect(new Point(Double.MaxValue, Double.MaxValue), new Size());

            // calculate the absolute bounds for all the elements in the diagram
            foreach (IMeasureable item in items)
            {
                Point location = new Point(
                    Canvas.GetLeft(item as UIElement),
                    Canvas.GetTop(item as UIElement));

                bounds.X = Math.Min(bounds.X, location.X);
                bounds.Y = Math.Min(bounds.Y, location.Y);
            }

            // create an absolute location
            Point point = new Point(-bounds.X, -bounds.Y);

            foreach (IMeasureable item in items)
            {
                /*item.Position*/
                Point location = new Point(
                    Canvas.GetLeft(item as UIElement) + point.X, 
                    Canvas.GetTop(item as UIElement) + point.Y);

                Rect rect = new Rect(location, item.DesiredSize);
                (item as UIElement).Arrange(rect);

                bounds.Width = Math.Max(bounds.Width, location.X + item.DesiredSize.Width);
                bounds.Height = Math.Max(bounds.Height, location.Y + item.DesiredSize.Height);
            }

            foreach (Connection connection in InternalChildren.OfType<Connection>())
            {
                Rect rect = new Rect(connection.SenderSource, connection.ReceiverSink);
                connection.Arrange(rect);
            }

            foreach (Transition transition in InternalChildren.OfType<Transition>())
            {
                Rect rect = new Rect(transition.Source.BottomCenter, transition.Sink.TopCenter);
                transition.Arrange(rect);
            }

            return new Size(bounds.Width, bounds.Height);
        }

        protected override void OnDrop(DragEventArgs e)
        {
            // this should work here but i think it's because the palette is at the wrong level
            base.OnDrop(e);
            /*
            DragObject drag = e.Data.GetData(typeof(DragObject)) as DragObject;
            if (drag != null)
            {
                // use the id to figure out the node type and create it
            }
             */
        }
        
        private void Clear()
        {
            UIElement[] children = new UIElement[this.InternalChildren.Count];
            this.InternalChildren.CopyTo(children, 0);
            foreach (UIElement element in children)
            {
                this.Children.Remove(element);
            }

            elements.Clear();
            
            Logic.Clear();
        }

        void Channels_DiagramUpdated(object sender, EventArgs e)
        {
            foreach (DiagramNode node in InternalChildren.OfType<DiagramNode>())
            {
                if (Channels.Instance.Channel1.Equals(node.Id))
                    Channels.Instance.Channel1.Select(node);
                else 
                if (Channels.Instance.Channel2.Equals(node.Id))
                    Channels.Instance.Channel2.Select(node);
                else 
                if (Channels.Instance.Channel3.Equals(node.Id))
                    Channels.Instance.Channel3.Select(node);
                else 
                if (Channels.Instance.Channel4.Equals(node.Id))
                    Channels.Instance.Channel4.Select(node);
                else 
                if (Channels.Instance.Channel5.Equals(node.Id))
                    Channels.Instance.Channel5.Select(node);
                else 
                if (Channels.Instance.Channel6.Equals(node.Id))
                    Channels.Instance.Channel6.Select(node);
                else 
                if (Channels.Instance.Channel7.Equals(node.Id))
                    Channels.Instance.Channel7.Select(node);
                else
                    node.Channel = ChannelIndex.ChannelNone;
            }
        }

        void UpdateDiagram()
        {
            if (Runtime.Database.Root == null)
                return;

            Clear();

            if (Runtime.Database.Active == null)
                return;
        }

        private void AnimateNewNode()
        {
            /*
            if (null == newNode)
                return;

            DiagramNode node = logic.GetDiagramNode(newNode);
            if (null != node)
            {
                DoubleAnimation anim = new DoubleAnimation(
                    0, 1, App.GetAnimationDuration(Const.NewNodeAnimationDuration));

                ScaleTransform transform = new ScaleTransform();
                transform.BeginAnimation(ScaleTransform.ScaleXProperty, anim);
                transform.BeginAnimation(ScaleTransform.ScaleYProperty, anim);
                node.RenderTransform = transform;
            }

            newNode = null;
             */
        }

        private void AnimateNode(DiagramNode node)
        {
            if (null != node)
            {
                DoubleAnimation anim = new DoubleAnimation(
                    0, 1, App.GetAnimationDuration(Const.NewNodeAnimationDuration));

                ScaleTransform transform = new ScaleTransform();
                transform.BeginAnimation(ScaleTransform.ScaleXProperty, anim);
                transform.BeginAnimation(ScaleTransform.ScaleYProperty, anim);
                node.RenderTransform = transform;
            }
        }

        void OnDiagramChanged(object sender, RoutedEventArgs e)
        {
            foreach (IMeasureable item in Children.OfType<IMeasureable>())
            {
                UIElement element = (UIElement)item;
                item.Position = new Point(Canvas.GetLeft(element), Canvas.GetTop(element));
            }
        }

        void OnConnectionChanged(object sender, RoutedEventArgs e)
        {
            // this should really be able to update just the one connection, but
            // it can't at the moment...
            IEnumerable<Connection> connections = InternalChildren.OfType<Connection>();
            UIElement[] children = new UIElement[connections.Count()];
            for (int i = 0; i < connections.Count(); ++i)
                children[i] = connections.ElementAt(i);

            foreach (UIElement connection in children)
                InternalChildren.Remove(connection);

            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                New.Move.Core.Motiontree active = (New.Move.Core.Motiontree)Runtime.Database.Active;
                foreach (IConnection connect in active.Connections)
                {
                    Connection connection = Logic.CreateConnection(this, connect);
                    if (connection != null)
                    {
                        Canvas.SetZIndex(connection, Children.Count);
                        Children.Add(connection);
                    }
                }
            }
        }

        void OnConnectionRemoved(object sender, RoutedEventArgs e)
        {
            IEnumerable<Connection> connections = InternalChildren.OfType<Connection>();
            UIElement[] children = new UIElement[connections.Count()];
            for (int i = 0; i < connections.Count(); ++i)
                children[i] = connections.ElementAt(i);
            
            foreach (UIElement connection in children)
                InternalChildren.Remove(connection);

            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                New.Move.Core.Motiontree active = (New.Move.Core.Motiontree)Runtime.Database.Active;
                foreach (IConnection connect in active.Connections)
                {
                    Connection connection = Logic.CreateConnection(this, connect);
                    if (connection != null)
                    {
                        Canvas.SetZIndex(connection, Children.Count);
                        Children.Add(connection);
                    }
                }
            }
        }

        void OnChildrenLoaded(object sender, EventArgs e)
        {
           if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                New.Move.Core.Motiontree active = (New.Move.Core.Motiontree)Runtime.Database.Active;
                foreach (IConnection connect in active.Connections)
                {
                    Connection connection = Logic.CreateConnection(this, connect);
                    if (connection != null)
                    {
                        Canvas.SetZIndex(connection, Children.Count);
                        Children.Add(connection);
                    }
                }
            }

            if (Runtime.Database.Active is New.Move.Core.StateMachine)
            {
                New.Move.Core.StateMachine active = (New.Move.Core.StateMachine)Runtime.Database.Active;
                /*
                foreach (IConnection connect in active.Connections)
                {
                    Connection connection = Logic.CreateConnection(this, connect);
                    if (connection != null)
                    {
                        Canvas.SetZIndex(connection, Children.Count);
                        Children.Add(connection);
                    }
                }
                 */

                foreach (Rage.Move.Core.Dg.Transition transit in active.Transitions)
                {
                    /*
                    Transition transition = Logic.CreateTransition(this, transit);
                    Canvas.SetZIndex(transition, Children.Count);
                    Children.Add(transition);

                    transition.ContextMenu = new ContextMenu();

                    Console.WriteLine("added transition {0}", transit.Id.ToString());

                    MenuItem item = new MenuItem();
                    item.Header = "Delete Transition";
                    item.Tag = transit;
                    item.Click += new RoutedEventHandler(
                        delegate(object obj, RoutedEventArgs args) 
                        { 
                            MenuItem menu_item = (MenuItem)obj;
                            ITransition selected = (ITransition)menu_item.Tag;

                            Console.WriteLine("removed transition {0}", selected.Id.ToString());
                            active.Transitions.Remove(selected);
                            OnDiagramChanged();
                        }
                    );

                    transition.ContextMenu.Items.Add(item);
                     */
                }
            }

            OnDiagramUpdated();
        }
    }
}
