﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace New.Move.Core
{
    interface IDatabaseIssue
    {
        string Description { get; }

        void Fix(Database database);
    }
}
