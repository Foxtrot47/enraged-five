﻿using Move.Core.Restorable;

namespace New.Move.Core
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml;

    /// <summary>
    /// Represents a flag property that can be referenced in an event.
    /// </summary>
    [Serializable]
    public class Flag : RestorableObject, ISerializable, IReferredFlag, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        private const string NameSerialisationTag = "Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        private const string IdSerialisationTag = "Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Value"/> property.
        /// </summary>
        private const string ValueSerialisationTag = "Value";

        /// <summary>
        /// The private field for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field for the <see cref="Value"/> property.
        /// </summary>
        private bool isEnabled;

        /// <summary>
        /// The private field used to the <see cref="UseIcon"/> property.
        /// </summary>
        private int usedCount = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Flag"/> class with the
        /// specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the flag.
        /// </param>
        public Flag(string name)
        {
            this.name = name;
            this.id = Guid.NewGuid();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Flag"/> class as a copy
        /// of the specified instance.
        /// </summary>
        /// <param name="name">
        /// The instance to copy.
        /// </param>
        public Flag(Flag other)
        {
            this.name = other.name;
            this.id = Guid.NewGuid();
            this.isEnabled = other.isEnabled;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Flag"/> class with the
        /// specified name and identifier.
        /// </summary>
        /// <param name="name">
        /// The name of the flag.
        /// </param>
        /// <param name="id">
        /// The identifier this flag will have.
        /// </param>
        public Flag(Guid id, string name)
        {
            this.name = name;
            this.id = id;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Flag"/> class using the
        /// specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public Flag(SerializationInfo info, StreamingContext context)
        {
            this.name = (string)info.GetValue(NameSerialisationTag, typeof(string));
            this.id = (Guid)info.GetValue(IdSerialisationTag, typeof(Guid));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Flag"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> parameter is null.
        /// </exception>
        public Flag(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.name = reader.GetAttribute(NameSerialisationTag);
            this.id = Guid.ParseExact(reader.GetAttribute(IdSerialisationTag), "D");
            reader.Skip();
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs whenever a value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the name this flag can be referenced by.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.NotifyPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets or sets the unique global identifier for this flag.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this flag is enabled or not.
        /// </summary>
        public bool Value
        {
            get
            {
                return isEnabled;
            }

            set
            {
                if (bool.Equals(value, this.isEnabled))
                {
                    return;
                }

                this.isEnabled = value;
                this.NotifyPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Gets the image source used for the icon to display to the user if the signal is
        /// currently not used throughout its owning database.
        /// </summary>
        public ImageSource UsedIcon
        {
            get
            {
                if (this.usedCount > 0)
                {
                    return null;
                }
                else
                {
                    BitmapImage bitmapImage = new BitmapImage();

                    try
                    {
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource =
                            Assembly.GetExecutingAssembly().GetManifestResourceStream(
                            "Move.Core.Images.NotUsed.png");
                        bitmapImage.EndInit();
                        return bitmapImage;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Create a new flag reference, that references a flag that is equal to this.
        /// </summary>
        /// <returns>
        /// A new flag reference that references this flag.
        /// </returns>
        public ReferenceFlag CreateReference()
        {
            return new ReferenceFlag(this.id, this.name);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("flag");

            writer.WriteAttributeString(
                NameSerialisationTag, this.Name);
            writer.WriteAttributeString(
                IdSerialisationTag,
                this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                ValueSerialisationTag,
                this.Value.ToString(CultureInfo.InvariantCulture));

            writer.WriteEndElement();
        }

        /// <summary>
        /// Increases or decreases a reference count that is used to either show or find
        /// the "NotUsed" icon to the user.
        /// </summary>
        /// <param name="used">
        /// A value indicating whether to increase or decrease the reference count.
        /// </param>
        public void SetUsed(bool used)
        {
            if (used)
            {
                this.usedCount++;
            }
            else
            {
                this.usedCount--;
            }

            NotifyPropertyChanged("UsedIcon");
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialize this instance.
        /// </summary>
        /// <param name="info">
        /// The serialization info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerialisationTag, Name);
            info.AddValue(IdSerialisationTag, Id);
        }

        /// <summary>
        /// Fires the <see cref="PropertyChanged"/> event for this class specifying the name
        /// of the property that has changed.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that changed.
        /// </param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    } // New.Move.Core.Flag
} // New.Move.Core
