﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

using Rage.Move.Core.Dag;
using Rage.Move.Core.Dg;
using Rage.Move.Core;
using Rage.Move.Utils;

namespace Rage.Move
{
    public delegate void ActivatedEventHandler();
    public delegate void CollapserClickEventHandler();
    public delegate void PositionChangedEventHandler(Point position);

    public delegate void ConnectionOverEventHandler(IAnchorSender sender);

    public class Collapser : Button
    {
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(Collapser));

        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        public Collapser()
        {
        }
    }

    public class AnchorAdorner : Adorner
    {
        public AnchorAdorner(Diagram diagram, IAnchorSender item, Point source)
            : base(diagram)
        {
            Source = source;

            Pen = new Pen(Brushes.LightSlateGray, 1);
            Pen.LineJoin = PenLineJoin.Round;
            Cursor = Cursors.Cross;

            Diagram = diagram;
            Item = item;

            Diagram.DiagramUpdated += new EventHandler(Diagram_Updated);
        }

        PathGeometry Geometry { get; set; }
        Point Source { get; set; }
        Pen Pen { get; set; }
        
        protected Diagram Diagram { get; private set; }
        protected IAnchorSender Item { get; private set; }

        IAnchorReceiver Hit { get; set; }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!IsMouseCaptured)
                    CaptureMouse();

                Hit = (Item as AnchorItem).HitTest(e.GetPosition(this));

                Geometry = Calculate(e.GetPosition(this));

                InvalidateVisual();
            }
            else
            {
                if (IsMouseCaptured)
                    ReleaseMouseCapture();
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            //base.OnMouseUp(e);

            if (Hit != null)
                Diagram.Logic.OnConnection(Item, Hit);

            if (IsMouseCaptured)
                ReleaseMouseCapture();

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(Diagram);
            if (layer != null)
                layer.Remove(this);
        }

        protected override void OnRender(DrawingContext context)
        {
            base.OnRender(context);

            context.DrawGeometry(null, Pen, Geometry);
            context.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));
        }

        PathGeometry Calculate(Point position)
        {
            PathGeometry geometry = new PathGeometry();

            PathFigure figure = new PathFigure();
            figure.StartPoint = Source;

            PointCollection points = new PointCollection();
            points.Add(new Point(((position.X - Source.X) / 2) + Source.X, Source.Y));
            points.Add(new Point(((position.X - Source.X) / 2) + Source.X, position.Y));
            points.Add(position);

            figure.Segments.Add(new PolyBezierSegment(points, true));
            geometry.Figures.Add(figure);

            return geometry;
        }

        private void Diagram_Updated(object sender, EventArgs e)
        {
            AnchorItem item = Item as AnchorItem;

            foreach (DiagramNode instance in Diagram.Children.OfType<DiagramNode>())
            {
                Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");
                if (PART_Anchors != null)
                {
                    foreach (AnchorItem anchor in PART_Anchors.Children.OfType<IAnchorSender>())
                    {
                        if (anchor.Id == item.Id)
                        {
                            Item = anchor as IAnchorSender;
                            return;
                        }
                    }
                }
            }

            foreach (DiagramState instance in Diagram.Children.OfType<DiagramState>())
            {
                Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");
                if (PART_Anchors != null)
                {
                    foreach (AnchorItem anchor in PART_Anchors.Children.OfType<IAnchorSender>())
                    {
                        if (anchor.Id == item.Id)
                        {
                            Item = anchor as IAnchorSender;
                            return;
                        }
                    }
                }
            }
        }
    }

    public class MessageAdorner : AnchorAdorner
    {
        public MessageAdorner(Diagram diagram, IAnchorSender item, Point source)
            : base(diagram, item, source) 
        {
            Receiver = null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            DependencyObject walk = Item as DependencyObject;
            while (walk != null && (walk is IConnectable) != true)
                walk = VisualTreeHelper.GetParent(walk);

            IConnectable parent = walk as IConnectable;
            if (parent != null)
            {
                IConnectable prev = Receiver;
                Receiver = null;

                DependencyObject hitobj = Diagram.InputHitTest(e.GetPosition(this)) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != parent)
                {
                    if (hitobj is IConnectable)
                    {
                        Receiver = hitobj as IConnectable;
                        break;
                    }

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }

                if (prev == null && Receiver != null)
                {
                    Receiver.OnConnectionEnter(Item);
                }
                else
                if (prev != null && Receiver == null)
                {
                    prev.OnConnectionExit(Item);
                }
                else
                if (prev != Receiver)
                {
                    prev.OnConnectionExit(Item);
                    Receiver.OnConnectionEnter(Item);
                }
            }
        }

        IConnectable Receiver { get; set; }
    }

    public class SignalAdorner : AnchorAdorner
    {
        public SignalAdorner(Diagram diagram, IAnchorSender item, Point source)
            : base(diagram, item, source) { }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            DependencyObject walk = Item as DependencyObject;
            while (walk != null && (walk is IConnectable) != true)
                walk = VisualTreeHelper.GetParent(walk);

            IConnectable parent = walk as IConnectable;
            if (parent != null)
            {
                IConnectable prev = Receiver;
                Receiver = null;

                DependencyObject hitobj = Diagram.InputHitTest(e.GetPosition(this)) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != parent)
                {
                    if (hitobj is IConnectable)
                    {
                        Receiver = hitobj as IConnectable;
                        break;
                    }

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }

                if (prev == null && Receiver != null)
                {
                    Receiver.OnConnectionEnter(Item);
                }
                else
                if (prev != null && Receiver == null)
                {
                    prev.OnConnectionExit(Item);
                }
                else
                if (prev != Receiver)
                {
                    prev.OnConnectionExit(Item);
                    Receiver.OnConnectionEnter(Item);
                }
            }
        }

        IConnectable Receiver { get; set; }
    }

    public interface IAnchorItem
    {
        event PropertyChangedEventHandler PropertyChanged;
    }

    public abstract class AnchorItem : UserControl, IAnchorItem, INotifyPropertyChanged
    {
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(AnchorItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public AnchorItem(string name, Guid id)
        {
            DataContext = this;
            Label = name;
            Id = id;
        }

        public Guid Id { get; private set; }

        public Point _Position;
        public Point Position
        {
            set
            {
                if (_Position != value)
                {
                    _Position = value;
                    OnPropertyChanged("Position");
                }
            }
        }

        public void OnPositionChanged(Point point) { Position = point; }

        public virtual IAnchorReceiver HitTest(Point point) { return null; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        protected Diagram GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }

        protected Point? Start = null;
    }

    public interface IAnchorSender
    {
        Point? SenderPoint { get; }
    }

    public interface IAnchorReceiver
    {
        Point? ReceiverPoint { get; }
    }

    public abstract class ClosedAnchorItem : UserControl, IAnchorItem
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        protected Diagram GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }
    }

    public class ClosedAnchorSourceItem : ClosedAnchorItem, IAnchorReceiver
    {
        public Point? ReceiverPoint
        {
            get
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    if (PART_ReceiverPoint != null)
                        return new Point?(PART_ReceiverPoint.TranslatePoint(new Point(0, 0), diagram));
                }

                return null;
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            PART_ReceiverPoint =
                (FrameworkElement)GetTemplateChild("PART_ReceiverPoint");
        }

        FrameworkElement PART_ReceiverPoint;
    }

    public class ClosedAnchorResultItem : ClosedAnchorItem, IAnchorSender
    {
        public Point? SenderPoint
        {
            get
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    if (PART_SenderPoint != null)
                        return new Point?(PART_SenderPoint.TranslatePoint(new Point(0, 0), diagram));
                }

                return null;
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            PART_SenderPoint = 
                (FrameworkElement)GetTemplateChild("PART_SenderPoint");
        }

        FrameworkElement PART_SenderPoint;
    }

    public class AnchorSourceItem : AnchorItem, IAnchorReceiver
    {
        public AnchorSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id) 
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_ReceiverGrapple = (FrameworkElement)GetTemplateChild("PART_ReceiverGrapple");
            PART_ReceiverGrapple.MouseDown +=
                   new MouseButtonEventHandler(PART_ReceiverGrapple_MouseDown);
            PART_ReceiverPoint = (FrameworkElement)GetTemplateChild("PART_ReceiverPoint");
        }

        protected virtual void PART_ReceiverGrapple_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                Diagram diagram = GetDiagram(this);

                New.Move.Core.Motiontree active = 
                    Runtime.Database.Active as New.Move.Core.Motiontree;

                /*
                IAnchor anchor = active.GetSourceAnchorOf(Id);
                if (anchor == null)
                    return;
                 */

                IAnchorSender source = null;
                foreach (DiagramNode node in diagram.Children.OfType<DiagramNode>())
                {
                    /*
                    if (node.Anchors.Contains(anchor.Id))
                    {
                        source = (IAnchorSender)node.Anchors[anchor.Id];
                        break;
                    }
                     */
                }

                if (source == null)
                    return;

                foreach (IConnection connection in active.Connections)
                {
                    if (connection.Sink == Id)
                    {
                        //active.Connections.Remove(connection);
                        diagram.RaiseEvent(new RoutedEventArgs(Diagram.ConnectionRemovedEvent));

                        break;
                    }
                }

                Point? start = source.SenderPoint;

                if (start.HasValue)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        Adorner adorner = new AnchorAdorner(diagram, source, start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            e.Handled = true;
        }

        public Point? ReceiverPoint
        {
            get
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    if (PART_ReceiverPoint != null)
                        return new Point?(PART_ReceiverPoint.TranslatePoint(new Point(0, 0), diagram));
                }

                return null;
            }
        }

        FrameworkElement PART_ReceiverGrapple { get; set; }
        FrameworkElement PART_ReceiverPoint { get; set; }
    }

    public class MessageSourceItem : AnchorSourceItem
    {
        public MessageSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }
    }

    public abstract class SourceItemBase : AnchorSourceItem
    {
        public SourceItemBase(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        protected override void PART_ReceiverGrapple_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                Diagram diagram = GetDiagram(this);

                New.Move.Core.Motiontree active = Runtime.Database.Active as New.Move.Core.Motiontree;
                /*
                IAnchor anchor = active.GetControlSignalOf(Id);
                if (anchor == null)
                    anchor = active.GetSourceAnchorOf(Id);
                if (anchor == null)
                    return;
                 */

                IAnchorSender source = null;
                foreach (DiagramNode node in diagram.Children.OfType<DiagramNode>())
                {
                    /*
                    if (node.Anchors.Contains(anchor.Id))
                    {
                        source = (IAnchorSender)node.Anchors[anchor.Id];
                        break;
                    }
                     */
                }

                if (source == null)
                    return;

                foreach (IConnection connection in active.Connections)
                {
                    if (connection.Sink == Id)
                    {
                        //active.Connections.Remove(connection);
                        diagram.RaiseEvent(new RoutedEventArgs(Diagram.ConnectionRemovedEvent));

                        break;
                    }
                }

                Point? start = source.SenderPoint;

                if (start.HasValue)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        Adorner adorner = new SignalAdorner(diagram, source, start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }
        }
    }

    public class SignalSourceItem : SourceItemBase
    {
        public SignalSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }
    }

    public class AnimationSourceItem : SourceItemBase
    {
        public AnimationSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.AnimationDataType), typeof(AnimationSourceItem));

        public Rage.Move.Core.Dg.Properties.AnimationDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.AnimationDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class BooleanSourceItem : SourceItemBase
    {
        public BooleanSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.BooleanDataType), typeof(BooleanSourceItem));

        public Rage.Move.Core.Dg.Properties.BooleanDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.BooleanDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class RealSourceItem : SourceItemBase
    {
        public RealSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.RealDataType), typeof(RealSourceItem));

        public Rage.Move.Core.Dg.Properties.RealDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.RealDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }
    /*
    public class WeightSourceItem : SourceItemBase
    {
        public WeightSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) {}

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.WeightDataType), typeof(WeightSourceItem));
        
        public Rage.Move.Core.Dg.Properties.WeightDataType Source
        { 
            get { return (Rage.Move.Core.Dg.Properties.WeightDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
    }
    */
    // TODO: change these over too

    public class ClipSourceItem : SourceItemBase
    {
        public ClipSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.ClipDataType), typeof(ClipSourceItem));

        public Rage.Move.Core.Dg.Properties.ClipDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.ClipDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class ExpressionSourceItem : SourceItemBase
    {
        public ExpressionSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.ExpressionsDataType), typeof(ExpressionSourceItem));

        public Rage.Move.Core.Dg.Properties.ExpressionsDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.ExpressionsDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class FilterSourceItem : SourceItemBase
    {
        public FilterSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }
        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.FilterDataType), typeof(FilterSourceItem));

        public Rage.Move.Core.Dg.Properties.FilterDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.FilterDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class FrameSourceItem : SourceItemBase
    {
        public FrameSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.FrameDataType), typeof(FrameSourceItem));

        public Rage.Move.Core.Dg.Properties.FrameDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.FrameDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class ParameterizedMotionSourceItem : SourceItemBase
    {
        public ParameterizedMotionSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        /*
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Rage.Move.Core.Dg.Properties.ParameterizedMotionDataType), typeof(ParameterizedMotionSourceItem));

        public Rage.Move.Core.Dg.Properties.ParameterizedMotionDataType Source
        {
            get { return (Rage.Move.Core.Dg.Properties.ParameterizedMotionDataType)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
         */
    }

    public class TransformSourceItem : SourceItemBase
    {
        public TransformSourceItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }
    }

    public class AnchorResultItem : AnchorItem, IAnchorSender
    {
        public AnchorResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id) 
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_SenderPoint = (FrameworkElement)GetTemplateChild("PART_SenderPoint");
        }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is AnchorSourceItem || hitobj is AnchorPassthroughItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                Point position = PART_SenderPoint.TranslatePoint(new Point(0, 0), diagram);
                Start = new Point?(position);
                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        public Point? SenderPoint
        {
            get
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    if (PART_SenderPoint != null)
                        return new Point?(PART_SenderPoint.TranslatePoint(new Point(0, 0), diagram));
                }

                return null;
            }
        }

        FrameworkElement PART_SenderPoint { get; set; }
    }

    public class MessageResultItem : AnchorResultItem
    {
        public MessageResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is MessageSourceItem || hitobj is MessagePassthroughItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        MessageAdorner adorner = new MessageAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

    }

    public class SignalResultItem : AnchorResultItem
    {
        public SignalResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is SignalSourceItem || hitobj is SignalPassthroughItem || hitobj is OperatorSourceItem || hitobj is RealSourceItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class AnimationResultItem : AnchorResultItem
    {
        public AnimationResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is AnimationSourceItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class ClipResultItem : AnchorResultItem
    {
        public ClipResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is ClipSourceItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class ExpressionResultItem : AnchorResultItem
    {
        public ExpressionResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is ExpressionSourceItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class FilterResultItem : AnchorResultItem
    {
        public FilterResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is FilterSourceItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class FrameResultItem : AnchorResultItem
    {
        public FrameResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is FrameSourceItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class ParameterizedMotionResultItem : AnchorResultItem
    {
        public ParameterizedMotionResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is ParameterizedMotionSourceItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class TransformResultItem : AnchorResultItem
    {
        public TransformResultItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is TransformSourceItem || hitobj is TransformPassthroughItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        AnchorAdorner adorner = new AnchorAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class AnchorPassthroughItem : AnchorItem, IAnchorSender, IAnchorReceiver
    {
        public AnchorPassthroughItem(string name, Guid id, FrameworkElement parent)
            : base(name, id) 
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_SenderPoint = (FrameworkElement)GetTemplateChild("PART_SenderPoint");
            PART_ReceiverGrapple = (FrameworkElement)GetTemplateChild("PART_ReceiverGrapple");
            PART_ReceiverGrapple.MouseDown += 
                new MouseButtonEventHandler(PART_ReceiverGrapple_MouseDown);
            PART_ReceiverPoint = (FrameworkElement)GetTemplateChild("PART_ReceiverPoint");
        }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is AnchorSourceItem || hitobj is AnchorPassthroughItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected virtual void PART_ReceiverGrapple_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                Diagram diagram = GetDiagram(this);

                New.Move.Core.Motiontree active = 
                    Runtime.Database.Active as New.Move.Core.Motiontree;

                /*
                IAnchor anchor = active.GetSourceAnchorOf(Id);
                if (anchor == null)
                    return;
                */
                IAnchorSender source = null;
                foreach (DiagramNode node in diagram.Children.OfType<DiagramNode>())
                {
                    /*
                    if (node.Anchors.Contains(anchor.Id))
                    {
                        source = (IAnchorSender)node.Anchors[anchor.Id];
                        break;
                    }
                     */
                }

                if (source == null)
                    return;

                foreach (IConnection connection in active.Connections)
                {
                    if (connection.Sink == Id)
                    {
                        //active.Connections.Remove(connection);
                        diagram.RaiseEvent(new RoutedEventArgs(Diagram.ConnectionRemovedEvent));

                        break;
                    }
                }

                Point? start = source.SenderPoint;

                if (start.HasValue)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        Adorner adorner = new AnchorAdorner(diagram, source, start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            e.Handled = true;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                Point position = PART_SenderPoint.TranslatePoint(new Point(0, 0), diagram);
                Start = new Point?(position);
                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        public Point? SenderPoint 
        {
            get 
            { 
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    if (PART_SenderPoint != null)
                        return new Point?(PART_SenderPoint.TranslatePoint(new Point(0, 0), diagram));
                }

                return null;
            }
        }

        public Point? ReceiverPoint
        {
            get
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    if (PART_ReceiverPoint != null)
                        return new Point?(PART_ReceiverPoint.TranslatePoint(new Point(0, 0), diagram));
                }

                return null;
            }
        }

        FrameworkElement PART_SenderPoint { get; set; }
        FrameworkElement PART_ReceiverGrapple { get; set; }
        FrameworkElement PART_ReceiverPoint { get; set; }
    }

    public class MessagePassthroughItem : AnchorPassthroughItem
    {
        public MessagePassthroughItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is SignalSourceItem || hitobj is SignalPassthroughItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        MessageAdorner adorner = new MessageAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class SignalPassthroughItem : AnchorPassthroughItem
    {
        public SignalPassthroughItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is SignalSourceItem || hitobj is SignalPassthroughItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        SignalAdorner adorner = new SignalAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public class TransformPassthroughItem : AnchorPassthroughItem
    {
        public TransformPassthroughItem(string name, Guid id, FrameworkElement parent)
            : base(name, id, parent) { }

        public override IAnchorReceiver HitTest(Point point)
        {
            Diagram diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (hitobj is TransformSourceItem || hitobj is TransformPassthroughItem)
                        return hitobj as IAnchorReceiver;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                Diagram diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        AnchorAdorner adorner = new AnchorAdorner(diagram, this as IAnchorSender, Start.Value);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }
    }

    public static class Anchor
    {
        public static List<AnchorItem> CreateAnchor(this Rage.Move.Core.Dg.Properties.Property item, FrameworkElement parent, string name)
        {
            /*
            if (item is Rage.Move.Core.Dg.Properties.Animation)
            {
                AnimationSourceItem anchor = new AnimationSourceItem(name, item.Id, parent);

                item.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
                    delegate(Rage.Move.Core.Dg.Properties.Property property)
                    {
                        Rage.Move.Core.Dg.Properties.Animation animation = 
                            (Rage.Move.Core.Dg.Properties.Animation)property;
                        anchor.Source = animation.Source;
                    });

                Rage.Move.Core.Dg.Properties.Animation prop = (Rage.Move.Core.Dg.Properties.Animation)item;
                anchor.Source = prop.Source;

                List<AnchorItem> anchors = new List<AnchorItem>();
                anchors.Add(anchor);
                return anchors;
            }
            else 
             */
            /*
             if (item is Rage.Move.Core.Dg.Properties.Clip)
            {
                ClipSourceItem anchor = new ClipSourceItem(name, item.Id, parent);

                item.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
                    delegate(Rage.Move.Core.Dg.Properties.Property property)
                    {
                        Rage.Move.Core.Dg.Properties.Clip clip =
                            (Rage.Move.Core.Dg.Properties.Clip)property;
                        anchor.Source = clip.Source;
                    });

                Rage.Move.Core.Dg.Properties.Clip prop = (Rage.Move.Core.Dg.Properties.Clip)item;
                anchor.Source = prop.Source;

                List<AnchorItem> anchors = new List<AnchorItem>();
                anchors.Add(anchor);
                return anchors;
            }
             */
            /*
       else 
       if (item is Rage.Move.Core.Dg.Properties.Boolean)
       {
           BooleanSourceItem anchor = new BooleanSourceItem(name, item.Id, parent);

           item.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
               delegate(Rage.Move.Core.Dg.Properties.Property property)
               {
                   Rage.Move.Core.Dg.Properties.Boolean boolean =
                       (Rage.Move.Core.Dg.Properties.Boolean)property;
                   anchor.Source = boolean.Source;
               });

           Rage.Move.Core.Dg.Properties.Boolean prop = (Rage.Move.Core.Dg.Properties.Boolean)item;
           anchor.Source = prop.Source;

           List<AnchorItem> anchors = new List<AnchorItem>();
           anchors.Add(anchor);
           return anchors;
       }
                 
       else  
       if (item is Rage.Move.Core.Dg.Properties.Expressions)
       {
           ExpressionSourceItem anchor = new ExpressionSourceItem(name, item.Id, parent);

           item.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
               delegate(Rage.Move.Core.Dg.Properties.Property property)
               {
                   Rage.Move.Core.Dg.Properties.Expressions expressions =
                       (Rage.Move.Core.Dg.Properties.Expressions)property;
                   anchor.Source = expressions.Source;
               });

           Rage.Move.Core.Dg.Properties.Expressions prop = (Rage.Move.Core.Dg.Properties.Expressions)item;
           anchor.Source = prop.Source;

           List<AnchorItem> anchors = new List<AnchorItem>();
           anchors.Add(anchor);
           return anchors;
       }
       else
            if (item is Rage.Move.Core.Dg.Properties.Filter)
            {
                FilterSourceItem anchor = new FilterSourceItem(name, item.Id, parent);

                item.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
                    delegate(Rage.Move.Core.Dg.Properties.Property property)
                    {
                        Rage.Move.Core.Dg.Properties.Filter filter =
                            (Rage.Move.Core.Dg.Properties.Filter)property;
                        anchor.Source = filter.Source;
                    });

                Rage.Move.Core.Dg.Properties.Filter prop = (Rage.Move.Core.Dg.Properties.Filter)item;
                anchor.Source = prop.Source;

                List<AnchorItem> anchors = new List<AnchorItem>();
                anchors.Add(anchor);
                return anchors;
            }
            else 
            if (item is Rage.Move.Core.Dg.Properties.Frame)
            {
                FrameSourceItem anchor = new FrameSourceItem(name, item.Id, parent);

                item.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
                    delegate(Rage.Move.Core.Dg.Properties.Property property)
                    {
                        Rage.Move.Core.Dg.Properties.Frame frame =
                            (Rage.Move.Core.Dg.Properties.Frame)property;
                        anchor.Source = frame.Source;
                    });

                Rage.Move.Core.Dg.Properties.Frame prop = (Rage.Move.Core.Dg.Properties.Frame)item;
                anchor.Source = prop.Source;

                List<AnchorItem> anchors = new List<AnchorItem>();
                anchors.Add(anchor);
                return anchors;
            }
            /*
        else if (item is Rage.Move.Core.Dg.Properties.ParameterizedMotion)
        {
            ParameterizedMotionSourceItem anchor = new ParameterizedMotionSourceItem(name, item.Id, parent);

            item.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
                delegate(Rage.Move.Core.Dg.Properties.Property property)
                {
                    Rage.Move.Core.Dg.Properties.ParameterizedMotion pm =
                        (Rage.Move.Core.Dg.Properties.ParameterizedMotion)property;
                    anchor.Source = pm.Source;
                });

            Rage.Move.Core.Dg.Properties.ParameterizedMotion prop =
                (Rage.Move.Core.Dg.Properties.ParameterizedMotion)item;
            anchor.Source = prop.Source;

            List<AnchorItem> anchors = new List<AnchorItem>();
            anchors.Add(anchor);
            return anchors;
        }
                 
        else if (item is Rage.Move.Core.Dg.Properties.RealArray)
        {
            List<AnchorItem> anchors = new List<AnchorItem>();

            Rage.Move.Core.Dg.Properties.RealArray property =
                (Rage.Move.Core.Dg.Properties.RealArray)item;

            foreach (Rage.Move.Core.Dg.Properties.Real real in property.Items)
            {
                RealSourceItem anchor = new RealSourceItem(name, real.Id, parent);

                real.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
                    delegate(Rage.Move.Core.Dg.Properties.Property prop)
                    {
                        Rage.Move.Core.Dg.Properties.Real r =
                            (Rage.Move.Core.Dg.Properties.Real)prop;
                        anchor.Source = r.Source;
                    });

                Rage.Move.Core.Dg.Properties.Real p = (Rage.Move.Core.Dg.Properties.Real)real;
                anchor.Source = p.Source;

                anchors.Add(anchor);
            }

            return anchors;
        }
             **/
            /*
        else 
        if (item is Rage.Move.Core.Dg.Properties.Real)
        {
            RealSourceItem anchor = new RealSourceItem(name, item.Id, parent);

            item.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
                delegate(Rage.Move.Core.Dg.Properties.Property property)
                {
                    Rage.Move.Core.Dg.Properties.Real real =
                        (Rage.Move.Core.Dg.Properties.Real)property;
                    anchor.Source = real.Source;
                });

            Rage.Move.Core.Dg.Properties.Real prop = (Rage.Move.Core.Dg.Properties.Real)item;
            anchor.Source = prop.Source;

            List<AnchorItem> anchors = new List<AnchorItem>();
            anchors.Add(anchor);
            return anchors;
        }
                 
        else if (item is Rage.Move.Core.Dg.Properties.WeightArray)
        {
            List<AnchorItem> anchors = new List<AnchorItem>();

            Rage.Move.Core.Dg.Properties.WeightArray property =
                (Rage.Move.Core.Dg.Properties.WeightArray)item;

            foreach (Rage.Move.Core.Dg.Properties.Weight weight in property.Items)
            {
                RealSourceItem anchor = new RealSourceItem(name, weight.Id, parent);

                weight.PropertySourceChanged += new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(
                    delegate(Rage.Move.Core.Dg.Properties.Property prop)
                    {
                        Rage.Move.Core.Dg.Properties.Weight w =
                            (Rage.Move.Core.Dg.Properties.Weight)prop;
                        anchor.Source = w.Source;
                    });

                Rage.Move.Core.Dg.Properties.Weight p = (Rage.Move.Core.Dg.Properties.Weight)weight;
                anchor.Source = p.Source;

                anchors.Add(anchor);

                TransformSourceItem transform = new TransformSourceItem(name, weight.TransformId, parent);
                anchors.Add(transform);
            }

            return anchors;
        }
             */
            if (item is Rage.Move.Core.Dg.Properties.SourceArray)
            {
                List<AnchorItem> anchors = new List<AnchorItem>();

                Rage.Move.Core.Dg.Properties.SourceArray property =
                    (Rage.Move.Core.Dg.Properties.SourceArray)item;

                foreach (SourceTransform transform in property.Items)
                {
                    TransformSourceItem anchor = new TransformSourceItem(name, transform.Id, parent);
                    anchors.Add(anchor);
                }

                return anchors;
            }

            return null;
        }

        public static AnchorItem Create(this IAnchor item, FrameworkElement parent, string name)
        {
            if (item is IInputAnchor_Animation)
            {
                AnimationSourceItem source = new AnimationSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is TransformPassthrough)
            {
                TransformPassthroughItem passthrough = new TransformPassthroughItem(name, item.Id, parent);
                return passthrough;
            }
            else 
            if (item is TransformSource)
            {
                TransformSourceItem source = new TransformSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is TransformResult)
            {
                TransformResultItem result = new TransformResultItem(name, item.Id, parent);
                return result;
            }
            else 
            if (item is MessageSource)
            {
                MessageSourceItem source = new MessageSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is MessageResult)
            {
                MessageResultItem result = new MessageResultItem(name, item.Id, parent);
                return result;
            }
            else 
            if (item is SignalSource)
            {
                SignalSourceItem source = new SignalSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is SignalResult)
            {
                SignalResultItem result = new SignalResultItem(name, item.Id, parent);
                return result;
            }
            else if (item is OperatorSource)
            {
                OperatorSourceItem source = new OperatorSourceItem(name, item.Id, parent);
                return source;
            }
            else if (item is OperatorResult)
            {
                OperatorResultItem result = new OperatorResultItem(name, item.Id, parent);
                return result;
            }
            else 
            if (item is AnimationSource)
            {
                AnimationSourceItem source = new AnimationSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is AnimationResult)
            {
                AnimationResultItem result = new AnimationResultItem(name, item.Id, parent);
                return result;
            }
            else 
            if (item is ClipSource)
            {
                ClipSourceItem source = new ClipSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is ClipResult)
            {
                ClipResultItem result = new ClipResultItem(name, item.Id, parent);
                return result;
            }
            else 
            if (item is ExpressionSource)
            {
                ExpressionSourceItem source = new ExpressionSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is ExpressionResult)
            {
                ExpressionResultItem result = new ExpressionResultItem(name, item.Id, parent);
                return result;
            }
            else 
            if (item is FilterSource)
            {
                FilterSourceItem source = new FilterSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is FilterResult)
            {
                FilterResultItem result = new FilterResultItem(name, item.Id, parent);
                return result;
            }
            else 
            if (item is FrameSource)
            {
                FrameSourceItem source = new FrameSourceItem(name, item.Id, parent);
                return source;
            }
            else 
            if (item is FrameResult)
            {
                FrameResultItem result = new FrameResultItem(name, item.Id, parent);
                return result;
            }
            else 
            if (item is ParameterizedMotionSource)
            {
                ParameterizedMotionSourceItem source = new ParameterizedMotionSourceItem(name, item.Id, parent);
                return source;
            }
            else
            if (item is ParameterizedMotionResult)
            {
                ParameterizedMotionResultItem result = new ParameterizedMotionResultItem(name, item.Id, parent);
                return result;
            }

             return null;
        }

        public static AnchorItem Create(this IAnchor item, FrameworkElement parent)
        {
            return Create(item, parent, item.Name);
        }
    }

    public class ChannelMenuItem : MenuItem
    {
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(SolidColorBrush), typeof(ChannelMenuItem));

        public SolidColorBrush Color
        {
            get { return (SolidColorBrush)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(ChannelMenuItem));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public ChannelMenuItem()
        {
            
        }
    }

    public class Channel : UserControl
    {
        public Channel()
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            ChannelMenuItem channel1 = (ChannelMenuItem)GetTemplateChild("ChannelMenuItemOne");
            channel1.Click +=
                new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Channels.Instance.Select(DataContext as IChannelable, ChannelIndex.Channel1);
                    }
            );

            ChannelMenuItem channel2 = (ChannelMenuItem)GetTemplateChild("ChannelMenuItemTwo");
            channel2.Click +=
                new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Channels.Instance.Select(DataContext as IChannelable, ChannelIndex.Channel2);
                    }
            );

            ChannelMenuItem channel3 = (ChannelMenuItem)GetTemplateChild("ChannelMenuItemThree");
            channel3.Click +=
                new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Channels.Instance.Select(DataContext as IChannelable, ChannelIndex.Channel3);
                    }
            );

            ChannelMenuItem channel4 = (ChannelMenuItem)GetTemplateChild("ChannelMenuItemFour");
            channel4.Click +=
                new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Channels.Instance.Select(DataContext as IChannelable, ChannelIndex.Channel4);
                    }
            );

            ChannelMenuItem channel5 = (ChannelMenuItem)GetTemplateChild("ChannelMenuItemFive");
            channel5.Click +=
                new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Channels.Instance.Select(DataContext as IChannelable, ChannelIndex.Channel5);
                    }
            );

            ChannelMenuItem channel6 = (ChannelMenuItem)GetTemplateChild("ChannelMenuItemSix");
            channel6.Click +=
                new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Channels.Instance.Select(DataContext as IChannelable, ChannelIndex.Channel6);
                    }
            );

            ChannelMenuItem channel7 = (ChannelMenuItem)GetTemplateChild("ChannelMenuItemSeven");
            channel7.Click +=
                new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e)
                    {
                        Channels.Instance.Select(DataContext as IChannelable, ChannelIndex.Channel7);
                    }
            );
        }
    }

    public partial class DiagramNode : UserControl, IMeasureable, ISelectable, IConnectable, IChannelable
    {
        private static class Const
        {
            public static double OpacityFiltered = 0.15;
            public static double OpacityNormal = 1.0;
            public static double AnimationDuration = 300;
        }

        public static readonly RoutedEvent PositionChangedEvent = EventManager.RegisterRoutedEvent(
           "PositionChanged", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(DiagramNode));

        public event EventHandler ChildrenLoaded;
        public void OnChildrenLoaded()
        {
            if (ChildrenLoaded != null)
                ChildrenLoaded(this, EventArgs.Empty);
        }

        public event ConnectionOverEventHandler ConnectionEnter;
        public void OnConnectionEnter(IAnchorSender sender)
        {
            //Console.WriteLine("OnConnectionEnter");
            if (ConnectionEnter != null)
                ConnectionEnter(sender);
        }

        public event ConnectionOverEventHandler ConnectionExit;
        public void OnConnectionExit(IAnchorSender sender)
        {
            //Console.WriteLine("OnConnectionExit");
            if (ConnectionExit != null)
                ConnectionExit(sender);
        }

        int _Count;
        public int Count
        {
            get { return _Count; }
            set
            {
                _Count = value;

                Panel PART_Anchors = (Panel)GetTemplateChild("PART_Anchors");
                if (PART_Anchors != null)
                {
                    if (PART_Anchors.Children.Count == _Count)
                        OnChildrenLoaded();
                }
            }
        }

        Point _Position;
        public Point Position 
        { 
            get { return _Position; } 
            set 
            {
                if (_Position != value)
                {
                    _Position = value;
                    if (PositionChanged != null) PositionChanged(_Position);
                }
            } 
        }

        public Guid Id { get; private set; }

        Hashtable _Anchors = new Hashtable();
        public Hashtable Anchors { get { return _Anchors; } }

        public static readonly DependencyProperty LabelProperty =
         DependencyProperty.Register("Label", typeof(string), typeof(DiagramNode));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public bool IsClosed { get; set; }

        public DiagramNode(Guid id)
        {
            InitializeComponent();
            DataContext = this;
            Id = id;

            Layer = new SolidColorBrush();

            IsClosed = false;

            AddHandler(PositionChangedEvent, new RoutedEventHandler(OnPositionChanged), true);
        }

        void OnPositionChanged(object sender, RoutedEventArgs e)
        {
            Position = new Point(Canvas.GetLeft(this), Canvas.GetTop(this));

            ClosedSourceItem.OnPropertyChanged("Position");
            ClosedResultItem.OnPropertyChanged("Position");
        }

        public event ActivatedEventHandler Activated = null;
        public event CollapserClickEventHandler CollapserClick = null;
        public event PositionChangedEventHandler PositionChanged = null;
        
        public Double Scale { get; set; }

        public static readonly DependencyProperty SelectedProperty =
            DependencyProperty.Register("Selected", typeof(bool), typeof(DiagramNode), new FrameworkPropertyMetadata(false));

        public bool Selected
        {
            get { return (bool)GetValue(SelectedProperty); }
            set { SetValue(SelectedProperty, value); }
        }

        public static readonly DependencyProperty IsLayeredProperty =
            DependencyProperty.Register("IsLayered", typeof(bool), typeof(DiagramNode), new FrameworkPropertyMetadata(false));

        public bool IsLayered
        {
            get { return (bool)GetValue(IsLayeredProperty); }
            set { SetValue(IsLayeredProperty, value); }
        }

        public static readonly DependencyProperty LayerProperty =
            DependencyProperty.Register("Layer", typeof(SolidColorBrush), typeof(DiagramNode));

        public SolidColorBrush Layer
        {
            get { return (SolidColorBrush)GetValue(LayerProperty); }
            set { SetValue(LayerProperty, value); }
        }

        public static readonly DependencyProperty ChannelProperty =
            DependencyProperty.Register("Channel", typeof(ChannelIndex), typeof(DiagramNode));

        public ChannelIndex Channel
        {
            get { return (ChannelIndex)GetValue(ChannelProperty); }
            set { SetValue(ChannelProperty, value); }
        }

        public static readonly DependencyProperty IsDragConnectionOverProperty =
            DependencyProperty.Register("IsDragConnectionOver", typeof(bool), typeof(DiagramNode), new FrameworkPropertyMetadata(false));

        public bool IsDragConnectionOver
        {
            get { return (bool)GetValue(IsDragConnectionOverProperty); }
            set { SetValue(IsDragConnectionOverProperty, value); }
        }

        public static readonly DependencyProperty AnchorVisibilityProperty =
            DependencyProperty.Register("AnchorVisibility", typeof(Visibility), typeof(DiagramNode));

        public Visibility AnchorVisibility
        {
            get { return (Visibility)GetValue(AnchorVisibilityProperty); }
            set { SetValue(AnchorVisibilityProperty, value); }
        }

        public static readonly DependencyProperty CollapserVisibilityProperty =
            DependencyProperty.Register("CollapserVisibility", typeof(Visibility), typeof(DiagramNode));

        public Visibility CollapserVisibility
        {
            get { return (Visibility)GetValue(CollapserVisibilityProperty); }
            set { SetValue(CollapserVisibilityProperty, value); }
        }

        public static readonly DependencyProperty ChannelVisibilityProperty =
            DependencyProperty.Register("ChannelVisibility", typeof(Visibility), typeof(DiagramNode));

        public Visibility ChannelVisibility
        {
            get { return (Visibility)GetValue(ChannelVisibilityProperty); }
            set { SetValue(ChannelVisibilityProperty, value); } 
        }

        public Point Center
        { 
            get { return new Point( Position.X + (DesiredSize.Width / 2), Position.Y + (DesiredSize.Height / 2)); } 
        }

        public Point TopCenter
        { 
            get { return new Point(Position.X + (DesiredSize.Width / 2), Position.Y); } 
        }
        
        public Point TopRight
        { 
            get { return new Point(Position.X + DesiredSize.Width, Position.Y); } 
        }

        public Point TopLeft 
        { 
            get { return new Point(Position.X, Position.Y); } 
        }

        public Point BottomCenter 
        { 
            get { return new Point(Position.X + (DesiredSize.Width / 2), Position.Y + DesiredSize.Height); } 
        }

        public Point LeftCenter 
        { 
            get { return new Point(Position.X, Position.Y + (DesiredSize.Height / 2)); } 
        }

        public Point RightCenter 
        { 
            get { return new Point(Position.X + DesiredSize.Width, Position.Y + (DesiredSize.Height / 2)); } 
        }

        public double Left
        {
            get { return Position.X; }
            set { Position = new Point(value, Position.Y); }
        }

        public double Top
        {
            get { return Position.Y; }
            set { Position = new Point(Position.X, value); }
        }

        protected override void OnPreviewMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDoubleClick(e);

            if (e.ChangedButton == MouseButton.Left)
            {
                if (Activated != null)
                    Activated();

                e.Handled = true;
            }
        }
        
        protected event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void CreateAnchors(LogicNode source, Collapsability collapsability)
        {
            StackPanel PART_Anchors = (StackPanel)GetTemplateChild("PART_Anchors");
            //PART_Anchors.Children.Clear(); this will need to be un commented later on
            //_Anchors.Clear();

            System.Reflection.PropertyInfo[] properties = 
                source.GetType().GetProperties(System.Reflection.BindingFlags.Public|System.Reflection.BindingFlags.Instance);
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                object value = property.GetValue(source, null);
                if (value.GetType().GetInterface("IAnchor") != null)
                {
                    IAnchor anchor = (IAnchor)property.GetValue(source, null);
                    AnchorItem item = anchor.Create(this, property.Name);
                    item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++Count; });
                    PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                    PART_Anchors.Children.Add(item);

                    _Anchors.Add(item.Id, item);
                }
            }

            // add anchors for the properties
            foreach (System.Reflection.PropertyInfo pi in properties)
            {
                object value = pi.GetValue(source, null);
                if (value.GetType().GetInterface("Property") != null)
                {
                    Rage.Move.Core.Dg.Properties.Property property = 
                        (Rage.Move.Core.Dg.Properties.Property)pi.GetValue(source, null);
                    List<AnchorItem> items = property.CreateAnchor(this, pi.Name);
                    foreach (AnchorItem item in items)
                    {
                        item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++Count; });
                        PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                        PART_Anchors.Children.Add(item);

                        _Anchors.Add(item.Id, item);
                    }
                }
            }
        }

        public void Attach(IEnumerable items, Collapsability collapsability)
        {
            if (Visibility != Visibility.Visible)
                return;

            StackPanel PART_Anchors = (StackPanel)GetTemplateChild("PART_Anchors");
            PART_Anchors.Children.Clear();
            _Anchors.Clear();

            foreach (IAnchor anchor in items)
            {
                AnchorItem item = anchor.Create(this);
                item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++Count; });
                PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                PART_Anchors.Children.Add(item);

                switch (collapsability)
                {
                    case Collapsability.Collapsed:
                        {
                            bool connected = false;

                            if (Runtime.Database.Active is New.Move.Core.Motiontree)
                            {
                                New.Move.Core.Motiontree active = (New.Move.Core.Motiontree)Runtime.Database.Active;
                                foreach (IConnection connection in active.Connections)
                                {
                                    if (item.Id == connection.Source || item.Id == connection.Sink)
                                    {
                                        connected = true;
                                        break;
                                    }
                                }
                            }

                            if (!connected)
                                item.Visibility = Visibility.Collapsed;
                        }
                        break;
                    case Collapsability.Open:
                        {
                            item.Visibility = Visibility.Visible;
                        }
                        break;
                    case Collapsability.Closed:
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                        break;
                }

                _Anchors.Add(item.Id, item);
            }
        }

        public object GetChild(string name)
        {
            return GetTemplateChild(name);
        }

        void Channel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (Channel == ChannelIndex.ChannelNone)
                    Channels.Instance.Select(this, ChannelIndex.Channel1);
                else
                    Channels.Instance.Deselect(Channel);

                e.Handled = true;
            }
        }

        public void OnCollapsabilityChanged(Collapsability collapsability)
        {
            Collapser collapser = (Collapser)GetTemplateChild("PART_Collapser");

            switch (collapsability)
            {
                case Collapsability.Collapsed:
                    {
                        foreach (AnchorItem item in _Anchors.Values)
                        {
                            bool connected = false;

                            if (Runtime.Database.Active is New.Move.Core.Motiontree)
                            {
                                New.Move.Core.Motiontree active = (New.Move.Core.Motiontree)Runtime.Database.Active;
                                foreach (IConnection connection in active.Connections)
                                {
                                    if (item.Id == connection.Source || item.Id == connection.Sink)
                                    {
                                        connected = true;
                                        break;
                                    }
                                }
                            }

                            if (!connected)
                                item.Visibility = Visibility.Collapsed;
                        }

                        AnchorVisibility = Visibility.Collapsed;

                        IsClosed = false;

                        collapser.Angle = 270.0f;

                        Diagram diagram = DiagramViewer.Instance.Diagram;
                        diagram.OnDiagramChanged();
                    }
                    break;
                case Collapsability.Open:
                    {
                        foreach (AnchorItem item in _Anchors.Values)
                            item.Visibility = Visibility.Visible;

                        AnchorVisibility = Visibility.Collapsed;

                        IsClosed = false;

                        collapser.Angle = 0.0f;

                        Diagram diagram = DiagramViewer.Instance.Diagram;
                        diagram.OnDiagramChanged();
                    }
                    break;
                case Collapsability.Closed:
                    {
                        foreach (AnchorItem item in _Anchors.Values)
                            item.Visibility = Visibility.Collapsed;

                        AnchorVisibility = Visibility.Visible;

                        IsClosed = true;

                        collapser.Angle = 180.0f;

                        Diagram diagram = DiagramViewer.Instance.Diagram;
                        diagram.OnDiagramChanged();
                    }
                    break;
            }
        }

        public ClosedAnchorSourceItem ClosedSourceItem
        {
            get { return (ClosedAnchorSourceItem)GetTemplateChild("ClosedSourceItem"); }
        }

        public ClosedAnchorResultItem ClosedResultItem
        {
            get { return (ClosedAnchorResultItem)GetTemplateChild("ClosedResultItem"); }
        }

        void Collapser_Click(object sender, RoutedEventArgs e)
        {
            if (CollapserClick != null)
                CollapserClick();
        }

        public override void OnApplyTemplate()
        {
            /*
            Collapser collapser = (Collapser)GetTemplateChild("PART_Collapser");
            LogicNodeBase node = 
                (LogicNodeBase)DiagramViewer.Instance.Diagram.Logic.GetLogicNode(this);

            if (node == null)
                return;

            switch (node.Collapsability)
            {
                case Collapsability.Collapsed:
                    collapser.Angle = 270.0f;
                    break;
                case Collapsability.Open:
                    collapser.Angle = 0.0f;
                    break;
                case Collapsability.Closed:
                    collapser.Angle = 180.0f;
                    break;
            }
             */
        }
    }
}
