﻿using System;
using System.Runtime.Serialization;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel
{
    [Serializable]
    public class NamedFloatVariableArray : LogicalPropertyArray<NamedFloatVariable>, ISerializable
    {
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public NamedFloatVariableArray(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public NamedFloatVariableArray()
            : base()
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }
        }

        public override object Clone()
        {
            NamedFloatVariableArray clone = new NamedFloatVariableArray();
            clone.Name = this.Name;
            clone.Parent = this.Parent;

            foreach (var item in this.Items)
            {
                NamedFloatVariable newItem = item.Clone() as NamedFloatVariable;
                clone.Items.Add(newItem);
            }
            return clone;
        }
    }
}