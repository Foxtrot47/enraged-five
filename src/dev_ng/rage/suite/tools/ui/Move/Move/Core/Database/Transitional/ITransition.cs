﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    public interface ITransition : ISelectable, ISerializable
    {
        Guid Id { get; }

        ITransitional Source { get; }
        ITransitional Dest { get; }
        IAutomaton Parent { get; }

        Type Descriptor { get; }
    }
}