﻿using System;

namespace New.Move.Core
{
    public class ConnectionProxyEventArgs : EventArgs
    {
        public ConnectionProxyEventArgs(bool proxy)
        {
            IsProxy = proxy;
        }

        public bool IsProxy { get; private set; }
    }
}