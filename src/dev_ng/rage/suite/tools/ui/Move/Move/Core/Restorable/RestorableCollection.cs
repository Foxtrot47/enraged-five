﻿using System.Collections.ObjectModel;
using System;

namespace Move.Core.Restorable
{
    [Serializable]
    internal class RestorableCollection : KeyedCollection<string, RestorableItem>
    {
        protected override string GetKeyForItem(RestorableItem item)
        {
            return item.Name;
        }
    }
}