﻿using System;

namespace New.Move.Core
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ParameterExportAttribute : Attribute
    {
        public ParameterExportAttribute(uint[] indices)
        {
            _Indices = new uint[indices.Length+1];
            for (int i = 0, j = 1; i < indices.Length; ++i, ++j)
            {
                _Indices[j] = indices[i];
            }
        }

        public ParameterExportAttribute(uint index)
        {
            _Indices = new uint[1];
            _Indices[0] = index;
        }

        public uint GetOffset(int index)
        {
            return _Indices[index];
        }

        uint[] _Indices;
    }
}