﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Rage.Move.Utils.Serialization
{
    public class XmlDeserializer : IDisposable
    {
        Hashtable _TypeDictionary = new Hashtable();
        Dictionary<string, Assembly> _AssemblyCache = new Dictionary<string, Assembly>();
        Dictionary<string, Assembly> _AssemblyRegister = new Dictionary<string, Assembly>();
        Dictionary<Type, TypeConverter> _TypeConverterCache = new Dictionary<Type, TypeConverter>();

        public XmlDeserializer()
        {
            Tags = new XmlSerializationTag();
            IgnoreCreationErrors = false;
        }

        public IXmlSerializationTag Tags { get; set; }

        public bool IgnoreCreationErrors { get; set; }

        public bool HasTypeDictionary
        {
            get { return (_TypeDictionary != null && 0 < _TypeDictionary.Count) ? true : false; }
        }

        public object Deserialize(string filename, Type type)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filename);

            return Deserialize(doc, type);
        }

        public object Deserialize(XmlDocument document, Type type)
        {
            XmlNode node = document.SelectSingleNode(type.Name);
            return Deserialize(node, type);
        }

        public object Deserialize(XmlNode node, Type type)
        {
            Reset();

            object obj = Activator.CreateInstance(type);

            if (TypeInfo.IsCollection(type))
            {
                Type[] arguments = type.GetGenericArguments();
                Type argument = null;
                if (arguments.Length != 0)
                    argument = arguments[0];

                foreach (XmlNode childnode in node.ChildNodes)
                {
                    Type infered = null;
                    if (childnode.Name != argument.Name)
                        infered = InferType(childnode.Name, argument);
                    else
                        infered = argument;

                    if (infered == null)
                        infered = FindTypeByName(childnode.Name);

                    object item = Activator.CreateInstance(infered);

                    MethodInfo method = type.GetMethod("Add");
                    method.Invoke(obj, new object[] { item });

                    foreach (PropertyInfo info in infered.GetProperties())
                    {
                        object[] attributes = info.GetCustomAttributes(typeof(XmlIgnoreAttribute), true);
                        if (attributes.Length == 0)
                            GetProperty(childnode, info, item);
                    }
                }
            }
            else
            {
                PropertyInfo[] properties = type.GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    object[] attributes = property.GetCustomAttributes(typeof(XmlIgnoreAttribute), true);
                    if (attributes.Length == 0)
                        GetProperty(node, property, obj);
                }
            }

            return obj;
        }

        void GetProperty(XmlNode node, PropertyInfo property, object obj)
        {
            Type type = property.PropertyType;
            string name = property.Name;

            object[] infer = property.GetCustomAttributes(typeof(XmlInferTypeAttribute), true);
            if (infer.Length != 0)
            {
                type = InferType(property);
                name = type.Name;
            }

            object[] elements = property.GetCustomAttributes(typeof(XmlElementAttribute), true);
            if (elements.Length != 0)
            {
                XmlElementAttribute element = (XmlElementAttribute)elements[0];
                type = element.Type;
            }

            object[] attribute = property.GetCustomAttributes(typeof(XmlAttributeAttribute), true);
            if (attribute.Length != 0)
            {
                XmlAttribute attrib = node.Attributes[name];
                if (attrib != null)
                {
                    object instance = TypeDescriptor.GetConverter(type).ConvertFrom(attrib.Value);
                    property.SetValue(obj, instance, null);
                }
            }
            else
            {
                XmlNode child = node.SelectSingleNode(name);
                if (child == null)
                    return;

                object[] convert = property.GetCustomAttributes(typeof(XmlTypeConverterAttribute), true);
                if (convert.Length != 0)
                {
                    XmlTypeConverterAttribute attrib = (XmlTypeConverterAttribute)convert[0];
                    TypeConverter converter = (TypeConverter)Activator.CreateInstance(attrib.Type);

                    if (converter.CanConvertFrom(typeof(string)))
                    {
                        object instance = converter.ConvertFrom(child.InnerText);
                        property.SetValue(obj, instance, null);
                    }
                }
                else
                if (TypeDescriptor.GetConverter(type).CanConvertFrom(typeof(string)))
                {
                    object instance = TypeDescriptor.GetConverter(type).ConvertFrom(child.InnerText);
                    property.SetValue(obj, instance, null);
                }
                else
                {
                    object instance = Activator.CreateInstance(type);
                    property.SetValue(obj, instance, null);

                    if (TypeInfo.IsCollection(type))
                    {
                        Type[] arguments = type.GetGenericArguments();
                        Type argument = null;
                        if (arguments.Length != 0)
                            argument = arguments[0];

                        foreach (XmlNode childnode in child.ChildNodes)
                        {
                            Type infered = null;
                            if (childnode.Name != argument.Name)
                                infered = InferType(childnode.Name, argument);
                            else
                                infered = argument;

                            object item = Activator.CreateInstance(infered);

                            MethodInfo method = type.GetMethod("Add");
                            method.Invoke(instance, new object[] { item });

                            foreach (PropertyInfo info in infered.GetProperties())
                            {
                                object[] attributes = info.GetCustomAttributes(typeof(XmlIgnoreAttribute), true);
                                if (attributes.Length == 0)
                                    GetProperty(childnode, info, item);
                            }
                        }
                    }
                    else
                    {
                        foreach (PropertyInfo info in type.GetProperties())
                        {
                            object[] attributes = info.GetCustomAttributes(typeof(XmlIgnoreAttribute), true);
                            if (attributes.Length == 0)
                                GetProperty(child, info, instance);
                        }
                    }
                }
            }
        }

        Type InferType(PropertyInfo property)
        {
            if (property.PropertyType.IsInterface)
            {
                foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    if (assembly.GetName().Name.Contains("Rage.Move"))
                    {
                        foreach (Type type in assembly.GetTypes())
                        {
                            Type iface = type.GetInterface(property.PropertyType.Name);
                            if (iface != null)
                                return type;
                        }
                    }
                }
            }

            return null;
        }

        Type InferType(string typename, Type iface)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assembly.GetName().Name.Contains("Rage.Move"))
                {
                    foreach (Type type in assembly.GetTypes())
                    {
                        Type i = type.GetInterface(iface.Name);
                        if (type.Name.Equals(typename) && i != null)
                            return type;
                    }
                }
            }

            return null;
        }

        Type FindTypeByName(string name)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assembly.GetName().Name.Contains("Rage.Move"))
                {
                    foreach (Type type in assembly.GetTypes())
                    {
                        if (type.Name.Equals(name))
                            return type;
                    }
                }
            }

            return null;
        }

        public void RegisterAssembly(Assembly assembly)
        {
            string name = assembly.FullName;

            int x = name.IndexOf(",");
            if (0 < x)
                name = name.Substring(0, x);

            _AssemblyRegister[name] = assembly;
        }

        public int RegisterAssemblies(List<Assembly> assemblies)
        {
            if (assemblies == null)
                return 0;

            int count = 0;
            foreach (Assembly assembly in assemblies)
            {
                RegisterAssembly(assembly);
                ++count;
            }

            return count;
        }

        public int RegisterAssemblies(Assembly[] assemblies)
        {
            if (assemblies == null)
                return 0;

            int count = 0;
            foreach (Assembly assembly in assemblies)
            {
                RegisterAssembly(assembly);
                ++count;
            }

            return count;
        }

        void AddAssemblyRegisterToCache()
        {
            if (_AssemblyRegister == null)
                return;

            Dictionary<string, Assembly>.Enumerator enumerator = _AssemblyRegister.GetEnumerator();
            while (enumerator.MoveNext())
                _AssemblyCache[enumerator.Current.Key] = enumerator.Current.Value;
        }

        public void Reset()
        {
            if (_TypeDictionary != null)
                _TypeDictionary.Clear();
        }

        public void Dispose()
        {
            Reset();

            if (_AssemblyCache != null)
                _AssemblyCache.Clear();

            if (_AssemblyRegister != null)
                _AssemblyRegister.Clear();

            if (_TypeConverterCache != null)
                _TypeConverterCache.Clear();
        }
    }
}
