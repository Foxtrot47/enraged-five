﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;

using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public abstract class AnchorBase : IConnectionAnchor, IAnchor
    {
        public AnchorBase(string name)
        {
            Name = name;
            Id = Guid.NewGuid();

            Connections = new NoResetObservableCollection<Connection>();
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        ILogic _Parent;
        public ILogic Parent 
        {
            get
            {
                return _Parent;
            }
            set
            {
                _Parent = value;
            }
        }

        public NoResetObservableCollection<Connection> Connections { get; protected set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            from.RemoveConnectionsFromAnchor(this);
        }

        object _Tag;
        public object Tag
        {
            get
            {
                return _Tag;
            }
            set
            {
                // annoying

                //if (value != null)
                //    Debug.Assert(_Tag == null);

                _Tag = value;
            }
        }
    }
}