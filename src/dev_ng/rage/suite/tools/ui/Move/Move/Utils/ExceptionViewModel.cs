﻿using System;

namespace MoVE.Utils
{
    public class ExceptionViewModel
    {
        public ExceptionViewModel(Exception e)
        {
            Message = e.Message;
            Source = e.Source;
            StackTrace = e.StackTrace;
        }

        private ExceptionViewModel()
        {
        }

        public string Message { get; set; }
        public string Source { get; set; }
        public string StackTrace { get; set; }

        public override string ToString()
        {
            return String.Format("Message: {0}\n Source: {1}\n Stack Trace:\n{2}", Message, Source, StackTrace);
        }
    }
}