﻿namespace New.Move.Core
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Runtime.Serialization;
    using System.Xml;

    /// <summary>
    /// Represents a signal that is a reference to a property in a
    /// <see cref="New.Move.Core.Reference"/> object.
    /// </summary>
    public class ReferenceSignal : Signal
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the
        /// <see cref="AssocProperty"/> property.
        /// </summary>
        protected const string AssocPropertySerializationTag = "AssocProperty";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Reference"/> property.
        /// </summary>
        protected const string ReferenceSerializationTag = "Reference";

        /// <summary>
        /// The private field used for the <see cref="Reference"/> property.
        /// </summary>
        private Guid reference;

        /// <summary>
        /// The private field used for the <see cref="AssocProperty"/> property.
        /// </summary>
        private Property associatedProperty;

        /// <summary>
        /// The private field used for the <see cref="HasInputAnchor"/> property.
        /// </summary>
        private bool hasInputAnchor;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.ReferenceSignal"/> class.
        /// </summary>
        /// <param name="reference">
        /// The identifier to the object referenced.
        /// </param>
        /// <param name="name">
        /// The name of the signal.
        /// </param>
        public ReferenceSignal(Guid reference, string name)
            : base(name)
        {
            this.reference = reference;
            this.AssocProperty = null;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ReferenceSignal"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        public ReferenceSignal(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ReferenceSignal"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        protected ReferenceSignal(SerializationInfo info, StreamingContext context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case Signal.NameSerializationTag:
                        this.Name = (string)entry.Value;
                        break;
                    case Signal.IdSerializationTag:
                        this.Id = (Guid)entry.Value;
                        break;
                    case Signal.ParentSerializationTag:
                        this.Parent = (ILogic)entry.Value;
                        break;
                    case Signal.EnabledSerializationTag:
                        this.Enabled = (bool)entry.Value;
                        break;
                    case AssocPropertySerializationTag:
                        this.AssocProperty = (Property)entry.Value;
                        break;
                    case ReferenceSerializationTag:
                        this.Reference = (Guid)entry.Value;
                        break;
                    default:
                        Trace.TraceWarning(StringTable.UnrecognisedBinaryData);
                        break;
                }
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier to the signal this is referencing.
        /// </summary>
        public Guid Reference
        {
            get { return this.reference; }
            protected set { this.reference = value; }
        }

        /// <summary>
        /// Gets the <see cref="New.Move.Core.IDesc"/> object that describes the creation of
        /// this object.
        /// </summary>
        public override IDesc Desc
        {
            get { return null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this signal has a input anchor.
        /// </summary>
        public bool HasInputAnchor
        {
            get { return this.hasInputAnchor; }
            protected set { this.hasInputAnchor = value; }
        }

        /// <summary>
        /// Gets or sets the assoicated property for this reference signal.
        /// </summary>
        public Property AssocProperty
        {
            get { return this.associatedProperty; }
            set { this.associatedProperty = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the parameter value for this signal that represents the type it is
        /// representing.
        /// </summary>
        /// <returns>
        /// The parameter value that this signal represents.
        /// </returns>
        public override Parameter GetParameter()
        {
            return Parameter.Reference;
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("ref");

            writer.WriteAttributeString(
                Signal.NameSerializationTag, this.Name);
            writer.WriteAttributeString(
                Signal.EnabledSerializationTag,
                this.Enabled.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                Signal.IdSerializationTag,
                this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                ReferenceSerializationTag,
                this.Reference.ToString("D", CultureInfo.InvariantCulture));

            IProperty property = this.AssocProperty as IProperty;
            if (property != null)
            {
                writer.WriteAttributeString(
                    AssocPropertySerializationTag,
                    property.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            if (this.Parent != null)
            {
                writer.WriteAttributeString(
                    Signal.ParentSerializationTag,
                    this.Parent.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue(ReferenceSerializationTag, this.Reference);
            info.AddValue(AssocPropertySerializationTag, this.AssocProperty);
        }

        /// <summary>
        /// Sets the parent value for this signal.
        /// </summary>
        /// <param name="parent">
        /// The reference that is the parent of this reference signal.
        /// </param>
        internal void SetParent(Reference parent)
        {
            this.Parent = parent;
        }
        #endregion
    } // New.Move.Core.ReferenceSignal {Class}
} // New.Move.Core {Namespace}
