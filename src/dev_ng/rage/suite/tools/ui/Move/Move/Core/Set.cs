﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace New.Move.Core
{
    /// <summary>
    /// Generic collection that represents a set of unique values.  Behaves in the same way as std::set<T>
    /// </summary>
    public class Set<T> : ICollection<T>
    {
        #region ICollection<T> Members

        public void Add(T item)
        {
            if (!Contains(item))
            {
                items_.Add(item);
            }
        }

        public void Clear()
        {
            items_.Clear();
        }

        public bool Contains(T item)
        {
            return ContainsImpl(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            items_.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return items_.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            return items_.Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return items_.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return items_.GetEnumerator();
        }

        #endregion

        protected virtual bool ContainsImpl(T item)
        {
            return items_.Contains(item);
        }

        protected List<T> items_ = new List<T>();
    }
}
