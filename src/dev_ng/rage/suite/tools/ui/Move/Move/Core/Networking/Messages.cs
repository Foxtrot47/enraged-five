﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using Rage.Move.Utils;

namespace Rage.Move.Core
{
    public enum NetworkMessageId
    {
        // NOTE: Keep this in sync with MessageFormats.txt!!!
        MSG_UNKNOWN = 0,
        MSG_SYSINFO = 1,
        MSG_REQUEST_NETWORK_INSTANCES = 2,
        MSG_NETWORK_INSTANCES = 3,
        MSG_REQUEST_DISCONNECT = 4,
        MSG_DISCONNECT = 5,

        LINK_MSG_HANDLER_START = 100,
        MSG_REQUEST_START_DEBUGGING = 101,
        MSG_START_DEBUGGING = 102,
        MSG_REQUEST_STOP_DEBUGGING = 103,
        MSG_STOP_DEBUGGING = 104,
        MSG_START_TRANSITION = 105,
        MSG_STOP_TRANSITION = 106,
        MSG_START_STATE_SET = 107,
        MSG_STOP_STATE_SET = 108,
        MSG_UPDATE_ROOT = 109,
        MSG_SIGNAL_SET = 110,
        // NOTE: Keep this in sync with MessageFormats.txt!!!
    }

    public abstract class NetworkMessageBase
    {
        public const int BufferOffset_Size = 0;
        public const int BufferOffset_MessageType = 4;
        public const int BufferOffset_ResponderId = 8;
        public const int BufferOffset_NetworkInstanceId = 12;

        public delegate void RequestResponse(NetworkMessageBase msg);

        protected NetworkMessageBase()
        {
            MessageType = NetworkMessageId.MSG_UNKNOWN;
            ResponderId = 0;
        }

        public virtual void Decode(ByteBuffer bb)
        {
            bb.ReadPosition = 0;
            _size = bb.ReadUInt32();
            MessageType = (NetworkMessageId)bb.ReadUInt32();
            ResponderId = bb.ReadInt32();
        }

        public override string ToString()
        {
            return String.Format("Size [{0}] Type[{1}] ResponderId[{2}]", _size, MessageType.ToString(), ResponderId);
        }

        public virtual string ToPayloadString()
        {
            return "";
        }

        public string PayloadString
        {
            get { return ToPayloadString(); }
        }

        private uint _size;

        public uint Size { get { return _size; } }
        public NetworkMessageId MessageType { get; protected set; }
        public int ResponderId { get; protected set; }
    }

    public class SystemInfoNetworkMessage : NetworkMessageBase
    {
        public SystemInfoNetworkMessage() { }
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);
            Version = bb.ReadUInt32();
        }

        public override string ToString()
        {
            return String.Format("{0} Version[{1}]", base.ToString(), Version);
        }

        public uint Version { get; protected set; }
    }

    public class NetworkInstanceData
    {
        public NetworkInstanceData() { }

        public override string ToString()
        {
            return String.Format("Id[{0}] Guid[{1}] Version[{2}] Crc[{3}] Filename[{4}]", NetworkInstanceId, NetworkGuid, Version, Crc, Filename);
        }

        public virtual string ToPayloadString()
        {
            return ToString();
        }

        public uint NetworkInstanceId { get; set; }
        public Guid NetworkGuid { get; set; }
        public int Version { get; set; }
        public int Crc { get; set; }
        public string Filename { get; set; }
        public string Description { get; set; }
    }

    public class DisconnectMessage : NetworkMessageBase
    {
        public DisconnectMessage()
        { }

        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);

            Acknowledged = bb.ReadBool();
        }

        public static ByteBuffer EncodeRequest(int responseId)
        {
            ByteBuffer bb = new ByteBuffer(1000);
            bb.BeginTcpBuffer();
            bb.WriteUInt32((uint)NetworkMessageId.MSG_REQUEST_DISCONNECT);
            bb.WriteInt32(responseId);
            bb.EndTcpBuffer();
            return bb;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override string ToPayloadString()
        {
            return base.ToPayloadString();
        }

        public bool Acknowledged { get; private set; }
    }

    public class NetworkInstancesMessage : NetworkMessageBase
    {
        public NetworkInstancesMessage()
        {
        }

        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);

            _instances.Clear();

            uint count = bb.ReadUInt32();
            for (uint i = 0; i < count; ++i)
            {
                NetworkInstanceData nid = new NetworkInstanceData();

                nid.NetworkInstanceId = bb.ReadUInt32();
                nid.NetworkGuid = bb.ReadGuid();
                nid.Version = bb.ReadInt32();
                nid.Crc = bb.ReadInt32();
                nid.Filename = bb.ReadString();
                nid.Description = bb.ReadString();

                _instances.Add(nid);
            }
        }

        public static ByteBuffer EncodeRequest(int responseId)
        {
            ByteBuffer bb = new ByteBuffer(1000);
            bb.BeginTcpBuffer();
            bb.WriteUInt32((uint)NetworkMessageId.MSG_REQUEST_NETWORK_INSTANCES);
            bb.WriteInt32(responseId);
            bb.EndTcpBuffer();
            return bb;
        }

        public override string ToString()
        {
            return base.ToString() + "\n" + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (NetworkInstanceData nid in _instances)
            {
                sb.Append(String.Format("{0}\n", nid.ToString()));
            }
            return sb.ToString();
        }

        public ReadOnlyCollection<NetworkInstanceData> Instances
        {
            get { return _instances.AsReadOnly(); }
        }

        List<NetworkInstanceData> _instances = new List<NetworkInstanceData>();
    }

    public abstract class LinkNetworkMessageBase : NetworkMessageBase
    {
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);
            NetworkInstanceId = bb.ReadUInt32();
            UpdatePass = bb.ReadUInt32();
        }

        public override string ToString()
        {
            return String.Format("{0} NetworkId[{1}] (@{2}s)", base.ToString(), NetworkInstanceId, UpdatePass);
        }

        public uint NetworkInstanceId { get; protected set; }
        public uint UpdatePass {get; protected set; }
    }

    public class StartDebuggingNetworkMessage : LinkNetworkMessageBase
    {
        public StartDebuggingNetworkMessage()
        {
            Confirmation = false;
        }
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);
            Confirmation = bb.ReadBool();
            StatebitWords = bb.ReadUInt16();
            NumFlags = bb.ReadUInt16();
            FlagOffset = bb.ReadUInt16();
            NumRequests = bb.ReadUInt16();
            RequestOffset = bb.ReadUInt16();
            InitialTime = bb.ReadFloat();
        }

        public override string ToString()
        {
            return base.ToString() + " " + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            return String.Format("Confirmation[{0}]", Confirmation);
        }

        public static ByteBuffer EncodeRequest(int responseId, uint networkInstance)
        {
            ByteBuffer bb = new ByteBuffer(1000);
            bb.BeginTcpBuffer();
            bb.WriteUInt32((uint)NetworkMessageId.MSG_REQUEST_START_DEBUGGING);
            bb.WriteInt32(responseId);
            bb.WriteUInt32(networkInstance);
            bb.EndTcpBuffer();
            return bb;
        }
        public bool Confirmation { get; protected set; }

        public ushort StatebitWords { get; protected set; }
        public ushort NumFlags { get; protected set; }
        public ushort FlagOffset { get; protected set; }
        public ushort NumRequests { get; protected set; }
        public ushort RequestOffset { get; protected set; }
        public float InitialTime { get; protected set; }
    }

    public class StopDebuggingNetworkMessage : LinkNetworkMessageBase
    {
        public StopDebuggingNetworkMessage()
        {
            Confirmation = false;
        }
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);
            Confirmation = bb.ReadBool();
        }

        public static ByteBuffer EncodeRequest(int responseId, Guid networkId)
        {
            ByteBuffer bb = new ByteBuffer(1000);
            bb.BeginTcpBuffer();
            bb.WriteUInt32((uint)NetworkMessageId.MSG_REQUEST_STOP_DEBUGGING);
            bb.WriteInt32(responseId);
            bb.WriteGuid(networkId);
            bb.EndTcpBuffer();
            return bb;
        }

        public override string ToString()
        {
            return base.ToString() + " " + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            return String.Format("Confirmation[{0}]", Confirmation);
        }


        public bool Confirmation { get; protected set; }
    }

    public struct RuntimeStateId
    {
        public static RuntimeStateId ReadFromByteBuffer(ByteBuffer bb)
        {
            RuntimeStateId id = new RuntimeStateId();
            id.RawId = bb.ReadUInt16();
            return id;
        }

        public static bool operator == (RuntimeStateId a, RuntimeStateId b)
        {
            return a.RawId == b.RawId;
        }
        public static bool operator != (RuntimeStateId a, RuntimeStateId b)
        {
            return a.RawId != b.RawId;
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return (RawId << 16) | RawId;
        }

        public ushort RawId { get; private set; }

        private RuntimeStateId(ushort rawId) : this() {
            RawId = rawId;
        }

        public override string ToString()
        {
            return String.Format("Node {0}", RawId);
        }

        public static RuntimeStateId RootState = new RuntimeStateId(0);
    }

    public struct RuntimeStateInstance
    {
        public RuntimeStateId State;
        public uint ParentMachineInstance;

        public static RuntimeStateInstance ReadFromByteBuffer(ByteBuffer bb)
        {
            RuntimeStateInstance sta = new RuntimeStateInstance();
            sta.State = RuntimeStateId.ReadFromByteBuffer(bb);
            sta.ParentMachineInstance = bb.ReadUInt32();
            return sta;
        }

        public static bool operator ==(RuntimeStateInstance a, RuntimeStateInstance b)
        {
            return a.State == b.State && a.ParentMachineInstance == b.ParentMachineInstance;
        }
        public static bool operator !=(RuntimeStateInstance a, RuntimeStateInstance b)
        {
            return a.State != b.State || a.ParentMachineInstance != b.ParentMachineInstance;
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return (int)(ParentMachineInstance ^ (uint)State.RawId);
        }

        public override string ToString()
        {
            return State.ToString();
        }
    }

    public struct RuntimeStateInstanceAndParent
    {
        public RuntimeStateId State;
        public RuntimeStateId Parent;
        public uint ParentMachineInstance;

        public static RuntimeStateInstanceAndParent ReadFromByteBuffer(ByteBuffer bb)
        {
            RuntimeStateInstanceAndParent sta = new RuntimeStateInstanceAndParent();
            sta.State = RuntimeStateId.ReadFromByteBuffer(bb);
            sta.Parent = RuntimeStateId.ReadFromByteBuffer(bb);
            sta.ParentMachineInstance = bb.ReadUInt32();
            return sta;
        }

        public RuntimeStateInstance JustInstance()
        {
            return new RuntimeStateInstance() {State = this.State, ParentMachineInstance = this.ParentMachineInstance};
        }

        public override string ToString()
        {
            return State.ToString();
        }
    }

    static class ByteBufferHelpers
    {
        public static void ReadFromByteBuffer(this List<RuntimeStateInstance> list, ByteBuffer bb)
        {
            list.Clear();
            byte count = bb.ReadUInt8();
            for(byte b = 0; b < count; b++)
            {
                RuntimeStateInstance inst = RuntimeStateInstance.ReadFromByteBuffer(bb);
                list.Add(inst);
            }
        }

        public static void ReadFromByteBuffer(this List<RuntimeStateInstanceAndParent> list, ByteBuffer bb)
        {
            list.Clear();
            byte count = bb.ReadUInt8();
            for(byte b = 0; b < count; b++)
            {
                RuntimeStateInstanceAndParent s = RuntimeStateInstanceAndParent.ReadFromByteBuffer(bb);
                list.Add(s);
            }
        }
    }

    public class StartTransitionMessage : LinkNetworkMessageBase
    {
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);

            FromId = RuntimeStateId.ReadFromByteBuffer(bb);
            ToId = RuntimeStateId.ReadFromByteBuffer(bb);
            ParentMachineInstance = bb.ReadUInt32();

            Via = bb.ReadUInt16();

            List<RuntimeStateInstanceAndParent> states = new List<RuntimeStateInstanceAndParent>();
            states.ReadFromByteBuffer(bb);
            Descendants = states;
        }

        public override string ToString()
        {
            return base.ToString() + " " + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Started From[{0}] To[{1}] Via[{2}]", From, To, Via);
            if (Descendants != null && Descendants.Count > 0)
            {
                sb.Append(" Kids: ");
                sb.AppendCollection(Descendants.Select((x) => (x.State)), ", ");
            }
            return sb.ToString();
        }

        public RuntimeStateInstance From { get { return new RuntimeStateInstance() { State = FromId, ParentMachineInstance = ParentMachineInstance }; } }
        public RuntimeStateInstance To { get { return new RuntimeStateInstance() { State = ToId, ParentMachineInstance = ParentMachineInstance }; } }

        protected RuntimeStateId FromId;
        protected RuntimeStateId ToId;
        public uint ParentMachineInstance { get; protected set; }
        public ushort Via { get; protected set; }

        public List<RuntimeStateInstanceAndParent> Descendants { get; protected set; }
    }

    public class StopTransitionMessage : LinkNetworkMessageBase
    {
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);

            FromId = RuntimeStateId.ReadFromByteBuffer(bb);
            ToId = RuntimeStateId.ReadFromByteBuffer(bb);
            ParentMachineInstance = bb.ReadUInt32();
            Via = bb.ReadUInt16();

            List<RuntimeStateInstance> states = new List<RuntimeStateInstance>();
            states.ReadFromByteBuffer(bb);
            Descendants = states;
        }

        public override string ToString()
        {
            return base.ToString() + " " + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Stopped From[{0}] To[{1}] Via[{2}]", From, To, Via);
            if (Descendants != null && Descendants.Count > 0)
            {
                sb.Append(" Kids: ");
                sb.AppendCollection(Descendants, ", ");
            }
            return sb.ToString();
        }


        public RuntimeStateInstance From { get { return new RuntimeStateInstance() { State = FromId, ParentMachineInstance = ParentMachineInstance }; } }
        public RuntimeStateInstance To { get { return new RuntimeStateInstance() { State = ToId, ParentMachineInstance = ParentMachineInstance }; } }

        protected RuntimeStateId FromId;
        protected RuntimeStateId ToId;
        public uint ParentMachineInstance { get; protected set; }
        public ushort Via { get; protected set; }

        public List<RuntimeStateInstance> Descendants { get; protected set; }
    }
    
    public class StartStateSetMessage : LinkNetworkMessageBase
    {
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);

            List<RuntimeStateInstanceAndParent> states = new List<RuntimeStateInstanceAndParent>();
            states.ReadFromByteBuffer(bb);
            States = states;
        }

        public override string ToString()
        {
            return base.ToString() + " " + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Started: ");
            sb.AppendCollection(States.Select((x) => x.State), ", ");
            return sb.ToString();
        }

        public List<RuntimeStateInstanceAndParent> States;
    }

    public class StopStateSetMessage : LinkNetworkMessageBase
    {
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);

            List<RuntimeStateInstance> states = new List<RuntimeStateInstance>();
            states.ReadFromByteBuffer(bb);
            States = states;
        }

        public override string ToString()
        {
            return base.ToString() + " " + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Stopped: ");
            sb.AppendCollection(States, ", ");
            return sb.ToString();
        }

        public List<RuntimeStateInstance> States;
    }

    public static class StringHelpers
    {
        public static StringBuilder AppendCollection<T>(this StringBuilder sb, IEnumerable<T> items, string seperator)
        {
            bool first = true;
            foreach(var item in items)
            {
                if (!first)
                {
                    sb.Append(seperator);
                }
                first = false;
                sb.Append(item);
            }
            return sb;
        }

    }

    public class UpdateRootMessage : LinkNetworkMessageBase
    {
        public override void Decode(ByteBuffer bb)
        {
            base.Decode(bb);

            Dt = bb.ReadFloat();

            ushort numStatebitWords = bb.ReadUInt16();
            StatebitWords = new UInt32[numStatebitWords];
            for (int i = 0; i < numStatebitWords; i++)
            {
                StatebitWords[i] = bb.ReadUInt32();
            }
        }

        public override string ToString()
        {
            return base.ToString() + " " + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("dt= ");
            sb.Append(Dt);
            sb.Append(", Statebits: ");
            sb.AppendCollection(StatebitWords.Select((x) => String.Format("0x{0:X8}", x)), ", ");
            return sb.ToString();
        }

        public float Dt;
        public UInt32[] StatebitWords;

    }

    public class SignalSetMessage : LinkNetworkMessageBase
    {
        public override void Decode(ByteBuffer bb)
        {
            throw new NotImplementedException("Can't Decode with just a ByteBuffer - use DecodeSignalSet instead");
        }

        public void DecodeSignalSet(ByteBuffer bb, New.Move.Core.Link link)
        {
            base.Decode(bb);

            ushort numSignals = bb.ReadUInt16();

            if (numSignals > 0) 
            {
                Signals = new SignalData[numSignals];
            }
            for(int i = 0; i < numSignals; i++)
            {
                var sig = Signals[i] = new SignalData();
                sig.Id = bb.ReadUInt32();
                sig.Writer = bb.ReadUInt16();
                sig.Parent = RuntimeStateInstance.ReadFromByteBuffer(bb);

                byte rawTypeId = bb.ReadUInt8();
                int type = rawTypeId >> 3;
                int flags = rawTypeId & 0x7;
                switch(type)
                {
                    case 0:
                        sig.Type = New.Move.Core.Parameter.None;
                        sig.Value.Bool = false;
                        break;
                    case 1:
                        sig.Type = New.Move.Core.Parameter.Real;
                        sig.Value.Float = bb.ReadFloat();
                        break;
                    case 2:
                        sig.Type = New.Move.Core.Parameter.Boolean;
                        sig.Value.Bool = ((flags & 0x1) != 0);
                        break;
                    case 3:
                        sig.Type = New.Move.Core.Parameter.Animation;
                        sig.String = ((flags & 0x1) != 0) ? link.LastAnimName : bb.ReadString();
                        link.LastAnimName = sig.String;
                        break;
                    case 4:
                        sig.Type = New.Move.Core.Parameter.Clip;
                        sig.String = ((flags & 0x1) != 0) ? link.LastClipName : bb.ReadString();
                        link.LastClipName = sig.String;
                        break;
                    case 5:
                        sig.Type = New.Move.Core.Parameter.Expressions;
                        sig.String = ((flags & 0x1) != 0) ? link.LastExprName : bb.ReadString();
                        link.LastExprName = sig.String;
                        break;
                    case 6:
                        sig.Type = New.Move.Core.Parameter.Filter;
                        sig.String = ((flags & 0x1) != 0) ? link.LastFilter : bb.ReadString();
                        link.LastFilter = sig.String;
                        break;
                    case 7:
                        sig.Type = New.Move.Core.Parameter.Frame;
                        sig.String = String.Empty;
                        break;
                    case 8:
                        sig.Type = New.Move.Core.Parameter.ParameterizedMotion;
                        sig.String = ((flags & 0x1) != 0) ? link.LastPMName : bb.ReadString();
                        link.LastPMName = sig.String;
                        break;
                    case 9:
                        sig.Type = New.Move.Core.Parameter.Node;
                        sig.Value.Node = RuntimeStateId.ReadFromByteBuffer(bb);
                        break;
                }
            }
        }

        public override string ToString()
        {
            return base.ToString() + " " + ToPayloadString();
        }

        public override string ToPayloadString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(" {0} Signals: ", Signals.Length);
            sb.AppendCollection(Signals.Select((x) => {
                Type dt = x.DataType;
                if (dt == typeof(float))
                {
                    return String.Format("[Signal {0}: {1}, Parent: {2}]", x.Id, x.Value.Float, x.Parent);
                }
                else if (dt == typeof(bool))
                {
                    return String.Format("[Signal {0}: {1}, Parent: {2}]", x.Id, x.Value.Bool, x.Parent);
                }
                else if (dt == typeof(RuntimeStateId))
                {
                    return String.Format("[Signal {0}: {1}, Parent: {2}]", x.Id, x.Value.Node, x.Parent);
                }
                else if (dt == typeof(string))
                {
                    return String.Format("[Signal {0}: {1}, Parent: {2}]", x.Id, x.String, x.Parent);
                }
                else
                {
                    return String.Format("[Signal {0}: ?, Parent: {2}]", x.Id, x.Parent);
                }
            }), ", ");
            return sb.ToString();
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct SignalValue
        {
            [FieldOffset(0)]
            public float Float;
            [FieldOffset(0)]
            public bool Bool;
            [FieldOffset(0)]
            public RuntimeStateId Node;
        }

        public class SignalData // could be a struct?
        {
            public New.Move.Core.Parameter Type;    // The type of signal
            public uint Id;                       // the ID of the signal
            public RuntimeStateInstance Parent;           // which state machine was responsible for modifying this signal (if any)
            public ushort Writer;                   // which node actually wrote the value
            public SignalValue Value;
            public string String;                   // string can't be in the union with the other values - doubt its worth making a subclass...

            public Type DataType
            {
                get {
                    switch(Type) {
                        case New.Move.Core.Parameter.None:
                        case New.Move.Core.Parameter.Frame: // No type for frame data (yet)
                            return null;
                        case New.Move.Core.Parameter.Real:
                            return typeof(Single);
                        case New.Move.Core.Parameter.Boolean:
                            return typeof(Boolean);
                        case New.Move.Core.Parameter.Animation:
                        case New.Move.Core.Parameter.Clip:
                        case New.Move.Core.Parameter.Expressions:
                        case New.Move.Core.Parameter.Filter:
                        case New.Move.Core.Parameter.ParameterizedMotion:
                            return typeof(String);
                        case New.Move.Core.Parameter.Node:
                            return typeof(RuntimeStateId);
                    }
                    return null;
                }
            }
        }

        public SignalData[] Signals;
    }
}
