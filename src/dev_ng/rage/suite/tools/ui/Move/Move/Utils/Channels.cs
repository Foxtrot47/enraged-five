﻿using System;
using System.Collections.Generic;

namespace Rage.Move.Utils
{
    public class ChannelSelectionChangedEventArgs
    {
        public ChannelSelectionChangedEventArgs(IChannelable selected, IChannelable old, ChannelIndex index)
        {
            Selected = selected;
            Old = old;
            Index = index;
        }

        public IChannelable Selected { get; private set; }
        public IChannelable Old { get; private set; }
        public ChannelIndex Index { get; private set; }
    }

    public delegate void ChannelSelectionChangedEventHandler(ChannelSelectionChangedEventArgs e);

    public interface IChannelable
    {
        ChannelIndex Channel { get; set; }
        Guid Id { get; }
    }

    public enum ChannelIndex
    {
        ChannelNone,
        Channel1,
        Channel2,
        Channel3,
        Channel4,
        Channel5,
        Channel6,
        Channel7,
    }

    public class ChannelSelection
    {
        public IChannelable Current { get; set; }
        ChannelIndex Index { get; set; }

        public ChannelSelection(ChannelIndex index)
        {
            Index = index;
        }

        public void Select(IChannelable selected)
        {
            if (Current != null)
                Current.Channel = ChannelIndex.ChannelNone;

            Current = selected;

            if (Current != null)
                Current.Channel = Index;
        }

        public void Deselect()
        {
            if (Current != null)
                Current.Channel = ChannelIndex.ChannelNone;

            Current = null;
        }

        public bool Equals(Guid id)
        {
            if (Current != null)
                return Current.Id == id;

            return false;
        }
    }

    public class Channels
    {
        ChannelSelection _Channel1 = new ChannelSelection(ChannelIndex.Channel1);
        public ChannelSelection Channel1 { get { return _Channel1; } }
        ChannelSelection _Channel2 = new ChannelSelection(ChannelIndex.Channel2);
        public ChannelSelection Channel2 { get { return _Channel2; } }
        ChannelSelection _Channel3 = new ChannelSelection(ChannelIndex.Channel3);
        public ChannelSelection Channel3 { get { return _Channel3; } }
        ChannelSelection _Channel4 = new ChannelSelection(ChannelIndex.Channel4);
        public ChannelSelection Channel4 { get { return _Channel4; } }
        ChannelSelection _Channel5 = new ChannelSelection(ChannelIndex.Channel5);
        public ChannelSelection Channel5 { get { return _Channel5; } }
        ChannelSelection _Channel6 = new ChannelSelection(ChannelIndex.Channel6);
        public ChannelSelection Channel6 { get { return _Channel6; } }
        ChannelSelection _Channel7 = new ChannelSelection(ChannelIndex.Channel7);
        public ChannelSelection Channel7 { get { return _Channel7; } }

        public Channels()
        {
            Instance = this;
        }

        public void Select(IChannelable selected, ChannelIndex channel)
        {
            switch (channel)
            {
                case ChannelIndex.Channel1:
                    {
                        IChannelable old = _Channel1.Current;
                        _Channel1.Select(selected);
                        OnChannelSelectionChanged(selected, old, channel);
                    }
                    break;
                case ChannelIndex.Channel2:
                    {
                        IChannelable old = _Channel2.Current;
                        _Channel2.Select(selected);
                        OnChannelSelectionChanged(selected, old, channel);
                    }
                    break;
                case ChannelIndex.Channel3:
                    {
                        IChannelable old = _Channel3.Current;
                        _Channel3.Select(selected);
                        OnChannelSelectionChanged(selected, old, channel);
                    }
                    break;
                case ChannelIndex.Channel4:
                    {
                        IChannelable old = _Channel4.Current;
                        _Channel4.Select(selected);
                        OnChannelSelectionChanged(selected, old, channel);
                    }
                    break;
                case ChannelIndex.Channel5:
                    {
                        IChannelable old = _Channel5.Current;
                        _Channel5.Select(selected);
                        OnChannelSelectionChanged(selected, old, channel);
                    }
                    break;
                case ChannelIndex.Channel6:
                    {
                        IChannelable old = _Channel6.Current;
                        _Channel6.Select(selected);
                        OnChannelSelectionChanged(selected, old, channel);
                    }
                    break;
                case ChannelIndex.Channel7:
                    {
                        IChannelable old = _Channel7.Current;
                        _Channel7.Select(selected);
                        OnChannelSelectionChanged(selected, old, channel);
                    }
                    break;
            }
        }

        public void Deselect(ChannelIndex channel)
        {
            switch (channel)
            {
                case ChannelIndex.Channel1:
                    {
                        IChannelable old = _Channel1.Current;
                        _Channel1.Deselect();
                        OnChannelSelectionChanged(null, old, channel);
                    }
                    break;
                case ChannelIndex.Channel2:
                    {
                        IChannelable old = _Channel2.Current;
                        _Channel2.Deselect();
                        OnChannelSelectionChanged(null, old, channel);
                    }
                    break;
                case ChannelIndex.Channel3:
                    {
                        IChannelable old = _Channel3.Current;
                        _Channel3.Deselect();
                        OnChannelSelectionChanged(null, old, channel);
                    }
                    break;
                case ChannelIndex.Channel4:
                    {
                        IChannelable old = _Channel4.Current;
                        _Channel4.Deselect();
                        OnChannelSelectionChanged(null, old, channel);
                    }
                    break;
                case ChannelIndex.Channel5:
                    {
                        IChannelable old = _Channel5.Current;
                        _Channel5.Deselect();
                        OnChannelSelectionChanged(null, old, channel);
                    }
                    break;
                case ChannelIndex.Channel6:
                    {
                        IChannelable old = _Channel6.Current;
                        _Channel6.Deselect();
                        OnChannelSelectionChanged(null, old, channel);
                    }
                    break;
                case ChannelIndex.Channel7:
                    {
                        IChannelable old = _Channel7.Current;
                        _Channel7.Deselect();
                        OnChannelSelectionChanged(null, old, channel);
                    }
                    break;
            }
        }

        public void Clear()
        {
            Deselect(ChannelIndex.Channel1);
            Deselect(ChannelIndex.Channel2);
            Deselect(ChannelIndex.Channel3);
            Deselect(ChannelIndex.Channel4);
            Deselect(ChannelIndex.Channel5);
            Deselect(ChannelIndex.Channel6);
            Deselect(ChannelIndex.Channel7);
        }

        public event ChannelSelectionChangedEventHandler ChannelSelectionChanged;
        void OnChannelSelectionChanged(IChannelable selected, IChannelable old, ChannelIndex index)
        {
            if (ChannelSelectionChanged != null)
                ChannelSelectionChanged(new ChannelSelectionChangedEventArgs(selected, old, index));
        }

        static public Channels Instance { get; private set; }
    }
}

