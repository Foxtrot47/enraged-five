﻿namespace New.Move.Core
{
    public enum SyncEvent
    {
        Event = 128,
        Unused,
        AmbientFlags,
        Audio,
        GestureSpeaker,
        GestureListener,
        FacialSpeaker,
        FacialListener
    }

    public enum EventId
    {
        AEF_LOOPSTART_1 = (1 << 0),
        AEF_LOOPSTOP_1 = (1 << 1),
        AEF_LOOPSTART_2 = (1 << 2),
        AEF_LOOPSTOP_2 = (1 << 3),
        AEF_CRITICAL_FRAME = (1 << 4),
        AEF_FOOT_HEEL_L = (1 << 5),
        AEF_FOOT_HEEL_R = (1 << 6),
        AEF_FOOT_TOE_L = (1 << 7),
        AEF_FOOT_TOE_R = (1 << 8),
        AEF_FIRE_1 = (1 << 9),
        AEF_FIRE_2 = (1 << 10),
        AEF_CREATE_OBJECT = (1 << 11),
        AEF_DESTROY_OBJECT = (1 << 12),
		AEF_RELEASE_OBJECT = (1 << 13),
		AEF_INTERRUPTIBLE = (1 << 14),
		AEF_SHELL_EJECTED = (1 << 15),
		AEF_DOOR_START = (1 << 16),
		AEF_DOOR_END = (1 << 17),
		AEF_SMASH = (1 << 18),
		AEF_CAN_SWITCH_TO_NM = (1 << 19),
		AEF_STOP_HOMING = (1 << 20),
		AEF_LEFT_HAND_IK_OFF = (1 << 21),
		AEF_PRIMARY_LOOKAT = (1 << 22),	
		AEF_SMOKE_EXHALE = (1 << 23),
		AEF_RIGHT_HAND_IK_OFF = (1 << 24),		
		AEF_SECONDARY_LOOKAT = (1 << 25),
		AEF_LEFT_HAND_IK_ON = (1 << 26),
		AEF_RIGHT_HAND_IK_ON = (1 << 27),
		AEF_OBJECT_VFX_REGISTER = (1 << 28),
		AEF_OBJECT_VFX_TRIGGER = (1 << 29),
		AEF_WEAPON_IN_LEFT_HAND_START = (1 << 30),
        AEF_WEAPON_IN_LEFT_HAND_STOP = (1 << 31),		
    }
}