﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Xml;
using System.Globalization;

namespace Rockstar.MoVE.Framework
{
    [Serializable]
    public class Curve : INotifyPropertyChanged, ISerializable
    {
        public Curve(string name)
        {
            _Color = Color.FromRgb(0, 255, 255);
            Name = name;

            Points = new CurveSegment[2];
            Points[0] = new CurveSegment(new Point(0, 0)) { Curve = this, Color = Color };
            Points[1] = new CurveSegment(new Point(1, 0)) { Curve = this, Color = Color };
        }

        public Curve(Curve other)
        {
            _Color = other.Color;
            Name = other.Name;

            foreach (CurveSegment osegment in other.Points)
            {
                CurveSegment segment = new CurveSegment(new Point(osegment.X, osegment.Y)) { Visible = osegment.Visible, Color = osegment.Color };
            }

            Points = new CurveSegment[2];
            Points[0] = new CurveSegment(new Point(0, 0)) { Curve = this, Color = Color };
            Points[1] = new CurveSegment(new Point(1, 0)) { Curve = this, Color = Color };
        }

        public string Name { get; set; }
        Color _Color;
        public Color Color
        {
            get
            {
                return _Color;
            }
            set
            {
                if (_Color != value)
                {
                    _Color = value;
                    foreach (CurveSegment point in Points)
                    {
                        point.Color = value;
                    }
                    NotifyPropertyChanged("Color");
                }
            }
        }

        bool _Visible = true;
        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                if (_Visible != value)
                {
                    _Visible = value;
                    foreach (CurveSegment point in Points)
                    {
                        point.Visible = value;
                    }
                    NotifyPropertyChanged("Visible");
                }
            }
        }

        double _ClampMinimum = 0.0;
        public double ClampMinimum 
        {
            get
            {
                return _ClampMinimum;
            }
        }
        public void SetClampMinimum(double clamp)
        {
            _ClampMinimum = clamp;
        }

        double _ClampMaximum = 1.0;
        public double ClampMaximum
        {
            get
            {
                return _ClampMaximum;
            }
        }
        public void SetClampMaximum(double clamp)
        {
            _ClampMaximum = clamp;
        }

        public CurveSegment Add(Point point)
        {
            CurveSegment[] points = new CurveSegment[Points.Length + 1];
            int idx = 0;
            foreach (CurveSegment p in Points)
            {
                if (p.X < point.X)
                {
                    points[idx] = p;
                    ++idx;
                }
                else
                {
                    break;
                }
            }

            CurveSegment ret = new CurveSegment(point) { Curve = this, Color = _Color };
            points[idx] = ret;

            for (int i = idx + 1; i < points.Length; ++i)
            {
                points[i] = Points[idx];
                ++idx;
            }

            Points = points;

            return ret;
        }

        public void Remove(CurveSegment point)
        {
            CurveSegment[] points = new CurveSegment[Points.Length - 1];

            int j = 0;
            for (int i = 0; i < Points.Length; ++i)
            {
                if (Points[i] != point)
                {
                    points[j] = Points[i];
                    ++j;
                }
            }

            Points = points;
        }
      
        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public CurveSegment[] Points { get; private set; }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            float[] color = new float[4];
            color[0] = _Color.ScA;
            color[1] = _Color.ScR;
            color[2] = _Color.ScG;
            color[3] = _Color.ScB;
            info.AddValue("Color", color);
            info.AddValue("Visible", _Visible);
            info.AddValue("Points", Points);
            info.AddValue("ClampMinimum", _ClampMinimum);
            info.AddValue("ClampMaximum", _ClampMaximum);
        }

        public Curve(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString("Name");
            float[] color = (float[])info.GetValue("Color", typeof(float[]));
            _Color.ScA = color[0];
            _Color.ScR = color[1];
            _Color.ScG = color[2];
            _Color.ScB = color[3];
            _Visible = info.GetBoolean("Visible");
            Points = (CurveSegment[])info.GetValue("Points", typeof(CurveSegment[]));
            try
            {
                _ClampMinimum = info.GetDouble("ClampMinimum");
                _ClampMaximum = info.GetDouble("ClampMaximum");
            }
            catch(Exception)
            {
                // Don't care
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Curve"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Curve(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            _Color = Color.FromRgb(0, 255, 255);
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {

            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("curve");
            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString("Visible", this.Visible.ToString());
            writer.WriteAttributeString("Min", this.ClampMinimum.ToString());
            writer.WriteAttributeString("Max", this.ClampMaximum.ToString());

            writer.WriteStartElement("Colour");
            writer.WriteAttributeString(
                "a", this._Color.ScA.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "r", this._Color.ScR.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "g", this._Color.ScG.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "b", this._Color.ScB.ToString(CultureInfo.InvariantCulture));
            writer.WriteEndElement();

            if (this.Points != null)
            {
                writer.WriteStartElement("Points");
                foreach (CurveSegment segment in this.Points)
                {
                    segment.Serialise(writer);                
                }
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }
            
            List<CurveSegment> loadedPoints = new List<CurveSegment>();
            this.Name = reader.GetAttribute("Name");
            this._Visible = bool.Parse(reader.GetAttribute("Visible"));
            this._ClampMinimum = double.Parse(reader.GetAttribute("Min"));
            this._ClampMaximum = double.Parse(reader.GetAttribute("Max"));

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Colour") == 0)
                {
                    _Color.ScA = float.Parse(reader.GetAttribute("a"), CultureInfo.InvariantCulture);
                    _Color.ScR = float.Parse(reader.GetAttribute("r"), CultureInfo.InvariantCulture);
                    _Color.ScG = float.Parse(reader.GetAttribute("g"), CultureInfo.InvariantCulture);
                    _Color.ScB = float.Parse(reader.GetAttribute("b"), CultureInfo.InvariantCulture);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Points") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            loadedPoints.Add(new CurveSegment(reader, this));
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Read();
                }
            }

            this.Points = loadedPoints.ToArray();
            reader.Skip();
        }
    }
}