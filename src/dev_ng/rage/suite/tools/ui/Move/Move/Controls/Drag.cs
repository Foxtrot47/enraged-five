﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

using Rage.Move.Utils;

namespace Rage.Move
{
    public class Drag : Thumb
    {
        public Drag()
        {
            base.DragDelta += new DragDeltaEventHandler(Drag_DragDelta);
        }

        void Drag_DragDelta(object sender, DragDeltaEventArgs e)
        {
            ISelectable selectable = DataContext as ISelectable;  
            Diagram diagram = VisualTreeHelper.GetParent(DataContext as UserControl) as Diagram;

            if (diagram != null && selectable != null && selectable.Selected)
            {
                double minleft = double.MaxValue;
                double mintop = double.MaxValue;

                var items = diagram.Selection.Current.OfType<UIElement>();

                foreach (UIElement item in items)
                {
                    double left = Canvas.GetLeft(item);
                    double top = Canvas.GetTop(item);
                    
                    minleft = double.IsNaN(left) ? 0 : Math.Min(left, minleft);
                    mintop = double.IsNaN(top) ? 0 : Math.Min(top, mintop);
                }

                double deltahoriz = e.HorizontalChange;
                double deltavert = e.VerticalChange;

                foreach (UIElement item in items)
                {
                    double left = Canvas.GetLeft(item);
                    double top = Canvas.GetTop(item);

                    if (double.IsNaN(left))
                        left = 0;
                    if (double.IsNaN(top))
                        top = 0;

                    Canvas.SetLeft(item, left + deltahoriz);
                    Canvas.SetTop(item, top + deltavert);

                    item.RaiseEvent(new RoutedEventArgs(DiagramNode.PositionChangedEvent));
                    diagram.RaiseEvent(new RoutedEventArgs(Diagram.DiagramChangedEvent));
                }

                diagram.InvalidateVisual();
                e.Handled = true;
            }
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            ISelectable selectable = this.DataContext as ISelectable;
            Diagram diagram = VisualTreeHelper.GetParent(selectable as UserControl) as Diagram;
            if (diagram != null && selectable != null)
            {
                if ((Keyboard.Modifiers & (ModifierKeys.Shift | ModifierKeys.Control)) != ModifierKeys.None)
                {
                    if (selectable.Selected)
                        diagram.Selection.Remove(selectable);
                    else
                        diagram.Selection.Add(selectable);
                }
                else
                if (!selectable.Selected)
                {
                    diagram.Selection.Select(selectable);
                }

                this.CaptureMouse();
                this.Cursor = Cursors.SizeAll;
            }
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            ISelectable selectable = this.DataContext as ISelectable;
            Diagram diagram = VisualTreeHelper.GetParent(selectable as UserControl) as Diagram;
            if (diagram != null && selectable != null)
            {
                //if (selectable.IsSelected)
                //    diagram.Selection.Remove(selectable);
            }

            if (this.IsMouseCaptured)
            {
                this.Cursor = Cursors.Arrow;
                this.ReleaseMouseCapture();
            }

            base.OnPreviewMouseUp(e);
        }
    }
}
