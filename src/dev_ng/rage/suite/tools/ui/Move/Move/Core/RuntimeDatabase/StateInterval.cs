﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rage.Move.Core;

namespace Move.Core
{

    public class StateInterval : Interval
    {
        //        public INode State;
        public RuntimeStateInstance StateId;
        public TimeSpan FinishedTransInTime = TimeSpan.MinValue;
        public TimeSpan StartedTransOutTime = TimeSpan.MaxValue;

        public TransitionInterval TransitionIn;
        public TransitionInterval TransitionOut;

        public StateInterval(RuntimeStateInstance stateId, TimeSpan startTime, TimeSpan endTime)
        {
            StateId = stateId;
            StartTime = startTime;
            EndTime = endTime;
        }

        protected StateInterval()
        {
        }

        public static StateInterval CreateWithKnownStart(RuntimeStateInstance stateId, TimeSpan time)
        {
            StateInterval s = new StateInterval();
            s.StateId = stateId;
            s.StartTime = time;
            return s;
        }

        public static StateInterval CreateWithKnownEnd(RuntimeStateInstance stateId, TimeSpan time)
        {
            StateInterval s = new StateInterval();
            s.StateId = stateId;
            s.EndTime = time;
            return s;
        }

        public override string ToString()
        {
            return String.Format("{0}: {1}", StateId, base.ToString());
        }

        public SignalHistory SignalHistory = new SignalHistory();
    }
}
