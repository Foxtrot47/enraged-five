﻿namespace New.Move.Core
{
    public enum EngagedChangedAction
    {
        Add,
        Remove
    }
}