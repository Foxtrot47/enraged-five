﻿namespace New.Move.Core
{
    using System;
    using System.Globalization;
    using System.Runtime.Serialization;
    using System.Xml;

    /// <summary>
    /// Represents a control parameter that uses a real floating point for its value.
    /// </summary>
    [Serializable]
    public class ParameterizedMotionSignal : Signal, IReferredSignal
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.ParameterizedMotionSignal"/> class.
        /// </summary>
        public ParameterizedMotionSignal()
        {
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.ParameterizedMotionSignal"/> class using the specified
        /// serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        protected ParameterizedMotionSignal(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.ParameterizedMotionSignal"/> class using the specified
        /// System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> parameter is null.
        /// </exception>
        public ParameterizedMotionSignal(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Enabled = true;
            this.Name = reader.GetAttribute(NameSerializationTag);
            this.Id = Guid.ParseExact(reader.GetAttribute(IdSerializationTag), "D");
            reader.Skip();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the <see cref="New.Move.Core.IDesc"/> object that describes the creation of
        /// this object.
        /// </summary>
        public override IDesc Desc
        {
            get { return new ParameterizedMotionDesc(this); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("parameterizedmotion");

            writer.WriteAttributeString(
                Signal.NameSerializationTag, this.Name);
            writer.WriteAttributeString(
                Signal.IdSerializationTag,
                this.Id.ToString("D", CultureInfo.InvariantCulture));

            writer.WriteEndElement();
        }

        /// <summary>
        /// Gets the parameter value for this signal that represents the type it is
        /// representing.
        /// </summary>
        /// <returns>
        /// The parameter value that this signal represents.
        /// </returns>
        public override Parameter GetParameter()
        {
            return Parameter.ParameterizedMotion;
        }

        /// <summary>
        /// Creates a reference to this parameterized motion signal.
        /// </summary>
        /// <returns>
        /// A new reference to this parameterized motion signal.
        /// </returns>
        public ReferenceSignal CreateReference()
        {
            return new ParameterizedMotionReference(this.Id, this.Name);
        }
        #endregion
    } // New.Move.Core.ParameterizedMotionSignal {Class}
} // New.Move.Core {Namespace}
