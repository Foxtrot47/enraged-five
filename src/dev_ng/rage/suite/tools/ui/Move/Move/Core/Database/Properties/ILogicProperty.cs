﻿using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    public interface ILogicContainerProperty
    {
        void SetParentOnChildren();
    }

    public interface ILogicProperty : IProperty
    {
        ILogic Parent { get; set; }
    }
}