﻿using System.Windows;

namespace Rage.Move.Utils
{
    public interface IMeasureable
    {
        Point Position { get; set; }
        Size DesiredSize { get; }
    }
}
