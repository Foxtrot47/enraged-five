﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Logging;

namespace New.Move.Core
{
    public class DatabaseIntegrityChecker
    {
        public DatabaseIntegrityChecker()
        {
        }

        public bool FoundIssues { get { return issuesFound_.Count > 0; } }
        public IEnumerable<string> IssueDescriptions
        {
            get 
            {
                foreach (IDatabaseIssue issue in issuesFound_)
                {
                    yield return issue.Description;
                }
            }
        }

        public void CheckDatabase(Database database)
        {
            issuesFound_.Clear();

            CheckTransitionOrderIntegrity(database);
            CheckNodes(database);
        }

        public void FixDatabase(Database database)
        {
            foreach (IDatabaseIssue issue in issuesFound_)
            {
                issue.Fix(database);
            }
        }

        private void CheckTransitionOrderIntegrity(Database database)
        {
            foreach (ITransitional transitionOwner in database.Nodes.Where(n => n is IAutomaton && n is ITransitional))
            {
                foreach (INode childNode in transitionOwner.Children)
                {
                    // Get the child's transition order (if it has one).  This is a little awkward because the TransitionOrder owners have no shared parent.
                    TransitionCollection childTransitionOrderCollection = null;
                    if (childNode is Motiontree)
                        childTransitionOrderCollection = ((Motiontree)childNode).TransitionOrder;
                    else if (childNode is StateMachine)
                        childTransitionOrderCollection = ((StateMachine)childNode).TransitionOrder;
                    else if (childNode is LogicParent)
                        childTransitionOrderCollection = ((LogicParent)childNode).TransitionOrder;

                    if (childTransitionOrderCollection != null)
                        CheckTransitionOrderIntegrity(database, transitionOwner as IAutomaton, childNode, childTransitionOrderCollection);
                }
            }
        }

        private void CheckTransitionOrderIntegrity(Database database, IAutomaton transitionOwner, INode childNode, TransitionCollection childTransitionOrderCollection)
        {
            foreach(Transition transition in childTransitionOrderCollection)
            {
                if (!transitionOwner.Transitions.Contains(transition))
                {
                    issuesFound_.Add(new OrphanedTransitionsIssue(transitionOwner, childNode, childTransitionOrderCollection, Database.GetHierarchicalNodeName(database, childNode)));
                }
            }
        }

        private void CheckNodes(Database database)
        {
            CheckForOrphanedNodes(database);
        }

        private void CheckForOrphanedNodes(Database database)
        {
            foreach (var node in database.Nodes)
            {
                if(database.Root.Id != node.Id && (node.Parent == Guid.Empty || database.Find(node.Parent) == null))
                {
                    issuesFound_.Add(new OrphanedNodeIssue(node, Database.GetHierarchicalNodeName(database, node)));
                }
            }
        }

        private List<IDatabaseIssue> issuesFound_ = new List<IDatabaseIssue>();
    }
}
