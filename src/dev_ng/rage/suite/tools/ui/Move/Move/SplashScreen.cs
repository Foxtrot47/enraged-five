﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

using Rockstar.MoVE.Interop.WindowsCodecs;
using System.Drawing;

namespace MoVE
{
    internal class SplashScreen
    {
        private void CreateBitmapBits(IStream resourceStream, out IntPtr hGlobal, out int cx, out int cy)
        {
            hGlobal = IntPtr.Zero;
            cx = cy = 0;
            int hr = 0;
            uint numFrames;
            IntPtr pImagingFactory = IntPtr.Zero;
            IntPtr pDecoder = IntPtr.Zero;
            IntPtr pFrameDecode = IntPtr.Zero;
            IntPtr pFormatConverter = IntPtr.Zero;
            IntPtr pBitmapFlipRotator = IntPtr.Zero;

            try
            {
                //Init WIC
                hr = WICCodec.CreateImagingFactory(WICCodec.WINCODEC_SDK_VERSION, out pImagingFactory);
                if (hr < 0)
                    throw new Exception("CreateImagingFactory failed");

                Guid vendor = Guid.Empty;
                hr = WICImagingFactory.CreateDecoderFromStream(pImagingFactory, resourceStream,
                                    ref vendor, WICDecodeOptions.WICDecodeMetadataCacheOnDemand, out pDecoder);
                if (hr < 0)
                    throw new Exception("CreateDecoderFromStream failed");

                hr = WICBitmapDecoder.GetFrameCount(pDecoder, out numFrames);
                if (hr < 0)
                    throw new Exception("GetFrameCount failed");

                for (uint i = 0; i < 1; i++)
                {
                    hr = WICBitmapDecoder.GetFrame(pDecoder, i, out pFrameDecode);
                    if (hr < 0)
                        throw new Exception("GetFrame failed");

                    int stride = 0;
                    uint width, height;
                    // Create a format converter

                    hr = WICImagingFactory.CreateFormatConverter(pImagingFactory, out pFormatConverter);
                    if (hr < 0)
                        throw new Exception("CreateFormatConverter failed");

                    Guid guigPixelFormat = WICPixelFormatGUIDs.WICPixelFormat32bppBGRA;

                    hr = WICFormatConverter.Initialize(pFormatConverter, pFrameDecode, ref guigPixelFormat,
                        DitherType.DitherTypeNone, IntPtr.Zero, 0, WICPaletteType.WICPaletteTypeCustom);

                    if (hr < 0)
                        throw new Exception("Initialize failed");

                    // Create a FlipRotator because windows requires the bitmap to be bottom-up
                    hr = WICImagingFactory.CreateBitmapFlipRotator(pImagingFactory, out pBitmapFlipRotator);
                    if (hr < 0)
                        throw new Exception("CreateBitmapFlipRotator failed");
                    // Init the format converter to output Bgra32
                    WICBitmapFlipRotator.Initialize(pBitmapFlipRotator, pFormatConverter,
                                    WICBitmapTransformOptions.WICBitmapTransformFlipVertical);

                    // Get the size
                    hr = WICBitmapSource.GetSize(pBitmapFlipRotator, out width, out height);
                    if (hr < 0)
                        throw new Exception("GetSize failed");

                    stride = (int)width * 4;
                    int dwordsize = Marshal.SizeOf(typeof(Int32));

                    // Force the stride to be a multiple of sizeof(DWORD)
                    stride = ((stride + dwordsize - 1) / dwordsize) * dwordsize;

                    int dibSize = Marshal.SizeOf(typeof(BITMAPINFOHEADER)) + stride * (int)height;

                    // Allocate the DIB bytes
                    hGlobal = GlobalAlloc(GMEM_MOVEABLE, (uint)dibSize);
                    if (hGlobal == IntPtr.Zero)
                        throw new Exception("GlobalAlloc failed");

                    IntPtr pBitmapHeader = GlobalLock(hGlobal);

                    if (pBitmapHeader == IntPtr.Zero)
                        throw new Exception("GlobalLock failed");

                    IntPtr pBitmapBits = (IntPtr)((int)pBitmapHeader + Marshal.SizeOf(typeof(BITMAPINFOHEADER)));

                    // Set the header
                    ZeroMemory(pBitmapHeader, Marshal.SizeOf(typeof(BITMAPINFOHEADER)));
                    BITMAPINFOHEADER bminfo = new BITMAPINFOHEADER();

                    bminfo.biSize = (uint)Marshal.SizeOf(typeof(BITMAPINFOHEADER));
                    bminfo.biPlanes = 1;
                    bminfo.biBitCount = 32;
                    bminfo.biCompression = BI_RGB;
                    bminfo.biWidth = (int)width;
                    bminfo.biHeight = (int)height;
                    bminfo.biSizeImage = (uint)stride * height;

                    Marshal.StructureToPtr(bminfo, pBitmapHeader, false);

                    // Copy the pixels
                    //Int32Rect rct = new Int32Rect();
                    Int32Rect rct = new Int32Rect();
                    rct.x = 0;
                    rct.y = 0;
                    rct.width = cx = (int)width;
                    rct.height = cy = (int)height;

                    hr = WICBitmapSource.CopyPixels(pBitmapFlipRotator, ref rct, (uint)stride, (uint)stride * height, pBitmapBits);
                    // Unlock the buffer
                    GlobalUnlock(hGlobal);
                    if (hr != 0)
                        throw new Exception("CopyPixels failed");
                }
            }
            catch
            {
                if (hGlobal != IntPtr.Zero)
                    GlobalFree(hGlobal);
            }
            finally
            {
                Helper.ReleaseInterface(ref pBitmapFlipRotator);
                Helper.ReleaseInterface(ref pFormatConverter);
                Helper.ReleaseInterface(ref pFrameDecode);
                Helper.ReleaseInterface(ref pDecoder);
                Helper.ReleaseInterface(ref pImagingFactory);
            }
        }

        private IStream GetHBitmapFromResource(IntPtr hInstance, string resourceType, int resourceId)
        {
            IStream pIStream = null;
            IntPtr hBitmap = IntPtr.Zero;
            IntPtr _hBuffer;

            IntPtr hResource = FindResource(hInstance, resourceId, resourceType);
            uint size = SizeofResource(hInstance, hResource);
            IntPtr pResourceData = LoadResource(hInstance, hResource);
            pResourceData = LockResource(pResourceData);

            _hBuffer = GlobalAlloc(GMEM_MOVEABLE, size);
            if (_hBuffer != IntPtr.Zero)
            {
                IntPtr pBuffer = GlobalLock(_hBuffer);
                CopyMemory(pBuffer, pResourceData, size);

                CreateStreamOnHGlobal(_hBuffer, false, out pIStream);
            }
            return pIStream;
        }

        public void Open()
        {
            IntPtr hInstance = Marshal.GetHINSTANCE(typeof(SplashScreen).Module);

            IntPtr hGlobal;
            int cx, cy;
            IntPtr hbr;

            IStream resourceStream = GetHBitmapFromResource(hInstance, SplashScreenResourceType, SplashScreenResourceId);
            CreateBitmapBits(resourceStream, out hGlobal, out cx, out cy);
            hbr = CreateDIBPatternBrush(hGlobal, (int)DibColorTableType.DIB_PAL_COLORS);   // handle to bitmap

            // Prepare the window class
            WNDCLASSEX splashScreenWindowClass = new WNDCLASSEX();
            splashScreenWindowClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
            splashScreenWindowClass.lpfnWndProc = _splashWndProc;
            splashScreenWindowClass.cbClsExtra = 0;
            splashScreenWindowClass.cbWndExtra = 0;
            splashScreenWindowClass.hInstance = hInstance;
            splashScreenWindowClass.hCursor = LoadCursor(IntPtr.Zero, IDC_ARROW);
            splashScreenWindowClass.hbrBackground = hbr;
            splashScreenWindowClass.lpszClassName = SplashScreenClassName;
            splashScreenWindowClass.lpszMenuName = String.Empty;

            // Register the window class
            if (RegisterClassEx(splashScreenWindowClass) != 0)
            {
                // Calculate the window position
                int screenWidth = GetSystemMetrics(SM_CXSCREEN);
                int screenHeight = GetSystemMetrics(SM_CYSCREEN);
                int x = (screenWidth - cx) / 2;
                int y = (screenHeight - cy) / 2;

                // Create and display the window
                _splashScreenHwnd = CreateWindowEx(
                    WS_EX_TOOLWINDOW,
                    SplashScreenClassName,
                    String.Empty,
                    WS_POPUP | WS_VISIBLE,
                    x, y, cx, cy,
                    IntPtr.Zero, IntPtr.Zero, hInstance, IntPtr.Zero);
            }
        }

        public void Close()
        {
            DestroyWindow(_splashScreenHwnd);
            _splashScreenHwnd = IntPtr.Zero;
        }

        public static int MainWindowProc(IntPtr hWnd, int uMsg, int wParam, int lParam)
        {
            //  Send message to default handler first
            int result = DefWindowProc(hWnd, uMsg, wParam, lParam);

            if (uMsg == WM_PAINT || uMsg == WM_ERASEBKGND)
            {
                //  Get the DC for the window
                IntPtr hdc = GetWindowDC(hWnd);

                if (hdc != IntPtr.Zero)
                {
                    //  Create a new font, store the old one and set to use the new one
                    IntPtr newFont = CreateFont(14, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "Arial");
                    IntPtr defaultFont = SelectObject(hdc, newFont);
                    //  Rectangle left and top is where text starts but right and bottom has to be size of window
                    //  otherwise clipping occurs
                    Rectangle rectangle = new Rectangle(55, 314, 498, 370);
                    //  Set the text colour and background mode to transparent (background colour set in case 
                    //  transparency doesnt work)
                    SetTextColor(hdc, ColorTranslator.ToWin32(Color.FromArgb(255, 241, 162, 44)));
                    SetBkColor(hdc, ColorTranslator.ToWin32(Color.FromArgb(0, 51, 51, 51)));
                    SetBkMode(hdc, TRANSPARENT);
                    //  Create the version string
                    string versionString = "Version " + New.Move.Core.DatabaseExportSerializer.GetVersionString();
                    string versionSubString = New.Move.Core.DatabaseExportSerializer.GetVersionSubString();

                    string str = versionString + "\n" + versionSubString;
                    //  Draw the text to the window
                    DrawText(hdc, str, str.Length, ref rectangle, 0);
                    //  Set the default font back
                    SelectObject(hdc, defaultFont);
                    //  Release the DC for the window
                    ReleaseDC(hWnd, hdc);
                }
            }
            //  Return the result from the default handler
            return result;
        }

        [DllImport("gdi32.dll", ExactSpelling = true, PreserveSig = true, SetLastError = true)]
        static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateFont(int nHeight, int nWidth, int nEscapement,
           int nOrientation, int fnWeight, uint fdwItalic, uint fdwUnderline, uint
           fdwStrikeOut, uint fdwCharSet, uint fdwOutputPrecision, uint
           fdwClipPrecision, uint fdwQuality, uint fdwPitchAndFamily, string lpszFace);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern int DrawText(IntPtr hdc, string lpStr, int nCount, ref Rectangle lpRect, int wFormat);

        [DllImport("gdi32.dll")]
        static extern int SetTextColor(IntPtr hdc, int crColor);
        
        [DllImport("gdi32.dll")]
        static extern int SetBkColor(IntPtr hdc, int crColor);

        [DllImport("gdi32.dll")]
        static extern int SetBkMode(IntPtr hdc, int iBkMode);

        [DllImport("user32.dll")]
        static extern IntPtr GetWindowDC(IntPtr hwnd);

        [DllImport("user32.dll")]
        static extern int ReleaseDC(IntPtr hwnd, IntPtr hDC);

        [DllImport("User32.dll", CharSet = CharSet.Unicode, EntryPoint = "LoadBitmapW")]
        private static extern IntPtr LoadBitmap(
            IntPtr hInstance,       // handle to application instance
            IntPtr lpBitmapName     // name of bitmap resource
        );

        [DllImport("Gdi32.dll")]
        private static extern bool DeleteObject(
          IntPtr hObject   // handle to graphic object
        );

        [DllImport("Gdi32.dll")]
        private static extern IntPtr CreatePatternBrush(
          IntPtr hbmp   // handle to bitmap
        );

        [DllImport("gdi32")]
        public static extern IntPtr CreateDIBPatternBrush(
            IntPtr hPackedDIB,
            int wUsage);


        [DllImport("Gdi32.dll", CharSet = CharSet.Unicode, EntryPoint = "GetObjectW")]
        private static extern int GetBitmapInformation(
            IntPtr hgdiobj,    // handle to graphics object
            int cbBuffer,                   // size of buffer for object information
            ref BITMAP lpvObject            // buffer for object information
        );

        [DllImport("User32.dll", CharSet = CharSet.Unicode, EntryPoint = "CreateWindowExW")]
        private static extern IntPtr CreateWindowEx(
            uint dwExStyle,
            string lpClassName,
            string lpWindowName,
            uint dwStyle,
            int x,
            int y,
            int nWidth,
            int nHeight,
            IntPtr hWndParent,
            IntPtr hMenu,
            IntPtr hInstance,
            IntPtr lpParam
            );

        [DllImport("User32.dll")]
        private static extern bool DestroyWindow(IntPtr hWnd);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private class WNDCLASSEX
        {
            public int cbSize = Marshal.SizeOf(typeof(WNDCLASSEX));
            public int style;
            [MarshalAs(UnmanagedType.FunctionPtr)]
            public WindowProc lpfnWndProc;
            public int cbClsExtra;
            public int cbWndExtra;
            public IntPtr hInstance;
            public IntPtr hIcon;
            public IntPtr hCursor;
            public IntPtr hbrBackground;
            public string lpszMenuName;
            public string lpszClassName;
            public IntPtr hSmIcon;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private struct BITMAP
        {
            public int bmType;
            public int bmWidth;
            public int bmHeight;
            public int bmWidthBytes;
            public short bmPlanes;
            public short bmBitsPixel;
            public IntPtr bmBits;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        internal struct BITMAPINFOHEADER
        {
            internal UInt32 biSize;
            internal Int32 biWidth;
            internal Int32 biHeight;
            internal UInt16 biPlanes;
            internal UInt16 biBitCount;
            internal UInt32 biCompression;
            internal UInt32 biSizeImage;
            internal Int32 biXPelsPerMeter;
            internal Int32 biYPelsPerMeter;
            internal UInt32 biClrUsed;
            internal UInt32 biClrImportant;
        }

        public enum DibColorTableType : int
        {
            DIB_RGB_COLORS = 0, /* color table in RGBs */
            DIB_PAL_COLORS = 1 /* color table in palette indices */
        }


        enum Bool
        {
            False = 0,
            True = 1
        }
        [DllImport("User32.dll", CharSet = CharSet.Unicode, EntryPoint = "RegisterClassExW")]
        private static extern short RegisterClassEx(WNDCLASSEX lpWndClass);

        [DllImport("User32.dll", CharSet = CharSet.Unicode, EntryPoint = "DefWindowProcW")]
        private static extern int DefWindowProc(IntPtr hWnd, int Msg, int wParam, int lParam);

        private delegate int WindowProc(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("User32.dll", CharSet = CharSet.Unicode, EntryPoint = "LoadCursorW")]
        private static extern IntPtr LoadCursor(
            IntPtr hInstance,
            IntPtr lpCursorName
        );

        [DllImport("User32.dll")]
        private static extern int GetSystemMetrics(int nIndex);

        [DllImport("Kernel32.dll")]
        static extern void Sleep(int nIndex);

        [DllImport("kernel32.dll")]
        private static extern IntPtr CopyMemory(IntPtr destination, IntPtr source, uint length);

        [DllImport("kernel32.dll")]
        private static extern IntPtr FindResource(IntPtr hModule, int lpName, int lpType);

        [DllImport("kernel32.dll")]
        private static extern IntPtr FindResource(IntPtr hModule, int lpName, string lpType);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalAlloc(uint uFlags, uint dwBytes);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalFree(IntPtr hMem);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalLock(IntPtr hMem);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalUnlock(IntPtr hMem);

        [DllImport("kernel32")]
        public static extern void ZeroMemory(IntPtr pointer, int size);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr LoadResource(IntPtr hModule, IntPtr hResInfo);

        [DllImport("kernel32.dll")]
        private static extern IntPtr LockResource(IntPtr hResData);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern uint SizeofResource(IntPtr hModule, IntPtr hResInfo);

        [DllImport("ole32.dll")]
        private static extern int CreateStreamOnHGlobal(IntPtr hGlobal, bool fDeleteOnRelease, out IStream ppstm);

        private const uint GMEM_MOVEABLE = 0x0002;

        private const int BitmapResourceId = 101;
        private const int SplashScreenResourceId = 101;
        private const string SplashScreenResourceType = "PNG";

        private const int SM_CXSCREEN = 0;
        private const int SM_CYSCREEN = 1;
        private const int CS_HREDRAW = 0x0001;
        private const int CS_VREDRAW = 0x0002;
        private const int CS_OWNDC = 0x0020;
        private const int WM_PAINT = 0x000F;
        private const int WM_ERASEBKGND = 0x0014;
        private const int TRANSPARENT = 1;
        private const uint WS_EX_PALETTEWINDOW = 0x00000100 | 0x00000080;
        private const uint WS_EX_TOOLWINDOW = 0x00000080;
        private const uint WS_POPUP = 0x80000000;
        private const uint WS_CHILD = 0x40000000;
        private const uint WS_VISIBLE = 0x10000000;

        public const int BI_RGB = 0;

        private const string SplashScreenClassName = "WpfSplashScreen";

        private readonly static IntPtr IDC_ARROW = new IntPtr(32512);
        private readonly static WindowProc _splashWndProc = new WindowProc(MainWindowProc); // Keep the reference alive

        private IntPtr _splashScreenHwnd;
    }
}