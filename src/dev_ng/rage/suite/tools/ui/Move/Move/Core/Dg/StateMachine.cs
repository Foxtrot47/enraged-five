﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;

using Rage.Move.Utils.Serialization;

namespace Rage.Move.Core.Dg
{
#if no_bdsm
    [Serializable]
    public class StateMachine : ITransitionalNode
    {
        public StateMachine(string name)
        {
            Name = name;
            Unid = Uniquifier16.Create();
            Id = Guid.NewGuid();

            _Children.CollectionChanged += new NotifyCollectionChangedEventHandler(Children_CollectionChanged);
            /*
            Activate += new ActivatedEventHandler(delegate() {  if (Parent != null) Runtime.Database.Active = this; });
            Deactivate += new ActivatedEventHandler(delegate() { if (Parent != null) Runtime.Database.Active = this.Parent; });
             */
        }

        public StateMachine()
        {
            _Children.CollectionChanged += new NotifyCollectionChangedEventHandler(Children_CollectionChanged);
            /*
            Activate += new ActivatedEventHandler(delegate() { if (Parent != null) Runtime.Database.Active = this; });
            Deactivate += new ActivatedEventHandler(delegate() { if (Parent != null) Runtime.Database.Active = Parent; });
             */
        }

        string _Name;
        [XmlAttribute]
        public string Name { get { return _Name; } set { _Name = value; OnNameChanged(); } }
        [XmlAttribute]
        public Guid Id { get; set; }
        [XmlTypeConverter(typeof(Uniquifier16Converter))]
        public Uniquifier16 Unid { get; set; }
        [XmlIgnore]
        public Guid Guid { get { return Id; } }
        public Point OutputPosition { get; set; }
        public Point InputPosition { get; set; }
        public Point Position { get; set; }
        
        [XmlIgnore]
        public LogicStateNode LogicalParent { get; set; } // TODO: find a way around this!!!
        [XmlIgnore]
        public StateMachine Parent { get; set; }

        [XmlIgnore]
        public bool HasInputs { get { return _Anchors.OfType<AnchorSource>().Count() != 0; } }

        [XmlIgnore]
        public bool HasOutputs { get { return _Anchors.OfType<AnchorResult>().Count() != 0; } }

        Guid _Default;
        public Guid Default
        {
            get { return _Default; }
            set
            {
                if (_Default != value)
                {
                    _Default = value;
                    OnDefaultChanged();
                }
            }
        }

        [field:NonSerialized]
        public event EventHandler DefaultChanged;
        void OnDefaultChanged()
        {
            if (DefaultChanged != null)
                DefaultChanged(this, EventArgs.Empty);
        }

        public void Remove()
        {
            Parent.Children.Remove(this);
        }

        ObservableCollection<ITransitionalNode> _Children = new ObservableCollection<ITransitionalNode>();
        public ObservableCollection<ITransitionalNode> Children { get { return _Children; } set { _Children = value; _Children.CollectionChanged += new NotifyCollectionChangedEventHandler(Children_CollectionChanged); } }

        ObservableCollection<ITransition> _Transitions = new ObservableCollection<ITransition>();
        public ObservableCollection<ITransition> Transitions { get { return _Transitions; } set { _Transitions = value; } }

        ObservableCollection<IAnchor> _Anchors = new ObservableCollection<IAnchor>();
        public ObservableCollection<IAnchor> Anchors { get { return _Anchors; } set { _Anchors = value; } }

        ObservableCollection<IConnection> _Connections = new ObservableCollection<IConnection>();
        public ObservableCollection<IConnection> Connections { get { return _Connections; } set { _Connections = value; } }

        Guid _Database = Guid.Empty;
        [XmlIgnore]
        public Guid Database
        {
            get { return _Database; }
            set
            {
                _Database = value;
                foreach (ITransitionalNode child in _Children)
                    child.Database = _Database;
            }
        }

        [field:NonSerialized]
        public event ActivatedEventHandler Activate = null;
        public void OnActivate() { if (Activate != null) Activate(); }

        [field:NonSerialized]
        public event ActivatedEventHandler Deactivate = null;
        public void OnDeactivate() { if (Deactivate != null) Deactivate(); }

        public void OnOutputPositionChanged(Point point) { OutputPosition = point; }
        public void OnInputPositionChanged(Point point) { InputPosition = point; }
        public void OnPositionChanged(Point point) { Position = point; }

        public event NameChangedEventHandler NameChanged;
        void OnNameChanged()
        {
            if (NameChanged != null)
                NameChanged(_Name);
        }

        public ITransitionalNode GetDefault()
        {
            foreach (ITransitionalNode child in _Children)
            {
                if (child.Id == Default)
                    return child;
            }

            return null;
        }

        public New.Move.Core.ILogic GetInitial()
        {
            ITransitionalNode def = GetDefault();
            return def.GetInitial();
        }

        void Children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (ITransitionalNode item in e.OldItems)
                        {
                            List<ITransition> transitions = new List<ITransition>();
                            foreach (ITransition transition in _Transitions)
                            {
                                if (transition.Source == item.Id || transition.Sink == item.Id)
                                    transitions.Add(transition);
                            }

                            foreach (ITransition transition in transitions)
                                _Transitions.Remove(transition);

                            if (Default == item.Id && _Children.Count != 0)
                                Default = _Children.First().Id;
                            else
                            if (Default == item.Id && _Children.Count == 0)
                                Default = Guid.Empty;

                            item.Parent = null;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (ITransitionalNode item in e.NewItems)
                            item.Parent = this;

                        if (Default == Guid.Empty && _Children.Count != 0)
                            Default = _Children.First().Id;
                    }
                    break;
            }
            /*
            if (Runtime.Database.Active == this)
                Runtime.Database.OnActiveChanged();
             */
        }
    }
#endif
}
