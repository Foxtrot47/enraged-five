﻿using System;

namespace New.Move.Core
{
    public interface IEngageable
    {
        event EventHandler<IsEngagedChangedEventArgs> IsEngagedChanged;
        void OnEngagedChanged(EngagedChangedAction action);
    }
}