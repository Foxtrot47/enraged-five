﻿using System;

namespace New.Move.Core
{
    [Flags]
    public enum SyncType
    {
        Phase = (1 << 0),
        Tag = (1 << 1),
        None = (1 << 2)
    }
}
