﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class RealTransitionProperty : ITransitionProperty, ISerializable
    {

        #region Contructors
        public static implicit operator RealTransitionProperty(double real)
        {
            RealTransitionProperty property = new RealTransitionProperty();
            property.Value = real;

            return property;
        }

        public RealTransitionProperty()
        {
            Flags = PropertyInput.Real | PropertyInput.Value;
            Input = PropertyInput.Value;
            
            Id = Guid.NewGuid();
        }

        public RealTransitionProperty(RealTransitionProperty other)
        {
            this.Flags = other.Flags;
            this.Input = other.Input;
            this.SelectedSignal = other.SelectedSignal;
            if (other.SelectedSignal != null && other.SelectedSignal is ICloneable)
            {
                this.SelectedSignal = other.SelectedSignal;
            }

            this.Value = other.Value;
            this.Name = other.Name;
            this.Parent = other.Parent;

            Id = Guid.NewGuid();
        }
        #endregion // Constructors

        #region Properties
        public string Name { get; set; }
        public Guid Id { get; set; }
        
        public PropertyInput Flags { get; private set; }

        public ITransition Parent { get; set; }
        public double Value { get; set; }

        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }
        PropertyInput _Input;

        public Signal SelectedSignal
        {
            get { return _SelectedSignal; }
            set
            {
                if (_SelectedSignal != value)
                {
                    _SelectedSignal = value;
                    OnPropertyChanged("SelectedSignal");
                }
            }
        }
        Signal _SelectedSignal;
        #endregion // Properties

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }
        #endregion // Events

        #region Serialisation
        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Value = "Value";

            public static string Input = "Input";
            public static string Flags = "Flags";
            public static string SelectedSignal = "SelectedSignal";
        }

        public RealTransitionProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ITransition)info.GetValue(SerializationTag.Parent, typeof(ITransition));
            Value = (double)info.GetValue(SerializationTag.Value, typeof(double));

            try
            {
                Input = (PropertyInput)info.GetValue(SerializationTag.Input, typeof(PropertyInput));
                Flags = (PropertyInput)info.GetValue(SerializationTag.Flags, typeof(PropertyInput));
                SelectedSignal = (Signal)info.GetValue(SerializationTag.SelectedSignal, typeof(Signal));
            }
            catch
            {
                Flags = PropertyInput.Real | PropertyInput.Value;
                Input = PropertyInput.Value;
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Value, Value);

            info.AddValue(SerializationTag.Input, Input);
            info.AddValue(SerializationTag.Flags, Flags);
            info.AddValue(SerializationTag.SelectedSignal, SelectedSignal);
        }
        #endregion // Serialisation

        public object Clone()
        {
            return new RealTransitionProperty(this);
        }

        
        /// <summary>
        /// Initialises a new instance of the <see cref="RealTransitionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public RealTransitionProperty(XmlReader reader, Database database, ITransition parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Flags = PropertyInput.Real | PropertyInput.Value;
            this.Parent = parent;
            this.Deserialise(reader, database);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Input", ((uint)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "Value", this.Value.ToString(CultureInfo.InvariantCulture));

            if (this.SelectedSignal != null)
            {
                writer.WriteAttributeString("Signal", this.SelectedSignal.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedSignal.Name);
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                return;
            }


            this.Input = (PropertyInput)(uint.Parse(reader.GetAttribute("Input")));
            this.Value = double.Parse(reader.GetAttribute("Value"));

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedSignal = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedSignal == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedSignal = database.GetSignal(signalName);
                    if (this.SelectedSignal == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}'. This needs fixing before a export is allowed.", signalId);
                    }
                }
            }

            reader.Skip();
        }
    }
}