﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Rage.Move.Core;

namespace Rage.Move
{
    public partial class NetworkInstanceDlg : Window
    {
        public NetworkInstanceDlg(ReadOnlyCollection<NetworkInstanceData> networkData)
        {
            InitializeComponent();
            _networkList.DataContext = networkData;

            Network = null;
            NetworkIndex = null;
        }

        public NetworkInstanceData Network { get; set; }
        public Nullable<int> NetworkIndex { get; private set; }

        void Ok_Click(object sender, RoutedEventArgs e)
        {
            Network = (NetworkInstanceData)_networkList.SelectedItem;
            NetworkIndex = _networkList.SelectedIndex;

            this.DialogResult = true;
        }

        void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
