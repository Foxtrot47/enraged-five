﻿using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

using Rage.Move.Utils.Serialization;

namespace Rage.Move.Core.Dg
{
    public interface IArrayItem
    {
        string Name { get; set; }
        Guid Id { get; }

        void Create(ObservableCollection<IAnchor> anchors);
    }

    [Serializable]
    public abstract class ArrayItemBase : IArrayItem
    {
        public ArrayItemBase(string name)
        {
            Name = name;
            Id = Guid.NewGuid();
        }

        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public Guid Id { get; set; }

        public abstract void Create(ObservableCollection<IAnchor> anchors);
    }

    [Serializable]
    public abstract class ArrayItemSource : ArrayItemBase
    {
        public ArrayItemSource(string name) 
            : base (name) { }
    }

    [Serializable]
    public class TransformSourceArray : ArrayItemSource 
    {
        public TransformSourceArray(string name) 
            : base (name) { }

        public TransformSourceArray()
            : base(null) { }

        public override void Create(ObservableCollection<IAnchor> anchors)
        {
            anchors.Add(new TransformSource(Name));
        }
    }

    [Serializable]
    public class SignalSourceArray : ArrayItemSource 
    {
        public SignalSourceArray(string name) 
            : base (name) { }

        public SignalSourceArray()
            : base(null) { }

        public override void Create(ObservableCollection<IAnchor> anchors)
        {
            anchors.Add(new SignalSource(Name));
        }
    }

    [Serializable]
    public class ArrayPair : ArrayItemBase
    {
        public ArrayPair(string name, IArrayItem first, IArrayItem second)
            : base(name)
        {
            First = first;
            Second = second;
        }

        public ArrayPair()
            : base(null) { }

        [XmlInferType]
        public IArrayItem First { get; set; }
        [XmlInferType]
        public IArrayItem Second { get; set; }

        public override void Create(ObservableCollection<IAnchor> anchors)
        {
            First.Create(anchors);
            Second.Create(anchors);
        }
    }
}
