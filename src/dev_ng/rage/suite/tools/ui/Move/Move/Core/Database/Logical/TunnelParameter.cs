﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml;

using Move.Core;
using Move.Core.Restorable;
using Move.Utils;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class TunnelParameter : RestorableObject, ILogic, ISerializable
    {
        public TunnelParameter()
        {
            Connections = new NoResetObservableCollection<Connection>();
            Id = Guid.NewGuid();
            ParameterAnchor = new TunnelParameterAnchor(this);
        }

        public void OnLoadComplete()
        {

        }

        public string Name
        {
            get { return ""; /*Parameter.Name;*/ }
            set { /* do nothing */ }
        }

        public string AdditionalText
        {
            get { return "";  }
            set {  }
        }

        public string Type
        {
            get
            {
                return "";
            }
        }

        public bool Enabled
        {
            get
            {
                return Parameter.Enabled;
            }
            set
            {
                Parameter.Enabled = value;
            }
        }

        public Point Position { get; set; }

        public Database Database { get; set; }
        public Guid Id { get; private set; }
        public Guid Parent { get; set; }

        public IDesc Desc { get; internal set; }

        public object Tag { get; set; }

        public TunnelParameterAnchor ParameterAnchor { get; private set; }
        public Signal Parameter { get; set; }

        NoResetObservableCollection<Connection> _Connections;
        public NoResetObservableCollection<Connection> Connections
        {
            get { return _Connections; }
            set { _Connections = value; }
        }

        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            if (IsSelectedChanged != null)
            {
                IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
            }
        }

        public event EventHandler<IsEngagedChangedEventArgs> IsEngagedChanged;
        public void OnEngagedChanged(EngagedChangedAction action)
        {
            if (IsEngagedChanged != null)
            {
                IsEngagedChanged(this, new IsEngagedChangedEventArgs(action));
            }
        }

        public void Dispose()
        {
            if (Parent != Guid.Empty)
            {
                ITransitional parent = Database.Find(Parent) as ITransitional;
                parent.Remove(this);
            }

            Database.Nodes.Remove(this);
        }

        static class SerializationTag
        {
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Database = "Database";
            public static string Position = "Position";
            public static string ParameterAnchor = "ParameterAnchor";
            public static string Parameter = "Parameter";
        }

        public TunnelParameter(SerializationInfo info, StreamingContext context)
        {
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (Guid)info.GetValue(SerializationTag.Parent, typeof(Guid));
            Database = (Database)info.GetValue(SerializationTag.Database, typeof(Database));

            Position = (Point)info.GetValue(SerializationTag.Position, typeof(Point));

            ParameterAnchor = (TunnelParameterAnchor)info.GetValue(
                SerializationTag.ParameterAnchor, typeof(TunnelParameterAnchor));
            Parameter = (Signal)info.GetValue(SerializationTag.Parameter, typeof(Signal));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Database, Database);

            info.AddValue(SerializationTag.Position, Position);

            info.AddValue(SerializationTag.ParameterAnchor, ParameterAnchor);
            info.AddValue(SerializationTag.Parameter, Parameter);
        }

        public void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        object ICloneable.Clone()
        {
            return null;
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Output"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public TunnelParameter(XmlReader reader, Database database, Guid parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("TunnelParameter");
            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Parameter != null)
            {
                writer.WriteAttributeString("Parameter", this.Parameter.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("ParameterName", this.Parameter.Name);
            }

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (ParameterAnchor != null)
            {
                writer.WriteStartElement("ParameterAnchor");
                this.ParameterAnchor.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            string signalId = reader.GetAttribute("Parameter");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.Parameter = this.Database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.Parameter == null)
                {
                    string signalName = reader.GetAttribute("ParameterName");
                    this.Parameter = this.Database.GetSignal(signalName);
                    if (this.Parameter == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}', name '{1}'. This needs fixing before a export is allowed.", signalId, signalName);
                    }
                }
            }

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "ParameterAnchor") == 0)
                {
                    this.ParameterAnchor = new TunnelParameterAnchor(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            return ids;
        }
    }
}