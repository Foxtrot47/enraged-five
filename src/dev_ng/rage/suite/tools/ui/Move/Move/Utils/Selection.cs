﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Rage.Move.Utils
{
    public delegate void SelectionChangingEventHandler(List<ISelectable> selected);
    public delegate void SelectionEventHandler(ISelectable selection);
    public delegate void SelectionClearedEventHandler();

    public class Selection
    {
        List<ISelectable> current = new List<ISelectable>();
        Panel panel;

        static public Selection Instance { get; private set; }
        
        public Selection(Panel panel)
        {
            this.panel = panel;
            Instance = this;
        }

        public List<ISelectable> Current
        {
            get { return this.current; }
        }

        public event SelectionChangingEventHandler SelectionChanging;
        void OnSelectionChanging()
        {
            if (SelectionChanging != null)
                SelectionChanging(Current);
        }

        public event SelectionEventHandler SelectionChanged;
        void OnSelectionChanged(ISelectable selection)
        {
            if (SelectionChanged != null)
                SelectionChanged(selection);
        }

        public event SelectionClearedEventHandler SelectionCleared;
        void OnSelectionCleared()
        {
            if (SelectionCleared != null)
                SelectionCleared();
        }

        public void Select(ISelectable selectable)
        {
            OnSelectionChanging();

            Clear();
            Add(selectable);

            OnSelectionChanged(selectable);
        }

        public void Add(ISelectable selectable)
        {
            if (selectable is IGroupable)
            {
                List<IGroupable> items = GetMembers(selectable as IGroupable);

                foreach (ISelectable item in items)
                {
                    item.Selected = true;
                    Current.Add(item);
                }
            }
            else
            {
                selectable.Selected = true;
                Current.Add(selectable);
            }
        }

        public void Remove(ISelectable selectable)
        {
            if (selectable is IGroupable)
            {
                List<IGroupable> items = GetMembers(selectable as IGroupable);

                foreach (ISelectable item in items)
                {
                    item.Selected = false;
                    Current.Remove(item);
                }
            }
            else
            {
                selectable.Selected = false;
                Current.Remove(selectable);
            }
        }

        public void Clear()
        {
            Current.ForEach(selectable => selectable.Selected = false);
            Current.Clear();

            OnSelectionCleared();
        }

        public void SelectAll()
        {
            Clear();
            Current.AddRange(panel.Children.OfType<ISelectable>());
            Current.ForEach(selectable => selectable.Selected = true);
        }

        public List<IGroupable> GetMembers(IGroupable groupable)
        {
            IEnumerable<IGroupable> list = panel.Children.OfType<IGroupable>();
            IGroupable root = GetRoot(list, groupable);
            return GetMembers(list, groupable);
        }

        public IGroupable GetRoot(IGroupable groupable)
        {
            IEnumerable<IGroupable> list = panel.Children.OfType<IGroupable>();
            return GetRoot(list, groupable);
        }

        IGroupable GetRoot(IEnumerable<IGroupable> list, IGroupable groupable)
        {
            if (groupable == null || groupable.Parent == Guid.Empty)
            {
                return groupable;
            }
            else
            {
                foreach (IGroupable item in list)
                {
                    if (item.Id == groupable.Parent)
                        return GetRoot(list, item);
                }

                return null;
            }
        }

        List<IGroupable> GetMembers(IEnumerable<IGroupable> list, IGroupable parent)
        {
            List<IGroupable> members = new List<IGroupable>();
            members.Add(parent);

            var children = list.Where(item => item.Parent == parent.Id);

            foreach (IGroupable child in children)
            {
                members.AddRange(GetMembers(list, child));
            }

            return members;
        }
    }
}
