﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class PropertyLogicalPropertyArray : LogicalPropertyArray<Property>, ISerializable
    {
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public PropertyLogicalPropertyArray(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public PropertyLogicalPropertyArray()
            : base()
        {
        }

        public PropertyLogicalPropertyArray(ILogic parent)
        {
            Parent = parent;
        }

        public void Add(ILogicProperty property)
        {
            property.Parent = Parent;
            _Items.Add((Property)property);
        }

        public TypeCode GetTypeCode()
        {
            return TypeCode.Object;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }

        }

        public override object Clone()
        {
            PropertyLogicalPropertyArray clone = new PropertyLogicalPropertyArray();
            clone.Name = this.Name;
            clone.Parent = this.Parent;

            foreach (Property item in this.Items)
            {
                clone.Add(item.Clone() as ILogicProperty);
            }
            return clone;
        }
    }
}
