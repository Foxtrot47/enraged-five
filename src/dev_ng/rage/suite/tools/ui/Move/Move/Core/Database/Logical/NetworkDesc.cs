﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using New.Move.Core;

//using Rockstar.MoVE.Framework;

namespace MoVE.Core
{
    //[Export("MotionTreeDescriptor", typeof(IDesc)), DescriptorMetadata(1)]
    public class NetworkDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Network";
            public static int Id = 30; 
            public static string Help = "Network";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get; private set; }

        public Type ConstructType { get { return typeof(Network); } }

        public NetworkDesc()
        {
            try
            {
                BitmapImage icon = new BitmapImage();
                icon.BeginInit();
                icon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Move.Core.Images.PaletteIcon_MotionTree.png");
                icon.EndInit();
                Icon = icon;
            }
            catch
            {
                Icon = null;
            }
        }

        public INode Create(Database database)
        {
            return new Network(this) { Name = Const.Name };
        }
    }
}