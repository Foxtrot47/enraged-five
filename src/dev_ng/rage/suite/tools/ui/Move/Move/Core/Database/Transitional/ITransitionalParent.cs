﻿using System;

namespace New.Move.Core
{
    /// <summary>
    /// When implemented represnts a object that is the parent of a set of transitional objects.
    /// </summary>
    public interface ITransitionalParent
    {
        #region Properties
        /// <summary>
        /// Gets or sets the transitional object that is a child of this logic parent and
        /// is set to the default transitional object. 
        /// </summary>
        ITransitional Default { get; set; }

        /// <summary>
        /// Gets the collection of transitions that are currently in this logic parent.
        /// </summary>
        TransitionCollection Transitions { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the transition inside this state machine that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the transition to retrieve.
        /// </param>
        /// <returns>
        /// The transition with the specified id if found; otherwise false.
        /// </returns>
        Transition GetTransition(Guid id);
        #endregion
    }
}
