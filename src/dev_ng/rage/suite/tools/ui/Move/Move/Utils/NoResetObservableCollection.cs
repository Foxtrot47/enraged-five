﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Move.Utils
{
    [Serializable]
    public class NoResetObservableCollection<T> : ObservableCollection<T>
    {
        public NoResetObservableCollection()
        {
        }

        protected override void ClearItems()
        {
            IList<T> items = new List<T>(this);
            foreach (T item in items)
            {
                Remove(item);
            }
        }
    }
}