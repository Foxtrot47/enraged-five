﻿namespace New.Move.Core
{
    public abstract class PassthroughAnchor : AnchorBase
    {
        public PassthroughAnchor(string name)
            : base(name) { }
    }
}