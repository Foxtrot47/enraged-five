﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

using Move.Core;
using Move.Utils;

using Rockstar.MoVE.Framework.DataModel;

namespace New.Move.Core
{
    [Serializable]
    public class WeightedTransform : LogicalPropertyPair<SourceTransform, RealLogicalProperty>, ISerializable
    {
           static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string First = "First";
            public static string Second = "Second";
            public static string Parent = "Parent";
        }

        public WeightedTransform(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public WeightedTransform()
            : base()
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.First, _First);
            info.AddValue(Const.Second, _Second);
            info.AddValue(Const.Parent, Parent);
        }

        ILogic _Parent;
        public override ILogic Parent
        {
            get
            {
                return _Parent;
            }
            set
            {
                if (_Parent != value)
                {
                    _Parent = value;
                    _First.Parent = _Parent;
                    _Second.Parent = _Parent;
                }
            }
        }

        public override object Clone()
        {
            WeightedTransform clone = new WeightedTransform();
            clone.Name = this.Name;
            clone.Parent = this.Parent;

            return clone;
        }
    }

    // WeightedTransformArray
    [Serializable]
    public class WeightedTransformArray : LogicalPropertyArray<WeightedTransform>, ISerializable//, IConvertible
    {
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public WeightedTransformArray(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public WeightedTransformArray()
            : base()
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }
        }

        public override object Clone()
        {
            WeightedTransformArray clone = new WeightedTransformArray();
            clone.Name = this.Name;
            clone.Parent = this.Parent;
            foreach (var item in this.Items)
            {
                WeightedTransform newItem = item.Clone() as WeightedTransform;
                clone.Items.Add(newItem);
            }
            return clone;
        }
    }

    // TransformArray
    [Serializable]
    public class TransformArray : LogicalPropertyArray<SourceTransform>, ISerializable
    {
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public TransformArray(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public TransformArray()
            : base()
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }
        }

        public override object Clone()
        {
            TransformArray clone = new TransformArray();
            clone.Name = this.Name;
            clone.Parent = this.Parent;
            foreach (var item in this.Items)
            {
                SourceTransform newItem = item.Clone() as SourceTransform;
                clone.Items.Add(newItem);
            }
            return clone;
        }
    }

    [Serializable]
    public class LogicalPropertyArray<T> : ILogicPropertyArray, ISerializable
    {
        public LogicalPropertyArray()
        {
            _Items.CollectionChanged += new NotifyCollectionChangedEventHandler(Items_CollectionChanged);
            Id = Guid.NewGuid();
        }

        public ObservableCollection<T> Items { get { return _Items; } }
        protected ObservableCollection<T> _Items = new ObservableCollection<T>();

        public string Name { get; set; }
        public Guid Id { get; set; }

        public object Tag { get; set; }

        public ILogic Parent { get; set; }

        public void SetParentOnChildren()
        {
            foreach (T item in _Items)
            {
                if (item is ILogicProperty)
                {
                    (item as ILogicProperty).Parent = Parent;
                }
            }
        }

        public IEnumerable<IProperty> Children
        {
            get
            {
                return _Items.OfType<IProperty>();
            }
        }

        public void AddHandler(NotifyCollectionChangedEventHandler handler)
        {
            _Items.CollectionChanged += handler;
        }

        public void RemoveHandler(NotifyCollectionChangedEventHandler handler)
        {
            _Items.CollectionChanged -= handler;
        }

        public void Add()
        {
            ILogicProperty p = (ILogicProperty)Activator.CreateInstance(typeof(T));
            p.Parent = Parent;
            _Items.Add((T)p);
        }

        public void Add(int n)
        {
            for(int i = 0; i < n; i++)
            {
                ILogicProperty p = (ILogicProperty)Activator.CreateInstance(typeof(T));
                p.Parent = Parent;
                _Items.Add((T)p);
            }
        }

        public void Remove(IProperty property)
        {
            //if (property.GetType() == typeof(T))
            {
                _Items.Remove((T)property);
            }
        }

        public void Clear()
        {

            while (_Items.Count() > 0 && _Items[0] != null)
            {
                if (_Items[0] is IProperty)
                {
                    Remove(_Items[0] as IProperty);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public LogicalPropertyArray(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));

            int count = (int)info.GetValue(SerializationTag.Count, typeof(int));
            for (int index = 0; index < count; ++index)
            {
                _Items.Add((T)info.GetValue(index.ToString(), typeof(T)));
            }

            _Items.CollectionChanged += new NotifyCollectionChangedEventHandler(Items_CollectionChanged);
        }

        void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (ILogicProperty property in e.NewItems.OfType<ILogicProperty>())
                {
                    property.Parent = Parent;
                }

                foreach (IAnchor anchor in e.NewItems.OfType<IAnchor>())
                {
                    anchor.Parent = Parent;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (ILogicProperty property in e.OldItems.OfType<ILogicProperty>())
                {
                    property.Parent = null;
                }

                foreach (IAnchor anchor in e.OldItems.OfType<IAnchor>())
                {
                    anchor.Parent = Parent;
                }
            }
        }

        public bool Contains(IProperty property, out IProperty at)
        {
            foreach (IProperty p in Children)
            {
                if (p is ILogicPropertyPair)
                {
                    if ((p as ILogicPropertyPair).Contains(property))
                    {
                        at = p;
                        return true;
                    }
                }
                else if (p is ILogicPropertyGroup)
                {
                    if ((p as ILogicPropertyGroup).Contains(property))
                    {
                        at = p;
                        return true;
                    }
                }
                else
                {
                    if (p == property)
                    {
                        at = p;
                        return true;
                    }
                }
            }

            at = null;
            return false;
        }

        public int IndexOf(IProperty property)
        {
            int i = 0;
            foreach (IProperty p in Children)
            {
                if (p is ILogicPropertyPair)
                {
                    if ((p as ILogicPropertyPair).Contains(property))
                    {
                        return i;
                    }
                }
                else if (p is ILogicPropertyGroup)
                {
                    if ((p as ILogicPropertyGroup).Contains(property))
                    {
                        return i;
                    }
                }
                else
                {
                    if (p == property)
                    {
                        return i;
                    }
                }

                ++i;
            }

            return -1;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);

            info.AddValue(SerializationTag.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }
        }

        public virtual object Clone()
        {
            LogicalPropertyArray<T> clone = new LogicalPropertyArray<T>();
            clone.Name = this.Name;
            clone.Parent = this.Parent;
            return clone;
        }
    }
}