﻿using System;

namespace Rage.Move.Utils
{
    public delegate void ReceivedDataEventHandler(Receiver sender);

    public class Receiver
    {
        uint messageId;
        ByteBuffer data;

        public event ReceivedDataEventHandler OnReceiveData = null;

        public Receiver(uint messageId)
        {
            this.messageId = messageId;
        }

        public uint MessageId
        {
            get { return this.messageId; }
        }

        public ByteBuffer Data
        {
            get { return this.data; }
        }

        public void RecieveData(ByteBuffer data)
        {
            this.data = data;
            if (OnReceiveData != null)
                OnReceiveData(this);
        }
    }

    public delegate void InterpretDataEventHandler(ref ByteBuffer buffer);

    public class Interpreter
    {
        public uint MessageId { get; private set; }

        public event InterpretDataEventHandler OnInterpretData = null;

        public Interpreter(uint messageId)
        {
            MessageId = messageId;
        }

        public void InterpretData(ref ByteBuffer buffer)
        {
            if (OnInterpretData != null)
                OnInterpretData(ref buffer);
        }
    }
}