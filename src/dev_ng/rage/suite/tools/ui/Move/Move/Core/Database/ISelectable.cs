﻿using System;

namespace New.Move.Core
{
    public interface ISelectable : ICloneable
    {
        event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        void OnSelectedChanged(SelectionChangedAction action, int count);

        void Dispose();

        object Tag { get; set; }


    }
}