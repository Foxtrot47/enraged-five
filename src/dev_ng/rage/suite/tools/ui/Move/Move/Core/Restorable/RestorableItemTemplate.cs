﻿using System;

namespace Move.Core.Restorable
{
    internal class RestorableItemTemplate
    {
        internal RestorableItemTemplate(string name, Type type)
        {
            Name = name;
            Type = type;
        }

        internal string Name;
        internal Type Type;
    }
}