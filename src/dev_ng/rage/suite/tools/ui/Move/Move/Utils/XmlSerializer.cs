﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Rage.Move.Utils.Serialization
{
    [AttributeUsage(AttributeTargets.Property)]
    public class XmlInferTypeAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Property)]
    public class XmlTypeConverterAttribute : Attribute 
    {
        public XmlTypeConverterAttribute(Type type)
        {
            Type = type;
        }

        public Type Type { get; set; }
    }

    public class XmlSerializer : IDisposable
    {
        private static class Const
        {
            public const string TypeKeyPrefix = "TK";
        }

        public XmlSerializer()
        {
            IgnoreSerializationAttribute = false;
            IgnoreSerializationErrors = false;
            UseTypeDictionary = true;

            Tags = new XmlSerializationTag();

            SerializationIgnoredAttributeType = null;
        }

        ArrayList _ObjectList = new ArrayList();

        Hashtable _TypeDictionary = new Hashtable();

        public bool IgnoreSerializationAttribute { get; set; }
        public bool IgnoreSerializationErrors { get; set; }
        public bool UseTypeDictionary { get; set; }

        public IXmlSerializationTag Tags { get; set; }

        public Type SerializationIgnoredAttributeType { get; set; }

        public void Serialize(string filename, object obj)
        {
            XmlDocument doc = Serialize(obj);
            doc.Save(filename);
        }

        public XmlDocument Serialize(object obj)
        {
            XmlDocument doc = new XmlDocument();
            XmlDeclaration decl = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(decl);

            Serialize(null, doc, obj);

            return doc;
        }

        public void Serialize(string name, XmlDocument doc, object obj)
        {
            XmlElement root = doc.CreateElement(obj.GetType().Name);

            //SetObjectInfoAttributes(name, obj.GetType(), root);

            if (doc.DocumentElement == null)
                doc.AppendChild(root);
            else
                doc.DocumentElement.AppendChild(root);

            /*
                Type ctor = TypeInfo.GetBinaryConstructorType(obj.GetType());
                if (ctor != null)
                    SerializeBinaryObject(root, obj, ctor);
                else
             */
    
            SerializeProperties(root, obj);

            //WriteTypeDictionary(root);
        }

        public void Serialize(string name, XmlNode parent, object obj)
        {
            XmlDocument doc = parent.OwnerDocument;

            XmlElement root = doc.CreateElement(obj.GetType().Name);
            parent.AppendChild(root);

            //SetObjectInfoAttributes(name, obj.GetType(), root);

            Type ctor = TypeInfo.GetBinaryConstructorType(obj.GetType());
            if (ctor != null)
                SerializeBinaryObject(root, obj, ctor);
            else
                SerializeProperties(root, obj);

            //WriteTypeDictionary(root);
        }

        ObjectInfo GetObjectInfo(string name, Type type)
        {
            ObjectInfo info = new ObjectInfo();
            info.Name = name;
            info.Type = type.FullName;
            info.Assembly = type.Assembly.FullName;

            return info;
        }

        void SetObjectInfoAttributes(string property, Type type, XmlNode node)
        {
            ObjectInfo info = new ObjectInfo();
            if (type != null)
                info = GetObjectInfo(property, type);

            if (UseTypeDictionary)
            {
                string key = GetTypeKey(type);

                XmlAttribute nameattr = node.OwnerDocument.CreateAttribute(Tags.Name);
                nameattr.Value = info.Name;
                node.Attributes.Append(nameattr);

                XmlAttribute typeattr = node.OwnerDocument.CreateAttribute(Tags.Type);
                typeattr.Value = info.Type;
                node.Attributes.Append(typeattr);

                XmlAttribute assemblyattr = node.OwnerDocument.CreateAttribute(Tags.Assembly);
                assemblyattr.Value = "";
                node.Attributes.Append(assemblyattr);
            }
            else
            {
                XmlAttribute nameattr = node.OwnerDocument.CreateAttribute(Tags.Name);
                nameattr.Value = info.Name;
                node.Attributes.Append(nameattr);

                XmlAttribute typeattr = node.OwnerDocument.CreateAttribute(Tags.Type);
                typeattr.Value = info.Type;
                node.Attributes.Append(typeattr);

                XmlAttribute assemblyattr = node.OwnerDocument.CreateAttribute(Tags.Assembly);
                assemblyattr.Value = info.Assembly;
                node.Attributes.Append(assemblyattr);
            }
        }

        bool CheckPropertyHasToBeSerialized(PropertyInfo property)
        {
            if (SerializationIgnoredAttributeType != null)
                return property.GetCustomAttributes(SerializationIgnoredAttributeType, true).Length == 0;

            return true;
        }

        void SerializeProperties(XmlNode parent, object obj)
        {
            if (TypeInfo.IsCollection(obj.GetType()))
            {
                SetCollectionItems(parent, obj, (ICollection)obj);
            }
            else
            {
                //XmlElement node = parent.OwnerDocument.CreateElement(Tags.Properties);
                SetProperties(parent as XmlElement, obj);
                //parent.AppendChild(node);
            }
        }

        void SetProperties(XmlElement node, object obj)
        {
            PropertyInfo[] info = obj.GetType().GetProperties();
            Debug.Assert(info.Length > 0, "No property found to serialize for type " + obj.GetType().Name + "! Currently Serializer only works on public properties with get & set");

            foreach (PropertyInfo property in obj.GetType().GetProperties())
            {
                object[] attributes = property.GetCustomAttributes(typeof(XmlIgnoreAttribute), true);
                if (attributes.Length == 0)
                    SetProperty(node, obj, property);
            }
        }

        void SetProperty(XmlElement parent, object obj, PropertyInfo info)
        {
            _ObjectList.Add(obj);

            object val = info.GetValue(obj, null);
            if (val == null)
                return;

            //if (_ObjectList.Contains(val))
            //    return;

            SetProperty(parent, obj, val, info);
        }

        void SetProperty(XmlElement parent, object obj, object value, PropertyInfo info)
        {
            try
            {
                if (value == null || value.Equals(""))
                    return;

                Type type = value.GetType();
                if (CheckPropertyHasToBeSerialized(info) && (type.IsSerializable || IgnoreSerializationAttribute) && /*info.CanWrite &&*/ (type.IsPublic || type.IsEnum))
                {
                    string name = info.Name;

                    object[] infer = info.GetCustomAttributes(typeof(XmlInferTypeAttribute), true);
                    if (infer.Length != 0)
                        name = type.Name;

                    object[] attribs = info.GetCustomAttributes(typeof(XmlAttributeAttribute), true);
                    if (attribs.Length != 0)
                    {
                        XmlAttribute attribute = parent.OwnerDocument.CreateAttribute(info.Name);
                        attribute.InnerText = value.ToString();
                        parent.Attributes.Append(attribute);
                    }
                    else
                    {
                        object[] attributes = info.GetCustomAttributes(typeof(XmlTypeConverterAttribute), true);
                        if (attributes.Length != 0)
                        {
                            XmlTypeConverterAttribute attribute = (XmlTypeConverterAttribute)attributes[0];
                            TypeConverter converter = (TypeConverter)Activator.CreateInstance(attribute.Type);

                            if (converter.CanConvertTo(typeof(string)))
                            {
                                string str = (string)converter.ConvertTo(value, typeof(string));

                                XmlElement property = parent.OwnerDocument.CreateElement(name);
                                SetXmlElementFromBasicPropertyValue(property, typeof(string), str, parent);
                                parent.AppendChild(property);
                            }
                        }
                        else
                        {
                            XmlElement property = parent.OwnerDocument.CreateElement(name);

                            if (TypeInfo.IsCollection(type))
                                SetCollectionItems(property, obj, (ICollection)value);
                            else
                                SetXmlElementFromBasicPropertyValue(property, type, value, parent);

                            parent.AppendChild(property);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                if (!IgnoreSerializationErrors)
                    throw e;
            }
        }

        void SetXmlElementFromBasicPropertyValue(XmlElement property, Type type, object value, XmlNode parent)
        {
            if (type.IsAssignableFrom(typeof(string)))
            {
                property.InnerText = value.ToString();
                return;
            }
            else 
            {
                TypeConverter converter = TypeDescriptor.GetConverter(type);
                if (converter.CanConvertFrom(typeof(string)) && converter.CanConvertTo(typeof(string)))
                {
                    property.InnerText = (string)converter.ConvertTo(value, typeof(string));
                    return;
                }
                else 
                {
                    bool complex = false;

                    XmlElement proplist = null;
                    Debug.Assert(type.GetProperties().Length > 0, "No property found to serialize for type " + type.Name + "! Currently serializer only works on public properties with get & set");

                    foreach (PropertyInfo info in type.GetProperties())
                    {
                        // this fails to write out the IArrayItem that is infered...
                        if (CheckPropertyHasToBeSerialized(info) && /*(info.PropertyType.IsSerializable || IgnoreSerializationAttribute) &&*/ /*info.CanWrite &&*/ (info.PropertyType.IsPublic || info.PropertyType.IsEnum))
                        {
                            complex = true;

                            if (proplist == null)
                            {
                                //proplist = parent.OwnerDocument.CreateElement(Tags.Properties);
                                //property.AppendChild(proplist);
                            }

                            object[] attributes = info.GetCustomAttributes(typeof(XmlIgnoreAttribute), true);
                            if (attributes.Length == 0)
                                SetProperty(property, value, info);
                        }
                    }

                    if (!complex)
                        property.InnerText = value.ToString();
                }
            }
        }

        void SerializeBinaryObject(XmlNode parent, object obj, Type type)
        {
            XmlElement proplist = null;
            string val = null;

            try
            {
                TypeConverter converter = TypeDescriptor.GetConverter(obj.GetType());
                if (converter.CanConvertTo(typeof(byte[])) || typeof(Stream).IsAssignableFrom(obj.GetType()))
                {
                    byte[] bytes = null;

                    if (typeof(Stream).IsAssignableFrom(obj.GetType()))
                    {
                        BinaryContainerTypeConverter conv = new BinaryContainerTypeConverter();
                        bytes = conv.ConvertStreamToByteArray((Stream)obj);
                    }
                    else
                    {
                        bytes = (byte[])converter.ConvertTo(obj, typeof(byte[]));
                    }

                    proplist = parent.OwnerDocument.CreateElement(Tags.Constructor);
                    parent.AppendChild(proplist);

                    SetObjectInfoAttributes("0", type, proplist);

                    XmlNode data = proplist.OwnerDocument.CreateElement(Tags.BinaryData);
                    proplist.AppendChild(data);

                    SetObjectInfoAttributes("0", typeof(byte[]), data);

                    val = Convert.ToBase64String(bytes, 0, bytes.Length);
                    data.InnerText = val.ToString();
                }
            }
            catch (Exception e)
            {
                if (!IgnoreSerializationErrors)
                    throw e;
            }
        }

        void SetCollectionItems(XmlNode parent, object obj, ICollection value)
        {
            if (parent == null || obj == null || value == null)
                return;

            try
            {
                ICollection collection = value;

                //XmlElement element = parent.OwnerDocument.CreateElement(Tags.Items);
                //parent.AppendChild(element);

                //int count = 0;

                if (TypeInfo.IsDictionary(collection.GetType()))
                {
                    IDictionary dictionary = (IDictionary)collection;
                    IDictionaryEnumerator enumerator = dictionary.GetEnumerator();

                    while (enumerator.MoveNext())
                    {
                        XmlElement item = parent.OwnerDocument.CreateElement(enumerator.Current.GetType().Name);
                        parent.AppendChild(item);

                        //XmlElement properties = parent.OwnerDocument.CreateElement(Tags.Properties);
                        //item.AppendChild(properties);

                        SetProperties(item, enumerator.Current);
                    }
                }
                else
                {
                    IEnumerator enumerator = collection.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        object current = enumerator.Current;

                        XmlElement item = parent.OwnerDocument.CreateElement(current.GetType().Name);

                        /*
                        if (current != null)
                            SetObjectInfoAttributes(null, current.GetType(), item);
                        else
                            SetObjectInfoAttributes(null, null, item);

                        item.Attributes[Tags.Name].Value = "" + count;
                        ++count;
                         */

                        parent.AppendChild(item);

                        if (current == null)
                            continue;

                        Type type = current.GetType();
                        if (TypeInfo.IsCollection(type))
                            SetCollectionItems(item, obj, (ICollection)current);
                        else
                            SetXmlElementFromBasicPropertyValue(item, type, current, parent);

                    }
                }
            }
            catch (Exception e)
            {
                if (!IgnoreSerializationErrors)
                    throw e;
            }
        }

        Hashtable BuildSerializableTypeDictionary()
        {
            Hashtable hashtable = new Hashtable();

            IDictionaryEnumerator enumerator = _TypeDictionary.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Type type = (Type)enumerator.Entry.Key;
                string key = (string)enumerator.Value;

                TypeInfo info = new TypeInfo(type);
                hashtable.Add(key, info);
            }

            return hashtable;
        }

        string GetTypeKey(object obj)
        {
            if (obj != null)
                return null;

            return GetTypeKey(obj.GetType());
        }

        string GetTypeKey(Type type)
        {
            if (type == null)
                return null;

            if (!_TypeDictionary.ContainsKey(type))
                _TypeDictionary.Add(type, Const.TypeKeyPrefix + _TypeDictionary.Count);

            return (string)_TypeDictionary[type];
        }

        void WriteTypeDictionary(XmlNode parent)
        {
            bool use = UseTypeDictionary;

            try
            {
                if (UseTypeDictionary)
                {
                    XmlElement element = parent.OwnerDocument.CreateElement(Tags.TypeDictionary);
                    parent.AppendChild(element);

                    Hashtable dictionary = BuildSerializableTypeDictionary();

                    UseTypeDictionary = false;

                    SetObjectInfoAttributes(null, dictionary.GetType(), element);
                    SerializeProperties(element, dictionary);

                    UseTypeDictionary = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                UseTypeDictionary = use;
            }
        }

        public void Reset()
        {
            if (_ObjectList != null)
                _ObjectList.Clear();

            if (_TypeDictionary != null)
                _TypeDictionary.Clear();
        }

        public void Dispose()
        {
            Reset();
        }
    }

    [Serializable]
    [TypeConverter(typeof(BinaryContainerTypeConverter))]
    public class BinaryContainer : ISerializable, IObjectReference
    {
        byte[] _Data = null;

        public BinaryContainer(byte[] data)
        {
            _Data = data;
        }

        public BinaryContainer(Stream data)
        {
            BinaryContainerTypeConverter converter = new BinaryContainerTypeConverter();
            _Data = converter.ConvertStreamToByteArray(data);
        }

        public int Size
        {
            get { return (_Data != null) ? _Data.Length : 0; }
        }

        public byte[] Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.SetType(typeof(byte[]));
        }

        public object GetRealObject(StreamingContext context)
        {
            return _Data;
        }
    }

    public class BinaryContainerTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type type)
        {
            if (type == typeof(byte[]))
                return true;

            return base.CanConvertFrom(context, type);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is byte[])
                return new BinaryContainer((byte[])value);
            else
            if (value is Stream)
                return ConvertStreamToByteArray((Stream)value);

            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type type)
        {
            if (type == typeof(byte[]))
                return true;
            else
            if (type == typeof(Stream))
                return true;

            return base.CanConvertTo(context, type);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type type)
        {
            if (type == typeof(byte[]))
                return ((BinaryContainer)value).Data;
            else
            if (type == typeof(Stream))
                return new MemoryStream(((BinaryContainer)value).Data);

            return base.ConvertTo(context, culture, value, type);
        }

        public byte[] ConvertStreamToByteArray(Stream s)
        {
            if (s == null)
                return null;

            byte[] bytes = new byte[s.Length];
            int bytesToRead = (int)s.Length;
            int bytesRead = 0;

            while (0 < bytesToRead)
            {
                int n = s.Read(bytes, bytesRead, bytesToRead);
                if (n == 0)
                    break;

                bytesRead += n;
                bytesToRead -= n;
            }

            s.Close();

            return bytes;
        }
    }

    public interface IXmlSerializationTag
    {
        string Assembly { get; }
        string Index { get; }
        string Item { get; }
        string Items { get; }
        string Key { get; }
        string Value { get; }
        string Name { get; }
        string Object { get; }
        string Properties { get; }
        string Property { get; }
        string TypeDictionary { get; }
        string Type { get; }
        string GenericTypeArguments { get; }
        string Constructor { get; }
        string BinaryData { get; }
    }

    public class XmlSerializationTag : IXmlSerializationTag
    {
        string _Assembly = "assembly";
        public virtual string Assembly { get { return _Assembly; } }
        
        string _Index = "index";
        public virtual string Index { get { return _Index; } }

        string _Item = "item";
        public virtual string Item { get { return _Item; } }

        string _Items = "items";
        public virtual string Items { get { return _Items; } }

        string _Key = "key";
        public virtual string Key { get { return _Key; } }

        string _Value = "value";
        public virtual string Value { get { return _Value; } }

        string _Name = "name";
        public virtual string Name { get { return _Name; } }

        string _Object = "object";
        public virtual string Object { get { return _Object; } }

        string _Properties = "properties";
        public virtual string Properties { get { return _Properties; } }

        string _Property = "property";
        public virtual string Property { get { return _Property; } }

        string _TypeDictionary = "typedictionary";
        public virtual string TypeDictionary { get { return _TypeDictionary; } }

        string _Type = "type";
        public virtual string Type { get { return _Type; } }

        string _GenericTypeArguments = "generictypearguments";
        public virtual string GenericTypeArguments { get { return _GenericTypeArguments; } }

        string _Constructor = "constructor";
        public virtual string Constructor { get { return _Constructor; } }

        string _BinaryData = "binarydata";
        public virtual string BinaryData { get { return _BinaryData; } }
    }

    internal struct ObjectInfo
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Assembly { get; set; }
        public string Value { get; set; }

        public string ConstructorParameterType { get; set; }
        public string ConstructorParameterAssembly { get; set; }

        public override string ToString()
        {
            string name = Name;
            if (String.IsNullOrEmpty(name))
                name = "<Name Not Set>";

            string type = Type;
            if (String.IsNullOrEmpty(type))
                type = "<Type Not Set>";

            string assembly = Assembly;
            if (String.IsNullOrEmpty(assembly))
                assembly = "<Asembly Not Set>";

            return name + ";" + type + ";" + assembly;
        }

        public bool IsSufficient
        {
            get { return (String.IsNullOrEmpty(Type) || String.IsNullOrEmpty(Assembly)) ? false : true; }
        }
    }

    public class TypeInfo
    {
        public TypeInfo(object obj)
        {
            if (obj == null)
                return;

            Assembly = obj.GetType().Assembly.FullName;
            Type = obj.GetType().FullName;
        }

        public TypeInfo(Type type)
        {
            if (type == null)
                return;

            Assembly = type.Assembly.FullName;
            Type = type.FullName;
        }

        public string Type { get; set; }
        public string Assembly { get; set; }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool IsCollection(Type type)
        {
            if (typeof(ICollection).IsAssignableFrom(type))
                return true;

            return false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool IsDictionary(Type type)
        {
            if (typeof(IDictionary).IsAssignableFrom(type))
                return true;

            return false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool IsList(Type type)
        {
            if (typeof(IList).IsAssignableFrom(type))
                return true;

            return false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool IsArray(string type)
        {
            if (type != null && type.EndsWith("[]"))
                return true;

            return false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool HasBinaryConstructor(Type type)
        {
            ConstructorInfo[] constructors = type.GetConstructors();
            for (int i = 0; i < constructors.Length; ++i)
            {
                ParameterInfo[] parameters = constructors[i].GetParameters();
                if (parameters.Length == 1)
                {
                    Type parameter = parameters[0].ParameterType;
                    if (typeof(Stream).IsAssignableFrom(parameter) || typeof(byte[]).IsAssignableFrom(parameter))
                        return true;
                }
            }

            return false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static Type GetBinaryConstructorType(Type type)
        {
            foreach (ConstructorInfo constructor in type.GetConstructors())
            {
                ParameterInfo[] parameters = constructor.GetParameters();
                if (parameters.Length == 1)
                {
                    Type parameter = parameters[0].ParameterType;
                    if (typeof(byte[]).IsAssignableFrom(parameter))
                        return typeof(byte[]);
                    else
                    if (typeof(Stream).IsAssignableFrom(parameter))
                        return typeof(Stream);
                }
            }

            return null;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool HasByteArrayConstructor(Type type)
        {
            foreach (ConstructorInfo constructor in type.GetConstructors())
            {
                ParameterInfo[] parameters = constructor.GetParameters();
                if (parameters.Length == 1)
                {
                    if (typeof(byte[]).IsAssignableFrom(parameters[0].ParameterType))
                        return true;
                }
            }

            return false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool HasStreamConstructor(Type type)
        {
            foreach (ConstructorInfo constructor in type.GetConstructors())
            {
                ParameterInfo[] parameters = constructor.GetParameters();
                if (parameters.Length == 1)
                {
                    if (typeof(Stream).IsAssignableFrom(parameters[0].ParameterType))
                        return true;
                }
            }

            return false;
        }
    }

}
