﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Core.Restorable;
using Move.Utils;

using RSG.Base.Logging;
using RSG.ManagedRage;
using System.Diagnostics;
using Rage.Move.Core;
using System.Globalization;

namespace New.Move.Core
{
    /// <summary>
    /// Represents a transition between two <see cref="ITransitional"/> objects.
    /// </summary>
    [Serializable]
    public class Transition : RestorableObject, INotifyPropertyChanged, ISelectable, ISerializable
    {        
        #region Fields
        /// <summary>
        /// The private field for the <see cref="Tag"/> property.
        /// </summary>
        private object tag;

        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field for the <see cref="Source"/> property.
        /// </summary>
        private ITransitional source;

        /// <summary>
        /// The private field for the <see cref="Dest"/> property.
        /// </summary>
        private ITransitional destination;

        /// <summary>
        /// The private field for the <see cref="Parent"/> property.
        /// </summary>
        private IAutomaton parent;

        /// <summary>
        /// The private field for the <see cref="Descriptor"/> property.
        /// </summary>
        private ITransitionDesc descriptor;

        /// <summary>
        /// The private field for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field for the <see cref="Content"/> property.
        /// </summary>
        private TransitionBase content;

        /// <summary>
        /// The private field for the <see cref="Conditions"/> property.
        /// </summary>
        private ConditionCollection conditions;

        ///// <summary>
        ///// The private field for the <see cref="ReEvaluate"/> property.
        ///// </summary>
        //private bool reevaluate;

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        private const string NameSerialisationTag = @"Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        private const string IdSerialisationTag = @"Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Source"/> property.
        /// </summary>
        private const string SourceSerialisationTag = @"Source";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Dest"/> property.
        /// </summary>
        private const string DestSerialisationTag = @"Dest";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="ReEvaluate"/> property.
        /// </summary>
        private const string ReEvaluateSerialisationTag = @"ReEvaluate";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Conditions"/> property.
        /// </summary>
        private const string ConditionsSerialisationTag = @"Conditions";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Content"/> property.
        /// </summary>
        private const string ContentSerialisationTag = @"Content";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Parent"/> property.
        /// </summary>
        private const string ParentSerialisationTag = @"Parent";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Transition"/> class.
        /// </summary>
        /// <param name="descriptor">
        /// The descriptor to use with this transition.
        /// </param>
        /// <param name="source">
        /// The source object this transition will have.
        /// </param>
        /// <param name="dest">
        /// The destination object this transition will have.
        /// </param>
        /// <param name="parent">
        /// The parent this transition will have.
        /// </param>
        public Transition(ITransitionDesc descriptor, ITransitional source, ITransitional dest, IAutomaton parent)
        {
            this.conditions = new ConditionCollection();
            this.id = Guid.NewGuid();

            this.name = descriptor.Name;
            this.source = source;
            this.destination = dest;
            this.descriptor = descriptor;
            this.parent = parent;

            if (this.descriptor == null)
                return;
            
            this.Content = this.descriptor.Create(this.source, this.destination) as TransitionBase;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Transition"/> class as
        /// a copy of the specified instance.
        /// </summary>
        /// <param name="name">
        /// The instance to copy.
        /// </param>
        /// <param name="source">
        /// The source for this new transition.
        /// </param>
        /// <param name="dest">
        /// The destination for this new transition.
        /// </param>
        public Transition(Transition other, ITransitional source, ITransitional dest)
        {
            this.conditions = new ConditionCollection();
            this.id = Guid.NewGuid();

            this.source = source;
            this.destination = dest;
            this.name = other.name;
            this.descriptor = other.descriptor;
            this.Parent = other.Parent;

            if (other.Content is ICloneable)
                this.Content = (other.content as ICloneable).Clone() as TransitionBase;

            if (this.content != null)
            {
                this.content.Source = source;
                this.content.Dest = dest;
                this.content.Parent = (IAutomaton)Source.Database.Find(source.Parent);
            }

            if (other.conditions != null)
            {
                foreach (Condition condition in other.Conditions)
                    Conditions.Add(new Condition(condition));
            }

            if (this.descriptor == null || this.content == null)
                return;

            if (this.content == null)
            {
                this.Content = this.descriptor.Create(this.source, this.destination) as TransitionBase;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Transition"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public Transition(SerializationInfo info, StreamingContext context)
        {
            this.id = Guid.NewGuid();
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case NameSerialisationTag:
                        this.name = (string)entry.Value;
                        break;
                    case SourceSerialisationTag:
                        this.source = (ITransitional)entry.Value;
                        break;
                    case DestSerialisationTag:
                        this.destination = (ITransitional)entry.Value;
                        break;
                    case ConditionsSerialisationTag:
                        this.conditions = (ConditionCollection)entry.Value;
                        break;
                    case ContentSerialisationTag:
                        this.Content = (TransitionBase)entry.Value;
                        break;
                    case ParentSerialisationTag:
                        this.parent = (IAutomaton)entry.Value;
                        break;
                    case IdSerialisationTag:
                        this.id = (Guid)entry.Value;
                        break;
                }
            }

            this.descriptor = Runtime.Instance.TransitionDescriptors.Find(desc => (desc.GetType() == Content.Descriptor));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Transition"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="database">
        /// The root database the transition belongs to.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public Transition(XmlReader reader, Database database, IAutomaton parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.parent = parent;
            this.Deserialise(reader, database);
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the selection state of this transition has changed.
        /// </summary>
        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;

        /// <summary>
        /// Occurs whenever a property value on this instance has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the unique global identifier for this transition.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or sets the object tag that can be set to the UI component representing
        /// this transition.
        /// </summary>
        public object Tag
        {
            get { return tag; } 
            set { tag = value; }
        }

        /// <summary>
        /// Gets or sets the source transitional object for this transition.
        /// </summary>
        public ITransitional Source
        {
            get
            {
                return this.source;
            }

            set
            {
                this.source = value;
                OnPropertyChanged("Source");
            }
        }

        /// <summary>
        /// Gets or sets the transitional object that is the destination for this transition.
        /// </summary>
        public ITransitional Dest
        {
            get
            {
                return this.destination;
            }

            set
            {
                this.destination = value;
                OnPropertyChanged("Dest");
            }
        }

        /// <summary>
        /// Gets or sets the automaton parent for this transition.
        /// </summary>
        public IAutomaton Parent
        {
            get { return this.parent; }
            set { this.parent = value; }
        }

        /// <summary>
        /// Gets the type descriptor for this transition.
        /// </summary>
        public ITransitionDesc Descriptor
        {
            get
            {
                return this.descriptor;
            }

            set
            {
                this.descriptor = value;
                this.Content = this.Descriptor.Create(Source, Dest) as TransitionBase;
                OnPropertyChanged("Descriptor");
            }
        }

        /// <summary>
        /// The name of this transition.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets the type name for this transition.
        /// </summary>
        public string Type
        {
            get { return Descriptor != null ? Descriptor.Name : string.Empty; }
        }

        ///// <summary>
        ///// Gets or sets a value indicating whether this transition re-evaluates itself or not.
        ///// </summary>
        //public bool ReEvaluate
        //{
        //    get { return this.reevaluate; }
        //    set { this.reevaluate = value; }
        //}

        /// <summary>
        /// Gets or sets the content transition that contains the properties for
        /// this transition.
        /// </summary>
        public TransitionBase Content
        {
            get
            {
                return this.content;
            }

            private set
            {
                if (object.ReferenceEquals(value, this.content))
                    return;

                if (this.content != null)
                    this.content.Enabled.PropertyChanged -= this.OnEnabledChanged;

                if (value != null && value.Enabled != null)
                    value.Enabled.PropertyChanged += this.OnEnabledChanged;

                this.content = value;
            }
        }

        /// <summary>
        /// Gets a collection of conditions that are associated with this transition.
        /// </summary>
        public ConditionCollection Conditions
        {
            get { return this.conditions; }
            private set { this.conditions = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Disposes of this current instance.
        /// </summary>
        public void Dispose()
        {
            if (this.Parent == null)
                return;

            //  Remove signal used counts
            List<Signal> usedSignals = GetReferencedSignals();

            if (usedSignals.Count > 0)
            {
                foreach (Signal s in usedSignals)
                {
                    if (s == null)
                    {
                        continue;
                    }

                    s.SetUsed(false);
                }
            }

            this.Parent.Transitions.Remove(this);
        }

        /// <summary>
        /// Gets all the signal variables that are being referenced by this transition as
        /// a list.
        /// </summary>
        /// <returns>
        /// A list of signals being referenced by this transition.
        /// </returns>
        public List<Signal> GetReferencedSignals()
        {
            List<Signal> signals = new List<Signal>();
            foreach (Condition condition in Conditions)
            {
                ICondition content = condition.Content;
                Type type = content.GetType();
                BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;

                PropertyInfo[] properties = type.GetProperties(flags);
                foreach (PropertyInfo pi in properties)
                {
                    if (pi.PropertyType != typeof(SignalConditionProperty))
                        continue;

                    object propertyValue = pi.GetValue(content, null);
                    if (!(propertyValue is SignalConditionProperty))
                        continue;

                    Signal signal = (propertyValue as SignalConditionProperty).Value;
                    if (!signals.Contains(signal))
                        signals.Add(signal);
                }
            }

            return signals;
        }

        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        public void ExportToXML(PargenXmlNode parentNode)
        {
            PargenXmlNode itemNode = parentNode.AppendChild("Item");

            itemNode.AppendValueElementNode("Enabled", "value", Content.Enabled.Value ? "true" : "false");

            itemNode.AppendTextElementNode("From", this.Source.Name);
            itemNode.AppendTextElementNode("To", this.Dest.Name);

            double duration = Math.Max(Content.Duration.Value, 0.0);// Prevent values less than 0.
            itemNode.AppendValueElementNode("Duration", "value", duration.ToString());

            switch (Content.Modifier.Value)
            {
                case Modifier.Linear:
                    itemNode.AppendTextElementNode("Modifier", "MODIFIER_LINEAR");
                    break;
                case Modifier.EaseInOut:
                    itemNode.AppendTextElementNode("Modifier", "MODIFIER_EASE_IN_OUT");
                    break;
                case Modifier.EaseOut:
                    itemNode.AppendTextElementNode("Modifier", "MODIFIER_EASE_OUT");
                    break;
                case Modifier.EaseIn:
                    itemNode.AppendTextElementNode("Modifier", "MODIFIER_EASE_IN");
                    break;
                case Modifier.Step:
                    itemNode.AppendTextElementNode("Modifier", "MODIFIER_STEP");
                    break;
            }

            PargenXmlNode synchronizerNode = itemNode.AppendChild("Synchronizer");
            Content.Sync.ExportToXML(synchronizerNode);

            var filterNode = itemNode.AppendChild("Filter");
            Content.FilterTemplate.ExportToXML(filterNode, null);

            bool isDynamic = false;
            if ((Content.FilterTemplate.Input & PropertyInput.Value) == PropertyInput.Value)
                isDynamic = Content.FilterTemplate.IsDynamic;
            itemNode.AppendValueElementNode("IsFilterDynamic", "value", isDynamic.ToString().ToLower());

            try
            {
                if (Content.Duration.Input == PropertyInput.Real)
                {
                    if (Content.Duration.SelectedSignal == null)
                    {

                    }

                    itemNode.AppendTextElementNode("DurationParameter", Content.Duration.SelectedSignal.Name);
                }
            }
            catch
            {

            }

            if (Content.TransitionWeightOutput.Enabled && Content.TransitionWeightOutput.SelectedSignal != null)
                itemNode.AppendTextElementNode("TransitionWeightOutputParameter", Content.TransitionWeightOutput.SelectedSignal.Name);

            itemNode.AppendValueElementNode("BlockUpdateOnTransition", "value", Content.BlockUpdateOnTransition.Value.ToString().ToLower());

            itemNode.AppendValueElementNode("Transitional", "value", Content.Transitional.Value.ToString().ToLower());
            itemNode.AppendValueElementNode("Immutable", "value", Content.Immutable.Value.ToString().ToLower());

            PargenXmlNode conditionsNode = itemNode.AppendChild("Conditions");
            foreach (var condition in Conditions)
                condition.ExportToXML(conditionsNode);
        }

        /// <summary>
        /// Fires the selection changed event.
        /// </summary>
        /// <param name="action">
        /// Defines whether this transition has been added to the selection or removed from
        /// the selection.
        /// </param>
        /// <param name="count">
        /// The total number of items that are currently selected.
        /// </param>
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            EventHandler<IsSelectedChangedEventArgs> handler = this.IsSelectedChanged;
            if (handler == null)
                return;

            this.IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("transition");
            writer.WriteAttributeString(NameSerialisationTag, this.Name);
            writer.WriteAttributeString(
                IdSerialisationTag, this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Dest != null)
            {
                writer.WriteAttributeString(
                    "Destination", this.Dest.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            if (this.Source != null)
            {
                writer.WriteAttributeString(
                    "Source", this.Source.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            if (this.Content != null)
            {
                this.Content.Serialise(writer);
            }

            writer.WriteStartElement("conditions");
            foreach (Condition condition in this.Conditions)
            {
                condition.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        object ICloneable.Clone()
        {
            return new Transition(this, this.Source, this.Dest);
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerialisationTag, Name);
            info.AddValue(SourceSerialisationTag, Source);
            info.AddValue(DestSerialisationTag, Dest);
            info.AddValue(ConditionsSerialisationTag, Conditions);
            info.AddValue(ContentSerialisationTag, Content);
            info.AddValue(ParentSerialisationTag, Parent);
            info.AddValue(IdSerialisationTag, Id);
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="database">
        /// The database this transition belongs to.
        /// </param>
        private void Deserialise(XmlReader reader, Database database)
        {
            this.Conditions = new ConditionCollection();
            this.Name = reader.GetAttribute(NameSerialisationTag);
            this.Id = Guid.ParseExact(reader.GetAttribute(IdSerialisationTag), "D");

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "base") == 0)
                {
                    string sourceId = reader.GetAttribute("Source");
                    if (!string.IsNullOrEmpty(sourceId))
                    {
                        Guid source = Guid.Parse(sourceId);
                    }

                    string destinationId = reader.GetAttribute("Destination");
                    if (!string.IsNullOrEmpty(destinationId))
                    {
                        Guid destination = Guid.Parse(destinationId);
                    }

                    this.Content = database.XmlFactory.CreateTransitionBase(reader, database);
                }
                else if (string.CompareOrdinal(reader.Name, "conditions") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            Condition newCondition = new Condition(reader, this, database);
                            this.Conditions.Add(newCondition);
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Gets the value of the object inside the specified serialisation info object in the
        /// specified type with the specified name.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value that is being initialised from the serialisation info.
        /// </typeparam>
        /// <param name="tagName">
        /// The name of the property to get from the serialisation info object.
        /// </param>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to get the property.
        /// </param>
        /// <returns>
        /// The value of the object with the specific tag in the specified serialisation info
        /// if found; otherwise the default value for T.
        /// </returns>
        private T GetValue<T>(string tagName, SerializationInfo info)
        {
            if (info == null)
                return default(T);

            try
            {
                object value = info.GetValue(tagName, typeof(T));
                if (value is T)
                    return (T)value;
            }
            catch (SerializationException)
            {
                LogFactory.ApplicationLog.Debug("Deserialisation exception with tag name {0}", tagName);
            }

            return default(T);
        }

        /// <summary>
        /// Fires the property changed event for the property with the specified name. 
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that has changed.
        /// </param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null)
                return;

            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Called when a property on the content enabled property changes.
        /// </summary>
        /// <param name="sender">
        /// The instance of a object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The property changed event data.
        /// </param>
        private void OnEnabledChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(string.Format("Content.Enabled.{0}", e.PropertyName));
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
            this.Content.ResetIds(map);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.AddRange(this.Content.GetAllGuids());
            return ids;
        }
        #endregion
    } // New.Move.Core.Transition
} // New.Move.Core