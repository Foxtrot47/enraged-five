﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml;
using Move.Core.Restorable;
using Move.Utils;
using System.Globalization;

namespace New.Move.Core
{
    /// <summary>
    /// Represents a transitional state machine in a MoVE network.
    /// </summary>
    [Serializable]
    public class StateMachine : RestorableObject, IAutomaton, ITransitional, ITransitionalParent
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="Desc"/> property.
        /// </summary>
        private static StateMachineDesc desc = new StateMachineDesc();

        /// <summary>
        /// The private field for the <see cref="AdditionalText"/> property.
        /// </summary>
        private string additionalText;

        /// <summary>
        /// The private field for the <see cref="Children"/> property.
        /// </summary>
        private NoResetObservableCollection<INode> children;

        /// <summary>
        /// The private field for the <see cref="Database"/> property.
        /// </summary>
        private Database database;

        /// <summary>
        /// The private field for the <see cref="DefaultTransitional"/> property.
        /// </summary>
        private ITransitional defaultTransitional;

        /// <summary>
        /// The private field for the <see cref="Enabled"/> property.
        /// </summary>
        private bool enabled = true;
        
        /// <summary>
        /// The private field for the <see cref="EnterEvent"/> property.
        /// </summary>
        private LogicalTunnelEventProperty enterEvent;
        
        /// <summary>
        /// The private field for the <see cref="ExitEvent"/> property.
        /// </summary>
        private LogicalTunnelEventProperty exitEvent;

        /// <summary>
        /// The private field for the <see cref="DeferBlockUpdate"/> property.
        /// </summary>
        private BoolTransitionProperty deferBlockUpdate;

        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field for the <see cref="Parent"/> property.
        /// </summary>
        private Guid parent;

        /// <summary>
        /// The private field for the <see cref="Position"/> property.
        /// </summary>
        private Point position;

        /// <summary>
        /// The private field for the <see cref="ScrollOffset"/> property.
        /// </summary>
        private Point scrolloffset;

        /// <summary>
        /// The private field for the <see cref="Tag"/> property.
        /// </summary>
        private object tag;

        /// <summary>
        /// The private field for the <see cref="TransitionOrder"/> property.
        /// </summary>
        private TransitionCollection transitionOrder;

        /// <summary>
        /// The private field for the <see cref="Transitions"/> property.
        /// </summary>
        private TransitionCollection transitions;

        /// <summary>
        /// The private field for the <see cref="Zoom"/> property.
        /// </summary>
        private double zoom;

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        public const string NameSerializationTag = @"Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        public const string IdSerializationTag = @"Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Parent"/> property.
        /// </summary>
        public const string ParentSerializationTag = @"Parent";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Position"/> property.
        /// </summary>
        public const string PositionSerializationTag = @"Position";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="ScrollOffset"/> property.
        /// </summary>
        public const string ScrollOffsetSerializationTag = @"ScrollOffset";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Zoom"/> property.
        /// </summary>
        public const string ZoomSerializationTag = @"Zoom";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Default"/> property.
        /// </summary>
        public const string DefaultSerializationTag = @"Default";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Children"/> property.
        /// </summary>
        public const string ChildrenSerializationTag = @"Children";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Transitions"/> property.
        /// </summary>
        public const string TransitionsSerializationTag = @"Transitions";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="TransitionOrder"/> property.
        /// </summary>
        public const string TransitionOrderSerializationTag = @"TransitionOrder";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="EnterEvent"/> property.
        /// </summary>
        public const string EnterEventSerializationTag = @"EnterEvent";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="ExitEvent"/> property.
        /// </summary>
        public const string ExitEventSerializationTag = @"ExitEvent";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="DeferBlockUpdate"/>
        /// property.
        /// </summary>
        public const string DeferBlockUpdateSerializationTag = @"DeferBlockUpdate";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Enabled"/> property.
        /// </summary>
        public const string EnabledSerializationTag = @"Enabled";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Database"/> property.
        /// </summary>
        public const string DatabaseSerializationTag = @"Database";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.StateMachine"/> class.
        /// </summary>
        public StateMachine()
        {
            this.id = Guid.NewGuid();
            this.additionalText = string.Empty;
            this.children = new NoResetObservableCollection<INode>();
            this.enterEvent = new LogicalTunnelEventProperty();
            this.exitEvent = new LogicalTunnelEventProperty();
            this.deferBlockUpdate = new BoolTransitionProperty();
            this.transitions = new TransitionCollection();
            this.transitions.CollectionChanged += OnTransitionsChanged;
            this.transitionOrder = new TransitionCollection();
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.StateMachine"/> class that will belong to the specified
        /// database.
        /// </summary>
        /// <param name="database">
        /// The database this stat machine will belong to.
        /// </param>
        public StateMachine(Database database)
            : this()
        {
            Database = database;
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.StateMachine"/> class that will belong to the specified
        /// database with the specified scrolled offset.
        /// </summary>
        /// <param name="database">
        /// The database this stat machine will belong to.
        /// </param>
        /// <param name="scrolloffset">
        /// The initial scroll offset for this state machine.
        /// </param>
        public StateMachine(Database database, Point scrolloffset)
            : this(database)
        {
            this.scrolloffset = scrolloffset;
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.StateMachine"/> class as
        /// a copy of the specified instance.
        /// </summary>
        /// <param name="name">
        /// The instance to copy.
        /// </param>
        public StateMachine(StateMachine other)
            : this()
        {
            this.Database = other.Database;
            this.Enabled = other.Enabled;
            this.Name = other.Name;
            this.additionalText = other.additionalText;
            this.Position = other.Position;
            this.ScrollOffset = other.ScrollOffset;
            this.Zoom = other.Zoom;
            this.EnterEvent = new LogicalTunnelEventProperty(other.EnterEvent);
            this.ExitEvent = new LogicalTunnelEventProperty(other.ExitEvent);
            this.DeferBlockUpdate = new BoolTransitionProperty(other.DeferBlockUpdate);

            Dictionary<ITransitional, ITransitional> transitionals = new Dictionary<ITransitional, ITransitional>();
            ITransitional defaultTransitional = null;
            foreach (INode child in other.Children)
            {
                INode childClone = child.Clone() as INode;
                if (childClone is Motiontree)
                {
                    this.Children.Add(childClone);
                }
                else
                {
                    this.Add(childClone);
                }

                if (childClone is ITransitional)
                {
                    transitionals.Add(child as ITransitional, childClone as ITransitional);
                    if (other.Default == child)
                    {
                        defaultTransitional = childClone as ITransitional;
                    }
                }
            }
            this.Default = defaultTransitional;

            foreach (Transition transition in other.Transitions)
            {
                if (transition.Source == null || transition.Dest == null)
                {
                    continue;
                }

                if (!transitionals.ContainsKey(transition.Source) && !transitionals.ContainsKey(transition.Dest))
                {
                    continue;
                }

                ITransitional source = null;
                ITransitional destination = null;

                if (transitionals.ContainsKey(transition.Source))
                {
                    source = transitionals[transition.Source];
                }
                else
                {
                    source = transition.Source;
                }

                if (transitionals.ContainsKey(transition.Dest))
                {
                    destination = transitionals[transition.Dest];
                }
                else
                {
                    destination = transition.Dest;
                }

                New.Move.Core.Transition transitionClone = new New.Move.Core.Transition(transition, source, destination);
                this.Transitions.Add(transitionClone);
            }

            // Make sure transition order is kept.
            foreach (KeyValuePair<ITransitional, ITransitional> clonePair in transitionals)
            {
                int orderCount = clonePair.Key.TransitionOrder.Count;
                if (orderCount <= 1)
                {
                    continue;
                }

                string name = null;
                List<Transition> newTransitions = new List<Transition>(clonePair.Value.TransitionOrder);
                clonePair.Value.TransitionOrder.Clear();
                foreach (Transition transition in clonePair.Key.TransitionOrder)
                {
                    name = transition.Name;
                    foreach (Transition newTransition in newTransitions)
                    {
                        if (newTransition.Name != name)
                        {
                            continue;
                        }

                        clonePair.Value.TransitionOrder.Add(newTransition);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.StateMachine"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public StateMachine(SerializationInfo info, StreamingContext context)
        {
            this.AdditionalText = string.Empty;
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case NameSerializationTag:
                        this.Name = (string)entry.Value;
                        break;
                    case IdSerializationTag:
                        this.Id = (Guid)entry.Value;
                        break;
                    case ParentSerializationTag:
                        this.parent = (Guid)entry.Value;
                        break;
                    case DatabaseSerializationTag:
                        this.Database = (Database)entry.Value;
                        break;
                    case PositionSerializationTag:
                        this.Position = (Point)entry.Value;
                        break;
                    case ScrollOffsetSerializationTag:
                        this.ScrollOffset = (Point)entry.Value;
                        break;
                    case ZoomSerializationTag:
                        this.Zoom = (double)entry.Value;
                        break;
                    case DefaultSerializationTag:
                        this.Default =(ITransitional)entry.Value;
                        break;
                    case ChildrenSerializationTag:
                        this.Children = (NoResetObservableCollection<INode>)entry.Value;
                        break;
                    case TransitionsSerializationTag:
                        this.Transitions = (TransitionCollection)entry.Value;
                        break;
                    case TransitionOrderSerializationTag:
                        this.TransitionOrder = (TransitionCollection)entry.Value;
                        break;
                    case EnterEventSerializationTag:
                        this.EnterEvent = (LogicalTunnelEventProperty)entry.Value;
                        break;
                    case ExitEventSerializationTag:
                        this.ExitEvent = (LogicalTunnelEventProperty)entry.Value;
                        break;
                    case EnabledSerializationTag:
                        this.Enabled = (bool)entry.Value;
                        break;
                }
            }

            if (this.Children == null)
            {
                this.Children = new NoResetObservableCollection<INode>();
            }

            if (this.Transitions == null)
            {
                this.Transitions = new TransitionCollection();
            }

            if (this.TransitionOrder == null)
            {
                this.TransitionOrder = new TransitionCollection();
            }

            this.Transitions.CollectionChanged += OnTransitionsChanged;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.StateMachine"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public StateMachine(XmlReader reader, Database database, ITransitional parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Parent = parent == null ? Guid.Empty : parent.Id;
            this.Database = database;
            this.transitions.CollectionChanged -= OnTransitionsChanged;
            this.Deserialise(reader, parent);
            this.transitions.CollectionChanged += OnTransitionsChanged;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the default transitional node changes for this state machine.
        /// </summary>
        public event EventHandler<DefaultChangedEventArgs> DefaultChanged;

        /// <summary>
        /// Occurs when this nodes engaged state has changed.
        /// </summary>
        public event EventHandler<IsEngagedChangedEventArgs> IsEngagedChanged;

        /// <summary>
        /// Occurs when the selection state of this transition has changed.
        /// </summary>
        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the descriptor that describes this state machine.
        /// </summary>
        public static StateMachineDesc Desc 
        { 
            get { return desc; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the update for this motion tree is defered.
        /// </summary>
        public BoolTransitionProperty DeferBlockUpdate
        {
            get { return this.deferBlockUpdate; }
            set { this.deferBlockUpdate = value; }
        }

        /// <summary>
        /// Gets or sets any additional text associated with this state machine.
        /// </summary>
        public string AdditionalText
        {
            get { return this.additionalText; }
            set { this.additionalText = value; }
        }

        /// <summary>
        /// Gets the collection of child nodes currently in this state machine.
        /// </summary>
        public NoResetObservableCollection<INode> Children
        {
            get { return children; }
            private set { children = value; }
        }

        /// <summary>
        /// Gets or sets the current database this state machine belongs to.
        /// </summary>
        public Database Database
        {
            get { return this.database; }
            set { this.database = value; ; }
        }

        /// <summary>
        /// Gets or sets the transitional object that is a child of this state machine and
        /// is set to the default transitional object. 
        /// </summary>
        public ITransitional Default
        {
            get
            {
                return defaultTransitional;
            }

            set
            {
                if (object.ReferenceEquals(value, this.defaultTransitional))
                    return;

                if (this.defaultTransitional != null)
                    this.defaultTransitional.OnDefaultChanged(false);

                defaultTransitional = value;

                if (value != null)
                    value.OnDefaultChanged(true);
            }
        }

        /// <summary>
        /// Gets the transitional descriptor for this state machine.
        /// </summary>
        public ITransitionalDesc Descriptor
        {
            get { return Motiontree.Desc; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this state machine is currently enabled.
        /// </summary>
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        /// <summary>
        /// Gets or sets the event property that defines the enter event for this
        /// state machine.
        /// </summary>
        public LogicalTunnelEventProperty EnterEvent
        {
            get { return this.enterEvent; }
            set { this.enterEvent = value; }
        }

        /// <summary>
        /// Gets or sets the event property that defines the exit event for this state machine.
        /// </summary>
        public LogicalTunnelEventProperty ExitEvent
        {
            get { return this.exitEvent; }
            set { this.exitEvent = value; }
        }

        /// <summary>
        /// Gets or sets the unique global identifier for this state machine.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; ; }
        }

        /// <summary>
        /// Gets a value indicating whether this state machine is a automaton.
        /// </summary>
        public bool IsAutomaton
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets the name for this state machine.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Gets or sets the identifier that references the parent for this state machine.
        /// </summary>
        public Guid Parent
        {
            get { return this.parent; }
            set { this.parent = value; ; }
        }

        /// <summary>
        /// Gets or sets the current position for this state machine.
        /// </summary>
        public Point Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// Gets or sets the current scroll offset for this state machine.
        /// </summary>
        public Point ScrollOffset
        {
            get { return this.scrolloffset; }
            set { this.scrolloffset = value; }
        }

        /// <summary>
        /// Gets or sets the object tag that can be set to the UI component representing
        /// this state machine.
        /// </summary>
        public object Tag
        {
            get { return this.tag; }
            set { this.tag = value; }
        }

        /// <summary>
        /// Gets the collection of transitions that are currently in this state machine.
        /// </summary>
        public TransitionCollection Transitions
        {
            get { return transitions; }
            private set { transitions = value; }
        }

        /// <summary>
        /// Gets or sets the collection of transitions in order for this state machine.
        /// </summary>
        public TransitionCollection TransitionOrder
        {
            get { return transitionOrder; }
            set { transitionOrder = value; }
        }

        /// <summary>
        /// Gets the string type for this motion tree.
        /// </summary>
        public string Type
        {
            get { return Desc.Name; }
        }

        /// <summary>
        /// Gets or sets the current zoom factor for this state machine.
        /// </summary>
        public double Zoom
        {
            get { return this.zoom; }
            set { this.zoom = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a node to this state machines child collection.
        /// </summary>
        /// <param name="node">
        /// The node to add to this static machine.
        /// </param>
        public void Add(INode node)
        {
            //  Changed this from an assert to just returning without adding.  This is so we can drag and drop in the control parameter editor to re-order.
            if (!(node is ITransitional))
            {
                return;
            }
            //  When cutting and pasting between instances of MoVE then the database
            //  of the node needs to be set to the current database.
            //Debug.Assert(node.Database == Database);
            node.Database = Database;

            ITransitional transitional = node as ITransitional;
            if (transitional == null || this.Children.Contains(transitional))
                return;

            transitional.Parent = this.Id;
            this.Children.Add(transitional);

            if (this.Default == null)
                this.Default = transitional;
        }

        /// <summary>
        /// Adds this given handler to the child collection changed event.
        /// </summary>
        /// <param name="handler">
        /// The handler to add to the children changed event.
        /// </param>
        public void AddHandler(NotifyCollectionChangedEventHandler handler)
        {
            Children.CollectionChanged += handler;
        }

        /// <summary>
        /// Disposes of this current instance.
        /// </summary>
        public void Dispose()
        {
            if (this.Parent != Guid.Empty && this.Database != null)
            {
                ITransitional parent = Database.Find(Parent) as ITransitional;
                if (parent != null)
                    parent.Remove(this);
            }

            if (this.Database != null)
                this.Database.Nodes.Remove(this);

            if (this.Children == null)
                return;

            INode[] childArray = new INode[this.Children.Count];
            this.Children.CopyTo(childArray, 0);
            foreach (INode child in childArray)
            {
                child.Dispose();
            }

            this.Children.Clear();
        }

        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        /// <param name="hashedNames">
        /// A collection of hashed names to use during the export process.
        /// </param>
        public void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            hashedNames.Add(Name);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "StateMachineNode");

            itemNode.AppendTextElementNode("Name", Name);
            itemNode.AppendTextElementNode("Initial", Default.Name);

            var enterEventNode = itemNode.AppendChild("OnEnterEvent");
            EnterEvent.ExportToXML(enterEventNode);

            var exitEventNode = itemNode.AppendChild("OnExitEvent");
            ExitEvent.ExportToXML(exitEventNode);

            var childrenNode = itemNode.AppendChild("Children");
            foreach (var childNode in Children)
                childrenNode.AppendTextElementNode("Item", childNode.Name);

            var transitionsNode = itemNode.AppendChild("Transitions");
            foreach (var transition in TransitionOrder)
                transition.ExportToXML(transitionsNode);

            itemNode.AppendValueElementNode("DeferBlockUpdate", "value", DeferBlockUpdate.Value.ToString().ToLower());
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("statemachine");

            writer.WriteAttributeString(
                ZoomSerializationTag, this.Zoom.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                IdSerializationTag, this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(NameSerializationTag, this.Name);
            if (this.Default != null)
            {
                writer.WriteAttributeString(
                    DefaultSerializationTag,
                    this.Default.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            if (this.DeferBlockUpdate != null)
            {
                writer.WriteStartElement(DeferBlockUpdateSerializationTag);
                this.DeferBlockUpdate.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.EnterEvent != null)
            {
                writer.WriteStartElement(EnterEventSerializationTag);
                this.EnterEvent.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ExitEvent != null)
            {
                writer.WriteStartElement(ExitEventSerializationTag);
                this.ExitEvent.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Position != null)
            {
                writer.WriteStartElement(PositionSerializationTag);
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.ScrollOffset != null)
            {
                writer.WriteStartElement(ScrollOffsetSerializationTag);
                writer.WriteAttributeString(
                    "x", this.ScrollOffset.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.ScrollOffset.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            writer.WriteStartElement(TransitionsSerializationTag);
            foreach (Transition transistion in this.Transitions)
            {
                transistion.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteStartElement(ChildrenSerializationTag);
            foreach (INode node in this.Children)
            {
                node.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteStartElement(TransitionOrderSerializationTag);
            foreach (Transition transition in this.TransitionOrder)
            {
                writer.WriteStartElement("transition");
                writer.WriteAttributeString(
                    "Id", transition.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void SerialiseSet(
            XmlWriter writer,
            IEnumerable<INode> childrenSet,
            IEnumerable<Transition> tranistionSet,
            bool writeTransitionOrder)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("statemachine");

            writer.WriteAttributeString(
                ZoomSerializationTag, this.Zoom.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                IdSerializationTag, this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(NameSerializationTag, this.Name);
            if (this.EnterEvent != null)
            {
                writer.WriteStartElement(EnterEventSerializationTag);
                this.EnterEvent.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.DeferBlockUpdate != null)
            {
                writer.WriteStartElement(DeferBlockUpdateSerializationTag);
                this.DeferBlockUpdate.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ExitEvent != null)
            {
                writer.WriteStartElement(ExitEventSerializationTag);
                this.ExitEvent.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Position != null)
            {
                writer.WriteStartElement(PositionSerializationTag);
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.ScrollOffset != null)
            {
                writer.WriteStartElement(ScrollOffsetSerializationTag);
                writer.WriteAttributeString(
                    "x", this.ScrollOffset.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.ScrollOffset.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            writer.WriteStartElement(TransitionsSerializationTag);
            foreach (Transition transistion in this.Transitions)
            {
                if (tranistionSet.Contains(transistion))
                {
                    transistion.Serialise(writer);
                }
            }
            writer.WriteEndElement();

            writer.WriteStartElement(ChildrenSerializationTag);
            foreach (INode node in this.Children)
            {
                if (childrenSet.Contains(node))
                {
                    node.Serialise(writer);
                }
            }
            writer.WriteEndElement();

            writer.WriteStartElement(TransitionOrderSerializationTag);
            if (writeTransitionOrder)
            {
                foreach (Transition transition in this.TransitionOrder)
                {
                    writer.WriteStartElement("transition");
                    writer.WriteAttributeString(
                        "Id", transition.Id.ToString("D", CultureInfo.InvariantCulture));
                    writer.WriteEndElement();
                }
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerializationTag, Name);
            info.AddValue(IdSerializationTag, Id);
            info.AddValue(ParentSerializationTag, Parent);
            info.AddValue(DatabaseSerializationTag, Database);
            info.AddValue(PositionSerializationTag, Position);
            info.AddValue(ScrollOffsetSerializationTag, ScrollOffset);
            info.AddValue(ZoomSerializationTag, Zoom);
            info.AddValue(DefaultSerializationTag, Default);
            info.AddValue(ChildrenSerializationTag, Children);
            info.AddValue(TransitionsSerializationTag, Transitions);
            info.AddValue(TransitionOrderSerializationTag, TransitionOrder);
            info.AddValue(EnterEventSerializationTag, EnterEvent);
            info.AddValue(ExitEventSerializationTag, ExitEvent);
            info.AddValue(EnabledSerializationTag, Enabled);
        }

        /// <summary>
        /// Gets called after this node has been loaded and is added to a database.
        /// </summary>
        public void OnLoadComplete()
        {
        }

        /// <summary>
        /// Fires the selection changed event.
        /// </summary>
        /// <param name="action">
        /// Defines whether this transition has been added to the selection or removed from
        /// the selection.
        /// </param>
        /// <param name="count">
        /// The total number of items that are currently selected.
        /// </param>
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            EventHandler<IsSelectedChangedEventArgs> handler = this.IsSelectedChanged;
            if (handler == null)
                return;

            this.IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, ITransitional parent)
        {
            if (reader == null)
            {
                return;
            }

            this.zoom = double.Parse(
                reader.GetAttribute(ZoomSerializationTag), CultureInfo.InvariantCulture);
            this.name = reader.GetAttribute(NameSerializationTag);
            this.id = Guid.ParseExact(reader.GetAttribute(IdSerializationTag), "D");
            string defaultId = reader.GetAttribute(DefaultSerializationTag);
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            Dictionary<Transition, Guid> sources = new Dictionary<Transition, Guid>();
            Dictionary<Transition, Guid> destinations = new Dictionary<Transition, Guid>();
            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, DeferBlockUpdateSerializationTag) == 0)
                {
                    this.DeferBlockUpdate = new BoolTransitionProperty(reader, this.Database, null);
                }
                else if (string.CompareOrdinal(reader.Name, EnterEventSerializationTag) == 0)
                {
                    this.EnterEvent = new LogicalTunnelEventProperty(reader, this.Database);
                }
                else if (string.CompareOrdinal(reader.Name, ExitEventSerializationTag) == 0)
                {
                    this.ExitEvent = new LogicalTunnelEventProperty(reader, this.Database);
                }
                else if (string.CompareOrdinal(reader.Name, PositionSerializationTag) == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, ScrollOffsetSerializationTag) == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.ScrollOffset = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, TransitionsSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            string sourceId = reader.GetAttribute("Source");
                            string destId = reader.GetAttribute("Destination");

                            Transition newTransition = new Transition(reader, this.Database, this);

                            this.Transitions.Add(newTransition);
                            if (sourceId != null)
                            {
                                sources.Add(newTransition, Guid.Parse(sourceId));
                            }

                            if (destId != null)
                            {
                                destinations.Add(newTransition, Guid.Parse(destId));
                            }
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }     
                }
                else if (string.CompareOrdinal(reader.Name, ChildrenSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        Dictionary<Guid, INode> map = new Dictionary<Guid, INode>();
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            INode newNode = this.Database.XmlFactory.CreateNode(reader, this.Database, this);
                            if (newNode != null)
                            {
                                map.Add(newNode.Id, newNode);
                                this.Children.Add(newNode);
                            }
                        }

                        reader.Skip();
                        foreach (KeyValuePair<Transition, Guid> kvp in sources)
                        {
                            INode source = null;
                            if (map.TryGetValue(kvp.Value, out source))
                            {
                                kvp.Key.Source = source as ITransitional;
                            }
                        }

                        foreach (KeyValuePair<Transition, Guid> kvp in destinations)
                        {
                            INode destination = null;
                            if (map.TryGetValue(kvp.Value, out destination))
                            {
                                kvp.Key.Dest = destination as ITransitional;
                            }
                        }

                        if (!string.IsNullOrEmpty(defaultId))
                        {
                            INode defaultNode = null;
                            if (map.TryGetValue(Guid.ParseExact(defaultId, "D"), out defaultNode))
                            {
                                this.Default = defaultNode as ITransitional;
                            }
                        }
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, TransitionOrderSerializationTag) == 0)
                { 
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            Guid id = Guid.Parse(reader.GetAttribute("Id"));
                            if (parent is ITransitionalParent)
                            {
                                Transition transition = (parent as ITransitionalParent).GetTransition(id);
                                if (transition != null)
                                {
                                    this.TransitionOrder.Add(transition);
                                }
                            }

                            reader.Skip();
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Get called whenever the transition in the state machine change. Makes sure that
        /// the order is kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The observable collection that changed.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnTransitionsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this.children == null || this.transitionOrder == null)
                return;

            if (e.Action == NotifyCollectionChangedAction.Remove && e.OldItems != null)
            {
                foreach (Transition transition in e.OldItems.Cast<Transition>())
                {
                    if (!this.children.Contains(transition.Source))
                        continue;

                    transition.Source.TransitionOrder.Remove(transition);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems != null)
            {
                foreach (Transition transition in e.NewItems.Cast<Transition>())
                {
                    if (!this.children.Contains(transition.Source))
                        continue;

                    transition.Source.TransitionOrder.Add(transition);
                }
            }
        }

        /// <summary>
        /// Called when this transitional node becomes or stops being the default
        /// transitional node for its parent.
        /// </summary>
        /// <param name="isDefault">
        /// A value indicating whether this node is the default transitional node or not.
        /// </param>
        public void OnDefaultChanged(bool isDefault)
        {
            EventHandler<DefaultChangedEventArgs> handler = this.DefaultChanged;
            if (handler == null)
                return;

            this.DefaultChanged(this, new DefaultChangedEventArgs(isDefault));
        }

        /// <summary>
        /// Called when this nodes engaged state has changed.
        /// </summary>
        /// <param name="action">
        /// The action associated with the change.
        /// </param>
        public void OnEngagedChanged(EngagedChangedAction action)
        {
            EventHandler<IsEngagedChangedEventArgs> handler = this.IsEngagedChanged;
            if (handler == null)
                return;

            this.IsEngagedChanged(this, new IsEngagedChangedEventArgs(action));
        }

        /// <summary>
        /// Removes this given handler to the child collection changed event.
        /// </summary>
        /// <param name="handler">
        /// The handler to remove to the children changed event.
        /// </param>
        public void RemoveHandler(NotifyCollectionChangedEventHandler handler)
        {
            Children.CollectionChanged -= handler;
        }

        /// <summary>
        /// Removes a node from this state machines child collection.
        /// </summary>
        /// <param name="node">
        /// The node to remove from this static machine.
        /// </param>
        public void Remove(INode node)
        {
            Debug.Assert(node is ITransitional);
            //  When cutting and pasting between instances of MoVE then the database
            //  of the node needs to be set to the current database.
            //Debug.Assert(node.Database == Database);
            node.Database = Database;

            ITransitional transitional = node as ITransitional;
            if (transitional == null || !this.Children.Contains(transitional))
                return;

            bool newDefault = false;
            if (object.ReferenceEquals(this.Default, transitional))
                newDefault = true;

            transitional.Parent = Guid.Empty;
            this.Children.Remove(transitional);

            List<Transition> invalidTransitions = new List<Transition>();
            foreach (Transition transition in this.Transitions)
            {
                if (object.ReferenceEquals(transition.Source, transitional))
                {
                    invalidTransitions.Add(transition);
                }
                else if (object.ReferenceEquals(transition.Dest, transitional))
                {
                    invalidTransitions.Add(transition);
                }
            }
            foreach (Transition transition in invalidTransitions)
                this.Transitions.Remove(transition);

            if (newDefault)
                this.Default = this.Children.FirstOrDefault() as ITransitional;
        }

        /// <summary>
        /// Gets the string representation of this instance.
        /// </summary>
        /// <returns>
        /// The string representation of this instance.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Gets the transition inside this state machine that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the transition to retrieve.
        /// </param>
        /// <returns>
        /// The transition with the specified id if found; otherwise false.
        /// </returns>
        public Transition GetTransition(Guid id)
        {
            foreach (Transition transition in this.Transitions)
            {
                if (transition.Id == id)
                {
                    return transition;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        object ICloneable.Clone()
        {
            return new StateMachine(this);
        }

        /// <summary>
        /// Copies this item and the speciifed child items by serialising them out to the
        /// specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the specified items get serialised out to.
        /// </param>
        /// <param name="items">
        /// The child items that should be serialised to the specified writer.
        /// </param>
        public void Copy(XmlWriter writer, IEnumerable<ISelectable> items)
        {
            writer.WriteStartElement(TransitionsSerializationTag);
            foreach (Transition transistion in this.Transitions)
            {
                if (!items.Contains(transistion))
                {
                    continue;
                }

                transistion.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteStartElement(ChildrenSerializationTag);
            foreach (INode node in this.Children)
            {
                if (!items.Contains(node))
                {
                    continue;
                }

                node.Serialise(writer);
            }

            writer.WriteEndElement();

            writer.WriteStartElement("Ordering");
            foreach (INode node in this.Children)
            {
                if (!items.Contains(node))
                {
                    continue;
                }

                ITransitional transitional = node as ITransitional;
                if (transitional != null)
                {
                    writer.WriteStartElement("Order");
                    writer.WriteAttributeString("Id", node.Id.ToString("D"));

                    foreach (Transition transition in transitional.TransitionOrder)
                    {
                        if (!items.Contains(transition))
                        {
                            continue;
                        }

                        writer.WriteStartElement("Transition");
                        writer.WriteAttributeString("Id", transition.Id.ToString("D"));
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Pastes the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The reader containing the data to paste.
        /// </param>
        /// <param name="offset">
        /// A offset point that determines the position offset for the child objects.
        /// </param>
        public void Paste(XmlReader reader, Point offset, bool cutting)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            List<INode> children = new List<INode>();
            List<INode> newTransitionals = new List<INode>();
            List<Transition> transitions = new List<Transition>();
            Dictionary<Transition, Guid> sources = new Dictionary<Transition, Guid>();
            Dictionary<Transition, Guid> destinations = new Dictionary<Transition, Guid>();
            Dictionary<Guid, List<Guid>> orderings = new Dictionary<Guid, List<Guid>>();
            Dictionary<Guid, INode> map = new Dictionary<Guid, INode>();
            foreach (INode child in this.Children)
            {
                map.Add(child.Id, child);
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals(TransitionsSerializationTag, reader.Name))
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            string sourceId = reader.GetAttribute("Source");
                            string destId = reader.GetAttribute("Destination");
                            Transition newTransition = new Transition(reader, this.Database, this);
                            transitions.Add(newTransition);
                            if (sourceId != null)
                            {
                                sources.Add(newTransition, Guid.Parse(sourceId));
                            }

                            if (destId != null)
                            {
                                destinations.Add(newTransition, Guid.Parse(destId));
                            }
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (String.Equals(ChildrenSerializationTag, reader.Name))
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            INode newNode = this.Database.XmlFactory.CreateNode(reader, this.Database, this);
                            newNode.Position = new Point(newNode.Position.X + offset.X, newNode.Position.Y + offset.Y);
                            if (!cutting)
                            {
                                this.Database.MakeNodeNameUnique(newNode);
                            }

                            this.Database.Nodes.Add(newNode);
                            ITransitional newTransitional = newNode as ITransitional;
                            if (newTransitional != null)
                            {
                                newTransitionals.Add(newTransitional);
                                newTransitional.TransitionOrder.Clear();
                            }

                            if (newNode != null)
                            {
                                children.Add(newNode);
                                map[newNode.Id] = newNode;
                            }
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (String.Equals("Ordering", reader.Name))
                {
                    reader.ReadStartElement();
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        if (String.Equals("Order", reader.Name))
                        {
                            string idString = reader.GetAttribute("Id");
                            List<Guid> orders = new List<Guid>();
                            orderings.Add(Guid.ParseExact(idString, "D"), orders);
                            if (reader.IsEmptyElement)
                            {
                                reader.Skip();
                                continue;
                            }

                            reader.ReadStartElement();
                            while (reader.MoveToContent() == XmlNodeType.Element)
                            {
                                if (String.Equals("Transition", reader.Name))
                                {
                                    string transitionIdString = reader.GetAttribute("Id");
                                    orders.Add(Guid.ParseExact(transitionIdString, "D"));
                                }

                                reader.Skip();
                            }

                            if (reader.MoveToContent() == XmlNodeType.EndElement)
                            {
                                reader.ReadEndElement();
                            }
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }

                    reader.Skip();
                }
                else
                {
                    reader.Skip();
                }
            }

            foreach (KeyValuePair<Transition, Guid> kvp in sources)
            {
                INode source = null;
                if (map.TryGetValue(kvp.Value, out source))
                {
                    kvp.Key.Content.Source = source as ITransitional;
                    kvp.Key.Source = source as ITransitional;
                }
            }

            foreach (KeyValuePair<Transition, Guid> kvp in destinations)
            {
                INode destination = null;
                if (map.TryGetValue(kvp.Value, out destination))
                {
                    kvp.Key.Content.Dest = destination as ITransitional;
                    kvp.Key.Dest = destination as ITransitional;
                }
            }

            List<Guid> ids = new List<Guid>();
            foreach (INode child in children)
            {
                ids.AddRange(child.GetAllGuids());
            }

            foreach (Transition transition in transitions)
            {
                ids.AddRange(transition.GetAllGuids());
            }

            Dictionary<Guid, Guid> idMap = new Dictionary<Guid, Guid>();
            foreach (Guid id in ids)
            {
                if (id == Guid.Empty)
                {
                    continue;
                }

                if (idMap.ContainsKey(id))
                {
                    continue;
                }

                idMap.Add(id, Guid.NewGuid());
            }

            foreach (INode child in children)
            {
                child.Parent = this.Id;
                child.ResetIds(idMap);
                this.Children.Add(child);
            }

            foreach (Transition transition in transitions)
            {
                if (transition.Dest == null || transition.Content.Dest == null)
                {
                    continue;
                }

                if (transition.Source == null || transition.Content.Source == null)
                {
                    continue;
                }

                transition.ResetIds(idMap);
                this.Transitions.Add(transition);
            }

            foreach (ITransitional newTransitional in newTransitionals)
            {
                newTransitional.TransitionOrder.Clear();
                List<Guid> realOrder = null;
                Guid previousId = newTransitional.Id;
                foreach (KeyValuePair<Guid, Guid> kvp in idMap)
                {
                    if (kvp.Value == newTransitional.Id)
                    {
                        previousId = kvp.Key;
                        break;
                    }
                }

                if (!orderings.TryGetValue(previousId, out realOrder))
                {
                    continue;
                }

                foreach (Guid transitionId in realOrder)
                {
                    Guid newId = transitionId;
                    if (!idMap.TryGetValue(transitionId, out newId))
                    {
                        newId = transitionId;
                    }

                    Transition transition = this.GetTransition(newId);
                    if (transition != null)
                    {
                        newTransitional.TransitionOrder.Add(transition);
                    }
                }
            }

            if (this.Default == null)
            {
                if (this.Children.Count > 0 && this.Children[0] is ITransitional)
                {
                    this.Default = this.Children[0] as ITransitional;
                }
            }
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
            foreach (Transition transition in this.Transitions)
            {
                transition.Id = Database.GetId(map, transition.Id);
            }

            foreach (INode child in this.Children)
            {
                child.Parent = this.Id;
                child.ResetIds(map);
            }
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            foreach (Transition transition in this.Transitions)
            {
                ids.Add(transition.Id);
            }

            foreach (INode node in this.Children)
            {
                ids.AddRange(node.GetAllGuids());
            }

            return ids;
        }
        #endregion
    } // New.Move.Core.StateMachine
} // New.Move.Core