﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;

namespace Move.Core.Restorable
{
    internal sealed class WithoutCollectionChanged : IDisposable
    {
        internal WithoutCollectionChanged(IList list, NotifyCollectionChangedEventHandler handler)
        {
            if (list is INotifyCollectionChanged)
            {
                _List = list as INotifyCollectionChanged;

                Type type = _List.GetType();
                FieldInfo fi = null;

                do
                {
                    fi = type.GetField("CollectionChanged", BindingFlags.Instance | BindingFlags.NonPublic);
                    type = type.BaseType;
                }
                while (fi == null && type != typeof(object));

                if (fi != null)
                {
                    object obj = fi.GetValue(_List);
                    if (obj is NotifyCollectionChangedEventHandler)
                    {
                        //_Handler = (NotifyCollectionChangedEventHandler)obj;
                        //_List.CollectionChanged -= _Handler;
                        _Handler = handler;
                        _List.CollectionChanged -= handler;
                    }
                }
            }
        }

        public void Dispose()
        {
            if (_List != null)
            {
                _List.CollectionChanged += _Handler;
            }
        }

        INotifyCollectionChanged _List;
        NotifyCollectionChangedEventHandler _Handler;
    }
}