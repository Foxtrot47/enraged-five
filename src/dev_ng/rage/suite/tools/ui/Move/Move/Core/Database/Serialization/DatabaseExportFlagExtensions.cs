﻿namespace New.Move.Core
{
    public static class DatabaseExportFlagExtensions
    {
        static class Const
        {
            public static uint Ignored = 0;
            public static uint Value = 1;
            public static uint Signal = 2;
            public static uint Callback = 3;

        }

        public static uint GetExportFlag(this AnimationLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.Animation:
                    return Const.Signal;
                case PropertyInput.Filename:
                    return Const.Value;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this BooleanLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.Boolean:
                    return Const.Signal;
                case PropertyInput.Value:
                    return Const.Value;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this ClipLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.Clip:
                    return Const.Signal;
                case PropertyInput.Filename:
                    return Const.Value;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this ExpressionsLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.Expressions:
                    return Const.Signal;
                case PropertyInput.Filename:
                    return Const.Value;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this FilterLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.Filter:
                    return Const.Signal;
                case PropertyInput.Value:
                    return Const.Value;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this FilterNLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.Filter:
                    return Const.Signal;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this FrameLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.Frame:
                    return Const.Signal;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this ParameterizedMotionLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.ParameterizedMotion:
                    return Const.Signal;
                case PropertyInput.Filename:
                    return Const.Value;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this RealLogicalProperty source)
        {
            switch (source.Input)
            {
                case PropertyInput.Real:
                    return Const.Signal;
                case PropertyInput.Value:
                    return Const.Value;
            }

            return Const.Ignored;
        }

        public static uint GetExportFlag(this SyncConditionProperty source)
        {
            switch(source.Input)
            {
                case SyncType.Phase:
                    return 0;
                case SyncType.Tag:
                    return 1;
                case SyncType.None:
                default:
                    return 2;
            }
        }
    }
}
