﻿namespace New.Move.Core
{
    public interface IConditionDesc
    {
        string Name { get; }
        int ListPosition { get; }

        ICondition Create();
    }
}