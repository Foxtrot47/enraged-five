﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class RequestConditionProperty : IConditionProperty, ISerializable
    {
        public RequestConditionProperty()
        {
            Id = Guid.NewGuid();
        }

        public RequestConditionProperty(RequestConditionProperty other)
        {
            Id = Guid.NewGuid();
            this.Name = other.Name;
            this.Value = other.Value;
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        private Request Value_ = null;
        public Request Value
        {
            get { return Value_; }
            set
            {
                if (object.ReferenceEquals(value, this.Value_))
                {
                    return;
                }

                if (value == null && this.Value_ != null)
                {
                    this.Value_.SetUsed(false);
                }

                this.Value_ = value;
                if (value != null)
                {
                    this.Value_.SetUsed(true);
                }
            }
        }
        public Condition Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag 
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Value = "Value";
            public static string Parent = "Parent";
        }

        public RequestConditionProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Value = (Request)info.GetValue(SerializationTag.Value, typeof(Request));
            Parent = (Condition)info.GetValue(SerializationTag.Parent, typeof(Condition));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Value, Value);
            info.AddValue(SerializationTag.Parent, Parent);
        }

        public object Clone()
        {
            return new RequestConditionProperty(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RequestConditionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public RequestConditionProperty(XmlReader reader, Database database, Condition parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            string requestId = reader.GetAttribute("Value");
            if (!string.IsNullOrEmpty(requestId))
            {
                this.Value = database.GetRequest(Guid.ParseExact(requestId, "D"));
                if (this.Value == null)
                {
                    string requestName = reader.GetAttribute("RequestName");
                    this.Value = database.GetRequest(requestName);
                    if (this.Value == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to request with id '{0}'. This needs fixing before a export is allowed.", requestId);
                    }
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Value != null)
            {
                writer.WriteAttributeString(
                    "Value", this.Value.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("RequestName", this.Value.Name);
            }
        }
    }
}