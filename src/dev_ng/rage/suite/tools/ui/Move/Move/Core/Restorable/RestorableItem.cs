﻿using System;

namespace Move.Core.Restorable
{
    [Serializable]
    internal class RestorableItem
    {
        internal string Name;
        internal RestorableObject Parent;

        object _Value;
        internal object Value
        {
            get
            {
                return _Value;
            }
            set
            {
                if ((_Value != null && !_Value.Equals(value)) || (_Value == null && value != null))
                {
                    OnValueChanging();
                    _Value = value;
                    OnValueChanged();
                }
            }
        }

        internal event EventHandler ValueChanging;
        void OnValueChanging()
        {
            if (ValueChanging != null)
                ValueChanging(Parent, EventArgs.Empty);
        }

        internal event EventHandler ValueChanged;
        void OnValueChanged()
        {
            if (ValueChanged != null)
                ValueChanged(Parent, EventArgs.Empty);
        }
    }
}