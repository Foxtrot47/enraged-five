﻿using System;
using System.Collections.ObjectModel;

namespace New.Move.Core
{
    public class ClearableCollection<T> : ObservableCollection<T>
    {
        public event EventHandler<EventArgs> Clearing;
        protected virtual void OnClearing()
        {
            if (Clearing != null)
                Clearing(this, EventArgs.Empty);
        }

        protected override void ClearItems()
        {
            OnClearing();
            base.ClearItems();
        }
    }
}