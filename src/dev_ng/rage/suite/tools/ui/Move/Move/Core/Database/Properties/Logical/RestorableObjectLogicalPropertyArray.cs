﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class RestorableObjectLogicalPropertyArray : LogicalPropertyArray<RestorableObjectLogicalProperty>, ISerializable, IConvertible
    {
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public RestorableObjectLogicalPropertyArray(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public RestorableObjectLogicalPropertyArray()
            : base()
        {
        }

        public RestorableObjectLogicalPropertyArray(ILogic parent)
        {
            Parent = parent;
        }

        public TypeCode GetTypeCode()
        {
            return TypeCode.Object;
        }

        bool IConvertible.ToBoolean(IFormatProvider provider)
        {
            return false;
        }

        double GetDoubleValue()
        {
            return 0.0;
        }

        byte IConvertible.ToByte(IFormatProvider provider)
        {
            return Convert.ToByte(GetDoubleValue());
        }

        char IConvertible.ToChar(IFormatProvider provider)
        {
            return Convert.ToChar(GetDoubleValue());
        }

        DateTime IConvertible.ToDateTime(IFormatProvider provider)
        {
            return Convert.ToDateTime(GetDoubleValue());
        }

        decimal IConvertible.ToDecimal(IFormatProvider provider)
        {
            return Convert.ToDecimal(GetDoubleValue());
        }

        double IConvertible.ToDouble(IFormatProvider provider)
        {
            return GetDoubleValue();
        }

        short IConvertible.ToInt16(IFormatProvider provider)
        {
            return Convert.ToInt16(GetDoubleValue());
        }

        int IConvertible.ToInt32(IFormatProvider provider)
        {
            return Convert.ToInt32(GetDoubleValue());
        }

        long IConvertible.ToInt64(IFormatProvider provider)
        {
            return Convert.ToInt64(GetDoubleValue());
        }

        sbyte IConvertible.ToSByte(IFormatProvider provider)
        {
            return Convert.ToSByte(GetDoubleValue());
        }

        float IConvertible.ToSingle(IFormatProvider provider)
        {
            return Convert.ToSingle(GetDoubleValue());
        }

        string IConvertible.ToString(IFormatProvider provider)
        {
            return String.Empty;
        }

        object IConvertible.ToType(Type conversionType, IFormatProvider provider)
        {
            if (conversionType == typeof(RestorableObjectLogicalPropertyArray))
            {
                RestorableObjectLogicalPropertyArray array = new RestorableObjectLogicalPropertyArray();

                foreach (RestorableObjectLogicalProperty wtrans in Items)
                {
                    RestorableObjectLogicalProperty wftrans = new RestorableObjectLogicalProperty();
                    wftrans.Parent = wtrans.Parent;
                    wftrans.Id = wtrans.Id;
                    wftrans.Name = wtrans.Name;
                    
                    array.Items.Add(wftrans);
                }

                return array;
            }

            return Convert.ChangeType(GetDoubleValue(), conversionType);
        }

        ushort IConvertible.ToUInt16(IFormatProvider provider)
        {
            return Convert.ToUInt16(GetDoubleValue());
        }

        uint IConvertible.ToUInt32(IFormatProvider provider)
        {
            return Convert.ToUInt32(GetDoubleValue());
        }

        ulong IConvertible.ToUInt64(IFormatProvider provider)
        {
            return Convert.ToUInt64(GetDoubleValue());
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }
           
        }

        public override object Clone()
        {
            RestorableObjectLogicalPropertyArray clone = new RestorableObjectLogicalPropertyArray();
            clone.Name = this.Name;
            clone.Parent = this.Parent;
            foreach (var item in this.Items)
            {
                RestorableObjectLogicalProperty newItem = item.Clone() as RestorableObjectLogicalProperty;
                clone.Items.Add(newItem);
            }
            return clone;
        }
    }
}
