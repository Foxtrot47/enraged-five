﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace New.Move.Core
{
    [Serializable]
    public class MotiontreeDesc : ITransitionalDesc
    {
        static class Const
        {
            public static string Name = "Motiontree";
            public static int Id = 0;
            public static string Help = "Motiontree";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get; private set; }

        public Type ConstructType { get { return typeof(Motiontree); } }

        public MotiontreeDesc()
        {
            try
            {
                BitmapImage icon = new BitmapImage();
                icon.BeginInit();
                icon.StreamSource =
                    System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Core.Images.PaletteIcon_MotionTree.png");
                icon.EndInit();

                Icon = icon;
            }
            catch
            {
                Icon = null;
            }
        }

        public ITransitional Create(Database database, Point offset)
        {
            return new Motiontree(database, offset);
        }

        public INode Create(Database database)
        {
            return new Motiontree(database);
        }
    }
}