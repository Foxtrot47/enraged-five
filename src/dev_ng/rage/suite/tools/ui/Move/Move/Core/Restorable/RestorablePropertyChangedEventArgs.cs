﻿using System;

namespace Move.Core.Restorable
{
    public class RestorablePropertyChangedEventArgs : EventArgs
    {
        internal RestorablePropertyChangedEventArgs(IRestorableAction action)
        {
            Action = action;
        }

        public IRestorableAction Action { get; private set; }
    }
}