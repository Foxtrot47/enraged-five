﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

using Move.Core.Restorable;

namespace New.Move.Core
{
    public class ConditionSurrogateSelector : ISerializationSurrogate, ISurrogateSelector
    {
        static class Const
        {
            public static string Descriptor = "Descriptor";
            public static string Content = "Content";
            public static string Parent = "Parent";
            public static string FlagWhenPassed = "FlagWhenPassed";
        }

        public ConditionSurrogateSelector(ICoreDataModel model)
        {
            DataModel = model;
        }

        ICoreDataModel DataModel { get; set; }

        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            Condition condition = obj as Condition;

            info.AddValue(Const.Content, condition.Content);
            info.AddValue(Const.Parent, condition.Parent);
            info.AddValue(Const.FlagWhenPassed, condition.FlagWhenPassed);
        }

        private bool IsKnown(Type type)
        {
            return type == typeof(string) || type.IsPrimitive || type.IsSerializable;
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            Condition condition = obj as Condition;
            condition.Properties = new RestorableCollection();
            RestorableProperty.CreateItems(condition);

            condition.Content = (ICondition)info.GetValue(Const.Content, typeof(ICondition));
            condition.Parent = (Transition)info.GetValue(Const.Parent, typeof(Transition));

            condition.Descriptor = DataModel.ConditionDescriptors.First(
                delegate(IConditionDesc desc)
                {
                    return desc.GetType() == condition.Content.Descriptor;
                }
            );

            try
            {
                condition.FlagWhenPassed = info.GetBoolean(Const.FlagWhenPassed);
            }
            catch (Exception)
            {
                condition.FlagWhenPassed = false;
            }

            RestorablePropertyDescriptor descritorDescriptor =
                RestorablePropertyDescriptor.FromProperty(Condition.DescriptorProperty, typeof(Condition));
            descritorDescriptor.AddValueChanged(condition, condition.Descriptor_ValueChanged);

            RestorablePropertyDescriptor contentDescriptor = RestorablePropertyDescriptor.FromProperty(Condition.ContentProperty, typeof(Condition));
            contentDescriptor.AddValueChanged(condition, condition.Content_ValueChanged);

            return null;
        }

        ISurrogateSelector _NextSelector;

        public void ChainSelector(ISurrogateSelector selector)
        {
            _NextSelector = selector;
        }

        public ISurrogateSelector GetNextSelector()
        {
            return _NextSelector;
        }

        public ISerializationSurrogate GetSurrogate(Type type, StreamingContext context, out ISurrogateSelector selector)
        {
            selector = this;

            if (type == typeof(Condition))
            {
                if (!_Handles.Keys.Contains(type))
                {
                    _Handles.Add(type, FormatterServices.GetSurrogateForCyclicalReference(new ConditionSurrogateSelector(DataModel)));
                }

                return _Handles[type];
            }

            if (_NextSelector != null)
            {
                return _NextSelector.GetSurrogate(type, context, out selector);
            }

            if (IsKnown(type))
            {
                return null;
            }
            else
            {
                return null;
            }
        }

        Dictionary<Type, ISerializationSurrogate> _Handles = new Dictionary<Type, ISerializationSurrogate>();
    }
}