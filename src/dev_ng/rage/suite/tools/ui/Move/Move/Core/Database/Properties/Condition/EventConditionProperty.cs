﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class EventConditionProperty : IConditionProperty, ISerializable
    {
        public EventConditionProperty()
        {
            Id = Guid.NewGuid();
        }

        public EventConditionProperty(EventConditionProperty other)
        {
            Id = Guid.NewGuid();
            this.Name = other.Name;
            this.Event = other.Event;
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        private LogicEvent Event_ = null;
        public LogicEvent Event
        {
            get { return Event_; }
            set
            {
                if (object.ReferenceEquals(value, this.Event_))
                {
                    return;
                }

                if (value == null && this.Event_ != null)
                {
                    this.Event_.SetUsed(false);
                }

                this.Event_ = value;
                if (value != null)
                {
                    this.Event_.SetUsed(true);
                }
            }
        }
        public Condition Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Event = "Event";
            public static string Parent = "Parent";
        }

        public EventConditionProperty(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(Const.Name);
            Id = (Guid)info.GetValue(Const.Id, typeof(Guid));
            Event = (LogicEvent)info.GetValue(Const.Event, typeof(LogicEvent));
            Parent = (Condition)info.GetValue(Const.Parent, typeof(Condition));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Event, Event);
            info.AddValue(Const.Parent, Parent);
        }

        public object Clone()
        {
            return new EventConditionProperty(this);
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="EventConditionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public EventConditionProperty(XmlReader reader, Database database, Condition parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            string eventId = reader.GetAttribute("Event");
            if (!string.IsNullOrEmpty(eventId))
            {
                this.Event = database.GetEvent(Guid.ParseExact(eventId, "D"));
                if (this.Event == null)
                {
                    string eventName = reader.GetAttribute("EventName");
                    this.Event = database.GetEvent(eventName);
                    if (this.Event == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to event with id '{0}'. This needs fixing before a export is allowed.", eventId);
                    }
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Event != null)
            {
                writer.WriteAttributeString("Event", this.Event.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("EventName", this.Event.Name);
            }
        }
    }
}