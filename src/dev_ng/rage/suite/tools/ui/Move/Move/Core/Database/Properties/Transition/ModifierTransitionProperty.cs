﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;

namespace New.Move.Core
{
    [Serializable]
    public class ModifierTransitionProperty : ITransitionProperty, ISerializable
    {
        public static implicit operator ModifierTransitionProperty(Modifier modifier)
        {
            ModifierTransitionProperty property = new ModifierTransitionProperty();
            property.Value = modifier;

            return property;
        }

        public ModifierTransitionProperty()
        {
            Id = Guid.NewGuid();
        }


        public ModifierTransitionProperty(ModifierTransitionProperty other)
        {
            this.Name = other.Name;
            this.Parent = other.Parent;
            this.Value = other.Value;

            Id = Guid.NewGuid();
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public ITransition Parent { get; set; }
        public Modifier Value { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Value = "Value";
        }

        public ModifierTransitionProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ITransition)info.GetValue(SerializationTag.Parent, typeof(ITransition));
            Value = (Modifier)info.GetValue(SerializationTag.Value, typeof(Modifier));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Value, Value);
        }

        public object Clone()
        {
            return new ModifierTransitionProperty(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ModifierTransitionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public ModifierTransitionProperty(XmlReader reader, ITransition parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Value", ((int)this.Value).ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Value = (Modifier)(int.Parse(reader.GetAttribute("Value")));
            reader.Skip();
        }
    }
}