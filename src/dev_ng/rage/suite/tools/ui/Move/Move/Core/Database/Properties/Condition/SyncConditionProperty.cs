﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;
using System.Globalization;

namespace New.Move.Core
{
    [Serializable]
    public class SyncConditionProperty : IConditionProperty, ISerializable
    {
        #region Constructors
        public SyncConditionProperty()
        {
            Flags = SyncType.None | SyncType.Phase | SyncType.Tag;
            Input = SyncType.None;

            Id = Guid.NewGuid();
        }

        public SyncConditionProperty(SyncConditionProperty other)
        {
            Flags = other.Flags;
            Input = other.Input;
            CurrentEvent0 = other._CurrentEvent0;
            _CurrentEvent1 = other._CurrentEvent1;
            _PhaseSync = other._PhaseSync;
            
            Id = Guid.NewGuid();
        }

        public object Clone()
        {
            return new SyncConditionProperty(this);
        }
        #endregion

        #region Inherited/Interface Members

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public Condition Parent { get; set; }

        #endregion

        #region Native Members
        public SyncType Flags { get; private set; }
        SyncType _Input;
        public SyncType Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }

        EventId _CurrentEvent0; 
        public EventId CurrentEvent0
        {
            get { return _CurrentEvent0; }
            set
            {
                if (_CurrentEvent0 != value)
                {
                    _CurrentEvent0 = value;
                    OnPropertyChanged("CurrentEvent");
                }
            }
        }

        EventId _CurrentEvent1;
        public EventId CurrentEvent1
        {
            get { return _CurrentEvent1; }
            set
            {
                if (_CurrentEvent1 != value)
                {
                    _CurrentEvent1 = value;
                    OnPropertyChanged("CurrentEvent1");
                }
            }
        }

        bool _PhaseSync;
        public bool PhaseSync
        {
            get { return _PhaseSync; }
            set 
            {
                if (_PhaseSync != value)
                {
                    _PhaseSync = value;
                    OnPropertyChanged("PhaseSync");
                }
            }
        }
        

        #endregion

        #region Serialisation

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Input = "Input";
            public static string CurrentEvent0 = "CurrentEvent";
            public static string CurrentEvent1 = "CurrentEvent1";
        }

        public SyncConditionProperty(SerializationInfo info, StreamingContext context)
        {
            try
            {
                Flags = SyncType.None | SyncType.Phase | SyncType.Tag;

                Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
                Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
                Parent = (Condition)info.GetValue(SerializationTag.Parent, typeof(Condition));
                Input = (SyncType)info.GetValue(SerializationTag.Input, typeof(SyncType));
                
                switch (Input)
                {
                    case SyncType.Tag:
                        CurrentEvent0 = (EventId)info.GetValue(SerializationTag.CurrentEvent0, typeof(EventId));
                        try
                        {
                            CurrentEvent1 = (EventId)info.GetValue(SerializationTag.CurrentEvent1, typeof(EventId));
                        }
                        catch (Exception)
                        {

                        }
                        break;
                }
            }
            catch (Exception)
            {
            	
            }
        }

        public SyncConditionProperty(ILogic parent)
        {

        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            try
            {
                info.AddValue(SerializationTag.Name, Name);
                info.AddValue(SerializationTag.Id, Id);
                info.AddValue(SerializationTag.Parent, Parent);
                info.AddValue(SerializationTag.Input, Input);
                switch (Input)
                {
                    case SyncType.Tag:
                        info.AddValue(SerializationTag.CurrentEvent0, CurrentEvent0);
                        info.AddValue(SerializationTag.CurrentEvent1, CurrentEvent1);
                        break;
                }
            }
            catch (Exception)
            {
            	
            }
        }
        #endregion

        #region Export Data
        public void ExportToXML(PargenXmlNode parentNode)
        {
            switch (Input)
            {
                case SyncType.None:
                    parentNode.AppendTextElementNode("Type", "ST_NONE");
                    break;
                case SyncType.Phase:
                    parentNode.AppendTextElementNode("Type", "ST_PHASE");
                    break;
                case SyncType.Tag:
                    parentNode.AppendTextElementNode("Type", "ST_TAG");

                    uint tagValue = (uint)CurrentEvent0 | (uint)CurrentEvent1;
                    parentNode.AppendValueElementNode("Tag", "value", tagValue.ToString());
                    break;
            }
        }
        #endregion
        
        /// <summary>
        /// Initialises a new instance of the <see cref="SyncConditionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public SyncConditionProperty(XmlReader reader, Condition parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            Flags = SyncType.None | SyncType.Phase | SyncType.Tag;
            this.Parent = parent;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Input", ((int)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "Event0", ((int)this.CurrentEvent0).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "Event1", ((int)this.CurrentEvent1).ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Input = (SyncType)(int.Parse(reader.GetAttribute("Input")));
            this.CurrentEvent0 = (EventId)(int.Parse(reader.GetAttribute("Event0")));
            this.CurrentEvent1 = (EventId)(int.Parse(reader.GetAttribute("Event1")));
            reader.Skip();
        }
    }
}
