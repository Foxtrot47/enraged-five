﻿namespace New.Move.Core
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml;

    /// <summary>
    /// Represents a event that can be referenced throughout a database where a trigger
    /// can be set.
    /// </summary>
    [Serializable]
    public class LogicEvent : ISerializable, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        protected const string NameSerializationTag = "Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        protected const string IdSerializationTag = "Id";

        /// <summary>
        /// The private field used to the <see cref="UseIcon"/> property.
        /// </summary>
        private int usedCount = 0;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string name = string.Empty;
        
        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.LogicEvent"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the event.
        /// </param>
        public LogicEvent(string name)
        {
            this.name = name;
            this.id = Guid.NewGuid();
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.LogicEvent"/> class as
        /// a copy of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        public LogicEvent(LogicEvent other)
        {
            this.name = other.Name;
            this.id = Guid.NewGuid();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.LogicEvent"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        protected LogicEvent(SerializationInfo info, StreamingContext context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case NameSerializationTag:
                        this.Name = (string)entry.Value;
                        break;
                    case IdSerializationTag:
                        this.Id = (Guid)entry.Value;
                        break;
                    default:
                        Trace.TraceWarning(StringTable.UnrecognisedBinaryData);
                        break;
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.LogicEvent"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> parameter is null.
        /// </exception>
        public LogicEvent(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.name = reader.GetAttribute(NameSerializationTag);
            this.id = Guid.ParseExact(reader.GetAttribute(IdSerializationTag), "D");
            reader.Skip();
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when this logic event fires.
        /// </summary>
        public event EventHandler LogicEventFired;

        /// <summary>
        /// Occurs when the value of a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the name for this event. This is the name that is referenced by other
        /// nodes that what to use it as a value for a property.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.NotifyPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets the image source used for the icon to display to the user if the signal is
        /// currently not used throughout its owning database.
        /// </summary>
        public ImageSource UsedIcon
        {
            get
            {
                if (this.usedCount > 0)
                {
                    return null;
                }
                else
                {
                    BitmapImage bitmapImage = new BitmapImage();

                    try
                    {
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource =
                            Assembly.GetExecutingAssembly().GetManifestResourceStream(
                            "Move.Core.Images.NotUsed.png");
                        bitmapImage.EndInit();
                        return bitmapImage;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the unique identifier for this event.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            private set { this.id = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Fires this event.
        /// </summary>
        public void FireEvent()
        {
            EventHandler handler = this.LogicEventFired;
            if (handler == null)
            {
                return;
            }
            
            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("event");

            writer.WriteAttributeString(
                NameSerializationTag, this.Name);
            writer.WriteAttributeString(
                IdSerializationTag,
                this.Id.ToString("D", CultureInfo.InvariantCulture));

            writer.WriteEndElement();
        }

        /// <summary>
        /// Increases or decreases a reference count that is used to either show or find
        /// the "NotUsed" icon to the user.
        /// </summary>
        /// <param name="used">
        /// A value indicating whether to increase or decrease the reference count.
        /// </param>
        public void SetUsed(bool used)
        {
            if (used)
            {
                this.usedCount++;
            }
            else
            {
                this.usedCount--;
            }

            this.NotifyPropertyChanged("UsedIcon");
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerializationTag, this.Name);
            info.AddValue(IdSerializationTag, this.Id);
        }

        /// <summary>
        /// Fires the <see cref="PropertyChanged"/> event for this class specifying the name
        /// of the property that has changed.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that changed.
        /// </param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    } // New.Move.Core.LogicEvent {Class}
} // New.Move.Core {Namespace}
