﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows;
using Move.Core.Restorable;
using Move.Utils;
using Rockstar.MoVE.Framework;
using Rockstar.MoVE.Framework.DataModel;
using Rockstar.MoVE.Services;
using RSG.Base.Logging;
using System.Xml;
using System.Globalization;
using System.Text.RegularExpressions;

namespace New.Move.Core
{
    public interface ICoreDataModel
    {
        ObservableCollection<IConditionDesc> ConditionDescriptors { get; }

        ObservableCollection<IDesc> MotionTreeDescriptors { get; }

        ObservableCollection<IDesc> StateMachineDescriptors { get; }
    }

    /// <summary>
    /// Represents a single Move Network.
    /// </summary>
    [Serializable]
    public class Database : RestorableObjectManager, ISerializable
    {
        #region Fields
        /// <summary>
        /// The default filename to use when saving.
        /// </summary>
        public const string DefaultFilename = "default.mtf";

        /// <summary>
        /// The name suffix format used when making sure all nodes have unique names.
        /// </summary>
        private const string duplicateNameSuffixFormat = "000";

        /// <summary>
        /// The length of the duplication suffix.
        /// </summary>
        private const int suffixLength = 3;

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Active"/> property.
        /// </summary>
        public static string ActiveSerializationTag = "Active";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Events"/> property.
        /// </summary>
        public static string EventsSerializationTag = "Events";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Flags"/> property.
        /// </summary>
        public static string FlagsSerializationTag = "Flags";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Identifiers"/> property.
        /// </summary>
        public static string IdentifiersSerializationTag = "Identifiers";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        public static string IdSerializationTag = "Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Messages"/> property.
        /// </summary>
        public static string MessagesSerializationTag = "Messages";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Nodes"/> property.
        /// </summary>
        public static string NodesSerializationTag = "Nodes";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="References"/> property.
        /// </summary>
        public static string ReferencesSerializationTag = "References";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Requests"/> property.
        /// </summary>
        public static string RequestsSerializationTag = "Requests";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Root"/> property.
        /// </summary>
        public static string RootSerializationTag = "Root";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="SelectedItems"/> property.
        /// </summary>
        public static string SelectedItemsSerializationTag = "SelectedItems";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Signals"/> property.
        /// </summary>
        public static string SignalsSerializationTag = "Signals";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="VariableClipSets"/> property.
        /// </summary>
        public static string VariableClipSetsSerializationTag = "VariableClipSets";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Version"/> property.
        /// </summary>
        public static string VersionSerializationTag = "Version";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="DataModel"/> property.
        /// </summary>
        public static string DataModelSerializationTag = "DataModel";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="DataModelService"/> property.
        /// </summary>
        public static string DataModelServiceSerializationTag = "DataModelService";

        private string _FullyQualifiedFilename;

        private NoResetObservableCollection<DatabaseReference> _References;

        /// <summary>
        /// The private field for the <see cref="Active"/> property.
        /// </summary>
        private ITransitional active;

        /// <summary>
        /// The private field for the <see cref="EngagedItems"/> property.
        /// </summary>
        private NoResetObservableCollection<IEngageable> engagedItems = new NoResetObservableCollection<IEngageable>();

        /// <summary>
        /// The private field for the <see cref="Events"/> property.
        /// </summary>
        private NoResetObservableCollection<LogicEvent> events;

        /// <summary>
        /// The version number to use when serialising the network out to a file.
        /// </summary>
        private string FileVersion = "1.0";

        /// <summary>
        /// The private field for the <see cref="Flags"/> property.
        /// </summary>
        private NoResetObservableCollection<Flag> flags;
        
        /// <summary>
        /// The private field for the <see cref="IsDirty"/> property.
        /// </summary>
        private bool isDirty;

        /// <summary>
        /// The private field for the <see cref="Link"/> property.
        /// </summary>
        private Link link = null;

        /// <summary>
        /// The private field for the <see cref="Isloading"/> property.
        /// </summary>
        private bool loading = false;

        /// <summary>
        /// The private field for the <see cref="Messages"/> property.
        /// </summary>
        private NoResetObservableCollection<Message> messages;

        /// <summary>
        /// The private field for the <see cref="Nodes"/> property.
        /// </summary>
        private NoResetObservableCollection<INode> nodes;

        /// <summary>
        /// The private field for the <see cref="Requests"/> property.
        /// </summary>
        private NoResetObservableCollection<Request> requests;

        /// <summary>
        /// The private field for the <see cref="Root"/> property.
        /// </summary>
        private ITransitional root;

        /// <summary>
        /// The private field for the <see cref="SelectedItems"/> property.
        /// </summary>
        private NoResetObservableCollection<ISelectable> selectedItems;

        /// <summary>
        /// The private field for the <see cref="Signal"/> property.
        /// </summary>
        private NoResetObservableCollection<Signal> signals;

        /// <summary>
        /// The private field for the <see cref="VariableClipSets"/> property.
        /// </summary>
        private NoResetObservableCollection<VariableClipSet> variableClipSets;

        /// <summary>
        /// The private field used for the <see cref="XmlFactory"/> property.
        /// </summary>
        private DatabaseXmlFactory xmlFactory;
        #endregion

        #region Constructors
        public Database(DatabaseXmlFactory factory)
        {
            Id = Guid.NewGuid();

            this.xmlFactory = factory;
            link = new Link(this);

            References = new NoResetObservableCollection<DatabaseReference>();

            Messages = new NoResetObservableCollection<Message>();
            Signals = new NoResetObservableCollection<Signal>();
            Events = new NoResetObservableCollection<LogicEvent>();
            Requests = new NoResetObservableCollection<Request>();
            Flags = new NoResetObservableCollection<Flag>();

            Nodes = new NoResetObservableCollection<INode>();

            VariableClipSets = new NoResetObservableCollection<VariableClipSet>();

            SelectedItems = new NoResetObservableCollection<ISelectable>();

            SetHandlers();

            VariableClipSets.Add(new VariableClipSet("Default"));
        }

        public Database(ITransitionalDesc desc, ICoreDataModel model, IDataModelService service, Point offset, DatabaseXmlFactory factory)
            : this(factory)
        {
            DataModel = model;
            DataModelService = service;
            Active = desc.Create(this, offset);
            Active.Name = desc.Name;
            Active.ScrollOffset = offset;
            Nodes.Add(Active);

            Root = Active;
        }

        public Database(ICoreDataModel model, IDataModelService service, Point offset, DatabaseXmlFactory factory)
            : this(factory)
        {
            DataModel = model;
            DataModelService = service;
        }

        public Database(ICoreDataModel model, IDataModelService service, DatabaseXmlFactory factory)
            : this(factory)
        {
            DataModel = model;
            DataModelService = service;
        }

        public Database(SerializationInfo info, StreamingContext context)
        {
            FileVersion = (string)info.GetValue(VersionSerializationTag, typeof(string));
            Id = (Guid)info.GetValue(IdSerializationTag, typeof(Guid));

            Nodes = (NoResetObservableCollection<INode>)info.GetValue(NodesSerializationTag, typeof(NoResetObservableCollection<INode>));

            Root = (ITransitional)info.GetValue(RootSerializationTag, typeof(ITransitional));

            Active = (ITransitional)info.GetValue(ActiveSerializationTag, typeof(ITransitional));

            Messages = (NoResetObservableCollection<Message>)info.GetValue(MessagesSerializationTag, typeof(NoResetObservableCollection<Message>));
            Signals = (NoResetObservableCollection<Signal>)info.GetValue(SignalsSerializationTag, typeof(NoResetObservableCollection<Signal>));
            foreach (var signal in Signals)
            {
                signal.PropertyChanged += new PropertyChangedEventHandler(OnSignalPropertyChanged);
            }
            Events = (NoResetObservableCollection<LogicEvent>)info.GetValue(EventsSerializationTag, typeof(NoResetObservableCollection<LogicEvent>));
            Flags = (NoResetObservableCollection<Flag>)info.GetValue(FlagsSerializationTag, typeof(NoResetObservableCollection<Flag>));
            Requests = (NoResetObservableCollection<Request>)info.GetValue(RequestsSerializationTag, typeof(NoResetObservableCollection<Request>));

            // Serialisation of SelectedItems is disabled - JWR
            // This can cause crashes if particular transitions are selected (B* 426286)
            SelectedItems = new NoResetObservableCollection<ISelectable>();

            References = (NoResetObservableCollection<DatabaseReference>)info.GetValue(ReferencesSerializationTag, typeof(NoResetObservableCollection<DatabaseReference>));

            try
            {
                VariableClipSets = (NoResetObservableCollection<VariableClipSet>)info.GetValue(VariableClipSetsSerializationTag, typeof(NoResetObservableCollection<VariableClipSet>));
            }
            catch (SerializationException)
            {
                VariableClipSets = new NoResetObservableCollection<VariableClipSet>();
                VariableClipSets.Add(new VariableClipSet("Default"));
            }

//             try
//             {
//                 DataModel = (ICoreDataModel)info.GetValue(DataModelSerializationTag, typeof(ICoreDataModel));
//                 DataModelService = (IDataModelService)info.GetValue(DataModelServiceSerializationTag, typeof(IDataModelService));
//             }
//             catch (SerializationException)
//             {
//             	
//             }
        } 
        #endregion

        #region Events
        public event RoutedEventHandler ActiveChanging;

        public event EventHandler Closed;

        public event EventHandler Closing;

        public event RoutedEventHandler DirtyChanged;

        public event RoutedEventHandler FullyQualifiedFilenameChanged;

        public event RoutedEventHandler RootChanging;

        public event Action SignalNameChanged; 
        #endregion

        #region Properties
        /// <summary>
        /// Gets the xml factory used to create this database.
        /// </summary>
        public DatabaseXmlFactory XmlFactory
        {
            get { return this.xmlFactory; }
        }

        public static string ApplicationFolderPath
        {
            get
            {
                string path = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Rage.Move.Core.Runtime.ApplicationFolderName);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                return path;
            }
        }

        public static string DefaultFullyQualifiedFilename
        {
            get
            {
                string appLocation = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Rage.Move.Core.Runtime.ApplicationFolderName);

                if (!Directory.Exists(appLocation))
                {
                    Directory.CreateDirectory(appLocation);
                }

                return Path.Combine(appLocation, DefaultFilename);
            }
        }

        public static bool IsLoading
        {
            get;
            private set;
        }

        public ITransitional Active
        {
            get
            {
                return this.active;
            }

            set
            {
                RoutedEventHandler handler = this.ActiveChanging;
                if (handler != null)
                    this.ActiveChanging(this.active, new RoutedEventArgs());

                if (this.SelectedItems != null)
                    this.SelectedItems.Clear();

                if (this.engagedItems != null)
                    this.engagedItems.Clear();

                this.active = value;

                if (this.DataModelService != null)
                    DataModelService.CurrentActive = value;
            }
        }

        public NoResetObservableCollection<IEngageable> EngagedItems
        {
            get { return engagedItems; }
        }

        public NoResetObservableCollection<LogicEvent> Events
        {
            get { return this.events; }
            private set { this.events = value; }
        }

        public NoResetObservableCollection<Flag> Flags
        {
            get { return this.flags; }
            protected set { this.flags = value; }
        }

        public string FullyQualifiedFilename
        {
            get
            {
                return _FullyQualifiedFilename;
            }

            set
            {
                if (_FullyQualifiedFilename != value)
                {
                    _FullyQualifiedFilename = value;
                    OnFullyQualifiedFilenameChanged();
                }
            }
        }

        public Guid Id
        {
            get;
            private set;
        }

        public bool IsDirty
        {
            get
            {
                return this.isDirty;
            }

            set
            {
                if (bool.Equals(value, this.isDirty))
                    return;

                this.isDirty = value;

                RoutedEventHandler handler = this.DirtyChanged;
                if (handler != null)
                    this.DirtyChanged(this, new RoutedEventArgs());
            }
        }

        public Link Link
        {
            get { return link; }
        }

        public NoResetObservableCollection<Message> Messages
        {
            get { return this.messages; }
            private set { this.messages = value; }
        }

        public NoResetObservableCollection<INode> Nodes
        {
            get { return this.nodes; }
            protected set { this.nodes = value; }
        }

        public NoResetObservableCollection<DatabaseReference> References
        {
            get { return _References; }
            set { _References = value; }
        }

        public NoResetObservableCollection<Request> Requests
        {
            get { return this.requests; }
            protected set { this.requests = value; }
        }

        public ITransitional Root
        {
            get
            {
                return this.root;
            }

            set
            {
                RoutedEventHandler handler = this.RootChanging;
                if (handler != null)
                    this.RootChanging(this.root, new RoutedEventArgs());

                this.root = value;

                if (this.DataModelService != null)
                    DataModelService.CurrentRoot = value;
            }
        }

        public NoResetObservableCollection<ISelectable> SelectedItems
        {
            get { return this.selectedItems; }
            private set { this.selectedItems = value; }
        }

        public NoResetObservableCollection<Signal> Signals
        {
            get { return this.signals; }
            protected set { this.signals = value; }
        }

        public NoResetObservableCollection<VariableClipSet> VariableClipSets
        {
            get { return this.variableClipSets; }
            protected set { this.variableClipSets = value; }
        }

        public string Version
        {
            get { return FileVersion; }
            set { FileVersion = value; }
        }

        internal ICoreDataModel DataModel
        {
            get;
            private set;
        }

        internal IDataModelService DataModelService
        {
            get;
            set;
        } 
        #endregion

        #region Methods
        public static string GetHierarchicalNodeName(Database database, INode node)
        {
            string simpleNodeName = node.Name;
            StringBuilder hierarchicalNameBuilder = new StringBuilder(node.Name);
            while (node.Parent != Guid.Empty)
            {
                node = database.Find(node.Parent);

                if (node == null)
                {
                    return simpleNodeName;
                }

                hierarchicalNameBuilder.Insert(0, "\\");
                if (node.Parent != Guid.Empty)
                {
                    hierarchicalNameBuilder.Insert(0, node.Name);
                }
                else
                {
                    hierarchicalNameBuilder.Insert(0, "Root");
                }
            }

            return hierarchicalNameBuilder.ToString();
        }

        public void AddSurrogates(SurrogateSelector selector)
        {
            selector.AddSurrogate(typeof(NoResetObservableCollection<DatabaseReference>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<DatabaseReference>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<ReferenceSignal>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<ReferenceSignal>());

            selector.AddSurrogate(typeof(ObservableCollection<Condition>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<Condition>());

            selector.AddSurrogate(typeof(ConditionCollection),
                new StreamingContext(StreamingContextStates.All),
                new ConditionCollectionSerializationSurrogate());

            selector.AddSurrogate(typeof(ObservableCollection<Connection>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<Connection>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<Connection>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<Connection>());

            selector.AddSurrogate(typeof(ObservableCollection<INode>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<INode>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<INode>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<INode>());

            selector.AddSurrogate(typeof(ObservableCollection<Guid>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<Guid>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<Guid>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<Guid>());

            selector.AddSurrogate(typeof(ObservableCollection<Message>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<Message>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<Message>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<Message>());

            selector.AddSurrogate(typeof(ObservableCollection<Signal>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<Signal>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<Signal>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<Signal>());

            selector.AddSurrogate(typeof(ObservableCollection<Flag>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<Flag>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<Flag>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<Flag>());

            selector.AddSurrogate(typeof(ObservableCollection<Request>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<Request>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<Request>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<Request>());

            selector.AddSurrogate(typeof(ObservableCollection<ITransition>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<ITransition>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<ITransition>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<ITransition>());

            selector.AddSurrogate(typeof(NoResetObservableCollection<ISelectable>),
                new StreamingContext(StreamingContextStates.All),
                new NoResetObservableCollectionSerializationSurrogate<ISelectable>());

            selector.AddSurrogate(typeof(ObservableCollection<Curve>),
                new StreamingContext(StreamingContextStates.All),
                new ObservableCollectionSerializationSurrogate<Curve>());
        }

        public void AppendChild(ITransitional parent, INode child)
        {
            parent.Add(child);

            if (parent == Active)
            {
                DataModelService.InvalidateActiveView();
            }
        }

        public ITransitional Create(ITransitionalDesc desc, Point offset)
        {
            ITransitional node = desc.Create(this, offset);
            node.Database = this;
            node.Name = desc.Name;
            node.ScrollOffset = offset;

            AddNode(node);

            return node;
        }

        public ILogic Create(ILogicDesc desc)
        {
            ILogic node = desc.Create(this) as ILogic;
            node.Database = this;
            node.Name = desc.Name;

            AddNode(node);

            return node;
        }

        public ILogic CreateReference(ReferenceDesc description, DatabaseXmlFactory factory)
        {
            Reference node = description.Create(this) as Reference;
            node.Database = this;
            node.Name = description.Name;
            node.XmlFactory = factory;

            AddNode(node);

            return node;
        }

        public INode Create(IDesc desc)
        {
            INode node = desc.Create(this);
            node.Database = this;
            node.Name = desc.Name;

            AddNode(node);

            PropertyInfo[] properties = node.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                if (pi != null && pi.PropertyType.GetInterface("ILogicPropertyArray") != null)
                {
                    ILogicPropertyArray property = (ILogicPropertyArray)pi.GetValue(node, null);
                    property.Parent = node as ILogic;
                    property.AddHandler(new NotifyCollectionChangedEventHandler(OnPropertyChildrenCollectionChanged));
                }
                else if (pi != null && pi.PropertyType.GetInterface("ILogicProperty") != null)
                {
                    ILogicProperty property = (ILogicProperty)pi.GetValue(node, null);
                    property.Parent = node as ILogic;
                    property.PropertyChanged += new PropertyChangedEventHandler(OnProperty_PropertyChanged);
                }
                else if (pi != null && pi.PropertyType.GetInterface("ILogicalEvent") != null)
                {
                    LogicalEventProperty logicalEvent = (LogicalEventProperty)pi.GetValue(node, null);
                    logicalEvent.Parent = node as ILogic;
                    logicalEvent.PropertyChanged += new PropertyChangedEventHandler(OnProperty_PropertyChanged);
                }
            }

            return node;
        }

        public void Execute(IExecuteAction action)
        {
            action.Execute();
        }

        public void ExportToXML(PargenXmlNode networkNode, ICollection<string> hashedNames)
        {
            ExportVersionToXML(networkNode);
            ExportNodesToXML(networkNode, hashedNames);
            ExportReferencesToXML(networkNode);
            ExportRequestsToXML(networkNode, hashedNames);
            ExportFlagsToXML(networkNode, hashedNames);
            ExportEventsToXML(networkNode, hashedNames);
            networkNode.AppendTextElementNode("Root", Root.Name);

            foreach (var signal in Signals)
            {
                hashedNames.Add(signal.Name);
            }
        }

        public INode Find(Guid id)
        {
            return Nodes.FirstOrDefault(delegate(INode node) { return node.Id == id; });
        }

        public void InitFromNode(INode node)
        {
            node.Position = new Point(node.Position.X + 10, node.Position.Y + 10);

            AddNode(node);

            PropertyInfo[] properties = node.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                if (pi != null && pi.PropertyType.GetInterface("ILogicPropertyArray") != null)
                {
                    ILogicPropertyArray property = (ILogicPropertyArray)pi.GetValue(node, null);
                    property.Parent = node as ILogic;
                    property.AddHandler(new NotifyCollectionChangedEventHandler(OnPropertyChildrenCollectionChanged));
                }
                else if (pi != null && pi.PropertyType.GetInterface("ILogicProperty") != null)
                {
                    ILogicProperty property = (ILogicProperty)pi.GetValue(node, null);
                    property.Parent = node as ILogic;
                    property.PropertyChanged += new PropertyChangedEventHandler(OnProperty_PropertyChanged);
                }
                else if (pi != null && pi.PropertyType.GetInterface("ILogicalEvent") != null)
                {
                    LogicalEventProperty logicalEvent = (LogicalEventProperty)pi.GetValue(node, null);
                    logicalEvent.Parent = node as ILogic;
                    logicalEvent.PropertyChanged += new PropertyChangedEventHandler(OnProperty_PropertyChanged);
                }
            }
        }

        public void Load()
        {
            Database.IsLoading = true;
            this.IsDirty = false;
            if (string.IsNullOrEmpty(this.FullyQualifiedFilename))
            {
                this.FullyQualifiedFilename = Database.DefaultFullyQualifiedFilename;
            }

            try
            {
                string extension = Path.GetExtension(this.FullyQualifiedFilename);
                if (extension == ".mtf")
                {
                    this.LoadBinaryFormat(this.FullyQualifiedFilename);
                    this.FullyQualifiedFilename = Path.ChangeExtension(this.FullyQualifiedFilename, "mxtf");
                    this.IsDirty = true;
                }
                else if (extension == ".mxtf")
                {
                    this.LoadXmlFormat(this.FullyQualifiedFilename);
                    this.IsDirty = false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                this.FullyQualifiedFilename = string.Empty;
                string msg = string.Format(
                    "Loading '{0}' failed! See inner exception for more details.",
                    this.FullyQualifiedFilename);
                throw new LoadFailedException(msg, e);
            }
            finally
            {
                Database.IsLoading = loading = false;
            }
            
            string name = Path.GetFileName(this.FullyQualifiedFilename);
            LogFactory.ApplicationLog.Message("Successfully loaded {0}.", name);
        }

        public void Load(string filename)
        {
            this.FullyQualifiedFilename = filename;
            Load();
            if (this.Active == null)
            {
                this.active = this.root;
            }
        }

        /// <summary>
        /// Retrieves the logic event associated with this database that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the event to retrieve.
        /// </param>
        /// <returns>
        /// The logic event associated with this database that has the specified id.
        /// </returns>
        public LogicEvent GetEvent(Guid id)
        {
            return (from e in this.Events
                    where e != null && e.Id == id
                    select e).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the logic event associated with this database that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the event to retrieve.
        /// </param>
        /// <returns>
        /// The logic event associated with this database that has the specified id.
        /// </returns>
        public LogicEvent GetEvent(string name)
        {
            return (from e in this.Events
                    where e != null && string.CompareOrdinal(e.Name, name) == 0
                    select e).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the signal associated with this database that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the signal to retrieve.
        /// </param>
        /// <returns>
        /// The signal associated with this database that has the specified id.
        /// </returns>
        public Signal GetSignal(Guid id)
        {
            Signal signal = (from e in this.Signals
                    where e != null && e.Id == id
                    select e).FirstOrDefault();

            if (signal == null)
            {
                Trace.TraceError(string.Format("Unable to find signal with id {0}.", id));
            }

            return signal;
        }

        /// <summary>
        /// Retrieves the signal associated with this database that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the signal to retrieve.
        /// </param>
        /// <returns>
        /// The signal associated with this database that has the specified id.
        /// </returns>
        public Signal GetSignal(string name)
        {
            Signal signal = (from e in this.Signals
                             where e != null && string.CompareOrdinal(e.Name, name) == 0
                             select e).FirstOrDefault();

            if (signal == null)
            {
                Trace.TraceError(string.Format("Unable to find signal with name {0}.", name));
            }

            return signal;
        }

        /// <summary>
        /// Retrieves the flag associated with this database that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the flag to retrieve.
        /// </param>
        /// <returns>
        /// The flag associated with this database that has the specified id.
        /// </returns>
        public Flag GetFlag(Guid id)
        {
            return (from e in this.Flags
                    where e != null && e.Id == id
                    select e).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the flag associated with this database that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the flag to retrieve.
        /// </param>
        /// <returns>
        /// The flag associated with this database that has the specified id.
        /// </returns>
        public Flag GetFlag(string name)
        {
            return (from e in this.Flags
                    where e != null && string.CompareOrdinal(e.Name, name) == 0
                    select e).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the variable clipset associated with this database that has the
        /// specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the variable clipset to retrieve.
        /// </param>
        /// <returns>
        /// The variable clipset associated with this database that has the specified id.
        /// </returns>
        public VariableClipSet GetVariableClipSet(Guid id)
        {
            return (from e in this.VariableClipSets
                    where e != null && e.Id == id
                    select e).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the variable clipset associated with this database that has the
        /// specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the variable clipset to retrieve.
        /// </param>
        /// <returns>
        /// The variable clipset associated with this database that has the specified id.
        /// </returns>
        public VariableClipSet GetVariableClipSet(string name)
        {
            return (from e in this.VariableClipSets
                    where e != null && string.CompareOrdinal(e.Name, name) == 0
                    select e).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the request associated with this database that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the request to retrieve.
        /// </param>
        /// <returns>
        /// The request associated with this database that has the specified id.
        /// </returns>
        public Request GetRequest(Guid id)
        {
            return (from e in this.Requests
                    where e != null && e.Id == id
                    select e).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the request associated with this database that has the specified id.
        /// </summary>
        /// <param name="id">
        /// The id of the request to retrieve.
        /// </param>
        /// <returns>
        /// The request associated with this database that has the specified id.
        /// </returns>
        public Request GetRequest(string name)
        {
            return (from e in this.Requests
                    where e != null && string.CompareOrdinal(e.Name, name) == 0
                    select e).FirstOrDefault();
        }

        public void MakeNodeNamesUnique()
        {
            var nodeNamesInUse = new List<string>();

            // Add in the names of all the nodes that can't be modified automatically
            IEnumerable<INode> networkNodeQuery =
                from child in Nodes
                where (child.GetType() == typeof(Motiontree) ||
                       child.GetType() == typeof(StateMachine) ||
                       child.GetType() == typeof(LogicLeafSM) ||
                       child.GetType() == typeof(LogicInlineSM))
                select child;

            foreach (INode node in networkNodeQuery)
            {
                string nodeNameLower = node.Name.ToLower();
                if (!nodeNamesInUse.Contains(nodeNameLower))
                {
                    nodeNamesInUse.Add(nodeNameLower);
                }
            }

            IEnumerable<INode> modifyableNodeQuery =
                from child in Nodes
                where (child.GetType() != typeof(Output) &&
                       child.GetType().BaseType != typeof(SignalBase) &&
                       child.GetType() != typeof(TunnelParameter) &&
                       child.GetType() != typeof(TunnelEvent) &&
                       child.GetType() != typeof(Motiontree) &&
                       child.GetType() != typeof(StateMachine) &&
                       child.GetType() != typeof(LogicLeafSM) &&
                       child.GetType() != typeof(LogicInlineSM) &&
                       !(child is IOperator))
                select child;

            foreach (INode node in modifyableNodeQuery)
            {
                node.Name = GetUniqueName(node.Name, nodeNamesInUse);
            }
        }

        public void MakeNodeNameUnique(INode node)
        {
            IEnumerable<string> existingNodeNames = from existingNode in Nodes
                                                    where (node.GetType() != typeof(Output) &&
                                                           node.GetType() != typeof(Input) &&
                                                           node.GetType().BaseType != typeof(SignalBase) &&
                                                           node.GetType() != typeof(TunnelParameter) &&
                                                           node.GetType() != typeof(TunnelEvent))
                                                    select existingNode.Name.ToLower();

            var nodeNamesInUse = new List<string>();
            foreach (string existingNodeName in existingNodeNames)
            {
                if (!nodeNamesInUse.Contains(existingNodeName))
                    nodeNamesInUse.Add(existingNodeName);
            }

            node.Name = GetUniqueName(node.Name, nodeNamesInUse);
        }

        public void OnProperty_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Input")
            {
                New.Move.Core.ILogicProperty property = sender as New.Move.Core.ILogicProperty;
                ILogic node = property.Parent;
                Motiontree parent = (Motiontree)node.Database.Find(node.Parent);

                parent.RemoveConnectionsFromProperty(property);
            }
            else if (e.PropertyName == "Enabled")
            {
                LogicalEventProperty property = sender as LogicalEventProperty;
                if (!property.Enabled)
                {
                    ILogic node = property.Parent;
                    Motiontree parent = (Motiontree)node.Database.Find(node.Parent);

                    parent.RemoveConnectionsFromEvent(property);
                }
            }
        }

        public void OnPropertyChildrenCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ILogicPropertyPair property in e.NewItems.OfType<ILogicPropertyPair>())
                {
                    property.SetParentOnChildren();
                }

                foreach (ILogicPropertyGroup property in e.NewItems.OfType<ILogicPropertyGroup>())
                {
                    property.SetParentOnChildren();
                }
            }
        }

        public override void Push(IRestorableAction action)
        {
        }

        public void Save()
        {
            if (this.Root == null)
            {
                return;
            }

            if (String.IsNullOrEmpty(this.FullyQualifiedFilename))
            {
                this.FullyQualifiedFilename = Database.DefaultFullyQualifiedFilename;
            }

            try
            {
                this.SaveXmlFormat(this.FullyQualifiedFilename);
            }
            catch (Exception ex)
            {
                LogFactory.ApplicationLog.Error("Save exception: {0}.", ex.Message);
                if (!System.Diagnostics.Debugger.IsAttached)
                {
                    System.Threading.Thread.Sleep(30000);
                }

                string msg = string.Format(
                    "Saving '{0}' failed! See inner exception for details.",
                    this.FullyQualifiedFilename);
                throw new SaveFailedException(msg, ex);
            }
            finally
            {
                this.IsDirty = false;
            }
        }

        public void Save(string filename)
        {
            this.FullyQualifiedFilename = filename;
            Save();
        }

        private void AddNode(INode node)
        {
            MakeNodeNameUnique(node);

            Nodes.Add(node);
        }

        private void ExportFlagsToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            var flagsNode = parentNode.AppendChild("Flags");
            foreach (var flag in Flags)
            {
                flagsNode.AppendTextElementNode("Item", flag.Name);
                hashedNames.Add(flag.Name);
            }
        }

        private void ExportEventsToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            var eventNode = parentNode.AppendChild("Events");
            foreach (var dbEvent in Events)
            {
                eventNode.AppendTextElementNode("Item", dbEvent.Name);
                hashedNames.Add(dbEvent.Name);
            }
        }

        private void ExportNodesToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            var nodesNode = parentNode.AppendChild("Nodes");
            SortedList<string, List<INode>> sortedNodes = new SortedList<string, List<INode>>();
            foreach (var node in Nodes)
            {
                if (!sortedNodes.ContainsKey(node.Name))
                {
                    sortedNodes.Add(node.Name, new List<INode>());
                }

                sortedNodes[node.Name].Add(node);
            }


            foreach (KeyValuePair<string, List<INode>> nodeList in sortedNodes)
            {
                foreach (INode node in nodeList.Value)
                {
                    node.ExportToXML(nodesNode, hashedNames);
                }
            }
        }

        private void ExportReferencesToXML(PargenXmlNode parentNode)
        {
            List<Reference> references = new List<Reference>();
            foreach (INode node in Root.Children)
                GetReferenceCountRecursive(node, references);

            var referencesNode = parentNode.AppendChild("References");
            foreach (Reference reference in references)
            {
                string referenceName = Path.GetFileNameWithoutExtension(reference.Filename.Value);
                referencesNode.AppendTextElementNode("Item", referenceName);
            }
        }

        private void ExportRequestsToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            var requestsNode = parentNode.AppendChild("Requests");
            foreach (var request in Requests)
            {
                requestsNode.AppendTextElementNode("Item", request.Name);
                hashedNames.Add(request.Name);
            }
        }

        private void ExportVersionToXML(PargenXmlNode parentNode)
        {
            var versionNode = parentNode.AppendChild("Version");
            versionNode.AppendTextElementNode("Major", ((int)DatabaseExportSerializer.Version.Major).ToString());
            versionNode.AppendTextElementNode("Minor", ((int)DatabaseExportSerializer.Version.Minor).ToString());
            versionNode.AppendTextElementNode("Patch", ((int)DatabaseExportSerializer.Version.Patch).ToString());
            versionNode.AppendTextElementNode("IsDev", ((int)DatabaseExportSerializer.Version.IsDev).ToString());
        }

        SignalBase[] FromEnumerable(IEnumerable<SignalBase> enumerable)
        {
            SignalBase[] nodes = new SignalBase[enumerable.Count()];
            int index = 0;

            foreach (SignalBase signal in enumerable)
            {
                nodes[index] = signal;
                ++index;
            }

            return nodes;
        }

        private void GetReferenceCountRecursive(INode node, List<Reference> references)
        {
            if (node is Reference)
                references.Add(node as Reference);
            else if (node is Motiontree)
            {
                foreach (INode childnode in (node as Motiontree).Children)
                    GetReferenceCountRecursive(childnode, references);
            }
            else if (node is StateMachine)
            {
                foreach (INode childnode in (node as StateMachine).Children)
                    GetReferenceCountRecursive(childnode, references);
            }
        }

        private Regex uniqueNameText = new Regex(".*_(\\d{3})");

        // Given a name and collection of existing names, generate a replacement (and update collection) by updating or appending a suffix
        private string GetUniqueName(string name, ICollection<string> existingNames)
        {
            // hashing is case insensitive, so names have to be unique on a case independent basis.
            if (!existingNames.Contains(name.ToLower()))// new entry
            {
                existingNames.Add(name.ToLower());
                return name;
            }
            else
            {
                if (uniqueNameText.IsMatch(name))
                {
                    int suffixIndex = name.Length - 3;
                    string preSuffixText = name.Substring(0, suffixIndex);
                    string suffixText = name.Substring(suffixIndex, 3);
                    int suffix = 0;
                    if (int.TryParse(suffixText, out suffix))
                    {
                        return GetUniqueName(preSuffixText + (++suffix).ToString("000"), existingNames);
                    }       
                }
            }

            return GetUniqueName(name + "_000", existingNames);// append a suffix that potentially makes the name unique and try again
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(VersionSerializationTag, FileVersion);
            info.AddValue(IdSerializationTag, Id);

            info.AddValue(NodesSerializationTag, Nodes);

            info.AddValue(RootSerializationTag, Root);
            info.AddValue(ActiveSerializationTag, Active);

            info.AddValue(MessagesSerializationTag, Messages);
            info.AddValue(SignalsSerializationTag, Signals);
            info.AddValue(EventsSerializationTag, Events);
            info.AddValue(FlagsSerializationTag, Flags);
            info.AddValue(RequestsSerializationTag, Requests);

            // Serialisation of SelectedItems is disabled - JWR
            // This can cause crashes if particular transitions are selected (B* 426286)

            info.AddValue(ReferencesSerializationTag, References);

            info.AddValue(VariableClipSetsSerializationTag, VariableClipSets);

//             info.AddValue(DataModelSerializationTag, DataModel);
//             info.AddValue(DataModelServiceSerializationTag, DataModelService);
        }

        void OnDatabaseCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            IsDirty = true;
        }

        void OnEngagedItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (IEngageable item in e.NewItems)
                    {
                        item.OnEngagedChanged(EngagedChangedAction.Add);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (IEngageable item in e.OldItems)
                    {
                        item.OnEngagedChanged(EngagedChangedAction.Remove);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    Debug.Assert(false);
                    break;
            }
        }

        void OnFullyQualifiedFilenameChanged()
        {
            if (FullyQualifiedFilenameChanged != null)
                FullyQualifiedFilenameChanged(this, new RoutedEventArgs());
        }

        void OnReferenceDatabaseClosed(object sender, EventArgs e)
        {
            Database database = sender as Database;
            if (database.IsDirty)
            {
                database.Load(database.FullyQualifiedFilename);
                foreach (Reference reference in Nodes.OfType<Reference>())
                {
                    if (reference.ReferencedDatabase == database)
                    {
                        database.Signals.CollectionChanged +=
                            new NotifyCollectionChangedEventHandler(reference.Signals_CollectionChanged);
                        reference.UpdateReferenceSignals();
                    }
                }
            }
        }

        void OnReferencesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Database database in e.NewItems)
                    {
                        database.Closed += new EventHandler(OnReferenceDatabaseClosed);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Database database in e.OldItems)
                    {
                        database.Closed -= new EventHandler(OnReferenceDatabaseClosed);
                    }
                    break;
            }
        }

        void OnSelectedItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ISelectable item in e.NewItems)
                    {
                        item.OnSelectedChanged(SelectionChangedAction.Add, SelectedItems.Count);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (ISelectable item in e.OldItems)
                    {
                        item.OnSelectedChanged(SelectionChangedAction.Remove, SelectedItems.Count);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    Debug.Assert(false);
                    break;
            }
        }

        void OnSignalPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name")
            {
                if (SignalNameChanged != null)
                    SignalNameChanged();
            }
        }

        void OnSignalsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Signal signal in e.NewItems)
                {
                    signal.PropertyChanged += new PropertyChangedEventHandler(OnSignalPropertyChanged);
                }
            }

            if (e.OldItems != null)
            {
                foreach (Signal signal in e.OldItems)
                {
                    signal.PropertyChanged -= new PropertyChangedEventHandler(OnSignalPropertyChanged);
                }
            }

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                    SignalBase[] nodes = FromEnumerable(Nodes.OfType<SignalBase>());
                    foreach (Signal signal in e.OldItems)
                    {
                        RemoveSignalNodeWithSignal(signal, nodes);
                    }
                    break;
            }
        }

        void RemoveHandlers()
        {
            SetHandlersManager(true);
        }

        void RemoveSignalNodeWithSignal(Signal signal, SignalBase[] nodes)
        {
            foreach (SignalBase node in nodes)
            {
                if (node.Signal == signal)
                {
                    node.Dispose();
                }
            }
        }

        void SetHandlers()
        {
            SetHandlersManager(false);
        }

        private void SetHandlersManager(bool remove)
        {
            if (remove)
            {
                this.References.CollectionChanged -= this.OnReferencesChanged;

                this.Nodes.CollectionChanged -= this.OnDatabaseCollectionChanged;

                this.Signals.CollectionChanged -= this.OnDatabaseCollectionChanged;
                this.Signals.CollectionChanged -= this.OnSignalsChanged;

                this.Events.CollectionChanged -= this.OnDatabaseCollectionChanged;
                this.Messages.CollectionChanged -= this.OnDatabaseCollectionChanged;
                this.Requests.CollectionChanged -= this.OnDatabaseCollectionChanged;
                this.Flags.CollectionChanged -= this.OnDatabaseCollectionChanged;

                this.SelectedItems.CollectionChanged -= this.OnDatabaseCollectionChanged;
                this.SelectedItems.CollectionChanged -= this.OnSelectedItemsChanged;

                this.engagedItems.CollectionChanged -= this.OnEngagedItemsChanged;

                this.VariableClipSets.CollectionChanged -= this.OnDatabaseCollectionChanged;
            }
            else
            {
                this.References.CollectionChanged += this.OnReferencesChanged;

                this.Nodes.CollectionChanged += this.OnDatabaseCollectionChanged;

                this.Signals.CollectionChanged += this.OnDatabaseCollectionChanged;
                this.Signals.CollectionChanged += this.OnSignalsChanged;

                this.Events.CollectionChanged += this.OnDatabaseCollectionChanged;
                this.Messages.CollectionChanged += this.OnDatabaseCollectionChanged;
                this.Requests.CollectionChanged += this.OnDatabaseCollectionChanged;
                this.Flags.CollectionChanged += this.OnDatabaseCollectionChanged;

                this.SelectedItems.CollectionChanged += this.OnDatabaseCollectionChanged;
                this.SelectedItems.CollectionChanged += this.OnSelectedItemsChanged;

                this.engagedItems.CollectionChanged += this.OnEngagedItemsChanged;

                this.VariableClipSets.CollectionChanged += this.OnDatabaseCollectionChanged;
            }
        }

        private void LoadBinaryFormat(string filename)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            SurrogateSelector selector = new SurrogateSelector();
            AddSurrogates(selector);

            formatter.SurrogateSelector = selector;
            formatter.SurrogateSelector.ChainSelector(new LogicSurrogateSelector(DataModel));
            formatter.SurrogateSelector.ChainSelector(new ConditionSurrogateSelector(DataModel));
            FileAttributes fileAttributes = File.GetAttributes(filename);
            FileAccess fileAccess = ((File.GetAttributes(filename) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) ? FileAccess.Read : FileAccess.ReadWrite;

            using (FileStream stream = new FileStream(filename, FileMode.Open, fileAccess))
            {
                formatter.Binder = new NameSpaceRedirection();
                Database database = (Database)formatter.Deserialize(stream);
                FileVersion = database.FileVersion;
                Id = database.Id;

                Nodes = database.Nodes;
                foreach (INode node in Nodes)
                {
                    node.Database = this;
                }

                Root = database.Root;
                Active = database.Active;

                References = database.References;
                foreach (Database db in References)
                {
                    db.Closed += new EventHandler(OnReferenceDatabaseClosed);
                    db.DataModel = DataModel;
                    db.DataModelService = DataModelService;
                }

                foreach (Reference node in Nodes.OfType<Reference>())
                {
                    DatabaseReference reference = References.FirstOrDefault(delegate(DatabaseReference db)
                    {
                        return db.FullyQualifiedFilename.Equals(node.Filename.Value);
                    }
                    );

                    reference.Load();
                    node.ReferencedDatabase = reference;
                }

                Messages = database.Messages;
                Signals = database.Signals;
                Events = database.Events;
                Flags = database.Flags;
                this.Requests = database.Requests;
                SelectedItems = database.SelectedItems;
                VariableClipSets = database.VariableClipSets;
                SetHandlers();

                this.Nodes.Clear();
                foreach (INode node in this.AllNodesFromRoot(this.Root))
                {
                    this.Nodes.Add(node);
                    node.OnLoadComplete();
                }

                database.SignalNameChanged = SignalNameChanged;// We need to make sure the two databases are in sync.  Kill me.

                // HACK
                // Need to resolve transitions due to early copy and paste bugs.
                foreach (ITransitional transitional in this.Nodes.OfType<ITransitional>())
                {
                    foreach (Transition transition in transitional.TransitionOrder)
                    {
                        transition.Parent = this.Find(transition.Source.Parent) as IAutomaton;
                    }

                    Motiontree motionTree = transitional as Motiontree;
                    if (motionTree == null)
                    {
                        continue;
                    }

                    motionTree.ResolveConnectionsAfterLoad();
                }
            }
        }

        private void SaveBinaryFormat(string filename)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            SurrogateSelector selector = new SurrogateSelector();
            AddSurrogates(selector);
            formatter.SurrogateSelector = selector;
            formatter.SurrogateSelector.ChainSelector(new LogicSurrogateSelector(DataModel));
            formatter.SurrogateSelector.ChainSelector(new ConditionSurrogateSelector(DataModel));
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, this);

                FileStream fs = new FileStream(FullyQualifiedFilename, FileMode.Create);
                stream.WriteTo(fs);
                fs.Close();
            }
        }

        private void LoadXmlFormat(string filename)
        {
            if (!File.Exists(filename))
            {
                throw new FileNotFoundException("Unable to located loading file", filename);
            }

            foreach (Signal signal in this.Signals)
            {
                if (signal == null)
                {
                    continue;
                }

                signal.PropertyChanged -= this.OnSignalPropertyChanged;
            }

            this.Signals.Clear();
            this.Events.Clear();
            this.Flags.Clear();
            this.Requests.Clear();
            this.VariableClipSets.Clear();
            Guid activeId = Guid.Empty;
            using (XmlReader reader = XmlReader.Create(filename))
            {
                while (reader.MoveToContent() != XmlNodeType.None)
                {
                    if (!reader.IsStartElement())
                    {
                        reader.Read();
                        continue;
                    }

                    if (string.CompareOrdinal(reader.Name, "database") == 0)
                    {
                        activeId = Guid.ParseExact(reader.GetAttribute("active"), "D");
                        reader.Read();
                    }
                    else if (string.CompareOrdinal(reader.Name, "real") == 0)
                    {
                        this.Signals.Add(new RealSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "animation") == 0)
                    {
                        this.Signals.Add(new AnimationSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "clip") == 0)
                    {
                        this.Signals.Add(new ClipSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "expression") == 0)
                    {
                        this.Signals.Add(new ExpressionSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "filter") == 0)
                    {
                        this.Signals.Add(new FilterSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "frame") == 0)
                    {
                        this.Signals.Add(new FrameSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "parameterizedmotion") == 0)
                    {
                        this.Signals.Add(new ParameterizedMotionSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "node") == 0)
                    {
                        this.Signals.Add(new NodeSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "bool") == 0)
                    {
                        this.Signals.Add(new BooleanSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "int") == 0)
                    {
                        this.Signals.Add(new IntSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "hashstring") == 0)
                    {
                        this.Signals.Add(new HashStringSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "tag") == 0)
                    {
                        this.Signals.Add(new TagSignal(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "event") == 0)
                    {
                        this.Events.Add(new LogicEvent(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "flag") == 0)
                    {
                        this.Flags.Add(new Flag(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "request") == 0)
                    {
                        this.Requests.Add(new Request(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "clipset") == 0)
                    {
                        this.VariableClipSets.Add(new VariableClipSet(reader));
                    }
                    else if (string.CompareOrdinal(reader.Name, "root") == 0)
                    {
                        if (!reader.IsEmptyElement)
                        {
                            reader.ReadStartElement();
                            reader.MoveToContent();
                            INode newNode = this.xmlFactory.CreateNode(reader, this, null);
                            if (newNode != null)
                            {
                                this.Root = newNode as ITransitional;
                                foreach (INode node in this.AllNodesFromRoot(this.Root))
                                {
                                    this.Nodes.Add(node);
                                    node.OnLoadComplete();
                                    ITransitional transitional = node as ITransitional;
                                    if (transitional != null && node.Id == activeId)
                                    {
                                        this.Active = transitional;
                                    }
                                }

                                foreach (INode node in this.Nodes)
                                {
                                    ITransitionalParent transitionalParent = node as ITransitionalParent;
                                    if (transitionalParent == null)
                                    {
                                        continue;
                                    }

                                    foreach (Transition transition in transitionalParent.Transitions)
                                    {
                                        transition.Content.ResolvePoints(this, transition.Source, transition.Dest);
                                    }
                                }

                            }
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }
                    else
                    {
                        reader.Read();
                    }
                }
            }

            foreach (Signal signal in this.Signals)
            {
                if (signal == null)
                {
                    continue;
                }

                signal.PropertyChanged += this.OnSignalPropertyChanged;
            }


        }

        private IEnumerable<INode> AllNodesFromRoot(INode root)
        {
            if (root == null)
            {
                yield break;
            }

            yield return root;
            ITransitional transitional = root as ITransitional;
            if (transitional == null)
            {
                yield break;
            }

            foreach (INode child in transitional.Children)
            {
                foreach (INode node in this.AllNodesFromRoot(child))
                {
                    yield return node;
                }
            }
        }

        public void SaveXmlFormat(string filename)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;
            using (XmlWriter writer = XmlWriter.Create(filename, settings))
            {
                writer.WriteStartElement("database");

                writer.WriteAttributeString(
                    "active",
                    this.Active.Id.ToString("D", CultureInfo.InvariantCulture));

                writer.WriteStartElement("signals");
                foreach (Signal signal in this.Signals)
                {
                    signal.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("events");
                foreach (LogicEvent logicEvent in this.Events)
                {
                    logicEvent.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("flags");
                foreach (Flag flag in this.Flags)
                {
                    flag.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("requests");
                foreach (Request request in this.Requests)
                {
                    request.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("clipsets");
                foreach (VariableClipSet variableClipSet in this.VariableClipSets)
                {
                    variableClipSet.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("root");
                if (this.root != null)
                {
                    this.Root.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        }

        public void SaveXmlFormatSelected(string filename)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;
            using (XmlWriter writer = XmlWriter.Create(filename, settings))
            {
                writer.WriteStartElement("database");

                writer.WriteAttributeString(
                    "active",
                    this.Active.Id.ToString("D", CultureInfo.InvariantCulture));

                writer.WriteStartElement("signals");
                foreach (Signal signal in this.Signals)
                {
                    signal.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("events");
                foreach (LogicEvent logicEvent in this.Events)
                {
                    logicEvent.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("flags");
                foreach (Flag flag in this.Flags)
                {
                    flag.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("requests");
                foreach (Request request in this.Requests)
                {
                    request.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("clipsets");
                foreach (VariableClipSet variableClipSet in this.VariableClipSets)
                {
                    variableClipSet.Serialise(writer);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("root");
                if (this.active is Motiontree)
                {
                    var nodes = from s in this.SelectedItems.OfType<INode>()
                                select s;

                    var connections = from s in this.SelectedItems.OfType<Connection>()
                                      select s;

                    (this.active as Motiontree).SerialiseSet(writer, nodes, connections, false);
                }
                else if (this.active is StateMachine)
                {
                    var nodes = from s in this.SelectedItems.OfType<INode>()
                                select s;

                    var transitions = from s in this.SelectedItems.OfType<Transition>()
                                      select s;

                    (this.active as StateMachine).SerialiseSet(writer, nodes, transitions, false);
                }
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        }

        public static Guid GetId(Dictionary<Guid, Guid> map, Guid current)
        {
            Guid newGuid;
            if (map.TryGetValue(current, out newGuid))
            {
                return newGuid;
            }

            return Guid.NewGuid();
        }

        public static Guid GetId(Dictionary<Guid, Guid> map, Guid current, Guid fallback)
        {
            Guid newGuid;
            if (map.TryGetValue(current, out newGuid))
            {
                return newGuid;
            }

            return fallback;
        }
        #endregion
    }
}
