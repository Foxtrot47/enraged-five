﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Rage.Move.Utils
{
    public class ByteBuffer
    {
        byte[] storage;
        int length = 0;
        int readpos = 0;

        [StructLayout(LayoutKind.Explicit)]
        struct Union
        {
            [FieldOffset(0)]
            public float f;
            [FieldOffset(0)]
            public uint u;
        }

        public ByteBuffer(uint size)
        {
            this.storage = new byte[size];
        }

        public ByteBuffer()
        {
        }

        public byte[] Storage
        {
            get { return this.storage; }
        }

        public int Length
        {
            get { return this.length; }
            set { this.length = value; }
        }

        public int ReadPosition
        {
            get { return this.readpos; }
            set { this.readpos = value; }
        }

        public void Create(int size)
        {
            this.storage = new byte[size];
            Reset();
        }

        public bool IsReady
        {
            get
            {
                if (this.length == 0)
                    return false;
                int pos = this.readpos;
                this.readpos = 0;
                int size = ReadInt32();
                this.readpos = pos;
                return size == (this.length - 4);
            }
        }

        public void BeginRead()
        {
            this.readpos = 0;
        }

        public void BeginWrite()
        {
            this.length = 0;
        }

        public void BeginTcpBuffer()
        {
            BeginWrite();
            WriteUInt32(0); // size of the buffer
        }

        public void EndTcpBuffer()
        {
            int len = this.length;
            this.length = 0;
            WriteUInt32((uint)len - 4);
            this.length = len;
        }

        public void Reset()
        {
            this.length = 0;
            this.readpos = 0;
        }

        public bool ReadBool()
        {
            return ReadUInt8() != 0;
        }

        public byte ReadUInt8()
        {
            CheckBuffer();
            if (this.Storage.Length <= this.ReadPosition)
                return 0;

            return this.Storage[this.ReadPosition++];
        }

        public sbyte ReadInt8()
        {
            return (sbyte)ReadUInt8();
        }

        public ushort ReadUInt16()
        {
            CheckBuffer();
            if (this.Storage.Length <= this.ReadPosition)
                return 0;

            ushort result = this.Storage[this.ReadPosition];
            ++this.ReadPosition;
            result |= (ushort)(this.Storage[this.ReadPosition] << 8);
            ++this.ReadPosition;

            return result;
        }

        public short ReadInt16()
        {
            return (short)ReadUInt16();
        }

        public uint ReadUInt32()
        {
            CheckBuffer();
            if (this.Storage.Length <= this.ReadPosition)
                return 0;

            uint result = this.Storage[this.ReadPosition];
            ++this.ReadPosition;
            result |= (uint)(this.Storage[this.ReadPosition] << 8);
            ++this.ReadPosition;
            result |= (uint)(this.Storage[this.ReadPosition] << 16);
            ++this.ReadPosition;
            result |= (uint)(this.Storage[this.ReadPosition] << 24);
            ++this.ReadPosition;

            return result;
        }

        public uint PeekReadUInt32(int offset)
        {
            CheckBuffer();
            if (this.Storage.Length + offset < 4)
                return 0;
            else
                return BitConverter.ToUInt32(this.Storage, offset);
        }

        public int ReadInt32()
        {
            return (int)ReadUInt32();
        }

        public int PeekReadInt32(int offset)
        {
            return (int)PeekReadUInt32(offset);
        }

        public float ReadFloat()
        {
            Union x;
            x.f = 0.0f;
            x.u = ReadUInt32();
            return x.f;
        }

        public string ReadString()
        {
            CheckBuffer();
            if (this.Storage.Length <= this.ReadPosition)
                return "";

            uint s = ReadUInt32();
            if (s != 0)
            {
                uint len = s;                       // len includes NULL byte
                char[] str = new char[len-1];
                for (uint i = 0; i < len-1; ++i)
                    str[i] = Convert.ToChar(ReadUInt8());

                byte nullChar = ReadUInt8();        // ignore the NULL byte
                Debug.Assert(nullChar == '\0');

                return new String(str);
            }

            return "";
        }

        public Guid ReadGuid()
        {
            CheckBuffer();
            byte[] guidBytes = new byte[16];
            for (int i = 0; i < 16; ++i)
                guidBytes[i] = ReadUInt8();
            return new Guid(guidBytes);
        }

        public Guid PeekReadGuid(int offset)
        {
            CheckBuffer();
            byte[] guidBytes = new byte[16];
            Buffer.BlockCopy(Storage, offset, guidBytes, 0, 16);
            return new Guid(guidBytes);
        }

        public void WriteBool(bool value)
        {
            WriteUInt8((byte)(value == true ? 1 : 0));
        }

        public void WriteUInt8(byte value)
        {
            this.Storage[this.Length] = value;
            ++this.Length;
            CheckBuffer();
        }

        public void WriteInt8(sbyte value)
        {
            WriteUInt8((byte)value);
        }

        public void WriteUInt16(ushort value)
        {
            this.Storage[this.Length] = (byte)value;
            ++this.Length;
            this.Storage[this.Length] = (byte)(value >> 8);
            ++this.Length;
            CheckBuffer();
        }

        public void WriteInt16(short value)
        {
            WriteUInt16((ushort)value);
        }

        public void WriteUInt32(uint value)
        {
            this.Storage[this.Length] = (byte)value;
            ++this.Length;
            this.Storage[this.Length] = (byte)(value >> 8);
            ++this.Length;
            this.Storage[this.Length] = (byte)(value >> 16);
            ++this.Length;
            this.Storage[this.Length] = (byte)(value >> 24);
            ++this.Length;
            CheckBuffer();
        }

        public void WriteInt32(int value)
        {
            WriteUInt32((uint)value);
        }

        public void Write(byte[] value)
        {
            for (int i = 0; i < value.Length; ++i)
                WriteUInt8(value[i]);
        }

        public void WriteFloat(float value)
        {
            Union x;
            x.u = 0;
            x.f = value;
            WriteUInt32(x.u);
        }

        public void WriteString(string value)
        {
            if (value == null || value.Length == 0)
            {
                WriteUInt32(0);
            }
            else
            {
                value = value.Trim();
                int len = value.Length;
                WriteUInt32((uint)len + 1);         // length includes a NULL byte
                for (int i = 0; i < len; ++i)
                    this.Storage[this.Length++] = Convert.ToByte(value[i]);
                this.Storage[this.Length] = 0x0;
                ++this.Length;
            }
            CheckBuffer();
        }

        public void WriteGuid(Guid value)
        {
            Write(value.ToByteArray());
        }

        public void OverwriteBool(uint position, bool value)
        {
            OverwriteUInt8(position, (byte)(value == true ? 1 : 0));
        }

        public void OverwriteUInt8(uint position, byte value)
        {
            Debug.Assert(position <= this.Length - 1);
            this.Storage[position] = value;
        }

        public void OverwriteInt8(uint position, sbyte value)
        {
            OverwriteUInt8(position, (byte)value);
        }

        public void OverwriteUInt16(uint position, ushort value)
        {
            Debug.Assert(position <= this.Length - 2);
            this.Storage[position + 0] = (byte)value;
            this.Storage[position + 1] = (byte)(value >> 8);
        }

        public void OverwriteInt16(uint position, short value)
        {
            OverwriteUInt16(position, (ushort)value);
        }

        public void OverwriteUInt32(uint position, uint value)
        {
            Debug.Assert(position <= this.Length - 4);
            this.Storage[position + 0] = (byte)value;
            this.Storage[position + 1] = (byte)(value >> 8);
            this.Storage[position + 2] = (byte)(value >> 16);
            this.Storage[position + 3] = (byte)(value >> 24);
        }

        public void OverwriteInt32(uint position, int value)
        {
            OverwriteUInt32(position, (uint)value);
        }

        public void Overwrite(uint position, byte[] value)
        {
            for(uint i = 0; i < value.Length; i++)
            {
                OverwriteUInt8(position + i, value[i]);
            }
        }

        public void OverwriteFloat(uint position, float value)
        {
            Union x;
            x.u = 0;
            x.f = value;
            OverwriteUInt32(position, x.u);
        }

        void CheckBuffer()
        {
            Debug.Assert(this.length <= this.Storage.Length);
        }
    }
}