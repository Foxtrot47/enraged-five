﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;

using Rage.Move.Core;

namespace Rage.Move.Core
{
    public enum Collapsability
    {
        Collapsed,      // connected anchors will still be visible
        Open,           // everything is visible
        Closed,         // nothing is visible...
    }
}

namespace Rage.Move.Core.Dg
{
    public delegate void CollapsabilityChangedEventHandler(Collapsability collapsibility);
    public delegate void NameChangedEventHandler(string name);

    public class LogicNodeBase : New.Move.Core.Logic, ILayerable
    {
        public LogicNodeBase(string type)
        {
            Name = type;
            //Id = Guid.NewGuid();
            //Type = type;
        }

        //string _Name;

        //[XmlAttribute]
        //public string Name { get { return _Name; } set { _Name = value; OnNameChanged(); } }
        

        Guid _Layer;
        public Guid Layer 
        {
            get { return _Layer; }
            set
            {
                if (_Layer != value)
                {
                    _Layer = value;
                    OnLayerChanged();
                }
            }
        }

        public event LayerChangedEventHandler LayerChanged;

        void OnLayerChanged()
        {
            if (LayerChanged != null)
                LayerChanged();
        }

        //[XmlAttribute]
        

        
        

        //[XmlIgnore]
        //public MotionTree Parent { get; set; }

        protected ObservableCollection<IAnchor> _Anchors = new ObservableCollection<IAnchor>();
        public virtual ObservableCollection<IAnchor> Anchors { get { return _Anchors; } set { _Anchors = value; } }

        //protected ObservableCollection<IArrayItem> _Arrays = new ObservableCollection<IArrayItem>();
        //public virtual ObservableCollection<IArrayItem> Arrays { get { return _Arrays; } set { _Arrays = value; } }

        //protected ObservableCollection<IMessage> _Messages = new ObservableCollection<IMessage>();
        //public virtual ObservableCollection<IMessage> Messages { get { return _Messages; } set { _Messages = value; } }

        public IAnchor GetAnchor(string name, int index)
        {
            IEnumerable<IAnchor> item =
                from anchor in _Anchors where anchor.Name == name select anchor;

            if (item.Count() == 0)
                return null;

            if (item.Count() <= index)
                return null;

            return item.ElementAt(index);
        }

        public IAnchor GetAnchor(string name)
        {
            return GetAnchor(name, 0);
        }

        public IAnchor GetAnchor(Guid id)
        {
            IEnumerable<IAnchor> items =
                from anchor in _Anchors where anchor.Id == id select anchor;

            if (items.Count() == 0)
                return null;
            return items.ElementAt(0);
        }

        public List<IAnchor> GetAnchors(string name)
        {
            IEnumerable<IAnchor> items =
                from anchor in _Anchors where anchor.Name == name select anchor;

            return new List<IAnchor>(items);
        }

        public bool HasAnchor(Guid id)
        {
            IEnumerable<IAnchor> item =
                from anchor in _Anchors where anchor.Id == id select anchor;

            return item.Count() != 0;
        }

        public int GetAnchorIndex(string name, Guid id)
        {
            List<IAnchor> items = GetAnchors(name);
            IAnchor anchor = items.Find(
                delegate(IAnchor a) { return a.Id == id; });

            int index = items.IndexOf(anchor);
            return index;
        }

        /*
        public event NameChangedEventHandler NameChanged;
        void OnNameChanged()
        {
            if (NameChanged != null)
                NameChanged(_Name);
        }
         */

        public event CollapsabilityChangedEventHandler CollapsabilityChanged;
        void OnCollapsabilityChanged()
        {
            if (CollapsabilityChanged != null)
                CollapsabilityChanged(_Collapsability);
        }

        public void Remove()
        {
            //Parent.Children.Remove(this);
        }

        public void OnPositionChanged(Point point) { Position = point; }

        Collapsability _Collapsability = Collapsability.Open;
        public Collapsability Collapsability
        {
            get { return _Collapsability; }
            set { _Collapsability = value; }
        }

        public void OnCollapserToggle()
        {
            switch (_Collapsability)
            {
                case Collapsability.Collapsed:
                    _Collapsability = Collapsability.Closed;
                    break;
                case Collapsability.Open:
                    _Collapsability = Collapsability.Collapsed;
                    break;
                case Collapsability.Closed:
                    _Collapsability = Collapsability.Open;
                    break;
            }

            OnCollapsabilityChanged();
        }
    }

    public delegate void LogicNodePropertyChangedEventHandler();

    [Serializable]
    public class LogicNode : LogicNodeBase
    {
        public LogicNode(string type)
            : base(type) 
        {
            //RegisterProperties();
        }

        public LogicNode()
            : base(null) 
        {
            //RegisterProperties();
        }

        /*
    protected void RegisterProperty(Rage.Move.Core.Dg.Properties.Property property)
    {
        property.PropertySourceChanged +=
            new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(OnLogicNodePropertySourceChanged);


        System.Reflection.PropertyInfo[] properties =
            GetType().GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
        foreach (System.Reflection.PropertyInfo property in properties)
        {
            if (property.PropertyType.GetInterface("Property") != null)
            {
                Rage.Move.Core.Dg.Properties.Property prop =
                    (Rage.Move.Core.Dg.Properties.Property)property.GetValue(this, null);

                prop.PropertySourceChanged += 
                    new Rage.Move.Core.Dg.Properties.PropertySourceChangedEventHandler(OnLogicNodePropertySourceChanged);
            }
        }
             
    }
    */
        public event LogicNodePropertyChangedEventHandler LogicNodePropertyChanged;
        void OnLogicNodePropertySourceChanged(Rage.Move.Core.Dg.Properties.Property property)
        {
            if (LogicNodePropertyChanged != null)
                LogicNodePropertyChanged();

#if disabled
            IConnection[] connections = new IConnection[Parent.Connections.Count];
            Parent.Connections.CopyTo(connections, 0);
            
            foreach (IConnection connection in connections)
            {
                if (connection.Sink == property.Id || connection.Source == property.Id)
                {
                    Parent.Connections.Remove(connection);
                }
            }
#endif
        }

        protected ObservableCollection<IAttribute> _Attributes = new ObservableCollection<IAttribute>();
        public ObservableCollection<IAttribute> Attributes { get { return _Attributes; } set { _Attributes = value; } }

        public IAttribute GetAttribute(string name)
        {
            IEnumerable<IAttribute> item =
                from attribute in _Attributes where attribute.Name == name select attribute;

            if (item.Count() == 0)
                return null;

            return item.ElementAt(0);
        }
    }

/*
    [Serializable]
    public class LogicStateNode : LogicNodeBase
    {
        public LogicStateNode(string type)
            : base(type) { }

        public LogicStateNode()
            : base(null) { }

        StateMachine _StateMachine;
        public StateMachine StateMachine 
        {
            get { return _StateMachine; }
            set
            {
                if (_StateMachine != null)
                {
#if disabled
                    _StateMachine.Deactivate -= new ActivatedEventHandler(
                        delegate() { Runtime.Database.Active = Parent; });
                    _StateMachine.Activate -= new ActivatedEventHandler(
                        delegate() { Runtime.Database.Active = StateMachine; });
                    _StateMachine.LogicalParent = null;
#endif
                }

                _StateMachine = value;
                
                if (_StateMachine != null)
                {
#if disabled
                    _StateMachine.LogicalParent = this;
                    _StateMachine.Activate += new ActivatedEventHandler(
                        delegate() { Runtime.Database.Active = StateMachine; });
                    _StateMachine.Deactivate += new ActivatedEventHandler(
                        delegate() { Runtime.Database.Active = Parent; });
#endif
                }
            }
        }

        [XmlIgnore]
        public bool HasInputs { get { return _Anchors.OfType<AnchorSource>().Count() != 0; } }

        public void OnActivate() { StateMachine.OnActivate(); }
        public void OnDeactivate() { StateMachine.OnDeactivate(); }
    }

    public class LogicInputNode : LogicNodeBase
    {
        public LogicInputNode(Guid id)
            : base("LogicInputNode") 
        {
            //Id = id;
        }
    }
 */
}
