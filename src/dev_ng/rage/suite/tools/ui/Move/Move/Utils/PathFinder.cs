﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Rage.Move.Utils
{
    public enum Edge
    {
        None,
        Left,
        Top,
        Right,
        Bottom
    }

    public class PathFinderInfo
    {
        double left;
        double top;
        Size size;
        Point position;
        Edge orientation;

        public double Left 
        {
            get { return this.left; }
            set { this.left = value; }
        }

        public double Top
        {
            get { return this.top; }
            set { this.top = value; }
        }

        public Size Size
        {
            get { return this.size; }
            set { this.size = value; }
        }

        public Point Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        public Edge Orientation
        {
            get { return this.orientation; }
            set { this.orientation = value; }
        }
    }

    public class PathFinder
    {
        private static class Const
        {
            public static int Margin = 0;
        }

        public static List<Point> GetConnection(PathFinderInfo sourceinfo, PathFinderInfo sinkinfo, bool showlast)
        {
            List<Point> points = new List<Point>();

            Rect source = GetRectWithMargin(sourceinfo, Const.Margin);
            Rect sink = GetRectWithMargin(sinkinfo, Const.Margin);

            Point start = GetOffsetPoint(sourceinfo, source);
            Point end = GetOffsetPoint(sinkinfo, sink);

            // hack
            if (start == end)
            {
                points.Remove(end);
                points.Add(new Point(start.X, start.Y + 1));
            }

            points.Add(start);
            points.Add(end);

            return points;
        }

        public static List<Point> GetConnection(PathFinderInfo source, Point sinkpoint/*, Orientation orientation*/)
        {
            List<Point> points = new List<Point>();

            //Rect sourcerect = GetRectWithMargin(source, 10);
            Point start = source.Position; // GetOffsetPoint(source, sourcerect);
            Point end = sinkpoint;

            points.Add(start);
            points.Add(end);

            return points;
        }

        static List<Point> Optimize(List<Point> oldpoints, Rect[] rectangles, Edge sourceorient, Edge sinkorient)
        {
            List<Point> points = new List<Point>();

            return points;
        }

        static Edge CalcOrientation(Point p1, Point p2)
        {
            if (p1.X == p2.X)
            {
                if (p1.Y != p2.Y)
                    return Edge.Bottom;
                else
                    return Edge.Top;
            }
            else
            if (p1.Y == p2.Y)
            {
                if (p1.X >= p2.X)
                    return Edge.Right;
                else
                    return Edge.Left;
            }

            throw new Exception("Failed to calculate orientation");
        }

        static Point GetNearestNeighbourSource(PathFinderInfo sourceinfo, Point end, Rect source, Rect sink, out bool flag)
        {
            Point point1, point2;
            GetNeighbourCorners(sourceinfo.Orientation, source, out point1, out point2);

            if (sink.Contains(point1))
            {
                flag = false;
                return point2;
            }

            if (sink.Contains(point2))
            {
                flag = true;
                return point1;
            }

            if (Distance(point1, end) <= Distance(point2, end))
            {
                flag = true;
                return point1;
            }
            else
            {
                flag = false;
                return point2;
            }
        }

        static Point GetNearestNeighbourSource(PathFinderInfo sourceinfo, Point end, Rect source, out bool flag)
        {
            Point point1, point2;
            GetNeighbourCorners(sourceinfo.Orientation, source, out point1, out point2);

            if (Distance(point1, end) <= Distance(point2, end))
            {
                flag = true;
                return point1;
            }
            else
            {
                flag = false;
                return point2;
            }
        }

        static Point GetNearestVisibleNeighbourSink(Point current, Point end, PathFinderInfo sinkinfo, Rect source, Rect sink)
        {
            Point point1, point2;
            GetNeighbourCorners(sinkinfo.Orientation, sink, out point1, out point2);

            bool flag = IsPointVisible(current, point2, new Rect[] { source, sink });

            if (IsPointVisible(current, point1, new Rect[] { source, sink }))
            {
                if (flag)
                {
                    if (sink.Contains(point1))
                        return point2;

                    if (sink.Contains(point2))
                        return point1;

                    if (Distance(point1, end) <= Distance(point2, end))
                        return point1;
                    else
                        return point2;
                }
                else
                {
                    return point1;
                }
            }
            else
            {
                if (flag)
                    return point2;
                else
                    return new Point(double.NaN, double.NaN);
            }
        }

        static bool IsPointVisible(Point from, Point target, Rect[] rects)
        {
            foreach (Rect rect in rects)
            {
                if (RectIntersectsLine(rect, from, target))
                    return false;
            }

            return true;
        }

        static bool IsRectVisible(Point from, Rect target, Rect[] rects)
        {
            if (IsPointVisible(from, target.TopLeft, rects))
                return true;

            if (IsPointVisible(from, target.TopRight, rects))
                return true;

            if (IsPointVisible(from, target.BottomLeft, rects))
                return true;

            if (IsPointVisible(from, target.BottomRight, rects))
                return true;

            return false;
        }

        static bool RectIntersectsLine(Rect rect, Point start, Point end)
        {
            rect.Inflate(-1, -1);
            return rect.IntersectsWith(new Rect(start, end));
        }

        static void GetOppositeCorners(Edge orientation, Rect rect, out Point point1, out Point point2)
        {
            switch (orientation)
            {
                case Edge.Left:
                    point1 = rect.TopRight; point2 = rect.BottomRight;
                    break;
                case Edge.Top:
                    point1 = rect.BottomLeft; point2 = rect.BottomRight;
                    break;
                case Edge.Right:
                    point1 = rect.TopLeft; point2 = rect.BottomLeft;
                    break;
                case Edge.Bottom:
                    point1 = rect.TopLeft; point2 = rect.TopRight;
                    break;
                default:
                    throw new Exception("Opposite corners not found");
            }
        }

        static void GetNeighbourCorners(Edge orientation, Rect rect, out Point point1, out Point point2)
        {
            switch (orientation)
            {
                case Edge.Left:
                    point1 = rect.TopLeft; point2 = rect.BottomLeft;
                    break;
                case Edge.Top:
                    point1 = rect.TopLeft; point2 = rect.TopRight;
                    break;
                case Edge.Right:
                    point1 = rect.TopRight; point2 = rect.BottomRight;
                    break;
                case Edge.Bottom:
                    point1 = rect.BottomLeft; point2 = rect.BottomRight;
                    break;
                default:
                    throw new Exception("Neighbour corners not found");
            }
        }

        static double Distance(Point p1, Point p2)
        {
            return Point.Subtract(p1, p2).Length;
        }

        static Rect GetRectWithMargin(PathFinderInfo info, double margin)
        {
            Rect rect = new Rect(info.Left, info.Top, info.Size.Width, info.Size.Height);
            rect.Inflate(margin, margin);

            return rect;
        }

        static Point GetOffsetPoint(PathFinderInfo info, Rect rect)
        {
            Point offset = new Point();

            switch (info.Orientation)
            {
                case Edge.Left:
                    offset = new Point(rect.Left, info.Position.Y);
                    break;
                case Edge.Top:
                    offset = new Point(info.Position.X, rect.Top);
                    break;
                case Edge.Right:
                    offset = new Point(rect.Right, info.Position.Y);
                    break;
                case Edge.Bottom:
                    offset = new Point(info.Position.X, rect.Bottom);
                    break;
                default:
                    break;
            }

            return offset;
        }

        static void CheckPathEnd(PathFinderInfo source, PathFinderInfo sink, bool showlast, List<Point> points)
        {
        }

        static Edge OppositeOrientation(Edge orientation)
        {
            switch (orientation)
            {
                case Edge.Left: return Edge.Right;
                case Edge.Top: return Edge.Bottom;
                case Edge.Right: return Edge.Left;
                case Edge.Bottom: return Edge.Top;
                default:
                    return Edge.Top;
            }
        }
    }
}

