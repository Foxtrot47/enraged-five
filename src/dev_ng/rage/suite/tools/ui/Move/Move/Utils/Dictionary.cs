﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Rage.Move.Utils
{
    public class ReadOnlyDictionary<KeyT, ValueT> : Dictionary< KeyT, ValueT >, IDictionary<KeyT, ValueT>
    {
        protected IDictionary<KeyT, ValueT> dict;

        public ReadOnlyDictionary(IDictionary<KeyT, ValueT> dict)
            :   base ( dict )
        {
            this.dict = dict;
        }

        public void Add(KeyValuePair<KeyT, ValueT> pair)
        {
            throw new InvalidOperationException("Collection is readonly");
        }

        public new void Add(KeyT key, ValueT value)
        {
            throw new InvalidOperationException("Collection is readonly");
        }

        public new void Clear()
        {
            throw new InvalidOperationException("Collection is readonly");
        }

        public new bool ContainsKey(KeyT key)
        {
            return dict.ContainsKey(key);
        }

        public bool Contains(KeyValuePair<KeyT, ValueT> pair)
        {
            return dict.Contains(pair);
        }

        /*
        public new IEnumerator<KeyValuePair<KeyT, ValueT> > GetEnumerator()
        {
            return (IEnumerator<KeyValuePair<KeyT, ValueT>>) new ReadOnlyDictionaryEnumerator<KeyT, ValueT>(dict.GetEnumerator());
        }
         */
        public new IDictionaryEnumerator GetEnumerator()
        {
            return new ReadOnlyDictionaryEnumerator<KeyT, ValueT>(dict.GetEnumerator());
        }

        public bool Remove(KeyValuePair<KeyT, ValueT> pair)
        {
            throw new InvalidOperationException("Collection is readonly");
        }

        public new bool Remove(KeyT key)
        {
            throw new InvalidOperationException("Collection is readonly");
        }

        public new ValueT this[KeyT key]
        {
            get { return dict[key]; }
            set
            {
                throw new InvalidOperationException("Collection is readonly");
            }
        }

        public new bool TryGetValue(KeyT key, out ValueT value)
        {
            return dict.TryGetValue(key, out value);
        }

        public new ICollection<KeyT> Keys
        {
            get 
            {
                KeyT[] keys = new KeyT[dict.Keys.Count];
                dict.Keys.CopyTo(keys, 0);
                return new ReadOnlyCollection<KeyT>(keys); 
            }
        }

        public new ICollection<ValueT> Values
        {
            get 
            {
                ValueT[] values = new ValueT[dict.Values.Count];
                dict.Values.CopyTo(values, 0);
                return new ReadOnlyCollection<ValueT>(values); 
            }
        }

        public void CopyTo(KeyValuePair<KeyT, ValueT>[] array, int count)
        {
        }

        public bool IsReadOnly
        {
            get { return true; }
        }
    }

    public class ReadOnlyDictionaryEnumerator<KeyT, ValueT> : IDictionaryEnumerator
    {
        protected IDictionaryEnumerator enumerator;

        public ReadOnlyDictionaryEnumerator(IEnumerator<KeyValuePair<KeyT, ValueT> > enumerator)
        {
            this.enumerator = (IDictionaryEnumerator)enumerator;
        }

        public bool MoveNext()
        {
            return enumerator.MoveNext();
        }

        public void Reset()
        {
            enumerator.Reset();
        }

        public void Remove()
        {
            throw new InvalidOperationException("Collection is readonly");
        }

        public object Current
        {
            get { return enumerator.Current; }
        }

        public DictionaryEntry Entry
        {
            get { return enumerator.Entry; }
        }

        public object Key
        {
            get { return enumerator.Key; }
        }

        public object Value
        {
            get { return enumerator.Value; }
            set
            {
                throw new InvalidOperationException("Collection is readonly");
            }
        }
    }
}
