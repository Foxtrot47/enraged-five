﻿using System.Collections;
using System.Collections.Specialized;

namespace Move.Core.Restorable
{
    internal sealed class AddCollectionItemsAction : IRestorableAction
    {
        public void Undo()
        {
            using (new WithoutCollectionChanged(List, CollectionChanged))
            {
                foreach (object arg in NewItems)
                {
                    List.RemoveAt(NewStartingIndex);
                }
            }
        }

        public void Redo()
        {
            using (new WithoutCollectionChanged(List, CollectionChanged))
            {
                foreach (object arg in NewItems)
                {
                    List.Insert(NewStartingIndex, arg);
                }
            }
        }

        internal NotifyCollectionChangedEventHandler CollectionChanged { get; set; }
        public IList NewItems { get; set; }
        public int NewStartingIndex { get; set; }

        public IList List { get; set; }
    }
}