﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class Boolean : SignalBase, ISerializable
    {
        public Boolean(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public Boolean(BooleanSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Boolean"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Boolean(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "booleansignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new Boolean(new BooleanSignal());
            (clone as Boolean).AdditionalText = this.AdditionalText;
            (clone as Boolean).Database = this.Database;
            (clone as Boolean).Enabled = this.Enabled;
            (clone as Boolean).Name = this.Name;
            (clone as Boolean).Position = this.Position;
            (clone as Boolean).Signal = this.Signal;

            return (ISelectable)clone;
        }
    }
}