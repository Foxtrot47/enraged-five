﻿using System.Windows;
using System.Xml;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Rockstar.MoVE.Framework.DataModel
{
    public class CurveStackItem : IOperatorItem
    {
        public CurveStackItem(Curve curve)
        {
            Curve = curve;
        }

        Curve Curve;

        struct Segment
        {
            public CurveSegment Start;
            public CurveSegment End;
        }

        public void ExportToXML(PargenXmlNode parentNode)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "CurveOperator");

            itemNode.AppendValueElementNode("MinClamp", "value", Curve.ClampMinimum.ToString());
            itemNode.AppendValueElementNode("MaxClamp", "value", Curve.ClampMaximum.ToString());

            var keyframesNode = itemNode.AppendChild("Keyframes");

            //we just have a list of points here. we need the proper data
            Segment[] segments = new Segment[Curve.Points.Length - 1];

            for (int i = 1; i < Curve.Points.Length; ++i)
            {
                segments[i - 1].Start = Curve.Points[i - 1];
                segments[i - 1].End = Curve.Points[i];
            }

            foreach (Segment segment in segments)
            {
                var keyframeItemNode = keyframesNode.AppendChild("Item");

                keyframeItemNode.AppendValueElementNode("Key", "value", segment.End.X.ToString());

                double m = (segment.End.Y - segment.Start.Y) / (segment.End.X - segment.Start.X);
                keyframeItemNode.AppendValueElementNode("M", "value", m.ToString());

                double b = segment.End.Y - m * segment.End.X;
                keyframeItemNode.AppendValueElementNode("B", "value", b.ToString());
            }
        }
    }
}