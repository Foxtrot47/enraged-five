﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

//using Move;
using Rage.Move.Core;
using Rage.Move.Core.Dag;
using Rage.Move.Core.Dg;

namespace Rage.Move
{
    public static class Logic
    {
        /*
        public static ILogicNode GetNode(this DiagramNode source)
        {
            Diagram diagram = DiagramViewer.Instance.Diagram;
            DiagramLogic logic = diagram.Logic;
            
            return logic.GetLogicNode(source);
        }
         */

        public static DiagramState Create(this New.Move.Core.Motiontree source)
        {
            DiagramState instance = new DiagramState(source.Id);
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", source.Name);
            //instance.Activated += new ActivatedEventHandler(source.OnActivate);
            //instance.PositionChanged += new PositionChangedEventHandler(source.OnPositionChanged);
            instance.PositionChanged += new PositionChangedEventHandler(
                delegate(Point position) { source.Position = position; }
            );

            /*
            NameChangedEventHandler handler =
                new NameChangedEventHandler(delegate(string name) { instance.Label = name; });

            source.NameChanged += handler;

            instance.Unloaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { source.NameChanged -= handler; });
            */
            /*
            instance.ConnectionEnter += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (item is MessageResultItem)
                        source.Anchors.Add(new MessageSource(item.Label));
                    else
                    if (item is SignalResultItem)
                        source.Anchors.Add(new SignalSource(item.Label));
                }
            );
             */
            /*
            instance.ConnectionExit += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (sender is MessageResultItem || sender is SignalResultItem)
                    {
                        IAnchor remove = source.Anchors.First(
                            delegate(IAnchor anchor) { return (anchor.Name == item.Label) ? true : false; });
                        if (remove != null)
                            source.Anchors.Remove(remove);
                    }
                }
            );
             */

            /*
            source.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) 
                { 
                    instance.Attach(source.Anchors);
                    ResetConnections(instance);
                }
            );
             */

            /*
            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { instance.Attach(source.Anchors); });
             */

            instance.ContextMenu = new ContextMenu();

            MenuItem delete = new MenuItem();
            delete.Header = "Delete Node";
            /*
            delete.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { source.Remove(); });
             */

            instance.ContextMenu.Items.Add(delete);

            Canvas.SetLeft(instance, source.Position.X);
            Canvas.SetTop(instance, source.Position.Y);

            return instance;
        }

        public static DiagramState Create(this New.Move.Core.StateMachine source)
        {
            DiagramState instance = new DiagramState(source.Id);
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", source.Name);
            //instance.Activated += new ActivatedEventHandler(source.OnActivate);
            instance.PositionChanged += new PositionChangedEventHandler(
                delegate(Point position) { source.Position = position; } );

            /*
            NameChangedEventHandler handler =
                new NameChangedEventHandler(delegate(string name) { instance.Label = name; });

            source.NameChanged += handler;

            instance.Unloaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { source.NameChanged -= handler; });
            */
            /*
            instance.ConnectionEnter += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (item is MessageResultItem)
                        source.Anchors.Add(new MessageSource(item.Label));
                    else
                    if (item is SignalResultItem)
                        source.Anchors.Add(new SignalSource(item.Label));
                }
            );

            instance.ConnectionExit += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (sender is MessageResultItem || sender is SignalResultItem)
                    {
                        IAnchor remove = source.Anchors.First(
                            delegate(IAnchor anchor) { return (anchor.Name == item.Label) ? true : false; });
                        if (remove != null)
                            source.Anchors.Remove(remove);
                    }
                }
            );
             */
            /*
            source.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) 
                { 
                    instance.Attach(source.Anchors);
                    ResetConnections(instance);
                }
            );

             */
            /*
            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { instance.Attach(source.Anchors); });
            */
            instance.ContextMenu = new ContextMenu();

            MenuItem delete = new MenuItem();
            delete.Header = "Delete Node";
            /*
            delete.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { source.Remove(); });
             */

            instance.ContextMenu.Items.Add(delete);

            Canvas.SetLeft(instance, source.Position.X);
            Canvas.SetTop(instance, source.Position.Y);

            return instance;
        }

        public static DiagramNode Create(this LogicNode source)
        {
            DiagramNode instance = new DiagramNode(source.Id);
            instance.ChannelVisibility = Visibility.Visible;
            instance.CollapserVisibility = Visibility.Visible;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", source.Name);
            instance.PositionChanged += new PositionChangedEventHandler(source.OnPositionChanged);
            instance.CollapserClick += new CollapserClickEventHandler(source.OnCollapserToggle);

            source.CollapsabilityChanged += 
                new CollapsabilityChangedEventHandler(instance.OnCollapsabilityChanged);

            if (source.Collapsability == Collapsability.Closed)
            {
                instance.AnchorVisibility = Visibility.Visible;
                instance.IsClosed = true;
            }
            else
            {
                instance.AnchorVisibility = Visibility.Collapsed;
                instance.IsClosed = false;
            }

            if (source.Layer != Guid.Empty)
            {
                instance.IsLayered = true;
                Layers.Layer layer = Runtime.Instance.Layers.Find(source.Layer);
                instance.Layer = new SolidColorBrush(layer.Color);

                if (!layer.Enabled)
                    instance.Visibility = Visibility.Collapsed;
            }
            else
            {
                instance.IsLayered = false;
                instance.Layer = new SolidColorBrush();
            }

            LayerChangedEventHandler lceh = new LayerChangedEventHandler(
                delegate() 
                {
                    if (source.Layer != Guid.Empty)
                    {
                        instance.IsLayered = true;
                        Layers.Layer layer = Runtime.Instance.Layers.Find(source.Layer);
                        instance.Layer = new SolidColorBrush(layer.Color);
                    }
                    else
                    {
                        instance.IsLayered = false;
                        instance.Layer = new SolidColorBrush();
                    }
                }
            );

            source.LayerChanged += lceh;

            /*
            NameChangedEventHandler handler =
                new NameChangedEventHandler(delegate(string name) { instance.Label = name; });

            source.NameChanged += handler;

            instance.Unloaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) 
                {
                    source.LayerChanged -= lceh;
                    source.NameChanged -= handler;

                    source.CollapsabilityChanged -=
                        new CollapsabilityChangedEventHandler(instance.OnCollapsabilityChanged);
                }
            );
             */

            source.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
               delegate(object sender, NotifyCollectionChangedEventArgs e) { instance.Attach(source.Anchors, source.Collapsability); });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { instance.Attach(source.Anchors, source.Collapsability); });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    instance.CreateAnchors(source, source.Collapsability);
                });

            source.LogicNodePropertyChanged += new LogicNodePropertyChangedEventHandler(
                delegate
                {
                    Diagram diagram = DiagramViewer.Instance.Diagram;
                    diagram.OnDiagramChanged();
                });

            instance.ContextMenu = new ContextMenu();

            MenuItem delete = new MenuItem();
            delete.Header = "Delete Node";
            delete.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { source.Remove(); });

            instance.ContextMenu.Items.Add(delete);

            EnabledChangedEventHandler eceh = new EnabledChangedEventHandler(
                delegate(bool enabled)
                {
                    if (!enabled)
                        instance.Visibility = Visibility.Collapsed;
                    else
                        instance.Visibility = Visibility.Visible;
                }
            );

            MenuItem layeritem = new MenuItem();
            layeritem.Header = "Select Node Layer";

            MenuItem none = new MenuItem();
            none.Header = "None";
            none.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
                    {
                        if (layer.Contains(source))
                        {
                            none.Visibility = Visibility.Collapsed;
                            layer.EnabledChanged -= eceh;
                            layer.Remove(source);

                            break;
                        }
                    }
                }
            );

            if (instance.IsLayered)
                none.Visibility = Visibility.Visible;
            else 
                none.Visibility = Visibility.Collapsed;
            layeritem.Items.Add(none);

            foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
            {
                //MenuItem item = new MenuItem();
                LayerMenuItem item = new LayerMenuItem();
                item.Label = layer.Name;
                item.Color = new SolidColorBrush(layer.Color);
                item.CommandParameter = layer;
                item.Click += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e) 
                    {
                        MenuItem sent = (MenuItem)sender;
                        Layers.Layer selected = (Layers.Layer)sent.CommandParameter;

                        selected.Add(source);
                        selected.EnabledChanged += eceh;

                        none.Visibility = Visibility.Visible;
                    }
                );

                layeritem.Items.Add(item);
            }

            if (Runtime.Instance.Layers.Items.Count == 0)
                layeritem.Visibility = Visibility.Collapsed;

            instance.ContextMenu.Items.Add(layeritem);

            Runtime.Instance.Layers.Items.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e)
                {
                    if (Runtime.Instance.Layers.Items.Count != 0)
                        layeritem.Visibility = Visibility.Visible;
                    else
                        layeritem.Visibility = Visibility.Collapsed;

                    layeritem.Items.Clear();
                    layeritem.Items.Add(none);

                    foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
                    {
                        //MenuItem item = new MenuItem();
                        LayerMenuItem item = new LayerMenuItem();
                        item.Label = layer.Name;
                        item.Color = new SolidColorBrush(layer.Color);
                        item.CommandParameter = layer;
                        item.Click += new RoutedEventHandler(
                            delegate(object obj, RoutedEventArgs args) 
                            {
                                MenuItem sent = (MenuItem)obj;
                                Layers.Layer selected = (Layers.Layer)sent.CommandParameter;

                                selected.Add(source);
                                selected.EnabledChanged += eceh;

                                none.Visibility = Visibility.Visible;
                            }
                        );

                        layeritem.Items.Add(item);
                    }
                }
            );

            if (source.Arrays.Count() != 0)
            {
                foreach (ArrayItemBase array in source.Arrays)
                {
                    MenuItem item = new MenuItem();
                    item.Header = "Add " + array.Name + "...";
                    item.Click += new RoutedEventHandler(
                        delegate(object sender, RoutedEventArgs e) { array.Create(source.Anchors); });

                    instance.ContextMenu.Items.Add(item);
                }
            }

            Canvas.SetLeft(instance, source.Position.X);
            Canvas.SetTop(instance, source.Position.Y);

            return instance;
        }

       /*
        public static DiagramNode Create(this LogicStateNode source)
        {
            DiagramNode instance = new DiagramNode(source.Id);
            instance.ChannelVisibility = Visibility.Visible;
            instance.CollapserVisibility = Visibility.Visible;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", source.Name);
            instance.Activated += new ActivatedEventHandler(source.OnActivate);
            instance.PositionChanged += new PositionChangedEventHandler(source.OnPositionChanged);
            instance.CollapserClick += new CollapserClickEventHandler(source.OnCollapserToggle);

            source.CollapsabilityChanged +=
                new CollapsabilityChangedEventHandler(instance.OnCollapsabilityChanged);

            if (source.Collapsability == Collapsability.Closed)
            {
                instance.AnchorVisibility = Visibility.Visible;
                instance.IsClosed = true;
            }
            else
            {
                instance.AnchorVisibility = Visibility.Collapsed;
                instance.IsClosed = false;
            }

            if (source.Layer != Guid.Empty)
            {
                instance.IsLayered = true;
                Layers.Layer layer = Runtime.Instance.Layers.Find(source.Layer);
                instance.Layer = new SolidColorBrush(layer.Color);

                if (!layer.Enabled)
                    instance.Visibility = Visibility.Collapsed;
            }
            else
            {
                instance.IsLayered = false;
                instance.Layer = new SolidColorBrush();
            }

            LayerChangedEventHandler lceh = new LayerChangedEventHandler(
                delegate()
                {
                    if (source.Layer != Guid.Empty)
                    {
                        instance.IsLayered = true;
                        Layers.Layer layer = Runtime.Instance.Layers.Find(source.Layer);
                        instance.Layer = new SolidColorBrush(layer.Color);
                    }
                    else
                    {
                        instance.IsLayered = false;
                        instance.Layer = new SolidColorBrush();
                    }
                }
            );

            source.LayerChanged += lceh;

            NameChangedEventHandler handler =
                new NameChangedEventHandler(delegate(string name) { instance.Label = name; });

            source.NameChanged += handler;

            instance.Unloaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) 
                {
                    source.LayerChanged -= lceh;
                    source.NameChanged -= handler;

                    source.CollapsabilityChanged -=
                        new CollapsabilityChangedEventHandler(instance.OnCollapsabilityChanged);
                }
            );

            instance.ConnectionEnter += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (item is MessageResultItem)
                        source.Anchors.Add(new MessageSource(item.Label));
                    else
                    if (item is SignalResultItem)
                        source.Anchors.Add(new SignalSource(item.Label));
                }
            );

            instance.ConnectionExit += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (sender is MessageResultItem || sender is SignalResultItem)
                    {
                        IAnchor remove = source.Anchors.First(
                            delegate(IAnchor anchor) { return (anchor.Name == item.Label) ? true : false; });
                        if (remove != null)
                            source.Anchors.Remove(remove);
                    }
                }
            );

            source.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) 
                { 
                    instance.Attach(source.Anchors, source.Collapsability); 
                    ResetConnections(instance); 
                }
            );

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { instance.Attach(source.Anchors, source.Collapsability); });

            instance.ContextMenu = new ContextMenu();

            MenuItem delete = new MenuItem();
            delete.Header = "Delete Node";
            delete.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { source.Remove(); });

            instance.ContextMenu.Items.Add(delete);

            EnabledChangedEventHandler eceh = new EnabledChangedEventHandler(
                delegate(bool enabled)
                {
                    if (!enabled)
                        instance.Visibility = Visibility.Collapsed;
                    else
                        instance.Visibility = Visibility.Visible;
                }
            );

            MenuItem layeritem = new MenuItem();
            layeritem.Header = "Select Node Layer";

            MenuItem none = new MenuItem();
            none.Header = "None";
            none.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
                    {
                        if (layer.Contains(source))
                        {
                            none.Visibility = Visibility.Collapsed;
                            layer.EnabledChanged -= eceh;
                            layer.Remove(source);

                            break;
                        }
                    }
                }
            );

            if (instance.IsLayered)
                none.Visibility = Visibility.Visible;
            else
                none.Visibility = Visibility.Collapsed;
            layeritem.Items.Add(none);

            foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
            {
                //MenuItem item = new MenuItem();
                LayerMenuItem item = new LayerMenuItem();
                item.Label = layer.Name;
                item.Color = new SolidColorBrush(layer.Color);
                item.CommandParameter = layer;
                item.Click += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e) 
                    {
                        MenuItem sent = (MenuItem)sender;
                        Layers.Layer selected = (Layers.Layer)sent.CommandParameter;

                        selected.Add(source);
                        selected.EnabledChanged += eceh;

                        none.Visibility = Visibility.Visible;
                    }
                );

                layeritem.Items.Add(item);
            }

            instance.ContextMenu.Items.Add(layeritem);

            if (Runtime.Instance.Layers.Items.Count == 0)
                layeritem.Visibility = Visibility.Collapsed;

            Runtime.Instance.Layers.Items.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e)
                {
                    if (Runtime.Instance.Layers.Items.Count != 0)
                        layeritem.Visibility = Visibility.Visible;
                    else
                        layeritem.Visibility = Visibility.Collapsed;

                    layeritem.Items.Clear();
                    layeritem.Items.Add(none);

                    foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
                    {
                        //MenuItem item = new MenuItem();
                        LayerMenuItem item = new LayerMenuItem();
                        item.Label = layer.Name;
                        item.Color = new SolidColorBrush(layer.Color);
                        item.CommandParameter = layer;
                        item.Click += new RoutedEventHandler(
                            delegate(object obj, RoutedEventArgs args) 
                            {
                                MenuItem sent = (MenuItem)obj;
                                Layers.Layer selected = (Layers.Layer)sent.CommandParameter;

                                selected.Add(source);
                                selected.EnabledChanged += eceh;

                                none.Visibility = Visibility.Visible;
                            }
                        );

                        layeritem.Items.Add(item);
                    }

                }
            );

            Canvas.SetLeft(instance, source.Position.X);
            Canvas.SetTop(instance, source.Position.Y);

            return instance;
        }
        */
#if disabled
        public static DiagramNode Create(this Reference source)
        {
            DiagramNode instance = new DiagramNode(source.Id);
            instance.ChannelVisibility = Visibility.Visible;
            instance.CollapserVisibility = Visibility.Visible;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", source.Name);
            instance.Activated += new ActivatedEventHandler(source.OnActivate);
            instance.PositionChanged += new PositionChangedEventHandler(source.OnPositionChanged);
            instance.CollapserClick += new CollapserClickEventHandler(source.OnCollapserToggle);

            source.CollapsabilityChanged +=
                new CollapsabilityChangedEventHandler(instance.OnCollapsabilityChanged);

            if (source.Collapsability == Collapsability.Closed)
            {
                instance.AnchorVisibility = Visibility.Visible;
                instance.IsClosed = true;
            }
            else
            {
                instance.AnchorVisibility = Visibility.Collapsed;
                instance.IsClosed = false;
            }

            if (source.Layer != Guid.Empty)
            {
                instance.IsLayered = true;
                Layers.Layer layer = Runtime.Instance.Layers.Find(source.Layer);
                instance.Layer = new SolidColorBrush(layer.Color);

                if (!layer.Enabled)
                    instance.Visibility = Visibility.Collapsed;
            }
            else
            {
                instance.IsLayered = false;
                instance.Layer = new SolidColorBrush();
            }

            LayerChangedEventHandler layerceh = new LayerChangedEventHandler(
                delegate()
                {
                    if (source.Layer != Guid.Empty)
                    {
                        instance.IsLayered = true;
                        Layers.Layer layer = Runtime.Instance.Layers.Find(source.Layer);
                        instance.Layer = new SolidColorBrush(layer.Color);
                    }
                    else
                    {
                        instance.IsLayered = false;
                        instance.Layer = new SolidColorBrush();
                    }
                }
            );

            source.LayerChanged += layerceh;

            NameChangedEventHandler handler =
                new NameChangedEventHandler(delegate(string name) { instance.Label = name; });

            source.NameChanged += handler;

            instance.Unloaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) 
                {
                    source.LayerChanged -= layerceh;
                    source.NameChanged -= handler;

                    source.CollapsabilityChanged -=
                        new CollapsabilityChangedEventHandler(instance.OnCollapsabilityChanged);
                }
            );

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { instance.Attach(source.Anchors, source.Collapsability); });

            instance.ContextMenu = new ContextMenu();

            MenuItem delete = new MenuItem();
            delete.Header = "Delete Node";
            delete.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { source.Remove(); });

            instance.ContextMenu.Items.Add(delete);

            EnabledChangedEventHandler eceh = new EnabledChangedEventHandler(
                delegate(bool enabled)
                {
                    if (!enabled)
                        instance.Visibility = Visibility.Collapsed;
                    else
                        instance.Visibility = Visibility.Visible;
                }
            );

            MenuItem layeritem = new MenuItem();
            layeritem.Header = "Select Node Layer";

            MenuItem none = new MenuItem();
            none.Header = "None";
            none.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
                    {
                        if (layer.Contains(source))
                        {
                            none.Visibility = Visibility.Collapsed;
                            layer.Remove(source);
                            layer.EnabledChanged -= eceh;

                            break;
                        }
                    }
                }
            );

            if (instance.IsLayered)
                none.Visibility = Visibility.Visible;
            else
                none.Visibility = Visibility.Collapsed;
            layeritem.Items.Add(none);

            foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
            {
                //MenuItem item = new MenuItem();
                LayerMenuItem item = new LayerMenuItem();
                item.Label = layer.Name;
                item.Color = new SolidColorBrush(layer.Color);
                item.CommandParameter = layer;
                item.Click += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e) 
                    {
                        MenuItem sent = (MenuItem)sender;
                        Layers.Layer selected = (Layers.Layer)sent.CommandParameter;

                        selected.Add(source);
                        selected.EnabledChanged += eceh;

                        none.Visibility = Visibility.Visible;
                    }
                );

                layeritem.Items.Add(item);
            }

            instance.ContextMenu.Items.Add(layeritem);

            if (Runtime.Instance.Layers.Items.Count == 0)
                layeritem.Visibility = Visibility.Collapsed;

            Runtime.Instance.Layers.Items.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e)
                {
                    if (Runtime.Instance.Layers.Items.Count != 0)
                        layeritem.Visibility = Visibility.Visible;
                    else 
                        layeritem.Visibility = Visibility.Collapsed;

                    layeritem.Items.Clear();
                    layeritem.Items.Add(none);

                    foreach (Layers.Layer layer in Runtime.Instance.Layers.Items)
                    {
                        //MenuItem item = new MenuItem();
                        LayerMenuItem item = new LayerMenuItem();
                        item.Label = layer.Name;
                        item.Color = new SolidColorBrush(layer.Color);
                        item.CommandParameter = layer;
                        item.Click += new RoutedEventHandler(
                            delegate(object obj, RoutedEventArgs args) 
                            {
                                MenuItem sent = (MenuItem)obj;
                                Layers.Layer selected = (Layers.Layer)sent.CommandParameter;

                                selected.Add(source);
                                selected.EnabledChanged += eceh;

                                none.Visibility = Visibility.Visible;
                            }
                        );

                        layeritem.Items.Add(item);
                    }
                }
            );

            Canvas.SetLeft(instance, source.Position.X);
            Canvas.SetTop(instance, source.Position.Y);

            return instance;
        }

#endif
        public static Operator Create(this OpNode source)
        {
            Operator instance = new Operator(source.Id);
            //instance.ChannelVisibility = Visibility.Visible;
            //instance.CollapserVisibility = Visibility.Visible;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", source.Name);
            instance.PositionChanged += new PositionChangedEventHandler(source.OnPositionChanged);
            //instance.CollapserClip += new CollapserClickEventHandler();

            instance.Unloaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                }
            );

            source.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e)
                {
                    instance.Attach(source.Anchors);
                }
            );

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    instance.Attach(source.Anchors);
                }
            );

            instance.ContextMenu = new ContextMenu();

            MenuItem delete = new MenuItem();
            delete.Header = "Delete Operator";
            delete.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { source.Remove(); }
            );

            instance.ContextMenu.Items.Add(delete);

            Canvas.SetLeft(instance, source.Position.X);
            Canvas.SetTop(instance, source.Position.Y);

            return instance;
        }

        public static AnchorResultItem CreateInputAnchor(this IAnchor source, DiagramNode parent)
        {
            if (source is TransformSource)
                return new TransformResultItem(source.Name, source.Id, parent);
            else 
            if (source is MessageSource)
                return new MessageResultItem(source.Name, source.Id, parent);
            else 
            if (source is SignalSource)
                return new SignalResultItem(source.Name, source.Id, parent);

            return null;
        }

        public static AnchorSourceItem CreateOutputAnchor(this IAnchor source, FrameworkElement parent)
        {
            if (source is TransformResult)
                return new TransformSourceItem(source.Name, source.Id, parent);
            else 
            if (source is MessageResult)
                return new MessageSourceItem(source.Name, source.Id, parent);
            else 
            if (source is SignalResult)
                return new SignalSourceItem(source.Name, source.Id, parent);

            return null;
        }

        static public void ResetConnections(DiagramNode instance)
        {
            Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");

            Diagram diagram = DiagramViewer.Instance.Diagram;
            foreach (Connection connection in diagram.Children.OfType<Connection>())
            {
                AnchorItem source = connection.Source as AnchorItem;
                AnchorItem sink = connection.Sink as AnchorItem;

                foreach (AnchorItem anchor in PART_Anchors.Children.OfType<AnchorItem>())
                {
                    if (anchor.Id == source.Id)
                        connection.Source = anchor as IAnchorSender;

                    if (anchor.Id == sink.Id)
                        connection.Sink = anchor as IAnchorReceiver;
                }
            }
        }

        static public void ResetConnections(DiagramState instance)
        {
            Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");

            Diagram diagram = DiagramViewer.Instance.Diagram;
            foreach (Connection connection in diagram.Children.OfType<Connection>())
            {
                AnchorItem source = connection.Source as AnchorItem;
                AnchorItem sink = connection.Sink as AnchorItem;

                foreach (AnchorItem anchor in PART_Anchors.Children.OfType<AnchorItem>())
                {
                    if (anchor.Id == source.Id)
                        connection.Source = anchor as IAnchorSender;

                    if (anchor.Id == sink.Id)
                        connection.Sink = anchor as IAnchorReceiver;
                }
            }
        }
    }
}

namespace Rage.Move
{
    // this shouldn't really be public but i need it at the moment
    public class DiagramLogic
    {
        public DiagramLogic()
        {
            Clear();
        }

        public event EventHandler ChildrenLoaded;
        public void OnChildrenLoaded()
        {
            if (ChildrenLoaded != null)
                ChildrenLoaded(this, EventArgs.Empty);
        }

        List<FrameworkElement> Children = new List<FrameworkElement>();

        // object is either an ILogicNode or ITransitionalNode
        Dictionary<FrameworkElement, object> Lookup = new Dictionary<FrameworkElement, object>();

        /*
        public ILogicNode GetLogicNode(DiagramNode node)
        {
            if (!Lookup.ContainsKey(node))
                return null;

            object value = Lookup[node];
            if (value is ILogicNode)
                return (ILogicNode)value;

            return null;
        }
         */
        
        public Connection CreateConnection(Diagram diagram, IConnection connection)
        {
            IAnchorSender source = null;
            IAnchorReceiver sink = null;

            var children = diagram.Children.OfType<DiagramNode>();
            foreach (DiagramNode child in children)
            {
                if (child.Anchors.Contains(connection.Source) && child.Visibility == Visibility.Visible)
                {
                    if (child.IsClosed)
                        source = child.ClosedResultItem;
                    else 
                        source = child.Anchors[connection.Source] as IAnchorSender;
                    break;
                }
            }

            foreach (DiagramNode child in children)
            {
                if (child.Anchors.Contains(connection.Sink) && child.Visibility == Visibility.Visible)
                {
                    if (child.IsClosed)
                        sink = child.ClosedSourceItem;
                    else
                        sink = child.Anchors[connection.Sink] as IAnchorReceiver;
                    break;
                }
            }

            foreach (Operator op in diagram.Children.OfType<Operator>())
            {
                if (op.Anchors.Contains(connection.Source) && op.Visibility == Visibility.Visible)
                {
                    source = op.Anchors[connection.Source] as IAnchorSender;
                    break;
                }
            }

            foreach (Operator op in diagram.Children.OfType<Operator>())
            {
                if (op.Anchors.Contains(connection.Sink) && op.Visibility == Visibility.Visible)
                {
                    sink = op.Anchors[connection.Sink] as IAnchorReceiver;
                    break;
                }
            }

            foreach (DiagramState child in diagram.Children.OfType<DiagramState>())
            {
                Panel PART_Anchors = (Panel)child.GetChild("PART_Anchors");
                if (PART_Anchors != null)
                {
                    foreach (AnchorItem item in PART_Anchors.Children)
                    {
                        if (item.Id == connection.Source)
                            source = item as IAnchorSender;

                        if (item.Id == connection.Sink)
                            sink = item as IAnchorReceiver;
                    }

                    if (source != null && sink != null)
                        break;
                }
            }

            if (source != null && sink != null)
                return new Connection(source, sink);

            return null;
        }

        public Transition CreateTransition(Diagram diagram, ITransition transition)
        {
            /*
            DiagramState source = null;
            DiagramState sink = null;

            foreach (DiagramState child in diagram.Children.OfType<DiagramState>())
            {
                if (child.Id == transition.Source)
                    source = child;

                if (child.Id == transition.Sink)
                    sink = child;

                if (source != null && sink != null)
                    break;
            }

            return new Transition(transition.Id, source, sink);
             */

            return null;
        }

        void Messages_AttachAnchors(DiagramNode instance, ObservableCollection<Message> messages)
        {
            Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");
            PART_Anchors.Children.Clear();
            instance.Anchors.Clear();

            foreach (Message message in messages)
            {
                AnchorItem item = new MessageSourceItem(message.Name, message.Id, instance);
                item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++instance.Count; });
                instance.PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                PART_Anchors.Children.Add(item);

                instance.Anchors.Add(item.Id, item);
            }

            if (PART_Anchors.Children.Count == 0)
                instance.OnChildrenLoaded();
        }

        void ControlSignals_AttachAnchors(DiagramNode instance, ObservableCollection<ControlSignal> signals)
        {
            Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");
            PART_Anchors.Children.Clear();
            instance.Anchors.Clear();

            foreach (IAnchor anchor in signals)
            {
                AnchorItem item = null;

                if (anchor is ControlSignalFloat)
                    item = new SignalResultItem(anchor.Name, anchor.Id, instance);
                else 
                if (anchor is ControlSignalBool)
                    item = new SignalResultItem(anchor.Name, anchor.Id, instance);
                else 
                if (anchor is ControlSignalAnimation)
                    item = new AnimationResultItem(anchor.Name, anchor.Id, instance);
                else 
                if (anchor is ControlSignalClip)
                    item = new ClipResultItem(anchor.Name, anchor.Id, instance);
                else 
                if (anchor is ControlSignalExpression)
                    item = new ExpressionResultItem(anchor.Name, anchor.Id, instance);
                else 
                if (anchor is ControlSignalFilter)
                    item = new FilterResultItem(anchor.Name, anchor.Id, instance);
                else 
                if (anchor is ControlSignalFrame)
                    item = new FrameResultItem(anchor.Name, anchor.Id, instance);
                else 
                if (anchor is ControlSignalParameterizedMotion)
                    item = new ParameterizedMotionResultItem(anchor.Name, anchor.Id, instance);

                item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++instance.Count; });
                instance.PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                PART_Anchors.Children.Add(item);

                instance.Anchors.Add(item.Id, item);
            }

            if (PART_Anchors.Children.Count == 0)
                instance.OnChildrenLoaded();
        }

        public DiagramNode CreateControlSignals(New.Move.Core.Motiontree parent)
        {
            DiagramNode instance = new DiagramNode(Guid.Empty);
            instance.AnchorVisibility = Visibility.Collapsed;
            instance.ChannelVisibility = Visibility.Collapsed;
            instance.CollapserVisibility = Visibility.Collapsed;
            //instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", Properties.Resources.ControlSignals);

            //instance.PositionChanged += new PositionChangedEventHandler(
            //    delegate(Point position) { parent.ControlSignalsPosition = position; }
            //);

#if disabled
            Database db = Runtime.Instance.Databases[parent.Database];
            db.ControlSignals.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) { instance.InvalidateVisual(); /*ControlSignals_AttachAnchors(instance);*/ });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { ControlSignals_AttachAnchors(instance, db.ControlSignals); });
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
#endif
            instance.ContextMenu = new ContextMenu();
            MenuItem menu = new MenuItem();
            menu.Header = "Add Control Signal...";
            /*
            menu.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { MainWindow.Instance.ControlSignalEditControl.Show(); }
            );
             */

            instance.ContextMenu.Items.Add(menu);

            Children.Add(instance);

            //Canvas.SetLeft(instance, parent.ControlSignalsPosition.X);
            //Canvas.SetTop(instance, parent.ControlSignalsPosition.Y);

            return instance;
        }

        public DiagramNode CreateMessages(New.Move.Core.Motiontree parent)
        {
            DiagramNode instance = new DiagramNode(Guid.Empty);
            instance.AnchorVisibility = Visibility.Collapsed;
            instance.ChannelVisibility = Visibility.Collapsed;
            instance.CollapserVisibility = Visibility.Collapsed;
            //instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", Properties.Resources.Messages);

            //instance.PositionChanged += new PositionChangedEventHandler(
            //    delegate(Point position) { parent.MessagesPosition = position; }
            //);

            /*
            Database db = Runtime.Instance.Databases[parent.Database];
            db.Messages.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) { instance.InvalidateVisual(); });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { Messages_AttachAnchors(instance, db.Messages); });
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
            */
            instance.ContextMenu = new ContextMenu();
            MenuItem menu = new MenuItem();
            menu.Header = "Add Message...";
            /*
            menu.Click += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    MainWindow.Instance.MessageEditControl.Show();
                }
            );
             */

            instance.ContextMenu.Items.Add(menu);

            Children.Add(instance);

            //Canvas.SetLeft(instance, parent.MessagesPosition.X);
            //Canvas.SetTop(instance, parent.MessagesPosition.Y);

            return instance;
        }

        void Input_AttachAnchors(DiagramNode instance, ObservableCollection<IAnchor> anchors)
        {
            Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");
            PART_Anchors.Children.Clear();
            instance.Anchors.Clear();

            foreach (IAnchor anchor in anchors.OfType<AnchorSource>())
            {
                AnchorItem item = anchor.CreateInputAnchor(instance);// new SignalResultItem(anchor.Name, anchor.Id, instance);
                item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++instance.Count; });
                instance.PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                PART_Anchors.Children.Add(item);

                instance.Anchors.Add(item.Id, item);
            }
        }

        public DiagramNode CreateInput(New.Move.Core.Motiontree parent)
        {
            DiagramNode instance = new DiagramNode(Guid.Empty);
            instance.AnchorVisibility = Visibility.Collapsed;
            instance.ChannelVisibility = Visibility.Collapsed;
            instance.CollapserVisibility = Visibility.Collapsed;
            //instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", Properties.Resources.Input);
            //instance.PositionChanged += new PositionChangedEventHandler(
             //   delegate(Point position) { parent.InputPosition = position; } );

#if not
            parent.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) { instance.InvalidateVisual(); /*Input_AttachAnchors(instance, parent.Anchors);*/ });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { Input_AttachAnchors(instance, parent.Anchors); });
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
#endif
            Children.Add(instance);
            
            //Canvas.SetLeft(instance, parent.InputPosition.X);
            //Canvas.SetTop(instance, parent.InputPosition.Y);

            return instance;
        }

        /*
        public DiagramNode CreateLogicInput(New.Move.Core.StateMachine parent)
        {
            DiagramNode instance = new DiagramNode(Guid.Empty);
            instance.AnchorVisibility = Visibility.Collapsed;
            instance.ChannelVisibility = Visibility.Collapsed;
            instance.CollapserVisibility = Visibility.Collapsed;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", Properties.Resources.Input);

            instance.PositionChanged += new PositionChangedEventHandler(
                delegate(Point position) { parent.InputPosition = position; } );

            parent.LogicalParent.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) { Input_AttachAnchors(instance, parent.LogicalParent.Anchors); });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { Input_AttachAnchors(instance, parent.LogicalParent.Anchors); });
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);

            Children.Add(instance);

            Canvas.SetLeft(instance, parent.InputPosition.X);
            Canvas.SetTop(instance, parent.InputPosition.Y);

            return instance;
        }

        public DiagramNode CreateInput(StateMachine parent)
        {
            DiagramNode instance = new DiagramNode(Guid.Empty);
            instance.AnchorVisibility = Visibility.Collapsed;
            instance.ChannelVisibility = Visibility.Collapsed;
            instance.CollapserVisibility = Visibility.Collapsed;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", Properties.Resources.Input);
            instance.PositionChanged += new PositionChangedEventHandler(parent.OnInputPositionChanged);

            parent.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) { Input_AttachAnchors(instance, parent.Anchors); });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { Input_AttachAnchors(instance, parent.Anchors); });
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);

            Children.Add(instance);

            Canvas.SetLeft(instance, parent.InputPosition.X);
            Canvas.SetTop(instance, parent.InputPosition.Y);

            return instance;
        }
         */

        void Output_AttachAnchors(DiagramNode instance, ObservableCollection<IAnchor> anchors)
        {
            Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");
            PART_Anchors.Children.Clear();
            instance.Anchors.Clear();

            foreach (IAnchor anchor in anchors.OfType<AnchorResult>())
            {
                AnchorSourceItem item = anchor.CreateOutputAnchor(instance);
                item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++instance.Count; });
                instance.PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                PART_Anchors.Children.Add(item);

                instance.Anchors.Add(item.Id, item);
            }

            Logic.ResetConnections(instance);            
        }

        void Output_AttachLogicalAnchors(DiagramNode instance, ObservableCollection<IAnchor> anchors)
        {
            Panel PART_Anchors = (Panel)instance.GetChild("PART_Anchors");
            PART_Anchors.Children.Clear();
            instance.Anchors.Clear();

            foreach (IAnchor anchor in anchors.OfType<AnchorResult>())
            {
                if (anchor is ITransformAnchor)
                    continue;

                AnchorSourceItem item = anchor.CreateOutputAnchor(instance);
                item.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { ++instance.Count; });
                instance.PositionChanged += new PositionChangedEventHandler(delegate(Point point) { item.OnPositionChanged(point); });
                PART_Anchors.Children.Add(item);

                instance.Anchors.Add(item.Id, item);
            }

            Logic.ResetConnections(instance);
        }

        public DiagramNode CreateOutput(New.Move.Core.Motiontree parent)
        {
            DiagramNode instance = new DiagramNode(Guid.Empty);
            instance.AnchorVisibility = Visibility.Collapsed;
            instance.ChannelVisibility = Visibility.Collapsed;
            instance.CollapserVisibility = Visibility.Collapsed;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", Properties.Resources.Output);
            //instance.PositionChanged += new PositionChangedEventHandler(
            //    delegate(Point position) { parent.OutputPosition = position; } );
                
            /*
            instance.ConnectionEnter += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (item is MessageResultItem)
                        parent.Anchors.Add(new MessageResult(item.Label));
                    else
                    if (item is SignalResultItem)
                        parent.Anchors.Add(new SignalResult(item.Label));
                }
            );
             */

            /*
            instance.ConnectionExit += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (sender is MessageResultItem || sender is SignalResultItem)
                    {
                        IAnchor remove = parent.Anchors.First(
                            delegate(IAnchor anchor) { return (anchor.Name == item.Label) ? true : false; });
                        if (remove != null)
                            parent.Anchors.Remove(remove);
                    }
                }
            );
            
            parent.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) { Output_AttachAnchors(instance, parent.Anchors); });
            
            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { Output_AttachAnchors(instance, parent.Anchors); });
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);

             */

            Children.Add(instance);

            //Canvas.SetLeft(instance, parent.OutputPosition.X);
            //Canvas.SetTop(instance, parent.OutputPosition.Y);

            return instance;
        }

        /*
        public DiagramNode CreateLogicOutput(StateMachine parent)
        {
            DiagramNode instance = new DiagramNode(Guid.Empty);
            instance.AnchorVisibility = Visibility.Collapsed;
            instance.ChannelVisibility = Visibility.Collapsed;
            instance.CollapserVisibility = Visibility.Collapsed;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", Properties.Resources.Output);
            instance.PositionChanged += new PositionChangedEventHandler(parent.OnOutputPositionChanged);

            instance.ConnectionEnter += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (item is MessageResultItem)
                        parent.LogicalParent.Anchors.Add(new MessageResult(item.Label));
                    else 
                    if (item is SignalResultItem)
                        parent.LogicalParent.Anchors.Add(new SignalResult(item.Label));
                }
            );

            instance.ConnectionExit += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (sender is MessageResultItem || sender is SignalResultItem)
                    {
                        IAnchor remove = parent.LogicalParent.Anchors.First(
                            delegate(IAnchor anchor) { return (anchor.Name == item.Label) ? true : false; });
                        if (remove != null)
                            parent.LogicalParent.Anchors.Remove(remove);
                    }
                }
            );

            parent.LogicalParent.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) { Output_AttachLogicalAnchors(instance, parent.LogicalParent.Anchors); });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { Output_AttachLogicalAnchors(instance, parent.LogicalParent.Anchors); });
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);

            Children.Add(instance);

            Canvas.SetLeft(instance, parent.OutputPosition.X);
            Canvas.SetTop(instance, parent.OutputPosition.Y);

            return instance;
        }
         */

        /*
        public DiagramNode CreateOutput(StateMachine parent)
        {
            DiagramNode instance = new DiagramNode(Guid.Empty);
            instance.AnchorVisibility = Visibility.Collapsed;
            instance.ChannelVisibility = Visibility.Collapsed;
            instance.CollapserVisibility = Visibility.Collapsed;
            instance.Label = String.Format(CultureInfo.CurrentCulture, "{0}", Properties.Resources.Output);
            instance.PositionChanged += new PositionChangedEventHandler(parent.OnOutputPositionChanged);

            instance.ConnectionEnter += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (item is MessageResultItem)
                        parent.Anchors.Add(new MessageResult(item.Label));
                    else
                    if (item is SignalResultItem)
                        parent.Anchors.Add(new SignalResult(item.Label));
                }
            );

            instance.ConnectionExit += new ConnectionOverEventHandler(
                delegate(IAnchorSender sender)
                {
                    AnchorItem item = sender as AnchorItem;
                    if (sender is MessageResultItem || sender is SignalResultItem)
                    {
                        IAnchor remove = parent.Anchors.First(
                            delegate(IAnchor anchor) { return (anchor.Name == item.Label) ? true : false; });
                        if (remove != null)
                            parent.Anchors.Remove(remove);
                    }
                }
            );

            parent.Anchors.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e) { Output_AttachAnchors(instance, parent.Anchors); });

            instance.Loaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e) { Output_AttachAnchors(instance, parent.Anchors); });
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);

            Children.Add(instance);

            Canvas.SetLeft(instance, parent.OutputPosition.X);
            Canvas.SetTop(instance, parent.OutputPosition.Y);

            return instance;
        }
        */
        public StartMarker CreateStartMarker(Diagram diagram, New.Move.Core.StateMachine parent)
        {
            /*
            DiagramState instance = null;
            foreach (DiagramState child in diagram.Children.OfType<DiagramState>())
            {
                if (child.Id == parent.Default)
                {
                    instance = child;
                    break;
                }
            }

            if (instance != null)
            {
                StartMarker marker = new StartMarker();
                marker.Target = instance; 

                parent.DefaultChanged += new EventHandler(
                    delegate(object sender, EventArgs e) 
                    {
                        foreach (DiagramState child in diagram.Children.OfType<DiagramState>())
                        {
                            if (child.Id == parent.Default)
                            {
                                marker.Target = child;
                                break;
                            }
                        }
                    }
                );

                return marker;
            }
            */
            return null;
        }

        public DiagramState Create(New.Move.Core.ITransitional template)
        {
            if (template is New.Move.Core.Motiontree)
            {
                DiagramState instance = (template as New.Move.Core.Motiontree).Create();
                instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
                Children.Add(instance);
                Lookup.Add(instance, template);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e) { instance.Count = 0; });
                return instance;
            }

            if (template is New.Move.Core.StateMachine)
            {
                DiagramState instance = (template as New.Move.Core.StateMachine).Create();
                instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
                Children.Add(instance);
                Lookup.Add(instance, template);
                instance.Loaded += new RoutedEventHandler(
                    delegate(object sender, RoutedEventArgs e) { instance.Count = 0; });
                return instance;
            }

            return null;
        }

        public DiagramNode Create(New.Move.Core.ILogic template)
        {
            if (template is New.Move.Core.Logic)
            {
                DiagramNode instance = (template as LogicNode).Create();
                Lookup.Add(instance, template);
                instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
                Children.Add(instance);

                return instance;
            }

            /*
            if (template is LogicStateNode)
            {
                DiagramNode instance = (template as LogicStateNode).Create();
                Lookup.Add(instance, template);
                instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
                Children.Add(instance);
                
                return instance;
            }
             */
            /*
            if (template is Reference)
            {
                DiagramNode instance = (template as Reference).Create();
                Lookup.Add(instance, template);
                instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
                Children.Add(instance);
                
                return instance;
            }
             */

            return null;
        }

        public Operator Create(IOpNode template)
        {
            Operator instance = (template as OpNode).Create();
            instance.ChildrenLoaded += new EventHandler(OnChildLoaded);
            Children.Add(instance);

            return instance;
        }

        /*
        public MotionTree CreateMotionTree()
        {
            MotionTree tree = new MotionTree("?");
            tree.Name = "Tree";

            return tree;
        }
         */

        public bool ChildrenHaveOutputs(New.Move.Core.StateMachine parent)
        {
            foreach (New.Move.Core.ITransitional child in parent.Children)
            {
                if (child is New.Move.Core.Motiontree)
                {
                    /*
                    if ((child as New.Move.Core.Motiontree).HasOutputs)
                        return true;
                     */
                }
                else
                if (child is New.Move.Core.StateMachine)
                {
                    /*
                    if ((child as New.Move.Core.StateMachine).HasOutputs)
                        return true;
                     */
                }
            }

            return false;
        }

        public void Clear()
        {
            // Just in case we lost track of what was loaded and what isn't
            Children.Clear();

            Lookup.Clear();
        }

        public void OnConnection(IAnchorSender sender, IAnchorReceiver receiver)
        {
            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                if ((sender is SignalResultItem && receiver is SignalSourceItem) || 
                    (sender is AnimationResultItem && receiver is AnimationSourceItem) || 
                    (sender is ClipResultItem  && receiver is ClipSourceItem) || 
                    (sender is ExpressionResultItem && receiver is ExpressionResultItem) || 
                    (sender is FilterResultItem && receiver is FilterSourceItem) || 
                    (sender is FrameResultItem && receiver is FrameSourceItem) ||
                    (sender is SignalResultItem && receiver is RealSourceItem)) 
                {
                    SignalConnection connect =
                        new SignalConnection((sender as AnchorItem).Id, (receiver as AnchorItem).Id);
                    //(Runtime.Database.Active as New.Move.Core.Motiontree).Connections.Add(connect);
                }
                else 
                if (sender is OperatorResultItem && receiver is OperatorSourceItem)
                {
                    OperatorConnection connect =
                        new OperatorConnection((sender as AnchorItem).Id, (receiver as AnchorItem).Id);
                    //(Runtime.Database.Active as New.Move.Core.Motiontree).Connections.Add(connect);
                }
                else 
                if (sender is OperatorResultItem && (receiver is SignalSourceItem || receiver is RealSourceItem))
                {
                    OperatorResultConnection connect =
                        new OperatorResultConnection((sender as AnchorItem).Id, (receiver as AnchorItem).Id);
                    //(Runtime.Database.Active as New.Move.Core.Motiontree).Connections.Add(connect);
                }
                else 
                if (sender is SignalResultItem && receiver is OperatorSourceItem)
                {
                    OperatorSourceConnection connect =
                        new OperatorSourceConnection((sender as AnchorItem).Id, (receiver as AnchorItem).Id);
                    //(Runtime.Database.Active as New.Move.Core.Motiontree).Connections.Add(connect);
                }
                else
                if (sender is MessageResultItem)
                {
                    MessageConnection connect =
                        new MessageConnection((sender as AnchorItem).Id, (receiver as AnchorItem).Id);
                    //(Runtime.Database.Active as New.Move.Core.Motiontree).Connections.Add(connect);
                }
                else
                {
                    TransformConnection connect =
                        new TransformConnection((sender as AnchorItem).Id, (receiver as AnchorItem).Id);
                    //(Runtime.Database.Active as New.Move.Core.Motiontree).Connections.Add(connect);
                }
            }

            if (Runtime.Database.Active is New.Move.Core.StateMachine)
            {
                /*
                TransformConnection connect = 
                    new TransformConnection((sender as AnchorItem).Id, (receiver as AnchorItem).Id);
                (Runtime.Database.Active as New.Move.Core.StateMachine).Connections.Add(connect);
                 */
            }

            Diagram diagram = GetDiagram(sender as DependencyObject);
            if (diagram != null)
                diagram.OnConnection();
        }

        public void OnTransition(ITransition transition)
        {
            /*
            if (Runtime.Database.Active is New.Move.Core.StateMachine)
                (Runtime.Database.Active as New.Move.Core.StateMachine).Transitions.Add(transition);

            Diagram diagram = DiagramViewer.Instance.Diagram;
            if (diagram != null)
                diagram.OnTransition();
             */
        }

        public void OnChildLoaded(object sender, EventArgs e)
        {
            Children.Remove(sender as FrameworkElement);

            if (Children.Count == 0)
                OnChildrenLoaded();
        }

        protected Diagram GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is Diagram))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as Diagram;
        }
    }
}
