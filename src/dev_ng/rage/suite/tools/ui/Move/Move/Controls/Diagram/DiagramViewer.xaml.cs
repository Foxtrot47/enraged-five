﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using Rage.Move.Core;
using Rage.Move.Core.Dag;
using Rage.Move.Core.Dg;

namespace Rage.Move
{
    public partial class DiagramViewer : UserControl
    {
        private static class Const
        {
            public static double PanMargin = 50;

            public static double AutoCenterAnimationPauseDuration = 1000;

            public static double AutoCenterAnimationDuration = 600;
        }

        private Point scrollStartPoint;
        private Point scrollStartOffset;

        private Point previousTopLeftOffset;

        DispatcherTimer autoCenterTimer;
        static DiagramViewer instance;

        public double Zoom
        {
            get { return ZoomSlider.Value; }
            set
            {
                if (value >= ZoomSlider.Minimum && value <= ZoomSlider.Maximum)
                {
                    Diagram.Scale = value;
                    ZoomSlider.Value = value;
                    UpdateScrollSize();
                }
            }
        }

        public DiagramViewer()
        {
            InitializeComponent();

            this.Zoom = 1;

            instance = this;
        }

        public static DiagramViewer Instance
        {
            get { return instance; }
        }

        protected override void OnInitialized(EventArgs e)
        {
            autoCenterTimer = new DispatcherTimer();

            Diagram.Loaded += new RoutedEventHandler(Diagram_Loaded);
            Diagram.SizeChanged += new SizeChangedEventHandler(Diagram_SizeChanged);
            Diagram.DiagramUpdated += new EventHandler(Diagram_DiagramUpdated);
            Diagram.DiagramPopulated += new EventHandler(Diagram_DiagramPopulated);

            ZoomSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(ZoomSlider_ValueChanged);
            ZoomSlider.MouseDoubleClick += new MouseButtonEventHandler(ZoomSlider_MouseDoubleClick);

            this.SizeChanged += new SizeChangedEventHandler(Diagram_SizeChanged);
            ScrollViewer.ScrollChanged += new ScrollChangedEventHandler(ScrollViewer_ScrollChanged);

            base.OnInitialized(e);
        }

        private void Diagram_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateScrollSize();
            AutoScrollToSelected();
        }

        private void Diagram_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateScrollSize();
        }

        private void Diagram_DiagramUpdated(object sender, EventArgs e)
        {
            UpdateLayout();
        }

        private void Diagram_DiagramPopulated(object sender, EventArgs e)
        {
            Point offset = GetTopLeftScrollOffset();
            previousTopLeftOffset = new Point(
                Grid.ActualWidth - ScrollViewer.HorizontalOffset - offset.X,
                Grid.ActualHeight - ScrollViewer.VerticalOffset - offset.Y);

            this.UpdateLayout();

            AutoScrollToSelected();
        }

        private void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.Zoom = e.NewValue;
        }

        private void ZoomSlider_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ZoomSlider.Value = 1.0;
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.ExtentWidthChange != 0 &&
               e.ExtentWidthChange != e.ExtentWidth)
            {
                // Keep centered horizontaly.
                double percent = e.ExtentWidthChange / (e.ExtentWidth - e.ExtentWidthChange);
                double middle = e.HorizontalOffset + (e.ViewportWidth / 2);
                ScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset + (middle * percent));
            }

            if (e.ExtentHeightChange != 0 &&
                e.ExtentHeightChange != e.ExtentHeight)
            {
                // Keep centered verically.
                double percent = e.ExtentHeightChange / (e.ExtentHeight - e.ExtentHeightChange);
                double middle = e.VerticalOffset + (e.ViewportHeight / 2);
                ScrollViewer.ScrollToVerticalOffset(e.VerticalOffset + (middle * percent));
            }
        }

        private Point GetTopLeftScrollOffset()
        {
            Point offset = new Point();

            if (Diagram.ActualWidth == 0 || Diagram.ActualHeight == 0)
                return offset;

            Size diagramSize = new Size(
                Diagram.ActualWidth * this.Zoom,
                Diagram.ActualHeight * this.Zoom);
            
            offset.X = this.ActualWidth + diagramSize.Width / 4 - (Const.PanMargin / 2);
            offset.Y = this.ActualHeight + diagramSize.Height / 4 - (Const.PanMargin / 2);

            return offset;
        }

        private void UpdateScrollSize()
        {
            if (this.ActualWidth == 0 || this.ActualHeight == 0)
                return;

            Size diagramSize = new Size(
                Diagram.ActualWidth * Zoom,
                Diagram.ActualHeight * Zoom);

            Grid.Width = Math.Max(0, (ActualWidth * 2) + diagramSize.Width - Const.PanMargin);
            Grid.Height = Math.Max(0, (ActualHeight * 2) + diagramSize.Height - Const.PanMargin);
        }

        private void AutoScrollToSelected()
        {
            if (Diagram.ActualWidth == 0 || Diagram.ActualHeight == 0)
                return;

            Point offset = GetTopLeftScrollOffset();

            Rect selectedBounds = Diagram.SelectedNodeBounds;

            if (selectedBounds.IsEmpty)
            {
                offset.X += ((this.ActualWidth - (Diagram.Size.Width * this.Zoom)) / 2);
                offset.Y += ((this.ActualHeight - (Diagram.Size.Height * this.Zoom)) / 2);
            }
            else
            {
                offset.X += previousTopLeftOffset.X;
                offset.Y += previousTopLeftOffset.Y;
            }

            ScrollViewer.ScrollToHorizontalOffset(Grid.Width - offset.X);
            ScrollViewer.ScrollToVerticalOffset(Grid.Height - offset.Y);

            autoCenterTimer.Interval = App.GetAnimationDuration(Const.AutoCenterAnimationPauseDuration);
            autoCenterTimer.Tick += new EventHandler(OnAutoCenterPauseTimer);
            autoCenterTimer.IsEnabled = true;
        }

        private void OnAutoCenterPauseTimer(object sender, EventArgs e)
        {
            autoCenterTimer.IsEnabled = false;

            AutoScrollToCenter();
        }

        private void AutoScrollToCenter()
        {
            Point offset = GetTopLeftScrollOffset();

            offset.X += ((this.ActualWidth - (Diagram.Size.Width * this.Zoom)) / 2);
            offset.Y += ((this.ActualHeight - (Diagram.Size.Height * this.Zoom)) / 2);

            Point startLocation = new Point(
                ScrollViewer.HorizontalOffset, ScrollViewer.VerticalOffset);

            Point endLocation = new Point(
                Grid.Width - offset.X - startLocation.X,
                Grid.Height - offset.Y - startLocation.Y);

            ScrollViewer.ScrollToHorizontalOffset(Grid.Width - offset.X);
            ScrollViewer.ScrollToVerticalOffset(Grid.Height - offset.Y);

            AnimateDiagram(endLocation);
        }

        private void AnimateDiagram(Point endLocation)
        {
            DoubleAnimation horzAnim = new DoubleAnimation(endLocation.X, 0,
                App.GetAnimationDuration(Const.AutoCenterAnimationDuration));
            horzAnim.AccelerationRatio = 0.5;
            horzAnim.DecelerationRatio = 0.5;

            DoubleAnimation vertAnim = new DoubleAnimation(endLocation.Y, 0,
                App.GetAnimationDuration(Const.AutoCenterAnimationDuration));
            vertAnim.AccelerationRatio = 0.5;
            vertAnim.DecelerationRatio = 0.5;

            TranslateTransform transform = new TranslateTransform();
            transform.BeginAnimation(TranslateTransform.XProperty, horzAnim);
            transform.BeginAnimation(TranslateTransform.YProperty, vertAnim);

            Grid.RenderTransform = transform;
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) > 0)
            {
                e.Handled = true;
                this.Zoom += (e.Delta > 0) ? ZoomSlider.LargeChange : -ZoomSlider.LargeChange;
            }

            base.OnPreviewMouseWheel(e);
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(Diagram);
            if ( ScrollViewer.IsMouseOver && !Diagram.IsMouseOver && !layer.IsMouseOver)
            {
                scrollStartPoint = e.GetPosition(this);
                scrollStartOffset.X = ScrollViewer.HorizontalOffset;
                scrollStartOffset.Y = ScrollViewer.VerticalOffset;

                this.Cursor = (ScrollViewer.ExtentWidth > ScrollViewer.ViewportWidth) ||
                    (ScrollViewer.ExtentHeight > ScrollViewer.ViewportHeight) ?
                    Cursors.ScrollAll : Cursors.Arrow;

                this.CaptureMouse();
            }
        }

        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            /*
            if (e.ChangedButton == MouseButton.Left)
                Runtime.Database.Active.OnDeactivate();
             */
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            if (this.IsMouseCaptured)
            {
                Point point = e.GetPosition(this);

                Point delta = new Point(
                    (point.X > this.scrollStartPoint.X) ?
                        -(point.X - this.scrollStartPoint.X) : (this.scrollStartPoint.X - point.X),
                    (point.Y > this.scrollStartPoint.Y) ?
                        -(point.Y - this.scrollStartPoint.Y) : (this.scrollStartPoint.Y - point.Y));

                ScrollViewer.ScrollToHorizontalOffset(this.scrollStartOffset.X + delta.X);
                ScrollViewer.ScrollToVerticalOffset(this.scrollStartOffset.Y + delta.Y);
            }

            base.OnPreviewMouseMove(e);
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            if (this.IsMouseCaptured)
            {
                this.Cursor = Cursors.Arrow;
                this.ReleaseMouseCapture();
            }

            base.OnPreviewMouseUp(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                ApplicationCommands.Delete.Execute(null, Diagram);
                e.Handled = true;
            }
        }

        private void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu menu = (ContextMenu)sender;

            {
                MenuItem item = new MenuItem();
                item.Header = "Create Parent State Machine";
                item.Click += new RoutedEventHandler(CreateParentStateMachine);
                menu.Items.Add(item);
            }

            if (Runtime.Database.Active is New.Move.Core.Motiontree)
            {
                MenuItem item = new MenuItem();
                item.Header = "Create Reference";
                item.Click += new RoutedEventHandler(CreateReference);
                menu.Items.Add(item);
            }
        }

        private void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu menu = (ContextMenu)sender;
            menu.Items.Clear();
        }

        // find a better place for this...
        void CreateParentStateMachine(object sender, RoutedEventArgs e)
        {
            New.Move.Core.ITransitional current = Runtime.Database.Active;
            New.Move.Core.ITransitional parent = 
                Runtime.Database.Create(New.Move.Core.StateMachine.Desc);
            parent.Parent = current.Parent;
            parent.Add(current);

            Runtime.Database.Active = parent;
            if (parent.Parent == null)
                Runtime.Database.Root = parent;
        }

        void CreateReference(object sender, RoutedEventArgs e)
        {
            CommonDialog dialog = new CommonDialog();
            dialog.InitialDirectory = New.Move.Core.Database.ApplicationFolderPath;
            dialog.Filter.Add(new FilterEntry(Properties.Resources.Files, Properties.Resources.FileExtension));
            dialog.Filter.Add(new FilterEntry(Properties.Resources.AllFiles, Properties.Resources.AllExtension));
            dialog.Title = Properties.Resources.Open;
            dialog.ShowOpen();
#if disabled_reference
            if (!String.IsNullOrEmpty(dialog.Filename))
            {
                MotionTree current = Runtime.Database.Active as MotionTree;
                Reference reference = new Reference(dialog.Filename);
                reference.Parent = current;
                current.Children.Add(reference);
            }
#endif
        }
    }
}
