﻿using System;
using System.Windows.Media;

namespace New.Move.Core
{
    public interface IFileAssociatedDesc
    {
        string FileExtension { get; }
    }
}