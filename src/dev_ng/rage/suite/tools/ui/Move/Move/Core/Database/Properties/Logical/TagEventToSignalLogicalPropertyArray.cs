﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class TagEventToSignalLogicalPropertyArray : LogicalPropertyArray<TagEventToSignalLogicalProperty>, ISerializable
    {
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public TagEventToSignalLogicalPropertyArray(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public TagEventToSignalLogicalPropertyArray()
            : base()
        {
        }

        public TagEventToSignalLogicalPropertyArray(ILogic parent)
        {
            Parent = parent;
        }

        public TagEventToSignalLogicalPropertyArray(ILogic parent, TagEventToSignalLogicalPropertyArray other)
            : base()
        {
            Parent = parent;
            Name = other.Name;
            Tag = null;

            foreach (TagEventToSignalLogicalProperty item in other.Children)
            {
                this.Items.Add(new TagEventToSignalLogicalProperty(item));
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }

        }

        public override object Clone()
        {
            return new TagEventToSignalLogicalPropertyArray(this.Parent, this);
        }
    }
}
