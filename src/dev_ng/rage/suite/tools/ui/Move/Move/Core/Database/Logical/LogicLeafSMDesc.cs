﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace New.Move.Core
{
    public class LogicLeafSMDesc : ITransitionalDesc
    {
        static class Const
        {
            public static string Name = "State machine";
            public static int Id = 0;
            public static string Help = "State machine";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get; private set; }

        public Type ConstructType { get { return typeof(LogicLeafSM); } }

        public LogicLeafSMDesc()
        {
            _Children.Add(new StateMachineDesc());
            _Children.Add(new MotiontreeDesc());

            try
            {
                BitmapImage icon = new BitmapImage();
                icon.BeginInit();
                icon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                    "Move.Core.Images.PaletteIcon_LeafState.png");
                icon.EndInit();

                Icon = icon;
            }
            catch
            {
                Icon = null;
            }
        }

        List<IDesc> _Children = new List<IDesc>();
        public List<IDesc> Children { get { return _Children; } }

        public ITransitional Create(Database database, Point offset)
        {
            return new LogicLeafSM(offset);
        }

        public INode Create(Database database)
        {
            return new LogicLeafSM();
        }
    }
}