﻿using System;

namespace New.Move.Core
{
    public class IsEngagedChangedEventArgs : EventArgs
    {
        public IsEngagedChangedEventArgs(EngagedChangedAction action)
        {
            Action = action;
        }
        public EngagedChangedAction Action { get; private set; }
    }
}