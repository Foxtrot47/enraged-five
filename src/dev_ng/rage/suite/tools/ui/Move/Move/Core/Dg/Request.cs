﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Xml.Serialization;

using Rage.Move.Core.Dag;

namespace Rage.Move.Core.Dg
{

    public class RequestConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type type)
        {
            if (type == typeof(string))
                return true;

            return base.CanConvertFrom(context, type);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                Guid id = new Guid(value as string);
#if removed
                foreach (Database database in Runtime.Instance.Databases.Values)
                {
                    IEnumerable<Request> item =
                        from request in database.Requests where request.Id == id select request;

                    if (item.Count() != 0)
                        return item.ElementAt(0);
                }
#endif
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type type)
        {
            //if (type == typeof(string))
            //    return (value as Request).Id.ToString();

            return base.ConvertTo(context, culture, value, type);
        }
    }
}
