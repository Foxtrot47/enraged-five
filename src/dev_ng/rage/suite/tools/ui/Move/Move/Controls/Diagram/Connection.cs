﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Rage.Move
{
    public class Connection : Control, INotifyPropertyChanged
    {
        public Connection(IAnchorSender source, IAnchorReceiver sink)
        {
            Id = Guid.NewGuid();
            Source = source;
            Sink = sink;

            Unloaded += new RoutedEventHandler(Connection_Unloaded);
        }

        public Guid Id { get; private set; }
        Adorner Adorner { get; set; }
        AdornerLayer Layer { get; set; }

        PathGeometry _Geometry;
        public PathGeometry Geometry
        { 
            get { return _Geometry; }
            set
            {
                if (_Geometry != value)
                {
                    _Geometry = value;
                    OnPropertyChanged("Geometry");
                }
            }
        }

        IAnchorSender _Source;
        public IAnchorSender Source
        {
            get { return _Source; }
            set
            {
                if (_Source != value)
                {
                    if (_Source != null)
                        (_Source as IAnchorItem).PropertyChanged -= new PropertyChangedEventHandler(AnchorItem_PropertyChanged);

                    _Source = value;

                    if (_Source != null)
                        (_Source as IAnchorItem).PropertyChanged += new PropertyChangedEventHandler(AnchorItem_PropertyChanged);

                    UpdateGeometry();
                }
            }
        }

        IAnchorReceiver _Sink;
        public IAnchorReceiver Sink
        {
            get { return _Sink; }
            set
            {
                if (_Sink != value)
                {
                    if (_Sink != null)
                        (_Sink as IAnchorItem).PropertyChanged -= new PropertyChangedEventHandler(AnchorItem_PropertyChanged);

                    _Sink = value;

                    if (_Sink != null)
                        (_Sink as IAnchorItem).PropertyChanged += new PropertyChangedEventHandler(AnchorItem_PropertyChanged);

                    UpdateGeometry();
                }
            }
        }

        public Point SenderSource
        {
            get { return Source.SenderPoint.Value; }
        }

        public Point ReceiverSink
        {
            get { return Sink.ReceiverPoint.Value; }
        }

        void UpdateGeometry()
        {
            if (_Source != null && _Source.SenderPoint.HasValue && _Sink != null && _Sink.ReceiverPoint.HasValue)
            {
                PathGeometry geometry = new PathGeometry();

                Point start = new Point(0, 0);

                Point end = new Point(
                    _Sink.ReceiverPoint.Value.X - _Source.SenderPoint.Value.X,
                    _Sink.ReceiverPoint.Value.Y - _Source.SenderPoint.Value.Y);

                if (_Sink.ReceiverPoint.Value.Y < _Source.SenderPoint.Value.Y)
                {
                    double delta = _Source.SenderPoint.Value.Y - _Sink.ReceiverPoint.Value.Y;

                    start.Y += delta;
                    end.Y += delta;
                }

                PathFigure figure = new PathFigure();
                figure.StartPoint = start;

                PointCollection points = new PointCollection();
                points.Add(new Point(end.X / 2, start.Y));
                points.Add(new Point(end.X / 2, end.Y));
                points.Add(end);

                figure.Segments.Add(new PolyBezierSegment(points, true));
                geometry.Figures.Add(figure);

                Geometry = geometry;
            }
        }

        void AnchorItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Position"))
                UpdateGeometry();
        }

        void Connection_Unloaded(object sender, RoutedEventArgs e)
        {
            Source = null;
            Sink = null;

            if (Adorner != null)
            {
                if (Layer != null)
                {
                    Layer.Remove(Adorner);
                    Adorner = null;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }

    public class ConnectionAdorner : Adorner
    {
        public ConnectionAdorner(Diagram diagram, Connection connection)
            : base(diagram)
        {
            Adorner = new Canvas();

            Children = new VisualCollection(this);
            Children.Add(Adorner);

            Connection = connection;
            Connection.PropertyChanged += new PropertyChangedEventHandler(Connection_PropertyChanged);

            Pen = new Pen(Brushes.LightSlateGray, 1);
            Pen.LineJoin = PenLineJoin.Round;

            base.Unloaded += new RoutedEventHandler(ConnectionAdorner_Unloaded);
        }

        Connection Connection { get; set; }
        Pen Pen { get; set; }
        Canvas Adorner { get; set; }
        VisualCollection Children { get; set; }

        protected override void OnRender(DrawingContext context)
        {
            base.OnRender(context);
        }

        void Connection_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SenderPoint"))
            {
            }

            if (e.PropertyName.Equals("ReceiverPoint"))
            {
            }
        }

        void ConnectionAdorner_Unloaded(object sender, RoutedEventArgs e)
        {
        }
    }

    public interface IConnectable
    {
        void OnConnectionEnter(IAnchorSender sender);
        void OnConnectionExit(IAnchorSender sender);
    }
}
