﻿using System;
using System.Runtime.Serialization;

using Move.Utils;
using System.Xml;

namespace New.Move.Core
{
    [Serializable]
    public class TunnelEventAnchor : ResultAnchor, ISerializable
    {
        public TunnelEventAnchor(TunnelEvent parent)
        {
            Parent = parent;
        }

        public TunnelEventAnchor(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(SerializationTag.Name);
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
            Connections = (NoResetObservableCollection<Connection>)info.GetValue(
                SerializationTag.Connections, typeof(NoResetObservableCollection<Connection>));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TunnelEventAnchor"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public TunnelEventAnchor(XmlReader reader, ILogic parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            reader.Skip();
        }


        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Connections, Connections);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Connections = "Connections";
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public virtual void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}