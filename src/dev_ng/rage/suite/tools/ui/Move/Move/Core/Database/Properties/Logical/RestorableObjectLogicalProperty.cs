﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

using Move.Core;
using Move.Utils;

using Move.Core.Restorable;

namespace New.Move.Core
{
    [Flags]
    public enum RestorableObjectType
    {
        Request = (1 << 0),
        Flag = (1 << 1)
    }

    [Serializable]
    public class RestorableObjectLogicalProperty : Property, ISerializable
    {
        public RestorableObjectLogicalProperty()
        {
            Id = Guid.NewGuid();
            Connections = new NoResetObservableCollection<Connection>();
        }

        public RestorableObjectLogicalProperty(RestorableObjectLogicalProperty other)
        {
            this._Input = other._Input;
            this._ObjectType = other._ObjectType;
            this._OutputEnabled = other._OutputEnabled;
            if (other.SelectedEvent != null && other.SelectedEvent is ICloneable)
                this._SelectedEvent = (other._SelectedEvent as ICloneable).Clone() as Signal;
            this._SelectedObject = other._SelectedObject;

            Id = Guid.NewGuid();
            Connections = new NoResetObservableCollection<Connection>();
        }

        public RestorableObjectLogicalProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public object Tag { get; set; }

        RestorableObjectType _ObjectType;
        public RestorableObjectType ObjectType
        {
            get { return _ObjectType; }
            set
            {
                if (_ObjectType != value)
                {
                    _ObjectType = value;
                    OnPropertyChanged("ObjectType");
                }
            }
        }

        RestorableObject _SelectedObject;
        public RestorableObject SelectedObject
        {
            get { return _SelectedObject; }
            set
            {
                if (_SelectedObject != value)
                {
                    _SelectedObject = value;
                    OnPropertyChanged("SelectedObject");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        public PropertyInput Flags { get; private set; }
        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }


        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }
        
        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            from.RemoveConnectionsFromAnchor(this);
        }
        
        public ILogic Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string SelectedEvent = "SelectedEvent";
            public static string SelectedObject = "SelectedObject";
            public static string ObjectType = "ObjectType";
        }

        public RestorableObjectLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
            SelectedEvent = (Signal)info.GetValue(SerializationTag.SelectedEvent, typeof(Signal));
            SelectedObject = (RestorableObject)info.GetValue(SerializationTag.SelectedObject, typeof(RestorableObject));
            ObjectType = (RestorableObjectType)info.GetValue(SerializationTag.ObjectType, typeof(RestorableObjectType));
            
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.SelectedEvent, SelectedEvent);
            info.AddValue(SerializationTag.SelectedObject, SelectedObject);
            info.AddValue(SerializationTag.ObjectType, ObjectType);
        }

        public object Clone()
        {
            return new RestorableObjectLogicalProperty(this);
        }
    }
}
