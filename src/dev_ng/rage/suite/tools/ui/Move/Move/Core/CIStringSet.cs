﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace New.Move.Core
{
    public class CIStringSet : Set<string>
    {
        protected override bool ContainsImpl(string item)
        {
            return items_.Contains(item, equalityComparer_);
        }

        private CIStringEqualityComparer equalityComparer_ = new CIStringEqualityComparer();
    }
}
