﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml;
using Move.Core.Restorable;

namespace New.Move.Core
{
    /// <summary>
    /// Provides a base class for a transition between two <see cref="ITransitional"/> objects.
    /// </summary>
    public abstract class TransitionBase : RestorableObject, ITransition, ISerializable
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="Tag"/> property.
        /// </summary>
        private object tag;

        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field for the <see cref="Source"/> property.
        /// </summary>
        private ITransitional source;

        /// <summary>
        /// The private field for the <see cref="Dest"/> property.
        /// </summary>
        private ITransitional destination;

        /// <summary>
        /// The private field for the <see cref="Parent"/> property.
        /// </summary>
        private IAutomaton parent;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.TransitionBase"/> class.
        /// </summary>
        protected TransitionBase()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.TransitionBase"/> class.
        /// </summary>
        /// <param name="source">
        /// Represents the source object this transition will have.
        /// </param>
        /// <param name="destination">
        /// Represents the destination object this transition will have.
        /// </param>
        public TransitionBase(ITransitional source, ITransitional destination)
        {
            this.id = Guid.NewGuid();

            this.source = source;
            this.destination = destination;

            Database database = null;
            Guid parent = Guid.Empty;
            if (source != null && Source.Database != null)
            {
                parent = source.Parent;
                database = Source.Database;
            }
            else if (destination != null && destination.Database != null)
            {
                parent = destination.Parent;
                database = Dest.Database;
            }

            if (database != null)
            {
                this.parent = (IAutomaton)database.Find(parent);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the selection state of this transition has changed.
        /// </summary>
        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the unique global identifier for this transition.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or sets the object tag that can be set to the UI component representing
        /// this transition.
        /// </summary>
        public object Tag
        {
            get
            {
                return tag;
            }

            set
            {
                Debug.Assert(tag == null);
                tag = value;
            }
        }

        /// <summary>
        /// Gets or sets the source transitional object for this transition.
        /// </summary>
        public ITransitional Source
        {
            get { return this.source; }
            set { this.source = value; }
        }

        /// <summary>
        /// Gets or sets the transitional object that is the destination for this transition.
        /// </summary>
        public ITransitional Dest
        {
            get { return this.destination; }
            set { this.destination = value; }
        }

        /// <summary>
        /// Gets or sets the automaton parent for this transition.
        /// </summary>
        public IAutomaton Parent
        {
            get { return this.parent; }
            set { this.parent = value; }
        }

        /// <summary>
        /// Gets the boolean property that represents whether or not this transition
        /// is currently enabled.
        /// </summary>
        public abstract BoolTransitionProperty Enabled { get; protected set; }

        /// <summary>
        /// Get the type of modifier for this transition.
        /// </summary>
        public abstract ModifierTransitionProperty Modifier { get; protected set; }

        /// <summary>
        /// Gets the duration value for this transition.
        /// </summary>
        public abstract RealTransitionProperty Duration { get; protected set; }

        /// <summary>
        /// Get the type of syncing this transition does.
        /// </summary>
        public abstract SyncConditionProperty Sync { get; protected set; }

        /// <summary>
        /// Gets the filter template for this transition.
        /// </summary>
        public abstract FilterLogicalProperty FilterTemplate { get; protected set; }

        /// <summary>
        /// Gets the boolean property that represents whether or not this transition
        /// blocks any updating going on while the transition is taking place.
        /// </summary>
        public abstract BoolTransitionProperty BlockUpdateOnTransition { get; protected set; }

        /// <summary>
        /// Gets the weight value that is applied to this transition.
        /// </summary>
        public abstract RealSignalOutputProperty TransitionWeightOutput { get; protected set; }

        /// <summary>
        /// Gets the boolean property that represents whether or not this transition
        /// is currently set to a transitional.
        /// </summary>
        public abstract BoolTransitionProperty Transitional { get; protected set; }

        /// <summary>
        /// Gets the boolean property that represents whether or not this transition
        /// is currently set to immutable.
        /// </summary>
        public abstract BoolTransitionProperty Immutable { get; protected set; }

        /// <summary>
        /// Gets the type descriptor for this transition.
        /// </summary>
        public abstract Type Descriptor { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Disposes of this current instance.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Fires the selection changed event.
        /// </summary>
        /// <param name="action">
        /// Defines whether this transition has been added to the selection or removed from
        /// the selection.
        /// </param>
        /// <param name="count">
        /// The total number of items that are currently selected.
        /// </param>
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            EventHandler<IsSelectedChangedEventArgs> handler = this.IsSelectedChanged;
            if (handler == null)
                return;

            this.IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        object ICloneable.Clone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        public abstract ISelectable Clone();

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public abstract void Serialise(XmlWriter writer);

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialize this instance.
        /// </summary>
        /// <param name="info">
        /// The serialization info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
        }

        public abstract void ResolvePoints(Database database, ITransitional parentSource, ITransitional parentDest);

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public abstract void ResetIds(Dictionary<Guid, Guid> map);

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public abstract List<Guid> GetAllGuids();
        #endregion
    } // New.Move.Core.TransitionBase
} // New.Move.Core
