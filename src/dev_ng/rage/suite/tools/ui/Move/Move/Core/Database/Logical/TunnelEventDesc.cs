﻿using System;
using System.Windows.Media;

namespace New.Move.Core
{
    public class TunnelEventDesc : ILogicDesc
    {
        static class Const 
        {
            public static string Name = "TunnelEvent";
            public static int Id = -1;
            public static string Help = "";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return null; } }
        public ImageSource Icon { get { return null; } }

        public Type ConstructType { get { return typeof(TunnelEvent); } }

        public INode Create(Database database)
        {
            return new TunnelEvent() { Name = Const.Name };
        }
    }
}