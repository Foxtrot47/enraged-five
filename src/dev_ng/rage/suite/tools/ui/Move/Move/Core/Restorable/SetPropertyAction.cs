﻿using System.Reflection;

namespace Move.Core.Restorable
{
    internal sealed class SetPropertyAction : IRestorableAction
    {
        public void Undo()
        {
            PropertyInfo pi = Target.GetType().GetProperty(PropertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            using (new WithoutUnregisteredChanged(Target))
            {
                pi.SetValue(Target, OldValue, new object[0]);
            }
        }

        public void Redo()
        {
            PropertyInfo pi = Target.GetType().GetProperty(PropertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            using (new WithoutUnregisteredChanged(Target))
            {
                pi.SetValue(Target, NewValue, new object[0]);
            }

        }

        public string PropertyName { get; set; }
        public object NewValue { get; set; }
        public object OldValue { get; set; }

        public object Target { get; set; }
    }
}