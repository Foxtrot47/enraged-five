﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace New.Move.Core
{
    public class StateMachineDesc : ITransitionalDesc
    {
        static class Const
        {
            public static string Name = "StateMachine";
            public static int Id = 1;
            public static string Help = "StateMachine";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get; private set; }

        public Type ConstructType { get { return typeof(StateMachine); } }

        public StateMachineDesc()
        {
            try
            {
                BitmapImage icon = new BitmapImage();
                icon.BeginInit();
                icon.StreamSource =
                    System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(
                    "Move.Core.Images.PaletteIcon_StateMachine.png");
                icon.EndInit();

                Icon = icon;
            }
            catch
            {
                Icon = null;
            }

            // This is a bit tricky, but Motiontree Desc is statically initialized before this
            //_Children.Add(this);
            //_Children.Add(Motiontree.Desc);
        }
        /*
        List<IDesc> _Children = new List<IDesc>();
        public List<IDesc> Children { get { return _Children; } }
         */

        public ITransitional Create(Database database, Point offset)
        {
            return new StateMachine(database, offset);
        }

        public INode Create(Database database)
        {
            return new StateMachine(database);
        }
    }
}