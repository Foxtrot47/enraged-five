﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class ExpressionReference : ReferenceSignal, ISerializable
    {
        public ExpressionReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public ExpressionReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override Parameter GetParameter()
        {
            return Parameter.Expressions;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}