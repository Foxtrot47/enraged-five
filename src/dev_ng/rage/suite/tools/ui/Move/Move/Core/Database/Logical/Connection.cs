﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;

using Move.Core.Restorable;
using System.Xml;
using System.Globalization;
using System.Collections.Generic;

namespace New.Move.Core
{
    /// <summary>
    /// Represents a connection between two <see cref="New.Move.Core.IAnchor"/> objects.
    /// </summary>
    [Serializable]
    public class Connection : RestorableObject, ISelectable, ISerializable
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field for the <see cref="Source"/> property.
        /// </summary>
        private IAnchor source;

        /// <summary>
        /// The private field for the <see cref="Dest"/> property.
        /// </summary>
        private IAnchor destination;

        /// <summary>
        /// The private field for the <see cref="Tag"/> property.
        /// </summary>
        private object tag;

        /// <summary>
        /// The private field for the <see cref="IsProxy"/> property.
        /// </summary>
        private bool isProxy;

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        private const string IdSerialisationTag = @"Id";
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Source"/> property.
        /// </summary>
        private const string SourceSerialisationTag = @"Source";
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Dest"/> property.
        /// </summary>
        private const string DestSerialisationTag = @"Dest";
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="IsProxy"/> property.
        /// </summary>
        private const string ProxySerialisationTag = @"Proxy";
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the unique global identifier for this flag.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets the source anchor for this connection.
        /// </summary>
        public IAnchor Source
        {
            get { return this.source; }
            private set { this.source = value; }
        }

        /// <summary>
        /// Gets the destination anchor for this connection.
        /// </summary>
        public IAnchor Dest
        {
            get { return this.destination; }
            private set { this.destination = value; }
        }

        /// <summary>
        /// Gets or sets the object tag that can be set to the UI component representing
        /// this logic parent.
        /// </summary>
        public object Tag
        {
            get { return this.tag; }
            set { this.tag = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this connection is a proxy connection.
        /// </summary>
        public bool IsProxy
        {
            get
            {
                return this.isProxy;
            }

            set
            {
                if (bool.Equals(value, this.isProxy))
                    return;

                this.isProxy = value;

                EventHandler<ConnectionProxyEventArgs> handler = this.IsProxyChanged;
                if (handler != null)
                    this.IsProxyChanged(this, new ConnectionProxyEventArgs(value));
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new <see cref="New.Move.Core.Connection"/> class with the specified
        /// source and destination.
        /// </summary>
        /// <param name="source">
        /// The source anchor for the connection.
        /// </param>
        /// <param name="dest">
        /// The destination anchor for the connection.
        /// </param>
        public Connection(IAnchor source, IAnchor dest)
        {
            this.Source = source;
            this.Dest = dest;
            this.Id = Guid.NewGuid();
            this.IsProxy = false;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Connection"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public Connection(SerializationInfo info, StreamingContext context)
        {
            this.Id = (Guid)info.GetValue(IdSerialisationTag, typeof(Guid));
            this.Source = (IAnchor)info.GetValue(SourceSerialisationTag, typeof(IAnchor));
            this.Dest = (IAnchor)info.GetValue(DestSerialisationTag, typeof(IAnchor));
            this.IsProxy = (bool)info.GetValue(ProxySerialisationTag, typeof(bool));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Connection"/> class using the
        /// specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Connection(XmlReader reader, Motiontree parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Deserialise(reader, parent, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Connection"/> class using the
        /// specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="idMap">
        /// A map containing a possible id mapping that has been applied to the parent ids.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Connection(XmlReader reader, Motiontree parent, Dictionary<Guid, Guid> idMap)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Deserialise(reader, parent, idMap);
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs whenever the <see cref="IsProxy"/> value changes.
        /// </summary>
        public event EventHandler<ConnectionProxyEventArgs> IsProxyChanged;

        /// <summary>
        /// Occurs when the selection state of this transition has changed.
        /// </summary>
        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        #endregion

        #region Methods
        /// <summary>
        /// Fires the selection changed event.
        /// </summary>
        /// <param name="action">
        /// Defines whether this transition has been added to the selection or removed from
        /// the selection.
        /// </param>
        /// <param name="count">
        /// The total number of items that are currently selected.
        /// </param>
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            EventHandler<IsSelectedChangedEventArgs> handler = this.IsSelectedChanged;
            if (handler == null)
                return;

            this.IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
        }

        /// <summary>
        /// Disposes of this current instance.
        /// </summary>
        public void Dispose()
        {
            if (Source.Parent.Parent != Guid.Empty)
            {
                Motiontree parent = Source.Parent.Database.Find(Source.Parent.Parent) as Motiontree;
                parent.Connections.Remove(this);
            }
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        protected void ObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(IdSerialisationTag, Id);
            info.AddValue(SourceSerialisationTag, Source);
            info.AddValue(DestSerialisationTag, Dest);
            info.AddValue(ProxySerialisationTag, IsProxy);
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            ObjectData(info, context);
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("connection");

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "Proxy", this.IsProxy.ToString(CultureInfo.InvariantCulture));

            if (this.Dest != null)
            {
                writer.WriteAttributeString(
                    "Destination", this.Dest.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            if (this.Source != null)
            {
                writer.WriteAttributeString(
                    "Source", this.Source.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, Motiontree parent, Dictionary<Guid, Guid> idMap)
        {
            if (reader == null)
            {
                return;
            }

            this.IsProxy = bool.Parse(reader.GetAttribute("Proxy"));
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");

            string destinationId = reader.GetAttribute("Destination");
            if (destinationId != null)
            {
                Guid id = Guid.ParseExact(destinationId, "D");
                if (idMap != null)
                {
                    id = Database.GetId(idMap, id, id);
                }

                this.Dest = parent.GetAnchor(id);
            }

            string sourceId = reader.GetAttribute("Source");
            if (sourceId != null)
            {
                Guid id = Guid.ParseExact(sourceId, "D");
                if (idMap != null)
                {
                    id = Database.GetId(idMap, id, id);
                }

                this.Source = parent.GetAnchor(id);
            }

            reader.Skip();
        }
        #endregion
    }
}