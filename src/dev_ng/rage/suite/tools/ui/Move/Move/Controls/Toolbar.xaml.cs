﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

using Rage.Move.Core;
using Rage.Move.Core.Dag;
using Rage.Move.Core.Dg;

namespace Rage.Move
{
    public partial class Toolbar : UserControl
    {
        public static readonly DependencyProperty PathProperty = 
            DependencyProperty.Register("Path", typeof(string), typeof(Toolbar));

        public string Path
        {
            get { return (string)GetValue(PathProperty); }
            set { SetValue(PathProperty, value); }
        }

        public Toolbar()
        {
            InitializeComponent();
            DataContext = this;

            //Runtime.Database.ActiveChanged += new ActiveChangedHandler(OnActiveChanged);
        }

        void OnActiveChanged(New.Move.Core.ITransitional active)
        {
#if disabled_path
            if (active == null)
            {
                Path = "";
                return;
            }

            Stack<string> path = new Stack<string>();

            Stack<ITransitionalNode> stack = new Stack<ITransitionalNode>();
            stack.Push(active);

            while (stack.Count != 0)
            {
                ITransitionalNode top = stack.Pop();
                
                path.Push(top.Name);

                if (top is StateMachine)
                {
                    StateMachine node = top as StateMachine;
                    if (node.LogicalParent != null)
                    {
                        ITransitionalNode parent = node.LogicalParent.Parent;
                        stack.Push(parent);
                    }
                    else
                    if (node.Parent != null)
                    {
                        stack.Push(node.Parent);
                    }
                }

                if (top is MotionTree)
                {
                    if (top.Parent != null)
                        stack.Push(top.Parent);
                }
            }

            string fullpath = "";

            path.Pop(); // ignore the root
            while (0 < path.Count)
            {
                string name = path.Pop();

                if (fullpath.Length != 0)
                    fullpath += "|";

                fullpath += name;
            }

            Path = fullpath;
#endif
        }
    }
}
