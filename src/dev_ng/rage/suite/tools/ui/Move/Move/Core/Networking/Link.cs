﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using System.Windows.Threading;

using Rage.Move.Core;
using Rage.Move.Utils;
using Move.Utils;

namespace New.Move.Core
{
    using LinkEventHandler = EventHandler<SimpleEventArgs<LinkNetworkMessageBase>>;

    public static class MessageFactory
    {
        public static NetworkMessageBase CreateFromBuffer(ByteBuffer bb, Link link)
        {
            NetworkMessageId msgType = (NetworkMessageId)bb.PeekReadUInt32(NetworkMessageBase.BufferOffset_MessageType);
            NetworkMessageBase msg = null;

            switch (msgType)
            {
                case NetworkMessageId.MSG_SYSINFO:
                    msg = new SystemInfoNetworkMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_NETWORK_INSTANCES:
                    msg = new NetworkInstancesMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_START_DEBUGGING:
                    msg = new StartDebuggingNetworkMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_STOP_DEBUGGING:
                    msg = new StopDebuggingNetworkMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_START_TRANSITION:
                    msg = new StartTransitionMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_STOP_TRANSITION:
                    msg = new StopTransitionMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_START_STATE_SET:
                    msg = new StartStateSetMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_STOP_STATE_SET:
                    msg = new StopStateSetMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_UPDATE_ROOT:
                    msg = new UpdateRootMessage();
                    msg.Decode(bb);
                    break;
                case NetworkMessageId.MSG_SIGNAL_SET:
                    msg = new SignalSetMessage();
                    ((SignalSetMessage)msg).DecodeSignalSet(bb, link);
                    break;
                default:
                    msg = null;
                    break;
            }

            return msg;
        }
    }

    public class Link
    {
        public LinkEventHandler StartDebugging;
        public LinkEventHandler StopDebugging;

        public LinkEventHandler StartTransition;
        public LinkEventHandler StopTransition;
        public LinkEventHandler StartStateSet;
        public LinkEventHandler StopStateSet;
        public LinkEventHandler UpdateRoot;
        public LinkEventHandler SignalSet;

        public EventHandler<SimpleEventArgs<NetworkMessageBase>> AnyMessage;
        
        /// <summary>
        /// Helper strings, used for message decoding. Don't use these... Only for Messages.cs. 
        /// I wish C# had friends. These would be private and SignalSetMessage would be a friend
        /// </summary>
        internal string LastAnimName, LastClipName, LastExprName, LastFilter, LastPMName;
        
        public Link(Database db)
        {
            Database = db;
            Alive = false;
        }

        protected bool Alive { get; set; }
        public Database Database {get; protected set;}
 
        public void BufferCompleteEventHandler(ByteBuffer bb)
        {
            NetworkMessageId msgType = (NetworkMessageId)bb.PeekReadUInt32(NetworkMessageBase.BufferOffset_MessageType);

            LinkNetworkMessageBase baseMsg = (LinkNetworkMessageBase)MessageFactory.CreateFromBuffer(bb, this);

            AnyMessage.Raise(this, baseMsg);

            if (msgType == NetworkMessageId.MSG_START_DEBUGGING)
            {
                StartDebuggingNetworkMessage startMsg = (StartDebuggingNetworkMessage)baseMsg;
                Alive = startMsg.Confirmation;
                StartDebugging.Raise(this, startMsg);
            }
            else if (msgType == NetworkMessageId.MSG_STOP_DEBUGGING)
            {
                StopDebuggingNetworkMessage stopMsg = (StopDebuggingNetworkMessage)baseMsg;
                Alive = !stopMsg.Confirmation;
                StopDebugging.Raise(this, stopMsg);
            }
            else
            {
                if (!Alive) return;
                switch(msgType)
                {
                    case NetworkMessageId.MSG_START_TRANSITION: StartTransition.Raise(this, baseMsg); break;
                    case NetworkMessageId.MSG_STOP_TRANSITION: StopTransition.Raise(this, baseMsg); break;
                    case NetworkMessageId.MSG_START_STATE_SET: StartStateSet.Raise(this, baseMsg); break;
                    case NetworkMessageId.MSG_STOP_STATE_SET: StopStateSet.Raise(this, baseMsg); break;
                    case NetworkMessageId.MSG_UPDATE_ROOT: UpdateRoot.Raise(this, baseMsg); break;
                    case NetworkMessageId.MSG_SIGNAL_SET: SignalSet.Raise(this, baseMsg); break;
                    default:
                        throw new InvalidOperationException(String.Format("Unhandled link message type {0}. Msg = {1}", msgType, baseMsg));
                }
            }
        }

    }

}