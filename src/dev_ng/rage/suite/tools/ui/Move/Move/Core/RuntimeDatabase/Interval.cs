﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Move.Core
{
    public class Interval
    {
        public Interval()
        {
        }

        public Interval(TimeSpan knownStart, TimeSpan knownEnd)
        {
            _StartTime = knownStart;
            _EndTime = knownEnd;
            Status = StatusType.CLOSED;
        }

        public void Reset()
        {
            _StartTime = TimeSpan.MinValue;
            _EndTime = TimeSpan.MaxValue;
            Status = StatusType.CLOSED;
        }

        public void CopyTimesFrom(Interval i)
        {
            Status = i.Status;
            _StartTime = i._StartTime;
            _EndTime = i._EndTime;
        }

        private TimeSpan _StartTime = TimeSpan.MinValue;
        public TimeSpan StartTime
        {
            get { return _StartTime; }
            set
            {
                _StartTime = value;
                if (Status == StatusType.UNKNOWN)
                {
                    Status = StatusType.NO_END;
                }
                else if (Status == StatusType.NO_START)
                {
                    Status = StatusType.CLOSED;
                }
            }
        }

        public bool HasStart
        {
            get {
                return Status == StatusType.CLOSED || Status == StatusType.NO_END;
            }
        }

        public bool HasEnd
        {
            get {
                return Status == StatusType.CLOSED || Status == StatusType.NO_START;
            }
        }

        private TimeSpan _EndTime = TimeSpan.MaxValue;
        public TimeSpan EndTime
        {
            get { return _EndTime; }
            set
            {
                _EndTime = value;
                if (Status == StatusType.UNKNOWN)
                {
                    Status = StatusType.NO_START;
                }
                else if (Status == StatusType.NO_END)
                {
                    Status = StatusType.CLOSED;
                }
            }
        }

        public double DurationInSeconds()
        {
            if (Status == StatusType.CLOSED)
            {
                return (EndTime - StartTime).TotalSeconds;
            }
            else
            {
                return double.NaN;
            }
        }

        public enum StatusType
        {
            UNKNOWN,    // Neither time is valid
            NO_START,   // The start time is unknown
            NO_END,     // The end time is unknown
            CLOSED,     // Both the Start and End time are valid
        }
        public StatusType Status;

        public bool Contains(TimeSpan d)
        {
            switch (Status)
            {
                case StatusType.CLOSED:
                    return StartTime <= d && d <= EndTime;
                case StatusType.NO_START:
                    return d <= EndTime;
                case StatusType.NO_END:
                    return StartTime <= d;
                case StatusType.UNKNOWN:
                    return false;
            }
            return false;
        }

       public bool Contains(Interval i)
       {
           if (Status == StatusType.UNKNOWN || i.Status == StatusType.UNKNOWN)
           {
               throw new InvalidOperationException("Can't compare using an UNKNOWN interval");
           }

           switch(i.Status)
           {
               case StatusType.NO_START:
                   return (Status == StatusType.NO_END) ? false : EndTime >= i.EndTime;
               case StatusType.NO_END:
                   return (Status == StatusType.NO_START) ? false : StartTime <= i.StartTime;
               case StatusType.CLOSED:
                   return (Status == StatusType.NO_START || StartTime <= i.StartTime) &&
                       (Status == StatusType.NO_END || EndTime >= i.EndTime);
           }
           return false;
       }

        /// <summary>
        /// Returns true if there is some time covered by both intervals
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public bool Overlaps(Interval i)
        {
            if (Status == StatusType.UNKNOWN || i.Status == StatusType.UNKNOWN)
            {
                throw new InvalidOperationException("Can't compare using an UNKNOWN interval");
            }

            switch(i.Status)
            {
                case StatusType.NO_START:
                    return (Status == StatusType.NO_START) ? true : StartTime <= i.EndTime;
                case StatusType.NO_END:
                    return (Status == StatusType.NO_END) ? true : EndTime >= i.StartTime;
                case StatusType.CLOSED:
                    return (Status == StatusType.NO_START || StartTime <= i.EndTime) &&
                        (Status == StatusType.NO_END || EndTime >= i.StartTime);
            }
            return false;
        }

        public string IntervalString()
        {
            switch (Status)
            {
                case StatusType.UNKNOWN: return "∞ -> ∞";
                case StatusType.NO_START: return String.Format("∞ -> {0:0.###}", EndTime.TotalSeconds);
                case StatusType.NO_END: return String.Format("{0:0.###} -> ∞", StartTime.TotalSeconds);
                case StatusType.CLOSED: return String.Format("{0:0.###} -> {1:0.###}", StartTime.TotalSeconds, EndTime.TotalSeconds);
            }
            return String.Empty;
        }

        public override string ToString()
        {
            return IntervalString();
        }
    }

}
