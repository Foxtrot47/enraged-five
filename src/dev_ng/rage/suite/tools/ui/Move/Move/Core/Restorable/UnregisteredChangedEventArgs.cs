﻿using System;

namespace Move.Core.Restorable
{
    public sealed class UnregisteredChangedEventArgs : EventArgs
    {
        public UnregisteredChangedEventArgs(string propertyName, object oldValue)
        {
            PropertyName = propertyName;
            OldValue = oldValue;
        }

        public string PropertyName { get; private set; }
        public object OldValue { get; private set; }
    }
}