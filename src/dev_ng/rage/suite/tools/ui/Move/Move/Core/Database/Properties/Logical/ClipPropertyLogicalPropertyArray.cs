﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class ClipPropertyLogicalPropertyArray : LogicalPropertyArray<ClipPropertyLogicalProperty>, ISerializable
    {
        static class Const
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Count = "Count";
        }

        public ClipPropertyLogicalPropertyArray(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public ClipPropertyLogicalPropertyArray()
            : base()
        {
        }

        public ClipPropertyLogicalPropertyArray(ILogic parent)
        {
            Parent = parent;
        }

        public ClipPropertyLogicalPropertyArray(ILogic parent, ClipPropertyLogicalPropertyArray other)
            : base()
        {
            Parent = parent;
            Name = other.Name;
            Tag = null;

            foreach (ClipPropertyLogicalProperty item in other.Children)
            {
                this.Items.Add(new ClipPropertyLogicalProperty(item));
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Name, Name);
            info.AddValue(Const.Id, Id);
            info.AddValue(Const.Parent, Parent);

            info.AddValue(Const.Count, _Items.Count);
            for (int index = 0; index < _Items.Count; ++index)
            {
                info.AddValue(index.ToString(), _Items[index]);
            }
           
        }

        public override object Clone()
        {
            return new ClipPropertyLogicalPropertyArray(this.Parent, this);
        }
    }
}
