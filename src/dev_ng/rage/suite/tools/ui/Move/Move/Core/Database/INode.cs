﻿using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Windows;
    using System.Xml;

    /// <summary>
    /// When implemented represents a selectable, engageable node that is inside a database.
    /// </summary>
    public interface INode : ISelectable, ISerializable, IEngageable
    {
        #region Properties
        /// <summary>
        /// Gets or sets the name for this node.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the additional text attached to this node.
        /// </summary>
        string AdditionalText { get; set; }

        /// <summary>
        /// Gets the name of the type that this node is representing.
        /// </summary>
        string Type { get; }

        /// <summary>
        /// Gets or sets the x,y coordinates that this node is located at within the
        /// parent diagram. (KILL ME).
        /// </summary>
        Point Position { get; set; }

        /// <summary>
        /// Gets or sets a reference to the root database this node is contained
        /// within. (KILL ME).
        /// </summary>
        Database Database { get; set; }
        
        /// <summary>
        /// Gets or sets the unique identifier for this node.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets or sets the identifier to the immediate parent node for this node.
        /// </summary>
        Guid Parent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this node is currently enabled.
        /// </summary>
        bool Enabled { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        /// <param name="hashedNames">
        /// A collection of hashed names to use during the export process.
        /// </param>
        void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames);

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        void Serialise(XmlWriter writer);

        /// <summary>
        /// Gets called once the owning database is fully loaded and initialised.
        /// </summary>
        void OnLoadComplete();

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        void ResetIds(Dictionary<Guid, Guid> map);

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        List<Guid> GetAllGuids();
        #endregion
    } // New.Move.Core.INode {Class}
} // New.Move.Core {Namespace}
