﻿using System.Collections;
using System.Collections.Specialized;

namespace Move.Core.Restorable
{
    internal sealed class ReplaceCollectionItemsAction : IRestorableAction
    {
        public void Undo()
        {
            using (new WithoutCollectionChanged(List, CollectionChanged))
            {
                int index = StartingIndex;
                foreach (object item in OldItems)
                {
                    List[index] = item;
                    ++index;
                }
            }
        }

        public void Redo()
        {
            using (new WithoutCollectionChanged(List, CollectionChanged))
            {
                int index = StartingIndex;
                foreach (object item in NewItems)
                {
                    List[index] = item;
                    ++index;
                }
            }
        }

        internal NotifyCollectionChangedEventHandler CollectionChanged { get; set; }
        public IList NewItems { get; set; }
        public IList OldItems { get; set; }
        public int StartingIndex { get; set; }

        public IList List { get; set; }
    }
}