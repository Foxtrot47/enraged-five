﻿namespace New.Move.Core
{
    public interface ITransitionProperty : IProperty
    {
        ITransition Parent { get; }
    }
}