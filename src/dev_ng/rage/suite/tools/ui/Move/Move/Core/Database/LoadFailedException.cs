﻿using System;
using System.Runtime.Serialization;

namespace Rockstar.MoVE.Framework
{
    public class LoadFailedException : ApplicationException
    {
        public LoadFailedException() 
        {
        }

        public LoadFailedException(string message)
            : base(message)
        {
        }

        public LoadFailedException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected LoadFailedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}