﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class Real : SignalBase, ISerializable
    {
        public Real(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public Real(RealSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Real"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Real(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }


        protected override string XmlNodeName { get { return "realsignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new Real(new RealSignal());
            (clone as Real).AdditionalText = this.AdditionalText;
            (clone as Real).Database = this.Database;
            (clone as Real).Enabled = this.Enabled;
            (clone as Real).Name = this.Name;
            (clone as Real).Position = this.Position;
            (clone as Real).Signal = this.Signal;

            return (ISelectable)clone;
        }
    }
}