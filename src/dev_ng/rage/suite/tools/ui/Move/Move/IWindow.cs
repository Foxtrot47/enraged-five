﻿using AvalonDock;

namespace Move
{
    public interface IWindow
    {
        //void Create(Rage.Move.MainWindow window);

        DockableContent Content { get; }
        string Name { get; }
    }
}