﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class RealReference : ReferenceSignal, ISerializable
    {
        public RealReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public RealReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override Parameter GetParameter()
        {
            return Parameter.Real;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}