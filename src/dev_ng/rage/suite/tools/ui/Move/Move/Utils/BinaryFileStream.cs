﻿using System.Runtime.InteropServices;
using System.IO;

namespace Move.Utils
{
    public unsafe class BinaryFileStream : FileStream
    {
        public BinaryFileStream(string path, FileMode mode)
            : base(path, mode)
        { }

        public BinaryFileStream(string path, FileMode mode, FileAccess access)
            : base(path, mode, access)
        { }

        public BinaryFileStream(string path)
            : base(path, FileMode.Open)
        { }

        public int Read<T>(void* buffer) where T : struct
        {
            return Read(buffer, Marshal.SizeOf(typeof(T)));
        }

        public void Write<T>(void* buffer) where T : struct
        {
            Write(buffer, Marshal.SizeOf(typeof(T)));
        }

        public void Write(byte[] buffer, int count)
        {
            for (int i = 0; i < count; ++i)
            {
                byte b = buffer[i];
                Write<byte>(&b);
            }
        }

        int Read(void* buffer, int count)
        {
            int n = 0;

            if (!ReadFile(this.SafeFileHandle, buffer, count, out n, 0))
                throw new IOException(string.Format("Error {0} reading from file!", Marshal.GetLastWin32Error()));

            return n;
        }

        int Write(void* buffer, int count)
        {
            int n = 0;

            if (!WriteFile(this.SafeFileHandle, buffer, count, out n, 0))
                throw new IOException(string.Format("Error {0} writing to file!", Marshal.GetLastWin32Error()));

            return n;
        }

        [DllImport("kernel32", SetLastError = true)]
        private static extern unsafe bool ReadFile(
            Microsoft.Win32.SafeHandles.SafeFileHandle handle,
            void* bytes,
            int numberOfBytesToRead,
            out int numberOfBytesRead,
            int overlapped
            );

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern unsafe bool WriteFile(
            Microsoft.Win32.SafeHandles.SafeFileHandle handle,
            void* bytes,
            int numBytesToWrite,
            out int numBytesWritten,
            int overlapped
            );
    }
}