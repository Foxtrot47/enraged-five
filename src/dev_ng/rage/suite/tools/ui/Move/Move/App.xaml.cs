﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Xml.Serialization;
using System.IO;
using System.Globalization;

using Rage.Move.Core;
using Rage.Move.Core.Dag;

namespace Rage.Move
{
    public partial class App : Application
    {
        Runtime _Runtime = new Runtime();

        public static App Instance { get { return (App)Application.Current; } }
        public Runtime Runtime { get { return _Runtime; } }

        /*
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Hierarchy StateChart
        {
            get { return stateChart; }
        }
        static Hierarchy stateChart = new Hierarchy();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static NodeCollection StateHierarchy
        {
            get { return stateHierarchy; }
        }
        static NodeCollection stateHierarchy = stateChart.NodeCollection;
        
         */

        private const int NumberOfRecentFiles = 5;

        private readonly static string RecentFilesFilePath =
           Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
           Path.Combine(Runtime.ApplicationFolderName, "RecentFiles.xml"));

        private static StringCollection recentFiles = new StringCollection();

        private readonly static string RuntimeTargetsFilePath =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            Path.Combine(Runtime.ApplicationFolderName, "RuntimeTargets.xml"));

        private static ObservableCollection<RuntimeTarget> targets = new ObservableCollection<RuntimeTarget>();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        protected override void OnStartup(StartupEventArgs e)
        {
            PluginServices plugins = new PluginServices();
            plugins.LoadPlugins("Plugins");

            foreach (Plugin plugin in plugins.AvailablePlugins)
            {
                Console.WriteLine(plugin.Instance.Name);
            }

            LoadRecentFiles();
            LoadRuntimeTargets();
            /*
            Properties.Settings appSettings = Properties.Settings.Default;

            if (!string.IsNullOrEmpty(appSettings.Skin))
            {
                try
                {
                    ResourceDictionary rd = new ResourceDictionary();
                    rd.MergedDictionaries.Add(Application.LoadComponent(new Uri(appSettings.Skin, UriKind.Relative)) as ResourceDictionary);
                    Application.Current.Resources = rd;
                }
                catch
                {
                }
            }
            */

            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            if (Runtime.Database.IsDirty && !String.IsNullOrEmpty(Runtime.Database.FullyQualifiedFilename))
                Runtime.Database.Save();

            SaveRecentFiles();
            SaveRuntimeTargets();

 	        base.OnExit(e);
        }

        public static StringCollection RecentFiles
        {
            get { return recentFiles; }
        }

        public static ObservableCollection<RuntimeTarget> RuntimeTargets
        {
            get { return targets; }
        }

        public static TimeSpan GetAnimationDuration(double milliseconds)
        {
            return TimeSpan.FromMilliseconds(
                Keyboard.IsKeyDown(Key.F12) ?
                milliseconds * 5 : milliseconds);
        }

        public static void LoadRecentFiles()
        {
            if ( File.Exists(RecentFilesFilePath) )
            {
                XmlSerializer serializer = new XmlSerializer(typeof(StringCollection));
                using (TextReader reader = new StreamReader(RecentFilesFilePath))
                {
                    recentFiles = (StringCollection)serializer.Deserialize(reader);
                }

                // Remove files from the Recent Files list that no longer exist.
                for ( int i = 0; i < recentFiles.Count; ++i )
                {
                    if ( !File.Exists(recentFiles[i]))
                        recentFiles.RemoveAt(i);
                }

                while (recentFiles.Count > NumberOfRecentFiles)
                    recentFiles.RemoveAt(NumberOfRecentFiles);
            }
        }

        public static void SaveRecentFiles()
        {
            if (!Directory.Exists(Database.ApplicationFolderPath))
                Directory.CreateDirectory(Database.ApplicationFolderPath);

            XmlSerializer serializer = new XmlSerializer(typeof(StringCollection));
            using (TextWriter writer = new StreamWriter(RecentFilesFilePath))
            {
                serializer.Serialize(writer, recentFiles);
            }
        }

        public static void LoadRuntimeTargets()
        {
            if (File.Exists(RuntimeTargetsFilePath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<RuntimeTarget>));
                using (TextReader reader = new StreamReader(RuntimeTargetsFilePath))
                {
                    targets = (ObservableCollection<RuntimeTarget>)serializer.Deserialize(reader);
                }
            }
        }

        public static void SaveRuntimeTargets()
        {
            if (!Directory.Exists(Database.ApplicationFolderPath))
                Directory.CreateDirectory(Database.ApplicationFolderPath);

            XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<RuntimeTarget>));    
            using (TextWriter writer = new StreamWriter(RuntimeTargetsFilePath))
            {
                serializer.Serialize(writer, targets);
            }
        }
    }
}
