﻿using System;
using System.Runtime.Serialization;

using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class SignalAnchor : SourceAnchor, ISerializable
    {
        public SignalAnchor(SignalBase parent)
        {
            Parent = parent;
        }

        public SignalAnchor(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(SerializationTag.Name);
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
            Connections = (NoResetObservableCollection<Connection>)info.GetValue(SerializationTag.Connections, typeof(NoResetObservableCollection<Connection>));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Connections, Connections);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Connections = "Connections";
        }
    }
}