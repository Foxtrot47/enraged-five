﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel
{
    [Serializable]
    public class NamedFloatVariable : ILogicPropertyGroup, ISerializable
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        private const string NameSerializationTag = "Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        private const string IdSerializationTag = "Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="NamedVariable"/> property.
        /// </summary>
        private const string VariableSerializationTag = "NamedVariable";

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field used for the <see cref="Parent"/> property.
        /// </summary>
        private ILogic parent;

        /// <summary>
        /// A array of properties that are associated with this variable.
        /// </summary>
        private IProperty[] properties = new IProperty[1];
        #endregion

        #region Constructors
        /// <summary>
        ///  Initialises a new instance of the
        ///  <see cref="Rockstar.MoVE.Framework.DataModel.NamedFloatVariable"/> class.
        /// </summary>
        public NamedFloatVariable()
        {
            this.NamedVariable = new NamedVariableLogicalProperty();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Rockstar.MoVE.Framework.DataModel.NamedFloatVariable"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public NamedFloatVariable(SerializationInfo info, StreamingContext context)
        {
            ILogic infoParent = null;
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case NameSerializationTag:
                    {
                        this.Name = (string)entry.Value;
                    }
                    break;
                    case IdSerializationTag:
                    {
                        this.Id = (Guid)entry.Value;
                    }
                    break;
                    case VariableSerializationTag:
                    {
                        this.NamedVariable = (NamedVariableLogicalProperty)entry.Value;
                    }
                    break;
                    default:
                    {
                        Trace.TraceWarning("Unrecognised value has been serialised for this NamedFloatVariable instance.");
                    }
                    break;
                }
            }

            this.Parent = infoParent;
        }

        /// <summary>
        ///  Initialises a new instance of the
        ///  <see cref="Rockstar.MoVE.Framework.DataModel.NamedFloatVariable"/> class
        ///  as a deep copy of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        public NamedFloatVariable(NamedFloatVariable other)
        {
            this.NamedVariable = new NamedVariableLogicalProperty(this.Parent, other.NamedVariable);

            this.Name = other.Name;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NamedFloatVariable"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public NamedFloatVariable(XmlReader reader, Database database, ILogic parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Parent = parent;
            this.Deserialise(reader);
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs whenever a value on a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// The unique global identifier for this transform.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or sets the logic parent for this weight|filter transform.
        /// </summary>
        public ILogic Parent
        {
            get
            {
                return this.parent;
            }

            set
            {
                if (this.parent == value)
                {
                    return;
                }

                this.parent = value;
                this.NamedVariable.Parent = value;
            }
        }

        /// <summary>
        /// The <see cref="NamedVariableLogicalProperty"/> instance that defines the value for
        /// this variable.
        /// </summary>
        public NamedVariableLogicalProperty NamedVariable
        {
            get
            {
                return this.properties[0] as NamedVariableLogicalProperty;
            }
            set
            {
                if (this.properties[0] == value)
                {
                    return;
                }

                this.properties[0] = value;
                if (value != null)
                {
                    value.Container = this;
                }
            }
        }

        /// <summary>
        /// Gets the motion tree that this variable belongs to.
        /// </summary>
        private Motiontree ParentMotionTree
        {
            get
            {
                if (this.parent == null || this.parent.Database == null)
                {
                    return null;
                }

                Guid motionTreeIdentifer = this.parent.Parent;
                return parent.Database.Find(parent.Parent) as Motiontree;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new NamedFloatVariable(this);
        }

        /// <summary>
        /// Removes all of the connections that are currently attached to this variable
        /// </summary>
        /// <param name="parent">
        /// The parent whose motion tree the connections need to be removed from.
        /// </param>
        public void RemoveConnections(INode nodeParent)
        {
            if (nodeParent == null || nodeParent.Database == null)
            {
                return;
            }

            Motiontree motionTree = nodeParent.Database.Find(nodeParent.Parent) as Motiontree;
            if (motionTree == null)
            {
                return;
            }

            motionTree.RemoveConnectionsFromProperty(this.NamedVariable);
        }

        /// <summary>
        /// Gets the index of the first occurence to the specified property in this variable.
        /// </summary>
        /// <param name="property">
        /// The property to get the index for.
        /// </param>
        /// <returns>
        /// The index to the first occurence to the specified property if found; otherwise, -1.
        /// </returns>
        public int IndexOf(IProperty property)
        {
            for (int i = 0; i < this.properties.Length; ++i)
            {
                if (this.properties[i] == property)
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Gets the property for this variable at the specified non-zero index.
        /// </summary>
        /// <param name="index">
        /// The non-zero index of the variable property to get.
        /// </param>
        /// <returns>
        /// The property for this variable at the given index.
        /// </returns>
        public object ItemAt(int index)
        {
            return properties[index];
        }

        /// <summary>
        /// Gets the number of properties associated with this variable.
        /// </summary>
        /// <returns>
        /// The number of properties associated with this variable.
        /// </returns>
        public int Count()
        {
            return properties.Length;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetParentOnChildren()
        {
            this.NamedVariable.Parent = this.parent;
        }

        /// <summary>
        /// Determines whether the specified property is contained within this variables
        /// properties.
        /// </summary>
        /// <param name="property">
        /// The property to find.
        /// </param>
        /// <returns>
        /// True if one of the properties for this variable is equal to the specified
        /// property; otherwise, false.
        /// </returns>
        public bool Contains(IProperty property)
        {
            foreach (IProperty p in properties)
            {
                if (p == property)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerializationTag, Name);
            info.AddValue(IdSerializationTag, Id);
            info.AddValue(VariableSerializationTag, NamedVariable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="handler"></param>
        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
            NamedVariable.SetPropertyChangedHandler(handler);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.NamedVariable != null)
            {
                writer.WriteStartElement("NamedVariable");
                this.NamedVariable.Serialise(writer);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "NamedVariable") == 0)
                {
                    this.NamedVariable = new NamedVariableLogicalProperty(reader, this.Parent.Database, this.Parent);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.NamedVariable.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.NamedVariable.Id = New.Move.Core.Database.GetId(map, this.NamedVariable.Id);
        }
        #endregion
    } // Rockstar.MoVE.Framework.DataModel.NamedFloatVariable
} // Rockstar.MoVE.Framework.DataModel