﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class BooleanReference : ReferenceSignal, ISerializable
    {
        public BooleanReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public BooleanReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override Parameter GetParameter()
        {
            return Parameter.Boolean;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}