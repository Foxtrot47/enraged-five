﻿using System;
using System.Windows.Threading;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;

using MoVE;
using RSG.Base.Configuration;
using System.Windows;

namespace Rage.Move
{
    public static class AppMain
    {
        [System.STAThreadAttribute()]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public static void Main(string[] args)
        {
            //  Get any args passed in
            string fileToLoadOnStart = null;
            if (args.Length > 0)
            {
                fileToLoadOnStart = args[0];
            }

            try
            {
                IConfig config = ConfigFactory.CreateConfig();
            }
            catch (ConfigurationVersionException ex)
            {
                MessageBox.Show("Unable to open MoVE as the currently installed tool configuration version is out of date.\nRe-install tools and try again.", "Configuration Version Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }

            OpenSplashScreen();
            StartApplication(fileToLoadOnStart);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void StartApplication(string fileToLoadOnStart)
        {
            Rage.Move.App app = new Rage.Move.App();
            app.InitializeComponent();
            //  Store the file to load when we start up
            app.SetFileToLoadOnStart(fileToLoadOnStart);
            app.Run();
        }

        private static void OpenSplashScreen()
        {
            _SplashScreen = new MoVE.SplashScreen();
            _SplashScreen.Open();
        }

        internal static void CloseSplashScreen()
        {
            if (_SplashScreen != null)
            {
                _SplashScreen.Close();
                _SplashScreen = null;
            }
        }

        private static MoVE.SplashScreen _SplashScreen;
    }
}