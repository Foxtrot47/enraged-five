﻿using System;

namespace Move.Core.Restorable
{
    public class RestorablePropertyDescriptor
    {
        public static RestorablePropertyDescriptor FromProperty(RestorableProperty restorableProperty, Type targetType)
        {
            RestorablePropertyDescriptor descriptor = new RestorablePropertyDescriptor();
            descriptor.Name = restorableProperty.Name;
            return descriptor;
        }

        public void AddValueChanging(object component, EventHandler handler)
        {
            if (component is RestorableObject)
            {
                RestorableObject obj = component as RestorableObject;
                RestorableItem item = obj.Properties[Name];
                item.ValueChanging += new EventHandler(handler);
            }
        }

        public void RemoveValueChanging(object component, EventHandler handler)
        {
            if (component is RestorableObject)
            {
                RestorableObject obj = component as RestorableObject;
                RestorableItem item = obj.Properties[Name];
                item.ValueChanging -= new EventHandler(handler);
            }
        }

        public void AddValueChanged(object component, EventHandler handler)
        {
            if (component is RestorableObject)
            {
                RestorableObject obj = component as RestorableObject;
                RestorableItem item = obj.Properties[Name];
                item.ValueChanged += new EventHandler(handler);
            }
        }

        public void RemoveValueChanged(object component, EventHandler handler)
        {
            if (component is RestorableObject)
            {
                RestorableObject obj = component as RestorableObject;
                RestorableItem item = obj.Properties[Name];
                item.ValueChanged -= new EventHandler(handler);
            }
        }

        string Name;
    }
}