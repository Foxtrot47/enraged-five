﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class NodeReference : ReferenceSignal, ISerializable
    {
        public NodeReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public NodeReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}