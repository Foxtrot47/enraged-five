﻿using Move.Core;

namespace New.Move.Core
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml;

    /// <summary>
    /// Represents a control parameter that uses a clip for its value.
    /// </summary>
    [Serializable]
    public class ExpressionSignal : Signal, IReferredSignal
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ExpressionDictionaries"/> property.
        /// </summary>
        private static IEnumerable<string> expressionDictionaries;

        /// <summary>
        /// The private field used for the <see cref="Expressions"/> property.
        /// </summary>
        private IEnumerable<string> expressions;

        /// <summary>
        /// The private field used for the <see cref="SelectedSource"/> property.
        /// </summary>
        private string selectedSource;

        /// <summary>
        /// The private field used for the <see cref="SelectedExpressionDictionary"/> property.
        /// </summary>
        private string selectedExpressionDictionary;

        /// <summary>
        /// The private field used for the <see cref="SelectedExpression"/> property.
        /// </summary>
        private string selectedExpression;

        /// <summary>
        /// The private field used for the <see cref="FilePath"/> property.
        /// </summary>
        private string filePath;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises static members of the
        /// <see cref="New.Move.Core.ExpressionSignal"/> class.
        /// </summary>
        static ExpressionSignal()
        {
            ExpressionArchiveReader archiveReader = new ExpressionArchiveReader();
            expressionDictionaries = archiveReader.ReadExpressionDictionaryNames();
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.ExpressionSignal"/> class.
        /// </summary>
        public ExpressionSignal()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ExpressionSignal"/>
        /// class using the specified serialisation info and streaming context as
        /// data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        protected ExpressionSignal(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ExpressionSignal"/>
        /// class using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> parameter is null.
        /// </exception>
        public ExpressionSignal(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Enabled = true;
            this.Name = reader.GetAttribute(NameSerializationTag);
            this.Id = Guid.ParseExact(reader.GetAttribute(IdSerializationTag), "D");
            reader.Skip();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the <see cref="New.Move.Core.IDesc"/> object that describes the creation of
        /// this object.
        /// </summary>
        public override IDesc Desc
        {
            get { return new ExpressionDesc(this); }
        }

        /// <summary>
        /// Gets a iterator around all of the expression dictionaries that are available to
        /// this signal as a source for the selected expression.
        /// </summary>
        public IEnumerable<string> ExpressionDictionaries
        {
            get { return expressionDictionaries; }
        }

        /// <summary>
        /// Gets or sets a iterator around the available expressions from the selected
        /// expression directory.
        /// </summary>
        public IEnumerable<string> Expressions
        {
            get
            {
                return this.expressions;
            }

            set
            {
                if (this.expressions != value)
                {
                    this.expressions = value;
                    this.NotifyPropertyChanged("Expressions");
                }
            }
        }

        /// <summary>
        /// Gets or sets the source location for the selected expression.
        /// </summary>
        public string SelectedSource
        {
            get
            {
                return this.selectedSource;
            }

            set
            {
                this.selectedSource = value;
                this.NotifyPropertyChanged("SelectedSource");
                if (value == "Expression Dictionary")
                {
                    this.SelectedExpressionDictionary =
                        this.ExpressionDictionaries.FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Gets or sets the dictionary of expressions the selected expression
        /// should come from.
        /// </summary>
        public string SelectedExpressionDictionary
        {
            get
            {
                return this.selectedExpressionDictionary;
            }

            set
            {
                this.selectedExpressionDictionary = value;
                this.NotifyPropertyChanged("SelectedExpressionDictionary");
                if (this.selectedExpressionDictionary == null)
                {
                    return;
                }

                ExpressionArchiveReader archiveReader = new ExpressionArchiveReader();
                this.Expressions = archiveReader.ReadExpressionNamesFromDictionary(
                    this.selectedExpressionDictionary);
                this.NotifyPropertyChanged("Expressions");
                if (this.Expressions.Count() > 0)
                {
                    this.SelectedExpression = this.Expressions.First();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected expression.
        /// </summary>
        public string SelectedExpression
        {
            get
            {
                return this.selectedExpression;
            }

            set
            {
                this.selectedExpression = value;
                this.NotifyPropertyChanged("SelectedExpression");
            }
        }

        /// <summary>
        /// Gets or sets the full file path to the selected local expression.
        /// </summary>
        public string FilePath
        {
            get
            {
                return this.filePath;
            }

            set
            {
                this.filePath = value;
                this.NotifyPropertyChanged("FilePath");
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("expression");

            writer.WriteAttributeString(
                Signal.NameSerializationTag, this.Name);
            writer.WriteAttributeString(
                Signal.IdSerializationTag,
                this.Id.ToString("D", CultureInfo.InvariantCulture));

            writer.WriteEndElement();
        }

        /// <summary>
        /// Gets the parameter value for this signal that represents the type it is
        /// representing.
        /// </summary>
        /// <returns>
        /// The parameter value that this signal represents.
        /// </returns>
        public override Parameter GetParameter()
        {
            return Parameter.Expressions;
        }

        /// <summary>
        /// Creates a reference to this clip signal.
        /// </summary>
        /// <returns>
        /// A new reference to this clip signal.
        /// </returns>
        public ReferenceSignal CreateReference()
        {
            return new ExpressionReference(this.Id, this.Name);
        }
        #endregion
    } // New.Move.Core.ExpressionSignal {Class}
} // New.Move.Core {Namespace}
