﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml;

using Move.Core;
using Move.Core.Restorable;
using Move.Utils;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class TunnelEvent : RestorableObject, ILogic, ISerializable
    {
        public TunnelEvent()
        {
            Connections = new NoResetObservableCollection<Connection>();
            Id = Guid.NewGuid();
            EventAnchor = new TunnelEventAnchor(this);
        }

        public void OnLoadComplete()
        {

        }

        public string Name
        {
            get { return ""; }
            set { /* do nothing */ }
        }

        public string AdditionalText
        {
            get { return ""; }
            set { }
        }

        public string Type
        {
            get
            {
                return "";
            }
            set {}
        }

        private bool _Enabled = true;
        public bool Enabled
        {
            get
            {
                return _Enabled;
            }
            set
            {
                _Enabled = value;
            }
        }

        public Point Position { get; set; }

        public Database Database { get; set; }
        public Guid Id { get; private set; }
        public Guid Parent { get; set; }

        public IDesc Desc { get; internal set; }

        public object Tag { get; set; }

        public TunnelEventAnchor EventAnchor { get; private set; }
        public LogicEvent Event 
        { 
            get; 
            set; 
        }

        NoResetObservableCollection<Connection> _Connections;
        public NoResetObservableCollection<Connection> Connections
        {
            get { return _Connections; }
            set { _Connections = value; }
        }

        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            if (IsSelectedChanged != null)
            {
                IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
            }
        }

        public event EventHandler<IsEngagedChangedEventArgs> IsEngagedChanged;
        public void OnEngagedChanged(EngagedChangedAction action)
        {
            if (IsEngagedChanged != null)
            {
                IsEngagedChanged(this, new IsEngagedChangedEventArgs(action));
            }
        }

        public void Dispose()
        {
            if (Parent != Guid.Empty)
            {
                ITransitional parent = Database.Find(Parent) as ITransitional;
                parent.Remove(this);
            }

            Database.Nodes.Remove(this);
        }

        static class SerializationTag
        {
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Database = "Database";
            public static string Position = "Position";
            public static string EventAnchor = "EventAnchor";
            public static string Event = "Event";
        }

        public TunnelEvent(SerializationInfo info, StreamingContext context)
        {
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (Guid)info.GetValue(SerializationTag.Parent, typeof(Guid));
            Database = (Database)info.GetValue(SerializationTag.Database, typeof(Database));

            Position = (Point)info.GetValue(SerializationTag.Position, typeof(Point));

            EventAnchor = (TunnelEventAnchor)info.GetValue(
                SerializationTag.EventAnchor, typeof(TunnelEventAnchor));
            Event = (LogicEvent)info.GetValue(SerializationTag.Event, typeof(LogicEvent));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Database, Database);

            info.AddValue(SerializationTag.Position, Position);

            info.AddValue(SerializationTag.EventAnchor, EventAnchor);
            info.AddValue(SerializationTag.Event, Event);
        }

        public void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        object ICloneable.Clone()
        {
            TunnelEvent clone = new TunnelEvent();
            return this.Clone(clone);
        }

        public virtual ISelectable Clone(TunnelEvent clone)
        {
            System.Type ot = clone.GetType();
            PropertyInfo[] oproperties = ot.GetProperties();

            System.Type t = this.GetType();
            PropertyInfo[] properties = t.GetProperties();

            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo prop = properties[i];
                PropertyInfo oprop = oproperties[i];

                oprop.SetValue(clone, prop.GetValue(this, null), null);
            }

            return (ISelectable)clone;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Output"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public TunnelEvent(XmlReader reader, Database database, Guid parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Parent = parent;
            this.Database = database;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("TunnelEvent");
            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Event != null)
            {
                writer.WriteAttributeString("Event", this.Event.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("EventName", this.Event.Name);
            }

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (EventAnchor != null)
            {
                writer.WriteStartElement("EventAnchor");
                this.EventAnchor.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            string eventId = reader.GetAttribute("Event");
            if (!string.IsNullOrEmpty(eventId))
            {
                this.Event = this.Database.GetEvent(Guid.ParseExact(eventId, "D"));
                if (this.Event == null)
                {
                    string eventName = reader.GetAttribute("EventName");
                    this.Event = this.Database.GetEvent(eventName);
                    if (this.Event == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to event with id '{0}'. This needs fixing before a export is allowed.", eventId);
                    }
                }
            }

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "EventAnchor") == 0)
                {
                    this.EventAnchor = new TunnelEventAnchor(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            return ids;
        }
    }
}