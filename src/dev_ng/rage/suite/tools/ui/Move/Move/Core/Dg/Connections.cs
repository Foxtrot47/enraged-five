﻿using System;
using System.Xml.Serialization;

namespace Rage.Move.Core.Dg
{
    /*
    public interface IConnection
    {
        Guid Source { get; }
        Guid Sink { get; }
        Guid Id { get; }
    }

    public abstract class ConnectionBase : IConnection 
    {
        public ConnectionBase(Guid source, Guid sink)
        {
            Source = source;
            Sink = sink;
            Id = Guid.NewGuid();
        }

        public Guid Source { get; set; }
        public Guid Sink { get; set; }
        [XmlAttribute]
        public Guid Id { get; set; }

        [XmlIgnore]
        public Guid Guid { get { return Id; } }
    }

    [Serializable]
    public class MessageConnection : ConnectionBase
    {
        public MessageConnection(Guid source, Guid sink)
            : base(source, sink) {}

        public MessageConnection()
            : base(Guid.Empty, Guid.Empty) { }
    }

    [Serializable]
    public class SignalConnection : ConnectionBase
    {
        public SignalConnection(Guid source, Guid sink)
            : base(source, sink) { }

        public SignalConnection()
            : base(Guid.Empty, Guid.Empty) { }
    }

    [Serializable]
    public class OperatorConnection : ConnectionBase
    {
        public OperatorConnection(Guid source, Guid sink)
            : base(source, sink) { }

        public OperatorConnection()
            : base(Guid.Empty, Guid.Empty) { }
    }

    [Serializable]
    public class OperatorResultConnection : ConnectionBase
    {
        public OperatorResultConnection(Guid source, Guid sink)
            : base(source, sink) { }

        public OperatorResultConnection()
            : base(Guid.Empty, Guid.Empty) { }
    }

    [Serializable]
    public class OperatorSourceConnection : ConnectionBase
    {
        public OperatorSourceConnection(Guid source, Guid sink)
            : base(source, sink) { }

        public OperatorSourceConnection()
            : base(Guid.Empty, Guid.Empty) { }
    }

    [Serializable]
    public class TransformConnection : ConnectionBase
    {
        public TransformConnection(Guid source, Guid sink)
            : base(source, sink) { }

        public TransformConnection()
            : base(Guid.Empty, Guid.Empty) { }
    }
     */
}
