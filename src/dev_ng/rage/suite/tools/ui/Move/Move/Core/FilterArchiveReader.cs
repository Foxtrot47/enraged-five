﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Move.Core
{
    internal class FilterArchiveReader
    {
        internal FilterArchiveReader()
        {

        }

        // Cache any data read from the zip to speed things up a little
        private static IEnumerable<string> cachedFilterDictionaryNames_;
        private static Dictionary<string, IEnumerable<string>> cachedFilterNames_ = new Dictionary<string, IEnumerable<string>>();

        internal IEnumerable<string> ReadFilterDictionaryNames()
        {
            if (cachedFilterDictionaryNames_ != null)
                return cachedFilterDictionaryNames_;

            try
            {
                using (var archive = Ionic.Zip.ZipFile.Read(Move.Utils.ContentConfig.ExportedFiltersFilename))
                {
                    var clipDictionaryNamesQuery = from entry in archive.Entries
                                                   orderby entry.FileName
                                                   select entry.FileName.Replace(".ifd.zip", "");
                    return cachedFilterDictionaryNames_ = clipDictionaryNamesQuery.ToArray();
                }
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }

        internal IEnumerable<string> ReadFilterNamesFromDictionary(string dictionaryName)
        {
            if (cachedFilterNames_.ContainsKey(dictionaryName))
                return cachedFilterNames_[dictionaryName];

            string fullDictionaryName = dictionaryName + ".ifd.zip";
            try
            {
                using (var clipsArchive = Ionic.Zip.ZipFile.Read(Move.Utils.ContentConfig.ExportedFiltersFilename))
                {
                    var entry = clipsArchive.Entries.First(en => en.FileName == fullDictionaryName);
                    using (var extractedDataStream = new System.IO.MemoryStream((int)entry.UncompressedSize))
                    {
                        entry.Extract(extractedDataStream);
                        extractedDataStream.Seek(0, System.IO.SeekOrigin.Begin);
                        using (var dictionaryArchive = Ionic.Zip.ZipFile.Read(extractedDataStream))
                        {
                            var query = from dictionaryEntry in dictionaryArchive.Entries
                                        where dictionaryEntry.FileName.EndsWith(".iff")
                                        orderby dictionaryEntry.FileName
                                        select dictionaryEntry.FileName.Replace(".iff", "");
                            cachedFilterNames_.Add(dictionaryName, query.ToArray());
                            return cachedFilterNames_[dictionaryName];
                        }
                    }
                }
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }
    }
}
