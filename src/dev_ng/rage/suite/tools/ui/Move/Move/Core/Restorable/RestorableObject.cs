﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;

using RSG.Base.Logging;

namespace Move.Core.Restorable
{
    [Serializable]
    public abstract class RestorableObject
    {
        internal RestorableCollection Properties = new RestorableCollection();

        protected RestorableObject()
        {
            RestorableProperty.CreateItems(this);
        }

        EventHandler<RestorablePropertyChangedEventArgs> _RestorablePropertyChanged;
        internal EventHandler<RestorablePropertyChangedEventArgs> RestorablePropertyChanged
        {
            get
            {
                return _RestorablePropertyChanged;
            }
            set
            {
                _RestorablePropertyChanged = value;

                // TODO: this happens because of serialization. needs to be fixed
                if (Properties != null)
                {
                    foreach (RestorableItem item in Properties)
                    {
                        if (item.Value is RestorableObject)
                        {
                            RestorableObject obj = item.Value as RestorableObject;
                            obj.RestorablePropertyChanged = value;
                        }
                    }
                }
            }
        }

        void OnRestorablePropertyChanged(IRestorableAction action)
        {
            if (RestorablePropertyChanged != null)
                RestorablePropertyChanged(this, new RestorablePropertyChangedEventArgs(action));
        }

        protected void SetValue(RestorableProperty property, object value)
        {
            //Console.WriteLine("set property {0} to {1} on object {2}", property.Name, value, this.GetType().ToString());

            if (!this.Properties.Contains(property.Name))
            {
                LogFactory.ApplicationLog.Debug("Unable to find property with name {0}", property.Name);
                return;
            }

            RestorableItem item = Properties[property.Name];

            SetRestorablePropertyAction action = new SetRestorablePropertyAction();
            action.PropertyName = property.Name;
            action.OldValue = item.Value;
            action.NewValue = value;
            action.Target = this;

            item.Value = value;

            if (value is RestorableObject)
            {
                (item.Value as RestorableObject).RestorablePropertyChanged = RestorablePropertyChanged;
            }
            else
            if (value is INotifyCollectionChanged)
            {
                INotifyCollectionChanged i = value as INotifyCollectionChanged;
                i.CollectionChanged += new NotifyCollectionChangedEventHandler(CollectionChanged);
            }
            else
            if (value is INotifyUnregisteredChanged)
            {
                INotifyUnregisteredChanged i = value as INotifyUnregisteredChanged;
                i.UnregisteredChanged += new EventHandler<UnregisteredChangedEventArgs>(PropertyChanged);
            }

            OnRestorablePropertyChanged(action);
        }

        protected object GetValue(RestorableProperty property)
        {
            return Properties[property.Name].Value;
        }

        protected virtual void OnBeforeUndo() { }

        internal void PreUndo()
        {
            OnBeforeUndo();

            foreach (RestorableObject obj in Properties.OfType<RestorableObject>())
            {
                obj.PreUndo();
            }
        }
        
        protected virtual void OnAfterUndo() { }

        internal void PostUndo()
        {
            foreach (RestorableObject obj in Properties.OfType<RestorableObject>())
            {
                obj.PostUndo();
            }

            OnAfterUndo();
        }

        protected virtual void OnBeforeRedo() { }

        internal void PreRedo()
        {
            OnBeforeRedo();

            foreach (RestorableObject obj in Properties.OfType<RestorableObject>())
            {
                obj.PreRedo();
            }
        }

        protected virtual void OnAfterRedo() { }

        internal void PostRedo()
        {
            foreach (RestorableObject obj in Properties.OfType<RestorableObject>())
            {
                obj.PreRedo();
            }
        }

        void CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        //Console.WriteLine("add item in object {0}", this.GetType().ToString());
                        AddCollectionItemsAction action = new AddCollectionItemsAction();
                        action.CollectionChanged = new NotifyCollectionChangedEventHandler(CollectionChanged);
                        action.NewItems = e.NewItems;
                        action.NewStartingIndex = e.NewStartingIndex;
                        action.List = sender as IList;

                        foreach (RestorableObject obj in action.NewItems.OfType<RestorableObject>())
                        {
                            obj.RestorablePropertyChanged = RestorablePropertyChanged;
                        }

                        OnRestorablePropertyChanged(action);
                    }
                    break;
                case NotifyCollectionChangedAction.Move:
                    {
                        MoveCollectionItemsAction action = new MoveCollectionItemsAction();
                        action.CollectionChanged = new NotifyCollectionChangedEventHandler(CollectionChanged);
                        action.Items = e.OldItems;
                        action.NewStartingIndex = e.NewStartingIndex;
                        action.OldStartingIndex = e.OldStartingIndex;
                        action.List = sender as IList;

                        OnRestorablePropertyChanged(action);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    {
                        //Console.WriteLine("remove item in object {0}", this.GetType().ToString());
                        RemoveCollectionItemsAction action = new RemoveCollectionItemsAction();
                        action.CollectionChanged = new NotifyCollectionChangedEventHandler(CollectionChanged);
                        action.OldItems = e.OldItems;
                        action.OldStartingIndex = e.OldStartingIndex;
                        action.List = sender as IList;

                        foreach (RestorableObject obj in action.OldItems.OfType<RestorableObject>())
                        {
                            obj.RestorablePropertyChanged = null;
                        }

                        OnRestorablePropertyChanged(action);
                    }
                    break;
                case NotifyCollectionChangedAction.Replace:
                    {
                        ReplaceCollectionItemsAction action = new ReplaceCollectionItemsAction();
                        action.CollectionChanged = new NotifyCollectionChangedEventHandler(CollectionChanged);
                        action.NewItems = e.NewItems;
                        action.OldItems = e.OldItems;
                        action.StartingIndex = e.OldStartingIndex;
                        action.List = sender as IList;

                        OnRestorablePropertyChanged(action);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    // can't do anything here...
                    break;
            }
        }

        void PropertyChanged(object sender, UnregisteredChangedEventArgs e)
        {
            SetPropertyAction action = new SetPropertyAction();
            action.PropertyName = e.PropertyName;
            action.OldValue = e.OldValue;
            action.Target = sender;

            PropertyInfo pi = sender.GetType().GetProperty(e.PropertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            action.NewValue = pi.GetValue(sender, new object[0]);

            OnRestorablePropertyChanged(action);
        }
    }
}