﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

using Move.Core;
using Move.Utils;
using System.Xml;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class AnimationLogicalProperty : Property, ISerializable, IExternalFileLink
    {
        public static implicit operator AnimationLogicalProperty(string filename)
        {
            AnimationLogicalProperty property = new AnimationLogicalProperty();
            property._Input = PropertyInput.Filename;
            property.Filename = filename;

            return property;
        }

        public AnimationLogicalProperty()
        {
            Flags = PropertyInput.Animation | PropertyInput.Filename | PropertyInput.Invalid;
            Input = PropertyInput.Filename;

            Id = Guid.NewGuid();
            Filename = "";
            Lookup = "";

            Connections = new NoResetObservableCollection<Connection>();
        }

        public AnimationLogicalProperty(AnimationLogicalProperty other)
        {
            Flags = other.Flags;
            Input = other.Input;
            _OutputEnabled = other.OutputEnabled;
            SelectedEvent = other.SelectedEvent;

            Id = Guid.NewGuid();
            Filename = other.Filename;
            Lookup = other.Lookup;

            Connections = new NoResetObservableCollection<Connection>();
        }

        public AnimationLogicalProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public string Filename { get; set; }
        public string Lookup { get; set; }

        public object Tag { get; set; }

        public PropertyInput Flags { get; private set; }

        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }

        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            if (_Input != PropertyInput.Animation)
            {
                from.RemoveConnectionsFromAnchor(this);
            }
        }

        public ILogic Parent { get; set; }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Flags = "Flags";
            public static string Input = "Input";
            public static string OutputEnabled = "OutputEnabled";
            public static string Filename = "Filename";
            public static string Lookup = "Lookup";
            public static string SelectedEvent = "SelectedEvent";
        }

        public AnimationLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));

            Flags = (PropertyInput)info.GetValue(SerializationTag.Flags, typeof(PropertyInput));
            _Input = (PropertyInput)info.GetValue(SerializationTag.Input, typeof(PropertyInput));
            try
            {
                _OutputEnabled = info.GetBoolean(SerializationTag.OutputEnabled);
                _SelectedEvent = (Signal)info.GetValue(SerializationTag.SelectedEvent, typeof(Signal));
            }
            catch (Exception)
            {
            }

            switch (_Input)
            {
                case PropertyInput.Filename:
                    Filename = (string)info.GetValue(SerializationTag.Filename, typeof(string));
                    break;
                case PropertyInput.Dictionary:
                    Lookup = (string)info.GetValue(SerializationTag.Lookup, typeof(string));
                    break;
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);

            info.AddValue(SerializationTag.Flags, Flags);
            info.AddValue(SerializationTag.Input, _Input);
            info.AddValue(SerializationTag.OutputEnabled, _OutputEnabled);
            info.AddValue(SerializationTag.SelectedEvent, _SelectedEvent);

            switch (_Input)
            {
                case PropertyInput.Filename:
                    info.AddValue(SerializationTag.Filename, Filename);
                    break;
                case PropertyInput.Dictionary:
                    info.AddValue(SerializationTag.Lookup, Lookup);
                    break;
            }
        }

        public void ExportToXML(PargenXmlNode parentNode, Motiontree motiontreeParent)
        {
            switch (Input)
            {
                case PropertyInput.Animation:
                    parentNode.AppendTextElementNode("Type", "kAnimationAttributeParameter");
                    foreach (Connection connection in motiontreeParent.Connections)
                    {
                        if (connection.Dest == this)
                        {
                            parentNode.AppendTextElementNode("Parameter", connection.Source.Parent.Name);
                            break;
                        }
                    }
                    break;
                case PropertyInput.Filename:
                    parentNode.AppendTextElementNode("Type", "kAnimationAttributeFilename");
                    parentNode.AppendTextElementNode("Filename", Filename);
                    break;
                case PropertyInput.Invalid:
                    parentNode.AppendTextElementNode("Type", "kAnimationAttributeIgnored");
                    break;
            }
        }

        public object Clone()
        {
            return new AnimationLogicalProperty(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="AnimationLogicalPropertyy"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public AnimationLogicalProperty(XmlReader reader, Database database, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Input = (PropertyInput)uint.Parse(reader.GetAttribute("Input"), CultureInfo.InvariantCulture);
            this.OutputEnabled = bool.Parse(reader.GetAttribute("OutputEnabled"));
            this.Lookup = reader.GetAttribute("Lookup");
            this.Filename = reader.GetAttribute("Filename");

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedEvent = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedEvent == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedEvent = database.GetSignal(signalName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}' inside the '{1}' node. This needs fixing before a export is allowed.", signalId, parent.Name);
                    }
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Input", ((uint)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("OutputEnabled", this.OutputEnabled.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Filename", this.Filename);
            writer.WriteAttributeString("Lookup", this.Lookup);

            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString("Signal", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedEvent.Name);
            }
        }
    }
}

