﻿using System;
using System.Windows.Media;

namespace New.Move.Core
{
    public interface IDesc
    {
        string Name { get; }

        int Id { get; }
        string Help { get; }

        string Group { get; }

        ImageSource Icon { get; }

        Type ConstructType { get; }

        INode Create(Database database);
    }
}