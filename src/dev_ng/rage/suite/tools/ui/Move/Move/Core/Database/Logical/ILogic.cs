﻿using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    public interface ILogic : INode
    {
        IDesc Desc { get; }
    }

    public interface IOperatorItem
    {
        void ExportToXML(PargenXmlNode parentNode);
    }

    public interface IOperator
    {
        System.Collections.Generic.List<IAnchor> Inputs { get; }
        IOperatorItem Item { get; }
    }

    public interface ILooseOperator
    {
        IOperatorItem Item { get; }
    }
}