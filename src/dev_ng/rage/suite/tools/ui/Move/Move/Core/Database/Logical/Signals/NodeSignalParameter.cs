﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class NodeSignalParameter : SignalBase, ISerializable
    {
        public NodeSignalParameter(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public NodeSignalParameter(NodeSignal signal)
            : base(signal)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.NodeSignalParameter"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public NodeSignalParameter(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "nodeparametersignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new NodeSignalParameter(new NodeSignal());
            (clone as NodeSignalParameter).AdditionalText = this.AdditionalText;
            (clone as NodeSignalParameter).Database = this.Database;
            (clone as NodeSignalParameter).Enabled = this.Enabled;
            (clone as NodeSignalParameter).Name = this.Name;
            (clone as NodeSignalParameter).Position = this.Position;
            (clone as NodeSignalParameter).Signal = this.Signal;

            return (ISelectable)clone;
        }
    }
}