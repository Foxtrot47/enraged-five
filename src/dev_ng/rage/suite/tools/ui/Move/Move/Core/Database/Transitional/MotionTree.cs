﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml;
using Move.Core.Restorable;
using Move.Utils;
using Rockstar.MoVE.Framework.DataModel;
using RSG.Base.Logging;
using RSG.ManagedRage;
using System.Globalization;

namespace New.Move.Core
{
    /// <summary>
    /// Represents a transitional motion tree in a MoVE network. 
    /// </summary>
    [Serializable]
    public class Motiontree : RestorableObject, ITransitional
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="Desc"/> property.
        /// </summary>
        private static MotiontreeDesc desc = new MotiontreeDesc();

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Children"/> property.
        /// </summary>
        private const string ChildrenSerializationTag = @"Children";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Connections"/> property.
        /// </summary>
        private const string ConnectionsSerializationTag = @"Connections";

        /// <summary>
        /// The constant offset to apply to the node position.
        /// </summary>
        private const int ConstOffset = 100;

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Database"/> property.
        /// </summary>
        private const string DatabaseSerializationTag = @"Database";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Enabled"/> property.
        /// </summary>
        private const string EnabledSerializationTag = @"Enabled";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="EnterEvent"/> property.
        /// </summary>
        private const string EnterEventSerializationTag = @"EnterEvent";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="ExitEvent"/> property.
        /// </summary>
        private const string ExitEventSerializationTag = @"ExitEvent";
        
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="DeferBlockUpdate"/>
        /// property.
        /// </summary>
        private const string DeferBlockUpdateSerializationTag = @"DeferBlockUpdate";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        private const string IdSerializationTag = @"Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        private const string NameSerializationTag = @"Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Parent"/> property.
        /// </summary>
        private const string ParentSerializationTag = @"Parent";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Position"/> property.
        /// </summary>
        private const string PositionSerializationTag = @"Position";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="ScrollOffset"/> property.
        /// </summary>
        private const string ScrollOffsetSerializationTag = @"ScrollOffset";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="TransitionOrder"/> property.
        /// </summary>
        private const string TransitionOrderSerializationTag = @"TransitionOrder";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Zoom"/> property.
        /// </summary>
        private const string ZoomSerializationTag = @"Zoom";

        /// <summary>
        /// The private field for the <see cref="AdditionalText"/> property.
        /// </summary>
        private string additionalText;

        /// <summary>
        /// The private field for the <see cref="Children"/> property.
        /// </summary>
        private NoResetObservableCollection<INode> children;

        /// <summary>
        /// The private field for the <see cref="Connections"/> property.
        /// </summary>
        private NoResetObservableCollection<Connection> connections;

        /// <summary>
        /// The private field for the <see cref="Database"/> property.
        /// </summary>
        private Database database;

        /// <summary>
        /// The private field for the <see cref="Enabled"/> property.
        /// </summary>
        private bool enabled = true;

        /// <summary>
        /// The private field for the <see cref="EnterEvent"/> property.
        /// </summary>
        private LogicalTunnelEventProperty enterEvent;

        /// <summary>
        /// The private field for the <see cref="ExitEvent"/> property.
        /// </summary>
        private LogicalTunnelEventProperty exitEvent;

        /// <summary>
        /// The private field for the <see cref="DeferBlockUpdate"/> property.
        /// </summary>
        private BoolTransitionProperty deferBlockUpdate;

        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field for the <see cref="Parent"/> property.
        /// </summary>
        private Guid parent;

        /// <summary>
        /// The private field for the <see cref="Position"/> property.
        /// </summary>
        private Point position;

        /// <summary>
        /// The private field for the <see cref="ScrollOffset"/> property.
        /// </summary>
        private Point scrolloffset;

        /// <summary>
        /// The private field for the <see cref="Tag"/> property.
        /// </summary>
        private object tag;

        /// <summary>
        /// The private field for the <see cref="TransitionOrder"/> property.
        /// </summary>
        private TransitionCollection transitionOrder;

        /// <summary>
        /// The private field for the <see cref="Zoom"/> property.
        /// </summary>
        private double zoom;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.Motiontree"/> class.
        /// </summary>
        internal Motiontree()
        {
            this.id = Guid.NewGuid();
            this.additionalText = string.Empty;
            this.children = new NoResetObservableCollection<INode>();
            this.enterEvent = new LogicalTunnelEventProperty();
            this.exitEvent = new LogicalTunnelEventProperty();
            this.deferBlockUpdate = new BoolTransitionProperty();
            this.children = new NoResetObservableCollection<INode>();
            this.connections = new NoResetObservableCollection<Connection>();
            this.connections.CollectionChanged += OnConnectionsChanged;
            this.TransitionOrder = new TransitionCollection();
            this.zoom = 1.5;
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.Motiontree"/> class that will belong to the specified
        /// database with the specified scrolled offset.
        /// </summary>
        /// <param name="database">
        /// The database this motion tree will belong to.
        /// </param>
        public Motiontree(Database database)
            : this()
        {
            this.Database = database;

            ILogic output = database.Create(new OutputDesc());
            output.Position = new Point(0, 0);
            output.Parent = this.Id;
            this.Children.Add(output);
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="New.Move.Core.Motiontree"/> class that will belong to the specified
        /// database with the specified scrolled offset.
        /// </summary>
        /// <param name="database">
        /// The database this motion tree will belong to.
        /// </param>
        /// <param name="scrolloffset">
        /// The initial scroll offset for this motion tree.
        /// </param>
        public Motiontree(Database database, Point scrolloffset)
            : this()
        {
            this.ScrollOffset = scrolloffset;
            this.Database = database;

            ILogic output = database.Create(new OutputDesc());
            output.Position = new Point(scrolloffset.X + ConstOffset, scrolloffset.Y + ConstOffset);
            output.Parent = this.Id;
            this.Children.Add(output);
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Motiontree"/> class as
        /// a copy of the specified instance.
        /// </summary>
        /// <param name="name">
        /// The instance to copy.
        /// </param>
        public Motiontree(Motiontree other)
            : this()
        {
            if (other.Name == "Moving_Run_WeaponHolding_Anim_Initial")
            {
                Trace.Write("fdsafd");
            }

            this.AdditionalText = other.AdditionalText;
            this.Enabled = other.Enabled;
            this.Name = other.Name;
            this.Position = other.Position;
            this.ScrollOffset = other.ScrollOffset;
            this.Tag = null;
            this.Zoom = other.Zoom;
            this.DeferBlockUpdate = new BoolTransitionProperty(other.DeferBlockUpdate);
            this.ExitEvent = new LogicalTunnelEventProperty(other.ExitEvent);
            this.EnterEvent = new LogicalTunnelEventProperty(other.EnterEvent);
            this.Database = other.Database;

            Dictionary<IAnchor, IAnchor> anchors = new Dictionary<IAnchor, IAnchor>();
            foreach (INode child in other.Children)
            {
                INode childClone = child.Clone() as INode;
                //if (childClone.Name != null)
                //    Database.MakeNodeNameUnique(childClone);

                this.Add(childClone);

                CollectionAnchors(child, childClone, ref anchors);
            }

            foreach (Connection connection in other.Connections)
            {
                if (connection.Source == null || connection.Dest == null)
                    continue;
                if (!anchors.ContainsKey(connection.Source) && !anchors.ContainsKey(connection.Dest))
                    continue;

                IAnchor source = null;
                IAnchor destination = null;

                if (anchors.ContainsKey(connection.Source))
                    source = anchors[connection.Source];
                else
                    source = connection.Source;

                if (anchors.ContainsKey(connection.Dest))
                    destination = anchors[connection.Dest];
                else
                    destination = connection.Dest;

                Connection cloneConnection = new Connection(source, destination);
                this.Connections.Add(cloneConnection);
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Motiontree"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public Motiontree(SerializationInfo info, StreamingContext context)
        {
            this.zoom = 1.5;
            this.Name = this.GetValue<string>(NameSerializationTag, info);
            this.Id = this.GetValue<Guid>(IdSerializationTag, info);
            this.Parent = this.GetValue<Guid>(ParentSerializationTag, info);

            this.Position = this.GetValue<Point>(PositionSerializationTag, info);
            this.ScrollOffset = this.GetValue<Point>(ScrollOffsetSerializationTag, info);
            this.Zoom = this.GetValue<double>(ZoomSerializationTag, info);
            this.Database = this.GetValue<Database>(DatabaseSerializationTag, info);

            this.Connections = this.GetValue<NoResetObservableCollection<Connection>>(ConnectionsSerializationTag, info);
            if (this.Connections == null)
                this.Connections = new NoResetObservableCollection<Connection>();

            this.Children = this.GetValue<NoResetObservableCollection<INode>>(ChildrenSerializationTag, info);
            if (this.Children == null)
                this.Children = new NoResetObservableCollection<INode>();

            this.TransitionOrder = this.GetValue<TransitionCollection>(TransitionOrderSerializationTag, info);
            if (this.TransitionOrder == null)
                this.TransitionOrder = new TransitionCollection();

            this.Connections.CollectionChanged += OnConnectionsChanged;

            this.EnterEvent = this.GetValue<LogicalTunnelEventProperty>(EnterEventSerializationTag, info);
            this.ExitEvent = this.GetValue<LogicalTunnelEventProperty>(ExitEventSerializationTag, info);

            this.Enabled = this.GetValue(EnabledSerializationTag, info, true);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Motiontree"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Motiontree(XmlReader reader, Database database, ITransitional parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Parent = parent == null ? Guid.Empty : parent.Id;
            this.Database = database;
            this.Deserialise(reader, parent);
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the default transitional node changes for this state machine.
        /// </summary>
        public event EventHandler<DefaultChangedEventArgs> DefaultChanged;

        /// <summary>
        /// Occurs when this nodes engaged state has changed.
        /// </summary>
        public event EventHandler<IsEngagedChangedEventArgs> IsEngagedChanged;

        /// <summary>
        /// Occurs when the selection state of this transition has changed.
        /// </summary>
        public event EventHandler<IsSelectedChangedEventArgs> IsSelectedChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the descriptor that describes this motion tree.
        /// </summary>
        static public MotiontreeDesc Desc
        {
            get { return desc; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the update for this motion tree is defered.
        /// </summary>
        public BoolTransitionProperty DeferBlockUpdate
        {
            get { return this.deferBlockUpdate; }
            set { this.deferBlockUpdate = value; }
        }

        /// <summary>
        /// Gets or sets any additional text associated with this motion tree.
        /// </summary>
        public string AdditionalText
        {
            get { return this.additionalText; }
            set { this.additionalText = value; }
        }

        /// <summary>
        /// Gets the collection of child nodes currently in this motion tree.
        /// </summary>
        public NoResetObservableCollection<INode> Children
        {
            get { return this.children; }
            private set { this.children = value; }
        }

        /// <summary>
        /// Gets the collection of connections that are currently in this motion tree.
        /// </summary>
        public NoResetObservableCollection<Connection> Connections
        {
            get { return this.connections; }
            private set { this.connections = value; }
        }

        /// <summary>
        /// Gets or sets the current database this motion tree belongs to.
        /// </summary>
        public Database Database
        {
            get { return this.database; }
            set { this.database = value; ; }
        }

        /// <summary>
        /// Gets the transitional descriptor for this motion tree.
        /// </summary>
        public ITransitionalDesc Descriptor
        {
            get { return Motiontree.Desc; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this motion tree is currently enabled.
        /// </summary>
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        /// <summary>
        /// Gets or sets the event property that defines the enter event for this motion tree.
        /// </summary>
        public LogicalTunnelEventProperty EnterEvent
        {
            get { return this.enterEvent; }
            set { this.enterEvent = value; }
        }

        /// <summary>
        /// Gets or sets the event property that defines the exit event for this motion tree.
        /// </summary>
        public LogicalTunnelEventProperty ExitEvent
        {
            get { return this.exitEvent; }
            set { this.exitEvent = value; }
        }

        /// <summary>
        /// Gets or sets the unique global identifier for this motion tree.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; ; }
        }

        /// <summary>
        /// Gets a value indicating whether this motion tree is a automaton.
        /// </summary>
        public bool IsAutomaton
        {
            get { return false; }
        }

        /// <summary>
        /// Gets or sets the name for this motion tree.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Gets or sets the identifier that references the parent for this motion tree.
        /// </summary>
        public Guid Parent
        {
            get
            {
                return this.parent;
            }

            set
            {
                if (object.ReferenceEquals(this.parent, value))
                    return;

                this.parent = value;
                this.OnParentChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current position for this motion tree.
        /// </summary>
        public Point Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// Gets or sets the current scroll offset for this motion tree.
        /// </summary>
        public Point ScrollOffset
        {
            get { return this.scrolloffset; }
            set { this.scrolloffset = value; }
        }

        /// <summary>
        /// Gets or sets the object tag that can be set to the UI component representing
        /// this motion tree.
        /// </summary>
        public object Tag
        {
            get { return this.tag; }
            set { this.tag = value; }
        }

        /// <summary>
        /// Gets or sets the collection of transitions in order for this motion tree.
        /// </summary>
        public TransitionCollection TransitionOrder
        {
            get { return transitionOrder; }
            set { transitionOrder = value; }
        }

        /// <summary>
        /// Gets the string type for this motion tree.
        /// </summary>
        public string Type
        {
            get { return Desc.Name; }
        }

        /// <summary>
        /// Gets or sets the current zoom factor for this motion tree.
        /// </summary>
        public double Zoom
        {
            get { return this.zoom; }
            set { this.zoom = value; }
        }

        private IEnumerable<INode> ChildrenToExport
        {
            get
            {
                IEnumerable<INode> query =
                    from child in Children
                    where (child.GetType() != typeof(Input) &&
                           child.GetType() != typeof(Output) &&
                           child.GetType().BaseType != typeof(SignalBase) &&
                           child.GetType() != typeof(TunnelParameter) &&
                           child.GetType() != typeof(TunnelEvent) &&
                           !(child is IOperator))
                    select child;

                return query;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a node to this motion trees child collection.
        /// </summary>
        /// <param name="node">
        /// The node to add to this motion tree.
        /// </param>
        public void Add(INode node)
        {
            Debug.Assert(node is ILogic);
            //  When cutting and pasting between instances of MoVE then the database
            //  of the node needs to be set to the current database.
            //Debug.Assert(node.Database == Database);
            node.Database = Database;

            ILogic logical = node as ILogic;
            if (logical == null || this.Children.Contains(logical))
                return;

            logical.Parent = this.Id;
            this.Children.Add(logical);

            SetSignalUsed(logical);
        }

        /// <summary>
        /// Adds this given handler to the child collection changed event.
        /// </summary>
        /// <param name="handler">
        /// The handler to add to the children changed event.
        /// </param>
        public void AddHandler(NotifyCollectionChangedEventHandler handler)
        {
            this.Children.CollectionChanged += handler;
        }

        /// <summary>
        /// Returns a object that is a deep clone of the current instance.
        /// </summary>
        /// <returns>
        /// A object that is a deep clone of the current instance.
        /// </returns>
        object ICloneable.Clone()
        {
            return new Motiontree(this);
        }

        /// <summary>
        /// Disposes of this current instance.
        /// </summary>
        public void Dispose()
        {
            if (Parent != Guid.Empty && this.Database != null)
            {
                ITransitional parent = this.Database.Find(Parent) as ITransitional;
                if (parent != null)
                    parent.Remove(this);
            }

            if (this.Database != null)
                this.Database.Nodes.Remove(this);

            if (this.Children == null)
                return;

            INode[] childArray = new INode[Children.Count];
            this.Children.CopyTo(childArray, 0);
            foreach (INode child in childArray)
            {
                child.Dispose();
            }

            this.Children.Clear();
        }

        /// <summary>
        /// Exports the par-code-gen xml representation of this instance.
        /// </summary>
        /// <param name="parentNode">
        /// The parent par-code-gen xml node that this instance appends itself to.
        /// </param>
        /// <param name="hashedNames">
        /// A collection of hashed names to use during the export process.
        /// </param>
        public void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            hashedNames.Add(Name);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "MotionTreeNode");

            itemNode.AppendTextElementNode("Name", Name);
            itemNode.AppendTextElementNode("Initial", FindOutputChild().Name);

            EnterEvent.ExportToXML(itemNode.AppendChild("OnEnterEvent"));
            ExitEvent.ExportToXML(itemNode.AppendChild("OnExitEvent"));

            var childrenNode = itemNode.AppendChild("Children");
            foreach (var childNode in ChildrenToExport)
                childrenNode.AppendTextElementNode("Item", childNode.Name);

            // Special case to add an input node if we have one

            Input input = Children.OfType<Input>().FirstOrDefault();
            if (input != null)
                childrenNode.AppendTextElementNode("Item", input.Name);

            ExportParametersToXML(itemNode);
            ExportEventsToXML(itemNode);
            ExportOutputsToXML(itemNode);
            ExportOperatorsToXML(itemNode);

            var transitionsNode = itemNode.AppendChild("Transitions");
            foreach (var transition in TransitionOrder)
                transition.ExportToXML(transitionsNode);

            itemNode.AppendValueElementNode("DeferBlockUpdate", "value", DeferBlockUpdate.Value.ToString().ToLower());
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("motiontree");

            writer.WriteAttributeString(
                ZoomSerializationTag, this.Zoom.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                IdSerializationTag, this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(NameSerializationTag, this.Name);

            if (this.DeferBlockUpdate != null)
            {
                writer.WriteStartElement(DeferBlockUpdateSerializationTag);
                this.DeferBlockUpdate.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.EnterEvent != null)
            {
                writer.WriteStartElement(EnterEventSerializationTag);
                this.EnterEvent.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ExitEvent != null)
            {
                writer.WriteStartElement(ExitEventSerializationTag);
                this.ExitEvent.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Position != null)
            {
                writer.WriteStartElement(PositionSerializationTag);
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.ScrollOffset != null)
            {
                writer.WriteStartElement(ScrollOffsetSerializationTag);
                writer.WriteAttributeString(
                    "x", this.ScrollOffset.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.ScrollOffset.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            writer.WriteStartElement(ChildrenSerializationTag);
            foreach (INode node in this.Children)
            {
                node.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteStartElement(ConnectionsSerializationTag);
            foreach (Connection connection in this.Connections)
            {
                connection.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteStartElement(TransitionOrderSerializationTag);
            foreach (Transition transition in this.TransitionOrder)
            {
                writer.WriteStartElement("transition");
                writer.WriteAttributeString(
                    "Id", transition.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void SerialiseSet(
            XmlWriter writer,
            IEnumerable<INode> childrenSet,
            IEnumerable<Connection> connectionSet,
            bool writeTransitionOrder)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("motiontree");

            writer.WriteAttributeString(
                ZoomSerializationTag, this.Zoom.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                IdSerializationTag, this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(NameSerializationTag, this.Name);

            if (this.DeferBlockUpdate != null)
            {
                writer.WriteStartElement(DeferBlockUpdateSerializationTag);
                this.DeferBlockUpdate.Serialise(writer);
                writer.WriteEndElement();
            } 
            
            if (this.EnterEvent != null)
            {
                writer.WriteStartElement(EnterEventSerializationTag);
                this.EnterEvent.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.ExitEvent != null)
            {
                writer.WriteStartElement(ExitEventSerializationTag);
                this.ExitEvent.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Position != null)
            {
                writer.WriteStartElement(PositionSerializationTag);
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.ScrollOffset != null)
            {
                writer.WriteStartElement(ScrollOffsetSerializationTag);
                writer.WriteAttributeString(
                    "x", this.ScrollOffset.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.ScrollOffset.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            writer.WriteStartElement(ChildrenSerializationTag);
            foreach (INode node in this.Children)
            {
                if (childrenSet.Contains(node) || node is Output || node is Input)
                {
                    node.Serialise(writer);
                }
            }
            writer.WriteEndElement();

            writer.WriteStartElement(ConnectionsSerializationTag);
            foreach (Connection connection in this.Connections)
            {
                if (connectionSet.Contains(connection))
                {
                    connection.Serialise(writer);
                }
            }
            writer.WriteEndElement();

            writer.WriteStartElement(TransitionOrderSerializationTag);
            if (writeTransitionOrder)
            {
                foreach (Transition transition in this.TransitionOrder)
                {
                    writer.WriteStartElement("transition");
                    writer.WriteAttributeString(
                        "Id", transition.Id.ToString("D", CultureInfo.InvariantCulture));
                    writer.WriteEndElement();
                }
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        public Connection FindConnectionFrom(IAnchor anchor)
        {
            Connection[] connections = new Connection[Connections.Count];
            Connections.CopyTo(connections, 0);

            foreach (Connection connection in connections)
            {
                if (connection.Source.Id == anchor.Id)
                {
                    return connection;
                }
            }

            return null;
        }

        public List<Connection> FindConnectionsFrom(IAnchor anchor)
        {
            List<Connection> connectionList = new List<Connection>();

            Connection[] connections = new Connection[Connections.Count];
            Connections.CopyTo(connections, 0);

            foreach (Connection connection in connections)
            {
                if (connection.Source.Id == anchor.Id)
                {
                    connectionList.Add(connection);
                }
            }

            return connectionList;
        }

        public Connection FindConnectionTo(IAnchor anchor)
        {
            Connection[] connections = new Connection[Connections.Count];
            Connections.CopyTo(connections, 0);

            foreach (Connection connection in connections)
            {
                if (connection == null || connection.Dest == null)
                {
                    continue;
                }

                if (connection.Dest.Id == anchor.Id)
                {
                    // Need to resolve connection anchor parent properties
                    foreach (ILogic child in this.Children.OfType<ILogic>())
                    {
                        List<IAnchor> anchors = new List<IAnchor>();
                        CollectionAnchors(child, ref anchors);
                        foreach (IAnchor childAnchor in anchors)
                        {
                            if (childAnchor == null)
                            {
                                continue;
                            }

                            childAnchor.Parent = child;
                        }
                    }

                    return connection;
                }
            }

            return null;
        }

        public void ResolveConnectionsAfterLoad()
        {
            foreach (Connection connection in connections)
            {
                foreach (ILogic child in this.Children.OfType<ILogic>())
                {
                    List<IAnchor> anchors = new List<IAnchor>();
                    CollectionAnchors(child, ref anchors);
                    foreach (IAnchor childAnchor in anchors)
                    {
                        if (childAnchor == null)
                        {
                            continue;
                        }

                        childAnchor.Parent = child;
                    }
                }
            }
        }

        public ILogic FindOutputChild()
        {
            Output output = Children.OfType<Output>().First();
            Connection connection = FindConnectionTo(output.OutputTransform);
            if (connection != null)
            {
                return connection.Source.Parent;
            }

            return null;
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerializationTag, Name);
            info.AddValue(IdSerializationTag, Id);
            info.AddValue(ParentSerializationTag, Parent);
            info.AddValue(PositionSerializationTag, Position);
            info.AddValue(ScrollOffsetSerializationTag, ScrollOffset);
            info.AddValue(ZoomSerializationTag, Zoom);
            info.AddValue(DatabaseSerializationTag, Database);
            info.AddValue(ConnectionsSerializationTag, Connections);
            info.AddValue(ChildrenSerializationTag, Children);
            info.AddValue(TransitionOrderSerializationTag, TransitionOrder);
            info.AddValue(EnterEventSerializationTag, EnterEvent);
            info.AddValue(ExitEventSerializationTag, ExitEvent);
            info.AddValue(EnabledSerializationTag, Enabled);
        }

        /// <summary>
        /// Called when this transitional node becomes or stops being the default
        /// transitional node for its parent.
        /// </summary>
        /// <param name="isDefault">
        /// A value indicating whether this node is the default transitional node or not.
        /// </param>
        public void OnDefaultChanged(bool isDefault)
        {
            EventHandler<DefaultChangedEventArgs> handler = this.DefaultChanged;
            if (handler == null)
                return;

            this.DefaultChanged(this, new DefaultChangedEventArgs(isDefault));
        }

        /// <summary>
        /// Called when this nodes engaged state has changed.
        /// </summary>
        /// <param name="action">
        /// The action associated with the change.
        /// </param>
        public void OnEngagedChanged(EngagedChangedAction action)
        {
            EventHandler<IsEngagedChangedEventArgs> handler = this.IsEngagedChanged;
            if (handler == null)
                return;

            this.IsEngagedChanged(this, new IsEngagedChangedEventArgs(action));
        }

        /// <summary>
        /// Gets called after this node has been loaded and is added to a database.
        /// </summary>
        public void OnLoadComplete()
        {
            CheckUsedSignals();
        }

        /// <summary>
        /// Looks through all of the child nodes and their anchors and returns the anchor
        /// that has the specified Id.
        /// </summary>
        /// <param name="anchorId">
        /// The id for the anchor to return.
        /// </param>
        /// <returns>
        /// The reference to the anchor that is located on one of the child nodes with the
        /// specified id if found; otherwise, null.
        /// </returns>
        public IAnchor GetAnchor(Guid anchorId)
        {
            List<object> tested = new List<object>();
            foreach (INode child in this.children)
            {
                if (child == null)
                {
                    continue;
                }

                IAnchor anchor = this.GetAnchor(child, anchorId, ref tested);
                if (anchor != null)
                {
                    return anchor;
                }
            }

            Trace.TraceError("Unable to find anchor");
            return null;
        }

        private IAnchor GetAnchor(object obj, Guid anchorId, ref List<object> tested)
        {
            if (tested.Contains(obj))
            {
                return null;
            }

            tested.Add(obj);
            PropertyInfo[] properties = obj.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.GetIndexParameters().Length != 0)
                {
                    continue;
                }

                object value = property.GetValue(obj, null);
                IAnchor anchor = value as IAnchor;
                if (anchor != null && anchor.Id == anchorId)
                {
                    return anchor;
                }

                ILogicProperty logicProperty = value as ILogicProperty;
                if (logicProperty != null)
                {
                    anchor = this.GetAnchor(logicProperty, anchorId, ref tested);
                    if (anchor != null)
                    {
                        return anchor;
                    }
                }

                ILogicPropertyArray logicArray = value as ILogicPropertyArray;
                if (logicArray != null)
                {
                    foreach (IProperty child in logicArray.Children)
                    {
                        anchor = this.GetAnchor(child, anchorId, ref tested);
                        if (anchor != null)
                        {
                            return anchor;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, ITransitional parent)
        {
            if (reader == null)
            {
                return;
            }

            this.zoom = double.Parse(
                reader.GetAttribute(ZoomSerializationTag), CultureInfo.InvariantCulture);
            this.name = reader.GetAttribute(NameSerializationTag);
            this.id = Guid.ParseExact(reader.GetAttribute(IdSerializationTag), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, DeferBlockUpdateSerializationTag) == 0)
                {
                    this.DeferBlockUpdate = new BoolTransitionProperty(reader, this.Database, null);
                }
                else if (string.CompareOrdinal(reader.Name, EnterEventSerializationTag) == 0)
                {
                    this.EnterEvent = new LogicalTunnelEventProperty(reader, this.Database);
                }
                else if (string.CompareOrdinal(reader.Name, ExitEventSerializationTag) == 0)
                {
                    this.ExitEvent = new LogicalTunnelEventProperty(reader, this.Database);
                }
                else if (string.CompareOrdinal(reader.Name, PositionSerializationTag) == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, ScrollOffsetSerializationTag) == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.ScrollOffset = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, ChildrenSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            INode newNode = this.Database.XmlFactory.CreateNode(reader, this.Database, this);
                            if (newNode != null)
                            {
                                this.Children.Add(newNode);
                            }
                        }

                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, ConnectionsSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            Connection newConnection = new Connection(reader, this);
                            if (newConnection.Dest == null || newConnection.Source == null)
                            {
                                Trace.TraceWarning("Invalid connection");
                            }
                            else
                            {
                                this.Connections.Add(newConnection);
                            }
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }     
                }
                else if (string.CompareOrdinal(reader.Name, TransitionOrderSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            Guid id = Guid.Parse(reader.GetAttribute("Id"));
                            if (parent is ITransitionalParent)
                            {
                                Transition transition = (parent as ITransitionalParent).GetTransition(id);
                                if (transition != null)
                                {
                                    this.TransitionOrder.Add(transition);
                                }
                            }

                            reader.Skip();
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Checks the signals in the signal editor to see if they are used in this motiontree
        /// </summary>
        private void CheckUsedSignals()
        {
            if (this.Database != null && this.Database.Signals.Count > 0)
            {
                foreach (Signal signal in this.Database.Signals)
                {
                    foreach (INode child in this.Children)
                    {
                        if (signal.Name == child.Name && signal.Type == child.Type)
                        {
                            signal.SetUsed(true);
                        }
                    }
                }
            }
        }

        private void SetSignalUsed(INode node)
        {
            if (this.Database != null && this.Database.Signals.Count > 0)
            {
                foreach (Signal signal in this.Database.Signals)
                {
                    if (signal.Name == node.Name && signal.Type == node.Type)
                    {
                        signal.SetUsed(true);
                    }
                }
            }
        }

        private void SetSignalRemoved(INode node)
        {
            if (this.Database != null && this.Database.Signals.Count > 0)
            {
                foreach (Signal signal in this.Database.Signals)
                {
                    if (signal.Name == node.Name && signal.Type == node.Type)
                    {
                        signal.SetUsed(false);
                    }
                }
            }
        }

        /// <summary>
        /// Fires the selection changed event.
        /// </summary>
        /// <param name="action">
        /// Defines whether this transition has been added to the selection or removed from
        /// the selection.
        /// </param>
        /// <param name="count">
        /// The total number of items that are currently selected.
        /// </param>
        public void OnSelectedChanged(SelectionChangedAction action, int count)
        {
            EventHandler<IsSelectedChangedEventArgs> handler = this.IsSelectedChanged;
            if (handler == null)
                return;

            this.IsSelectedChanged(this, new IsSelectedChangedEventArgs(action, count));
        }

        /// <summary>
        /// Removes a node from this motion trees child collection.
        /// </summary>
        /// <param name="node">
        /// The node to remove from this motion tree.
        /// </param>
        public void Remove(INode node)
        {
            //  When cutting and pasting between instances of MoVE then the database
            //  of the node needs to be set to the current database.
            //Debug.Assert(node.Database == Database);
            node.Database = Database;
            if (!this.Children.Contains(node))
                return;

            node.Parent = Guid.Empty;
            this.Children.Remove(node);

            SetSignalRemoved(node);

            List<Connection> invalidConnections = new List<Connection>();
            foreach (Connection connection in this.Connections)
            {
                if (object.ReferenceEquals(connection.Source.Parent, node))
                {
                    invalidConnections.Add(connection);
                }
                else if (object.ReferenceEquals(connection.Dest.Parent, node))
                {
                    invalidConnections.Add(connection);
                }
            }
            foreach (Connection connection in invalidConnections)
                this.Connections.Remove(connection);
        }

        public void RemoveConnectionsFromAnchor(IAnchor anchor)
        {
            Connection[] connections = new Connection[Connections.Count];
            Connections.CopyTo(connections, 0);

            foreach (Connection connection in connections)
            {
                if (connection.Dest.Id == anchor.Id || connection.Source.Id == anchor.Id)
                {
                    Connections.Remove(connection);
                }
            }
        }

        public void RemoveConnectionsFromEvent(LogicalEventProperty property)
        {
            Connection[] connections = new Connection[Connections.Count];
            Connections.CopyTo(connections, 0);

            foreach (Connection connection in connections)
            {
                if (connection.Dest.Id == property.Id || connection.Source.Id == property.Id)
                {
                    Connections.Remove(connection);
                }
            }
        }

        public void RemoveConnectionsFromProperty(IProperty property)
        {
            Connection[] connections = new Connection[Connections.Count];
            Connections.CopyTo(connections, 0);

            foreach (Connection connection in connections)
            {
                if (connection.Dest.Id == property.Id || connection.Source.Id == property.Id)
                {
                    Connections.Remove(connection);
                }
            }
        }

        public void RemoveConnectionsTo(IAnchor anchor)
        {
            Connection[] connections = new Connection[Connections.Count];
            Connections.CopyTo(connections, 0);

            foreach (Connection connection in connections)
            {
                if (connection.Dest.Id == anchor.Id)
                {
                    Connections.Remove(connection);
                }
            }
        }

        /// <summary>
        /// Removes this given handler to the child collection changed event.
        /// </summary>
        /// <param name="handler">
        /// The handler to remove to the children changed event.
        /// </param>
        public void RemoveHandler(NotifyCollectionChangedEventHandler handler)
        {
            this.Children.CollectionChanged -= handler;
        }

        /// <summary>
        /// Gets the string representation of this instance.
        /// </summary>
        /// <returns>
        /// The string representation of this instance.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }

        private void CollectionAnchors(object node, ref List<IAnchor> anchors)
        {
            System.Type nodeType = node.GetType();
            System.Reflection.PropertyInfo[] nodeProperties = nodeType.GetProperties();

            for (int i = 0; i < nodeProperties.Length; i++)
            {
                System.Reflection.PropertyInfo nodeProp = nodeProperties[i];
                if (nodeProp == null)
                    continue;

                ParameterInfo[] parameters = nodeProp.GetIndexParameters();
                if (parameters.Length >= 1)
                    continue;

                if (nodeProp.PropertyType.GetInterface("IAnchor", true) != null)
                {
                    object nodeValue = nodeProp.GetValue(node, null);
                    if (!anchors.Contains(nodeValue as IAnchor))
                        anchors.Add(nodeValue as IAnchor);
                }
                else if (nodeProp.PropertyType.GetInterface("ILogicPropertyArray", true) != null)
                {
                    object nodeValue = nodeProp.GetValue(node, null);
                    List<IProperty> nodeChildren = new List<IProperty>();
                    foreach (var item in (nodeValue as ILogicPropertyArray).Children)
                    {
                        nodeChildren.Add(item);
                    }

                    for (int j = 0; j < nodeChildren.Count; j++)
                    {
                        CollectionAnchors(nodeChildren[j], ref anchors);
                    }
                }
                else if (nodeProp.PropertyType.GetInterface("IProperty", true) != null)
                {
                    object nodeValue = nodeProp.GetValue(node, null);
                    CollectionAnchors(nodeValue, ref anchors);
                }
            }
        }

        private void CollectionAnchors(object node, object clone, ref Dictionary<IAnchor, IAnchor> anchors)
        {
            System.Type nodeType = node.GetType();
            System.Reflection.PropertyInfo[] nodeProperties = nodeType.GetProperties();

            System.Type cloneType = clone.GetType();
            System.Reflection.PropertyInfo[] cloneProperties = cloneType.GetProperties();

            for (int i = 0; i < nodeProperties.Length; i++)
            {
                if (i > cloneProperties.Length - 1)
                    break;

                System.Reflection.PropertyInfo nodeProp = nodeProperties[i];
                if (nodeProp == null)
                    continue;

                ParameterInfo[] parameters = nodeProp.GetIndexParameters();
                if (parameters.Length >= 1)
                    continue;

                System.Reflection.PropertyInfo cloneProp = null;
                foreach (PropertyInfo info in cloneProperties)
                {
                    if (info.Name == nodeProperties[i].Name)
                    {
                        cloneProp = info;
                        break;
                    }
                }

                if (cloneProp == null)
                    continue;

                object nodeValue = nodeProp.GetValue(node, null);
                object cloneValue = cloneProp.GetValue(clone, null);

                if (nodeValue is IAnchor)
                {
                    if (!anchors.ContainsKey(nodeValue as IAnchor))
                        anchors.Add(nodeValue as IAnchor, cloneValue as IAnchor);
                }
                else if (nodeValue is ILogicPropertyArray)
                {
                    List<IProperty> nodeChildren = new List<IProperty>();
                    List<IProperty> cloneChildren = new List<IProperty>();
                    foreach (var item in (nodeValue as ILogicPropertyArray).Children)
                    {
                        nodeChildren.Add(item);
                    }
                    foreach (var item in (cloneValue as ILogicPropertyArray).Children)
                    {
                        cloneChildren.Add(item);
                    }
                    for (int j = 0; j < nodeChildren.Count; j++)
                    {
                        CollectionAnchors(nodeChildren[j], cloneChildren[j], ref anchors);
                    }
                }
                else if (nodeValue is IProperty)
                {
                    CollectionAnchors(nodeValue, cloneValue, ref anchors);
                }
            }
        }

        private void OnConnectionsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (Connection connection in e.NewItems)
                {
                    Connection ex = Connections.FirstOrDefault(
                        delegate(Connection c)
                        {
                            return c != connection && (c.Source is ITransformAnchor) && c.Source == connection.Source;
                        }
                    );

                    if (ex != null)
                        connection.IsProxy = true;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Connection connection in e.OldItems)
                {
                    Connection ex = Connections.FirstOrDefault(
                        delegate(Connection c)
                        {
                            return c != connection && c.Source == connection.Source;
                        }
                    );

                    if (ex != null)
                        ex.IsProxy = false;
                }
            }
        }

        private void ExportEventsToXML(PargenXmlNode parentNode)
        {
            var eventExportData = new List<EventExportItem>();

            // embedded events
            foreach (INode child in ChildrenToExport)
            {
                PropertyInfo[] properties = child.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo pi in properties)
                {
                    object pobj = pi.GetValue(child, null);
                    if (pobj as LogicalEventProperty != null)
                    {
                        LogicalEventProperty evt = pobj as LogicalEventProperty;
                        if (evt.Enabled && evt.SelectedEvent != null && evt.SelectedEvent.Name != "")
                        {
                            eventExportData.Add(
                                new EventExportItem(evt.SelectedEvent.Name, evt.Parent.Name, pi.Name));
                        }
                    }
                }
            }

            var events = new List<TunnelEvent>();// Build container of unique events
            foreach (TunnelEvent evt in Children.OfType<TunnelEvent>())
            {
                if (!events.Contains(evt))
                {
                    events.Add(evt);
                }
            }

            foreach (TunnelEvent evt in events)
            {
                // NOTE: does not support more than one connection to an event!!!!!
                IAnchor src = null;
                foreach (Connection connection in Connections)
                {
                    if (connection.Dest.Parent == evt)
                    {
                        src = connection.Source;
                        break;
                    }
                }

                // get the type of the event from the property attribute
                PropertyInfo[] properties = src.Parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo pi in properties)
                {
                    object value = pi.GetValue(src.Parent, null);
                    if (value == src)
                    {
                        eventExportData.Add(
                            new EventExportItem(evt.Event.Name, src.Parent.Name, pi.Name));
                        break;
                    }
                }
            }

            var eventsNode = parentNode.AppendChild("Events");
            foreach (var eventItem in eventExportData)
            {
                var eventItemNode = eventsNode.AppendChild("Item");
                eventItemNode.AppendTextElementNode("Event", eventItem.Name);
                eventItemNode.AppendTextElementNode("Target", eventItem.TargetName);
                eventItemNode.AppendTextElementNode("Type", eventItem.TypeName);
            }
        }

        private void ExportOperatorsToXML(PargenXmlNode parentNode)
        {
            List<Connection> fromOperatorToLogic = new List<Connection>();
            foreach (Connection connection in Connections)
            {
                if (connection.Source.Parent is IOperator && !(connection.Dest.Parent is IOperator))
                {
                    if (!fromOperatorToLogic.Contains(connection))
                    {
                        fromOperatorToLogic.Add(connection);
                    }
                }
            }

            List<Connection> fromOperatorToLogicWithCurve = new List<Connection>();
            foreach (Connection connection in Connections)
            {
                if ((connection.Dest is Property) && ((connection.Source.Parent is IOperator && !(connection.Dest.Parent is IOperator)) || (connection.Source.Parent is SignalBase)))
                {
                    if ((connection.Dest as Property).Input == PropertyInput.Curve)
                    {
                        if (!fromOperatorToLogicWithCurve.Contains(connection))
                        {
                            fromOperatorToLogicWithCurve.Add(connection);
                        }
                    }
                }
            }

            List<Connection> handledConnections = new List<Connection>();

            var operatorExportData = new List<OperatorExportItem>();

            foreach (Connection connection in fromOperatorToLogicWithCurve)
            {
                if (connection.Dest != null)
                {
                    if (connection.Dest is RealLogicalProperty && (connection.Dest as Property).Input == PropertyInput.Curve)
                    {
                        handledConnections.Add(connection);

                        List<IOperatorItem> operations = TraverseOperatorStackAlt(connection);
                        if ((connection.Dest as RealLogicalProperty).Container is ILooseOperator)
                        {
                            IOperatorItem embeddedOp = ((connection.Dest as RealLogicalProperty).Container as ILooseOperator).Item;
                            operations.Add(embeddedOp);
                        }
                        else if ((connection.Dest as RealLogicalProperty).Parent is ILooseOperator)
                        {
                            IOperatorItem embeddedOp = ((connection.Dest as RealLogicalProperty).Parent as ILooseOperator).Item;
                            operations.Add(embeddedOp);
                        }

                        string attributeName = "";// Lookup the property name from the parent node via reflection (this is very mental)
                        PropertyInfo[] propertyInfoData = connection.Dest.Parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                        foreach (PropertyInfo pi in propertyInfoData)
                        {
                            object value = pi.GetValue(connection.Dest.Parent, null);
                            if (value == connection.Dest)
                            {
                                attributeName = pi.Name;
                            }
                            else if (value is ILogicPropertyArray)
                            {
                                ILogicPropertyArray arr = (ILogicPropertyArray)value;
                                IProperty at = null;
                                if (arr.Contains(connection.Dest as IProperty, out at))
                                {
                                    uint index = (uint)arr.IndexOf(connection.Dest as IProperty);

                                    PropertyInfo[] arrayProperties = at.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                    foreach (PropertyInfo arrayPI in arrayProperties)
                                    {
                                        object arrayValue = arrayPI.GetValue(at, null);
                                        if (arrayValue == connection.Dest)
                                        {
                                            attributeName = arrayPI.Name;
                                        }
                                    }
                                }
                            }
                        }

                        operatorExportData.Add(
                            new OperatorExportItem(
                                connection.Dest.Parent.Name,
                                attributeName,
                                (uint)FindParameterOffsetIndex(connection.Dest),
                                operations));
                    }
                }
            }

            foreach (Connection connection in fromOperatorToLogic)
            {
                if (connection.Dest != null)
                {
                    if (null == handledConnections.Find(delegate(Connection c) { return c == connection; }))
                    {
                        List<IOperatorItem> operations = TraverseOperatorStack(connection.Source.Parent as IOperator);

                        string attributeName = "";// Lookup the property name from the parent node (this is pretty mental)
                        foreach (PropertyInfo pi in connection.Dest.Parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                        {
                            object value = pi.GetValue(connection.Dest.Parent, null);
                            if (value == connection.Dest)
                            {
                                attributeName = pi.Name;
                            }
                            else if (value is ILogicPropertyArray)
                            {
                                ILogicPropertyArray arr = (ILogicPropertyArray)value;
                                IProperty at = null;
                                if (arr.Contains(connection.Dest as IProperty, out at))
                                {
                                    uint index = (uint)arr.IndexOf(connection.Dest as IProperty);

                                    PropertyInfo[] arrayProperties = at.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                    foreach (PropertyInfo arrayPI in arrayProperties)
                                    {
                                        object arrayValue = arrayPI.GetValue(at, null);
                                        if (arrayValue == connection.Dest)
                                        {
                                            attributeName = arrayPI.Name;
                                        }
                                    }
                                }
                            }
                        }

                        operatorExportData.Add(
                            new OperatorExportItem(
                                connection.Dest.Parent.Name,
                                attributeName,
                                (uint)FindParameterOffsetIndex(connection.Dest),
                                operations));
                    }
                }
            }

            var operatorsNode = parentNode.AppendChild("Operators");
            foreach (var item in operatorExportData)
                item.ExportToXML(operatorsNode);
        }

        private void ExportOutputsToXML(PargenXmlNode parentNode)
        {
            var outputExportData = new List<OutputExportItem>();

            outputExportData.AddRange(GetOutputsAttachedToProperties());

            List<TunnelParameter> outputParams = new List<TunnelParameter>();
            foreach (TunnelParameter parameter in Children.OfType<TunnelParameter>())
            {
                if (!outputParams.Contains(parameter))
                {
                    outputParams.Add(parameter);
                }
            }

            foreach (TunnelParameter parameter in outputParams)
            {
                // NOTE: doesn't support more than one connection to an event!!!
                IAnchor src = null;
                foreach (Connection connection in Connections)
                {
                    if (connection.Dest.Parent == parameter)
                    {
                        src = connection.Source;
                        break;
                    }
                }

                if (src != null)
                {
                    // get the 'type' of the signal from the property attribute
                    PropertyInfo[] properties = src.Parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    foreach (PropertyInfo pi in properties)
                    {
                        object value = pi.GetValue(src.Parent, null);
                        if (value == src)
                        {
                            outputExportData.Add(
                                new OutputExportItem(
                                    parameter.Parameter.Name,
                                    src.Parent.Name,
                                    pi.Name,
                                    0));
                        }
                        else if (value is ILogicPropertyArray)
                        {
                            ILogicPropertyArray arr = value as ILogicPropertyArray;
                            if (arr.Children.Contains(src as IProperty))
                            {
                                outputExportData.Add(
                                new OutputExportItem(
                                    parameter.Parameter.Name,
                                    src.Parent.Name,
                                    pi.Name,
                                    0));
                            }
                        }
                    }
                }
            }

            // export the makeshift output parameters, this will need to include anything that outputs to an operator!!!
            List<Connection> sameLevelConnections = new List<Connection>();

            foreach (Connection connection in Connections)
            {
                ILogic src = connection.Source.Parent;
                ILogic dest = connection.Dest.Parent;

                if (!(src is IOperator) && src is Logic && !(dest is IOperator) && dest is Logic)
                {
                    if (!(connection.Source is ITransformAnchor) &&
                        !(connection.Dest is ITransformAnchor))
                    {
                        if (!sameLevelConnections.Contains(connection))
                        {
                            sameLevelConnections.Add(connection);
                        }
                    }
                }
            }

            // sameLevelConnections contains all Logic->Logic connections
            foreach (Connection connection in sameLevelConnections)
            {
                if (connection.Source != null)
                {
                    PropertyInfo[] sourceNodePropertyInfoArray = connection.Source.Parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    foreach (PropertyInfo sourceNodePropertyInfo in sourceNodePropertyInfoArray)
                    {
                        object sourceNodePropertyObject = sourceNodePropertyInfo.GetValue(connection.Source.Parent, null);
                        if (sourceNodePropertyObject == connection.Source)// Is the source anchored to a regular node property?
                        {
                            outputExportData.Add(
                                new OutputExportItem(
                                    connection.Id.ToString(),
                                    connection.Source.Parent.Name,
                                    sourceNodePropertyInfo.Name,
                                    0));
                        }
                        else if (sourceNodePropertyObject is ILogicPropertyArray)
                        {
                            // is the source anchored to a child of a property array?  As in an n-add/blend/merge
                            ILogicPropertyArray propertyArray = sourceNodePropertyObject as ILogicPropertyArray;
                            for (int propertyArrayChildIndex = 0; propertyArrayChildIndex < propertyArray.Children.Count(); ++propertyArrayChildIndex)
                            {
                                IProperty childProperty = propertyArray.Children.ElementAt(propertyArrayChildIndex);
                                if (childProperty is ILogicPropertyGroup)
                                {
                                    ILogicPropertyGroup childPropertyGroup = (ILogicPropertyGroup)childProperty;
                                    PropertyInfo[] childPropertyGroupPropertyInfoArray = childPropertyGroup.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                    foreach (PropertyInfo childPropertyGroupPropertyInfo in childPropertyGroupPropertyInfoArray)
                                    {
                                        object childPropertyGroupPropertyObject = childPropertyGroupPropertyInfo.GetValue(childPropertyGroup, null);
                                        if (childPropertyGroupPropertyObject == connection.Source)
                                        {
                                            outputExportData.Add(
                                                new OutputExportItem(
                                                    connection.Id.ToString(),
                                                    connection.Source.Parent.Name,
                                                    childPropertyGroupPropertyInfo.Name,
                                                    (uint)propertyArrayChildIndex));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // fromLogicToOperator
            List<Connection> fromLogicToOperator = new List<Connection>();
            foreach (Connection connection in Connections)
            {
                if (!(connection.Source.Parent is IOperator) && !(connection.Source.Parent is SignalBase) && connection.Dest.Parent is IOperator)
                {
                    if (!fromLogicToOperator.Contains(connection))
                    {
                        fromLogicToOperator.Add(connection);
                    }
                }
            }

            foreach (Connection connection in fromLogicToOperator)
            {
                if (connection.Source != null)
                {
                    outputExportData.Add(
                        new OutputExportItem(
                            connection.Id.ToString(),
                            connection.Source.Parent.Name,
                            "",
                            0));
                }
            }

            List<ProxyNodeViaConnection> proxyConnections = new List<ProxyNodeViaConnection>();
            foreach (Connection connection in Connections)
            {
                if (connection.IsProxy && !(connection.Dest.Parent is TunnelParameter) && !(connection.Source.Parent is TunnelParameter))
                {
                    ProxyNodeViaConnection proxy = new ProxyNodeViaConnection() { ParentNode = this, Connection = connection };
                    proxy.Parent = Id;
                    //                    proxyNodes.Add(proxy);
                    //                    childrenToExportList.Add(proxy);
                    proxyConnections.Add(proxy);
                }
            }

            foreach (ProxyNodeViaConnection proxy in proxyConnections)
            {
                outputExportData.Add(
                    new OutputExportItem(
                        proxy.Connection.Id.ToString(),
                        proxy.Connection.Source.Parent.Name,
                        "",
                        0));
            }

            // Now that the output data has been built up in memory, export it to XML
            var outputsNode = parentNode.AppendChild("Outputs");
            foreach (var output in outputExportData)
            {
                var outputItemNode = outputsNode.AppendChild("Item");
                outputItemNode.AppendTextElementNode("Parameter", output.Name);
                outputItemNode.AppendTextElementNode("Target", output.TargetName);
                outputItemNode.AppendTextElementNode("Attribute", output.AttributeName);
                outputItemNode.AppendValueElementNode("Index", "value", output.Index.ToString());
            }
        }

        private void ExportParametersToXML(PargenXmlNode parentNode)
        {
            var parameterExportData = new List<ParameterExportItem>();

            List<Connection> fromOperatorToLogicWithCurve = new List<Connection>();
            foreach (Connection connection in Connections)
            {
                if ((connection.Dest is Property) && ((connection.Source.Parent is IOperator && !(connection.Dest.Parent is IOperator)) || (connection.Source.Parent is SignalBase)))
                {
                    if ((connection.Dest as Property).Input == PropertyInput.Curve)
                    {
                        if (!fromOperatorToLogicWithCurve.Contains(connection))
                        {
                            fromOperatorToLogicWithCurve.Add(connection);
                        }
                    }
                }
            }

            List<Connection> parameters = new List<Connection>();
            foreach (SignalBase node in Children.OfType<SignalBase>())
            {
                if (!(node is NodeSignalParameter))
                {
                    List<Connection> connectionList = FindConnectionsFrom(node.Anchor);
                    foreach (Connection connection in connectionList)
                    {
                        if (null == fromOperatorToLogicWithCurve.Find(delegate(Connection conn) { return connection == conn; }))
                        {
                            if (connection.Dest != null)
                            {
                                if (!(connection.Dest.Parent is IOperator) && !(connection.Dest.Parent is Reference))
                                {
                                    if (!parameters.Contains(connection))
                                    {
                                        parameters.Add(connection);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            foreach (Connection connection in parameters)
            {
                // find the node I'm connected to
                IAnchor dest = connection.Dest;
                ILogic s = connection.Source.Parent;

                if (dest != null)
                {
                    ILogic target = null;
                    bool isRefTarget = false;
                    foreach (ILogic logic in Children)
                    {
                        PropertyInfo[] properties = logic.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                        foreach (PropertyInfo pi in properties)
                        {
                            object value = pi.GetValue(logic, null);
                            if (value == dest)
                            {
                                target = logic;
                                break;
                            }
                            else if (value is ILogicPropertyArray)
                            {
                                ILogicPropertyArray arr = value as ILogicPropertyArray;
                                IProperty at = null;
                                if (arr.Contains(dest as IProperty, out at))
                                {
                                    target = logic;
                                    break;
                                }
                            }
                        }
                    }

                    if (target != null && isRefTarget == false)
                    {
                        // get the 'type' of the signal from the property attribute
                        PropertyInfo[] properties = target.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                        foreach (PropertyInfo pi in properties)
                        {
                            object value = pi.GetValue(target, null);
                            if (value == dest)
                            {
                                parameterExportData.Add(
                                    new ParameterExportItem(s.Name, target.Name, pi.Name, 0));
                            }
                            else if (value is ILogicPropertyArray)
                            {
                                ILogicPropertyArray arr = value as ILogicPropertyArray;
                                IProperty at = null;
                                if (arr.Contains(dest as IProperty, out at))
                                {
                                    uint index = (uint)arr.IndexOf(dest as IProperty);

                                    PropertyInfo[] arrayProperties = at.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                    foreach (PropertyInfo arrayPI in arrayProperties)
                                    {
                                        object arrayValue = arrayPI.GetValue(at, null);
                                        if (arrayValue == dest)
                                        {
                                            parameterExportData.Add(
                                                new ParameterExportItem(s.Name, target.Name, arrayPI.Name, index));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            List<Connection> sameLevelConnections = new List<Connection>();
            foreach (Connection connection in Connections)
            {
                if (connection.Dest == null || connection.Source == null)
                {
                    continue;
                }

                ILogic src = connection.Source.Parent;
                ILogic dest = connection.Dest.Parent;

                if (!(src is IOperator) && src is Logic && !(dest is IOperator) && dest is Logic)
                {
                    if (!(connection.Source is ITransformAnchor) &&
                        !(connection.Dest is ITransformAnchor))
                    {
                        if (!sameLevelConnections.Contains(connection))
                        {
                            sameLevelConnections.Add(connection);
                        }
                    }
                }
            }

            foreach (Connection connection in sameLevelConnections)
            {
                if (connection.Dest != null)
                {
                    PropertyInfo[] destNodePropertyInfoArray = connection.Dest.Parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    foreach (PropertyInfo destNodePropertyInfo in destNodePropertyInfoArray)
                    {
                        object destNodePropertyObject = destNodePropertyInfo.GetValue(connection.Dest.Parent, null);
                        if (destNodePropertyObject == connection.Dest)
                        {
                            parameterExportData.Add(
                                new ParameterExportItem(
                                    connection.Id.ToString(),
                                    connection.Dest.Parent.Name,
                                    destNodePropertyInfo.Name,
                                    0));
                        }
                        else if (destNodePropertyObject is ILogicPropertyArray)
                        {
                            ILogicPropertyArray propertyArray = (ILogicPropertyArray)destNodePropertyObject;
                            if (propertyArray.Children.Contains((IProperty)connection.Dest))
                            {
                                string attributeName = "";// get the attribute name using reflection
                                PropertyInfo[] propertyArrayPropertyInfoArray = propertyArray.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                foreach (PropertyInfo propertyArrayPropertyInfo in propertyArrayPropertyInfoArray)
                                {
                                    object propertyArrayPropertyObject = propertyArrayPropertyInfo.GetValue(propertyArray, null);
                                    if (propertyArrayPropertyObject == connection.Dest)
                                    {
                                        attributeName = propertyArrayPropertyInfo.Name;
                                        break;
                                    }
                                }

                                uint index = (uint)propertyArray.IndexOf((IProperty)connection.Dest);

                                parameterExportData.Add(
                                    new ParameterExportItem(
                                        connection.Id.ToString(),
                                        connection.Dest.Parent.Name,
                                        attributeName,
                                        index));
                            }
                        }
                    }
                }
            }

            var parametersNode = parentNode.AppendChild("Parameters");
            foreach (var parameter in parameterExportData)
            {
                var outputItemNode = parametersNode.AppendChild("Item");
                outputItemNode.AppendTextElementNode("Parameter", parameter.Name);
                outputItemNode.AppendTextElementNode("Target", parameter.TargetName);
                outputItemNode.AppendTextElementNode("Attribute", parameter.AttributeName);
                outputItemNode.AppendValueElementNode("Index", "value", parameter.Index.ToString());
            }
        }

        private ushort FindParameterExportIndex(IAnchor anchor)
        {
            PropertyInfo[] properties = anchor.Parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                object value = pi.GetValue(anchor.Parent, null);
                if (value == anchor)
                {
                    object[] attributes = pi.GetCustomAttributes(typeof(ParameterExportAttribute), false);
                    if (attributes.Length != 0)
                    {
                        ParameterExportAttribute attribute = attributes[0] as ParameterExportAttribute;
                        return (ushort)attribute.GetOffset(0);
                    }
                }
                else if (value is ILogicPropertyArray)
                {
                    ILogicPropertyArray arr = value as ILogicPropertyArray;
                    IProperty at = null;
                    if (arr.Contains(anchor as IProperty, out at))
                    {
                        object[] attributes = pi.GetCustomAttributes(typeof(ParameterExportAttribute), false);
                        if (attributes.Length != 0)
                        {
                            ParameterExportAttribute attribute = attributes[0] as ParameterExportAttribute;
                            if (at is ILogicPropertyGroup)
                            {
                                ILogicPropertyGroup group = at as ILogicPropertyGroup;
                                int index = group.IndexOf(anchor as IProperty);
                                return (ushort)attribute.GetOffset(index);
                            }
                            else
                            {
                                return (ushort)attribute.GetOffset(0);
                            }
                        }
                    }
                }
            }

            return 0xffff;
        }

        private ushort FindParameterOffsetIndex(IAnchor anchor)
        {
            PropertyInfo[] properties = anchor.Parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                object value = pi.GetValue(anchor.Parent, null);
                if (value == anchor)
                {
                    object[] attributes = pi.GetCustomAttributes(typeof(ParameterExportAttribute), false);
                    if (attributes.Length != 0)
                    {
                        ParameterExportAttribute attribute = attributes[0] as ParameterExportAttribute;
                        return (ushort)attribute.GetOffset(0);
                    }
                }
                else if (value is ILogicPropertyArray)
                {
                    ILogicPropertyArray arr = value as ILogicPropertyArray;
                    //if (arr.Children.Contains(anchor as IProperty))
                    IProperty at = null;
                    if (arr.Contains(anchor as IProperty, out at))
                    {
                        return (ushort)arr.IndexOf((IProperty)anchor);
                    }
                }
            }

            return 0xffff;
        }

        private IEnumerable<OutputExportItem> GetOutputsAttachedToProperties()
        {
            // Here we go through every property on every exported node and use reflection (!) to determine if the user has selected to output
            // that property to a control parameter.
            var outputs = new List<OutputExportItem>();

            foreach (INode childNode in ChildrenToExport)
            {
                if (childNode is Reference)
                    continue;

                PropertyInfo[] nodeProperties = childNode.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo propertyInfo in nodeProperties)
                {
                    object propertyObject = propertyInfo.GetValue(childNode, null);
                    if (propertyObject is Property)
                    {
                        // This is a regular named property of type New.Move.Core.Property, such as Weight from a Clip node
                        Property property = (Property)propertyObject;
                        if (property.OutputEnabled && property.SelectedEvent != null && !string.IsNullOrEmpty(property.SelectedEvent.Name))
                        {
                            outputs.Add(new OutputExportItem(
                                    property.SelectedEvent.Name,
                                    (property as IAnchor).Parent.Name,
                                    propertyInfo.Name,
                                    0));
                        }
                    }
                    else if (propertyObject is ILogicPropertyArray)
                    {
                        // This is an array of properties, such as the inputs of an n-add/blend/merge node
                        ILogicPropertyArray propertyArray = (ILogicPropertyArray)propertyObject;
                        uint arrayChildIndex = 0;
                        foreach (IProperty propertyArrayChild in propertyArray.Children)
                        {
                            if (propertyArrayChild is Property)
                            {
                                Property property = propertyArrayChild as Property;
                                if (property.OutputEnabled && property.SelectedEvent != null && !string.IsNullOrEmpty(property.SelectedEvent.Name))
                                {
                                    outputs.Add(new OutputExportItem(
                                            property.SelectedEvent.Name,
                                            (property as IAnchor).Parent.Name,
                                            propertyInfo.Name,
                                            0));
                                }
                            }
                            else if (propertyArrayChild is ILogicPropertyGroup)
                            {
                                ILogicPropertyGroup propertyGroup = propertyArrayChild as ILogicPropertyGroup;
                                PropertyInfo[] propertyGroupProperties = propertyGroup.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                for (int i = 0; i < propertyGroup.Count(); i++)
                                {
                                    if (propertyGroup.ItemAt(i) is Property)
                                    {
                                        Property property = (Property)propertyGroup.ItemAt(i);
                                        if (property.OutputEnabled && property.SelectedEvent != null && !string.IsNullOrEmpty(property.SelectedEvent.Name))
                                        {
                                            string attributeName = "";// lookup the attribute name via reflection
                                            foreach (var propertyGroupPropertyInfo in propertyGroupProperties)
                                            {
                                                object propertyGroupPropertyObject = propertyGroupPropertyInfo.GetValue(propertyGroup, null);
                                                if (propertyGroupPropertyObject == property)
                                                {
                                                    attributeName = propertyGroupPropertyInfo.Name;
                                                    break;
                                                }
                                            }

                                            outputs.Add(
                                                new OutputExportItem(
                                                    property.SelectedEvent.Name,
                                                    (property as IAnchor).Parent.Name,
                                                    attributeName,
                                                    arrayChildIndex));
                                        }
                                    }
                                }
                            }

                            ++arrayChildIndex;
                        }
                    }
                }
            }

            return outputs;
        }

        /// <summary>
        /// Gets the value of the object inside the specified serialisation info object in the
        /// specified type with the specified name.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value that is being initialised from the serialisation info.
        /// </typeparam>
        /// <param name="tagName">
        /// The name of the property to get from the serialisation info object.
        /// </param>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to get the property.
        /// </param>
        /// <returns>
        /// The value of the object with the specific tag in the specified serialisation info
        /// if found; otherwise the default value for T.
        /// </returns>
        private T GetValue<T>(string tagName, SerializationInfo info)
        {
            if (info == null)
                return default(T);

            try
            {
                object value = info.GetValue(tagName, typeof(T));
                if (value is T)
                    return (T)value;
            }
            catch (SerializationException)
            {
                LogFactory.ApplicationLog.Debug("Deserialisation exception with tag name {0}", tagName);
            }

            return default(T);
        }

        /// <summary>
        /// Gets the value of the object inside the specified serialisation info object in the
        /// specified type with the specified name.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value that is being initialised from the serialisation info.
        /// </typeparam>
        /// <param name="tagName">
        /// The name of the property to get from the serialisation info object.
        /// </param>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to get the property.
        /// </param>#
        /// <param name="fallback">
        /// A fallback value to return if a exception is thrown during the deserialisation.
        /// </param>
        /// <returns>
        /// The value of the object with the specific tag in the specified serialisation info
        /// if found; otherwise the specified fallback value.
        /// </returns>
        private T GetValue<T>(string tagName, SerializationInfo info, T fallback)
        {
            if (info == null)
                return fallback;

            try
            {
                object value = info.GetValue(tagName, typeof(T));
                if (value is T)
                    return (T)value;
            }
            catch (SerializationException)
            {
                LogFactory.ApplicationLog.Debug("Deserialisation exception with tag name {0}", tagName);
            }

            return fallback;
        }

        /// <summary>
        /// Gets called whenever the parent property has been changed.
        /// </summary>
        private void OnParentChanged()
        {
            if (Parent == Guid.Empty || Database == null)
                return;

            List<INode> path = new List<INode>();
            Guid id = Parent;

            bool inputValid = false;
            while (id != Guid.Empty)
            {
                INode curr = Database.Find(id);
                if (curr == null)
                    break;

                if (curr is LogicInlineSM)
                {
                    INode first = path.FirstOrDefault(
                        delegate(INode node) { return (node is Motiontree); });
                    if (first == null)
                    {
                        inputValid = true;
                    }

                    break;
                }

                path.Add(curr);
                id = curr.Parent;
            }
            if (inputValid)
            {
                Input[] inputs = Children.OfType<Input>().ToArray();
                if (inputs.Length == 0)
                {
                    ILogic input = Database.Create(new InputDesc());
                    Point pos = new Point(0, 0);
                    if (Children.Count() > 0)
                    {
                        pos = new Point(Children[0].Position.X - ConstOffset, Children[0].Position.Y - ConstOffset);
                    }
                    input.Position = pos;
                    input.Parent = Id;
                    Children.Add(input);
                }
                else if (inputs.Length > 1)
                {
                    for (int i = inputs.Length - 1; i > 0; i++)
                    {
                        Children.Remove(inputs[i]);
                    }
                }
            }
        }

        private List<IOperatorItem> TraverseOperatorStack(IOperator op)
        {
            Stack<object> traversal = new Stack<object>();
            traversal.Push(op);

            Stack<IOperatorItem> operators = new Stack<IOperatorItem>();

            while (traversal.Count != 0)
            {
                object obj = traversal.Pop();
                if (obj is IOperator)
                {
                    IOperator curr = obj as IOperator;
                    operators.Push(curr.Item);

                    foreach (IAnchor input in curr.Inputs)
                    {
                        Connection connection = FindConnectionTo(input);
                        ILogic node = connection.Source.Parent;
                        if (node is IOperator)
                        {
                            traversal.Push(node);
                        }
                        else if (node is SignalBase)
                        {
                            traversal.Push(node);
                        }
                        else if (node is Logic)
                        {
                            traversal.Push(connection);
                        }
                    }
                }
                else if (obj is SignalBase)
                {
                    SignalBase curr = obj as SignalBase;
                    operators.Push(new OperatorFindAndPushValue(curr.Signal.Name));
                }
                else if (obj is Connection)
                {
                    Connection curr = obj as Connection;
                    operators.Push(new OperatorFindAndPushValue(curr.Id.ToString()));
                }
            }

            return new List<IOperatorItem>(operators);
        }

        private List<IOperatorItem> TraverseOperatorStackAlt(Connection con)
        {
            Stack<object> traversal = new Stack<object>();
            traversal.Push(con.Source.Parent);

            Stack<IOperatorItem> operators = new Stack<IOperatorItem>();

            while (traversal.Count != 0)
            {
                object obj = traversal.Pop();
                if (obj is IOperator)
                {
                    IOperator curr = obj as IOperator;
                    operators.Push(curr.Item);

                    foreach (IAnchor input in curr.Inputs)
                    {
                        Connection connection = FindConnectionTo(input);
                        ILogic node = connection.Source.Parent;
                        if (node is IOperator)
                        {
                            traversal.Push(node);
                        }
                        else if (node is SignalBase)
                        {
                            traversal.Push(node);
                        }
                        else if (node is Logic)
                        {
                            traversal.Push(connection);
                        }
                    }
                }
                else if (obj is SignalBase)
                {
                    SignalBase curr = obj as SignalBase;
                    operators.Push(new OperatorFindAndPushValue(curr.Signal.Name));
                }
                else if (obj is Connection)
                {
                    Connection curr = obj as Connection;
                    operators.Push(new OperatorFindAndPushValue(curr.Id.ToString()));
                }
            }

            return new List<IOperatorItem>(operators);
        }

        /// <summary>
        /// Copies this item and the speciifed child items by serialising them out to the
        /// specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the specified items get serialised out to.
        /// </param>
        /// <param name="items">
        /// The child items that should be serialised to the specified writer.
        /// </param>
        public void Copy(XmlWriter writer, IEnumerable<ISelectable> items)
        {
            writer.WriteStartElement(ChildrenSerializationTag);
            foreach (INode node in this.Children)
            {
                if (!items.Contains(node) || node is Output || node is Input)
                {
                    continue;
                }

                node.Serialise(writer);
            }

            writer.WriteEndElement();

            writer.WriteStartElement(ConnectionsSerializationTag);
            foreach (Connection connection in this.Connections)
            {
                if (!items.Contains(connection))
                {
                    continue;
                }

                connection.Serialise(writer);
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Pastes the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The reader containing the data to paste.
        /// </param>
        /// <param name="offset">
        /// A offset point that determines the position offset for the child objects.
        /// </param>
        public void Paste(XmlReader reader, Point offset, bool cutting)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            List<INode> children = new List<INode>();
            List<Connection> connections = new List<Connection>();
            reader.ReadStartElement();
            List<Guid> ids = new List<Guid>();
            Dictionary<Guid, Guid> idMap = new Dictionary<Guid, Guid>();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals(ChildrenSerializationTag, reader.Name))
                {
                    if (!reader.IsEmptyElement)
                    {
                        Dictionary<Guid, INode> map = new Dictionary<Guid, INode>();
                        reader.ReadStartElement();
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            INode newNode = this.Database.XmlFactory.CreateNode(reader, this.Database, this);
                            newNode.Position = new Point(newNode.Position.X + offset.X, newNode.Position.Y + offset.Y);
                            if (!cutting)
                            {
                                if (!(newNode is SignalBase))
                                {
                                    this.Database.MakeNodeNameUnique(newNode);
                                }
                            }

                            this.Database.Nodes.Add(newNode);
                            if (newNode != null)
                            {
                                map.Add(newNode.Id, newNode);
                                children.Add(newNode);
                            }
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }

                    foreach (INode child in children)
                    {
                        ids.AddRange(child.GetAllGuids());
                    }

                    foreach (Guid id in ids)
                    {
                        if (id == Guid.Empty)
                        {
                            continue;
                        }

                        if (idMap.ContainsKey(id))
                        {
                            continue;
                        }

                        idMap.Add(id, Guid.NewGuid());
                    }

                    foreach (INode child in children)
                    {
                        child.ResetIds(idMap);
                        this.Children.Add(child);
                    }
                }
                else if (string.CompareOrdinal(reader.Name, ConnectionsSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            Connection newConnection = new Connection(reader, this, idMap);
                            this.Connections.Add(newConnection);
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Skip();
                }
            }
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
            foreach (Connection connection in this.Connections)
            {
                connection.Id = Database.GetId(map, connection.Id);
            }

            foreach (INode child in this.Children)
            {
                child.Parent = this.Id;
                child.ResetIds(map);
            }
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            foreach (Connection connection in this.Connections)
            {
                ids.Add(connection.Id);
            }

            foreach (INode node in this.Children)
            {
                List<Guid> nodeIds = node.GetAllGuids();
                ids.AddRange(nodeIds);
            }

            return ids;
        }
        #endregion

        private class EventExportItem
        {
            internal EventExportItem(string name, string targetName, string typeName)
            {
                Name = name;
                TargetName = targetName;
                TypeName = typeName;
            }

            internal string Name { get; private set; }

            internal string TargetName { get; private set; }

            internal string TypeName { get; private set; }
        }

        private class OperatorExportItem
        {
            private string attributeName_;

            private uint index_;

            private List<IOperatorItem> operations_;

            private string targetName_;

            internal OperatorExportItem(string targetName, string attributeName, uint index, IEnumerable<IOperatorItem> operations)
            {
                targetName_ = targetName;
                attributeName_ = attributeName;
                index_ = index;
                operations_ = new List<IOperatorItem>(operations);
            }

            internal void ExportToXML(PargenXmlNode parentNode)
            {
                var itemNode = parentNode.AppendChild("Item");
                itemNode.AppendTextElementNode("Target", targetName_);
                itemNode.AppendTextElementNode("Attribute", attributeName_);
                itemNode.AppendValueElementNode("Index", "value", index_.ToString());

                var operationsNode = itemNode.AppendChild("Operations");
                foreach (IOperatorItem operation in operations_)
                    operation.ExportToXML(operationsNode);
            }
        }

        class OperatorFindAndPushValue : IOperatorItem
        {
            public OperatorFindAndPushValue(string parameterName)
            {
                ParameterName = parameterName;
                ParameterNameHash = StringHashUtil.atStringHash(parameterName, 0);
            }

            public string ParameterName { get; private set; }

            public uint ParameterNameHash { get; private set; }

            public void ExportToXML(PargenXmlNode parentNode)
            {
                var itemNode = parentNode.AppendValueElementNode("Item", "type", "FindAndPushValueOperator");
                itemNode.AppendTextElementNode("Parameter", ParameterName);
            }
        }

        class OperatorPushValue : IOperatorItem
        {
            public OperatorPushValue(float value)
            {
                Value = value;
            }

            public float Value { get; private set; }

            public void ExportToXML(PargenXmlNode parentNode)
            {
                var itemNode = parentNode.AppendValueElementNode("Item", "type", "PushValueOperator");
                itemNode.AppendValueElementNode("Value", "type", Value.ToString());
            }
        }

        private class OutputExportItem
        {
            internal OutputExportItem(string name, string targetName, string attributeName, uint index)
            {
                Name = name;
                TargetName = targetName;
                AttributeName = attributeName;
                Index = index;
            }

            internal string AttributeName { get; private set; }

            internal uint Index { get; private set; }

            internal string Name { get; private set; }

            internal string TargetName { get; private set; }
        }

        private class ParameterExportItem
        {
            internal ParameterExportItem(string name, string targetName, string attributeName, uint index)
            {
                Name = name;
                TargetName = targetName;
                AttributeName = attributeName;
                Index = index;
            }

            internal string AttributeName { get; private set; }

            internal uint Index { get; private set; }

            internal string Name { get; private set; }

            internal string TargetName { get; private set; }
        }
    }

    public abstract class ProxyNode : Logic 
    {
        protected ProxyNode()
            : base(null)
        {
        }

        public ITransitional ParentNode { get; set; }

        public abstract uint GenerateId();
    }   

    public class ProxyNodeViaConnection : ProxyNode
    {
        public ProxyNodeViaConnection()
        {
            Result = new ResultTransform(this);
        }

        public Connection Connection { get; set; }

        public ResultTransform Result { get; private set; }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override uint GenerateId()
        {
            uint hash = (uint)StringHashUtil.atStringHash(Connection.Id.ToString(), 0);
            return hash;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            return ids;
        }
    }   
}