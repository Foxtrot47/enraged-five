﻿namespace Move.Core.Restorable
{
    internal sealed class SetRestorablePropertyAction : IRestorableAction
    {
        public void Undo()
        {
            Target.Properties[PropertyName].Value = OldValue;
        }

        public void Redo()
        {
            Target.Properties[PropertyName].Value = NewValue;
        }

        public string PropertyName { get; set; }
        public object NewValue { get; set; }
        public object OldValue { get; set; }

        public RestorableObject Target { get; set; }
    }
}