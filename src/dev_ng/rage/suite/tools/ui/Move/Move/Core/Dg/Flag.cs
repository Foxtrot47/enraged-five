﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Xml.Serialization;

using Rage.Move.Core.Dag;

namespace Rage.Move.Core.Dg
{
    public class GuidPair
    {
        public Guid Database { get; set; }
        public Guid Flag { get; set; }

        public static bool operator ==(GuidPair lhs, GuidPair rhs)
        {
            return (lhs.Database == rhs.Database) && (lhs.Flag == rhs.Flag);
        }

        public static bool operator !=(GuidPair lhs, GuidPair rhs)
        {
            return (lhs.Database != rhs.Database) || (lhs.Flag != rhs.Flag);
        }

        public override bool Equals(object obj)
        {
            if (obj is GuidPair)
            {
                GuidPair rhs = (GuidPair)obj;
                return (Database == rhs.Database) && (Flag == rhs.Flag);
            }

            return false;
        }
    }
}

/*
    [Serializable]
    public class Flag
    {
        public Flag(string name)
        {
            Name = name;
            Id = Guid.NewGuid();
        }

        public Flag() { }

        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public Guid Id { get; set; }
 */
        /*
        public int GetIndex(Database db)
        {
            foreach (Flag flag in db.Flags)
            {
                if (flag.Id == Id)
                {
                    List<string> flaglist = new List<string>();
                    foreach (Flag f in Runtime.Database.Flags)
                    {
                        string[] split = f.Name.Split(':');
                        if (!flaglist.Contains(split[0]))
                            flaglist.Add(split[0]);
                    }

                    if (Runtime.Database.Id == db.Id)
                        return flaglist.IndexOf(flag.Name);

                    // see if there's a special path string
                    Reference reference = Runtime.Instance.References[db.Id];
                    string name = String.Format("{0}:{1}", reference.Name, flag.Name);

                    foreach (Flag f in Runtime.Database.Flags)
                    {
                        if (f.Name.EndsWith(name))
                        {
                            string[] split = f.Name.Split(':');
                            return flaglist.IndexOf(split[0]);
                            //return Runtime.Database.Flags.IndexOf(f);
                        }
                    }
                }
            }

            return -1;
        }
         */

#if no_index
        public int GetIndex(Database db)
        {
            foreach (Flag flag in db.Flags)
            {
                if (flag.Id == Id)
                {
                    // this is the index of actual flags that will be baked out
                    List<string> flaglist = new List<string>();
                    foreach (Flag f in Runtime.Database.Flags)
                    {
                        string[] split = f.Name.Split(':');
                        if (!flaglist.Contains(split[0]))
                            flaglist.Add(split[0]);
                    }

                    if (Runtime.Database.Id == db.Id)
                        return flaglist.IndexOf(flag.Name);
#if disabled
                    if (Runtime.Database.HACK_EXPORT_FLAG != null)
                    {
                        GuidPair search = new GuidPair();
                        search.Database = db.Id;
                        search.Flag = Id;
                        foreach (KeyValuePair<GuidPair, string> i in Runtime.Database.HACK_EXPORT_FLAG)
                        {
                            if (i.Key == search)
                            {
                                return flaglist.IndexOf(i.Value);
                            }
                        }

                        return -1;
                    }
#endif
                    Dictionary<GuidPair, string> remaps = new Dictionary<GuidPair, string>();
                    foreach (Flag f in Runtime.Database.Flags)
                    {
                        string[] split = f.Name.Split(':');
                        if (split.Length > 1)
                        {
                            Queue<string[]> queue = new Queue<string[]>();
                            queue.Enqueue(new string[] { split[1], split[2] });
                            while (queue.Count != 0)
                            {
                                string[] item = queue.Dequeue();
#if disabled
                                foreach (Reference reference in Runtime.Instance.References.Values)
                                {
                                    if (reference.Name == item[0])
                                    {
                                        foreach (Flag f2 in reference.Database.Flags)
                                        {
                                            if (f2.Name.StartsWith(item[1] + ":"))
                                            {
                                                string[] path = f2.Name.Split(':');
                                                if (path.Length > 1)
                                                {
                                                    queue.Enqueue(new string[] { path[1], path[2] });
                                                }
                                            }
                                            else if (f2.Name == item[1])
                                            {
                                                GuidPair pair = new GuidPair();
                                                pair.Database = reference.Database.Id;
                                                pair.Flag = f2.Id;
                                                if (!remaps.ContainsKey(pair))
                                                {
                                                    //Console.WriteLine("added {0}", f.Name);
                                                    remaps.Add(pair, split[0]);
                                                }
                                            }
                                        }

                                        break;
                                    }
                                }
#endif
                            }
                        }
                        else
                        {
                            GuidPair pair = new GuidPair();
                            pair.Database = Runtime.Database.Id;
                            pair.Flag = f.Id;
                            if (!remaps.ContainsKey(pair))
                            {
                                //Console.WriteLine("added {0}", f.Name);
                                remaps.Add(pair, f.Name);
                            }
                        }
                    }
#if disabled
                    Runtime.Database.HACK_EXPORT_FLAG = remaps;
#endif
                    GuidPair searchitem = new GuidPair();
                    searchitem.Database = db.Id;
                    searchitem.Flag = Id;
                    foreach (KeyValuePair<GuidPair, string> i in remaps)
                    {
                        if (i.Key == searchitem)
                        {
                            return flaglist.IndexOf(i.Value);
                        }
                    }

                    return -1;

                    // foreach flag, go through and resolve all possible
                    //  mappings as flagname->flag.id (id as key)

                    // return indexof this list[flag.id]

                    //end

                    // can we find a reference node with the right name?

                }
            }

            return -1;
        }
#endif
   // }
#if not_used
    public class FlagConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type type)
        {
            if (type == typeof(string))
                return true;

            return base.CanConvertFrom(context, type);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                Guid id = new Guid(value as string);
                /*
                foreach (Database database in Runtime.Instance.Databases.Values)
                {
                    IEnumerable<Flag> item =
                        from flag in database.Flags where flag.Id == id select flag;

                    if (item.Count() != 0)
                        return item.ElementAt(0);
                }
                 */
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type type)
        {
            if (type == typeof(string))
                return (value as Flag).Id.ToString();

            return base.ConvertTo(context, culture, value, type);
        }
    }
}
#endif

