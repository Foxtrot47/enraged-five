﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class TagReference : ReferenceSignal, ISerializable
    {
        public TagReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public TagReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}