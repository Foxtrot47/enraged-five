﻿namespace New.Move.Core
{
    public interface ILogicPropertyPair : ILogicProperty, ILogicContainerProperty
    {
        void RemoveConnections(INode parent);

        object First { get; }
        object Second { get; }

        bool Contains(IProperty property);
    }
}