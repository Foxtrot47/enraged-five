﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    internal sealed class ObservableCollectionSerializationSurrogate<T> : ISerializationSurrogate
    {
        const string ItemsKey = "Items";

        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            System.Diagnostics.Debug.Assert(obj is ObservableCollection<T>);

            ObservableCollection<T> items = obj as ObservableCollection<T>;
            info.AddValue("Count", items.Count);
            for (int index = 0; index < items.Count; ++index)
            {
                info.AddValue(index.ToString(), items[index]);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            ObservableCollection<T> items = new ObservableCollection<T>();
            int count = (int)info.GetValue("Count", typeof(int));
            for (int index = 0; index < count; ++index)
            {
                items.Add((T)info.GetValue(index.ToString(), typeof(T)));
            }

            return items;
        }
    }
}