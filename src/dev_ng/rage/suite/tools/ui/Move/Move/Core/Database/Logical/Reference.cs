﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Core.Restorable;
using Move.Utils;

using Rockstar.MoVE.Framework.DataModel;
using RSG.Base.Logging;
using RSG.ManagedRage;
using System.Globalization;
using System.Windows;


namespace New.Move.Core
{
    [Serializable]
    public class Reference : Logic
    {
        public enum ConstParamType {
            kSignalAnimation,
            kSignalClip,
            kSignalExpression,
            kSignalPM = 6,
            kSignalReal,
        }; // these are the only supported signals at the moment. Filter could be added since you have a way to data-drive that.


        
        public FilenameLogicalProperty Filename
        {
            get;
            private set;
        }

        new public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Properties = new RestorableCollection();
        }

        public event EventHandler ReferencedDatabaseChanging;
        void OnReferencedDatabaseChanging()
        {
            if (ReferencedDatabaseChanging != null)
                ReferencedDatabaseChanging(this, EventArgs.Empty);
        }

        public event EventHandler ReferencedDatabaseChanged;
        void OnReferencedDatabaseChanged()
        {
            if (ReferencedDatabaseChanged != null)
                ReferencedDatabaseChanged(this, EventArgs.Empty);
        }

        DatabaseReference _ReferencedDatabase;
        public DatabaseReference ReferencedDatabase
        {
            get { return _ReferencedDatabase; }
            set
            {
                if (_ReferencedDatabase != value)
                {
                    OnReferencedDatabaseChanging();
                    _ReferencedDatabase = value;
                    OnReferencedDatabaseChanged();
                }
            }
        }

        public RestorableObjectLogicalPropertyArray ReferenceRequestProperties
        {
            get;
            private set;
        }

        public RestorableObjectLogicalPropertyArray ReferenceFlagProperties
        {
            get;
            private set;
        }

        public PropertyLogicalPropertyArray ReferenceSignalProperties
        //public LogicalPropertyArray<ClipLogicalProperty> ReferenceSignalProperties
        {
            get;
            private set;
        }

        NoResetObservableCollection<ReferenceSignal> _ReferenceSignals;
        public NoResetObservableCollection<ReferenceSignal> ReferenceSignals
        {
            get { return _ReferenceSignals; }
            private set { _ReferenceSignals = value; }
        }

        
        NoResetObservableCollection<ReferenceRequest> _ReferenceRequests;
        public NoResetObservableCollection<ReferenceRequest> ReferenceRequests
        {
            get { return _ReferenceRequests; }
            private set { _ReferenceRequests = value; }
        }

        NoResetObservableCollection<ReferenceFlag> _ReferenceFlags;
        public NoResetObservableCollection<ReferenceFlag> ReferenceFlags
        {
            get { return _ReferenceFlags; }
            private set { _ReferenceFlags = value; }
        }

        public ResultTransform Source { get; private set; }


        public DatabaseXmlFactory XmlFactory { get; set; }

        public Reference(IDesc desc)
            : base(desc)
        {
            Filename = new FilenameLogicalProperty();
            Filename.PropertyChanged += new PropertyChangedEventHandler(Filename_PropertyChanged);

            Source = new ResultTransform(this);

            ReferenceSignals = new NoResetObservableCollection<ReferenceSignal>();
            ReferenceRequests = new NoResetObservableCollection<ReferenceRequest>();
            ReferenceFlags = new NoResetObservableCollection<ReferenceFlag>();
            ReferenceSignalProperties = new PropertyLogicalPropertyArray();
            ReferenceSignalProperties.Parent = this;

            ReferenceRequestProperties = new RestorableObjectLogicalPropertyArray();
            ReferenceFlagProperties = new RestorableObjectLogicalPropertyArray();

            ReferencedDatabaseChanging += new EventHandler(Reference_ReferenceDatabaseChanging);
            ReferencedDatabaseChanged += new EventHandler(Reference_ReferenceDatabaseChanged);
        }

        public Reference(Reference other)
            : base(other.Desc)
        {
            Filename = new FilenameLogicalProperty(other.Filename);
            Filename.PropertyChanged += new PropertyChangedEventHandler(Filename_PropertyChanged);
            Name = other.Name;
            this.AdditionalText = other.AdditionalText;
            this.Database = other.Database;
            this.Enabled = other.Enabled;
            this.Id = Guid.NewGuid();
            this.Parent = other.Parent;
            this.Position = other.Position;
            this.Source = new ResultTransform(this);

            ReferenceSignals = new NoResetObservableCollection<ReferenceSignal>();
            ReferenceRequests = new NoResetObservableCollection<ReferenceRequest>();
            ReferenceFlags = new NoResetObservableCollection<ReferenceFlag>();
            ReferenceSignalProperties = new PropertyLogicalPropertyArray();
            ReferenceSignalProperties.Parent = this;

            ReferenceRequestProperties = new RestorableObjectLogicalPropertyArray();
            ReferenceFlagProperties = new RestorableObjectLogicalPropertyArray();

            ReferencedDatabaseChanging += new EventHandler(Reference_ReferenceDatabaseChanging);
            ReferencedDatabaseChanged += new EventHandler(Reference_ReferenceDatabaseChanged);

            Filename_PropertyChanged(this.Filename, new PropertyChangedEventArgs("Value"));
            this.XmlFactory = other.XmlFactory;
        }

        public override ISelectable Clone(Logic clone)
        {
            return new Reference(this);
        }

        public override void ExportToXMLImpl(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "ReferenceNode");

            itemNode.AppendTextElementNode("Name", Name);

            string referneceText = Path.GetFileNameWithoutExtension(Filename.Value);
            itemNode.AppendTextElementNode("Reference", referneceText);

            ExportInputParamaterRemappingsToXML(itemNode.AppendChild("Parameters"), motiontreeParent.Connections);
            ExportOutputParameterRemappingsToXML(itemNode.AppendChild("OutputParameters"));
            ExportRequestRemappingsToXML(itemNode.AppendChild("Requests"));
            ExportFlagRemappingsToXML(itemNode.AppendChild("Flags"));
            ExportInitDataToXML(itemNode.AppendChild("InitData"));
        }

        private void ExportInputParamaterRemappingsToXML(PargenXmlNode parentNode, IEnumerable<Connection> connections)
        {
            foreach (ReferenceSignal signal in ReferenceSignals)
            {
                foreach (Connection connection in connections)
                {
                    if (connection.Dest == signal)
                    {
                        var remappingNode = parentNode.AppendChild("Item");
                        remappingNode.AppendTextElementNode("From", connection.Source.Parent.Name);
                        remappingNode.AppendTextElementNode("To", signal.Name);
                    }
                }
            }
        }

        private void ExportOutputParameterRemappingsToXML(PargenXmlNode parentNode)
        {
            foreach (ReferenceSignal signal in ReferenceSignals)
            {
                if (signal.AssocProperty != null && signal.AssocProperty.OutputEnabled && signal.AssocProperty.SelectedEvent != null)
                {
                    var remappingNode = parentNode.AppendChild("Item");
                    remappingNode.AppendTextElementNode("From", signal.Name);
                    remappingNode.AppendTextElementNode("To", signal.AssocProperty.SelectedEvent.Name);
                }
            }
        }

        private void ExportRequestRemappingsToXML(PargenXmlNode parentNode)
        {
            foreach (RestorableObjectLogicalProperty flag in ReferenceFlagProperties.Children)
            {
                var remappingNode = parentNode.AppendChild("Item");
                remappingNode.AppendTextElementNode("From", (flag.SelectedObject as Flag).Name);
                remappingNode.AppendTextElementNode("To", flag.Name);
            }
        }

        private void ExportFlagRemappingsToXML(PargenXmlNode parentNode)
        {
            foreach (RestorableObjectLogicalProperty request in ReferenceRequestProperties.Children)
            {
                var remappingNode = parentNode.AppendChild("Item");
                remappingNode.AppendTextElementNode("From", (request.SelectedObject as Request).Name);
                remappingNode.AppendTextElementNode("To", request.Name);
            }
        }

        private void ExportInitDataToXML(PargenXmlNode parentNode)
        {
            foreach (ReferenceSignal refSignal in ReferenceSignals.Where(rs => rs.AssocProperty != null))
            {
                switch (refSignal.AssocProperty.Input)
                {
                    case PropertyInput.Filename:
                        switch (refSignal.GetParameter())
                        {
                            case Parameter.Animation:
                                ExportAnimationInitDataToXML(parentNode, refSignal.Name, refSignal.AssocProperty);
                                break;
                            case Parameter.Clip:
                                ExportClipInitDataToXML(parentNode, refSignal.Name, refSignal.AssocProperty);
                                break;
                            case Parameter.Expressions:
                                ExportExpressionInitDataToXML(parentNode, refSignal.Name, refSignal.AssocProperty);
                                break;
                            case Parameter.ParameterizedMotion:
                                ExportParameterizedMotionInitDataToXML(parentNode, refSignal.Name, refSignal.AssocProperty);
                                break;
                        }
                        break;
                    case PropertyInput.Clip:
                    case PropertyInput.ParameterizedMotion:
                    case PropertyInput.Expressions:
                    case PropertyInput.Real:
                        break;
                    case PropertyInput.Value:
                        switch (refSignal.GetParameter())
                        {
                            case Parameter.Boolean:
                                ExportBooleanInitDataToXML(parentNode, refSignal.Name, refSignal.AssocProperty);
                                break;
                            default:
                                ExportFloatInitDataToXML(parentNode, refSignal.Name, refSignal.AssocProperty);
                                break;
                        }
                        break;
                }
            }
        }

        private void ExportAnimationInitDataToXML(PargenXmlNode parentNode, string refParamName, Property refParamProperty)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "AnimationInitData");
            itemNode.AppendTextElementNode("Key", refParamName);
            IExternalFileLink refParamPropertyAsFileLink = (IExternalFileLink)refParamProperty;
            itemNode.AppendTextElementNode("Filename", refParamPropertyAsFileLink.Filename);
        }

        private void ExportClipInitDataToXML(PargenXmlNode parentNode, string refParamName, Property refParamProperty)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "ClipInitData");
            itemNode.AppendTextElementNode("Name", refParamName);
            PargenXmlNode childNode = itemNode.AppendChild("Clip");

            ClipLogicalProperty clipProperty = refParamProperty as ClipLogicalProperty;

            childNode.AppendTextElementNode("Type", "kClipAttributeFilename");
            switch (clipProperty.Source)
            {
                case ClipSource.VariableClipSet:
                {
                    childNode.AppendTextElementNode("Context", "kClipContextVariableClipSet");
                    if (clipProperty.VariableClipSet != null)
                    {
                        childNode.AppendTextElementNode("BankName", clipProperty.VariableClipSet.Name);
                    }
                    else
                    {
                        childNode.AppendTextElementNode("BankName", "Default");
                    }
                    childNode.AppendTextElementNode("ClipName", clipProperty.VariableClipSetClip);
                }
                break;
                case ClipSource.ClipDictionary:
                {
                    childNode.AppendTextElementNode("Context", "kClipContextClipDictionary");
                    childNode.AppendTextElementNode("BankName", clipProperty.ClipDictionary);
                    childNode.AppendTextElementNode("ClipName", clipProperty.ClipDictionaryClip);
                }
                break;
                case ClipSource.ClipSet:
                {
                    childNode.AppendTextElementNode("Context", "kClipContextAbsoluteClipSet");
                    childNode.AppendTextElementNode("BankName", clipProperty.AbsoluteClipSet);
                    childNode.AppendTextElementNode("ClipName", clipProperty.AbsoluteClipSetClip);
                }
                break;
                case ClipSource.LocalFile:
                {
                    childNode.AppendTextElementNode("Context", "kClipContextLocalFile");
                    childNode.AppendTextElementNode("ClipName", clipProperty.Filename);
                }
                break;
            }
        }

        private void ExportExpressionInitDataToXML(PargenXmlNode parentNode, string refParamName, Property refParamProperty)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "ExpressionInitData");
            itemNode.AppendTextElementNode("Key", refParamName);
            IExternalFileLink refParamPropertyAsFileLink = (IExternalFileLink)refParamProperty;
            itemNode.AppendTextElementNode("Filename", refParamPropertyAsFileLink.Filename);
        }

        private void ExportParameterizedMotionInitDataToXML(PargenXmlNode parentNode, string refParamName, Property refParamProperty)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "ParameterizedMotionInitData");
            itemNode.AppendTextElementNode("Key", refParamName);
            IExternalFileLink refParamPropertyAsFileLink = (IExternalFileLink)refParamProperty;
            itemNode.AppendTextElementNode("Filename", refParamPropertyAsFileLink.Filename);
        }

        private void ExportFloatInitDataToXML(PargenXmlNode parentNode, string refParamName, Property refParamProperty)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "FloatInitData");
            itemNode.AppendTextElementNode("Key", refParamName);
            RealLogicalProperty realRefParamProperty = (RealLogicalProperty)refParamProperty;
            itemNode.AppendValueElementNode("Value", "value", realRefParamProperty.Value.ToString());
        }

        private void ExportBooleanInitDataToXML(PargenXmlNode parentNode, string refParamName, Property refParamProperty)
        {
            var itemNode = parentNode.AppendValueElementNode("Item", "type", "BooleanInitData");
            itemNode.AppendTextElementNode("Key", refParamName);
            BooleanLogicalProperty booleanRefParamProperty = (BooleanLogicalProperty)refParamProperty;
            itemNode.AppendValueElementNode("Value", "value", booleanRefParamProperty.Value.ToString());
        }

        void Reference_ReferenceDatabaseChanging(object sender, EventArgs e)
        {
            if (ReferencedDatabase != null)
            {
                ReferencedDatabase.Signals.CollectionChanged -= new NotifyCollectionChangedEventHandler(Signals_CollectionChanged);
                ReferencedDatabase.Requests.CollectionChanged -= new NotifyCollectionChangedEventHandler(Requests_CollectionChanged);
                ReferencedDatabase.Flags.CollectionChanged -= new NotifyCollectionChangedEventHandler(Flags_CollectionChanged);

                ReferenceRequests.CollectionChanged -= new NotifyCollectionChangedEventHandler(Requests_CollectionChanged);
                ReferenceFlags.CollectionChanged -= new NotifyCollectionChangedEventHandler(Flags_CollectionChanged);
                ReferenceSignals.CollectionChanged -= new NotifyCollectionChangedEventHandler(SignalsProperties_CollectionChanged);
            }
        }

        internal void UpdateReferenceSignals()
        {
            //ReferencedDatabase.Signals.CollectionChanged += new NotifyCollectionChangedEventHandler(Signals_CollectionChanged);

            List<Signal> signals = new List<Signal>();

            ReferenceSignal[] oldSignals = new ReferenceSignal[ReferenceSignals.Count];
            ReferenceSignals.CopyTo(oldSignals, 0);
            
            foreach (ReferenceSignal referenced in oldSignals)
            {
                Signal found = ReferencedDatabase.Signals.FirstOrDefault(delegate(Signal signal)
                {
                    return signal.Id == referenced.Reference;
                }
                );

                if (found != null)
                {
                    signals.Add(found);
                }
                else
                {
                    ReferenceSignals.Remove(referenced);
                }
            }

            foreach (IReferredSignal signal in ReferencedDatabase.Signals.OfType<IReferredSignal>())
            {
                if (!signals.Contains(signal as Signal))
                {
                    int index = ReferencedDatabase.Signals.IndexOf(signal as Signal);
                    if (index < ReferenceSignals.Count)
                    {
                        ReferenceSignal reference = signal.CreateReference();
                        reference.SetParent(this);
                        //  ROBM - Not needed anymore
//                         bool found = false;
//                         IEnumerator<IProperty> propEnum = ReferenceSignalProperties.Children.GetEnumerator();
//                         while (propEnum != null && propEnum.MoveNext())
//                         {
//                             if (propEnum.Current is IProperty)
//                             {
//                                 if (reference.Name == (propEnum.Current as IProperty).Name)
//                                 {
//                                     found = true;
//                                     break;
//                                 }
//                             }
//                         }
//                         if (!found)
//                         {
//                             switch (reference.GetParameter())
//                             {
//                                 case Parameter.Animation:
//                                     AnimationLogicalProperty anim = new AnimationLogicalProperty();
//                                     anim.Name = reference.Name;
//                                     reference.AssocProperty = anim;
//                                     ReferenceSignalProperties.Add(anim);
//                                     break;
//                                 case Parameter.Clip:
//                                     ClipLogicalProperty clip = new ClipLogicalProperty();
//                                     clip.Name = reference.Name;
//                                     clip.PropertyChanged += (new PropertyChangedEventHandler(Database.Property_PropertyChanged));
//                                     reference.AssocProperty = clip;
//                                     ReferenceSignalProperties.Add(clip);
//                                     break;
//                                 case Parameter.Expressions:
//                                     ExpressionsLogicalProperty expr = new ExpressionsLogicalProperty();
//                                     expr.Name = reference.Name;
//                                     reference.AssocProperty = expr;
//                                     ReferenceSignalProperties.Add(expr);
//                                     break;
//                                 case Parameter.ParameterizedMotion:
//                                     ParameterizedMotionLogicalProperty pm = new ParameterizedMotionLogicalProperty();
//                                     pm.Name = reference.Name;
//                                     reference.AssocProperty = pm;
//                                     ReferenceSignalProperties.Add(pm);
//                                     break;
//                                 case Parameter.Real:
//                                     RealLogicalProperty real = new RealLogicalProperty();
//                                     real.Name = reference.Name;
//                                     reference.AssocProperty = real;
//                                     ReferenceSignalProperties.Add(real);
//                                     break;
//                                 case Parameter.Boolean:
//                                     BooleanLogicalProperty b = new BooleanLogicalProperty();
//                                     b.Name = reference.Name;
//                                     reference.AssocProperty = b;
//                                     ReferenceSignalProperties.Add(b);
//                                     break;
//                             }
//                         }
                        ReferenceSignals.Insert(index, reference);
                        
                    }
                    else
                    {
                        ReferenceSignal reference = signal.CreateReference();
                        reference.SetParent(this);
                        //  ROBM - Not needed anymore
//                         bool found = false;
//                         IEnumerator<IProperty> propEnum = ReferenceSignalProperties.Children.GetEnumerator();
//                         while (propEnum != null && propEnum.MoveNext())
//                         {
//                             if (propEnum.Current is IProperty)
//                             {
//                                 if (reference.Name == (propEnum.Current as IProperty).Name)
//                                 {
//                                     found = true;
//                                     break;
//                                 }
//                             }
//                         }
//                         if (!found)
//                         {
//                             switch (reference.GetParameter())
//                             {
//                                 case Parameter.Animation:
//                                     AnimationLogicalProperty anim = new AnimationLogicalProperty();
//                                     anim.Name = reference.Name;
//                                     reference.AssocProperty = anim;
//                                     ReferenceSignalProperties.Add(anim);
//                                     break;
//                                 case Parameter.Clip:
//                                     ClipLogicalProperty clip = new ClipLogicalProperty();
//                                     clip.Name = reference.Name;
//                                     clip.PropertyChanged += (new PropertyChangedEventHandler(Database.Property_PropertyChanged));
//                                     reference.AssocProperty = clip;
//                                     ReferenceSignalProperties.Add(clip);
//                                     break;
//                                 case Parameter.Expressions:
//                                     ExpressionsLogicalProperty expr = new ExpressionsLogicalProperty();
//                                     expr.Name = reference.Name;
//                                     reference.AssocProperty = expr;
//                                     ReferenceSignalProperties.Add(expr);
//                                     break;
//                                 case Parameter.ParameterizedMotion:
//                                     ParameterizedMotionLogicalProperty pm = new ParameterizedMotionLogicalProperty();
//                                     pm.Name = reference.Name;
//                                     reference.AssocProperty = pm;
//                                     ReferenceSignalProperties.Add(pm);
//                                     break;
//                                 case Parameter.Real:
//                                     RealLogicalProperty real = new RealLogicalProperty();
//                                     real.Name = reference.Name;
//                                     reference.AssocProperty = real;
//                                     ReferenceSignalProperties.Add(real);
//                                     break;
//                                 case Parameter.Boolean:
//                                     BooleanLogicalProperty b = new BooleanLogicalProperty();
//                                     b.Name = reference.Name;
//                                     reference.AssocProperty = b;
//                                     ReferenceSignalProperties.Add(b);
//                                     break;
//                             }
//                         }
                        ReferenceSignals.Add(reference);

                    }
                }
            }

            //  ROBM - Clear the signal properties array
            ReferenceSignalProperties.Clear();

            //  ROBM - Populate the signal properties array
            foreach (ReferenceSignal refSignal in ReferenceSignals)
            {
                switch (refSignal.GetParameter())
                {
                    case Parameter.Animation:
                        AnimationLogicalProperty anim = new AnimationLogicalProperty();
                        anim.Name = refSignal.Name;
                        refSignal.AssocProperty = anim;
                        ReferenceSignalProperties.Add(anim);
                        break;
                    case Parameter.Clip:
                        ClipLogicalProperty clip = new ClipLogicalProperty();
                        clip.Name = refSignal.Name;
                        clip.PropertyChanged += (new PropertyChangedEventHandler(Database.OnProperty_PropertyChanged));
                        refSignal.AssocProperty = clip;
                        ReferenceSignalProperties.Add(clip);
                        break;
                    case Parameter.Expressions:
                        ExpressionsLogicalProperty expr = new ExpressionsLogicalProperty();
                        expr.Name = refSignal.Name;
                        refSignal.AssocProperty = expr;
                        ReferenceSignalProperties.Add(expr);
                        break;
                    case Parameter.ParameterizedMotion:
                        ParameterizedMotionLogicalProperty pm = new ParameterizedMotionLogicalProperty();
                        pm.Name = refSignal.Name;
                        refSignal.AssocProperty = pm;
                        ReferenceSignalProperties.Add(pm);
                        break;
                    case Parameter.Real:
                        RealLogicalProperty real = new RealLogicalProperty();
                        real.Name = refSignal.Name;
                        refSignal.AssocProperty = real;
                        ReferenceSignalProperties.Add(real);
                        break;
                    case Parameter.Boolean:
                        BooleanLogicalProperty b = new BooleanLogicalProperty();
                        b.Name = refSignal.Name;
                        refSignal.AssocProperty = b;
                        ReferenceSignalProperties.Add(b);
                        break;
                }
            }
        }

        internal void UpdateReferenceRequests()
        {
            List<Request> requests = new List<Request>();

            ReferenceRequest[] oldRequests = new ReferenceRequest[ReferenceRequests.Count];
            ReferenceRequests.CopyTo(oldRequests, 0);

            foreach (ReferenceRequest referenced in oldRequests)
            {
                Request found = ReferencedDatabase.Requests.FirstOrDefault(delegate(Request request)
                {
                    return request.Id == referenced.Reference;
                }
                );

                if (found != null)
                {
                    requests.Add(found);
                }
                else
                {
                    ReferenceRequests.Remove(referenced);
                }
            }

            foreach (IReferredRequest request in ReferencedDatabase.Requests.OfType<IReferredRequest>())
            {
                if (!requests.Contains(request as Request))
                {
                    int index = ReferencedDatabase.Requests.IndexOf(request as Request);
                    if (index < ReferenceRequests.Count)
                    {
                        ReferenceRequest reference = request.CreateReference();
                        ReferenceRequests.Insert(index, reference);
                    }
                    else
                    {
                        ReferenceRequest reference = request.CreateReference();
                        ReferenceRequests.Add(reference);
                    }
                }
            }
        }

        internal void UpdateReferenceFlags()
        {
            List<Flag> flags = new List<Flag>();

            ReferenceFlag[] oldFlags = new ReferenceFlag[ReferenceFlags.Count];
            ReferenceFlags.CopyTo(oldFlags, 0);

            foreach (ReferenceFlag referenced in oldFlags)
            {
                Flag found = ReferencedDatabase.Flags.FirstOrDefault(delegate(Flag flag)
                {
                    return flag.Id == referenced.Reference;
                }
                );

                if (found != null)
                {
                    flags.Add(found);
                }
                else
                {
                    ReferenceFlags.Remove(referenced);
                }
            }

            foreach (IReferredFlag flag in ReferencedDatabase.Flags.OfType<IReferredFlag>())
            {
                if (!flags.Contains(flag as Flag))
                {
                    int index = ReferencedDatabase.Flags.IndexOf(flag as Flag);
                    if (index < ReferenceRequests.Count)
                    {
                        ReferenceFlag reference = flag.CreateReference();
                        ReferenceFlags.Insert(index, reference);
                    }
                    else
                    {
                        ReferenceFlag reference = flag.CreateReference();
                        ReferenceFlags.Add(reference);
                    }
                }
            }
        }

        void Reference_ReferenceDatabaseChanged(object sender, EventArgs e)
        {
            ReferenceRequests.CollectionChanged += new NotifyCollectionChangedEventHandler(Requests_CollectionChanged);
            ReferenceFlags.CollectionChanged += new NotifyCollectionChangedEventHandler(Flags_CollectionChanged);
            ReferenceSignals.CollectionChanged += new NotifyCollectionChangedEventHandler(SignalsProperties_CollectionChanged);
            UpdateReferenceSignals();
            UpdateReferenceRequests();
            UpdateReferenceFlags();
        }

        internal void Signals_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (IReferredSignal signal in e.NewItems.OfType<IReferredSignal>())
                    {
                        ReferenceSignal reference = signal.CreateReference();
                        reference.SetParent(this);
                        ReferenceSignals.Add(reference);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Signal signal in e.OldItems)
                    {
                        ReferenceSignal reference = ReferenceSignals.FirstOrDefault(delegate(ReferenceSignal rs)
                            {
                                return rs.Reference == signal.Id;
                            }
                        );
                        if (reference != null)
                        {
                            ReferenceSignals.Remove(reference);
                        }
                    }
                    break;
            }
        }

        internal void SignalsProperties_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            /*
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Signal signal in e.NewItems)
                    {
                        bool found = false;
                        IEnumerator<IProperty> propEnum = ReferenceSignalProperties.Children.GetEnumerator();
                        while (propEnum != null && propEnum.MoveNext())
                        {
                            if (propEnum.Current is IProperty)
                            {
                                if (signal.Name == (propEnum.Current as IProperty).Name)
                                {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            switch (signal.GetParameter())
                            {
                                case Parameter.Animation:
                                    AnimationLogicalProperty anim = new AnimationLogicalProperty();
                                    anim.Name = signal.Name;
                                    ReferenceSignalProperties.Add(anim);
                                    break;
                                case Parameter.Clip:
                                    ClipLogicalProperty clip = new ClipLogicalProperty();
                                    clip.Name = signal.Name;
                                    ReferenceSignalProperties.Add(clip);
                                    break;
                                case Parameter.Expressions:
                                    ExpressionsLogicalProperty expr = new ExpressionsLogicalProperty();
                                    expr.Name = signal.Name;
                                    ReferenceSignalProperties.Add(expr);
                                    break;
                                case Parameter.ParameterizedMotion:
                                    AnimationLogicalProperty pm = new AnimationLogicalProperty();
                                    pm.Name = signal.Name;
                                    ReferenceSignalProperties.Add(pm);
                                    break;
                            }
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Signal signal in e.OldItems)
                    {
                        IEnumerator<IProperty> propEnum = ReferenceSignalProperties.Children.GetEnumerator();
                        while (propEnum != null && propEnum.MoveNext())
                        {
                            if (propEnum.Current is IProperty)
                            {
                                if (signal.Name == (propEnum.Current as IProperty).Name)
                                {
                                    ReferenceSignalProperties.Remove(propEnum.Current as Property);
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }
            */
        }

        internal void Requests_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Request request in e.NewItems)
                    {
                        bool found = false;
                        IEnumerator<IProperty> propEnum = ReferenceRequestProperties.Children.GetEnumerator();
                        while (propEnum != null && propEnum.MoveNext())
                        {
                            if (propEnum.Current is IProperty)
                            {
                                if (request.Name == (propEnum.Current as IProperty).Name)
                                {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            ReferenceRequestProperties.Add();
                            ReferenceRequestProperties.Children.ElementAt(ReferenceRequestProperties.Items.Count() - 1).Name = request.Name;
                            (ReferenceRequestProperties.Children.ElementAt(ReferenceRequestProperties.Items.Count() - 1) as RestorableObjectLogicalProperty).ObjectType = RestorableObjectType.Request;
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Request request in e.OldItems)
                    {
                        IEnumerator<IProperty> propEnum = ReferenceRequestProperties.Children.GetEnumerator();
                        while (propEnum != null && propEnum.MoveNext())
                        {
                            if (propEnum.Current is IProperty)
                            {
                                if (request.Name == (propEnum.Current as IProperty).Name)
                                {
                                    ReferenceRequestProperties.Remove(propEnum.Current as IProperty);
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }
        }

        internal void Flags_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Flag flag in e.NewItems)
                    {
                        bool found = false;
                        IEnumerator<IProperty> propEnum = ReferenceFlagProperties.Children.GetEnumerator();
                        while (propEnum != null && propEnum.MoveNext())
                        {
                            if (propEnum.Current is IProperty)
                            {
                                if (flag.Name == (propEnum.Current as IProperty).Name)
                                {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            ReferenceFlagProperties.Add();
                            ReferenceFlagProperties.Children.ElementAt(ReferenceFlagProperties.Items.Count() - 1).Name = flag.Name;
                            (ReferenceFlagProperties.Children.ElementAt(ReferenceFlagProperties.Items.Count() - 1) as RestorableObjectLogicalProperty).ObjectType = RestorableObjectType.Flag;
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Flag flag in e.OldItems)
                    {
                        IEnumerator<IProperty> propEnum = ReferenceFlagProperties.Children.GetEnumerator();
                        while (propEnum != null && propEnum.MoveNext())
                        {
                            if (propEnum.Current is IProperty)
                            {
                                if (flag.Name == (propEnum.Current as IProperty).Name)
                                {
                                    ReferenceFlagProperties.Remove(propEnum.Current as IProperty);
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }
        }


        void Filename_ValueChanging(object sender, EventArgs e)
        {
            if (Filename != null)
            {
                Filename.PropertyChanged -= new PropertyChangedEventHandler(Filename_PropertyChanged);
            }
        }

        void Filename_ValueChanged(object sender, EventArgs e)
        {
            if (Filename != null)
            {
                Filename.PropertyChanged += new PropertyChangedEventHandler(Filename_PropertyChanged);
            }
        }

        void Filename_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            if (e.PropertyName.Equals("Value"))
            {
                if (ReferencedDatabase != null)
                {
                    Database.References.Remove(ReferencedDatabase);
                    --ReferencedDatabase.RefCount;
                }

                _ReferencedDatabase = null;

                foreach (DatabaseReference database in Database.References)
                {
                    if (database.FullyQualifiedFilename == Filename.Value)
                    {
                        ReferencedDatabase = database;
                        break;
                    }
                }

                if (ReferencedDatabase == null)
                {

                    DatabaseReference database = new DatabaseReference(this.Database.DataModel, this.Database.DataModelService, this.XmlFactory);
                    database.Load(Filename.Value);
                    Database.References.Add(database);
                    ReferencedDatabase = database;
                }

                ++ReferencedDatabase.RefCount;
            }
        }

        public override void Dispose()
        {
            if (ReferencedDatabase != null)
            {
                --ReferencedDatabase.RefCount;
                if (ReferencedDatabase.RefCount == 0)
                {
                    Database.References.Remove(ReferencedDatabase);
                }
            }

            base.Dispose();
        }

        public static class Const
        {
            public static string ReferenceSignalsTag = "ReferenceSignals";
            public static string ReferenceRequestsTag = "ReferenceRequests";
            public static string ReferenceFlagsTag = "ReferenceFlags";
            public static string ReferencedDatabase = "ReferencedDatabase";
            public static string Filename = "Filename";

            public static string ReferenceSignalProperties = "ReferenceSignalProperties";
        }

        public override void GetCustomData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.ReferenceSignalsTag, ReferenceSignals);
            info.AddValue(Const.ReferenceRequestsTag, ReferenceRequests);
            info.AddValue(Const.ReferenceFlagsTag, ReferenceFlags);
        }

        public override void SetCustomData(SerializationInfo info, StreamingContext context)
        {
            ReferencedDatabaseChanging += new EventHandler(Reference_ReferenceDatabaseChanging);
            ReferencedDatabaseChanged += new EventHandler(Reference_ReferenceDatabaseChanged);

            Desc = new ReferenceDesc();
            ReferenceSignals = (NoResetObservableCollection<ReferenceSignal>)info.GetValue(
                Const.ReferenceSignalsTag, typeof(NoResetObservableCollection<ReferenceSignal>));
            ReferenceSignals.CollectionChanged += new NotifyCollectionChangedEventHandler(Signals_CollectionChanged);
            ReferenceRequests = (NoResetObservableCollection<ReferenceRequest>)info.GetValue(
                Const.ReferenceRequestsTag, typeof(NoResetObservableCollection<ReferenceRequest>));
            ReferenceFlags = (NoResetObservableCollection<ReferenceFlag>)info.GetValue(
                Const.ReferenceFlagsTag, typeof(NoResetObservableCollection<ReferenceFlag>));
            Filename = (FilenameLogicalProperty)info.GetValue(Const.Filename, typeof(FilenameLogicalProperty));
            Filename.PropertyChanged += new PropertyChangedEventHandler(Filename_PropertyChanged); 
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Reference"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Reference(XmlReader reader, Database database, Guid parent)
            : this(new ReferenceDesc())
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Name = reader.GetAttribute("Name");
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Filename") == 0)
                {
                    Filename.PropertyChanged -= new PropertyChangedEventHandler(Filename_PropertyChanged);
                    this.Filename = new FilenameLogicalProperty(reader, this.Database, this);
                    Filename.PropertyChanged += new PropertyChangedEventHandler(Filename_PropertyChanged);
                    Filename_PropertyChanged(this.Filename, new PropertyChangedEventArgs("Value"));
                }
                else if (string.CompareOrdinal(reader.Name, "Source") == 0)
                {
                    this.Source = new ResultTransform(reader, this);
                }
                else
                {
                    reader.Read();
                }
            }
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Reference");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Filename != null)
            {
                writer.WriteStartElement("Filename");
                this.Filename.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Source != null)
            {
                writer.WriteStartElement("Source");
                this.Source.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            return ids;
        }
    }
}