﻿using System;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Move.Utils
{

    // event raising pattern from 
    // http://geekswithblogs.net/HouseOfBilz/archive/2009/02/15/re-thinking-c-events.aspx

    public class SimpleEventArgs<T> : EventArgs
    {
        public SimpleEventArgs(T payload) { Payload = payload; }
        public T Payload { get; protected set; }
    }

    public static class EventTriggers
    {
        public static void Raise<T>(this EventHandler<SimpleEventArgs<T>> evt, object sender, T payload)
        {
            if (evt != null) { evt(sender, new SimpleEventArgs<T>(payload)); }
        }

        public static void Raise<T>(this EventHandler<T> evt, object sender, T args) where T: EventArgs
        {
            if (evt != null) { evt(sender, args); }
        }

        public static void Raise(this EventHandler<EventArgs> evt, object sender)
        {
            if (evt != null) { evt(sender, EventArgs.Empty); }
        }

        public static SimpleEventArgs<T> RaiseAndReturn<T>(this EventHandler<SimpleEventArgs<T>> evt, object sender, T payload)
        {
            SimpleEventArgs<T> args = new SimpleEventArgs<T>(payload);
            if (evt != null) { evt(sender, args); }
            return args;
        }

        public static void RaiseOnThread<T>(this EventHandler<SimpleEventArgs<T>> evt, Dispatcher d, object sender, T payload)
        {
            if (evt != null)
            {
                Object[] argArray = new Object[2];
                argArray[0] = sender;
                argArray[1] = new SimpleEventArgs<T>(payload);
                d.BeginInvoke(evt, argArray);
            }
        }

        public static void RaiseOnThread<T>(this EventHandler<T> evt, Dispatcher d, object sender, T args) where T : EventArgs
        {
            if (evt != null)
            {
                Object[] argArray = new Object[2];
                argArray[0] = sender;
                argArray[1] = args;
                d.BeginInvoke(evt, argArray);
            }
        }

        public static void RaiseOnThread(this EventHandler<EventArgs> evt, Dispatcher d, object sender)
        {
            if (evt != null)
            {
                Object[] argArray = new Object[2];
                argArray[0] = sender;
                argArray[1] = EventArgs.Empty;
                d.BeginInvoke(evt, argArray);
            }
        }
    }

    public static class InvokeHelpers
    {
        public static void InvokeAction(this Control widget, Action fn)
        {
            if (!widget.Dispatcher.CheckAccess())
            {
                widget.Dispatcher.Invoke(fn);
            }
            else
            {
                fn();
            }
        }

        public static void BeginInvokeAction(this Control widget, Action fn)
        {
            if (!widget.Dispatcher.CheckAccess())
            {
                widget.Dispatcher.BeginInvoke(fn);
            }
            else
            {
                fn();
            }
        }
    }
}