﻿using System;

namespace Rage.Move
{
    public interface IPlugin
    {
        IPluginHost Host { get; set; }

        string Name { get; }
        string Description { get; }
        string Author { get; }
        string Version { get; }

        void Initialize();
        void Dispose();
    }

    public interface IPluginHost
    {
    }
}