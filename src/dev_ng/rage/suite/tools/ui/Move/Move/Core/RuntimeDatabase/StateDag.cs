﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Rage.Move.Core;

namespace Move.Core
{
    public class StateDagNode
    {
        public RuntimeStateId Id;

        // Does this really need to be a tree? Probably not, but could store
        // links to StateDagNodes instead of RuntimeStateIds here.
        public List<RuntimeStateId> Children;
        public List<RuntimeStateId> Parents;
    }
    
    public class StateDag
    {
        // Purpose: Adds a parent and child to the dag.
        // Returns: the child node that was added/modified - otherwise NULL
        public StateDagNode Add(RuntimeStateId childId, RuntimeStateId parentId)
        {
            StateDagNode parentNode = null;
            StateDagNode childNode = null;

            // Is this child in the list? if not - add it
            if (!AllNodes.TryGetValue(childId, out childNode))
            {
                childNode = new StateDagNode();
                childNode.Id = childId;
                AllNodes[childId] = childNode;
            }
            else
            {
                // look for an early out if we've seen this pair already
                if (childNode.Parents != null)
                {
                    foreach (var parent in childNode.Parents)
                    {
                        if (parent == parentId)
                        {
                            return null;
                        }
                    }
                }
            }

            // Is the parent in the list? if not - add it (and make it a new root node)
            if (!AllNodes.TryGetValue(parentId, out parentNode))
            {
                parentNode = new StateDagNode();
                parentNode.Id = parentId;
                AllNodes[parentId] = parentNode;
                Roots.Add(parentNode);
            }
            
            if (parentNode.Children == null)
            {
                parentNode.Children = new List<RuntimeStateId>();
            }
            parentNode.Children.Add(childId);

            if (childNode.Parents == null)
            {
                childNode.Parents = new List<RuntimeStateId>();
            }
            childNode.Parents.Add(parentId);

            // If the child node was a root node, its not anymore
            if (Roots.Contains(childNode))
            {
                Roots.Remove(childNode);
            }

            return childNode;
        }

        // Purpose; Adds sibling nodes to the dag. One of the two siblings must be in the dag already
        public StateDagNode AddSibling(RuntimeStateId siblingA, RuntimeStateId siblingB)
        {
            StateDagNode sibANode = null;
            StateDagNode sibBNode = null;
            if (AllNodes.TryGetValue(siblingA, out sibANode))
            {
                // sibA exists, try sibB
                if (AllNodes.TryGetValue(siblingB, out sibBNode))
                {
                    // Both exist, make sure they have a common parent.
                    if (sibBNode.Parents.Intersect(sibANode.Parents).Any()) {
                        // ok they have a common parent. Nothing to do
                        return null;
                    }
                    else
                    {
                        throw new InvalidOperationException(String.Format("Found {0} and {1}, but they don't have a common parent", siblingA, siblingB));
                    }
                }
                else
                {
                    Debug.Assert(sibANode != null && sibANode.Parents.Count == 1, "AddSibling only works with strict hierarchies");
                    var parent = sibANode.Parents[0];
                    var parentNode = AllNodes[parent];
                    sibBNode = new StateDagNode();
                    sibBNode.Id = siblingB;
                    sibBNode.Parents = new List<RuntimeStateId>() {parent};
                    parentNode.Children.Add(siblingB);
                    AllNodes[siblingB] = sibBNode;
                    return sibBNode;
                }
            }
            else
            {
                // No sibA, try sibB
                if (AllNodes.TryGetValue(siblingB, out sibBNode))
                {
                    Debug.Assert(sibBNode != null && sibBNode.Parents.Count == 1, "AddSibling only works with strict hierarchies");
                    var parent = sibBNode.Parents[0];
                    var parentNode = AllNodes[parent];
                    sibANode = new StateDagNode();
                    sibANode.Id = siblingB;
                    sibANode.Parents = new List<RuntimeStateId>() { parent };
                    parentNode.Children.Add(siblingA);
                    AllNodes[siblingA] = sibANode;
                    return sibANode;
                }
                else
                {
                    throw new InvalidOperationException(String.Format("Couldn't find {0} or {1} in the StateDag", siblingA, siblingB));
                }
            }
        }

        public void Reset()
        {
            Roots.Clear();
            AllNodes.Clear();
        }

        public IEnumerable<RuntimeStateId> FindChildren(RuntimeStateId id)
        {
            return AllNodes[id].Children ?? Enumerable.Empty<RuntimeStateId>();
        }

        public IEnumerable<RuntimeStateId> FindParents(RuntimeStateId id)
        {
            return AllNodes[id].Parents ?? Enumerable.Empty<RuntimeStateId>();
        }

        public IEnumerable<RuntimeStateId> FindSiblings(RuntimeStateId id)
        {
            return from p in FindParents(id) 
                   from c in FindChildren(p) 
                   select c;
        }

        public List<StateDagNode> Roots = new List<StateDagNode>();
        public Dictionary<RuntimeStateId, StateDagNode> AllNodes = new Dictionary<RuntimeStateId,StateDagNode>();
    }
}
