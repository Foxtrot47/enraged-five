﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Globalization;

namespace New.Move.Core
{
    [Serializable]
    public class IntConditionProperty : IConditionProperty, ISerializable
    {
        public static implicit operator IntConditionProperty(int i)
        {
            IntConditionProperty property = new IntConditionProperty();
            property.Value = i;

            return property;
        }

        public IntConditionProperty()
        {
            Id = Guid.NewGuid();
        }

        public IntConditionProperty(IntConditionProperty other)
        {
            Id = Guid.NewGuid();
            this.Name = other.Name;
            this.Value = other.Value;
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        public int Value { get; set; }
        public Condition Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Value = "Value";
            public static string Parent = "Parent";
        }

        public IntConditionProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Value = (int)info.GetValue(SerializationTag.Value, typeof(int));
            Parent = (Condition)info.GetValue(SerializationTag.Parent, typeof(Condition));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Value, Value);
            info.AddValue(SerializationTag.Parent, Parent);
        }

        public object Clone()
        {
            return new IntConditionProperty(this);
        }
             
        /// <summary>
        /// Initialises a new instance of the <see cref="IntConditionProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public IntConditionProperty(XmlReader reader, Database database, Condition parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Parent = parent;
            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Value = int.Parse(reader.GetAttribute("Value"), CultureInfo.InvariantCulture);
            
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Value", this.Value.ToString(CultureInfo.InvariantCulture));
        }
    }
}