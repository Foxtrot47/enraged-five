﻿using System;

namespace New.Move.Core
{
    public interface ITransformResult
    {
        string Name { get; }
        Guid Id { get; }
    }
}