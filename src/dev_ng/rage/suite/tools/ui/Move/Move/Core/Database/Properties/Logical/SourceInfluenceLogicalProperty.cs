﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class SourceInfluenceLogicalProperty : ILogicProperty, ISerializable
    {
        public SourceInfluenceLogicalProperty()
        {
            Id = Guid.NewGuid();
            _Items = new List<string>();
            _Items.Add("Default");
            _Items.Add("Zero");
            _Items.Add("One");
            _SelectedItem = "Default";
        }

        public SourceInfluenceLogicalProperty(SourceInfluenceLogicalProperty other)
        {
            Name = other.Name;
            Id = Guid.NewGuid();
            _Items = new List<string>();
            _Items.Add("Default");
            _Items.Add("Zero");
            _Items.Add("One");
            _SelectedItem = other._SelectedItem;
        }

        public object Clone()
        {
            return new SourceInfluenceLogicalProperty(this);
        }


        public SourceInfluenceLogicalProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        private List<string> _Items;
        public IEnumerable<string> Items
        {
            get 
            { 
                return _Items; 
            }
            set
            {
                _Items = new List<string>(value);
                OnPropertyChanged("Items");
            }
        }

        private string _SelectedItem;
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public ILogic Parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Items = "Items";
            public static string SelectedItem = "SelectedItem";
        }

        public SourceInfluenceLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Items = (List<string>)info.GetValue(SerializationTag.Items, typeof(List<string>));
            SelectedItem = (string)info.GetValue(SerializationTag.SelectedItem, typeof(string));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);            
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Items, Items);
            info.AddValue(SerializationTag.SelectedItem, SelectedItem);
        }

        public void ExportToXML(PargenXmlNode parentNode, string nodeName)
        {
            if( SelectedItem == "Default")
            {
                parentNode.AppendTextElementNode(nodeName, "kChildInfluenceOverrideDefault");
            }
            else if( SelectedItem == "Zero")
            {
                parentNode.AppendTextElementNode(nodeName, "kChildInfluenceOverrideZero");
            }
            else if( SelectedItem == "One")
            {
                parentNode.AppendTextElementNode(nodeName, "kChildInfluenceOverrideOne");
            }
            else
            {
                parentNode.AppendTextElementNode(nodeName, "");
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SourceInfluenceLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public SourceInfluenceLogicalProperty(XmlReader reader, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.SelectedItem = reader.GetAttribute("SelectedItem");
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", System.Globalization.CultureInfo.InvariantCulture));
            writer.WriteAttributeString("SelectedItem", this.SelectedItem);
        }
    }
}
