﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml;

using Move.Core;
using Move.Core.Restorable;
using Move.Utils;

using RSG.ManagedRage;
using System.Globalization;

namespace New.Move.Core
{
    [Serializable]
    public class LogicInlineSM : LogicParent, ISerializable
    {
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        public const string NameSerializationTag = "Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        public const string IdSerializationTag = "Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Position"/> property.
        /// </summary>
        public const string PositionSerializationTag = "Position";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="ScrollOffset"/> property.
        /// </summary>
        public const string ScrollOffsetSerializationTag = "ScrollOffset";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Zoom"/> property.
        /// </summary>
        public const string ZoomSerializationTag = "Zoom";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Default"/> property.
        /// </summary>
        public const string DefaultSerializationTag = "Default";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Children"/> property.
        /// </summary>
        public const string ChildrenSerializationTag = "Children";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Transitions"/> property.
        /// </summary>
        public const string TransitionsSerializationTag = "Transitions";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="TransitionOrder"/> property.
        /// </summary>
        public const string TransitionOrderSerializationTag = "TransitionOrder";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Default"/> property.
        /// </summary>
        public const string ResultSerializationTag = "Result";

        public PassthroughTransform Result { get; private set; }

        public override ITransitionalDesc Descriptor { get { return LogicInlineSM._Desc; } }

        static LogicInlineSMDesc _Desc = new LogicInlineSMDesc();

        public LogicInlineSM(Point offset)
            : this()
        {
        }

        public LogicInlineSM()
        {
            Result = new PassthroughTransform(this);
            Desc = _Desc;
        }

        public LogicInlineSM(LogicInlineSM other)
            : this()
        {
            this.Database = other.Database;
            this.Enabled = other.Enabled;
            this.Name = other.Name;
            this.AdditionalText = other.AdditionalText;
            this.Position = other.Position;
            this.ScrollOffset = other.ScrollOffset;
            this.Zoom = other.Zoom;

            Dictionary<ITransitional, ITransitional> transitionals = new Dictionary<ITransitional, ITransitional>();
            ITransitional defaultTransitional = null;
            foreach (INode child in other.Children)
            {
                INode childClone = child.Clone() as INode;
                if (childClone is Motiontree)
                {
                    this.Children.Add(childClone);
                }
                else
                {
                    this.Add(childClone);
                }

                if (childClone is ITransitional)
                {
                    transitionals.Add(child as ITransitional, childClone as ITransitional);
                    if (other.Default == child)
                    {
                        defaultTransitional = childClone as ITransitional;
                    }
                }
            }
            this.Default = defaultTransitional;

            foreach (Transition transition in other.Transitions)
            {
                if (transition.Source == null || transition.Dest == null)
                {
                    continue;
                }

                if (!transitionals.ContainsKey(transition.Source) && !transitionals.ContainsKey(transition.Dest))
                {
                    continue;
                }

                ITransitional source = null;
                ITransitional destination = null;

                if (transitionals.ContainsKey(transition.Source))
                {
                    source = transitionals[transition.Source];
                }
                else
                {
                    source = transition.Source;
                }

                if (transitionals.ContainsKey(transition.Dest))
                {
                    destination = transitionals[transition.Dest];
                }
                else
                {
                    destination = transition.Dest;
                }

                New.Move.Core.Transition transitionClone = new New.Move.Core.Transition(transition, source, destination);
                transitionClone.Parent = this;
                this.Transitions.Add(transitionClone);
            }

            // Make sure transition order is kept.
            foreach (KeyValuePair<ITransitional, ITransitional> clonePair in transitionals)
            {
                int orderCount = clonePair.Key.TransitionOrder.Count;
                if (orderCount <= 1)
                {
                    continue;
                }

                string name = null;
                List<Transition> newTransitions = new List<Transition>(clonePair.Value.TransitionOrder);
                clonePair.Value.TransitionOrder.Clear();
                foreach (Transition transition in clonePair.Key.TransitionOrder)
                {
                    name = transition.Name;
                    foreach (Transition newTransition in newTransitions)
                    {
                        if (newTransition.Name != name)
                        {
                            continue;
                        }

                        clonePair.Value.TransitionOrder.Add(newTransition);
                        break;
                    }
                }
            }
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Position = "Position";
            public static string ScrollOffset = "ScrollOffset";
            public static string Zoom = "Zoom";
            public static string Default = "Default";
            public static string Children = "Children";
            public static string Transitions = "Transitions";
            public static string TransitionOrder = "TransitionOrder";
            public static string Result = "Result";
            public static string Enabled = "Enabled";
        }

        public LogicInlineSM(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));

            Parent = (Guid)info.GetValue(SerializationTag.Parent, typeof(Guid));
            
            Position = (Point)info.GetValue(SerializationTag.Position, typeof(Point));
            ScrollOffset = (Point)info.GetValue(SerializationTag.ScrollOffset, typeof(Point));
            Zoom = (double)info.GetValue(SerializationTag.Zoom, typeof(double));
            
            Default = (ITransitional)info.GetValue(SerializationTag.Default, typeof(ITransitional));

            Children = (NoResetObservableCollection<INode>)info.GetValue(SerializationTag.Children, typeof(NoResetObservableCollection<INode>));
            Transitions = (TransitionCollection)info.GetValue(SerializationTag.Transitions, typeof(TransitionCollection));
            Transitions.CollectionChanged += new NotifyCollectionChangedEventHandler(Transitions_CollectionChanged);

            TransitionOrder = (TransitionCollection)info.GetValue(SerializationTag.TransitionOrder, typeof(TransitionCollection));

            Result = (PassthroughTransform)info.GetValue(SerializationTag.Result, typeof(PassthroughTransform));

            try
            {
                Enabled = (bool)info.GetValue(SerializationTag.Enabled, typeof(bool));
            }
            catch (SerializationException)
            {
                Enabled = true;
            }

            Desc = _Desc;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);

            info.AddValue(SerializationTag.Parent, Parent);

            info.AddValue(SerializationTag.Position, Position);
            info.AddValue(SerializationTag.ScrollOffset, ScrollOffset);
            info.AddValue(SerializationTag.Zoom, Zoom);
            
            info.AddValue(SerializationTag.Default, Default);
            
            info.AddValue(SerializationTag.Children, Children);
            info.AddValue(SerializationTag.Transitions, Transitions);

            info.AddValue(SerializationTag.TransitionOrder, TransitionOrder);

            info.AddValue(SerializationTag.Result, Result);

            info.AddValue(SerializationTag.Enabled, Enabled);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
            Motiontree motiontreeParent = (Motiontree)Database.Find(Parent);

            hashedNames.Add(Name);

            var itemNode = parentNode.AppendValueElementNode("Item", "type", "InlineStateMachineNode");

            itemNode.AppendTextElementNode("Name", Name);
            itemNode.AppendTextElementNode("Initial", Default.Name);

            foreach (Connection connection in motiontreeParent.Connections)
            {
                if (connection.Dest == Result)
                {
                    itemNode.AppendTextElementNode("Input", connection.Source.Parent.Name);
                    break;
                }
            }

            var childrenNode = itemNode.AppendChild("Children");
            foreach (var childNode in Children)
                childrenNode.AppendTextElementNode("Item", childNode.Name);

            var transitionsNode = itemNode.AppendChild("Transitions");
            foreach (var transition in TransitionOrder)
                transition.ExportToXML(transitionsNode);
        }

        public override object Clone()
        {
            return new LogicInlineSM(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.LogicInlineSM"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public LogicInlineSM(XmlReader reader, Database database, ITransitional parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }

            this.Parent = parent == null ? Guid.Empty : parent.Id;
            this.Database = database;
            this.Transitions.CollectionChanged -= Transitions_CollectionChanged;
            this.Deserialise(reader, parent);
            this.Transitions.CollectionChanged += Transitions_CollectionChanged;
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("logicinline");

            writer.WriteAttributeString(
                ZoomSerializationTag, this.Zoom.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                IdSerializationTag, this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(NameSerializationTag, this.Name);
            if (this.Default != null)
            {
                writer.WriteAttributeString(
                    DefaultSerializationTag,
                    this.Default.Id.ToString("D", CultureInfo.InvariantCulture));
            }

            if (this.Position != null)
            {
                writer.WriteStartElement(PositionSerializationTag);
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.ScrollOffset != null)
            {
                writer.WriteStartElement(ScrollOffsetSerializationTag);
                writer.WriteAttributeString(
                    "x", this.ScrollOffset.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.ScrollOffset.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            if (this.Result != null)
            {
                writer.WriteStartElement(ResultSerializationTag);
                this.Result.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteStartElement(TransitionsSerializationTag);
            foreach (Transition transistion in this.Transitions)
            {
                transistion.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteStartElement(ChildrenSerializationTag);
            foreach (INode node in this.Children)
            {
                node.Serialise(writer);
            }
            writer.WriteEndElement();

            writer.WriteStartElement(TransitionOrderSerializationTag);
            foreach (Transition transition in this.TransitionOrder)
            {
                writer.WriteStartElement("transition");
                writer.WriteAttributeString(
                    "Id", transition.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, ITransitional parent)
        {
            if (reader == null)
            {
                return;
            }

            this.Zoom = double.Parse(
                reader.GetAttribute(ZoomSerializationTag), CultureInfo.InvariantCulture);
            this.Name = reader.GetAttribute(NameSerializationTag);
            this.Id = Guid.ParseExact(reader.GetAttribute(IdSerializationTag), "D");
            string defaultId = reader.GetAttribute(DefaultSerializationTag);
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            Dictionary<Transition, Guid> sources = new Dictionary<Transition, Guid>();
            Dictionary<Transition, Guid> destinations = new Dictionary<Transition, Guid>();
            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, PositionSerializationTag) == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, ScrollOffsetSerializationTag) == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.ScrollOffset = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, ResultSerializationTag) == 0)
                {
                    this.Result = new PassthroughTransform(reader, this);
                }
                else if (string.CompareOrdinal(reader.Name, TransitionsSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            string sourceId = reader.GetAttribute("Source");
                            string destId = reader.GetAttribute("Destination");

                            Transition newTransition = new Transition(reader, this.Database, this);

                            this.Transitions.Add(newTransition);
                            if (sourceId != null)
                            {
                                sources.Add(newTransition, Guid.Parse(sourceId));
                            }

                            if (destId != null)
                            {
                                destinations.Add(newTransition, Guid.Parse(destId));
                            }
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, ChildrenSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        Dictionary<Guid, INode> map = new Dictionary<Guid, INode>();
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            INode newNode = this.Database.XmlFactory.CreateNode(reader, this.Database, this);
                            if (newNode != null)
                            {
                                map.Add(newNode.Id, newNode);
                                this.Children.Add(newNode);
                            }
                        }

                        reader.Skip();
                        foreach (KeyValuePair<Transition, Guid> kvp in sources)
                        {
                            INode source = null;
                            if (map.TryGetValue(kvp.Value, out source))
                            {
                                kvp.Key.Source = source as ITransitional;
                            }
                        }

                        foreach (KeyValuePair<Transition, Guid> kvp in destinations)
                        {
                            INode destination = null;
                            if (map.TryGetValue(kvp.Value, out destination))
                            {
                                kvp.Key.Dest = destination as ITransitional;
                            }
                        }

                        if (!string.IsNullOrEmpty(defaultId))
                        {
                            INode defaultNode = null;
                            if (map.TryGetValue(Guid.ParseExact(defaultId, "D"), out defaultNode))
                            {
                                this.Default = defaultNode as ITransitional;
                            }
                        }
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else if (string.CompareOrdinal(reader.Name, TransitionOrderSerializationTag) == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            Guid id = Guid.Parse(reader.GetAttribute("Id"));
                            if (parent is ITransitionalParent)
                            {
                                Transition transition = (parent as ITransitionalParent).GetTransition(id);
                                if (transition != null)
                                {
                                    this.TransitionOrder.Add(transition);
                                }
                            }
                            else
                            {
                                transitionOrderResolve.Add(id);
                            }

                            reader.Skip();
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public override void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = Database.GetId(map, this.Id);
            this.Result.Id = New.Move.Core.Database.GetId(map, this.Result.Id);
            foreach (Transition transition in this.Transitions)
            {
                transition.Id = Database.GetId(map, transition.Id);
            }

            foreach (INode child in this.Children)
            {
                child.Parent = this.Id;
                child.ResetIds(map);
            }
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public override List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(Result.Id);
            foreach (Transition transition in this.Transitions)
            {
                ids.Add(transition.Id);
            }

            foreach (INode node in this.Children)
            {
                ids.AddRange(node.GetAllGuids());
            }

            return ids;
        }
    }
}