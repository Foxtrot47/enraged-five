﻿using System;
using System.Collections.Generic;

namespace Move.Core.Restorable
{
    public class RestorableProperty
    {
        public static RestorableProperty Register(string name, Type propertyType, Type ownerType)
        {
            RestorableProperty property = new RestorableProperty();
            property.Name = name;

            Register(ownerType, new RestorableItemTemplate(name, propertyType));

            return property;
        }

        static void Register(Type ownerType, RestorableItemTemplate template)
        {
            List<RestorableItemTemplate> templates = null;
            if (!_Registery.TryGetValue(ownerType, out templates))
            {
                templates = new List<RestorableItemTemplate>();
                _Registery.Add(ownerType, templates);
            }

            templates.Add(template);
        }

        static public void CreateItems(RestorableObject obj)
        {
            CreateItems(obj, obj.GetType());
        }

        static void CreateItems(RestorableObject obj, Type objectType)
        {
            Type baseType = objectType.BaseType;
            if (baseType.IsSubclassOf(typeof(RestorableObject)))
            {
                CreateItems(obj, baseType);
            }

            List<RestorableItemTemplate> templates = null;
            if (_Registery.TryGetValue(objectType, out templates))
            {
                foreach (RestorableItemTemplate template in templates)
                {
                    RestorableItem item = new RestorableItem() { Name = template.Name, Value = GetDefault(template.Type) };
                    item.Parent = obj;

                    obj.Properties.Add(item);
                }
            }
        }

        static object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }

            return null;
        }

        public string Name { get; private set; }

        static Dictionary<Type, List<RestorableItemTemplate>> _Registery = new Dictionary<Type, List<RestorableItemTemplate>>();
    }
}