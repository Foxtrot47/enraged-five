﻿using System;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

using System.Collections.Generic;

using Move.Core;
using Move.Utils;

using RSG.Base.ConfigParser;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class FilterProperty : INotifyPropertyChanged, ISerializable
    {
        public FilterProperty()
        {
        }

        public FilterProperty(FilterProperty other)
        {
            if (other == null)
                return;

            this.Face = other.Face;
            this.Head = other.Head;
            this.LeftElbow = other.LeftElbow;
            this.LeftFoot = other.LeftFoot;
            this.LeftHand = other.LeftHand;
            this.LeftHip = other.LeftHip;
            this.LeftKnee = other.LeftKnee;
            this.LeftShoulder = other.LeftShoulder;
            this.Mover = other.Mover;
            this.Neck = other.Neck;
            this.Pelvis = other.Pelvis;
            this.RightElbow = other.RightElbow;
            this.RightFoot = other.RightFoot;
            this.RightHand = other.RightHand;
            this.RightHip = other.RightHip;
            this.RightKnee = other.RightKnee;
            this.RightShoulder = other.RightShoulder;
            this.Root = other.Root;
            this.Spine = other.Spine;
        }

        static class Const
        {
            public static string Root = "Root";
            public static string RightFoot = "RightFoot";
            public static string LeftFoot = "LeftFoot";
            public static string RightKnee = "RightKnee";
            public static string LeftKnee = "LeftKnee";
            public static string RightHip = "RightHip";
            public static string LeftHip = "LeftHip";
            public static string Pelvis = "Pelvis";
            public static string Spine = "Spine";
            public static string Neck = "Neck";
            public static string Head = "Head";
            public static string Face = "Face";
            public static string RightHand = "RightHand";
            public static string LeftHand = "LeftHand";
            public static string RightElbow = "RightElbow";
            public static string LeftElbow = "LeftElbow";
            public static string RightShoulder = "RightShoulder";
            public static string LeftShoulder = "LeftShoulder";
            public static string Mover = "Mover";
        }

        public float _Root;
        public float Root 
        {
            get
            {
                return _Root;
            }
            set
            {
                if (_Root != value)
                {
                    _Root = value;
                    OnPropertyChanged(Const.Root);
                }
            }
        }

        public float _RightFoot;
        public float RightFoot 
        {
            get
            {
                return _RightFoot;
            }
            set
            {
                if (_RightFoot != value)
                {
                    _RightFoot = value;
                    OnPropertyChanged(Const.RightFoot);
                }
            }
        }

        public float _LeftFoot;
        public float LeftFoot 
        {
            get
            {
                return _LeftFoot;
            }
            set
            {
                if (_LeftFoot != value)
                {
                    _LeftFoot = value;
                    OnPropertyChanged(Const.LeftFoot);
                }
            }
        }

        public float _RightKnee;
        public float RightKnee
        {
            get
            {
                return _RightKnee;
            }
            set
            {
                if (_RightKnee != value)
                {
                    _RightKnee = value;
                    OnPropertyChanged(Const.RightKnee);
                }
            }
        }

        public float _LeftKnee;
        public float LeftKnee
        {
            get
            {
                return _LeftKnee;
            }
            set
            {
                if (_LeftKnee != value)
                {
                    _LeftKnee = value;
                    OnPropertyChanged(Const.LeftKnee);
                }
            }
        }

        public float _RightHip;
        public float RightHip
        {
            get
            {
                return _RightHip;
            }
            set
            {
                if (_RightHip != value)
                {
                    _RightHip = value;
                    OnPropertyChanged(Const.RightHip);
                }
            }
        }

        public float _LeftHip;
        public float LeftHip
        {
            get
            {
                return _LeftHip;
            }
            set
            {
                if (_LeftHip != value)
                {
                    _LeftHip = value;
                    OnPropertyChanged(Const.LeftHip);
                }
            }
        }

        public float _Pelvis;
        public float Pelvis
        {
            get
            {
                return _Pelvis;
            }
            set
            {
                if (_Pelvis != value)
                {
                    _Pelvis = value;
                    OnPropertyChanged(Const.Pelvis);
                }
            }
        }

        public float _Spine;
        public float Spine
        {
            get
            {
                return _Spine;
            }
            set
            {
                if (_Spine != value)
                {
                    _Spine = value;
                    OnPropertyChanged(Const.Spine);
                }
            }
        }

        public float _Neck;
        public float Neck
        {
            get
            {
                return _Neck;
            }
            set
            {
                if (_Neck != value)
                {
                    _Neck = value;
                    OnPropertyChanged(Const.Neck);
                }
            }
        }

        public float _Head;
        public float Head
        {
            get
            {
                return _Head;
            }
            set
            {
                if (_Head != value)
                {
                    _Head = value;
                    OnPropertyChanged(Const.Head);
                }
            }
        }

        public float _Face;
        public float Face
        {
            get
            {
                return _Face;
            }
            set
            {
                if (_Face != value)
                {
                    _Face = value;
                    OnPropertyChanged(Const.Face);
                }
            }
        }

        public float _RightHand;
        public float RightHand
        {
            get
            {
                return _RightHand;
            }
            set
            {
                if (_RightHand != value)
                {
                    _RightHand = value;
                    OnPropertyChanged(Const.RightHand);
                }
            }
        }

        public float _LeftHand;
        public float LeftHand
        {
            get
            {
                return _LeftHand;
            }
            set
            {
                if (_LeftHand != value)
                {
                    _LeftHand = value;
                    OnPropertyChanged(Const.LeftHand);
                }
            }
        }

        public float _RightElbow;
        public float RightElbow
        {
            get
            {
                return _RightElbow;
            }
            set
            {
                if (_RightElbow != value)
                {
                    _RightElbow = value;
                    OnPropertyChanged(Const.RightElbow);
                }
            }
        }

        public float _LeftElbow;
        public float LeftElbow
        {
            get
            {
                return _LeftElbow;
            }
            set
            {
                if (_LeftElbow != value)
                {
                    _LeftElbow = value;
                    OnPropertyChanged(Const.LeftElbow);
                }
            }
        }

        public float _RightShoulder;
        public float RightShoulder
        {
            get
            {
                return _RightShoulder;
            }
            set
            {
                if (_RightShoulder != value)
                {
                    _RightShoulder = value;
                    OnPropertyChanged(Const.RightShoulder);
                }
            }
        }

        public float _LeftShoulder;
        public float LeftShoulder
        {
            get
            {
                return _LeftShoulder;
            }
            set
            {
                if (_LeftShoulder != value)
                {
                    _LeftShoulder = value;
                    OnPropertyChanged(Const.LeftShoulder);
                }
            }
        }

        public float _Mover;
        public float Mover
        {
            get
            {
                return _Mover;
            }
            set
            {
                if (_Mover != value)
                {
                    _Mover = value;
                    OnPropertyChanged(Const.Mover);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public FilterProperty(SerializationInfo info, StreamingContext context)
        {
            Root = (float)info.GetValue(Const.Root, typeof(float));
            RightFoot = (float)info.GetValue(Const.RightFoot, typeof(float));
            LeftFoot = (float)info.GetValue(Const.LeftFoot, typeof(float));
            RightKnee = (float)info.GetValue(Const.RightKnee, typeof(float));
            LeftKnee = (float)info.GetValue(Const.LeftKnee, typeof(float));
            RightHip = (float)info.GetValue(Const.RightHip, typeof(float));
            LeftHip = (float)info.GetValue(Const.LeftHip, typeof(float));
            Pelvis = (float)info.GetValue(Const.Pelvis, typeof(float));
            Spine = (float)info.GetValue(Const.Spine, typeof(float));
            Neck = (float)info.GetValue(Const.Neck, typeof(float));
            Head = (float)info.GetValue(Const.Head, typeof(float));
            Face = (float)info.GetValue(Const.Face, typeof(float));
            RightHand = (float)info.GetValue(Const.RightHand, typeof(float));
            LeftHand = (float)info.GetValue(Const.LeftHand, typeof(float));
            RightElbow = (float)info.GetValue(Const.RightElbow, typeof(float));
            LeftElbow = (float)info.GetValue(Const.LeftElbow, typeof(float));
            RightShoulder = (float)info.GetValue(Const.RightShoulder, typeof(float));
            LeftShoulder = (float)info.GetValue(Const.LeftShoulder, typeof(float));
            Mover = (float)info.GetValue(Const.Mover, typeof(float));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Const.Root, Root);
            info.AddValue(Const.RightFoot, RightFoot);
            info.AddValue(Const.LeftFoot, LeftFoot);
            info.AddValue(Const.RightKnee, RightKnee);
            info.AddValue(Const.LeftKnee, LeftKnee);
            info.AddValue(Const.RightHip, RightHip);
            info.AddValue(Const.LeftHip, LeftHip);
            info.AddValue(Const.Pelvis, Pelvis);
            info.AddValue(Const.Spine, Spine);
            info.AddValue(Const.Neck, Neck);
            info.AddValue(Const.Head, Head);
            info.AddValue(Const.Face, Face);
            info.AddValue(Const.RightHand, RightHand);
            info.AddValue(Const.LeftHand, LeftHand);
            info.AddValue(Const.RightElbow, RightElbow);
            info.AddValue(Const.LeftElbow, LeftElbow);
            info.AddValue(Const.RightShoulder, RightShoulder);
            info.AddValue(Const.LeftShoulder, LeftShoulder);
            info.AddValue(Const.Mover, Mover);
        }
    }

    [Serializable]
    public class FilterLogicalProperty : Property, ISerializable
    {
        public FilterLogicalProperty()
        {
            Flags = PropertyInput.Filter | PropertyInput.Value | PropertyInput.Invalid;
            Input = PropertyInput.Invalid;
            Value = new FilterProperty();

            Id = Guid.NewGuid();

            Connections = new NoResetObservableCollection<Connection>();

            LoadFilters();
        }

        public FilterLogicalProperty(FilterLogicalProperty other)
        {
            Flags = other.Flags;
            Input = other.Input;
            Value = new FilterProperty();
            _IsDynamic = other.IsDynamic;
            _OutputEnabled = other._OutputEnabled;
            _SelectedEvent = other.SelectedEvent;
            
            if (other.FilterDictionary != null)
                this.FilterDictionary = other.FilterDictionary;

            this.FilterName = other.FilterName;


            this.Name = other.Name;
            this.OutputEnabled = other.OutputEnabled;
            this.SelectedEvent = other.SelectedEvent;
            this.Value = new FilterProperty(other.Value);

            Id = Guid.NewGuid();

            Connections = new NoResetObservableCollection<Connection>();

            LoadFilters();
        }

        public FilterLogicalProperty(New.Move.Core.ILogic parent)
        {
            Parent = parent;
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public object Tag { get; set; }

        public PropertyInput Flags { get; private set; }
        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }

        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        bool _IsDynamic = false;
        public bool IsDynamic
        {
            get { return _IsDynamic; }
            set
            {
                if (_IsDynamic != value)
                {
                    _IsDynamic = value;
                    OnPropertyChanged("IsDynamic");
                }
            }
        }

        public IEnumerable<string> FilterDictionaries { get; private set; }
        public string FilterDictionary
        {
            get 
            {
                return filterDictionary_;
            }
            set 
            { 
                filterDictionary_ = value;
                if (value != null)
                {
                    FilterArchiveReader filterArchiveReader = new FilterArchiveReader();
                    FilterNames = filterArchiveReader.ReadFilterNamesFromDictionary(filterDictionary_);
                }
                else
                {
                    FilterNames = new List<string>();
                }
                OnPropertyChanged("FilterDictionary");
                OnPropertyChanged("FilterNames");
            }
        }

        public IEnumerable<string> FilterNames { get; private set; }
        public string FilterName
        {
            get
            {
                return filterName_;
            }
            set
            {
                filterName_ = value;
                OnPropertyChanged("FilterName");
            }
        }

        private string filterDictionary_;
        private string filterName_;

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            if (_Input != PropertyInput.Filter)
            {
                from.RemoveConnectionsFromAnchor(this);
            }
        }

        public New.Move.Core.ILogic Parent { get; set; }

        public FilterProperty Value { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Input = "Input";
            public static string OutputEnabled = "OutputEnabled";
            public static string Flags = "Flags";
            public static string Value = "Value";
            public static string SelectedEvent = "SelectedEvent";
            public static string FilterDictionary = "FilterDictionary";
            public static string FilterName = "FilterName";

            public static string FilterTemplate = "FilterTemplate";// deprecated data
        }

        public FilterLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            LoadFilters();

            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));

            _Input = (PropertyInput)info.GetValue(SerializationTag.Input, typeof(PropertyInput));
            try
            {
                _OutputEnabled = info.GetBoolean(SerializationTag.OutputEnabled);
                _SelectedEvent = (Signal)info.GetValue(SerializationTag.SelectedEvent, typeof(Signal));
            }
            catch (Exception)
            {
            }
            Flags = (PropertyInput)info.GetValue(SerializationTag.Flags, typeof(PropertyInput));

            switch (_Input)
            {
                case PropertyInput.Value:
                    try
                    {
                        // try to load data in the new format
                        FilterDictionary = info.GetString(SerializationTag.FilterDictionary);
                        FilterName = info.GetString(SerializationTag.FilterName);
                    }
                    catch (SerializationException)
                    {
                        // fall back on old data
                        string filterTemplate = info.GetString(SerializationTag.FilterTemplate);
                        string filterTemplateWithoutExtensionLower = filterTemplate.Replace(".xml", "").ToLower();

                        FilterArchiveReader filterArchiveReader = new FilterArchiveReader();
                        IEnumerable<string> playerFilters = filterArchiveReader.ReadFilterNamesFromDictionary("player");

                        string matchingDictionaryFilter = playerFilters.Where(filter => filter.ToLower() == filterTemplateWithoutExtensionLower).FirstOrDefault();
                        if (matchingDictionaryFilter != null)
                        {
                            FilterDictionary = "player";
                            FilterName = matchingDictionaryFilter;
                        }
                    }
                    break;
            }
        }

        private void LoadFilters()
        {
            FilterArchiveReader filterArchiveReader = new FilterArchiveReader();
            FilterDictionaries = filterArchiveReader.ReadFilterDictionaryNames();
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);

            info.AddValue(SerializationTag.Input, _Input);
            info.AddValue(SerializationTag.OutputEnabled, _OutputEnabled);
            info.AddValue(SerializationTag.Flags, Flags);
            info.AddValue(SerializationTag.SelectedEvent, _SelectedEvent);

            switch (_Input)
            {
                case PropertyInput.Value:
                    try
                    {
                        info.AddValue(SerializationTag.FilterDictionary, FilterDictionary);
                        info.AddValue(SerializationTag.FilterName, FilterName);
                    }
                    catch (Exception)
                    {

                    }
                    break;
            }
        }

        public void ExportToXML(PargenXmlNode parentNode, Motiontree motiontreeParent)
        {
            switch (Input)
            {
                case PropertyInput.Filter:
                    if (motiontreeParent == null)
                        throw new ArgumentNullException("motiontreeParent", "Cannot export filter as parameter unless it has a motiontree parent.");

                    parentNode.AppendTextElementNode("Type", "kFilterParameter");

                    foreach (Connection connection in motiontreeParent.Connections)
                    {
                        if (connection.Dest == this)
                        {
                            parentNode.AppendTextElementNode("Parameter", connection.Source.Parent.Name);
                            break;
                        }
                    }
                    break;
                case PropertyInput.Value:
                    parentNode.AppendTextElementNode("Type", "kFilterFilename");
                    parentNode.AppendTextElementNode("FilterContext", "FilterDictionary");
                    parentNode.AppendTextElementNode("FilterDictionary", FilterDictionary);
                    parentNode.AppendTextElementNode("FilterName", FilterName);
                    break;
                case PropertyInput.Invalid:
                    parentNode.AppendTextElementNode("Type", "kFilterIgnored");
                    break;
            }  
        }

        public object Clone()
        {
            return new FilterLogicalProperty(this);
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> is parameter is null.
        /// </exception>
        public FilterLogicalProperty(XmlReader reader, Database database, ILogic parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            Flags = PropertyInput.Filter | PropertyInput.Value | PropertyInput.Invalid;
            this.LoadFilters();
            this.Parent = parent;
            this.Deserialise(reader, database);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "Input", ((uint)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "OutputEnabled", this.OutputEnabled.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "IsDynamic", this.IsDynamic.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "Dictionary", this.FilterDictionary);
            writer.WriteAttributeString(
                "Filter", this.FilterName);
            

            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString(
                    "Signal", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedEvent.Name);
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, Database database)
        {
            if (reader == null)
            {
                return;
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Input = (PropertyInput)(uint.Parse(reader.GetAttribute("Input")));
            this.OutputEnabled = bool.Parse(reader.GetAttribute("OutputEnabled"));
            this.IsDynamic = bool.Parse(reader.GetAttribute("IsDynamic"));
            string filterDictionary = reader.GetAttribute("Dictionary");
            if (!string.IsNullOrEmpty(filterDictionary))
            {
                this.FilterDictionary = filterDictionary;
            }

            string filterName = reader.GetAttribute("Filter");
            if (!string.IsNullOrEmpty(filterName))
            {
                this.FilterName = filterName;
            }

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedEvent = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedEvent == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedEvent = database.GetSignal(signalName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}' inside the '{1}' node. This needs fixing before a export is allowed.", signalId, this.Parent.Name);
                    }
                }
            }

            reader.Skip();
        }
    }
}
