﻿namespace New.Move.Core
{
    public interface IReferredSignal
    {
        ReferenceSignal CreateReference();
    }
}