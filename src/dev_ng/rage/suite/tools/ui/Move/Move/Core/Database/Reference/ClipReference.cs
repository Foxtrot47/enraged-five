﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class ClipReference : ReferenceSignal, ISerializable
    {
        public ClipReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public ClipReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override Parameter GetParameter()
        {
            return Parameter.Clip;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}