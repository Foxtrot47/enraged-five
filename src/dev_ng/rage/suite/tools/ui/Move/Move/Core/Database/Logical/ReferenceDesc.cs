﻿using System;
using System.Windows.Media;

namespace New.Move.Core
{
    public class ReferenceDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Reference";
            public static int Id = 31;
            public static string Help = "";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return null; } }
        public ImageSource Icon { get { return null; } }

        public Type ConstructType { get { return typeof(Reference); } }

        public INode Create(Database database)
        {
            return new Reference(this) { Name = Const.Name };
        }
    }
}