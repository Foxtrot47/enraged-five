﻿using System;

namespace New.Move.Core
{
    public class DefaultChangedEventArgs : EventArgs
    {
        public DefaultChangedEventArgs(bool isDefault)
        {
            IsDefault = isDefault;
        }

        public bool IsDefault { get; private set; }
    }
}