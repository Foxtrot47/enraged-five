﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Windows.Threading;

using Move.Core;
using Move.Utils;
using New.Move.Core;
using Rage.Move.Utils;

namespace Rage.Move.Core
{
    public delegate void ClientConnectedEventHandler(Client client);
    public delegate void ClientDisconnectedEventHandler(Client client);
    public delegate void ClientBufferCompleteEventHandler(ByteBuffer buffer);

    public delegate void ClientDatabaseAttachmentEventHandler(Database db);
 
    public class Client
    {
        #region ClientPacket Class
        public class ClientPacket
        {
            private const int PacketBufferSize = 1024;

            public ClientPacket(Socket s)
            {
                Socket = s;
                PacketSize = -1;
                BytesRead = 0;
            }

            public Socket Socket;
            public int PacketSize { get; set; }
            public int BytesRead { get; set; }

            public byte[] recvBuffer = new byte[PacketBufferSize];
        }
        #endregion

        #region LinkedDatabase class
        public class LinkedDatabase
        {
            public LinkedDatabase(Database db, ClientBufferCompleteEventHandler eh)
            {
                Database = db;
                BufferHandler = eh;
            }

            public Database Database { get; protected set; }
            public ClientBufferCompleteEventHandler BufferHandler { get; protected set; }
        }
        #endregion

        #region DebugSession class
        public class DebugSession : Rockstar.MoVE.Services.IDebugSession
        {
            protected DebugSession(Database db, RuntimeDatabase rdb, Link link)
            {
                Database = db;
                RuntimeDb = rdb;
                Link = link;
            }

            public Database Database { get; protected set; }
            public RuntimeDatabase RuntimeDb { get; protected set; }
            public Link Link { get; protected set; }

            public static DebugSession Create(Database db, StartDebuggingNetworkMessage msg, Link link)
            {
                RuntimeDatabase rdb = new RuntimeDatabase(msg);
                rdb.ConnectTo(db, link);
                return new DebugSession(db, rdb, link);
            }

            public void Stop()
            {
                RuntimeDb.Disconnect();
            }
        }
        #endregion

        public event EventHandler<EventArgs> Connect;
        public event EventHandler<EventArgs> Disconnect;

        public event EventHandler<SimpleEventArgs<ByteBuffer>> BufferComplete;

        public event EventHandler<SimpleEventArgs<Database>> DatabaseAttached;
        public event EventHandler<SimpleEventArgs<Database>> DatabaseDetached;

        public event EventHandler<SimpleEventArgs<DebugSession>> StartDebugging;
        public event EventHandler<SimpleEventArgs<DebugSession>> StopDebugging;

        IPAddress address = IPAddress.Any;
        int port = 16000;
        
        private Socket _clientSocket;

        private ObservableCollection<ByteBuffer> _completedBuffers = new ObservableCollection<ByteBuffer>();

        private Dictionary<uint, LinkedDatabase> _completedBufferHandlers = new Dictionary<uint, LinkedDatabase>();

        private Dictionary<int, NetworkMessageBase.RequestResponse> _responders = new Dictionary<int, NetworkMessageBase.RequestResponse>();

        private DebugSession CurrentDebugSession;

        public Client()
        {
            _completedBuffers.CollectionChanged +=
               new NotifyCollectionChangedEventHandler(CompletedBuffers_CollectionChanged);
        }

        public string Address
        {
            get { return address.ToString(); }
            set
            {
                address = Dns.GetHostAddresses(value)[0];
            }
        }

        public int Port
        {
            get { return this.port; }
            set { this.port = value; }
        }

        public bool IsConnected
        {
            get 
            {
                if (_clientSocket != null)
                    return _clientSocket.Connected;
                else
                    return false;
            }
        }

        public void Start()
        {
            IPEndPoint serverEndPoint = new IPEndPoint(this.address, this.port);
            _clientSocket = new Socket(serverEndPoint.Address.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            _clientSocket.BeginConnect(serverEndPoint, new AsyncCallback(ConnectCallback), null);
        }
        
        public void Stop()
        {
            DisconnectClient();
        }

        public void DisconnectClient()
        {
            _clientSocket.Disconnect(false);
            Disconnect.Raise(this, EventArgs.Empty);
        }

        void CompletedBuffers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach(ByteBuffer bb in e.NewItems)
                {
                    //Fire off the buffer completed event for any listeners that want access to the raw
                    //unprocessed buffers
                    BufferComplete.Raise(this, bb);

                    uint msgId = bb.PeekReadUInt32(NetworkMessageBase.BufferOffset_MessageType);
                    if (msgId < (uint)NetworkMessageId.LINK_MSG_HANDLER_START)
                    {
                        //Process the message directly
                        ProcessClientMessage(bb);
                    }
                    else
                    {
                        //Route the message to the correct network handler
                        uint networkInstanceId = bb.PeekReadUInt32(NetworkMessageBase.BufferOffset_NetworkInstanceId);
                        if (_completedBufferHandlers.ContainsKey(networkInstanceId))
                        {
                            _completedBufferHandlers[networkInstanceId].BufferHandler.Invoke(bb);
                        }
                    }
               
                    lock(_completedBuffers) _completedBuffers.Remove(bb);
                }
            }
        }

        void ConnectCallback(IAsyncResult result)
        {
            try
            {
                _clientSocket.EndConnect(result);

                Connect.Raise(this, EventArgs.Empty);

                ClientPacket packet = new ClientPacket(_clientSocket);

                _clientSocket.BeginReceive(packet.recvBuffer, 0, 4, SocketFlags.None,
                                        ReceiveCallback, packet);
            }
            catch (SocketException ex)
            {
            	//The connection failed...
                //TODO : Inform the user that the connection could not be established
                Console.WriteLine(ex.Message);
            }
        }

        void ReceiveCallback(IAsyncResult result)
        {
            SocketError sErr;

            try
            {
                ClientPacket packet = (ClientPacket)result.AsyncState;
                int bytesRead = packet.Socket.EndReceive(result, out sErr);

                if (0 != bytesRead && sErr == SocketError.Success)
                {
                    if (packet.PacketSize == -1)
                    {
                        //Starting a new packet.  So with this receive we have pulled down the first four bytes
                        //of the packet which indicates the total size of the packet we'll need to read
                        packet.PacketSize = BitConverter.ToInt32(packet.recvBuffer, 0) + 4;
                        packet.BytesRead = 4;

                        _clientSocket.BeginReceive(packet.recvBuffer, bytesRead, packet.PacketSize - packet.BytesRead,
                            SocketFlags.None, ReceiveCallback, packet);
                    }
                    else
                    {
                        packet.BytesRead += bytesRead;

                        if (packet.BytesRead == packet.PacketSize)
                        {
                            //This read was the last required read to complete the current packet
                            //so create a ByteBuffer representation of the packet data and pass it off
                            //for processing
                            ByteBuffer bb = new ByteBuffer((uint)packet.PacketSize);
                            bb.Length = packet.PacketSize;
                            Buffer.BlockCopy(packet.recvBuffer, 0, bb.Storage, 0, packet.PacketSize);
                            
                            lock(_completedBuffers)
                            {
                                _completedBuffers.Add(bb);
                            }
                           
                            //Start a new receive
                            ClientPacket nextPacket = new ClientPacket(_clientSocket);
                            _clientSocket.BeginReceive(nextPacket.recvBuffer, 0, 4, SocketFlags.None,
                                                    ReceiveCallback, nextPacket);
                        }
                        else
                        {
                            //There is still data left to read to complete the packet
                            _clientSocket.BeginReceive(packet.recvBuffer, packet.BytesRead, packet.PacketSize - packet.BytesRead,
                                SocketFlags.None, ReceiveCallback, packet);
                        }
                    }
                }
                else
                {
                    //The client disconnected
                    //Disconnect();
                }
            }
            catch (SocketException)
            {
                DisconnectClient();
            }
            
        }

        public bool Send(ByteBuffer buffer)
        {
            if (IsConnected)
            {
                _clientSocket.Send(buffer.Storage, buffer.Length, SocketFlags.None);
            }

            return true;
        }

        private void ProcessClientMessage(ByteBuffer bb)
        {
            NetworkMessageId msgId = (NetworkMessageId)bb.PeekReadUInt32(NetworkMessageBase.BufferOffset_MessageType);

            switch (msgId)
            {
                case NetworkMessageId.MSG_SYSINFO:
                    {
                        SystemInfoNetworkMessage msg = new SystemInfoNetworkMessage();
                        msg.Decode(bb);
                        ProcessSystemInfoMessage(msg);
                        break;
                    }
                case NetworkMessageId.MSG_NETWORK_INSTANCES:
                    {
                        int responderId = bb.PeekReadInt32(NetworkMessageBase.BufferOffset_ResponderId);
                        NetworkInstancesMessage msg = new NetworkInstancesMessage();
                        msg.Decode(bb);
                        ProcessResponse(msg, responderId);
                        break;
                    }
                case NetworkMessageId.MSG_DISCONNECT:
                    {
                        int responderId = bb.PeekReadInt32(NetworkMessageBase.BufferOffset_ResponderId);
                        DisconnectMessage msg = new DisconnectMessage();
                        msg.Decode(bb);
                        ProcessResponse(msg, responderId);
                        break;
                    }
            }
        }

        private void ProcessResponse(NetworkMessageBase msg, int responderId)
        {
            if(_responders.ContainsKey(responderId))
            {
                _responders[responderId](msg);

                lock(_responders) _responders.Remove(responderId);
            }
        }

        public void RequestActiveNetworks(NetworkMessageBase.RequestResponse responder)
        {
            int responderId = responder.GetHashCode();

            if(_responders.ContainsKey(responderId))
            {
                throw new ApplicationException(String.Format("RequestActiveNetworks : Responder Id of {0} already exists", responderId));
            }

            lock(_responders) _responders.Add(responderId, responder);

            ByteBuffer bb = NetworkInstancesMessage.EncodeRequest(responderId);
            Send(bb);
        }

        public void RequestDisconnect(NetworkMessageBase.RequestResponse responder)
        {
            int responderId = responder.GetHashCode();
            if (_responders.ContainsKey(responderId))
            {
                throw new ApplicationException(String.Format("RequestDisconnect: Responder Id of {0} already exists", responderId));
            }
            lock (_responders) _responders.Add(responderId, responder);
            ByteBuffer bb = DisconnectMessage.EncodeRequest(responderId);
            Send(bb);
        }

        public void Link_OnStartDebugging(object sender, SimpleEventArgs<LinkNetworkMessageBase> e)
        {
            StartDebuggingNetworkMessage msg = (StartDebuggingNetworkMessage)e.Payload;

            Link link = (Link)sender;
            Database db = ((Link)sender).Database;
            if (msg.Confirmation)
            {
                if (CurrentDebugSession != null)
                {
                    StopDebugging.Raise(this, CurrentDebugSession);
                    CurrentDebugSession.Stop();
                    CurrentDebugSession = null;
                }

                // Set up a new debugsession
                CurrentDebugSession = DebugSession.Create(db, msg, link);

                // And let the world know
                StartDebugging.Raise(this, CurrentDebugSession);
            }
            else
            {
                Debug.Assert(db == _completedBufferHandlers[msg.NetworkInstanceId].Database);
                Debug.Assert(link == _completedBufferHandlers[msg.NetworkInstanceId].Database.Link);

                //Detach the start/stop event handlers
                link.StartDebugging -= Link_OnStartDebugging;
                link.StopDebugging -= Link_OnStopDebugging;

                //Invoke the database detached event
                DatabaseDetached.Raise(this, db);

                //The runtime refused to start sending debugging information so stop trying to route messages
                //to it
                lock(_completedBufferHandlers) _completedBufferHandlers.Remove(msg.NetworkInstanceId);
            }
        }

        public void AttachToDatabase(Database db, uint networkInstance)
        {
            //Setup the handler to route messages for this network link
            lock(_completedBufferHandlers) _completedBufferHandlers[networkInstance] = new LinkedDatabase(db, db.Link.BufferCompleteEventHandler);

            //Attach to the start/stop debugging events from the network link
            db.Link.StartDebugging += Link_OnStartDebugging;
            db.Link.StopDebugging += Link_OnStopDebugging;
          
            //Tell the runtime to start sending debug data
            ByteBuffer bb = StartDebuggingNetworkMessage.EncodeRequest(0, networkInstance);
            Send(bb);

            //Invoke the database attached event
            DatabaseAttached.Raise(this, db);
        }

        public void AttachWithoutDatabase(uint networkInstance)
        {
            Link freeLink = new Link(null);

            //Setup the handler to route messages for this network link
            lock (_completedBufferHandlers) _completedBufferHandlers[networkInstance] = new LinkedDatabase(null, freeLink.BufferCompleteEventHandler);

            //Attach to the start/stop debugging events from the network link
            freeLink.StartDebugging += Link_OnStartDebugging;
            freeLink.StopDebugging += Link_OnStopDebugging;

            //Tell the runtime to start sending debug data
            ByteBuffer bb = StartDebuggingNetworkMessage.EncodeRequest(0, networkInstance);
            Send(bb);

            //Invoke the database attached event
            DatabaseAttached.Raise<SimpleEventArgs<Database>>(this, null);

        }

        public void Link_OnStopDebugging(object sender, SimpleEventArgs<LinkNetworkMessageBase> e)
        {
            //The stop debugging response came back from the runtime, so stop routing messages
            //to it
            StopDebuggingNetworkMessage msg = (StopDebuggingNetworkMessage)e.Payload;

            Link link = (Link)sender;
            Database db = link.Database;

            if (CurrentDebugSession != null)
            {
                StopDebugging.Raise(this, CurrentDebugSession);
                CurrentDebugSession.Stop();
                CurrentDebugSession = null;
            }

            //Detach the start/stop event handlers
            link.StartDebugging -= Link_OnStartDebugging;
            link.StopDebugging -= Link_OnStopDebugging;

            DatabaseDetached.Raise(this, db);
            
            //Remove the handler to route messages for this network link
            lock(_completedBufferHandlers) _completedBufferHandlers.Remove(msg.NetworkInstanceId);            
        }

#if disabled
        public void DetatchFromDatabase(Database db)
        {
            if(_completedBufferHandlers.ContainsKey(db.Id))
            {
                //Tell the runtime to stop sending debug data
                ByteBuffer bb = StopDebuggingNetworkMessage.EncodeRequest(0, db.Id);
                Send(bb);

                //Invoke the database detached event
                DatabaseDetached.Raise(this, db);
            }
        }
#endif

        private void ProcessSystemInfoMessage(SystemInfoNetworkMessage msg)
        {
            Console.WriteLine("System Info : Version = {0}", msg.Version);
        }
    }


    //public class Client
    //{
    //    string ip = "0.0.0.0";
    //    IPAddress address = IPAddress.Any;
    //    int port = 16000;
    //    TcpClient client = null;
    //    bool connected = false;
    //    uint bytesToRead = 0;
    //    ByteBuffer packet = null;
    //    ByteBuffer ping = new ByteBuffer(8);

    //    DispatcherTimer _UpdateTimer = new DispatcherTimer();
    //    DispatcherTimer _PingTimer = new DispatcherTimer();

    //    public Client()
    //    {
    //        this.ping.WriteUInt32(4);
    //        this.ping.WriteUInt32(0);

    //        _UpdateTimer.Interval = new TimeSpan(16);
    //        _UpdateTimer.Tick += new EventHandler(Update);
    //        //_UpdateTimer.Elapsed += new ElapsedEventHandler(Update);

    //        _PingTimer.Interval = new TimeSpan(0, 0, 1);
    //        _PingTimer.Tick += new EventHandler(Tick);
    //    }

    //    public string Address
    //    {
    //        get { return address.ToString(); }
    //        set
    //        {
    //            IPAddress addr = new IPAddress(address.GetAddressBytes());
    //            if (IPAddress.TryParse(value, out addr))
    //            {
    //                ip = addr.ToString();
    //                address = addr;
    //            }
    //        }
    //    }

    //    public int Port
    //    {
    //        get { return this.port; }
    //        set { this.port = value; }
    //    }

    //    public bool IsConnected
    //    {
    //        get { return this.connected; }
    //    }

    //    public event ClientConnectedEventHandler OnConnect = null;
    //    public event ClientDisconnectedEventHandler OnDisconnect = null;
    //    public event ClientBufferCompleteEventHandler OnBufferComplete = null;

    //    public void Start()
    //    {
    //        if (this.client != null)
    //            Disconnect();

    //        this.client = new TcpClient();
    //        try
    //        {
    //            this.client = new TcpClient();
    //            this.client.Connect(new IPEndPoint(this.address, this.port));
    //            if (this.client.Connected)
    //                Connect();
    //            else
    //                Disconnect();
    //        }
    //        catch
    //        {
    //            Disconnect();
    //        }
    //    }

    //    public void Stop()
    //    {
    //        Disconnect();
    //    }

    //    public void Update(object sender, EventArgs e)
    //    {
    //        if (!this.connected) return;

    //        if (this.client.GetStream() != null && this.client.GetStream().DataAvailable)
    //        {
    //            if (this.packet == null)
    //            {
    //                //Console.WriteLine("Start packet");
    //                StartPacket();
    //                ContinuePacket();
    //            }
    //            else
    //            {
    //                //Console.WriteLine("Continue packet");
    //                ContinuePacket();
    //            }
    //        }

    //        if (packet != null && packet.IsReady)
    //        {
    //            //Console.WriteLine("Process packet");
    //            Process();
    //        }
    //    }

    //    void Tick(object sender, EventArgs e)
    //    {
    //        //Send(ping);
    //    }

    //    public int ReadData(byte[] buffer, int numBytesToRead, bool readExact)
    //    {
    //        int numread = 0;
    //        try
    //        {
    //            do
    //            {
    //                int thisread = this.client.Client.Receive(buffer, numread, numBytesToRead, SocketFlags.None);
    //                numread += thisread;
    //                numBytesToRead -= thisread;
    //            }
    //            while (readExact && 0 < numBytesToRead);
    //            _PingTimer.Start();
    //        }
    //        catch
    //        {
    //            Disconnect();
    //        }

    //        return numread;
    //    }

    //    public int WriteData(byte[] buffer, int numBytesToWrite)
    //    {
    //        try
    //        {
    //            this.client.Client.Send(buffer, numBytesToWrite, SocketFlags.None);
    //            _PingTimer.Start();
    //        }
    //        catch
    //        {
    //            Disconnect();
    //        }

    //        return numBytesToWrite;
    //    }

    //    public bool Send(ByteBuffer buffer)
    //    {
    //        if (!this.connected) return false;
    //        if (!buffer.IsReady) return false;
    //        if (0 < WriteData(buffer.Storage, buffer.Length))
    //            return true;

    //        return false;
    //    }

    //    void Connect()
    //    {
    //        this.connected = true;
    //        _UpdateTimer.Start();
    //        _PingTimer.Start();
    //        if (OnConnect != null)
    //            OnConnect(this);
    //    }

    //    void Disconnect()
    //    {
    //        this.connected = false;
    //        _PingTimer.Stop();
    //        _UpdateTimer.Stop();
    //        if (client != null && client.Client != null)
    //            client.Client.Close();
    //        this.client = null;
    //        this.packet = null;
    //        if (OnDisconnect != null)
    //            OnDisconnect(this);
    //    }

    //    void StartPacket()
    //    {
    //        if (!this.connected) return;

    //        this.packet = new ByteBuffer(4);
    //        ReadData(this.packet.Storage, 4, true);
    //        uint size = this.packet.ReadUInt32();
    //        this.packet = new ByteBuffer(size + 4);
    //        this.packet.WriteUInt32(size);
    //        this.bytesToRead = size;
    //        this.packet.Length = 4;

    //        //Console.WriteLine("packet size: {0}", size);
    //    }

    //    void ContinuePacket()
    //    {
    //        //Console.WriteLine("bytes to read: {0}", bytesToRead);

    //        byte[] buffer = new byte[this.bytesToRead];
    //        if (ReadData(buffer, (int)this.bytesToRead, true) <= 0)
    //            return;

    //        foreach (byte b in buffer)
    //            this.packet.WriteUInt8(b);
    //    }

    //    void Process()
    //    {
    //        if (OnBufferComplete != null)
    //            OnBufferComplete(this.packet);
    //        this.packet = null;
    //    }
    //}
}
