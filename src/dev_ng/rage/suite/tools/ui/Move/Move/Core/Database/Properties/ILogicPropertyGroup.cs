﻿using System;

using New.Move.Core;

namespace Rockstar.MoVE.Framework.DataModel
{
    public interface ILogicPropertyGroup : ILogicProperty, ILogicContainerProperty
    {
        void RemoveConnections(INode parent);

        int IndexOf(IProperty property);

        object ItemAt(int index);
        int Count();
        bool Contains(IProperty property);
    }
}
