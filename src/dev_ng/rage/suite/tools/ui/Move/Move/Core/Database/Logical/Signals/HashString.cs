﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class HashString : SignalBase, ISerializable
    {
        public HashString(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public HashString(HashStringSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.HashString"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public HashString(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "hashstringsignal"; } }


        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new HashString(new HashStringSignal());
            (clone as HashString).AdditionalText = this.AdditionalText;
            (clone as HashString).Database = this.Database;
            (clone as HashString).Enabled = this.Enabled;
            (clone as HashString).Name = this.Name;
            (clone as HashString).Position = this.Position;
            (clone as HashString).Signal = this.Signal;

            return (ISelectable)clone;
        }
    }
}