﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using New.Move.Core;

namespace New.Move.Core
{
    [ValueConversion(typeof(New.Move.Core.SyncType), typeof(IEnumerable<SyncType>))]
    public class SyncTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<SyncType> ret = new List<SyncType>();
            SyncType flags = (SyncType)value;
            foreach (SyncType pi in Enum.GetValues(typeof(SyncType)))
            {
                if ((flags & pi) != 0)
                {
                    ret.Add(pi);
                }
            }

            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}