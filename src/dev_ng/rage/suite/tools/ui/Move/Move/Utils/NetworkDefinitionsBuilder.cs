﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

using Ionic.Zip;

using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace Move.Utils
{
    public static class NetworkDefinitionsBuilder
    {
        public static bool AddFileToIntermediateArchive(string mrfPathname)
        {
            string networkDefsIntZipPathname;
            IConfig config = ConfigFactory.CreateConfig();
            networkDefsIntZipPathname = Path.Combine(config.Project.DefaultBranch.Export, "anim/networkdefs.zip");

            try
            {
                if (!File.Exists(networkDefsIntZipPathname))
                {
                    LogFactory.ApplicationLog.Error("Target file at '{0}' doesn't exist.", networkDefsIntZipPathname);
                    return false;
                }

                FileInfo networkDefsIntZipFileInfo = new FileInfo(networkDefsIntZipPathname);
                if (networkDefsIntZipFileInfo.IsReadOnly)
                {
                    LogFactory.ApplicationLog.Error("Target file at '{0}' is read only.", networkDefsIntZipPathname);
                    return false;
                }

                string mrfFilename = Path.GetFileName(mrfPathname);
                using (var archive = Ionic.Zip.ZipFile.Read(networkDefsIntZipPathname))
                {
                    byte[] mrfData = File.ReadAllBytes(mrfPathname);

                    if (archive.ContainsEntry(mrfFilename))
                    {
                        archive.UpdateEntry(mrfFilename, mrfData);
                        LogFactory.ApplicationLog.Message("Updated '{0}' in '{1}'.", mrfFilename, networkDefsIntZipPathname);
                    }
                    else
                    {
                        archive.AddEntry(mrfFilename, mrfData);
                        LogFactory.ApplicationLog.Message("Added '{0}' to '{1}'.", mrfFilename, networkDefsIntZipPathname);
                    }

                    archive.Save();
                }

                return true;
            }
            catch (Exception ex)
            {
                LogFactory.ApplicationLog.Error("Unexpected error occurred when adding '{0}' to '{1}'.", ex.Message, networkDefsIntZipPathname);

                return false;
            }
        }

        public static bool DoPlatformConversion()
        {
            string convertFileScriptPathname;
            string networkDefsIntZipPathname;

            IConfig config = ConfigFactory.CreateConfig();
            convertFileScriptPathname = Path.Combine(config.ToolsLib, "util/data_convert_file.rb");
            networkDefsIntZipPathname = Path.Combine(config.Project.DefaultBranch.Export, "anim/networkdefs.zip");

            try
            {
                Process process = Process.Start(convertFileScriptPathname, networkDefsIntZipPathname + " --rebuild");
                process.WaitForExit();

                if (process.ExitCode == 0)
                {
                    LogFactory.ApplicationLog.Message("Built platform data from '{0}'.", networkDefsIntZipPathname);
                }
                else
                {
                    LogFactory.ApplicationLog.Error("Unable to build platform data from '{0}'.  Exit code from '{1}' was {2}",
                        networkDefsIntZipPathname, convertFileScriptPathname, process.ExitCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                LogFactory.ApplicationLog.Error("Unexpected error occurred during platform conversion of '{0}' via '{1}'.",
                    networkDefsIntZipPathname, convertFileScriptPathname);

                return false;
            }
        }
    }
}
