﻿using System;
using System.ComponentModel;
using System.Xml;

namespace New.Move.Core
{
    public interface IProperty : INotifyPropertyChanged, ICloneable
    {
        string Name { get; set; }
        Guid Id { get; }

        void SetPropertyChangedHandler(PropertyChangedEventHandler handler);
    }
}