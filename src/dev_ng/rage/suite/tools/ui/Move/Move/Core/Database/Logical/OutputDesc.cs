﻿using System;
using System.Windows.Media;

namespace New.Move.Core
{
    internal class OutputDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Output";
            public static int Id = -1;
            public static string Help = "";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return null; } }
        public ImageSource Icon { get { return null; } }

        public Type ConstructType { get { return typeof(Output); } }

        public INode Create(Database database)
        {
            return new Output(this) { Name = Const.Name };
        }
    }
}