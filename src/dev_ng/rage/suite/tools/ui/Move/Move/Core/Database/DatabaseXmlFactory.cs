﻿using System;
using System.Xml;
using New.Move.Core;

namespace New.Move.Core
{
    /// <summary>
    /// Provides a abstract base class to create a node factory used during deserialisation
    /// of a database.
    /// </summary>
    public abstract class DatabaseXmlFactory
    {
        #region Methods
        /// <summary>
        /// Create a new <see cref="INode"/> object based on the specified reader.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as a data provider for the new node object.
        /// </param>
        /// <param name="database">
        /// The database the new node exists in.
        /// </param>
        /// <param name="parent">
        /// The identifier of the parent to the new node.
        /// </param>
        /// <returns>
        /// A new node that is represented by the specified reader.
        /// </returns>
        public abstract INode CreateNode(XmlReader reader, Database database, ITransitional parent);

        /// <summary>
        /// Create a new <see cref="TransitionBase"/> object based on the specified reader.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as a data provider for the new transition
        /// base object.
        /// </param>
        /// <param name="database">
        /// The database the new transition exists in.
        /// </param>
        /// <returns>
        /// A new transition base that is represented by the specified reader.
        /// </returns>
        public abstract TransitionBase CreateTransitionBase(XmlReader reader, Database database);

        /// <summary>
        /// Create a new <see cref="ICondition"/> object based on the specified reader.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as a data provider for the new condition object.
        /// </param>
        /// <param name="database">
        /// The database the new condition exists in.
        /// </param>
        /// <returns>
        /// A new transition condition that is represented by the specified reader.
        /// </returns>
        public abstract ICondition CreateTransitionCondition(XmlReader reader, Database database);
        #endregion
    }
}
