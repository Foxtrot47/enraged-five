﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class HashStringReference : ReferenceSignal, ISerializable
    {
        public HashStringReference(Guid reference, string name)
            : base(reference, name)
        {
            Reference = reference;
        }

        public HashStringReference(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override Parameter GetParameter()
        {
            return Parameter.HashString;
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}