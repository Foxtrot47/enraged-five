﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;

using Rage.Move.Utils;
using Rage.Move.Utils.Serialization;
using XmlSerializer = 
    Rage.Move.Utils.Serialization.XmlSerializer;

using Rage.Move.Core.Dg;

namespace Rage.Move.Core.Dag
{
    //public delegate void ActiveChangedHandler(ITransitionalNode active);
    //public delegate void ControlSignalsChangedEventHandler(ObservableCollection<ControlSignal> signals);
    //public delegate void MessagesChangedEventHandler(ObservableCollection<Message> messages);
    //public delegate void RequestsChangedEventHandler(ObservableCollection<Request> requests);
    //public delegate void FlagsChangedEventHandler(ObservableCollection<Flag> flags);
    public delegate void DatabaseChangedHandler();

#if disabled
    [Serializable]
    public class Database
    {
        // should come from the node when everything else is in order.
        public static int NODE_INDIRECT = 1;
        public static int NODE_TAIL = 2;
        public static int NODE_INLINE = 3;
        public static int NODE_ANIMATION = 4;
		public static int NODE_BLEND = 5;
		public static int NODE_ADDSUBTRACT = 6;
		public static int NODE_FILTER = 7;
		public static int NODE_MIRROR = 8;
		public static int NODE_FRAME = 9;
		public static int NODE_IK = 10;
		public static int NODE_RAGDOLL = 11;
		public static int NODE_MOVER = 12;
		public static int NODE_BLENDN = 13;
		public static int NODE_PONYTAIL = 14;
		public static int NODE_CLIP = 15;
		public static int NODE_STYLE = 16;
		public static int NODE_PM = 17;
		public static int NODE_EXTRAPOLATE = 18;
		public static int NODE_EXPRESSION = 19;
		public static int NODE_CAPTURE = 20;
		public static int NODE_PROXY = 21;
		public static int NODE_ADDN = 22;
		public static int NODE_IDENTITY = 23;
		public static int NODE_MERGE = 24;
		public static int NODE_POSE = 25;
		public static int NODE_MERGEN = 26;
		public static int NODE_STATE = 27;
        public static int NODE_INVALID = 28;

        public static int OPERATOR_ADD = 1;
        public static int OPERATOR_ADD_CONSTANT = 2;
        public static int OPERATOR_MULTIPLY = 3;
        public static int OPERATOR_MULTIPLY_CONSTANT = 4;

        public static class Const
        {
            public const string DataFilename = "default.motion";
        }

        public Dictionary<int, ITransitionalNode> DEBUG_HACK_NODES;
        public Dictionary<ITransitionalNode, int> DEBUG_HACK_NODE_IDS;
        public Dictionary<uint, string> DEBUG_HACK_SIGNAL_NAMES;
        //public Dictionary<ITransitionalNode, Reference> DEBUG_HACK_REFERENCES;
        public Dictionary<ITransitionalNode, List<ControlSignal>> DEBUG_HACK_SIGNALS;

        public Dictionary<GuidPair, string> HACK_EXPORT_FLAG;

        string _FullyQualifiedFilename;

        string _FileVersion = "1.0";

        [XmlIgnore]
        [NonSerialized]
        public ControlSignalsChangedEventHandler ControlSignalsChanged;
        void OnControlSignalsChanged()
        {
            if (ControlSignalsChanged != null)
                ControlSignalsChanged(_ControlSignals);
        }

        [XmlIgnore]
        [NonSerialized]
        public MessagesChangedEventHandler MessagesChanged;
        void OnMessagesChanged()
        {
            if (MessagesChanged != null)
                MessagesChanged(_Messages);
        }


        ObservableCollection<ControlSignal> _ControlSignals = new ObservableCollection<ControlSignal>();
        public ObservableCollection<ControlSignal> ControlSignals { get { return _ControlSignals; } set { _ControlSignals = value; OnControlSignalsChanged(); } }

        ObservableCollection<Message> _Messages = new ObservableCollection<Message>();
        public ObservableCollection<Message> Messages { get { return _Messages; } set { _Messages = value; OnMessagesChanged(); } }

        [XmlIgnore]
        [NonSerialized]
        public RequestsChangedEventHandler RequestsChanged;
        void OnRequestsChanged()
        {
            if (RequestsChanged != null)
                RequestsChanged(_Requests);
        }

        [XmlIgnore]
        [NonSerialized]
        public FlagsChangedEventHandler FlagsChanged;
        void OnFlagsChanged()
        {
            if (FlagsChanged != null)
                FlagsChanged(_Flags);
        }

        ObservableCollection<Request> _Requests = new ObservableCollection<Request>();
        public ObservableCollection<Request> Requests { get { return _Requests; } set { _Requests = value; OnRequestsChanged(); } }

        ObservableCollection<Flag> _Flags = new ObservableCollection<Flag>();
        public ObservableCollection<Flag> Flags { get { return _Flags; } set { _Flags = value; OnFlagsChanged(); } }

        [XmlIgnore]
        [NonSerialized]
        public ActiveChangedHandler ActiveChanged = null;
        public void OnActiveChanged()
        {
            if (ActiveChanged != null)
                ActiveChanged(_Active);
        }

        ITransitionalNode _Active;
        [XmlIgnore]
        public ITransitionalNode Active
        {
            get { return _Active; }
            set 
            {
                _Active = value;
                IsDirty = true;
                OnActiveChanged(); 
            }
        }

        ITransitionalNode _Root;
        [XmlInferType]
        public ITransitionalNode Root
        { 
            get { return _Root; }
            set { _Root = value; IsDirty = true; }
        }

        [XmlAttribute(AttributeName = "FileVersion")]
        public string Version
        {
            get { return _FileVersion; }
            set
            {
                _FileVersion = value;
            }
        }

        [XmlIgnore]
        public static string ApplicationFolderPath
        {
            get
            {
                string path = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Runtime.ApplicationFolderName);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                return path;
            }
        }

        [XmlIgnore]
        public static string DefaultFullyQualifiedFilename
        {
            get
            {
                string appLocation = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Runtime.ApplicationFolderName);

                if (!Directory.Exists(appLocation))
                {
                    Directory.CreateDirectory(appLocation);
                }

                return Path.Combine(appLocation, Const.DataFilename);
            }
        }

        [XmlIgnore]
        public string FullyQualifiedFilename
        {
            get { return _FullyQualifiedFilename; }
            set
            {
                _FullyQualifiedFilename = value;
            }
        }

        [XmlIgnore]
        public Guid Id { get; private set; }

        [XmlIgnore]
        public bool IsDirty { get; set; }

        public Database()
        {
            _ControlSignals.CollectionChanged += 
                new NotifyCollectionChangedEventHandler(
                    delegate(object sender, NotifyCollectionChangedEventArgs e) { OnActiveChanged(); }
                );
            _Messages.CollectionChanged +=
                new NotifyCollectionChangedEventHandler(
                    delegate(object sender, NotifyCollectionChangedEventArgs e) { OnActiveChanged(); }
                );
            IsDirty = false;

            Id = Guid.NewGuid();
        }

        public void Clear()
        {
            _ControlSignals.Clear();
            _Requests.Clear();
            Root = null;
            Active = null;
        }

        public MotionTree CreateMotionTree()
        {
            MotionTree mt = new MotionTree("");
            mt.Database = Id;
            mt.Anchors.Add(new TransformResult("Result"));

            mt.ParentChanged += new EventHandler(
                delegate(object sender, EventArgs e)
                {
                    StateMachine parent = mt.Parent;
                    LogicStateNode logical = null;
                    while (parent != null)
                    {
                        if (parent.LogicalParent != null)
                        {
                            logical = parent.LogicalParent;
                            break;
                        }

                        parent = parent.Parent;
                    }

                    if (logical != null)
                    {
                        if (logical.Anchors.OfType<TransformPassthrough>().Count() != 0)
                            mt.Anchors.Add(new TransformSource("Source"));
                    }                    
                }
            );

            return mt;
        }

        public StateMachine CreateStateMachine()
        {
            StateMachine sm = new StateMachine("");
            sm.Database = Id;
            return sm;
        }

        public void Create(int id, Point position)
        {
            if (Active is New.Move.Core.Motiontree)
            {
                New.Move.Core.Motiontree active = (New.Move.Core.Motiontree)Active;

                New.Move.Core.ILogicDesc template =
                        Runtime.Instance.LogicDescriptions.Find(delegate(New.Move.Core.ILogicDesc n) { return n.Id == id; });
                //ILogicNode instance = template.Create(this);
                New.Move.Core.ILogic instance = template.Create();
                instance.Position = position;

                active.Add(instance);

                //active.Children.Add(instance);
            }

            if (Active is New.Move.Core.StateMachine)
            {
                New.Move.Core.StateMachine active = (New.Move.Core.StateMachine)Active;

                New.Move.Core.ITransitionalDesc template =
                    Runtime.Instance.TransitionalDescriptions.Find(delegate(New.Move.Core.ITransitionalDesc n) { return n.Id == id; });
                New.Move.Core.ITransitional instance = template.Create();
                instance.Position = position;

                active.Add(instance);

                //active.Children.Add(instance);
            }
        }

       public void CreateOperator(int id, Point position)
       {
           if (Active is MotionTree) 
           {
               MotionTree active = (MotionTree)Active;

               IRegisterOperator_Ex template =
                   Runtime.Instance.RegisteredOperators.Find(delegate(IRegisterOperator_Ex o) { return o.Id == id; });
               IOpNode instance = template.Create();
               instance.Position = position;

               // this should be able to be a child!!!
               active.Operators.Add(instance);
           }
       }

        public void Save()
        {
            if (Root == null)
                return;

            if (String.IsNullOrEmpty(FullyQualifiedFilename))
                FullyQualifiedFilename = Database.DefaultFullyQualifiedFilename;

            try
            {
                XmlSerializer serializer = new XmlSerializer();
                serializer.Serialize(FullyQualifiedFilename, this);
            }
            catch (Exception)
            {
            }

            IsDirty = false;
        }

        public void Save(string filename)
        {
            this.FullyQualifiedFilename = filename;
            Save();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public void Load()
        {
            try
            {
                if (String.IsNullOrEmpty(FullyQualifiedFilename))
                    FullyQualifiedFilename = Database.DefaultFullyQualifiedFilename;

                // can't just load the requests and signals like this. have to load them first!!!

                if (Runtime.Instance.Databases.ContainsKey(Id) != true)
                    Runtime.Instance.Databases.Add(Id, this);

                XmlDocument document = new XmlDocument();
                document.Load(FullyQualifiedFilename);

                XmlDeserializer deserializer = new XmlDeserializer();

                XmlNode database = document.SelectSingleNode("Database");
                XmlNode signals = database.SelectSingleNode("ControlSignals");
                ControlSignals = (ObservableCollection<ControlSignal>)deserializer.Deserialize(signals, typeof(ObservableCollection<ControlSignal>));

                XmlNode messages = database.SelectSingleNode("Messages");
                Messages = (ObservableCollection<Message>)deserializer.Deserialize(messages, typeof(ObservableCollection<Message>));

                XmlNode requests = database.SelectSingleNode("Requests");
                Requests = (ObservableCollection<Request>)deserializer.Deserialize(requests, typeof(ObservableCollection<Request>));

                XmlNode flags = database.SelectSingleNode("Flags");
                Flags = (ObservableCollection<Flag>)deserializer.Deserialize(flags, typeof(ObservableCollection<Flag>));

                XmlNode root = database.SelectSingleNode("MotionTree");
                Root = (root != null) ?
                    (ITransitionalNode)deserializer.Deserialize(root, typeof(MotionTree)) :
                    (ITransitionalNode)deserializer.Deserialize(
                        database.SelectSingleNode("StateMachine"), typeof(StateMachine));

                Root.Database = Id;

                Active = Root;

                _ControlSignals.CollectionChanged +=
                    new NotifyCollectionChangedEventHandler(
                        delegate(object sender, NotifyCollectionChangedEventArgs e) { OnActiveChanged(); }
                    );
                _Messages.CollectionChanged +=
                    new NotifyCollectionChangedEventHandler(
                        delegate(object sender, NotifyCollectionChangedEventArgs e) { OnActiveChanged(); }
                    );

                IsDirty = false;
            }
            catch (Exception e)
            {
                FullyQualifiedFilename = String.Empty;
            }
        }

        public void Load(string filename)
        {
            this.FullyQualifiedFilename = filename;
            Load();
        }

        public void Serialize(string filename)
        {
            if (Root == null)
                return;

            Validate();

            //Serializer serializer = new Serializer(this);
            //serializer.Serialize(Root, null, null);
            //serializer.Write(filename);
        }

        public void Validate()
        {
            //if (null == this.NodeCollection || 0 == this.NodeCollection.Count)
            //    return;

            //Runtime.Nodes.Root.Validate();
        }
    }
#endif

    public static class FreeFuncs
    {
        /*
        public static bool IsOperator(this OperatorSource source, List<OperatorConnection> connections)
        {
            foreach (OperatorConnection connection in connections)
            {
                if (source.Id == connection.Sink)
                    return true;
            }

            return false;
        }

        public static uint? GetOperator(this OperatorSource source, List<OperatorConnection> connections, List<OpNode> siblings)
        {
            foreach (OperatorConnection connection in connections)
            {
                if (source.Id == connection.Sink)
                {
                    foreach (OpNode sibling in siblings)
                    {
                        if (sibling.HasAnchor(connection.Source))
                        {
                            uint index = (uint)siblings.IndexOf(sibling);
                            return index;
                        }
                    }
                }
            }

            return null;
        }
        */
        /*
        public static bool IsSignal(this OperatorSource source, List<OperatorSourceConnection> connections)
        {
            foreach (OperatorSourceConnection connection in connections)
            {
                if (source.Id == connection.Sink)
                    return true;
            }

            return false;
        }

        public static uint? GetSignal(this OperatorSource source, List<OperatorSourceConnection> connections, Guid database, Dictionary<ControlSignal, ControlSignal> remaps)
        {
            foreach (OperatorSourceConnection connection in connections)
            {
                if (source.Id == connection.Sink)
                {
#if blah
                    Database db = Runtime.Instance.Databases[database];
                    foreach (ControlSignal signal in db.ControlSignals)
                    {
                        if (signal.Id == connection.Source)
                        {
                            ControlSignal remapped = signal;
                            if (remaps != null && remaps.ContainsKey(signal))
                                remapped = remaps[signal];

                            uint crc = Crc32.Generate(0, remapped.Name);
                            return crc;
                        }
                    }
#endif
                }
            }

            return null;
        }
         */
    }
#if NO_SERIALIZER
    public class Serializer
    {
        Dictionary<object, Chunk.LabelId> _Labels = new Dictionary<object, Chunk.LabelId>();
        Database _Database;
        Chunk _Root = new Chunk();

        List<Chunk.LabelId> _States = new List<Chunk.LabelId>();

        Stack<ILogicNode> _Stack = new Stack<ILogicNode>();

        Dictionary<MotionTree, HashSerializer> _Hashes = new Dictionary<MotionTree, HashSerializer>();
        Dictionary<MotionTree, HashSerializer> _Hashes2 = new Dictionary<MotionTree, HashSerializer>();
        Dictionary<MotionTree, Dictionary<ControlSignal, ControlSignal>> _Remaps =
                new Dictionary<MotionTree, Dictionary<ControlSignal, ControlSignal>>();
        Dictionary<MotionTree, List<ILogicNode>> _Children =
                new Dictionary<MotionTree, List<ILogicNode>>();
        //Dictionary<MotionTree, List<Guid>> _Connects = new Dictionary<MotionTree, List<Guid>>();

        public Serializer(Database database)
        {
            _Database = database;
            _Database.DEBUG_HACK_NODES = new Dictionary<int, ITransitionalNode>();
            _Database.DEBUG_HACK_NODE_IDS = new Dictionary<ITransitionalNode, int>();
            _Database.DEBUG_HACK_SIGNAL_NAMES = new Dictionary<uint, string>();
            _Database.DEBUG_HACK_REFERENCES = new Dictionary<ITransitionalNode, Reference>();
            _Database.DEBUG_HACK_SIGNALS = new Dictionary<ITransitionalNode, List<ControlSignal>>();

            foreach (ControlSignal signal in _Database.ControlSignals)
                _Database.DEBUG_HACK_SIGNAL_NAMES.Add(Crc32.Generate(0, signal.Name), signal.Name);

            SerializeHeader();
            //SerializeSignals();
            SerializeRequests();
            SerializeFlags();
        }

        public ILogicNode GetSourceOf(LogicNodeBase node, string name)
        {
            /*
            IAnchor anchor = node.GetAnchor(name);
            MotionTree parent = node.Parent;
            LogicNodeBase source = parent.GetSourceOf(anchor) as LogicNodeBase;

            // add this to a stack of things that need to be serialized
            if (source is LogicInputNode)
                _Stack.Push(source);

            return source;
             */

            return null;
        }

        public uint? GetHashIndexOf(MotionTree parent, Guid sink)
        {
            List<ControlSignal> items = new List<ControlSignal>();
            foreach (SignalConnection connection in parent.Connections.OfType<SignalConnection>())
            {
                if (connection.Sink == sink)
                { 
                    Database database = Runtime.Instance.Databases[parent.Database];
                    foreach (ControlSignal signal in database.ControlSignals)
                    {
                        if (signal.Id == connection.Source)
                        {
                            ControlSignal remapped = signal;
                            if (_Remaps.ContainsKey(parent))
                                remapped = _Remaps[parent][signal];

                            uint? val = _Hashes[parent].Get(remapped.Name);
                            return val;
                        }
                    }
                }
            }

            return null;
        }

        public uint? GetHash2IndexOf(MotionTree parent, Guid sink)
        {
            List<ControlSignal> items = new List<ControlSignal>();
            foreach (SignalConnection connection in parent.Connections.OfType<SignalConnection>())
            {
                if (connection.Sink == sink)
                {
                    Database database = Runtime.Instance.Databases[parent.Database];
                    foreach (ControlSignal signal in database.ControlSignals)
                    {
                        if (signal.Id == connection.Source)
                        {
                            ControlSignal remapped = signal;
                            if (_Remaps.ContainsKey(parent))
                                remapped = _Remaps[parent][signal];

                            uint? val = _Hashes2[parent].Get(remapped.Name);
                            return val;
                        }
                    }
                }
            }

            return null;
        }

        public uint? GetSignalIdOf(MotionTree parent, Guid sink)
        {
            foreach (OperatorResultConnection connection in parent.Connections.OfType<OperatorResultConnection>())
            {
                if (connection.Sink == sink)
                {
                    int? id = GetSignalId(connection.Source);

                    if (id.HasValue)
                        return (uint)id.Value;
                    else
                        return null;
                }
            }

            return null;
        }

        public string GetNameOf(MotionTree parent, Guid sink)
        {
            List<ControlSignal> items = new List<ControlSignal>();
            foreach (SignalConnection connection in parent.Connections.OfType<SignalConnection>())
            {
                if (connection.Sink == sink)
                {
                    Database database = Runtime.Instance.Databases[parent.Database];
                    foreach (ControlSignal signal in database.ControlSignals)
                    {
                        if (signal.Id == connection.Source)
                        {
                            ControlSignal remapped = signal;
                            if (_Remaps.ContainsKey(parent))
                                remapped = _Remaps[parent][signal];

                            return remapped.Name;
                        }
                    }
                }
            }

            return null;
        }

        public void Serialize(ITransitionalNode node, Dictionary<ControlSignal, ControlSignal> remaps, Reference reference)
        {
            if (node is MotionTree)
                Serialize(node as MotionTree, _Root, remaps, reference);
            else
            if (node is StateMachine)
                Serialize(node as StateMachine, _Root, null, reference);
        }

        public void Serialize(ITransitionalNode node, Chunk writer, Dictionary<ControlSignal, ControlSignal> remaps, Reference reference)
        {
            if (node is MotionTree)
                Serialize(node as MotionTree, writer, remaps, reference);
            else if (node is StateMachine)
                Serialize(node as StateMachine, writer, remaps, reference);
        }

        public void Write(string filename)
        {
            using (BinaryFileStream stream = new BinaryFileStream(filename, FileMode.Create, FileAccess.Write))
            {
                Chunk data = _Root.AppendChild();

                /*
                data.AppendUInt32((uint)_States.Count);
                foreach (Chunk.LabelId label in _States)
                    data.AppendOffset(label, Chunk.OffsetOrigin.BiffFile, 4, 1);
                 * */

                _Root.Write(stream, Chunk.Endianess.LittleEndian);

                stream.Seek(12, SeekOrigin.Begin);
#if disabled
                int length = (int)stream.Length - (/*header*/16 + 
                    /*hash table*/_Database.ControlSignals.Count * 8 +
                    /*signals*/4 + _Database.ControlSignals.Count * 4 + 
                    /*requests hash table*/ 4 + _Database.Requests.Count * 8);
#endif
                //int length = (int)stream.Length - (/*header*/16 + 4 + _States.Count * 4);
                int length = (int)stream.Length - (/*header*/16);
                stream.Write(BitConverter.GetBytes(length), 0, 4);
            }
        }

        void SerializeHeader()
        {
            Chunk header = _Root.AppendChild();
            header.AppendData("MOVE", 4);
            header.AppendData((int)0x1234);
            header.AppendData(0);
            header.AppendData(0); // placeholder for size
            //int filename_len = _Database.FullyQualifiedFilename.Length;
            //header.AppendData(filename_len);
            //header.AppendString(_Database.FullyQualifiedFilename, filename_len);
        }

        private class HashSerializer
        {
            public class Entry
            {
                public Entry(uint key, uint index)
                {
                    Key = key;
                    Index = index;
                }

                public uint Key { get; set; }
                public uint Index { get; set; }
            }

            public HashSerializer(int count)
            {
                Entries = new Entry[count];
                for (int i = 0; i < count; ++i)
                    Entries[i] = new Entry(0xffffffff, 0);
            }

            public Entry[] Entries { get; private set; }

            public void Insert(string key, uint index)
            {
                uint crc = Crc32.Generate(0, key);

                Insert(crc, index);
            }

            public void Insert(uint key, uint index)
            {
                long? i = Get(key);
                Entries[i.Value].Key = key;
                Entries[i.Value].Index = index;
            }

            public uint Find(string key)
            {
                uint crc = Crc32.Generate(0, key);
                return Find(crc);
            }

            public uint Find(uint key)
            {
                long? i = Get(key);
                if (!i.HasValue) return 0xffffffff;

                if (Entries[i.Value].Key != 0xffffffff)
                    return Entries[i.Value].Index;

                return 0xffffffff;
            }

            public uint? Get(string key)
            {
                uint crc = Crc32.Generate(0, key);
                return Get(crc);
            }

            uint? Get(uint key)
            {
                if (Entries.Length.Equals(0))
                    return null;

                long i = key % Entries.Length;

                while (Entries[i].Key != 0xffffffff && Entries[i].Key != key)
                    i = (i + 1) % Entries.Length;

                return (uint)i;
            }
        }

        void SerializeSignals()
        {
            Chunk signals = _Root.AppendChild();
            signals.AppendData(_Database.ControlSignals.Count);

            HashSerializer hash = new HashSerializer(_Database.ControlSignals.Count);

            uint index = 0;

            foreach (ControlSignal signal in _Database.ControlSignals)
                hash.Insert(signal.Name, index++);

            foreach (HashSerializer.Entry entry in hash.Entries)
            {
                signals.AppendUInt32(entry.Key);
                signals.AppendUInt32(entry.Index);
            }

            foreach (ControlSignal signal in _Database.ControlSignals)
            {
                /*
                char[] chars = signal.Name.ToCharArray();
                byte[] bytes = new byte[chars.Length];
                for (int i = 0; i < chars.Length; ++i)
                    bytes[i] = BitConverter.GetBytes(chars[i])[0];

                uint crc = Crc32.Generate(0, bytes, bytes.Length);

                Console.WriteLine(String.Format("{0} = 0x{1:x8},", signal.Name, crc));
                */
                if (signal is ControlSignalFloat)
                {
                    ControlSignalFloat floatSignal = (ControlSignalFloat)signal;
                    //signals.AppendData(floatSignal.Name, 32);
                    //signals.AppendData((float)floatSignal.Min);
                    //signals.AppendData((float)floatSignal.Max);
                    signals.AppendData((float)floatSignal.Default);
                }
                else 
                if (signal is ControlSignalBool)
                {
                    ControlSignalBool boolSignal = (ControlSignalBool)signal;
                    //signals.AppendData(boolSignal.Name, 32);
                    //signals.AppendData((float)0);
                    //signals.AppendData((float)0);
                    signals.AppendData(Convert.ToSingle(boolSignal.Default));
                }
                else 
                if (signal is ControlSignalAnimation)
                {
                    ControlSignalAnimation animSignal = (ControlSignalAnimation)signal;
                    //signals.AppendData(animSignal.Name, 32);
                    //signals.AppendData((float)0);
                    //signals.AppendData((float)0);
                    signals.AppendData((float)0);
                }
                else 
                if (signal is ControlSignalClip)
                {
                    ControlSignalClip clipSignal = (ControlSignalClip)signal;
                    //signals.AppendData(clipSignal.Name, 32);
                    //signals.AppendData((float)0);
                    //signals.AppendData((float)0);
                    signals.AppendData((float)0);
                }
                else 
                if (signal is ControlSignalExpression)
                {
                    ControlSignalExpression exprSignal = (ControlSignalExpression)signal;
                    //signals.AppendData(exprSignal.Name, 32);
                    //signals.AppendData((float)0);
                    //signals.AppendData((float)0);
                    signals.AppendData((float)0);
                }
                else 
                if (signal is ControlSignalFilter)
                {
                    ControlSignalFilter filterSignal = (ControlSignalFilter)signal;
                    //signals.AppendData(filterSignal.Name, 32);
                    //signals.AppendData((float)0);
                    //signals.AppendData((float)0);
                    signals.AppendData((float)0);
                }
                else 
                if (signal is ControlSignalFrame)
                {
                    ControlSignalFrame frameSignal = (ControlSignalFrame)signal;
                    signals.AppendData((float)0);
                }
                else
                if (signal is ControlSignalParameterizedMotion)
                {
                    ControlSignalParameterizedMotion pmSignal = (ControlSignalParameterizedMotion)signal;
                    //signals.AppendData(pmSignal.Name, 32);
                    //signals.AppendData((float)0);
                    //signals.AppendData((float)0);
                    signals.AppendData((float)0);
                }
            }
        }

        void SerializeRequests()
        {
            Chunk requests = _Root.AppendChild();

            List<string> requestlist = new List<string>();
            foreach (Request request in _Database.Requests)
            {
                string[] split = request.Name.Split(':');
                if (!requestlist.Contains(split[0]))
                    requestlist.Add(split[0]);
            }

            HashSerializer hash = new HashSerializer(requestlist.Count + 1);

            uint index = 0;
            foreach (string request in requestlist)
            {
                hash.Insert(request, index++);
            }

            requests.AppendInt32(hash.Entries.Count());
            foreach (HashSerializer.Entry entry in hash.Entries)
            {
                requests.AppendUInt32(entry.Key);
                requests.AppendUInt32(entry.Index);
            }
        }

        void SerializeFlags()
        {
            Chunk flags = _Root.AppendChild();

            List<string> flaglist = new List<string>();
            foreach (Flag flag in _Database.Flags)
            {
                string[] split = flag.Name.Split(':');
                if (!flaglist.Contains(split[0]))
                    flaglist.Add(split[0]);
            }

            HashSerializer hash = new HashSerializer(flaglist.Count + 1);

            uint index = 0;
            foreach (string flag in flaglist)
            {
                hash.Insert(flag, index++);
            }

            flags.AppendInt32(hash.Entries.Count());
            foreach (HashSerializer.Entry entry in hash.Entries)
            {
                flags.AppendUInt32(entry.Key);
                flags.AppendUInt32(entry.Index);
            }
        }

        void PrintPath(MotionTree node)
        {
            /*
            Stack<string> path = new Stack<string>();

            Stack<ITransitionalNode> stack = new Stack<ITransitionalNode>();
            stack.Push(node);

            while (stack.Count != 0)
            {
                ITransitionalNode top = stack.Pop();

                path.Push(top.Name);

                if (top is StateMachine)
                {
                    StateMachine n = top as StateMachine;
                    if (n.LogicalParent != null)
                    {
                        ITransitionalNode parent = n.LogicalParent.Parent;
                        stack.Push(parent);
                    }
                    else
                    if (n.Parent != null)
                    {
                        stack.Push(n.Parent);
                    }
                }

                if (top is MotionTree)
                {
                    if (top.Parent != null)
                        stack.Push(top.Parent);
                }
            }

            string fullpath = "";

            path.Pop(); // ignore the root
            while (0 < path.Count)
            {
                string name = path.Pop();

                if (fullpath.Length != 0)
                    fullpath += "|";

                fullpath += name;
            }

            Console.WriteLine(fullpath);
             */
        }

        Dictionary<Guid, int> _RandomIds = new Dictionary<Guid, int>();

        int GetRandomId(Guid guid)
        {
            int id = guid.GetHashCode();

            while (_RandomIds.ContainsValue(id))
            {
                ++id;
            }

            _RandomIds.Add(guid, id);

            return id;
        }

        int? GetId(Guid guid)
        {
            if (_RandomIds.ContainsKey(guid))
                return _RandomIds[guid];

            return null;
        }

        Dictionary<Guid, int> _RandomSignalIds = new Dictionary<Guid, int>();

        int GetRandomSignalId(Guid guid)
        {
            int id = guid.GetHashCode();

            while (_RandomSignalIds.ContainsValue(id))
                ++id;

            _RandomSignalIds.Add(guid, id);

            return id;
        }

        int? GetSignalId(Guid guid)
        {
            if (_RandomSignalIds.ContainsKey(guid))
                return _RandomSignalIds[guid];

            return null;
        }

        void Serialize(MotionTree node, Chunk writer, Dictionary<ControlSignal, ControlSignal> remaps, Reference reference)
        {
            Chunk chunk = writer.AppendChild();
            Chunk.LabelId start = GetLabel(node);
            chunk.AppendLabel(start);

            _States.Add(start);

            Chunk data = chunk.AppendChild();
            //data.AppendData(/*SOME ID*/1);
            data.AppendData(Database.NODE_STATE);

            if (reference != null)
            { 
                int unid = (reference.Unid.Value <<16) | node.Unid.Value;
                //Console.WriteLine(String.Format("unid {0}, reference {1}, node {2}", unid, reference.Unid.Value, node.Unid.Value));
                _Database.DEBUG_HACK_NODES.Add(unid, node);
                _Database.DEBUG_HACK_NODE_IDS.Add(node, unid);
                _Database.DEBUG_HACK_REFERENCES.Add(node, reference);
                data.AppendData(unid);
            }
            else 
            {
                _Database.DEBUG_HACK_NODES.Add(node.Unid.Value, node);
                _Database.DEBUG_HACK_NODE_IDS.Add(node, node.Unid.Value);
                _Database.DEBUG_HACK_REFERENCES.Add(node, reference);
                data.AppendInt32(node.Unid.Value);
            }

            // get the output transform signal
            object input = SerializerHelpers.FindTransformResult(node);
            // the problem with the lower bit of code: you can't have a reference as an initial input
            //ILogicNode input = SerializerHelpers.FindTransformResult(node);
            // i think i put a special 'cap' on here for inline states, but i can't remember...

            // once all the signal connections are aquired, bucket them into 
            // control signals and 'regular' signals

            List<SignalConnection> connections = new List<SignalConnection>();
            List<ControlSignal> items = new List<ControlSignal>();

            List<object> signal_items = new List<object>();

             Dictionary<ControlSignal, List<SignalConnection>> dict =
                new Dictionary<ControlSignal, List<SignalConnection>>();

            foreach (SignalConnection connection in node.Connections.OfType<SignalConnection>())
            {
                Database database = Runtime.Instance.Databases[node.Database];
                foreach (ControlSignal signal in database.ControlSignals)
                {
                    if (signal.Id == connection.Source)
                    {
                        if (SerializerHelpers.IsSinkReference(node, connection.Sink))
                        {
                            break;
                        }
                        else
                        {
                            if (!dict.ContainsKey(signal))
                            {
                                List<SignalConnection> list = new List<SignalConnection>();
                                list.Add(connection);
                                dict.Add(signal, list);
                            }
                            else
                            {
                                dict[signal].Add(connection);
                            }

                            // this is a hack to prevent this kind of signal happening for a clip
                            //  eventually everything will be like the clip
                            ILogicNode ignore = node.Children.SingleOrDefault(
                                delegate(ILogicNode child)
                                {
                                    return 
                                        (child.Type == "Clip" /*|| child.Type == "Anim"*/) && (child as LogicNodeBase).HasAnchor(connection.Sink);
                                }
                            );

                            if (ignore == null)
                            {
                                if (connections.Contains(connection) != true)
                                    connections.Add(connection);

                                if (signal_items.Contains(signal) != true)
                                    signal_items.Add(signal);
                            }

                            if (items.Contains(signal) != true)
                                items.Add(signal);
                 
                            break;
                        }
                    }
                }
            }

            // flags
            if (node.Connections.OfType<SignalConnection>().Count() != 0 || node.Operators.Count != 0)
            {
                // should also check event count and probably some other things
                data.AppendUInt32(1); // this will create the data block in the state switch/create
            }
            else
            {
                data.AppendUInt32(0);
            }

            if (Runtime.Database.Id != node.Database)
                _Remaps.Add(node, remaps);

            if (remaps != null)
            {
                List<object> remapped = new List<object>();
                foreach (ControlSignal item in signal_items.OfType<ControlSignal>()) {
                    remapped.Add(remaps[item]);

                    if (dict.ContainsKey(item))
                    {
                        List<SignalConnection> list = dict[item];
                        dict.Remove(item);
                        dict.Add(remaps[item], list);
                    }
                }

                signal_items = remapped;
            }

            //_Database.DEBUG_HACK_SIGNALS.Add(node, signal_items);

            // child_count
            List<ILogicNode> children = new List<ILogicNode>();
            foreach (SignalConnection connect in connections)
            {
                foreach (LogicNodeBase child in node.Children)
                {
                    if (child.HasAnchor(connect.Sink))
                    {
                        if (children.Contains(child) != true)
                            children.Add(child);
                    }
                }
            }

            _Children.Add(node, children);

            data.AppendUInt16((ushort)children.Count);

            // transit_count
            List<StateTransition> transitions = SerializerHelpers.FindTransitionsFrom(node);
            data.AppendUInt16((ushort)transitions.Count);

            // connect_count
            //data.AppendUInt16((ushort)connections.Count);
            data.AppendUInt16((ushort)0);

            // TODO: this should only include op_connections that
            //  have a different source...
            List<OperatorResultConnection> op_connections =
                node.Connections.OfType<OperatorResultConnection>().ToList();

            // NOTE: op_signals is one signal per operator node that is called in the 
            //  state update. it tells the operator to calc its value and propogate
            //  it again through a different signal...
            List<Guid> op_signals = new List<Guid>();
            foreach (OperatorResultConnection connect in op_connections)
            {
                foreach (OpNode op in node.Operators)
                {
                    if (op.HasAnchor(connect.Source))
                    {
                        if (!op_signals.Contains(op.Id))
                            op_signals.Add(op.Id);
                        break;
                    }
                }

                //if (!op_signals.Contains(connect.Source))
                //    op_signals.Add(connect.Source);
            }

            List<OpNodeBase> opnodes = node.Operators.OfType<OpNodeBase>().ToList();

            int opnode_signal_count = 0;
            foreach (OpNode op in opnodes)
            {
                IEnumerable<OperatorResultConnection> item_connect =
                        from orc in op_connections where op.HasAnchor(orc.Source) select orc;
                if (item_connect.SingleOrDefault() != null)
                {
                    ++opnode_signal_count;
                }

                List<OperatorSource> inputs = op.Anchors.OfType<OperatorSource>().ToList();
                for (int i = 0; i < inputs.Count; ++i)
                {
                    OperatorSource source = inputs[i];
                    if (source.IsSignal(node.Connections.OfType<OperatorSourceConnection>().ToList()))
                    {
                        ++opnode_signal_count;
                    }
                }
            }

            // signal_count
            int signalCount = (signal_items.Count + op_signals.Count + opnode_signal_count) != 0 ? (signal_items.Count + op_signals.Count + opnode_signal_count) + 1 : 0;
            data.AppendUInt16((ushort)(signalCount));

            // event count
            List<uint> event_crcs = new List<uint>();
            foreach (StateTransition transition in transitions)
            {
                foreach (Rage.Move.Core.Dg.ConditionBase condition in transition.Conditions)
                {
                    if (condition.Name == "At Event")
                    {
                        Message message = condition.GetAttribute("Message").Value as Message;
                        uint crc = Crc32.Generate(0, message.Name);
                        if (!event_crcs.Contains(crc))
                        {
                            event_crcs.Add(crc);
                        }
                    }
                }
            }

            data.AppendUInt16((ushort)event_crcs.Count);

            // child operator node count
            data.AppendUInt16((ushort)opnodes.Count);

            

            // operator signal count (the number of signals that traverse from an operator output to a node)
            data.AppendUInt16((ushort)op_signals.Count);

            data.AppendUInt16(0); // pad

            // signals
            Chunk.LabelId signals = Chunk.ReserveLabel();
            data.AppendOffset(signals);

            // events 
            Chunk.LabelId events = Chunk.ReserveLabel();
            data.AppendOffset(events);

            // operators
            Chunk.LabelId ops = Chunk.ReserveLabel();
            data.AppendOffset(ops);

            // operator signals
            Chunk.LabelId opsignals = Chunk.ReserveLabel();
            data.AppendOffset(opsignals);

            // connects
            Chunk.LabelId connects = Chunk.ReserveLabel();
            data.AppendOffset(connects);

            // source
            data.AppendOffset(GetLabel(input), Chunk.OffsetOrigin.OffsetAddress, 4, 1);

            
            foreach (StateTransition transition in transitions)
            {
                Serialize(transition, node.Parent, data, event_crcs);
            }

            // signal data
            HashSerializer store_hash = new HashSerializer(items.Count + 1);

            uint index = 0;
            foreach (ControlSignal item in items)
            {
                store_hash.Insert(item.Name, index);
                ++index;
            }

            _Hashes2.Add(node, store_hash);

            data.AppendLabel(signals);

            // i want to put the op_signals in here
            foreach (Guid op_signal in op_signals)
            {
                int id = GetRandomId(op_signal);
                signal_items.Add(id);
            }

            foreach (OpNode op in opnodes)
            {
                IEnumerable<OperatorResultConnection> item_connect =
                        from orc in op_connections where op.HasAnchor(orc.Source) select orc;
                if (item_connect.SingleOrDefault() != null)
                {
                    int? signal = GetRandomSignalId(item_connect.Single().Source);
                    signal_items.Add(signal.Value);
                    //data.AppendInt32(signal.Value);
                }

                List<OperatorSource> inputs = op.Anchors.OfType<OperatorSource>().ToList();
                for (int i = 0; i < inputs.Count; ++i)
                {
                    OperatorSource source = inputs[i];
                    if (source.IsSignal(node.Connections.OfType<OperatorSourceConnection>().ToList()))
                    {
                        uint? signal = source.GetSignal(
                            node.Connections.OfType<OperatorSourceConnection>().ToList(),
                            node.Database,
                            remaps);
                        signal_items.Add(signal.Value);
                    }
                }
            }

            HashSerializer hash = new HashSerializer(signalCount);
            index = 0;
            foreach (object item in signal_items)
            {
                if (item is ControlSignal) {
                    hash.Insert((item as ControlSignal).Name, index);
                }
                else if (item is int) {
                    int id = (int)item;
                    hash.Insert((uint)id, index);
                }
                else if (item is uint)
                {
                    hash.Insert((uint)item, index);
                }
                ++index;
            }

            _Hashes.Add(node, hash);

            Dictionary<uint, Chunk.LabelId> targets = new Dictionary<uint, Chunk.LabelId>();

            // TEST
            //if (signal_items.Count != 0) 
            {
                
                foreach (HashSerializer.Entry entry in hash.Entries)
                {
                    data.AppendUInt32(entry.Key);

                    object item = signal_items.ElementAt((int)entry.Index);
                    if (item is ControlSignalAnimation)
                    {
                        data.AppendUInt32(0);
                    }
                    else
                    if (item is ControlSignalClip)
                    {
                        data.AppendUInt32(0);
                    }
                    else
                    if (item is ControlSignalExpression)
                    {
                        data.AppendUInt32(0);
                    }
                    else
                    if (item is ControlSignalFilter)
                    {
                        data.AppendUInt32(0);
                    }
                    else
                    if (item is ControlSignalFrame)
                    {
                        data.AppendUInt32(0);
                    }
                    else
                    if (item is ControlSignalParameterizedMotion)
                    {
                        data.AppendUInt32(0);
                    }
                    else
                    if (item is ControlSignalFloat)
                    {
                        data.AppendFloat((item as ControlSignalFloat).Default);
                    }
                    else
                    if (item is ControlSignalBool)
                    {
                        data.AppendBool((item as ControlSignalBool).Default);
                    }
                    else
                    {
                        data.AppendUInt32(0);
                    }

                    Chunk.LabelId target = Chunk.ReserveLabel();
                    data.AppendOffset(target);

                    targets.Add(entry.Key, target);
                }
            }

            // event data
            data.AppendLabel(events);
            foreach (uint e in event_crcs)
            {
                data.AppendUInt32(e);
            }

            // operator signal data
            data.AppendLabel(opsignals);

            foreach (Guid guid in op_signals)
            {
                //int id = GetRandomId(guid);
                int? id = GetId(guid);
                data.AppendInt32(id.Value);
            }

            // operator data
            data.AppendLabel(ops);

            List<Chunk.LabelId> operator_ids = new List<Chunk.LabelId>();
            foreach (OpNode op in opnodes)
            {
                Chunk.LabelId operator_id = Chunk.ReserveLabel();
                data.AppendOffset(operator_id);
                operator_ids.Add(operator_id);
            }

            int operator_idx = 0;
            foreach (OpNode op in opnodes)
            {
                data.AppendLabel(operator_ids[operator_idx]);
                ++operator_idx;

                RegisterOperator registered = (RegisterOperator)Runtime.Instance.FindRegisteredOperator(op.Type);

                data.AppendInt32(registered.Id);

                int flags = 0;

                List<OperatorSource> inputs = op.Anchors.OfType<OperatorSource>().ToList();
                uint?[] child_data = new uint?[inputs.Count];
                for (int i = 0; i < inputs.Count; ++i)
                {
                    OperatorSource source = inputs[i];
                    if (source.IsOperator(node.Connections.OfType<OperatorConnection>().ToList()))
                    {
                        child_data[i] = source.GetOperator(
                            node.Connections.OfType<OperatorConnection>().ToList(), 
                            node.Operators.OfType<OpNode>().ToList());

                        flags |= (1 << (i * 4));
                    }
                    else if (source.IsSignal(node.Connections.OfType<OperatorSourceConnection>().ToList()))
                    {
                        child_data[i] = source.GetSignal(
                            node.Connections.OfType<OperatorSourceConnection>().ToList(),
                            node.Database,
                            remaps);

                        flags |= (2 << (i * 4));
                    }
                }

                data.AppendInt32(flags);

                // need to output some flags for operator construction
                /*
                List<Guid> sigs = new List<Guid>();

                foreach (OperatorResultConnection connect in op_connections)
                {
                    if (op.HasAnchor(connect.Source))
                    {
                        sigs.Add(connect.Source);
                    }
                }

                data.AppendInt32(sigs.Count);
                */
                //Chunk.LabelId signal_label = Chunk.ReserveLabel();
                //data.AppendOffset(signal_label);

                int? id = GetId(op.Id);
                data.AppendInt32(id.Value);

                IEnumerable<OperatorResultConnection> item_connect = 
                    from orc in op_connections where op.HasAnchor(orc.Source) select orc;
                if (item_connect.SingleOrDefault() != null)
                {
                    int? signal = GetSignalId(item_connect.Single().Source);
                    data.AppendInt32(signal.Value);
                }
                else 
                {
                    data.AppendInt32(0);
                }
            
                
                registered.OnSerialize(op, data, _Database);

                // output children data.

                foreach (uint? item in child_data)
                {
                    data.AppendUInt32(item.Value);
                }

                //data.AppendLabel(signal_label);
                /*
                foreach (Guid sig in sigs)
                {
                    int? id = GetId(sig);
                    
                    data.AppendInt32(id.Value);
                }
                 */
            }

            // connect data
            data.AppendLabel(connects);

            //_Connects.Add(node, new List<Guid>());

            // TEST
            //if (signal_items.Count != 0)
#if NO_CONNECTS
            {
                foreach (HashSerializer.Entry entry in hash.Entries)
                {
                    if (entry.Key == 0xffffffff)
                        continue;

                    ControlSignal item = signal_items.ElementAt((int)entry.Index);
                    List<SignalConnection> cs = dict[item];

                    Chunk.LabelId target = targets[entry.Key];
                    data.AppendLabel(target);

                    Dictionary<Guid, Chunk.LabelId> labels = new Dictionary<Guid, Chunk.LabelId>();
                    foreach (SignalConnection connect in cs)
                    {
                        // this is a hack to prevent this kind of signal happening for a clip
                        //  eventually everything will be like the clip
                        ILogicNode ignore = node.Children.SingleOrDefault(
                            delegate(ILogicNode another_child)
                            {
                                return (another_child.Type == "Clip" /*|| another_child.Type == "Anim"*/) && (another_child as LogicNodeBase).HasAnchor(connect.Sink);
                            }
                        );

                        if (ignore != null)
                            continue;

                        //_Connects[node].Add(connect.Sink);

                        if (labels.ContainsKey(connect.Id))
                            data.AppendLabel(labels[connect.Id]);

                        ushort signal = (ushort)hash.Get(item.Name);
                        ILogicNode child = children.Find(
                            delegate(ILogicNode n) { return (n as LogicNodeBase).HasAnchor(connect.Sink); }
                        );
                        byte childidx = (byte)children.IndexOf(child);

                        LogicNode logic = (LogicNode)child;
                        RegisterNode registered = SerializerHelpers.FindRegisteredNode(logic);
                        ushort ntype = (ushort)registered.Id;

                        IAnchor anchor = logic.GetAnchor(connect.Sink);
                        string name = anchor.Name;
                        uint? sig = registered.OnFindSignal(name);
                        ushort stype = (ushort)sig.Value;

                        byte propidx = (byte)logic.GetAnchorIndex(name, connect.Sink);

                        data.AppendUInt16(ntype);
                        data.AppendUInt16(stype);
                        data.AppendUInt16(signal);
                        data.AppendUInt8(childidx);
                        data.AppendUInt8(propidx);

                        int pos = cs.IndexOf(connect) + 1;
                        if (pos != cs.Count)
                        {
                            SignalConnection next = cs[pos];
                            Chunk.LabelId label = Chunk.ReserveLabel();
                            data.AppendOffset(label);
                            labels.Add(next.Id, label);
                        }
                        else
                        {
                            data.AppendUInt32(0);
                        }

                    }
                }
            }
#endif
            if (input is LogicInputNode)
            {
                Chunk child = chunk.AppendChild();
                child.AppendLabel(GetLabel(input));
                Chunk tail = child.AppendChild();
                tail.AppendInt32(Database.NODE_TAIL);

                // id, not used but needed
                tail.AppendUInt32(0);

                _Labels.Remove(input);
            }


            foreach (ILogicNode child in node.Children)
                SerializeI(child, chunk, remaps, reference);
        }

        void Serialize(StateMachine node, Chunk writer, Dictionary<ControlSignal, ControlSignal> remaps, Reference reference)
        {
            Chunk chunk = writer.AppendChild();
            Chunk.LabelId start = GetLabel(node);
            chunk.AppendLabel(start);

            _States.Add(start);

            Chunk data = chunk.AppendChild();
            //data.AppendData(/*SOME ID*/1);
            data.AppendData(Database.NODE_STATE);

            if (reference != null)
            {
                int unid = (reference.Unid.Value << 16) | node.Unid.Value;
                data.AppendData(unid);
            }
            else
            {
                data.AppendInt32(node.Unid.Value);
            }

            data.AppendUInt32(0); // flags

            data.AppendUInt16(0); // children count

            List<StateTransition> transitions = SerializerHelpers.FindTransitionsFrom(node);
            data.AppendUInt16((ushort)transitions.Count);

            // connect count
            data.AppendUInt16(0);

            // signal count
            data.AppendUInt16(0);

            // event count
            data.AppendUInt16(0);

            // operator count
            data.AppendUInt16(0);

            // operator signal count
            data.AppendUInt16(0);

            // padding
            data.AppendUInt16(0);

            // signals
            data.AppendUInt32(0);

            // events
            data.AppendUInt32(0);

            // operators
            data.AppendUInt32(0);

            // operator signals
            data.AppendUInt32(0);

            // connects
            data.AppendUInt32(0);

            // source
            data.AppendOffset(GetLabel(node.GetDefault()), Chunk.OffsetOrigin.OffsetAddress, 4, 1);

            foreach (StateTransition transition in transitions)
                Serialize(transition, node.Parent, data, new List<uint>());

            foreach (ITransitionalNode child in node.Children)
                Serialize(child, chunk, remaps, reference);
        }

        void Serialize(StateTransition transition, StateMachine parent, Chunk writer, List<uint> event_crcs)
        {
            IRegisterTransition_Ex registered = SerializerHelpers.FindRegisteredTransition(transition);

            uint size =
                (uint)Marshal.SizeOf(typeof(uint)) +
                (uint)Marshal.SizeOf(typeof(uint)) +
                (uint)Marshal.SizeOf(typeof(ushort)) +
                (uint)Marshal.SizeOf(typeof(ushort)) +
                (uint)Marshal.SizeOf(typeof(float)) +
                (uint)Marshal.SizeOf(typeof(int));
            foreach (ICondition condition in transition.Conditions)
                size += SizeOf(condition);

            writer.AppendData(registered.Id);
            writer.AppendUInt32(size);
            
            ushort conditions = (ushort)transition.Conditions.Count;
            writer.AppendUInt16(conditions);

            //registered.OnSerialize(transition, writer);

            ITransitionalNode sink = SerializerHelpers.FindNode(transition.Sink, parent);
            writer.AppendOffset(GetLabel(sink), Chunk.OffsetOrigin.OffsetAddress, 4, 1);

            //registered.Serialize.__call__(new object[] { transition, writer });

            foreach (ICondition condition in transition.Conditions)
                Serialize(condition, writer, parent.Database, event_crcs);

        }

        void Serialize(SignalConnection signal, MotionTree parent, Chunk writer, Dictionary<ControlSignal, ControlSignal> remaps)
        {
            ILogicNode source = SerializerHelpers.FindNodeWithAnchor(signal.Source, parent);

            // TODO: this could be coming from a control signal, or an operator node!!!

            ILogicNode sink = SerializerHelpers.FindNodeWithAnchor(signal.Sink, parent);

            // type 0 is the output of one animation phase into another (Type = 1, Source = node id, Sink = node id)
            // type 1 is the output of a control signal into the weight of a blend node

            if (source != null && sink != null)
            {
                if (source.Type == "Anim" && source.Type == "Anim")
                    writer.AppendInt32(0);

                //writer.AppendData(GetLabel(source).Value);
                writer.AppendData(Unid.Generate(source));
                //writer.AppendData(GetLabel(sink).Value);
                writer.AppendData(Unid.Generate(sink));
            }
            else
            if (source == null && sink != null)
            {
                Database database = Runtime.Instance.Databases[parent.Database];
                IEnumerable<ControlSignal> item =
                    from control in database.ControlSignals where control.Id == signal.Source select control;

                if (item.Count() != 0)
                {
                    ControlSignal control = item.ElementAt(0);
                    if (remaps != null)
                        control = remaps[control];

                    IAnchor anchor = SerializerHelpers.GetAnchor(sink, signal.Sink);

                    // TODO: find a better way to do this!!!
                    if (sink.Type == "Add2")
                    {
                        if (anchor.Name.Equals("Weight"))
                            writer.AppendInt32(17);
                        else
                        if (anchor.Name.Equals("Filter"))
                            writer.AppendInt32(28);
                    }
                    else
                    if (sink.Type == "AddN")
                    {
                        int index = (sink as LogicNodeBase).GetAnchors("Weight").IndexOf(anchor);
                        int type = (index << 27) | 16;
                        writer.AppendInt32(type);
                    }
                    else
                    if (sink.Type == "Anim")
                    {
                        if (anchor.Name.Equals("Phase"))
                            writer.AppendInt32(2);
                        else
                        if (anchor.Name.Equals("Anim"))
                            writer.AppendInt32(3);
                        else 
                        if(anchor.Name.Equals("Rate"))
                            writer.AppendInt32(8);
                        else 
                        if (anchor.Name.Equals("Time"))
                            writer.AppendInt32(9);
                        else 
                        if (anchor.Name.Equals("Delta"))
                            writer.AppendInt32(10);
                        else 
                        if (anchor.Name.Equals("Loop"))
                            writer.AppendInt32(18);
                        else 
                        if (anchor.Name.Equals("Absolute"))
                            writer.AppendInt32(19);
                          
                        //writer.AppendInt32(2);
                    }
                    else
                    if (sink.Type == "Blend2")
                    {
                        if (anchor.Name.Equals("Weight"))
                            writer.AppendInt32(1);
                        else 
                        if (anchor.Name.Equals("Filter"))
                            writer.AppendInt32(29);
                    }
                    else
                    if (sink.Type == "BlendN")
                    {
                        int index = (sink as LogicNodeBase).GetAnchors("Weight").IndexOf(anchor);
                        int type = (index << 27) | 15;
                        writer.AppendInt32(type);
                    }
                    else 
                    if (sink.Type == "Clip")
                    {
                        if (anchor.Name.Equals("Clip"))
                            writer.AppendInt32(4);
                        else
                        if (anchor.Name.Equals("Phase"))
                            writer.AppendInt32(11);
                        else 
                        if (anchor.Name.Equals("Rate"))
                            writer.AppendInt32(12);
                        else
                        if (anchor.Name.Equals("Time"))
                            writer.AppendInt32(13);
                        else 
                        if (anchor.Name.Equals("Delta"))
                            writer.AppendInt32(14);
                    }
                    else 
                    if (sink.Type == "Expr")
                    {
                        if (anchor.Name.Equals("Expression"))
                            writer.AppendInt32(5);
                    }
                    else 
                    if (sink.Type == "Extrapolate")
                    {
                        if (anchor.Name.Equals("Damping"))
                            writer.AppendInt32(20);
                        else 
                        if (anchor.Name.Equals("Delta Time"))
                            writer.AppendInt32(21);
                        else 
                        if (anchor.Name.Equals("Last Frame"))
                            writer.AppendInt32(22);
                        else 
                        if (anchor.Name.Equals("Own Last Frame"))
                            writer.AppendInt32(23);
                        else 
                        if (anchor.Name.Equals("Delta Frame"))
                            writer.AppendInt32(24);
                        else 
                        if (anchor.Name.Equals("Own Delta Frame"))
                            writer.AppendInt32(25);
                    }
                    else 
                    if (sink.Type == "Filter")
                    {
                        if (anchor.Name.Equals("Filter"))
                            writer.AppendInt32(6);
                    }
                    else
                    if (sink.Type == "Frame")
                    {
                        if (anchor.Name.Equals("Frame"))
                            writer.AppendInt32(26);
                        else 
                        if (anchor.Name.Equals("Owner"))
                            writer.AppendInt32(27);
                    }
                    else 
                    if(sink.Type == "Pm")
                    {
                        if (anchor.Name.Equals("Pm"))
                            writer.AppendInt32(7);
                    }
                    else
                    if (sink.Type == "Reference")
                    {
                        return; // there is no connection to a reference
                    }
                    else
                    {
                        writer.AppendInt32(-1);
                    }

                    writer.AppendData(Runtime.Database.ControlSignals.IndexOf(control));
                    //writer.AppendData(GetLabel(sink).Value);
                    writer.AppendData(Unid.Generate(sink));
                }
            }
        }

        void Serialize(SignalConnection signal, StateMachine parent, Chunk writer)
        {
            ITransitionalNode source = SerializerHelpers.FindNode(signal.Source, parent);
            //writer.AppendData(GetLabel(source).Value);
            writer.AppendData(Unid.Generate(source));
            ITransitionalNode sink = SerializerHelpers.FindNode(signal.Sink, parent);
            //writer.AppendData(GetLabel(sink).Value);
            writer.AppendData(Unid.Generate(sink));
        }

        void Serialize(MessageConnection message, MotionTree parent, Chunk writer)
        {
            ILogicNode source = SerializerHelpers.FindNode(message.Source, parent);
            //writer.AppendData(GetLabel(source).Value);
            writer.AppendData(Unid.Generate(source));
            ILogicNode sink = SerializerHelpers.FindNode(message.Sink, parent);
            //writer.AppendData(GetLabel(sink).Value);
            writer.AppendData(Unid.Generate(sink));
        }

        void Serialize(MessageConnection message, StateMachine parent, Chunk writer)
        {
            ITransitionalNode source = SerializerHelpers.FindNode(message.Source, parent);
            //writer.AppendData(GetLabel(source).Value);
            writer.AppendData(Unid.Generate(source));
            ITransitionalNode sink = SerializerHelpers.FindNode(message.Sink, parent);
            //writer.AppendData(GetLabel(sink).Value);
            writer.AppendData(Unid.Generate(sink));
        }

        void SerializeI(ILogicNode node, Chunk writer, Dictionary<ControlSignal, ControlSignal> remaps, Reference reference)
        {
            if (node is LogicNode)
                Serialize(node as LogicNode, writer, remaps, reference);
            else
            if (node is LogicStateNode)
                Serialize(node as LogicStateNode, writer, remaps, reference);
            else
            if (node is Reference)
                Serialize(node as Reference, writer);
        }

        void Serialize(LogicStateNode node, Chunk writer, Dictionary<ControlSignal, ControlSignal> remaps, Reference reference)
        {
            Chunk chunk = writer.AppendChild();
            chunk.AppendLabel(GetLabel(node));

            Chunk data = chunk.AppendChild();

            IEnumerable<TransformPassthrough> results = node.Anchors.OfType<TransformPassthrough>();
            if (results.Count() != 0)
            {
                // inline state
                //data.AppendData(2);
                data.AppendData(Database.NODE_INLINE);

                // id, nothing but needed
                data.AppendUInt32(0);

                TransformPassthrough result = results.ElementAt(0);

                LogicNodeBase source = SerializerHelpers.FindSource(node, result);
                data.AppendOffset(GetLabel(source), Chunk.OffsetOrigin.OffsetAddress, 4, 1);
                data.AppendOffset(GetLabel(node.StateMachine.GetDefault()), Chunk.OffsetOrigin.OffsetAddress, 4, 1);

                foreach (ITransitionalNode child in node.StateMachine.Children)
                {
                    Serialize(child, remaps, reference);

                    // write out a tail for each relavant input node
                    if (child is MotionTree)
                    {
                        MotionTree motiontree = (MotionTree)child;
                        IEnumerable<TransformSource> sources = motiontree.Anchors.OfType<TransformSource>();
                        if (sources.Count() != 0)
                        {
                            TransformSource transform = sources.ElementAt(0);

                            IEnumerable<LogicInputNode> inputs = _Labels.Keys.OfType<LogicInputNode>();
                            IEnumerable<LogicInputNode> items =
                                from input in inputs where input.Id == transform.Id select input;

                            /*
                            if (items.Count() != 0)
                            {
                                Chunk tail = chunk.AppendChild();
                                tail.AppendLabel(GetLabel(items.ElementAt(0)));

                                Chunk taild = tail.AppendChild();
                                taild.AppendData(9); // magic tail number!!!
                                
                            }
                             */
                        }
                    }
                }
            }
            else
            {
                //data.AppendData(/*SOME ID*/0);
                data.AppendData(Database.NODE_INDIRECT);

                // id, nothing but needded
                data.AppendUInt32(0);

                data.AppendOffset(GetLabel(node.StateMachine.GetDefault()), Chunk.OffsetOrigin.OffsetAddress, 4, 1);

                foreach (ITransitionalNode child in node.StateMachine.Children)
                    Serialize(child, remaps, reference);
            }
        }

        void Serialize(LogicNode node, Chunk writer, Dictionary<ControlSignal, ControlSignal> remaps, Reference reference)
        {
            Chunk chunk = writer.AppendChild();
            chunk.AppendLabel(GetLabel(node));

            Chunk data = chunk.AppendChild();

            RegisterNode registered = SerializerHelpers.FindRegisteredNode(node);
            data.AppendData(registered.Id);
            //data.AppendData(GetLabel(node).Value);
            data.AppendData(Unid.Generate(node));

            ILogicNode child = _Children[node.Parent].Find(
                delegate(ILogicNode n) { return n.Id == node.Id; }
            );

            if (child != null)
            {
                uint index = (uint)((1 << 31) | _Children[node.Parent].IndexOf(child));
                data.AppendUInt32(index);
            }
            else
            {
                data.AppendUInt32(0);
            }

            registered.OnSerialize(node, data, this);
            //registered.Serialize.__call__(new object[] { node, data, this });

            while (_Stack.Count != 0)
            {
                ILogicNode popped = _Stack.Pop();
                Chunk tail = chunk.AppendChild();
                tail.AppendLabel(GetLabel(popped));

                Chunk taild = tail.AppendChild();
                taild.AppendData(Database.NODE_TAIL);

                // id, not used but needed
                taild.AppendUInt32(0);
            }
        }

        void Serialize(Reference node, Chunk writer)
        {
            Dictionary<ControlSignal, ControlSignal> remaps = CreateControlSignalRemaps(node);

            if (node.Database.Root is MotionTree)
                Serialize(node.Database.Root as MotionTree, writer, remaps, node);
            else
            if (node.Database.Root is StateMachine)
                Serialize(node.Database.Root as StateMachine, writer, remaps, node);
        }

        Dictionary<ControlSignal, ControlSignal> CreateControlSignalRemaps(Reference node)
        {
            // use the name string to dereference the id...
            Dictionary<ControlSignal, ControlSignal> remap = new Dictionary<ControlSignal, ControlSignal>();

            MotionTree parent = node.Parent;
            Database pdb = Runtime.Instance.Databases[parent.Database];

            foreach (ControlSignal control in pdb.ControlSignals)
            {
                IEnumerable<SignalConnection> connections =
                    from connect in parent.Connections.OfType<SignalConnection>() where connect.Source == control.Id select connect;
                foreach (SignalConnection connection in connections)
                {
                    foreach (AnchorSource source in node.Anchors.OfType<AnchorSource>())
                    {
                        if (connection.Sink == source.Id)
                        {
                            Database database = node.Database;
                            IEnumerable<ControlSignal> signals =
                                from signal in database.ControlSignals where signal.Name == source.Name select signal;

                            if (signals.Count() != 0)
                            {
                                ControlSignal controlSignal = signals.ElementAt(0);
                                remap.Add(controlSignal, control);
                            }
                        }
                    }

                }
            }

            return remap;
        }

        void Serialize(ICondition condition, Chunk writer, Guid database, List<uint> event_crcs)
        {
            RegisterCondition registered = SerializerHelpers.FindRegisteredCondition(condition);
            writer.AppendData(registered.Id);
            Database db = Runtime.Instance.Databases[database];
            registered.OnSerialize(condition as Rage.Move.Core.Dg.ConditionBase, writer, db, event_crcs);
        }

        uint SizeOf(ICondition condition)
        {
            RegisterCondition registered = 
                SerializerHelpers.FindRegisteredCondition(condition);
            return registered.SizeOf();
        }

        public Chunk.LabelId GetLabel(object key)
        {
            if (_Labels.ContainsKey(key))
                return _Labels[key];

            Chunk.LabelId label = Chunk.ReserveLabel();
            _Labels.Add(key, label);

            return label;
        }
    }

    public static class SerializerHelpers
    {
        public static List<StateTransition> FindTransitionsFrom(MotionTree node)
        {
            List<StateTransition> transitions = new List<StateTransition>();
            
            if (node.Parent == null)
                return transitions;

            foreach (StateTransition transition in node.Parent.Transitions)
            {
                if (transition.Source == node.Id)
                    transitions.Add(transition);
            }

            return transitions;
        }

        public static List<StateTransition> FindTransitionsFrom(StateMachine node)
        {
            List<StateTransition> transitions = new List<StateTransition>();
            if (node.Parent != null)
            {
                foreach (StateTransition transition in node.Parent.Transitions)
                {
                    if (transition.Source == node.Id)
                        transitions.Add(transition);
                }
            }

            return transitions;
        }

        public static object FindTransformResult(MotionTree tree)
        {
            Guid id = Guid.Empty;
            foreach (TransformResult result in tree.Anchors.OfType<TransformResult>())
            {
                if (result.Name == "Result")
                {
                    id = result.Id;
                    break;
                }
            }

            if (id == Guid.Empty)
                return null;

            Guid source = Guid.Empty;
            foreach (IConnection connection in tree.Connections)
            {
                if (connection.Sink == id)
                {
                    source = connection.Source;
                    break;
                }
            }

            if (source == Guid.Empty)
                return null;

            foreach (LogicNodeBase child in tree.Children)
            {
                foreach (IAnchor anchor in child.Anchors.OfType<ITransformResult>())
                {
                    if (anchor.Id == source)
                    {
                        // we need to be trickier than this if we want to fold references up properly. this just causes bugs.
                        //if (child is Reference)
                        //    return (child as Reference).Database.Root.GetInitial();
                        if (child is Reference)
                            return (child as Reference).Database.Root;
                        else
                            return child;
                    }
                }
            }

            // does it reference itself???

            foreach (TransformSource transform in tree.Anchors.OfType<TransformSource>())
            {
                if (transform.Name == "Source")
                    return new LogicInputNode(transform.Id);
            }

            return null;
        }

        public static ILogicNode FindNodeWithAnchor(Guid id, MotionTree parent)
        {
            foreach (LogicNodeBase child in parent.Children)
            {
                IEnumerable<IAnchor> item =
                    from anchor in child.Anchors where anchor.Id == id select anchor;

                if (item.Count() != 0)
                    return child;
            }

            return null;
        }

        public static ILogicNode FindNode(Guid id, MotionTree parent)
        {
            foreach (ILogicNode child in parent.Children)
            {
                if (child.Id == id)
                    return child;
            }

            return null;
        }

        public static LogicNodeBase FindSource(LogicNodeBase node, AnchorBase anchor)
        {
            MotionTree parent = node.Parent;

            Guid source = Guid.Empty;
            foreach (IConnection connection in parent.Connections)
            {
                if (connection.Sink == anchor.Id)
                {
                    source = connection.Source;
                    break;
                }
            }

            if (source != Guid.Empty)
            {
                foreach (LogicNodeBase child in parent.Children)
                {
                    IEnumerable<IAnchor> items =
                        from item in child.Anchors where item.Id == source select item;

                    if (items.Count() != 0)
                        return child;
                }
            }

            return null;
        }

        public static IAnchor GetAnchor(ILogicNode parent, Guid id)
        {
            LogicNodeBase node = (LogicNodeBase)parent;

            IEnumerable<IAnchor> anchors =
                from anchor in node.Anchors where anchor.Id == id select anchor;

            if (anchors.Count() != 0)
                return anchors.ElementAt(0);

            return null;
        }

        public static ITransitionalNode FindNodeWithAnchor(Guid id, StateMachine parent)
        {
            foreach (ITransitionalNode child in parent.Children)
            {
                if (child is MotionTree)
                {
                    MotionTree node = child as MotionTree;

                    IEnumerable<IAnchor> item =
                        from anchor in node.Anchors where anchor.Id == id select anchor;

                    if (item.Count() != 0)
                        return node;
                }

                if (child is StateMachine)
                {
                    StateMachine node = child as StateMachine;

                    IEnumerable<IAnchor> item =
                        from anchor in node.Anchors where anchor.Id == id select anchor;

                    if (item.Count() != 0)
                        return node;
                }
            }

            return null;
        }

        public static ITransitionalNode FindNode(Guid id, StateMachine parent)
        {
            foreach (ITransitionalNode child in parent.Children)
            {
                if (child.Id == id)
                    return child;
            }

            return null;
        }

        public static RegisterCondition FindRegisteredCondition(ICondition condition)
        {
            foreach (RegisterCondition registered in Runtime.Instance.RegisteredConditions)
            {
                if (registered.Name == condition.Name)
                    return registered;
            }

            return null;
        }

        public static RegisterNode FindRegisteredNode(LogicNode node)
        {
            /*
            foreach (RegisterNode registered in Runtime.Instance.RegisteredNodes.OfType<RegisterNode>())
            {
                // THIS NAME IS WRONG!!!
                if (registered.Type == node.Type)
                    return registered;
            }
             */

            return null;
        }

        public static IRegisterTransition_Ex FindRegisteredTransition(StateTransition transition)
        {
            foreach (IRegisterTransition_Ex registered in Runtime.Instance.RegisteredTransitions)
            {
                if (registered.Name == transition.Name)
                    return registered;
            }

            return null;
        }

        public static bool IsSinkReference(MotionTree node, Guid anchor)
        {
            ILogicNode logic = FindNodeWithAnchor(anchor, node);
            if (logic is Reference)
                return true;

            return false;
        }
    }

    public class Unid
    {
        // final key, path that generated them...
        static Dictionary<uint, List<Guid>> Unids = new Dictionary<uint,List<Guid>>();

        public static uint Generate(ILogicNode item)
        {
            uint crc = Crc32.Generate(0, item.Id.ToString());

            ITransitionalNode parent = item.Parent;
            crc = Crc32.Generate(crc, parent.Id.ToString());

            return Generate(crc, parent.Parent);
        }

        public static uint Generate(ITransitionalNode item)
        {
            uint crc = Crc32.Generate(0, item.Id.ToString());

            if (item is StateMachine)
            {
                StateMachine sm = (StateMachine)item;
                if (sm.LogicalParent != null)
                {
                    LogicStateNode parent = sm.LogicalParent;
                    crc = Crc32.Generate(crc, parent.Id.ToString());

                    return Generate(crc, parent.Parent);
                }
            }

            return Generate(crc, item.Parent);
        }

        static uint Generate(uint path, ITransitionalNode item)
        {
            if (item == null)
                return path;

            uint crc = Crc32.Generate(path, item.Id.ToString());

            if (item is StateMachine)
            {
                StateMachine sm = (StateMachine)item;
                if (sm.LogicalParent != null)
                {
                    LogicStateNode parent = sm.LogicalParent;
                    crc = Crc32.Generate(crc, parent.Id.ToString());

                    return Generate(crc, parent.Parent);
                }
            }

            return Generate(crc, item.Parent);
        }
    }
#endif
}
