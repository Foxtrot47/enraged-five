﻿using System;

namespace New.Move.Core
{
    public interface Property : ILogicProperty, IAnchor, ICloneable
    {
        PropertyInput Flags { get; }
        PropertyInput Input { get; set; }
        Signal SelectedEvent { get; set; }
        bool OutputEnabled { get; set; }
    }
}