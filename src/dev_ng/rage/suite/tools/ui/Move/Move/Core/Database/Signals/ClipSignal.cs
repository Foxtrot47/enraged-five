﻿using Move.Core;

namespace New.Move.Core
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml;

    /// <summary>
    /// Represents a control parameter that uses a clip for its value.
    /// </summary>
    [Serializable]
    public class ClipSignal : Signal, IReferredSignal
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ClipDictionaries"/> property.
        /// </summary>
        private static IEnumerable<string> clipDictionaries;

        /// <summary>
        /// The private field used for the <see cref="AbsoluteClipSets"/> property.
        /// </summary>
        private static IEnumerable<string> absoluteClipSets;

        /// <summary>
        /// The private field used for the <see cref="SelectedSource"/> property.
        /// </summary>
        private string selectedSource;

        /// <summary>
        /// The private field used for the <see cref="SelectedClipDictionary"/> property.
        /// </summary>
        private string selectedClipDictionary;

        /// <summary>
        /// The private field used for the <see cref="SelectedDictionaryClip"/> property.
        /// </summary>
        private string selectedDictionaryClip;

        /// <summary>
        /// The private field used for the <see cref="SelectedAbsoluteClipSet"/> property.
        /// </summary>
        private string selectedAbsoluateClipSet;

        /// <summary>
        /// The private field used for the <see cref="SelectedAbsoluateClip"/> property.
        /// </summary>
        private string selectedAbsoluateClip;

        /// <summary>
        /// The private field used for the <see cref="VariableClipSet"/> property.
        /// </summary>
        private string variableClipSet;

        /// <summary>
        /// The private field used for the <see cref="VariableClipName"/> property.
        /// </summary>
        private string variableClipName;

        /// <summary>
        /// The private field used for the <see cref="ClipFilePath"/> property.
        /// </summary>
        private string clipFilePath;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises static members of the <see cref="New.Move.Core.ClipSignal"/> class.
        /// </summary>
        static ClipSignal()
        {
            ClipArchiveReader archiveReader = new ClipArchiveReader();
            clipDictionaries = archiveReader.ReadClipDictionaryNames();

            ClipSetReader clipSetReader = new ClipSetReader();
            absoluteClipSets = clipSetReader.ReadClipSetNames();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ClipSignal"/> class.
        /// </summary>
        public ClipSignal()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ClipSignal"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        protected ClipSignal(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ClipSignal"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> parameter is null.
        /// </exception>
        public ClipSignal(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Enabled = true;
            this.Name = reader.GetAttribute(NameSerializationTag);
            this.Id = Guid.ParseExact(reader.GetAttribute(IdSerializationTag), "D");
            reader.Skip();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the <see cref="New.Move.Core.IDesc"/> object that describes the creation of
        /// this object.
        /// </summary>
        public override IDesc Desc
        {
            get { return new ClipDesc(this); }
        }

        /// <summary>
        /// Gets a iterator around all of the clip dictionaries that are available to this
        /// signal as a source for the selected clip dictionary.
        /// </summary>
        public IEnumerable<string> ClipDictionaries
        {
            get { return clipDictionaries; }
        }

        /// <summary>
        /// Gets a iterator around all of the absolute clip sets that are available to this
        /// signal as a source for the selected absolute clip.
        /// </summary>
        public IEnumerable<string> AbsoluteClipSets
        {
            get { return absoluteClipSets; }
        }

        /// <summary>
        /// Gets or sets the iterator around the available clips inside the selected
        /// clip dictionary.
        /// </summary>
        public IEnumerable<string> DictionaryClips
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the iterator around the available clips inside the selected
        /// absolute clip set.
        /// </summary>
        public IEnumerable<string> AbsoluteClips
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the source location for the selected clip.
        /// </summary>
        public string SelectedSource
        {
            get
            {
                return this.selectedSource;
            }

            set
            {
                this.selectedSource = value;
                this.NotifyPropertyChanged("SelectedSource");

                if (value == "Absolute Clipset")
                {
                    this.SelectedAbsoluateClipSet = this.AbsoluteClipSets.FirstOrDefault();
                }

                if (value == "Clip Dictionary")
                {
                    this.SelectedClipDictionary = this.ClipDictionaries.FirstOrDefault();
                }

                if (value == "Variable Clipset")
                {
                    this.VariableClipSet = "$default";
                }
            }
        }

        /// <summary>
        /// Gets or sets the clip dictionary that is the location for the selected clip.
        /// </summary>
        public string SelectedClipDictionary
        {
            get
            {
                return this.selectedClipDictionary;
            }

            set
            {
                this.selectedClipDictionary = value;
                this.NotifyPropertyChanged("SelectedClipDictionary");
                if (this.selectedClipDictionary == null)
                {
                    return;
                }

                ClipArchiveReader archiveReader = new ClipArchiveReader();
                this.DictionaryClips =
                    archiveReader.ReadClipNamesFromDictionary(this.selectedClipDictionary);
                this.NotifyPropertyChanged("ClipDictionaryClips");
                if (this.DictionaryClips.Count() > 0)
                {
                    this.SelectedDictionaryClip = this.DictionaryClips.First();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected clip from a clip dictionary.
        /// </summary>
        public string SelectedDictionaryClip
        {
            get
            {
                return this.selectedDictionaryClip;
            }

            set
            {
                this.selectedDictionaryClip = value;
                this.NotifyPropertyChanged("SelectedClipDictionaryClip");
            }
        }

        /// <summary>
        /// Gets or sets the absolute clip set that is the location for the selected clip.
        /// </summary>
        public string SelectedAbsoluateClipSet
        {
            get
            {
                return this.selectedAbsoluateClipSet;
            }

            set
            {
                this.selectedAbsoluateClipSet = value;
                this.NotifyPropertyChanged("SelectedAbsoluateClip");
                if (this.selectedAbsoluateClipSet == null)
                {
                    return;
                }

                ClipSetReader clipSetReader = new ClipSetReader();
                this.AbsoluteClips =
                    clipSetReader.ReadClipSetItems(this.selectedAbsoluateClipSet);
                this.NotifyPropertyChanged("AbsoluteClipClips");
                this.NotifyPropertyChanged("AbsoluteClips");
                if (this.AbsoluteClips.Count() > 0)
                {
                    this.SelectedAbsoluateClip = this.AbsoluteClips.First();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected clip from a absolute clip set.
        /// </summary>
        public string SelectedAbsoluateClip
        {
            get
            {
                return this.selectedAbsoluateClip;
            }

            set
            {
                this.selectedAbsoluateClip = value;
                this.NotifyPropertyChanged("SelectedAbsoluateClip");
            }
        }

        /// <summary>
        /// Gets or sets the variable clip set that is the location for the selected clip.
        /// </summary>
        public string VariableClipSet
        {
            get
            {
                return this.variableClipSet;
            }

            set
            {
                this.variableClipSet = value;
                this.NotifyPropertyChanged("VariableClipSet");
            }
        }

        /// <summary>
        /// Gets or sets the selected clip from the selected variable clip set.
        /// </summary>
        public string VariableClipName
        {
            get
            {
                return this.variableClipName;
            }

            set
            {
                this.variableClipName = value;
                this.NotifyPropertyChanged("VariableClipName");
            }
        }

        /// <summary>
        /// Gets or sets the full file path to the selected local clip.
        /// </summary>
        public string ClipFilePath
        {
            get
            {
                return this.clipFilePath;
            }

            set
            {
                this.clipFilePath = value;
                this.NotifyPropertyChanged("ClipFilePath");
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("clip");

            writer.WriteAttributeString(
                Signal.NameSerializationTag, this.Name);
            writer.WriteAttributeString(
                Signal.IdSerializationTag,
                this.Id.ToString("D", CultureInfo.InvariantCulture));

            writer.WriteEndElement();
        }
        
        /// <summary>
        /// Gets the parameter value for this signal that represents the type it is
        /// representing.
        /// </summary>
        /// <returns>
        /// The parameter value that this signal represents.
        /// </returns>
        public override Parameter GetParameter()
        {
            return Parameter.Clip;
        }

        /// <summary>
        /// Creates a reference to this clip signal.
        /// </summary>
        /// <returns>
        /// A new reference to this clip signal.
        /// </returns>
        public ReferenceSignal CreateReference()
        {
            return new ClipReference(this.Id, this.Name);
        }
        #endregion
    } // New.Move.Core.ExpressionSignal {Class}
} // New.Move.Core {Namespace}
