﻿//using IronPython.Runtime;
//using IronPython.Runtime.Calls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
//using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Point = System.Windows.Point;

using Rage.Move.Core;
using Rage.Move.Utils;

using MoVE.Core;

using ITransitional = New.Move.Core.ITransitional;

namespace Rage.Move.Core
{
    public interface ICommandResponse
    {
        string Filename { get; set; }
    }

    public delegate void ControlSignalChangedHandler(object data);
   
    public class Runtime
    {
        public const string ApplicationFolderName = "RAGE.MOVE";

        static Dictionary<string, int> nodeids = new Dictionary<string, int>();
        static Dictionary<string, int> transitionids = new Dictionary<string, int>();
        static Dictionary<string, int> conditionids = new Dictionary<string, int>();

        public Runtime()
        {
            Instance = this;

            /*
            NetworkDesc networkDesc = new NetworkDesc();
            New.Move.Core.Motiontree.Desc.Children.Add(networkDesc);

            New.Move.Core.LogicLeafSMDesc leafDesc = new New.Move.Core.LogicLeafSMDesc();
            New.Move.Core.Motiontree.Desc.Children.Add(leafDesc);

            New.Move.Core.LogicInlineSMDesc inlineDesc = new New.Move.Core.LogicInlineSMDesc();
            New.Move.Core.Motiontree.Desc.Children.Add(inlineDesc);
             */
        }

        public static Runtime Instance { get; private set; }

        Layers _Layers = new Layers();
        public Layers Layers
        {
            get { return _Layers; }
        }

        List<New.Move.Core.ITransitionalDesc> _TransitionalDescriptions = new List<New.Move.Core.ITransitionalDesc>();
        public List<New.Move.Core.ITransitionalDesc> TransitionalDescriptions { get { return _TransitionalDescriptions; } }

        List<New.Move.Core.ITransitionDesc> _TransitionDescriptors = new List<New.Move.Core.ITransitionDesc>();
        public List<New.Move.Core.ITransitionDesc> TransitionDescriptors { get { return _TransitionDescriptors; } }

        List<New.Move.Core.IConditionDesc> _RegisteredConditions = new List<New.Move.Core.IConditionDesc>();
        public List<New.Move.Core.IConditionDesc> RegisteredConditions { get { return _RegisteredConditions; } }

        public New.Move.Core.ITransitionalDesc FindRegisteredTransitionalNode(string name)
        {
            foreach (New.Move.Core.ITransitionalDesc node in _TransitionalDescriptions)
            {
                if (node.Name.Equals(name))
                    return node;
            }

            return null;
        }

        public New.Move.Core.ITransitionDesc FindRegisteredTransition(string type)
        {
            foreach (New.Move.Core.ITransitionDesc transition in _TransitionDescriptors)
            {
                if (transition.Name.Equals(type))
                    return transition;
            }

            return null;
        }

        public New.Move.Core.IConditionDesc FindRegisteredCondition(string type)
        {
            foreach (New.Move.Core.IConditionDesc condition in _RegisteredConditions)
            {
                if (condition.Name.Equals(type))
                    return condition;
            }

            return null;
        }
     
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]

        Dictionary<uint, ControlSignalChangedHandler> _ControlSignalHandlers = new Dictionary<uint, ControlSignalChangedHandler>();

        public void RegisterControlSignalHandler(uint id, ControlSignalChangedHandler handler)
        {
            _ControlSignalHandlers.Add(id, handler);
        }

        void Runtime_Linked()
        {

        }
    }
}