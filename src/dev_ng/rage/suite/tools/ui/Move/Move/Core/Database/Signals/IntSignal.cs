﻿namespace New.Move.Core
{
    using System;
    using System.Globalization;
    using System.Runtime.Serialization;
    using System.Xml;

    /// <summary>
    /// Represents a control parameter that uses a integer for its value.
    /// </summary>
    [Serializable]
    public class IntSignal : Signal, IReferredSignal
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private int value;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.IntSignal"/> class.
        /// </summary>
        public IntSignal()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.IntSignal"/> class using
        /// the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        protected IntSignal(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.IntSignal"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> parameter is null.
        /// </exception>
        public IntSignal(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Enabled = true;
            this.Name = reader.GetAttribute(NameSerializationTag);
            this.Id = Guid.ParseExact(reader.GetAttribute(IdSerializationTag), "D");
            this.Value = int.Parse(
                reader.GetAttribute(ValueSerializationTag), CultureInfo.InvariantCulture);
            reader.Skip();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the <see cref="New.Move.Core.IDesc"/> object that describes the creation of
        /// this object.
        /// </summary>
        public override IDesc Desc
        {
            get { return new IntDesc(this); }
        }

        /// <summary>
        /// Gets or sets the value this parameter is currently set to.
        /// </summary>
        public int Value
        {
            get
            {
                return this.value;
            }

            set
            {
                if (value == this.value)
                {
                    return;
                }

                this.value = value;
                this.NotifyPropertyChanged("Value");
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("int");

            writer.WriteAttributeString(
                Signal.NameSerializationTag, this.Name);
            writer.WriteAttributeString(
                Signal.IdSerializationTag,
                this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                Signal.ValueSerializationTag,
                this.Value.ToString(CultureInfo.InvariantCulture));

            writer.WriteEndElement();
        }

        /// <summary>
        /// Gets the parameter value for this signal that represents the type it is
        /// representing.
        /// </summary>
        /// <returns>
        /// The parameter value that this signal represents.
        /// </returns>
        public override Parameter GetParameter()
        {
            return Parameter.Int;
        }

        /// <summary>
        /// Creates a reference to this real signal.
        /// </summary>
        /// <returns>
        /// A new reference to this real signal.
        /// </returns>
        public ReferenceSignal CreateReference()
        {
            return new IntReference(this.Id, this.Name);
        }
        #endregion
    } // New.Move.Core.IntSignal {Class}
} // New.Move.Core {Namespace}
