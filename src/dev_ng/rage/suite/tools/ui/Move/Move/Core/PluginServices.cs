﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

namespace Rage.Move
{
    public class PluginServices : IPluginHost
    {
        public PluginServices()
        {
        }

        List<Assembly> _AvailableAssemblies = new List<Assembly>();
        
        Plugins _AvailablePlugins = new Plugins();
        public Plugins AvailablePlugins
        {
            get { return _AvailablePlugins; }
            set { _AvailablePlugins = value; }
        }

        public void LoadPlugins(string plugpath)
        {
            FindPlugins(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, plugpath));
        }

        void FindPlugins(string plugins)
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            _AvailablePlugins.Clear();

            Queue<string> paths = new Queue<string>();
            paths.Enqueue(plugins);

            while (paths.Count != 0)
            {
                string path = paths.Dequeue();

                foreach (string plug in Directory.GetFiles(path, "*.plug"))
                {
                    AddPlugin(plug);
                }

                foreach (string directory in Directory.GetDirectories(path))
                {
                    paths.Enqueue(directory);
                }
            }
        }

        public void ClosePlugins()
        {
            foreach (Plugin plugin in _AvailablePlugins)
            {
                plugin.Instance.Dispose();
                plugin.Instance = null;
            }

            _AvailablePlugins.Clear();
            _AvailableAssemblies.Clear();
        }

        void AddPlugin(string plug)
        {
            string directory = Path.GetDirectoryName(plug); 

            using (FileStream fs = new FileStream(plug, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(fs);

                    XmlNodeList assemblies = doc.GetElementsByTagName("assembly");
                    XmlNode assemblyitem = assemblies.Item(0);

                    string name = assemblyitem.Attributes.GetNamedItem("name").Value;
                    string path = Path.Combine(directory, name);
                    Assembly assembly = Assembly.LoadFrom(path);
                    _AvailableAssemblies.Add(assembly);

                    XmlNodeList directories = doc.GetElementsByTagName("virtualdirectory");
                    foreach (XmlNode directoryitem in directories)
                    {
                        XmlNode pathitem = directoryitem.Attributes.GetNamedItem("path");
                        string virtualdir = pathitem.Value;

                        foreach (XmlNode classitem in directoryitem.SelectNodes("class"))
                        {
                            XmlNode nameitem = classitem.Attributes.GetNamedItem("name");
                            string classname = nameitem.Value;

                            Type type = assembly.GetType(classname);
                            if (type != null)
                            {
                                Plugin plugin = new Plugin();
                                plugin.AssemblyPath = path;

                                plugin.Instance = (IPlugin)Activator.CreateInstance(type);
                                plugin.Instance.Host = this;
                                plugin.Instance.Initialize();

                                _AvailablePlugins.Add(plugin);

                                plugin = null;
                            }
                        }
                        assembly = null;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs e)
        {
            foreach (Assembly assembly in _AvailableAssemblies)
            {
                if (assembly.FullName.Equals(e.Name))
                    return assembly;
            }

            return null;
        }
    }

    public class Plugins : CollectionBase
    {
        public void Add(Plugin plugin)
        {
            List.Add(plugin);
        }

        public void Remove(Plugin plugin)
        {
            List.Remove(plugin);
        }

        public Plugin Find(string name)
        {
            Plugin ret = null;

            foreach (Plugin plugin in List)
            {
                if (plugin.Instance.Name.Equals(name) || plugin.AssemblyPath.Equals(name))
                {
                    ret = plugin;
                    break;
                }
            }

            return ret;
        }
    }

    public class Plugin
    {
        public Plugin()
        {
            Instance = null;
            AssemblyPath = "";
        }

        public IPlugin Instance { get; set; }
        public string AssemblyPath { get; set; }
    }
}