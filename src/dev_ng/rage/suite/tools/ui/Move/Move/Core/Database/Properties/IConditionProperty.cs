﻿namespace New.Move.Core
{
    public interface IConditionProperty : IProperty
    {
        New.Move.Core.Condition Parent { get; set; }
    }
}