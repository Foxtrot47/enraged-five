﻿namespace New.Move.Core
{
    public interface ITransitionDesc
    {
        string Name { get; }
        int Id { get; }
        string Help { get; }

        ITransition Create(ITransitional source, ITransitional dest);
    }
}