﻿using System.Collections.Generic;
using System.Windows;

namespace New.Move.Core
{
    public interface ITransitionalDesc : IDesc
    {
        ITransitional Create(Database database, Point offset);
        //List<IDesc> Children { get; }
    }
}