﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public abstract class LogicalPropertyPair<T0, T1> : ILogicPropertyPair, ISerializable
    {
        public LogicalPropertyPair()
        {
            _First = (T0)Activator.CreateInstance(typeof(T0));
            _Second = (T1)Activator.CreateInstance(typeof(T1));
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        public object Tag { get; set; }

        public object First { get { return _First; } }
        protected T0 _First;

        public object Second { get { return _Second; } }
        protected T1 _Second;

        public virtual ILogic Parent { get; set; }

        public void RemoveConnections(INode parent)
        {
            // hack because the actual Parent has already been nulled
            Motiontree motionTree = parent.Database.Find(parent.Parent) as Motiontree;
            if (_First is IProperty)
            {
                motionTree.RemoveConnectionsFromProperty(_First as IProperty);
            }
            else if (_First is IAnchor)
            {
                motionTree.RemoveConnectionsFromAnchor(_First as IAnchor);
            }

            if (_Second is IProperty)
            {
                motionTree.RemoveConnectionsFromProperty(_Second as IProperty);
            }
            else if (_Second is IAnchor)
            {
                motionTree.RemoveConnectionsFromAnchor(_Second as IAnchor);
            }
        }

        public void SetParentOnChildren()
        {
            if (_First is ILogicProperty)
            {
                (_First as ILogicProperty).Parent = Parent;
            }
            else if (_First is IAnchor)
            {
                (_First as IAnchor).Parent = Parent;
            }

            if (_Second is ILogicProperty)
            {
                (_Second as ILogicProperty).Parent = Parent;
            }
            else if (_Second is IAnchor)
            {
                (_Second as IAnchor).Parent = Parent;
            }
        }

        public bool Contains(IProperty property)
        {
            if (_First as IProperty == property)
            {
                return true;
            }
            if (_Second as IProperty == property)
            {
                return true;
            }

            return false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string First = "First";
            public static string Second = "Second";
            public static string Parent = "Parent";
        }

        public LogicalPropertyPair(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            _First = (T0)info.GetValue(SerializationTag.First, typeof(T0));
            _Second = (T1)info.GetValue(SerializationTag.Second, typeof(T1));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.First, _First);
            info.AddValue(SerializationTag.Second, _Second);
            info.AddValue(SerializationTag.Parent, Parent);
        }

        public abstract object Clone();
    }
}