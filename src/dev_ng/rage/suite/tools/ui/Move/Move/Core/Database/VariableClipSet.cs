﻿using Move.Core.Restorable;

namespace New.Move.Core
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml;

    /// <summary>
    /// Represents a variable clip set definition that can be referenced by the clip
    /// property as a clip set source.
    /// </summary>
    [Serializable]
    public class VariableClipSet : RestorableObject, ISerializable, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        private const string NameSerialisationTag = "Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        private const string IdSerialisationTag = "Id";

        /// <summary>
        /// The private field for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field used to the <see cref="UseIcon"/> property.
        /// </summary>
        private int usedCount = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.VariableClipSet"/>
        /// class with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the variable clip set.
        /// </param>
        public VariableClipSet(string name)
        {
            this.name = name;
            this.id = Guid.NewGuid();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.VariableClipSet"/>
        /// class as a copy of the specified instance.
        /// </summary>
        /// <param name="name">
        /// The instance to copy.
        /// </param>
        public VariableClipSet(VariableClipSet other)
        {
            this.name = other.name;
            this.id = Guid.NewGuid();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.VariableClipSet"/> class
        /// with the specified name and identifier.
        /// </summary>
        /// <param name="name">
        /// The name of the flag.
        /// </param>
        /// <param name="id">
        /// The identifier this flag will have.
        /// </param>
        public VariableClipSet(Guid id, string name)
        {
            this.name = name;
            this.id = id;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.VariableClipSet"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public VariableClipSet(SerializationInfo info, StreamingContext context)
        {
            this.name = (string)info.GetValue(NameSerialisationTag, typeof(string));
            this.id = (Guid)info.GetValue(IdSerialisationTag, typeof(Guid));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.VariableClipSet"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> parameter is null.
        /// </exception>
        public VariableClipSet(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.name = reader.GetAttribute(NameSerialisationTag);
            this.id = Guid.ParseExact(reader.GetAttribute(IdSerialisationTag), "D");
            reader.Skip();
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs whenever a value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the name this variable clip set can be referenced by.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.Notify("Name");
            }
        }

        /// <summary>
        /// Gets or sets the unique global identifier for this variable clip set.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets the image source used for the icon to display to the user if the signal is
        /// currently not used throughout its owning database.
        /// </summary>
        public ImageSource UsedIcon
        {
            get
            {
                if (this.usedCount > 0)
                {
                    return null;
                }
                else
                {
                    BitmapImage bitmapImage = new BitmapImage();

                    try
                    {
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource =
                            Assembly.GetExecutingAssembly().GetManifestResourceStream(
                            "Move.Core.Images.NotUsed.png");
                        bitmapImage.EndInit();
                        return bitmapImage;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the string representation of this instance.
        /// </summary>
        /// <returns>
        /// The string representation of this instance.
        /// </returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("clipset");

            writer.WriteAttributeString(
                NameSerialisationTag, this.Name);
            writer.WriteAttributeString(
                IdSerialisationTag,
                this.Id.ToString("D", CultureInfo.InvariantCulture));

            writer.WriteEndElement();
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialize this instance.
        /// </summary>
        /// <param name="info">
        /// The serialization info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerialisationTag, Name);
            info.AddValue(IdSerialisationTag, Id);
        }

        /// <summary>
        /// Fires the property changed event for the property with the specified name. 
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that has changed.
        /// </param>
        protected void Notify(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null)
                return;

            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Increases or decreases a reference count that is used to either show or find
        /// the "NotUsed" icon to the user.
        /// </summary>
        /// <param name="used">
        /// A value indicating whether to increase or decrease the reference count.
        /// </param>
        public void SetUsed(bool used)
        {
            if (used)
            {
                this.usedCount++;
            }
            else
            {
                this.usedCount--;
            }

            Notify("UsedIcon");
        }
        #endregion
    } // New.Move.Core.VariableClipSet
} // New.Move.Core