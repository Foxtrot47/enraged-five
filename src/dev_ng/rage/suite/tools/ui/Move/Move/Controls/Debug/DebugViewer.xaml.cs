﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rage.Move.Core;
using Rage.Move.Core.Dag;
using Rage.Move.Core.Dg;
using Rage.Move.Utils;

namespace Rage.Move
{
    public class DebugItems : StackPanel
    {
        protected override Size MeasureOverride(Size available)
        {
            Size size = new Size(double.PositiveInfinity, double.PositiveInfinity);

            foreach (UIElement element in InternalChildren)
                element.Measure(size);

            Rect bounds = new Rect(new Point(Double.MaxValue, Double.MaxValue), new Size());
            bounds.X = 0.0;
            bounds.Y = 0.0f;

            foreach (Bar bar in Children.OfType<Bar>())
            {
                double left = Canvas.GetLeft(bar);
                bounds.Width = Math.Max(bounds.Width, left + bar.DesiredSize.Width);
                double height = Canvas.GetTop(bar);
                bounds.Height = Math.Max(bounds.Height, height + bar.DesiredSize.Height);
            }

            return new Size(bounds.Width + 150.0f, bounds.Height);
        }
    }

    public class DebugSignal
    {
        public DebugSignal(uint id, string label)
        {
            Id = id;
            Label = label;
        }

        public string Label { get; set; }
        public uint Id { get; set; }
    }

    public class DebugSignalAnimation : DebugSignal 
    {
        public DebugSignalAnimation(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalClip : DebugSignal
    {
        public DebugSignalClip(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalExpression : DebugSignal
    {
        public DebugSignalExpression(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalFilter : DebugSignal 
    {
        public DebugSignalFilter(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalFilterN : DebugSignal
    {
        public DebugSignalFilterN(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalFrame : DebugSignal
    {
        public DebugSignalFrame(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalPm : DebugSignal
    {
        public DebugSignalPm(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalFloat : DebugSignal
    {
        public DebugSignalFloat(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalBool : DebugSignal
    {
        public DebugSignalBool(uint id, string label)
            : base(id, label) { }
    }

    public class DebugSignalData : DebugSignal
    {
        public DebugSignalData(uint id, string label)
            : base(id, label) { }
    }

    public class Bar : Canvas
    {
        static Random _Random = new Random();
        public Bar()
        {
            Visibility = Visibility.Collapsed;

            Height = 13.0f;
            Width = 0.0f;
        }

        public void Add()
        {
            Add(Color.FromRgb(
                    (byte)_Random.Next(0, 255),
                    (byte)_Random.Next(0, 255),
                    (byte)_Random.Next(0, 255)));
        }

        public void Add(Color color)
        {
            _Rect = new Rectangle();
            _Rect.Height = 11.0f;
            _Rect.Width = 2.0f;

            _Rect.HorizontalAlignment = HorizontalAlignment.Left;
            _Rect.VerticalAlignment = VerticalAlignment.Center;
            _Rect.StrokeThickness = 1;
            _Rect.Fill = new SolidColorBrush(color);

            Canvas.SetLeft(_Rect, Width);
            Canvas.SetTop(_Rect, 0.0f);

            Children.Add(_Rect);

            //Visibility = Visibility.Collapsed;

            Height = _Rect.Height + 2.0f;
            Width += _Rect.Width;
        }

        public void Stop()
        {
            _Rect = null;
        }

        public void Tick()
        {
            if (_Rect.Width == 2.0f)
                _Rect.Width = 10.0f;
            else
                _Rect.Width += 10.0f;
        }

        public void End()
        {
            if (Width == 2.0f)
                Width = 10.0f;
            else
                Width += 10.0f;
        }

        protected Rectangle _Rect;
    }

    public class StateBar : Bar {}
    public class AnimationBar : Bar 
    {
        public void Add(string anim)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = anim;
            _Rect.ToolTip = tooltip;
        }
    }
    public class ClipBar : Bar 
    {
        public void Add(string clip)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = clip;
            _Rect.ToolTip = tooltip;
        }
    }

    public class ExpressionBar : Bar 
    {
        public void Add(int expression)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = String.Format("0x{0:X8}", expression);
            _Rect.ToolTip = tooltip;
        }
    }

    public class FilterBar : Bar 
    {
        public void Add(int filter)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = String.Format("0x{0:X8}", filter);
            _Rect.ToolTip = tooltip;
        }
    }

    public class FilterNBar : Bar 
    {
        public void Add(int filtern)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = String.Format("0x{0:X8}", filtern);
            _Rect.ToolTip = tooltip;
        }
    }

    public class FrameBar : Bar 
    {
        public void Add(int frame)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = String.Format("0x{0:X8}", frame);
            _Rect.ToolTip = tooltip;
        }
    }

    public class PmBar : Bar 
    {
        public void Add(int pm)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = String.Format("0x{0:X8}", pm);
            _Rect.ToolTip = tooltip;
        }
    }

    public class RealBar : Bar 
    {
        public void Add(float real)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = real.ToString();
            _Rect.ToolTip = tooltip;
        }
    }

    public class BooleanBar : Bar 
    {
        public void Add(bool boolean)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = boolean.ToString();
            _Rect.ToolTip = tooltip;
        }
    }

    public class DataBar : Bar 
    {
        public void Add(int data)
        {
            Add();
            ToolTip tooltip = new ToolTip();
            tooltip.Content = String.Format("0x{0:X8}", data);
            _Rect.ToolTip = tooltip;
        }
    }

    public class DebugRow : Canvas
    {
        private float _Labels = 20.0f;
        public DebugRow(string label, int id)
        {
            Label = label;
            Id = id;

            Height = 20.0f;
            Width = 0.0f;

            text = new Label();
            text.Content = Label;

            Canvas.SetLeft(text, Width + 10.0f);
            Canvas.SetTop(text, 0.0f);

            Children.Add(text);

            _Signals.CollectionChanged += new NotifyCollectionChangedEventHandler(
                delegate(object sender, NotifyCollectionChangedEventArgs e)
                {
                    if (e.Action == NotifyCollectionChangedAction.Add)
                    {
                        foreach (DebugSignal signal in e.NewItems)
                        {
                            Label l = new Label();
                            l.Content = signal.Label;

                            Canvas.SetLeft(l, Width + 10.0f);
                            Canvas.SetTop(l, _Labels);
                            _Labels += 20.0f;

                            Children.Add(l);
                        }
                    }
                }
            );
        }

        ObservableCollection<DebugSignal> _Signals = new ObservableCollection<DebugSignal>();
        public ObservableCollection<DebugSignal> Signals { get { return _Signals; } }

        public void Add()
        {
            rect = new Rectangle();
            rect.Height = 20.0f;
            rect.Width = 2.0f;
            rect.HorizontalAlignment = HorizontalAlignment.Left;
            rect.VerticalAlignment = VerticalAlignment.Center;
            rect.StrokeThickness = 1;
            rect.Fill = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            Canvas.SetLeft(rect, Width);
            Canvas.SetTop(rect, 0.0f);

            Children.Add(rect);

            rect.MouseLeftButtonUp += new MouseButtonEventHandler(Rectangle_MouseLeftButtonUp);
            rect.MouseEnter += new MouseEventHandler(Rectangle_MouseEnter);
            rect.MouseLeave += new MouseEventHandler(Rectangle_MouseLeave);

            Width += rect.Width;
        }

        public void Update()
        {
            if (rect.Width == 2.0f)
                rect.Width = 10.0f;
            else
                rect.Width += 10.0f;
        }

        public void PostUpdate()
        {
            if (Width == 2.0f)
                Width = 10.0f;
            else 
                Width += 10.0f;

            Canvas.SetLeft(text, Width + 10.0f);
        }

        void Rectangle_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Rectangle rect = (sender as Rectangle);
        }

        void Rectangle_MouseEnter(object sender, MouseEventArgs e)
        {
            
        }

        void Rectangle_MouseLeave(object sender, MouseEventArgs e)
        {
        }

        public string Label { get; private set; }
        public int Id { get; private set; }

        Label text;
        Rectangle rect;
    }

    public static class SignalBar
    {
        public static Bar Create(this ControlSignal item, DebugViewer viewer, int state)
        {
            Int64 id = (Int64)state << 32;
            id |= Crc32.Generate(0, item.Name);

            if (item is ControlSignalAnimation)
            {
                AnimationBar bar = new AnimationBar();
                viewer.Animations.Add(id, bar);
                return bar;
            }
            else if (item is ControlSignalClip)
            {
                ClipBar bar = new ClipBar();
                viewer.Clips.Add(id, bar);
                return bar;
            }
            else if (item is ControlSignalExpression)
            {
                ExpressionBar bar = new ExpressionBar();
                viewer.Expressions.Add(id, bar);
                return bar;
            }
            else if (item is ControlSignalFilter)
            {
                FilterBar bar = new FilterBar();
                viewer.Filters.Add(id, bar);
                return bar;
            }
            else if (item is ControlSignalFrame)
            {
                FrameBar bar = new FrameBar();
                viewer.Frames.Add(id, bar);
                return bar;
            }
            else if (item is ControlSignalParameterizedMotion)
            {
                PmBar bar = new PmBar();
                viewer.Pms.Add(id, bar);
                return bar;
            }
            else if (item is ControlSignalFloat)
            {
                RealBar bar = new RealBar();
                viewer.Floats.Add(id, bar);
                return bar;
            }
            else if (item is ControlSignalBool)
            {
                BooleanBar bar = new BooleanBar();
                viewer.Booleans.Add(id, bar);
                return bar;
            }
            // data & controlsignalfiltern

            return null;
        }
    }

    public partial class DebugViewer : UserControl
    {
        public DebugViewer()
        {
            InitializeComponent();

            Canvas.Width = 0.0f;
        }

        void TreeViewItem_IsVisibilityChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            TreeViewItem item = (TreeViewItem)sender;
            Bar bar = (Bar)item.Tag;

            if ((bool)args.NewValue == true)
                bar.Visibility = Visibility.Visible;
            else
                bar.Visibility = Visibility.Collapsed;
        }

#if no_db
        void AddChildItems(Database db, TreeViewItem parent, ITransitionalNode node)
        {
            if (node is MotionTree)
            {
                MotionTree motiontree = (MotionTree)node;
                foreach (ILogicNode ln in motiontree.Children)
                {
                    if (ln is LogicStateNode)
                    {
                        StateMachine statemachine = (ln as LogicStateNode).StateMachine;
                        foreach (ITransitionalNode child in statemachine.Children)
                        {
                            TreeViewItem item = new TreeViewItem();
                            item.Header = child.Name;
                            parent.Items.Add(item);
                            item.Tag = new StateBar();
                            Canvas.Children.Add(item.Tag as Bar);
                            item.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);

                            int id = db.DEBUG_HACK_NODE_IDS[child];
                            State state = new State(item.Tag as StateBar);
                            _States.Add(id, state);

                            if (child is MotionTree)
                            {
                                if (db.DEBUG_HACK_SIGNALS.ContainsKey(child))
                                {
                                    List<ControlSignal> signals = db.DEBUG_HACK_SIGNALS[child];
                                    foreach (ControlSignal signal in signals)
                                    {
                                        TreeViewItem i = new TreeViewItem();
                                        i.Header = signal.Name;
                                        item.Items.Add(i);
                                        Bar bar = signal.Create(this, id);
                                        state.Signals.Add(bar);
                                        i.Tag = bar;
                                        Canvas.Children.Add(bar);
                                        i.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);
                                    }
                                }
                            }

                            AddChildItems(db, item, child);
                        }
                    }
                        /*
                    else 
                    if (ln is Reference)
                    {
                        Reference reference = (Reference)ln;
                        Database database = reference.Database;
                        TreeViewItem item = new TreeViewItem();
                        item.Header = reference.Name;
                        parent.Items.Add(item);
                        item.Tag = new StateBar();
                        Canvas.Children.Add(item.Tag as Bar);
                        item.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);

                        int id = db.DEBUG_HACK_NODE_IDS[database.Root];
                        State state = new State(item.Tag as StateBar);
                        _States.Add(id, state);

                        if (database.Root is MotionTree)
                        {
                            if (db.DEBUG_HACK_SIGNALS.ContainsKey(database.Root))
                            {
                                List<ControlSignal> signals = db.DEBUG_HACK_SIGNALS[database.Root];
                                foreach (ControlSignal signal in signals)
                                {
                                    TreeViewItem i = new TreeViewItem();
                                    i.Header = signal.Name;
                                    item.Items.Add(i);
                                    Bar bar = signal.Create(this, id);
                                    state.Signals.Add(bar);
                                    i.Tag = bar;
                                    Canvas.Children.Add(bar);
                                    i.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);
                                }
                            }
                        }

                        AddChildItems(db, item, database.Root);
                    }
                         */
                }
            }
            else 
            if (node is StateMachine)
            {
                StateMachine statemachine = (StateMachine)node;
                foreach (ITransitionalNode child in statemachine.Children)
                {
                    TreeViewItem item = new TreeViewItem();
                    item.Header = child.Name;
                    parent.Items.Add(item);
                    item.Tag = new StateBar();
                    Canvas.Children.Add(item.Tag as Bar);
                    item.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);

                    int id = db.DEBUG_HACK_NODE_IDS[child];
                    State state = new State(item.Tag as StateBar);
                    _States.Add(id, state);

                    if (child is MotionTree)
                    {
                        if (db.DEBUG_HACK_SIGNALS.ContainsKey(child))
                        {
                            List<ControlSignal> signals = db.DEBUG_HACK_SIGNALS[child];
                            foreach (ControlSignal signal in signals)
                            {
                                TreeViewItem i = new TreeViewItem();
                                i.Header = signal.Name;
                                item.Items.Add(i);
                                Bar bar = signal.Create(this, id);
                                state.Signals.Add(bar);
                                i.Tag = bar;
                                Canvas.Children.Add(bar);
                                i.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);
                            }
                        }
                    }

                    AddChildItems(db, item, child);
                }
            }
        }

        public void Create(Database db, ITransitionalNode root)
        {
            if (root is MotionTree)
            {
                MotionTree motiontree = (MotionTree)root;
                foreach (LogicStateNode child in motiontree.Children.OfType<LogicStateNode>())
                {
                    StateMachine statemachine = child.StateMachine;
                    foreach (ITransitionalNode node in statemachine.Children)
                    {
                        TreeViewItem item = new TreeViewItem();
                        item.Header = node.Name;
                        Tree.Items.Add(item);
                        item.Tag = new StateBar();
                        (item.Tag as StateBar).Visibility = Visibility.Visible;
                        Canvas.Children.Add(item.Tag as Bar);
                        item.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);

                        int id = db.DEBUG_HACK_NODE_IDS[node];
                        State state = new State(item.Tag as StateBar);
                        _States.Add(id, state);

                        if (node is MotionTree)
                        {
                            if (db.DEBUG_HACK_SIGNALS.ContainsKey(node))
                            {
                                List<ControlSignal> signals = db.DEBUG_HACK_SIGNALS[node];
                                foreach (ControlSignal signal in signals)
                                {
                                    TreeViewItem i = new TreeViewItem();
                                    i.Header = signal.Name;
                                    item.Items.Add(i);
                                    Bar bar = signal.Create(this, id);
                                    state.Signals.Add(bar);
                                    i.Tag = bar;
                                    //i.Tag = new Bar(); signal.Create
                                    Canvas.Children.Add(bar);
                                    i.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);
                                }
                            }
                        }

                        AddChildItems(db, item, node);
                    }
                }
            }
            else 
            if (root is StateMachine)
            {
                StateMachine statemachine = (StateMachine)root;
                foreach (ITransitionalNode node in statemachine.Children)
                {
                    TreeViewItem item = new TreeViewItem();
                    item.Header = node.Name;
                    Tree.Items.Add(item);
                    item.Tag = new StateBar();
                    (item.Tag as StateBar).Visibility = Visibility.Visible;
                    Canvas.Children.Add(item.Tag as Bar);
                    item.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);

                    int id = db.DEBUG_HACK_NODE_IDS[node];
                    State state = new State(item.Tag as StateBar);
                    _States.Add(id, state);

                    if (node is MotionTree)
                    {
                        if (db.DEBUG_HACK_SIGNALS.ContainsKey(node))
                        {
                            List<ControlSignal> signals = db.DEBUG_HACK_SIGNALS[node];
                            foreach (ControlSignal signal in signals)
                            {
                                TreeViewItem i = new TreeViewItem();
                                i.Header = signal.Name;
                                item.Items.Add(i);
                                Bar bar = signal.Create(this, id);
                                state.Signals.Add(bar);
                                i.Tag = bar;
                                Canvas.Children.Add(bar);
                                i.IsVisibleChanged += new DependencyPropertyChangedEventHandler(TreeViewItem_IsVisibilityChanged);
                            }
                        }
                    }

                    AddChildItems(db, item, node);
                }
            }
        }
#endif


        private class State
        {
            public State(StateBar bar)
            {
                Bar = bar;
            }

            List<Bar> _Signals = new List<Bar>();
            public List<Bar> Signals { get { return _Signals; } }

            public StateBar Bar { get; set; }

            public void Update()
            {
                foreach (Bar signal in Signals)
                    signal.End();

                Bar.End();
            }
        }

        Dictionary<int, State> _States = new Dictionary<int, State>();
        Dictionary<int, State> _EnabledStates = new Dictionary<int, State>();

        public void EnableState(int id)
        {
            State state = _States[id];
            state.Bar.Add(Color.FromRgb(0, 255, 0));
            _EnabledStates.Add(id, state);
        }

        public void DisableState(int id)
        {
            State removed = _EnabledStates[id];
            foreach (Bar signal in removed.Signals)
            {
                signal.Stop();
            }
            _EnabledStates.Remove(id);
        }

        // this is just for ease of access
        Dictionary<Int64, AnimationBar> _Animations = new Dictionary<Int64, AnimationBar>();
        public Dictionary<Int64, AnimationBar> Animations { get { return _Animations; } }
        public void SetAnimation(int state, uint signal, string anim)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Animations.ContainsKey(id))
                _Animations[id].Add(anim);
        }

        // TODO: have a list of state nodes with their child bars. if the state is removed then remove them from their update
        //  buckets too. a null is a set with a null value...
        // when the state is ticked, it should tick all of its children

        Dictionary<Int64, ClipBar> _Clips = new Dictionary<Int64, ClipBar>();
        public Dictionary<Int64, ClipBar> Clips { get { return _Clips; } }
        public void SetClip(int state, uint signal, string clip)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Clips.ContainsKey(id))
                _Clips[id].Add(clip);
        }

        Dictionary<Int64, ExpressionBar> _Expressions = new Dictionary<Int64, ExpressionBar>();
        public Dictionary<Int64, ExpressionBar> Expressions { get { return _Expressions; } }
        public void SetExpressions(int state, uint signal, int expression)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Expressions.ContainsKey(id))
                _Expressions[id].Add(expression);
        }

        Dictionary<Int64, FilterBar> _Filters = new Dictionary<Int64, FilterBar>();
        public Dictionary<Int64, FilterBar> Filters { get { return _Filters; } }
        public void SetFilter(int state, uint signal, int filter)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Filters.ContainsKey(id))
                _Filters[id].Add(filter);
        }

        Dictionary<Int64, FilterNBar> _FilterNs = new Dictionary<Int64, FilterNBar>();
        public Dictionary<Int64, FilterNBar> FilterNs { get { return _FilterNs; } }
        public void SetFilterN(int state, uint signal, int filtern)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_FilterNs.ContainsKey(id))
                _FilterNs[id].Add(filtern);
        }

        Dictionary<Int64, FrameBar> _Frames = new Dictionary<Int64, FrameBar>();
        public Dictionary<Int64, FrameBar> Frames { get { return _Frames; } }
        public void SetFrame(int state, uint signal, int frame)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Frames.ContainsKey(id))
                _Frames[id].Add(frame);
        }

        Dictionary<Int64, PmBar> _Pms = new Dictionary<Int64, PmBar>();
        public Dictionary<Int64, PmBar> Pms { get { return _Pms; } }
        public void SetPm(int state, uint signal, int pm)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Pms.ContainsKey(id))
                _Pms[id].Add(pm);
        }

        Dictionary<Int64, RealBar> _Floats = new Dictionary<Int64, RealBar>();
        public Dictionary<Int64, RealBar> Floats { get { return _Floats; } }
        public void SetFloat(int state, uint signal, float real)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Floats.ContainsKey(id))
                _Floats[id].Add(real);
        }

        Dictionary<Int64, BooleanBar> _Booleans = new Dictionary<Int64, BooleanBar>();
        public Dictionary<Int64, BooleanBar> Booleans { get { return _Booleans; } }
        public void SetBool(int state, uint signal, bool boolean)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Booleans.ContainsKey(id))
                _Booleans[id].Add(boolean);
        }

        Dictionary<Int64, DataBar> _Datas = new Dictionary<Int64, DataBar>();
        public Dictionary<Int64, DataBar> Datas { get { return _Datas; } }
        public void SetData(int state, uint signal, int data)
        {
            Int64 id = (Int64)state << 32;
            id |= signal;
            if (_Datas.ContainsKey(id))
                _Datas[id].Add(data);
        }

        public int Count { get { return 0; /* Items.Children.OfType<DebugRow>().Count();*/ } }

        public void PostUpdate()
        {
            foreach (State state in _EnabledStates.Values)
            {
                foreach (Bar signal in state.Signals)
                    signal.Tick();

                state.Bar.Tick();
            }

            foreach (State state in _States.Values)
                state.Update();

            Canvas.Width += 10.0f;
            Bars.Width = Canvas.Width;
            Bars.ScrollToRightEnd();
        }
    }
}
