﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Xml;
using System.Globalization;

namespace Rockstar.MoVE.Framework
{
    [Serializable]
    public class CurveSegment : INotifyPropertyChanged, ISerializable
    {
        public CurveSegment(Point position)
        {
            Position = position;
        }

        public double X
        {
            get
            {
                return Position.X;
            }
            set
            {
                if (Position.X != value)
                {
                    Position = new Point(value, Position.Y);
                    NotifyPropertyChanged("X");
                    NotifyPropertyChanged("Position");
                }
            }
        }

        public double Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                if (Position.Y != value)
                {
                    Position = new Point(Position.X, value);
                    NotifyPropertyChanged("Y");
                    NotifyPropertyChanged("Position");
                }
            }
        }

        Point Position { get; set; }

        Color _Color;
        public Color Color
        {
            get
            {
                return _Color;
            }
            set
            {
                if (_Color != value)
                {
                    _Color = value;
                    NotifyPropertyChanged("Color");
                }
            }
        }

        bool _Visible = true;
        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                if (_Visible != value)
                {
                    _Visible = value;
                    NotifyPropertyChanged("Visible");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public Curve Curve = null;

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Position", Position);
            float[] color = new float[4];
            color[0] = _Color.ScA;
            color[1] = _Color.ScR;
            color[2] = _Color.ScG;
            color[3] = _Color.ScB;
            info.AddValue("Color", color);
            info.AddValue("Visible", _Visible);
            info.AddValue("Curve", Curve);
        }

        public CurveSegment(SerializationInfo info, StreamingContext context)
        {
            Position = (Point)info.GetValue("Position", typeof(Point));
            float[] color = (float[])info.GetValue("Color", typeof(float[]));
            _Color.ScA = color[0];
            _Color.ScR = color[1];
            _Color.ScG = color[2];
            _Color.ScB = color[3];
            _Visible = info.GetBoolean("Visible");
            Curve = (Curve)info.GetValue("Curve", typeof(Curve));
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="CurveSegment"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public CurveSegment(XmlReader reader, Curve parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Curve = parent;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("segment");
            writer.WriteAttributeString("Visible", this.Visible.ToString());

            if (this.Position != null)
            {
                writer.WriteStartElement("Position");
                writer.WriteAttributeString(
                    "x", this.Position.X.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString(
                    "y", this.Position.Y.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            writer.WriteStartElement("Colour");
            writer.WriteAttributeString(
                "a", this._Color.ScA.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "r", this._Color.ScR.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "g", this._Color.ScG.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString(
                "b", this._Color.ScB.ToString(CultureInfo.InvariantCulture));
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this._Visible = bool.Parse(reader.GetAttribute("Visible"));

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Position") == 0)
                {
                    double x = double.Parse(reader.GetAttribute("x"), CultureInfo.InvariantCulture);
                    double y = double.Parse(reader.GetAttribute("y"), CultureInfo.InvariantCulture);
                    this.Position = new Point(x, y);
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Colour") == 0)
                {
                    _Color.ScA = float.Parse(reader.GetAttribute("a"), CultureInfo.InvariantCulture);
                    _Color.ScR = float.Parse(reader.GetAttribute("r"), CultureInfo.InvariantCulture);
                    _Color.ScG = float.Parse(reader.GetAttribute("g"), CultureInfo.InvariantCulture);
                    _Color.ScB = float.Parse(reader.GetAttribute("b"), CultureInfo.InvariantCulture);
                    reader.Skip();
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }
    }
}