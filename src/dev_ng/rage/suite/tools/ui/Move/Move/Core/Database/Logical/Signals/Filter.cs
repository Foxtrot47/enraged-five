﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class Filter : SignalBase, ISerializable
    {
        public Filter(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public Filter(FilterSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Filter"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Filter(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "filtersignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new Filter(new FilterSignal());
            (clone as Filter).AdditionalText = this.AdditionalText;
            (clone as Filter).Database = this.Database;
            (clone as Filter).Enabled = this.Enabled;
            (clone as Filter).Name = this.Name;
            (clone as Filter).Position = this.Position;
            (clone as Filter).Signal = this.Signal;

            return (ISelectable)clone;
        }

    }
}