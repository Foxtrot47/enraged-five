﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace New.Move.Core
{
    

    
}

namespace Rage.Move.Core.Dg
{
    

    // Message input
    /*
    [Serializable]
    public class MessageSource : AnchorSource
    {
        public MessageSource(string name) 
            : base(name) { }

        public MessageSource()
            : base(null) { }
    }

    // Message output
    [Serializable]
    public class MessageResult : AnchorResult
    {
        public MessageResult(string name)
            : base(name) { }

        public MessageResult()
            : base(null) { }
    }
    */
    //public interface ISignal { }

    // Signal input
    /*
    [Serializable]
    public class SignalSource : AnchorSource, ISignal
    {
        public SignalSource(string name)
            : base(name) { }

        public SignalSource()
            : base(null) { }
    }
     */

    // Signal output
    /*
    [Serializable]
    public class SignalResult : AnchorResult, ISignal
    {
        public SignalResult(string name)
            : base(name) { }

        public SignalResult()
            : base(null) { }
    }
     * */
    /*
    public class SignalConverter : TypeConverter
    {
        // NOTE: I need some 'runtime' load registry.

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type type)
        {
            return base.CanConvertFrom(context, type);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type type)
        {
            return base.ConvertTo(context, culture, value, type);
        }
    }

    // operator input
    [Serializable]
    public class OperatorSource : AnchorSource, ISignal
    {
        public OperatorSource(string name)
            : base(name) { }

        public OperatorSource()
            : base(null) { }
    }

    // operator output
    [Serializable]
    public class OperatorResult : AnchorResult, ISignal
    {
        public OperatorResult(string name)
            : base(name) { }

        public OperatorResult()
            : base(null) { }
    }
    */
    // TODO: would be nice if these were extended in ipy
    /*
    [Serializable]
    public class AnimationSource : AnchorSource
    {
        public AnimationSource(string name)
            : base(name) { }

        public AnimationSource()
            : base(null) { }
    }

    [Serializable]
    public class AnimationResult : AnchorResult
    {
        public AnimationResult(string name)
            : base(name) { }

        public AnimationResult()
            : base(null) { }
    }
     */
    /*
    [Serializable]
    public class ClipSource : AnchorSource
    {
        public ClipSource(string name)
            : base(name) { }

        public ClipSource()
            : base(null) { }
    }

    [Serializable]
    public class ClipResult : AnchorResult
    {
        public ClipResult(string name)
            : base(name) { }

        public ClipResult()
            : base(null) { }
    }

    [Serializable]
    public class ExpressionSource : AnchorSource
    {
        public ExpressionSource(string name)
            : base(name) { }

        public ExpressionSource()
            : base(null) { }
    }

    [Serializable]
    public class ExpressionResult : AnchorResult
    {
        public ExpressionResult(string name)
            : base(name) { }

        public ExpressionResult()
            : base(null) { }
    }

    [Serializable]
    public class FilterSource : AnchorSource
    {
        public FilterSource(string name)
            : base(name) { }

        public FilterSource()
            : base(null) { }
    }

    [Serializable]
    public class FilterResult : AnchorResult
    {
        public FilterResult(string name)
            : base(name) { }

        public FilterResult()
            : base(null) { }
    }

    [Serializable]
    public class FrameSource : AnchorSource
    {
        public FrameSource(string name)
            : base(name) { }

        public FrameSource()
            : base(null) { }
    }

    [Serializable]
    public class FrameResult : AnchorResult
    {
        public FrameResult(string name)
            : base(name) { }

        public FrameResult()
            : base(null) { }
    }

    [Serializable]
    public class ParameterizedMotionSource : AnchorSource
    {
        public ParameterizedMotionSource(string name)
            : base(name) { }

        public ParameterizedMotionSource()
            : base(null) { }
    }

    [Serializable]
    public class ParameterizedMotionResult : AnchorResult
    {
        public ParameterizedMotionResult(string name)
            : base(name) { }

        public ParameterizedMotionResult()
            : base(null) { }
    }
    */


    // Transform input


    

    // Transform output


    // Control signal input
    // need different types for animation, expression etc
    /*
    [Serializable]
    public abstract class ControlSignal : AnchorSource
    {
        public ControlSignal(string name)
            : base(name) { }

        public ControlSignal()
            : base(null) { }
    }

    [Serializable]
    public class ControlSignalFloat : ControlSignal
    {
        public ControlSignalFloat(string name)
            : base(name) 
        {
            Default = 0.0f;
            Min = 0.0f;
            Max = 1.0f;
        }

        public ControlSignalFloat()
            : base(null) { }

        public float Min { get; set; }
        public float Max { get; set; }
        public float Default { get; set; }
    }

    [Serializable]
    public class ControlSignalBool : ControlSignal
    {
        public ControlSignalBool(string name)
            : base(name) { }

        public ControlSignalBool()
            : base(null) { }

        public bool Default { get; set; }
    }

    [Serializable]
    public class ControlSignalAnimation : ControlSignal
    {
        public ControlSignalAnimation(string name)
            : base(name) { }

        public ControlSignalAnimation()
            : base(null) { }
    }

    [Serializable]
    public class ControlSignalClip : ControlSignal
    {
        public ControlSignalClip(string name)
            : base(name) { }

        public ControlSignalClip()
            : base(null) { }
    }

    [Serializable]
    public class ControlSignalExpression : ControlSignal
    {
        public ControlSignalExpression(string name)
            : base(name) { }

        public ControlSignalExpression()
            : base(null) { }
    }

    [Serializable]
    public class ControlSignalFilter : ControlSignal
    {
        public ControlSignalFilter(string name)
            : base(name) { }

        public ControlSignalFilter()
            : base(null) { }
    }

    [Serializable]
    public class ControlSignalFrame : ControlSignal
    {
        public ControlSignalFrame(string name)
            : base(name) { }

        public ControlSignalFrame()
            : base(null) { }
    }

    [Serializable]
    public class ControlSignalParameterizedMotion : ControlSignal
    {
        public ControlSignalParameterizedMotion(string name)
            : base(name) { }

        public ControlSignalParameterizedMotion()
            : base(null) { }
    }

    public class ControlSignalConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type type)
        {
            if (type == typeof(string))
                return true;

            return base.CanConvertFrom(context, type);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                Guid id = new Guid(value as string);

                // this is a horrible hack, but it's assumed the last database is the current one!!!

                //Database database = Runtime.Instance.Databases.Last().Value;

                //foreach (Database database in Runtime.Instance.Databases.Values)
                {
                    /*
                    IEnumerable<ControlSignal> item =
                        from signal in database.ControlSignals where signal.Id == id select signal;

                    if (item.Count() != 0)
                        return item.ElementAt(0);
                     
                }

                return null;
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type type)
        {
            if (type == typeof(string))
                return (value as ControlSignal).Id.ToString();

            return base.ConvertTo(context, culture, value, type);
        }
    }

    public class MessageConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type type)
        {
            if (type == typeof(string))
                return true;

            return base.CanConvertFrom(context, type);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                Guid id = new Guid(value as string);
                /*
                Database database = Runtime.Instance.Databases.Last().Value;

                IEnumerable<Message> item = 
                    from signal in database.Messages where signal.Id == id select signal;

                if (item.Count() != 0)
                    return item.ElementAt(0);
                
                return null;
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type type)
        {
            //if (type == typeof(string))
              //  return (value as Message).Id.ToString();

            return base.ConvertTo(context, culture, value, type);
        }
    }
     */
}
