﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Move.Core.Restorable
{
    internal class RestorableTransaction : IRestorableAction, IDisposable
    {
        public static RestorableTransaction Create(IActionManager manager)
        {
            RestorableTransaction transaction = new RestorableTransaction();
            transaction._Actions = new List<IRestorableAction>();
            transaction._Manager = manager;

            manager.PushHandler(transaction.UndoRedoPropertyChanged);

            return transaction;
        }

        internal static RestorableTransaction Create()
        {
            RestorableTransaction transaction = new RestorableTransaction();
            transaction._Actions = new List<IRestorableAction>();

            return transaction;
        }

        public void Undo()
        {
            foreach (IRestorableAction action in _Actions.Reverse<IRestorableAction>())
            {
                action.Undo();
            }
        }

        public void Redo()
        {
            foreach (IRestorableAction action in _Actions)
            {
                action.Redo();
            }
        }

        public void Dispose()
        {
            _Manager.Push(this);
            _Manager.PopHandler();
        }

        void UndoRedoPropertyChanged(object sender, RestorablePropertyChangedEventArgs e)
        {
            _Actions.Add(e.Action);
        }

        List<IRestorableAction> _Actions;
        IActionManager _Manager;
    }
}