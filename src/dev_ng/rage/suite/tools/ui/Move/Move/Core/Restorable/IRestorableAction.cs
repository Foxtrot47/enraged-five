﻿namespace Move.Core.Restorable
{
    public interface IRestorableAction
    {
        void Undo();
        void Redo();
    }
}