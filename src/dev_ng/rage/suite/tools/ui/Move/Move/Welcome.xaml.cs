﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rage.Move
{
    public partial class Welcome : UserControl
    {
        public Welcome()
        {
            InitializeComponent();

            CreateRecentFiles();
            DisplayVersion();
        }

        public static readonly RoutedEvent NewStateMachineButtonClickEvent = EventManager.RegisterRoutedEvent(
           "NewStateMachineButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Welcome));

        public static readonly RoutedEvent NewMotiontreeButtonClickEvent = EventManager.RegisterRoutedEvent(
           "NewMotiontreeButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Welcome));

        public static readonly RoutedEvent OpenButtonClickEvent = EventManager.RegisterRoutedEvent(
            "OpenButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Welcome));

        public static readonly RoutedEvent OpenRecentFileButtonClickEvent = EventManager.RegisterRoutedEvent(
            "OpenRecentFileButtonClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Welcome));
        
        public event RoutedEventHandler NewStateMachineButtonClick
        {
            add { AddHandler(NewStateMachineButtonClickEvent, value); }
            remove { RemoveHandler(NewStateMachineButtonClickEvent, value); }
        }

        public event RoutedEventHandler NewMotiontreeButtonClick
        {
            add { AddHandler(NewMotiontreeButtonClickEvent, value); }
            remove { RemoveHandler(NewMotiontreeButtonClickEvent, value); }
        }

        public event RoutedEventHandler OpenButtonClick
        {
            add { AddHandler(OpenButtonClickEvent, value); }
            remove { RemoveHandler(OpenButtonClickEvent, value); }
        }

        public event RoutedEventHandler OpenRecentFileButtonClick
        {
            add { AddHandler(OpenRecentFileButtonClickEvent, value); }
            remove { RemoveHandler(OpenRecentFileButtonClickEvent, value); }
        }

        private void NewStateMachineButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(NewStateMachineButtonClickEvent));
        }

        private void NewMotiontreeButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(NewMotiontreeButtonClickEvent));
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(OpenButtonClickEvent));
        }

        private void OpenRecentFile_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(OpenRecentFileButtonClickEvent, sender));
        }

        private void CreateRecentFiles()
        {
            foreach (string file in App.RecentFiles)
            {
                Button fileButton = new Button();
                fileButton.Content = System.IO.Path.GetFileName(file);
                fileButton.CommandParameter = file;
                fileButton.Style = (Style)FindResource("RecentFileButtonStyle");
                fileButton.Click += new RoutedEventHandler(OpenRecentFile_Click);

                RecentFilesStackPanel.Children.Add(fileButton);
            }
        }

        private void DisplayVersion()
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            VersionLabel.Content += String.Format(CultureInfo.CurrentCulture,
                "{0}.{1}.{2}", version.Major, version.Minor, version.Build);
        }
    }
}
