﻿using System;

using Move.Utils;

namespace New.Move.Core
{
    public interface IAnchor
    {
        string Name { get; set; }
        Guid Id { get; }

        ILogic Parent { get; set; }

        void CheckBeforeRemoveConnections(Motiontree from);
        void RemoveConnections();

        NoResetObservableCollection<Connection> Connections { get; }

        object Tag { get; set; }
    }
}