﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using System.Net;
using System.Net.Sockets;

using Move;
using Move.Utils;
using Rage.Move.Core;
using Rage.Move.Utils;

using New.Move.Core;

using AvalonDock;

namespace Rage.Move
{
    internal class DatabaseDocument : DocumentContent
    {
        public DatabaseDocument(string title, Database database)
        {
            Title = database.IsDirty ? String.Concat(title, "*") : title;
            Tag = database;

            database.DirtyChanged += new RoutedEventHandler(Database_DirtyChanged);
            database.FullyQualifiedFilenameChanged +=
                new RoutedEventHandler(Database_FullyQualifiedFilenameChanged);

            Closed += new EventHandler(DatabaseDocument_Closed);
        }

        void Database_DirtyChanged(object sender, RoutedEventArgs e)
        {
            Title = Database.IsDirty ? String.Concat(Title, "*") : Title.TrimEnd('*');
        }

        void Database_FullyQualifiedFilenameChanged(object sender, RoutedEventArgs e)
        {
            string filename = Path.GetFileNameWithoutExtension(Database.FullyQualifiedFilename);
            Title = Database.IsDirty ? String.Concat(filename, "*") : filename.TrimEnd('*');
        }

        void DatabaseDocument_Closed(object sender, EventArgs e)
        {
            Database database = Tag as Database;
            database.OnClosing();

            database.DirtyChanged -= new RoutedEventHandler(Database_DirtyChanged);
            database.FullyQualifiedFilenameChanged -=
                new RoutedEventHandler(Database_FullyQualifiedFilenameChanged);

            database.OnClosed();
        }

        Database Database { get { return Tag as Database; } }
    }

    public partial class MainWindow : Window
    {
        public static RoutedCommand EditRequests = new RoutedCommand();
        public static RoutedCommand EditFlags = new RoutedCommand();
        public static RoutedCommand EditSignals = new RoutedCommand();
       
        readonly static string LayoutFilePath =
           Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
           Path.Combine(Properties.Resources.ApplicationFolderName, "Layout.xml"));

        delegate void VoidDelegate();

        public MainWindow()
        {
            InitializeComponent();
            /*
            foreach (var window in App.Instance.WindowList)
            {
                window.Create(this);
            }
             */

            HideEditorPanes();
            ShowWelcomeScreen();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            App.OnInitialized();

            DockingManagerControl.ActiveDocumentChanging += new EventHandler(
                delegate(object sender, EventArgs ea) { OnActiveDocumentChanging(sender); }
            );

            DockingManagerControl.ActiveDocumentChanged += new EventHandler(
                delegate(object sender, EventArgs ea) { OnActiveDocumentChanged(sender); }
            );

            TargetsMenu.DataContext = App.RuntimeTargets;

            App.Client.Connect += App_OnConnected;
            App.Client.Disconnect += App_OnDisconnected;

#if DEBUG
            AddMoveDebugMenu();
#endif 
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            SaveLayout();
            e.Cancel = PromptToSaveAll();

            base.OnClosing(e);
        }

        public EventHandler ActiveDocumentChanging;
        void OnActiveDocumentChanging(object document)
        {
            if (ActiveDocumentChanging != null)
                ActiveDocumentChanging(document, EventArgs.Empty);
        }
        
        public EventHandler ActiveDocumentChanged;
        void OnActiveDocumentChanged(object document)
        {
            if (ActiveDocumentChanged != null)
                ActiveDocumentChanged(document, EventArgs.Empty);
        }

        void LoadLayout()
        {
            if (File.Exists(LayoutFilePath))
            {
                using (TextReader reader = new StreamReader(LayoutFilePath))
                {
                    DockingManagerControl.RestoreLayout(reader);
                }
            }
        }

        void SaveLayout()
        {
            string path = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Properties.Resources.ApplicationFolderName);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            using (TextWriter writer = new StreamWriter(LayoutFilePath))
            {
                DockingManagerControl.SaveLayout(writer);
            }
        }

        bool PromptToSave(string name, Database database)
        {
            if (!database.IsDirty)
                return false;

            MessageBoxResult result = MessageBox.Show(String.Concat(name, Properties.Resources.NotSavedMessage),
            Properties.Resources.NotSaved, MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

            if (result == MessageBoxResult.Yes)
            {
                if (String.IsNullOrEmpty(database.FullyQualifiedFilename))
                {
                    CommonDialog dialog = new CommonDialog();
                    dialog.InitialDirectory = Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                        Properties.Resources.ApplicationFolderName);
                    dialog.Filter.Add(new FilterEntry(Properties.Resources.Files, Properties.Resources.FileExtension));
                    dialog.Filter.Add(new FilterEntry(Properties.Resources.AllFiles, Properties.Resources.AllExtension));
                    dialog.Title = Properties.Resources.SaveAs;
                    dialog.DefaultExtension = Properties.Resources.Extension;
                    dialog.ShowSave();

                    if (!string.IsNullOrEmpty(dialog.Filename))
                    {
                        database.Save(dialog.Filename);

                        if (!App.RecentFiles.Contains(database.FullyQualifiedFilename))
                        {
                            App.RecentFiles.Add(database.FullyQualifiedFilename);
                            //BuildOpenMenu();
                        }
                    }
                }
                else 
                {
                    database.Save();

                    App.RecentFiles.Remove(database.FullyQualifiedFilename);
                    App.RecentFiles.Insert(0, database.FullyQualifiedFilename);
                }
            }
            else if (result == MessageBoxResult.Cancel)
            {
                return true;
            }

            return false;
        }

        bool PromptToSaveAll()
        {
            foreach (DocumentContent document in DocumentHost.Items)
            {
                if (PromptToSave(document.Title, document.Tag as Database))
                {
                    return true;
                }
            }

            return false;
        }

        void ShowWelcomeScreen()
        {
            WelcomeControl.Visibility = Visibility.Visible;
        }

        void HideWelcomeScreen()
        {
            WelcomeControl.Visibility = Visibility.Hidden;
        }

        void ShowEditorPanes()
        {
            ContentControl.Visibility = Visibility.Visible;
        }

        void HideEditorPanes()
        {
            ContentControl.Visibility = Visibility.Hidden;
        }

        void DockManager_Loaded(object sender, RoutedEventArgs e)
        {
            LoadLayout();
        }

        void WelcomeControl_NewStateMachineButtonClick(object sender, RoutedEventArgs e)
        {
            HideWelcomeScreen();
            ShowEditorPanes();

            Database database = new Database(StateMachine.Desc);

            DatabaseDocument doc = new DatabaseDocument(GetDefaultFileName(), database);
            doc.Closing += new EventHandler<CancelEventArgs>(DocumentContent_Closing);

            App.OnDocumentCreated(doc);

            DocumentHost.Items.Add(doc);
            DocumentHost.SelectedItem = doc;
        }

        void WelcomeControl_NewMotiontreeButtonClick(object sender, RoutedEventArgs e)
        {
            HideWelcomeScreen();
            ShowEditorPanes();

            Database database = new Database(Motiontree.Desc);

            DatabaseDocument doc = new DatabaseDocument(GetDefaultFileName(), database);
            doc.Closing += new EventHandler<CancelEventArgs>(DocumentContent_Closing);
            
            App.OnDocumentCreated(doc);

            DocumentHost.Items.Add(doc);
            DocumentHost.SelectedItem = doc;
        }

        void WelcomeControl_OpenButtonClick(object sender, RoutedEventArgs e)
        {
            HideWelcomeScreen();
            ShowEditorPanes();

            CommonDialog dialog = new CommonDialog();
            dialog.InitialDirectory = New.Move.Core.Database.ApplicationFolderPath;
            dialog.Filter.Add(new FilterEntry(Properties.Resources.Files, Properties.Resources.FileExtension));
            dialog.Filter.Add(new FilterEntry(Properties.Resources.AllFiles, Properties.Resources.AllExtension));
            dialog.Title = Properties.Resources.Open;
            dialog.ShowOpen();

            if (!String.IsNullOrEmpty(dialog.Filename))
            {
                New.Move.Core.Database database = new New.Move.Core.Database();
                database.Load(dialog.Filename);

                DatabaseDocument doc = new DatabaseDocument(Path.GetFileNameWithoutExtension(dialog.Filename), database);
                doc.Closing += new EventHandler<CancelEventArgs>(DocumentContent_Closing);

                App.OnDocumentCreated(doc);

                DocumentHost.Items.Add(doc);
                DocumentHost.SelectedItem = doc;

                App.RecentFiles.Remove(database.FullyQualifiedFilename);
                App.RecentFiles.Insert(0, database.FullyQualifiedFilename);
                //BuildOpenMenu();
            }
        }

        void WelcomeControl_OpenRecentFileButtonClick(object sender, RoutedEventArgs e)
        {
            HideWelcomeScreen();
            ShowEditorPanes();

            Button item = (Button)e.OriginalSource;
            string file = item.CommandParameter as string;

            if (!String.IsNullOrEmpty(file))
            {
                New.Move.Core.Database database = new New.Move.Core.Database();
                database.Load(file);

                DatabaseDocument doc = new DatabaseDocument(Path.GetFileNameWithoutExtension(file), database);
                doc.Closing += new EventHandler<CancelEventArgs>(DocumentContent_Closing);

                App.OnDocumentCreated(doc);

                DocumentHost.Items.Add(doc);
                DocumentHost.SelectedItem = doc;

                App.RecentFiles.Remove(file);
                App.RecentFiles.Insert(0, file);
                //BuildOpenMenu();
            }
        }

        void ExportMenu_Click(object sender, RoutedEventArgs e)
        {
            CommonDialog dialog = new CommonDialog();
            dialog.InitialDirectory = Database.ApplicationFolderPath;
            dialog.Filter.Add(new FilterEntry(Properties.Resources.ExportFiles, Properties.Resources.ExportExtension));
            dialog.Filter.Add(new FilterEntry(Properties.Resources.AllFiles, Properties.Resources.AllExtension));
            dialog.Title = Properties.Resources.Export;
            dialog.DefaultExtension = Properties.Resources.ExportExtension;
            dialog.ShowSave();

            if (!String.IsNullOrEmpty(dialog.Filename))
            {
                using (BinaryFileStream stream = new BinaryFileStream(dialog.Filename, FileMode.Create))
                {
                    DatabaseDocument doc = DocumentHost.SelectedItem as DatabaseDocument;
                    Database database = doc.Tag as Database;

                    DatabaseExportSerializer serializer = new DatabaseExportSerializer();
                    try
                    {
                        serializer.Export(stream, database);
                    }
                    catch (Exception)
                    {
                    }

                    
                }
            }
        }

        void DocumentContent_Closing(object sender, CancelEventArgs e)
        {
            DocumentContent doc = sender as DocumentContent;
            Database database = doc.Tag as Database;

            e.Cancel = PromptToSave(doc.Title, database);

            if (!e.Cancel)
            {
                OnActiveDocumentChanging(doc);
            }
        }

        void ApplicationCommand_Close(object sender, RoutedEventArgs e)
        {
        }

        bool OpenDocument(string fileName)
        {   
            //TODO: None of the loading code returns any indication of success or failure.. this needs to be fixed!!!
            DocumentContent doc = FindOpenDocument(fileName);
            if (doc == null)
            {
                New.Move.Core.Database database = new New.Move.Core.Database();
                database.Load(fileName);

                doc = new DatabaseDocument(Path.GetFileNameWithoutExtension(fileName), database);
                doc.Closing += new EventHandler<CancelEventArgs>(DocumentContent_Closing);

                App.OnDocumentCreated(doc);

                DocumentHost.Items.Add(doc);
                DocumentHost.SelectedItem = doc;
               
                App.RecentFiles.Remove(database.FullyQualifiedFilename);
                App.RecentFiles.Insert(0, database.FullyQualifiedFilename);
            }
            else
            {
                DocumentHost.SelectedItem = doc;
            }

            return true;
        }

        bool OpenDocument(Database database)
        {
            DocumentContent doc = FindOpenDocument(database.FullyQualifiedFilename);
            if (doc == null)
            {
                doc = new DatabaseDocument(Path.GetFileNameWithoutExtension(database.FullyQualifiedFilename), database);
                doc.Closing += new EventHandler<CancelEventArgs>(DocumentContent_Closing);

                App.OnDocumentCreated(doc);

                DocumentHost.Items.Add(doc);
                DocumentHost.SelectedItem = doc;

                App.RecentFiles.Remove(database.FullyQualifiedFilename);
                App.RecentFiles.Insert(0, database.FullyQualifiedFilename);
            }
            else
            {
                DocumentHost.SelectedItem = doc;
            }

            return true;
        }

        void ApplicationCommand_Open(object sender, RoutedEventArgs e)
        {
            ExecutedRoutedEventArgs args = e as ExecutedRoutedEventArgs;
            ICommandResponse response = args.Parameter as ICommandResponse;

            if (response == null || (response != null && String.IsNullOrEmpty(response.Filename)))
            {
                CommonDialog dialog = new CommonDialog();
                dialog.InitialDirectory = New.Move.Core.Database.ApplicationFolderPath;
                dialog.Filter.Add(new FilterEntry(Properties.Resources.Files, Properties.Resources.FileExtension));
                dialog.Filter.Add(new FilterEntry(Properties.Resources.AllFiles, Properties.Resources.AllExtension));
                dialog.Title = Properties.Resources.Open;
                dialog.ShowOpen();

                if (!String.IsNullOrEmpty(dialog.Filename))
                {
                    OpenDocument(dialog.Filename);

                    if (response != null)
                        response.Filename = dialog.Filename;
                }
            }
            else
            {
                OpenDocument(response.Filename);
            }
        }

        void ApplicationCommand_Save(object sender, RoutedEventArgs e)
        {
            DocumentContent doc = DocumentHost.SelectedItem as DocumentContent;
            New.Move.Core.Database database = doc.Tag as New.Move.Core.Database;

            if (String.IsNullOrEmpty(database.FullyQualifiedFilename))
            {
                CommonDialog dialog = new CommonDialog();
                dialog.InitialDirectory = New.Move.Core.Database.ApplicationFolderPath;
                dialog.Filter.Add(new FilterEntry(Properties.Resources.Files, Properties.Resources.FileExtension));
                dialog.Filter.Add(new FilterEntry(Properties.Resources.AllFiles, Properties.Resources.AllExtension));
                dialog.Title = Properties.Resources.SaveAs;
                dialog.DefaultExtension = Properties.Resources.Extension;
                dialog.ShowSave();

                if (!String.IsNullOrEmpty(dialog.Filename))
                {
                    database.Save(dialog.Filename);

                    App.RecentFiles.Remove(database.FullyQualifiedFilename);
                    App.RecentFiles.Insert(0, database.FullyQualifiedFilename);
                    //BuildOpenMenu();
                }
            }
            else
            {
                database.Save();

                App.RecentFiles.Remove(database.FullyQualifiedFilename);
                App.RecentFiles.Insert(0, database.FullyQualifiedFilename);
                //BuildOpenMenu();
            }
        }

        void ApplicationCommand_SaveAs(object sender, RoutedEventArgs e)
        {
            DocumentContent doc = DocumentHost.SelectedItem as DocumentContent;
            New.Move.Core.Database database = doc.Tag as New.Move.Core.Database;

            CommonDialog dialog = new CommonDialog();
            dialog.InitialDirectory = New.Move.Core.Database.ApplicationFolderPath;
            dialog.Filter.Add(new FilterEntry(Properties.Resources.Files, Properties.Resources.FileExtension));
            dialog.Filter.Add(new FilterEntry(Properties.Resources.AllFiles, Properties.Resources.AllExtension));
            dialog.Title = Properties.Resources.SaveAs;
            dialog.DefaultExtension = Properties.Resources.Extension;
            dialog.ShowSave();

            if (!String.IsNullOrEmpty(dialog.Filename))
            {
                database.Save(dialog.Filename);

                App.RecentFiles.Remove(database.FullyQualifiedFilename);
                App.RecentFiles.Insert(0, database.FullyQualifiedFilename);
                //BuildOpenMenu();
            }
        }

        void ApplicationCommand_Undo(object sender, RoutedEventArgs e)
        {
            DocumentContent doc = DocumentHost.SelectedItem as DocumentContent;
            New.Move.Core.Database database = doc.Tag as New.Move.Core.Database;

            database.Undo();
        }

        void ApplicationCommand_Redo(object sender, RoutedEventArgs e)
        {
            DocumentContent doc = DocumentHost.SelectedItem as DocumentContent;
            New.Move.Core.Database database = doc.Tag as New.Move.Core.Database;

            database.Redo();
        }

        void NavigationCommand_GoToPage(object sender, RoutedEventArgs e)
        {
            ExecutedRoutedEventArgs args = e as ExecutedRoutedEventArgs;
            Database database = args.Parameter as Database;

            OpenDocument(database);
        }

        void EditRuntimeTargetsMenu_Click(object sender, RoutedEventArgs e)
        {
            RuntimeTargetEditor dialog = new RuntimeTargetEditor();
            dialog.DoModal();
        }

        void AttachToNetworkMenu_Click(object sender, RoutedEventArgs e)
        {
            if (App.Client.IsConnected)
            {
                App.Client.RequestActiveNetworks(
                        delegate(NetworkMessageBase baseMsg)
                        {
                            NetworkInstancesMessage msg = (NetworkInstancesMessage)baseMsg;

                            Action a = () =>
                            {
                                NetworkInstanceDlg dlg = new NetworkInstanceDlg(msg.Instances);
                                Nullable<bool> result = dlg.ShowDialog();
                                if (result == true)
                                {
                                    if (dlg.Network != null)
                                    {
                                        //Make sure the file exists
                                        System.IO.FileInfo fi = new System.IO.FileInfo(dlg.Network.Filename);
                                        if (!fi.Exists)
                                        {
                                            var keepGoing = System.Windows.MessageBox.Show(String.Format("Failed to locate the network : {0}\nDebug anyway?", dlg.Network.Filename),
                                                "MoVE File Error", System.Windows.MessageBoxButton.YesNo);
                                            if (keepGoing == MessageBoxResult.Yes)
                                            {
                                                App.Client.AttachWithoutDatabase(dlg.Network.NetworkInstanceId);
                                            }
                                            return;
                                        }

                                        if(OpenDocument(dlg.Network.Filename))
                                        {
                                            DocumentContent dc = FindOpenDocument(dlg.Network.Filename);
                                            if (dc != null)
                                            {
                                                Database db = (Database)dc.Tag;
                                                App.Client.AttachToDatabase(db, dlg.Network.NetworkInstanceId);
                                            }
                                            else
                                            {
                                                MessageBox.Show("Failed to load document {0}", dlg.Network.Filename);
                                            }
                                        }
                                    }
                                }
                            };

                            App.Instance.Dispatcher.Invoke(a);
                        }
                    );
            }
            else
            {
                MessageBox.Show("Can't request network instances because MoVE is not connected to the runtime");
            }
        }

        void RuntimeTargetMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)e.OriginalSource;
            RuntimeTarget rt = (RuntimeTarget)mi.Tag;

            if(App.Client.IsConnected)
            {
                App.Client.DisconnectClient();

                if(App.Client.Address != rt.Address)
                {
                    App.ConnectToTarget(rt);
                }
            }
            else
            {
                App.ConnectToTarget(rt);
            }
        }
/*
        void BuildOpenMenu()
        {
            OpenMenu.Items.Clear();

            MenuItem openMenuItem = new MenuItem();
            openMenuItem.Header = "Open";
            openMenuItem.Command = ApplicationCommands.Open;
            OpenMenu.Items.Add(openMenuItem);

            if (App.RecentFiles.Count > 0)
            {
                OpenMenu.Items.Add(new Separator());

                foreach (string file in App.RecentFiles)
                {
                    MenuItem item = new MenuItem();
                    item.Header = System.IO.Path.GetFileName(file);
                    item.CommandParameter = file;
                    item.Click += new RoutedEventHandler(OpenRecentFile_Click);

                    OpenMenu.Items.Add(item);
                }
            }
        }
        */

        void WindowsMenu_PreviewMouseDown(object sender, RoutedEventArgs e)
        {
            if (e.Source == WindowsMenu)
            {
                WindowsMenu.Items.Clear();
                                                        /*
                foreach (IWindow window in App.Instance.WindowList)
                {
                    MenuItem menuItem = new MenuItem();

                    menuItem.Header = window.Name;
                    menuItem.IsCheckable = true;
                    menuItem.IsChecked = window.Content.State != DockableContentState.Hidden;
                    menuItem.Click += new RoutedEventHandler(WindowsMenuItem_Click);
                    menuItem.Tag = window;

                    WindowsMenu.Items.Add(menuItem);
                }
                                                         */
            }
        }

        void WindowsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;

            IWindow window = menuItem.Tag as IWindow;
            if (menuItem.IsChecked)
            {
                DockingManagerControl.Show(window.Content, DockableContentState.FloatingWindow);
            }
            else
            {
                DockingManagerControl.Hide(window.Content);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DocumentContent document = DocumentHost.SelectedItem as DocumentContent;
                ApplicationCommands.Delete.Execute(null, document.Content as IInputElement);
                e.Handled = true;
            }
        }

        void App_OnConnected(object sender, EventArgs e)
        {
            Client client = (Client)sender;
            //Update the targets menu to show the connected item
            RuntimeTarget target = (from t in App.RuntimeTargets where t.Address == client.Address select t).FirstOrDefault();
            
            if (target != null)
            {
                MenuItem mi = (MenuItem)TargetsMenu.ItemContainerGenerator.ContainerFromItem(target);
                if(mi.Dispatcher.CheckAccess()) {
                    mi.IsChecked = true;
                }
                else {
                    mi.Dispatcher.Invoke(new Action<MenuItem>((p) => p.IsChecked = true), mi);
                }
            }
        }

        void App_OnDisconnected(object sender, EventArgs e)
        {
            Client client = (Client)sender;
            RuntimeTarget target = (from t in App.RuntimeTargets where t.Address == client.Address select t).FirstOrDefault();

            if (target != null)
            {
                MenuItem mi = (MenuItem)TargetsMenu.ItemContainerGenerator.ContainerFromItem(target);
                if (mi.Dispatcher.CheckAccess()) {
                    mi.IsChecked = false;
                }
                else {
                    mi.Dispatcher.Invoke(new Action<MenuItem>((p) => p.IsChecked = false), mi);
                }
            }
        }

        DocumentContent FindOpenDocument(string filename)
        {
            return DocumentHost.Items.OfType<DatabaseDocument>().FirstOrDefault(
                delegate(DatabaseDocument doc) 
                {
                    return (doc.Tag as Database).FullyQualifiedFilename == filename;
                }
            );
        }

        string GetDefaultFileName()
        {
            List<int> used = new List<int>();

            foreach (DocumentContent doc in DocumentHost.Items)
            {
                if (doc.Title.StartsWith(Properties.Resources.DefaultFileName))
                {
                    Match match  = new Regex(@"\d+", RegexOptions.Compiled).Match(doc.Title);
                    if (!String.IsNullOrEmpty(match.Value))
                    {
                        used.Add(Int32.Parse(match.Value));
                    }
                    else
                    {
                        used.Add(0);
                    }
                }
            }

            int available = 0;
            while (used.Contains(available))
            {
                ++available;
            }

            if (available == 0)
            {
                return Properties.Resources.DefaultFileName;
            }
            else 
            {
                return String.Format("{0} {1}", Properties.Resources.DefaultFileName, available);
            }
        }

        void MainWindow_Loaded(object sender, EventArgs e)
        {
            AppMain.CloseSplashScreen();
        }

#if DEBUG
        void AddMoveDebugMenu()
        {
            //Some useful stuff for debugging...
            MenuItem debugMenuItem = new MenuItem();
            debugMenuItem.Header = "MoVE Debug Tools";

            MenuItem dumpIdsMenuItem = new MenuItem();
            dumpIdsMenuItem.Header = "Dump Node Ids";
            dumpIdsMenuItem.Click += new RoutedEventHandler(DebugMenu_DumpIdsClick);

            MenuItem testNetworkInstanceDlgItem = new MenuItem();
            testNetworkInstanceDlgItem.Header = "Request Network Instances";
            testNetworkInstanceDlgItem.Click += new RoutedEventHandler(DebugMenu_RequestNetworkInstances);

            debugMenuItem.Items.Add(dumpIdsMenuItem);
            debugMenuItem.Items.Add(testNetworkInstanceDlgItem);
            FileMenu.Items.Add(debugMenuItem);
        }

        void DebugMenu_RequestNetworkInstances(object sender, RoutedEventArgs e)
        {
            if (App.Client.IsConnected)
            {
                App.Client.RequestActiveNetworks(
                        delegate(NetworkMessageBase baseMsg)
                        {
                            NetworkInstancesMessage msg = (NetworkInstancesMessage)baseMsg;

                            Action a = () =>
                                {
                                    NetworkInstanceDlg dlg = new NetworkInstanceDlg(msg.Instances);
                                    Nullable<bool> result = dlg.ShowDialog();
                                    if (result == true)
                                    {
                                        Console.WriteLine("Selected Network {0},{1}", dlg.Network.NetworkInstanceId, dlg.Network.Filename);
                                    }
                                };

                            App.Instance.Dispatcher.Invoke(a);
                        }
                    );
            }
            else
            {
                MessageBox.Show("Can't request network instances because MoVE is not connected to the runtime");
            }
        }

        void DebugMenu_DumpIdsClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text Files (*.txt)|*.txt";

            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                using (TextWriter tw = new StreamWriter(dlg.FileName))
                {
                    Database db = (Database)DockingManagerControl.ActiveDocument.Tag;
                    if (db != null)
                    {
                        tw.WriteLine("{0}", db.Id.ToString());

                        foreach (KeyValuePair<ushort, INode> item in db.Identifiers)
                        {
                            tw.WriteLine("{0}, {1}", item.Key, item.Value.Name);
                            if(item.Value is ITransitional)
                            {
                                ITransitional transitional = (ITransitional)item.Value;
                                if(transitional.IsAutomaton)
                                {
                                    IAutomaton automaton = transitional as IAutomaton;
                  
                                    int transIdx = 0;
                                    foreach (Transition transition in automaton.Transitions)
                                    {
                                        tw.WriteLine("\tTrans : {0}, {1}",  transition.Name, transIdx++);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
#endif //DEBUG

    }
}
