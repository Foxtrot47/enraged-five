﻿using System;

namespace Rockstar.MoVE.Framework
{
    public interface IPersistentContent
    {
        string FullyQualifiedFilename { get; }

        bool IsDirty { get; }

        void Save();
        void Save(string filename);
        void SaveSelection(string filename);
    }
}