﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using New.Move.Core;
using RSG.TrackViewer.Data;
using System.Globalization;
using System.Xml;
using System.Collections.Generic;

namespace Rockstar.MoVE.Framework.DataModel
{
    [Serializable]
    public class WeightFilteredTransform : ILogicPropertyGroup, ISerializable, ILooseOperator
    {
        #region Fields
        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Name"/> property.
        /// </summary>
        private const string NameSerializationTag = "Name";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Id"/> property.
        /// </summary>
        private const string IdSerializationTag = "Id";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Input"/> property.
        /// </summary>
        private const string InputSerializationTag = "Input";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Weight"/> property.
        /// </summary>
        private const string WeightSerializationTag = "Weight";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Filter"/> property.
        /// </summary>
        private const string FilterSerializationTag = "Filter";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="Parent"/> property.
        /// </summary>
        private const string ParentSerializationTag = "Parent";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="OutTrack"/> property.
        /// </summary>
        private const string CurveSerializationTag = "Curve";

        /// <summary>
        /// The tag that is used to serialise/deserialise the <see cref="OutTrack"/> property.
        /// </summary>
        private const string TrackSerializationTag = "Track";

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The private field used for the <see cref="Tag"/> property.
        /// </summary>
        private object tag;

        /// <summary>
        /// The private field used for the <see cref="Parent"/> property.
        /// </summary>
        private ILogic parent;

        /// <summary>
        /// The private field used for the <see cref="OutTrack"/> property.
        /// </summary>
        private Track outTrack;

        /// <summary>
        /// The private field used for the <see cref="OutCurve"/> property.
        /// </summary>
        private Curve outCurve;

        /// <summary>
        /// A array of properties that are associated with this transform.
        /// </summary>
        private IProperty[] properties = new IProperty[3];
        #endregion

        #region Constructors
        /// <summary>
        ///  Initialises a new instance of the
        ///  <see cref="Rockstar.MoVE.Framework.DataModel.WeightFilteredTransform"/> class.
        /// </summary>
        public WeightFilteredTransform()
        {
            this.Input = new SourceTransform();
            this.Weight = new RealLogicalProperty();
            this.Filter = new FilterLogicalProperty();
            this.Immutable = new BoolTransitionProperty();
            outCurve = new Curve("Out Curve");
            outCurve.Visible = false;
            outTrack = new Track("Track");
            outTrack.IsActive = false;
            TrackPoint start = new FloatTrackPoint(0.0, 0.0);
            start.IsLockedX = true;

            TrackPoint end = new FloatTrackPoint(1.0, 0.0);
            end.IsLockedX = true;

            outTrack.AddPoint(start);
            outTrack.AddPoint(end);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Motiontree"/> class
        /// using the specified serialisation info and streaming context as data providers.
        /// </summary>
        /// <param name="info">
        /// The serialisation info that provides access to the data used to initialise
        /// this instance.
        /// </param>
        /// <param name="context">
        /// The source of the serialisation info.
        /// </param>
        public WeightFilteredTransform(SerializationInfo info, StreamingContext context)
        {
            ILogic infoParent = null;
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case NameSerializationTag:
                        this.Name = (string)entry.Value;
                        break;
                    case IdSerializationTag:
                        this.Id = (Guid)entry.Value;
                        break;
                    case InputSerializationTag:
                        this.Input = (SourceTransform)entry.Value;
                        break;
                    case WeightSerializationTag:
                        this.Weight = (RealLogicalProperty)entry.Value;
                        break;
                    case FilterSerializationTag:
                        this.Filter = (FilterLogicalProperty)entry.Value;
                        break;
                    case ParentSerializationTag:
                        infoParent = (ILogic)entry.Value;
                        break;
                    case CurveSerializationTag:
                        this.OutCurve = (Curve)entry.Value;
                        if (OutCurve != null)
                        {
                            this.OutTrack = ConvertCurveToTrack(OutCurve);
                        }
                        break;
                    case TrackSerializationTag:
                        this.OutTrack = (Track)entry.Value;
                        break;
                    default:
                        Trace.TraceWarning("Unrecognised value has been serialised for this WeightFilteredTransform instance.");
                        break;
                }
            }

            if (this.Weight.Input == PropertyInput.Curve)
            {
                this.OutTrack.IsActive = true;
            }

            this.Parent = infoParent;
        }

        /// <summary>
        ///  Initialises a new instance of the
        ///  <see cref="Rockstar.MoVE.Framework.DataModel.WeightFilteredTransform"/> class
        ///  as a deep copy of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        public WeightFilteredTransform(WeightFilteredTransform other)
        {
            this.Input = new SourceTransform();
            this.Weight = new RealLogicalProperty(this.Parent, other.Weight);
            this.Filter = new FilterLogicalProperty(other.Filter);
            this.Immutable = new BoolTransitionProperty(other.Immutable);

            this.outCurve = other.OutCurve != null ? new Curve(other.OutCurve) : null;
            this.outTrack = other.OutTrack != null ? new Track(other.OutTrack) : null;

            if (outTrack != null)
            {
                foreach (TrackPoint opoint in other.OutTrack.Points)
                {
                    outTrack.AddPoint(opoint.Clone());
                }
            }

            this.Name = other.Name;
        }
                      
        /// <summary>
        /// Initialises a new instance of the <see cref="WeightFilteredTransform"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public WeightFilteredTransform(XmlReader reader, Database database, ILogic parent)
            : this()
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (database == null)
            {
                throw new ArgumentNullException("database");
            }
            
            this.Parent = parent;
            this.Deserialise(reader);
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs whenever a value on a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// The unique global identifier for this transform.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public IOperatorItem Item
        {
            get { return new TrackStackItem(this.outTrack); }
        }

        /// <summary>
        /// 
        /// </summary>
        public object Tag
        {
            get { return this.tag; }
            set { this.tag = value; }
        }

        /// <summary>
        /// Gets or sets the logic parent for this weight|filter transform.
        /// </summary>
        public ILogic Parent
        {
            get
            {
                return this.parent;
            }

            set
            {
                if (this.parent == value)
                {
                    return;
                }
                
                this.parent = value;
                this.Input.Parent = value;
                this.Weight.Parent = value;
                this.Filter.Parent = value;
                if (this.Weight != null)
                {
                    this.Weight.Parent = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Track OutTrack
        {
            get
            {
                return outTrack;
            }

            set
            {
                if (this.outTrack == value)
                {
                    return;
                }
                
                outTrack = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Curve OutCurve
        {
            get
            {
                return outCurve;
            }

            set
            {
                if (this.outCurve == value)
                {
                    return;
                }

                outCurve = value;
            }
        }

        /// <summary>
        /// The <see cref="SourceTransform"/> instance that defines the input value for
        /// this transform.
        /// </summary>
        public SourceTransform Input
        {
            set { properties[0] = value; }
            get { return properties[0] as SourceTransform; }
        }

        /// <summary>
        /// The <see cref="RealLogicalProperty"/> instance that defines the weight value for
        /// this transform.
        /// </summary>
        public RealLogicalProperty Weight
        {
            get
            {
                return this.properties[1] as RealLogicalProperty;

            }

            set
            {
                if (this.properties[1] == value)
                {
                    return;
                }

                if (this.properties[1] != null)
                {
                    this.properties[1].PropertyChanged -= this.WeightInputMethodChanged;
                }

                this.properties[1] = value;
                if (value != null)
                {
                    value.Container = this;
                    value.PropertyChanged += this.WeightInputMethodChanged;
                }
            }
        }

        /// <summary>
        /// The <see cref="FilterLogicalProperty"/> instance that defines the filter value for
        /// this transform.
        /// </summary>
        public FilterLogicalProperty Filter
        {
            set { properties[2] = value; }
            get { return properties[2] as FilterLogicalProperty; }
        }

        public BoolTransitionProperty Immutable
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the motion tree that this transform belongs to.
        /// </summary>
        private Motiontree ParentMotionTree
        {
            get
            {
                if (this.parent == null || this.parent.Database == null)
                {
                    return null;
                }

                Guid motionTreeIdentifer = this.parent.Parent;
                return parent.Database.Find(parent.Parent) as Motiontree;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new WeightFilteredTransform(this);
        }

        /// <summary>
        /// Removes all of the connections that are currently attached to this transform and
        /// </summary>
        /// <param name="parent">
        /// The parent whose motion tree the connections need to be removed from.
        /// </param>
        public void RemoveConnections(INode nodeParent)
        {
            if (nodeParent == null || nodeParent.Database == null)
            {
                return;
            }

            Motiontree motionTree = nodeParent.Database.Find(nodeParent.Parent) as Motiontree;
            if (motionTree == null)
            {
                return;
            }

            motionTree.RemoveConnectionsFromProperty(this.Input);
            motionTree.RemoveConnectionsFromProperty(this.Weight);
            motionTree.RemoveConnectionsFromProperty(this.Filter);
        }

        /// <summary>
        /// Gets the index of the first occurence to the specified property in this transform.
        /// </summary>
        /// <param name="property">
        /// The property to get the index for.
        /// </param>
        /// <returns>
        /// The index to the first occurence to the specified property if found; otherwise, -1.
        /// </returns>
        public int IndexOf(IProperty property)
        {
            for (int i = 0; i < this.properties.Length; ++i)
            {
                if (this.properties[i] == property)
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Gets the property for this transform at the specified non-zero index.
        /// </summary>
        /// <param name="index">
        /// The non-zero index of the transform property to get.
        /// </param>
        /// <returns>
        /// The property for this transform at the given index.
        /// </returns>
        public object ItemAt(int index)
        {
            return properties[index];
        }

        /// <summary>
        /// Gets the number of properties associated with this transfrom.
        /// </summary>
        /// <returns>
        /// The number of properties associated with this transfrom.
        /// </returns>
        public int Count()
        {
            return properties.Length;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetParentOnChildren()
        {
            this.Input.Parent = this.parent;
            this.Weight.Parent = parent;
            this.Filter.Parent = parent;
        }

        /// <summary>
        /// Determines whether the specified property is contained within this transforms
        /// properties.
        /// </summary>
        /// <param name="property">
        /// The property to find.
        /// </param>
        /// <returns>
        /// True if one of the properties for this transform is equal to the specified
        /// property; otherwise, false.
        /// </returns>
        public bool Contains(IProperty property)
        {
            foreach (IProperty p in properties)
            {
                if (p == property)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Populates a System.Runtime.Serialization.SerializationInfo with the data
        /// needed to serialise this instance.
        /// </summary>
        /// <param name="info">
        /// The serialisation info to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination for this serialisation.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(NameSerializationTag, Name);
            info.AddValue(IdSerializationTag, Id);
            info.AddValue(InputSerializationTag, Input);
            info.AddValue(WeightSerializationTag, Weight);
            info.AddValue(FilterSerializationTag, Filter);
            info.AddValue(ParentSerializationTag, Parent);
            info.AddValue(TrackSerializationTag, OutTrack);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="handler"></param>
        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
            Weight.SetPropertyChangedHandler(handler);
            Filter.SetPropertyChangedHandler(handler);
        }

        /// <summary>
        /// Converts a <see cref="Curve"/> to a <see cref="Track"/>.
        /// </summary>
        /// <param name="curve">
        /// The curve to convert.
        /// </param>
        /// <returns>
        /// A new track that represents the single specified curve.
        /// </returns>
        private static Track ConvertCurveToTrack(Curve curve)
        {
            Track track = new Track(curve.Name);
            for (int i = 0; i < curve.Points.Length; i++)
            {
                CurveSegment segment = curve.Points[i];
                TrackPoint point = new FloatTrackPoint(segment.X, segment.Y);
                if (i == 0 || i == curve.Points.Length - 1)
                {
                    point.IsLockedX = true;
                }

                track.AddPoint(point);
            }

            return track;
        }

        /// <summary>
        /// Gets called whenever the input method of the Weight property changes.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs data for the event.
        /// </param>
        private void WeightInputMethodChanged(object sender, PropertyChangedEventArgs e)
        {
            RealLogicalProperty prop = (sender as RealLogicalProperty);
            switch (prop.Input)
            {
                case PropertyInput.Curve:
                    outTrack.IsActive = true;
                    break;
                default:
                    outTrack.IsActive = false;
                    break;
            }

        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));

            if (this.OutTrack != null)
            {
                this.OutTrack.Serialise(writer);
            }

            if (this.Input != null)
            {
                writer.WriteStartElement("Input");
                this.Input.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Weight != null)
            {
                writer.WriteStartElement("Weight");
                this.Weight.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Filter != null)
            {
                writer.WriteStartElement("Filter");
                this.Filter.Serialise(writer);
                writer.WriteEndElement();
            }

            if (this.Immutable != null)
            {
                writer.WriteStartElement("Immutable");
                this.Immutable.Serialise(writer);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "curve") == 0)
                {
                    reader.Skip();
                }
                else if (string.CompareOrdinal(reader.Name, "Track") == 0)
                {
                    this.OutTrack = new Track(reader);
                }
                else if (string.CompareOrdinal(reader.Name, "Input") == 0)
                {
                    this.Input = new SourceTransform(reader, this.Parent);
                }
                else if (string.CompareOrdinal(reader.Name, "Weight") == 0)
                {
                    this.Weight = new RealLogicalProperty(reader, this.Parent.Database, this.Parent);
                }
                else if (string.CompareOrdinal(reader.Name, "Filter") == 0)
                {
                    this.Filter = new FilterLogicalProperty(reader, this.Parent.Database, this.Parent);
                }
                else if (string.CompareOrdinal(reader.Name, "Immutable") == 0)
                {
                    this.Immutable = new BoolTransitionProperty(reader, this.Parent.Database, null);
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            ids.Add(this.Filter.Id);
            ids.Add(this.Immutable.Id);
            ids.Add(this.Input.Id);
            ids.Add(this.Weight.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
            this.Filter.Id = New.Move.Core.Database.GetId(map, this.Filter.Id);
            this.Immutable.Id = New.Move.Core.Database.GetId(map, this.Immutable.Id);
            this.Input.Id = New.Move.Core.Database.GetId(map, this.Input.Id);
            this.Weight.Id = New.Move.Core.Database.GetId(map, this.Weight.Id);
        }
        #endregion
    } // Rockstar.MoVE.Framework.DataModel.WeightFilteredTransform
} // Rockstar.MoVE.Framework.DataModel