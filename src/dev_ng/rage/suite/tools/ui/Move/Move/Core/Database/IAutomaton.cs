﻿using System;
using System.Collections.ObjectModel;

using Move.Utils;

namespace New.Move.Core
{
    public interface IAutomaton
    {
        TransitionCollection Transitions { get; }

        ITransitional Default { get; set; }

        Database Database { get; }
        Guid Id { get; }
    }
}