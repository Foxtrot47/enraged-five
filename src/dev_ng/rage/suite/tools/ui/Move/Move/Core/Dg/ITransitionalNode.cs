﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Xml.Serialization;

namespace Rage.Move.Core.Dg
{
    public delegate void ActivatedEventHandler();

    [Serializable]
    public class Uniquifier16
    {
        public static Uniquifier16 Create() 
        {
            Uniquifier16 unique = new Uniquifier16();
            return unique;
        }

        public Uniquifier16(short value)
        {
            _Value = value;
        }

        private Uniquifier16()
        {
            _Value = _Pos;
            ++_Pos;
        }

        public override string ToString()
        {
            return _Value.ToString();
        }

        [XmlIgnore]
        public int Value { get { return _Value; } }

        private short _Value;
        private static short _Pos = 0;
    }

    public class Uniquifier16Converter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type type)
        {
            if (type == typeof(string))
                return true;

            return base.CanConvertFrom(context, type);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                return new Uniquifier16(Convert.ToInt16(value as string));
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type type)
        {
            if (type == typeof(string))
                return (value as Uniquifier16).ToString();

            return base.ConvertTo(context, culture, value, type);
        }
    }

    /*
    public interface ITransitionalNode
    {
        string Name { get; set;  }
        Uniquifier16 Unid { get; set; }
        Guid Id { get; }

        Point Position { get; set; }

        StateMachine Parent { get; set; }

        Guid Database { get; set; }

        New.Move.Core.ILogic GetInitial();

        event NameChangedEventHandler NameChanged;

        void OnActivate();
        void OnDeactivate();
    }
     */
}
