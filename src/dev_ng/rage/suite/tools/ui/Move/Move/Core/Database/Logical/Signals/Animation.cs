﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class Animation : SignalBase, ISerializable
    {
        public Animation(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public Animation(AnimationSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Animation"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Animation(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "animationsignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new Animation(new AnimationSignal());
            (clone as Animation).AdditionalText = this.AdditionalText;
            (clone as Animation).Database = this.Database;
            (clone as Animation).Enabled = this.Enabled;
            (clone as Animation).Name = this.Name;
            (clone as Animation).Position = this.Position;
            (clone as Animation).Signal = this.Signal;

            return (ISelectable)clone;
        }
    }
}