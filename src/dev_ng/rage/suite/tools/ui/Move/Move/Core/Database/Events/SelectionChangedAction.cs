﻿namespace New.Move.Core
{
    public enum SelectionChangedAction
    {
        Add,
        Remove
    }
}