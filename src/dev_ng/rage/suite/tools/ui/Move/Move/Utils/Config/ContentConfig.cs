﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;

namespace Move.Utils
{
    public static class ContentConfig
    {
        #region Fields
        //static ConfigGameView m_ConfigGameView;

        private static IConfig m_config;
        #endregion // Fields

        #region Constructor
        static ContentConfig()
        {
            m_config = ConfigFactory.CreateConfig();
        }
        #endregion

        #region Properties
        public static IConfig Config
        {
            get { return m_config; }
        }

        public static string ToolsTempDirectory
        {
            get { return m_config.ToolsTemp; }
        }

        public static string ExportDirectory
        {
            get { return m_config.Project.DefaultBranch.Export; }
        }

        public static string MetadataDirectory
        {
            get { return m_config.Project.DefaultBranch.Metadata; }
        }

        public static string ToolsConfigDirectory
        {
            get { return m_config.ToolsConfig; }
        }

        public static string ToolsLibDirectory
        {
            get { return m_config.ToolsLib; }
        }

        public static string AssetsDirectory
        {
            get { return m_config.Project.DefaultBranch.Assets; }
        }

        public static string ControlTemplatePath
        {
            get
            {
                return Path.Combine(m_config.ToolsConfig, "config", "anim", "control_templates");
            }
        }

        public static string NodeMapExportPath
        {
            get
            {
                return Path.Combine(m_config.Project.DefaultBranch.Build, "common", "non_final");
            }
        }

        public static string ClipSetsPath
        {
            get
            {
                return Path.Combine(m_config.Project.DefaultBranch.Common, "data", "anim", "clip_sets" );
            }
        }

        public static string PreviewPath
        {
            get
            {
                return Path.Combine(m_config.Project.DefaultBranch.Assets, "anim", "move", "networks", "preview");
            }
        }

        public static string NetworkPath
        {
            get
            {
                return Path.Combine(m_config.Project.DefaultBranch.Assets, "anim", "move", "networks");
            }
        }

        public static string ExportedClipsDirectory
        {
            get
            {
                return Path.Combine(m_config.Project.DefaultBranch.Export, "anim", "ingame");
            }
        }

        public static string ExportedClipsFilename
        {
            get
            {
                return Path.Combine(m_config.Project.DefaultBranch.Export, "anim", "clip.zip");
            }
        }

        public static string ExportedFiltersFilenameOverride
        {
            get;
            set;
        }

        public static string ExportedFiltersFilename
        {
            get
            {
                if (string.IsNullOrEmpty(ExportedFiltersFilenameOverride))
                    return Path.Combine(m_config.Project.DefaultBranch.Export, "anim", "filters.zip");
                else
                    return ExportedFiltersFilenameOverride;
            }
        }

        public static string ExportedExpressionsDirectory
        {
            get
            {
                return Path.Combine(m_config.Project.DefaultBranch.Export, "anim", "expressions");
            }
        }

        public static string ExportedClipSetsFilename
        {
            get
            {
                return Path.Combine(m_config.Project.DefaultBranch.Common, "data", "anim", "clip_sets", "clip_sets.xml");
            }
        }

        public static string MoveExecutableDirectory
        {
            get
            {
                return Path.Combine(m_config.ToolsBin, "MoVE", "tool");
            }
        }

        public static bool LoadClipSets( out List<string> clipSets )
        {
            clipSets = new List<string>();
            try
            {
                IConfig config = Move.Utils.ContentConfig.Config;
                IBranch branch = config.Project.DefaultBranch;
                List<string> directories = new List<string>();
                directories.Add(Path.Combine(branch.Common, "data", "anim", "clip_sets"));
                foreach (IProject project in config.DLCProjects.Values)
                {
                    branch = project.DefaultBranch;
                    directories.Add(Path.Combine(branch.Common, "data", "anim", "clip_sets"));
                }

                string xmlPattern = "*clip_sets.xml";
                string metaPattern = "*clip_sets.meta";
                SearchOption option = SearchOption.AllDirectories;
                foreach (string directory in directories)
                {
                    if (!Directory.Exists(directory))
                    {
                        continue;
                    }

                    List<string> filenames = new List<string>();
                    filenames.AddRange(Directory.GetFiles(directory, xmlPattern, option));
                    filenames.AddRange(Directory.GetFiles(directory, metaPattern, option));
                    foreach (string filename in filenames)
                    {
                        if (!File.Exists(filename))
                        {
                            continue;
                        }

                        XmlDocument doc = new XmlDocument();
                        doc.Load(filename);
                        XmlNodeList clipSetRoot = doc.GetElementsByTagName("clipSets");
                        foreach (XmlNode clipSetsGroup in clipSetRoot)
                        {
                            foreach (XmlNode clipSet in clipSetsGroup.ChildNodes)
                            {
                                XmlAttribute keyAttribute = clipSet.Attributes["key"];
                                clipSets.Add(keyAttribute.Value);
                            }
                        }
                    }
                }

                clipSets.Sort();
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.Message );
                throw new ApplicationException(String.Format("Loading '{0}' failed! {1}", ExportedClipSetsFilename, e.Message), e);
            }

            return true;

        }
        #endregion // Properties

    }
}
