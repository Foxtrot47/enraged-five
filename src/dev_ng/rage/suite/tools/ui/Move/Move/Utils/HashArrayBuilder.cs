﻿using RSG.ManagedRage;

namespace Move.Utils
{
    public sealed class HashArrayEntry
    {
        public HashArrayEntry(uint key, uint index)
        {
            Key = key;
            Index = index;
        }

        public uint Key { get; set; }
        public uint Index { get; set; }
    }

    public class HashArrayBuilder
    {
        public HashArrayBuilder(int count)
        {
            Entries = new HashArrayEntry[count];
            for (int i = 0; i < count; ++i)
                Entries[i] = new HashArrayEntry(0xffffffff, 0);
        }

        public HashArrayEntry[] Entries { get; private set; }

        public void Insert(string key, uint index)
        {
            uint hash = (uint)StringHashUtil.atStringHash(key, 0);

            Insert(hash, index);
        }

        public void Insert(uint key, uint index)
        {
            long? i = Get(key);
            Entries[i.Value].Key = key;
            Entries[i.Value].Index = index;
        }

        public uint Find(string key)
        {
            uint hash = (uint)StringHashUtil.atStringHash(key, 0);
            return Find(hash);
        }

        public uint Find(uint key)
        {
            long? i = Get(key);
            if (!i.HasValue) return 0xffffffff;

            if (Entries[i.Value].Key != 0xffffffff)
                return Entries[i.Value].Index;

            return 0xffffffff;
        }

        public uint? Get(string key)
        {
            uint hash = (uint)StringHashUtil.atStringHash(key, 0);
            return Get(hash);
        }

        uint? Get(uint key)
        {
            if (Entries.Length.Equals(0))
                return null;

            long i = key % Entries.Length;

            while (Entries[i].Key != 0xffffffff && Entries[i].Key != key)
                i = (i + 1) % Entries.Length;

            return (uint)i;
        }
    }
}