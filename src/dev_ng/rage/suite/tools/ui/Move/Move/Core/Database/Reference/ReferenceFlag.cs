﻿using System;
using System.Runtime.Serialization;

namespace New.Move.Core
{
    [Serializable]
    public class ReferenceFlag : Flag, ISerializable
    {
        public ReferenceFlag(Guid reference, string name)
            : base(name)
        {
            Reference = reference;
        }

        public Guid Reference { get; protected set; }

        new public ReferenceFlag CreateReference()
        {
            return new ReferenceFlag(Reference, Name);
        }
        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Reference = "Reference";
        }

        public ReferenceFlag(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Reference = (Guid)info.GetValue(SerializationTag.Reference, typeof(Guid));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Reference, Reference);
        }

    }
}