﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

using Move.Core;
using Move.Utils;
using System.Xml;
using System.Globalization;
using RSG.Base.Logging;
using System.Collections.Generic;

namespace New.Move.Core
{
    [Serializable]
    public class ClipPropertyLogicalProperty : Property, ISerializable
    {
        public ClipPropertyLogicalProperty()
        {
            PropertyName = "";
            Flags = PropertyInput.String;
            Input = PropertyInput.String;
            Id = Guid.NewGuid();

            Connections = new NoResetObservableCollection<Connection>();
        }

        public ClipPropertyLogicalProperty(ClipPropertyLogicalProperty other)
        {
            Parent = other.Parent;
            PropertyName = other.PropertyName;
            Flags = other.Flags;
            Input = other.Input;
            OutputEnabled = other.OutputEnabled;
            SelectedEvent = other.SelectedEvent;

            Id = Guid.NewGuid();

            Connections = new NoResetObservableCollection<Connection>();
        }

        public ClipPropertyLogicalProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        public string Name { get; set; }
        public Guid Id { get; private set; }

        public object Tag { get; set; }

        public PropertyInput Flags { get; private set; }
        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }


        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            from.RemoveConnectionsFromAnchor(this);
        }

        public ILogic Parent { get; set; }

        public string PropertyName { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Input = "Input";
            public static string OutputEnabled = "OutputEnabled";
            public static string Flags = "Flags";
            public static string SelectedEvent = "SelectedEvent";
            public static string PropertyName = "PropertyName";
        }

        public ClipPropertyLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
            _Input = (PropertyInput)info.GetValue(SerializationTag.Input, typeof(PropertyInput));
            _OutputEnabled = info.GetBoolean(SerializationTag.OutputEnabled);
            _SelectedEvent = (Signal)info.GetValue(SerializationTag.SelectedEvent, typeof(Signal));
            Flags = (PropertyInput)info.GetValue(SerializationTag.Flags, typeof(PropertyInput));

            switch (_Input)
            {
                case PropertyInput.String:
                    PropertyName = (string)info.GetValue(SerializationTag.PropertyName, typeof(string));
                    break;
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
            info.AddValue(SerializationTag.Input, _Input);
            info.AddValue(SerializationTag.OutputEnabled, _OutputEnabled);
            info.AddValue(SerializationTag.Flags, Flags);
            info.AddValue(SerializationTag.SelectedEvent, _SelectedEvent);
            info.AddValue(SerializationTag.PropertyName, PropertyName);
        }

        public object Clone()
        {
            return new ClipPropertyLogicalProperty(this);
        }
                    
        /// <summary>
        /// Initialises a new instance of the <see cref="ClipPropertyLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public ClipPropertyLogicalProperty(XmlReader reader, Database database, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Input = (PropertyInput)uint.Parse(reader.GetAttribute("Input"), CultureInfo.InvariantCulture);
            this.OutputEnabled = bool.Parse(reader.GetAttribute("OutputEnabled"));

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedEvent = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedEvent == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedEvent = database.GetSignal(signalName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}' inside the '{1}' node. This needs fixing before a export is allowed.", signalId, parent.Name);
                    }
                }
            }

            this.PropertyName = reader.GetAttribute("PropertyName");           
            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Input", ((uint)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("OutputEnabled", this.OutputEnabled.ToString(CultureInfo.InvariantCulture));

            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString("Signal", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedEvent.Name);
            }

            writer.WriteAttributeString("PropertyName", this.PropertyName);
        }

        /// <summary>
        /// Retrieves all of the identifiers currently in use in this node and all children.
        /// </summary>
        /// <returns>
        /// All of the identifiers currently in use in this node and all children.
        /// </returns>
        public List<Guid> GetAllGuids()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.Id);
            return ids;
        }

        /// <summary>
        /// Resets all of the identifiers for this node and its children making sure other
        /// nodes and connections are also updated.
        /// </summary>
        public void ResetIds(Dictionary<Guid, Guid> map)
        {
            this.Id = New.Move.Core.Database.GetId(map, this.Id);
        }
    }
}
