﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using Move.Core;
using Move.Utils;
using RSG.TrackViewer.Data;
using Rockstar.MoVE.Framework.DataModel;
using System.Globalization;
using RSG.Base.Logging;

namespace New.Move.Core
{
    [Serializable]
    public class NamedVariableLogicalProperty : Property, ISerializable
    {
        public static implicit operator NamedVariableLogicalProperty(float real)
        {
            NamedVariableLogicalProperty property = new NamedVariableLogicalProperty();
            property._Input = PropertyInput.Value;
            property.Value = real;
            property.VariableName = string.Empty;

            return property;
        }

        /// <summary>
        /// Creates a new object that is a copy of the specified instance but with a new
        /// id value, its parent set to null, and its tag is set to null.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public NamedVariableLogicalProperty(ILogic parent, NamedVariableLogicalProperty other)
        {
            Flags = other.Flags;
            _Input = other._Input;
            _OutputEnabled = other._OutputEnabled;
            Value = other.Value;
            VariableName = other.VariableName;
            _SelectedEvent = other.SelectedEvent;
            this.Parent = parent;

            Connections = new NoResetObservableCollection<Connection>();
            Id = Guid.NewGuid();
        }

        public NamedVariableLogicalProperty()
        {
            Flags = PropertyInput.Real | PropertyInput.Value;
            _Input = PropertyInput.Value;

            Connections = new NoResetObservableCollection<Connection>();

            Value = 0.0f;
            VariableName = string.Empty;

            Id = Guid.NewGuid();
        }

        public NamedVariableLogicalProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        public string Name { get; set; }
        public Guid Id { get; set; }

        public PropertyInput Flags { get; private set; }

        public void RemoveConnections()
        {
            Motiontree motionTree = Parent.Database.Find(Parent.Parent) as Motiontree;
            motionTree.RemoveConnectionsFromAnchor(this);
        }

        public void CheckBeforeRemoveConnections(Motiontree from)
        {
            if (_Input != PropertyInput.Real)
            {
                from.RemoveConnectionsFromAnchor(this);
            }
        }

        public NoResetObservableCollection<Connection> Connections { get; private set; }

        public object Tag { get; set; }

        PropertyInput _Input;
        public PropertyInput Input
        {
            get { return _Input; }
            set
            {
                if (_Input != value)
                {
                    _Input = value;
                    OnPropertyChanged("Input");
                }
            }
        }

        bool _OutputEnabled = false;
        public bool OutputEnabled
        {
            get { return _OutputEnabled; }
            set
            {
                if (_OutputEnabled != value)
                {
                    _OutputEnabled = value;

                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(_OutputEnabled);
                    }

                    OnPropertyChanged("OutputEnabled");
                }
            }
        }

        Signal _SelectedEvent;
        public Signal SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                if (_SelectedEvent != value)
                {
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(false);
                    }
                    _SelectedEvent = value;
                    if (_SelectedEvent != null)
                    {
                        _SelectedEvent.SetUsed(true);
                    }
                    OnPropertyChanged("SelectedEvent");
                }
            }
        }

        New.Move.Core.ILogic _Parent;
        public New.Move.Core.ILogic Parent
        {
            get
            {
                return _Parent;
            }
            set
            {
                _Parent = value;
            }
        }

        IProperty _Container;
        public IProperty Container
        {
            get
            {
                return _Container;
            }
            set
            {
                if (_Container != value)
                {
                    _Container = value;
                }
            }
        }

        public float _Value;
        public float Value
        {
            get { return _Value; }
            set
            {
                if (_Value != value)
                {
                    _Value = value;
                    OnPropertyChanged("Value");
                }
            }
        }

        private string _VariableName;
        public string VariableName
        {
            get { return _VariableName; }
            set
            {
                if (_VariableName != value)
                {
                    _VariableName = value;
                    OnPropertyChanged("VariableName");
                }
            }
        }

        static class SerializationTag
        {
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
            public static string Input = "Input";
            public static string OutputEnabled = "OutputEnabled";
            public static string Flags = "Flags";
            public static string Value = "Value";
            public static string SelectedEvent = "SelectedEvent";
            public static string VariableName = "VariableName";
        }

        public NamedVariableLogicalProperty(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));

            _Input = (PropertyInput)info.GetValue(SerializationTag.Input, typeof(PropertyInput));
            try
            {
                _OutputEnabled = info.GetBoolean(SerializationTag.OutputEnabled);
                _SelectedEvent = (Signal)info.GetValue(SerializationTag.SelectedEvent, typeof(Signal));
            }
            catch (Exception)
            {
            }
            Flags = (PropertyInput)info.GetValue(SerializationTag.Flags, typeof(PropertyInput));

            switch (_Input)
            {
                case PropertyInput.Value:
                Value = (float)info.GetValue(SerializationTag.Value, typeof(float));
                break;
            }

            VariableName = (string)info.GetValue(SerializationTag.VariableName, typeof(string));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);

            info.AddValue(SerializationTag.Input, _Input);
            info.AddValue(SerializationTag.OutputEnabled, _OutputEnabled);
            info.AddValue(SerializationTag.Flags, Flags);
            info.AddValue(SerializationTag.SelectedEvent, _SelectedEvent);

            switch (_Input)
            {
                case PropertyInput.Value:
                info.AddValue(SerializationTag.Value, Value);
                break;
            }

            info.AddValue(SerializationTag.VariableName, VariableName);
        }

        public void ExportToXML(PargenXmlNode parentNode, Motiontree motiontreeParent)
        {
            parentNode.AppendTextElementNode("Name", VariableName);
            var inputsNode = parentNode.AppendChild("Value");

            switch (Input)
            {
                case PropertyInput.Real:
                inputsNode.AppendTextElementNode("Type", "kF32AttributeParameter");
                foreach (Connection connection in motiontreeParent.Connections)
                {
                    if (connection.Dest == this)
                    {
                        inputsNode.AppendTextElementNode("Parameter", connection.Source.Parent.Name);
                        break;
                    }
                }
                break;
                case PropertyInput.Value:
                inputsNode.AppendTextElementNode("Type", "kF32AttributeValue");
                inputsNode.AppendValueElementNode("Value", "value", Value.ToString());
                break;
                case PropertyInput.Invalid:
                inputsNode.AppendTextElementNode("Type", "kF32AttributeIgnored");
                break;
            }
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance but with a new
        /// id value, its parent set to null, and its tag is set to null.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            NamedVariableLogicalProperty clone = new NamedVariableLogicalProperty();
            clone._Value = this._Value;
            clone._Input = this._Input;
            clone._OutputEnabled = this._OutputEnabled;
            clone.Flags = this.Flags;
            clone.VariableName = this.VariableName;

            return clone;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NamedVariableLogicalProperty"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public NamedVariableLogicalProperty(XmlReader reader, Database database, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            this.Input = (PropertyInput)uint.Parse(reader.GetAttribute("Input"), CultureInfo.InvariantCulture);
            this.OutputEnabled = bool.Parse(reader.GetAttribute("OutputEnabled"));
            this.Value = float.Parse(reader.GetAttribute("Value"), CultureInfo.InvariantCulture);

            string signalId = reader.GetAttribute("Signal");
            if (!string.IsNullOrEmpty(signalId))
            {
                this.SelectedEvent = database.GetSignal(Guid.ParseExact(signalId, "D"));
                if (this.SelectedEvent == null)
                {
                    string signalName = reader.GetAttribute("SignalName");
                    this.SelectedEvent = database.GetSignal(signalName);
                    if (this.SelectedEvent == null)
                    {
                        LogFactory.ApplicationLog.Warning("Unable to find a reference to signal with id '{0}' inside the '{1}' node. This needs fixing before a export is allowed.", signalId, parent.Name);
                    }
                }
            }

            this.VariableName = reader.GetAttribute("VariableName");

            reader.Skip();
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Input", ((uint)this.Input).ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("OutputEnabled", this.OutputEnabled.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Value", this.Value.ToString(CultureInfo.InvariantCulture));

            if (this.SelectedEvent != null)
            {
                writer.WriteAttributeString("Signal", this.SelectedEvent.Id.ToString("D", CultureInfo.InvariantCulture));
                writer.WriteAttributeString("SignalName", this.SelectedEvent.Name);
            }

            writer.WriteAttributeString("VariableName", this.VariableName);
        }
    }
}