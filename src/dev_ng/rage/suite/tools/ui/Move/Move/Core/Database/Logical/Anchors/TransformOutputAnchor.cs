﻿using System;
using System.Runtime.Serialization;
using System.Xml;

namespace New.Move.Core
{
    [Serializable]
    public class ResultTransform : ResultAnchor, ITransformAnchor, ITransformResult, ISerializable
    {
        public ResultTransform(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        public ResultTransform(SerializationInfo info, StreamingContext context)
            : this()
        {
            Id = (Guid)info.GetValue("Id", typeof(Guid));
            Parent = (New.Move.Core.ILogic)info.GetValue("Parent", typeof(New.Move.Core.ILogic));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.ResultTransform"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public ResultTransform(XmlReader reader, ILogic parent)
            : this(parent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Id = Guid.ParseExact(reader.GetAttribute("Id"), "D");
            reader.Skip();
        }

        ResultTransform()
            : base(null)
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Id", Id);
            info.AddValue("Parent", Parent);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString(
                "Id", this.Id.ToString("D", System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}