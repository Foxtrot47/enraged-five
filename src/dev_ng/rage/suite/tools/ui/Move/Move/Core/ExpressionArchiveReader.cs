﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Move.Utils;

namespace Move.Core
{
    internal class ExpressionArchiveReader
    {
        internal ExpressionArchiveReader()
        {
        }

        // Cache any data read from the zip to speed things up a little
        private static IEnumerable<string> cachedExpressionDictionaryNames_ = null;
        private static Dictionary<string, IEnumerable<string>> cachedExpressionNames_ = new Dictionary<string, IEnumerable<string>>();

        internal IEnumerable<string> ReadExpressionDictionaryNames()
        {
            if (cachedExpressionDictionaryNames_ != null)
                return cachedExpressionDictionaryNames_;

            try
            {
                if (Directory.Exists(Move.Utils.ContentConfig.ExportedExpressionsDirectory))
                {
                    var expressionDictionaryNamesQuery = from f in Directory.GetFiles(Move.Utils.ContentConfig.ExportedExpressionsDirectory, "*.ied.zip", SearchOption.AllDirectories)
                                                         select Path.GetFileNameWithoutExtension(f).Replace(".ied", "");
                    return cachedExpressionDictionaryNames_ = expressionDictionaryNamesQuery.ToArray();
                }
                return new string[] { };
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }

        internal IEnumerable<string> ReadExpressionNamesFromDictionary(string dictionaryName)
        {
            if (cachedExpressionNames_.ContainsKey(dictionaryName))
                return cachedExpressionNames_[dictionaryName];

            if (!Directory.Exists(Move.Utils.ContentConfig.ExportedExpressionsDirectory))
                return new string[] { };

            string filename = Directory.GetFiles(Move.Utils.ContentConfig.ExportedExpressionsDirectory, dictionaryName + ".ied.zip", SearchOption.AllDirectories).FirstOrDefault();
            try
            {
                if (!File.Exists(filename))
                    return new string[] { };

                List<string> expressions = new List<string>();
                using (var expressionsArchive = Ionic.Zip.ZipFile.Read(filename))
                {
                    foreach (var entry in expressionsArchive.Entries)
                    {
                        if (entry.FileName.EndsWith(".expr"))
                            expressions.Add(entry.FileName.Replace(".expr", ""));
                    }
                }
                cachedExpressionNames_.Add(dictionaryName, expressions.ToArray());
                return cachedExpressionNames_[dictionaryName];
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }
    }
}
