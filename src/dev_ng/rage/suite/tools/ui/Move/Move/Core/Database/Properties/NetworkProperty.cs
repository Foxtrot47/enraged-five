﻿using System;
using System.IO;
using System.ComponentModel;
using System.Runtime.Serialization;

using System.Collections.Generic;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class NetworkProperty : IProperty, ISerializable
    {
        
        public NetworkProperty()
        {
            Value = "";
            Name = "";
            Id = Guid.NewGuid();
        }
        

        public NetworkProperty(ILogic parent)
            : this()
        {
            Parent = parent;
        }

        public string Value { get; set; }
        public string Name { get; set; }
        public Guid Id { get; private set; }
        public ILogic Parent { get; set; }

        static class SerializationTag
        {
            public static string Value = "Value";
            public static string Name = "Name";
            public static string Id = "Id";
            public static string Parent = "Parent";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SetPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            PropertyChanged += new PropertyChangedEventHandler(handler);
        }

        public NetworkProperty(SerializationInfo info, StreamingContext context)
        {
            Value = (string)info.GetValue(SerializationTag.Value, typeof(string));
            Name = (string)info.GetValue(SerializationTag.Name, typeof(string));
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Parent = (ILogic)info.GetValue(SerializationTag.Parent, typeof(ILogic));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Value, Value);
            info.AddValue(SerializationTag.Name, Name);
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Parent, Parent);
        }
    }
}
