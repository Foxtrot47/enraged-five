﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

using Move.Core;
using Move.Utils;

namespace New.Move.Core
{
    [Serializable]
    public class Frame : SignalBase, ISerializable
    {
        public Frame(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public Frame(FrameSignal signal)
            : base(signal)
        {

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="New.Move.Core.Frame"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Frame(XmlReader reader, Database database, ITransitional parent)
            : base(reader, database, parent)
        {
        }

        protected override string XmlNodeName { get { return "framesignal"; } }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override void ExportToXML(PargenXmlNode parentNode, ICollection<string> hashedNames)
        {
        }

        public override ISelectable Clone(SignalBase clone)
        {
            clone = new Frame(new FrameSignal());
            (clone as Frame).AdditionalText = this.AdditionalText;
            (clone as Frame).Database = this.Database;
            (clone as Frame).Enabled = this.Enabled;
            (clone as Frame).Name = this.Name;
            (clone as Frame).Position = this.Position;
            (clone as Frame).Signal = this.Signal;

            return (ISelectable)clone;
        }
    }
}