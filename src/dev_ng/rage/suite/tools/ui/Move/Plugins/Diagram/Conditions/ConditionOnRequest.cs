﻿using System;
using System.Runtime.Serialization;

using Rage.Move.Core;

using Move.Core;
using New.Move.Core;
using Move.Utils;

namespace Move.Diagram.Conditions
{
    [Serializable]
    public class ConditionOnRequest : ConditionBase, ISerializable
    {
        public RequestConditionProperty Request { get; private set; }
        public BooleanConditionProperty NotSet { get; private set; }

        public ConditionOnRequest(string name)
            : base(name)
        {
            Request = new RequestConditionProperty();
            NotSet = false;
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredConditionOnRequest);
            }
        }

        static class SerializationTag
        {
            public static string Request = "Request";
            public static string NotSet = "NotSet";
        }

        public ConditionOnRequest(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Request = (RequestConditionProperty)info.GetValue(SerializationTag.Request, typeof(RequestConditionProperty));
            NotSet = (BooleanConditionProperty)info.GetValue(SerializationTag.NotSet, typeof(BooleanConditionProperty));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetSerializationData(info, context);

            info.AddValue(SerializationTag.Request, Request);
            info.AddValue(SerializationTag.NotSet, NotSet);
        }

        public override void GetExportData(ChunkObject info, ChunkContext context)
        {
            info.AppendUInt32(context.GetRequest(Request));
            uint notset = (uint)(NotSet.Value ? 1 : 0);
            info.AppendUInt32(notset);
        }

        public override uint GetExportSize()
        {
            return 
                sizeof(uint) + // request
                sizeof(uint); // notset
        }

        public override ushort GetExportId()
        {
            return (ushort)RegisteredConditionOnRequest.Const.Id;
        }
    }

    public class RegisteredConditionOnRequest : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "On Request";
            public static int Id = 2;
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }

        public ICondition Create()
        {
            return new ConditionOnRequest(Const.Name);
        }
    }
}