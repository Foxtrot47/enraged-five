﻿using System.Windows;

using New.Move.Core;
using Move.Core.Restorable;

namespace Move
{
    internal class CreateNodeFromDescAction : IExecuteAction
    {
        internal CreateNodeFromDescAction(Database database, IDesc desc, Point position)
        {
            _Database = database;
            _Desc = desc;
            _Position = position;
        }

        Database _Database;
        IDesc _Desc;

        Point _Position;

        public void Execute()
        {
            INode node = _Database.Create(_Desc);
            node.Position = _Position;

            _Database.Active.Add(node);
        }
    }
}