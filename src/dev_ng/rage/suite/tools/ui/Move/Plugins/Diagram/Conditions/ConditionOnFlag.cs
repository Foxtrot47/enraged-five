﻿using System;
using System.Runtime.Serialization;
using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Conditions
{
    [Serializable]
    public class ConditionOnFlag : ConditionBase, ISerializable
    {
        public FlagConditionProperty Flag { get; private set; }
        public BooleanConditionProperty NotSet { get; private set; }

        public ConditionOnFlag(string name)
        {
            Flag = new FlagConditionProperty();
            NotSet = false;
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredConditionOnFlag);
            }
        }

        static class SerializationTag
        {
            public static string Flag = "Flag";
            public static string NotSet = "NotSet";
        }

        public ConditionOnFlag(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Flag = (FlagConditionProperty)info.GetValue(SerializationTag.Flag, typeof(FlagConditionProperty));
            NotSet = (BooleanConditionProperty)info.GetValue(SerializationTag.NotSet, typeof(BooleanConditionProperty));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetSerializationData(info, context);

            info.AddValue(SerializationTag.Flag, Flag);
            info.AddValue(SerializationTag.NotSet, NotSet);
        }

        public override void GetExportData(ChunkObject info, ChunkContext context)
        {
            info.AppendUInt32(context.GetFlag(Flag));
            uint notset = (uint)(NotSet.Value ? 1 : 0);
            info.AppendUInt32(notset);
        }

        public override uint GetExportSize()
        {
            return
                sizeof(uint) + // flags
                sizeof(uint); // notset
        }

        public override ushort GetExportId()
        {
            return (ushort)RegisteredConditionOnFlag.Const.Id;
        }
    }

    public class RegisteredConditionOnFlag : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "On Flag";
            public static int Id = 3;
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }

        public ICondition Create()
        {
            return new ConditionOnFlag(Const.Name);
        }
    }
}