﻿using Rage.Move;
using New.Move.Core;

namespace Move.Diagram.Operators
{
    public class Multiply : IPlugin
    {
        static class Const
        {
            public static string Name = "multiply";
            public static string Description = "provides operator multiplication";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredOperatorMultiply desc = new RegisteredOperatorMultiply();

            //App.Instance.Runtime.LogicDescriptions.Add(desc);
            Motiontree.Desc.Children.Add(desc);
        }

        public void Dispose()
        {
        }
    }

    public class MultiplyConstant : IPlugin
    {
        static class Const
        {
            public static string Name = "multiply constant";
            public static string Description = "provides operator multiplication constant";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredOperatorMultiplyConstant desc = new RegisteredOperatorMultiplyConstant();

            //App.Instance.Runtime.LogicDescriptions.Add(desc);
            Motiontree.Desc.Children.Add(desc);
        }

        public void Dispose()
        {
        }
    }
}
