﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class AddNNode : Logic
    {
        static class ParameterOffset 
        {
            public const uint FilterN = 0;
            public const uint Filter = 1;
            public const uint Weight = 2;
            public const uint Output = ushort.MaxValue;
        }

        [ParameterExport(ParameterOffset.Weight)]
        public LogicalPropertyArray<
            LogicalPropertyPair<SourceTransform, RealLogicalProperty>> Inputs { get; private set; }

        [ParameterExport(ParameterOffset.FilterN)]
        public FilterLogicalProperty FilterN { get; private set; }

        [ParameterExport(ParameterOffset.Filter)]
        public FilterLogicalProperty Filter { get; private set; }

        [ParameterExport(ParameterOffset.Output)]
        public ResultTransform Result 
        { 
            get; private set; 
        }

        public AddNNode(IDesc desc)
            : base(desc)
        {
            Inputs = new LogicalPropertyArray<
                LogicalPropertyPair<SourceTransform, RealLogicalProperty>>();

            FilterN = new FilterLogicalProperty();
            Filter = new FilterLogicalProperty();

            Result = new ResultTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;

            uint flags = 0;
            flags |= FilterN.GetExportFlag();
            flags |= (Filter.GetExportFlag() << 4);

            flags |= ((uint)Inputs.Items.Count << 26);

            info.AppendUInt32(flags);

            FilterN.GetExportData(info, context);
            Filter.GetExportData(info, context);

            foreach (LogicalPropertyPair<SourceTransform, RealLogicalProperty> item in Inputs.Items)
            {
                info.AppendOffset(context.GetSourceLabelOf(item.First as SourceTransform));
            }

            uint words = (uint)((Inputs.Items.Count | 7) + 1) >> 3;
            uint[] subflags = new uint[words];

            for (int i = 0; i < Inputs.Items.Count; ++i)
            {
                int idx = (i & ~7) >> 3;
                int set = (i & 7);

                LogicalPropertyPair<SourceTransform, RealLogicalProperty> item = Inputs.Items[i];
                RealLogicalProperty weight = item.Second as RealLogicalProperty;
                subflags[idx] |= (weight.GetExportFlag() << (set * 4));
            }

            foreach (uint flag in subflags)
            {
                info.AppendUInt32(flag);
            }

            foreach (LogicalPropertyPair<SourceTransform, RealLogicalProperty> item in Inputs.Items)
            {
                RealLogicalProperty weight = item.Second as RealLogicalProperty;
                weight.GetExportData(info, context);
            }
        }
    }

    public class AddNNodeDesc : ILogicDesc
    {
        static class Const 
        {
            public static string Name = "N-Way Add";
            public static int Id = 22;
            public static string Help = "Insert AddN";
            public static string Group = "Blend";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _paletteIcon; } }

        public Type ConstructType { get { return typeof(AddNNode); } }

        public AddNNodeDesc()
        {
            try
            {
                // Load the palette icon embedded resource
                _paletteIcon = new BitmapImage();
                _paletteIcon.BeginInit();
                _paletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Move.Diagram.Nodes.AddN.Images.PaletteIcon_AddN.png" );
                _paletteIcon.EndInit();
            }
            catch
            {
            	_paletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new AddNNode(this);
        }

        protected BitmapImage _paletteIcon = null;
    }
}