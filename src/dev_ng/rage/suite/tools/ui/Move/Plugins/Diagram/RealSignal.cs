﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Move.UI
{
    public partial class DiagramSignal_Real : DiagramSignalControl
    {
        public DiagramSignal_Real(IAnchor source, FrameworkElement element)
            : base(source.Name, source)
        {
            _Element = element;
        }

        FrameworkElement _Element;

        public override Point? Sender
        {
            get
            {
                return new Point?(_Element.TranslatePoint(new Point(0, 0), GetDiagram(this)));
            }
        }

        protected override bool HitTestValid(DependencyObject hitobj)
        {
            return hitobj is DiagramAnchorRealInput || hitobj is DiagramAnchorRealSource;
        }
    }
}
