﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class FilterNode : Logic //LogicNode
    {
        static class ParameterOffset
        {
            public const uint Filter = 0;
            public const uint Output = 1;
        }

        [ParameterExport(ParameterOffset.Filter)]
        public FilterLogicalProperty Filter { get; private set; }

        [ParameterExport(ParameterOffset.Output)]
        public PassthroughTransform Result { get; private set; }

        public FilterNode(IDesc desc)
            : base(desc)
        {
            Filter = new FilterLogicalProperty();

            Result = new PassthroughTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;
            
            uint flags = 0;
            flags |= Filter.GetExportFlag();

            info.AppendUInt32(flags);

            info.AppendOffset(context.GetSourceLabelOf(Result));

            Filter.GetExportData(info, context);
        }
    }

    public class FilterNodeDesc : ILogicDesc //IRegisterNode
    {
        static class Const
        {
            public static string Name = "Filter";
            public static int Id = 7;
            public static string Help = "Filters an input source";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } } 
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(FilterNode); } }

        public FilterNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Diagram.Nodes.Filter.Images.PaletteIcon_Filter.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new FilterNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}