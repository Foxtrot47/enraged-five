﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

using New.Move.Core;

namespace Move.UI
{
    public abstract class DiagramSignalControl : DiagramAnchor
    {
        public DiagramSignalControl(string name, IAnchor source) 
            : base(name, source)
        {
            source.Tag = this;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            DiagramCanvas diagram = GetDiagram(this);
            if (diagram != null)
            {
                Start = Sender;
                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                DiagramCanvas diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        DiagramAdorner adorner = new DiagramAdorner(diagram, Start.Value, this);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

        protected virtual bool HitTestValid(DependencyObject hitobj)
        {
            return false;
        }

        public override IDiagramAnchor HitTest(Point point)
        {
            DiagramCanvas diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    //if (hitobj is DiagramAnchorSourceTransform || hitobj is DiagramAnchorPassthroughTransform)
                    if (HitTestValid(hitobj))
                        return hitobj as DiagramAnchor;

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }

            return null;
        }
    }
}