﻿using System;
using System.Runtime.Serialization;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Conditions
{
    [Serializable]
    public class ConditionBoolEqual : ConditionBase, ISerializable
    {
        public SignalConditionProperty Signal { get; private set; }
        public BooleanConditionProperty Value { get; private set; }

        public ConditionBoolEqual(string name)
            : base(name)
        {
            Signal = new SignalConditionProperty();
            Value = true;
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredConditionBoolEqual);
            }
        }

        static class SerializationTag
        {
            public static string Signal = "Signal";
            public static string Value = "Value";
        }

        public ConditionBoolEqual(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Signal = (SignalConditionProperty)info.GetValue(SerializationTag.Signal, typeof(SignalConditionProperty));
            Value = (BooleanConditionProperty)info.GetValue(SerializationTag.Value, typeof(BooleanConditionProperty));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetSerializationData(info, context);

            info.AddValue(SerializationTag.Signal, Signal);
            info.AddValue(SerializationTag.Value, Value);
        }

        public override void GetExportData(ChunkObject info, ChunkContext context)
        {
            throw new NotImplementedException();
        }

        public override uint GetExportSize()
        {
            throw new NotImplementedException();
        }

        public override ushort GetExportId()
        {
            return (ushort)RegisteredConditionBoolEqual.Const.Id;
        }
    }

    public class RegisteredConditionBoolEqual : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "Bool Equals";
            public static int Id = 9;
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }

        public ICondition Create()
        {
            return new ConditionBoolEqual(Const.Name);
        }
    }
}