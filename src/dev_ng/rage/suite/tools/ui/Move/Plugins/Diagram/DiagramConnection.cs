﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using New.Move.Core;

namespace Move.UI
{
    public class DiagramConnection : Control, INotifyPropertyChanged
    {
        public static readonly DependencyProperty IsProxyProperty =
            DependencyProperty.Register("IsProxy", typeof(bool), typeof(DiagramConnection));

        public bool IsProxy
        {
            get { return (bool)GetValue(IsProxyProperty); }
            set { SetValue(IsProxyProperty, value); }
        }

        public DiagramConnection(IDiagramAnchor sender, IDiagramAnchor receiver, Connection connection)
        {
            Id = Guid.NewGuid();

            Unloaded += new RoutedEventHandler(DiagramConnection_Unloaded);

            try {
                DiagramNode parent = sender.Container;
                parent.PositionChanged += new RoutedEventHandler(DiagramNode_PositionChanged);
                parent.SizeChanged += new SizeChangedEventHandler(DiagramNode_SizeChanged);
            }
            catch { }

            Sender = sender;
            Sender.Loaded += new RoutedEventHandler(DiagramAnchor_Loaded);

            try
            {
                DiagramNode parent = receiver.Container;
                parent.PositionChanged += new RoutedEventHandler(DiagramNode_PositionChanged);
                parent.SizeChanged += new SizeChangedEventHandler(DiagramNode_SizeChanged);
            }
            catch { }

            Receiver = receiver;
            Receiver.Loaded += new RoutedEventHandler(DiagramAnchor_Loaded);

            Connection = connection;
            IsProxy = connection.IsProxy;

            Connection.IsProxyChanged += new EventHandler<ConnectionProxyEventArgs>(Connection_IsProxyChanged);
        }

        void Connection_IsProxyChanged(object sender, ConnectionProxyEventArgs e)
        {
            IsProxy = e.IsProxy;
        }

        void DiagramConnection_Unloaded(object sender, RoutedEventArgs e)
        {
            Sender.Loaded -= new RoutedEventHandler(DiagramAnchor_Loaded);
            try
            {
                DiagramNode parent = Sender.Container;
                parent.PositionChanged -= new RoutedEventHandler(DiagramNode_PositionChanged);
                parent.SizeChanged -= new SizeChangedEventHandler(DiagramNode_SizeChanged);

            }
            catch { }

            Receiver.Loaded -= new RoutedEventHandler(DiagramAnchor_Loaded);
            try
            {
                DiagramNode parent = Receiver.Container;
                parent.PositionChanged -= new RoutedEventHandler(DiagramNode_PositionChanged);
                parent.SizeChanged -= new SizeChangedEventHandler(DiagramNode_SizeChanged);

            }
            catch { }

            Connection.IsProxyChanged -= new EventHandler<ConnectionProxyEventArgs>(Connection_IsProxyChanged);

            Connection.Tag = null;
        }

        void DiagramAnchor_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateGeometry();
        }

        public Guid Id { get; private set; }

        public Connection Connection { get; private set; }

        PathGeometry _Geometry;
        public PathGeometry Geometry
        {
            get { return _Geometry; }
            set
            {
                if (_Geometry != value)
                {
                    _Geometry = value;
                    OnPropertyChanged("Geometry");
                }
            }
        }

        IDiagramAnchor _Sender;
        public IDiagramAnchor Sender
        {
            get { return _Sender; }
            set
            {
                if (_Sender != value)
                {
                    _Sender = value;
                    UpdateGeometry();
                }
            }
        }

        IDiagramAnchor _Receiver;
        public IDiagramAnchor Receiver
        {
            get { return _Receiver; }
            set
            {
                if (_Receiver != value)
                {
                    _Receiver = value;
                    UpdateGeometry();
                }
            }
        }

        void DiagramNode_PositionChanged(object sender, EventArgs e)
        {
            UpdateGeometry();
        }

        void DiagramNode_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateGeometry();
        }

        void UpdateGeometry()
        {
            if (_Sender != null && _Sender.Sender.HasValue && _Receiver != null && _Receiver.Receiver.HasValue)
            {
                PathGeometry geometry = new PathGeometry();

                Point start = new Point(0, 0);

                Point end = new Point(
                    _Receiver.Receiver.Value.X - _Sender.Sender.Value.X,
                    _Receiver.Receiver.Value.Y - _Sender.Sender.Value.Y);

                if (_Receiver.Receiver.Value.Y < _Sender.Sender.Value.Y)
                {
                    double delta = _Sender.Sender.Value.Y - _Receiver.Receiver.Value.Y;

                    start.Y += delta;
                    end.Y += delta;
                }

                PathFigure figure = new PathFigure();
                figure.StartPoint = start;

                PointCollection points = new PointCollection();
                points.Add(new Point(end.X / 2, start.Y));
                points.Add(new Point(end.X / 2, end.Y));
                points.Add(end);

                figure.Segments.Add(new PolyBezierSegment(points, true));
                geometry.Figures.Add(figure);

                Geometry = geometry;
            }
        }

        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}