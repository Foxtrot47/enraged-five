﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

using New.Move.Core;

using Move.UI;

namespace Move
{
    public partial class DiagramViewer : UserControl
    {
        private static class Const
        {
            public static double PanMargin = 50;
        }

        public double Zoom
        {
            get
            {
                return ZoomSlider.Value;
            }
            set
            {
                if (value >= ZoomSlider.Minimum && value <= ZoomSlider.Maximum)
                {
                    Diagram.Scale = value;
                    ZoomSlider.Value = value;
                    UpdateScrollSize();
                    UpdateDatabaseZoom();
                }
            }
        }

        Point _ScrollStartPoint;
        Point _ScrollStartOffset;

        public DiagramViewer()
        {
            InitializeComponent();

            Zoom = 1.5;

            Diagram.DatabaseChanged += new EventHandler<EventArgs>(Diagram_DatabaseChanged);
        }

        bool _Loaded = false;

        protected override void OnInitialized(EventArgs e)
        {
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete,
                Diagram.ApplicationCommandDelete_Execute, Diagram.ApplicationCommandDelete_CanExecute));
            Diagram.Loaded += new RoutedEventHandler(Diagram_Loaded);
            Diagram.Unloaded += new RoutedEventHandler(Diagram_Unloaded);

            Diagram.SizeChanged += new SizeChangedEventHandler(Diagram_SizeChanged);

            ZoomSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(ZoomSlider_ValueChanged);
            ZoomSlider.MouseDoubleClick += new MouseButtonEventHandler(ZoomSlider_MouseDoubleClick);

            SizeChanged += new SizeChangedEventHandler(Diagram_SizeChanged);
            ScrollViewer.ScrollChanged += new ScrollChangedEventHandler(ScrollViewer_ScrollChanged);

            base.OnInitialized(e);
        }

        void Diagram_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_Loaded)
            {
                Diagram.Database.ActiveChanged += new RoutedEventHandler(Database_ActiveChanged);

                PathRoot.ItemsSource = Diagram.Database.Root.Children;

                Stack<INode> path = new Stack<INode>();

                INode node = Diagram.Database.Active;
                while (node != Diagram.Database.Root)
                {
                    path.Push(node);
                    node = Diagram.Database.Find(node.Parent);
                }

                if (path.Count != 0)
                {
                    INode curr = path.Pop();
                    Path.AddTrail(PathRoot, curr);

                    while (path.Count != 0)
                    {
                        INode last = curr;
                        curr = path.Pop();
                        Path.AddTrail(last, curr);
                    }
                }

                DependencyPropertyDescriptor desc =
                    DependencyPropertyDescriptor.FromProperty(Breadcrumb.SelectedItemProperty, typeof(Breadcrumb));
                desc.AddValueChanged(Path, Path_SelectedItemChanged);

                _Loaded = true;
            }

            UpdateScrollSize();
        }

        void Diagram_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_Loaded)
            {
                Diagram.Database.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);

                _Loaded = false;
            }
        }

        public ObservableCollection<BreadcrumbItem> PathItems = new ObservableCollection<BreadcrumbItem>();

        void Diagram_DatabaseChanged(object sender, EventArgs e)
        {
            Database db = sender as Database;
            ITransitional active = db.Active;

            ScrollViewer.ScrollToHorizontalOffset(active.ScrollOffset.X);
            ScrollViewer.ScrollToVerticalOffset(active.ScrollOffset.Y);

            Zoom = active.Zoom;
        }

        void Path_SelectedItemChanged(object sender, EventArgs e)
        {
            Breadcrumb path = sender as Breadcrumb;
            if (Diagram.Database != null)
            {
                if (path.SelectedItem is ITransitional)
                {
                    Diagram.Database.Active = path.SelectedItem as ITransitional;
                }
                else if (path.SelectedItem == PathRoot)
                {
                    Diagram.Database.Active = Diagram.Database.Root;
                }
            }
        }

        void Diagram_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateScrollSize();
        }

        void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Zoom = e.NewValue;
        }

        void ZoomSlider_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ZoomSlider.Value = 1.0;
        }

        void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.ExtentWidthChange != 0 && e.ExtentWidthChange != e.ExtentWidth)
            {
                // Keep centered horizontally.
                double percent = e.ExtentWidthChange / (e.ExtentWidth - e.ExtentWidthChange);
                double middle = e.HorizontalOffset + (e.ViewportWidth / 2);
                ScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset + (middle * percent));
            }

            if (e.ExtentHeightChange != 0 && e.ExtentHeightChange != e.ExtentHeight)
            {
                // Keep centered vertically.
                double percent = e.ExtentHeightChange / (e.ExtentHeight - e.ExtentHeightChange);
                double middle = e.VerticalOffset + (e.ViewportHeight / 2);
                ScrollViewer.ScrollToVerticalOffset(e.VerticalOffset + (middle * percent));
            }
        }

        void Database_ActiveChanged(object sender, RoutedEventArgs e)
        {
            ITransitional active = sender as ITransitional;

            UpdateScrollSize();

            ScrollViewer.ScrollToHorizontalOffset(active.ScrollOffset.X);
            ScrollViewer.ScrollToVerticalOffset(active.ScrollOffset.Y);

            Zoom = active.Zoom;

            ITransitional parent = active.Database.Find(active.Parent) as ITransitional;
            if (parent != null)
            {
                Path.AddTrail(parent, active);
            }
            else
            {
                Path.GoTo(PathRoot);
            }
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu context = (ContextMenu)sender;

            MenuItem createParent = new MenuItem();
            createParent.Header = "Create parent state machine...";
            createParent.Click += new RoutedEventHandler(CreateParent_Click);

            context.Items.Add(createParent);

            if (!Diagram.Database.Active.IsAutomaton)
            {
                MenuItem createReference = new MenuItem();
                createReference.Header = "Create reference...";
                createReference.Click += new RoutedEventHandler(CreateReference_Click);

                context.Items.Add(createReference);
            }

            MenuItem createParameterTunnel = new MenuItem();
            createParameterTunnel.Header = "Create parameter tunnel";
            createParameterTunnel.Click += new RoutedEventHandler(CreateParameterTunnel_Click);

            context.Items.Add(createParameterTunnel);

            MenuItem createEventTunnel = new MenuItem() { Header = "Create event tunnel" };
            createEventTunnel.Click += new RoutedEventHandler(CreateEventTunnel_Click);
            context.Items.Add(createEventTunnel);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu context = (ContextMenu)sender;
            context.Items.Clear();
        }

        void CreateParent_Click(object sender, RoutedEventArgs e)
        {
            ITransitional active = Diagram.Database.Active;
            ITransitional parent = active.Database.Create(new StateMachineDesc());
            parent.Parent = active.Parent;
            parent.Add(active);

            Diagram.Database.Active = parent;

            if (parent.Parent == null)
                Diagram.Database.Root = parent;
        }

        void CreateReference_Click(object sender, RoutedEventArgs e)
        {
            ITransitional active = Diagram.Database.Active;
            ILogic reference = active.Database.Create(new ReferenceDesc());
            active.Add(reference);
        }

        void CreateParameterTunnel_Click(object sender, RoutedEventArgs e)
        {
            ITransitional active = Diagram.Database.Active;
            ILogic tunnel = active.Database.Create(new TunnelParameterDesc());
            active.Add(tunnel);
        }

        void CreateEventTunnel_Click(object sender, RoutedEventArgs e)
        {
            ITransitional active = Diagram.Database.Active;
            ILogic tunnel = active.Database.Create(new TunnelEventDesc());
            active.Add(tunnel);
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            if (ScrollViewer.IsMouseOver && !Diagram.IsMouseOver)
            {
                _ScrollStartPoint = e.GetPosition(this);
                _ScrollStartOffset.X = ScrollViewer.HorizontalOffset;
                _ScrollStartOffset.Y = ScrollViewer.VerticalOffset;

                Cursor = (ScrollViewer.ExtentWidth > ScrollViewer.ViewportWidth) ||
                    (ScrollViewer.ExtentHeight > ScrollViewer.ViewportHeight) ?
                    Cursors.ScrollAll : Cursors.Arrow;

                CaptureMouse();

                Diagram.Database.SelectedItems.Clear();
            }
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            if (IsMouseCaptured)
            {
                Cursor = Cursors.Arrow;
                ReleaseMouseCapture();
            }
            base.OnPreviewMouseUp(e);
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            if (IsMouseCaptured)
            {
                Point point = e.GetPosition(this);

                Point delta = new Point(
                    (point.X > _ScrollStartPoint.X) ? -(point.X - _ScrollStartPoint.X) : (_ScrollStartPoint.X - point.X),
                    (point.Y > _ScrollStartPoint.Y) ? -(point.Y - _ScrollStartPoint.Y) : (_ScrollStartPoint.Y - point.Y));

                Point offset = new Point(_ScrollStartOffset.X + delta.X, _ScrollStartOffset.Y + delta.Y);

                ScrollViewer.ScrollToHorizontalOffset(offset.X);
                ScrollViewer.ScrollToVerticalOffset(offset.Y);

                Diagram.Database.Active.ScrollOffset = offset;
            }

            base.OnPreviewMouseMove(e);
        }

        void UpdateScrollSize()
        {
            if (ActualWidth == 0 || ActualHeight == 0)
                return;

            Size diagramSize = new Size(Diagram.ActualWidth * Zoom, Diagram.ActualHeight * Zoom);

            Grid.Width = Math.Max(0, (ActualWidth * 2) + diagramSize.Width - Const.PanMargin);
            Grid.Height = Math.Max(0, (ActualHeight * 2) + diagramSize.Height - Const.PanMargin);
        }

        void UpdateDatabaseZoom()
        {
            if (Diagram.Database != null)
            {
                Diagram.Database.Active.Zoom = ZoomSlider.Value;
            }
        }
        
        void CollectionView_Filter(object sender, FilterEventArgs e)
        {
            if (e.Item is IAutomaton)
                e.Accepted = true;

            e.Accepted = false;
        }

        void BackButton_Click(object sender, RoutedEventArgs e)
        {
            ITransitional active = Diagram.Database.Active;
            ITransitional parent = (ITransitional)Diagram.Database.Find(active.Parent);

            if (parent != null)
                Diagram.Database.Active = parent;
        }
    }
}
