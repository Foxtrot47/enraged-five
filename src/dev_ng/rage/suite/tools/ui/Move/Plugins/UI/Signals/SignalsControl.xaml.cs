﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AvalonDock;

using Move.Core;
using New.Move.Core;

namespace Move.UI
{
    public partial class SignalsControl : UserControl
    {
        public SignalsControl(Rage.Move.MainWindow wnd)
        {
            InitializeComponent();

            wnd.DocumentHost.SelectionChanged +=
                new SelectionChangedEventHandler(DocumentHost_SelectionChanged);
        }

        Database Database { get; set; }

        void DocumentHost_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (Database != null)
                Database.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);

            try
            {
                DocumentPane pane = sender as DocumentPane;
                DocumentContent content = pane.SelectedItem as DocumentContent;
                Database database = content.Tag as Database;

                Database = database;

                Database.ActiveChanged += new RoutedEventHandler(Database_ActiveChanged);

                UpdateSignals();
            }
            catch
            {
                Database = null;
            }
        }

        void SignalItems_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ItemsControl parent = sender as ItemsControl;

            Signal signal = GetObjectDataFromPoint(parent, e.GetPosition(parent));
            if (signal != null && !Database.Active.IsAutomaton)
            {
                DragObject drag = new DragObject(signal.Desc);
                DragDrop.DoDragDrop(parent, drag, DragDropEffects.Copy);
            }
        }

        void Database_ActiveChanged(object sender, RoutedEventArgs e)
        {
            UpdateSignals();
        }

        void UpdateSignals()
        {
            SignalItems.ItemsSource = Database.Signals;
            SignalItems.DisplayMemberPath = "Name";
        }

        static Signal GetObjectDataFromPoint(ItemsControl source, Point point)
        {
            UIElement element = source.InputHitTest(point) as UIElement;
            if (element != null)
            {
                object data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue)
                {
                    data = source.ItemContainerGenerator.ItemFromContainer(element);
                    if (data == DependencyProperty.UnsetValue)
                        element = VisualTreeHelper.GetParent(element) as UIElement;

                    if (element == source)
                        return null;
                }

                if (data != DependencyProperty.UnsetValue)
                    return data as Signal;
            }

            return null;
        }
    }
}
