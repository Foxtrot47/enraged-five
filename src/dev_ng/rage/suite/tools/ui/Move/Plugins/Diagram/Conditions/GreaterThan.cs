﻿using Rage.Move;

namespace Move.Diagram.Conditions
{
    public class GreaterThan : IPlugin
    {
        static class Const
        {
            public static string Name = "greater than";
            public static string Description = "provides transition condition greater than";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredConditionGreaterThan registered = new RegisteredConditionGreaterThan();
            App.Instance.Runtime.RegisteredConditions.Add(registered);
        }

        public void Dispose()
        {
        }
    }
}
