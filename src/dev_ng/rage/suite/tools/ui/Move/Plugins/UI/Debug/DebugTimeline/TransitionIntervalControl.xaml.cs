﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using rage;
using Move.Core;
using New.Move.Core;

namespace Move.UI
{
    /// <summary>
    /// Interaction logic for TransitionIntervalControl.xaml
    /// </summary>
    public partial class TransitionIntervalControl : UserControl
    {
        public TransitionIntervalControl()
        {
            InitializeComponent();
        }

        public TransitionIntervalControl(TransitionInterval t, bool up, RuntimeDatabase rdb)
        {
            Interval = t;
            OrientedUpwards = up;
            InitializeComponent();

            SourceTransition = rdb.GetTransition(t.StateFrom.StateId.State, t.TransId);

            ToolTip = new ToolTip();
        }

        string GetTooltipString()
        {
            string str = "";
            if (SourceTransition != null)
            {
                str += String.Format("Transition {0} from {1} to {2}", SourceTransition.Name, ((INode)(SourceTransition.Source)).Name, ((INode)(SourceTransition.Dest)));
            }
            else
            {
                str += String.Format("Transition {0} from {1}", Interval.TransId, Interval.StateFrom, Interval.StateTo);
            }
            str += String.Format("\n{0}", Interval.IntervalString());
            if (Interval.Status == Move.Core.Interval.StatusType.CLOSED)
            {
                str += String.Format("\nDuration: {0:0.###}ms", (Interval.EndTime - Interval.StartTime).TotalMilliseconds);
            }
            else
            {
                str += "\nDuration: N/A";
            }
            return str;
        }

        protected override void OnToolTipOpening(ToolTipEventArgs e)
        {
            ToolTip tt = (ToolTip)(ToolTip);
            tt.Content = GetTooltipString();
            base.OnToolTipOpening(e);
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            /*
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(this);
            layer.Add(new ActiveTransitionIntervalAdorner(this, OrientedUpwards));
             */
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);

            /*
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(this);
            if (layer != null)
            {
                Adorner[] adorners = layer.GetAdorners(this);
                if (adorners != null)
                {
                    int index = 0;
                    for (index = 0; index < adorners.Length; index++)
                    {
                        if (adorners[index] is ActiveTransitionIntervalAdorner)
                        {
                            layer.Remove(adorners[index]);
                            index = 0; // start over (who know if layer.Remove reorders things)
                        }
                    }
                }
            }
             */
        }

        public TransitionInterval Interval;
        public Transition SourceTransition; // always check for NULL - may not be available
        public bool OrientedUpwards;
    }

    public partial class ActiveTransitionIntervalAdorner : Adorner
    {
        public ActiveTransitionIntervalAdorner(UIElement adornedElement, bool up)
            : base(adornedElement)
        {
            OrientedUpwards = up;
            if (! (adornedElement is TransitionIntervalControl) )
            {
                throw new ArgumentException("adornedElement must be a TransitionIntervalControl", "adornedElement");
            }
            this.IsHitTestVisible = false;
        }

        protected override void OnRender(DrawingContext dc)
        {
            TransitionIntervalControl tic = (TransitionIntervalControl)AdornedElement;
            Rect controlRect = new Rect(AdornedElement.DesiredSize);

            double fromMiddle = (double)controlRect.Top - 10.0;
            double toMiddle = controlRect.Bottom + 10.0;

            double arrowBase = -10.0;
            if (OrientedUpwards)
            {
                math.Swap(ref fromMiddle, ref toMiddle);
                arrowBase = -arrowBase;
            }

            double axis = rage.math.Average(controlRect.Left, controlRect.Right);
            double left = rage.math.Lerp(0.25, controlRect.Left, controlRect.Right);
            double right = rage.math.Lerp(0.75, controlRect.Left, controlRect.Right);

            if (axis - left > 15.0)
            {
                left = axis - 15.0;
            }
            if (right - axis > 15.0)
            {
                right = axis + 15.0;
            }

            StreamGeometry sg = new StreamGeometry();
            sg.FillRule = FillRule.Nonzero;

            using (StreamGeometryContext ctx = sg.Open())
            {
                ctx.BeginFigure(new Point(math.Lerp(0.5, left, axis), fromMiddle), true, true);

                ctx.BezierTo(
                    new Point(axis, math.Lerp(0.3333, fromMiddle, toMiddle)),
                    new Point(axis, math.Lerp(0.6666, fromMiddle, toMiddle)),
                    new Point(rage.math.Lerp(0.5, left, axis), toMiddle + arrowBase), true, true
                    );

                ctx.LineTo(new Point(left, toMiddle + arrowBase), true, true);
                ctx.LineTo(new Point(axis, toMiddle), true, true);
                ctx.LineTo(new Point(right, toMiddle + arrowBase), true, true);
                ctx.LineTo(new Point(rage.math.Lerp(0.5, right, axis), toMiddle + arrowBase), true, true);
                ctx.BezierTo(
                    new Point(axis, math.Lerp(0.6666, fromMiddle, toMiddle)),
                    new Point(axis, math.Lerp(0.3333, fromMiddle, toMiddle)),
                    new Point(rage.math.Lerp(0.5, right, axis), fromMiddle), true, true
                    );
            }

            sg.Freeze();

            Brush fill = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));
            Pen outline = new Pen(Brushes.Black, 0.5);

            dc.DrawGeometry(fill, outline, sg);
        }

        public bool OrientedUpwards;
    }
}
