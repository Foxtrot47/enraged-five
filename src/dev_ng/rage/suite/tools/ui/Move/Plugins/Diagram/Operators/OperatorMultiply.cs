﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Operators
{
    public class OperatorItemMultiply : IOperatorItem
    {
        public void GetExportData(ChunkObject info, ChunkContext context)
        {
            info.AppendUInt32(4);
            info.AppendUInt32(0);
        }
    }

    public class OperatorMultiply : Logic, IOperator
    {
        public RealOutputAnchor Result { get; private set; }
        public RealInputAnchor Input0 { get; private set; }
        public RealInputAnchor Input1 { get; private set; }

        public OperatorMultiply(IDesc desc)
            : base(desc)
        {
            Result = new RealOutputAnchor(this);
            Input0 = new RealInputAnchor(this);
            Input1 = new RealInputAnchor(this);
        }

        public List<IAnchor> Inputs
        {
            get
            {
                List<IAnchor> inputs = new List<IAnchor>();
                inputs.Add(Input0);
                inputs.Add(Input1);

                return inputs;
            }
        }

        public IOperatorItem Item { get { return new OperatorItemMultiply(); } }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class RegisteredOperatorMultiply : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Multiply";
            public static int Id = 3;
            public static string Help = "Multiply operation";
            public static string Group = "Operator";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _paletteIcon; } }

        public Type ConstructType { get { return typeof(OperatorMultiply); } }

        public RegisteredOperatorMultiply()
        {
            try
            {
                // Load the palette icon embedded resource
                _paletteIcon = new BitmapImage();
                _paletteIcon.BeginInit();
                _paletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Move.Diagram.Operators.Multiply.Images.PaletteIcon_OpMultiply.png" );
                _paletteIcon.EndInit();
            }
            catch
            {
            	_paletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new OperatorMultiply(this);
        }

        protected BitmapImage _paletteIcon = null;
    }

    public class OperatorMultiplyConstant : Logic, IOperator
    {
        public RealLogicalProperty Constant { get; private set; }
        public RealOutputAnchor Result { get; private set; }
        public RealInputAnchor Input { get; private set; }

        public OperatorMultiplyConstant(IDesc desc)
            : base(desc)
        {
            Constant = 1.0f;

            Result = new RealOutputAnchor(this);
            Input = new RealInputAnchor(this);
        }

        public List<IAnchor> Inputs
        {
            get
            {
                List<IAnchor> inputs = new List<IAnchor>();
                inputs.Add(Input);

                return inputs;
            }
        }

        public IOperatorItem Item { get { return new OperatorItemMultiply(); } }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class RegisteredOperatorMultiplyConstant : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Multiply Constant";
            public static int Id = 4;
            public static string Help = "Multiply constant operation";
            public static string Group = "Operator";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _paletteIcon; } }

        public Type ConstructType { get { return typeof(OperatorMultiplyConstant); } }

        public RegisteredOperatorMultiplyConstant()
        {
            try
            {
                // Load the palette icon embedded resource
                _paletteIcon = new BitmapImage();
                _paletteIcon.BeginInit();
                _paletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Move.Diagram.Operators.Multiply.Images.PaletteIcon_OpMultiplyConstant.png" );
                _paletteIcon.EndInit();
            }
            catch
            {
            	_paletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new OperatorMultiplyConstant(this);
        }

        protected BitmapImage _paletteIcon = null;
    }
}