﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    public class AnimationNode : Logic
    {
        static class Const
        {
            public const uint AnimationParameterExportOffset = 0;
            public const uint PhaseParameterExportOffset = 1;
            public const uint RateParameterExportOffset = 2;
            public const uint DeltaParameterExportOffset = 3;
            public const uint LoopedParameterExportOffset = 4;
            public const uint AbsoluteParameterExportOffset = 5;
            public const uint OutputParameterExportOffset = ushort.MaxValue;

            public const uint LoopedEventExportOffset = 0;
            public const uint EndedEventExportOffset = 1;
        }

        [ParameterExport(Const.AnimationParameterExportOffset)]
        public AnimationLogicalProperty Animation 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.PhaseParameterExportOffset)]
        public RealLogicalProperty Phase 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.RateParameterExportOffset)]
        public RealLogicalProperty Rate 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.DeltaParameterExportOffset)]
        public RealLogicalProperty Delta 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.LoopedParameterExportOffset)]
        public BooleanLogicalProperty Looped 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.AbsoluteParameterExportOffset)]
        public BooleanLogicalProperty Absolute 
        { 
            get; 
            private set; 
        }

        [EventExport(Const.LoopedEventExportOffset)]
        public LogicalEventProperty AnimationLooped
        {
            get;
            private set;
        }

        [EventExport(Const.EndedEventExportOffset)]
        public LogicalEventProperty AnimationEnded
        {
            get;
            private set;
        }

        [ParameterExport(Const.OutputParameterExportOffset)]
        public ResultTransform Result 
        { 
            get; 
            private set; 
        }

        public AnimationNode(IDesc desc)
            : base(desc)
        {
            Animation = "";
            Phase = 0.0f;
            Rate = 1.0f;
            Delta = 0.0f;
            Looped = true;
            Absolute = false;

            AnimationLooped = new LogicalEventProperty();
            AnimationEnded = new LogicalEventProperty();

            Result = new ResultTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;

            uint flags = 0;
            flags |= Animation.GetExportFlag();
            flags |= (Phase.GetExportFlag() << 4);
            flags |= (Rate.GetExportFlag() << 8);
            flags |= (Delta.GetExportFlag() << 12);
            flags |= (Looped.GetExportFlag() << 16);
            flags |= (Absolute.GetExportFlag() << 20);

            info.AppendUInt32(flags);

            // write out the additional data block

            Animation.GetExportData(info, context);
            Phase.GetExportData(info, context);
            Rate.GetExportData(info, context);
            Delta.GetExportData(info, context);
            Looped.GetExportData(info, context);
            Absolute.GetExportData(info, context);
        }
    }

    public class AnimationNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Animation";
            public static int Id = 4;
            public static string Help = "Supplies animation data";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(AnimationNode); } }

        public AnimationNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                    "Move.Diagram.Nodes.Animation.Images.PaletteIcon_Animation.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new AnimationNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}