﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class MirrorNode : Logic
    {
        static class Const
        {
            public const uint FilterParameterExportOffset = 0;
            public const uint OUtputParameterExportOffset = ushort.MaxValue;
        }

        [ParameterExport(Const.FilterParameterExportOffset)]
        public FilterLogicalProperty Filter 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.OUtputParameterExportOffset)]
        public PassthroughTransform Result 
        { 
            get; 
            private set; 
        }

        public MirrorNode(IDesc desc)
            : base(desc)
        {
            Filter = new FilterLogicalProperty();

            Result = new PassthroughTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;

            uint flags = 0;
            flags |= Filter.GetExportFlag();

            info.AppendUInt32(flags);

            info.AppendOffset(context.GetSourceLabelOf(Result));

            // write out the additional data block
            Filter.GetExportData(info, context);
        }
    }

    public class MirrorNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "mirror";
            public static int Id = 8;
            public static string Help = "mirrors input";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(MirrorNode); } }

        public MirrorNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Move.Diagram.Nodes.Mirror.Images.PaletteIcon_Mirror.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new MirrorNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}