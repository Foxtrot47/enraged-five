﻿using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

using New.Move.Core;

using Rage.Move;

namespace Move.UI
{
    public class DiagramTransitionAdorner : Adorner
    {
        public DiagramTransitionAdorner(DiagramViewer diagram, DiagramState item, Point source)
            : base(diagram)
        {
            _Source = source;

            _Pen = new Pen(Brushes.LightSlateGray, 2);
            _Pen.LineJoin = PenLineJoin.Round;
            Cursor = Cursors.Cross;

            _Diagram = diagram;
            _Item = item;

        }

        PathGeometry _Geometry;
        Point _Source;
        Pen _Pen;

        DiagramViewer _Diagram;

        DiagramState _Item;
        DiagramState _Hit;

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!IsMouseCaptured)
                    CaptureMouse();

                _Hit = HitTest(e.GetPosition(this));

                _Geometry = Calculate(e.GetPosition(this));

                InvalidateVisual();
            }
            else
            {
                if (IsMouseCaptured)
                    ReleaseMouseCapture();
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            Database database = _Item.Source.Database;
            IAutomaton parent = database.Find(_Item.Source.Parent) as IAutomaton;

            if (_Hit != null)
            {
                ITransitionDesc desc = App.Instance.Runtime.TransitionDescriptors.FirstOrDefault();
                //Rage.Move.Core.Dg.ITransition transition = desc.Create(_Item.Source as ITransitional, _Hit.Source as ITransitional);

                Transition transition = new Transition(desc, _Item.Source as ITransitional, _Hit.Source as ITransitional, parent);

                parent.Transitions.Add(transition);
            }

            if (IsMouseCaptured)
                ReleaseMouseCapture();

            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(_Diagram);
            if (adornerLayer != null)
                adornerLayer.Remove(this);
        }

        protected override void OnRender(DrawingContext context)
        {
            base.OnRender(context);

            context.DrawGeometry(null, _Pen, _Geometry);
            context.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));
        }

        DiagramState HitTest(Point point)
        {
            DependencyObject obj = _Item as DependencyObject;
            while (obj != null & !(obj is DiagramState))
                obj = VisualTreeHelper.GetParent(obj);

            DiagramState parent = obj as DiagramState;
            if (parent != null)
            {
                DependencyObject hit = _Diagram.InputHitTest(point) as DependencyObject;
                while (hit != null && hit.GetType() != typeof(DiagramViewer) && hit != parent)
                {
                    if (hit is DiagramState)
                        return hit as DiagramState;

                    hit = VisualTreeHelper.GetParent(hit);
                }
            }

            return null;
        }

        PathGeometry Calculate(Point position)
        {
            PathGeometry geometry = new PathGeometry();

            PathFigure figure = new PathFigure();
            figure.StartPoint = _Source;

            figure.Segments.Add(new PolyLineSegment(new PointCollection() { position }, true));
            geometry.Figures.Add(figure);

            return geometry;
        }
    }
}