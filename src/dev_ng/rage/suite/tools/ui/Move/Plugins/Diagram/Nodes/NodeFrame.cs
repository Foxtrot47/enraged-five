﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class FrameNode : Logic
    {
        static class Const
        {
            public const uint FrameParameterExportOffset = 0;
            public const uint OwnerParameterExportOffset = 1;
            public const uint OutputParameterExportOffset = ushort.MaxValue;
        }

        [ParameterExport(Const.FrameParameterExportOffset)]
        public FrameLogicalProperty Frame 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.OwnerParameterExportOffset)]
        public BooleanLogicalProperty Owner 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.OutputParameterExportOffset)]
        public ResultTransform Result 
        { 
            get; 
            private set; 
        }

        public FrameNode(IDesc desc)
            : base(desc)
        {
            Frame = new FrameLogicalProperty();
            Owner = true;

            Result = new ResultTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;

            uint flags = 0;
            flags |= Frame.GetExportFlag();
            flags |= (Owner.GetExportFlag() << 4);

            info.AppendUInt32(flags);

            // write out additional data block
            Frame.GetExportData(info, context);
            Owner.GetExportData(info, context);
        }
    }

    public class FrameNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Frame";
            public static int Id = 9;
            public static string Help = "Insert frame";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(FrameNode); } }

        public FrameNodeDesc()
        {
            try
            {
                // Load the palette icon embedded resource
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream("Move.Diagram.Nodes.Frame.Images.PaletteIcon_Frame.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new FrameNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}
