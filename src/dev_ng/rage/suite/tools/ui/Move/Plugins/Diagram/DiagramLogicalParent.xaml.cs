﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rage.Move.Utils;
using New.Move.Core;

using Move.UI;

namespace Move
{
    public partial class DiagramLogicalParent : DiagramNode
    {
        public DiagramLogicalParent(ILogic source)
            : base(source)
        {
            InitializeComponent();

            CreateAnchors();
        }

        protected override void OnPreviewMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDoubleClick(e);

            if (e.ChangedButton == MouseButton.Left)
            {
                DiagramViewer viewer = GetDiagram(this);
                DiagramCanvas diagram = viewer.Diagram;

                Database database = diagram.Database;
                database.Active = Source as LogicParent;

                e.Handled = true;
            }
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete node";
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);

            contextMenu.Items.Add(deleteItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;
            contextMenu.Items.Clear();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }

        public void CreateAnchors()
        {
            Anchors.Children.Clear();

            PropertyInfo[] properties = Source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                object value = pi.GetValue(Source, null);
                if (value != null && value.GetType().GetInterface("IConnectionAnchor") != null)
                {
                    IAnchor anchor = (IAnchor)pi.GetValue(Source, null);
                    Type type = anchor.GetType();
                    DiagramAnchor item = AnchorLookup[type](anchor, pi.Name);
                    item.Container = this;

                    Anchors.Children.Add(item);
                }
            }
        }

        static DiagramAnchor Instantiate_PassthroughTransform(IAnchor anchor, string name)
        {
            DiagramAnchorPassthroughTransform inst = new DiagramAnchorPassthroughTransform(name, anchor);
            anchor.Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_ResultTransform(IAnchor anchor, string name)
        {
            DiagramAnchorResultTransform inst = new DiagramAnchorResultTransform(name, anchor);
            anchor.Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_SourceTransform(IAnchor anchor, string name)
        {
            DiagramAnchorSourceTransform inst = new DiagramAnchorSourceTransform(name, anchor);
            anchor.Tag = inst;
            return inst;
        }

        static Dictionary<Type, AnchorDelegate> AnchorLookup = new Dictionary<Type, AnchorDelegate>
        {
            { typeof(PassthroughTransform), Instantiate_PassthroughTransform },
            { typeof(ResultTransform), Instantiate_ResultTransform },            
            { typeof(SourceTransform), Instantiate_SourceTransform },
        };
    }
}
