﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class MergeNNode : Logic
    {
        static class ParameterOffset
        {
            public const uint FilterN = 0;
            public const uint Filter = 1;
            public const uint Output = ushort.MaxValue;
        }

        public LogicalPropertyArray<SourceTransform> Inputs { get; private set; }

        [ParameterExport(ParameterOffset.FilterN)]
        public FilterLogicalProperty FilterN
        {
            get;
            private set;
        }

        [ParameterExport(ParameterOffset.Filter)]
        public FilterLogicalProperty Filter
        {
            get;
            private set;
        }

        [ParameterExport(ParameterOffset.Output)]
        public ResultTransform Result 
        { 
            get; private set; 
        }

        public MergeNNode(IDesc desc)
            : base(desc)
        {
            Inputs = new LogicalPropertyArray<SourceTransform>();
            FilterN = new FilterLogicalProperty();
            Filter = new FilterLogicalProperty();
            Result = new ResultTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class MergeNNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "N-Way Merge";
            public static int Id = 26;
            public static string Help = "merges n input sources";
            public static string Group = "Blend";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(MergeNNode); } }

        public MergeNNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Diagram.Nodes.MergeN.Images.PaletteIcon_MergeN.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new MergeNNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}