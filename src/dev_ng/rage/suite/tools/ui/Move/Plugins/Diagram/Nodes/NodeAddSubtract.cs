﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class AddSubtractNode : Logic
    {
        static class ParameterOffset
        {
            public const uint Filter = 0;
            public const uint Weight = 1;
            public const uint Output = ushort.MaxValue;
        }

        [ParameterExport(ParameterOffset.Filter)]
        public FilterLogicalProperty Filter 
        { 
            get; 
            private set; 
        }

        [ParameterExport(ParameterOffset.Weight)]
        public RealLogicalProperty Weight 
        { 
            get; 
            private set; 
        }

        [ParameterExport(ParameterOffset.Output)]
        public ResultTransform Result 
        { 
            get; private set; 
        }

        public SourceTransform Source0 { get; private set; }
        public SourceTransform Source1 { get; private set; }

        public AddSubtractNode(IDesc desc)
            : base(desc)
        {
            Filter = new FilterLogicalProperty();
            Weight = 0.0f;

            Result = new ResultTransform(this);
            Source0 = new SourceTransform(this);
            Source1 = new SourceTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;
            
            uint flags = 0;
            flags |= Weight.GetExportFlag();
            flags |= (Filter.GetExportFlag() << 4);

            info.AppendUInt32(flags);

            info.AppendOffset(context.GetSourceLabelOf(Source0));
            info.AppendOffset(context.GetSourceLabelOf(Source1));

            Weight.GetExportData(info, context);
            Filter.GetExportData(info, context);
        }
    }

    public class AddSubtractNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Add Subtract";
            public static int Id = 6;
            public static string Help = "Insert Add Subtract";
            public static string Group = "Blend";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(AddSubtractNode); } }

        public AddSubtractNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource =
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Diagram.Nodes.AddSubtract.Images.PaletteIcon_AddSubtract.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new AddSubtractNode(this);
        }

        protected BitmapImage _PaletteIcon = null;

    }

}