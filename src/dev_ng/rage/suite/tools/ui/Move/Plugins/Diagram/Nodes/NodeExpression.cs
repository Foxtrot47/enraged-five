﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class ExpressionNode : Logic
    {
        static class ParameterOffset
        {
            public const uint Expressions = 0;
            public const uint Output = 1;
        }

        [ParameterExport(ParameterOffset.Expressions)]
        public ExpressionsLogicalProperty Expressions { get; private set; }

        [ParameterExport(ParameterOffset.Output)]
        public PassthroughTransform Result { get; private set; }

        public ExpressionNode(IDesc desc)
            : base(desc)
        {
            Expressions = "";

            Result = new PassthroughTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;
            
            uint flags = 0;
            flags |= Expressions.GetExportFlag();

            info.AppendUInt32(flags);

            info.AppendOffset(context.GetSourceLabelOf(Result));

            Expressions.GetExportData(info, context);
        }
    }

    public class ExpressionNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Expression";
            public static int Id = 19;
            public static string Help = "Adds expression data";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(ExpressionNode); } }

        public ExpressionNodeDesc()
        {
            try
            {
                // Load the palette icon embedded resource
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Move.Diagram.Nodes.Expression.Images.PaletteIcon_Expression.png" );
                _PaletteIcon.EndInit();
            }
            catch
            {
            	_PaletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new ExpressionNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}