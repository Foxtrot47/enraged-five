﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using Rage.Move;
using Rage.Move.Core;
using New.Move.Core;
using Rage.Move.Utils;
using Move.Core;
using Move.Utils;
using Rockstar.MoVE.Services;
using Rockstar.MoVE.Components.MainWindow;

using AvalonDock;

namespace Move.UI
{

    /// <summary>
    /// Interaction logic for DebugTimelineControl.xaml
    /// </summary>
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Debug Timeline", WindowDockRegion.Bottom, WindowDockState.Hidden)]
    public partial class DebugTimelineControl : DockableContent
    {
        public DebugTimelineControl()
        {
            InitializeComponent();

            _TimelineRows = (TimelineRowSet)Resources["TimelineRows"];

            ResetEverything();

            _TimeStrip.Timeline = _Timeline;

            // Connect the vertical scroll bars of the two scrollviewers
            TreeScroller.ScrollChanged += delegate { TimelineScroller.ScrollToVerticalOffset(TreeScroller.VerticalOffset); };
            TimelineScroller.ScrollChanged += delegate { TreeScroller.ScrollToVerticalOffset(TimelineScroller.VerticalOffset); };

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(100);
            timer.Tick += RedrawTimeline;
            timer.Start();
        }

        [Import(typeof(IRuntimeCommunicationService))]
        IRuntimeCommunicationService RuntimeCommunicationService
        {
            set
            {
                _RuntimeCommunicationService = value;
                _RuntimeCommunicationService.StartDebugging += Client_StartDebugging;
                _RuntimeCommunicationService.StopDebugging += Client_StopDebugging;
            }
            get
            {
                return _RuntimeCommunicationService;
            }
        }
        IRuntimeCommunicationService _RuntimeCommunicationService;

        #region Network Event Handlers

        void ResetEverything()
        {
            DetachDebugEventHandlers();

            _RuntimeDb = null;
            Database = null;
            _Link = null;

            _TimelineRows.Reset();
            _Timeline.SetPhase(0.0, 1.0);
            _Timeline.SetDisplay(0.0, 1.0);
            _Timeline.EndInsetPixels = 0; // Turn this off for now til I fix more fundamental drawing problems

            _StartTime = 0.0;
            _LatestTime = 1.0;

            InvalidateTimeline(null, null);
        }

        void Client_StartDebugging(object sender, SimpleEventArgs<IDebugSession> args)
        {
            // This uses Invoke instead of BeginInvoke so that we know for sure the network and UI threads are 
            // synchronized when this function is called
            // (don't want queued actions in the UI thread to refer to the wrong runtime DB)
            this.InvokeAction(() => {
                // Keep a handle to the runtime db and main db
                ResetEverything();
                ClampAndSetPhaseView(0.0, 1.0);

                Client.DebugSession session = (Client.DebugSession)args.Payload;

                _RuntimeDb = session.RuntimeDb;
                Database = session.Database;
                _Link = session.Link;
                AttachDebugEventHandlers();

                if (Database != null)
                {
                    Debug.Assert(Database.Flags.Count == _RuntimeDb.NumFlags, "Number of flags doesn't match");
                    Debug.Assert(Database.Requests.Count == _RuntimeDb.NumRequests, "Number of requests doesn't match");
                }

                for (int i = 0; i < _RuntimeDb.NumFlags; i++)
                {
                    string flagName = String.Format("Flag {0}", i);
                    if (Database != null)
                    {
                        flagName = Database.Flags[i].Name;
                    }
                    var row = new StatebitTimelineRow(flagName, i);
                    _TimelineRows.AddNewRowToParent(row, _TimelineRows.Flags);
                }

                for (int i = 0; i < _RuntimeDb.NumRequests; i++)
                {
                    string reqName = String.Format("Request {0}", i);
                    if (Database != null)
                    {
                        reqName = Database.Requests[i].Name;
                    }
                    var row = new StatebitTimelineRow(reqName, i);
                    _TimelineRows.AddNewRowToParent(row, _TimelineRows.Requests);
                }

                RedrawTimeline(null, null);
            });
        }

        void Client_StopDebugging(object sender, SimpleEventArgs<IDebugSession> args)
        {
            DetachDebugEventHandlers();
        }

        void DetachDebugEventHandlers()
        {
            if (_RuntimeDb != null)
            {
                _RuntimeDb.StateIntervalChanged -= HandleStateIntervalChanged;
                _RuntimeDb.StateIntervalAdded -= HandleStateIntervalChanged;
                _RuntimeDb.StateDagChanged -= HandleStateDagChanged;
            }
            if (_Link != null)
            {
                _Link.UpdateRoot -= HandleUpdateRoot;
                _Link.StartTransition -= HandleStartTransition;
                _Link.StopTransition -= HandleStopTransition;
                _Link.StartStateSet -= HandleStartStateSet;
                _Link.StopStateSet -= HandleStopStateSet;
                _Link.SignalSet -= HandleSignalSet;
            }
        }

        void AttachDebugEventHandlers()
        {
            _RuntimeDb.StateIntervalAdded += HandleStateIntervalChanged;
            _RuntimeDb.StateIntervalChanged += HandleStateIntervalChanged;
            _RuntimeDb.StateDagChanged += HandleStateDagChanged;
            _Link.UpdateRoot += HandleUpdateRoot;
            _Link.StartTransition += HandleStartTransition;
            _Link.StopTransition += HandleStopTransition;
            _Link.StartStateSet += HandleStartStateSet;
            _Link.StopStateSet += HandleStopStateSet;
            _Link.SignalSet += HandleSignalSet;
        }

        #endregion

        #region UI Event Handlers

        private void TimelineScroller_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            // Only do this stuff on horizontal scrolling
            if (Math.Abs(e.ExtentWidthChange) > 0.01 || Math.Abs(e.HorizontalChange) > 0.01 || Math.Abs(e.ViewportWidthChange) != 0.0)
            {
                double min = rage.math.RampValue(e.HorizontalOffset, 0, e.ExtentWidth, _Timeline.PhaseMin, _Timeline.PhaseMax);
                double max = rage.math.RampValue(e.HorizontalOffset + e.ViewportWidth, 0, e.ExtentWidth, _Timeline.PhaseMin, _Timeline.PhaseMax);

                ClampAndSetPhaseView(min, max);

                UpdateIntervalsForNewPhaseView();
            }

        }

        void AddBackgroundHighlight(object sender, MouseEventArgs e)
        {
            if (_BackgroundHighlightRect != null)
            {
                RemoveBackgroundHighlight(null, null);
            }

            StateIntervalControl sic = (StateIntervalControl)sender;
            var row = _TimelineRows.FindStateIntervalRow(sic.Interval.StateId.State);

            if (!row.HasChildren)
            {
                return;
            }

            _BackgroundHighlightInterval.CopyTimesFrom(sic.Interval);

            _BackgroundHighlightRect = new Rectangle();
            _BackgroundHighlightRect.Fill = GetColorForState(sic.Interval.StateId.State).Clone();
            _BackgroundHighlightRect.Height = row.ChildBottom - row.Top;
            _BackgroundHighlightRect.SetValue(Canvas.TopProperty, row.Top);
            _BackgroundHighlightRect.SetValue(Canvas.ZIndexProperty, ZOrderBackgroundHighlightRect);

            _BackgroundHighlightRect.Fill.Opacity = 0.0;
            DoubleAnimation opacityAnim = new DoubleAnimation(0.25, new Duration(TimeSpan.FromSeconds(0.1)));
            _BackgroundHighlightRect.Fill.BeginAnimation(Brush.OpacityProperty, opacityAnim);
            
            RepositionOneInterval(_BackgroundHighlightInterval, _BackgroundHighlightRect);

            TimelineCanvas.Children.Add(_BackgroundHighlightRect);
            _DisplayedIntervals[_BackgroundHighlightInterval] = new List<FrameworkElement>() { _BackgroundHighlightRect };
        }

        void RemoveBackgroundHighlight(object sender, MouseEventArgs e)
        {
            if (_BackgroundHighlightRect != null)
            {
                _DisplayedIntervals.Remove(_BackgroundHighlightInterval);

                var currentRect = _BackgroundHighlightRect;

                DoubleAnimation opacityAnim = new DoubleAnimation(0.0, new Duration(TimeSpan.FromSeconds(0.3)));
                opacityAnim.Completed += (s, a) => { TimelineCanvas.Children.Remove(currentRect); };
                _BackgroundHighlightRect.Fill.BeginAnimation(Brush.OpacityProperty, opacityAnim);

                _BackgroundHighlightRect = null;
            }
        }

        #endregion

        #region Runtime DB Event Handlers

        // Comes from the networking thread - just update the database diagram (more work is done via the StateInterval messages)
        void HandleStartTransition(object sender, SimpleEventArgs<LinkNetworkMessageBase> args)
        {
            if (Database == null)
            {
                return;
            }
            this.BeginInvokeAction(() => 
            {
                StartTransitionMessage msg = (StartTransitionMessage)args.Payload;
                INode toNode = Database.Find(msg.To.State.RawId);
                if (toNode != null)
                {
                    if (!Database.EngagedItems.Contains(toNode))
                        Database.EngagedItems.Add(toNode);
                }

                foreach (var state in msg.Descendants)
                {
                    INode node = Database.Find(state.State.RawId);
                    if (node != null && !Database.EngagedItems.Contains(node))
                    {
                        Database.EngagedItems.Add(node);
                    }
                }
            });
        }

        void HandleStopTransition(object sender, SimpleEventArgs<LinkNetworkMessageBase> args)
        {
            if (Database == null) 
            {
                return;
            }
            this.BeginInvokeAction(() =>
            {
                StopTransitionMessage msg = (StopTransitionMessage)args.Payload;
                INode fromNode = Database.Find(msg.From.State.RawId);
                if (fromNode != null)
                {
                    if (Database.EngagedItems.Contains(fromNode))
                        Database.EngagedItems.Remove(fromNode);
                }
                foreach (var state in msg.Descendants)
                {
                    INode node = Database.Find(state.State.RawId);
                    if (node != null && Database.EngagedItems.Contains(node))
                    {
                        Database.EngagedItems.Remove(node);
                    }
                }
            });
        }

        void HandleStartStateSet(object sender, SimpleEventArgs<LinkNetworkMessageBase> args)
        {
            if (Database == null)
            {
                return;
            }
            this.BeginInvokeAction(() =>
            {
                StartStateSetMessage msg = (StartStateSetMessage)args.Payload;
                foreach (var nodeId in msg.States)
                {
                    INode startNode = Database.Find(nodeId.State.RawId);
                    if (startNode != null)
                    {
                        if (!Database.EngagedItems.Contains(startNode))
                        {
                            Database.EngagedItems.Add(startNode);
                        }
                    }
                }
            });
        }

        void HandleStopStateSet(object sender, SimpleEventArgs<LinkNetworkMessageBase> args)
        {
            if (Database == null)
            {
                return;
            }
            this.BeginInvokeAction(() =>
            {
                StopStateSetMessage msg = (StopStateSetMessage)args.Payload;
                foreach (var nodeId in msg.States)
                {
                    INode startNode = Database.Find(nodeId.State.RawId);
                    if (startNode != null)
                    {
                        if (Database.EngagedItems.Contains(startNode))
                        {
                            Database.EngagedItems.Remove(startNode);
                        }
                    }
                }
            });
        }

        void HandleSignalSet(object sender, SimpleEventArgs<LinkNetworkMessageBase> args)
        {
            this.BeginInvokeAction(() =>
            {
                SignalSetMessage msg = (SignalSetMessage)args.Payload;

                foreach (var sig in msg.Signals)
                {
                    var intervalRow = _TimelineRows.FindStateIntervalRow(sig.Parent.State);
                    if (!intervalRow.HasSignalGroup)
                    {
                        _TimelineRows.AddSignalGroupRow(sig.Parent.State);
                    }
                    TimelineRow signal = intervalRow.SignalGroup.FindSignal((ushort)sig.Id);
                    if (signal == null)
                    {
                        string name;
                        if (Database != null)
                        {
                            name = Database.Signals[(ushort)sig.Id].Name;
                        }
                        else
                        {
                            name = String.Format("Signal {0}", sig.Id);
                        }
                        intervalRow.SignalGroup.AddSignalRow(name, (ushort)sig.Id, sig.Type);
                    }
                }
            });
        }

        // Comes in from the networking thread - posts a message on the UI thread
        void HandleStateIntervalChanged(object sender, SimpleEventArgs<Interval> args)
        {
            UpdateTimeline(args.Payload as StateInterval);
        }

        void HandleUpdateRoot(object sender, SimpleEventArgs<LinkNetworkMessageBase> args)
        {
            UpdateTimeline(null);
        }

        // Comes in from the networking thread - posts a message on the UI thread
        void HandleStateDagChanged(object sender, RuntimeDatabase.StateDagEventArgs args)
        {
            this.BeginInvokeAction(() => {
                // Safely copy the latest time ranges. Keep in mind these are the _latest_ time intervals
                // not the intervals from when the message was sent (other msgs may have been processed in the meantime)
                _RuntimeDb.GetTimeSpansInSeconds(out _StartTime, out _LatestTime);

                Debug.Assert(args.ChildNode.Parents != null && args.ChildNode.Parents.Count == 1, "Only strict hierarchies are currently supported");
                StateTimelineRow parent = _TimelineRows.FindStateIntervalRow(args.ChildNode.Parents[0]);
                if (parent == null)
                {
                    var newRow = _TimelineRows.AddStateIntervalRow(RuntimeStateId.RootState, args.ChildNode.Parents[0], _AutoSort.IsChecked ?? false);
                    newRow.Transitional = _RuntimeDb.GetState(args.ChildNode.Parents[0]);
                }
                else
                {
                    var grandparent = parent.Parent as StateTimelineRow;
                    if (grandparent != null && (_AutoSort.IsChecked ?? false))
                    {
                        grandparent.BubbleUp(parent.Id);
                    }
                }

                if (args.ChildNode != null)
                {
                    StateTimelineRow child = _TimelineRows.FindStateIntervalRow(args.ChildNode.Id);
                    if (child == null)
                    {
                        var newRow = _TimelineRows.AddStateIntervalRow(args.ChildNode.Parents[0], args.ChildNode.Id, false);
                        newRow.Transitional = _RuntimeDb.GetState(args.ChildNode.Id);
                    }
                }

                InvalidateTimeline(null, null);
            });
        }

        #endregion

        #region Timeline Functions

        void UpdateTimeline(StateInterval mostRecentInterval)
        {
            this.BeginInvokeAction(() =>
            {
                // Safely copy the latest time ranges. Keep in mind these are the _latest_ time intervals
                // not the intervals from when the message was sent (other msgs may have been processed in the meantime)
                _RuntimeDb.GetTimeSpansInSeconds(out _StartTime, out _LatestTime);

                // If the view window is within .1% of the end of the view range:
                if ((_Timeline.PhaseMax - _Timeline.PhaseViewMax) < 0.001 * (_Timeline.PhaseMax - _Timeline.PhaseMin))
                {
                    // Track the end of the timeline
                    UpdatePhaseRange();
                    ClampAndSetPhaseView(_LatestTime - _Timeline.PhaseViewRange, _LatestTime);
                }
                else
                {
                    UpdatePhaseRange();
                    ClampAndSetPhaseView(_Timeline.PhaseViewMin, _Timeline.PhaseViewMax);
                }

                // See if we need to update the state order
                if ((_AutoSort.IsChecked ?? false) && mostRecentInterval != null && !mostRecentInterval.HasEnd)
                {
                    bool bubbled = false;
                    var row = _TimelineRows.FindStateIntervalRow(mostRecentInterval.StateId.State);
                    if (!row.HasChildren)
                    {
                        row = row.Parent as StateTimelineRow;
                    }
                    while (row.Parent != null)
                    {
                        var parent = row.Parent as StateTimelineRow;
                        if (parent != null)
                        {
                            _TimelineRows.BubbleUp(parent.Id, row.Id);
                            bubbled = true;
                        }
                        row = parent;
                    }
                    if (bubbled)
                    {
                        InvalidateTimeline(null, null);
                    }
                }

                // Redraw intervals no matter what, cause we might have closed a visible state or something
                UpdateIntervalsForNewPhaseView();
            });
        }

        void UpdatePhaseRange()
        {
            double startTime = _StartTime;
            double stopTime = _LatestTime;

            if (stopTime < startTime + 2.0)
            {
                stopTime = startTime + 2.0;
            }
            _Timeline.SetPhase(startTime, stopTime);
            _Timeline.SetDisplay(startTime, stopTime);
        }

        // Makes sure start and stop fit within the view range, adjusts them if they don't,
        // and sets the view.
        void ClampAndSetPhaseView(double start, double stop)
        {
            double diff = stop - start;
            if (start < _Timeline.PhaseMin)
            {
                start = _Timeline.PhaseMin;
                stop = start + diff;
            }

            if (stop > _Timeline.PhaseMax)
            {
                stop = _Timeline.PhaseMax;
                start = stop - diff;
            }

            start = Math.Max(_Timeline.PhaseMin, start);
            stop = Math.Min(_Timeline.PhaseMax, stop);

            _Timeline.SetPhaseView(start, stop);

            TimelineCanvas.Width = Math.Round(_Timeline.PhaseToAbsPixel(Math.Max(_LatestTime, 1.0)), 2, MidpointRounding.ToEven); 
            TimelineScroller.ScrollToHorizontalOffset(_Timeline.PhaseToAbsPixel(_Timeline.PhaseViewMin));
            _TimeStrip.InvalidateVisual();
        }


        #endregion

        #region Drawing Functions

        void RedrawTimeline(object sender, EventArgs e)
        {
            if (_IsTimelineDirty)
            {
                _IsTimelineDirty = false;

                TimelineCanvas.Children.Clear();
                _DisplayedIntervals.Clear();

                TimelineCanvas.Height = StateTree.ActualHeight; // Height will get set based on what's visible

                TimelineCanvas.Width = _Timeline.PixelMax - _Timeline.PixelMin;

                _TimeStrip.InvalidateVisual();

                MeasureTreeViewItems();

                // Get a list of all the visible transitions and states in our time window,
                // and draw them.
                Interval visibleInterval = new Interval(
                    TimeSpan.FromSeconds(_Timeline.PhaseViewMin),
                    TimeSpan.FromSeconds(_Timeline.PhaseViewMax)
                    );

                CreateFullTimespanControls(visibleInterval);

                CreateStatesAndTransitionsInInterval(visibleInterval, false);

                UpdateIntervalsForNewPhaseView();

                // Make sure we're looking at the right part of the window
                TimelineCanvas.Width = _Timeline.PhaseToAbsPixel(Math.Max(_LatestTime, 1.0));
                TimelineScroller.ScrollToHorizontalOffset(_Timeline.PhaseToAbsPixel(_Timeline.PhaseViewMin));

                CommandManager.InvalidateRequerySuggested();
            }
        }

        // PURPOSE: InvalidateTimeline triggers a FULL redraw. Use only when necessary
        void InvalidateTimeline(object sender, EventArgs e)
        {
            _IsTimelineDirty = true;
        }

        #endregion

        #region Tree View Functions

        void MeasureRecursive(ItemContainerGenerator gen, int count)
        {
            for(int i = 0; i < count; i++)
            {
                TreeViewItem item = (TreeViewItem)gen.ContainerFromIndex(i);
                if (item != null)
                {
                    object rowObj = gen.ItemFromContainer(item);
                    if (rowObj == DependencyProperty.UnsetValue)
                    {
                        throw new InvalidOperationException("Woah got an unset value... What do we do here, remeasure later?");
                    }
                    TimelineRow row = (TimelineRow)rowObj;
                    row.Visible = item.IsVisible;
                    if (row.Visible)
                    {
                        GeneralTransform xform = item.TransformToAncestor(StateTree);
                        double height = FindHeaderHeight(item);
                        Point topPoint = xform.Transform(new Point(0, 0));
                        Point bottomPoint = xform.Transform(new Point(0, height));
                        Point childBottomPoint = xform.Transform(new Point(0, item.ActualHeight));

                        row.Top = rage.math.Lerp(0.15, topPoint.Y, bottomPoint.Y);
                        row.Bottom = rage.math.Lerp(0.85, topPoint.Y, bottomPoint.Y);
                        row.ChildBottom = childBottomPoint.Y;

                    }

                    if (item.HasItems)
                    {
                        MeasureRecursive(item.ItemContainerGenerator, item.Items.Count);
                    }
                }
            }
        }

        void MeasureTreeViewItems()
        {
            // Clear out the old measurements
            foreach(var row in _TimelineRows.AllRows)
            {
                row.Top = row.Bottom = row.ChildBottom = 0.0;
                row.Visible = false;
            }

            // Iterate through the tree view items, setting new measurements.
            MeasureRecursive(StateTree.ItemContainerGenerator, StateTree.Items.Count);
            
        }

        double FindHeaderHeight(Control item)
        {
            var obj = item.Template.FindName("PART_Header", item);
            var elt = (FrameworkElement)obj;
            return (elt != null) ? elt.ActualHeight : 0.0;
        }

        #endregion

        #region State Functions

        bool StateIsVisible(RuntimeStateId id)
        {
            var row = _TimelineRows.FindStateIntervalRow(id);
            return row != null ? row.Visible : false;
        }

        Brush GetColorForState(RuntimeStateId id)
        {
            // See if we already know what brush to use
            Brush b = null;
            if (!_StateToColorMap.TryGetValue(id, out b))
            {
                // look at the timelinerow's siblings
                var row = _TimelineRows.FindStateIntervalRow(id);
                if (row != null && row.Parent != null && row.Parent.Children != null)
                {
                    foreach(var sib in row.Parent.Children.OfType<StateTimelineRow>())
                    {
                        if (_StateToColorMap.TryGetValue(sib.Id, out b))
                        {
                            _StateToColorMap[id] = b;
                            break;
                        }
                    }
                }

                if (b == null)
                {
                    // pick the next color in the array
                    b = _StateColorTable[_NextColor];
                    _StateToColorMap[id] = b;
                    _NextColor = (_NextColor + 1) % _StateColorTable.Length;
                }
            }

            return b;
        }

        #endregion

        #region Interval Control Functions

        FrameworkElement CreateStateInterval(StateTimelineRow row, StateInterval s, Brush fillColor)
        {
            double startTime = s.StartTime.TotalSeconds;
            double endTime = s.EndTime.TotalSeconds;

            double startPixel = _Timeline.PhaseToAbsPixel(startTime);
            double endPixel = _Timeline.PhaseToAbsPixel(endTime);

            double width = endPixel - startPixel;
            if (width < 0.5)
            {
                return null; // Don't draw states narrower than a half pixel
            }

            FrameworkElement newElt = null;
            if (width < 3)
            {
                // make a rectangle instead
                Rectangle rect = new Rectangle();
                rect.Stroke = Brushes.Black;
                rect.StrokeThickness = 0.5f;
                rect.Fill = fillColor;
                newElt = rect;
            }
            else
            {
                StateIntervalControl sic = new StateIntervalControl(s, _RuntimeDb);
                sic.Background = fillColor;
                sic.MouseEnter += new MouseEventHandler(AddBackgroundHighlight);
                sic.MouseLeave += new MouseEventHandler(RemoveBackgroundHighlight);
                sic.MouseDoubleClick += new MouseButtonEventHandler(ZoomToFill);
                newElt = sic;
            }

            newElt.Height = row.Bottom - row.Top;
            newElt.SetValue(Canvas.TopProperty, row.Top);
            newElt.SetValue(Canvas.ZIndexProperty, ZOrderStates + row.Depth); // children get higher z-order than their parents

            TimelineCanvas.Children.Add(newElt);
            return newElt;
        }

        FrameworkElement CreateTransitionInterval(StateTimelineRow fromRow, StateTimelineRow toRow, TransitionInterval t, Brush fillColor)
        {
            double left = _Timeline.PhaseToAbsPixel(t.StartTime.TotalSeconds);
            double right = _Timeline.PhaseToAbsPixel(t.EndTime.TotalSeconds);

            double width = right - left;
            if (width < 0.5)
            {
                return null; // don't draw transitions narrower than a half pixel
            }

            double top = Math.Max(fromRow.Top, toRow.Top);
            double bottom = Math.Min(fromRow.Bottom, toRow.Bottom);

            rage.math.OrderEm(ref top, ref bottom);

            FrameworkElement newElt = null;
            if (width < 3)
            {
                Rectangle rect = new Rectangle();
                rect.Stroke = null;
                rect.StrokeThickness = 0;
                rect.Fill = fillColor;
                newElt = rect;
            }
            else
            {
                TransitionIntervalControl tic = new TransitionIntervalControl(t, fromRow.Top > toRow.Top, _RuntimeDb);
                tic.Background = fillColor;
                newElt = tic;
            }

            newElt.Height = bottom - top;
            newElt.SetValue(Canvas.TopProperty, top);
            newElt.SetValue(Canvas.ZIndexProperty, ZOrderTransitions);

            TimelineCanvas.Children.Add(newElt);
            return newElt;
        }

        void CreateStatesAndTransitionsInInterval(Interval visibleInterval, bool checkForDupes)
        {
            if (_RuntimeDb == null)
            {
                return;
            }
            lock (_RuntimeDb.LockObject)
            {
                var transList = _RuntimeDb.TransitionSet.Where(t => t.StateFrom != null && t.Overlaps(visibleInterval) && StateIsVisible(t.StateFrom.StateId.State));
                foreach (TransitionInterval trans in transList)
                {
                    if (checkForDupes && _DisplayedIntervals.ContainsKey(trans))
                    {
                        continue;
                    }
                    RuntimeStateId fromId = trans.StateFrom.StateId.State;
                    RuntimeStateId toId = trans.StateTo.StateId.State;

                    StateTimelineRow fromRow = _TimelineRows.FindStateIntervalRow(fromId);
                    StateTimelineRow toRow = _TimelineRows.FindStateIntervalRow(toId);

                    if (fromRow != null && toRow != null)
                    {
                        var newElt = CreateTransitionInterval(fromRow, toRow, trans, GetColorForState(fromId));

                        if (newElt != null)
                        {
                            _DisplayedIntervals[trans] = new List<FrameworkElement>() {newElt};
                        }
                    }
                }

                var stateList = _RuntimeDb.StateSet.Where(s => s.Overlaps(visibleInterval) && StateIsVisible(s.StateId.State));
                foreach (StateInterval state in stateList)
                {
                    if (checkForDupes && _DisplayedIntervals.ContainsKey(state))
                    {
                        continue;
                    }

                    StateTimelineRow row = _TimelineRows.FindStateIntervalRow(state.StateId.State);

                    double spanMid = rage.math.Average(row.Top, row.Bottom);

                    var newElt = CreateStateInterval(row, state, GetColorForState(state.StateId.State));

                    if (newElt != null)
                    {
                        var elements = new List<FrameworkElement>() { newElt };
                        _DisplayedIntervals[state] = elements;
                        // see if we need to draw any signals too
                        if (row.HasSignalGroup)
                        {
                            foreach(var signalRow in row.SignalGroup.Children)
                            {
                                SignalTimelineRow realSigRow = signalRow as SignalTimelineRow;
                                if (realSigRow.Visible)
                                {
                                    var hist = state.SignalHistory.FindSignal(realSigRow.Id);
                                    if (hist == null)
                                    {
                                        continue;
                                    }
                                    FrameworkElement sigElt = null;
                                    switch(realSigRow.Type)
                                    {
                                        case Parameter.Boolean:
                                            sigElt = new BoolRowControl((int)hist.Id, state, _Timeline, _RuntimeDb);
                                            break;
                                        case Parameter.Real:
                                            sigElt = new FloatRowControl((int)hist.Id, state, _Timeline, _RuntimeDb);
                                            break;
                                        case Parameter.Animation:
                                        case Parameter.Clip:
                                        case Parameter.Expressions:
                                        case Parameter.Filter:
                                        case Parameter.Frame:
                                        case Parameter.ParameterizedMotion:
                                            sigElt = new StringRowControl((int)hist.Id, state, _Timeline, _RuntimeDb);
                                            break;
                                        default:
                                            // TODO: Add support for other types here
                                            break;
                                    }

                                    if (sigElt != null)
                                    {
                                        sigElt.Height = realSigRow.Height;
                                        sigElt.SetValue(Canvas.TopProperty, realSigRow.Top);
                                        sigElt.SetValue(Canvas.ZIndexProperty, ZOrderSignals);
                                        elements.Add(sigElt);
                                        TimelineCanvas.Children.Add(sigElt);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        void AddSingleRowFullTimespanControl(TimelineRow r, Control ctrl, int zorder)
        {
            ctrl.Height = r.Bottom - r.Top;
            ctrl.SetValue(Canvas.TopProperty, r.Top);
            ctrl.SetValue(Canvas.ZIndexProperty, zorder);
            TimelineCanvas.Children.Add(ctrl);
            _FullTimespanElements.Add(ctrl);
        }

        void CreateFullTimespanControls(Interval visibleInterval)
        {
            // Check to see if any visible row wants a background drawn
            foreach(var row in _TimelineRows.AllRows)
            {
                // Sort of a hack for now, just create background elements for StateTimelineRows
                if (row is StateTimelineRow)
                {
                    var ctrl = new StateIntervalBackgroundControl();
                    AddSingleRowFullTimespanControl(row, ctrl, ZOrderBackground);
                }
                
            }

            foreach(var row in _TimelineRows.SignalGroup.Children)
            {
                var sigRow = row as SignalTimelineRow;
                if (sigRow == null || !sigRow.Visible)
                {
                    continue;
                }

                switch(sigRow.Type)
                {
                    case Parameter.Boolean:
                        {
                            var ctrl = new BoolRowControl(sigRow.Id, null, _Timeline, _RuntimeDb);
                            AddSingleRowFullTimespanControl(sigRow, ctrl, ZOrderSignals);
                        }
                        break;
                    case Parameter.Real:
                        {
                            var ctrl = new FloatRowControl(sigRow.Id, null, _Timeline, _RuntimeDb);
                            AddSingleRowFullTimespanControl(sigRow, ctrl, ZOrderSignals);
                        }
                        break;
                    case Parameter.Animation:
                    case Parameter.Clip:
                    case Parameter.Expressions:
                    case Parameter.Filter:
                    case Parameter.Frame:
                    case Parameter.ParameterizedMotion:
                        {
                            var ctrl = new StringRowControl(sigRow.Id, null, _Timeline, _RuntimeDb);
                            AddSingleRowFullTimespanControl(sigRow, ctrl, ZOrderSignals);
                        }
                        break;
                    default:
                        // do nothing, for now...
                        break;
                }

            }

            // Check the flags and requests to see if they're visible
            foreach(var row in _TimelineRows.Flags.Children)
            {
                var boolRow = row as StatebitTimelineRow;
                if (boolRow != null && boolRow.Visible)
                {
                    var ctrl = new BoolRowControl(_RuntimeDb.GetStatebitIndexForFlag(boolRow.BitIndex), _Timeline, _RuntimeDb);

                    AddSingleRowFullTimespanControl(boolRow, ctrl, ZOrderStates);
                }
            }

            foreach (var row in _TimelineRows.Requests.Children)
            {
                var boolRow = row as StatebitTimelineRow;
                if (boolRow != null && boolRow.Visible)
                {
                    var ctrl = new BoolRowControl(_RuntimeDb.GetStatebitIndexForRequest(boolRow.BitIndex), _Timeline, _RuntimeDb);

                    AddSingleRowFullTimespanControl(boolRow, ctrl, ZOrderStates);
                }
            }
        }

        void DeleteStatesAndTransitionsInInterval(Interval invisibleInterval)
        {
            List<Interval> toRemove = new List<Interval>();
            toRemove.Capacity = _DisplayedIntervals.Count;
            foreach (var kvp in _DisplayedIntervals)
            {
                if (invisibleInterval.Contains(kvp.Key))
                {
                    foreach (var elt in kvp.Value)
                    {
                        TimelineCanvas.Children.Remove(elt);
                    }
                    toRemove.Add(kvp.Key);
                }
            }

            foreach (var inter in toRemove)
            {
                _DisplayedIntervals.Remove(inter);
            }
        }

        private void RepositionOneInterval(Interval inter, FrameworkElement elt)
        {
            double startPixel = _Timeline.PhaseToAbsPixel(inter.StartTime.TotalSeconds);
            double endPixel = startPixel;
            if (inter.Status == Interval.StatusType.CLOSED)
            {
                endPixel = _Timeline.PhaseToAbsPixel(inter.EndTime.TotalSeconds);
            }
            else
            {
                endPixel = _Timeline.PhaseToAbsPixel(_LatestTime);
            }

            elt.Width = Math.Round(endPixel - startPixel, 2, MidpointRounding.ToEven);
            elt.SetValue(Canvas.LeftProperty, Math.Round(startPixel, 2, MidpointRounding.ToEven));
        }

        private void RepositionIntervals()
        {
            // For every interval in the _DisplayedIntervals list, make sure it has the
            // right start and end position on the timeline
            foreach (var kvp in _DisplayedIntervals)
            {
                foreach(var elt in kvp.Value)
                {
                    RepositionOneInterval(kvp.Key, elt);
                }
            }

            // For every interval in the _FullTimespanElements list make sure it spans the full phase range
            Interval fullInter = new Interval(TimeSpan.FromSeconds(_Timeline.PhaseMin), TimeSpan.FromSeconds(_Timeline.PhaseMax));
            foreach (var elt in _FullTimespanElements)
            {
                RepositionOneInterval(fullInter, elt);
            }
        }

        private void UpdateIntervalsForNewPhaseView()
        {
            // Find the new interval we've uncovered, search there for controls
            Interval visibleInterval = new Interval(
                TimeSpan.FromSeconds(_Timeline.PhaseViewMin),
                TimeSpan.FromSeconds(_Timeline.PhaseViewMax)
                );

            // Remove anything no longer in view
            Interval offTheLowEnd = new Interval();
            offTheLowEnd.EndTime = TimeSpan.FromSeconds(_Timeline.PhaseViewMin);
            DeleteStatesAndTransitionsInInterval(offTheLowEnd);

            Interval offTheHighEnd = new Interval();
            offTheHighEnd.StartTime = TimeSpan.FromSeconds(_Timeline.PhaseViewMax);
            DeleteStatesAndTransitionsInInterval(offTheHighEnd);

            CreateStatesAndTransitionsInInterval(visibleInterval, true);

            RepositionIntervals();
        }

        void ZoomToFill(object sender, MouseButtonEventArgs args)
        {
            StateIntervalControl sic = (StateIntervalControl)sender;

            double phaseStartSec = sic.Interval.HasStart ? sic.Interval.StartTime.TotalSeconds : _StartTime;
            double phaseEndSec = sic.Interval.HasEnd ? sic.Interval.EndTime.TotalSeconds : _LatestTime;

            ClampAndSetPhaseView(rage.math.Lerp(-0.1, phaseStartSec, phaseEndSec), rage.math.Lerp(1.1, phaseStartSec, phaseEndSec));
            UpdateIntervalsForNewPhaseView();
        }

        #endregion

        #region Constants

        public const Int32 ZOrderBackground = 0;
        public const Int32 ZOrderBackgroundHighlightRect = 50;
        public const Int32 ZOrderTransitions = 1000000;
        public const Int32 ZOrderStates = 2000000;
        public const Int32 ZOrderSignals = 3000000;
        public const Int32 ZOrderOverlay = 4000000;

        #endregion

        #region Member Variables

        // These two go together. The first list, _DisplayedIntervals, is a set of framework element objects along with
        // an associated Interval objects. We'll automatically reposition the FrameworkElements when the view changes based
        // on the associated interval.
        // The second list, _FullTimespanElements is a set of framework elements that should span the full timeline. We'll automatically
        // update their size and make sure they re-render as the view changes.
        private Dictionary<Interval, List<FrameworkElement>> _DisplayedIntervals = new Dictionary<Interval, List<FrameworkElement>>();
        private List<FrameworkElement> _FullTimespanElements = new List<FrameworkElement>();

        private WindowedTimeLine _Timeline = new WindowedTimeLine();

        public Database Database { get; protected set; }
        private Link _Link = null;
        private RuntimeDatabase _RuntimeDb = null;

        private Dictionary<RuntimeStateId, Brush> _StateToColorMap = new Dictionary<RuntimeStateId,Brush>();

        public bool _IsTimelineDirty { get; set; }

        private Rectangle _BackgroundHighlightRect;
        private Interval _BackgroundHighlightInterval = new Interval();

        private TimelineRowSet _TimelineRows = null;

        // We copy these from the runttime DB so we can avoid a lot of locking
        private double _StartTime = 0.0;
        private double _LatestTime = 1.0;

        private static Brush[] _StateColorTable = new Brush[]{
            Brushes.BurlyWood,
            Brushes.DeepSkyBlue,
            Brushes.MediumSpringGreen,
            Brushes.Gold,
            Brushes.PaleGoldenrod,
            Brushes.MediumAquamarine,
            Brushes.DarkBlue,
            Brushes.Firebrick,
            Brushes.Orange,
            Brushes.DarkMagenta,
            Brushes.LavenderBlush,
            Brushes.LightSkyBlue,
            Brushes.LightGoldenrodYellow,
            Brushes.Chocolate,
            Brushes.MidnightBlue,
            Brushes.DarkGray,
            Brushes.Green,
            Brushes.HotPink,
            Brushes.Aquamarine,
            Brushes.LightPink,
            Brushes.Bisque,
            Brushes.LightYellow,
            Brushes.MediumPurple,
            Brushes.OldLace,
            Brushes.Blue,
            Brushes.AliceBlue,
            Brushes.DarkSeaGreen,
            Brushes.DarkTurquoise,
            Brushes.Moccasin,
            Brushes.Linen,
            Brushes.Fuchsia,
            Brushes.LightBlue,
        };
        private int _NextColor = 0;

        #endregion

        #region Command Handlers

        private void navigationCommandZoom_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (e.Parameter == null)
            {
                e.CanExecute = false;
                return;
            }

            string scaleString = (string)e.Parameter;
            if (scaleString == "Fit")
            {
                e.CanExecute = _Timeline.PhaseViewMin != _Timeline.PhaseMin || _Timeline.PhaseViewMax != _Timeline.PhaseMax;
                return;
            }

            double scale = Double.Parse(scaleString);

            double newRange = _Timeline.PhaseViewRange * scale;

            if ((scale < 1 && newRange < 0.001) || 
                (scale > 1 && _Timeline.PhaseViewRange >= _Timeline.PhaseMax - _Timeline.PhaseMin))
            {
                e.CanExecute = false;
                return;
            }

            // Can't execute when there's no data to zoom
            if (!StateTree.HasItems)
            {
                e.CanExecute = false;
                return;
            }

            e.CanExecute = true;
        }

        private void navigationCommandZoom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            double newMin = _Timeline.PhaseViewMin;
            double newMax = _Timeline.PhaseViewMax;

            string scaleString = (string)e.Parameter;
            if (scaleString == "Fit")
            {
                newMin = _Timeline.PhaseMin;
                newMax = _Timeline.PhaseMax;
            }
            else
            {
                double scale = Double.Parse(scaleString);

                double oldHalfRange = _Timeline.PhaseViewRange * 0.5;
                double newHalfRange = oldHalfRange * scale;

                double phaseViewMid = _Timeline.PhaseViewMin + oldHalfRange;

                newMin = phaseViewMid - newHalfRange;
                newMax = phaseViewMid + newHalfRange;

                if (newHalfRange > (_Timeline.PhaseMax - _Timeline.PhaseMin) * 0.45)
                { // if we're within 90% of all the way zoomed out, zoom all the way out
                    newMin = _Timeline.PhaseMin;
                    newMax = _Timeline.PhaseMax;
                }
            }

            ClampAndSetPhaseView(newMin, newMax);
            UpdateIntervalsForNewPhaseView();
        }

        private void navigationCommandRefresh_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void navigationCommandRefresh_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            _IsTimelineDirty = true;
            RedrawTimeline(null,null);
        }

        private void navigationCommandFirstPage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (_Timeline.PhaseViewMin != _Timeline.PhaseMin);
        }

        private void navigationCommandFirstPage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClampAndSetPhaseView(_Timeline.PhaseMin, _Timeline.PhaseMin + _Timeline.PhaseViewRange);
            UpdateIntervalsForNewPhaseView();
        }

        private void navigationCommandLastPage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (_Timeline.PhaseViewMax != _Timeline.PhaseMax);
        }

        private void navigationCommandLastPage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClampAndSetPhaseView(_Timeline.PhaseMax - _Timeline.PhaseViewRange, _Timeline.PhaseMax);
            UpdateIntervalsForNewPhaseView();
        }

        #endregion


    }

}
