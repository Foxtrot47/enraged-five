﻿using System;
using System.ComponentModel.Composition;
using Rage.Move;
using Move;

using AvalonDock;

namespace Move.UI
{
    public class Attributes : IPlugin
    {
        static class Const
        {
            public static string Name = "attributes";
            public static string Description = "provides attribute control";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
        }

        public void Dispose()
        {
        }
    }

    [Export(typeof(IWindow))]
    public class AttributesWindow : IWindow
    {
        public AttributesWindow()
        {
            Content = new DockableContent();
            Content.Name = Name;
            Content.Title = Name;
        }

        public DockableContent Content { get; private set; }

        public string Name { get { return "Attributes"; } }

        public void Create(Rage.Move.MainWindow window)
        {
            Content.Content = new AttributesControl(window);
            window.RightPane.Items.Add(Content);
        }
    }
}
