﻿using System;
using System.Runtime.Serialization;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Conditions
{
    [Serializable]
    public class ConditionLessThanEqual : ConditionBase, ISerializable
    {
        public SignalConditionProperty Signal { get; private set; }
        public RealConditionProperty TriggerValue { get; private set; }

        public ConditionLessThanEqual(string name)
            : base(name)
        {
            Signal = new SignalConditionProperty();
            TriggerValue = 0.5;
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredConditionLessThanEqual);
            }
        }

        static class SerializationTag
        {
            public static string Signal = "Signal";
            public static string TriggerValue = "TriggerValue";
        }

        public ConditionLessThanEqual(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Signal = (SignalConditionProperty)info.GetValue(SerializationTag.Signal, typeof(SignalConditionProperty));
            TriggerValue = (RealConditionProperty)info.GetValue(SerializationTag.TriggerValue, typeof(RealConditionProperty));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetSerializationData(info, context);

            info.AddValue(SerializationTag.Signal, Signal);
            info.AddValue(SerializationTag.TriggerValue, TriggerValue);
        }

        public override void GetExportData(ChunkObject info, ChunkContext context)
        {
            uint crc = Crc32.Generate(0, Signal.Value.Name);
            info.AppendUInt32(crc);
            info.AppendFloat((float)TriggerValue.Value);
        }

        public override uint GetExportSize()
        {
            return
                sizeof(uint) +  // signal
                sizeof(float);   // value
        }

        public override ushort GetExportId()
        {
            return (ushort)RegisteredConditionLessThanEqual.Const.Id;
        }
    }

    public class RegisteredConditionLessThanEqual : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "Less Than Equal";
            public static int Id = 8;
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }

        public ICondition Create()
        {
            return new ConditionLessThanEqual(Const.Name);
        }
    }
}