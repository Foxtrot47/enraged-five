﻿using System;
using System.Runtime.Serialization;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Conditions
{
    [Serializable]
    public class ConditionGreaterThan : ConditionBase, ISerializable
    {
        public SignalConditionProperty Signal { get; private set; }
        public RealConditionProperty TriggerValue { get; private set; }

        public ConditionGreaterThan(string name)
            : base(name)
        {
            Signal = new SignalConditionProperty();
            TriggerValue = 0.5;
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredConditionGreaterThan);
            }
        }

        static class SerializationTag
        {
            public static string Signal = "Signal";
            public static string TriggerValue = "TriggerValue";
        }

        public ConditionGreaterThan(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Signal = (SignalConditionProperty)info.GetValue(SerializationTag.Signal, typeof(SignalConditionProperty));
            TriggerValue = (RealConditionProperty)info.GetValue(SerializationTag.TriggerValue, typeof(RealConditionProperty));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetSerializationData(info, context);

            info.AddValue(SerializationTag.Signal, Signal);
            info.AddValue(SerializationTag.TriggerValue, TriggerValue);
        }

        public override void GetExportData(ChunkObject info, ChunkContext context)
        {
            uint crc = Crc32.Generate(0, Signal.Value.Name);
            info.AppendUInt32(crc);
            info.AppendFloat((float)TriggerValue.Value);
        }

        public override uint GetExportSize()
        {
            return
                sizeof(uint) +  // signal
                sizeof(float);  // value
        }

        public override ushort GetExportId()
        {
            return (ushort)RegisteredConditionGreaterThan.Const.Id;
        }
    }

    public class RegisteredConditionGreaterThan : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "Greater Than";
            public static int Id = 5;
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }

        public ICondition Create()
        {
            return new ConditionGreaterThan(Const.Name);
        }
    }
}