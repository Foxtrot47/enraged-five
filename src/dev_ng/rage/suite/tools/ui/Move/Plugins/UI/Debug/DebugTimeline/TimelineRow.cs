﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Move.Utils;
using New.Move.Core;
using Rage.Move.Core;

namespace Move.UI
{
    /// <summary>
    /// TimelineRows are really just entries in the tree view that describe what should be shown in the timeline,
    /// but don't actually contain references to the timeline intervals or associated data.
    /// 
    /// They exist so we have an easy way to build and organize a tree without messing with the TreeView internally.
    /// I.e. they are the ViewModel for the tree
    /// </summary>
    public abstract class TimelineRow : INotifyPropertyChanged
    {
        internal TimelineRow(string name)
        {
            _Name = name;
        }

        public TimelineRow Parent;
        public virtual bool IsExpanded
        {
            get {return _IsExpanded;}
            set {
                if (value != _IsExpanded)
                {
                    _IsExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }

                if (_IsExpanded && Parent != null)
                {
                    Parent.IsExpanded = true; // expand all parents
                }
            }
        }

        public bool IsSelected
        {
            get {return _IsSelected;}
            set {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        public double Height
        {
            get { return _Height; }
            protected set 
            {
                if (value != _Height)
                {
                    _Height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        public bool HasChildren 
        {
            get { return Children.Count > 0; }
        }

        public string ShortName
        {
            get {
                return ToString();
            }
        }

        // These are computed once the TreeView is laid out
        public double Top = 0;
        public double Bottom = 0;
        public double ChildBottom = 0;
        public bool Visible = false;

        public int Depth = 0;

        public StateTimelineRow FindStateIntervalRowRecursive(RuntimeStateId id)
        {
            // check the children
            foreach(var node in Children)
            {
                var s = node as StateTimelineRow;
                if (s != null && s.Id == id)
                {
                    return s;
                }
            }

            // not in any of the children, so recurse.
            foreach (var node in Children)
            {
                var s = node.FindStateIntervalRowRecursive(id);
                if (s != null)
                {
                    return s;
                }
            }
            return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<TimelineRow> Children 
        {
            get { return _Children; }
        }

        public ObservableCollection<TimelineRow> _Children = new ObservableCollection<TimelineRow>();

        protected void OnPropertyChanged(string prop)
        {
            if (PropertyChanged != null) 
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        internal virtual void Disconnect()
        {
            _Children.Clear();
            Parent = null;
            PropertyChanged = null;
        }

        public override string ToString()
        {
            if (_Name != String.Empty)
            {
                return _Name;
            }
            return base.ToString();
        }

        internal TimelineRowSet FindRoot()
        {
            var r = this;
            while(r.Parent != null) 
            {
                r = r.Parent;
            }
            return r as TimelineRowSet;
        }

        protected bool _IsExpanded;
        protected bool _IsSelected;
        protected double _Height = Double.NaN;

        protected string _Name = String.Empty;

    }

    public class StateTimelineRow : TimelineRow
    {
        protected StateTimelineRow(RuntimeStateId id, StateTimelineRow parent) : base(String.Empty)
        {
            Id = id;
            Parent = parent;
            Depth = parent == null ? 0 : parent.Depth + 1;
            _IsExpanded = true;
        }

        public RuntimeStateId Id;
        public ITransitional Transitional = null;

        public bool HasSignalGroup
        {
            get { return HasChildren && Children[0] is SignalGroupTimelineRow; }
        }

        public SignalGroupTimelineRow SignalGroup
        {
            get { return HasSignalGroup ? (SignalGroupTimelineRow)Children[0] : null; }
        }

        public void BubbleUp(RuntimeStateId child)
        {
            // find the child with matching id, move it to the front of the list
            int foundIndex = -1;
            for(int i = 0; i < Children.Count; i++)
            {
                var siRow = Children[i] as StateTimelineRow;
                if (siRow != null && siRow.Id == child)
                {
                    foundIndex = i;
                    break;
                }
            }

            if (foundIndex == -1)
            {
                throw new InvalidOperationException(String.Format("Can't bubble up node {0} because its not a child of {1}", child, Id));
            }

            int insertionPoint = 0;
            for (int i = 0; i < Children.Count; i++)
            {
                if (Children[i] is StateTimelineRow)
                {
                    insertionPoint = i;
                    break;
                }
            }

            Children.Move(foundIndex, insertionPoint);
        }

        /// <summary>
        /// This should only be called from one of the TimelineRows functions. Hands off!
        /// </summary>
        /// <param name="child"></param>
        /// <param name="insertAtBeginning"></param>
        /// <returns></returns>
        public StateTimelineRow AddChild(RuntimeStateId child, bool insertAtBeginning)
        {
            // Make sure its not already there
            foreach(var row in Children)
            {
                var sirow = row as StateTimelineRow;
                if (sirow != null && sirow.Id == child)
                {
                    throw new InvalidOperationException(String.Format("Can't re-add node {0} to parent {1}", child, Id));
                }
            }

            StateTimelineRow childRow = new StateTimelineRow(child, this);

            // Are we auto sorting? If so add it to the beginning of the list. Else add it in node order
            int insertionPoint = Children.Count;
            if (insertAtBeginning)
            {
                for(int i = 0; i < Children.Count; i++)
                {
                    if (Children[i] is StateTimelineRow)
                    {
                        insertionPoint = i;
                        break;
                    }
                }
            }
            else
            {
                for(int i = 0; i < Children.Count; i++)
                {
                    var sirow = Children[i] as StateTimelineRow;
                    if (sirow != null && sirow.Id.RawId > child.RawId)
                    {
                        insertionPoint = i;
                        break;
                    }
                }
            }
            Children.Insert(insertionPoint, childRow);

            return childRow;
        }

        public override string ToString()
        {
            return Transitional != null ? Transitional.ToString() : Id.ToString();
        }

        internal override void Disconnect()
        {
            base.Disconnect();
            Transitional = null;
        }

    }

    public class SignalGroupTimelineRow : TimelineRow
    {
        /// <summary>
        /// Don't call this. Call TimelineRowSet.AddSignalGroupRow instead.
        /// </summary>
        /// <param name="parent"></param>
        internal SignalGroupTimelineRow(StateTimelineRow parent) : base(String.Empty)
        {
            _IsExpanded = false;
            Parent = parent;
        }

        public override string ToString()
        {
            return "Signals";
        }

        public TimelineRow FindSignal(ushort sigId)
        {
            foreach(var kid in Children)
            {
                var sigKid = kid as SignalTimelineRow;
                if (sigKid != null && sigKid.Id == sigId)
                {
                    return sigKid;
                }
            }
            return null;
        }

        public TimelineRow AddSignalRow(string name, ushort id, New.Move.Core.Parameter type)
        {
            var newSig = new SignalTimelineRow(name, id, type);
            var rowSet = FindRoot();
            rowSet.AddNewRowToParent(newSig, this);
            return newSig;
        }
    }


    public class StatebitTimelineRow : TimelineRow
    {
        internal StatebitTimelineRow(string name, int bitIndex) : base(name)
        {
            _IsExpanded = false;
            BitIndex = bitIndex;
        }

        public int BitIndex;
    }

    public class BooleanGroupTimelineRow : TimelineRow
    {
        internal BooleanGroupTimelineRow(string name) : base(name)
        {
            _IsExpanded = false;
        }
    }

    public class SignalTimelineRow : TimelineRow
    {
        internal SignalTimelineRow(string name, ushort id, New.Move.Core.Parameter type)
            : base(name)
        {
            Id = id;
            Type = type;

            // TODO: Height for signals should probably be driven by what the rows in the timeline itself need
            switch(Type)
            {
                case Parameter.Real:
                    Height = 40.0; // more headroom for float signals
                    break;
                case Parameter.Animation:
                case Parameter.Clip:
                case Parameter.Expressions:
                case Parameter.Filter:
                case Parameter.Frame:
                case Parameter.ParameterizedMotion:
                    Height = 24;
                    break;
            }
        }

        public ushort Id { get; protected set; }
        public New.Move.Core.Parameter Type {get; protected set;}
    }


    /// <summary>
    /// A collection of timeline rows - encapsulates some of the functions we need to query and manipulate the rows
    /// 
    /// Its a StateIntervalTimelineRow for convenience
    /// </summary>
    public class TimelineRowSet : StateTimelineRow
    {
        public TimelineRowSet() : base(RuntimeStateId.RootState, null)
        {
            _IsExpanded = true;
            AddDefaultRows();
        }

        public override bool IsExpanded
        {
            get { return true; }
            set { if (value == false) { throw new ArgumentException("TimelineRowSet can't be collapsed", "IsExpanded"); } }
        }

        public void BubbleUp(RuntimeStateId parent, RuntimeStateId child)
        {
            var siRow = FindStateIntervalRow(parent);
            siRow.BubbleUp(child);
        }

        public StateTimelineRow AddStateIntervalRow(RuntimeStateId parent, RuntimeStateId child, bool insertAtBeginning)
        {
            var siRow = FindStateIntervalRow(parent);
            if (siRow == null)
            {
                throw new InvalidOperationException(String.Format("Couldn't find parent with id {0}", parent));
            }
            var childRow = siRow.AddChild(child, insertAtBeginning);
            childRow.Parent = siRow;
            _MasterStateToRowMap[child] = childRow;
            _MasterRowList.Add(childRow);
            return childRow;
        }

        public StateTimelineRow FindStateIntervalRow(RuntimeStateId id)
        {
            StateTimelineRow siRow = null;
            return _MasterStateToRowMap.TryGetValue(id, out siRow) ? siRow : null;
        }

        public SignalGroupTimelineRow AddSignalGroupRow(RuntimeStateId id)
        {
            StateTimelineRow stateRow = FindStateIntervalRow(id);
            if (stateRow == null)
            {
                throw new InvalidOperationException(String.Format("Couldn't find a state with id {0}"));
            }
            SignalGroupTimelineRow sgRow = new SignalGroupTimelineRow(stateRow);
            stateRow.Children.Insert(0, sgRow);
            _MasterRowList.Add(sgRow);

            return sgRow;
        }

        public void AddNewRowToParent(TimelineRow row, TimelineRow parent)
        {
            Debug.Assert(!(row is SignalGroupTimelineRow), "use AddSignalGroupRow instead");
            Debug.Assert(!(row is StateTimelineRow), "use AddStateIntervalRow instead");
            row.Parent = parent;
            parent.Children.Add(row);
            _MasterRowList.Add(row);
        }

        public IEnumerable<TimelineRow> AllRows
        {
            get { return _MasterRowList; }
        }

        private void Clear()
        {
            foreach(var row in _MasterRowList)
            {
                row.Disconnect();
            }
            _MasterRowList.Clear();
            _MasterStateToRowMap.Clear();
            Flags = null;
            Requests = null;
        }

        public void Reset()
        {
            Clear();
            AddDefaultRows();
        }

        private void AddDefaultRows()
        {
            _MasterStateToRowMap[Id] = this;
            _MasterRowList.Add(this);
            AddSignalGroupRow(Id);

            Flags = new BooleanGroupTimelineRow("Flags");
            AddNewRowToParent(Flags, this);

            Requests = new BooleanGroupTimelineRow("Requests");
            AddNewRowToParent(Requests, this);
        }

        public BooleanGroupTimelineRow Flags;
        public BooleanGroupTimelineRow Requests;

        protected Dictionary<RuntimeStateId, StateTimelineRow> _MasterStateToRowMap = new Dictionary<RuntimeStateId,StateTimelineRow>();
        protected List<TimelineRow> _MasterRowList = new List<TimelineRow>();
    }
}
