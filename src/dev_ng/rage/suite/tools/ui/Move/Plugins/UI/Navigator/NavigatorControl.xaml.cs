﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AvalonDock;

using Rage.Move.Core;

using New.Move.Core;

namespace Move.UI
{
    public partial class NavigatorControl : UserControl
    {
        public NavigatorControl(Rage.Move.MainWindow wnd)
        {
            InitializeComponent();
            //Items = new ObservableCollection<INode>();

            wnd.ActiveDocumentChanged += new EventHandler(Window_ActiveDocumentChanged);
        }

        //public ObservableCollection<INode> Items { get; private set; }

        void Window_ActiveDocumentChanged(object sender, EventArgs e)
        {
            if (sender is DocumentContent)
            {
                DocumentContent doc = sender as DocumentContent;
                Database database = doc.Tag as Database;

                RootTreeViewItem.ItemsSource = database.Root.Children;
            }
        }
#if THIS_IS_FUCKED
        Dictionary<Guid, NavigatorTreeViewItem> m_itemDictionary;
        ContextMenu m_itemContextMenu;

        //NOTE : Uncomment for drag and drop
        //NavigatorDragDropMgr<TransitionTreeViewItem> m_transitionDragDropMgr;

        Database Database { get; set; }

        public NavigatorControl(Rage.Move.MainWindow wnd)
        {
            InitializeComponent();

            //NOTE : Uncomment for drag and drop
            //m_transitionDragDropMgr = new NavigatorDragDropMgr<TransitionTreeViewItem>(m_navigatorTree);

            m_itemDictionary = new Dictionary<Guid, NavigatorTreeViewItem>();
            
            m_itemContextMenu = new ContextMenu();
            MenuItem miMakeActive = new MenuItem();
            miMakeActive.Header = "Make Active";
            miMakeActive.Click +=
                new RoutedEventHandler(TreeViewItemContextMenu_MakeActive_OnClick);

            m_itemContextMenu.Items.Add(miMakeActive);
            
            wnd.DocumentHost.SelectionChanged +=
                new SelectionChangedEventHandler(DocumentHost_SelectionChanged);

            m_navigatorTree.MouseRightButtonUp +=
                new MouseButtonEventHandler(TreeView_OnMouseRightButtonUp);

            m_navigatorTree.SelectedItemChanged +=
                new RoutedPropertyChangedEventHandler<object>(TreeView_SelectedItemChanged);
        }

        void ExpandToItem(NavigatorTreeViewItem tvi, bool bSelect)
        {
            NavigatorTreeViewItem tviCurrent = tvi;

            if(bSelect && !tvi.IsSelected)
                tvi.IsSelected = true;
       
            while (tviCurrent != null)
            {
                tviCurrent.IsExpanded = true;
                if(tviCurrent.Parent != null && tviCurrent.Parent is NavigatorTreeViewItem)
                {
                    tviCurrent = (NavigatorTreeViewItem)tviCurrent.Parent;
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Expands the navigator tree up to the specified node
        /// </summary>
        /// <param name="id">GUID of the node to expand to</param>
        /// <param name="bSelect">Flag to indicate of the node should also be marked as selected</param>
        void ExpandToItem(Guid id, bool bSelect)
        {
           if(m_itemDictionary.ContainsKey(id))
           {
               NavigatorTreeViewItem tvi = m_itemDictionary[id];
               ExpandToItem(tvi, bSelect);
           }
        }

        /// <summary>
        /// Removes the specified item from the navigator tree
        /// </summary>
        /// <param name="tvi">The tree item to remove</param>
        /// <returns>True if the item was removed</returns>
        bool RemoveItem(NavigatorTreeViewItem tvi)
        {
            bool bRemoved = false;
            
            //If the node being removed is currently selected then
            //clear the selection on the tree
            if (tvi.IsSelected)
            {
                tvi.IsSelected = false;
            }

            //Remove all the descendant guids of this item from the item dictionary
            List<Guid> descendantsToRemove = new List<Guid>();
            Queue<NavigatorTreeViewItem> tviQueue = new Queue<NavigatorTreeViewItem>();
            tviQueue.Enqueue(tvi);
            while(tviQueue.Count > 0)
            {
                NavigatorTreeViewItem curTvi = tviQueue.Dequeue();
                descendantsToRemove.Add(curTvi.Guid);

                foreach (NavigatorTreeViewItem childTvi in curTvi.Items)
                    tviQueue.Enqueue(childTvi);
            }

            foreach (Guid g in descendantsToRemove)
                m_itemDictionary.Remove(g);

            //Remove the item itself
            if(tvi.Parent is NavigatorTreeViewItem)
            {
                NavigatorTreeViewItem tviParent = (NavigatorTreeViewItem)tvi.Parent;
                tviParent.Items.Remove(tvi);
                bRemoved = true;
            }
            else
            {
                m_navigatorTree.Items.Remove(tvi);
                bRemoved = true;
            }
            return bRemoved;
        }

        /// <summary>
        /// Removes the specified item from the navigator tree
        /// </summary>
        /// <param name="id">GUID of the node to remove</param>
        /// <returns>True if the item was removed</returns>
        bool RemoveItem(Guid id)
        {
            //Find the item representing the node
            if (!m_itemDictionary.ContainsKey(id))
                return false;

            NavigatorTreeViewItem tvi = m_itemDictionary[id];
            return RemoveItem(tvi);
        }

        TreeViewItem CreateTreeItem(Transition transition, int index)
        {
            TransitionTreeViewItem tvi = new TransitionTreeViewItem();
           
            tvi.Guid = transition.Id;
            tvi.ParentGuid = transition.Parent.Id;
            tvi.Index = index;

            //Setup the binding between the name property and the header text
            Binding headerTextBinding = new Binding("Name");
            headerTextBinding.Source = transition;
            
            tvi.SetBinding(TreeViewItem.HeaderProperty, headerTextBinding);

            transition.IsSelectedChanged +=
                new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);

            m_itemDictionary[tvi.Guid] = tvi;

            return tvi;
        }

        /// <summary>
        /// Utility method to generate a tree item from the supplied INode
        /// </summary>
        /// <param name="node">Node to create a tree item for</param>
        /// <returns>Newly created tree item</returns>
        TreeViewItem CreateTreeItem(INode node)
        {
            NavigatorTreeViewItem tvi = new NavigatorTreeViewItem();

            if (node is ITransitional)
            {
                ITransitional transNode = (ITransitional)node;

                if (transNode.IsAutomaton)
                {
                    IAutomaton automaton = transNode as IAutomaton;
                  
                    int transIdx = 0;
                    foreach (Transition transition in automaton.Transitions)
                    {
                        TransitionTreeViewItem transitionItem = (TransitionTreeViewItem)CreateTreeItem(transition, transIdx++);
                        tvi.Items.Add(transitionItem);
                    }

                    //Add the transition changed callback
                    automaton.Transitions.CollectionChanged +=
                        new NotifyCollectionChangedEventHandler(this.TransitionCollectionChangedHandler);
                    
                }
              
                //Add the child node collection changed callback
                transNode.AddHandler(new NotifyCollectionChangedEventHandler(this.ChildCollectionChangedHandler));  
            }
           
            tvi.Guid = node.Id;

            //Setup the binding between the name property and the header text
            Binding headerTextBinding = new Binding("Name");
            headerTextBinding.Source = node;
            tvi.SetBinding(TreeViewItem.HeaderProperty, headerTextBinding);

            //Add the selection callback 
            node.IsSelectedChanged +=
                   new EventHandler<IsSelectedChangedEventArgs>(this.Node_IsSelectedChanged);

            m_itemDictionary[tvi.Guid] = tvi;

            return tvi;
        }

        /// <summary>
        /// Utility method that disconnects all the collection changed handlers from the 
        /// items in the tree.
        /// </summary>
        void DetachNodeEventHandlers()
        {
            //Detach the observable collection handlers in our tree view items
            Queue<NavigatorTreeViewItem> treeItemQueue = new Queue<NavigatorTreeViewItem>();

            foreach (NavigatorTreeViewItem tvi in m_navigatorTree.Items)
                treeItemQueue.Enqueue(tvi);

            while (treeItemQueue.Count > 0)
            {
                NavigatorTreeViewItem tvi = treeItemQueue.Dequeue();

                INode node = null;
                try
                {
                    node = Database.Find(tvi.Guid);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(String.Format("NavigatorControl.DetachNodeEventHandlers, {0}",ex.Message));
                }

                if (node != null )
                {
                    //Remove the selection changed handler
                    node.IsSelectedChanged -=
                        new EventHandler<IsSelectedChangedEventArgs>(this.Node_IsSelectedChanged);

                    //Remove the collection changed handler
                    if( node is ITransitional )
                    {
                        ITransitional transNode = (ITransitional)node;
                        transNode.RemoveHandler(new NotifyCollectionChangedEventHandler(this.ChildCollectionChangedHandler));

                        if( transNode.IsAutomaton )
                        {
                            IAutomaton automaton = (IAutomaton)transNode;
                            automaton.Transitions.CollectionChanged -=
                                new NotifyCollectionChangedEventHandler(this.TransitionCollectionChangedHandler);
                        }
                    }
                }

                foreach (NavigatorTreeViewItem tviChild in tvi.Items)
                    treeItemQueue.Enqueue(tviChild);
            }
        }

        /// <summary>
        /// Utility method to clear the navigator tree.
        /// </summary>
        void Clear()
        {
            DetachNodeEventHandlers();

            //Clear out our guid->item lookup table
            m_itemDictionary.Clear();

            //Clear out the items
            m_navigatorTree.Items.Clear();
        }

        /// <summary>
        /// Utility method for use in filtering nodes that should or shouldn't appear in
        /// the tree
        /// </summary>
        /// <param name="node">Node to check</param>
        /// <returns>True if the node should be in the tree.</returns>
        bool ShouldNodeExistInTree(INode node)
        {
            if (node is New.Move.Core.Input ||
                node is New.Move.Core.Output ||
                node is New.Move.Core.SignalBase)
                return false;

            return true;
        }

        /// <summary>
        /// Recursive utility method to populate the navigator tree
        /// </summary>
        /// <param name="root">Root transitional node to generate tree items from</param>
        /// <param name="tviParent">Parent tree item for the generated items</param>
        void PopulateTree(ITransitional root, NavigatorTreeViewItem tviParent)
        {
            if (tviParent == null)
            {
                //ITransitional nodes always need to exist in the tree, so there is
                //no need to do an explicit check in this case
                tviParent = (NavigatorTreeViewItem)CreateTreeItem(root);
                m_navigatorTree.Items.Add(tviParent);
            }

            foreach (INode childNode in root.Children)
            {
                if(ShouldNodeExistInTree(childNode))
                {
                    NavigatorTreeViewItem tvi = (NavigatorTreeViewItem)CreateTreeItem(childNode);
                    tviParent.Items.Add(tvi);
                   
                    if (childNode is ITransitional)
                    {
                        PopulateTree((ITransitional)childNode, tvi);
                    }
                }
            }
        }

        void DocumentHost_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (Database != null)
                Database.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);

            //Clear out our content that is associated with the previous
            //database
            Clear();

            try
            {
                DocumentPane pane = sender as DocumentPane;
                DocumentContent content = pane.SelectedItem as DocumentContent;
                Database database = content.Tag as Database;

                Database = database;

                Database.ActiveChanged += new RoutedEventHandler(Database_ActiveChanged);
                
                //Populate our tree to reflect the new database
                if(Database.Root != null)
                {
                    PopulateTree(Database.Root, null);
                }
               
                if(Database.Active != null)
                {
                    ExpandToItem(Database.Active.Id, false);
                }
            }
            catch
            {
                Database = null;
            }
        }

        /// <summary>
        /// Event handler to be called when the active item in the database has changed.
        /// When the active item has changed we need to ensure the tree view is expanded
        /// out to show the active item.
        /// </summary>
        void Database_ActiveChanged(object sender, RoutedEventArgs e)
        {
            if (Database != null && Database.Active != null)
            {
                if (m_itemDictionary.ContainsKey(Database.Active.Id))
                {
                    NavigatorTreeViewItem tvi = m_itemDictionary[Database.Active.Id];
                    ExpandToItem(tvi, false);
                }
            }
        }

        /// <summary>
        /// Event handler for the navigator tree right mouse button up event.  Used
        /// to select the item and display the context menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TreeView_OnMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(e.Source is NavigatorTreeViewItem)
            {
                NavigatorTreeViewItem tvi = (NavigatorTreeViewItem)e.Source;
                tvi.IsSelected = true;
                m_itemContextMenu.IsOpen = true;
            }
        }

        /// <summary>
        /// Event handler to update the navigator tree to reflect transition selection changes
        /// from outside of the navigator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Transition_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            Transition transition = sender as Transition;
            if(m_itemDictionary.ContainsKey(transition.Id))
            {
                NavigatorTreeViewItem tvi = m_itemDictionary[transition.Id];

                if (e.Action == SelectionChangedAction.Add)
                {
                    if (tvi.IsSelected == false)
                        tvi.IsSelected = true;
                }
                else if (e.Action == SelectionChangedAction.Remove)
                {
                    if (tvi.IsSelected == true)
                        tvi.IsSelected = false;
                }
            }
        }

        /// <summary>
        /// Event handler to update the navigator tree to reflect node selection changes from
        /// outside of the navigator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Node_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            INode node = sender as INode;

            if (m_itemDictionary.ContainsKey(node.Id))
            {
                NavigatorTreeViewItem tvi = m_itemDictionary[node.Id];

                if (e.Action == SelectionChangedAction.Add)
                {
                  if(tvi.IsSelected == false)
                      tvi.IsSelected = true;
                }
                else if (e.Action == SelectionChangedAction.Remove)
                {
                    if (tvi.IsSelected == true)
                        tvi.IsSelected = false;
                }
            }
        }

        /// <summary>
        /// Event handler to update the database based on selection changes in the navigator tree view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //Was the selection cleared?
            if (e.NewValue == null)
                return;

            NavigatorTreeViewItem tvi = e.NewValue as NavigatorTreeViewItem;
            if (tvi is TransitionTreeViewItem)
            {
                TransitionTreeViewItem transTvi = tvi as TransitionTreeViewItem;

                INode parentNode = null;
                try
                {
                    parentNode = Database.Find(transTvi.ParentGuid);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(String.Format("NavigatorControl.TreeView_SelectedItemChanged, {0}", ex.Message));
                }

                if(parentNode != null)
                {
                    IAutomaton automaton = (IAutomaton)parentNode;

//                    if ((Database.Active != null) && (Database.Active != automaton))
//                        Database.Active = (ITransitional)automaton;

                    //Resolve the transition GUID
                    Transition transNode = (from transition in automaton.Transitions where transition.Id == transTvi.Guid select transition).FirstOrDefault();
                    if (transNode != null && !Database.SelectedItems.Contains(transNode))
                    {
                        // WTF: this whole event handler just seems to be fucked!
                        //Database.SelectedItems.Clear();
                        //Database.SelectedItems.Add(transNode);
                    }
                }
            }
            else
            {
                INode node = null;
                try
                {
                    node = Database.Find(tvi.Guid);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(String.Format("NavigatorControl.TreeView_SelectedItemChanged, {0}", ex.Message));
                }

                if (node != null)
                {
                    if (!(node is ITransitional))
                    {
                        //Make sure that the diagram view is set to display the diagram for the 
                        //parent transitional of the selected node
                        INode parentTransitional = node;
                        while ((parentTransitional != null) &&
                               !(parentTransitional is ITransitional))
                        {
                            parentTransitional = Database.Find(node.Parent);
                        }

                        if (parentTransitional != null)
                        {
                            if (Database.Active != parentTransitional)
                                Database.Active = (ITransitional)parentTransitional;
                        }
                    }

                    //Set the node as selected
                    if (!Database.SelectedItems.Contains(node))
                    {
                        Database.SelectedItems.Clear();
                        Database.SelectedItems.Add(node);
                    }
                }
            }
        }

        /// <summary>
        /// Event handler for the context menu "Make Active" item.  This calls back into the database
        /// to set the active item to the item that was chosen in the navigator tree.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TreeViewItemContextMenu_MakeActive_OnClick(object sender, RoutedEventArgs e)
        {
            if(m_navigatorTree.SelectedItem != null)
            {
                NavigatorTreeViewItem tvi = (NavigatorTreeViewItem)m_navigatorTree.SelectedItem;
                INode node = null;
                
                try
                {
                    node = Database.Find(tvi.Guid);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(String.Format("NavigatorControl.TreeViewItemContextMenu_MakeActive_OnClick, {0}", ex.Message));
                }
              
                if(node != null)
                {
                    if(node is ITransitional)
                    {
                        Database.Active = (ITransitional)node;
                    }
                }
            }
        }

        /// <summary>
        /// Event handler for managing the addition and removal of transitions 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TransitionCollectionChangedHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(e.Action == NotifyCollectionChangedAction.Add)
            {
                for (int i = 0; i < e.NewItems.Count; i++)
                {
                    object obj = e.NewItems[i];

                    if (obj is Transition)
                    {
                        Transition transition = obj as Transition;
                        if (m_itemDictionary.ContainsKey(transition.Parent.Id))
                        {
                            NavigatorTreeViewItem parentTvi = m_itemDictionary[transition.Parent.Id];
                            NavigatorTreeViewItem tvi = (NavigatorTreeViewItem)CreateTreeItem(transition, e.NewStartingIndex + i);
                            parentTvi.Items.Add(tvi);
                        }
                    }
                }
            }
            else if(e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (object obj in e.OldItems)
                {
                    if (obj is Transition)
                    {
                        Transition transition = obj as Transition;
                        RemoveItem(transition.Id);
                    }
                }
            }
        }

        /// <summary>
        /// Event handler for collection changed events coming from the tree items. This handler ensures that
        /// the tree view is synchronized with the database as nodes are added or removed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ChildCollectionChangedHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (object obj in e.NewItems)
                {
                    if( obj is INode)
                    {
                        INode node = obj as INode;
                        if (ShouldNodeExistInTree(node))
                        {
                            //Create the item for the node that was added
                            NavigatorTreeViewItem tvi = (NavigatorTreeViewItem)CreateTreeItem(node);

                            //If the node is a child of another node then we need to root it at the 
                            //right place in the navigator tree, otherwise hang it off the navigator
                            //root
                            if (node.Parent != Guid.Empty)
                            {
                                if (m_itemDictionary.ContainsKey(node.Parent))
                                {
                                    NavigatorTreeViewItem tviParent = m_itemDictionary[node.Parent];
                                    tviParent.Items.Add(tvi);
                                }
                            }
                            else
                            {
                                m_navigatorTree.Items.Add(tvi);
                            }
                        }
                    }
                }
            }
            else if(e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach(object obj in e.OldItems)
                {
                    if( obj is INode )
                    {
                        RemoveItem(((INode)obj).Id);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Base class for all the navigator tree view items
    /// </summary>
    public class NavigatorTreeViewItem : TreeViewItem
    {
        public Guid Guid { get; set; }
    }

    /// <summary>
    /// Class to represent transitions in the navigator tree
    /// </summary>
    public class TransitionTreeViewItem : NavigatorTreeViewItem
    {
        public Guid ParentGuid { get; set; }
        public int Index { get; set; }
    }
#endif
    }
}
