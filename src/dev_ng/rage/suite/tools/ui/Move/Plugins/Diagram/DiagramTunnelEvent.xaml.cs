﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;
using Move.UI;

namespace Move
{
    public partial class DiagramTunnelEvent : DiagramNode, IDiagramAnchor
    {
        public DiagramTunnelEvent(TunnelEvent source)
            : base(source)
        {
            InitializeComponent();
            Anchor = source.EventAnchor;
            Anchor.Tag = this;

            Database db = source.Database;

            Events.ItemsSource = db.Events;
            Events.DisplayMemberPath = "Name";

            Events.SetBinding(ComboBox.SelectedItemProperty,
                new Binding("Event") { Source = source });

            Unloaded += new RoutedEventHandler(DiagramTunnelEvent_Unloaded);
        }

        void DiagramTunnelEvent_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(Events, ComboBox.SelectedItemProperty);
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            MenuItem deleteItem = new MenuItem() { Header = "Delete" };
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);

            contextMenu.Items.Add(deleteItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;
            contextMenu.Items.Clear();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }

        protected DiagramCanvas GetDiagramCanvas(DependencyObject obj)
        {
            while (obj != null && !(obj is DiagramCanvas))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as DiagramCanvas;
        }

        void Grapple_MouseDown(object sender, RoutedEventArgs e)
        {
            DiagramCanvas diagram = GetDiagramCanvas(this);
            if (diagram != null)
            {
            }
        }

        public IDiagramAnchor HitTest(Point point)
        {
            return null;
        }

        public DiagramNode Container { get; set; }
        public IAnchor Anchor { get; private set; }

        public Point? Sender { get { return null; } }
        public Point? Receiver
        {
            get
            {
                return new Point?(PART_Source.TranslatePoint(new Point(0, 0), GetDiagramCanvas(this)));
            }
        }
    }
}
