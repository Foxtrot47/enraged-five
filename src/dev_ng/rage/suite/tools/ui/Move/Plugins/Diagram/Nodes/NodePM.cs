﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class PMNode : Logic
    {
        static class ParameterOffset
        {
            public const uint ParameterizedMotion = 0;
            public const uint Phase = 1;
            public const uint Delta = 2;
            public const uint Rate = 3;
            public const uint Parameters = 4;
            public const uint Output = ushort.MaxValue;
        }

        [ParameterExport(ParameterOffset.ParameterizedMotion)]
        public ParameterizedMotionLogicalProperty PM { get; private set; }

        [ParameterExport(ParameterOffset.Phase)]
        public RealLogicalProperty Phase { get; private set; }

        [ParameterExport(ParameterOffset.Rate)]
        public RealLogicalProperty Rate { get; private set; }

        [ParameterExport(ParameterOffset.Delta)]
        public RealLogicalProperty Delta { get; private set; }

        [ParameterExport(ParameterOffset.Parameters)]
        public LogicalPropertyArray<RealLogicalProperty> Parameters { get; private set; }

        [ParameterExport(ParameterOffset.Output)]
        public ResultTransform Result { get; private set; }

        public PMNode(IDesc desc)
            : base(desc)
        {
            PM = "";
            Phase = 1.0f;
            Rate = 1.0f;
            Delta = 0.0f;
            Parameters = new LogicalPropertyArray<RealLogicalProperty>(); ;

            Result = new ResultTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;
            
            uint flags = 0;
            flags |= PM.GetExportFlag();
            flags |= (Rate.GetExportFlag() << 4);
            flags |= (Phase.GetExportFlag() << 8);
            flags |= (Delta.GetExportFlag() << 12);
            flags |= ((uint)Parameters.Items.Count << 16);
            int shift = 20;
            foreach (RealLogicalProperty parameter in Parameters.Items)
            {
                flags |= (parameter.GetExportFlag() << shift);
                shift += 2;
            }

            info.AppendUInt32(flags);

            PM.GetExportData(info, context);
            Rate.GetExportData(info, context);
            Phase.GetExportData(info, context);
            Delta.GetExportData(info, context);
            foreach (RealLogicalProperty parameter in Parameters.Items)
            {
                parameter.GetExportData(info, context);
            }
        }
    }

    public class PMNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Parameterized Motion";
            public static int Id = 17;
            public static string Help = "Supplies parameterized motion data";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(PMNode); } }

        public PMNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream( 
                        "Move.Diagram.Nodes.PM.Images.PaletteIcon_PM.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
            	_PaletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new PMNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}