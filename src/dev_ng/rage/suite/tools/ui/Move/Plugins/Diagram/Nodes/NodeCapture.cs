﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class CaptureNode : Logic
    {
        static class Const
        {
            public const uint FrameParameterExportOffset = 0;
            public const uint OwnerParameterExportOffset = 1;
            public const uint OutputParameterExportOffset = ushort.MaxValue;
        }

        [ParameterExport(Const.FrameParameterExportOffset)]
        public FrameLogicalProperty Frame 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.OwnerParameterExportOffset)]
        public BooleanLogicalProperty Owner 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.OutputParameterExportOffset)]
        public PassthroughTransform Result 
        { 
            get; 
            private set; 
        }

        public CaptureNode(IDesc desc)
            : base(desc)
        {
            Frame = new FrameLogicalProperty();
            Owner = true;

            Result = new PassthroughTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;

            uint flags = 0;
            flags |= Frame.GetExportFlag();
            flags |= (Owner.GetExportFlag() << 4);

            info.AppendUInt32(flags);

            info.AppendOffset(context.GetSourceLabelOf(Result));

            // write out additional data block
            Frame.GetExportData(info, context);
            Owner.GetExportData(info, context);
        }
    }

    public class CaptureNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "capture";
            public static int Id = 20;
            public static string Help = "captures data";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(CaptureNode); } }

        public CaptureNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Diagram.Nodes.Capture.Images.PaletteIcon_Capture.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new CaptureNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}