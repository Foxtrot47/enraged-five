﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rage.Move;
using New.Move.Core;

using AvalonDock;

namespace Move.UI
{
    public partial class SignalDialog : Window
    {
        public SignalDialog()
        {
            InitializeComponent();
        }

        Database Database
        {
            get
            {
                DocumentContent doc = App.Window.DocumentHost.SelectedItem as DocumentContent;
                return doc.Tag as Database;
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            Signals.ItemsSource = Database.Signals;
            Signals.DisplayMemberPath = "Name";
        }

        public void DoModal()
        {
            ShowDialog();
        }

        void AddButton_Click(object sender, RoutedEventArgs e)
        {
            SignalEditDialog dialog = new SignalEditDialog();
            dialog.DoModal();
        }

        void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            Signal selected = Signals.SelectedItem as Signal;
            Database.Signals.Remove(selected);
        }

        void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
