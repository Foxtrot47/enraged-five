﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

using Rage.Move;

using AvalonDock;

namespace Move.UI
{
    public partial class SignalEditDialog : Window
    {
        public SignalEditDialog()
        {
            InitializeComponent();
        }

        Database Database
        {
            get
            {
                DocumentContent doc = App.Window.DocumentHost.SelectedItem as DocumentContent;
                return doc.Tag as Database;
            }
        }

        public void DoModal()
        {
            ShowDialog();
        }

        delegate void Delegate();

        void OkButton_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItem item = Type.SelectedItem as ComboBoxItem;
            string selection = item.Content as string;

            Dictionary<string, Delegate> create = new Dictionary<string, Delegate>()
            {
                { "Real", 
                    delegate() 
                    {
                        Database.Signals.Add(new RealSignal() { Name = Signal.Text });
                    }
                },
                { "Boolean", 
                    delegate() 
                    {
                        Database.Signals.Add(new BooleanSignal() { Name = Signal.Text });
                    }
                },
                { "Animation", 
                    delegate() 
                    {
                        Database.Signals.Add(new AnimationSignal() { Name = Signal.Text });
                    }
                },
                { "Clip", 
                    delegate() 
                    {
                        Database.Signals.Add(new ClipSignal() { Name = Signal.Text });
                    }
                },
                { "Expression", 
                    delegate() 
                    {
                        Database.Signals.Add(new ExpressionSignal() { Name = Signal.Text });
                    }
                },
                { "Filter", 
                    delegate() 
                    {
                        Database.Signals.Add(new FilterSignal() { Name = Signal.Text });
                    }
                },
                { "Frame", 
                    delegate() 
                    {
                        Database.Signals.Add(new FrameSignal() { Name = Signal.Text });
                    }
                },
                { "Parameterized Motion", 
                    delegate() 
                    {
                        Database.Signals.Add(new ParameterizedMotionSignal() { Name = Signal.Text });
                    }
                },
                { "Node",
                    delegate()
                    {
                        Database.Signals.Add(new NodeSignal() { Name = Signal.Text });
                    }
                },
            };

            create[selection]();            

            Close();
        }

        void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }
    }
}
