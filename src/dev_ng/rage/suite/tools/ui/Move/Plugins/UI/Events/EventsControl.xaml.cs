﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AvalonDock;
using Move.Core;
using New.Move.Core;

namespace Move.UI
{
    public partial class EventsControl : UserControl
    {
        public EventsControl(Rage.Move.MainWindow wnd)
        {
            InitializeComponent();

            wnd.DocumentHost.SelectionChanged +=
                new SelectionChangedEventHandler(DocumentHost_SelectionChanged);
        }

        Database _Database;

        void Database_ActiveChanged(object sender, RoutedEventArgs e)
        {
            UpdateEvents();
        }

        void UpdateEvents()
        {
            EventList.ItemsSource = _Database.Events;
            EventList.DisplayMemberPath = "Name";
        }

        void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            NewEventGrid.Visibility = Visibility.Collapsed;
            NewEventName.Clear();
        }

        void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(NewEventName.Text))
            {
                _Database.Events.Add(new LogicEvent(NewEventName.Text));
            }
            NewEventGrid.Visibility = Visibility.Collapsed;
            NewEventName.Clear();
        }

        void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            LogicEvent selected = EventList.SelectedItem as LogicEvent;
            _Database.Events.Remove(selected);
        }

        void AddButton_Click(object sender, RoutedEventArgs e)
        {
            NewEventGrid.Visibility = Visibility.Visible;
        }

        void DocumentHost_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_Database != null)
            {
                _Database.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);
            }

            try
            {
                DocumentPane pane = sender as DocumentPane;
                DocumentContent content = pane.SelectedItem as DocumentContent;
                Database database = content.Tag as Database;

                _Database = database;

                _Database.ActiveChanged += new RoutedEventHandler(Database_ActiveChanged);

                UpdateEvents();
            }
            catch
            {
                _Database = null;
            }
        }
    }
}
