﻿using Rage.Move;

namespace Move.Diagram.Conditions
{
    public class InRange : IPlugin
    {
        static class Const
        {
            public static string Name = "in range";
            public static string Description = "provides transition condition for in range";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredConditionInRange registered = new RegisteredConditionInRange();
            App.Instance.Runtime.RegisteredConditions.Add(registered);
        }

        public void Dispose()
        {
        }
    }
}
