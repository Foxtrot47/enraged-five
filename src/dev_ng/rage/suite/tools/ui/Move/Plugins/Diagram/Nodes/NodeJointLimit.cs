﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class JointLimitNode : Logic //LogicNode
    {
        public PassthroughTransform Result { get; private set; }

        public JointLimitNode(IDesc desc)
            : base(desc)
        {
            Result = new PassthroughTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class JointLimitNodeDesc : ILogicDesc //IRegisterNode
    {
        static class Const
        {
            public static string Name = "joint limit";
            public static int Id = 29;
            public static string Help = "enforces joint limits";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(JointLimitNode); } }

        public JointLimitNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Move.Diagram.Nodes.JointLimit.Images.PaletteIcon_JointLimit.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new JointLimitNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}