﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AvalonDock;

using Move.Utils;
using Rage.Move.Core;
using Rockstar.MoVE.Components.MainWindow;
using Rockstar.MoVE.Services;

namespace Move.UI
{
    /// <summary>
    /// Interaction logic for DebugNetworkControl.xaml
    /// </summary>
    [Export("WindowDock", typeof(DockableContent)), WindowDockMetadata("Debug Network", WindowDockRegion.Bottom, WindowDockState.Hidden)]
    public partial class DebugNetworkControl : DockableContent
    {
        public DebugNetworkControl()
        {
            InitializeComponent();

            Binding b = new Binding();
            b.Source = _Messages;
            _networkMsgList.SetBinding(ListBox.ItemsSourceProperty, b);

            ShowUpdatesToggle.IsChecked = true;
        }

        [Import(typeof(IRuntimeCommunicationService))]
        IRuntimeCommunicationService RuntimeCommunicationService
        {
            set
            {
                _RuntimeCommunicationService = value;
                _RuntimeCommunicationService.StartDebugging += Client_StartDebugging;
                _RuntimeCommunicationService.StopDebugging += Client_StopDebugging;
            }
            get
            {
                return _RuntimeCommunicationService;
            }
        }
        IRuntimeCommunicationService _RuntimeCommunicationService;

        void Client_StartDebugging(object sender, SimpleEventArgs<IDebugSession> session)
        {
            this.InvokeAction(() => { _Messages.Clear(); }); // Synchronous

            ((Client.DebugSession)session.Payload).Link.AnyMessage += HandleAnyMessage;
        }

        void Client_StopDebugging(object sender, SimpleEventArgs<IDebugSession> session)
        {
            ((Client.DebugSession)session.Payload).Link.AnyMessage -= HandleAnyMessage;
        }

        void HandleAnyMessage(object sender, SimpleEventArgs<NetworkMessageBase> msg)
        {
            this.BeginInvokeAction(() => { _Messages.Add(msg.Payload); });
        }

        void ShowUpdates_OnCheck(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            ICollectionView view = CollectionViewSource.GetDefaultView(_Messages);
            if (ShowUpdatesToggle.IsChecked == true)
            {
                view.Filter = null;
            }
            else 
            {
                view.Filter = (x) => (((NetworkMessageBase)x).MessageType != NetworkMessageId.MSG_UPDATE_ROOT);
            }
        }

        protected ObservableCollection<NetworkMessageBase> _Messages = new ObservableCollection<NetworkMessageBase>();
    }
}
