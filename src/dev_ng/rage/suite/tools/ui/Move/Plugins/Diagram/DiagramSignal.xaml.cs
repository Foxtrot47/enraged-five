﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

using Move.UI;

using Boolean = New.Move.Core.Boolean;
using Expression = New.Move.Core.Expression;
using Frame = New.Move.Core.Frame;

namespace Move
{
    public partial class DiagramSignal : DiagramNode, IDiagramAnchor
    {
        private static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof(Brush), typeof(DiagramSignal));

        public Brush Fill
        {
            get { return (Brush)GetValue(FillProperty); }
            set { SetValue(FillProperty, value); }
        }

        public DiagramSignal(SignalBase source)
            : base(source)
        {
            InitializeComponent();
            Anchor = source.Anchor;
            Anchor.Tag = this;

            Fill = (Brush)FindBrush(source.Signal);

            Container = this;

            Unloaded += new RoutedEventHandler(DiagramSignal_Unloaded);
        }

        void DiagramSignal_Unloaded(object sender, RoutedEventArgs e)
        {
            Unloaded -= new RoutedEventHandler(DiagramSignal_Unloaded);
            Anchor.Tag = null;
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete";
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);

            contextMenu.Items.Add(deleteItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu context = (ContextMenu)sender;
            context.Items.Clear();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }

        public IDiagramAnchor HitTest(Point point)
        {
            DiagramCanvas diagram = GetDiagramCanvas(this);
            if (diagram != null)
            {
                DependencyObject hitobj = diagram.InputHitTest(point) as DependencyObject;
                while (hitobj != null && hitobj.GetType() != typeof(Diagram) && hitobj != this)
                {
                    if (CanBeConnected(hitobj))
                    {
                        return hitobj as DiagramAnchor;
                    }

                    hitobj = VisualTreeHelper.GetParent(hitobj);
                }
            }
            
            return null;
        }

        public DiagramNode Container { get; set; } 

        public IAnchor Anchor { get; private set; }

        protected Point? Start = null;

        public Point? Sender
        {
            get
            {
                return new Point?(PART_Source.TranslatePoint(new Point(0, 0), GetDiagramCanvas(this)));
            }
        }

        public Point? Receiver { get { return null; } }
        
        protected DiagramCanvas GetDiagramCanvas(DependencyObject obj)
        {
            while (obj != null && !(obj is DiagramCanvas))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as DiagramCanvas;
        }

        void Grapple_MouseDown(object sender, RoutedEventArgs e)
        {
            DiagramCanvas diagram = GetDiagramCanvas(this);
            if (diagram != null)
            {
                Point position = PART_Source.TranslatePoint(new Point(0, 0), diagram);
                Start = new Point?(position);
                e.Handled = true;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                Start = null;

            if (Start.HasValue)
            {
                DiagramCanvas diagram = GetDiagramCanvas(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        DiagramAdorner adorner = new DiagramAdorner(diagram, Start.Value, this);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

        bool CanBeConnected(DependencyObject hitObj)
        {
            if (Source is Animation)
            {
                return hitObj is IAnimationAnchor;
            }
            else if (Source is Boolean)
            {
                return hitObj is IBooleanAnchor;
            }
            else if (Source is Clip)
            {
                return hitObj is IClipAnchor;
            }
            else if (Source is Expression)
            {
                return hitObj is IExpressionAnchor;
            }
            else if (Source is Filter)
            {
                return hitObj is IFilterAnchor;
            }
            else if (Source is Frame)
            {
                return hitObj is IFrameAnchor;
            }
            else if (Source is ParameterizedMotion)
            {
                return hitObj is IParameterizedMotionAnchor;
            }
            else if (Source is Real)
            {
                return hitObj is IRealAnchor;
            }
            else if (Source is NodeSignalParameter)
            {
                return hitObj is ITransformAnchor;
            }

            return false;
        }

        Brush FindBrush(Signal signal)
        {
            if (signal.GetType() == typeof(AnimationSignal))
            {
                return (Brush)FindResource("AnimationMarkerBrush");
            }
            else if (signal.GetType() == typeof(Boolean))
            {
                return (Brush)FindResource("BooleanMarkerBrush");
            }
            else if (signal.GetType() == typeof(Clip))
            {
                return (Brush)FindResource("ClipMarkerBrush");
            }
            else if (signal.GetType() == typeof(Expression))
            {
                return (Brush)FindResource("ExpressionsMarkerBrush");
            }
            else if (signal.GetType() == typeof(Filter))
            {
                return (Brush)FindResource("FilterMarkerBrush");
            }
            else if (signal.GetType() == typeof(Frame))
            {
                return (Brush)FindResource("FrameMarkerBrush");
            }
            else if (signal.GetType() == typeof(ParameterizedMotion))
            {
                return (Brush)FindResource("ParameterizedMotionMarkerBrush");
            }
            else if (signal.GetType() == typeof(RealSignal))
            {
                return (Brush)FindResource("RealMarkerBrush");
            }
            else if (signal.GetType() == typeof(NodeSignal))
            {
                return (Brush)FindResource("TransformMarkerBrush");
            }

            return null;
        }
    }
}
