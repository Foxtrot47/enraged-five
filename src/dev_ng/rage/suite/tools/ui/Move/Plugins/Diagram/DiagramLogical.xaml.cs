﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Rage.Move.Utils;
using New.Move.Core;

using Move.UI;

namespace Move
{
   
    delegate DiagramAnchor AnchorDelegate(IAnchor anchor, string name);
    delegate DiagramAnchor PropertyDelegate(IProperty property, string name, UserControl container);

    delegate void MenuDelegate(DiagramLogical node, ContextMenu contextMenu);

    class DiagramAnchorPair : UserControl
    {
        public DiagramAnchorPair(string name, IProperty source, DiagramLogical container)
        {
            _Source = source as ILogicPropertyPair;
            _Name = name;
            Container = container;
        }

        DiagramLogical Container { get; set; }

        public ILogicPropertyPair Source { get { return _Source; } }
        ILogicPropertyPair _Source;
        string _Name;

        Panel PART_Anchors;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_Anchors = (Panel)GetTemplateChild("PART_Anchors");
            if (PART_Anchors != null)
            {
                if (_Source.First is IProperty)
                {
                    DiagramAnchor anchor = DiagramLogical.PropertyLookup[_Source.First.GetType()](_Source.First as IProperty, _Name, this);
                    anchor.Container = Container;
                    PART_Anchors.Children.Add(anchor);
                }
                else if (_Source.First is IAnchor)
                {
                    DiagramAnchor anchor = DiagramLogical.AnchorLookup[_Source.First.GetType()](_Source.First as IAnchor, _Name);
                    anchor.Container = Container;
                    PART_Anchors.Children.Add(anchor);
                }

                if (_Source.Second is IProperty)
                {
                    DiagramAnchor anchor = DiagramLogical.PropertyLookup[_Source.Second.GetType()](_Source.Second as IProperty, _Name, this);
                    anchor.Container = Container;
                    PART_Anchors.Children.Add(anchor);
                }
                else if (_Source.Second is IAnchor)
                {
                    DiagramAnchor anchor = DiagramLogical.AnchorLookup[_Source.Second.GetType()](_Source.Second as IAnchor, _Name);
                    anchor.Container = Container;
                    PART_Anchors.Children.Add(anchor);
                }
            }
        }
    }

    class DiagramAnchorCollection : UserControl
    {
        public DiagramAnchorCollection(string name, ILogicPropertyArray source) 
        {
            _Source = source;
            _Source.AddHandler(Source_ItemsChanged);

            _Name = name;

            Unloaded += new RoutedEventHandler(DiagramAnchorCollection_Unloaded);

            // this is a bit of a hack because of the way wpf does things, or the way we're doing things in wpf.
            // we store a list of the child properties so that the controls exist for adding connections to
            // array items straight away, but they can't actually be added to the anchors children until the template
            // is applied... sigh, like all things .net, the simple stuff is simple and anything else is a mess.

            foreach (IProperty property in _Source.Children)
            {
                if (property is ILogicPropertyPair)
                {
                    _Items.Add(new DiagramAnchorPair(_Name, property, Container));
                }
                else
                {
                    DiagramAnchor anchor = DiagramLogical.PropertyLookup[property.GetType()](property, _Name, this);
                    anchor.Container = Container;
                    _Items.Add(anchor);
                }
            }
        }

        internal DiagramLogical Container { get; set; }

        List<UserControl> _Items = new List<UserControl>();

        Panel PART_Anchors;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_Anchors = (Panel)GetTemplateChild("PART_Anchors");
            if (PART_Anchors != null)
            {
                foreach (UserControl item in _Items)
                {
                    PART_Anchors.Children.Add(item);
                }
                /*
                foreach (IProperty property in _Source.Children)
                {
                    if (property is ILogicPropertyPair)
                    {
                        PART_Anchors.Children.Add(new DiagramAnchorPair(_Name, property));
                    }
                    else
                    {
                        UserControl anchor = DiagramLogical.PropertyLookup[property.GetType()](property, _Name, this);
                        PART_Anchors.Children.Add(anchor);
                    }
                }
                 */
            }
        }

        ILogicPropertyArray _Source;
        string _Name;

        void DiagramAnchorCollection_Unloaded(object sender, RoutedEventArgs e)
        {
            _Source.RemoveHandler(Source_ItemsChanged);
        }

        void Source_ItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IProperty property in e.OldItems)
                {
                    UIElement[] children = new UIElement[PART_Anchors.Children.Count];
                    PART_Anchors.Children.CopyTo(children, 0);
                    
                    foreach (DiagramAnchor anchor in children.OfType<DiagramAnchor>())
                    {
                        if (anchor.Anchor == property)
                        {
                            anchor.Anchor.RemoveConnections();
                            PART_Anchors.Children.Remove(anchor);
                        }
                    }
                    
                    foreach (DiagramAnchorPair pair in children.OfType<DiagramAnchorPair>())
                    {
                        if (pair.Source == property)
                        {
                            pair.Source.RemoveConnections(Container.Source);
                            PART_Anchors.Children.Remove(pair);
                        }
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IProperty property in e.NewItems)
                {
                    if (property is ILogicPropertyPair)
                    {
                        PART_Anchors.Children.Add(new DiagramAnchorPair(_Name, property, Container));
                    }
                    else
                    {
                        DiagramAnchor anchor = DiagramLogical.PropertyLookup[property.GetType()](property, _Name, this);
                        anchor.Container = Container;
                        PART_Anchors.Children.Add(anchor);
                    }
                }
            }
        }
    }

    class AnchorCollection : List<DiagramAnchor>
    {
    }

    public partial class DiagramLogical : DiagramNode
    {
        public DiagramLogical(ILogic source)
            : base(source)
        {
            InitializeComponent();

            CreateAnchors();       
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            if (!(Source is Output) && !(Source is Input))
            {
                SelectionBehaviour.SetIsSelectionEnabled(this, true);
                SelectionBehaviour.SetIsSelected(this, IsSelected);
            }
        }

        public void CreateAnchors()
        {
            Anchors.Children.Clear();
            
            PropertyInfo[] properties = Source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                object value = pi.GetValue(Source, null);
                if (value != null && value.GetType().GetInterface("IConnectionAnchor") != null)
                {
                    IAnchor anchor = (IAnchor)pi.GetValue(Source, null);
                    Type type = anchor.GetType();
                    DiagramAnchor item = AnchorLookup[type](anchor, pi.Name);
                    item.Container = this;

                    Anchors.Children.Add(item);
                }
            }

            // add anchors for the properties
            foreach (PropertyInfo pi in properties)
            {
                object value = pi.GetValue(Source, null);
                if (value != null && value.GetType().GetInterface("ILogicPropertyArray") != null)
                {
                    ILogicPropertyArray property = (ILogicPropertyArray)pi.GetValue(Source, null);
                    DiagramAnchorCollection item = new DiagramAnchorCollection(pi.Name, property);
                    item.Container = this;
                    Anchors.Children.Add(item);
                }
                else 
                if (value != null && value.GetType().GetInterface("ILogicProperty") != null)
                {
                    Property property = pi.GetValue(Source, null) as Property;
                    Type type = property.GetType();
                    UserControl item = PropertyLookup[type](property, pi.Name, this);
                    Anchors.Children.Add(item);
                }
                else if (value != null && value.GetType().GetInterface("ILogicalEvent") != null)
                {
                    ILogicalEvent logicalEvent = pi.GetValue(Source, null) as ILogicalEvent;
                    DiagramAnchor inst = new DiagramAnchorEvent(pi.Name, logicalEvent as LogicalEventProperty);
                    inst.Container = this as DiagramNode;
                    logicalEvent.Tag = inst;
                    Anchors.Children.Add(inst);
                }
            }
        }

        static DiagramAnchor Instantiate_PassthroughTransform(IAnchor anchor, string name)
        {
            DiagramAnchorPassthroughTransform inst = new DiagramAnchorPassthroughTransform(name, anchor);
            anchor.Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_ResultTransform(IAnchor anchor, string name)
        {
            DiagramAnchorResultTransform inst = new DiagramAnchorResultTransform(name, anchor);
            anchor.Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_SourceTransform(IAnchor anchor, string name)
        {
            DiagramAnchorSourceTransform inst = new DiagramAnchorSourceTransform(name, anchor);
            anchor.Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_RealInput(IAnchor anchor, string name)
        {
            DiagramAnchorRealInput inst = new DiagramAnchorRealInput(name, anchor);
            anchor.Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_RealOutput(IAnchor anchor, string name)
        {
            DiagramAnchorRealOutput inst = new DiagramAnchorRealOutput(name, anchor);
            anchor.Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_SourceTransformProperty(IProperty property, string name, UserControl container)
        {
            IAnchor anchor = property as IAnchor;
            DiagramAnchorSourceTransform inst = new DiagramAnchorSourceTransform(name, anchor);
            anchor.Tag = inst;
            inst.Container = container as DiagramNode;
            return inst;
        }

        internal static Dictionary<Type, AnchorDelegate> AnchorLookup = new Dictionary<Type, AnchorDelegate>
        {
            { typeof(PassthroughTransform), Instantiate_PassthroughTransform },
            { typeof(ResultTransform), Instantiate_ResultTransform },            
            { typeof(SourceTransform), Instantiate_SourceTransform },
            { typeof(RealInputAnchor), Instantiate_RealInput },
            { typeof(RealOutputAnchor), Instantiate_RealOutput },
        };

        static DiagramAnchor Instantiate_AnimationSource(IProperty property, string name, UserControl container)
        {
            DiagramAnchor inst = new DiagramAnchorAnimationSource(name, property as Property);
            inst.Container = container as DiagramNode;
            (property as IAnchor).Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_ClipSource(IProperty property, string name, UserControl container)
        {
            DiagramAnchor inst = new DiagramAnchorClipSource(name, property as Property);
            inst.Container = container as DiagramNode;
            (property as IAnchor).Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_BooleanSource(IProperty property, string name, UserControl container)
        {
            DiagramAnchor inst = new DiagramAnchorBooleanSource(name, property as Property);
            inst.Container = container as DiagramNode;
            (property as IAnchor).Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_ExpressionSource(IProperty property, string name, UserControl container)
        {
            DiagramAnchor inst = new DiagramAnchorExpressionSource(name, property as Property);
            inst.Container = container as DiagramNode;
            (property as IAnchor).Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_FilterSource(IProperty property, string name, UserControl container)
        {
            DiagramAnchor inst = new DiagramAnchorFilterSource(name, property as Property);
            inst.Container = container as DiagramNode;
            (property as IAnchor).Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_FrameSource(IProperty property, string name, UserControl container)
        {
            DiagramAnchor inst = new DiagramAnchorFrameSource(name, property as Property);
            inst.Container = container as DiagramNode;
            (property as IAnchor).Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_ParameterizedMotionSource(IProperty property, string name, UserControl container)
        {
            DiagramAnchor inst = new DiagramAnchorParameterizedMotionSource(name, property as Property);
            inst.Container = container as DiagramNode;
            (property as IAnchor).Tag = inst;
            return inst;
        }

        static DiagramAnchor Instantiate_RealSource(IProperty property, string name, UserControl container)
        {
            DiagramAnchor inst = new DiagramAnchorRealSource(name, property as Property);
            inst.Container = container as DiagramNode;
            (property as IAnchor).Tag = inst;
            return inst;
        }

        internal static Dictionary<Type, PropertyDelegate> PropertyLookup = new Dictionary<Type, PropertyDelegate>
        {
            { typeof(AnimationLogicalProperty), Instantiate_AnimationSource },
            { typeof(ClipLogicalProperty), Instantiate_ClipSource },
            { typeof(BooleanLogicalProperty), Instantiate_BooleanSource },
            { typeof(ExpressionsLogicalProperty), Instantiate_ExpressionSource },
            { typeof(FilterLogicalProperty), Instantiate_FilterSource },
            { typeof(FrameLogicalProperty), Instantiate_FrameSource },
            { typeof(ParameterizedMotionLogicalProperty), Instantiate_ParameterizedMotionSource },
            { typeof(RealLogicalProperty), Instantiate_RealSource },
            { typeof(SourceTransform), Instantiate_SourceTransformProperty },
        };

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            if (MenuLookup.ContainsKey(Source.GetType()))
            {
                MenuLookup[Source.GetType()](this, contextMenu);
                return;
            }

            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete node";
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);

            contextMenu.Items.Add(deleteItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu context = (ContextMenu)sender;
            context.Items.Clear();
        }

        static void MenuMessages_Opened(DiagramLogical node, ContextMenu contextMenu)
        {
            MenuItem addItem = new MenuItem();
            addItem.Header = "Add message...";

            contextMenu.Items.Add(addItem);
        }

        static void MenuNull_Opened(DiagramLogical node, ContextMenu contextMenu)
        {
            // do nothing
        }

        static Dictionary<Type, MenuDelegate> MenuLookup = new Dictionary<Type, MenuDelegate>
        {
            { typeof(Input), MenuNull_Opened },
            //{ typeof(Messages), MenuMessages_Opened },
            { typeof(Output), MenuNull_Opened },
        };

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }
    }
}
