﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

using New.Move.Core;

namespace Move.UI
{
    public abstract class DiagramAnchor : UserControl, IDiagramAnchor
    {
        static class Const
        {
            public static string PartName_Anchor = "PART_Anchor";
        }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(DiagramAnchor));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public DiagramAnchor(string name, IAnchor source) : this()
        {
            DataContext = this;
            Label = name;
            Anchor = source;
        }

        DiagramAnchor()
        {
            Unloaded += new RoutedEventHandler(DiagramAnchor_Unloaded);
        }

        public IAnchor Anchor { get; private set; }

        public Guid Id { get { return Anchor.Id; } }

        public virtual IDiagramAnchor HitTest(Point point) { return null; }

        public DiagramNode Container { get; set; } 

        protected Point? Start = null;

        public virtual Point? Sender { get { return null; } }
        public virtual Point? Receiver { get { return null; } }

        protected DiagramCanvas GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is DiagramCanvas))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as DiagramCanvas;
        }

        void DiagramAnchor_Unloaded(object sender, RoutedEventArgs e)
        {
            if (Anchor != null)
            {
                Anchor.Tag = null;
                Anchor = null;
            }
        }

        protected void Grapple_MouseDown(object sender, RoutedEventArgs e)
        {
            try
            {
                Motiontree parent = Anchor.Parent.Database.Find(Anchor.Parent.Parent) as Motiontree;

                // TODO: store a reference to connections in the anchor
                New.Move.Core.Connection connection = parent.Connections.FirstOrDefault(
                    delegate(New.Move.Core.Connection c) { return c.Dest == Anchor; });

                DiagramConnection tag = connection.Tag as DiagramConnection;

                parent.Connections.Remove(connection);

                if (tag.Sender.Sender.HasValue)
                {
                    DiagramCanvas diagram = GetDiagram(this);
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        Adorner adorner = new DiagramAdorner(diagram, tag.Sender.Sender.Value, tag.Sender);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
                
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}