﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Move.Core;

namespace Move.UI
{
    /// <summary>
    /// Interaction logic for BoolRowControl.xaml
    /// </summary>
    public partial class BoolRowControl : UserControl
    {
        public BoolRowControl()
        {
            InitializeComponent();
        }

        public BoolRowControl(int bit, WindowedTimeLine timeline, RuntimeDatabase runtimeDb)
        {
            StateBit = bit;
            Timeline = timeline;
            RuntimeDb = runtimeDb;
            SignalId = -1;
            SignalInterval = null;
        }

        public BoolRowControl(int signal, StateInterval interval, WindowedTimeLine timeline, RuntimeDatabase runtimeDb)
        {
            Timeline = timeline;
            RuntimeDb = runtimeDb;
            SignalId = signal;
            StateBit = -1;
            SignalInterval = interval;
        }


        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            // lock the DB so the indices don't change
            lock(RuntimeDb.LockObject)
            {
                Interval queryInter = new Interval(TimeSpan.FromSeconds(Timeline.PhaseMin), TimeSpan.FromSeconds(Timeline.PhaseMax));

                if (StateBit >= 0)
                {
                    // should share this info among rows
                    int startIndex, stopIndex;
                    RuntimeDb.FindFrameIndicesForInterval(queryInter, out startIndex, out stopIndex);
                    if (startIndex >= stopIndex)
                    {
                        return; // Not enough data available to draw anything
                    }

                    foreach (BoolInterval i in RuntimeDb.GenerateIntervalsForStatebit(StateBit, startIndex, stopIndex))
                    {
                        DrawInterval(i, dc, 0.0);

                    }
                }
                else if (SignalId >= 0)
                {
                    if (SignalInterval == null)
                    {
                        // It's a global signal row
                        var signal = RuntimeDb.GlobalSignalHistory.FindSignal((ushort)SignalId) as SignalHistory.BoolSignal;

                        Debug.Assert(signal != null, String.Format("Couldn't find global signal with id {0}", SignalId));
                        
                        foreach(var inter in signal.GenerateCoveringIntervals(queryInter))
                        {
                            DrawInterval(inter, dc, 0.0);
                        }
                    }
                    else
                    {
                        double offset = Timeline.PhaseToAbsPixel(SignalInterval.StartTime.TotalSeconds);
                        var signal = SignalInterval.SignalHistory.FindSignal((ushort)SignalId) as SignalHistory.BoolSignal;
                        Debug.Assert(signal != null, String.Format("Couldn't find signal with id {0} in state interval {1}", SignalId, SignalInterval));
                        foreach(var inter in signal.GenerateCoveringIntervals(SignalInterval))
                        {
                            DrawInterval(inter, dc, offset);
                        }
                    }
                }
                else
                {
                    throw new InvalidOperationException("Every BoolRowControl needs either a StateBit to view, or a SignalId to view");
                }
            }
        }

        private void DrawInterval(BoolInterval i, DrawingContext dc, double pixelOffset)
        {
            double top = 0;
            double bottom = RenderSize.Height;
            double startPix = Timeline.PhaseToAbsPixel(i.StartTime.TotalSeconds) - pixelOffset;
            double endPix = Timeline.PhaseToAbsPixel(i.EndTime.TotalSeconds) - pixelOffset;

            Brush b = Brushes.Blue;
            switch (i.Data)
            {
                case BoolInterval.StateEnum.BI_False:
                    b = OffBrush;
                    break;
                case BoolInterval.StateEnum.BI_True:
                    b = OnBrush;
                    break;
                case BoolInterval.StateEnum.BI_Unknown:
                    b = UnknownBrush;
                    break;
            }

            dc.DrawRectangle(b, null, new Rect(startPix, top, endPix - startPix, bottom));
        }
        public int StateBit { get; protected set; }
        public WindowedTimeLine Timeline { get; protected set; }
        public RuntimeDatabase RuntimeDb { get; protected set; }
        public int SignalId { get; protected set; }
        public StateInterval SignalInterval { get; protected set; }

        public Brush OnBrush = Brushes.Green;
        public Brush OffBrush = Brushes.DarkGray;
        public Brush UnknownBrush = Brushes.Transparent;
    }
}
