﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Operators
{
    public class OperatorItemAdd : IOperatorItem
    {
        public void GetExportData(ChunkObject info, ChunkContext context)
        {
            info.AppendUInt32(3);
            info.AppendUInt32(0);
        }
    }

    public class OperatorAdd : Logic, IOperator
    {
        public RealOutputAnchor Result { get; private set; }
        public RealInputAnchor Input0 { get; private set; }
        public RealInputAnchor Input1 { get; private set; }
       
        public OperatorAdd(IDesc desc)
            : base(desc)
        {
            Result = new RealOutputAnchor(this);
            Input0 = new RealInputAnchor(this);
            Input1 = new RealInputAnchor(this);
        }

        public List<IAnchor> Inputs
        {
            get
            {
                List<IAnchor> inputs = new List<IAnchor>();
                inputs.Add(Input0);
                inputs.Add(Input1);

                return inputs;
            }
        }

        public IOperatorItem Item { get { return new OperatorItemAdd(); } }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class RegisteredOperatorAdd : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Add";
            public static int Id = 1;
            public static string Help = "Add operation";
            public static string Group = "Operator";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _paletteIcon; } }

        public Type ConstructType { get { return typeof(OperatorAdd); } }

        public RegisteredOperatorAdd()
        {
            try
            {
                // Load the palette icon embedded resource
                _paletteIcon = new BitmapImage();
                _paletteIcon.BeginInit();
                _paletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Move.Diagram.Operators.Add.Images.PaletteIcon_OpAdd.png" );
                _paletteIcon.EndInit();
            }
            catch
            {
            	_paletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new OperatorAdd(this);
        }

        protected BitmapImage _paletteIcon = null;
    }

    public class OperatorAddConstant : Logic, IOperator
    {
        public RealLogicalProperty Constant { get; private set; }
        public RealOutputAnchor Result { get; private set; }
        public RealInputAnchor Input { get; private set; }
        
        public OperatorAddConstant(IDesc desc)
            : base(desc)
        {
            Constant = 1.0f;

            Result = new RealOutputAnchor(this);
            Input = new RealInputAnchor(this);
        }

        public List<IAnchor> Inputs
        {
            get
            {
                List<IAnchor> inputs = new List<IAnchor>();
                inputs.Add(Input);

                return inputs;
            }
        }

        public IOperatorItem Item { get { return new OperatorItemAdd(); } }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class RegisteredOperatorAddConstant : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Add Constant";
            public static int Id = 2;
            public static string Help = "Add constant operation";
            public static string Group = "Operator";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _paletteIcon; } }

        public Type ConstructType { get { return typeof(OperatorAddConstant); } }

        public RegisteredOperatorAddConstant()
        {
            try
            {
                // Load the palette icon embedded resource
                _paletteIcon = new BitmapImage();
                _paletteIcon.BeginInit();
                _paletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Move.Diagram.Operators.Add.Images.PaletteIcon_OpAddConstant.png" );
                _paletteIcon.EndInit();
            }
            catch
            {
            	_paletteIcon = null;
            }
        }

        public INode Create(Database database)
        {
            return new OperatorAddConstant(this);
        }

        protected BitmapImage _paletteIcon = null;
    }
}