﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Move.UI
{
    class NavigatorDragDropMgr<ItemType> where ItemType : NavigatorTreeViewItem
    {
        TreeView _treeView;
        bool _canInitiateDrag;
        bool _isDragInProcess;
        NavigatorDragAdorner _dragAdorner;
        Point _pointMouseDown;
        NavigatorTreeViewItem _itemDragging;
        NavigatorTreeViewItem _itemDraggingOver;

        public NavigatorDragDropMgr()
        {
            //_showDragAdorner = false;
            _canInitiateDrag = false;
            _isDragInProcess = false;
            
        }

        public NavigatorDragDropMgr( TreeView treeView )
            : this()
        {
            this.TreeView = treeView;
        }

        public TreeView TreeView
        {
            get { return this._treeView;  }
            set
            {
                if (_isDragInProcess)
                    throw new InvalidOperationException("Attempting to set the TreeView property during a drag operation.");

                if(_treeView != null)
                {
                    _treeView.PreviewMouseLeftButtonDown -= TreeView_PreviewMouseLeftButtonDown;
                    _treeView.PreviewMouseMove -= TreeView_PreviewMouseMove;
                    _treeView.DragOver -= TreeView_DragOver;
                    _treeView.Drop -= TreeView_Drop;
                }

                _treeView = value;

                if(_treeView != null)
                {
                    if (!_treeView.AllowDrop)
                        _treeView.AllowDrop = true;

                    _treeView.PreviewMouseLeftButtonDown += TreeView_PreviewMouseLeftButtonDown;
                    _treeView.PreviewMouseMove += TreeView_PreviewMouseMove;
                    _treeView.DragOver += TreeView_DragOver;
                    _treeView.Drop += TreeView_Drop;
                    _treeView.DragEnter += TreeView_DragEnter;
                    _treeView.DragLeave += TreeView_DragLeave;
                }
            }
        }

        ItemType GetItemAtPoint(Point p)
        {
            DependencyObject depObj = this._treeView.InputHitTest(p) as DependencyObject;
            while (depObj != null && !(depObj is TreeViewItem))
            {
                depObj = VisualTreeHelper.GetParent(depObj);
            }

            if (depObj != null && (depObj is ItemType))
                return depObj as ItemType;
            else
                return null;
        }

        AdornerLayer InitializeAdorner( NavigatorTreeViewItem item )
        {
            TextBlock clone = new TextBlock();
            clone.Text = item.Header.ToString();
            clone.FontWeight = item.FontWeight;

            VisualBrush brush = new VisualBrush( clone );
            brush.Stretch = Stretch.None;

            Typeface tf = new Typeface(item.FontFamily, FontStyles.Normal, FontWeights.Bold, FontStretches.Normal);
            FormattedText ft = new FormattedText(clone.Text, System.Globalization.CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, tf, 10.0, item.Foreground);

            this._dragAdorner = new NavigatorDragAdorner(this._treeView, new Size(ft.Width, ft.Height), brush);
            this._dragAdorner.Opacity = 0.5f;

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(_treeView);
            layer.Add(_dragAdorner);

            _pointMouseDown = Mouse.GetPosition(_treeView);

            return layer;
        }

        void InitDrag()
        {
            _canInitiateDrag = false;
            _isDragInProcess = true;
            _itemDraggingOver = null;

            TreeViewItemDragState.SetIsBeingDragged(_itemDragging, true);
        }

        void PerformDrag()
        {
            ItemType selectedItem = this._treeView.SelectedItem as ItemType;
            DragDropEffects allowedEffects = DragDropEffects.Move;
            if (DragDrop.DoDragDrop(this._treeView, selectedItem, allowedEffects) != DragDropEffects.None)
            {
                //The item was dropped into a new location, so do something about it...
            }
        }

        void FinishDrag( AdornerLayer adornerLayer )
        {
            TreeViewItemDragState.SetIsBeingDragged(_itemDragging, false);

            if (_itemDraggingOver != null)
            {
                TreeViewItemDragState.SetIsUnderDragCursorTop(_itemDraggingOver, false);
                TreeViewItemDragState.SetIsUnderDragCursorBottom(_itemDraggingOver, false);
            }

            if(adornerLayer != null)
            {
                adornerLayer.Remove(_dragAdorner);
                _dragAdorner = null;
            }

            _itemDraggingOver = null;
            _isDragInProcess = false;
        }

        void TreeView_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _canInitiateDrag = false;
        }

        void TreeView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(this._treeView);
            ItemType item = GetItemAtPoint(p);
            if (item == null)
            {
                _itemDragging = null;
                _canInitiateDrag = false;
                _pointMouseDown = new Point(Int32.MinValue, Int32.MinValue);
            }
            else
            {
                _itemDragging = item;
                _canInitiateDrag = true;
                _pointMouseDown = p;
            }
        }

        void TreeView_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (!_canInitiateDrag)
                return;

            AdornerLayer adornerLayer = InitializeAdorner(_itemDragging);

            InitDrag();
            PerformDrag();
            FinishDrag( adornerLayer );
        }

        void TreeView_DragOver(object Sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Move;

            Point p = e.GetPosition(this._treeView);
            ItemType tvi = (ItemType)GetItemAtPoint(p);
            if(tvi != null)
            {
                if (_itemDraggingOver != null && _itemDraggingOver != tvi)
                {
                    TreeViewItemDragState.SetIsUnderDragCursorTop(_itemDraggingOver, false);
                    TreeViewItemDragState.SetIsUnderDragCursorBottom(_itemDraggingOver, false);
                }

                _itemDraggingOver = tvi;

                Point itemPoint = e.GetPosition(_itemDraggingOver);

                if (itemPoint.Y >= (_itemDraggingOver.DesiredSize.Height / 2.0))
                {
                    TreeViewItemDragState.SetIsUnderDragCursorBottom(_itemDraggingOver, true);
                    TreeViewItemDragState.SetIsUnderDragCursorTop(_itemDraggingOver, false);
                }
                else
                {
                    TreeViewItemDragState.SetIsUnderDragCursorTop(_itemDraggingOver, true);
                    TreeViewItemDragState.SetIsUnderDragCursorBottom(_itemDraggingOver, false);
                }
            }

            UpdateDragAdornerLocation();
        }

        void TreeView_Drop(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;

            //Get the item that was dropped
            if( !e.Data.GetDataPresent( typeof(ItemType)) )
                return;

            ItemType data = e.Data.GetData( typeof(ItemType) ) as ItemType;
            if (data == null)
                return;

            //Get the item that was being dropped on
            Point p = e.GetPosition(this._treeView);
            ItemType tvi = (ItemType)GetItemAtPoint(p);
            if(tvi != null)
            {
                Console.WriteLine("Item {0} was dropped onto item {1}", data.Header, tvi.Header);

                e.Effects = DragDropEffects.Move;
            }
        }

        void TreeView_DragEnter(object sender, DragEventArgs e)
        {
            if(_dragAdorner != null && _dragAdorner.Visibility != Visibility.Visible)
            {
                UpdateDragAdornerLocation();
                _dragAdorner.Visibility = Visibility.Visible;
            }
        }

        void TreeView_DragLeave(object sender, DragEventArgs e)
        {
            if(_dragAdorner != null)
            {
                _dragAdorner.Visibility = Visibility.Collapsed;
            }
        }

        void UpdateDragAdornerLocation()
        {
            if(_dragAdorner != null)
            {
                Point ptCursor = Mouse.GetPosition(_treeView);

                double left = ptCursor.X - _pointMouseDown.X;
                Point itemLoc = _itemDragging.TranslatePoint(new Point(0, 0), _treeView);
                double top = itemLoc.Y + ptCursor.Y - _pointMouseDown.Y;

                _dragAdorner.SetOffsets(left, top);
            }
        }
    }

    public static class TreeViewItemDragState
    {
        public static readonly DependencyProperty IsBeingDraggedProperty =
            DependencyProperty.RegisterAttached(
                "IsBeingDragged",
                typeof(bool),
                typeof(TreeViewItemDragState),
                new UIPropertyMetadata(false));

        public static bool GetIsBeingDragged( TreeViewItem item )
        {
            return (bool)item.GetValue( IsBeingDraggedProperty );
        }

        internal static void SetIsBeingDragged( TreeViewItem item, bool value)
        {
            item.SetValue( IsBeingDraggedProperty, value);
        }

        //-------------------------------------

        public static readonly DependencyProperty IsUnderDragCursorProperty =
            DependencyProperty.RegisterAttached(
                "IsUnderDragCursor",
                typeof(bool),
                typeof(TreeViewItemDragState),
                new UIPropertyMetadata(false));

        public static bool GetIsUnderDragCursor(TreeViewItem item)
        {
            return (bool)item.GetValue(IsUnderDragCursorProperty);
        }

        internal static void SetIsUnderDragCursor(TreeViewItem item, bool value)
        {
            item.SetValue(IsUnderDragCursorProperty, value);
        }

        //-------------------------------------

        public static readonly DependencyProperty IsUnderDragCursorTopProperty =
            DependencyProperty.RegisterAttached(
                "IsUnderDragCursorTop",
                typeof(bool),
                typeof(TreeViewItemDragState),
                new UIPropertyMetadata(false));

        public static bool GetIsUnderDragCursorTop(TreeViewItem item)
        {
            return (bool)item.GetValue(IsUnderDragCursorTopProperty);
        }

        internal static void SetIsUnderDragCursorTop(TreeViewItem item, bool value)
        {
            item.SetValue(IsUnderDragCursorTopProperty, value);
        }

        //-------------------------------------

        public static readonly DependencyProperty IsUnderDragCursorBottomProperty =
           DependencyProperty.RegisterAttached(
               "IsUnderDragCursorBottom",
               typeof(bool),
               typeof(TreeViewItemDragState),
               new UIPropertyMetadata(false));

        public static bool GetIsUnderDragCursorBottom(TreeViewItem item)
        {
            return (bool)item.GetValue(IsUnderDragCursorBottomProperty);
        }

        internal static void SetIsUnderDragCursorBottom(TreeViewItem item, bool value)
        {
            item.SetValue(IsUnderDragCursorBottomProperty, value);
        }

        //-------------------------------------
    }
}
