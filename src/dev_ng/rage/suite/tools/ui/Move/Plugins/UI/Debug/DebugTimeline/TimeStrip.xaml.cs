﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Move.UI
{
    /// <summary>
    /// Interaction logic for TimeStrip.xaml
    /// </summary>
    public partial class TimeStrip : UserControl
    {
        public TimeStrip()
        {
            InitializeComponent();

            Timeline = new WindowedTimeLine();

            Loaded += new RoutedEventHandler(TimeStrip_Loaded);
        }

        void TimeStrip_Loaded(object sender, RoutedEventArgs e)
        {
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext dc)
        {
            Timeline.PixelWidth = RenderSize.Width;

            // Draw the tick marks
            Brush tickBrush = this.Foreground;
            Pen tickPen = new Pen(tickBrush, 1.0);

            double topOfTick = RenderSize.Height - 3;
            double bottomOfTick = RenderSize.Height;

            double topOfMajorTick = RenderSize.Height - 5;
            double bottomOfMajorTick = RenderSize.Height;

            double bottomOfLabel = RenderSize.Height - 5;

            double range = Timeline.DisplayViewRange;
            double tickInterval = Timeline.TickInterval;
            double startTick = Timeline.GetIntervalViewStart(tickInterval);
            double endTick = Timeline.GetIntervalViewEnd(tickInterval);

            double tick = startTick;
            while (tick <= endTick)
            {
                double xPos = Timeline.DisplayToPixel(tick);
                dc.DrawLine(tickPen, new Point(xPos, topOfTick), new Point(xPos, bottomOfTick));
                tick += tickInterval;
            }

            // Now draw the labels
            double labelInterval = Timeline.LabelInterval;

            // pick a good format string for the interval
            string formatString = "{0:0.#}";
            if (labelInterval < 0.01f)
            {
                formatString = "{0:0.000}";
            }
            else if (labelInterval < 0.1f)
            {
                formatString = "{0:0.00}";
            }

            // so now we know we want to draw labels every 'interval' units
            // (we could recompute this just when the size or interval changes too)

            double startLabel = Timeline.GetIntervalViewStart(labelInterval);
            double endLabel = Timeline.GetIntervalViewEnd(labelInterval);

            Brush labelBrush = this.Foreground;

            // Draw the labels

            Typeface typeface = new Typeface(this.FontFamily, this.FontStyle, this.FontWeight, this.FontStretch);

            double label = startLabel;
            while (label <= endLabel + labelInterval) // Draw one extra label, just in case the incremental adds don't work out
            {
                double xPos = Timeline.DisplayToPixel(label);

                string numberStr = String.Format(formatString, label);

                FormattedText text = new FormattedText(numberStr, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, typeface, this.FontSize * 0.8, labelBrush);

                double width = text.Width;

                dc.DrawText(text, new Point(xPos - width * 0.5, bottomOfLabel - text.Height));
                dc.DrawLine(tickPen, new Point(xPos, topOfMajorTick), new Point(xPos, bottomOfMajorTick));
                label += labelInterval;
            }
        }

        public WindowedTimeLine Timeline { get; set; }

    }
}
