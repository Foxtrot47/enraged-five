﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Move
{
    public partial class DiagramOperator : DiagramNode
    {
        public DiagramOperator(IOperator source) : base(source)
        {
            InitializeComponent();
            
        }
    }
}
