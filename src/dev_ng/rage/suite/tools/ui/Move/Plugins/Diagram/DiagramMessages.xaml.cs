﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Move.UI
{
    public partial class DiagramMessages : DiagramNode
    {
        public DiagramMessages(Messages source)
            : base(source)
        {
            InitializeComponent();
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
        }
    }
}
