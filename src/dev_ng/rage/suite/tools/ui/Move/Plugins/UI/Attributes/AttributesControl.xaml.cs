﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AvalonDock;

using Move.Core;
using New.Move.Core;

using Condition = New.Move.Core.Condition;

namespace Move.UI
{
    internal class PropertyCollection : UserControl
    {
        internal PropertyCollection(ILogicPropertyArray property, string name)
        {
            _Property = property;
            _Property.AddHandler(Property_ItemsChanged);
            _Name = name;
            DataContext = this;

            Unloaded += new RoutedEventHandler(PropertyCollection_Unloaded);
        }

        ILogicPropertyArray _Property;
        string _Name;

        Panel PART_Properties;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            TextBlock PART_Label = (TextBlock)GetTemplateChild("PART_Label");
            if (PART_Label != null)
                PART_Label.Text = _Name;

            Button PART_AddButton = (Button)GetTemplateChild("PART_AddButton");
            if (PART_AddButton != null)
            {
                PART_AddButton.Click += new RoutedEventHandler(AddButton_Click);
            }

            PART_Properties = (Panel)GetTemplateChild("PART_Properties");
            if (PART_Properties != null)
            {
                foreach (IProperty property in _Property.Children)
                {
                    if (property is ILogicPropertyPair)
                    {
                        PART_Properties.Children.Add(new PropertyItem_Pair(property, _Name, _Property));
                    }
                    else 
                    {
                        PropertyItem item = AttributesControl._Create[property.GetType()](property, _Name);
                        if (item is PropertyItem_SourceTransform)
                        {
                            (item as PropertyItem_SourceTransform).Collection = _Property;
                        }
                        // removeable so it has a property and can be deleted
                        PART_Properties.Children.Add(item);
                    }
                }
            }
        }

        void AddButton_Click(object sender, RoutedEventArgs e)
        {
            _Property.Add();
        }

        void PropertyCollection_Unloaded(object sender, RoutedEventArgs e)
        {
            _Property.RemoveHandler(Property_ItemsChanged);
        }

        void Property_ItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IProperty property in e.OldItems.OfType<IProperty>())
                {
                    UIElement[] children = new UIElement[PART_Properties.Children.Count];
                    PART_Properties.Children.CopyTo(children, 0);

                    foreach (PropertyItem item in children.OfType<PropertyItem>())
                    {
                        if (item.Property == property)
                        {
                            PART_Properties.Children.Remove(item);
                        }
                    }
                }

                foreach (IAnchor anchor in e.OldItems.OfType<IAnchor>())
                {
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IProperty property in e.NewItems.OfType<IProperty>())
                {
                    if (property is ILogicPropertyPair)
                    {
                        PART_Properties.Children.Add(new PropertyItem_Pair(property, _Name, _Property));
                    }
                    else
                    {
                        PropertyItem item = AttributesControl._Create[property.GetType()](property, _Name);
                        if (item is PropertyItem_SourceTransform)
                        {
                            (item as PropertyItem_SourceTransform).Collection = _Property;
                        }
                        PART_Properties.Children.Add(item);
                    }
                }
            }
        }
    }

    internal abstract class PropertyItem : UserControl
    {
        protected PropertyItem(IProperty property, string name)
        {
            _Property = property;
            _Name = name;
            DataContext = this;
        }

        TextBlock PART_Label { get; set; }

        internal IProperty Property { get { return _Property; } }

        protected IProperty _Property;
        protected string _Name;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_Label = (TextBlock)GetTemplateChild("PART_Label");
            if (PART_Label != null)
            {
                PART_Label.Text = _Name;
            }
        }
    }

    internal class EventItem : UserControl
    {
        internal EventItem(ILogicalEvent logicalEvent, string name)
        {
            _LogicalEvent = logicalEvent;
            _Name = name;

            DataContext = this;
        }

        internal ILogicalEvent _LogicalEvent;
        internal string _Name;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            TextBlock PART_Label = (TextBlock)GetTemplateChild("PART_Label");
            if (PART_Label != null)
            {
                PART_Label.Text = _Name;
            }

            CheckBox PART_Enabled = (CheckBox)GetTemplateChild("PART_Enabled");
            if (PART_Enabled != null)
            {
                PART_Enabled.SetBinding(CheckBox.IsCheckedProperty, new Binding("Enabled") 
                    { Source = _LogicalEvent, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });
            }
        }
    }

    internal class PropertyItem_Pair : PropertyItem
    {
        internal PropertyItem_Pair(IProperty property, string name, ILogicPropertyArray collection)
            : base(property, name)
        {
            Collection = collection;
        }

        ILogicPropertyArray Collection { get; set; }

        public override void OnApplyTemplate()
        {
 	        base.OnApplyTemplate();

            Panel PART_Properties = (Panel)GetTemplateChild("PART_Properties");
            if (PART_Properties != null)
            {
                ILogicPropertyPair property = _Property as ILogicPropertyPair;
                if (property.First is IProperty)
                {
                    PropertyItem item  = AttributesControl._Create[property.First.GetType()](property.First as IProperty, _Name);
                    PART_Properties.Children.Add(item);
                }

                if (property.Second is IProperty)
                {
                    PropertyItem item = AttributesControl._Create[property.Second.GetType()](property.Second as IProperty, _Name);
                    PART_Properties.Children.Add(item);
                }
            }

            Button RemoveButtonPart = (Button)GetTemplateChild("PART_Remove");
            if (RemoveButtonPart != null)
            {
                RemoveButtonPart.Click += new RoutedEventHandler(RemoveButtonPart_Click);
            }
        }

        void RemoveButtonPart_Click(object sender, RoutedEventArgs e)
        {
            Collection.Remove(_Property);
        }
    }

    internal class LogicPropertyItem : PropertyItem
    {
        public static readonly DependencyProperty InputProperty =
            DependencyProperty.Register("Input", typeof(PropertyInput), typeof(LogicPropertyItem));

        internal PropertyInput Input
        {
            get { return (PropertyInput)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        internal LogicPropertyItem(IProperty property, string name)
            : base(property, name)
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Binding input = new Binding("Input");
            input.Source = _Property;
            SetBinding(LogicPropertyItem.InputProperty, input);

            PART_Button = (Button)GetTemplateChild("PART_Button");
            if (PART_Button != null)
            {
                Binding binding = new Binding("Input");
                binding.Source = _Property;
                PART_Button.SetBinding(Button.ContentProperty, binding);

                PART_Button.Click += new RoutedEventHandler(PART_Button_Click);
            }

            PART_Popup = (Popup)GetTemplateChild("PART_Popup");
            if (PART_Popup != null)
            {

            }

            PART_ListBox = (ListBox)GetTemplateChild("PART_ListBox");
            if (PART_ListBox != null)
            {
                PART_ListBox.SelectionChanged += new SelectionChangedEventHandler(PART_ListBox_SelectionChanged);
            }
        }

        void PART_Button_Click(object sender, RoutedEventArgs e)
        {
            PART_ListBox.Items.Clear();

            if (PART_Popup != null)
            {
                foreach (PropertyInput input in Enum.GetValues(typeof(PropertyInput)))
                {
                    Property property = _Property as Property;
                    if ((property.Flags & input) != 0 && property.Input != input)
                    {
                        PART_ListBox.Items.Add(input);
                    }
                }

                PART_Popup.IsOpen = true;
            }
        }

        void PART_ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PART_Popup != null)
            {
                if (e.AddedItems != null && e.AddedItems.Count != 0)
                {
                    (_Property as Property).Input = (PropertyInput)e.AddedItems[0];
                }
                PART_Popup.IsOpen = false;
            }
        }

        protected Button PART_Button { get; private set; }
        protected Popup PART_Popup { get; private set; }
        protected ListBox PART_ListBox { get; private set; }
    }

    internal class PropertyItem_Filename : PropertyItem
    {
        static class Const
        {
            static public string PartName_Filename = "PART_Filename";
            static public string PartName_Button = "PART_Button";
        }

        TextBox _FilenameTextBox;
        Button _Button;

        internal PropertyItem_Filename(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _FilenameTextBox = (TextBox)GetTemplateChild(Const.PartName_Filename);
            if (_FilenameTextBox != null)
            {
                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                _FilenameTextBox.SetBinding(TextBox.TextProperty, binding);
            }

            _Button = (Button)GetTemplateChild(Const.PartName_Button);
            if (_Button != null)
            {
                _Button.Click += new RoutedEventHandler(Button_Click);
            }
        }

        void Button_Click(object sender, RoutedEventArgs e)
        {
            Rage.Move.CommonDialog dialog = new Rage.Move.CommonDialog();
            dialog.InitialDirectory = New.Move.Core.Database.ApplicationFolderPath;
            //dialog.Filter.Add(new FilterEntry(Properties.Resources.Files, Properties.Resources.FileExtension));
            //dialog.Filter.Add(new FilterEntry(Properties.Resources.AllFiles, Properties.Resources.AllExtension));
            //dialog.Title = Properties.Resources.Open;
            dialog.ShowOpen();

            if (!String.IsNullOrEmpty(dialog.Filename))
            {
                _FilenameTextBox.Text = dialog.Filename;
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(_FilenameTextBox, TextBox.TextProperty);
        }
    }

    internal class PropertyItem_Animation : LogicPropertyItem
    {
        internal PropertyItem_Animation(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Animation_Unloaded);
        }

        TextBox PART_FilenameTextBox { get; set; }

        CheckBox PART_Output { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_FilenameTextBox = (TextBox)GetTemplateChild("PART_FilenameTextBox");
           
            Binding binding = new Binding("Filename");
            binding.Source = _Property;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            PART_FilenameTextBox.SetBinding(TextBox.TextProperty, binding);

            PART_Output = (CheckBox)GetTemplateChild("PART_Output");
            if (PART_Output != null)
            {
                Binding output = new Binding("OutputEnabled")
                    { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
                PART_Output.SetBinding(CheckBox.IsCheckedProperty, output);
            }
        }

        void PropertyItem_Animation_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_FilenameTextBox, TextBox.TextProperty);
        }
    }

    internal class PropertyItem_Boolean : LogicPropertyItem
    {
        internal PropertyItem_Boolean(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Boolean_Unloaded);
        }

        CheckBox PART_CheckBox { get; set; }

        CheckBox PART_Output { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_CheckBox = (CheckBox)GetTemplateChild("PART_CheckBox");

            Binding binding = new Binding("Value");
            binding.Source = _Property;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            PART_CheckBox.SetBinding(CheckBox.IsCheckedProperty, binding);

            PART_Output = (CheckBox)GetTemplateChild("PART_Output");
            if (PART_Output != null)
            {
                Binding output = new Binding("OutputEnabled") 
                    { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
                PART_Output.SetBinding(CheckBox.IsCheckedProperty, output);
            }
        }

        void PropertyItem_Boolean_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_CheckBox, CheckBox.IsCheckedProperty);
        }
    }

    internal class PropertyItem_Clip : LogicPropertyItem
    {
        internal PropertyItem_Clip(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Clip_Unloaded);
        }

        TextBox PART_FilenameTextBox { get; set; }

        CheckBox PART_Output { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_FilenameTextBox = (TextBox)GetTemplateChild("PART_FilenameTextBox");

            Binding binding = new Binding("Filename");
            binding.Source = _Property;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            PART_FilenameTextBox.SetBinding(TextBox.TextProperty, binding);

            PART_Output = (CheckBox)GetTemplateChild("PART_Output");
            if (PART_Output != null)
            {
                Binding output = new Binding("OutputEnabled") 
                    { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
                PART_Output.SetBinding(CheckBox.IsCheckedProperty, output);
            }
        }

        void PropertyItem_Clip_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_FilenameTextBox, TextBox.TextProperty);
        }
    }

    internal class PropertyItem_Expressions : LogicPropertyItem
    {
        internal PropertyItem_Expressions(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Expressions_Unloaded);
        }

        TextBox PART_FilenameTextBox { get; set; }

        CheckBox PART_Output { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_FilenameTextBox = (TextBox)GetTemplateChild("PART_FilenameTextBox");

            Binding binding = new Binding("Filename");
            binding.Source = _Property;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            PART_FilenameTextBox.SetBinding(TextBox.TextProperty, binding);

            PART_Output = (CheckBox)GetTemplateChild("PART_Output");
            if (PART_Output != null)
            {
                Binding output = new Binding("OutputEnabled")
                    { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
                PART_Output.SetBinding(CheckBox.IsCheckedProperty, output);
            }
        }

        void PropertyItem_Expressions_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_FilenameTextBox, TextBox.TextProperty);
        }
    }

    internal class PropertyItem_Filter : LogicPropertyItem
    {
        internal PropertyItem_Filter(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(
                delegate(object sender, RoutedEventArgs e)
                {
                    BindingOperations.ClearBinding(RootNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(RightFootNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(LeftFootNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(RightKneeNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(LeftKneeNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(RightHipNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(LeftHipNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(PelvisNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(SpineNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(NeckNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(HeadNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(FaceNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(RightHandNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(LeftHandNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(RightElbowNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(LeftElbowNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(RightShoulderNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(LeftShoulderNumericUpDown, NumericUpDown.ValueProperty);
                    BindingOperations.ClearBinding(MoverNumericUpDown, NumericUpDown.ValueProperty);
                }
            );
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            FilterProperty property = (_Property as FilterLogicalProperty).Value;

            RootNumericUpDown = (NumericUpDown)GetTemplateChild("PART_RootNumericUpDown");
            RootNumericUpDown.SetBinding(NumericUpDown.ValueProperty, 
                new Binding("Root") { Source = property, Mode = BindingMode.TwoWay });
            RightFootNumericUpDown = (NumericUpDown)GetTemplateChild("PART_RightFootNumericUpDown");
            RightFootNumericUpDown.SetBinding(NumericUpDown.ValueProperty, 
                new Binding("RightFoot") { Source = property, Mode = BindingMode.TwoWay });
            LeftFootNumericUpDown = (NumericUpDown)GetTemplateChild("PART_LeftFootNumericUpDown");
            LeftFootNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("LeftFoot") { Source = property, Mode = BindingMode.TwoWay });
            RightKneeNumericUpDown = (NumericUpDown)GetTemplateChild("PART_RightKneeNumericUpDown");
            RightKneeNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("RightKnee") { Source = property, Mode = BindingMode.TwoWay });
            LeftKneeNumericUpDown = (NumericUpDown)GetTemplateChild("PART_LeftKneeNumericUpDown");
            LeftKneeNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("LeftKnee") { Source = property, Mode = BindingMode.TwoWay });
            RightHipNumericUpDown = (NumericUpDown)GetTemplateChild("PART_RightHipNumericUpDown");
            RightHipNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("RightHip") { Source = property, Mode = BindingMode.TwoWay });
            LeftHipNumericUpDown = (NumericUpDown)GetTemplateChild("PART_LeftHipNumericUpDown");
            LeftHipNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("LeftHip") { Source = property, Mode = BindingMode.TwoWay });
            PelvisNumericUpDown = (NumericUpDown)GetTemplateChild("PART_PelvisNumericUpDown");
            PelvisNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("Pelvis") { Source = property, Mode = BindingMode.TwoWay });
            SpineNumericUpDown = (NumericUpDown)GetTemplateChild("PART_SpineNumericUpDown");
            SpineNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("Spine") { Source = property, Mode = BindingMode.TwoWay });
            NeckNumericUpDown = (NumericUpDown)GetTemplateChild("PART_NeckNumericUpDown");
            NeckNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("Neck") { Source = property, Mode = BindingMode.TwoWay });
            HeadNumericUpDown = (NumericUpDown)GetTemplateChild("PART_HeadNumericUpDown");
            HeadNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("Head") { Source = property, Mode = BindingMode.TwoWay });
            FaceNumericUpDown = (NumericUpDown)GetTemplateChild("PART_FaceNumericUpDown");
            FaceNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("Face") { Source = property, Mode = BindingMode.TwoWay });
            RightHandNumericUpDown = (NumericUpDown)GetTemplateChild("PART_RightHandNumericUpDown");
            RightHandNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("RightHand") { Source = property, Mode = BindingMode.TwoWay });
            LeftHandNumericUpDown = (NumericUpDown)GetTemplateChild("PART_LeftHandNumericUpDown");
            LeftHandNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("LeftHand") { Source = property, Mode = BindingMode.TwoWay });
            RightElbowNumericUpDown = (NumericUpDown)GetTemplateChild("PART_RightElbowNumericUpDown");
            RightElbowNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("RightElbow") { Source = property, Mode = BindingMode.TwoWay });
            LeftElbowNumericUpDown = (NumericUpDown)GetTemplateChild("PART_LeftElbowNumericUpDown");
            LeftElbowNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("LeftElbow") { Source = property, Mode = BindingMode.TwoWay });
            RightShoulderNumericUpDown = (NumericUpDown)GetTemplateChild("PART_RightShoulderNumericUpDown");
            RightShoulderNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("RightShoulder") { Source = property, Mode = BindingMode.TwoWay });
            LeftShoulderNumericUpDown = (NumericUpDown)GetTemplateChild("PART_LeftShoulderNumericUpDown");
            LeftShoulderNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("LeftShoulder") { Source = property, Mode = BindingMode.TwoWay });
            MoverNumericUpDown = (NumericUpDown)GetTemplateChild("PART_MoverNumericUpDown");
            MoverNumericUpDown.SetBinding(NumericUpDown.ValueProperty,
                new Binding("Mover") { Source = property, Mode = BindingMode.TwoWay });

            PART_Output = (CheckBox)GetTemplateChild("PART_Output");
            if (PART_Output != null)
            {
                Binding output = new Binding("OutputEnabled") 
                    { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
                PART_Output.SetBinding(CheckBox.IsCheckedProperty, output);
            }
        }

        CheckBox PART_Output { get; set; }

        NumericUpDown RootNumericUpDown { get; set; }
        NumericUpDown RightFootNumericUpDown { get; set; }
        NumericUpDown LeftFootNumericUpDown { get; set; }
        NumericUpDown RightKneeNumericUpDown { get; set; }
        NumericUpDown LeftKneeNumericUpDown { get; set; }
        NumericUpDown RightHipNumericUpDown { get; set; }
        NumericUpDown LeftHipNumericUpDown { get; set; }
        NumericUpDown PelvisNumericUpDown { get; set; }
        NumericUpDown SpineNumericUpDown { get; set; }
        NumericUpDown NeckNumericUpDown { get; set; }
        NumericUpDown HeadNumericUpDown { get; set; }
        NumericUpDown FaceNumericUpDown { get; set; }
        NumericUpDown RightHandNumericUpDown { get; set; }
        NumericUpDown LeftHandNumericUpDown { get; set; }
        NumericUpDown RightElbowNumericUpDown { get; set; }
        NumericUpDown LeftElbowNumericUpDown { get; set; }
        NumericUpDown RightShoulderNumericUpDown { get; set; }
        NumericUpDown LeftShoulderNumericUpDown { get; set; }
        NumericUpDown MoverNumericUpDown { get; set; }
    }

    internal class PropertyItem_Frame : LogicPropertyItem
    {
        internal PropertyItem_Frame(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(OnUnloaded);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            CheckBox OutputPart = (CheckBox)GetTemplateChild("PART_Output");
            if (OutputPart != null)
            {
                Binding output = new Binding("OutputEnabled") { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
                OutputPart.SetBinding(CheckBox.IsCheckedProperty, output);
            }
        }

        void OnUnloaded(object sender, RoutedEventArgs e)
        {
            CheckBox OutputPart = (CheckBox)GetTemplateChild("PART_Output");
            if (OutputPart != null)
            {
                BindingOperations.ClearBinding(OutputPart, CheckBox.IsCheckedProperty);
            }
        }
    }

    internal class PropertyItem_ParameterizedMotion : LogicPropertyItem
    {
        internal PropertyItem_ParameterizedMotion(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_ParameterizedMotion_Unloaded);
        }

        TextBox PART_FilenameTextBox { get; set; }

        CheckBox PART_Output { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_FilenameTextBox = (TextBox)GetTemplateChild("PART_FilenameTextBox");

            Binding binding = new Binding("Filename");
            binding.Source = _Property;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            PART_FilenameTextBox.SetBinding(TextBox.TextProperty, binding);

            PART_Output = (CheckBox)GetTemplateChild("PART_Output");
            if (PART_Output != null)
            {
                Binding output = new Binding("OutputEnabled")
                    { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
                PART_Output.SetBinding(CheckBox.IsCheckedProperty, output);
            }
        }

        void PropertyItem_ParameterizedMotion_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_FilenameTextBox, TextBox.TextProperty);
        }
    }

    internal class PropertyItem_Real : LogicPropertyItem
    {
        internal PropertyItem_Real(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Real_Unloaded);            
        }

        NumericUpDown PART_NumericUpDown { get; set; }

        CheckBox PART_Output { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_NumericUpDown = (NumericUpDown)GetTemplateChild("PART_NumericUpDown");
            if (PART_NumericUpDown != null)
            {
                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                PART_NumericUpDown.SetBinding(NumericUpDown.ValueProperty, binding);
            }

            PART_Output = (CheckBox)GetTemplateChild("PART_Output");
            if (PART_Output != null)
            {
                Binding output = new Binding("OutputEnabled") 
                    { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
                PART_Output.SetBinding(CheckBox.IsCheckedProperty, output);
            }
        }
        
        void PropertyItem_Real_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_NumericUpDown, NumericUpDown.ValueProperty);
        }
    }

    internal class PropertyItem_SourceTransform : PropertyItem
    {
        internal PropertyItem_SourceTransform(IProperty property, string name)
            : base(property, name)
        {
        }

        internal ILogicPropertyArray Collection { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Button RemoveButtonPart = (Button)GetTemplateChild("PART_Remove");
            if (RemoveButtonPart != null)
            {
                RemoveButtonPart.Click += new RoutedEventHandler(RemoveButtonPart_Click);
            }
        }

        void RemoveButtonPart_Click(object sender, RoutedEventArgs e)
        {
            Collection.Remove(_Property);
        }
    }

    internal class TransitionPropertyItem_Modifier : PropertyItem
    {
        internal TransitionPropertyItem_Modifier(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        ComboBox PART_ComboBox { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_ComboBox = (ComboBox)GetTemplateChild("PART_ComboBox");
            if (PART_ComboBox != null)
            {
                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                PART_ComboBox.SetBinding(ComboBox.SelectedItemProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_ComboBox, ComboBox.SelectedItemProperty);
        }
    }

    internal class TransitionPropertyItem_Boolean : UserControl
    {
        internal TransitionPropertyItem_Boolean(object obj, string name, string label)
        {
            _Object = obj;
            _Name = name;
            _Label = label;

            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        object _Object;
        string _Name;
        string _Label;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            TextBlock LabelPart = (TextBlock)GetTemplateChild("PART_Label");
            if (LabelPart != null)
            {
                LabelPart.Text = _Label;
            }

            CheckBox CheckBoxPart = (CheckBox)GetTemplateChild("PART_CheckBox");
            if (CheckBoxPart != null)
            {
                Binding binding = new Binding(_Name) 
                    { Source = _Object, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, Mode = BindingMode.TwoWay };
                CheckBoxPart.SetBinding(CheckBox.IsCheckedProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            CheckBox CheckBoxPart = (CheckBox)GetTemplateChild("PART_CheckBox");
            if (CheckBoxPart != null)
            {
                BindingOperations.ClearBinding(CheckBoxPart, CheckBox.IsCheckedProperty);
            }
        }
    }

    internal class TransitionPropertyItem_Real : PropertyItem
    {
        internal TransitionPropertyItem_Real(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        NumericUpDown PART_NumericUpDown { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_NumericUpDown = (NumericUpDown)GetTemplateChild("PART_NumericUpDown");
            if (PART_NumericUpDown != null)
            {
                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                PART_NumericUpDown.SetBinding(NumericUpDown.ValueProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_NumericUpDown, NumericUpDown.ValueProperty);
        }
    }

    internal class ConditionPropertyItem_Real : PropertyItem
    {
        internal ConditionPropertyItem_Real(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        NumericUpDown PART_NumericUpDown { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_NumericUpDown = (NumericUpDown)GetTemplateChild("PART_NumericUpDown");
            if (PART_NumericUpDown != null)
            {
                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                PART_NumericUpDown.SetBinding(NumericUpDown.ValueProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_NumericUpDown, NumericUpDown.ValueProperty);
        }
    }

    internal class ConditionPropertyItem_Flag : PropertyItem
    {
        internal ConditionPropertyItem_Flag(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        ComboBox PART_ComboBox { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_ComboBox = (ComboBox)GetTemplateChild("PART_ComboBox");
            if (PART_ComboBox != null)
            {
                IConditionProperty property = _Property as IConditionProperty;
                Condition condition = property.Parent;
                Transition transition = condition.Parent;
                IAutomaton automaton = transition.Parent;

                PART_ComboBox.ItemsSource = automaton.Database.Flags;
                PART_ComboBox.DisplayMemberPath = "Name";

                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                PART_ComboBox.SetBinding(ComboBox.SelectedItemProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_ComboBox, ComboBox.SelectedItemProperty);
        }
    }

    internal class ConditionPropertyItem_Request : PropertyItem
    {
        internal ConditionPropertyItem_Request(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        ComboBox PART_ComboBox { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_ComboBox = (ComboBox)GetTemplateChild("PART_ComboBox");
            if (PART_ComboBox != null)
            {
                IConditionProperty property = _Property as IConditionProperty;
                Condition condition = property.Parent;
                Transition transition = condition.Parent;
                IAutomaton automaton = transition.Parent;

                PART_ComboBox.ItemsSource = automaton.Database.Requests;
                PART_ComboBox.DisplayMemberPath = "Name";

                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                PART_ComboBox.SetBinding(ComboBox.SelectedItemProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_ComboBox, ComboBox.SelectedItemProperty);
        }
    }

    internal class ConditionPropertyItem_Signal : PropertyItem
    {
        internal ConditionPropertyItem_Signal(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        ComboBox PART_ComboBox { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_ComboBox = (ComboBox)GetTemplateChild("PART_ComboBox");
            if (PART_ComboBox != null)
            {
                IConditionProperty property = _Property as IConditionProperty;
                Condition condition = property.Parent;
                Transition transition = condition.Parent;
                IAutomaton automaton = transition.Parent;

                PART_ComboBox.ItemsSource = automaton.Database.Signals;
                PART_ComboBox.DisplayMemberPath = "Name";

                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                PART_ComboBox.SetBinding(ComboBox.SelectedItemProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_ComboBox, ComboBox.SelectedItemProperty);
        }
    }

    internal class ConditionPropertyItem_Event : PropertyItem 
    {
        internal ConditionPropertyItem_Event(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            ComboBox PART_EventCombo = (ComboBox)GetTemplateChild("PART_EventCombo");
            if (PART_EventCombo != null)
            {
                IConditionProperty property = _Property as IConditionProperty;
                Condition condition = property.Parent;
                Transition transition = condition.Parent;
                IAutomaton automaton = transition.Parent;

                PART_EventCombo.ItemsSource = automaton.Database.Events;
                PART_EventCombo.DisplayMemberPath = "Name";

                Binding binding = new Binding("Event") 
                    { Source = _Property, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, Mode = BindingMode.TwoWay };
                PART_EventCombo.SetBinding(ComboBox.SelectedItemProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            ComboBox PART_EventCombo = (ComboBox)GetTemplateChild("PART_EventCombo");
            if (PART_EventCombo != null)
            {
                BindingOperations.ClearBinding(PART_EventCombo, ComboBox.SelectedItemProperty);
            }
        }
    }

    internal class ConditionPropertyItem_Boolean : PropertyItem
    {
        internal ConditionPropertyItem_Boolean(IProperty property, string name)
            : base(property, name)
        {
            Unloaded += new RoutedEventHandler(PropertyItem_Unloaded);
        }

        CheckBox PART_CheckBox { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_CheckBox = (CheckBox)GetTemplateChild("PART_CheckBox");
            if (PART_CheckBox != null)
            {
                Binding binding = new Binding("Value");
                binding.Source = _Property;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                binding.Mode = BindingMode.TwoWay;
                PART_CheckBox.SetBinding(CheckBox.IsCheckedProperty, binding);
            }
        }

        void PropertyItem_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingOperations.ClearBinding(PART_CheckBox, CheckBox.IsCheckedProperty);
        }
    }

    internal class ConditionItem : UserControl
    {
        internal ConditionItem(Condition condition)
        {
            Condition = condition;
        }

        internal Condition Condition { get; private set; }

        ComboBox PART_Condition { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Button PART_Remove = (Button)GetTemplateChild("PART_Remove");
            if (PART_Remove != null)
            {
                PART_Remove.Click += new RoutedEventHandler(Remove_Click);
            }

            PART_Condition = (ComboBox)GetTemplateChild("PART_Condition");
            if (PART_Condition != null)
            {
                PART_Condition.ItemsSource = Rage.Move.App.Instance.Runtime.RegisteredConditions;
                PART_Condition.DisplayMemberPath = "Name";

                Binding binding = new Binding("Descriptor");
                binding.Source = Condition;
                binding.Mode = BindingMode.TwoWay;
                PART_Condition.SetBinding(ComboBox.SelectedItemProperty, binding);

                PART_Condition.SelectionChanged += 
                    new SelectionChangedEventHandler(Condition_SelectionChanged);

                UpdateProperties();
            }

            CheckBox FlagWhenPassed = (CheckBox)GetTemplateChild("PART_FlagWhenPassed");
            if (FlagWhenPassed != null)
            {
                Binding binding = new Binding("FlagWhenPassed") 
                    { Source = Condition, Mode = BindingMode.TwoWay };
                FlagWhenPassed.SetBinding(CheckBox.IsCheckedProperty, binding);
            }
        }

        void Remove_Click(object sender, RoutedEventArgs e)
        {
            Condition.Dispose();
        }

        void Condition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateProperties();
        }

        void UpdateProperties()
        {
            StackPanel PART_Properties = (StackPanel)GetTemplateChild("PART_Properties");
            if (PART_Properties != null)
            {
                PART_Properties.Children.Clear();
                
                PropertyInfo[] properties = Condition.Content.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo pi in properties)
                {
                    if (pi != null && pi.PropertyType.GetInterface("IProperty") != null)
                    {
                        IProperty property = (IProperty)pi.GetValue(Condition.Content, null);
                        PropertyItem item = _CreateConditionProperty[property.GetType()](property, pi.Name);
                        PART_Properties.Children.Add(item);
                    }
                }
            }
        }

        delegate PropertyItem Delegate(IProperty property, string name);

        static Dictionary<Type, Delegate> _CreateConditionProperty = new Dictionary<Type, Delegate>()
        {
            { typeof(BooleanConditionProperty), delegate(IProperty property, string name)
                {
                    return new ConditionPropertyItem_Boolean(property, name);
                }
            },
            { typeof(RealConditionProperty), delegate(IProperty property, string name)
                {
                    return new ConditionPropertyItem_Real(property, name);
                }
            },
            { typeof(SignalConditionProperty), delegate(IProperty property, string name)
                {
                    return new ConditionPropertyItem_Signal(property, name);
                }
            },
            { typeof(EventConditionProperty), delegate(IProperty property, string name)
                {
                    return new ConditionPropertyItem_Event(property, name);
                }
            },
            { typeof(FlagConditionProperty), delegate(IProperty property, string name)
                {
                    return new ConditionPropertyItem_Flag(property, name);
                }
            },
            { typeof(RequestConditionProperty), delegate(IProperty property, string name)
                {
                    return new ConditionPropertyItem_Request(property, name);
                }
            },
        };
    }

    public class UniqueNetworkNameValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            return new ValidationResult(false, "this is a test");
        }
    }

    public partial class AttributesControl : UserControl
    {
        public AttributesControl(Rage.Move.MainWindow wnd)
        {
            InitializeComponent();

            wnd.ActiveDocumentChanging += new EventHandler(Window_ActiveDocumentChanging);
            wnd.ActiveDocumentChanged += new EventHandler(Window_ActiveDocumentChanged);
        }

        void Clear()
        {
            TypeBox.SelectionChanged -= new SelectionChangedEventHandler(TypeBox_SelectionChanged);

            BindingOperations.ClearBinding(NameBox, TextBox.TextProperty);
            BindingOperations.ClearBinding(TypeBox, ComboBox.SelectedItemProperty);

            ConditionsExpander.Visibility = Visibility.Collapsed;
            Conditions.Children.Clear();

            Properties.Children.Clear();

            EventPanel.Children.Clear();
            EventExpander.Visibility = Visibility.Collapsed;

            TransitionsExpander.Visibility = Visibility.Collapsed;
            TransitionsListBox.ItemsSource = null;

            BindingOperations.ClearBinding(NameBox, TextBox.TextProperty);
            NameBox.IsEnabled = false;

            TypeBox.ItemsSource = null;
            TypeBox.Items.Clear();
            TypeBox.IsEnabled = false;

            ConditionsExpander.Visibility = Visibility.Collapsed;
            Conditions.Children.Clear();
        }

        void Window_ActiveDocumentChanging(object sender, EventArgs e)
        {
            Clear();

            if (sender is DocumentContent)
            {
                DocumentContent doc = sender as DocumentContent;
                Database db = doc.Tag as Database;

                db.SelectedItems.CollectionChanged -=
                    new NotifyCollectionChangedEventHandler(SelectedItems_CollectionChanged);

                db.ActiveChanging -= new RoutedEventHandler(Database_ActiveChanging);
                db.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);

                db.Closing -= new EventHandler(Database_Closing);

                Database_ActiveChanging(db.Active);
            }
        }

        void Window_ActiveDocumentChanged(object sender, EventArgs e)
        {
            Clear();

            if (sender is DocumentContent)
            {
                DocumentContent doc = sender as DocumentContent;
                Database db = doc.Tag as Database;

                db.SelectedItems.CollectionChanged +=
                    new NotifyCollectionChangedEventHandler(SelectedItems_CollectionChanged);

                db.Closing += new EventHandler(Database_Closing);
                db.ActiveChanging += new RoutedEventHandler(Database_ActiveChanging);
                db.ActiveChanged += new RoutedEventHandler(Database_ActiveChanged);
                Database_ActiveChanged(db.Active);

                if (db.SelectedItems.Count == 1)
                {
                    foreach (ISelectable selected in db.SelectedItems)
                    {
                        if (selected is ITransitional)
                        {
                            UpdateTransitional(selected as ITransitional);
                        }
                        else if (selected is ILogic)
                        {
                            UpdateLogic(selected as ILogic);
                        }
                        else if (selected is Transition)
                        {
                            UpdateTransition(selected as Transition);
                        }
                    }
                }
            }
        }

        void Database_ActiveChanging(ITransitional active)
        {
            if (active.IsAutomaton)
            {
                IAutomaton automaton = active as IAutomaton;
                automaton.Transitions.CollectionChanged -= new NotifyCollectionChangedEventHandler(Transitions_CollectionChanged);
                foreach (Transition transition in automaton.Transitions)
                {
                    transition.IsSelectedChanged -=
                        new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);
                }
            }

            active.RemoveHandler(new NotifyCollectionChangedEventHandler(Active_CollectionChanged));

            foreach (ITransitional child in active.Children.OfType<ITransitional>())
            {
                child.IsSelectedChanged -= new EventHandler<IsSelectedChangedEventArgs>(Transitional_IsSelectedChanged);
            }

            foreach (ILogic child in active.Children.OfType<ILogic>())
            {
                child.IsSelectedChanged -= new EventHandler<IsSelectedChangedEventArgs>(Logic_IsSelectedChanged);
            }
        }

        void Database_ActiveChanged(ITransitional active)
        {
            if (active.IsAutomaton)
            {
                IAutomaton automaton = active as IAutomaton;
                automaton.Transitions.CollectionChanged += new NotifyCollectionChangedEventHandler(Transitions_CollectionChanged);
                foreach (Transition transition in automaton.Transitions)
                {
                    transition.IsSelectedChanged +=
                        new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);
                }
            }

            active.AddHandler(new NotifyCollectionChangedEventHandler(Active_CollectionChanged));

            foreach (ITransitional child in active.Children.OfType<ITransitional>())
            {
                child.IsSelectedChanged += new EventHandler<IsSelectedChangedEventArgs>(Transitional_IsSelectedChanged);
            }

            foreach (ILogic child in active.Children.OfType<ILogic>())
            {
                child.IsSelectedChanged += new EventHandler<IsSelectedChangedEventArgs>(Logic_IsSelectedChanged);
            }
        }

        void Database_Closing(object sender, EventArgs e)
        {
            Database database = sender as Database;
            database.ActiveChanging -= new RoutedEventHandler(Database_ActiveChanging);
            database.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);
        }

        void Database_ActiveChanging(object sender, RoutedEventArgs e)
        {
            Database_ActiveChanging(sender as ITransitional);
        }

        void Database_ActiveChanged(object sender, RoutedEventArgs e)
        {
            Database_ActiveChanged(sender as ITransitional);
        }

        void Transitions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Transition transition in e.NewItems)
                    {
                        transition.IsSelectedChanged +=
                            new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Transition transition in e.OldItems)
                    {
                        ConditionsExpander.Visibility = Visibility.Collapsed;
                        Conditions.Children.Clear();

                        transition.IsSelectedChanged -=
                            new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);
                    }
                    break;
            }
        }

        void Active_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ITransitional transitional in e.NewItems.OfType<ITransitional>())
                    {
                        transitional.IsSelectedChanged += new EventHandler<IsSelectedChangedEventArgs>(Transitional_IsSelectedChanged);
                    }
                    foreach (ILogic logic in e.NewItems.OfType<ILogic>())
                    {
                        logic.IsSelectedChanged += new EventHandler<IsSelectedChangedEventArgs>(Logic_IsSelectedChanged);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (ITransitional transitional in e.OldItems.OfType<ITransitional>())
                    {
                        transitional.IsSelectedChanged -= new EventHandler<IsSelectedChangedEventArgs>(Transitional_IsSelectedChanged);
                    }
                    foreach (ILogic logic in e.OldItems.OfType<ILogic>())
                    {
                        logic.IsSelectedChanged -= new EventHandler<IsSelectedChangedEventArgs>(Logic_IsSelectedChanged);
                    }
                    break;
            }
        }

        void UpdateProperties(ISelectable selected)
        {
            Binding nameBinding = new Binding("Name");
            nameBinding.Source = selected;
            nameBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            NameBox.SetBinding(TextBox.TextProperty, nameBinding);

            PropertyInfo[] properties = selected.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                if (pi != null && pi.PropertyType.GetInterface("ILogicPropertyArray") != null)
                {
                    ILogicPropertyArray property = (ILogicPropertyArray)pi.GetValue(selected, null);
                    PropertyCollection item = new PropertyCollection(property, pi.Name);
                    Properties.Children.Add(item);
                }
                else if (pi != null && pi.PropertyType.GetInterface("IProperty") != null)
                {
                    IProperty property = (IProperty)pi.GetValue(selected, null);
                    PropertyItem item = _Create[property.GetType()](property, pi.Name);
                    Properties.Children.Add(item);
                }
            }
        }

        void UpdateEvents(ISelectable selected)
        {
            EventPanel.Children.Clear();

            PropertyInfo[] properties = selected.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                if (pi != null && pi.PropertyType.GetInterface("ILogicalEvent") != null)
                {
                    ILogicalEvent le = (ILogicalEvent)pi.GetValue(selected, null);
                    EventItem item = new EventItem(le, pi.Name);
                    EventPanel.Children.Add(item);
                    EventExpander.Visibility = Visibility.Visible;
                }
            }
        }

        void UpdateName(ISelectable selected)
        {
            NameBox.IsEnabled = true;
            TypeBox.IsEnabled = true;

            TypeBox.Items.Add(selected);
            TypeBox.DisplayMemberPath = "Name";
            TypeBox.SelectedIndex = 0;
        }

        void ClearName()
        {
            BindingOperations.ClearBinding(NameBox, TextBox.TextProperty);
            NameBox.IsEnabled = false;
            TypeBox.IsEnabled = false;
        }

        void TransitionOrder_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            TransitionsExpander.Visibility = (sender as ObservableCollection<Transition>).Count != 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        void UpdateTransitional(ITransitional transitional)
        {
            UpdateProperties(transitional);
            UpdateName(transitional);

            TransitionsListBox.ItemsSource = transitional.TransitionOrder;

            if (transitional.TransitionOrder.Count != 0)
            {
                TransitionsExpander.Visibility = Visibility.Visible;
            }

            transitional.TransitionOrder.CollectionChanged +=
                new NotifyCollectionChangedEventHandler(TransitionOrder_CollectionChanged);
        }

        void Transitional_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            ITransitional transitional = sender as ITransitional;
            switch (e.Action)
            {
                case SelectionChangedAction.Add:
                    Properties.Children.Clear();
                    if (e.Count == 1)
                    {
                        UpdateTransitional(transitional);
                    }
                    break;
                case SelectionChangedAction.Remove:
                    ClearName();
                    TypeBox.Items.Clear();

                    Properties.Children.Clear();

                    TransitionsExpander.Visibility = Visibility.Collapsed;
                    TransitionsListBox.ItemsSource = null;

                    transitional.TransitionOrder.CollectionChanged -=
                        new NotifyCollectionChangedEventHandler(TransitionOrder_CollectionChanged);

                    break;
            }
        }

        void UpdateLogic(ILogic logic)
        {
            UpdateProperties(logic);
            UpdateEvents(logic);
            UpdateName(logic);
        }

        void Logic_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            ILogic logic = sender as ILogic;
            switch (e.Action)
            {
                case SelectionChangedAction.Add:
                    Properties.Children.Clear();
                    if (e.Count == 1)
                    {
                        UpdateLogic(logic);
                    }
                    break;
                case SelectionChangedAction.Remove:
                    ClearName();
                    TypeBox.Items.Clear();

                    Properties.Children.Clear();
                    EventPanel.Children.Clear();
                    EventExpander.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        void TypeBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateTranstionProperties(TypeBox.Tag as Transition);
        }

        void UpdateTranstionProperties(Transition transition)
        {
            Properties.Children.Clear();

            TransitionPropertyItem_Boolean reevaluate = new TransitionPropertyItem_Boolean(transition, "ReEvaluate", "Re-evaluate on Success");
            Properties.Children.Add(reevaluate);

            PropertyInfo[] properties = transition.Content.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                if (pi != null && pi.PropertyType.GetInterface("IProperty") != null)
                {
                    IProperty property = (IProperty)pi.GetValue(transition.Content, null);
                    PropertyItem item = _Create[property.GetType()](property, pi.Name);
                    Properties.Children.Add(item);
                }
            }
        }

        void UpdateTransition(Transition transition)
        {
            Binding nameBinding = new Binding("Name");
            nameBinding.Source = transition;
            nameBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            NameBox.SetBinding(TextBox.TextProperty, nameBinding);

            foreach (Condition condition in transition.Conditions)
            {
                Conditions.Children.Add(new ConditionItem(condition));
            }

            transition.Conditions.CollectionChanged +=
                new NotifyCollectionChangedEventHandler(Conditions_CollectionChanged);

            TypeBox.Items.Clear();
            TypeBox.ItemsSource = Rage.Move.App.Instance.Runtime.TransitionDescriptors;
            TypeBox.DisplayMemberPath = "Name";
            TypeBox.Tag = transition;

            Binding binding = new Binding("Descriptor");
            binding.Source = transition;
            binding.Mode = BindingMode.TwoWay;
            TypeBox.SetBinding(ComboBox.SelectedItemProperty, binding);

            UpdateTranstionProperties(transition);

            NameBox.IsEnabled = true;
            TypeBox.IsEnabled = true;

            ConditionsExpander.Visibility = Visibility.Visible;

            TypeBox.SelectionChanged += new SelectionChangedEventHandler(TypeBox_SelectionChanged);
        }

        void Transition_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            Transition transition = sender as Transition;
            switch (e.Action)
            {
                case SelectionChangedAction.Add:
                    Properties.Children.Clear();
                    if (e.Count == 1)
                    {
                        UpdateTransition(transition);
                    }
                    break;
                case SelectionChangedAction.Remove:
                    TypeBox.SelectionChanged -= new SelectionChangedEventHandler(TypeBox_SelectionChanged);

                    BindingOperations.ClearBinding(NameBox, TextBox.TextProperty);
                    BindingOperations.ClearBinding(TypeBox, ComboBox.SelectedItemProperty);

                    Properties.Children.Clear();

                    TypeBox.ItemsSource = null;

                    transition.Conditions.CollectionChanged -=
                        new NotifyCollectionChangedEventHandler(Conditions_CollectionChanged);

                    ClearName();

                    Conditions.Children.Clear();
                    ConditionsExpander.Visibility = Visibility.Collapsed;

                    break;
            }
        }

        void SelectedItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Reset)
                Clear();
        }
 
        void Conditions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Condition condition in e.NewItems)
                    {
                        Conditions.Children.Add(new ConditionItem(condition));
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Condition condition in e.OldItems)
                    {
                        ConditionItem[] items = new ConditionItem[Conditions.Children.Count]; 
                        Conditions.Children.CopyTo(items, 0);

                        foreach (ConditionItem item in items)
                        {
                            if (item.Condition == condition)
                                Conditions.Children.Remove(item);
                        }
                    }
                    break;
            }
        }

        void CreateConditionButton_Click(object sender, RoutedEventArgs e)
        {
            Transition transition = TypeBox.Tag as Transition;
            if (Rage.Move.App.Instance.Runtime.RegisteredConditions.Count != 0)
            {
                IConditionDesc descriptor = Rage.Move.App.Instance.Runtime.RegisteredConditions[0];
                transition.Conditions.Add(new Condition(descriptor, transition));
            }


            // TODO: should just jam the transition in the Tag and fetch it here instead!
            /*
            if (_Database.SelectedItems.Count() == 1)
            {
                ISelectable selected = _Database.SelectedItems[0];
                if (selected is Rage.Move.Core.Dg.Transition)
                {
                    Rage.Move.Core.Dg.Transition transition = selected as Rage.Move.Core.Dg.Transition;
                    if (Rage.Move.App.Instance.Runtime.RegisteredConditions.Count != 0)
                    {
                        Rage.Move.Core.IRegisterCondition_Ex descriptor = Rage.Move.App.Instance.Runtime.RegisteredConditions[0];
                        transition.Conditions.Add(new Rage.Move.Core.Dg.Condition(descriptor, transition));
                    }
                }
            }
             */
        }

        void TransitionUp_Click(object sender, RoutedEventArgs e)
        {
            if (TransitionsListBox.SelectedIndex < 0)
                return;

            if (TransitionsListBox.SelectedIndex != 0)
            {
                ((ObservableCollection<Transition>)
                    TransitionsListBox.ItemsSource).Move(TransitionsListBox.SelectedIndex, TransitionsListBox.SelectedIndex-1);
            }
        }

        void TransitionDown_Click(object semder, RoutedEventArgs e)
        {
            if (TransitionsListBox.SelectedIndex < 0)
                return;

            ObservableCollection<Transition> transitions = 
                ((ObservableCollection<Transition>)TransitionsListBox.ItemsSource);
            if (TransitionsListBox.SelectedIndex < transitions.Count-1)
            {
                transitions.Move(TransitionsListBox.SelectedIndex, TransitionsListBox.SelectedIndex + 1);
            }
        }

        internal delegate PropertyItem Delegate(IProperty property, string name);

        internal static Dictionary<Type, Delegate> _Create = new Dictionary<Type, Delegate>()
        {
            { typeof(FilenameLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_Filename(property, name);
                }
            },
            { typeof(AnimationLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_Animation(property, name);
                }
            },
            { typeof(BooleanLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_Boolean(property, name);
                }
            },
            { typeof(ClipLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_Clip(property, name);
                }
            },
            { typeof(ExpressionsLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_Expressions(property, name);
                }
            },
            { typeof(FilterLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_Filter(property, name);
                }
            },
            { typeof(FrameLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_Frame(property, name);
                }
            },
            { typeof(ParameterizedMotionLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_ParameterizedMotion(property, name);
                }
            },
            { typeof(RealLogicalProperty), delegate(IProperty property, string name)
                {
                    return new PropertyItem_Real(property, name);
                }
            },
            { typeof(ModifierTransitionProperty), delegate(IProperty property, string name)
                {
                    return new TransitionPropertyItem_Modifier(property, name);
                }
            },
            { typeof(RealTransitionProperty), delegate(IProperty property, string name)
                {
                    return new TransitionPropertyItem_Real(property, name);
                }
            },
            { typeof(SourceTransform), delegate(IProperty property, string name)
                {
                    return new PropertyItem_SourceTransform(property, name);
                }
            },
        };
    }
}
