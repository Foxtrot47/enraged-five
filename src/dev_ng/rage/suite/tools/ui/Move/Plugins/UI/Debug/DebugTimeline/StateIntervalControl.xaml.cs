﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Move.Core;
using New.Move.Core;

namespace Move.UI
{
    /// <summary>
    /// Interaction logic for StateIntervalControl.xaml
    /// </summary>
    public partial class StateIntervalControl : UserControl
    {
        public StateIntervalControl()
        {
            InitializeComponent();


        }

        public StateIntervalControl(StateInterval s, RuntimeDatabase rdb)
        {
            Interval = s;
            InitializeComponent();
            SourceNode = rdb.GetState(s.StateId.State);

            ToolTip = new ToolTip();
        }

        string GetTooltipString()
        {
            string str = String.Format("State {0}: {1}", SourceNode != null ? (object)SourceNode.Name : (object)Interval.StateId, Interval.IntervalString());
            if (Interval.Status == Move.Core.Interval.StatusType.CLOSED)
            {
                str += String.Format("\nDuration: {0:0.###}ms", (Interval.EndTime - Interval.StartTime).TotalMilliseconds);
            }
            else
            {
                str += "\nDuration: N/A";
            }
            return str;
        }

        protected override void OnToolTipOpening(ToolTipEventArgs e)
        {
            ToolTip tt = (ToolTip)(ToolTip);
            tt.Content = GetTooltipString();
            base.OnToolTipOpening(e);
        }

        public StateInterval Interval;
        public INode SourceNode; // always check for NULL - may not be available

    }

    public class StateIntervalBackgroundControl : UserControl
    {
        public StateIntervalBackgroundControl()
        {
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            Pen p = new Pen(Foreground, 0.5);
            double middle = RenderSize.Height * 0.5;

            dc.DrawLine(p, new Point(0, middle), new Point(RenderSize.Width, middle));
        }
    }
}
