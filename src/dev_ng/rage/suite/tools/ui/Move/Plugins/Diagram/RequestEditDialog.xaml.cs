﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

using Rage.Move;

using AvalonDock;

namespace Move.UI
{
    public partial class RequestEditDialog : Window
    {
        public RequestEditDialog()
        {
            InitializeComponent();
        }

        Database Database
        {
            get
            {
                DocumentContent doc = App.Window.DocumentHost.SelectedItem as DocumentContent;
                return doc.Tag as Database;
            }
        }

        public void DoModal()
        {
            ShowDialog();
        }

        void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.Text))
                Database.Requests.Add(new Request(Request.Text));
            Close();
        }

        void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}

