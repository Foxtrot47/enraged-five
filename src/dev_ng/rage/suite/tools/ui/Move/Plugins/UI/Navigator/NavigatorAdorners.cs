﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Controls;

namespace Move.UI
{
    public class NavigatorDragAdorner : Adorner
    {
        private Rectangle _child = null;
        private double _offsetLeft = 0;
        private double _offsetTop = 0;

        public NavigatorDragAdorner( UIElement adornedElement, Size size, Brush brush)
            : base ( adornedElement )
        {
            Rectangle rect = new Rectangle();
            rect.Fill = brush;
            rect.Width = size.Width;
            rect.Height = size.Height;
            rect.IsHitTestVisible = false;
            this._child = rect;
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup result = new GeneralTransformGroup();
            result.Children.Add(base.GetDesiredTransform(transform));
            result.Children.Add(new TranslateTransform(this._offsetLeft, this._offsetTop));
            return result;
        }

        public double OffsetLeft
        {
            get { return this._offsetLeft;  }
            set 
            { 
                this._offsetLeft = value;
                UpdateLocation();
            }
        }

        public double OffsetTop
        {
            get { return this._offsetTop;  }
            set
            {
                this._offsetTop = value;
                UpdateLocation();
            }
        }

        public void SetOffsets( double left, double top)
        {
            this._offsetLeft = left;
            this._offsetTop = top;
            UpdateLocation();
        }

        protected override Size MeasureOverride(Size constraint)
        {
            this._child.Measure(constraint);
            return this._child.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            this._child.Arrange(new Rect(finalSize));
            return finalSize;
        }

        protected override Visual GetVisualChild(int index)
        {
            return this._child;
        }

        private void UpdateLocation()
        {
            AdornerLayer adornerLayer = this.Parent as AdornerLayer;
            if (adornerLayer != null)
                adornerLayer.Update(this.AdornedElement);
        }
    }
}
