﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Move.Core;

namespace Move.UI
{
    /// <summary>
    /// Interaction logic for FloatRowControl.xaml
    /// </summary>
    public partial class FloatRowControl : UserControl
    {
        public FloatRowControl()
        {
            InitializeComponent();
            BackgroundBrush = Brushes.Black.Clone();
            BackgroundBrush.Opacity = 0.65;
        }

        public FloatRowControl(int signal, StateInterval interval, WindowedTimeLine timeline, RuntimeDatabase runtimeDb)
        {
            Timeline = timeline;
            RuntimeDb = runtimeDb;
            SignalId = signal;
            SignalInterval = interval;
            BackgroundBrush = Brushes.Black.Clone();
            BackgroundBrush.Opacity = 0.65;

            // The SignalHistory should be constant, and created before the FloatRowControl is.
            if (SignalInterval == null)
            {
                Signal = RuntimeDb.GlobalSignalHistory.FindSignal((ushort)SignalId) as SignalHistory.FloatSignal;
                Debug.Assert(Signal != null, String.Format("Couldn't find global signal with id {0}", SignalId));
            }
            else
            {
                Signal = SignalInterval.SignalHistory.FindSignal((ushort)SignalId) as SignalHistory.FloatSignal;
                Debug.Assert(Signal != null, String.Format("Couldn't find signal with id {0} in state interval {1}", SignalId, SignalInterval));
            }

            ToolTip = new ToolTip();
        }
        
        public bool IsGlobalSignal
        {
            get { return SignalInterval == null; }
        }

        string GetTooltipString()
        {
            // find out where the mouse is relative to the containing window.
            Point mousePos = Mouse.GetPosition((FrameworkElement)Parent);

            TimeSpan mouseTime = TimeSpan.FromSeconds(Timeline.AbsPixelToPhase(mousePos.X));

            float sigVal = Signal.ValueAtTime(mouseTime);

            return String.Format("Signal {0} = {1} at {2}s\n", SignalId, sigVal, mouseTime.TotalSeconds);
        }

        protected override void OnToolTipOpening(ToolTipEventArgs e)
        {
            ToolTip tt = (ToolTip)(ToolTip);
            tt.Content = GetTooltipString();
            base.OnToolTipOpening(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            ToolTip tt = (ToolTip)(ToolTip);
            if (tt.Visibility == Visibility.Visible)
            {
                tt.Content = GetTooltipString();
            }
            base.OnMouseMove(e);
        }

        protected override void OnRender(DrawingContext dc)
        {
            double top = 0;
            double bottom = RenderSize.Height;

            // lock the DB so the indices don't change when we iterate over the signal histories
            lock (RuntimeDb.LockObject)
            {
                double offset = 0.0;
                double startBGPix, stopBGPix;

                float sigMin = Signal.Min;
                float sigMax = Signal.Max;

                if (IsGlobalSignal)
                {
                    startBGPix = Timeline.PhaseToAbsPixel(Timeline.PhaseMin);
                    stopBGPix = Timeline.PhaseToAbsPixel(Timeline.PhaseMax);
                }
                else
                {
                    // A state interval's signal row. These need to be offset since pixel 0 isn't Timeline.PhaseMin, its actually
                    // SignalInterval.StartTime.
                    offset = Timeline.PhaseToAbsPixel(SignalInterval.StartTime.TotalSeconds);
                    startBGPix = Timeline.PhaseToAbsPixel(SignalInterval.StartTime.TotalSeconds) - offset;
                    stopBGPix = Timeline.PhaseToAbsPixel(SignalInterval.EndTime.TotalSeconds) - offset;

                    // also want the min and max to contain the global signal's min and max too (so its easier to compare 
                    // the local signal graph with a global one
                    var globalSig = RuntimeDb.GlobalSignalHistory.FindSignal((ushort)SignalId) as SignalHistory.FloatSignal;
                    if (globalSig != null)
                    {
                        sigMin = Math.Min(sigMin, globalSig.Min);
                        sigMax = Math.Max(sigMax, globalSig.Max);
                    }
                }

                dc.DrawRectangle(BackgroundBrush, null, new Rect(startBGPix, top, stopBGPix - startBGPix, bottom));

                float lastVal = float.NaN;
                Interval queryInter = SignalInterval ?? new Interval(TimeSpan.FromSeconds(Timeline.PhaseMin), TimeSpan.FromSeconds(Timeline.PhaseMax));
                foreach (var inter in Signal.GenerateCoveringIntervals(queryInter))
                {
                    DrawInterval(inter, dc, Signal.Min, Signal.Max, lastVal, offset);
                    lastVal = inter.Data;
                }
            }

            base.OnRender(dc);
        }

        private void DrawInterval(FloatInterval inter, DrawingContext dc, float rangeMin, float rangeMax, float lastVal, double pixelOffset)
        {
            double top = 0;
            double bottom = RenderSize.Height;
            double startPix = Timeline.PhaseToAbsPixel(inter.StartTime.TotalSeconds) - pixelOffset;
            double endPix = Timeline.PhaseToAbsPixel(inter.EndTime.TotalSeconds) - pixelOffset;

            if (!float.IsNaN(inter.Data))
            {
                double yVal = rage.math.RampValue(inter.Data, rangeMin, rangeMax, bottom, top);

                if (!float.IsNaN(lastVal))
                {
                    double oldYVal = rage.math.RampValue(lastVal, rangeMin, rangeMax, bottom, top);
                    dc.DrawLine(ValueLine, new Point(startPix, oldYVal), new Point(startPix, yVal));
                }
                dc.DrawLine(ValueLine, new Point(startPix, yVal), new Point(endPix, yVal));
            }
        }

        public WindowedTimeLine Timeline { get; protected set; }
        public RuntimeDatabase RuntimeDb { get; protected set; }
        public int SignalId { get; protected set; }
        public StateInterval SignalInterval { get; protected set; }

        public SignalHistory.FloatSignal Signal;

        public Brush BackgroundBrush;
        public Pen ValueLine = new Pen(Brushes.Green, 2.0);

    }
}
