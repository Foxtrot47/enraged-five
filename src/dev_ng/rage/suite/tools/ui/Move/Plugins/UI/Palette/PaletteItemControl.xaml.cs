﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Move.UI
{
    public partial class PaletteItemControl : UserControl
    {
        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(ImageSource), typeof(PaletteItemControl),
                new FrameworkPropertyMetadata((ImageSource)null,
                    new PropertyChangedCallback(OnIconChanged)));

        public ImageSource Icon
        {
            get { return (ImageSource)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        IDesc Desc { get; set; }

        public PaletteItemControl(IDesc desc)
        {
            InitializeComponent();

            Desc = desc;
            
            ToolTip = Desc.Help;
            Icon = Desc.Icon;
        }

        protected Point? DragStart { get; set; }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            DragStart = new Point?(e.GetPosition(this));
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton != MouseButtonState.Pressed)
                DragStart = null;

            if (DragStart.HasValue)
            {
                Move.Core.DragObject drag = new Move.Core.DragObject(Desc);
                
                DragDrop.DoDragDrop(this, drag, DragDropEffects.Copy);

                e.Handled = true;
            }
        }

        private static void OnIconChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PaletteItemControl)d).OnIconChanged(e);
        }

        protected virtual void OnIconChanged(DependencyPropertyChangedEventArgs e)
        {
            _IconImage.Source = e.NewValue as ImageSource;
        }
    }
}
