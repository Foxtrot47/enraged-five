﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    /*
    [Serializable]
    public class ProxyNode : Logic //LogicNode
    {
        public ResultTransform Result { get; private set; }

        public ProxyNode(IDesc desc)
            : base(desc)
        {
            Result = new ResultTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class ProxyNodeDesc : ILogicDesc //IRegisterNode
    {
        static class Const
        {
            public static string Name = "proxy";
            public static int Id = 21;
            public static string Help = "references other part of tree";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(ProxyNode); } }

        public ProxyNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Diagram.Nodes.Proxy.Images.PaletteIcon_Proxy.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new ProxyNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
     */
}