﻿using System;
using System.Runtime.Serialization;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Conditions
{
    [Serializable]
    public class ConditionInRange : ConditionBase, ISerializable
    {
        public SignalConditionProperty Signal { get; private set; }
        public RealConditionProperty UpperValue { get; private set; }
        public RealConditionProperty LowerValue { get; private set; }

        public ConditionInRange(string name)
            : base(name)
        {
            Signal = new SignalConditionProperty();
            UpperValue = 0.5;
            LowerValue = 0.5;
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredConditionInRange);
            }
        }

        static class SerializationTag
        {
            public static string Signal = "Signal";
            public static string UpperValue = "UpperValue";
            public static string LoweValue = "LowerValue";
        }

        public ConditionInRange(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Signal = (SignalConditionProperty)info.GetValue(SerializationTag.Signal, typeof(SignalConditionProperty));
            UpperValue = (RealConditionProperty)info.GetValue(SerializationTag.UpperValue, typeof(RealConditionProperty));
            LowerValue = (RealConditionProperty)info.GetValue(SerializationTag.LoweValue, typeof(RealConditionProperty));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetSerializationData(info, context);

            info.AddValue(SerializationTag.Signal, Signal);
            info.AddValue(SerializationTag.UpperValue, UpperValue);
            info.AddValue(SerializationTag.LoweValue, LowerValue);
        }

        public override void GetExportData(ChunkObject info, ChunkContext context)
        {
            uint crc = Crc32.Generate(0, Signal.Value.Name);
            info.AppendUInt32(crc);
            info.AppendFloat((float)UpperValue.Value);
            info.AppendFloat((float)LowerValue.Value);
        }

        public override uint GetExportSize()
        {
            return
                sizeof(uint) +      // signal
                sizeof(float) +     // upper
                sizeof(float);      // lower
        }

        public override ushort GetExportId()
        {
            return (ushort)RegisteredConditionInRange.Const.Id;
        }
    }

    public class RegisteredConditionInRange : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "In Range";
            public static int Id = 0;
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }

        public ICondition Create()
        {
            return new ConditionInRange(Const.Name);
        }
    }
}