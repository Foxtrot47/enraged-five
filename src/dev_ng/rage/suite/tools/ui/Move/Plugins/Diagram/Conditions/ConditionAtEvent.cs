﻿using System;
using System.Runtime.Serialization;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Conditions
{
    [Serializable]
    public class ConditionAtEvent : ConditionBase, ISerializable
    {
        public EventConditionProperty Event { get; private set; }

        public ConditionAtEvent(string name)
            : base(name)
        {
            Event = new EventConditionProperty();
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredConditionAtEvent);
            }
        }

        static class Const
        {
            public static string Event = "Event";
        }

        public ConditionAtEvent(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Event = (EventConditionProperty)info.GetValue(Const.Event, typeof(EventConditionProperty));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetSerializationData(info, context);

            info.AddValue(Const.Event, Event);
        }

        public override void GetExportData(ChunkObject info, ChunkContext context)
        {
            info.AppendUInt32(Crc32.Generate(0, Event.Event.Name));
        }

        public override uint GetExportSize()
        {
            return sizeof(uint);   // event
        }

        public override ushort GetExportId()
        {
            return (ushort)RegisteredConditionAtEvent.Const.Id;
        }
    }

    public class RegisteredConditionAtEvent : IConditionDesc
    {
        static internal class Const
        {
            public static string Name = "At Event";
            public static int Id = 4;
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }

        public ICondition Create()
        {
            return new ConditionAtEvent(Const.Name);
        }
    }
}
