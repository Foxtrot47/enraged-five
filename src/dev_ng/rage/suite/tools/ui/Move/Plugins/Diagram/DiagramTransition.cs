﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using New.Move.Core;

using ITransition = New.Move.Core.ITransition;

namespace Move.UI
{
    public class DiagramTransition : Control, INotifyPropertyChanged
    {
        public DiagramTransition(DiagramState source, DiagramState dest, DiagramCanvas diagram, Transition transition)
        {
            Transition = transition;
            _Diagram = diagram;

            Loaded += new RoutedEventHandler(DiagramTransition_Loaded);
            Unloaded += new RoutedEventHandler(DiagramTransition_Unloaded);

            Source = source;
            Source.Loaded += new RoutedEventHandler(DiagramState_Loaded);
            Source.PositionChanged += new RoutedEventHandler(DiagramState_PositionChanged);

            Dest = dest;
            Dest.Loaded += new RoutedEventHandler(DiagramState_Loaded);
            Dest.PositionChanged += new RoutedEventHandler(DiagramState_PositionChanged);

            SelectionBehaviour.SetIsSelectionEnabled(this, true);

            Tag = transition;
            /*
            Console.WriteLine("Source top left {0}", source.TopLeft);
            Console.WriteLine("Source bottom right {0}", source.BottomRight);
            Console.WriteLine("Dest top left {0}", dest.TopLeft);
            Console.WriteLine("Dest bottom right {0}", dest.BottomRight);
             */
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            ContextMenu = new ContextMenu();

            MenuItem deleteItem = new MenuItem();
            deleteItem.Header = "Delete Transition";
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);
            ContextMenu.Items.Add(deleteItem);

            SelectionBehaviour.SetIsSelected(this, IsSelected);

            Transition.IsSelectedChanged +=
                new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);
        }

        void DiagramTransition_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        void DiagramTransition_Unloaded(object sender, RoutedEventArgs e)
        {
            Transition.IsSelectedChanged -=
                new EventHandler<IsSelectedChangedEventArgs>(Transition_IsSelectedChanged);

            Source.Loaded -= new RoutedEventHandler(DiagramState_Loaded);
            Source.PositionChanged -= new RoutedEventHandler(DiagramState_PositionChanged);

            Dest.Loaded -= new RoutedEventHandler(DiagramState_Loaded);
            Dest.PositionChanged -= new RoutedEventHandler(DiagramState_PositionChanged);

            Transition.Tag = null;
        }

        void Transition_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            switch (e.Action)
            {
                case SelectionChangedAction.Add:
                    SetValue(SelectionBehaviour.IsSelectedProperty, true);
                    break;
                case SelectionChangedAction.Remove:
                    SetValue(SelectionBehaviour.IsSelectedProperty, false);
                    break;
            }
        }

        void DiagramState_PositionChanged(object sender, RoutedEventArgs e)
        {
            UpdateGeometry();
        }

        void DiagramState_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateGeometry();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Transition.Dispose();
        }

        public Guid Id { get; private set; }

        public Transition Transition { get; private set; }

        DiagramCanvas _Diagram;

        PathGeometry _Geometry;
        public PathGeometry Geometry
        {
            get
            {
                return _Geometry;
            }
            set
            {
                if (_Geometry != value)
                {
                    _Geometry = value;
                    OnPropertyChanged("Geometry");
                    UpdatePosition();
                }
            }
        }

        DiagramState _Source;
        public DiagramState Source
        {
            get
            {
                return _Source;
            }
            set
            {
                if (_Source != value)
                {
                    _Source = value;
                    UpdateGeometry();
                }
            }
        }

        DiagramState _Dest;
        public DiagramState Dest
        {
            get
            {
                return _Dest;
            }
            set
            {
                if (_Dest != value)
                {
                    _Dest = value;
                    UpdateGeometry();
                }
            }
        }

        Point _SourcePosition;
        public Point SourcePosition
        {
            get
            {
                return _SourcePosition;
            }
            set
            {
                if (_SourcePosition != value)
                {
                    _SourcePosition = value;
                    OnPropertyChanged("SourcePosition");
                }
            }
        }

        double _SourceAngle;
        public double SourceAngle
        {
            get
            {
                return _SourceAngle;
            }
            set
            {
                if (_SourceAngle != value)
                {
                    _SourceAngle = value;
                    OnPropertyChanged("SourceAngle");
                }
            }
        }

        Point _DestPosition;
        public Point DestPosition
        {
            get
            {
                return _DestPosition;
            }
            set
            {
                if (_DestPosition != value)
                {
                    _DestPosition = value;
                    OnPropertyChanged("DestPosition");
                }
            }
        }

        double _DestAngle;
        public double DestAngle
        {
            get
            {
                return _DestAngle;
            }
            set
            {
                if (_DestAngle != value)
                {
                    _DestAngle = value;
                    OnPropertyChanged("DestAngle");
                }
            }
        }

        public bool IsSelected
        {
            get
            {
                return _Diagram.Database.SelectedItems.Contains(Transition);
            }
        }

        void UpdateGeometry()
        {
            if (_Source != null && _Dest != null)
            {
                PathGeometry geometry = new PathGeometry();

                Point p = _Source.Center;

                Point sink = new Point(0, 0);

                double vx = p.X;
                if (vx < _Dest.TopLeft.X) vx = _Dest.TopLeft.X;
                if (vx > _Dest.BottomRight.X) vx = _Dest.BottomRight.X;
                sink.X = vx;

                double vy = p.Y;
                if (vy < _Dest.TopLeft.Y) vy = _Dest.TopLeft.Y;
                if (vy > _Dest.BottomRight.Y) vy = _Dest.BottomRight.Y;
                sink.Y = vy;

                Point source = new Point(0, 0);
                
                double vx1 = sink.X;
                if (vx1 < _Source.TopLeft.X) vx1 = _Source.TopLeft.X;
                if (vx1 > _Source.BottomRight.X) vx1 = _Source.BottomRight.X;
                source.X = vx1;

                double vy1 = sink.Y;
                if (vy1 < _Source.TopLeft.Y) vy1 = _Source.TopLeft.Y;
                if (vy1 > _Source.BottomRight.Y) vy1 = _Source.BottomRight.Y;
                source.Y = vy1;

                // now use this to generate a transition that starts at 0,0

                Point start = new Point(0, 0);
                Point end = new Point(0, 0);

                if (sink.X < source.X)
                    start.X = source.X - sink.X;
                else
                    end.X = sink.X - source.X;

                if (sink.Y < source.Y)
                    start.Y = source.Y - sink.Y;
                else
                    end.Y = sink.Y - source.Y;

                PathFigure figure = new PathFigure();
                figure.StartPoint = start;
                figure.Segments.Add(new PolyLineSegment(new PointCollection() { end }, true));
                geometry.Figures.Add(figure);

                Geometry = geometry;
            }
        }

        void UpdatePosition()
        {
            if (Geometry.Bounds.Width > 0 || Geometry.Bounds.Height > 0)
            {
                Point start, stangent;
                Geometry.GetPointAtFractionLength(0, out start, out stangent);

                Point end, etangent;
                Geometry.GetPointAtFractionLength(1, out end, out etangent);

                SourceAngle = Math.Atan2(-stangent.Y, -stangent.X) * (180 / Math.PI);
                DestAngle = Math.Atan2(etangent.Y, etangent.X) * (180 / Math.PI);

                start.Offset(-stangent.X * 5, -stangent.Y * 5);
                end.Offset(etangent.X * 5, etangent.Y * 5);

                SourcePosition = start;
                DestPosition = end;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
