﻿using Rage.Move;

namespace Move.Diagram.Transitions
{
    public class Transit : IPlugin
    {
        static class Const
        {
            public static string Name = "transit";
            public static string Description = "provides transition transit";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredTransitionTransit registered = new RegisteredTransitionTransit();
            App.Instance.Runtime.TransitionDescriptors.Add(registered);
        }

        public void Dispose()
        {
        }
    }
}
