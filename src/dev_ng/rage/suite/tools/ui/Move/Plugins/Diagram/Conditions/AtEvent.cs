﻿using Rage.Move;

namespace Move.Diagram.Conditions
{
    public class AtEvent : IPlugin
    {    
        static class Const
        {
            public static string Name = "atevent";
            public static string Description = "provides transition condition atevent";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredConditionAtEvent registered = new RegisteredConditionAtEvent();
            App.Instance.Runtime.RegisteredConditions.Add(registered);
        }

        public void Dispose()
        {
        }    
    }
}
