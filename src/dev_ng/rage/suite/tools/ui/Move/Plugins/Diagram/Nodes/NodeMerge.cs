﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class MergeNode : Logic
    {
        static class Const
        {
            public const uint FilterParameterExportOffset = 0;
            public const uint OutputParameterExportOffset = ushort.MaxValue;
        }

        [ParameterExport(Const.FilterParameterExportOffset)]
        public FilterLogicalProperty Filter 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.OutputParameterExportOffset)]
        public ResultTransform Result 
        { 
            get; 
            private set; 
        }

        public SourceTransform Source0 { get; private set; }
        public SourceTransform Source1 { get; private set; }

        public MergeNode(IDesc desc)
            : base(desc)
        {
            Filter = new FilterLogicalProperty();

            Result = new ResultTransform(this);
            Source0 = new SourceTransform(this);
            Source1 = new SourceTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;

            uint flags = 0;
            flags |= Filter.GetExportFlag();

            info.AppendUInt32(flags);

            info.AppendOffset(context.GetSourceLabelOf(Source0));
            info.AppendOffset(context.GetSourceLabelOf(Source1));

            // write out additional data block
            Filter.GetExportData(info, context);
        }
    }

    public class MergeNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "merge";
            public static int Id = 24;
            public static string Help = "merges 2 input sources";
            public static string Group = "Blend";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(MergeNode); } }

        public MergeNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Move.Diagram.Nodes.Merge.Images.PaletteIcon_Merge.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new MergeNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}