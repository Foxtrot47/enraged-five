﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AvalonDock;

using New.Move.Core;

namespace Move.UI
{
    public partial class PaletteControl : UserControl
    {
        public PaletteControl(Rage.Move.MainWindow wnd)
        {
            InitializeComponent();

            wnd.DocumentHost.SelectionChanged += 
                new SelectionChangedEventHandler(DocumentHost_SelectionChanged);
        }

        Database Database { get; set; }

        void DocumentHost_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (Database != null)
                Database.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);

            try
            {
                DocumentPane pane = sender as DocumentPane;
                DocumentContent content = pane.SelectedItem as DocumentContent;
                Database database = content.Tag as Database;

                Database = database;

                Database.ActiveChanged += new RoutedEventHandler(Database_ActiveChanged);

                UpdatePalette();
            }
            catch
            {
                Database = null;
            }
        }

        void Database_ActiveChanged(object sender, RoutedEventArgs e)
        {
            UpdatePalette();
        }

        void Clear()
        {
            TabControl.Items.Clear();
        }

        void UpdatePalette()
        {
            Clear();

            ITransitional active = Database.Active;

            foreach (IDesc desc in active.Descriptor.Children)
            {
                PaletteItemControl item = new PaletteItemControl(desc);
                Panel panel = FindOrCreate(desc.Group);
                panel.Children.Add(item);
            }

            if (TabControl.Items.Count != 0)
                TabControl.SelectedIndex = 0;
        }

        Panel FindOrCreate(string header)
        {
            foreach (TabItem item in TabControl.Items)
            {
                if ((string)item.Header == header)
                    return item.Content as Panel;
            }

            StackPanel panel = new StackPanel();
            panel.Orientation = Orientation.Vertical;

            TabItem tab = new TabItem();
            tab.DataContext = tab;
            tab.Header = header;
            tab.Content = panel;

            TabControl.Items.Add(tab);

            return panel;
        }
    }
}
