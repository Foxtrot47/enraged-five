﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using Rage.Move;
using Move.Core;

using New.Move.Core;
//using Rage.Move.Utils;

using MoVE.Core;

using Move;
using Move.UI;

using AvalonDock;

namespace Move
{
    public class Diagram : IPlugin
    {
        static class Const
        {
            public static string Name = "diagram";
            public static string Description = "provides diagram";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            App.DocumentCreated += new RoutedEventHandler(DiagramViewer_DocumentCreated);

            App.Initialized += new RoutedEventHandler(MainWindow_Initialized);
        }

        public void Dispose()
        {
            App.DocumentCreated -= new RoutedEventHandler(DiagramViewer_DocumentCreated);
        }

        void DiagramViewer_DocumentCreated(object sender, RoutedEventArgs e)
        {
            try
            {
                DocumentCreatedEventArgs args = e as DocumentCreatedEventArgs;

                DiagramViewer viewer = new DiagramViewer();
                DiagramCanvas diagram = viewer.Diagram;

                viewer.MouseDoubleClick += new MouseButtonEventHandler(diagram.DiagramViewer_MouseDoubleClick);

                DocumentContent doc = (DocumentContent)sender;
                doc.Content = viewer;
                doc.AllowDrop = true;
                doc.Drop += new DragEventHandler(diagram.DocumentContent_Drop);

                diagram.Database = doc.Tag as Database;
                diagram.Document = doc;

                args.Host.SelectionChanged += new SelectionChangedEventHandler(diagram.DocumentHost_SelectionChanged);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void MainWindow_Initialized(object sender, RoutedEventArgs e)
        {
            MainWindow window = sender as MainWindow;

            CommandBinding editRequests = new CommandBinding(MainWindow.EditRequests,
                EditRequests_Execute, EditRequests_CanExecute);
            window.CommandBindings.Add(editRequests);

            CommandBinding editFlags = new CommandBinding(MainWindow.EditFlags,
                EditFlags_Execute, EditFlags_CanExecute);
            window.CommandBindings.Add(editFlags);

            CommandBinding editSignals = new CommandBinding(MainWindow.EditSignals,
                EditSignals_Execute, EditSignals_CanExecute);
            window.CommandBindings.Add(editSignals);
        }

        void EditRequests_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            RequestDialog dialog = new RequestDialog();
            dialog.DoModal();
        }

        void EditRequests_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        void EditFlags_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            FlagDialog dialog = new FlagDialog();
            dialog.DoModal();
        }

        void EditFlags_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        void EditSignals_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            SignalDialog dialog = new SignalDialog();
            dialog.DoModal();
        }

        void EditSignals_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }
    }

    public class DiagramCanvas : Canvas
    {      
        public DiagramCanvas()
        {
            AllowDrop = true;

            Loaded += new RoutedEventHandler(DiagramCanvas_Loaded);
            Unloaded += new RoutedEventHandler(DiagramCanvas_Unloaded);            
        }

        bool _Updated = false;

        internal DocumentContent Document { get; set; }

        public EventHandler<EventArgs> DatabaseChanged;
        void OnDatabaseChanged()
        {
            if (DatabaseChanged != null)
                DatabaseChanged(_Database, EventArgs.Empty);
        }

        Database _Database;
        internal Database Database
        {
            get
            {
                return _Database;
            }
            set
            {
                if (_Database != null)
                {
                    _Database.ActiveChanging -= new RoutedEventHandler(Database_ActiveChanging);
                    _Database.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);

                    _Database.Closing -= new EventHandler(Database_Closing);
                }

                if (_Database != value)
                {
                    _Database = value;
                    _Database.Closing += new EventHandler(Database_Closing);

                    _Database.ActiveChanging += new RoutedEventHandler(Database_ActiveChanging);
                    _Database.ActiveChanged += new RoutedEventHandler(Database_ActiveChanged);

                    if (_Database.Active is Motiontree)
                    {
                        ((Motiontree)(_Database.Active)).Connections.CollectionChanged +=
                            new NotifyCollectionChangedEventHandler(Connections_CollectionChanged);
                    }

                    if (_Database.Active is IAutomaton)
                    {
                        ((IAutomaton)(_Database.Active)).Transitions.CollectionChanged +=
                            new NotifyCollectionChangedEventHandler(Transitions_CollectionChanged);
                    }

                    _Database.Active.AddHandler(
                        new NotifyCollectionChangedEventHandler(Children_CollectionChanged));

                    OnDatabaseChanged();
                }
            }
        }

        void Database_Closing(object sender, EventArgs e)
        {
            _Database.ActiveChanging -= new RoutedEventHandler(Database_ActiveChanging);
            _Database.ActiveChanged -= new RoutedEventHandler(Database_ActiveChanged);
        }

        double _Scale = 1.0;
        public double Scale
        {
            get { return _Scale; }
            set
            {
                if (_Scale != value)
                {
                    _Scale = value;
                    LayoutTransform = new ScaleTransform(_Scale, _Scale);
                }
            }
        }

        Rect _SelectedNodeBounds = Rect.Empty;
        public Rect SelectedNodeBounds
        {
            get { return _SelectedNodeBounds; }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
        }

        public Point RelativePosition(Point position)
        {
            IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();

            if (children.Count().Equals(0))
                return new Point(0, 0);

            Point topleft = new Point(double.MaxValue, double.MaxValue);

            foreach (Control child in children)
            {
                topleft.X = Math.Min(topleft.X, Canvas.GetLeft(child));
                topleft.Y = Math.Min(topleft.Y, Canvas.GetTop(child));
            }

            return new Point(topleft.X + position.X, topleft.Y + position.Y);
        }

        public Size Size
        {
            get
            {
                IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();

                Size size = new Size(0, 0);

                foreach (IDiagramElement child in children)
                {
                    double width = child.Position.X + child.DesiredSize.Width;

                    if (size.Width < width)
                        size.Width = width;

                    double height = child.Position.Y + child.DesiredSize.Height;

                    if (size.Height < height)
                        size.Height = height;
                }

                return size;
            }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
             Size size = new Size(Double.PositiveInfinity, Double.PositiveInfinity);

             foreach (UIElement element in this.InternalChildren)
                 element.Measure(size);
           
            return MeasureItems();
        }

        private Size MeasureItems()
        {
            IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();
            if (children.Count().Equals(0))
                return new Size();

            Rect bounds = new Rect(new Point(Double.MaxValue, Double.MaxValue), new Size());

            foreach (Control child in children)
            {
                Point location = new Point(Canvas.GetLeft(child), Canvas.GetTop(child));

                bounds.X = Math.Min(bounds.X, location.X);
                bounds.Y = Math.Min(bounds.Y, location.Y);
            }

            Point point = new Point(-bounds.X, -bounds.Y);

            foreach (Control child in children)
            {
                Point location = new Point(Canvas.GetLeft(child) + point.X, Canvas.GetTop(child) + point.Y);

                bounds.Width = Math.Max(bounds.Width, location.X + child.DesiredSize.Width);
                bounds.Height = Math.Max(bounds.Height, location.Y + child.DesiredSize.Height);
            }

            return new Size(bounds.Width, bounds.Height);
        }
        
        protected override Size ArrangeOverride(Size finalSize)
        {
            IEnumerable<IDiagramElement> children = Children.OfType<IDiagramElement>();
            if (children.Count().Equals(0))
                return new Size();

            Rect bounds = new Rect(new Point(Double.MaxValue, Double.MaxValue), new Size());

            foreach (Control child in children)
            {
                Point location = new Point(Canvas.GetLeft(child), Canvas.GetTop(child));

                bounds.X = Math.Min(bounds.X, location.X);
                bounds.Y = Math.Min(bounds.Y, location.Y);
            }

            // create an absolute location
            Point point = new Point(-bounds.X, -bounds.Y);

            foreach (Control child in children)
            {
                Point location = new Point(Canvas.GetLeft(child) + point.X, Canvas.GetTop(child) + point.Y);

                Rect rect = new Rect(location, child.DesiredSize);
                child.Arrange(rect);

                bounds.Width = Math.Max(bounds.Width, location.X + child.DesiredSize.Width);
                bounds.Height = Math.Max(bounds.Height, location.Y + child.DesiredSize.Height);
            }

            foreach (Move.UI.DiagramConnection connection in InternalChildren.OfType<Move.UI.DiagramConnection>())
            {
                Rect rect = new Rect(connection.Sender.Sender.Value, connection.Receiver.Receiver.Value);
                connection.Arrange(rect);
            }

            foreach (Move.UI.DiagramTransition transition in InternalChildren.OfType<Move.UI.DiagramTransition>())
            {
                // TODO: gather the transitions into groups of where they start and end and space them out

                Point source0 = transition.Source.TopLeft;
                Point source1 = transition.Source.BottomRight;

                Point p = transition.Source.Center;

                Point dest0 = transition.Dest.TopLeft;
                Point dest1 = transition.Dest.BottomRight;

                Point dest = new Point(0, 0);

                double vx = p.X;
                if (vx < dest0.X) vx = dest0.X;
                if (vx > dest1.X) vx = dest1.X;
                dest.X = vx;

                double vy = p.Y;
                if (vy < dest0.Y) vy = dest0.Y;
                if (vy > dest1.Y) vy = dest1.Y;
                dest.Y = vy;

                Point source = new Point(0, 0);
                
                double vx1 = dest.X;
                if (vx1 < source0.X) vx1 = source0.X;
                if (vx1 > source1.X) vx1 = source1.X;
                source.X = vx1;

                double vy1 = dest.Y;
                if (vy1 < source0.Y) vy1 = source0.Y;
                if (vy1 > source1.Y) vy1 = source1.Y;
                source.Y = vy1;

                Rect rect = new Rect(source, dest);
                transition.Arrange(rect);
            }
            
            return new Size(bounds.Width, bounds.Height);
        }

        UserControl GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is DiagramViewer))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as UserControl;
        }

        public void DocumentContent_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(DragObject)))
            {
                DragObject drag = e.Data.GetData(typeof(DragObject)) as DragObject;
                Point position = TranslateWithOffset(e.GetPosition(this));

                _Database.Execute(new CreateNodeFromDescAction(_Database, drag.Desc, position));
            }
        }

        public void DiagramViewer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ITransitional active = _Database.Active;
            ITransitional parent = (ITransitional)_Database.Find(active.Parent);

            if (parent != null)
                _Database.Active = parent;
        }

        void Database_ActiveChanging(object sender, RoutedEventArgs e)
        {
            _Database.Active.RemoveHandler(new NotifyCollectionChangedEventHandler(Children_CollectionChanged));            

            if (sender is Motiontree)
            {
                ((Motiontree)sender).Connections.CollectionChanged -= new NotifyCollectionChangedEventHandler(Connections_CollectionChanged);
            }

            if (sender is IAutomaton)
            {
                ((IAutomaton)sender).Transitions.CollectionChanged -= new NotifyCollectionChangedEventHandler(Transitions_CollectionChanged);
            }
        }

        void Database_ActiveChanged(object sender, RoutedEventArgs e)
        {
            UpdateDiagram();

            _Database.Active.AddHandler(new NotifyCollectionChangedEventHandler(Children_CollectionChanged));

            if (sender is Motiontree)
            {
                ((Motiontree)sender).Connections.CollectionChanged += new NotifyCollectionChangedEventHandler(Connections_CollectionChanged);
            }

            if (sender is IAutomaton)
            {
                ((IAutomaton)sender).Transitions.CollectionChanged += new NotifyCollectionChangedEventHandler(Transitions_CollectionChanged);
            }
        }

        void Children_CollectionChanged(object obj, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (INode item in e.OldItems)
                {
                    RemoveChild(item);
                }
            }

            if (e.NewItems != null)
            {
                foreach (INode item in e.NewItems)
                {
                    InsertChild(item);
                }
            }
        }

        void Connections_CollectionChanged(object obj, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (New.Move.Core.Connection item in e.OldItems)
                {
                    RemoveConnection(item);
                }
            }

            if (e.NewItems != null)
            {
                foreach (New.Move.Core.Connection item in e.NewItems)
                {
                    InsertConnection(item);
                }
            }
        }

        void Transitions_CollectionChanged(object obj, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Transition item in e.NewItems)
                    {
                        InsertTransition(item);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Transition item in e.OldItems)
                    {
                        RemoveTransition(item);
                    }
                    break;
            }
        }

        public void DocumentHost_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DocumentPane host = sender as DocumentPane;
            if (!_Updated && host.SelectedItem == Document)
            {
                UpdateDiagram();
                e.Handled = true;
            }
        }

        bool _Loaded = false;

        void DiagramCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_Loaded)
            {
                SelectionBehaviour.Selected += new EventHandler<SelectedEventArgs>(SelectionBehaviour_Selected);

                _Loaded = true;
            }

            if (!_Updated)
            {
                UpdateDiagram();
            }
        }

        void DiagramCanvas_Unloaded(object sender, RoutedEventArgs e)
        {
            SelectionBehaviour.Selected -= new EventHandler<SelectedEventArgs>(SelectionBehaviour_Selected);

            Children.Clear();
            _Updated = false;

            _Loaded = false;
        }

        void UpdateDiagram()
        {
            Children.Clear();

            foreach (INode child in _Database.Active.Children)
            {
                InsertChild(child);
            }

            var mt = _Database.Active as Motiontree;
            if (mt != null)
            {
                foreach (New.Move.Core.Connection connection in mt.Connections)
                {
                    InsertConnection(connection);
                }
            }

            var automaton = _Database.Active as IAutomaton;
            if (automaton != null)
            {
                foreach (Transition transition in automaton.Transitions)
                {
                    InsertTransition(transition);
                }
            }
            
            _Updated = true;
        }

        void InsertChild(INode item)
        {
            if (item is Output)
            {
                DiagramLogical control = new DiagramLogical(item as Output);
                Children.Add(control);
            }
            else if (item is Input)
            {
                DiagramLogical control = new DiagramLogical(item as Input);
                Children.Add(control);
            }
                /*
            else if (item is Messages)
            {
                DiagramMessages control = new DiagramMessages(item as Messages);
                Children.Add(control);
            }
                 */
                /*
            else if (item is Signals)
            {
                Children.Add(new DiagramSignals(item as Signals));
            }
                 */
            else if (item is Reference)
            {
                DiagramReference control = new DiagramReference(item as Reference);
                Children.Add(control);
            }
            else if (item is Network)
            {
                DiagramNetwork control = new DiagramNetwork(item as Network);
                Children.Add(control);
            }
            else
            if (item is LogicParent)
            {
                DiagramLogicalParent control = new DiagramLogicalParent(item as LogicParent);
                Children.Add(control);
            }
            else if (item is ITransitional)
            {
                DiagramState control = new DiagramState(item as ITransitional);
                Children.Add(control);
            }
            else if (item is SignalBase)
            {
                DiagramSignal control = new DiagramSignal(item as SignalBase);
                Children.Add(control);
            }
            else if (item is TunnelParameter)
            {
                DiagramTunnelParameter control = new DiagramTunnelParameter(item as TunnelParameter);
                Children.Add(control);
            }
            else if (item is TunnelEvent)
            {
                DiagramTunnelEvent control = new DiagramTunnelEvent(item as TunnelEvent);
                Children.Add(control);
            }
            else if (item is ILogic)
            {
                DiagramLogical control = new DiagramLogical(item as ILogic);
                Children.Add(control);
            }
        }

        void RemoveChild(INode item)
        {
            UIElement[] children = new UIElement[InternalChildren.Count];
            InternalChildren.CopyTo(children, 0);

            foreach (DiagramNode node in children.OfType<DiagramNode>())
            {
                if (node.Source == item)
                {
                    Children.Remove(node);
                    //node.Tag = null;
                }
            }
        }

        void InsertConnection(New.Move.Core.Connection item)
        {
            Move.UI.IDiagramAnchor sender = item.Source.Tag as Move.UI.IDiagramAnchor;
            Move.UI.IDiagramAnchor receiver = item.Dest.Tag as Move.UI.IDiagramAnchor;

            Move.UI.DiagramConnection connection = new Move.UI.DiagramConnection(sender, receiver, item);
            item.Tag = connection;
            Canvas.SetZIndex(connection, Children.Count);
            Children.Add(connection);
        }

        void RemoveConnection(New.Move.Core.Connection item)
        {
            UIElement[] children = new UIElement[InternalChildren.Count];
            InternalChildren.CopyTo(children, 0);

            foreach (Move.UI.DiagramConnection connection in children.OfType<Move.UI.DiagramConnection>())
            {
                if (connection.Connection == item)
                {
                    Children.Remove(connection);
                    connection.Tag = null;
                }
            }
        }

        void InsertTransition(Transition item)
        {
            DiagramState source = item.Source.Tag as DiagramState;
            DiagramState dest = item.Dest.Tag as DiagramState;

            Move.UI.DiagramTransition transition = new Move.UI.DiagramTransition(source, dest, this, item);
            item.Tag = transition;
            Canvas.SetZIndex(transition, Children.Count);
            Children.Add(transition);
        }

        void RemoveTransition(Transition item)
        {
            UIElement[] children = new UIElement[InternalChildren.Count];
            InternalChildren.CopyTo(children, 0);

            foreach (Move.UI.DiagramTransition transition in children.OfType<Move.UI.DiagramTransition>())
            {
                if (transition.Transition == item)
                {
                    Children.Remove(transition);
                    transition.Tag = null;
                }
            }
        }

        Point TranslateWithOffset(Point point)
        {
            if (Children.Count != 0)
            {
                Point offset = new Point(Double.MaxValue, Double.MaxValue);

                foreach (UIElement element in Children)
                {
                    if (Canvas.GetLeft(element) < offset.X)
                        offset.X = Canvas.GetLeft(element);
                    if (Canvas.GetTop(element) < offset.Y)
                        offset.Y = Canvas.GetTop(element);
                }

                return new Point(point.X + offset.X, point.Y + offset.Y);
            }

            return point;
        }

        void SelectionBehaviour_Selected(object sender, SelectedEventArgs e)
        {
            if (!((Keyboard.Modifiers & ModifierKeys.Control) > 0))
            {
                ISelectable[] items = new ISelectable[_Database.SelectedItems.Count];
                _Database.SelectedItems.CopyTo(items, 0);
                foreach (ISelectable item in items)
                {
                    if (item != e.SelectedItem)
                    {
                        _Database.SelectedItems.Remove(item);
                    }
                }
            }

            if (!_Database.SelectedItems.Contains(e.SelectedItem))
            {
                _Database.SelectedItems.Add(e.SelectedItem);   
            }
            else
            {
                if ((Keyboard.Modifiers & ModifierKeys.Control) > 0)
                {
                    if (_Database.SelectedItems.Contains(e.SelectedItem))
                        _Database.SelectedItems.Remove(e.SelectedItem);
                }
            }
        }

        public void ApplicationCommandDelete_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            ISelectable[] selectedItems = new ISelectable[_Database.SelectedItems.Count];
            _Database.SelectedItems.CopyTo(selectedItems, 0);

            foreach (ISelectable selectable in selectedItems)
            {
                selectable.Dispose();
            }

            _Database.SelectedItems.Clear();
        }

        public void ApplicationCommandDelete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _Database.SelectedItems.Count != 0;
            //e.Handled = true;
        }
    }
}
