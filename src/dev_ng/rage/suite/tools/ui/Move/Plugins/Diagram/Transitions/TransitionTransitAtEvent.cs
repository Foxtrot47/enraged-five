﻿using Move.Diagram.Transitions;

using Rage.Move.Core;

using System;
using System.Runtime.Serialization;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Transitions
{
    [Serializable]
    public class TransitionTransitAtEvent : TransitionBase, ISerializable
    {
        public ModifierTransitionProperty Modifier { get; private set; }
        public RealTransitionProperty Duration { get; private set; }
        public RealTransitionProperty Event { get; private set; }

        public TransitionTransitAtEvent(ITransitional source, ITransitional sink)
            : base(source, sink)
        {
            Modifier = New.Move.Core.Modifier.Linear;
            Duration = 0.5;
            Event = 0.0;
        }

        public override Type Descriptor
        {
            get
            {
                return typeof(RegisteredTransitionTransitAtEvent);
            }
        }

        static class SerializationTag
        {
            public static string Id = "Id";
            public static string Source = "Source";
            public static string Dest = "Dest";
            public static string Parent = "Parent";
            public static string Modifier = "Modifier";
            public static string Duration = "Duration";
            public static string Event = "Event";
        }

        public TransitionTransitAtEvent(SerializationInfo info, StreamingContext context)
        {
            Id = (Guid)info.GetValue(SerializationTag.Id, typeof(Guid));
            Source = (ITransitional)info.GetValue(SerializationTag.Source, typeof(ITransitional));
            Sink = (ITransitional)info.GetValue(SerializationTag.Dest, typeof(ITransitional));
            Parent = (IAutomaton)info.GetValue(SerializationTag.Parent, typeof(IAutomaton));

            Modifier = (ModifierTransitionProperty)info.GetValue(SerializationTag.Modifier, typeof(ModifierTransitionProperty));
            Duration = (RealTransitionProperty)info.GetValue(SerializationTag.Duration, typeof(RealTransitionProperty));
            Event = (RealTransitionProperty)info.GetValue(SerializationTag.Event, typeof(RealTransitionProperty));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(SerializationTag.Id, Id);
            info.AddValue(SerializationTag.Source, Source);
            info.AddValue(SerializationTag.Dest, Sink);
            info.AddValue(SerializationTag.Parent, Parent);

            info.AddValue(SerializationTag.Modifier, Modifier);
            info.AddValue(SerializationTag.Duration, Duration);
            info.AddValue(SerializationTag.Event, Event);
        }

        public override void GetExportData(ChunkObject info, ChunkContext context)
        {
            info.AppendUInt16((ushort)Modifier.Value);
            info.AppendFloat((float)Duration.Value);
        }
    }

    public class RegisteredTransitionTransitAtEvent : ITransitionDesc
    {
        static class Const
        {
            public static string Name = "Transit at Event";
            public static int Id = 1;
            public static string Help = "Performs a transitional blend at an event between two states over a given period of time";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }

        public ITransition Create(ITransitional source, ITransitional sink)
        {
            return new TransitionTransitAtEvent(source, sink);
        }
    }
}