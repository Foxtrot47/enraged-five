﻿using Rage.Move;

namespace Move.Diagram.Conditions
{
    public class LessThanEqual : IPlugin
    {
        static class Const
        {
            public static string Name = "less than equal";
            public static string Description = "provides transition condition for less than equal";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredConditionLessThanEqual registered = new RegisteredConditionLessThanEqual();
            App.Instance.Runtime.RegisteredConditions.Add(registered);
        }

        public void Dispose()
        {
        }
    }
}
