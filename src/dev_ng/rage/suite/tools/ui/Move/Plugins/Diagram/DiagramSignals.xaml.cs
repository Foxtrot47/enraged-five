﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Move.UI
{
    delegate DiagramAnchor SignalDelegate(Signal signal);

    public partial class DiagramSignals : DiagramNode
    {
        public DiagramSignals(Signals source)
            : base(source)
        {
            InitializeComponent();

            Unloaded += new RoutedEventHandler(DiagramSignals_Unloaded);
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            InitSignals();

            Source.Database.Signals.CollectionChanged += new NotifyCollectionChangedEventHandler(Signals_CollectionChanged);
        }

        void DiagramSignals_Unloaded(object sender, RoutedEventArgs e)
        {
            Source.Database.Signals.CollectionChanged -= new NotifyCollectionChangedEventHandler(Signals_CollectionChanged);
        }

        void InitSignals()
        {
            foreach (Signal signal in Source.Database.Signals)
            {
                DiagramAnchor item = CreateSignal[signal.GetType()](signal);
                signal.Tag = item;

                item.Container = this;

                Anchors.Children.Add(item);
            }
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;
            MenuItem addItem = new MenuItem();
            addItem.Header = "Add signal...";
            addItem.Click += new RoutedEventHandler(AddItem_Click);

            contextMenu.Items.Add(addItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu context = (ContextMenu)sender;
            context.Items.Clear();
        }

        void AddItem_Click(object sender, RoutedEventArgs e)
        {
            SignalEditDialog dialog = new SignalEditDialog();
            dialog.DoModal();
        }

        void Signals_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                UIElement[] children = new UIElement[Anchors.Children.Count];
                Anchors.Children.CopyTo(children, 0);

                foreach (Signal signal in e.OldItems)
                {
                    foreach (DiagramAnchor child in children)
                    {
                        if (child.Source == signal)
                        {
                            Anchors.Children.Remove(child);
                        }
                    }
                }
            }

            if (e.NewItems != null)
            {
                foreach (Signal signal in e.NewItems)
                {
                    DiagramAnchor item = CreateSignal[signal.GetType()](signal);
                    signal.Tag = item;

                    item.Container = this;

                    Anchors.Children.Add(item);
                }
            }
        }

        static DiagramAnchor CreateSignal_Real(Signal signal)
        {
            return new DiagramSignal_Real(signal);
        }

        static DiagramAnchor CreateSignal_Boolean(Signal signal)
        {
            return new DiagramSignal_Boolean(signal);
        }

        static DiagramAnchor CreateSignal_Animation(Signal signal)
        {
            return new DiagramSignal_Animation(signal);
        }

        static DiagramAnchor CreateSignal_Clip(Signal signal)
        {
            return new DiagramSignal_Clip(signal);
        }

        static DiagramAnchor CreateSignal_Expression(Signal signal)
        {
            return new DiagramSignal_Expression(signal);
        }

        static DiagramAnchor CreateSignal_Filter(Signal signal)
        {
            return new DiagramSignal_Filter(signal);
        }

        static DiagramAnchor CreateSignal_Frame(Signal signal)
        {
            return new DiagramSignal_Frame(signal);
        }

        static DiagramAnchor CreateSignal_ParameterizedMotion(Signal signal)
        {
            return new DiagramSignal_ParameterizedMotion(signal);
        }

        static Dictionary<Type, SignalDelegate> CreateSignal = new Dictionary<Type, SignalDelegate>
        {
            { typeof(Signal_Real), CreateSignal_Real },
            { typeof(Signal_Boolean), CreateSignal_Boolean },            
            { typeof(Signal_Animation), CreateSignal_Animation },
            { typeof(Signal_Clip), CreateSignal_Clip },
            { typeof(Signal_Expression), CreateSignal_Expression },
            { typeof(Signal_Filter), CreateSignal_Filter },
            { typeof(Signal_Frame), CreateSignal_Frame },
            { typeof(Signal_ParameterizedMotion), CreateSignal_ParameterizedMotion },
        };
    }
}
