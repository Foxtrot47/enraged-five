﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Move.Core;
using Move.Utils;

namespace Move.UI
{
    /// <summary>
    /// Interaction logic for StringRowControl.xaml
    /// </summary>
    public partial class StringRowControl : UserControl
    {
        public StringRowControl()
        {
            InitializeComponent();
        }

        public StringRowControl(int signal, StateInterval interval, WindowedTimeLine timeline, RuntimeDatabase runtimeDb)
        {
            InitializeComponent();

            Timeline = timeline;
            RuntimeDb = runtimeDb;
            SignalId = signal;
            SignalInterval = interval;

            // The SignalHistory should be constant, and created before the FloatRowControl is.
            if (SignalInterval == null)
            {
                Signal = RuntimeDb.GlobalSignalHistory.FindSignal((ushort)SignalId) as SignalHistory.StringSignal;
                Debug.Assert(Signal != null, String.Format("Couldn't find global signal with id {0}", SignalId));
            }
            else
            {
                Signal = SignalInterval.SignalHistory.FindSignal((ushort)SignalId) as SignalHistory.StringSignal;
                Debug.Assert(Signal != null, String.Format("Couldn't find signal with id {0} in state interval {1}", SignalId, SignalInterval));
            }

            Signal.IntervalAdded += (sender, args) => { AddString(args.Payload); };
            Signal.IntervalExtended += (sender, args) => { ExtendString(args.Payload); };

            Binding b = new Binding();
            b.Source = ObservableStringIntervals;
            StringItems.SetBinding(ItemsControl.ItemsSourceProperty, b);

            AddIntervals();
        }

        void AddString(StringInterval si)
        {
            this.InvokeAction(() =>
            {
                var interVM = new StringIntervalVM();
                interVM.Interval = si;
                interVM.Signal = Signal;

                ObservableStringIntervals.Add(interVM);
            });
        }

        void ExtendString(StringInterval si)
        {
            this.InvokeAction(() =>
            {
                // reset the interval object. That should cause a re-layout
                var lastInter = ObservableStringIntervals[ObservableStringIntervals.Count - 1];
                lastInter.Interval = si;
            });
        }

        void AddIntervals()
        {
            ObservableStringIntervals.Clear();

            lock(RuntimeDb.LockObject)
            {
                foreach(var inter in Signal.Values)
                {
                    AddString(inter);
                }
            }
        }

        public bool IsGlobalSignal
        {
            get { return SignalInterval == null; }
        }

        public WindowedTimeLine Timeline { get; protected set; }
        public RuntimeDatabase RuntimeDb { get; protected set; }
        public int SignalId { get; protected set; }
        public StateInterval SignalInterval { get; protected set; }
        public SignalHistory.StringSignal Signal;

        public ObservableCollection<StringIntervalVM> ObservableStringIntervals = new ObservableCollection<StringIntervalVM>();

    }

    // This is a ViewModel class that wraps a StringInterval for use on an IntervalRowCanvas
    public class StringIntervalVM : DependencyObject
    {
        public StringInterval Interval
        {
            get { return (StringInterval)this.GetValue(IntervalRowCanvas.IntervalProperty); }
            set { this.SetValue(IntervalRowCanvas.IntervalProperty, value); }
        }

        public SignalHistory.StringSignal Signal;

        public string Name { get { return Interval.Data; } }
        public string Description { get { return String.Format("Signal {0} = {1} from {2} to {3}", Signal.Id, Name, Interval.StartTime, Interval.EndTime); } }

        public override string ToString()
        {
            return Name;
        }
    }

    public class StringRowSelector : DataTemplateSelector
    {
        // there's probably a better way to get these...
        public DataTemplate AnimationTemplate { get; set; }
        public DataTemplate ClipTemplate { get; set; }
        public DataTemplate ExpressionsTemplate { get; set; }
        public DataTemplate FilterTemplate { get; set; }
        public DataTemplate FrameTemplate { get; set; }
        public DataTemplate ParameterizedMotionTemplate { get; set; }
        public DataTemplate DefaultTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            StringIntervalVM vm = item as StringIntervalVM;
            if (vm != null)
            {
                switch(vm.Signal.Type)
                {
                    case New.Move.Core.Parameter.Animation:
                        return AnimationTemplate;
                    case New.Move.Core.Parameter.Clip:
                        return ClipTemplate;
                    case New.Move.Core.Parameter.Expressions:
                        return ExpressionsTemplate;
                    case New.Move.Core.Parameter.Filter:
                        return FilterTemplate;
                    case New.Move.Core.Parameter.Frame:
                        return FrameTemplate;
                    case New.Move.Core.Parameter.ParameterizedMotion:
                        return ParameterizedMotionTemplate;
                    default:
                        return DefaultTemplate;
                }
            }
            return base.SelectTemplate(item, container);
        }

    }
}
