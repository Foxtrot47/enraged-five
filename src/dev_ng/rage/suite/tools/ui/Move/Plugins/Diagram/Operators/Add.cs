﻿using Rage.Move;
using New.Move.Core;

namespace Move.Diagram.Operators
{
    public class Add : IPlugin
    {
        static class Const
        {
            public static string Name = "add";
            public static string Description = "provides operator add";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredOperatorAdd desc = new RegisteredOperatorAdd();

            //App.Instance.Runtime.LogicDescriptions.Add(desc);
            Motiontree.Desc.Children.Add(desc);
        }

        public void Dispose()
        {
        }
    }

    public class AddConstant : IPlugin
    {
        static class Const
        {
            public static string Name = "add constant";
            public static string Description = "provides operator add constant";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            RegisteredOperatorAddConstant desc = new RegisteredOperatorAddConstant();

            //App.Instance.Runtime.LogicDescriptions.Add(desc);
            Motiontree.Desc.Children.Add(desc);
        }

        public void Dispose()
        {
        }
    }
}
