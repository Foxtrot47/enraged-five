﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class MoverNode : Logic //LogicNode
    {
        public PassthroughTransform Result { get; private set; }

        public MoverNode(IDesc desc)
            : base(desc)
        {
            Result = new PassthroughTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class MoverNodeDesc : ILogicDesc //IRegisterNode
    {
        static class Const
        {
            public static string Name = "mover";
            public static int Id = 12;
            public static string Help = "Supplies mover data";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(MoverNode); } }

        public MoverNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Move.Diagram.Nodes.Mover.Images.PaletteIcon_Mover.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new MoverNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}