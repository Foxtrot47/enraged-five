﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Move.UI
{
    public partial class DiagramAnchorRealSource : DiagramAnchor, IRealAnchor
    {
        public static readonly DependencyProperty InputProperty =
            DependencyProperty.Register("Input", typeof(PropertyInput), typeof(DiagramAnchorRealSource));

        public static readonly DependencyProperty ResultEnabledProperty =
            DependencyProperty.Register("ResultEnabled", typeof(bool), typeof(DiagramAnchorRealSource));

        internal PropertyInput Input
        {
            get { return (PropertyInput)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        internal bool ResultEnabled
        {
            get { return (bool)GetValue(ResultEnabledProperty); }
            set { SetValue(ResultEnabledProperty, value); }
        }

        public DiagramAnchorRealSource(string name, Property source) : base(name, source)
        {
            InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            SetBinding(DiagramAnchorRealSource.InputProperty,
                new Binding("Input") { Source = Anchor });

            SetBinding(DiagramAnchorRealSource.ResultEnabledProperty,
                new Binding("OutputEnabled") { Source = Anchor });
        }

        public override Point? Sender
        {
            get
            {
                return new Point?(ResultPoint.TranslatePoint(new Point(0, 0), GetDiagram(this)));
            }
        }

        public override Point? Receiver
        {
            get 
            { 
                return new Point?(SourcePoint.TranslatePoint(new Point(0, 0), GetDiagram(this))); 
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            DiagramCanvas diagram = GetDiagram(this);
            if (diagram != null)
            {
                Point position = ResultPoint.TranslatePoint(new Point(0, 0), diagram);
                Start = new Point?(position);
                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                Start = null;
            }

            if (Start.HasValue)
            {
                DiagramCanvas diagram = GetDiagram(this);
                if (diagram != null)
                {
                    AdornerLayer layer = AdornerLayer.GetAdornerLayer(diagram);
                    if (layer != null)
                    {
                        DiagramAdorner adorner = new DiagramAdorner(diagram, Start.Value, this);
                        if (adorner != null)
                        {
                            layer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }

            base.OnMouseMove(e);
        }

        public override IDiagramAnchor HitTest(Point point)
        {
            DiagramCanvas diagram = GetDiagram(this);
            if (diagram != null)
            {
                DependencyObject hitObj = diagram.InputHitTest(point) as DependencyObject;
                while (hitObj != null && hitObj.GetType() != typeof(Diagram) && hitObj != this)
                {
                    if (hitObj is DiagramAnchorRealSource || (hitObj is DiagramTunnelParameter && (hitObj as DiagramTunnelParameter).Parameter == Parameter.Real) || hitObj is DiagramAnchorRealInput || hitObj is IRealAnchor)
                        return hitObj as IDiagramAnchor;

                    hitObj = VisualTreeHelper.GetParent(hitObj);
                }
            }

            return null;
        }
    }
}
