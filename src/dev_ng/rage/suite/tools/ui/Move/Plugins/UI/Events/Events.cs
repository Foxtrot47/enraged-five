﻿using System;
using System.ComponentModel.Composition;

using AvalonDock;
using Rage.Move;
using Move;

namespace Move.UI
{
    public class Events : IPlugin
    {
        static class Const
        {
            public static string Name = "Events";
            public static string Description = "provides events control";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
        }

        public void Dispose()
        {
        }
    }

    [Export(typeof(IWindow))]
    public class EventsWindow : IWindow
    {
        public EventsWindow()
        {
            Content = new DockableContent();
            Content.Name = Name;
            Content.Title = Name;
        }

        public DockableContent Content { get; private set; }

        public string Name { get { return "Events"; } }

        public void Create(Rage.Move.MainWindow window)
        {
            Content.Content = new EventsControl(window);
            window.RightPane.Items.Add(Content);
        }
    }
}
