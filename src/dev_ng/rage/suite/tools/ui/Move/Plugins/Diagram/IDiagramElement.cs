﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using New.Move.Core;

namespace Move
{
    public interface IDiagramElement
    {
        System.Windows.Point Position { get; set; }
        System.Windows.Size DesiredSize { get; }
    }

    public abstract class DiagramNode : UserControl, IDiagramElement
    {
        protected DiagramNode(INode source)
        {
            DataContext = this;

            Tag = source;
            source.Tag = this;

            Binding nameBinding = new Binding("Name");
            nameBinding.Source = source;
            nameBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            SetBinding(DiagramNode.LabelProperty, nameBinding);

            Loaded += new RoutedEventHandler(DiagramNode_Loaded);
            Unloaded += new RoutedEventHandler(DiagramNode_Unloaded);
        }

        public INode Source 
        { 
            get 
            { 
                return Tag as INode; 
            }
            protected set
            {
                Tag = value;
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            Source.IsSelectedChanged +=
                new EventHandler<IsSelectedChangedEventArgs>(Source_IsSelectedChanged);

            Source.IsEngagedChanged +=
                new EventHandler<IsEngagedChangedEventArgs>(Source_IsEngagedChanged);
        }

        void DiagramNode_Loaded(object sender, RoutedEventArgs e)
        {
        }

        void DiagramNode_Unloaded(object sender, RoutedEventArgs e)
        {
            if (Source != null)
            {
                Source.IsSelectedChanged -=
                    new EventHandler<IsSelectedChangedEventArgs>(Source_IsSelectedChanged);

                Source.IsEngagedChanged -=
                    new EventHandler<IsEngagedChangedEventArgs>(Source_IsEngagedChanged);

                Source.Tag = null;
            }

            BindingOperations.ClearBinding(this, DiagramNode.LabelProperty);
        }

        void Source_IsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
        {
            switch (e.Action)
            {
                case SelectionChangedAction.Add:
                    SetValue(SelectionBehaviour.IsSelectedProperty, true);
                    break;
                case SelectionChangedAction.Remove:
                    SetValue(SelectionBehaviour.IsSelectedProperty, false);
                    break;
            }
        }

        void Source_IsEngagedChanged(object sender, IsEngagedChangedEventArgs e)
        {
            Engaged = e.Action == EngagedChangedAction.Add;
        }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(DiagramNode));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public static readonly DependencyProperty EngagedProperty =
          DependencyProperty.Register("Engaged", typeof(bool), typeof(DiagramNode), new FrameworkPropertyMetadata(false));

        public bool Engaged
        {
            get { return (bool)GetValue(EngagedProperty); }
            set 
            {
                Action a = () => SetValue(EngagedProperty, value);

                if(this.Dispatcher.CheckAccess()) {
                    a();
                }
                else {
                    this.Dispatcher.Invoke(a);
                }
            }
        }

        public Point Position
        {
            get
            {
                return Source.Position;
            }
            set
            {
                if (Source.Position != value)
                {
                    Source.Position = value;
                    OnPositionChanged();
                }
            }
        }

        public double Top
        {
            get
            {
                return Source.Position.Y;
            }
            set
            {
                if (Source.Position.Y != value)
                {
                    Source.Position = new Point(Source.Position.X, value);
                    OnPositionChanged();
                }
            }
        }

        public double Left
        {
            get
            {
                return Source.Position.X;
            }
            set
            {
                if (Source.Position.X != value)
                {
                    Source.Position = new Point(value, Source.Position.Y);
                    OnPositionChanged();
                }
            }
        }

        public Guid Id
        {
            get
            {
                return Source.Id;
            }
        }

        public bool IsSelected
        {
            get
            {
                return Source.Database.SelectedItems.Contains(Source);
            }
        }

        public event RoutedEventHandler PositionChanged;
        void OnPositionChanged()
        {
            if (PositionChanged != null)
                PositionChanged(this, new RoutedEventArgs());
        }

        protected DiagramCanvas GetCanvas(DependencyObject obj)
        {
            while (obj != null && !(obj is DiagramCanvas))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as DiagramCanvas;
        }

        protected DiagramViewer GetDiagram(DependencyObject obj)
        {
            while (obj != null && !(obj is DiagramViewer))
                obj = VisualTreeHelper.GetParent(obj);

            return obj as DiagramViewer;
        }
    }
}