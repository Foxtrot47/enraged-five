﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Move.Core;

namespace Move.UI
{
    public class IntervalRowCanvas : Panel
    {

        public static readonly DependencyProperty IntervalProperty = DependencyProperty.RegisterAttached(
            "Interval", 
            typeof(Interval), 
            typeof(IntervalRowCanvas),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsParentArrange));

        public static Interval GetInterval(UIElement target) { return (Interval)target.GetValue(IntervalProperty); }
        public static void SetInterval(UIElement target, Interval inter) { target.SetValue(IntervalProperty, inter); }

        public static readonly DependencyProperty TimelineProperty = DependencyProperty.Register("Timeline", typeof(WindowedTimeLine), typeof(IntervalRowCanvas),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));
        public static readonly DependencyProperty FullIntervalProperty = DependencyProperty.Register("FullInterval", typeof(Interval), typeof(IntervalRowCanvas),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));

        // Do our own layout so that the contained controls always span the whole vertical height of this control, but use the
        // appropriate Canvas.Left and Width properties
        protected override Size MeasureOverride(Size constraint)
        {
            foreach (UIElement child in Children)
            {
                // need to call this, but I'm ignoring the values
                child.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            }

            return new Size(0, 0);
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            foreach (UIElement child in Children)
            {
                FrameworkElement frElt = child as FrameworkElement;
                Interval inter = child.GetValue(IntervalProperty) as Interval;
                if (inter != null && Timeline != null)
                {
                    double offset = FullInterval != null ? Timeline.PhaseToAbsPixel(FullInterval.StartTime.TotalSeconds) : 0.0;
                    double minX = Timeline.PhaseToAbsPixel(inter.StartTime.TotalSeconds) - offset;
                    double maxX = Timeline.PhaseToAbsPixel(inter.EndTime.TotalSeconds) - offset;
                    child.Arrange(new Rect(minX, 0.0, maxX - minX, arrangeBounds.Height));
                }
                else
                {
                    Debug.Assert(false, "This control really works better if the children have an Interval property");
                    child.Arrange(new Rect(0.0, 0.0, arrangeBounds.Width, arrangeBounds.Height));
                }
            }
            return arrangeBounds;
        }

        public WindowedTimeLine Timeline
        {
            get { return (WindowedTimeLine)GetValue(TimelineProperty); }
            set { SetValue(TimelineProperty, value); }
        }

        public Interval FullInterval 
        {
            get { return (Interval)GetValue(FullIntervalProperty); }
            set { SetValue(FullIntervalProperty, value); }
        }
    }
}
