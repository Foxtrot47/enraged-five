﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class BehaviorNode : Logic //LogicNode
    {
        public PassthroughTransform Result { get; private set; }

        public BehaviorNode(IDesc desc)
            : base(desc)
        {
            Result = new PassthroughTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class BehaviorNodeDesc : ILogicDesc //IRegisterNode
    {
        static class Const
        {
            public static string Name = "behavior";
            public static int Id = 11;
            public static string Help = "Supplies behavior data";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(BehaviorNode); } }

        public BehaviorNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Diagram.Nodes.Behavior.Images.PaletteIcon_Behavior.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new BehaviorNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}