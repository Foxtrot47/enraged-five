﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

using New.Move.Core;

namespace Move.UI
{
    public partial class DiagramSignal_Animation : DiagramSignalControl
    {
        public DiagramSignal_Animation(IAnchor source) 
            : base(source.Name, source)
        {
            InitializeComponent();
        }

        public override Point? Sender
        {
            get 
            {
                Ellipse PART_Source = (Ellipse)GetTemplateChild("PART_Source");
                return new Point?(PART_Source.TranslatePoint(new Point(0, 0), GetDiagram(this))); 
            }
        }

        protected override bool HitTestValid(DependencyObject hitobj)
        {
            return hitobj is DiagramAnchorAnimationSource;
        }
    }
}
