﻿using Rage.Move;
using New.Move.Core;

namespace Move.Diagram.Nodes
{
    public class BlendN : IPlugin
    {
        static class Const
        {
            public static string Name = "blendn";
            public static string Description = "provides blend nodes";
            public static string Author = "RAGE";
            public static string Version = "";
        }

        public IPluginHost Host { get; set; }

        public string Name { get { return Const.Name; } }
        public string Description { get { return Const.Description; } }
        public string Author { get { return Const.Author; } }
        public string Version { get { return Const.Version; } }

        public void Initialize()
        {
            ILogicDesc desc = new BlendNNodeDesc();

            //App.Instance.Runtime.LogicDescriptions.Add(desc);
            Motiontree.Desc.Children.Add(desc);
        }

        public void Dispose()
        {
        }
    }
}