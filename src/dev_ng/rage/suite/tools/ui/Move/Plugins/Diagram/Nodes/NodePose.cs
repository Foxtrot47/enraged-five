﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class PoseNode : Logic
    {
        static class Const
        {
            public const uint OutputParameterExportOffset = ushort.MaxValue;
        }

        [ParameterExport(Const.OutputParameterExportOffset)]
        public ResultTransform Result 
        { 
            get; 
            private set; 
        }

        public PoseNode(IDesc desc)
            : base(desc)
        {
            Result = new ResultTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
        }
    }

    public class PoseNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "pose";
            public static int Id = 25;
            public static string Help = "Supplies pose data";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(PoseNode); } }

        public PoseNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Diagram.Nodes.Pose.Images.PaletteIcon_Pose.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new PoseNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}