﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Rage.Move.Core;

using New.Move.Core;
using Move.Core;
using Move.Utils;

namespace Move.Diagram.Nodes
{
    [Serializable]
    public class ExtrapolateNode : Logic
    {
        static class Const
        {
            public const uint DampingParameterExportOffset = 0;
            public const uint LastFrameParameterExportOffset = 1;
            public const uint LastFrameOwnerParameterExportOffset = 2;
            public const uint DeltaFrameParameterExportOffset = 3;
            public const uint DeltaFrameOwnerParameterExportOffset = 4;
            public const uint DeltaTimeParameterExportOffset = 5;
            public const uint OutputParameterExportOffset = ushort.MaxValue;
        }

        [ParameterExport(Const.DampingParameterExportOffset)]
        public RealLogicalProperty Damping 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.DeltaTimeParameterExportOffset)]
        public RealLogicalProperty DeltaTime 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.LastFrameParameterExportOffset)]
        public FrameLogicalProperty LastFrame 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.LastFrameOwnerParameterExportOffset)]
        public BooleanLogicalProperty OwnLastFrame 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.DeltaFrameParameterExportOffset)]
        public FrameLogicalProperty DeltaFrame 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.DeltaFrameOwnerParameterExportOffset)]
        public BooleanLogicalProperty OwnDeltaFrame 
        { 
            get; 
            private set; 
        }

        [ParameterExport(Const.OutputParameterExportOffset)]
        public ResultTransform Result 
        { 
            get; 
            private set; 
        }

        public ExtrapolateNode(IDesc desc) 
            : base(desc)
        {
            Damping = 1.0f;
            DeltaTime = 0.0f;
            LastFrame = new FrameLogicalProperty();
            OwnLastFrame = true;
            DeltaFrame = new FrameLogicalProperty();
            OwnDeltaFrame = true;

            Result = new ResultTransform(this);
        }

        protected override void GetExportInfo(ChunkObject info, ChunkContext context)
        {
            Motiontree parent = Database.Find(Parent) as Motiontree;

            uint flags = 0;
            flags |= Damping.GetExportFlag();
            flags |= (LastFrame.GetExportFlag() << 4);
            flags |= (OwnLastFrame.GetExportFlag() << 8);
            flags |= (DeltaFrame.GetExportFlag() << 12);
            flags |= (OwnDeltaFrame.GetExportFlag() << 16);
            flags |= (DeltaTime.GetExportFlag() << 20);
            
            info.AppendUInt32(flags);

            Damping.GetExportData(info, context);
            LastFrame.GetExportData(info, context);
            OwnLastFrame.GetExportData(info, context);
            DeltaFrame.GetExportData(info, context);
            OwnDeltaFrame.GetExportData(info, context);
            DeltaTime.GetExportData(info, context);
        }
    }

    public class ExtrapolateNodeDesc : ILogicDesc
    {
        static class Const
        {
            public static string Name = "Extrapolate";
            public static int Id = 18;
            public static string Help = "Extrapolate from last frame";
            public static string Group = "Logic";
        }

        public string Name { get { return Const.Name; } }
        public int Id { get { return Const.Id; } }
        public string Help { get { return Const.Help; } }
        public string Group { get { return Const.Group; } }
        public ImageSource Icon { get { return _PaletteIcon; } }

        public Type ConstructType { get { return typeof(ExtrapolateNode); } }

        public ExtrapolateNodeDesc()
        {
            try
            {
                _PaletteIcon = new BitmapImage();
                _PaletteIcon.BeginInit();
                _PaletteIcon.StreamSource = 
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        "Move.Diagram.Nodes.Extrapolate.Images.PaletteIcon_Extrapolate.png");
                _PaletteIcon.EndInit();
            }
            catch
            {
                _PaletteIcon = null;
            }
        }
        
        public INode Create(Database database)
        {
            return new ExtrapolateNode(this);
        }

        protected BitmapImage _PaletteIcon = null;
    }
}