﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MoVE.Core;
using Move.UI;

namespace Move
{
    public partial class DiagramNetwork : DiagramNode
    {
        public DiagramNetwork(Network source)
            : base(source)
        {
            InitializeComponent();

            Result.Children.Clear();

            DiagramAnchorResultTransform result = new DiagramAnchorResultTransform("Result", source.Result);
            source.Result.Tag = result;
            result.Container = this;
            Result.Children.Add(result);
        }

        void Menu_Opened(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;

            MenuItem deleteItem = new MenuItem() { Header = "Delete node" };
            deleteItem.Click += new RoutedEventHandler(DeleteItem_Click);

            contextMenu.Items.Add(deleteItem);
        }

        void Menu_Closed(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = (ContextMenu)sender;
            contextMenu.Items.Clear();
        }

        void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Source.Dispose();
            Source = null;
        }
    }
}
