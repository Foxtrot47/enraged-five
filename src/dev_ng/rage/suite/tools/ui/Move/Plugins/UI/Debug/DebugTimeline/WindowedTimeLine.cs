﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using rage;

namespace Move.UI
{
    /// <summary>
    /// A WindowedTimeLine is a time interval along with a visible window.
    /// It can convert between Phase units (the natural units that get sent
    /// from the game) to Display units (the units we display to the users)
    /// and Pixels that are used for rendering (if given a PixelWidth)
    /// 
    /// Typically all operations should be done in phase units, except for UI drawing,
    /// where you need to convert from phase to display units for labels, 
    /// or convert from phase to pixels for positioning.
    /// 
    /// The PixelWidth property can be data-bound to a UIElement's ActualWidth to get
    /// consistent updates.
    /// </summary>
    public class WindowedTimeLine
    {
        public WindowedTimeLine()
        {
            PixelWidth = 100;
            EndInsetPixels = 10;
            PhaseMin = 0;
            PhaseMax = 1.0;
            DisplayMin = 0;
            DisplayMax = 1.0;
            PhaseViewMin = 0.0;
            PhaseViewMax = 1.0;
        }

        #region Constants
        private const double _MinLabelSpacing = 40.0;
        private const double _MinTickSpacing = 6.0;

        private static readonly double[] s_LabelIntervals = new double[]
        {
            .001,
            .005,
            .01,
            .05,
            .1,
            .5,
            1,
            2,
            5,
            10,
            20,
            50,
            100,
            200,
            500,
            1000,
            2000,
            5000,
            10000,
          };

        private static readonly double[] s_TickIntervals = new double[]
        {
            0.001,
            0.01,
            0.1,
            1,
            10,
            100,
            1000,
            10000,
        };
        #endregion

        #region Properties
        /// <summary>
        /// The minimum phase value for the entire timeline
        /// </summary>
        public double PhaseMin {get; protected set;}

        /// <summary>
        /// The maximum phase value for the entire timeline
        /// </summary>
        public double PhaseMax {get; protected set;}

        /// <summary>
        /// The pixel for the minimum phase
        /// </summary>
        public double PixelMin {
            get { return PhaseToPixel(PhaseMin); }
        }

        /// <summary>
        /// The pixel for the maximum phase
        /// </summary>
        public double PixelMax {
            get { return PhaseToPixel(PhaseMax); }
        }

        /// <summary>
        /// A property containing the width of the UI timeline, used to convert
        /// time values to and from pixel values
        /// </summary>
        public double PixelWidth
        {
            get { return _PixelWidth; }
            set
            {
                _PixelWidth = value;
                UpdateIntervals();
            }
        }
        double _PixelWidth;

        /// <summary>
        /// When drawing DisplayMin and DisplayMax - how much to inset them, 
        /// </summary>
        public double EndInsetPixels
        {
            get { return _EndInsetPixels; }
            set
            {
                _EndInsetPixels = value;
                UpdateIntervals();
            }
        }
        double _EndInsetPixels;

        /// <summary>
        /// The minimum displayed value (frames, seconds, whatever) for the entire timeline
        /// </summary>
        public double DisplayMin {
            get { return _DisplayMin; }
            set { 
                if (_DisplayMin != value) 
                {
                    _DisplayMin = value;
                    UpdateIntervals();
                }
            }
        }
        double _DisplayMin;

		/// <summary>
		/// The maximum displayed value (frames, seconds, whatever) for the entire timeline
		/// </summary>
        public double DisplayMax {
            get { return _DisplayMax; }
            set {
                if (_DisplayMax != value) 
                {
                    _DisplayMax = value;
                    UpdateIntervals();
                }
            }
        }
        double _DisplayMax;

        /// <summary>
        /// The minimum (leftmost) phase value of the time range shown
        /// </summary>
        public double PhaseViewMin {
            get { return _PhaseViewMin; }
            set {
                if (_PhaseViewMin != value)
                {
                    if (value >= _PhaseViewMax)
                    {
                        throw new ArgumentOutOfRangeException("value", "value must be < PhaseViewMax");
                    }

                    if (value < PhaseMin)
                    {
                        throw new ArgumentOutOfRangeException("value", "value must be >= PhaseMin");
                    }

                    _PhaseViewMin = value;

                    UpdateIntervals();
                }
            }
        }
        double _PhaseViewMin;

        /// <summary>
        /// The maximum (rightmost) phase value of the time range shown
        /// </summary>
        public double PhaseViewMax
        {
            get
            {
                return _PhaseViewMax;
            }
            set
            {
                if (_PhaseViewMax != value)
                {
                    if (value <= _PhaseViewMin)
                    {
                        throw new ArgumentOutOfRangeException("value", "value must be > PhaseViewMin");
                    }

                    if (value > PhaseMax)
                    {
                        throw new ArgumentOutOfRangeException("value", "value must be <= PhaseMax");
                    }

                    _PhaseViewMax = value;

                    UpdateIntervals();
                }
            }
        }
        double _PhaseViewMax;

        /// <summary>
        /// The range of the phase that is shown
        /// </summary>
        public double PhaseViewRange
        {
            get
            {
                return this.PhaseViewMax - this.PhaseViewMin;
            }
        }

        /// <summary>
        /// The minimum (leftmost) displayed value (frames, seconds, whatever) 
        /// </summary>
        public double DisplayViewMin
        {
            get
            {
                return PhaseToDisplay(_PhaseViewMin);
            }
            set
            {
                if (value >= this.DisplayViewMax)
                {
                    throw new ArgumentOutOfRangeException("value", "value must be < DisplayViewMax");
                }

                if (value < this.DisplayMin)
                {
                    throw new ArgumentOutOfRangeException("value", "value must be >= DisplayMin");
                }

                this.PhaseViewMin = DisplayToPhase(value);
            }
        }

        /// <summary>
        /// The maximum (rightmost) displayed value (frames, seconds, whatever) 
        /// </summary>
        public double DisplayViewMax
        {
            get
            {
                return PhaseToDisplay(_PhaseViewMax);
            }
            set
            {
                if (value <= this.DisplayViewMin)
                {
                    throw new ArgumentOutOfRangeException("value", "value must be > DisplayViewMin");
                }

                if (value > this.DisplayMax)
                {
                    throw new ArgumentOutOfRangeException("value", "value must be <= DisplayMax");
                }

                this.PhaseViewMax = DisplayToPhase(value);
            }
        }

        /// <summary>
        /// The range of the values that are shown
        /// </summary>
        public double DisplayViewRange
        {
            get
            {
                return this.DisplayViewMax - this.DisplayViewMin;
            }
        }

        /// <summary>
        /// The interval at which value labels should be displayed
        /// </summary>
        public double LabelInterval {get; protected set;}

        /// <summary>
        /// The interval at which value ticks should be displayed
        /// </summary>
        public double TickInterval {get; protected set;}
        #endregion

        #region Public Functions
        /// <summary>
        /// Set the minimum and maximum phase for the entire timeline
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public void SetPhase(double min, double max)
        {
            if (min >= max)
            {
                throw new ArgumentOutOfRangeException("min must be < max");
            }

            if (max - min < 0.00001)
            {
                throw new InvalidOperationException("phase range is really small");
            }

            PhaseMin = min;
            PhaseMax = max;

            UpdateIntervals();
        }

        /// <summary>
        /// Set the minimum and maximum values for the entire timeline
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public void SetDisplay(double min, double max)
        {
            if (min >= max)
            {
                throw new ArgumentOutOfRangeException("min must be < max");
            }

            DisplayMin = min;
            DisplayMax = max;

            UpdateIntervals();
        }

        /// <summary>
        /// Set the minimum and maximum phase values that are displayed
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public void SetPhaseView(double min, double max)
        {
            if (min < PhaseMin)
            {
                throw new ArgumentOutOfRangeException("min", "min must be < PhaseMin");
            }

            if (max > PhaseMax)
            {
                throw new ArgumentOutOfRangeException("max", "max must be > PhaseMax");
            }

            if (min >= max)
            {
                throw new ArgumentOutOfRangeException("min must be < max");
            }

            if (max - min < 0.00001)
            {
                throw new InvalidOperationException("phase view range is really small");
            }

            _PhaseViewMin = min;
            _PhaseViewMax = max;

            UpdateIntervals();
        }

        /// <summary>
        /// Sets the minimum and maximum displayed values
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public void SetDisplayView(double min, double max)
        {
            if (min < DisplayMin)
            {
                throw new ArgumentOutOfRangeException("min", "min must be < DisplayMin");
            }

            if (max > DisplayMax)
            {
                throw new ArgumentOutOfRangeException("max", "max must be > DisplayMax");
            }

            if (min >= max)
            {
                throw new ArgumentOutOfRangeException("min must be < max");
            }

            _PhaseViewMin = Math.Max(DisplayToPhase(min), PhaseMin);
            _PhaseViewMax = Math.Min(DisplayToPhase(max), PhaseMax);

            UpdateIntervals();
        }

        /// <summary>
        /// Convert from phase value to display value
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public double PhaseToDisplay(double t)
        {
            return rage.math.RampValueUnclamped(t, this.PhaseMin, this.PhaseMax, this.DisplayMin, this.DisplayMax);
        }

        /// <summary>
        /// Convert from display value to phase value
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public double DisplayToPhase(double t)
        {
            return rage.math.RampValueUnclamped(t, this.DisplayMin, this.DisplayMax, this.PhaseMin, this.PhaseMax);
        }

        /// <summary>
        /// Convert from phase value to pixel value in the window (where the PhaseViewMin should be near 0, PhaseViewMax near PixelWidth)
        /// i.e. if I need to draw a line at phase N, which pixel should I draw the line on 
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public double PhaseToPixel(double t)
        {
            return rage.math.RampValueUnclamped(t, this.PhaseViewMin, this.PhaseViewMax, EndInsetPixels, PixelWidth - EndInsetPixels);
        }

        /// <summary>
        /// Convert from a phase value to an absolute pixel value (where PhaseMin is at 0)
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public double PhaseToAbsPixel(double t)
        {
            double pixelScale = PixelWidth / PhaseViewRange; // pixels / phase
            return rage.math.RampValueUnclamped(t, PhaseMin, PhaseMax, 0, (PhaseMax - PhaseMin) * pixelScale);
        }

        /// <summary>
        /// Convert from pixel to phase value
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public double PixelToPhase(double t)
        {
            return rage.math.RampValueUnclamped(t, EndInsetPixels, PixelWidth - EndInsetPixels, this.PhaseViewMin, this.PhaseViewMax);
        }

        public double AbsPixelToPhase(double t)
        {
            double pixelScale = PixelWidth / PhaseViewRange; // pixels / phase
            return rage.math.RampValueUnclamped(t, 0, (PhaseMax - PhaseMin) * pixelScale, PhaseMin, PhaseMax);
        }

        /// <summary>
        /// Convert from display value to pixel
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public double DisplayToPixel(double t)
        {
            return rage.math.RampValueUnclamped(t, this.DisplayViewMin, this.DisplayViewMax, EndInsetPixels, PixelWidth - EndInsetPixels);
        }

        /// <summary>
        /// Convert from pixel to display value
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public double PixelToDisplay(double t)
        {
            return rage.math.RampValueUnclamped(t, EndInsetPixels, PixelWidth - EndInsetPixels, this.DisplayViewMin, this.DisplayViewMax);
        }

        /// <summary>
        /// Given an interval, computes the largest multiple of that interval which is
        /// smaller than the view min (in display units). I.e. if we wanted to draw
        /// a text label every 5 units, computes which multiple of 5 we should start with.
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public double GetIntervalViewStart(double interval)
        {
            return Math.Floor(this.DisplayViewMin / interval) * interval;
        }

        /// <summary>
        /// Given an interval, computes the smallest multiple of that interval which is
        /// greater than the view max (in display units). I.e. if we wanted to draw
        /// a text label every 5 units, computes which multiple of 5 we should stop with.
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public double GetIntervalViewEnd(double interval)
        {
            return Math.Ceiling(DisplayViewMax / interval) * interval;
        }

        #endregion

        #region Private Functions
        /// <summary>
        /// Determines the proper interval based on the display settings and minimum spacing
        /// </summary>
        /// <param name="intervalTable"></param>
        /// <param name="spacing"></param>
        /// <returns></returns>
        private double FindInterval(double[] intervalTable, double spacing)
        {
            double range = this.DisplayViewRange;
            double maxNum = (PixelWidth - 2 * EndInsetPixels) / spacing;
            maxNum = Math.Max(maxNum, 1.0);
            foreach (double testInter in intervalTable)
            {
                if (range / testInter <= maxNum)
                {
                    return testInter;
                }
            }

            return range;
        }

        /// <summary>
        /// Updates the label and tick intervals based on the current display settings.
        /// </summary>
        private void UpdateIntervals()
        {
            LabelInterval = FindInterval(s_LabelIntervals, _MinLabelSpacing);
            TickInterval = FindInterval(s_TickIntervals, _MinTickSpacing);
        }
        #endregion

    }
}
