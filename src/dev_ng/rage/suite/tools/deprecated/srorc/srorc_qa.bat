@ECHO Run viewfrag on PC with -rscfile=$/fragments/stagecoach.wft -file=$/fragments/stagecoach/entity.type -fragrsc -noregen
@ECHO and run viewfrag on Xenon with -rscfile=$/fragments/stagecoach.xft -file=$/fragments/stagecoach/entity.type -fragrsc -noregen
@ECHO and THEN run viewfrag on PS3 with -rscfile=$/fragments/stagecoach.cft -file=$/fragments/stagecoach/entity.type -fragrsc -noregen
@ECHO and run viewfrag on PC with -rscfile=$/fragments/env_sheet.wft -file=$/fragments/env_sheet/env_sheet.type -fragrsc -noregen
@ECHO and run viewfrag on Xenon with -rscfile=$/fragments/env_sheet.xft -file=$/fragments/env_sheet/env_sheet.type -fragrsc -noregen
@ECHO and THEN run viewfrag on PS3 with -rscfile=$/fragments/env_sheet.cft -file=$/fragments/env_sheet/env_sheet.type -fragrsc -noregen
@ECHO.
@ECHO You should see either the files load correctly OR a resource version assert, otherwise report a FAIL.
@PAUSE
@ECHO Make sure Win32 RscBeta build of srorc is up-to-date, and check out the *ft and *td files in Perforce.
@PAUSE

set SRORC_EXE=win32_rscbeta_2008\srorc_win32_rscbeta_2008.exe
%SRORC_EXE% stagecoach\entity.type stagecoach.wft -path %RAGE_ASSET_ROOT%\fragments -platform=pc  -shaderpath=%RAGE_ASSET_ROOT% -autotexdict
%SRORC_EXE% stagecoach\entity.type stagecoach.xft -path %RAGE_ASSET_ROOT%\fragments -platform=xenon -shaderpath=%RAGE_ASSET_ROOT% -autotexdict
%SRORC_EXE% stagecoach\entity.type stagecoach.cft -path %RAGE_ASSET_ROOT%\fragments -platform=ps3 -shaderpath=%RAGE_ASSET_ROOT% -autotexdict
%SRORC_EXE% env_sheet\env_sheet.type env_sheet.wft -path %RAGE_ASSET_ROOT%\fragments -platform=pc  -shaderpath=%RAGE_ASSET_ROOT% -autotexdict
%SRORC_EXE% env_sheet\env_sheet.type env_sheet.xft -path %RAGE_ASSET_ROOT%\fragments -platform=xenon -shaderpath=%RAGE_ASSET_ROOT% -autotexdict
%SRORC_EXE% env_sheet\env_sheet.type env_sheet.cft -path %RAGE_ASSET_ROOT%\fragments -platform=ps3 -shaderpath=%RAGE_ASSET_ROOT% -autotexdict
@ECHO Now run viewfrag again on PC with -rscfile=$/fragments/stagecoach.wft -file=$/fragments/stagecoach/entity.type -fragrsc -noregen
@ECHO and run viewfrag on Xenon with -rscfile=$/fragments/stagecoach.xft -file=$/fragments/stagecoach/entity.type -fragrsc -noregen
@ECHO and THEN run viewfrag on PS3 with -rscfile=$/fragments/stagecoach.cft -file=$/fragments/stagecoach/entity.type -fragrsc -noregen
@ECHO and run viewfrag on PC with -rscfile=$/fragments/env_sheet.wft -file=$/fragments/env_sheet/env_sheet.type -fragrsc -noregen
@ECHO and run viewfrag on Xenon with -rscfile=$/fragments/env_sheet.xft -file=$/fragments/env_sheet/env_sheet.type -fragrsc -noregen
@ECHO and THEN run viewfrag on PS3 with -rscfile=$/fragments/env_sheet.cft -file=$/fragments/env_sheet/env_sheet.type -fragrsc -noregen
@ECHO.
@ECHO Once all tests pass, check in the newly generated *ft and *td resource files.
