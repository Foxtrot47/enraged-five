// 
// /srorc.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if !__RESOURCECOMPILER
#error "Only Rsc platform will will work properly."
#endif

//  set off the bank widgets for the rmptfx so resourcing doesn't 
//  use the fat structures
//  need the memory for big particle files
#define SIMPLE_HEAP_SIZE	(248*1024)

#include "system/main.h"
#include "system/param.h"

#include "crclip/clip.h"
#include "crclip/clipdictionary.h"
#include "crextra/expressions.h"
#include "crmetadata/property.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "crparameterizedmotion/parameterizedmotiondictionary.h"
#include "event/manager.h"
#include "event/player.h"
#include "event/set.h"
#include "event/timeline.h"
#include "eventtypes/debug.h"
#include "eventtypes/ptfx.h"
#include "fragment/cache.h"
#include "fragment/eventplayer.h"
#include "fragment/instance.h"
#include "fragment/manager.h"
#include "fragment/tune.h"
#include "fragment/type.h"
#include "fragment/resourceversions.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "phcore/config.h"
#include "phcore/materialmgrimpl.h"
#include "rmptfx/ptxfxlist.h"
#include "rmptfx/ptxmanager.h"
#include "vectormath/vec3v.h"
#include "softrasterizer\occludeModel.h"
// #include "speedtree/treedata.h"

#include "grass/field.h"
#include "grass/staticparticles.h"

#include "offlinersc/offlinersc.h"

using namespace rage;

PARAM(fragtune,"[srorc] The folder to use to look for tuning data");
PARAM(DisableLods,"[srorc] Disable leaf lods for speedtree");
PARAM(BatchSize,"[srorc] Maximum Batch Size for grass" );
PARAM(seed,"[srorc] the seed for the speedtree asset");
PARAM(size,"[srorc] the size of the speedtree");
PARAM(sizeVariance,"[srorc] the variance in size for the speedtree object");
PARAM(referenceAllTextures, "[srorc] All speedtree textures are referenced rather than resourced");
PARAM(dumpVersionNumbers,"[srorc] outputs the current version numbers of supported assets in text file" );
PARAM(billboardsize,"[srorc] sets the billboard texture size for the tree" );
PARAM(billboardshrinksize,"[srorc] sets the size at which an object is treated as being small" );
PARAM(swapAxis,"[srorc] will switch z and y axis's on grass" );
PARAM(compactanim,"[rorc] generate compact animation resource, rather than just packed");
PARAM(basenamekeys, "[rorc] use base names (without relative paths) for clip/pm dictionary keys");

PARAM(ptxtex, "[srorc] will put textures (from a texlist file) into the particle resource");
PARAM(ptxmod, "[srorc] will put models (from a modellist file) into the particle resource");

PARAM(ptxresave, "[srorc] Will save particle effects to a temp dir before resourcing (performing any file conversions, shader fixups, etc. as necessary");

int Main() {
	INIT_PARSER;
	PARSER.Settings().SetFlag(parSettings::USE_TEMPORARY_HEAP, true);

	const char* versFile;

	if ( PARAM_dumpVersionNumbers.Get( versFile ) )
	{
		VersionFile	vers( versFile);
		// vers.Output( "speedTree", speedTreeData::RORC_VERSION );
		vers.Output( "grass", FieldList::RORC_VERSION );
		vers.Output( "fxlist", ptxFxList::RORC_VERSION);
		vers.Output( "cliplist", crClipDictionary::RORC_VERSION);
		vers.Output( "pmlist", crpmParameterizedMotionData::RORC_VERSION);
		vers.Output( "exprlist", crExpressions::RORC_VERSION);

		SHUTDOWN_PARSER;
		return 0;
	}

	if (sysParam::GetArgCount() < 3)
		Quitf("Usage:\nsrorc entity.type outputFile.xeck [-path=xxx -texdict=xxx ]");

	const char *inputExt = strrchr(sysParam::GetArg(1),'.');
	if (!inputExt)
		Quitf("Input file must have extension so we can determine file type.");
	bool needGraphics = !stricmp(inputExt,".type") || !stricmp(inputExt,".spt") || !stricmp(inputExt,".grs") || !stricmp(inputExt,".fxlist");
	bool needShaders = !stricmp(inputExt,".type") || !stricmp(inputExt,".spt") || !stricmp(inputExt,".grs") || !stricmp(inputExt,".fxlist");
	OffLineResourceCreator	resourcer(needGraphics, needShaders);
	

	if (resourcer.HasExtension( "type")) {

		phConfig::EnableRefCounting();

		// Init the event manager
		INIT_EVENTMGR;

		crProperty::InitClass();

		// Register the types of events that we recognize
		evtAnimPlayer::RegisterPlayerClass();
		fragContinuousEventPlayer::RegisterPlayerClass();
		fragBreakEventPlayer::RegisterPlayerClass();
		fragCollisionEventPlayer::RegisterPlayerClass();

		EVENTMGR.RegisterTypeInstancePair<evtTypePrintf, evtInstancePrintf>("Debug", "printf");
		EVENTMGR.RegisterTypeInstancePair<evtTypeDrawTick, evtInstanceDrawTick>("Debug", "Draw Tick Mark");
		EVENTMGR.RegisterTypeInstancePair<evtTypePtfx, evtInstancePtfx>("Debug", "RmPtfx");

		//		EVENTMGR.RegisterEventType("sound", new evtTypeSoundNamed);

		// Create the fragment tuning manager
		fragTune::Instantiate();
		const char* tunepath = "$/tune/types/fragments";
		PARAM_fragtune.Get(tunepath);
		FRAGTUNE->SetForcedTuningPath(tunepath);
		FRAGTUNE->Load();

		// Create and load the materials into the material manager
		phMaterialMgrImpl<phMaterial>::Create();
		ASSET.PushFolder("$/physics/materials");
		MATERIALMGR.Load();
		ASSET.PopFolder();

		// Create physics managers
		phLevelNew* level = rage_new phLevelNew;
		Vec3V worldMin(-1e5f, -1e5f, -1e5f);
		Vec3V worldMax( 1e5f,  1e5f,  1e5f);
		level->SetExtents(worldMin, worldMax);
		level->SetMaxObjects(500);
		level->SetMaxActive(500);
		level->SetNumOctreeNodes(500);
		level->Init();
		level->SetActiveInstance();
		phSimulator::InitClass();
		phSimulator* sim = rage_new phSimulator;
		phSimulator::InitParams params;
		params.maxInstBehaviors = 16;
		params.maxManagedColliders = 4;
		params.maxConstraints = 32;
		sim->Init(level, params);
		sim->SetActiveInstance();
		rage_new fragManager;

		while( resourcer.Next())
		{
			fragType *type = fragType::Load( resourcer.GetInput());
			Assert(type);

	#if __ASSERT
			size_t available = sysMemAllocator::GetCurrent().GetMemoryAvailable();
	#endif

			// Make a inst and put it in the cache, to get an estimate of the size of cache data it needs
			{
				fragInst dummyInst(type, M34_IDENTITY);
				dummyInst.Insert();
				if (!dummyInst.GetCached())
				{
					dummyInst.PutIntoCache(); 
				}
                if (fragCacheEntry* entry = dummyInst.GetCacheEntry())
                {
                    entry->LazyArticulatedInit();
                }
				dummyInst.Remove();
			}

			// Be sure that the dummy inst hasn't allocated any resource memory
			Assert(available == sysMemAllocator::GetCurrent().GetMemoryAvailable());
	
			resourcer.Resource( type , "#ft"); 
		}

		// Tear everything down
		delete sim;
		level->Shutdown();
		delete level;
		phSimulator::ShutdownClass();
		delete FRAGMGR;
		fragTune::Destroy();
		evtAnimPlayer::UnregisterPlayerClass();
		fragContinuousEventPlayer::UnregisterPlayerClass();
		fragBreakEventPlayer::UnregisterPlayerClass();
		fragCollisionEventPlayer::UnregisterPlayerClass();

		SHUTDOWN_EVENTMGR;

		MATERIALMGR.Destroy();
	}
	/* else if ( resourcer.HasExtension( "spt") )
	{
		int seed = 0;
		float size = -1.0f;
		float sizeVariance = -1.0f;
		int defaultBillboardSize;
		float shrinkSize;

		// do setup
		PARAM_seed.Get( seed );
		PARAM_size.Get( size );
		PARAM_sizeVariance.Get( sizeVariance );
		if ( PARAM_referenceAllTextures.Get() )
		{
			speedTreeData::SetReferenceAllTextures();
		}
		speedTreeData::SetUseMipMapping( true );
		speedTreeData::DisableLeafLods( PARAM_DisableLods.Get() );

		if ( PARAM_billboardsize.Get( defaultBillboardSize ) )
		{
			speedTreeData::SetDefaultBillboardSize( defaultBillboardSize);
		}
		if ( PARAM_billboardshrinksize.Get(shrinkSize ))
		{
			speedTreeData::SetBillboardShrinkSize( shrinkSize);		// use smaller rendertargets if size is less
		}

		while( resourcer.Next())
		{
			speedTreeData*	tree = rage_new speedTreeData;
			
			tree->Create( resourcer.GetInput(), seed, size, sizeVariance, true, true, false );
			resourcer.Resource( tree , "#st"); 
		}

		CSpeedTreeRT::SetNumWindMatrices(0);
	} */
	else if ( resourcer.HasExtension( "grs") )
	{
		int MaxBatchSize = 2000;
		PARAM_BatchSize.Get( MaxBatchSize );

		grassField::SetupStaticIndexBuffer( 6, MaxBatchSize  );

		while( resourcer.Next())
		{
			sysMemStartTemp();
			atPtr<ParticleList>		particles = ParticleList::Create( resourcer.GetInput(), "grs");
			sysMemEndTemp();

			if ( PARAM_swapAxis.Get() )
			{
				for ( Particle* p = particles->begin(); p != particles->end(); ++p )
				{
					p->Position.y = -p->Position.y ;
					std::swap( p->Position.z, p->Position.y );
					
				}
			}
			FieldList*	 fields = CreateFieldList( particles, MaxBatchSize );
			resourcer.Resource( fields , "#grs"); 
		}
	}
	else if (resourcer.HasExtension("fxlist"))
	{
		// temporarily set g_SysPlatform to PC so we load the embedded PC shaders (the only ones that exist in this exe)
		char oldPlatform = g_sysPlatform;
		g_sysPlatform = platform::WIN32PC;

		if (PARAM_ptxresave.Get())
		{
			ptxManager::InitClass();
			RMPTFXMGR.CreatePointPool(1);
			RMPTFXMGR.CreateEffectInstPool(1);
//			RMPTFXMGR.CreateDrawList();
			RMPTFXMGR.LoadFxList(resourcer.GetInput(), 0);
            RMPTFXMGR.SetAssetPath("C:\\temp"); // keep it set here so that's where subsequent loads come from
			RMPTFXMGR.SaveAll();
			ptxManager::ShutdownClass();
		}

		ptxManager::InitClass();

		if (PARAM_ptxresave.Get())
		{
			RMPTFXMGR.SetAssetPath("C:\\temp"); // keep it set here so that's where subsequent loads come from
		}

		g_sysPlatform = oldPlatform;
		RMPTFXMGR.CreatePointPool(1);
		RMPTFXMGR.CreateEffectInstPool(1);
//		RMPTFXMGR.CreateDrawList();

		char baseName[RAGE_MAX_PATH];
		strncpy(baseName, resourcer.GetInput(), 128);
		*strrchr(baseName, '.') = '\0';

		const char* texturePath = NULL;
		PARAM_ptxtex.Get(texturePath);
		if (texturePath && texturePath[0] == '\0')
		{
			texturePath = NULL;
		}

		const char* modelPath = NULL;
		PARAM_ptxmod.Get(modelPath);
		if (modelPath && modelPath[0] == '\0')
		{
			modelPath = NULL;
		}


		while( resourcer.Next())
		{
			ptxFxList* fxl = rage_new ptxFxList();

			fxl->Load(baseName, modelPath, texturePath);

			resourcer.Resource( fxl , "#pfl"); 
		}

		ptxManager::ShutdownClass();

	}
	else if (resourcer.HasExtension("expr"))
	{
		Assert(0);  // crExpressions now needs to be a derive from pgBase
/*		crExpressions::InitClass();

		while ( resourcer.Next() )
		{
			crExpressions *expressions = rage_new crExpressions;
			// Load the expressions
			ValidateResult(expressions->Load(resourcer.GetInput(), true));

			resourcer.Resource( expressions, "#ex" );
		}
		
		crExpressions::ShutdownClass();
*/
	}
	else if (resourcer.HasExtension("exprlist"))
	{
		crExpressions::InitClass();

		while ( resourcer.Next() )
		{
			crExpressionsDictionary* dictionary = NULL;

			ValidateResult(((dictionary = crExpressions::AllocateAndLoadDictionary(resourcer.GetInput())) != NULL));

			resourcer.Resource( dictionary, "#edt", crExpressions::RORC_VERSION );
		}

		crExpressions::ShutdownClass();
	}
	else if (resourcer.HasExtension("cliplist"))
	{
		const bool baseNameKeys = PARAM_basenamekeys.Get();

		crClip::InitClass();

		crAnimLoaderBasic loader(PARAM_compactanim.Get());

		while(resourcer.Next())
		{
			crClipDictionary* dictionary = rage_new crClipDictionary;
			dictionary->AddRef();

			// Load the clip
			ValidateResult(dictionary->LoadClips(resourcer.GetInput(), NULL, &loader, baseNameKeys));

			resourcer.Resource( dictionary, "#cdt" );
		}

		crClip::ShutdownClass();
	}
	else if (resourcer.HasExtension("pmlist"))
	{
		const bool baseNameKeys = PARAM_basenamekeys.Get();

		crClip::InitClass();
		crpmParameterizedMotion::InitClass();

		crAnimLoaderBasic loader(PARAM_compactanim.Get());

		while(resourcer.Next())
		{
			crpmParameterizedMotionDictionary* dictionary = rage_new crpmParameterizedMotionDictionary;
			dictionary->AddRef();

			// Load the clip
			ValidateResult(dictionary->LoadParameterizedMotions(resourcer.GetInput(), NULL, NULL, &loader, baseNameKeys));

			resourcer.Resource( dictionary, "#pdt" );
		}

		crpmParameterizedMotion::ShutdownClass();
		crClip::ShutdownClass();

	}
#if 0
	else if (resourcer.HasExtension("mesh"))
	{
		while ( resourcer.Next() )
		{
			OccludeModelList* occList = rage_new OccludeModelList();
			float bitPrecision = OCCLUDE_MODEL_DEFAULT_ERROR;
			LoadOccludeModel( occList->m_models,resourcer.GetInput(), ScalarVFromF32(bitPrecision));
			resourcer.Resource( occList, "#om" );
		}
	}
#endif
	else
		Quitf("Unable to open input type file '%s'", resourcer.GetInput() );


	SHUTDOWN_PARSER;
	return resourcer.HasErrors() ? 1 : 0;
}
