#include "application.h"

#include "actiontree/Condition.h"
#include "actiontree/textparser.h"

#include "system/endian.h"

#ifndef PI
const float PI = 3.14159265359f;
#endif

//--------------------------------------------------------------------------------
//  Units
//--------------------------------------------------------------------------------

float Units::sm_TimeUnitsConversion[kNumTimeUnits] =
{
	1.0f,
	30.0f
};

const char * Units::sm_TimeUnitsOptionNames[kNumTimeUnits] =
{
    "seconds",
	"30Hz frames"
};

float Units::sm_LengthUnitsConversion[kNumLengthUnits] =
{
    1.0f,
	100.0f
};

const char * Units::sm_LengthUnitsOptionNames[kNumLengthUnits] =
{
    "meters",
	"centimeters"
};

float Units::sm_VelocityUnitsConversion[kNumVelocityUnits] =
{
    1.0f,
    1/30.0f
};

const char * Units::sm_VelocityUnitsOptionNames[kNumVelocityUnits] =
{
    "meters/second",
    "meters/30Hz frame"
};

float Units::sm_AccelUnitsConversion[kNumAccelUnits] =
{
	1.0f,
	1.0f/900.0f
};

const char * Units::sm_AccelUnitsOptionNames[kNumAccelUnits] =
{
	
	"meters/second^2",
	"meters/30Hz frame^2"
};

float Units::sm_AngleUnitsConversion[kNumAngleUnits] =
{
	1.0f,
	180.0f/PI
};

const char * Units::sm_AngleUnitsOptionNames[kNumAngleUnits] =
{
	"radians",
	"degrees"
};

BEGIN_DATAMEMBERS_NOT_TRACK(Units)

    ENUM_DATAMEMBER(Units, "timeUnits", GetTimeUnits, SetTimeUnits, sm_TimeUnitsOptionNames, kNumTimeUnits)
	ENUM_DATAMEMBER(Units, "lengthUnits", GetLengthUnits, SetLengthUnits, sm_LengthUnitsOptionNames, kNumLengthUnits)
	ENUM_DATAMEMBER(Units, "velocityUnits", GetVelocityUnits, SetVelocityUnits, sm_VelocityUnitsOptionNames, kNumVelocityUnits)
	ENUM_DATAMEMBER(Units, "accelUnits", GetAccelUnits, SetAccelUnits, sm_AccelUnitsOptionNames, kNumAccelUnits)
	ENUM_DATAMEMBER(Units, "angleUnits", GetAngleUnits, SetAngleUnits, sm_AngleUnitsOptionNames, kNumAngleUnits)

END_DATAMEMBERS(Units)


Units::Units() 
:
m_TimeUnits(TU_30TH_Seconds),
m_LengthUnits(LU_Meters),
m_AngleUnits(AU_Degrees),
m_VelocityUnits(VU_MetersPerSecond),
m_AccelUnits(VU_MetersPerSecondSquared),
m_AngleSpeedUnits(ASU_RadiansPerSecond)
{

}

Units::~Units()
{

}

float Units::GetConverted
( 
    IFloatDataMember*   floatDataMember, 
    IDataMembers*       dataMembersInstance 
)
{
    DataMember::TYPE type = floatDataMember->GetType();
    
    float value = floatDataMember->GetFloat(dataMembersInstance);

    switch(type)
    {
    case DataMember::DM_TIME:
    case DataMember::DM_TRACK_TIME:
        {
            return(getConverted( value, 0, m_TimeUnits, sm_TimeUnitsConversion, kNumTimeUnits ));
        }
        break;

    case DataMember::DM_ANGLE:
        {
            return(getConverted( value, 0, m_AngleUnits, sm_AngleUnitsConversion, kNumAngleUnits));
        }
        break;

	case DataMember::DM_LENGTH:
		{
			return(getConverted( value, 0, m_LengthUnits, sm_LengthUnitsConversion, kNumLengthUnits));
		}
		break;

	case DataMember::DM_VELOCITY:
		{
			return(getConverted( value, 0, m_VelocityUnits, sm_VelocityUnitsConversion, kNumVelocityUnits));
		}
		break;

	case DataMember::DM_ACCELERATION:
		{
			return(getConverted( value, 0, m_AccelUnits, sm_AccelUnitsConversion, kNumAccelUnits));
		}
		break;

    default:
        {
            return(value);
        }
        break;
    }
}

void Units::SetConverted
(   
    IFloatDataMember* floatDataMember, 
    IDataMembers* dataMembersInstance, 
    float displayValue 
)
{
    DataMember::TYPE type = floatDataMember->GetType();
    float storedValue = displayValue;
    switch(type)
    {
    case DataMember::DM_TIME:
    case DataMember::DM_TRACK_TIME:
        {
            storedValue = getConverted(displayValue, m_TimeUnits, 0, sm_TimeUnitsConversion, kNumTimeUnits);
        }
        break;

    case DataMember::DM_ANGLE:
        {
            storedValue = getConverted( displayValue, m_AngleUnits, 0, sm_AngleUnitsConversion, kNumAngleUnits);
        }
        break;

	case DataMember::DM_LENGTH:
		{
			storedValue = getConverted( displayValue, m_LengthUnits, 0, sm_LengthUnitsConversion, kNumLengthUnits);
		}
		break;

	case DataMember::DM_VELOCITY:
		{
			storedValue = getConverted( displayValue, m_VelocityUnits, 0, sm_VelocityUnitsConversion, kNumVelocityUnits);
		}
		break;

	case DataMember::DM_ACCELERATION:
		{
			storedValue = getConverted( displayValue, m_AccelUnits, 0, sm_AccelUnitsConversion, kNumAccelUnits);
		}
		break;

    default:
        break;
    }

    //
    // Now set the Value
    //
    floatDataMember->SetFloat(dataMembersInstance, storedValue);
}


const char * Units::GetUnitsString(IFloatDataMember* floatDataMember)
{
	const char * retval = "";

	switch(floatDataMember->GetType())
	{
	case DataMember::DM_TIME:
	case DataMember::DM_TRACK_TIME:
		retval = sm_TimeUnitsOptionNames[m_TimeUnits];
		break;

	case DataMember::DM_ANGLE:
		retval = sm_AngleUnitsOptionNames[m_AngleUnits];
		break;

	case DataMember::DM_LENGTH:
		retval = sm_LengthUnitsOptionNames[m_LengthUnits];
		break;

	case DataMember::DM_VELOCITY:
		retval = sm_VelocityUnitsOptionNames[m_VelocityUnits];
		break;

	case DataMember::DM_ACCELERATION:
		retval = sm_AccelUnitsOptionNames[m_AccelUnits];
		break;

	default:
		break;
	}

	return retval;
}


float Units::GetConverted( float value, TimeUnits from,  TimeUnits to)
{
    return(getConverted( value, (int)from, (int)to, sm_TimeUnitsConversion, kNumTimeUnits));
}

struct SecondsToTimeUnitsStruct
{
	const char* m_name;
	float m_SecondsToTimeUnits;
};

static SecondsToTimeUnitsStruct s_SecondsToTimeUnits[] =
{
	{"seconds",			1.0f	},
	{"1/30 seconds",	30.0f	}
};


//--------------------------------------------------------------------------------
//  ActionNodeIterator...
//--------------------------------------------------------------------------------
class ActionNodeIterator
{
public:
    ActionNodeIterator(ActionNode* startNode);
	virtual ~ActionNodeIterator() {}
    virtual void First();
    virtual void Next();
    virtual bool IsDone() const;
    virtual ActionNode* CurrentItem() const {return(m_CurrentItem);}

private:

    ActionNode* next();

    struct ParentChild
    {
        ActionNode* parent;
        int childIndex;
    };
    std::vector<ParentChild> m_SearchStack;

    ActionNode* m_First;
    ActionNode* m_CurrentSearchPoint;
    ActionNode* m_CurrentItem;
    int m_CurrentChildIndex;
};

ActionNodeIterator::ActionNodeIterator(ActionNode* startNode)
:
m_First(startNode),
m_CurrentSearchPoint(startNode),
m_CurrentItem(startNode),
m_CurrentChildIndex(0)
{
    //
    // dont expect it to go anywhere near 40levels deep
    //
    m_SearchStack.reserve(40);
}

void ActionNodeIterator::First()
{
    m_SearchStack.clear();
    m_CurrentItem = m_First;

    ParentChild searchPoint;
    searchPoint.parent = m_CurrentItem;
    searchPoint.childIndex = 0;
    m_SearchStack.push_back(searchPoint);
}

void ActionNodeIterator::Next()
{
    //
    // See if we need to pop back in the tree
    //
    bool popingBack = false;
    do
    {
        int lastElement = m_SearchStack.size() - 1;
        if(lastElement<0) return;
        ParentChild searchPointBackTrack = m_SearchStack[lastElement];
        int childCount = searchPointBackTrack.parent->GetChildCount();
        
        //
        // See if geting the next child would exceed total children
        //
        if(childCount <= searchPointBackTrack.childIndex)
        {
            popingBack = true;
            m_SearchStack.pop_back();
        }
        else
        {
            popingBack = false;
        }
        
    }while(popingBack);

    //
    // Are we finished???
    //
    if(IsDone())
    {
        m_CurrentItem = NULL;
        return;
    }
    
    //
    // Here we should be able to freely get the next child
    //
    int lastElement = m_SearchStack.size() - 1;
    if(lastElement<0) return;
    ParentChild searchPointBackTrack = m_SearchStack[lastElement];

    int childIndex = searchPointBackTrack.childIndex;
    int childCount = searchPointBackTrack.parent->GetChildCount();

    if(childIndex>childCount)
    {
        Assert( 0 && "The child search is wrong... bailing NEXT call");
        m_CurrentItem = NULL;
        return;
    }
    
    ActionNode* child = searchPointBackTrack.parent->GetChild(childIndex);

    //
    // This is the current
    //
    m_CurrentItem = child;
   
    //
    // Next child... and remember
    //
    searchPointBackTrack.childIndex++;

    //
    // Since we'll modify this node then push it back...
    // A reference would be better
    //
    m_SearchStack.pop_back(); 
    m_SearchStack.push_back(searchPointBackTrack);


    //
    // Now push this sucker on the stack
    //
    ParentChild newSearch;
    newSearch.parent = child;
    newSearch.childIndex = 0;
    m_SearchStack.push_back(newSearch);
}

bool ActionNodeIterator::IsDone() const
{
    int traversalDepth = m_SearchStack.size();
    if(traversalDepth == 0)
    {
        return(true);
    }

    return(false);
}

/*
class ActionNodeVisitor
{
public:
    virtual void visit(ActionNode*) = 0;
};

class ActionNodeVisitor
{
public:
    virtual void visit(ActionNode*) = 0;
};

void TraverseDo(ActionNode *node, ActionNodeVisitor& visitor)
{
    //
    // Here is the "current"
    //
    visitor.visit(node); //->DoSomething();

    for(int i=0;i<node->GetChildCount();i++)
    {
        ActionNode* child = node->GetChild(i);
        TraverseDo(child, visitor);
    }
}
*/

//--------------------------------------------------------------------------------
//  TrackReference
//--------------------------------------------------------------------------------
/*
#include <stdlib.h> 
#include <boost/regex.hpp>
#include <string> 
#include <iostream>

using namespace boost; 

regex expression("([0-9]+)(\\-| |$)(.*)"); 

regex expressionName("ActionTree(.*)"); 
regex expressionName2("ActionTree/(.*)/ass"); 

const char* testActionTreeName[] = 
{
    "ActionTree/Fuck",
    "ActionTree/FuckYou",
    "ActionTre",
    "ActionTre/Fuck",
    "ActionTree/Fuck/you/in/the/ass",
};

// process_ftp: 
// on success returns the ftp response code, and fills 
// msg with the ftp response message. 
int test() //(const char* response, std::string* msg) 
{ 
   cmatch what; 
   int i;
   for(i=0;i<5;i++)
   {
    if(regex_match(testActionTreeName[i], what, expressionName))
    {
        printf("Yes: ActionTree(.*):%s\n", testActionTreeName[i]);
    }
    else
    {
        printf("No: ActionTree(.*):%s\n", testActionTreeName[i]);
    }
   }
   for(i=0;i<5;i++)
   {
    if(regex_match(testActionTreeName[i], what, expressionName2))
    {
        printf("Yes: ActionTree(.*):%s\n", testActionTreeName[i]);
    }
    else
    {
        printf("No: ActionTree(.*):%s\n", testActionTreeName[i]);
    }
   }
   return(1);
}
*/

/*
#include <stdlib.h> 
#include <boost/regex.hpp>
#include <string> 
#include <iostream>

using namespace boost;
*/

bool Match(const char* stringA, const char* stringB)
{
    if(stringA == NULL || stringB == NULL)
    {
        //
        // no string defaults to match ALL
        //
        return(true);
    }

    bool matched = (stricmp(stringA, stringB) == 0);
/*
    cmatch what;
    regex stringAExpressionName(stringA);
    matched = regex_match(stringB, what, stringAExpressionName);

    if(!matched)
    {
        regex stringBExpressionName(stringB);
        matched = regex_match(stringA, what, stringBExpressionName);
    }
*/
    return(matched);
}


bool MatchIStringDataMemberValue
(
    IDataMembers* instance,
    IStringDataMember* stringDataMember, 
    const char* stringValue
)
{
    const char* stringDataMemberValue = stringDataMember->GetString(instance);    
    return(Match(stringValue, stringDataMemberValue));
}

bool MatchIEnumDataMemberValue
(
 IDataMembers* instance,
 IEnumDataMember* enumDataMember, 
 const char* stringValue
 )
{
    int enumInt = enumDataMember->GetEnum(instance);   
    const char* enumDataMemberValue = enumDataMember->GetEnumName(enumInt);    
    return(Match(stringValue, enumDataMemberValue));
}

bool MatchINodeReferenceDataMember
(
    IDataMembers* instance,
    INodeReferenceDataMember* nodeReferenceDataMember, 
    const char* fullPath
)
{
    const char* nodeReferenceFullPathValue = nodeReferenceDataMember->GetFullPath(instance);    
    if(nodeReferenceFullPathValue)
    {
        return(Match(fullPath, nodeReferenceFullPathValue));
    }
    else
    {
        return(NULL);
    }
}

bool MatchIFloatDataMemberValue
(
    IDataMembers* instance,
    IFloatDataMember* floatDataMember, 
    float floatValue, 
    Conditional::Operators conditionalOperator
 )
{
    float floatDataMemberValue = floatDataMember->GetFloat(instance);
    return(Conditional::CompareFloat(floatDataMemberValue,floatValue, conditionalOperator));
}

bool MatchDataMemberValue
(
    IDataMembers* instance,
    DataMember* dataMember, 
    const char* stringValue, 
    float floatValue, 
    Conditional::Operators conditionalOperator
)
{

    DataMember::TYPE type = dataMember->GetType();
    
    switch(type)
    {
    case DataMember::DM_FLOAT:
    case DataMember::DM_TIME:
    case DataMember::DM_TRACK_TIME:
    case DataMember::DM_LENGTH:
    case DataMember::DM_VELOCITY:
    case DataMember::DM_ACCELERATION:
    case DataMember::DM_ANGLE:
        {
            IFloatDataMember* floatDataMember = dynamic_cast<IFloatDataMember*>(dataMember);
            if(floatDataMember)
            {
                return(MatchIFloatDataMemberValue(instance, floatDataMember, floatValue, conditionalOperator));
            }
            else
            {
                Assert(0 && "This should support a IFloatDataMember interface");
            }
        }
        break;
    case DataMember::DM_ENUM:
        {
            IEnumDataMember* enumDataMember = dynamic_cast<IEnumDataMember*>(dataMember);
            if(enumDataMember)
            {
                return(MatchIEnumDataMemberValue(instance, enumDataMember, stringValue));
            }
            else
            {
                Assert(0 && "This should support a IEnumDataMember interface");
            }
            

        }
        break;
    case DataMember::DM_STRING:
    case DataMember::DM_COMMAND:
        {
            IStringDataMember* stringDataMember = dynamic_cast<IStringDataMember*>(dataMember);
            if(stringDataMember)
            {
                return(MatchIStringDataMemberValue(instance, stringDataMember, stringValue));
            }
            else
            {
                Assert(0 && "This should support a IStringDataMember interface");
            }
        }
        break;

    case DataMember::DM_NODEREF:
        {
            INodeReferenceDataMember* nodeRefDataMember = dynamic_cast<INodeReferenceDataMember*>(dataMember);
            if(nodeRefDataMember)
            {
                return(MatchINodeReferenceDataMember(instance, nodeRefDataMember, stringValue));
            }
            else
            {
                Assert(0 && "This should support a INodeReferenceDataMember interface");
            }
        }
        break;

    default:
        break;
    }

    return(true);
}

bool MatchTrackAndDataMemberValue
(
    IDataMembers* instance,
    const char* IDataMembersName,
    bool  matchValue,
    const char* stringValue, 
    float floatValue, 
    Conditional::Operators conditionalOperator
)
{

    int numDataMembers = instance->GetNumMembers();
    for(int i=0;i<numDataMembers;i++)
    {
        DataMember* dataMember = instance->GetMember(i);
        if(Match(IDataMembersName, dataMember->GetName()->GetString()))
        {
            if(matchValue)
            {
                bool matchMemberValue = MatchDataMemberValue(
                                                instance, 
                                                dataMember,
                                                stringValue,
                                                floatValue,
                                                conditionalOperator);
                if(matchMemberValue)
                {
                    return(true);
                }
            }
            else
            {
                return(true);
            }
        }
    }

    return(false);
}


TrackReference::TrackReference()
:
m_DataMembersMatch(NULL),
m_QueryValueMatch(false),
m_StringDataMembersValueMatch(NULL),
m_FloatDataMembersValueMatch(0.0f),
m_FloatDataMembersValueMatchCondition(Conditional::C_EQUAL),
m_TrackReferenceIndex(-1),
m_ConditionReferenceIndex(-1),
m_ActionNodeRef(0)
{

}

TrackReference::~TrackReference()
{
    delete [] m_DataMembersMatch;
    delete [] m_StringDataMembersValueMatch;
}

CName* TrackReference::GetName(void)
{
    Track* track = GetTrack();
    if(track)
    {
        return(track->GetName());
    }
    return(NULL);
}

const CName* TrackReference::GetName(void) const
{
	Track* track = GetTrack();
	if(track)
	{
		return(track->GetName());
	}
	return(NULL);
}

void TrackReference::SetName(const char* name)
{
    Track* track = GetTrack();
    if(track)
    {
        track->GetName()->SetString(name);
    }
}

Track* TrackReference::GetTrack() const
{
    if(m_TrackReferenceIndex>=0)
    {
        return(getReferenceTrack());
    }
    else
    if(m_ConditionReferenceIndex>=0)
    {
        return(getReferenceCondition());
    }

    return(NULL);
}

Track* TrackReference::GetTrack()
{
	if(m_TrackReferenceIndex>=0)
	{
		return(getReferenceTrack());
	}
	else
		if(m_ConditionReferenceIndex>=0)
		{
			return(getReferenceCondition());
		}

		return(NULL);
}

Track* TrackReference::getReferenceCondition(void) const
{
    if(m_ActionNodeRef)
    {
        ActionNode* actionNode = m_ActionNodeRef->GetActionNode();
        if(actionNode)
        {
            return((Track*)actionNode->GetCondition(m_ConditionReferenceIndex));
        }
    }

    return(NULL);
}

Track* TrackReference::getReferenceTrack(void) const
{
    if(m_ActionNodeRef)
    {
        ActionNode* actionNode = m_ActionNodeRef->GetActionNode();
        if(actionNode)
        {
            return(actionNode->GetTrack(m_TrackReferenceIndex));
        }
    }

    return(NULL);
}

int TrackReference::GetNumMembers(void)
{
    Track* refTrack = GetTrack();
    if(refTrack)
    {
        return(refTrack->GetNumMembers());
    }

    return(0);
}

DataMember* TrackReference::GetMember(int i)
{
    Track* refTrack = GetTrack();
    if(refTrack)
    {
        DataMember* dataMember = refTrack->GetMember(i);
        if(Match(m_DataMembersMatch, dataMember->GetName()->GetString()))
        {
            if(m_QueryValueMatch)
            {
                bool matchMemberValue = MatchDataMemberValue(
                                                refTrack, 
                                                dataMember,
                                                m_StringDataMembersValueMatch,
                                                m_FloatDataMembersValueMatch,
                                                m_FloatDataMembersValueMatchCondition);
                if(matchMemberValue)
                {
                    return(dataMember);
                }
            }
            else
            {
                return(dataMember);
            }
        }
    }
    return(NULL);
}

//--------------------------------------------------------------------------------
//  FindQuery
//--------------------------------------------------------------------------------
BEGIN_DATAMEMBERS_NO_BREAKPOINT(NodeReferenceFilterTrack)

	NODE_REFERENCE_DATAMEMBER(NodeReferenceFilterTrack, "ReferenceNode",  GetActionNodeReference,  SetActionNodeReference, GetActionNodeReferenceFullPath, IsActionNodeResolved)

END_DATAMEMBERS(NodeReferenceFilterTrack)

bool NodeReferenceFilterTrack::m_QueryConditions = true;
char* NodeReferenceFilterTrack::m_ConditionNamesMatch = NULL;
char* NodeReferenceFilterTrack::m_ConditionDataMembersMatch = NULL;

bool NodeReferenceFilterTrack::m_QueryTracks = true;
char* NodeReferenceFilterTrack::m_TrackNamesMatch = NULL;
char* NodeReferenceFilterTrack::m_TrackDataMembersMatch = NULL;

bool  NodeReferenceFilterTrack::m_QueryValueMatch = true;
char* NodeReferenceFilterTrack::m_StringDataMembersValueMatch = NULL;
float NodeReferenceFilterTrack::m_FloatDataMembersValueMatch = 0.0f;
Conditional::Operators NodeReferenceFilterTrack::m_FloatDataMembersValueMatchCondition = Conditional::C_EQUAL;


NodeReferenceFilterTrack::NodeReferenceFilterTrack()
{
    m_conditions.reserve(10);
    m_tracks.reserve(10);
}

NodeReferenceFilterTrack::~NodeReferenceFilterTrack()
{
    int conditionCount = m_conditions.size();
    for(int i=0;i<conditionCount;i++)
    {
        delete m_conditions[i];
    }
    m_conditions.clear();

    int trackCount = m_tracks.size();
    for(int i=0;i<trackCount;i++)
    {
        delete m_tracks[i];
    }
    m_tracks.clear();
}

void NodeReferenceFilterTrack::SetActionNode(ActionNode* node)
{
    NodeReferenceTrack::SetActionNode(node);

    //
    // Now build some proxy Tracks
    //
    if(m_QueryConditions)
    {
        int conditionCount = node->GetConditionCount();
        for(int i=0;i<conditionCount;i++)
        {
            Condition* condition = node->GetCondition(i);

            if(m_QueryConditions)
            {
                if(condition && Match(m_ConditionNamesMatch, condition->GetName()->GetString())) 
                {
                    bool matchTrackAndDataMemberValue = MatchTrackAndDataMemberValue(   condition,
                                                                                        m_ConditionDataMembersMatch,
                                                                                        m_QueryValueMatch,
                                                                                        m_StringDataMembersValueMatch, 
                                                                                        m_FloatDataMembersValueMatch, 
                                                                                        m_FloatDataMembersValueMatchCondition
                                                                                    );
                    if(matchTrackAndDataMemberValue)
                    {
                        TrackReference* trackRef = new TrackReference;
                        trackRef->SetActionNodeReference(&m_ActionNodeRef);
                        trackRef->SetConditionIndexReference(i);
                        trackRef->SetDataMembersMatch(m_ConditionDataMembersMatch);
                        
                        trackRef->SetQueryValueMatch(m_QueryValueMatch);
                        trackRef->SetStringDataMembersValueMatch(m_StringDataMembersValueMatch);
                        trackRef->SetFloatDataMembersValueMatch(m_FloatDataMembersValueMatch);
                        trackRef->SetFloatDataMembersValueMatchCondition(m_FloatDataMembersValueMatchCondition);

                        m_conditions.push_back(trackRef);
                    }
                }
            }
        }
    }

    if(m_QueryTracks)
    {
        int trackCount = node->GetTrackCount();
        for(int i=0;i<trackCount;i++)
        {
            Track* track = node->GetTrack(i);

            if(m_QueryTracks)
            {
                if(track && Match(m_TrackNamesMatch, track->GetName()->GetString())) 
                {
  
                    bool matchTrackAndDataMemberValue = MatchTrackAndDataMemberValue(   track,
                                                                                        m_TrackDataMembersMatch,
                                                                                        m_QueryValueMatch,
                                                                                        m_StringDataMembersValueMatch, 
                                                                                        m_FloatDataMembersValueMatch, 
                                                                                        m_FloatDataMembersValueMatchCondition
                                                                                    );
                    if(matchTrackAndDataMemberValue)
                    {
                        TrackReference* trackRef = new TrackReference;
                        trackRef->SetActionNodeReference(&m_ActionNodeRef);
                        trackRef->SetTrackIndexReference(i);
                        trackRef->SetDataMembersMatch(m_TrackDataMembersMatch);

                        trackRef->SetQueryValueMatch(m_QueryValueMatch);
                        trackRef->SetStringDataMembersValueMatch(m_StringDataMembersValueMatch);
                        trackRef->SetFloatDataMembersValueMatch(m_FloatDataMembersValueMatch);
                        trackRef->SetFloatDataMembersValueMatchCondition(m_FloatDataMembersValueMatchCondition);

                        m_tracks.push_back(trackRef);
                    }
                }
            }
        }
    }
}

TrackReference* NodeReferenceFilterTrack::GetConditionTrackReference(int trackIndex)
{
    TrackReference* trackRef = m_conditions[trackIndex];
    return(trackRef);
}

TrackReference* NodeReferenceFilterTrack::GetTrackReference(int trackIndex)
{
    TrackReference* trackRef = m_tracks[trackIndex];
    return(trackRef);
}

//--------------------------------------------------------------------------------
//  FindQuery
//--------------------------------------------------------------------------------
BEGIN_DATAMEMBERS_NOT_TRACK(FindQuery)

    COMMAND_DATAMEMBER(FindQuery, "branchNameMatch", GetBranchNameMatch, SetBranchNameMatch, Execute)

    BOOL_DATAMEMBER(FindQuery,    "queryEditPriority",          GetQueryEditPriority, SetQueryEditPriority)

    BOOL_DATAMEMBER(FindQuery,    "queryConditions",            GetQueryConditions, SetQueryConditions)
    STRING_DATAMEMBER(FindQuery,  "conditionNamesMatch",        GetConditionNamesMatch, SetConditionNamesMatch)
    STRING_DATAMEMBER(FindQuery,  "conditionDataMembersMatch",  GetConditionDataMembersMatch, SetConditionDataMembersMatch)

    BOOL_DATAMEMBER(FindQuery,    "queryTracks",            GetQueryTracks, SetQueryTracks)
    STRING_DATAMEMBER(FindQuery,  "TrackNamesMatch",        GetTrackNamesMatch, SetTrackNamesMatch)
    STRING_DATAMEMBER(FindQuery,  "TrackDataMembersMatch",  GetTrackDataMembersMatch, SetTrackDataMembersMatch)

    BOOL_DATAMEMBER(FindQuery,    "queryValueMatch",            GetQueryValueMatch, SetQueryValueMatch)
    STRING_DATAMEMBER(FindQuery,  "stringDataMembersValueMatch",  GetStringDataMembersValueMatch, SetStringDataMembersValueMatch)
	FLOAT_DATAMEMBER(FindQuery,   "floatValue",    GetFloatDataMembersValueMatch,       SetFloatDataMembersValueMatch)
    ENUM_DATAMEMBER(FindQuery,    "floatValueMatch", 
        GetFloatDataMembersValueMatchCondition, SetFloatDataMembersValueMatchCondition, 
        Conditional::m_ConditionalStringName, Conditional::m_ConditionalStringNameSize)

END_DATAMEMBERS(FindQuery)

FindQuery::FindQuery()
:
m_BranchNameMatch(NULL),

m_QueryEditPriority(false),

m_QueryConditions(false),
m_ConditionNamesMatch(NULL),
m_ConditionDataMembersMatch(NULL),

m_QueryTracks(true),
m_TrackNamesMatch(NULL),
m_TrackDataMembersMatch(NULL),

m_QueryValueMatch(false),
m_StringDataMembersValueMatch(NULL),
m_FloatDataMembersValueMatch(0.0f),
m_FloatDataMembersValueMatchCondition(Conditional::C_EQUAL)
{
    m_ConditionNames.reserve(128);
    m_TrackNames.reserve(128);
    m_NodeReferences.reserve(1024);

    NodeReferenceFilterTrack::CreateMembersInterFace();

    StringCopyNew(m_BranchNameMatch, "/Global");
}

FindQuery::~FindQuery()
{
    delete [] m_BranchNameMatch;

    delete [] m_ConditionNamesMatch;
    delete [] m_ConditionDataMembersMatch;

    delete [] m_TrackNamesMatch;
    delete [] m_TrackDataMembersMatch;

    delete [] m_StringDataMembersValueMatch;

	NodeReferenceFilterTrack::DestroyMembersInterFace();
}

void FindQuery::clearNodeReferences(void)
{
	int numNodeReferences = m_NodeReferences.size();
	for(int i=0;i<numNodeReferences;i++)
	{
		NodeReferenceFilterTrack* NodeReferences = m_NodeReferences[i];
		delete NodeReferences;
	}
	m_NodeReferences.clear();
}

bool MatchTrackParameters
(
    Track* track, 
    const char* matchString
)
{
    int numMembers = track->GetNumMembers();
    for(int i=0;i<numMembers;i++)
    {
        DataMember* dataMember = track->GetMember(i);
        
        if(Match(dataMember->GetName()->GetString(), matchString))
        {
            return(true);
        }
    }

    return(false);
}

bool FindQuery::Execute(void)
{
    ActionTreeLog::GetInstance()->Log("FINDING...");

    clearNodeReferences();
    //
    // Set the Static Filter for the Query
    //
    NodeReferenceFilterTrack::SetQueryConditions(m_QueryConditions);
    NodeReferenceFilterTrack::SetConditionNamesMatch(m_ConditionNamesMatch);
    NodeReferenceFilterTrack::SetConditionDataMembersMatch(m_ConditionDataMembersMatch);

    NodeReferenceFilterTrack::SetQueryTracks(m_QueryTracks);
    NodeReferenceFilterTrack::SetTrackNamesMatch(m_TrackNamesMatch);
    NodeReferenceFilterTrack::SetTrackDataMembersMatch(m_TrackDataMembersMatch);

    NodeReferenceFilterTrack::SetQueryValueMatch(m_QueryValueMatch);
    NodeReferenceFilterTrack::SetStringDataMembersValueMatch(m_StringDataMembersValueMatch);
    NodeReferenceFilterTrack::SetFloatDataMembersValueMatch(m_FloatDataMembersValueMatch);
    NodeReferenceFilterTrack::SetFloatDataMembersValueMatchCondition(m_FloatDataMembersValueMatchCondition);

    //
    // First Setup the cached set of Tracks and conditions to Match
    //
    m_ConditionNames.clear();
    m_TrackNames.clear();

    if(m_QueryConditions)
    {
        int numConditions = Condition::GetNumRegistered();
        for(int i=0;i<numConditions;i++)
        {
	        const char* conditionName = Condition::GetRegistered(i)->GetString();
            if(Match(conditionName, m_ConditionNamesMatch))
            {
                std::string matchedConditionName(conditionName);
                m_ConditionNames.push_back(matchedConditionName);
            }
        }
    }

    if(m_QueryTracks)
    {
        int numTracks = Track::GetNumRegistered();
        for(int i=0;i<numTracks;i++)
        {
	        const char* trackName = Track::GetRegistered(i)->GetString();
            if(Match(trackName, m_TrackNamesMatch))
            {
                std::string matchedTrackName(trackName);
                m_TrackNames.push_back(matchedTrackName);
            }
        }
    }


    //
    // Ok... first start the search by using the Start Node
    //
    ActionNode* searchRoot = ActionNode::Find(m_BranchNameMatch);

    if(searchRoot)
    {
        ActionNodeIterator it(searchRoot);

        for(it.First();!it.IsDone();it.Next())
        {
            ActionNode* current = it.CurrentItem();
            if(current)
            {
                //
                // Add a NodeReferencTrack
                //
                bool matchedNode = false;

                if(m_QueryEditPriority)
                {
                    ActionNodePlayable* playable = dynamic_cast<ActionNodePlayable*>(current);
                    if(playable)
                    {
                        int playableEditLevel = playable->GetEditPriority();
                        int intDataMembersValueMatch = (int)(m_FloatDataMembersValueMatch);
                        matchedNode = Conditional::CompareInt(playableEditLevel, intDataMembersValueMatch, m_FloatDataMembersValueMatchCondition);
                    }
                }

                if(m_QueryConditions && !matchedNode)
                {
                    int conditionCount = current->GetConditionCount();
                    for(int i=0;i<conditionCount;i++)
                    {
                        Condition* condition = current->GetCondition(i);
                        if(condition && Match(m_ConditionNamesMatch, condition->GetName()->GetString())) 
                        {
                            matchedNode = MatchTrackAndDataMemberValue(
                                                condition,
                                                m_ConditionDataMembersMatch,
                                                m_QueryValueMatch,
                                                m_StringDataMembersValueMatch, 
                                                m_FloatDataMembersValueMatch, 
                                                m_FloatDataMembersValueMatchCondition
                                            );

                           // matchedNode = MatchTrackParameters(condition, m_ConditionDataMembersMatch);
                        }

                        if(matchedNode)
                        {
                            break;
                        }
                    }
                }

                if(m_QueryTracks && !matchedNode)
                {
                    int trackCount = current->GetTrackCount();
                    for(int i=0;i<trackCount;i++)
                    {
                        Track* track = current->GetTrack(i);
                        if(track && Match(m_TrackNamesMatch, track->GetName()->GetString())) 
                        {
//                            matchedNode = MatchTrackParameters(track, m_TrackDataMembersMatch);
                            matchedNode = MatchTrackAndDataMemberValue(     
                                                track,
                                                m_TrackDataMembersMatch,
                                                m_QueryValueMatch,
                                                m_StringDataMembersValueMatch, 
                                                m_FloatDataMembersValueMatch, 
                                                m_FloatDataMembersValueMatchCondition
                                            );

                        }

                        if(matchedNode)
                        {
                            break;
                        }
                    }
                }

                if(matchedNode)
                { 
                   NodeReferenceFilterTrack* nodeReference = new NodeReferenceFilterTrack();
                   nodeReference->SetActionNode(current);
                   m_NodeReferences.push_back(nodeReference);
                }
            }
        }
    }

    int numMatches = m_NodeReferences.size();
    char message[128];
    sprintf(message, "FOUND:%d", numMatches);
    ActionTreeLog::GetInstance()->Log(message);

    return(true);
}

//--------------------------------------------------------------------------------
//  FileProperties
//--------------------------------------------------------------------------------
BEGIN_DATAMEMBERS_NOT_TRACK(FileProperties)
	BOOL_DATAMEMBER_SIMPLE(FileProperties, TrySaveToGameFolders)
    STRING_DATAMEMBER(FileProperties, "scriptPath", GetScriptPath,  SetScriptPath)
    COMMAND_DATAMEMBER(FileProperties, "TargetIP", GetTargetIP,  SetTargetIP, ConnectDisconnectToHost)
    INT_DATAMEMBER(FileProperties, "editPriority", GetGlobalEditPriority, SetGlobalEditPriority)
END_DATAMEMBERS(FileProperties)

FileProperties::FileProperties()
:
m_ScriptPath(NULL),
m_TargetIP(NULL) 
{
    StringCopyNew(m_ScriptPath, "./");
    StringCopyNew(m_TargetIP, "127.0.0.1"); // localhost
	SetTrySaveToGameFolders(true);
}
FileProperties::~FileProperties()
{
    delete [] m_ScriptPath;
    delete [] m_TargetIP;
    ClearAllFileRecords();
}

bool FileProperties::ConnectDisconnectToHost(void)
{
    Application* theApplication = Application::GetInstance();
    if(theApplication)
    {
        bool isConnected = theApplication->IsConnected();
        if(isConnected)
        {
            theApplication->DisconnectFromRemoteHost();
        }
        else
        {
            theApplication->ConnectToRemoteHost();
        }
    }

    return(true);
}

void FileProperties::AddFileRecord( ArchiveFileRecord*  archiveFileRecord )
{
    std::vector<ArchiveFileRecord*>::iterator finder = m_archiveFileRecords.begin();
    while(finder != m_archiveFileRecords.end())
    {
        if(*finder == archiveFileRecord)
        {
            //
            // Already added
            //
            return;
        }

        //
        // Next
        //
        finder++;
    }

    //
    // Didn't find it so... 
    //
    m_archiveFileRecords.push_back(archiveFileRecord);
}
void FileProperties::RemoveFileRecord( ArchiveFileRecord*  archiveFileRecord )
{
    std::vector<ArchiveFileRecord*>::iterator deletor = m_archiveFileRecords.begin();
    while(deletor != m_archiveFileRecords.end())
    {
        if(*deletor == archiveFileRecord)
        {
            m_archiveFileRecords.erase(deletor);
            return;
        }

        //
        // Next
        //
        deletor++;
    }
}

void FileProperties::ClearAllFileRecords(void)
{
    m_archiveFileRecords.clear();
}

//--------------------------------------------------------------------------------
//  ApplicationPreferences
//--------------------------------------------------------------------------------
BEGIN_DATAMEMBERS_NO_NAME(ApplicationPreferences)
    STRING_DATAMEMBER(ApplicationPreferences, "ViewStyle",     GetStringViewStyle, SetStringViewStyle)
END_DATAMEMBERS(ApplicationPreferences)

ApplicationPreferences::ApplicationPreferences()
{
    m_ViewStyle.SetString("QSGIStyle");
}


//--------------------------------------------------------------------------------
//  ViewProperties
//--------------------------------------------------------------------------------
const char* g_ShowModeStrings[] = {"Hidden", "Normal", "Minimized", "Maximized"};
int g_ShowModeStringsNum = sizeof(g_ShowModeStrings)/sizeof(char*);

BEGIN_DATAMEMBERS_NO_NAME(ViewProperties)

    ENUM_DATAMEMBER(ViewProperties, "open",     GetOpen, SetOpen, g_ShowModeStrings, g_ShowModeStringsNum)
    INT_DATAMEMBER(ViewProperties, "XPosition", GetXposition, SetXposition)
    INT_DATAMEMBER(ViewProperties, "Yposition", GetYposition, SetYposition)
    INT_DATAMEMBER(ViewProperties, "Height",    GetHeight, SetHeight)
    INT_DATAMEMBER(ViewProperties, "Width",     GetWidth, SetWidth)

END_DATAMEMBERS(ViewProperties)

ViewProperties::ViewProperties()
:
m_Open(SM_NORMAL),
m_Xposition(0),
m_Yposition(0),
m_Height(300),
m_Width(300)
{

}

ViewProperties* ViewProperties::Create( TextParser& textParser )
{ 
    ViewProperties* viewProperties = new ViewProperties;
    viewProperties->GetName()->SetString(textParser.GetTokenString());
    viewProperties->Read(textParser);
    return(viewProperties);
}
void ViewProperties::Write(TextBuffer& textBuff, const char* /*headerName = 0*/)
{
    textBuff.Write("ViewProperties");
    textBuff.Write("\t");
    textBuff.Write(m_Name.GetString());
    IDataMembers::Write(textBuff);
}


//--------------------------------------------------------------------------------
//  Application
//--------------------------------------------------------------------------------
Application* Application::m_thisInstance = 0;

Application::Application()
:
m_Name("DrV_Application"),
m_Units(0),
m_FindQuery(0),
m_FileProperties(0),
m_ApplicationPreferences(0),
m_ConfigFile(0),
m_Connected(false)
{
	m_sem = 0;
	m_thisInstance = this;

    m_Units = new Units;
    m_FindQuery = new FindQuery;
    m_FileProperties = new FileProperties;
    m_ApplicationPreferences = new ApplicationPreferences;
    m_ConfigFile = new ArchiveFileRecord;

    Units::CreateMembersInterFace();
    FindQuery::CreateMembersInterFace();
    FileProperties::CreateMembersInterFace();
    ViewProperties::CreateMembersInterFace();
    ApplicationPreferences::CreateMembersInterFace();

    Init();
}

Application::~Application()
{
    DeInit();

	Units::DestroyMembersInterFace();
	FindQuery::DestroyMembersInterFace();
	FileProperties::DestroyMembersInterFace();
    ViewProperties::DestroyMembersInterFace();
    ApplicationPreferences::DestroyMembersInterFace();

    delete m_Units;
    delete m_FindQuery;
    delete m_FileProperties;
    delete m_ApplicationPreferences;
    delete m_ConfigFile;
}

void Application::Init(void)
{
    m_ConfigFile->SetFileName("drvConfig.cfg");
    m_ConfigFile->SetPathName("./");
    m_ConfigFile->SetFactoryIArchive(CreateProperties);
    m_ConfigFile->SetArchiveObject(this);
    m_ConfigFile->ReadFromFile();

//    RemoteMgr::Connect(m_FileProperties->GetTargetIP());

    m_MessageToRemoteHostBuffer.reserve(50);
    m_MessageFromRemoteHostBuffer.reserve(300);
    m_RemoteTransactionsBuffer.reserve(100);

	m_ComChannel = fiHandleInvalid;
	m_ComAccept = fiHandleInvalid;
	m_Com.InitClass(NULL, NULL);
}

void Application::WriteConfig(void)
{
    m_ConfigFile->WriteToFile();
}

void Application::DeInit(void)
{
//    RemoteMgr::Disconnect();
    WriteConfig();
}
const int comBuffSize = 32768;
char comBuff[comBuffSize];

void Application::ConnectToRemoteHost(void)
{
    if(m_Connected == false)
    {
		const char* targetIP = m_FileProperties->GetTargetIP();
		char message[128];
		sprintf(message, "CONNECTING... TO:%s", targetIP);
		ActionTreeLog::GetInstance()->Log(message);

		if(m_ComChannel == fiHandleInvalid)
		{
			m_ComChannel = m_Com.Listen(69000,1, targetIP);
		}
		
		if(m_ComChannel != fiHandleInvalid)
		{
			if(m_ComAccept == fiHandleInvalid)
			{
				m_ComAccept = m_Com.Pickup(m_ComChannel);
				if(m_ComAccept != fiHandleInvalid)
				{
					m_Connected = true;
					sprintf(message, "CONNECTED TO:%s", targetIP);
					ActionTreeLog::GetInstance()->Log(message);

					//
					// Below is the handshake back
					//
					//sprintf(comBuff, "yo");
					//m_Com.Write(m_ComAccept, comBuff,comBuffSize);
				}
			}
		}
	}
}
void Application::HandleCommand(CBasicString* command)
{
    TextParser textParser(command->GetString(), " ,\t\r\n:");
    bool handled = false;
    if(!textParser.EndOfText()) 
    {
        if(textParser.MatchCurrentToken("[act]BreakPoint"))
        {
 //           handleBreakPoint(textParser);
            handled = true;
            delete command;
        }
    }

    if(handled == false)
    {
        ActionTreeLog::GetInstance()->Log(command->GetString());
        delete command;
    }    
}

void Application::DisconnectFromRemoteHost(void)
{
	if(m_Connected)
    {
        m_Connected = false;

        const char* targetIP = m_FileProperties->GetTargetIP();
        char message[128];
        sprintf(message, "DISCONNECTED FROM:%s", targetIP);
        ActionTreeLog::GetInstance()->Log(message);
		
		m_Com.Close(m_ComAccept);
		m_Com.Close(m_ComChannel);
		m_ComAccept = fiHandleInvalid;
		m_ComChannel = fiHandleInvalid;

		bool bHandled = false;
		int iNumObservers = m_Observers.size();
		CBasicString *pDisconnectString = new CBasicString("DISCONNECT");
		for (int i = 0; i < iNumObservers; i++)
		{
			if (m_Observers[i]->HandleMessage(pDisconnectString))
			{
				bHandled = true;
				continue;
			}
		}

		if (!bHandled)
		{
			delete pDisconnectString;
		}

	}
}

void Application::PostMessageToRemoteHost(const char* message)
{
    CBasicString* messageCName = new CBasicString(message);
    m_MessageToRemoteHostBuffer.push_back(messageCName);
}

void Application::readComPort(void)
{
	static bool s_bIsWaitingForData = false;
	static int s_iWaitingPacketSize = 0;

	if (s_bIsWaitingForData)
	{
		const bool bReadPacket = TryToReadPacket(s_iWaitingPacketSize);
		if (bReadPacket)
		{
			s_bIsWaitingForData = false;
			s_iWaitingPacketSize = 0;
		}
		else
		{
			return;
		}
	}

	if (m_ComAccept != fiHandleInvalid)
	{
		char headerBuf[5];
		int *packetSize = (int*)&headerBuf[1];
	
		//
		// Read all messages and dispatch the commands
		//
		int readCount = m_Com.GetReadCount(m_ComAccept);
		while(readCount>0)
		{
			bool bSuccess = m_Com.SafeRead(m_ComAccept, headerBuf, 5);
			if (bSuccess)
			{
				*packetSize = rage::sysEndian::NtoB(*packetSize);

				if(*packetSize != 0)
				{
					if(*packetSize > comBuffSize)
					{
						printf("Buffer too small flushing\n");
						int iBufferSize = m_Com.GetReadCount(m_ComAccept);
						while (iBufferSize > 0)
						{
							const int iFlushSize = rage::Min(comBuffSize, iBufferSize);
							m_Com.Read(m_ComAccept, &comBuff, iFlushSize);
							iBufferSize -= iFlushSize;
						}
						break;
					}
					else
					{
						const bool bReadPacket = TryToReadPacket(*packetSize);
						if (!bReadPacket)
						{
							s_bIsWaitingForData = true;
							s_iWaitingPacketSize = *packetSize;
							break;
						}
					}
				}				
			}

			//
			// Is there still stuff left to go?
			//
			readCount = m_Com.GetReadCount(m_ComAccept);
		}
	}	
}

bool Application::TryToReadPacket(const int iPacketSize)
{
	bool bSuccess = m_Com.SafeRead(m_ComAccept, &comBuff, iPacketSize);

	if (bSuccess)
	{
		//
		// Set message to observers and build a message buffer
		//
		RecieveMessageFromRemoteHost(comBuff);					
	}
	else
	{
		printf("Couldn't Read Packetsize:%d\n", iPacketSize);
	}

	return bSuccess;
}

void Application::Update(float /*deltaTime*/)
{
	
	readComPort();

    int numMsgs = m_MessageToRemoteHostBuffer.size();
    if(numMsgs>0)
    {
        std::vector<CBasicString*>::iterator msgIT = m_MessageToRemoteHostBuffer.begin();
		
        CBasicString* msg = *msgIT;
        const char* msgString = msg->GetString();
		int messageSize = strlen(msgString)+1;
		if(messageSize>comBuffSize)
		{
			messageSize=comBuffSize;
		}

		char sizeBuffHeader[5];
		int* sizeptr = (int*)(&sizeBuffHeader[1]);
		memset(sizeBuffHeader, 0, sizeof(char));
		*sizeptr=messageSize-5;

		*sizeptr = rage::sysEndian::BtoN(*sizeptr);
		
		memcpy((void*)msgString,(void*)sizeBuffHeader, 5*sizeof(char)); 

        if(msgString)
        {
            //RemoteMgr::SendMsg(msgString);
			m_Com.Write(m_ComAccept, msgString,messageSize);
        }

        delete *msgIT; // delete the object
        m_MessageToRemoteHostBuffer.erase(msgIT);
    }

	//
    // Thread safe Handle Recieved
    //
    if(m_sem == 0)
    {
        m_sem = 1;
        int numMsgs = m_MessageFromRemoteHostBuffer.size();
        if(numMsgs>0)
        {
            std::vector<CBasicString*>::iterator msgIT = m_MessageFromRemoteHostBuffer.begin();
            CBasicString* msg = *msgIT;
            m_MessageFromRemoteHostBuffer.erase(msgIT);

            //
            // Move the message
            //
            m_RemoteTransactionsBuffer.push_back(msg);
        }
        m_sem = 0;
    }
    
    //
    // Handle RemoteTransactions
    //
    numMsgs = m_RemoteTransactionsBuffer.size();
    if(numMsgs>0)
    {
        std::vector<CBasicString*>::iterator msgIT = m_RemoteTransactionsBuffer.begin();
        CBasicString* msg = *msgIT;
        m_RemoteTransactionsBuffer.erase(msgIT);

        HandleCommand(msg);       
        //delete *msgIT; // delete the object
    }

}
void Application::RecieveMessageFromRemoteHost(const char* message)
{
    CBasicString* messageAT = new CBasicString(message);

    int numObservers = m_Observers.size();
    for(int i=0;i< numObservers;i++)
    {
        if(m_Observers[i]->HandleMessage(messageAT))
        {
            //
            // Handled
            //
            return;
        }
    } 

    //
    // Another command
    //
    while(m_sem != 0); // wait
    m_sem = 1;
    m_MessageFromRemoteHostBuffer.push_back(messageAT);
    m_sem = 0;
}

IArchive* Application::CreateProperties(TextParser& textParser)
{
    Application* theApplication = Application::GetInstance();
    if(theApplication)
    {
	    while(!textParser.EndOfText()) 
	    {
		    if(textParser.MatchCurrentToken("FileProperties"))
		    {
                theApplication->GetFileProperties()->Read(textParser);
		    }
            if(textParser.MatchCurrentToken("ApplicationPreferences"))
            {
                theApplication->GetApplicationPreferences()->Read(textParser);
            }
            else if(textParser.MatchCurrentToken("ViewProperties"))
            {
                textParser.GetNextToken();
                if(textParser.EndOfText())
                {
                    break;
                }
                ViewProperties* viewProperties = ViewProperties::Create( textParser );
                theApplication->AddViewProperties(viewProperties);
            }

            textParser.GetNextToken();
        }

        return(m_thisInstance);
    }
    else
    {
        Assert(0 && "Application Not Created");
        return(NULL);
    }
}

void Application::AddViewProperties(ViewProperties* viewProperty)
{
    m_ViewProperties.push_back(viewProperty);
}

ViewProperties* Application::GetCreateViewProperties(const char* viewName)
{
    int numViews = m_ViewProperties.size();
    for(int i=0;i<numViews;i++)
    {
        if(*(m_ViewProperties[i]->GetName()) == viewName)
        {
            return(m_ViewProperties[i]);
        }
    }

    //
    // If you are here this is a new Veiw Properties request
    //
	TextParser txtParse(viewName, " ,\t\r\n");
    ViewProperties* view = ViewProperties::Create( txtParse);
    m_ViewProperties.push_back(view);
    return(view);
}

void Application::Write(TextBuffer& buffer)
{
    m_FileProperties->Write(buffer, "FileProperties");
    m_ApplicationPreferences->Write(buffer, "ApplicationPreferences");

    int numViews = m_ViewProperties.size();
    for(int i=0;i<numViews;i++)
    {
        m_ViewProperties[i]->Write(buffer);
    }
}


