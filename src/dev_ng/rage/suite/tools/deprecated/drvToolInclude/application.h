// 
// actiontree/datamember.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef APPLICATION_H
#define APPLICATION_H

#include <string>
#include "actiontree/datamember.h"
#include "actiontree/ActionNodeTracks.h"

class Units : public IDataMembers
{
public:
    Units();
    ~Units();

    DATAMEMEBERS_INTERFACE();
    
    float GetConverted( IFloatDataMember* floatDataMember, IDataMembers* dataMembersInstance );
    void  SetConverted( IFloatDataMember* floatDataMember, IDataMembers* dataMembersInstance, float displayValue );

	const char * GetUnitsString(IFloatDataMember* floatDataMember);

    //
    // TimeUnits
    //
	enum TimeUnits { TU_Seconds, TU_30TH_Seconds, kNumTimeUnits };
    static float sm_TimeUnitsConversion[kNumTimeUnits];
	static const char* sm_TimeUnitsOptionNames[kNumTimeUnits];
    TRACK_PARAMETER_ENUM(TimeUnits, TimeUnits)
    static float GetConverted( float value, TimeUnits from,  TimeUnits to);

    //
    // LengthUnits
    //
	enum LengthUnits { LU_Meters, LU_Centimeters, kNumLengthUnits };
	static float sm_LengthUnitsConversion[kNumLengthUnits];
	static const char * sm_LengthUnitsOptionNames[kNumLengthUnits];
    TRACK_PARAMETER_ENUM(LengthUnits, LengthUnits)

    //
    // VelocityUnits
    //
	enum VelocityUnits { VU_MetersPerSecond, VU_MetersPer30Th_Seconds, kNumVelocityUnits };
    static float sm_VelocityUnitsConversion[kNumVelocityUnits];
	static const char * sm_VelocityUnitsOptionNames[kNumVelocityUnits];
    TRACK_PARAMETER_ENUM(VelocityUnits, VelocityUnits)

	//
	// AccelUnits
	//
	enum AccelUnits { VU_MetersPerSecondSquared, VU_MetersPer30Th_SecondsSquared, kNumAccelUnits };
	static float sm_AccelUnitsConversion[kNumAccelUnits];
	static const char * sm_AccelUnitsOptionNames[kNumAccelUnits];
	TRACK_PARAMETER_ENUM(AccelUnits, AccelUnits)

	//
	// AngleUnits
	//
	enum AngleUnits { AU_Radians, AU_Degrees, kNumAngleUnits };
	static float sm_AngleUnitsConversion[kNumAngleUnits];
	static const char* sm_AngleUnitsOptionNames[kNumAngleUnits];
	TRACK_PARAMETER_ENUM(AngleUnits, AngleUnits)

    //
    // AngleSpeedUnits
    //
    enum AngleSpeedUnits{ ASU_RadiansPerSecond, ASU_RadiansPer30Th_Seconds, TSU_DegreesPerSecond, TSU_DegreesPer30Th_Seconds, kNumAngleSpeedUnits };
    TRACK_PARAMETER_ENUM(AngleSpeedUnits, AngleSpeedUnits)

private:

    static inline float getConverted( float value, int from,  int to, const float* conversionList, int /*listSize*/)
    {
        return( value*conversionList[to]/conversionList[from] );
    }

};


class TrackReference : public Track
{
public:
    TrackReference();
    ~TrackReference();

	virtual CName* GetName(void);
	virtual const CName* GetName(void) const;
	virtual void SetName(const char* name);

    TRACK_PARAMETER_STRING(DataMembersMatch)

	TRACK_PARAMETER(bool, QueryValueMatch)
    TRACK_PARAMETER_STRING(StringDataMembersValueMatch)
	TRACK_PARAMETER(float, FloatDataMembersValueMatch)
    TRACK_PARAMETER_ENUM(Conditional::Operators, FloatDataMembersValueMatchCondition)

    virtual int GetNumMembers(void);
	virtual DataMember* GetMember(int i);

    void SetActionNodeReference(ActionNodeReference* actionNodeRef){m_ActionNodeRef = actionNodeRef;}
    void SetTrackIndexReference(int trackID){m_TrackReferenceIndex = trackID;}
    int  GetTrackIndexReference(void){return(m_TrackReferenceIndex);}
    void SetConditionIndexReference(int trackID){m_ConditionReferenceIndex = trackID;}
    int  GetConditionIndexReference(void){return(m_ConditionReferenceIndex);}

	Track* GetTrack();
	Track* GetTrack(void) const;

    //
    // This is just baggage
    //
    virtual ATask* NewTask(ActionContext* /*contex*t*/){return(NULL);}

protected:
    
    Track* getReferenceTrack(void) const;
    Track* getReferenceCondition(void) const;

    int m_ConditionReferenceIndex;
    int m_TrackReferenceIndex;
    ActionNodeReference* m_ActionNodeRef;    
};

class NodeReferenceFilterTrack : public NodeReferenceTrack
{
public:
    NodeReferenceFilterTrack();
    virtual ~NodeReferenceFilterTrack();

    DATAMEMEBERS_INTERFACE()

    TRACK_PARAMETER_STATIC(bool, QueryConditions)
    TRACK_PARAMETER_STRING_STATIC(ConditionNamesMatch)
    TRACK_PARAMETER_STRING_STATIC(ConditionDataMembersMatch)

    TRACK_PARAMETER_STATIC(bool, QueryTracks)
    TRACK_PARAMETER_STRING_STATIC(TrackNamesMatch)
    TRACK_PARAMETER_STRING_STATIC(TrackDataMembersMatch)

    TRACK_PARAMETER_STATIC(bool, QueryValueMatch)
    TRACK_PARAMETER_STRING_STATIC(StringDataMembersValueMatch)
	TRACK_PARAMETER_STATIC(float, FloatDataMembersValueMatch)
    TRACK_PARAMETER_ENUM_STATIC(Conditional::Operators, FloatDataMembersValueMatchCondition)

    virtual int GetConditionCount(void){return(m_conditions.size());}
	virtual TrackReference* GetConditionTrackReference(int trackIndex);

    virtual int GetTrackCount(void){return(m_tracks.size());}
    virtual TrackReference* GetTrackReference(int trackIndex);

    //
    // This is just baggage
    //
    virtual ATask* NewTask(ActionContext* /*context*/){return(NULL);}

	virtual void SetActionNode(ActionNode* node);

private:

    std::vector<TrackReference*> m_conditions;
    std::vector<TrackReference*> m_tracks;

};


class FindQuery : public IDataMembers
{
public:
    FindQuery();
    ~FindQuery();

    DATAMEMEBERS_INTERFACE(); 

    bool Execute(void);

    TRACK_PARAMETER_STRING(BranchNameMatch)

    TRACK_PARAMETER(bool, QueryEditPriority)

    TRACK_PARAMETER(bool, QueryConditions)
    TRACK_PARAMETER_STRING(ConditionNamesMatch)
    TRACK_PARAMETER_STRING(ConditionDataMembersMatch)

    TRACK_PARAMETER(bool, QueryTracks)
    TRACK_PARAMETER_STRING(TrackNamesMatch)
    TRACK_PARAMETER_STRING(TrackDataMembersMatch)

    TRACK_PARAMETER(bool, QueryValueMatch)
    TRACK_PARAMETER_STRING(StringDataMembersValueMatch)
	TRACK_PARAMETER(float, FloatDataMembersValueMatch)
    TRACK_PARAMETER_ENUM(Conditional::Operators, FloatDataMembersValueMatchCondition)

    int GetFoundItemNum(void){return(m_NodeReferences.size());}
    NodeReferenceFilterTrack* GetFoundItem(int i){return(m_NodeReferences[i]);}

private:

    //
    // This is the set of Registered conditions
    // That match the ConditionNamesMatch
    //
    std::vector<std::string> m_ConditionNames;
    std::vector<std::string> m_TrackNames;

    void clearNodeReferences(void);
    std::vector<NodeReferenceFilterTrack*> m_NodeReferences;

};

class FileProperties : public IDataMembers
{
public:
    FileProperties();
    ~FileProperties();

    DATAMEMEBERS_INTERFACE(); 

    void AddFileRecord( ArchiveFileRecord*  archiveFileRecord );
    void RemoveFileRecord( ArchiveFileRecord*  archiveFileRecord );
    void ClearAllFileRecords(void);

    int                 GetArchiveFileRecordsSize(void);
    ArchiveFileRecord*  GetArchiveFileRecords(int index);

    TRACK_PARAMETER_STRING(ScriptPath)
    TRACK_PARAMETER_STRING(TargetIP)
    bool ConnectDisconnectToHost(void);

    //
    // This code directly sets the ActionNodes 
    // m_GlobalEditPriority Parameter
    //
    int  GetGlobalEditPriority(void){return(ActionNode::m_GlobalEditPriority);}
    void SetGlobalEditPriority(int editPriority){ActionNode::m_GlobalEditPriority = editPriority;}

	TRACK_PARAMETER(bool, TrySaveToGameFolders)

private:
    std::vector<ArchiveFileRecord*> m_archiveFileRecords;

};

inline int FileProperties::GetArchiveFileRecordsSize(void)
{
    return(m_archiveFileRecords.size());
}

inline ArchiveFileRecord*  FileProperties::GetArchiveFileRecords(int index)
{
    return(m_archiveFileRecords[index]);
}

class ApplicationPreferences : public IDataMembers
{
public:
    ApplicationPreferences();
    ~ApplicationPreferences(){}
    DATAMEMEBERS_INTERFACE_NO_NAME(); 
    TRACK_PARAMETER_CNAME(ViewStyle)
};

class ViewProperties : public IDataMembers , public IName
{
public:
    ViewProperties();
    ~ViewProperties(){}

    static ViewProperties* Create( TextParser& textParser );
    virtual CName* GetName(void){return(&m_Name);}
	virtual const CName* GetName(void) const {return(&m_Name);}
    virtual void Write(TextBuffer& textBuff, const char* headerName = 0);

    DATAMEMEBERS_INTERFACE_NO_NAME(); 

    enum ShowMode{SM_HIDDEN, SM_NORMAL, SM_MINIMIZED, SM_MAXIMIZED};
    TRACK_PARAMETER_ENUM(ShowMode, Open);
    TRACK_PARAMETER(bool, Normal);
    TRACK_PARAMETER(int, Xposition);
    TRACK_PARAMETER(int, Yposition);
    TRACK_PARAMETER(int, Height);
    TRACK_PARAMETER(int, Width);

private:
    CName m_Name;
};

//#include "system/namedpipe.h"
#include "file/tcpip.h"
//#include "system/pipepacket.h"
class Application : public IArchive , public CSubject
{
public:

	static Application* CreateInstance(){return(new Application);}
	static Application* GetInstance(){return(m_thisInstance);}
    
    //
    // This is weird suggesting a refactor
    //
    static IArchive* CreateProperties(TextParser& textParser);

    CName* GetName(){return(&m_Name);}
	const CName* GetName() const {return(&m_Name);}

    void WriteConfig(void);    
    void Write(TextBuffer& buffer);

    void Init(void);
    void DeInit(void);

    Units*          GetUnits(void){return(m_Units);}
    FindQuery*      GetFindQuery(void){return(m_FindQuery);}
    FileProperties* GetFileProperties(void){return(m_FileProperties);}

    ApplicationPreferences* GetApplicationPreferences(void){return(m_ApplicationPreferences);}
    ViewProperties* GetCreateViewProperties(const char* viewName);
    void AddViewProperties(ViewProperties* viewProperty);

    bool IsConnected(void){return(m_Connected);}
    void ConnectToRemoteHost(void);
    void DisconnectFromRemoteHost(void);
    void PostMessageToRemoteHost(const char* message);
    void Update(float deltaTime);
    void RecieveMessageFromRemoteHost(const char* message);

    void HandleCommand(CBasicString* command);
private:
	Application();
	~Application();

    static Application*     m_thisInstance;

    Units*                  m_Units;
    FindQuery*              m_FindQuery;
    FileProperties*         m_FileProperties;
    ApplicationPreferences* m_ApplicationPreferences;
    ArchiveFileRecord*      m_ConfigFile;

    std::vector<ViewProperties*> m_ViewProperties;
    std::vector<CBasicString*>m_MessageToRemoteHostBuffer;
    std::vector<CBasicString*>m_MessageFromRemoteHostBuffer;
    bool m_sem;
    std::vector<CBasicString*>m_RemoteTransactionsBuffer;
    
    bool m_Connected;
    CName m_Name;
	
	void readComPort(void);
	bool TryToReadPacket(const int iPacketSize);

	rage::fiDeviceTcpIp m_Com;
	fiHandle m_ComChannel;
	fiHandle m_ComAccept;
};

class ApplicationDataMember
{
public:
	ApplicationDataMember(Application* app):m_application(app){}

protected:
	Application* m_application;
};
#endif
