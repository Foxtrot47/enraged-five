// 
// /rageViewer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

// See the beginning of sample_simpleworld/sample_simpleworld.cpp for command line options.
//	Examples:
//
//	-file=T:\rage\assets\rage_male\rage_male.type
//
//	-fragfile=T:\rage\assets\fragments\Piranha\entity.type
// -drawthreegrids -zup -notextures -nodrawablecontainer -hidewindow -noBlockOnLostFocus -modeldebugger -takesnapshotset="x:/streammc5/debugrenders/a_m_m_desrebel_03" -farclip="10000" -hidewidgets -shownumverts -shownumtris -shownumgeometries -shaderdb="x:/mc5/build/dev/common/shaders/db" -shaderlib="x:/mc5/build/dev/common/shaders" -autoframe -elist="x:/streammc5/debugrenders/a_m_m_desrebel_03/entitylist.xml"
// -drawthreegrids -zup -notextures -nodrawablecontainer -hidewindow -noBlockOnLostFocus -modeldebugger -takesnapshotset="x:/streammc5/debugrenders/napalm" -farclip="10000" -hidewidgets -shownumverts -shownumtris -shownumgeometries -shaderdb="x:/mc5/build/dev/common/shaders/db" -shaderlib="x:/mc5/build/dev/common/shaders" -autoframe -file="x:/streammc5/debugrenders/entity.type"


#include "file/stream.h"
#include "system/main.h"
#include "sample_simpleworld/sample_simpleworld.h"

using namespace ragesamples;

//-----------------------------------------------------------------------------

class rmcRageViewer : public rmcSampleSimpleWorld
{
protected:
	virtual const char* GetSampleName() const
	{
		return "rageViewer";
	}
};

//-----------------------------------------------------------------------------

int Main()
{
//	diagFileAccess::Init();
	fiStream::OutputAllFileAccess();
	rmcRageViewer rv;
	rv.Init("$\\rageViewer");

	rv.UpdateLoop();

	rv.Shutdown();

	return 0;
}
