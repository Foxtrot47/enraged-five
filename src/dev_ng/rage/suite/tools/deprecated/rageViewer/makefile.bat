set TESTERS=rageViewer
set SAMPLE_LIBS=%RAGE_SAMPLE_LIBS% sample_physics sample_fragment sample_simpleworld
set LIBS=%SAMPLE_LIBS% %RAGE_GFX_LIBS% %RAGE_CORE_LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% %RAGE_NEW_AUD_LIBS%
set LIBS=%LIBS% %RAGE_DIR%\base\src\vcproj\RageGraphics2\RageGraphics2.vcproj
set LIBS=%LIBS% %RAGE_PH_LIBS% %RAGE_DIR%\suite\src\vcproj\RageSuiteMisc\RageSuiteMisc.vcproj %RAGE_SCRIPT_LIBS% vieweraudio cliptools
set XPROJ=%RAGE_DIR%\suite\src %RAGE_DIR%\suite\samples %RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\script\src %RAGE_DIR%\suite\tools\cli
