// RexMBPython/forceInclude.h

#include <windows.h>
#include <commctrl.h>

#define _CRT_SECURE_NO_DEPRECATE

#include <fbsdk/fbsdk.h>

#pragma warning(disable:4996)	//POSIX name deprecated warnings


#if defined(_DEBUG)
	#if defined(_M_X64) || defined(_M_AMD64)
		#include "forceinclude/win64_tooldebug.h"
	#else
		#include "forceinclude/win32_tooldebug.h"
	#endif

#elif defined(NDEBUG)
	#if defined(_M_X64) || defined(_M_AMD64)
		#include "forceinclude/win64_toolrelease.h"
	#else
		#include "forceinclude/win32_toolrelease.h"
	#endif

#else
	#if defined(_M_X64) || defined(_M_AMD64)
		#include "forceinclude/win64_toolbeta.h"
	#else
		#include "forceinclude/win32_toolbeta.h"
	#endif

#endif

