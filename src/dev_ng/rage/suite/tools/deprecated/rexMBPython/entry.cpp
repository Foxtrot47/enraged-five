// /entry.cpp
#include <string>

#include "crAnimation/animation.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfobject.h"
#include "diag/channel.h"
#include "parser/manager.h"
#include "system/param.h"

#include <fbsdk/fbsdk.h>

#ifdef _DEBUG
#undef _DEBUG
#include "Python.h"
#define _DEBUG
#else
#include "Python.h"
#endif

#include "rexMBAnimExportCommon/animExport.h"
#include "rexMBAnimExportCommon/cutsceneExport.h"
#include "rexMBAnimExportCommon/utility.h"

using namespace std;
using namespace rage;

//-----------------------------------------------------------------------------

FBProgress*	g_pExportProgress = NULL;

static const char *s_logCallbackPrefixes[DIAG_SEVERITY_COUNT] = 
{
    "Fatal Error: ", 
    "Error: ", 
    "Error: ",
    "Warning: ",
    "", 
    "", 
    "", 
    ""
};

static int g_errorCount = 0;
static int g_warningCount = 0;

//-----------------------------------------------------------------------------

void UpdateProgressCaption(const char* caption)
{
	if(g_pExportProgress)
	{
		g_pExportProgress->Caption = FBString(caption);
	}
}

//-----------------------------------------------------------------------------

void UpdateProgressPercent(float percent)
{
	if(g_pExportProgress)
	{
		g_pExportProgress->Percent = percent;
	}
}

//-----------------------------------------------------------------------------

bool (*previousLogCallback)(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char *msg);

bool RexMBPythonLogCallback( const diagChannel &channel, diagSeverity severity, const char * /*file*/, int /*line*/, const char *msg )
{
    switch ( severity )
    {
    case DIAG_SEVERITY_WARNING:
        ++g_warningCount;
        break;
    case DIAG_SEVERITY_ERROR:
        ++g_errorCount;
        break;
    default:
        break;
    }

    char cMsg[1024];
    sprintf( cMsg, "%s%s%s\n", s_logCallbackPrefixes[severity], channel.Tag, msg );

	PyObject *pyStdout = PySys_GetObject("stdout");
	PyFile_WriteString( cMsg, pyStdout );

    return severity != DIAG_SEVERITY_FATAL;
}

//-----------------------------------------------------------------------------

void PrintResults( const char *pPrefix )
{
    char cMsg[256];
    sprintf( cMsg, "%s: %d errors, %d warnings\n", pPrefix, g_errorCount, g_warningCount );

    PyObject *pyStdout = PySys_GetObject("stdout");
    PyFile_WriteString( cMsg, pyStdout );
}

//-----------------------------------------------------------------------------

static PyObject* doExport(PyObject* pSelf, PyObject* args)
{
    g_errorCount = 0;
    g_warningCount = 0;

	const char* szOutputPathOverride;

    previousLogCallback = diagLogCallback;
	diagLogCallback = RexMBPythonLogCallback;

	int iArgCheck = PyArg_ParseTuple(args, "s", &szOutputPathOverride);
	if(!iArgCheck)
	{
		PyErr_SetString( PyExc_SyntaxError, "Incorrect number of arguments provided to 'doExport', got 0 expected 1");

        diagLogCallback = previousLogCallback;
		Py_RETURN_FALSE;
	}

	std::string rexMbFilePath = rexMBAnimExportCommon::GetAnimExportMarkupFilePath();
	if(!rexMbFilePath.size())
	{
		Errorf("Failed to load markup file '%s', animation export aborted.", rexMbFilePath.c_str());

        diagLogCallback = previousLogCallback;
		Py_RETURN_FALSE;
	}
    else if ( !ASSET.Exists( rexMbFilePath.c_str(), "" ) )
    {
        Errorf("The expected markup file '%s' does not exist, export aborted.", rexMbFilePath.c_str());
        Py_RETURN_FALSE;
    }

	FBApplication fbApp;
	char* szFBXPath = fbApp.FBXFileName.AsString();

	RexAnimMarkupData* pAnimData = new RexAnimMarkupData();
	if(PARSER.LoadObject<RexAnimMarkupData>(rexMbFilePath.c_str(), "", *pAnimData))
	{
		if(strlen(szOutputPathOverride))
		{
			Displayf("Overriding markup output path of '%s', with output path of '%s'", pAnimData->m_outputPath.c_str(), szOutputPathOverride);
			pAnimData->m_outputPath = szOutputPathOverride;
		}

		if(!pAnimData->ValidateAnimExportData(false))
		{
			Errorf("Markup data validation for file '%s' failed.", szFBXPath);

            diagLogCallback = previousLogCallback;
			Py_RETURN_FALSE;
		}

		g_pExportProgress = new FBProgress();

		RexRageAnimExport	animExporter;
		
		animExporter.SetProgressCaptionCallback( MakeFunctor(&UpdateProgressCaption) );
		animExporter.SetProgressPercentCallback( MakeFunctor(&UpdateProgressPercent) );

		Displayf("Exporting animation from file '%s'", szFBXPath );

		bool bRes = animExporter.DoExport( pAnimData );
		
		delete pAnimData;
		delete g_pExportProgress;
		g_pExportProgress = NULL;

        diagLogCallback = previousLogCallback;

		if ( bRes )
		{
			Py_RETURN_TRUE;
		}
		else
		{
			Py_RETURN_FALSE;
		}
	}

    diagLogCallback = previousLogCallback;
	Py_RETURN_FALSE;
}

//-----------------------------------------------------------------------------

void UpdateProgressMessage( const char *pMessage )
{
    atString msg(pMessage);
    msg.Uppercase();
    if ( msg.StartsWith( "ERROR: " ) || (strstr( msg.c_str(), " ERROR : " ) != NULL) )
    {
        ++g_errorCount;
    }
    else if ( msg.StartsWith( "WARNING: " ) || (strstr( msg.c_str(), " WARNING : " ) != NULL))
    {
        ++g_warningCount;
    }

    char cMsg[1024];
    sprintf( cMsg, "%s\n", pMessage );

    PyObject *pyStdout = PySys_GetObject("stdout");
    PyFile_WriteString( cMsg, pyStdout );
}

//-----------------------------------------------------------------------------

static PyObject* doCutsceneExport(PyObject* pSelf, PyObject* args)
{
    g_errorCount = 0;
    g_warningCount = 0;

    const char* szExportRootOverride = NULL;
    const char* szFaceDirOverride = NULL;
    char iJustCutfile = 0;
    
    previousLogCallback = diagLogCallback;
    diagLogCallback = RexMBPythonLogCallback;

    int iArgCheck = PyArg_ParseTuple(args, "ssb", &szExportRootOverride, &szFaceDirOverride, &iJustCutfile );
    if ( !iArgCheck )
    {
        char msg[256];
        sprintf( msg, "Incorrect number of arguments provided to 'doCutsceneExport'.  Got %d, expected 3.", iArgCheck );
        PyErr_SetString( PyExc_SyntaxError, msg );

        diagLogCallback = previousLogCallback;
        Py_RETURN_FALSE;
    }

	std::string markupFilePath = rexMBAnimExportCommon::GetCutsceneExportMarkupFilePath();
    if ( !markupFilePath.size() )
    {
        Errorf("Failed to load markup file '%s', cut scene export aborted.", markupFilePath.c_str());

        PrintResults( "Export Cutscene" );

        diagLogCallback = previousLogCallback;
        Py_RETURN_FALSE;
    }
    else if ( !ASSET.Exists( markupFilePath.c_str(), "" ) )
    {
        Errorf("The expected markup file '%s' does not exist, cut scene export aborted.", markupFilePath.c_str());

        PrintResults( "Export Cutscene" );

        diagLogCallback = previousLogCallback;
        Py_RETURN_FALSE;
    }

    FBApplication fbApp;
    char* szFBXPath = fbApp.FBXFileName.AsString();

    RexRageCutsceneExport cutsceneExporter;
    if ( cutsceneExporter.LoadCutsceneDataFromFile( markupFilePath.c_str() ) )
    {
        if ( !cutsceneExporter.ValidateCutsceneExportData() )
        {
            Errorf( "Markup data validation for file '%s' failed.", szFBXPath );

            PrintResults( "Export Cutscene" );

            diagLogCallback = previousLogCallback;
            Py_RETURN_FALSE;
        }

        if ( strlen(szExportRootOverride) > 0 )
        {
            char cExportPathOverride[RAGE_MAX_PATH];
            formatf( cExportPathOverride, sizeof(cExportPathOverride), "%s\\%s", szExportRootOverride, cutsceneExporter.GetSceneName() );

            char cExportPath[RAGE_MAX_PATH];
            cutsceneExporter.GetExportPath( cExportPath, sizeof(cExportPath) );
         
            if ( strcmpi( cExportPathOverride, cExportPath ) != 0 )
            {
                Displayf("Overriding markup export root of '%s', with export path of '%s'", cExportPath, cExportPathOverride );
                cutsceneExporter.SetExportRootOverride( szExportRootOverride );
            }
        }

        if ( strlen(szFaceDirOverride) > 0 )
        {
            char cFaceExportPath[RAGE_MAX_PATH];
            cutsceneExporter.GetFaceExportPath( cFaceExportPath, sizeof(cFaceExportPath) );

            if ( strcmpi( szFaceDirOverride, cFaceExportPath ) != 0 )
            {
                Displayf("Overriding markup face dir of '%s', with face dir of '%s'", cFaceExportPath, szFaceDirOverride );
                cutsceneExporter.SetFaceExportPathOverride( szFaceDirOverride );
            }
        }

        g_pExportProgress = new FBProgress();
        
        cutsceneExporter.SetInteractiveMode( false );
        cutsceneExporter.SetProgressCaptionCallback( MakeFunctor(&UpdateProgressCaption) );
        cutsceneExporter.SetProgressMessageCallback( MakeFunctor(&UpdateProgressMessage) );
        cutsceneExporter.SetProgressPercentCallback( MakeFunctor(&UpdateProgressPercent) );

        Displayf( "Exporting cut scene from file '%s'", szFBXPath );

        cutsceneExporter.SetStartFrame( 0 );    // Zero-based Timeline

        cutsceneExporter.SetEndFrame( cutsceneExporter.GetCutsceneFile().GetRangeEnd() );
        if ( cutsceneExporter.GetEndFrame() == 0 )
        {
            cutsceneExporter.SetEndFrame( s32(rexMBAnimExportCommon::GetAnimEndTime() * rexMBAnimExportCommon::GetFPS()) );
        }

        cutsceneExporter.PrepareCutfileForAnimationExport();

        atArray<float> &cameraCutList = const_cast<atArray<float> &>( cutsceneExporter.GetCutsceneFile().GetCameraCutList() );
        cutsceneExporter.GetCameraCutTimes( cameraCutList );            

        // If we have screen fades or overlays, make sure we have the appropriate object(s) created
        SModelObject cameraModelObject;
        atArray<cutfObject *> cameraObjectList;
        cutsceneExporter.GetCutsceneFile().FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );
        if ( cameraObjectList.GetCount() > 0 )
        {
            cameraModelObject.pObject = cameraObjectList[0];
            cameraModelObject.pModel = cutsceneExporter.FindModelForObject( cameraModelObject.pObject );
        }

        if ( cameraModelObject.pModel != NULL )
        {
            // get the section splits
            atArray<float> &sectionSplitList = const_cast<atArray<float> &>( cutsceneExporter.GetCutsceneFile().GetSectionSplitList() );
            cutsceneExporter.GetSectionSplitTimes( cameraModelObject.pModel, sectionSplitList );

            // find all of the overlays we have in the cutfile
            atArray<cutfObject *> overlayObjectList;
            cutsceneExporter.GetCutsceneFile().FindObjectsOfType( CUTSCENE_OVERLAY_OBJECT_TYPE, overlayObjectList );

            // find all of the overlays we have in the scene
            atArray<ConstString> overlayNames;
            cutsceneExporter.GetOverlayNames( cameraModelObject.pModel, overlayNames );

            // create each overlay, if necessary
            if ( overlayNames.GetCount() > 0 )
            {
                // find or add the needed overlays
                for ( int i = 0; i < overlayNames.GetCount(); ++i )
                {
                    bool bFound = false;
                    for ( int j = 0; j < overlayObjectList.GetCount(); ++j )
                    {
                        if ( stricmp( overlayObjectList[j]->GetDisplayName().c_str(), overlayNames[i].c_str() ) == 0 )
                        {
                            overlayObjectList.Delete( j );
                            bFound = true;
                            break;
                        }
                    }

                    if ( !bFound )
                    {
                        cutfOverlayObject *pOverlayObject = rage_new cutfOverlayObject( 0, overlayNames[i].c_str() );                    
                        cutsceneExporter.GetCutsceneFile().AddObject( pOverlayObject );
                    }
                }
            }

            // remove unused overlays
            if ( overlayObjectList.GetCount() > 0 )
            {
                for ( int i = 0; i < overlayObjectList.GetCount(); ++i )
                {
                    cutsceneExporter.GetCutsceneFile().RemoveObject( overlayObjectList[i] );
                }
            }
        }

        // Make sure we have the latest event defs file, if any
        cutsceneExporter.LoadEventDefsFile();

        // update all of the objects and their events
        atArray<cutfObject *> &objectList = const_cast<atArray<cutfObject *> &>( cutsceneExporter.GetCutsceneFile().GetObjectList() );
        for ( int i = 0; i < objectList.GetCount(); ++i )
        {
            cutsceneExporter.LoadObjectData( objectList[i] );
        }

        cutsceneExporter.GetCutsceneFile().SortLoadEvents();
        cutsceneExporter.GetCutsceneFile().SortEvents();

        char cExportPath[RAGE_MAX_PATH];
        cutsceneExporter.GetExportPath( cExportPath, sizeof(cExportPath) );

        char cSceneFilename[RAGE_MAX_PATH];
        sprintf( cSceneFilename, "%s.%s", cExportPath, PI_CUTSCENE_XMLFILE_EXT );

        Displayf( "Saving cut file '%s'.", cSceneFilename );

        bool bRes = cutsceneExporter.SaveCutFile( cSceneFilename );

        if ( (iJustCutfile == 0) && bRes )
        {
            // find all of the models
            atArray<SModelObject> modelObjectList;
            for ( int i = 0; i < objectList.GetCount(); ++i )
            {
                if ( !cutsceneExporter.IsObjectAnimatable( objectList[i] ) )
                {
                    continue;
                }

                SModelObject modelObject;
                modelObject.pObject = objectList[i];
                modelObject.pModel = cutsceneExporter.FindModelForObject( modelObject.pObject );
                modelObjectList.Grow() = modelObject;
            }        

            if ( !cutsceneExporter.DoExport( modelObjectList, 
                cutsceneExporter.GetCutsceneFile().GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_INTERP_CAMERA_FLAG ) ) )
            {
                delete g_pExportProgress;
                g_pExportProgress = NULL;

                PrintResults( "Export Cutscene" );

                diagLogCallback = previousLogCallback;
                Py_RETURN_FALSE;
            }
        }

        delete g_pExportProgress;
        g_pExportProgress = NULL;

        PrintResults( "Export Cutscene" );

        diagLogCallback = previousLogCallback;

        if ( bRes && (g_errorCount == 0) )
        {
            Py_RETURN_TRUE;
        }
        else
        {
            Py_RETURN_FALSE;
        }
    }

    PrintResults( "Export Cutscene" );

    diagLogCallback = previousLogCallback;
    Py_RETURN_FALSE;
}

//-----------------------------------------------------------------------------

static PyMethodDef rexMbPython_methods[] =
{
	{"doExport", doExport, METH_VARARGS, "Export"},
    {"doCutsceneExport", doCutsceneExport, METH_VARARGS, "Export"},
	{NULL, NULL, 0, NULL},
};

PyMODINIT_FUNC initRexMBPython(void)
{
	(void) Py_InitModule("RexMBPython", rexMbPython_methods);
}

//-----------------------------------------------------------------------------

BOOL WINAPI DllMain(
    HINSTANCE hinstDLL,  // handle to DLL module
    DWORD fdwReason,     // reason for calling function
    LPVOID lpReserved )  // reserved
{
	switch( fdwReason )
	{
	case DLL_PROCESS_ATTACH:
		{
			char appName[] = "RexMBPython.pyd";
			char* p_Argv[1];

			p_Argv[0] = appName;

			sysParam::Init(1,p_Argv);
			INIT_PARSER;
            RexRageCutsceneExport::InitClass();     // initializes crAnimation and crClip
		}
		break;
	case DLL_PROCESS_DETACH:
		{
			RexRageCutsceneExport::ShutdownClass(); // shuts down crAnimation and crClip
			SHUTDOWN_PARSER;
		}
		break;
	}
	return TRUE;  // Successful DLL_PROCESS_ATTACH.
}

//-----------------------------------------------------------------------------
