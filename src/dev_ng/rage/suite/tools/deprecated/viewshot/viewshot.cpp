//
// viewshot\viewshot.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.


#define ENABLE_RFS 1


#include "bank/bkmgr.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "gizmo/manager.h"
#include "gizmo/rotation.h"
#include "gizmo/translation.h"
#include "input/keys.h"
#include "math/angmath.h"
#include "paging/streamer.h"
#include "parser/manager.h"
#include "pharticulated/articulatedcollider.h"
#include "phbound/boundbox.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundsphere.h"
#include "phcore/conversion.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "sample_physics/demoobject.h"
#include "sample_physics/sample_physics.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"	


PARAM(file, "Reserved for future expansion, not currently hooked up");

namespace ragesamples {

using namespace rage;

PFD_DECLARE_GROUP_ON(ViewShots);
PFD_DECLARE_ITEM_ON(ShotPaths,Color_red,ViewShots);

////////////////////////////////////////////////////////////////
// 

struct ComputeBallHitData
{
	Vector3 ServeTableHitPos;
	Vector3 HitTableSidePos;
	bool HitServerSide;
	bool HitTableSide;
	bool TouchedNet;
};


class viewShotSampleManager : public physicsSampleManager
{
public:

	virtual void InitClient()
	{
		physicsSampleManager::InitClient();

		// Load assets from the "assets" folder one level up from the executable's location in the source tree, or from the "assets" folder in the
		// executable's location when copied elsewhere.
		//ASSET.SetPath("assets");

		m_Mapper.Reset();
		m_Mapper.Map(IOMS_KEYBOARD,KEY_F4,m_Reset);
		m_Mapper.Map(IOMS_KEYBOARD,KEY_SPACE,m_Serve);
		m_Mapper.Map(IOMS_KEYBOARD,KEY_RETURN,m_Return);
		m_Mapper.Map(IOMS_KEYBOARD,KEY_UP,m_IncreaseSpeed);
		m_Mapper.Map(IOMS_KEYBOARD,KEY_DOWN,m_DecreaseSpeed);

		// Create the demo world with a table and ball.
		phDemoWorld& pongShotWorld = MakeNewDemoWorld("pongShotWorld");
		phDemoObject* tableObject = pongShotWorld.CreateFixedObject("pingpongtable");
		Vector3 position(0.0f,1.0f,2.0f);
		Vector3 rotation(0.0f,0.0f,0.0f);
		bool alignBottom = false;
		bool alwaysActive = false;
		bool startActive = false;
		phDemoObject* ballObject = pongShotWorld.CreateObject("sphere_020",position,alignBottom,rotation,alwaysActive,startActive);

		// Set the table and ball parameters for shooting.
		Assert(ballObject && ballObject->GetPhysInst());
		phInst& ballInstance = *ballObject->GetPhysInst();
		Assert(ballInstance.GetArchetype() && ballInstance.GetArchetype()->GetBound());
		const phBoundSphere& ballBound = *static_cast<phBoundSphere*>(ballInstance.GetArchetype()->GetBound());
		m_BallRadius = ballBound.GetRadius();
		const phBoundComposite& tableAndNetBound = *static_cast<phBoundComposite*>(tableObject->GetPhysInst()->GetArchetype()->GetBound());
		const phBoundBox& tableBound = *static_cast<phBoundBox*>(tableAndNetBound.GetBound(0));
		const phBoundBox& netBound = *static_cast<phBoundBox*>(tableAndNetBound.GetBound(1));
		m_TableHeight = tableBound.GetBoundingBoxMax().y-tableBound.GetBoundingBoxMin().y;
		m_TableHalfLength = tableBound.GetBoundingBoxMax().z;
		m_TableWidth = tableBound.GetBoundingBoxMax().x-tableBound.GetBoundingBoxMin().x;
		m_NetHeight = netBound.GetBoundingBoxMax().y-netBound.GetBoundingBoxMin().y;
		m_TargetPosition.Set(0.0f,m_TableHeight,-1.0f);
		m_BallPosition = (Vector3*)(&ballInstance.GetPosition());
		m_Serving = false;
		m_Iterating = false;
		m_MaxPitch = PH_DEG2RAD(60.0f);
		m_SpinAirForce = 0.017f;
		m_Drag = 0.011f;
		m_BallTableFriction = 0.1f;
		m_BallTableElasticity = 0.9f;
		m_MaxNumIterations = 64;
		m_CurrentMaxIterations = 0;
		m_ServeSpeed = 5.0f;
		m_ReturnSpeed = 10.0f;
		m_Gravity = 11.647f;

		// Create the gizmos for controlling the ball hit position and the target position.
		m_TargetGizmo = new gzTranslation(m_TargetPosition);
		m_BallHitGizmo = new gzTranslation(*m_BallPosition);

		// Set and activate the current demo world.
		m_Demos.SetCurrentDemo(0);
		m_Demos.GetCurrentWorld()->Activate();
	}

	virtual void InitCamera()
	{
		Vector3 lookFrom(0.0f,2.0f,4.0f);
		Vector3 lookTo(0.0f,0.0f,0.0f);
		InitSampleCamera(lookFrom,lookTo);
	}

	virtual void Update()
	{
		physicsSampleManager::Update();

		if (m_IncreaseSpeed.IsPressed())
		{
			if (m_Serving)
			{
				m_ServeSpeed += 0.1f;
			}
			else
			{
				m_ReturnSpeed += 0.1f;
			}
		}
		else if (m_DecreaseSpeed.IsPressed())
		{
			if (m_Serving)
			{
				m_ServeSpeed -= 0.1f;
			}
			else
			{
				m_ReturnSpeed -= 0.1f;
			}
		}

		bool serve = m_Serve.IsPressed();
		bool hitBack = m_Return.IsPressed();
		bool serveOrHitBack = (serve || hitBack);
		if (!m_Iterating && serveOrHitBack)
		{
			// Set the maximum number of iterations to zero, and set the serving boolean.
			m_CurrentMaxIterations = 0;
			m_Serving = serve;
			m_Iterating = true;
		}

		if (m_Iterating && serveOrHitBack)
		{
			// Find the post-hit with each incrementing maximum number of iterations.
			if (m_CurrentMaxIterations<m_MaxNumIterations)
			{
				// Find the post-hit velocity with the current number of iterations.
				m_CurrentMaxIterations++;
				bool goodShot = IterateComputeBallHit(m_Serving);

				// Set the post-hit velocity of this iteration for drawing.
				m_IteratedBallVel[m_CurrentMaxIterations-1].Set(m_PostHitVel);

				// See if this shot hit the target.
				if (goodShot)
				{
					// This shot hit the target, so stop iterating.
					m_Iterating = false;
				}
			}
		}

		for (int iteration=0; iteration<m_CurrentMaxIterations; iteration++)
		{
			// Draw the ball path.
			Vector3 tableHitPos;
			bool ignoreTableTarget = false;
			bool drawPath = true;
			ComputeBallHit(*m_BallPosition,m_IteratedBallVel[iteration],m_PostHitAngVel,m_TargetPosition,m_Serving,tableHitPos,ignoreTableTarget,NULL,drawPath);
		}

		m_Mapper.Update();
	}

	virtual void ShutdownClient()
	{
		physicsSampleManager::ShutdownClient();
	}

	virtual void DrawClient()
	{
		physicsSampleManager::DrawClient();
	}

	virtual void DrawHelpClient()
	{
		physicsSampleManager::DrawHelpClient();

		// Draw the ball hit parameters.
		char tmpString[100];
		formatf(tmpString,sizeof(tmpString),"serve speed: %6.3f", m_ServeSpeed);
		DrawString(tmpString, false);
		formatf(tmpString,sizeof(tmpString),"return speed: %6.3f", m_ReturnSpeed);
		DrawString(tmpString, false);

		// Draw input instructions.
		float textLeft = 30.0f;
		float textTop = 140.0f;
		SetDrawStringPos(textLeft,textTop);
		formatf(tmpString,sizeof(tmpString),"serve iteration: space bar");
		DrawString(tmpString, false);
		formatf(tmpString,sizeof(tmpString),"hit iteration: enter key");
		DrawString(tmpString, false);
		formatf(tmpString,sizeof(tmpString),"change speed: arrow up/down");
		DrawString(tmpString, false);
	}

#if __BANK
	virtual void AddWidgetsClient ()
	{
		bkBank& bank = BANKMGR.CreateBank("viewshot");
		bank.AddSlider("Gravity",&m_Gravity,0.0f,100.0f,0.1f);
	}
#endif

	bool IterateComputeBallHit (bool serving)
	{
		m_PostHitAngVel.Set(-11.06f,0.0f,0.0f);
		float postHitSpeed = serving ? m_ServeSpeed : m_ReturnSpeed;
		bool aimStraightAtTarget = false;
		bool ignoreTableTarget = false;
		float servePitchAngle = -0.03125f*PI;
		m_NumTimesComputePostHitVel = 0;
		bool goodShot = ComputePostHitVel(*m_BallPosition,m_TargetPosition,postHitSpeed,m_PostHitAngVel,serving,m_PostHitVel,
											aimStraightAtTarget,ignoreTableTarget,servePitchAngle);
		return goodShot;
	}

	bool ComputePostHitVel (const Vector3& hitPos, const Vector3& targetPos, float initialSpeed, const Vector3& ballAngVel, bool serving, Vector3& postHitVel,
							bool aimStraightAtTarget, bool ignoreTableTarget, float servePitchAngle)
	{
		if (++m_NumTimesComputePostHitVel>128)
		{
			DebugAssert(0);
			return false;
		}

		// Set the initial velocity to be horizontally straight for the target, and vertically toward a point
		// 1 meter above or below the target. If this is not a serve, the aim point is above the target. If this
		// is a serve, the aim point is below the target.
		float flatDistToTarget = hitPos.FlatDist(targetPos);
		float yaw = (flatDistToTarget>SMALL_FLOAT ? Asinf((targetPos.x-hitPos.x)/flatDistToTarget) : 0.0f);
		float maxPitch = m_MaxPitch;
		float pitch = 0.0f;
		if (serving)
		{
			pitch = servePitchAngle;
		}
		else
		{
			// This is not a speed-computing serve, so aim toward the target (or above or below it) instead of aiming horizontally.
			Vector3 sightTarget(targetPos);
			if (!aimStraightAtTarget)
			{
				sightTarget.y += 1.0f;
			}

			float distanceToSightTarget = hitPos.Dist(sightTarget);
			pitch = Min(Asinf((sightTarget.y-hitPos.y)/distanceToSightTarget),maxPitch);
		}

		float minPitch;
		if (serving || hitPos.y>targetPos.y)
		{
			minPitch = -0.5f*PI;
		}
		else
		{
			minPitch = safe_atan2f(targetPos.y-hitPos.y,flatDistToTarget);
		}

		// Initialize the ball hit data to hit the server's side of the table and not hit the end of the table.
		ComputeBallHitData ballHitData;
		ballHitData.HitServerSide = true;
		ballHitData.HitTableSide = false;
		ballHitData.TouchedNet = false;

		// See where the ball hits the table and iterate to find a better solution.
		Vector3 hitVel,testHitVel,bestHitVel,testTablePos;
		float hitSpeed = initialSpeed;
		float bestDifference2 = FLT_MAX;
		float delYaw = 0.0f;
		float delPitch = 0.0f;
		float delSpeed = 0.0f;
		float testDelAngle = 0.1f;
		float testDelSpeed = 0.1f;
		float prevDiff2 = LARGE_FLOAT;
		const float maxDelAngle = 0.5f;
		const float maxDelSpeed = 5.0f;
		const float maxError2 = 1.0e-4f;
		int numHalves=0,maxNumHalves=8;
		int numBadChanges=0,maxNumBadChanges=4,numTries=0;
		const int maxNumIterations = m_CurrentMaxIterations;
		bool bestHitHitServerSide = true;
		bool bestHitTableSide = false;
		bool bestHitTouchedNet = false;

		Assert(maxNumIterations>0);
		while (numTries<maxNumIterations)
		{
			// Set the ball's post-hit velocity and find the resulting table hit position.
			float yawSine,yawCosine,pitchSine,pitchCosine;
			cos_and_sin(yawCosine,yawSine,yaw);
			cos_and_sin(pitchCosine,pitchSine,pitch);
			if (hitPos.z>0.0f)
			{
				yawCosine = -yawCosine;
			}
			hitVel.Set(hitSpeed*yawSine*pitchCosine,hitSpeed*pitchSine,hitSpeed*yawCosine*pitchCosine);
			ballHitData.HitServerSide = true;
			ballHitData.HitTableSide = false;
			ballHitData.TouchedNet = false;
			Vector3 tableHitPos;
			const bool newDrawBallPath = false;
			ComputeBallHit(hitPos,hitVel,ballAngVel,targetPos,serving,tableHitPos,ignoreTableTarget,&ballHitData,newDrawBallPath);
			float differenceX = targetPos.x-tableHitPos.x;
			float differenceZ = targetPos.z-tableHitPos.z;
			float difference2 = square(differenceX)+square(differenceZ);

			// See if the error improved from the last iteration.
			if (difference2<prevDiff2 || numTries==0)
			{
				// The error is smaller than the previous error, so do another iteration.
				// See if it's the best solution found so far.
				if (difference2<bestDifference2)
				{
					// This is the best solution found so far, so save it in case a better solution is not found.
					bestDifference2 = difference2;
					bestHitVel.Set(hitVel);
					bestHitTableSide = ballHitData.HitTableSide;
					bestHitHitServerSide = ballHitData.HitServerSide;
					bestHitTouchedNet = ballHitData.TouchedNet;
					if (difference2<maxError2)
					{
						// The combined errors are small enough, so use the current hit velocity.
						break;
					}
				}

				// Estimate the partial derivatives of the position with respect to the yaw sine.
				float testAngle = CanonicalizeAngleFast(yaw+testDelAngle);
				float testCosine,testSine;
				cos_and_sin(testCosine,testSine,testAngle);
				if (hitPos.z>0.0f)
				{
					testCosine = -testCosine;
				}
				testHitVel.Set(hitSpeed*testSine*pitchCosine,hitSpeed*pitchSine,hitSpeed*testCosine*pitchCosine);
				const bool newDrawBallPath = false;
				ComputeBallHit(hitPos,testHitVel,ballAngVel,targetPos,serving,testTablePos,ignoreTableTarget,NULL,newDrawBallPath);
				float invTestDelAngle = 1.0f/testDelAngle;
				float dXdYaw = (testTablePos.x-tableHitPos.x)*invTestDelAngle;
				float dZdYaw = (testTablePos.z-tableHitPos.z)*invTestDelAngle;
				if (serving)
				{
					// Estimate the partial derivatives of the position with respect to the ball speed.
					float testSpeed = hitSpeed+testDelSpeed;
					testHitVel.Set(testSpeed*yawSine*pitchCosine,testSpeed*pitchSine,testSpeed*yawCosine*pitchCosine);
					ComputeBallHit(hitPos,testHitVel,ballAngVel,targetPos,serving,testTablePos,ignoreTableTarget,NULL,newDrawBallPath);
					float invTestDelSpeed = 1.0f/testDelSpeed;
					float dXdSpeed = (testTablePos.x-tableHitPos.x)*invTestDelSpeed;
					float dZdSpeed = (testTablePos.z-tableHitPos.z)*invTestDelSpeed;

					// Find the change in hit velocity to get closer to the target.
					float derivDenom = dXdYaw*dZdSpeed-dZdYaw*dXdSpeed;
					if (fabsf(derivDenom)>SMALL_FLOAT)
					{
						// The equations are linearly independent.
						float derivDenomInv = 1.0f/derivDenom;
						delYaw = (differenceX*dZdSpeed-differenceZ*dXdSpeed)*derivDenomInv;
						delSpeed = (differenceZ*dXdYaw-differenceX*dZdYaw)*derivDenomInv;
					}
					else
					{
						// The equations are linearly dependent. Choose identical changes.
						float derivDenomX = dXdYaw+dXdSpeed;
						float derivDenomZ = dZdYaw+dZdSpeed;
						delYaw = (fabsf(derivDenomX)>VERY_SMALL_FLOAT ? differenceX/derivDenomX : 0.0f);
						delSpeed = (fabsf(derivDenomZ)>VERY_SMALL_FLOAT ? differenceZ/derivDenomZ : 0.0f);
						if (fabsf(delYaw)<VERY_SMALL_FLOAT && fabsf(delSpeed<VERY_SMALL_FLOAT))
						{
							// The terms in the equations are all nearly zero, so stop iterating.
							hitVel.Set(bestHitVel);
							break;
						}
					}

					// Clamp the changes in yaw angle and speed to a range that will make an approximately linear response.
					delYaw = Clamp(delYaw,-maxDelAngle,maxDelAngle);
					float minDelSpeed = Max(-hitSpeed,-maxDelSpeed);
					delSpeed = Clamp(delSpeed,minDelSpeed,maxDelSpeed);

					// Change the yaw angle and speed.
					yaw = CanonicalizeAngleFast(yaw+delYaw);
					hitSpeed += delSpeed;
				}
				else
				{
					// Estimate the partial derivatives of the position with respect to the pitch sine.
					testAngle = CanonicalizeAngle(pitch+testDelAngle);
					cos_and_sin(testCosine,testSine,testAngle);
					testHitVel.Set(hitSpeed*yawSine*testCosine,hitSpeed*testSine,hitSpeed*yawCosine*testCosine);
					ComputeBallHit(hitPos,testHitVel,ballAngVel,targetPos,serving,testTablePos,ignoreTableTarget,NULL,newDrawBallPath);
					float dXdPitch = (testTablePos.x-tableHitPos.x)*invTestDelAngle;
					float dZdPitch = (testTablePos.z-tableHitPos.z)*invTestDelAngle;

					// Find the change in hit velocity to get closer to the target.
					float derivDenom = dXdYaw*dZdPitch-dZdYaw*dXdPitch;
					if (fabsf(derivDenom)>SMALL_FLOAT)
					{
						// The equations are linearly independent.
						float derivDenomInv = 1.0f/derivDenom;
						delYaw = (differenceX*dZdPitch-differenceZ*dXdPitch)*derivDenomInv;
						delPitch = (differenceZ*dXdYaw-differenceX*dZdYaw)*derivDenomInv;
					}
					else
					{
						// The equations are linearly dependent. Choose identical changes in the x and z velocities.
						float derivDenomX = dXdYaw+dXdPitch;
						float derivDenomZ = dZdYaw+dZdPitch;
						delYaw = (fabsf(derivDenomX)>VERY_SMALL_FLOAT ? differenceX/derivDenomX : 0.0f);
						delPitch = (fabsf(derivDenomZ)>VERY_SMALL_FLOAT ? differenceZ/derivDenomZ : 0.0f);
						if (fabsf(delYaw)<VERY_SMALL_FLOAT && fabsf(delPitch<VERY_SMALL_FLOAT))
						{
							// The terms in the equations are all nearly zero, so stop iterating.
							hitVel.Set(bestHitVel);
							break;
						}
					}

					// Clamp the changes in yaw and pitch angles to a range that will make an approximately linear response.
					delYaw = Clamp(delYaw,-maxDelAngle,maxDelAngle);
					float maxDelPitch = Min(maxPitch-pitch,maxDelAngle);
					float minDelPitch = Max(minPitch-pitch,-maxDelAngle);
					delPitch = Clamp(delPitch,minDelPitch,maxDelPitch);

					// Change the ball hit yaw and pitch.
					yaw = CanonicalizeAngleFast(yaw+delYaw);
					pitch = CanonicalizeAngleFast(pitch+delPitch);
				}

				// Set the previous difference squared before finding a new hit.
				prevDiff2 = difference2;

				// Reset the number of consecutive times halving the sine changes (in the else below).
				numHalves = 0;

				// Increment the number of iterations for this shot.
				numTries++;
			}
			else
			{
				// The error is larger than the previous error, try again with half the changes.
				delYaw *= 0.5f;
				delPitch *= 0.5f;
				yaw = CanonicalizeAngleFast(yaw-delYaw);
				pitch = CanonicalizeAngleFast(pitch-delPitch);

				if (numHalves==0)
				{
					numBadChanges++;
					if (numBadChanges>maxNumBadChanges)
					{
						hitVel.Set(bestHitVel);
						break;
					}
				}

				numHalves++;
				if (numHalves>maxNumHalves)
				{
					hitVel.Set(bestHitVel);
					break;
				}
			}
		}
	
		if (numTries<maxNumIterations && bestHitTableSide && !aimStraightAtTarget)
		{
			// This shot is from below the table height and it hit the side or end of the table on its way up.
			// Recalculate the shot to miss the table on the way up and ignore the target position.
			Vector3 newTargetPos;
			const float smallTableOffset = 0.002f;
			float maxHalfWidth = 0.5f*m_TableWidth+m_BallRadius+smallTableOffset;
			newTargetPos.x = Clamp(ballHitData.HitTableSidePos.x,-maxHalfWidth,maxHalfWidth);
			newTargetPos.y = targetPos.y;
			float maxHalfLength = m_TableHalfLength+m_BallRadius+smallTableOffset;
			newTargetPos.z = Clamp(ballHitData.HitTableSidePos.z,-maxHalfLength,maxHalfLength);
			float targetTowardNet = (targetPos.y-ballHitData.HitTableSidePos.y)/tanf(m_MaxPitch);
			if (newTargetPos.z>0.0f && newTargetPos.z>hitPos.z-targetTowardNet)
			{
				newTargetPos.z = hitPos.z-targetTowardNet;
			}
			else if (newTargetPos.z<0.0f && newTargetPos.z<hitPos.z+targetTowardNet)
			{
				newTargetPos.z = hitPos.z+targetTowardNet;
			}
			const bool effectiveServing = false;
			const bool newAimStraightAtTarget = true;
			return ComputePostHitVel(hitPos,newTargetPos,initialSpeed,ballAngVel,effectiveServing,postHitVel,newAimStraightAtTarget,ignoreTableTarget,servePitchAngle);
		}

		if (numTries<maxNumIterations && serving)
		{
			const float delServePitchAngle = 0.03125f*PI;
			if (servePitchAngle<0.0f)
			{
				if (bestHitTouchedNet)
				{
					// This is a horizontal serve that hit the net face, so keep the computed speed and recompute the serve with a different pitch angle.
					const float minServePitchAngle = -0.375f*PI;
					float newServePitchAngle = 0.0f;
					if (bestHitHitServerSide && servePitchAngle>minServePitchAngle+delServePitchAngle)
					{
						newServePitchAngle = servePitchAngle-delServePitchAngle;
					}

					return ComputePostHitVel(hitPos,targetPos,hitSpeed,ballAngVel,serving,postHitVel,aimStraightAtTarget,ignoreTableTarget,newServePitchAngle);
				}
			}
			
			if ((servePitchAngle<0.0f && !bestHitHitServerSide) || (servePitchAngle>=0.0f && bestHitTouchedNet))
			{
				const float maxServePitchAngle = m_MaxPitch;
				if (servePitchAngle<maxServePitchAngle-delServePitchAngle)
				{
					// This serve aimed downward and missed the server's side of the table, so try aiming horizontally and then angling up if needed, or it was not aimed
					// downward and it hit the net, so aim upward more.
					float newServePitchAngle = Max(0.0f,servePitchAngle+delServePitchAngle);
					return ComputePostHitVel(hitPos,targetPos,hitSpeed,ballAngVel,serving,postHitVel,aimStraightAtTarget,ignoreTableTarget,newServePitchAngle);
				}
				
				const float minAbsTargetZ = 0.2f;
				if (bestHitTouchedNet && fabsf(targetPos.z)>minAbsTargetZ)
				{
					// This serve aimed upward as far as it could and it hit the net, so move the target position closer to the net and aim horizontally.
					Vector3 newTargetPos(targetPos);
					newTargetPos.z *= 0.75f;
					float newServePitchAngle = 0.0f;
					return ComputePostHitVel(hitPos,newTargetPos,hitSpeed,ballAngVel,serving,postHitVel,aimStraightAtTarget,ignoreTableTarget,newServePitchAngle);
				}
			}

			if (!ballHitData.HitServerSide)
			{
				// This is a serve that missed the server's side of the table. Move the target by the amount of the miss and try again.
				bool tryAgain = false;
				Vector3 newTargetPos(targetPos);
				float tableHalfWidth = 0.5f*m_TableWidth;
				const float smallInset = 0.01f;
				if (fabsf(ballHitData.ServeTableHitPos.x)>tableHalfWidth)
				{
					// The ball missed the server's side of the table off to one side.
					float tableSideAimPos = (ballHitData.ServeTableHitPos.x>0.0f ? tableHalfWidth-smallInset : -tableHalfWidth+smallInset);
					float offTableSide = ballHitData.ServeTableHitPos.x-tableSideAimPos;
					newTargetPos.x -= offTableSide;
					tryAgain = true;
				}

				float tableHalfLength = m_TableHalfLength;
				if (fabsf(ballHitData.ServeTableHitPos.z)>tableHalfLength)
				{
					// The ball missed the server's side of the table off the end.
					float tableEndAimPos = (ballHitData.ServeTableHitPos.z>0.0f ? tableHalfLength-smallInset : -tableHalfLength+smallInset);
					float offTableEnd = ballHitData.ServeTableHitPos.z-tableEndAimPos;
					newTargetPos.z -= offTableEnd;
					tryAgain = true;
				}

				if (tryAgain)
				{
					return ComputePostHitVel(hitPos,newTargetPos,initialSpeed,ballAngVel,serving,postHitVel,aimStraightAtTarget,ignoreTableTarget,servePitchAngle);
				}
			}
		}

		postHitVel.Set(hitVel);

		// Return true if the number of iterations is less than the maximum, false if it hit the maximum.
		return (numTries<maxNumIterations);
	}

	void CheckNetHit (ComputeBallHitData* ballHitData, const Vector3& ballPos, const Vector3& prevBallPos, float netHeight)
	{
		if (ballHitData && !SameSign(ballPos.z,prevBallPos.z))
		{
			// There is a ball hit data, and the ball crossed z==0, so see if it hit the net face.
			if (ballPos.y<netHeight || prevBallPos.y<netHeight)
			{
				// The ball crossed z==0 and is lower than the net top, or it was on the last frame, so record a net face hit.
				// This a little less accurate than the actual net collisions, but it only results in recalculation of the serve
				// pitch angle to avoid a net hit. The inaccuracy should avoid some net top collisions, which should be a good thing.
				ballHitData->TouchedNet = true;
			}
		}
	}

	void ComputeBallHit (const Vector3& hitPos, const Vector3& hitVel, const Vector3& hitAngVel, const Vector3& targetPos, bool serving,
							Vector3& tableHitPos, bool ignoreTableTarget, ComputeBallHitData* ballHitData, bool drawPath)
	{
		// Make sure the ball's not getting hit downward from below the table.
		DebugAssert(hitPos.y>targetPos.y || hitVel.y>0.0f);
		Vector3 ballVel(hitVel);
		Vector3 ballPos(hitPos);
		Vector3 ballAngVel(hitAngVel);
		Vector3 ballAccel(ORIGIN),prevBallPos;
		const float frameTime = TIME.GetSeconds();
		float elapsedTime = 0.0f;
		float tableHalfLength = m_TableHalfLength;
		float tableHalfWidth = 0.5f*m_TableWidth;
		float ballRadius = m_BallRadius;
		float netHeight = m_NetHeight+m_TableHeight+ballRadius;
		bool aimToMissTableSide = ignoreTableTarget && fabsf(targetPos.z)>tableHalfLength;
		bool testHitTableSide = (ballHitData && hitPos.y<targetPos.y && !aimToMissTableSide &&
								(fabsf(hitPos.z)>tableHalfLength || fabsf(hitPos.x)>tableHalfWidth));
		const float maxElapsedTime = 24.0f;
		float bounceHeight = targetPos.y+ballRadius;
		while ((ballPos.y>bounceHeight || ballVel.y>0.0f) && (elapsedTime<maxElapsedTime) &&
				(!aimToMissTableSide || ballPos.y<bounceHeight))
		{
			ballAccel.Set(CalcBallAccel(ballVel,ballAngVel));
			ballVel.AddScaled(ballAccel,frameTime);
			prevBallPos.Set(ballPos);
			ballPos.AddScaled(ballVel,frameTime);
			elapsedTime += frameTime;
			CheckNetHit(ballHitData,ballPos,prevBallPos,netHeight);

			if (drawPath)
			{
				DrawPathSegment(prevBallPos,ballPos);
			}

			if (testHitTableSide)
			{
				DebugAssert(prevBallPos.y<bounceHeight);
				if (((fabsf(prevBallPos.x)>tableHalfWidth && fabsf(ballPos.x)<tableHalfWidth) ||
					(fabsf(prevBallPos.z)>tableHalfLength && fabsf(ballPos.z)<tableHalfLength)) && prevBallPos.y<bounceHeight)
				{
					// The ball just hit the side or end of the table.
					ballHitData->HitTableSide = true;
					ballHitData->HitTableSidePos.Set(prevBallPos);
					testHitTableSide = false;
				}
				else if (ballPos.y>bounceHeight)
				{
					// The ball passed above the table without hitting its side or end.
					ballHitData->HitTableSide = false;
					testHitTableSide = false;
				}
			}
		}

		if (ballPos.y<=bounceHeight && prevBallPos.y>bounceHeight)
		{
			// The ball hit the table. Back up to the table height.
			DebugAssert(ballVel.y<=0.0f);
			float postBounceFrameTime = (ballVel.y<-SMALL_FLOAT ? (ballPos.y-bounceHeight)/ballVel.y : 0.0f);
			DebugAssert(postBounceFrameTime<=frameTime);
			ballPos.SubtractScaled(ballVel,postBounceFrameTime);
			if (drawPath)
			{
				DrawPathSegment(prevBallPos,ballPos);
			}

			if (serving)
			{
				// The ball bounced during a serve.
				if (ballHitData)
				{
					// Set the position where the ball hit the table on the first serving bounce.
					ballHitData->ServeTableHitPos.Set(ballPos);

					// Record whether the ball is hitting the server's side of the table within the table extents.
					const float insideFromEdge = 0.005f;
					ballHitData->HitServerSide = (SameSign(ballPos.z,hitPos.z) &&
													fabsf(ballPos.z)<=m_TableHalfLength-insideFromEdge &&
													fabsf(ballPos.x)<=0.5f*m_TableWidth-insideFromEdge);
				}

				// Back up the ball velocity.
				ballVel.SubtractScaled(ballAccel,postBounceFrameTime);
				DebugAssert(ballVel.y<0.0f);

				// Change the velocity and angular velocity from the table collision.
				Vector3 normal(YAXIS);
				Vector3 postHitVelocity,postHitAngVelocity;
				CalcBallCollision(normal,ballVel,ballAngVel,m_BallRadius,m_BallTableFriction,m_BallTableElasticity,postHitVelocity,postHitAngVelocity,ballPos);
				ballVel.Set(postHitVelocity);
				ballAngVel.Set(postHitAngVelocity);

				// Finish the bounce frame.
				prevBallPos.Set(ballPos);
				ballAccel.Set(CalcBallAccel(ballVel,ballAngVel));
				ballVel.AddScaled(ballAccel,postBounceFrameTime);
				ballPos.AddScaled(ballVel,postBounceFrameTime);
				CheckNetHit(ballHitData,ballPos,prevBallPos,netHeight);

				// Continue the ball's trajectory to hit the table a second time.
				while ((ballPos.y>targetPos.y || ballVel.y>0.0f) && (elapsedTime<maxElapsedTime))
				{
					ballAccel.Set(CalcBallAccel(ballVel,ballAngVel));
					ballVel.AddScaled(ballAccel,frameTime);
					prevBallPos.Set(ballPos);
					ballPos.AddScaled(ballVel,frameTime);
					elapsedTime += frameTime;
					CheckNetHit(ballHitData,ballPos,prevBallPos,netHeight);
					if (drawPath)
					{
						DrawPathSegment(prevBallPos,ballPos);
					}
					
					prevBallPos.Set(ballPos);
				}

				// Back up to the table height.
				postBounceFrameTime = (ballPos.y-targetPos.y)/ballVel.y;
				ballPos.SubtractScaled(ballVel,postBounceFrameTime);
			}
		}

		// Set the position at which the ball hit the table.
		tableHitPos.Set(ballPos);
	}

	Vector3 CalcBallAccel (const Vector3& ballVel, const Vector3& ballAngVel) const
	{
		// Calculate the acceleration from spin air resistance.
		Vector3 accel(ballAngVel);
		accel.Cross(ballVel);
		float spinAirForce = m_SpinAirForce;
		accel.Scale(spinAirForce);

		// Calculate the acceleration from drag air resistance.
		float ballSpeed2 = ballVel.Mag2();
		if (ballSpeed2>SMALL_FLOAT)
		{ 
			float drag = m_Drag;
			accel.SubtractScaled(ballVel,drag*sqrtf(ballSpeed2));
		}

		// Add the acceleration from gravity.
		accel.y -= m_Gravity;

		return accel;
	}

	void CalcBallCollision (const Vector3& normal, const Vector3& velocity, const Vector3& angVelocity, float ballRadius, float friction, float elasticity,
							Vector3& postHitVelocity, Vector3& postHitAngVelocity, const Vector3& PF_DRAW_ONLY(ballPos))
	{
		// Find the local velocity of the ball at the collision point.
		Vector3 ballLocalVel(angVelocity);
		ballLocalVel.Cross(normal);
		ballLocalVel.Scale(-ballRadius);
		ballLocalVel.Add(velocity);

		// Find the local velocity of the ball along the collision normal.
		Vector3 normalLocalVel(normal);
		normalLocalVel.Scale(ballLocalVel.Dot(normal));
		float normalLocalSpeed = SqrtfSafe(normalLocalVel.Mag2());

		// Find the local velocity of the ball perpendicular to the collision normal.
		Vector3 planeLocalVel(ballLocalVel);
		planeLocalVel.Subtract(normalLocalVel);

		// See if the ball will slide on the net during the collision, and find the change in velocity.
		float ballPlaneVel2 = planeLocalVel.Mag2();
		float frictionVel2 = square(friction*normalLocalSpeed);
		float elasPlus1 = elasticity+1.0f;
		Vector3 delVel(normalLocalVel);
		delVel.Scale(-elasPlus1);
		if (0.16f*ballPlaneVel2<=frictionVel2 && frictionVel2>SMALL_FLOAT)
		{
			// The ball will not slide during the collision.
			delVel.SubtractScaled(planeLocalVel,0.4f*elasPlus1);
		}
		else
		{
			// The ball will slide during the collision.
			float planeScale = -2.0f*friction*normalLocalSpeed*invsqrtf(ballPlaneVel2);
			delVel.AddScaled(planeLocalVel,planeScale);
		}

		// Find the change in angular velocity of the ball.
		Vector3 delAngVel(normal);
		float threeOverTwoRadius = 1.5f/ballRadius;
		delAngVel.Scale(-threeOverTwoRadius);
		delAngVel.Cross(delVel);

		// Change the ball's velocity and angular velocity.
		postHitVelocity.Add(velocity,delVel);
		postHitAngVelocity.Add(angVelocity,delAngVel);

	#if __PFDRAW
		// Draw the ball at the bounce position.
		PFD_ShotPaths.DrawSphere(m_BallRadius,ballPos);
	#endif

	}

	void DrawPathSegment (const Vector3& PF_DRAW_ONLY(prevPosition), const Vector3& PF_DRAW_ONLY(position))
	{
	#if __PFDRAW
		if (PFD_ShotPaths.Begin())
		{
			grcWorldIdentity();
			PFD_ShotPaths.DrawLine(prevPosition,position);
			PFD_ShotPaths.End();
		}
	#endif
	}

	ioValue m_Reset,m_Serve,m_Return,m_IncreaseSpeed,m_DecreaseSpeed;
	gzTranslation* m_TargetGizmo;
	gzTranslation* m_BallHitGizmo;
	Vector3 m_TargetPosition,m_PostHitVel,m_PostHitAngVel;
	Vector3* m_BallPosition;
	Vector3 m_IteratedBallVel[256];
	float m_TableHeight,m_TableHalfLength,m_TableWidth,m_NetHeight,m_BallRadius;
	float m_MaxPitch,m_SpinAirForce,m_Drag;
	float m_BallTableFriction,m_BallTableElasticity;
	float m_ServeSpeed,m_ReturnSpeed;
	float m_Gravity;
	int m_MaxNumIterations,m_CurrentMaxIterations;
	int m_NumTimesComputePostHitVel;
	bool m_Serving,m_Iterating;
};

} // namespace ragesamples


int Main()
{
	ragesamples::viewShotSampleManager viewShot;
	viewShot.Init();
	viewShot.UpdateLoop();
	viewShot.Shutdown();
	return 0;
}





