set FILES=
set CUSTOM=
set TESTERS=viewshot
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% %RAGE_PH_LIBS%
set LIBS=%LIBS% cranimation devcam gizmo
set LIBS=%LIBS% profile 
set LIBS=%LIBS% parser parsercore 
set LIBS=%LIBS% breakableglass phglass fragment cloth spatialdata pheffects rmlighting
set LIBS=%LIBS% %RAGE_SAMPLE_GRCORE_LIBS% sample_physics sample_rmcore
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\src
set XDEFINE=USING_RAGE
