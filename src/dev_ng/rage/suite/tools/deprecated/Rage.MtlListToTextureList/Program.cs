using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Text;

namespace Rage.MtlListToTextureList
{
	class Program
	{
		static int Main(string[] args)
		{
			if (args.Length != 2)
			{
				Console.WriteLine("Usage : Rage.MtlListToTextureList.exe <input mtl list> <output texture list>");
				return -1;
			}

			// Get args
			string strInputMtlList = args[0];
			string strOutputTextureList = args[1];

			List<String> astrTextures = new List<string>();

			StreamReader sr = File.OpenText(strInputMtlList);

			string strMtlPathAndFilename;
			while ((strMtlPathAndFilename = sr.ReadLine()) != null)
			{
				if (strMtlPathAndFilename.Length > 0)
				{
					// Does the mtl file exist?
					if (!File.Exists(strMtlPathAndFilename))
					{
						// Mtl file is missing, so bail
						Console.Error.WriteLine("Error : File is missing : " + strMtlPathAndFilename);
						return -2;
					}

					// Open the material file for reading
					XmlDocument doc = new XmlDocument();
					doc.Load(strMtlPathAndFilename);

					// Get all the texture images referenced by this file
					string xpath = "//filename";
					XmlNodeList nodeList = doc.SelectNodes(xpath);

					// Add these textures to the list
					foreach (XmlNode node in nodeList)
					{
						if (node.InnerText != "none")
						{
							string strTexturePathAndFilename = node.InnerText.ToLower();
							if (!astrTextures.Contains(strTexturePathAndFilename))
							{
								astrTextures.Add(strTexturePathAndFilename);
							}
						}
					}
				}
			}
			// Console.WriteLine ("The end of the stream has been reached.");
			sr.Close();

			// Write out texture list
			astrTextures.Sort();

			StreamWriter sw = File.CreateText(strOutputTextureList);
			foreach (string strTexture in astrTextures)
			{
				sw.WriteLine(strTexture);
			}
			sw.Close();

			return 0;
		}
	}
}
