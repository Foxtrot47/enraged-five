#include "../../../Game/ActionTree/datamember.h"
#include "application.hpp"
#include <qcombobox.h>

// test
class DataMemberEnumComboBox : public QComboBox
{
	Q_OBJECT

public:
	DataMemberEnumComboBox( QWidget* parent = 0, const char* name = 0 );
	~DataMemberEnumComboBox(){}

    void SetDataMember( IDataMembers * dataMember, IEnumDataMember* dataMemberInterface );

public slots:
    virtual void SetValue( int value );

private:
    IEnumDataMember*  m_dataMemberInterface;
    IDataMembers*	  m_dataMembersInstance;
};
