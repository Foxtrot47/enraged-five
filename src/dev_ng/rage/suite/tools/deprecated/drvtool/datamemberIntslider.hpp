#include "datamemberslider.h"
#include "D:\Proto\testApp\slidermain\ActionTree\datamember.hpp"

class DataMemberIntSlider : public DataMemberSlider
{
public:
	DataMemberIntSlider()
    void SetDataMember( IDataMembers * dataMember, IIntDataMember * dataMemberInterface );

private:
    IIntDataMember* m_IIntDataMember;
    IDataMembers* m_dataMembersInstance;
}

class DataMemberFloatSlider : public DataMemberSlider
{
public:
	DataMemberIntSlider()
    void SetDataMember( IDataMembers * dataMember, IFloatDataMember * dataMemberInterface );

private:
    IFloatDataMember* m_IFloatDataMember;
    IDataMembers* m_dataMembersInstance;
}

void DataMemberIntSlider::SetDataMember( IDataMembers * dataMember, IIntDataMember * dataMemberInterface )
{
	m_dataMembersInstance = dataMember;
	
	m_IIntDataMember =  dataMemberInterface;
	
	QString displayName(m_IIntDataMember->GetName()->GetString());
	
	DataMemberName->setText(displayName);	
	
	int value = m_IIntDataMember->GetInt(m_dataMembersInstance);
		
	QString stringValue;
	
	stringValue.setNum(value);
	
	lineEdit->setText( stringValue );
}

void DataMemberFloatSlider::SetDataMember( IDataMembers * dataMember, IFloatDataMember * dataMemberInterface )
{
	m_dataMembersInstance = dataMember;
	
	m_IFloatDataMember =  dataMemberInterface;
	
	QString displayName(m_IIntDataMember->GetName()->GetString());
	
	DataMemberName->setText(displayName);	
	
	float value = m_IFloatDataMember->GetFloat(m_dataMembersInstance);
		
	QString stringValue;
	
	stringValue.setNum(value);
	
	lineEdit->setText( stringValue );
}