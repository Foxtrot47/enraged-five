
#include <qpopupmenu.h>
#include <qsplitter.h> 
#include <qlayout.h>

#include "ActionControllerView.hpp"
#include "ActionNodeView.hpp"
#include "actiontree.h"

#include "MotionTreeView.hpp"

extern Application* g_theApp;
extern ActionTree* g_ActionTree;
extern ActionControllerView* g_ActionControllerView;
extern MotionTreeView* g_MotionTreeView;
extern ActionControllerOwnerView* g_ActionControllerOwnerView;

extern char *s_red_exclamation[];
static int s_playHistory = 30;

//---------------------------------------------------------------------------
// Generic util funktions
//---------------------------------------------------------------------------
QListViewItem* findChild(QListViewItem* parent, QString& text, int textID)
{
    QListViewItem* myChild = parent->firstChild();
    while( myChild ) 
    {
        if(myChild->text(textID) == text)
        {
            return(myChild);
            break;
        }
        myChild = myChild->nextSibling();
    }

    return(NULL);
}

void ClearChildren(QListViewItem* parent)
{
    QListViewItem* child = parent->firstChild();
    while(child)
    {
        delete child;
        child = parent->firstChild();
    }
}

QListViewItem* findChildView(QListView* parent, QString& text, int textID)
{
    QListViewItem* myChild = parent->firstChild();
    while( myChild ) 
    {
        if(myChild->text(textID) == text)
        {
            return(myChild);
            break;
        }
        myChild = myChild->nextSibling();
    }

    return(NULL);
}

//---------------------------------------------------------------------------
// EntityObjectType... defines a gameObject
//---------------------------------------------------------------------------
EntityObjectType::EntityObjectType(TextParser& message)
{
    char charBuff[16];
    message.GetTokenString(charBuff, 16);
    m_Name.SetString(charBuff);
    message.GetNextToken();
    while(!message.EndOfText())
    {
        ObjectParameter* newObjectParameter = new ObjectParameter(message);
        message.GetNextToken();
        m_Parameters.push_back(newObjectParameter);
    }
}

//---------------------------------------------------------------------------
// ObjectParameter... defines a parameter Type and Instance
//---------------------------------------------------------------------------
ObjectParameter::ObjectParameter(TextParser& message)
{
    char charBuff[64];
    message.GetTokenString(charBuff, 64);
    this->m_Type.SetString(charBuff);
    message.GetNextToken();
    message.GetTokenString(charBuff, 64);
    this->m_Name.SetString(charBuff);
}

//---------------------------------------------------------------------------
// ObjectParameterViewControl... defines a parameter Type and Instance View control
// it basically turns on/off output to a ObjectParameterHistoryView 
// ie ActionControllerView/MotionTreeView
//---------------------------------------------------------------------------
ObjectParameterViewControl::ObjectParameterViewControl( QListViewItem *parent, ObjectParameter* objectParameter )
:
QCheckListItem( parent, QString(objectParameter->m_Name.GetString()), QCheckListItem::CheckBox ),
m_View(0)
{
	//
	// Ok... this is better then before but... still not perfect... 
	//
	if(objectParameter->m_Type == "MotionTree")
	{
		m_View = g_MotionTreeView;
	}
	else
	{
		m_View = g_ActionControllerView;
	}
}

void ObjectParameterViewControl::stateChange( bool isChecked)
{
	
	QCheckListItem::stateChange( isChecked );
	if(m_View)
	{
		if(isChecked)
		{
			//
			// Set up a message receiver in the ActionControllerView/Motiontree
			//
			m_View->AddFullPathCopy(this);
		}
		else
		{
			//
			// Remove up a message receiver in the ActionControllerView/Motiontree
			//
			m_View->RemoveFullPathCopy(this);
		}
	}

}

//---------------------------------------------------------------------------
// MakeTreeNodeView... 
// This is a factory for making an ActionTreeNodeViews... 
// from an actionController::Write output string that's sent remotely from
// the game
//---------------------------------------------------------------------------
static char s_nodeNamePath[1024];
TreeNodeView* MakeTreeNodeView(QListViewItem* parent, QListViewItem *after, TextParser& playinfo, int column)
{
	TreeNodeView* treeNodeView = NULL;
    playinfo.GetTokenString(s_nodeNamePath, sizeof s_nodeNamePath);
    ActionNode* playingNode = ActionNode::Find(s_nodeNamePath);

	ActionNodePlayable* playable = NULL;
    if(playingNode)
    {
        playable = dynamic_cast<ActionNodePlayable*>(playingNode);
		if(playable)
		{
			ActionNodePlayableTreeView* pANPTV = new ActionNodePlayableTreeView(playable, parent, after, 0, column);
			pANPTV->setOpen(pANPTV->LocalIsOpen());
			treeNodeView = pANPTV;
		}
		else
		{
			ActionNodeBank* bank = dynamic_cast<ActionNodeBank*>(playingNode);
			if(bank)
			{
				ActionNodeBankTreeView* pANBTV = new ActionNodeBankTreeView(bank, parent, after, 0, column);
				pANBTV->setOpen(pANBTV->LocalIsOpen());
				treeNodeView = pANBTV;
			}
		}
    }

    //
    // Here we can find the node so at 
    // least display the name of the node we are looking at.
    //
    if(!treeNodeView) //playable)
    {
        QString unresolvedNodeName(s_nodeNamePath);
		treeNodeView = new ActionNodePlayableTreeView(playable, parent, after, 0, column);
        treeNodeView->setText(column, unresolvedNodeName);
		static QPixmap redExclamation((const char **)s_red_exclamation);
        treeNodeView->setPixmap(column, redExclamation);
    }
	
	if(treeNodeView)
	{
		treeNodeView->SetNodeRefColumn(column);
	}	

	return(treeNodeView);
}
bool g_ShowNodeRefDetails = false;
static bool s_bDisable = false;
QListViewItem* BuildPlayTree(QListViewItem* parent, QListViewItem * after, TextParser& playinfo, QString* thirdName, bool showTracksMembers)
{
	if (s_bDisable)
	{
		return NULL;
	}

    static const QString spawnName("Spawn");
    static const QString opportunityName("Opportunity");
    static const QString sequenceName("Sequence");
    static const QString executeName("Execute");
	static const QString namedSpawnName("Spawn-");
	static const QString unexpandedSpawnToken("-");

    int sortOrder = 0;
    ActionNodePlayable* playable = NULL;
    TreeNodeView* actionNodePlayableView = NULL; 
    playinfo.GetNextToken();
    if(!playinfo.MatchCurrentToken("NULL"))
    {
		actionNodePlayableView = MakeTreeNodeView(parent, after, playinfo, 0);     
    }
    else
    {
        actionNodePlayableView = new ActionNodePlayableTreeView(playable, parent, after, sortOrder);
        actionNodePlayableView->setText(0, QString("Playing NULL"));
    }

    playinfo.GetNextToken();

	float timeInSec = playinfo.GetTokenAsFloat();
	int currTimeUnits = g_theApp->GetUnits()->GetTimeUnits();
	float timeInUnits = Units::GetConverted( timeInSec, Units::TU_Seconds,  (Units::TimeUnits)currTimeUnits);
	
	actionNodePlayableView->setText(1, QString::number(timeInUnits));

	bool bViewExpanded = true;

	if (thirdName)
	{
		if (thirdName->length() > namedSpawnName.length())
		{
			thirdName->remove(0, namedSpawnName.length());

			if (thirdName->startsWith(unexpandedSpawnToken))
			{
				thirdName->remove(0, 1);
				bViewExpanded = false;
			}

			if (thirdName->length() == 0)
			{
				*thirdName = spawnName;
			}
		}

		actionNodePlayableView->setText(2, *thirdName);
	}

 	if (actionNodePlayableView->GetActionNode())
	{
		if (!actionNodePlayableView->GetActionNode()->m_bUpdatedOnce)
		{
			actionNodePlayableView->GetActionNode()->m_bUpdatedOnce = true;
			actionNodePlayableView->GetActionNode()->m_bViewExpanded = bViewExpanded;
		}

		actionNodePlayableView->setOpen(actionNodePlayableView->GetActionNode()->m_bViewExpanded);
	}
	else
	{
		actionNodePlayableView->setOpen(true);
	}

    playinfo.AddvanceToToken("{");
    playinfo.GetNextToken();
    QListViewItem* lastTrack = NULL;
    while(!playinfo.EndOfText() && !playinfo.MatchCurrentToken("}"))
    {
        playinfo.GetTokenString(s_nodeNamePath, sizeof s_nodeNamePath);
        QString trackName(s_nodeNamePath);

        if(trackName.startsWith(spawnName))
        {
            lastTrack = BuildPlayTree(actionNodePlayableView, lastTrack, playinfo, &trackName, showTracksMembers);
        }
        else
        {
            if(showTracksMembers)
            {
				//
				// some addition assembly required
				//
				if(trackName.contains(opportunityName) || (sequenceName == trackName) || (executeName == trackName)) 
				{
					//playinfo.AddvanceToToken("{");
					playinfo.PushReadPosition();

					playinfo.GetNextToken();

					if (playinfo.MatchCurrentToken("{"))
					{
						playinfo.GetNextToken();

						TreeNodeView* treeView = NULL;
						
						//
						// first nodeRef is at the track level
						//
						if(!playinfo.EndOfText() && !playinfo.MatchCurrentToken("}"))
						{
							TreeNodeView* newTreeView = MakeTreeNodeView(actionNodePlayableView, lastTrack, playinfo,1);
							treeView = newTreeView;
							playinfo.GetNextToken();
							
							if(treeView)
							{
								treeView->setText(0, trackName);
							}
						}

						lastTrack = treeView;

						//
						// this is expecting a series of extra stuff right now only node refs
						// so there are no markers in the script
						//
						while(!playinfo.EndOfText() && !playinfo.MatchCurrentToken("}"))
						{
							TreeNodeView* newTreeView = MakeTreeNodeView(treeView, lastTrack, playinfo, 1);
							treeView = newTreeView;
							playinfo.GetNextToken();
						}
					}
					else
					{
						playinfo.PopReadPosition();

						// insert empty/bad opportunity
						QListViewItem* playingTrack = new QListViewItem(actionNodePlayableView, lastTrack);
						playingTrack->setText(0, trackName);
						lastTrack = playingTrack;
					}
				}
				else
				{
					QListViewItem* playingTrack = new QListViewItem(actionNodePlayableView, lastTrack);
					playingTrack->setText(0, trackName);
					lastTrack = playingTrack;
				}
            }
        }

        playinfo.GetNextToken();
    }

    return(actionNodePlayableView);
}

EntityObjectInstanceQListViewItem::EntityObjectInstanceQListViewItem(QListViewItem* parent, QString text):QListViewItem ( parent, text )
{
	m_IsFocus = false;
}

void EntityObjectInstanceQListViewItem::SetFocus(bool tf)
{
	static QPixmap redExclamation((const char **)s_red_exclamation);
	static QPixmap nothing((const char **)0);

	m_IsFocus = tf;
	if(m_IsFocus)
	{
		listView()->setSelected(this, true);
		setOpen(true);
		setPixmap( 0, redExclamation );

		QListViewItem *pLastItem = firstChild();
		while (pLastItem->nextSibling())
		{
			pLastItem = pLastItem->nextSibling();
		}

		listView()->ensureItemVisible(pLastItem);
		listView()->ensureItemVisible(this);
	}
	else
	{
		setPixmap( 0, nothing );
	}
}


QListViewItemParameterStore::QListViewItemParameterStore(QListViewItem* parent, QString text):QListViewItem ( parent, text )
{
    m_PlayHistory.reserve(s_playHistory);  
    m_sem = 0;

    m_CurrentPlay = new QListViewItem(this);
    m_CurrentPlay->setText(0, QString("GameClock"));
    m_CurrentPlay->setOpen(true);

	setOpen(true);

	m_NextHistoryItem = NULL;
}

void QListViewItemParameterStore::Playing(CBasicString* playInfo)
{
    while(m_sem != 0); // wait
    m_sem = 1;
    m_PlayHistory.push_back(playInfo);
    m_Updated = true;
    m_sem = 0;
}


void SetFocus(bool tf);

void QListViewItemParameterStore::Update(void)
{
    if(!m_Updated) return;

    while(m_sem != 0); // wait
    m_sem = 1;
    m_Updated = false;

    int numPlayUpdates = m_PlayHistory.size();
    if(numPlayUpdates > 0)
    {
        if(m_CurrentPlay->isOpen())
        {
            ClearChildren(m_CurrentPlay);
            std::vector<CBasicString*>::iterator msgIT = m_PlayHistory.end();
            msgIT--; // since the end marks an unused entry
            CBasicString* playinfoString = *msgIT;

            TextParser playinfo(playinfoString->GetString(), " ,\t\r\n:");
            playinfo.GetNextToken(); 
            playinfo.GetTokenString(s_nodeNamePath, sizeof s_nodeNamePath);
            
            //
            // Build the Tree update the gameClock
            //
            QString gameClock(s_nodeNamePath);
            m_CurrentPlay->setText(1, gameClock);

            //
            // Build the Tree... this is the key "View" abstraction
            //
            BuildPlayTree(m_CurrentPlay, NULL, playinfo, NULL, true);
        }
        
        //
        // Clear History
        //
        if(numPlayUpdates>s_playHistory)
        {
            std::vector<CBasicString*>::iterator msgIT = m_PlayHistory.begin();
			int playHistorySize = m_PlayHistory.size();
            while(playHistorySize>s_playHistory)
            {
                msgIT = m_PlayHistory.begin();
                CBasicString* playInfo = *msgIT;
                m_PlayHistory.erase(msgIT);
                delete playInfo;
				playHistorySize = m_PlayHistory.size();
            }
        }
    }

    m_sem = 0;
}

extern void buildDebutMT(QListViewItem* mtRoot, TextParser& message);

void QListViewItemParameterStore::UpdateMT(void)
{
    if(!m_Updated) return;

    while(m_sem != 0); // wait
    m_sem = 1;
    m_Updated = false;

    int numPlayUpdates = m_PlayHistory.size();
    if(numPlayUpdates > 0)
    {
        if(m_CurrentPlay->isOpen())
        {
            ClearChildren(m_CurrentPlay);
            std::vector<CBasicString*>::iterator msgIT = m_PlayHistory.end();
            msgIT--; // since the end marks an unused entry
            CBasicString* playinfoString = *msgIT;

            TextParser playinfo(playinfoString->GetString(), " ,\t\r\n:");
            playinfo.GetNextToken(); 
            playinfo.GetTokenString(s_nodeNamePath, sizeof s_nodeNamePath);
            
            //
            // Build the Tree update the gameClock
            //
            QString gameClock(s_nodeNamePath);
            m_CurrentPlay->setText(1, gameClock);

            //
            // Build the Tree... this is the key "View" abstraction
            //
            buildDebutMT(m_CurrentPlay, playinfo);	
        }
        
        //
        // Clear History
        //
        if(numPlayUpdates>s_playHistory)
        {
            std::vector<CBasicString*>::iterator msgIT = m_PlayHistory.begin();
			int playHistorySize = m_PlayHistory.size();
            while(playHistorySize>s_playHistory)
            {
                msgIT = m_PlayHistory.begin();
                CBasicString* playInfo = *msgIT;
                m_PlayHistory.erase(msgIT);
                delete playInfo;
				playHistorySize = m_PlayHistory.size();            
            }
        }
    }

    m_sem = 0;
}


//---------------------------------------------------------------------------
// View for ActionController Owners... Peds/Props/ETC
//---------------------------------------------------------------------------
ActionControllerOwnerView::ActionControllerOwnerView(QWidget* parent, const char* name, WFlags fl)
:
QWidget(parent, name, fl)
{
    FormLayout = new QGridLayout( this, 1, 1, 11, 6, "Form1Layout"); 

	//
	// ActionTree view control
	//
    layoutAT = new QHBoxLayout( 0, 0, 6, "layoutAT"); 

    m_ActionControllerHistoryLevelLabel = new QLabel( this, "ActionControllerHistoryLevelLabel" );
    m_ActionControllerHistoryLevelLabel->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)2, (QSizePolicy::SizeType)5, 0, 0, m_ActionControllerHistoryLevelLabel->sizePolicy().hasHeightForWidth() ) );
	m_ActionControllerHistoryLevelLabel->setText( tr( "ActionController History Level" ) );

	layoutAT->addWidget( m_ActionControllerHistoryLevelLabel );

    m_ActionControllerHistoryLevel = new QSpinBox( this, "ActionControllerHistoryLevel" );
    m_ActionControllerHistoryLevel->setPrefix( QString::null );
    m_ActionControllerHistoryLevel->setSpecialValueText( QString::null );
	m_ActionControllerHistoryLevel->setMinValue(1);
	m_ActionControllerHistoryLevel->setMaxValue(s_playHistory);


	layoutAT->addWidget( m_ActionControllerHistoryLevel );

    FormLayout->addLayout( layoutAT, 0, 1 );

    layoutAT = new QHBoxLayout( 0, 0, 6, "layoutAT"); 
		
	//
	// ActiontreeTree ViewSize control
	//
    layoutViewAT = new QHBoxLayout( 0, 0, 6, "layoutViewAT"); 

	m_ActionControllerViewSizeLabel = new QLabel( this, "ActionControllerViewSizeLabel" );
    m_ActionControllerViewSizeLabel->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)2, (QSizePolicy::SizeType)5, 0, 0, m_ActionControllerViewSizeLabel->sizePolicy().hasHeightForWidth() ) );
	m_ActionControllerViewSizeLabel->setText( tr( "ActionController ViewSize" ) );

	layoutViewAT->addWidget( m_ActionControllerViewSizeLabel );

    m_ActionControllerViewSize = new QSpinBox( this, "ActionControllerViewSize" );
    m_ActionControllerViewSize->setPrefix( QString::null );
    m_ActionControllerViewSize->setSpecialValueText( QString::null );
	m_ActionControllerViewSize->setMinValue(10);
	m_ActionControllerViewSize->setMaxValue(2000);

	layoutViewAT->addWidget( m_ActionControllerViewSize );

    FormLayout->addLayout( layoutViewAT, 1, 1 );

	//
	// MotionTree view control
	//
    layoutMT = new QHBoxLayout( 0, 0, 6, "layoutMT"); 

    m_MotionTreeHistoryLevelLabel = new QLabel( this, "MotionTreeHistoryLevelLabel" );
    m_MotionTreeHistoryLevelLabel->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)2, (QSizePolicy::SizeType)5, 30, 0, m_MotionTreeHistoryLevelLabel->sizePolicy().hasHeightForWidth() ) );
    m_MotionTreeHistoryLevelLabel->setText( tr( "MotionTree History Level       " ) );

    layoutMT->addWidget( m_MotionTreeHistoryLevelLabel );

    m_MotionTreeHistoryLevel = new QSpinBox( this, "MotionTreeHistoryLevel" );
    m_MotionTreeHistoryLevel->setPrefix( QString::null );
    m_MotionTreeHistoryLevel->setSpecialValueText( QString::null );
	m_MotionTreeHistoryLevel->setMinValue(1);
	m_MotionTreeHistoryLevel->setMaxValue(s_playHistory);

	layoutMT->addWidget( m_MotionTreeHistoryLevel );

    FormLayout->addLayout( layoutMT, 0, 0 );

	//
	// MotionTree ViewSize control
	//
    layoutViewMT = new QHBoxLayout( 0, 0, 6, "layoutViewMT"); 

	m_MotionTreeViewSizeLabel = new QLabel( this, "MotionTreeViewSizeLabel" );
    m_MotionTreeViewSizeLabel->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)2, (QSizePolicy::SizeType)5, 0, 0, m_MotionTreeViewSizeLabel->sizePolicy().hasHeightForWidth() ) );
	m_MotionTreeViewSizeLabel->setText( tr( "MotionTree ViewSize" ) );

	layoutViewMT->addWidget( m_MotionTreeViewSizeLabel );

    m_MotionTreeViewSize = new QSpinBox( this, "MotionTreeViewSize" );
    m_MotionTreeViewSize->setPrefix( QString::null );
    m_MotionTreeViewSize->setSpecialValueText( QString::null );
	m_MotionTreeViewSize->setMinValue(10);
	m_MotionTreeViewSize->setMaxValue(2000);

	layoutViewMT->addWidget( m_MotionTreeViewSize );

    FormLayout->addLayout( layoutViewMT, 1, 0 );

	//
	// This part is the actual viewtree for the Game Objects
	//
	m_ItemListView = new QListView(this, name);

    m_ItemListView->addColumn( tr( "Type" ) );
    m_ItemListView->addColumn( tr( "Model" ) );
    m_ItemListView->addColumn( tr( "Pointer" ) );
    //header()->setClickEnabled( FALSE, header()->count() - 1 );
    m_ItemListView->setAcceptDrops( TRUE );
    m_ItemListView->setRootIsDecorated( FALSE );
    m_ItemListView->setDefaultRenameAction( QListView::Accept );
	m_ItemListView->setSorting(-1);
	m_ItemListView->show();

    FormLayout->addMultiCellWidget( m_ItemListView, 2, 2, 0, 1 );

    //
    // connect events
    //
    connect( m_ItemListView, SIGNAL( rightButtonPressed(QListViewItem*,const QPoint&,int) ), 
             this, SLOT( rightButtonPressedHandler(QListViewItem*,const QPoint&,int) ) );
    connect( m_ItemListView, SIGNAL( clicked(QListViewItem*,const QPoint&,int) ), 
			this, SLOT( clickedHandler(QListViewItem*,const QPoint&,int) ) );

	//
	// History of views
	//
	connect( m_ActionControllerHistoryLevel, SIGNAL( valueChanged(int) ),	this, SLOT( SetActionControllerHistoryLevel(int) ) );
	connect( m_MotionTreeHistoryLevel, SIGNAL( valueChanged(int) ),			this, SLOT( SetMotionTreeHistoryLevel(int) ) );

	connect( m_ActionControllerViewSize, SIGNAL( valueChanged(int) ),	this, SLOT( SetActionControllerViewSize(int) ) );
	connect( m_MotionTreeViewSize, SIGNAL( valueChanged(int) ),			this, SLOT( SetMotionTreeViewSize(int) ) );

    //
    // All popup menus
    //
    m_ObjectClassPopupMenu = new QPopupMenu(this, "ObjectClassPopup");
    m_ObjectClassPopupMenu->insertItem( "Refresh From Game", this, SLOT(refreshFromSelected()));
    m_ObjectClassPopupMenu->insertItem( "Play", this, SLOT(playOnSelected()));
	m_ObjectClassPopupMenu->insertItem( "Set Focus Actor", this, SLOT(setFocusActor()));

	//
	// Init message buffer
	//
    m_sem = 0;
    m_MessageBuff.reserve(20);
    m_Updated = false;
}

ActionControllerOwnerView::~ActionControllerOwnerView()
{

}

void ActionControllerOwnerView::rightButtonPressedHandler 
( 
    QListViewItem *item, 
    const QPoint& coords, 
    int column
)
{
   if(item)
   {
       m_ObjectClassPopupMenu->move( coords );
       m_ObjectClassPopupMenu->show();
   }
   else
   {
       g_theApp->PostMessageToRemoteHost("[act]ENTITY:TYPE"); 
   }
}

//---------------------------------------------------------------------------
// On mouse click for the Owner View
//---------------------------------------------------------------------------
void ActionControllerOwnerView::clickedHandler 
( 
 QListViewItem *item, 
 const QPoint& coords, 
 int column
 )
{
/*
    if(item)
    {
        QCheckListItem* objectParameter = dynamic_cast<QCheckListItem*>(item);
        if(objectParameter)
        {
            int isChecked = objectParameter->isOn()?1:0;

            QListViewItem* ObjectInstance = objectParameter->parent();
            if(ObjectInstance)
            {
                QListViewItem* ObjectClass = ObjectInstance->parent();
                if(ObjectClass)
                {
                    //
					// Tell the game to send information about 
					// Some ActionController
					//
					char messageBuff[1024];
                    sprintf(messageBuff, "[act]ENTITY:PED:WATCH:%s:%s:%d", 
                            ObjectInstance->text(COL_POINTER).ascii(),
                            objectParameter->text(COL_TYPE).ascii(),
                            isChecked);

                    g_theApp->PostMessageToRemoteHost(messageBuff); 

					//
					// Now make members
					// Find what type of Object this is...
					//
					EntityObjectType* objectTypedef = 0;
					int size = m_EntityObjectType.size();
					for(int i=0;i<size;i++)
					{
						EntityObjectType* currobjectTypedef = m_EntityObjectType[i];
						if(currobjectTypedef->m_Name == ObjectClass->text(COL_TYPE).ascii())
						{
							objectTypedef = currobjectTypedef;
							break;
						}
					}
					
					//
					// What type do we send the message to?
					//
					CBasicString* m_Type = NULL;
					if(objectTypedef)
					{
						int size = objectTypedef->m_Parameters.size();
						for(int i=0;i<size;i++)
						{
							if(objectTypedef->m_Parameters[i]->m_Name == objectParameter->text(COL_TYPE).ascii())
							{
								m_Type = &(objectTypedef->m_Parameters[i]->m_Type);
							}
						}
					}

					if(m_Type && *m_Type == "MotionTree")
					{
						//
						// Hack... here we should have a better way to connect the
						// Owner View and the Actually ActionController view
						//
						if(isChecked)
						{
							//
							// Set up a message receiver in the ActionControllerView
							//
							g_MotionTreeView->AddFullPathCopy(objectParameter);
						}
						else
						{
							//
							// Remove up a message receiver in the ActionControllerView
							//
							g_MotionTreeView->RemoveFullPathCopy(objectParameter);
						}
					}
					else
					{
						//
						// Hack... here we should have a better way to connect the
						// Owner View and the Actually ActionController view
						//
						if(isChecked)
						{
							//
							// Set up a message receiver in the ActionControllerView
							//
							g_ActionControllerView->AddFullPathCopy(objectParameter);
						}
						else
						{
							//
							// Remove up a message receiver in the ActionControllerView
							//
							g_ActionControllerView->RemoveFullPathCopy(objectParameter);
						}
					}
				}
			}
        }
    }
*/
}

//---------------------------------------------------------------------------
// Delete all owners and ask the game to send over a refreshed list
//---------------------------------------------------------------------------
void ActionControllerOwnerView::refreshFromSelected(void)
{
    m_ObjectClassPopupMenu->hide();
    QListViewItem* firstItem = m_ItemListView->firstChild();
    if(firstItem)
    {
        char messageBuff[1024];
        sprintf(messageBuff, "[act]ENTITY:PED:REFRESH:%s", firstItem->text(COL_TYPE).ascii());            
        g_theApp->PostMessageToRemoteHost(messageBuff);
        
        QListViewItem* firstChild = firstItem->firstChild();
        while(firstChild)
        {
            delete firstChild;
            firstChild = firstItem->firstChild();
        }
    }
}

//---------------------------------------------------------------------------
// Send a Play Call Directly to a specific ActionController
//---------------------------------------------------------------------------
void ActionControllerOwnerView::playOnSelected(void)
{
    m_ObjectClassPopupMenu->hide();
    QListViewItem* selected = m_ItemListView->selectedItem();
    if(selected)
    {
        QListViewItem* ObjectInstance = selected->parent();
        if(ObjectInstance)
        {
            QListViewItem* ObjectClass = ObjectInstance->parent();
            if(ObjectClass)
            {
                char nodeFullPath[1024];
                if(g_ActionTree->m_SelectedItem)
                {
                    ActionNode* actionNode = g_ActionTree->m_SelectedItem->GetActionNode();
                    actionNode->GetNameFullPath(nodeFullPath, 1024);
                }
                else
                {
                    strcpy(nodeFullPath, "NULL");
                }

                char messageBuff[1024];
                sprintf(messageBuff,"[act]Play:%s:%s:%s:%s",
                    nodeFullPath,
                    ObjectClass->text(COL_TYPE).ascii(),
                    ObjectInstance->text(COL_POINTER).ascii(),
                    selected->text(COL_TYPE).ascii());

                g_theApp->PostMessageToRemoteHost(messageBuff);
            }
        }
    }
}

void ActionControllerOwnerView::setFocusActor()
{
    m_ObjectClassPopupMenu->hide();
    QListViewItem *pSelectedItem = m_ItemListView->selectedItem();
    if (pSelectedItem)
    {
		pSelectedItem->setOpen(true);
        char messageBuff[1024];
        sprintf(messageBuff, "[act]ENTITY:PED:SETFOCUS:%s", pSelectedItem->text(COL_POINTER).ascii());
        g_theApp->PostMessageToRemoteHost(messageBuff);

    }
}

void ActionControllerOwnerView::ClearFocusActors()
{
	// clear focus actors
	
    QListViewItem *pChild = m_ItemListView->firstChild()->firstChild();
    while (pChild)
    {
		EntityObjectInstanceQListViewItem *pObjectInstance = dynamic_cast<EntityObjectInstanceQListViewItem *>(pChild);
		if (pObjectInstance)
		{
			pObjectInstance->SetFocus(false);
		}
        pChild = pChild->nextSibling();
    }
}

void ActionControllerView::playOnSelected(void)
{
/*
    QListViewItem* selected = selectedItem();
    if(selected)
    {

        char nodeFullPath[1024];
        if(g_ActionTree->m_SelectedItem)
        {
            ActionNode* actionNode = g_ActionTree->m_SelectedItem->GetActionNode();
            actionNode->GetNameFullPath(nodeFullPath, 1024);
        }
        else
        {
            strcpy(nodeFullPath, "NULL");
        }
*/

/*
        char messageBuff[1024];
        snprintf(messageBuff, 1024, "[act]PLAY:%s:%s:%s:%s",
            nodeFullPath,
            ObjectClass->text(0).ascii(),
            ObjectInstance->text(1).ascii(),
            objectParameter->text(0).ascii());

        g_theApp->PostMessageToRemoteHost(messageBuff);
*/
//    }

}

void ActionControllerOwnerView::Update(CSubject* /*theChangedSubject*/)
{
    /*
    ActionTreeLog* actionTreeLog = dynamic_cast<ActionTreeLog*>(theChangedSubject);

    if(actionTreeLog)
    {   
    QString message(actionTreeLog->GetMessage());
    append(message);
    repaintContents();
    }
    */
}

void ActionControllerOwnerView::RecieveMessage(CBasicString* message)
{
    while(m_sem != 0); // wait
    m_sem = 1;
    m_MessageBuff.push_back(message);
    m_Updated = true;
    m_sem = 0;
}

void ActionControllerOwnerView::Update(void)
{
    if(!m_Updated) return;

    while(m_sem != 0); // wait
    m_sem = 1;
    m_Updated = false;

    //
    // Handle all updates
    //
    std::vector<CBasicString*>::iterator msgIT;
    while(m_MessageBuff.size()>0)
    {
        msgIT = m_MessageBuff.begin();
        CBasicString* msg = *msgIT;
        
        handleMessage(msg);

        m_MessageBuff.erase(msgIT);
        delete msg;
    }

    m_sem = 0;
}

bool ActionControllerOwnerView::HandleMessage(CBasicString* msg)
{
	if (*msg == "DISCONNECT")
	{
		Clear();
		g_ActionControllerView->ClearAllViews();
		g_MotionTreeView->ClearAllViews();
		delete msg;
		return true;
	}

    TextParser textParser(msg->GetString(), " ,\t\r\n:");
    if(!textParser.EndOfText())
    {
        if(textParser.MatchCurrentToken("ENTITY"))
        {
            RecieveMessage(msg);            
            return(true);
        }
    }

    return(false);
}

void ActionControllerOwnerView::handleMessage(CBasicString* msg)
{
    TextParser message(msg->GetString(), " ,\t\r\n:");
    message.GetNextToken();

    if(message.MatchCurrentToken("TYPE"))
    {
        message.GetNextToken();
        createGameObjectType(message);
		refreshFromSelected();
        return;
    }

    char charBuff[16];
    message.GetTokenString(charBuff, 16);
    QString objectClassName(charBuff);
    QListViewItem* objectClass = findChildView(m_ItemListView, objectClassName, COL_TYPE);
    
    if(objectClass == NULL)
    {
        return;

        objectClass = new QListViewItem(m_ItemListView);
        objectClass->setText(COL_TYPE, objectClassName);
    }
    
	objectClass->setOpen(true);

    message.GetNextToken();
    if(message.MatchCurrentToken("UPDATE"))
    {
        message.GetNextToken();
        OnUpdate(objectClass, message);        
    }
    if(message.MatchCurrentToken("CREATED"))
    {
        message.GetNextToken();
        createGameObject(objectClass, message);
    }
    else
    if(message.MatchCurrentToken("DELETED"))
    {
        message.GetNextToken();
        deleteGameObject(objectClass, message);
    }
	if(message.MatchCurrentToken("SETFOCUS"))
	{
		message.GetNextToken();
		message.GetTokenString(charBuff, 16);
		QString objectInstanceGuid(charBuff);
	    EntityObjectInstanceQListViewItem* objectInstance = dynamic_cast<EntityObjectInstanceQListViewItem*>(findChild(objectClass, objectInstanceGuid, COL_POINTER));
		if(objectInstance)
		{
			ClearFocusActors();
			objectInstance->SetFocus(true);
		}
	}
}

//
// OnUpdate should just notify "update" listeners... 
// be setup as a message reciever like the HandleMessage 
// used to above and the below fucktion should be reworked
//
void ActionControllerOwnerView::OnUpdate
(
    QListViewItem* objectClassName, 
    TextParser& message
)
{
	// FIX THIS!
	// Hack... these should just be listeners...
	message.PushReadPosition();
	g_ActionControllerView->OnUpdate( objectClassName, message);
	
	// otherwise g_ActionControllerView->OnUpdate would have changed the message
	message.PopReadPosition();	
	g_MotionTreeView->OnUpdate( objectClassName, message);
}

void ActionControllerOwnerView::createGameObjectType(TextParser& message)
{
    char charBuff[16];
    message.GetTokenString(charBuff, 16);
    
    int numClasses = m_EntityObjectType.size();
    for(int i=0;i<numClasses;i++)
    {
        EntityObjectType* classType = m_EntityObjectType[i];
        if(classType->m_Name == charBuff)
        {
            //
            // Found
            //
            return;
        }
    }
    
    //
    // Create a new object
    //
    EntityObjectType* classType = new EntityObjectType(message);
    m_EntityObjectType.push_back(classType);
    QListViewItem* objectClass = new QListViewItem(m_ItemListView);
    objectClass->setText(COL_TYPE, QString(classType->m_Name.GetString()));
}

void ActionControllerOwnerView::getObjectInfo
(
 TextParser& message, 
 QString& objectType, 
 QString& pointerHandle,
 QString& modelName 
 )
{
    char charBuff[64];
    message.GetTokenString(charBuff, 64);
    objectType.setAscii(charBuff);
    message.GetNextToken();

    message.GetTokenString(charBuff, 64);
    pointerHandle.setAscii(charBuff);
    message.GetNextToken();

    message.GetTokenString(charBuff, 64);
    modelName.setAscii(charBuff);       
}
void ActionControllerOwnerView::createGameObject
(
    QListViewItem* objectClassName, 
    TextParser& message
)
{
    QString objectType;
    QString pointerHandle;
    QString modelName;       

    getObjectInfo(message, objectType, pointerHandle, modelName);    
    createGameObject(objectClassName, objectType, pointerHandle, modelName);
}

void ActionControllerOwnerView::createGameObject
(
 QListViewItem* objectClassName, 
 QString& objectType, 
 QString& pointerHandle,
 QString& modelName 
 )
{
    QListViewItem* object = findChild(objectClassName, pointerHandle, COL_POINTER);
    if(!object)
    {
        QListViewItem* newObject = new EntityObjectInstanceQListViewItem(objectClassName, objectType);
        newObject->setText(COL_POINTER, pointerHandle);
        newObject->setText(COL_MODEL, modelName);
        
        //
        // Now make members
		// Find what type of Object this is...
        //
        EntityObjectType* objectTypedef = 0;
        int size = m_EntityObjectType.size();
        for(int i=0;i<size;i++)
        {
            EntityObjectType* currobjectTypedef = m_EntityObjectType[i];
            if(currobjectTypedef->m_Name == objectClassName->text(COL_TYPE).ascii())
            {
                objectTypedef = currobjectTypedef;
                break;
            }
        }

        if(objectTypedef)
        {
            int size = objectTypedef->m_Parameters.size();
            for(int i=0;i<size;i++)
            {
                ObjectParameter* objectParameter = objectTypedef->m_Parameters[i];
                ObjectParameterViewControl* newObjectParam = new ObjectParameterViewControl( newObject, objectParameter );

				ObjectParameterHistoryView *pView;
				if (objectParameter->m_Type == "MotionTree")
				{
					pView = g_MotionTreeView;
				}
				else
				{
					pView = g_ActionControllerView;
				}

				if (pView->DoesItemExist(pointerHandle, QString(objectParameter->m_Name.GetString())))
				{
					newObject->setOpen(true);
					newObjectParam->setOn(true);
				}
            }
        }
    }
}

void ActionControllerOwnerView::deleteGameObject
(
    QListViewItem* objectClassName, 
    TextParser& message
)
{
    QString objectType;
    QString pointerHandle;
    QString modelName;       

    getObjectInfo(message, objectType, pointerHandle, modelName); 
    deleteGameObject(objectClassName, objectType, pointerHandle, modelName);
}

void ActionControllerOwnerView::deleteGameObject
(
 QListViewItem* objectClassName, 
 QString& objectType, 
 QString& pointerHandle,
 QString& modelName 
 )
{
    QListViewItem* object = findChild(objectClassName, pointerHandle, COL_POINTER);
    if(object)
    {
        delete object;
    }
}

void ActionControllerOwnerView::UpdateDetach(CSubject* theChangedSubject)
{
    theChangedSubject->Dettach(this);
}

extern void SyncWithViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);
extern void SaveViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);

void ActionControllerOwnerView::SyncWithViewProperies(void)
{
    SyncWithViewProperiesUtil(this, m_ViewProperties);
}

void ActionControllerOwnerView::SaveViewProperies(void)
{
    SaveViewProperiesUtil(this, m_ViewProperties);
}


void ActionControllerOwnerView::SetActionControllerHistoryLevel(int level)
{
	g_ActionControllerView->SetHistoryLevel(level);
}

int  ActionControllerOwnerView::GetActionControllerHistoryLevel(void)
{
	return(g_ActionControllerView->GetHistoryLevel());
}

void ActionControllerOwnerView::SetMotionTreeHistoryLevel(int level)
{
	g_MotionTreeView->SetHistoryLevel(level);
}

int  ActionControllerOwnerView::GetMotionTreeHistoryLevel(void)
{
	return(g_MotionTreeView->GetHistoryLevel());
}

void ActionControllerOwnerView::SetActionControllerViewSize(int width)
{
	g_ActionControllerView->setminwidth(width);
}

void ActionControllerOwnerView::SetMotionTreeViewSize(int width)
{
	g_MotionTreeView->setminwidth(width);
}

void ActionControllerOwnerView::Clear()
{
	m_ItemListView->clear();
	m_EntityObjectType.clear();
}

//---------------------------------------------------------------------------
// Base class for ActionControllerviews MotionTreeViews
//---------------------------------------------------------------------------
ObjectParameterHistoryView::ObjectParameterHistoryView(QWidget* parent, const char* name, WFlags fl )
:
ScrollViewHorizontal(parent, name, fl),
m_MinWidth(50)
{

}

void ObjectParameterHistoryView::setminwidth(int w)
{
	m_MinWidth = w;
}

//---------------------------------------------------------------------------
// View for RunningActionControllers
//---------------------------------------------------------------------------
ActionControllerView::ActionControllerView(QWidget* parent, const char* name, WFlags fl)
:
ObjectParameterHistoryView(parent, name, fl)
{
	m_PlayHistoryView.reserve(10);
	SetHistoryLevel(1);

    m_sem = 0;
    m_PlayHistory.reserve(s_playHistory);
    m_Updated = false;

}

ActionControllerView::~ActionControllerView()
{

}

void ActionControllerView::OnUpdate
(
    QListViewItem* objectClassName, 
    TextParser& message
)
{
	char charBuff[64];
    message.GetTokenString(charBuff, 64);
    QString objectHandle(charBuff);

    QListViewItem* objectInstance = m_PlayHistoryView[0]->findItem( objectHandle, ActionControllerOwnerView::COL_POINTER);
    
	if(objectInstance == NULL)
	{
		// Create One 
		
	}
	
	if(objectInstance)
    {
        message.GetNextToken();
        message.GetTokenString(charBuff, 64);
        QString objectParameterName(charBuff);
		QListViewItem* objectParameter = findChild(objectInstance, objectParameterName, ActionControllerOwnerView::COL_TYPE);
        QListViewItemParameterStore* itemParameterStore = dynamic_cast<QListViewItemParameterStore*>(objectParameter);
        if(itemParameterStore)
        {
            const char* messageRaw = message.GetCurrentReadPosition();
            CBasicString* playRecord = new CBasicString(messageRaw);
            itemParameterStore->Playing(playRecord);
			m_Updated = true;
        }
    }
}

//
// note: ActionControllerView and MotionTreeView are basically the same...
// this funktion only differs by calling:Update(); instead of UpdateMT();
// Consider Uniting the Fucking Godamn thing ;)
//
void ActionControllerView::Update(void)
{
	if(!m_Updated)return;

	const int historyLevel = m_PlayHistoryView.size();
	// for all views
	QListViewItemIterator it( m_PlayHistoryView[0] );
	while ( it.current() ) 
	{
		QListViewItem *item = it.current();
		++it;

		//
		// Dynamic_cast to find a current runnining parameterStore
		//
		QListViewItemParameterStore* itemParameterStoreCurrent = dynamic_cast<QListViewItemParameterStore*>(item);
		if ( itemParameterStoreCurrent )
		{
			itemParameterStoreCurrent->Update();

			//
			// Now find subquent QListViewItemParameterStore* items in the history view...
			//				
			int history = 1;
			QListViewItemParameterStore* nextHistory = itemParameterStoreCurrent->m_NextHistoryItem;

			//
			// Yes I'm seeting the string pointer in the 
			//
			CBasicString* playInfo = itemParameterStoreCurrent->GetHistory(history);
			while(nextHistory && playInfo)
			{
				CBasicString* playRecord = new CBasicString(playInfo->GetString());
				nextHistory->Playing(playRecord);
				nextHistory->Update();
				nextHistory->clearHistory(); // we're not 'storing' updateInfos in this view

				//
				// Next
				//
				history++;
				playInfo = itemParameterStoreCurrent->GetHistory(history);
				nextHistory = nextHistory->m_NextHistoryItem;
			}
		}
	}

	m_Updated = false;
}

//
// This adds and inserts a copy of the tree branch that leads to the objectParameter
// This practically is the actionController instance for a particular charcter 
// this is used to represent another view of the Running actionController
//
void ActionControllerView::AddFullPathCopy(QListViewItem* objectParameter)
{

	const int historyLevel = m_PlayHistoryView.size();
	
	// objectParameter is coming from ActionControllerOwnerView, so the text
	//	columns are relative to that view (so should use the COL_* enum to
	//	find the text.)

	//
	// This is just to connect the ViewObjects together for faster updating of history
	//
	QListViewItemParameterStore* lastParamStore = NULL;

	// for all views
	for(int i=0;i<historyLevel;i++)
	{
		ActionNodeListView* this_ = m_PlayHistoryView[i];

		std::vector<QListViewItem*> parentStack;
		parentStack.reserve(6);
	    
		QListViewItem* currentItem = objectParameter;
		do
		{
			parentStack.push_back(currentItem);
			currentItem = currentItem->parent();

		} while(currentItem);

		std::vector<QListViewItem*>::iterator copyItemIT;
	    
		QListViewItem* currentParent = NULL;

		if(parentStack.size())
		{
			copyItemIT = parentStack.end();
			copyItemIT--;
			QListViewItem* currentCopyItem = *copyItemIT;
			currentParent = findChildView(this_, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE), ActionControllerOwnerView::COL_TYPE);
			if(currentParent == 0)
			{
				//
				// Seed the root (like: PED clase etc.)
				//
				currentParent = new QListViewItem(this_, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE));
				currentParent->setOpen(true);
			}
	        
			parentStack.pop_back();
		}

		if(parentStack.size())
		{
			copyItemIT = parentStack.end();
			copyItemIT--;
			QListViewItem* currentCopyItem = *copyItemIT;
			QListViewItem* itemFound = findChild(currentParent, currentCopyItem->text(ActionControllerOwnerView::COL_POINTER), ActionControllerOwnerView::COL_POINTER);
			if(itemFound == 0)
			{
				//
				// Make an Entity Object
				//
				currentParent = new EntityObjectInstanceQListViewItem(currentParent, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE));
				currentParent->setText(ActionControllerOwnerView::COL_POINTER, currentCopyItem->text(ActionControllerOwnerView::COL_POINTER));
				currentParent->setText(ActionControllerOwnerView::COL_MODEL, currentCopyItem->text(ActionControllerOwnerView::COL_MODEL));
				currentParent->setOpen(true);
			}
			else
			{
				currentParent = itemFound;
			}
	        
			parentStack.pop_back();
		}

		if(parentStack.size())
		{
			copyItemIT = parentStack.end();
			copyItemIT--;
			QListViewItem* currentCopyItem = *copyItemIT;
			QListViewItem* itemFound = findChild(currentParent, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE), ActionControllerOwnerView::COL_TYPE);
			if(itemFound == 0)
			{
				//
				// this makes the Object that receives acitoncontroller updates
				//
				QListViewItemParameterStore* paramStore = new QListViewItemParameterStore(currentParent, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE));				

				if(lastParamStore)
				{
					lastParamStore->m_NextHistoryItem = paramStore;
				}

				lastParamStore = paramStore;
			}
	        
			parentStack.pop_back();
		}
	}
}

void ActionControllerView::RemoveFullPathCopy(QListViewItem* objectParameter)
{
    //
    // This will delete the entire branch if it's a vine otherwise just remove the
	// objectParameter that matches the input objectParameter
    //
	// for all views
	int viewCount = m_PlayHistoryView.size();
	for(int i=0;i<viewCount;i++)
	{
		ActionNodeListView* this_ = m_PlayHistoryView[i];

		QListViewItem* currentItem =  this_->findItem( objectParameter->text(ActionControllerOwnerView::COL_TYPE), ActionControllerOwnerView::COL_TYPE);
		while(currentItem)
		{
			QListViewItem* parent = currentItem->parent();
			delete currentItem;
			currentItem = NULL;

			if(parent && parent->childCount() == 0)
			{
				currentItem = parent;
			}
		}
	}
}

extern void SyncWithViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);
extern void SaveViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);

void ActionControllerView::SyncWithViewProperies(void)
{
    SyncWithViewProperiesUtil(this, m_ViewProperties);
}

void ActionControllerView::SaveViewProperies(void)
{
    SaveViewProperiesUtil(this, m_ViewProperties);
}

static void disconnectHistory(QListViewItem* parent)
{
	//
	// Disconnect the history pointers ;)
	//
	QListViewItem* myChild = parent->firstChild();
	while( myChild ) 
	{
		//
		// disconnect the history
		//
		QListViewItemParameterStore* paramStore = dynamic_cast<QListViewItemParameterStore*>(myChild);
		if(paramStore)
		{
			paramStore->m_NextHistoryItem = NULL;
		}
		
		//
		// Recurse children
		//
		disconnectHistory(myChild);

		//
		// next sibling
		//
		myChild = myChild->nextSibling();
	}
}

void ActionControllerView::SetHistoryLevel(int count)
{

	//
	// This clears ALL 
	//
	//m_PlayHistoryView.erase(m_PlayHistoryView.begin(), m_PlayHistoryView.end());
	//clear();

	int currentCount = m_PlayHistoryView.size();
	if(currentCount>count)
	{
		for(int i=count;i<currentCount;i++)
		{
			remomeChild(m_PlayHistoryView[i]);
			delete m_PlayHistoryView[i];
		}
		m_PlayHistoryView.erase(m_PlayHistoryView.begin()+count, m_PlayHistoryView.end());

		QListViewItem* root = m_PlayHistoryView[count-1]->firstChild();
		if(root)
		{
			disconnectHistory(root);
		}
	}

	//
	// Add New Shit
	//
	currentCount = m_PlayHistoryView.size();
	for(int i=currentCount;i<count;i++)
	{
		ActionNodeListView* m_PlayView = new ActionNodeListView(viewport(),"slave");

		m_PlayHistoryView.push_back(m_PlayView);

		m_PlayView->addColumn( tr( "Playing" ) );
		m_PlayView->addColumn( tr( "ExtraInfo" ) );
		m_PlayView->addColumn( tr( "SpawnPlay" ) );

		m_PlayView->setAcceptDrops( TRUE );
		m_PlayView->setRootIsDecorated( FALSE );
		m_PlayView->setDefaultRenameAction( QListView::Accept );

		m_PlayView->setColumnWidthMode( 0, QListView::Manual );
		m_PlayView->setColumnAlignment( 0, Qt::AlignJustify );
		static int s_defaultWidth = 250;
		m_PlayView->setColumnWidth( 0, s_defaultWidth );
		m_PlayView->setSorting(-1);

		m_PlayView->setMinimumSize( QSize( m_MinWidth, 0 ) );
		m_PlayView->setMaximumSize( QSize( 32767, 32767 ) );
		
		m_PlayView->show();
		addChild(m_PlayView);
	}
	
	m_scrollView->viewport()->adjustSize();
    m_FrameLayout->activate(); // this is KEY!!!! after the internals are drawn!!!!

}

int  ActionControllerView::GetHistoryLevel(void)
{
	return(m_PlayHistoryView.size());
}

void ActionControllerView::setminwidth(int w)
{
	ObjectParameterHistoryView::setminwidth(w);
	
	int count = m_PlayHistoryView.size();
	
	for(int i=0;i<count;i++)
	{
		m_PlayHistoryView[i]->setMinimumSize( QSize( m_MinWidth, 0 ) );
	}

	m_scrollView->viewport()->adjustSize();
	m_FrameLayout->activate(); // this is KEY!!!! after the internals are drawn!!!!
}

void ActionControllerView::ClearAllViews()
{
	int iNumPlayHistories = m_PlayHistoryView.size();
	
	for (int i = 0; i < iNumPlayHistories; i++)
	{
		m_PlayHistoryView[i]->clear();

	} // for i

} // ActionControllerView::ClearAllViews

bool ActionControllerView::DoesItemExist(QString& guidNumber, QString& controllerName)
{
	QListViewItem *pObjectInstance = m_PlayHistoryView[0]->findItem(guidNumber, ActionControllerOwnerView::COL_POINTER);
	if (pObjectInstance)
	{
		QListViewItem *pObjectParameter = findChild(pObjectInstance, controllerName, ActionControllerOwnerView::COL_TYPE);
		QListViewItemParameterStore *pObjectParameterStore = dynamic_cast<QListViewItemParameterStore*>(pObjectParameter);
		if (pObjectParameterStore)
		{
			return true;
		}
	}

	return false;

} // ActionControllerView::DoesItemExist
