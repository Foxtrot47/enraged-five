#include <qapplication.h>
#include <qmime.h>
#include <qpixmap.h>

#include <direct.h>
#include <stdlib.h>
#include <stdio.h>

//
// Styles
//
#include <qcdestyle.h> 
#include <qplatinumstyle.h> 
#include <qsgistyle.h> 

#include "actiontree.h"

Application* g_theApp;

QStyle* GetStyle(Application* theApp)
{
    bool useHighlightCols = false;
    QStyle* style = NULL;

    ApplicationPreferences* applicationPreferences = theApp->GetApplicationPreferences();
    const CName& viewStyle = applicationPreferences->GetViewStyle();

    if(viewStyle == "QWindowsStyle")
    {
        style = new QWindowsStyle;
    }
    else
    if(viewStyle == CName("QPlatinumStyle"))
    {
        style = new QPlatinumStyle;
    }
    else
    if(viewStyle == CName("QCDEStyle"))
    {
        style = new QCDEStyle(useHighlightCols);
    }
    else
    {
        //
        // SGI is default
        // 
        style = new QSGIStyle(useHighlightCols);
    }

    return(style);
}

int mainQT(Application* drvAppDataBase, int argc, char ** argv )
{
    if(drvAppDataBase == 0)
    {
        return(-1);
    }

    //
    // bind the Application 
    //
    g_theApp = drvAppDataBase;

    QApplication a( argc, argv );

    QStyle* style = GetStyle(g_theApp);
    if(style)
    {
        QApplication::setStyle( style );
    }

    //
    // ProperRelative paths
    //
    char directorybuffer[1024];
    _getcwd(directorybuffer, 1024);
    strcat(directorybuffer, "\\DrVart");
    QMimeSourceFactory::defaultFactory()->addFilePath( QString(directorybuffer) );

    ActionTree w;
    w.show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );

    int executeReturn = a.exec();

    return(executeReturn);
}
