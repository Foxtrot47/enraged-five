#ifndef SCROLLVIEWDIALOG_H
#define SCROLLVIEWDIALOG_H

#include <qsizegrip.h>
#include <qscrollview.h>
#include <qlayout.h>
#include <qvbox.h>
#include <qhbox.h>

#include <qlabel.h>
#include <qsplitter.h> 

//---------------------------------------------------------------------------
// ScrollViewDialog... 
// you can add widgets to this view and it will control
// scrolling those widgets vertically
//---------------------------------------------------------------------------
class ScrollViewDialog : public QWidget 
{
    Q_OBJECT

public:
    ScrollViewDialog(QWidget* parent=0, const char* name=0, WFlags fl=0) :
	QWidget(parent,name,fl)
    {

        //
		// This makes the scrollView resize to the parent widget
		//
		QVBoxLayout* vbox = new QVBoxLayout(this);

        //
        // This is the "ToolBar" frame... just for adding buttons and shit
        //
        m_ToolBarFrame = new QFrame(this);
        m_ToolBarFrame->setMinimumSize(0, 30);
        m_ToolBarFrame->setMaximumSize(32767, 30);


        //
        // This is where all the scrollable contents go
        //
		m_scrollView = new QScrollView(this);

        //
        // Now part of the main widget
        //
        vbox->addWidget(m_ToolBarFrame);
		vbox->addWidget(m_scrollView);
		vbox->activate();

		//
		// This is the widget that holds all the added widgets
		//
        initScrollView();
	}

	QWidget * viewport () const
	{
        //return(m_scrollView->viewport());
		return(m_Frame);
	}

	virtual void addChild(QWidget* child, int x = 0, int y = 0)
	{
        //m_FrameLayout->insertWidget( m_NumWidgetsInLayout - 1, child); 
        //m_scrollView->addChild(child);
        m_scrollViewVbox->insertWidget( m_NumWidgetsInLayout - 1, child); 
        m_NumWidgetsInLayout++;
	}
	
	void clear(void)
	{
        m_FrameLayout->removeChild(m_Frame);
		delete m_Frame;

        recreateFrame();
	}

    virtual void ActiveLayout(void)
    {
        m_FrameLayout->activate();
    }

    virtual bool close( bool alsoDelete ); 

signals:
    void closedSignal(bool on_off);

protected:

    void initScrollView(void)
    {
        m_FrameLayout = new QVBoxLayout(m_scrollView->viewport());
  //      m_FrameLayout->addItem ( new QSpacerItem (0, 20) );
  //      m_NumWidgetsInLayout = 1;

        recreateFrame();
    }

	void recreateFrame(void)
	{
		//
		// The Widget that will activate the scroll
		//
		m_Frame = new QFrame(m_scrollView->viewport());
/*
		QSizePolicy sp(QSizePolicy::Minimum, QSizePolicy::Minimum);
		m_Frame->setSizePolicy(sp);
		QSizePolicy sp2(QSizePolicy::Expanding, QSizePolicy::Expanding);
		m_scrollView->setSizePolicy(sp2);
*/
        m_FrameLayout->addWidget(m_Frame);  // now will stretch
        m_scrollView->addChild(m_Frame);    // now scrollable
    
				
		//
		// This is the Layout that sorts the addedWidgets
		//
		m_scrollViewVbox = new QVBoxLayout(m_Frame); 
        m_scrollViewVbox->addItem ( new QSpacerItem (0, 20) );
        m_NumWidgetsInLayout = 1;
        
        //
        // Done now show
        //
        m_Frame->show();
	}

    int m_NumWidgetsInLayout;
	QScrollView* m_scrollView;
	QSizeGrip* corner;
	QVBoxLayout* m_scrollViewVbox;
	QVBoxLayout* m_FrameLayout;
	QFrame* m_Frame;
    QFrame* m_ToolBarFrame;

};	    

inline bool ScrollViewDialog::close( bool alsoDelete )
{
    closedSignal(false); //signal
    return(QWidget::close(alsoDelete));
}

//---------------------------------------------------------------------------
// ScrollViewHorizontal... 
// you can add widgets to this view and it will control
// scrolling those widgets horizontally
//---------------------------------------------------------------------------
class ScrollViewHorizontal : public QWidget 
{
    Q_OBJECT

public:
    ScrollViewHorizontal(QWidget* parent=0, const char* name=0, WFlags fl=0) :
	QWidget(parent,name,fl)
    {

        //
		// This makes the scrollView resize to the parent widget
		//
		QHBoxLayout* hbox = new QHBoxLayout(this);

        //
        // This is the "ToolBar" frame... just for adding buttons and shit
        //
        m_ToolBarFrame = new QFrame(this);
        m_ToolBarFrame->setMinimumSize(0, 30);//30);
        m_ToolBarFrame->setMaximumSize(32767, 30);//30);


        //
        // This is where all the scrollable contents go
        //
		m_scrollView = new QScrollView(this);

        //
        // Now part of the main widget
        //
        hbox->addWidget(m_ToolBarFrame);
		hbox->addWidget(m_scrollView);
		hbox->activate();

		//
		// This is the widget that holds all the added widgets
		//
        initScrollView();
	}

	QWidget * viewport () const
	{
        //return(m_scrollView->viewport());
		return(m_Frame);
	}

	virtual void addChild(QWidget* child, int x = 0, int y = 0)
	{
        //m_FrameLayout->insertWidget( m_NumWidgetsInLayout - 1, child); 
        //m_scrollView->addChild(child);
        m_scrollViewHbox->insertWidget( m_NumWidgetsInLayout - 1, child); 
        m_NumWidgetsInLayout++;
	}
	
	virtual void remomeChild(QWidget* child)
	{
		m_scrollViewHbox->remove( child );
		m_NumWidgetsInLayout--;
	}

	void clear(void)
	{
        m_FrameLayout->removeChild(m_Frame);
		delete m_Frame;

        recreateFrame();
	}

    virtual void ActiveLayout(void)
    {
        m_FrameLayout->activate();
    }

    virtual bool close( bool alsoDelete ); 

signals:
    void closedSignal(bool on_off);

protected:

    void initScrollView(void)
    {
        m_FrameLayout = new QHBoxLayout(m_scrollView->viewport());
  //      m_FrameLayout->addItem ( new QSpacerItem (0, 20) );
  //      m_NumWidgetsInLayout = 1;

        recreateFrame();
    }

	void recreateFrame(void)
	{
		//
		// The Widget that will activate the scroll
		//
		m_Frame = new QFrame(m_scrollView->viewport());
        m_FrameLayout->addWidget(m_Frame);  // now will stretch
        m_scrollView->addChild(m_Frame);    // now scrollable
				
		//
		// This is the Layout that sorts the addedWidgets
		//
		m_scrollViewHbox = new QHBoxLayout(m_Frame); 
        m_scrollViewHbox->addItem ( new QSpacerItem (0, 20) );
        m_NumWidgetsInLayout = 1;
        
		//
        // Done now show
        //
        m_Frame->show();
	}

    int m_NumWidgetsInLayout;
	QScrollView* m_scrollView;
	QSizeGrip* corner;
	QHBoxLayout* m_scrollViewHbox;
	QHBoxLayout* m_FrameLayout;
	QFrame* m_Frame;
    QFrame* m_ToolBarFrame;

};	    

inline bool ScrollViewHorizontal::close( bool alsoDelete )
{
    closedSignal(false); //signal
    return(QWidget::close(alsoDelete));
}

#endif

