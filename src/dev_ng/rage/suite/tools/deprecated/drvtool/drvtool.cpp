#pragma	comment(lib, "../../libs/qt-mt312.lib")
#pragma	comment(lib, "../../libs/qtmain.lib")
  
#if !__TOOL  
#error "Use ToolDebug or ToolRelease Win32 configuration please!"
#endif

#include "mainQt.h"
#include "actiontree/Tracks.h"
#include "actiontree/Condition.h"
#include "drvToolinclude/application.h"
 
int main( int argc, char ** argv)
{
#if __TOOL
	Track::Init();
	Condition::Init();
	Application* drvAppDataBase = Application::CreateInstance();
	return mainQT( drvAppDataBase, argc, argv );
#else
	argc;
	argv;

	return 0;
#endif
}
