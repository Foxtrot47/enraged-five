#ifndef DATAMEMBERSSLIDERDERIVED_H
#define DATAMEMBERSSLIDERDERIVED_H


#include "generated/datamemberslider.h"
#include "generated/datamemberstring.h"
#include "generated/tracktimingsliding.h"
#include "generated/datamembervector3.h"
#include "actionTree/datamember.h"
#include "vector/vector3.h"
#include "drvToolInclude/application.h"
#include "ScrollViewDialog.h"
#include <qcombobox.h>
#include <QString.h>


class DataMemberIntSlider : public DataMemberSlider, public ApplicationDataMember , public IObserver
{
	Q_OBJECT
public:
	DataMemberIntSlider(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMemberIntSlider();

    void SetDataMember( IDataMembers * dataMember, IIntDataMember * dataMemberInterface );
    virtual void Update(CSubject* theChangedCSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
    virtual void setValue();

private:
    void syncronize(void);
    IIntDataMember* m_IIntDataMember;
    IDataMembers* m_dataMembersInstance;
};

class DataMemberFloatSlider : public DataMemberSlider, public ApplicationDataMember , public IObserver
{
    Q_OBJECT
public:
	DataMemberFloatSlider(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMemberFloatSlider();

    void SetDataMember( IDataMembers * dataMember, IFloatDataMember * dataMemberInterface );
    virtual void Update(CSubject* theChangedCSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
    virtual void setValue();

private:
	void setValue( float rawValue , bool updateSlider , bool updateText);
    void syncronize(void);

    IFloatDataMember* m_IFloatDataMember;
    IDataMembers* m_dataMembersInstance;
};


class DataMemberVector2Slider : public DataMemberVector3, public ApplicationDataMember, public IObserver
{
	Q_OBJECT
public:
	DataMemberVector2Slider(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0);
	~DataMemberVector2Slider();

	void SetDataMember( IDataMembers * dataMember, IVector2DataMember * dataMemberInterface );
	virtual void Update(CSubject* theChangedCSubject);
	virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
	void setValueX();
	void setValueY();
	void setValueZ() {}

private:
	void setValueX(float rawValue, bool updateText);
	void setValueY(float rawValue, bool updateText);
	void syncronize();
	void synchronizeX();
	void synchronizeY();

	IVector2DataMember* m_IVector2DataMember;
	IDataMembers* m_dataMembersInstance;
};


class DataMemberVector3Slider : public DataMemberVector3, public ApplicationDataMember, public IObserver
{
	Q_OBJECT
public:
	DataMemberVector3Slider(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0);
	~DataMemberVector3Slider();

	void SetDataMember( IDataMembers * dataMember, IVector3DataMember * dataMemberInterface );
	virtual void Update(CSubject* theChangedCSubject);
	virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
	void setValueX();
	void setValueY();
	void setValueZ();

private:
	void setValueX(float rawValue, bool updateText);
	void setValueY(float rawValue, bool updateText);
	void setValueZ(float rawValue, bool updateText);
	void syncronize();
	void synchronizeX();
	void synchronizeY();
	void synchronizeZ();

	IVector3DataMember* m_IVector3DataMember;
	IDataMembers* m_dataMembersInstance;
};


class DataMemberStringImp : public DataMemberString, public ApplicationDataMember, public IObserver
{
    Q_OBJECT
public:
	DataMemberStringImp(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMemberStringImp();

    void SetDataMember( IDataMembers * dataMember, IStringDataMember * dataMemberInterface );
    virtual void Update(CSubject* theChangedCSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
    virtual void SetValue( const QString & value );
    virtual void SetValue( void );
    virtual void textChanged( const QString & value );

private:
    void syncronize(void);
    IStringDataMember*  m_dataMemberInterface;
    IDataMembers*		m_dataMembersInstance;

	QColor defaultColor;
};

class DataMemberNodeRef : public DataMemberString, public ApplicationDataMember, public IObserver
{
    Q_OBJECT
public:
	DataMemberNodeRef(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMemberNodeRef();

    void SetDataMember( IDataMembers * dataMember, INodeReferenceDataMember* dataMemberInterface );
    virtual void Update(CSubject* theChangedCSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
    virtual void SetValue( const QString & value );
    virtual void SetValue( void );
    virtual void textChanged( const QString & value );
    virtual void OnButtonAction( void );

private:
    void syncronize(void);
    INodeReferenceDataMember*  m_dataMemberInterface;
	IDataMembers*			   m_dataMembersInstance;

	QColor defaultColor;
};

class DataMemberCommand : public DataMemberString, public ApplicationDataMember, public IObserver
{
    Q_OBJECT
public:
	DataMemberCommand(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMemberCommand();

    void SetDataMember( IDataMembers * dataMember, ICommandDataMember* dataMemberInterface );
    virtual void Update(CSubject* theChangedCSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
    virtual void SetValue( const QString & value );
    virtual void SetValue( void );
    virtual void textChanged( const QString & value );
    virtual void OnButtonAction( void );

private:
    void syncronize(void);
    ICommandDataMember*  m_dataMemberInterface;
    IDataMembers*		 m_dataMembersInstance;
};


class DataMemberTrackTimingSliding : public TrackTimingSliding, public ApplicationDataMember, public IObserver
{
    Q_OBJECT
public:
	DataMemberTrackTimingSliding(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMemberTrackTimingSliding();

    void SetDataMember( IDataMembers * dataMember );
    virtual void Update(CSubject* theChangedCSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);
    
    enum {MAX_TIME_DATAMEMBERS = 8};
    void AddTimeMember( IFloatDataMember * dataMemberInterface );

public slots:
//	virtual void increment( int percent );
//    virtual void setValue( const QString & value );
    virtual void update(void);

private:
	void setValue( float rawValue , bool updateSlider , bool updateText);

    void syncronize(void);
    IDataMembers* m_dataMembersInstance;
    IFloatDataMember* m_IFloatDataMember[MAX_TIME_DATAMEMBERS];

    int m_NumTimeDataMembers;
    int m_MaxTimeLine;

};

//--------------------------------------------------------------------------------
// DataMembersView
// This is the holder view for the set of DataMembers
// There are usually only 2 of these...
// 1)Tracks View
// 2)Condition View
//--------------------------------------------------------------------------------
class DataMembersView : public ScrollViewDialog
{
    Q_OBJECT

public:
	DataMembersView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMembersView(){}

    QWidget* CreateDataMemberWidget(IDataMembers *instance, DataMember* dataMember);
    void AddDataMembers( IDataMembers * instance );

	ViewProperties* m_ViewProperties;

public slots:
    virtual void refresh( IDataMembers * instance );
    virtual void refresh( void );

    virtual void Reset(void);

    //
    // This is just for haveing hierarchical DataMemberViews
    //
    virtual void ActiveLayout(void);
    DataMembersView* GetParentDataMembersView(void){return(m_ParentDataMembersView);}
    void SetParentDataMembersView(DataMembersView* parent){m_ParentDataMembersView = parent;}

    //
    // This is just to have a connection to some Properties data
    //
    void SyncWithViewProperies(void);
    void SaveViewProperies(void);

signals:
    virtual void updated(void);

protected:
    QPushButton* m_ResetButton;
    QLabel*   m_DataMemberName;

    //
    // This is only here to support the reset button...
    // This is kindof a hack
    //
    IDataMembers * m_Instance;

    DataMembersView* m_ParentDataMembersView;
};

//--------------------------------------------------------------------------------
// TimeDataMembersView
// This is the holder view for the set of Track Time DataMembers 
// There is usually only 1 of these...
// 1)Tracks Timings View
//--------------------------------------------------------------------------------
class TimeDataMembersView : public DataMembersView
{
    Q_OBJECT

public:
	TimeDataMembersView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 )
	:
	DataMembersView(parent, name, fl),
    m_ActionNode(NULL)
	{
        
	}
	~TimeDataMembersView(){}

    void SetActionNode(ActionNode* actionNode)
	{
		m_ActionNode = actionNode;
		refreshItems();
	}

//public slots:
  //  virtual void refresh( IDataMembers * instance );

private:
    void refreshItems();
    ActionNode*	m_ActionNode;

};


#endif

