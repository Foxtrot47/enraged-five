#ifndef SPEWWINDOW_H
#define SPEWWINDOW_H

#include <qtextedit.h> 
#include "allActionTree.h"
#include "drvToolInclude/application.h"

class SpewWindow : public QTextEdit , public IObserver
{
    Q_OBJECT
public:

    SpewWindow(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~SpewWindow();
    
    void initStyle(void);
    void appendPostTime(const QString& message);
    virtual void Update(CSubject* theChangedSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

    //
    // This is just to have a connection to some Properties data
    //
    ViewProperties* m_ViewProperties;
    void SyncWithViewProperies(void);
    void SaveViewProperies(void);

    virtual bool close( bool alsoDelete ); 

signals:
    void closedSignal(bool on_off);
};

inline bool SpewWindow::close( bool alsoDelete )
{
    closedSignal(false); //signal
    return(QWidget::close(alsoDelete));
}

















#endif