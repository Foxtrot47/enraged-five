#ifndef ACTIONCONTROLLERVEIW_H
#define ACTIONCONTROLLERVEIW_H

#include "allActionTree.h"
#include "../../drvToolInclude/application.h"
#include "ActionNodeView.hpp"
#include "ScrollViewDialog.h"
#include <qlistview.h>

class QPopupMenu;

//---------------------------------------------------------------------------
// ObjectParameter... 
// it Really is a definition of a simple game object... 
// m_Type Could be a "float" or something like that... but right now 
// we are only using m_Type for ActionControllers and MotionTrees
// m_Name is the instance variable name like "m_ActionController"
// NOTE: ObjectParameter and EntityObjectType could be unifed with
// EntityObjectType being a Composite of ObjectParameter to allow 
// a hierarchical data representation... but that's a later expantion.
//---------------------------------------------------------------------------
class ObjectParameter
{
public:
    ObjectParameter(TextParser& message);
    CBasicString m_Type;
    CBasicString m_Name;
};

//---------------------------------------------------------------------------
// ObjectParameterViewControl... 
// See... ObjectParameter but basically it's a checkbox that turns on 
// view of an actionController or a motionTree
//---------------------------------------------------------------------------
class ObjectParameterHistoryView;
class ObjectParameterViewControl : public QCheckListItem
{
public:
	ObjectParameterViewControl( QListViewItem *parent, ObjectParameter* objectParameter );
	ObjectParameterHistoryView* m_View;

protected:
	//
	// This is the actual QT handler
	//
    virtual void stateChange( bool );

};

//---------------------------------------------------------------------------
// EntityObjectType... 
// it Really is a definition of a simple game object
// in the case of Rage it could describe an actor with a set of ObjectParameters
// (members really)
//---------------------------------------------------------------------------
class EntityObjectType
{
public:
    EntityObjectType(TextParser& message);
    CBasicString m_Name;
    std::vector<ObjectParameter*>m_Parameters;
};

//---------------------------------------------------------------------------
// EntityObjectInstanceQListViewItem... 
// it Really is an instance of a simple game object
// in the case of Rage it could describe an actor Instance
//---------------------------------------------------------------------------
class EntityObjectInstanceQListViewItem : public QListViewItem
{
public:
    EntityObjectInstanceQListViewItem(QListViewItem* parent, QString text);
	~EntityObjectInstanceQListViewItem(){}
    
 
	void SetFocus(bool tf);

private:

	bool m_IsFocus;
};


//---------------------------------------------------------------------------
// QListViewItemParameterStore... 
// it Really is an actionController/Motion tree Display
//---------------------------------------------------------------------------
class QListViewItemParameterStore : public QListViewItem
{
public:
    QListViewItemParameterStore(QListViewItem* parent, QString text);  
    ~QListViewItemParameterStore()
    {
        clearHistory();
    }
    void Update();
	void UpdateMT();
    void Playing(CBasicString* playInfo);
    void clearHistory(void);
	
	CBasicString* GetHistory(int i)
	{
		int numPlayUpdates = m_PlayHistory.size();
		if(	i < numPlayUpdates)
		{
			//since from the bottom
			numPlayUpdates -= 1; // the last entry
			numPlayUpdates -= i;

			return(m_PlayHistory[numPlayUpdates]);
		}
		else
		{
			return(NULL);
		}
	}
	
	// this really means it has new shit... 
	bool NeedsUpdate(void){return(m_Updated);}

	QListViewItemParameterStore* m_NextHistoryItem;

private:
	
    QListViewItem* m_CurrentPlay;

    bool m_sem;
    std::vector<CBasicString*>m_PlayHistory;
    bool m_Updated; // it has new data to be displayed... not a good name
};

inline void QListViewItemParameterStore::clearHistory(void)
{
    std::vector<CBasicString*>::iterator msgIT = m_PlayHistory.begin();
    while(m_PlayHistory.size()>0)
    {
        msgIT = m_PlayHistory.begin();
        CBasicString* playInfo = *msgIT;
        m_PlayHistory.erase(msgIT);
        delete playInfo;
    }
}

//---------------------------------------------------------------------------
// ActionControllerOwnerView... 
// it Really is just a listing of EntityObjectType instances in the game.
// ObjectParameters of the EntityObjectType are setup with a checkbox to
// tell the game to send back information about that ObjectParameter...
// Right now just the MotionTree and ActionController types are availible.
//---------------------------------------------------------------------------
class ActionControllerOwnerView : public QWidget , public IObserver
{
    Q_OBJECT

public:
	enum ColumnTypes
	{
		COL_TYPE,
		COL_MODEL,
		COL_POINTER,
		NUM_COL_TYPES,
	};

public:

    ActionControllerOwnerView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~ActionControllerOwnerView();

    void initStyle(void);
    void Update(void);
    virtual void Update(CSubject* theChangedSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);
    virtual bool HandleMessage(CBasicString* msg);

    //
    // This is just to have a connection to some Properties data
    //
    ViewProperties* m_ViewProperties;
    void SyncWithViewProperies(void);
    void SaveViewProperies(void);

    virtual bool close( bool alsoDelete ); 
    
    //
    // This can be called by multiple Threads
    //
    void RecieveMessage(CBasicString* msg);
    void handleMessage(CBasicString* msg);
  
	int  GetActionControllerHistoryLevel(void);
	int  GetMotionTreeHistoryLevel(void);

	void Clear();

signals:
    void closedSignal(bool on_off);

public slots:
    void rightButtonPressedHandler( QListViewItem* item, const QPoint& selectionPoint, int column);
    void clickedHandler( QListViewItem* item, const QPoint& selectionPoint, int column);
    void refreshFromSelected(void);
    void playOnSelected(void);
	void setFocusActor();
	void SetActionControllerHistoryLevel(int level);
	void SetMotionTreeHistoryLevel(int level);
	void SetActionControllerViewSize(int width);
	void SetMotionTreeViewSize(int width);

private:
	void ClearFocusActors();

protected:

	// for controlling history levels
    QLabel* m_ActionControllerHistoryLevelLabel;
    QSpinBox* m_ActionControllerHistoryLevel;
	QLabel* m_MotionTreeHistoryLevelLabel;
    QSpinBox* m_MotionTreeHistoryLevel;

	// for controlling history views min veiwsize
    QLabel* m_ActionControllerViewSizeLabel;
    QSpinBox* m_ActionControllerViewSize;
	QLabel* m_MotionTreeViewSizeLabel;
    QSpinBox* m_MotionTreeViewSize;

	QListView*  m_ItemListView;
    QPopupMenu* m_ObjectClassPopupMenu;
    QPopupMenu* m_ObjectPopupMenu;
    
    QGridLayout* FormLayout;
    QHBoxLayout* layoutMT;
    QHBoxLayout* layoutAT;
    QHBoxLayout* layoutViewMT;
    QHBoxLayout* layoutViewAT;

    void getObjectInfo
    (
		TextParser& message, 
		QString& objectType, 
		QString& pointerHandle,
		QString& modelName 
    );

    void OnUpdate(QListViewItem* objectClassName, TextParser& message);
    void createGameObjectType(TextParser& message);
    void createGameObject(QListViewItem* objectClassName, TextParser& message);
    void createGameObject
    (
		QListViewItem* objectClassName,
		QString& objectType,
		QString& pointerHandle,
		QString& modelName 
    );

    void deleteGameObject(QListViewItem* objectClassName, TextParser& message);
    void deleteGameObject
    (
        QListViewItem* objectClassName,
        QString& objectType,
        QString& pointerHandle,
        QString& modelName 
    );

    bool m_sem;
    std::vector<CBasicString*>m_MessageBuff;
    bool m_Updated;

    std::vector<EntityObjectType*>m_EntityObjectType;
};

inline bool ActionControllerOwnerView::close( bool alsoDelete )
{
    closedSignal(false); //signal
    return(QWidget::close(alsoDelete));
}

//
// Abstract a historyViewtype that can handle viewing history information
// ActionTreeViews and MotionTreeViews
//
class ObjectParameterHistoryView : public ScrollViewHorizontal
{
	Q_OBJECT
public:

	ObjectParameterHistoryView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );

	//
	// Add/remove an item to watch
	//
	virtual void AddFullPathCopy(QListViewItem* objectParameter) = 0;
	virtual void RemoveFullPathCopy(QListViewItem* objectParameter) = 0;

	//
	// Control history viewing
	//
	virtual void SetHistoryLevel(int count) = 0;
	virtual int  GetHistoryLevel(void) = 0;

	int m_MinWidth;

	virtual void setminwidth(int w);

	virtual bool DoesItemExist(QString& guidNumber, QString& controllerName) = 0;

};


//---------------------------------------------------------------------------
// ActionControllerView... 
// Since the ActionControllerOwnerView would get unrully if it displayed
// ActionController info in the same view it gets dispatched to this view
//---------------------------------------------------------------------------
class ActionControllerView : public ObjectParameterHistoryView
{
    Q_OBJECT
public:

    ActionControllerView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~ActionControllerView();

    void initStyle(void);
    void Update(void);

	void ClearAllViews();

	virtual bool DoesItemExist(QString& guidNumber, QString& controllerName);

    //
    // This is just to have a connection to some Properties data
    //
    ViewProperties* m_ViewProperties;
    void SyncWithViewProperies(void); 
    void SaveViewProperies(void);

    //
    // This can be called by multiple Threads
    //
    void OnUpdate(QListViewItem* objectClassName, TextParser& message);

	//
	// Add/remove an item to watch
	//
	virtual void AddFullPathCopy(QListViewItem* objectParameter);
	virtual void RemoveFullPathCopy(QListViewItem* objectParameter);

	//
	// Control history viewing
	//
	virtual void SetHistoryLevel(int count);
	virtual int  GetHistoryLevel(void);

	virtual void setminwidth(int w);

protected:



public slots:
    void playOnSelected(void);

private:

    bool m_sem;
    std::vector<CBasicString*>m_PlayHistory;
    bool m_Updated;
	
	std::vector<ActionNodeListView*>m_PlayHistoryView;
};

#endif //ACTIONCONTROLLERVEIW_H