#include "datamemberslider.h"
#include "\Proto\testApp\slidermain\ActionTree\datamember.h"

class DataMemberFloatSlider : public DataMemberSlider, public ApplicationDataMember
{
	Q_OBJECT
public:
	DataMemberFloatSlider(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMemberFloatSlider(){}

    void SetDataMember( IDataMembers * dataMember, IFloatDataMember * dataMemberInterface );

public slots:
    virtual void setValue( const QString & value );

private:
    IFloatDataMember* m_IFloatDataMember;
    IDataMembers* m_dataMembersInstance;
};
